﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;
using Wisej.Web.Ext.CustomProperties;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmAccountListing.
	/// </summary>
	public partial class frmAccountListing : BaseForm
	{
		public frmAccountListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAccountListing InstancePtr
		{
			get
			{
				return (frmAccountListing)Sys.GetInstance(typeof(frmAccountListing));
			}
		}

		protected frmAccountListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/09/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/20/2006              *
		// ********************************************************
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolSaveReport;
		bool boolLoaded;
		string strGridToolTipText = "";
		string strBookList;
		// these will be to pass the string on to the reports when needed
		public string strRSWhere = string.Empty;
		public string strRSFrom = "";
		public string strRSOrder = string.Empty;
		public string strMeterSort = "";
		public string strBinaryWhere = "";
		public int lngMax;
		// Public boolFullStatusAmounts    As Boolean
		// Public boolShowCurrentInterest  As Boolean
		public int intShowOwnerType;
		public int intMasterReportType;
		public string strListSQL = "";
		public bool boolShowUTBookZero;
		public int lngRowWS;
		// Service
		public int lngRowAccount;
		public int lngRowOwnerName;
		public int lngRowTenantName;
		public int lngRowBillMessage;
		public int lngRowBillType;
		public int lngRowBillToSameAsOwner;
		public int lngRowCombinationType;
		public int lngRowDataEntryMessage;
		public int lngRowMeterFinalBill;
		public int lngRowAcctFinalBill;
		public int lngRowMeterSize;
		public int lngRowMeterNoBill;
		public int lngRowAcctNoBill;
		public int lngRowREAccount;
		public int lngRowMasterOverrideS;
		public int lngRowRateTableS;
		public int lngRowCategoryS;
		public int lngRowMasterOverrideW;
		public int lngRowRateTableW;
		public int lngRowCategoryW;
		public int lngRowUseRE;
		public int lngRowWaterTax;
		public int lngRowSewerTax;
		public int lngRowSerialNumber;
		public int lngRowRemoteNumber;
		public int lngRowLongitude;
		public int lngRowLatitude;
		public int lngRowMXUNumber;
		public int lngRowReadType;
		public int lngTotalWhereRows;

		public void Init(ref string strPassBookList)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				strBookList = strPassBookList;
				// kk08042015 trouts-155
				boolShowUTBookZero = false;
				clsDRWrapper rsZChk = new clsDRWrapper();
				rsZChk.OpenRecordset("SELECT TOP 1 ID FROM Master WHERE UTBook = 0 OR UTBook IS NULL", modExtraModules.strUTDatabase);
				if (!rsZChk.BeginningOfFile() && !rsZChk.EndOfFile())
				{
					if (MessageBox.Show("Some accounts are not assigned a book. Would you like a listing of these accounts?", "Unassigned Accounts", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
						boolShowUTBookZero = true;
					}
				}
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PrintReport()
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				// set the cursor to an hourglass
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				vsWhere.Select(0, 0);
				if (!ValidateWhereGrid())
				{
					return;
				}
				// SetExtraFields
				if (BuildSQL())
				{
					// SHOW THE REPORT
					if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (cmbHardCode.SelectedIndex > -1)
						{
							ShowHardCodedReport_2(FCConvert.ToInt16(cmbHardCode.ItemData(cmbHardCode.SelectedIndex)));
						}
						else
						{
							MessageBox.Show("Please select a report to show.", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
							cmbHardCode.Focus();
						}
					}
					else
					{
						frmReportViewer.InstancePtr.Init(arAcctListing.InstancePtr);
					}
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				return;
			}
			catch (Exception ex)
			{
				
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("ERROR #:" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Print Status Lists ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public bool BuildSQL()
		{
			bool BuildSQL = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
				int intCounter;
				string strPaymentDateRage = "";
				string strSort;
				string strWhere;
				int lngRow;
				string strField = "";
				// This field name just makes it easier to duplicate/manipulate code and is set to the field name in the database when entering a case
				int lngValue = 0;
				string strTemp = "";
				strSort = " ";
				strMeterSort = " ";
				// kk08042015 trouts-155  UTBook = 0 is confusing - using MeterTable.BookNumber and Master.UTBook = 0  -   strWhere = "((" & strBookList & ") OR UTBook = 0)"
				strWhere = "(" + strBookList + ")";
				vsWhere.Select(0, 0);
				// GET THE FIELDS TO SORT BY
				for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
				{
					if (lstSort.Selected(intCounter))
					{
						// it is checked
						switch (lstSort.ItemData(intCounter))
						{
							case 0:
								{
									// "Account Number"           0
									strSort += "AccountNumber, ";
									break;
								}
							case 1:
								{
									// "Bill To Name"             1
									strSort += "Name, ";
									break;
								}
							case 2:
								{
									// "Book"                     2
									strSort += "BookNumber, ";
									break;
								}
							case 3:
								{
									// "Location"                 3
									strSort += "StreetName, Convert(StreetNumber,int), ";
									break;
								}
							case 4:
								{
									// "Meter Size"               4
									strSort += "Size, ";
									strMeterSort += "Size, ";
									break;
								}
							case 5:
								{
									// "Owner Name"               5
									strSort += "OwnerName, ";
									break;
								}
							case 6:
								{
									// "Real Estate Account"      6
									strSort += "REAccount, ";
									break;
								}
							case 7:
								{
									// "Sequence"                 7
									strSort += "Sequence, ";
									strMeterSort += "Sequence, ";
									break;
								}
							case 8:
								{
									// "Service"                  8
									strSort += "Service, ";
									strMeterSort += "Service, ";
									break;
								}
							case 9:
								{
									// "Sewer Category"           9
									// DJW 07/12/2009 When showing size and cat and sorting by those fields this cause an error because SCat is in metertable not SewerCategory
									strSort += "SCat, ";
									strMeterSort += "SCat, ";
									// strSort = strSort & "SewerCategory, "
									// strMeterSort = strMeterSort & "SewerCategory, "
									break;
								}
							case 10:
								{
									// "Water Category"           10
									strSort += "WCat, ";
									strMeterSort += "WCat, ";
									// strSort = strSort & "WaterCategory, "
									// strMeterSort = strMeterSort & "WaterCategory, "
									break;
								}
							case 11:
								{
									// "Serial Number"                  8
									strSort += "SerialNumber, ";
									strMeterSort += "SerialNumber, ";
									break;
								}
							case 12:
								{
									// "Remote Number"                  8
									strSort += "Remote, ";
									strMeterSort += "Remote, ";
									break;
								}
							case 13:
								{
									// "MXU Number"                  8
									strSort += "MXU, ";
									strMeterSort += "MXU, ";
									break;
								}
						}
						//end switch
					}
				}
				if (Strings.Trim(strMeterSort) != "")
				{
					strMeterSort = Strings.Left(strMeterSort, strMeterSort.Length - 2);
					// take the trailing comma off
					strMeterSort = " ORDER BY " + strMeterSort;
				}
				else
				{
					strMeterSort = " ORDER BY Sequence";
				}
				if (Strings.Trim(strSort) != "")
				{
					strSort = Strings.Left(strSort, strSort.Length - 2);
					// take the trailing comma off
					strSort = " ORDER BY " + strSort;
				}
				else
				{
					strSort = " ORDER BY Name, AccountNumber, Sequence";
				}
				// create the where string here
				for (lngRow = 0; lngRow <= lngTotalWhereRows - 1; lngRow++)
				{
					if (lngRow == lngRowWS)
					{
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Service
							string vbPorterVar = vsWhere.TextMatrix(lngRow, 1);
							if (vbPorterVar == "Sewer")
							{
								strWhere += " AND ";
								strWhere += "Service <> 'W'";
							}
							else if (vbPorterVar == "Water")
							{
								strWhere += " AND ";
								strWhere += "Service <> 'S'";
							}
							else if (vbPorterVar == "Sewer Only")
							{
								strWhere += " AND ";
								strWhere += "Service = 'S'";
							}
							else if (vbPorterVar == "Water Only")
							{
								strWhere += " AND ";
								strWhere += "Service = 'W'";
							}
						}
					}
					else if (lngRow == lngRowAccount)
					{
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Account Number
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) != "")
							{
								strWhere += "AccountNumber <= " + vsWhere.TextMatrix(lngRow, 2) + " AND AccountNumber  >= " + vsWhere.TextMatrix(lngRow, 1);
							}
							else
							{
								strWhere += "AccountNumber  >= " + vsWhere.TextMatrix(lngRow, 1);
							}
						}
						else
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								strWhere += " AND ";
								strWhere += "AccountNumber  <= " + vsWhere.TextMatrix(lngRow, 2);
							}
						}
					}
					else if (lngRow == lngRowOwnerName)
					{
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Name
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) == "")
							{
								strWhere += " AND ";
								// kgk 07-30-2012           strWhere = strWhere & "Name >= '" & .TextMatrix(lngRow, 1) & "    ' AND Name < '" & .TextMatrix(lngRow, 1) & "zzzz'"
								strWhere += "pBill.FullNameLF >= '" + vsWhere.TextMatrix(lngRow, 1) + "    ' AND pBill.FullNameLF < '" + vsWhere.TextMatrix(lngRow, 1) + "zzzz'";
							}
							else
							{
								// this is when they both have values in the fields and will be a range by name
								strWhere += " AND ";
								// kgk 07-30-2012           strWhere = strWhere & "Name >= '" & .TextMatrix(lngRow, 1) & "    ' AND Name < '" & .TextMatrix(lngRow, 2) & "zzzz'"
								strWhere += "pBill.FullNameLF >= '" + vsWhere.TextMatrix(lngRow, 1) + "    ' AND pBill.FullNameLF < '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz'";
							}
						}
					}
					else if (lngRow == lngRowBillMessage)
					{
					}
					else if (lngRow == lngRowBillType)
					{
						strField = "Type";
						if (vsWhere.TextMatrix(lngRow, 1).Length > 0)
						{
							if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2)) == "CO")
							{
								lngValue = 1;
							}
							else if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2)) == "FL")
							{
								lngValue = 2;
							}
							else if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2)) == "UN")
							{
								lngValue = 3;
							}
							else if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2)) == "AD")
							{
								// this will skip the rest of the processing
								// but should add the string for adjustments
								lngValue = 4;
							}
							else
							{
								// this will skip the rest of the processing
								lngValue = 0;
							}
							if (lngValue != 0)
							{
								// Bill Type
								strWhere += " AND (";
								if (lngValue < 4)
								{
									// Consumption, Flat and Units
									for (intCounter = 1; intCounter <= 5; intCounter++)
									{
										strWhere += "Sewer" + strField + FCConvert.ToString(intCounter) + " = " + FCConvert.ToString(lngValue) + " OR Water" + strField + FCConvert.ToString(intCounter) + " = " + FCConvert.ToString(lngValue) + " OR ";
									}
									// take off the last OR and add a closing parenthesis
									strWhere = Strings.Left(strWhere, strWhere.Length - 4) + ")";
								}
								else
								{
									// Adjustment
									strWhere += "(SewerAdjustAmount <> 0 Or WaterAdjustAmount <> 0)";
								}
							}
						}
					}
					else if (lngRow == lngRowBillToSameAsOwner)
					{
						// DJW@01142013 TROUT-885 changed SameAsOwner to SameBillOwner
						strField = "SameBillOwner";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "" || Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 4))) == "BOTH")
						{
							// MeterTable.FinalBill
							if (Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2))) == "YE")
							{
								// TRUE
								strWhere += " AND ";
								strWhere += "ISNULL(" + strField + ",0) = 1";
							}
							else
							{
								// FALSE
								strWhere += " AND ";
								strWhere += "ISNULL(" + strField + ",0) = 0";
							}
						}
					}
					else if (lngRow == lngRowCombinationType)
					{
						strField = "Combine";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Combination Type
							strWhere += " AND " + strField + " = '" + Strings.Left(vsWhere.TextMatrix(lngRow, 1), 1) + "'";
						}
					}
					else if (lngRow == lngRowDataEntryMessage)
					{
					}
					else if (lngRow == lngRowMeterFinalBill)
					{
						strField = "FinalBilled";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "" || Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 4))) == "BOTH")
						{
							// MeterTable.FinalBill
							strWhere += " AND ";
							if (Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2))) == "FI")
							{
								// TRUE
								strWhere += "ISNULL(" + strField + ",0) = 1";
							}
							else
							{
								// FALSE
								strWhere += "ISNULL(" + strField + ",0) = 0";
							}
						}
					}
					else if (lngRow == lngRowAcctFinalBill)
					{
						strField = "FinalBill";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "" || Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 4))) == "BOTH")
						{
							// Master.FinalBill
							strWhere += " AND ";
							if (Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2))) == "FI")
							{
								// TRUE
								strWhere += "ISNULL(" + strField + ",0) = 1";
							}
							else
							{
								// FALSE
								strWhere += "ISNULL(" + strField + ",0) = 0";
							}
						}
					}
					else if (lngRow == lngRowMeterSize)
					{
						strField = "Size";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Meter Size
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) != "")
							{
								strWhere += strField + " <= " + vsWhere.TextMatrix(lngRow, 2) + " AND " + strField + " >= " + vsWhere.TextMatrix(lngRow, 1);
							}
							else
							{
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1);
							}
						}
						else
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								strWhere += " AND ";
								strWhere += strField + " <= " + vsWhere.TextMatrix(lngRow, 2);
							}
						}
					}
					else if (lngRow == lngRowMeterNoBill)
					{
						strField = "MeterTable.NoBill";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "" || Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 4))) == "BOTH")
						{
							// MeterTable.NoBill
							strWhere += " AND ";
							if (Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2))) == "NO")
							{
								// TRUE
								strWhere += "ISNULL(" + strField + ",0) = 1";
							}
							else
							{
								// FALSE
								strWhere += "ISNULL(" + strField + ",0) = 0";
							}
						}
					}
					else if (lngRow == lngRowAcctNoBill)
					{
						strField = "Master.NoBill";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "" || Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 4))) == "BOTH")
						{
							// Master.NoBill
							strWhere += " AND ";
							if (Strings.UCase(Strings.Trim(Strings.Left(vsWhere.TextMatrix(lngRow, 1), 2))) == "NO")
							{
								// TRUE
								strWhere += "ISNULL(" + strField + ",0) = 1";
							}
							else
							{
								// FALSE
								strWhere += "ISNULL(" + strField + ",0) = 0";
							}
						}
					}
					else if (lngRow == lngRowREAccount)
					{
						strField = "REAccount";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// RE Account
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) == "")
							{
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " < " + vsWhere.TextMatrix(lngRow, 1);
							}
							else
							{
								// this is when they both have values in the fields and will be a range by name
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " < " + vsWhere.TextMatrix(lngRow, 2);
							}
						}
					}
					else if (lngRow == lngRowMasterOverrideS)
					{
						strTemp = Strings.Trim(vsWhere.TextMatrix(lngRow, 1).Replace("_", ""));
						if (strTemp != "")
						{
							// Name
							// this is when they both have values in the fields and will be a range by name
							strWhere += " AND ";
							strWhere += "SewerAccount >= '" + strTemp + "    ' AND SewerAccount < '" + strTemp + "zzzz'";
						}
					}
					else if (lngRow == lngRowRateTableS)
					{
						strField = "SewerKey";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Sewer Table
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) == "")
							{
								strWhere += " AND (";
								for (intCounter = 1; intCounter <= 5; intCounter++)
								{
									strWhere += "(" + strField + FCConvert.ToString(intCounter) + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + FCConvert.ToString(intCounter) + " <= " + vsWhere.TextMatrix(lngRow, 1) + ") OR ";
								}
								// take off the last parenthesis and OR and add a closing parenthesis
								strWhere = Strings.Left(strWhere, strWhere.Length - 4) + ")";
							}
							else
							{
								// this is when they both have values in the fields and will be a range by name
								strWhere += " AND (";
								for (intCounter = 1; intCounter <= 5; intCounter++)
								{
									strWhere += "(" + strField + FCConvert.ToString(intCounter) + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + FCConvert.ToString(intCounter) + " <= " + vsWhere.TextMatrix(lngRow, 2) + ") OR ";
								}
								// take off the last parenthesis and OR and add a closing parenthesis
								strWhere = Strings.Left(strWhere, strWhere.Length - 4) + ")";
							}
						}
					}
					else if (lngRow == lngRowCategoryS)
					{
						strField = "SewerCategory";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Sewer Category
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) == "")
							{
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " <= " + vsWhere.TextMatrix(lngRow, 1);
							}
							else
							{
								// this is when they both have values in the fields and will be a range by name
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " <= " + vsWhere.TextMatrix(lngRow, 2);
							}
						}
					}
					else if (lngRow == lngRowMasterOverrideW)
					{
						strTemp = Strings.Trim(vsWhere.TextMatrix(lngRow, 1).Replace("_", ""));
						if (strTemp != "")
						{
							// W override account
							// this is when they both have values in the fields and will be a range by name
							strWhere += " AND ";
							strWhere += "WaterAccount >= '" + strTemp + "    ' AND WaterAccount < '" + strTemp + "zzzz'";
						}
					}
					else if (lngRow == lngRowRateTableW)
					{
						strField = "WaterKey";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Water Table
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) == "")
							{
								strWhere += " AND (";
								for (intCounter = 1; intCounter <= 5; intCounter++)
								{
									strWhere += "(" + strField + FCConvert.ToString(intCounter) + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + FCConvert.ToString(intCounter) + " <= " + vsWhere.TextMatrix(lngRow, 1) + ") OR ";
								}
								// take off the last parenthesis and OR and add a closing parenthesis
								strWhere = Strings.Left(strWhere, strWhere.Length - 4) + ")";
							}
							else
							{
								// this is when they both have values in the fields and will be a range by name
								strWhere += " AND (";
								for (intCounter = 1; intCounter <= 5; intCounter++)
								{
									strWhere += "(" + strField + FCConvert.ToString(intCounter) + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + FCConvert.ToString(intCounter) + " <= " + vsWhere.TextMatrix(lngRow, 2) + ") OR ";
								}
								// take off the last parenthesis and OR and add a closing parenthesis
								strWhere = Strings.Left(strWhere, strWhere.Length - 4) + ")";
							}
						}
					}
					else if (lngRow == lngRowCategoryW)
					{
						strField = "WaterCategory";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Water Category
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) == "")
							{
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " <= " + vsWhere.TextMatrix(lngRow, 1);
							}
							else
							{
								// this is when they both have values in the fields and will be a range by name
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " <= " + vsWhere.TextMatrix(lngRow, 2);
							}
						}
						// MAL@20071211
					}
					else if (lngRow == lngRowUseRE)
					{
						strField = "UseREAccount";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							strWhere += " AND ";
							//strWhere += strField + " = " + FCConvert.ToString(FCConvert.CBool(vsWhere.TextMatrix(lngRow, 1) == "Yes"));
                            if (vsWhere.TextMatrix(lngRow, 1).Trim().ToLower() == "no")
                            {
                                strWhere = strWhere + strField + " = 0";
                            }
                            else
                            {
                                strWhere = strWhere + strField + " = 1";
                            }
						}
						// MAL@20080527: Tracker Reference: 13713
					}
					else if (lngRow == lngRowWaterTax)
					{
						strField = "WaterTaxPercent";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Water Tax Percent
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) == "")
							{
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " <= " + vsWhere.TextMatrix(lngRow, 1);
							}
							else
							{
								// this is when they both have values in the fields and will be a range by value
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " <= " + vsWhere.TextMatrix(lngRow, 2);
							}
						}
						// MAL@20080527: Tracker Reference: 13713
					}
					else if (lngRow == lngRowSewerTax)
					{
						strField = "SewerTaxPercent";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Sewer Tax Percent
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) == "")
							{
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " <= " + vsWhere.TextMatrix(lngRow, 1);
							}
							else
							{
								// this is when they both have values in the fields and will be a range by value
								strWhere += strField + " >= " + vsWhere.TextMatrix(lngRow, 1) + " AND " + strField + " <= " + vsWhere.TextMatrix(lngRow, 2);
							}
						}
					}
					else if (lngRow == lngRowSerialNumber)
					{
						strField = "SerialNumber";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Account Number
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) != "")
							{
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz' AND " + strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
							else
							{
								strWhere += strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
						}
						else
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								strWhere += " AND ";
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz'";
							}
						}
					}
					else if (lngRow == lngRowRemoteNumber)
					{
						strField = "Remote";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Account Number
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) != "")
							{
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz' AND " + strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
							else
							{
								strWhere += strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
						}
						else
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								strWhere += " AND ";
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz'";
							}
						}
					}
					else if (lngRow == lngRowMXUNumber)
					{
						strField = "MXU";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Account Number
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) != "")
							{
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz' AND " + strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
							else
							{
								strWhere += strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
						}
						else
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								strWhere += " AND ";
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz'";
							}
						}
					}
					else if (lngRow == lngRowReadType)
					{
						strField = "RadioAccessType";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							strWhere += " AND ";
							if (vsWhere.TextMatrix(lngRow, 1) == "Manual")
							{
								strWhere += "ISNULL(" + strField + ",0) = 0";
							}
							else if (vsWhere.TextMatrix(lngRow, 1) == "Auto, Non Radio")
							{
								strWhere += "ISNULL(" + strField + ",0) = 1";
							}
							else
							{
								strWhere += "ISNULL(" + strField + ",0) = 2";
							}
						}
					}
					else if (lngRow == lngRowLatitude)
					{
						strField = "Lat";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Account Number
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) != "")
							{
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz' AND " + strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
							else
							{
								strWhere += strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
						}
						else
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								strWhere += " AND ";
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz'";
							}
						}
					}
					else if (lngRow == lngRowLongitude)
					{
						strField = "Long";
						if (Strings.Trim(vsWhere.TextMatrix(lngRow, 1)) != "")
						{
							// Account Number
							strWhere += " AND ";
							if (Strings.Trim(vsWhere.TextMatrix(lngRow, 2)) != "")
							{
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz' AND " + strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
							else
							{
								strWhere += strField + " >= '" + vsWhere.TextMatrix(lngRow, 1) + "    '";
							}
						}
						else
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								strWhere += " AND ";
								strWhere += strField + " <= '" + vsWhere.TextMatrix(lngRow, 2) + "zzzz'";
							}
						}
					}
				}
				if (Strings.Trim(strWhere) == "")
				{
					// kgk  XXXXX  DO WE NEED SecondOwner and Name2 in here???
					strListSQL = "SELECT AccountNumber, Deleted, StreetNumber, StreetName, Telephone, MapLot, UseREAccount, BillMessage, SameBillOwner, DataEntry, FinalBill, Master.NoBill, REAccount, SewerAccount, WaterAccount, SewerCategory, WaterCategory, " + "MeterTable.ID AS MeterKey, WaterTaxPercent, SewerTaxPercent, BookNumber, SerialNumber, Remote, MXU, RadioAccessType, Lat, Long, " + "DeedName1 AS OwnerName,DeedName2 as SecondOwnerName, OwnerPartyID, SecondOwnerPartyID, pBill.FullNameLF AS Name, BillingPartyID, SecondBillingPartyID " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN MeterTable ON Master.ID = MeterTable.AccountKey " + strSort;
				}
				else
				{
					strListSQL = "SELECT AccountNumber, Deleted, StreetNumber, StreetName, Telephone, MapLot, UseREAccount, BillMessage, SameBillOwner, DataEntry, FinalBill, Master.NoBill, REAccount, SewerAccount, WaterAccount, SewerCategory, WaterCategory, " + "MeterTable.ID AS MeterKey, WaterTaxPercent, SewerTaxPercent, BookNumber, SerialNumber, Remote, MXU, RadioAccessType, Lat, Long, " + "DeedName1 AS OwnerName, DeedName2 AS SecondOwnerName, pBill.FullNameLF AS Name, pBill2.FullNameLF AS Name2, BillingPartyID " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID LEFT JOIN MeterTable ON Master.ID = MeterTable.AccountKey WHERE " + strWhere + strSort;
				}
				BuildSQL = true;
				strRSWhere = strWhere;
				strRSOrder = strSort;
				return BuildSQL;
			}
			catch (Exception ex)
			{
				
				BuildSQL = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building SQL", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildSQL;
		}

		private void chkHardCode_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
			{
				// this turns the hard coded report on
				fraHardCode.Enabled = true;
				cmbHardCode.Enabled = true;
				fraSave.Enabled = false;
				if (cmbHardCode.Items.Count > 0)
				{
					// this should select the first item in the combo box
					cmbHardCode.SelectedIndex = 0;
				}
				fraSort.Enabled = false;
				lstSort.Enabled = false;
				fraFields.Enabled = false;
				lstFields.Enabled = false;
			}
			else
			{
				// this turns the hard coded reports off
				fraHardCode.Enabled = false;
				cmbHardCode.Enabled = false;
				cmbHardCode.SelectedIndex = -1;
				// fraSave.Enabled = True
				fraSort.Enabled = true;
				lstSort.Enabled = true;
				fraFields.Enabled = true;
				lstFields.Enabled = true;
			}
			ShowAutomaticFields();
		}

		private void chkHardCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						KeyCode = (Keys)0;
						if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
						{
							chkHardCode.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						else
						{
							chkHardCode.CheckState = Wisej.Web.CheckState.Checked;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbHardCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			ShowAutomaticFields();
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
			vsWhere.EditText = "";
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void cmdExit_Click()
		{
			cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void frmAccountListing_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
			}
			else
			{
				lngMax = 7;
				EnableFrames_2(true);
				//Application.DoEvents();
				Format_WhereGrid_2(true);
				ShowAutomaticFields();
				modAccountListing.SetupAccountListingCombos_2(true);
				Fill_Lists();
				boolLoaded = true;
			}
		}

		private void frmAccountListing_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Unload();
						break;
					}
				default:
					{
						if (Strings.UCase(this.ActiveControl.GetName()) == Strings.UCase("vsReceipt"))
						{
							if (vsWhere.Row == lngRowMasterOverrideS || vsWhere.Row == lngRowMasterOverrideW)
							{
								if (vsWhere.Col == 1 && (KeyCode < Keys.F1 || KeyCode > Keys.F12))
								{
									modNewAccountBox.CheckFormKeyDown(vsWhere, vsWhere.Row, vsWhere.Col, KeyCode, Shift, vsWhere.EditSelStart, vsWhere.EditText, vsWhere.EditSelLength);
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void frmAccountListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAccountListing properties;
			//frmAccountListing.ScaleWidth	= 9045;
			//frmAccountListing.ScaleHeight	= 7350;
			//frmAccountListing.LinkTopic	= "Form1";
			//frmAccountListing.LockControls	= true;
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			this.Text = "Account Listing";
			// 
			lngRowWS = 0;
			// Service
			lngRowAccount = 1;
			lngRowOwnerName = 2;
			lngRowTenantName = 3;
			lngRowBillMessage = 4;
			lngRowBillType = 5;
			lngRowBillToSameAsOwner = 6;
			lngRowCombinationType = 7;
			lngRowDataEntryMessage = 8;
			lngRowMeterFinalBill = 9;
			lngRowAcctFinalBill = 10;
			lngRowMeterSize = 11;
			lngRowMeterNoBill = 12;
			lngRowAcctNoBill = 13;
			lngRowREAccount = 14;
			lngRowMasterOverrideS = 15;
			lngRowRateTableS = 16;
			lngRowCategoryS = 17;
			lngRowSewerTax = 18;
			// MAL@20080527
			lngRowMasterOverrideW = 19;
			lngRowRateTableW = 20;
			lngRowCategoryW = 21;
			lngRowWaterTax = 22;
			// MAL@20080527: Also changed in-between rows to match correct order
			lngRowUseRE = 23;
			// MAL@20071211
			lngRowSerialNumber = 24;
			lngRowRemoteNumber = 25;
			lngRowMXUNumber = 26;
			lngRowReadType = 27;
			lngRowLatitude = 28;
			lngRowLongitude = 29;
			lngTotalWhereRows = 30;
			// MAL@20071211: Changed from 21 ; 'MAL@20080527: Changed from 22
			// this is for the grid that has the account box in it
			if (modGlobalConstants.Statics.gboolBD)
			{
				modValidateAccount.SetBDFormats();
			}
			vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			// fill the hard coded combo list with the completed reports
			FillHardCodeCombo();
			modGlobalFunctions.SetTRIOColors(this);
			//txtNotes.BackColor = this.BackColor;
			Set_Note_Text();
			modMain.Statics.gdtUTStatusListAsOfDate = DateTime.Today;
			// set the default as of date
		}

		private void frmAccountListing_Resize(object sender, System.EventArgs e)
		{
			Format_WhereGrid_2(false);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	bool boolSecondSelected = false;
		//	//FC:FINAL:DDU:#i866 - drag and drop not needed
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	//if (intStart != lstSort.SelectedIndex)
		//	//{
		//	//    // SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	//    strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
		//	//    intID = lstSort.ItemData(lstSort.SelectedIndex);
		//	//    boolSecondSelected = lstSort.Selected(lstSort.SelectedIndex);
		//	//    // CHANGE THE NEW ITEM
		//	//    lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//	//    lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//	//    // SAVE THE OLD ITEM
		//	//    lstSort.Items[intStart].Text = strTemp;
		//	//    lstSort.ItemData(intStart, intID);
		//	//    // SET BOTH ITEMS TO BE SELECTED
		//	//    lstSort.SetSelected(lstSort.ListIndex, true);
		//	//    lstSort.SetSelected(intStart, boolSecondSelected);
		//	//}
		//}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			PrintReport();
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			// vsWhere.TextMatrix(Row, 2) = vsWhere.ComboData
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
			{
				if (vsWhere.Row == lngRowREAccount || vsWhere.Row == lngRowAccount || vsWhere.Row == lngRowTenantName || vsWhere.Row == lngRowOwnerName || vsWhere.Row == lngRowWaterTax || vsWhere.Row == lngRowSewerTax || vsWhere.Row == lngRowSerialNumber || vsWhere.Row == lngRowRemoteNumber || vsWhere.Row == lngRowMXUNumber)
				{
					// do nothing
				}
				else if (vsWhere.Row == lngRowWS || vsWhere.Row == lngRowBillType || vsWhere.Row == lngRowBillToSameAsOwner || vsWhere.Row == lngRowCombinationType || vsWhere.Row == lngRowMeterFinalBill || vsWhere.Row == lngRowAcctFinalBill || vsWhere.Row == lngRowMeterNoBill || vsWhere.Row == lngRowAcctNoBill || vsWhere.Row == lngRowREAccount || vsWhere.Row == lngRowUseRE || vsWhere.Row == lngRowReadType)
				{
					// single field dropdown
					if (vsWhere.Col == 1)
					{
						vsWhere.ComboList = modUTStatusList.Statics.strComboList[vsWhere.Row, 0];
					}
				}
				else if (vsWhere.Row == lngRowRateTableS || vsWhere.Row == lngRowRateTableW || vsWhere.Row == lngRowCategoryS || vsWhere.Row == lngRowCategoryW || vsWhere.Row == lngRowMeterSize)
				{
					// range dropdowns
					if (vsWhere.Col == 1 || vsWhere.Col == 2)
					{
						vsWhere.ComboList = modUTStatusList.Statics.strComboList[vsWhere.Row, 0];
					}
				}
				else if (vsWhere.Row == lngRowMasterOverrideS || vsWhere.Row == lngRowMasterOverrideW)
				{
					if (vsWhere.Col == 1)
					{
						// only show the list if the user is in the first col
						// vsWhere.ComboList = strComboList(Row, 0)
						vsWhere.EditMaxLength = 17;
					}
				}
				else
				{
				}
			}
		}

		private void vsWhere_ChangeEdit(object sender, System.EventArgs e)
		{
			ShowAutomaticFields();
		}

		private void vsWhere_Enter(object sender, System.EventArgs e)
		{
			if (vsWhere.Row == lngRowMasterOverrideS || vsWhere.Row == lngRowMasterOverrideW)
			{
				if (vsWhere.Col == 1)
				{
					modNewAccountBox.SetGridFormat(vsWhere, vsWhere.Row, vsWhere.Col, false);
				}
			}
		}

		private void vsWhere_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (vsWhere.Col > 0)
			{
				switch (e.KeyCode)
				{
					case Keys.Delete:
						{
							vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
							break;
						}
				}
				//end switch
			}
		}

		private void vsWhere_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:CHN - issue #930: allow numbers only to enter the account
				if (vsWhere.Row == lngRowAccount)
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
						this.javaScriptNumbersOnlyWithoutBackspace.GetJavaScriptEvents(box).Remove(this.clientEventKeyPressNumbersOnlyWithoutBackspace);
						this.javaScriptNumbersOnlyWithoutBackspace.GetJavaScriptEvents(box).Add(this.clientEventKeyPressNumbersOnlyWithoutBackspace);
					}
				}
				e.Control.KeyDown -= vsWhere_KeyDownEdit;
				e.Control.KeyPress -= vsWhere_KeyPressEdit;
				e.Control.KeyUp -= vsWhere_KeyUpEdit;
				e.Control.KeyDown += vsWhere_KeyDownEdit;
				e.Control.KeyPress += vsWhere_KeyPressEdit;
				e.Control.KeyUp += vsWhere_KeyUpEdit;
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
				if (col == 1)
				{
					//FC:FINAL:CHN - issue #932: when entering letters, they was in .ToUpperCase only
					// if (e.Control is TextBox)
					// {
					//     (e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
					// }
					DynamicObject customClientData = new DynamicObject();
					//custom chec kis done for this grid. set GRID7Light_Col to 9999 when condition does not match, to avoid account formatting
					if (vsWhere.Row == lngRowMasterOverrideS || vsWhere.Row == lngRowMasterOverrideW)
					{
						customClientData["GRID7Light_Col"] = col;
					}
					else
					{
						customClientData["GRID7Light_Col"] = 9999;
					}
					customClientData["intAcctCol"] = 1;
					customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
					customClientData["gboolTownAccounts"] = true;
					customClientData["gboolSchoolAccounts"] = false;
					customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
					customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
					customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
					customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
					customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
					customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
					customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
					customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
					customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
					customProperties.SetCustomPropertiesValue(e.Control, customClientData);
					if (e.Control.UserData.GRID7LightClientSideKeys == null)
					{
						e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
						JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
						JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
						keyDownEvent.Event = "keydown";
						keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
						JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
						keyUpEvent.Event = "keyup";
						keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
						clientEvents.Add(keyDownEvent);
						clientEvents.Add(keyUpEvent);
					}
				}
			}
		}

		private void vsWhere_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
						break;
					}
				case Keys.Escape:
					{
						vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
			//if (vsWhere.Row == lngRowMasterOverrideS || vsWhere.Row == lngRowMasterOverrideW)
			//{
			//	if (vsWhere.Col == 1)
			//	{
			//		string vsWhereEditText = vsWhere.EditText;
			//		modNewAccountBox.CheckKeyDownEditF2(vsWhere, vsWhere.Row, vsWhere.Col, e.KeyCode, FCConvert.ToInt32(e.Shift), vsWhere.EditSelStart, ref vsWhereEditText, vsWhere.EditSelLength);
			//		vsWhere.EditText = vsWhereEditText;
			//	}
			//}
		}

		private void vsWhere_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (vsWhere.Row == lngRowMasterOverrideS || vsWhere.Row == lngRowMasterOverrideW)
			{
				if (vsWhere.Col == 1)
				{
					//modNewAccountBox.CheckAccountKeyPress(vsWhere, vsWhere.Row, vsWhere.Col, FCConvert.ToInt32(KeyAscii), vsWhere.EditSelStart, vsWhere.EditText);
				}
			}
			else if (vsWhere.Row == lngRowAccount)
			{
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Decimal) || (KeyAscii == Keys.Return))
				{
					// allow these characters
				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsWhere_KeyUpEdit(object sender, KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			if (vsWhere.Col == 1 && (vsWhere.Row == lngRowMasterOverrideS || vsWhere.Row == lngRowMasterOverrideW))
			{
				if (KeyCode != 40 && KeyCode != 38 && KeyCode != 123 && KeyCode != 122)
				{
					// up and down arrows
					//modNewAccountBox.CheckAccountKeyCode(vsWhere, vsWhere.Row, vsWhere.Row, KeyCode, 0, vsWhere.EditSelStart, vsWhere.EditText, vsWhere.EditSelLength);
				}
			}

            if (vsWhere.Row == lngRowMasterOverrideS || vsWhere.Row == lngRowMasterOverrideW)
            {
                if (vsWhere.Col == 1)
                {
                    string vsWhereEditText = vsWhere.EditText;
                    modNewAccountBox.CheckKeyDownEditF2(vsWhere, vsWhere.Row, vsWhere.Col, e.KeyCode, FCConvert.ToInt32(e.Shift), vsWhere.EditSelStart, ref vsWhereEditText, vsWhere.EditSelLength);
                    vsWhere.EditText = vsWhereEditText;
                }
            }
        }

		private void vsWhere_Leave(object sender, System.EventArgs e)
		{
			vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			vsWhere.Select(0, 1);
		}

		private void vsWhere_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = e.RowIndex;
			lngMC = e.ColumnIndex;
			//FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
			if (lngMR > -1 && lngMC > -1)
			{
				DataGridViewCell cell = vsWhere[lngMC, lngMR];
				cell.ToolTipText = "";
				if (lngMC == 0)
				{
					// lngRowHeader
					// lngRowWS                'Service
					if (lngMR == lngRowWS)
					{
						cell.ToolTipText = "Search for meters with this type of Service.";
						// lngRowAccount
					}
					else if (lngMR == lngRowAccount)
					{
						cell.ToolTipText = "Search for a range of accounts.";
						// lngRowOwnerName
					}
					else if (lngMR == lngRowOwnerName)
					{
						cell.ToolTipText = "Search for a range of names.";
						// lngRowTenantName
					}
					else if (lngMR == lngRowTenantName)
					{
						cell.ToolTipText = "Search for a range of names.";
						// lngRowBillMessage
					}
					else if (lngMR == lngRowBillMessage)
					{
						cell.ToolTipText = "Search for text in the bill message.";
						// lngRowBillType
					}
					else if (lngMR == lngRowBillType)
					{
						cell.ToolTipText = "Search for a billing type.";
						// lngRowBillToSameAsOwner
					}
					else if (lngMR == lngRowBillToSameAsOwner)
					{
						cell.ToolTipText = "Seach for an account that the tenant name is locked.";
						// lngRowCombinationType
					}
					else if (lngMR == lngRowCombinationType)
					{
						cell.ToolTipText = "Search for meters that are combined by this type";
						// lngRowDataEntryMessage
					}
					else if (lngMR == lngRowDataEntryMessage)
					{
						cell.ToolTipText = "Search for text in the Data Entry Message.";
						// lngRowMeterFinalBill
					}
					else if (lngMR == lngRowMeterFinalBill)
					{
						cell.ToolTipText = "Search for meters that are Final Billed.";
						// lngRowAcctFinalBill
					}
					else if (lngMR == lngRowAcctFinalBill)
					{
						cell.ToolTipText = "Search for accounts that are Final Billed.";
						// lngRowMeterSize
					}
					else if (lngMR == lngRowMeterSize)
					{
						cell.ToolTipText = "Search for meters of this size.";
						// lngRowMeterNoBill
					}
					else if (lngMR == lngRowMeterNoBill)
					{
						cell.ToolTipText = "Search for meters that are No Billed.";
						// lngRowAcctNoBill
					}
					else if (lngMR == lngRowAcctNoBill)
					{
						cell.ToolTipText = "Search for accounts that are No Billed.";
						// lngRowREAccount
					}
					else if (lngMR == lngRowREAccount)
					{
						cell.ToolTipText = "Search for a Real Estate account associated with a UT account.";
						// lngRowMasterOverrideS
					}
					else if (lngMR == lngRowMasterOverrideS)
					{
						cell.ToolTipText = "Search for accounts that use a Sewer Override Account.";
						// lngRowRateTableS
					}
					else if (lngMR == lngRowRateTableS)
					{
						cell.ToolTipText = "Search for meters that have this rate table associated with it.";
						// lngRowCategorys
					}
					else if (lngMR == lngRowCategoryS)
					{
						cell.ToolTipText = "Search for accounts that have this category associated with it.";
						// lngRowMasterOverrideW
					}
					else if (lngMR == lngRowMasterOverrideW)
					{
						// Amount of override on meter
						cell.ToolTipText = "Search for accounts that use a Water Override Account.";
						// lngRowRateTableW
					}
					else if (lngMR == lngRowRateTableW)
					{
						cell.ToolTipText = "Search for meters that have this rate table associated with it.";
						// lngRowCategoryW
					}
					else if (lngMR == lngRowCategoryW)
					{
						cell.ToolTipText = "Search for accounts that have this category associated with it.";
						// lngRowUseRE
					}
					else if (lngMR == lngRowUseRE)
					{
						cell.ToolTipText = "Search for accounts that have this setting for using a Real Estate account.";
						// lngRowWaterTax
					}
					else if (lngMR == lngRowWaterTax)
					{
						cell.ToolTipText = "Search for accounts that have this water taxable percentage.";
						// lngRowSewerTax
					}
					else if (lngMR == lngRowSewerTax)
					{
						cell.ToolTipText = "Search for accounts that have this sewer taxable percentage.";
						// lngRowSerialNumber
					}
					else if (lngMR == lngRowSerialNumber)
					{
						cell.ToolTipText = "Search for accounts that have this serial number.";
						// lngRowRemoteNumber
					}
					else if (lngMR == lngRowRemoteNumber)
					{
						cell.ToolTipText = "Search for accounts that have this remote number.";
						// lngRowMXUNumber
					}
					else if (lngMR == lngRowMXUNumber)
					{
						cell.ToolTipText = "Search for accounts that have this MXU number.";
						// lngRowReadType
					}
					else if (lngMR == lngRowReadType)
					{
						cell.ToolTipText = "Search for accounts that have this meter reading type.";
					}
					else if (lngMR == lngRowLatitude)
					{
						cell.ToolTipText = "Search for accounts that have this latitude.";
					}
					else if (lngMR == lngRowLongitude)
					{
						cell.ToolTipText = "Search for accounts that have this longitude.";
					}
				}
			}
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			int lngRow;
			int lngCol;
			lngRow = vsWhere.Row;
			lngCol = vsWhere.Col;
			if (lngRow > 0)
			{
				if (lngRow == lngRowMasterOverrideS || lngRow == lngRowMasterOverrideW)
				{
					if (lngCol == 1)
					{
						vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						modNewAccountBox.SetGridFormat(vsWhere, vsWhere.Row, vsWhere.Col, true);
					}
				}
				else
				{
					if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngCol) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						if (lngRow == vsWhere.Rows - 1)
						{
							vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
						}
						else
						{
							vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
						}
						if (modUTStatusList.Statics.strComboList[lngRow, 0] != string.Empty)
						{
							vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsWhere.ComboList = modUTStatusList.Statics.strComboList[lngRow, 0];
						}
						if (vsWhere.Col == 2)
						{
							if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
							{
								Support.SendKeys("{Tab}", false);
							}
						}
					}
					else
					{
						vsWhere.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - issue #926: save and use correct indexes of the cell
			int row = vsWhere.GetFlexRowIndex(e.RowIndex);
			int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
			ShowAutomaticFields();
			if (row == lngRowMasterOverrideS || row == lngRowMasterOverrideW)
			{
				if (col == 1)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						e.Cancel = modNewAccountBox.CheckAccountValidate(vsWhere, row, col, e.Cancel);
					}
					else
					{
						if (FCConvert.CBool(Strings.Left(vsWhere.EditText, 1) != "M"))
						{
							vsWhere.EditText = "";
						}
					}
				}
			}
		}

		private void EnableFrames_2(bool boolEnable)
		{
			EnableFrames(ref boolEnable);
		}

		private void EnableFrames(ref bool boolEnable)
		{
			fraFields.Enabled = boolEnable;
			fraSort.Enabled = boolEnable;
			fraWhere.Enabled = boolEnable;
			vsWhere.Enabled = boolEnable;
			lstFields.Enabled = boolEnable;
			lstSort.Enabled = boolEnable;
		}

		private void Set_Note_Text()
		{
			string strTemp;
			strTemp = "1. The Fields To Sort By section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
			strTemp += "2. Set any criteria needed in the Select Search Criteria list." + "\r\n" + "\r\n";
			strTemp += "Other Notes:" + "\r\n" + "Some fields will be printed automatically." + "\r\n" + "\r\n";
			// strTemp = strTemp & vbCrLf & "If you choose a default report, only the account number and the tax year criteria will affect the report.  Any changes in the other fields will not be used in the creation of the report." & vbCrLf
			//txtNotes.Text = strTemp;
		}

		private string GetCategoryString()
		{
			string GetCategoryString = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return a string with all of the possible categories and an All Categories option
				clsDRWrapper rsCategory = new clsDRWrapper();
				string strCategoryChoice = "";
				rsCategory.OpenRecordset("SELECT * FROM Category", modExtraModules.strUTDatabase);
				if (rsCategory.EndOfFile() != true && rsCategory.BeginningOfFile() != true)
				{
					// strYearChoice = ""
					strCategoryChoice = "#0;All Categories|";
					while (!rsCategory.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						strCategoryChoice += "#" + rsCategory.Get_Fields("Code") + ";" + rsCategory.Get_Fields("Code") + "\t" + rsCategory.Get_Fields_String("LongDescription") + "|";
						rsCategory.MoveNext();
					}
					// take off the last pipe character '|'
					strCategoryChoice = Strings.Left(strCategoryChoice, strCategoryChoice.Length - 1);
				}
				else
				{
					strCategoryChoice = "#0;All Years";
				}
				GetCategoryString = strCategoryChoice;
				return GetCategoryString;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Getting Category String", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCategoryString;
		}

		private string GetWhereConstraint(ref short intRow)
		{
			string GetWhereConstraint = "";
			// this will go to the grid and get the constraint for that row and return it
			// if there is none, then it will return a nullstring
			int lngType;
			string strTemp;
			strTemp = vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 1) + "|||" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 2);
			GetWhereConstraint = strTemp;
			return GetWhereConstraint;
		}

		private bool ItemInSortList(ref short intCounter)
		{
			bool ItemInSortList = false;
			// this will return true if the item has already been added to the Sort List
			int intTemp;
			ItemInSortList = false;
			for (intTemp = 0; intTemp <= lstSort.Items.Count - 1; intTemp++)
			{
				if (lstSort.ItemData(intTemp) == intCounter)
				{
					ItemInSortList = true;
					break;
				}
			}
			return ItemInSortList;
		}

		private void FillHardCodeCombo()
		{
			// this will fill the hard coded combo list with the list of reports that the user can choose
			cmbHardCode.Clear();
			cmbHardCode.AddItem("Non Zero Balance on All Accounts");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 2);
			cmbHardCode.AddItem("Non Zero Balance on Non Lien Accounts");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 0);
			cmbHardCode.AddItem("Non Zero Balance on Lien Accounts");
			cmbHardCode.ItemData(cmbHardCode.NewIndex, 1);
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void ShowHardCodedReport_2(short intIndex)
		{
			ShowHardCodedReport(ref intIndex);
		}

		private void ShowHardCodedReport(ref short intIndex)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				switch (intIndex)
				{
					case 0:
						{
							// Non Zero Balance on Non Lien Accounts
							frmReportViewer.InstancePtr.Init(rptUTOutstandingBalances.InstancePtr);
							break;
						}
					case 1:
						{
							// Non Zero Balance on Lien Accounts
							frmReportViewer.InstancePtr.Init(rptUTOutstandingLienBalances.InstancePtr);
							break;
						}
					case 2:
						{
							// Non Zero Balance on Lien Accounts
							frmReportViewer.InstancePtr.Init(rptUTOutstandingBalancesAll.InstancePtr);
							intMasterReportType = 0;
							break;
						}
					default:
						{
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Hard Coded Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowAutomaticFields()
		{
			string strRptType = "";
			// chkUseFullStatus.Enabled = True
			if (chkHardCode.CheckState != Wisej.Web.CheckState.Checked)
			{
				// this is not a hard coded report
				lblShowFields.Text = "Fields automatically included on report:" + "\r\n";
				lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
				lblShowFields.Text = lblShowFields.Text + "    Bill Name" + "\r\n";
				lblShowFields.Text = lblShowFields.Text + "    Location" + "\r\n";
			}
			else
			{
				// this is a hardcoded report
				if (cmbHardCode.SelectedIndex == -1)
				{
					lblShowFields.Text = "Please select a report from the 'Default Report' list." + "\r\n";
				}
				else
				{
					if (cmbHardCode.ItemData(cmbHardCode.SelectedIndex) == 3)
					{
						// chkUseFullStatus.Value = vbChecked
						// chkUseFullStatus.Enabled = False
					}
					lblShowFields.Text = "Fields automatically included on report:" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Type" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Rate Key" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Original Principal" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Payments and Adjustments" + "\r\n";
					lblShowFields.Text = lblShowFields.Text + "    Amount Due" + "\r\n";
				}
			}
		}

		private bool ValidateWhereGrid()
		{
			bool ValidateWhereGrid = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This will switch any ranges that are backwards (ie. lowest amount in the 2nd column)
				string strSwap = "";
				// lngRowAccount
				if (Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 2)))
					{
						strSwap = vsWhere.TextMatrix(0, 1);
						vsWhere.TextMatrix(lngRowAccount, 1, vsWhere.TextMatrix(lngRowAccount, 2));
						vsWhere.TextMatrix(lngRowAccount, 2, strSwap);
					}
				}
				// lngRowOwnerName
				if (Strings.Trim(vsWhere.TextMatrix(lngRowOwnerName, 1)) != "" && Strings.Trim(vsWhere.TextMatrix(lngRowOwnerName, 2)) != "")
				{
					if (Strings.StrComp(vsWhere.TextMatrix(lngRowOwnerName, 1), vsWhere.TextMatrix(lngRowOwnerName, 2), CompareConstants.vbTextCompare) > 0)
					{
						strSwap = vsWhere.TextMatrix(lngRowOwnerName, 1);
						vsWhere.TextMatrix(lngRowOwnerName, 1, vsWhere.TextMatrix(lngRowOwnerName, 2));
						vsWhere.TextMatrix(lngRowOwnerName, 2, strSwap);
					}
				}
				// lngRowTenantName
				if (Strings.Trim(vsWhere.TextMatrix(lngRowTenantName, 1)) != "" && Strings.Trim(vsWhere.TextMatrix(lngRowTenantName, 2)) != "")
				{
					if (Strings.StrComp(vsWhere.TextMatrix(lngRowTenantName, 1), vsWhere.TextMatrix(lngRowTenantName, 2), CompareConstants.vbTextCompare) > 0)
					{
						strSwap = vsWhere.TextMatrix(lngRowTenantName, 1);
						vsWhere.TextMatrix(lngRowTenantName, 1, vsWhere.TextMatrix(lngRowTenantName, 2));
						vsWhere.TextMatrix(lngRowTenantName, 2, strSwap);
					}
				}
				// lngRowRateTableS
				if (Conversion.Val(vsWhere.TextMatrix(lngRowRateTableS, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowRateTableS, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowRateTableS, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowRateTableS, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowRateTableS, 1);
						vsWhere.TextMatrix(lngRowRateTableS, 1, vsWhere.TextMatrix(lngRowRateTableS, 2));
						vsWhere.TextMatrix(lngRowRateTableS, 2, strSwap);
					}
				}
				// lngRowRateTableW
				if (Conversion.Val(vsWhere.TextMatrix(lngRowRateTableW, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowRateTableW, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowRateTableW, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowRateTableW, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowRateTableW, 1);
						vsWhere.TextMatrix(lngRowRateTableW, 1, vsWhere.TextMatrix(lngRowRateTableW, 2));
						vsWhere.TextMatrix(lngRowRateTableW, 2, strSwap);
					}
				}
				// lngRowMeterSize
				if (Conversion.Val(vsWhere.TextMatrix(lngRowMeterSize, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowMeterSize, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowMeterSize, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowMeterSize, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowMeterSize, 1);
						vsWhere.TextMatrix(lngRowMeterSize, 1, vsWhere.TextMatrix(lngRowMeterSize, 2));
						vsWhere.TextMatrix(lngRowMeterSize, 2, strSwap);
					}
				}
				// lngRowCategoryS
				if (Conversion.Val(vsWhere.TextMatrix(lngRowCategoryS, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowCategoryS, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowCategoryS, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowCategoryS, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowCategoryS, 1);
						vsWhere.TextMatrix(lngRowCategoryS, 1, vsWhere.TextMatrix(lngRowCategoryS, 2));
						vsWhere.TextMatrix(lngRowCategoryS, 2, strSwap);
					}
				}
				// lngRowCategoryW
				if (Conversion.Val(vsWhere.TextMatrix(lngRowCategoryW, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowCategoryW, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowCategoryW, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowCategoryW, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowCategoryW, 1);
						vsWhere.TextMatrix(lngRowCategoryW, 1, vsWhere.TextMatrix(lngRowCategoryW, 2));
						vsWhere.TextMatrix(lngRowCategoryW, 2, strSwap);
					}
				}
				// lngRowREAccount
				if (Conversion.Val(vsWhere.TextMatrix(lngRowREAccount, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowREAccount, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowREAccount, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowREAccount, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowREAccount, 1);
						vsWhere.TextMatrix(lngRowREAccount, 1, vsWhere.TextMatrix(lngRowREAccount, 2));
						vsWhere.TextMatrix(lngRowREAccount, 2, strSwap);
					}
				}
				// MAL@20080527: 13713
				// lngRowWaterTax
				if (Conversion.Val(vsWhere.TextMatrix(lngRowWaterTax, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowWaterTax, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowWaterTax, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowWaterTax, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowWaterTax, 1);
						vsWhere.TextMatrix(lngRowWaterTax, 1, vsWhere.TextMatrix(lngRowWaterTax, 2));
						vsWhere.TextMatrix(lngRowWaterTax, 2, strSwap);
					}
				}
				// lngRowSewerTax
				if (Conversion.Val(vsWhere.TextMatrix(lngRowSewerTax, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowSewerTax, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowSewerTax, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowSewerTax, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowSewerTax, 1);
						vsWhere.TextMatrix(lngRowSewerTax, 1, vsWhere.TextMatrix(lngRowSewerTax, 2));
						vsWhere.TextMatrix(lngRowSewerTax, 2, strSwap);
					}
				}
				// lngRowSerialNumber
				if (Conversion.Val(vsWhere.TextMatrix(lngRowSerialNumber, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowSerialNumber, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowSerialNumber, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowSerialNumber, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowSerialNumber, 1);
						vsWhere.TextMatrix(lngRowSerialNumber, 1, vsWhere.TextMatrix(lngRowSerialNumber, 2));
						vsWhere.TextMatrix(lngRowSerialNumber, 2, strSwap);
					}
				}
				// lngRowRemoteNumber
				if (Conversion.Val(vsWhere.TextMatrix(lngRowRemoteNumber, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowRemoteNumber, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowRemoteNumber, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowRemoteNumber, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowRemoteNumber, 1);
						vsWhere.TextMatrix(lngRowRemoteNumber, 1, vsWhere.TextMatrix(lngRowRemoteNumber, 2));
						vsWhere.TextMatrix(lngRowRemoteNumber, 2, strSwap);
					}
				}
				// lngRowMXUNumber
				if (Conversion.Val(vsWhere.TextMatrix(lngRowMXUNumber, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowMXUNumber, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowMXUNumber, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowMXUNumber, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowMXUNumber, 1);
						vsWhere.TextMatrix(lngRowMXUNumber, 1, vsWhere.TextMatrix(lngRowMXUNumber, 2));
						vsWhere.TextMatrix(lngRowMXUNumber, 2, strSwap);
					}
				}
				// lngRowLatitude
				if (Conversion.Val(vsWhere.TextMatrix(lngRowLatitude, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowLatitude, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowLatitude, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowLatitude, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowLatitude, 1);
						vsWhere.TextMatrix(lngRowLatitude, 1, vsWhere.TextMatrix(lngRowLatitude, 2));
						vsWhere.TextMatrix(lngRowLatitude, 2, strSwap);
					}
				}
				// lngRowLongitude
				if (Conversion.Val(vsWhere.TextMatrix(lngRowLongitude, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowLongitude, 2)) > 0)
				{
					if (Conversion.Val(vsWhere.TextMatrix(lngRowLongitude, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowLongitude, 2)))
					{
						strSwap = vsWhere.TextMatrix(lngRowLongitude, 1);
						vsWhere.TextMatrix(lngRowLongitude, 1, vsWhere.TextMatrix(lngRowLongitude, 2));
						vsWhere.TextMatrix(lngRowLongitude, 2, strSwap);
					}
				}
				ValidateWhereGrid = true;
				return ValidateWhereGrid;
			}
			catch (Exception ex)
			{
				
				ValidateWhereGrid = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Where Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateWhereGrid;
		}

		public void Format_WhereGrid_2(bool boolReset)
		{
			Format_WhereGrid(boolReset);
		}

		public void Format_WhereGrid(bool boolReset = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCounter;
				int lngWid = 0;
				if (boolReset)
				{
					vsWhere.Rows = 0;
				}
				vsWhere.Rows = lngTotalWhereRows;
				vsWhere.Cols = 4;
				lngWid = vsWhere.WidthOriginal;
				vsWhere.ColWidth(0, FCConvert.ToInt32(lngWid * 0.45));
				vsWhere.ColWidth(1, FCConvert.ToInt32(lngWid * 0.25));
				vsWhere.ColWidth(2, FCConvert.ToInt32(lngWid * 0.25));
				vsWhere.ColWidth(3, 0);
				if (vsWhere.Rows > 0)
				{
					// Service
					vsWhere.TextMatrix(lngRowWS, 0, "Service");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowWS, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// Account Range
					vsWhere.TextMatrix(lngRowAccount, 0, "Account");
					// Owner Name Range
					vsWhere.TextMatrix(lngRowOwnerName, 0, "Owner Name");
					// Tenant Name Range
					vsWhere.TextMatrix(lngRowTenantName, 0, "Tenant Name");
					// lngRowBillMessage
					vsWhere.TextMatrix(lngRowBillMessage, 0, "Bill Message");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowBillMessage, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowBillType 'Billing Types (ie Consumption, Flat, Units, Adjust)
					vsWhere.TextMatrix(lngRowBillType, 0, "Billing Type");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowBillType, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowBillToSameAsOwner
					vsWhere.TextMatrix(lngRowBillToSameAsOwner, 0, "Bill To Same As Owner");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowBillToSameAsOwner, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowCombinationType
					vsWhere.TextMatrix(lngRowCombinationType, 0, "Combination Type");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowCombinationType, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowDataEntryMessage
					vsWhere.TextMatrix(lngRowDataEntryMessage, 0, "Data Entry Message");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowDataEntryMessage, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowMeterFinalBill
					vsWhere.TextMatrix(lngRowMeterFinalBill, 0, "Meter Final Billed");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterFinalBill, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowAcctFinalBill
					vsWhere.TextMatrix(lngRowAcctFinalBill, 0, "Account Final Billed");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowAcctFinalBill, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowMeterSize
					vsWhere.TextMatrix(lngRowMeterSize, 0, "Meter Size");
					// lngRowMeterNoBill
					vsWhere.TextMatrix(lngRowMeterNoBill, 0, "Meter No Bill");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterNoBill, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowAcctNoBill
					vsWhere.TextMatrix(lngRowAcctNoBill, 0, "Account No Bill");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowAcctNoBill, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowREAccount
					vsWhere.TextMatrix(lngRowREAccount, 0, "Real Estate Account");
					// lngRowMasterOverrideS
					vsWhere.TextMatrix(lngRowMasterOverrideS, 0, "Sewer Override Account");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMasterOverrideS, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// lngRowRateTableS
					vsWhere.TextMatrix(lngRowRateTableS, 0, "Sewer Rate Table");
					// lngRowCategoryS
					vsWhere.TextMatrix(lngRowCategoryS, 0, "Sewer Category");
					// MAL@20080527 ; Tracker Reference: 13713
					vsWhere.TextMatrix(lngRowSewerTax, 0, "Sewer Taxable %");
					// kk trouts-6 03012013  Change Water to Stormwater for Bangor
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						// lngRowMasterOverrideW
						vsWhere.TextMatrix(lngRowMasterOverrideW, 0, "Stormwater Override Account");
						//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMasterOverrideW, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						// lngRowRateTableW
						vsWhere.TextMatrix(lngRowRateTableW, 0, "Stormwater Rate Table");
						// lngRowCategoryW
						vsWhere.TextMatrix(lngRowCategoryW, 0, "Stormwater Category");
						// MAL@20080527 ; Tracker Reference: 13713
						vsWhere.TextMatrix(lngRowWaterTax, 0, "Stormwater Taxable %");
					}
					else
					{
						// lngRowMasterOverrideW
						vsWhere.TextMatrix(lngRowMasterOverrideW, 0, "Water Override Account");
						//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
						vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMasterOverrideW, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						// lngRowRateTableW
						vsWhere.TextMatrix(lngRowRateTableW, 0, "Water Rate Table");
						// lngRowCategoryW
						vsWhere.TextMatrix(lngRowCategoryW, 0, "Water Category");
						// MAL@20080527 ; Tracker Reference: 13713
						vsWhere.TextMatrix(lngRowWaterTax, 0, "Water Taxable %");
					}
					// MAL@20071211
					vsWhere.TextMatrix(lngRowUseRE, 0, "Use RE Information");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowUseRE, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					// 
					vsWhere.TextMatrix(lngRowSerialNumber, 0, "Serial Number");
					// lngRowCategoryW
					vsWhere.TextMatrix(lngRowRemoteNumber, 0, "Remote Number");
					// MAL@20080527 ; Tracker Reference: 13713
					vsWhere.TextMatrix(lngRowMXUNumber, 0, "MXU Number");
					// MAL@20071211
					vsWhere.TextMatrix(lngRowReadType, 0, "Meter Reading Type");
					//FC:FINAL:MSH - issue #926: incorrect color. In original app BackColor is gray, but in web BackColor changed to white
					vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowReadType, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsWhere.TextMatrix(lngRowLatitude, 0, "Latitude");
					vsWhere.TextMatrix(lngRowLongitude, 0, "Longitude");
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Where Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
		}

		private void Fill_Lists()
		{
			// this routine will fill the extra fields and the sort lists
			lstFields.Clear();
			lstFields.AddItem("Account Final Bill");
			lstFields.ItemData(lstFields.NewIndex, 21);
			lstFields.AddItem("Account No Bill");
			lstFields.ItemData(lstFields.NewIndex, 22);
			lstFields.AddItem("Address");
			lstFields.ItemData(lstFields.NewIndex, 1);
			lstFields.AddItem("Book");
			lstFields.ItemData(lstFields.NewIndex, 2);
			lstFields.AddItem("Billing Type");
			lstFields.ItemData(lstFields.NewIndex, 3);
			lstFields.AddItem("Bill Message");
			lstFields.ItemData(lstFields.NewIndex, 4);
			lstFields.AddItem("Bill To Same As Owner");
			lstFields.ItemData(lstFields.NewIndex, 5);
			lstFields.AddItem("Combination Type");
			lstFields.ItemData(lstFields.NewIndex, 6);
			lstFields.AddItem("Current Reading");
			lstFields.ItemData(lstFields.NewIndex, 7);
			lstFields.AddItem("Data Entry Message");
			lstFields.ItemData(lstFields.NewIndex, 8);
			lstFields.AddItem("Map Lot");
			lstFields.ItemData(lstFields.NewIndex, 25);
			lstFields.AddItem("Meter Final Bill");
			lstFields.ItemData(lstFields.NewIndex, 9);
			lstFields.AddItem("Meter Size");
			lstFields.ItemData(lstFields.NewIndex, 10);
			lstFields.AddItem("Meter No Bill");
			lstFields.ItemData(lstFields.NewIndex, 11);
			lstFields.AddItem("Owner Name");
			lstFields.ItemData(lstFields.NewIndex, 12);
			// MAL@20071121: Add phone number option
			// Call Reference: 117108
			lstFields.AddItem("Phone Number");
			lstFields.ItemData(lstFields.NewIndex, 26);
			lstFields.AddItem("Previous Reading");
			lstFields.ItemData(lstFields.NewIndex, 13);
			lstFields.AddItem("Real Estate Account");
			lstFields.ItemData(lstFields.NewIndex, 14);
			lstFields.AddItem("Sequence");
			lstFields.ItemData(lstFields.NewIndex, 15);
			lstFields.AddItem("Service");
			lstFields.ItemData(lstFields.NewIndex, 16);
			lstFields.AddItem("Sewer Category");
			lstFields.ItemData(lstFields.NewIndex, 24);
			lstFields.AddItem("Sewer Master Account");
			lstFields.ItemData(lstFields.NewIndex, 17);
			lstFields.AddItem("Sewer Rate Table");
			lstFields.ItemData(lstFields.NewIndex, 18);
			// MAL@20080527 ; Tracker Reference: 13713
			lstFields.AddItem("Sewer Taxable %");
			lstFields.ItemData(lstFields.NewIndex, 29);
			// MAL@20071211: Reference: 101984
			lstFields.AddItem("Use RE Information");
			lstFields.ItemData(lstFields.NewIndex, 27);
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lstFields.AddItem("Stormwater Category");
				lstFields.ItemData(lstFields.NewIndex, 23);
				lstFields.AddItem("Stormwater Master Account");
				lstFields.ItemData(lstFields.NewIndex, 19);
				lstFields.AddItem("Stormwater Rate Table");
				lstFields.ItemData(lstFields.NewIndex, 20);
				// MAL@20080527 ; Tracker Reference: 13713
				lstFields.AddItem("Stormwater Taxable %");
				lstFields.ItemData(lstFields.NewIndex, 28);
			}
			else
			{
				lstFields.AddItem("Water Category");
				lstFields.ItemData(lstFields.NewIndex, 23);
				lstFields.AddItem("Water Master Account");
				lstFields.ItemData(lstFields.NewIndex, 19);
				lstFields.AddItem("Water Rate Table");
				lstFields.ItemData(lstFields.NewIndex, 20);
				// MAL@20080527 ; Tracker Reference: 13713
				lstFields.AddItem("Water Taxable %");
				lstFields.ItemData(lstFields.NewIndex, 28);
			}
			lstFields.AddItem("Serial Number");
			lstFields.ItemData(lstFields.NewIndex, 30);
			lstFields.AddItem("Remote Number");
			lstFields.ItemData(lstFields.NewIndex, 31);
			lstFields.AddItem("MXU Number");
			lstFields.ItemData(lstFields.NewIndex, 32);
			lstFields.AddItem("Meter Reading Type");
			lstFields.ItemData(lstFields.NewIndex, 33);
			lstFields.AddItem("Latitude");
			lstFields.ItemData(lstFields.NewIndex, 34);
			lstFields.AddItem("Longitude");
			lstFields.ItemData(lstFields.NewIndex, 35);
			lstSort.Clear();
			lstSort.AddItem("Account Number");
			lstSort.ItemData(lstSort.NewIndex, 0);
			lstSort.AddItem("Bill To Name");
			lstSort.ItemData(lstSort.NewIndex, 1);
			lstSort.AddItem("Book");
			lstSort.ItemData(lstSort.NewIndex, 2);
			lstSort.AddItem("Location");
			lstSort.ItemData(lstSort.NewIndex, 3);
			lstSort.AddItem("Meter Size");
			lstSort.ItemData(lstSort.NewIndex, 4);
			lstSort.AddItem("Owner Name");
			lstSort.ItemData(lstSort.NewIndex, 5);
			lstSort.AddItem("Real Estate Account");
			lstSort.ItemData(lstSort.NewIndex, 6);
			lstSort.AddItem("Sequence");
			lstSort.ItemData(lstSort.NewIndex, 7);
			lstSort.AddItem("Service");
			lstSort.ItemData(lstSort.NewIndex, 8);
			lstSort.AddItem("Sewer Category");
			lstSort.ItemData(lstSort.NewIndex, 9);
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lstSort.AddItem("Stormwater Category");
				lstSort.ItemData(lstSort.NewIndex, 10);
			}
			else
			{
				lstSort.AddItem("Water Category");
				lstSort.ItemData(lstSort.NewIndex, 10);
			}
			lstSort.AddItem("Serial Number");
			lstSort.ItemData(lstSort.NewIndex, 11);
			lstSort.AddItem("Remote Number");
			// kk10312014 trouts-114  Fix typo  "Reomte Number"
			lstSort.ItemData(lstSort.NewIndex, 12);
			lstSort.AddItem("MXU Number");
			lstSort.ItemData(lstSort.NewIndex, 13);
		}
	}
}
