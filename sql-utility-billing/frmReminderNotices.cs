﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReminderNotices.
	/// </summary>
	public partial class frmReminderNotices : BaseForm
	{
		public frmReminderNotices()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkSendTo = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkSendTo.AddControlArrayElement(chkSendTo_1, 1);
			this.chkSendTo.AddControlArrayElement(chkSendTo_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReminderNotices InstancePtr
		{
			get
			{
				return (frmReminderNotices)Sys.GetInstance(typeof(frmReminderNotices));
			}
		}

		protected frmReminderNotices _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/02/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		DateTime dtMailDate;
		string[,] strList = new string[12 + 1, 4 + 1];
		bool boolChange;
		// to tell if intAction changed for the vsGrid_BeforeEdit
		int intReminderType;
		string strRateKeyList;
		public int lngHighestRK;
		public bool boolLienedRecords;
		public string strPassRNFORMSQL = "";
		public bool boolWater;
		clsPrintLabel labLabelTypes = new clsPrintLabel();
		// vbPorter upgrade warning: intLabelType As int	OnWrite(short, string)
		int intLabelType;

		public void Init(ref string strPassRKList, bool boolPassWater, bool boolPassLienedRecords)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will pass a list of the rate keys that will determine which accounts to use
				strRateKeyList = strPassRKList;
                //FC:FINAL:SBE:#4129 - in VB6 Val("1234,5678") returns 1234. In .NET it will return 12345678. Replaced "," with another char(";") to have the same result when Val() is executed
                //lngHighestRK = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Right(Strings.Trim(strPassRKList), Strings.Trim(strPassRKList).Length - 1))));
                string tempStrPassRKList = strPassRKList.Replace(",", ";");
                lngHighestRK = FCConvert.ToInt32(Conversion.Val(Strings.Right(Strings.Trim(tempStrPassRKList), Strings.Trim(tempStrPassRKList).Length - 1)));
                boolLienedRecords = boolPassLienedRecords;
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void chkCombineService_CheckedChanged(object sender, System.EventArgs e)
		{
			if (txtReportTitle.Visible == true)
			{
				if (FCConvert.CBool(chkCombineService.CheckState == Wisej.Web.CheckState.Checked))
				{
					// kk trouts-6 03012013  Change Water to Stormwater for Bangor
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						txtReportTitle.Text = "Past Due Sewer and Stormwater Reminder Statement";
					}
					else
					{
						txtReportTitle.Text = "Past Due Water and Sewer Reminder Statement";
					}
				}
				else
				{
					if (boolWater)
					{
						// kk trouts-6 03012013  Change Water to Stormwater for Bangor
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							txtReportTitle.Text = "Past Due Stormwater Reminder Statement";
						}
						else
						{
							txtReportTitle.Text = "Past Due Water Reminder Statement";
						}
					}
					else
					{
						txtReportTitle.Text = "Past Due Sewer Reminder Statement";
					}
				}
			}
		}

		private void chkSendTo_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						if (chkSendTo[0].CheckState == Wisej.Web.CheckState.Checked)
						{
							chkSendTo[1].Text = "Owner(if different)";
						}
						else
						{
							chkSendTo[1].Text = "Owner ";
						}
						break;
					}
				case 1:
					{
						break;
					}
			}
			//end switch
		}

		private void chkSendTo_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = chkSendTo.GetIndex((FCCheckBox)sender);
			chkSendTo_CheckedChanged(index, sender, e);
		}

		private void frmReminderNotices_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!boolLoaded)
				{
					boolLoaded = true;
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
					this.Text = "Reminder Notices";
					FillStringArray();
					ShowAllFrames();
					OptionChoice(0);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmReminderNotices_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReminderNotices properties;
			//frmReminderNotices.FillStyle	= 0;
			//frmReminderNotices.ScaleWidth	= 9045;
			//frmReminderNotices.ScaleHeight	= 7230;
			//frmReminderNotices.LinkTopic	= "Form2";
			//frmReminderNotices.LockControls	= true;
			//frmReminderNotices.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsUpdate = new clsDRWrapper();
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				intLabelType = 0;
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Reminder Notices", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Unload();
			}
		}

		private void frmReminderNotices_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape || KeyAscii == Keys.F7)
			{
				// MAL@20070831: Added check for F7 to behave the same way as ESC
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			// Unload rptReminderForm
		}

		private void frmReminderNotices_Resize(object sender, System.EventArgs e)
		{
			FormatGrid_2(true);
			// this will set the height of the grid
			//FC:FINAL:DDU:#i885 - align grids
			if (!vsReturnAddress.Visible)
			{
				vsGrid.Height = fraRN.Height - 50;
				//(vsGrid.Rows * vsGrid.RowHeight(0)) + 70;
			}
			else
			{
				//FC:FINAL:DDU:#i921 - align second grid as well
				vsGrid.Height = fraRN.Height / 2 - 30;
				vsReturnAddress.Top = vsGrid.Top + vsGrid.Height + 20;
				vsReturnAddress.Height = fraRN.Height - vsReturnAddress.Top - 20;
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void FormatGrid_2(bool boolResize)
		{
			FormatGrid(boolResize);
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsGrid.Rows = 1;
			}
			vsGrid.Cols = 3;
			wid = vsGrid.WidthOriginal;
			vsGrid.ColWidth(0, FCConvert.ToInt32(wid * 0.36));
			// Question
			vsGrid.ColWidth(1, FCConvert.ToInt32(wid * 0.3));
			// Answer
			vsGrid.ColWidth(1, FCConvert.ToInt32(wid * 0.3));
			// Answer Range
			vsGrid.ExtendLastCol = true;
			// align the columns
			vsGrid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrid.FixedRows = 1;
			// header row
			//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			string strSQL = "";
			int intType = 0;
			bool boolUseFont = false;
			string strFont = "";
			string strPrinterName = "";
			int lngNumFonts = 0;
			int intCPI = 0;
			int lngCT;
			string strDesc = "";
			string strTemp = "";
			string strPayment = "";
			string strOldDefault = "";
			if (ValidateAnswers())
			{
				// validate the criteria selected by the user
				switch (intAction)
				{
					case 0:
						{
							if (intAction != 4)
							{
								/*? On Error Resume Next  */
								strOldDefault = FCGlobal.Printer.DeviceName;
								// kk11212014 troge-241 Fix issue with printer and default printer flip-flopping
								// MDIParent.CommonDialog1.CancelError = True
								// MDIParent.CommonDialog1.ShowPrinter
								// 
								// If Err.Number = 0 Then

                                //FC:FINAL:SBE - #3636 - do not show the dialog with server printers. Reports are exported to PDF, and user will print the PDF to client printers
								//if (modPrinterDialogBox.ShowPrinterDialog(this.Handle.ToInt32()))
								//{
								//	strPrinterName = FCGlobal.Printer.DeviceName;
								//	lngNumFonts = FCGlobal.Printer.FontCount;
								//	boolUseFont = false;
								//	intCPI = 10;
								//	for (lngCT = 0; lngCT <= lngNumFonts - 1; lngCT++)
								//	{
								//		//Application.DoEvents();
								//		strFont = FCGlobal.Printer.Fonts[lngCT];
								//		if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
								//		{
								//			strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
								//			if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
								//			{
								//				boolUseFont = true;
								//				strFont = FCGlobal.Printer.Fonts[lngCT];
								//				break;
								//			}
								//		}
								//	}
								//	// lngCT
								//}
								//else
								//{
								//	return;
								//}

								// Select Case UCase(Left(vsGrid.TextMatrix(1, 1), 10))
								// Case UCase("Avery 4013")
								// intType = 0
								// strDesc = "Style 4013.  1 in. x 3.5 in. This type is a continuous sheet of labels used with a dot matrix."
								// 
								// Case UCase("Avery 4014")
								// intType = 1
								// strDesc = "Style 4014.  1 7/16 in. x 4 in. This type is a contiuous sheet of labels used with a dot matrix."
								// 
								// Case UCase("Avery 5160")
								// intType = 2
								// strDesc = "Style 5160, 5260, 5560, 5660, 5960, 5970, 5971, 5972, 5979, 5980, 6241, 6460. Sheet of 3 x 10. 1 in. X 2 5/8 in."
								// 
								// Case UCase("Avery 5161")
								// intType = 3
								// strDesc = "Style 5161, 5261, 5661, 5961. Sheet of 2 X 10. 1 in. X 4 in."
								// 
								// Case UCase("Avery 5162")
								// intType = 4
								// strDesc = "Style 5162, 5262, 5662, 5962. Sheet of 2 X 7. 1 1/3 in. X 4 in."
								// 
								// Case UCase("Avery 5163")
								// intType = 5
								// strDesc = "Style 5163, 5263, 5663, 5963. Sheet of 2 X 5. 2 in. X 4 in."
								// 
								// End Select
							}
							break;
						}
				}
				//end switch
				intReminderType = 4;
				// SHOW THE REPORT
				if (!boolUseFont)
					strFont = "";
				intType = intLabelType;
				strSQL = BuildSQL();
				switch (intAction)
				{
					case 0:
						{
							// Labels
							
							rptReminderNoticeLabels.InstancePtr.Init(ref strSQL, 0, ref intType, ref strPrinterName, ref strFont, ref boolWater, intReminderType, strRateKeyList);
							
							break;
						}
					case 1:
						{
							// Post Cards
							rptReminderPostCard.InstancePtr.Init(ref strSQL, ref strPrinterName, ref boolWater, ref boolLienedRecords, ref strRateKeyList);
							// saves the default data
							strTemp = vsReturnAddress.TextMatrix(1, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress1", strTemp);
							strTemp = vsReturnAddress.TextMatrix(2, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress2", strTemp);
							strTemp = vsReturnAddress.TextMatrix(3, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress3", strTemp);
							strTemp = vsReturnAddress.TextMatrix(4, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress4", strTemp);
							strTemp = vsGrid.TextMatrix(3, 1);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeMessage", strTemp);
							break;
						}
					case 2:
						{
							// Forms
							// saves the default data
							strTemp = vsGrid.TextMatrix(4, 1);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeIntDept", strTemp);
							strTemp = vsGrid.TextMatrix(5, 1);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeIntDeptPhone", strTemp);
							strTemp = vsGrid.TextMatrix(6, 1);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeMatDept", strTemp);
							strTemp = vsGrid.TextMatrix(7, 1);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeMatDeptPhone", strTemp);
							// kgk 12-20-2011 trout-772
							strTemp = vsReturnAddress.TextMatrix(1, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress1", strTemp);
							strTemp = vsReturnAddress.TextMatrix(2, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress2", strTemp);
							strTemp = vsReturnAddress.TextMatrix(3, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress3", strTemp);
							strTemp = vsReturnAddress.TextMatrix(4, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress4", strTemp);
							strTemp = vsReturnAddress.TextMatrix(5, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress5", strTemp);
							strPassRNFORMSQL = strSQL;
							modRhymalReporting.Statics.strFreeReportType = "RNFORM";
							modUTStatusList.Statics.strReportType = "RNFORM";
							frmFreeReport.InstancePtr.Show(App.MainForm);
							//Application.DoEvents();
							frmFreeReport.InstancePtr.Focus();
							break;
						}
					case 3:
						{
							// Mailers
							//Application.DoEvents();
							frmReminderMailer.InstancePtr.Init(ref strSQL, ref strRateKeyList, "FI", DateAndTime.DateValue(vsGrid.TextMatrix(5, 1)), ref boolWater, ref boolLienedRecords);
							// Left$(vsGrid.TextMatrix(1, 1), 4)
							// saves the default data
							strTemp = vsReturnAddress.TextMatrix(1, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress1", strTemp);
							strTemp = vsReturnAddress.TextMatrix(2, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress2", strTemp);
							strTemp = vsReturnAddress.TextMatrix(3, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress3", strTemp);
							strTemp = vsReturnAddress.TextMatrix(4, 0);
							modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress4", strTemp);
							break;
						}
					case 4:
						{
							// Outprinting File
							//Application.DoEvents();
							BuildOutPrintingFile(ref strSQL, ref boolWater);
							break;
						}
				}
				//end switch
			}
		}

		private void optRN_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			OptionChoice(Index);
		}

		private void optRN_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRN.SelectedIndex;
			optRN_CheckedChanged(index, sender, e);
		}

		private void OptionChoice(int intIndex)
		{
			// this will show all of the questions for the selection
			switch (intIndex)
			{
				case 0:
					{
						// Labels
						// this will show the options for labels in the grid
						FillGrid_2(0);
						intAction = 0;
						chkBulkMailing.Enabled = false;
						chkCombineService.Enabled = false;
						chkPastDueOnly.Enabled = false;
						intLabelType = -1;
						lblTitle.Visible = false;
						txtReportTitle.Visible = false;
						break;
					}
				case 1:
					{
						// Post Cards
						// this will show the options for the Post Cards in the grid
						FillGrid_2(1);
						intAction = 1;
						chkBulkMailing.Enabled = true;
						chkCombineService.Enabled = false;
						chkPastDueOnly.Enabled = false;
						lblTitle.Visible = false;
						txtReportTitle.Visible = false;
						break;
					}
				case 2:
					{
						// Forms
						// this will show the options for Reminder Notice Forms
						FillGrid_2(2);
						intAction = 2;
						chkBulkMailing.Enabled = false;
						chkCombineService.Enabled = FCConvert.CBool(modUTStatusPayments.Statics.TownService == "B");
						chkPastDueOnly.Enabled = true;
						// kgk 12-16-2011  trout-406 and trout-774  Make the report title configurable
						lblTitle.Visible = true;
						txtReportTitle.Visible = true;
						if (FCConvert.CBool(chkCombineService.CheckState == Wisej.Web.CheckState.Checked))
						{
							// kk trouts-6 03012013  Change Water to Stormwater for Bangor
							if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
							{
								txtReportTitle.Text = "Past Due Sewer and Stormwater Reminder Statement";
							}
							else
							{
								txtReportTitle.Text = "Past Due Water and Sewer Reminder Statement";
							}
						}
						else
						{
							if (boolWater)
							{
								// kk trouts-6 03012013  Change Water to Stormwater for Bangor
								if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
								{
									txtReportTitle.Text = "Past Due Stormwater Reminder Statement";
								}
								else
								{
									txtReportTitle.Text = "Past Due Water Reminder Statement";
								}
							}
							else
							{
								txtReportTitle.Text = "Past Due Sewer Reminder Statement";
							}
						}
						break;
					}
				case 3:
					{
						// Mailer
						FillGrid_2(3);
						intAction = 3;
						chkBulkMailing.Enabled = true;
						chkCombineService.Enabled = FCConvert.CBool(modUTStatusPayments.Statics.TownService == "B");
						chkPastDueOnly.Enabled = false;
						lblTitle.Visible = false;
						txtReportTitle.Visible = false;
						break;
					}
				case 4:
					{
						// Outprinting File
						FillGrid_2(4);
						intAction = 4;
						chkBulkMailing.Enabled = false;
						chkCombineService.Enabled = FCConvert.CBool(modUTStatusPayments.Statics.TownService == "B");
						chkPastDueOnly.Enabled = true;
						lblTitle.Visible = false;
						txtReportTitle.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void vsGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			switch (intAction)
			{
				case 2:
					{
						if (vsGrid.Row == 3)
						{
							if (Strings.Trim(vsGrid.TextMatrix(vsGrid.Row, 1)) == "__/__/____")
							{
								vsGrid.TextMatrix(vsGrid.Row, 1, string.Empty);
							}
						}
						break;
					}
				case 3:
					{
						if (vsGrid.Row == 5)
						{
							if (Strings.Trim(vsGrid.TextMatrix(vsGrid.Row, 1)) == "__/__/____")
							{
								vsGrid.TextMatrix(vsGrid.Row, 1, string.Empty);
							}
						}
						break;
					}
				case 4:
					{
						if (vsGrid.Row == 3)
						{
							if (Strings.Trim(vsGrid.TextMatrix(vsGrid.Row, 1)) == "__/__/____")
							{
								vsGrid.TextMatrix(vsGrid.Row, 1, string.Empty);
							}
						}
						break;
					}
			}
			//end switch
		}

		private void vsGrid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsGrid.Col > 0)
			{
				vsGrid.ComboList = Strings.Trim(strList[vsGrid.Row, intAction] + " ");
				// If boolChange Then
				// if the type has changed then
				switch (intAction)
				{
					case 0:
						{
							// Labels
							// set the first cell to the first item in the string so that it will merge into one cell
							vsGrid.EditMask = "";
							break;
						}
					case 1:
						{
							// Post Cards
							vsGrid.EditMask = "";
							break;
						}
					case 2:
						{
							// Forms
							if (vsGrid.Row == 3)
							{
								vsGrid.EditMask = "##/##/####";
							}
							else
							{
								vsGrid.EditMask = "";
							}
							break;
						}
					case 3:
						{
							// Mailer
							if (vsGrid.Row == 5)
							{
								vsGrid.EditMask = "##/##/####";
							}
							else
							{
								vsGrid.EditMask = "";
							}
							break;
						}
					case 4:
						{
							// OutPrinting
							if (vsGrid.Row == 3)
							{
								vsGrid.EditMask = "##/##/####";
							}
							else
							{
								vsGrid.EditMask = "";
							}
							break;
						}
				}
				//end switch
				// End If
			}
		}

		private void vsGrid_ChangeEdit(object sender, System.EventArgs e)
		{
			int intIndex = 0;
			if (intAction == 0)
			{
				switch (vsGrid.Row)
				{
					case 1:
						{
							intLabelType = FCConvert.ToInt32(vsGrid.ComboData(vsGrid.ComboIndex));
							intIndex = labLabelTypes.Get_IndexFromID(intLabelType);
							ToolTip1.SetToolTip(vsGrid, labLabelTypes.Get_Description(intIndex));
							break;
						}
				}
				//end switch
			}
		}

		private void vsGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						if (vsGrid.Col > 0 && vsGrid.Row > 0)
						{
							vsGrid.TextMatrix(vsGrid.Row, vsGrid.Col, "");
						}
						break;
					}
			}
			//end switch
		}

		private void vsGrid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						// If vsGrid.Col > 0 And vsGrid.Row > 0 Then
						// vsGrid.Select 0, 0
						// vsGrid.Select Row, Col
						// vsGrid.TextMatrix(Row, Col) = ""
						// End If
						break;
					}
			}
			//end switch
		}

		private void vsGrid_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsGrid[e.ColumnIndex, e.RowIndex];
            int intIndex = 0;
			if (intAction == 0 && vsGrid.GetFlexRowIndex(e.RowIndex) == 1 && vsGrid.GetFlexColIndex(e.ColumnIndex) == 1)
			{
				intIndex = labLabelTypes.Get_IndexFromID(intLabelType);
				if (intLabelType != -1)
				{
					//ToolTip1.SetToolTip(vsGrid, labLabelTypes.Get_Description(intIndex));
					cell.ToolTipText = labLabelTypes.Get_Description(intIndex);
				}
				else
				{
                    //ToolTip1.SetToolTip(vsGrid, "");
                    cell.ToolTipText = "";
				}
			}
			else
			{
				if (vsGrid.Row == 1 && vsGrid.Col == 1)
				{
					// do nothing
				}
				else
				{
                    //ToolTip1.SetToolTip(vsGrid, "");
                    cell.ToolTipText = "";
				}
			}
		}

		private void vsGrid_RowColChange(object sender, System.EventArgs e)
		{
			switch (vsGrid.Col)
			{
				case 0:
					{
						vsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				default:
					{
						if (FCConvert.ToInt32(vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsGrid.Row, vsGrid.Col)) != modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX)
						{
							vsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						}
						else
						{
							vsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						break;
					}
			}
			//end switch
			if (vsGrid.Row == vsGrid.Rows - 1)
			{
				// if this is the last row in the grid
				if (FCConvert.ToInt32(vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsGrid.Row, 2)) != modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX && vsGrid.Col == 3)
				{
					// if the third col is grayed out then if it is the second then
					vsGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					if (vsGrid.Col == 1)
					{
						vsGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						vsGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
					}
				}
			}
			else
			{
				vsGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void BuildOutPrintingFile(ref string strSQL, ref bool boolWater)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will create a file with the reminder notice information
				bool boolOK;
				clsDRWrapper rsFile = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				string strBuildString = "";
				// this is the string that will be built and placed as one record in the file
				double dblCurrentDue = 0;
				// this is the total tax due up to this period
				double dblPeriodDue = 0;
				// this is the tax that will be due next period
				double dblCurrentPaid;
				// total Principal Paid
				int lngLandVal = 0;
				// Land Value
				int lngBuildingVal = 0;
				// Building Value
				int lngExemptVal = 0;
				// Exemption Value
				string strLocation = "";
				// Location string that will be built
				string strPayment = "";
				// this is set to the value of the payment field in the grid "First", "Second", "Third" or "Fourth"
				clsDRWrapper rsTemp = new clsDRWrapper();
				int intPeriod;
				double dblInterestDue;
				double dblTotalDue = 0;
				double dblTotalAccountDue = 0;
				string strType = "";
				DateTime dtChargeInterestUntil;
				double dblCurInterest = 0;
				boolOK = true;
				// get the interest date for all of the accounts
				dtChargeInterestUntil = DateAndTime.DateValue(vsGrid.TextMatrix(3, 1));
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
				MDIParent.InstancePtr.CommonDialog1.DefaultExt = "TXT";
				MDIParent.InstancePtr.CommonDialog1.ShowSave();
				if (MDIParent.InstancePtr.CommonDialog1.FileName != "")
				{
					rsFile.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					if (rsFile.RecordCount() > 0)
					{
						frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Saving Records", true, rsFile.RecordCount(), true);
						// open the file
						FCFileSystem.FileOpen(1, MDIParent.InstancePtr.CommonDialog1.FileName, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						while (!rsFile.EndOfFile())
						{
							frmWait.InstancePtr.IncrementProgress();
							//Application.DoEvents();
							strBuildString = "";
							// start with a clean string
							if (boolWater)
							{
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								rsTemp.OpenRecordset("SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE Service <> 'S' AND Bill.ID = " + rsFile.Get_Fields("Bill"), modExtraModules.strUTDatabase);
							}
							else
							{
								// kgk 07-30-2012           rsTemp.OpenRecordset "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE Service <> 'M' AND Bill.ID = " & rsFile.Fields("Bill"), strUTDatabase
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								rsTemp.OpenRecordset("SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE Service <> 'W' AND Bill.ID = " + rsFile.Get_Fields("Bill"), modExtraModules.strUTDatabase);
							}
							if (!rsTemp.EndOfFile())
							{
								// set all of the variables
								dblCurrentDue = 0;
								if (boolWater)
								{
									dblPeriodDue = rsTemp.Get_Fields_Double("WPrinOwed");
									dblCurrentPaid = rsTemp.Get_Fields_Double("WPrinPaid");
								}
								else
								{
									dblPeriodDue = rsTemp.Get_Fields_Double("SPrinOwed");
									dblCurrentPaid = rsTemp.Get_Fields_Double("SPrinPaid");
								}
								// create the location string
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("StreetNumber"))) != "")
								{
									if (FCConvert.ToString(rsTemp.Get_Fields_String("Apt")) != "")
									{
										// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
										strLocation = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("StreetNumber"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
										strLocation = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("StreetNumber"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
									}
								}
								else
								{
									strLocation = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("StreetName")));
								}
								// Create the string by filling in the information
								// Size of field - Name of field
								// 6  - Account
								strBuildString = modMain.PadToString_8(rsTemp.Get_Fields_Int32("ActualAccountNumber"), 8);
								// 38 - RE Name
								// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
								if (rsTemp.Get_Fields_String("OName") != rsTemp.Get_Fields("OwnerName"))
								{
									// only show the RE name if the RE and the CL name are different
									// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
									strBuildString += modGlobalFunctions.PadStringWithSpaces("C/O " + rsTemp.Get_Fields("OwnerName"), 38, false);
								}
								else
								{
									strBuildString += modGlobalFunctions.PadStringWithSpaces("", 38, false);
								}
								// 34 - Addr1
								// TODO Get_Fields: Field [Bill.OAddress1] not found!! (maybe it is an alias?)
								strBuildString += modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields("Bill.OAddress1"), 34, false);
								// 34 - Addr2
								// TODO Get_Fields: Field [Bill.OAddress2] not found!! (maybe it is an alias?)
								strBuildString += modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields("Bill.OAddress2"), 34, false);
								// 24 - Addr3
								// TODO Get_Fields: Field [Bill.OAddress3] not found!! (maybe it is an alias?)
								strBuildString += modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields("Bill.OAddress3"), 24, false);
								// 20  - City
								// TODO Get_Fields: Field [Bill.OCity] not found!! (maybe it is an alias?)
								strBuildString += modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields("Bill.OCity"), 20, false);
								// 2  - State
								// TODO Get_Fields: Field [Bill.OState] not found!! (maybe it is an alias?)
								strBuildString += modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields("Bill.OState"), 2, false);
								// 5  - Zip
								// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
								strBuildString += modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields("Bill.OZip"), 5, false);
								// 5  - Zip 4
								// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
								if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Bill.OZip4"))) != "")
								{
									// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
									strBuildString += modGlobalFunctions.PadStringWithSpaces("-" + rsTemp.Get_Fields("Bill.OZip4"), 5, false);
								}
								else
								{
									strBuildString += modGlobalFunctions.PadStringWithSpaces("", 5);
								}
								if ((boolWater && FCConvert.ToInt32(rsTemp.Get_Fields_Int32("WLienRecordNumber")) != 0) || (!boolWater && FCConvert.ToInt32(rsTemp.Get_Fields_Int32("SLienRecordNumber")) != 0))
								{
									strType = "L";
									if (boolWater)
									{
										rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields_Int32("WLienRecordNumber"));
									}
									else
									{
										rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsTemp.Get_Fields_Int32("SLienRecordNumber"));
									}
									if (!rsLien.EndOfFile())
									{
										dblCurInterest = 0;
										dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLien, dtChargeInterestUntil, ref dblCurInterest, boolWater);
									}
									else
									{
										dblTotalDue = 0;
										dblCurInterest = 0;
									}
								}
								else
								{
									strType = "R";
									dblCurInterest = 0;
									dblTotalDue = modUTCalculations.CalculateAccountUT(rsTemp, dtChargeInterestUntil, ref dblCurInterest, boolWater);
								}
								// 34 - Name
								strBuildString += modGlobalFunctions.PadStringWithSpaces(rsTemp.Get_Fields_String("OName"), 34, false);
								// 12 - Land Val
								strBuildString += modGlobalFunctions.PadStringWithSpaces(lngLandVal.ToString(), 12);
								// 12 - Building Val
								strBuildString += modGlobalFunctions.PadStringWithSpaces(lngBuildingVal.ToString(), 12);
								// 12 - Land + Building
								strBuildString += modGlobalFunctions.PadStringWithSpaces((lngBuildingVal + lngLandVal).ToString(), 12);
								// 12 - Exempt Val
								strBuildString += modGlobalFunctions.PadStringWithSpaces(lngExemptVal.ToString(), 12);
								// 12 - Land + Building - Exempt
								strBuildString += modGlobalFunctions.PadStringWithSpaces(FCConvert.ToString((lngBuildingVal + lngLandVal - lngExemptVal)), 12);
								// 31 - Location
								strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Trim(strLocation), 31, false);
								// 17 - Map Lot
								if (modMain.Statics.boolRE)
								{
									strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSMapLot"))), 17, false);
								}
								else
								{
									strBuildString += modGlobalFunctions.PadStringWithSpaces("", 17, false);
								}
								// 10 - Interest Date
								strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dtChargeInterestUntil, "MM/dd/yyyy"), 10);
								// 15 - Past Due
								strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblCurrentDue, "####0.00"), 15);
								// 15 - Amount Coming Due
								strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblPeriodDue, "####0.00"), 15);
								// 15 - Interest and Costs Due
								// XXXXXXXXX COLLECTIONS CODE!!!  TaxDue1, TaxDue2,...
								if (strType == "L")
								{
									if (FCConvert.ToDecimal(dblTotalDue) - rsTemp.Get_Fields_Decimal("PrincipalPaid") - (rsTemp.Get_Fields_Decimal("TaxDue1") + rsTemp.Get_Fields_Decimal("TaxDue2") + rsTemp.Get_Fields_Decimal("TaxDue3") + rsTemp.Get_Fields_Decimal("TaxDue4")) > 0)
									{
										strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(FCConvert.ToDecimal(dblTotalDue) - (rsTemp.Get_Fields_Decimal("TaxDue1") + rsTemp.Get_Fields_Decimal("TaxDue2") + rsTemp.Get_Fields_Decimal("TaxDue3") + rsTemp.Get_Fields_Decimal("TaxDue4")), "####0.00"), 15);
									}
									else
									{
										strBuildString += modGlobalFunctions.PadStringWithSpaces("0.00", 15, true);
									}
								}
								else
								{
									if ((FCConvert.ToDecimal(dblTotalDue) + rsTemp.Get_Fields_Decimal("PrincipalPaid")) - (rsTemp.Get_Fields_Decimal("TaxDue1") + rsTemp.Get_Fields_Decimal("TaxDue2") + rsTemp.Get_Fields_Decimal("TaxDue3") + rsTemp.Get_Fields_Decimal("TaxDue4")) > 0)
									{
										strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format((FCConvert.ToDecimal(dblTotalDue) + rsTemp.Get_Fields_Decimal("PrincipalPaid")) - (rsTemp.Get_Fields_Decimal("TaxDue1") + rsTemp.Get_Fields_Decimal("TaxDue2") + rsTemp.Get_Fields_Decimal("TaxDue3") + rsTemp.Get_Fields_Decimal("TaxDue4")), "####0.00"), 15);
									}
									else
									{
										strBuildString += modGlobalFunctions.PadStringWithSpaces("0.00", 15, true);
									}
								}
								// 15 - Total Due
								strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblTotalDue, "####0.00"), 15);
								// 15 - Total Account Due
								dblTotalAccountDue = modUTCalculations.CalculateAccountUTTotal(rsTemp.Get_Fields_Int32("AccountKey"), boolWater);
								strBuildString += modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblTotalAccountDue, "####0.00"), 15);
								// 1 - Type
								strBuildString += modGlobalFunctions.PadStringWithSpaces(strType, 1);
								FCFileSystem.PrintLine(1, strBuildString);
							}
							rsFile.MoveNext();
						}
						FCFileSystem.FileClose(1);
						boolOK = true;
						if (boolOK)
						{
							frmWait.InstancePtr.Unload();
							MessageBox.Show(MDIParent.InstancePtr.CommonDialog1.FileName + " was saved successfully.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
							frmReportViewer.InstancePtr.Init(rptOutprintingFormat.InstancePtr);
							// this will show the format of the file that was printed
						}
					}
					else
					{
						MessageBox.Show("No eligible records found.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					MessageBox.Show("Please name the file to save.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCFileSystem.FileClose(1);
				frmWait.InstancePtr.Unload();
				if (Information.Err(ex).Number == 32755)
				{
					// cancel was selected
				}
				else
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Out Printing Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp;
				string strMain;
				string strMinimum;
				string strLMinimum = "";
				string strOrder = "";
				vsGrid.Select(0, 0);
				strTemp = "";
				strMain = "";
				strMinimum = "";
				// check all of the questions and
				switch (intAction)
				{
					case 0:
						{
							// Labels
							// 3 - Name
							if (Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(2, 2)) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" && Strings.Trim(vsGrid.TextMatrix(2, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(2, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2))))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(3, 1)) & "'"
									strTemp += " AND OName > '" + Strings.Trim(vsGrid.TextMatrix(2, 1)) + "     ' AND OName < '" + Strings.Trim(vsGrid.TextMatrix(2, 1)) + "zzzzz'";
								}
								else if (Strings.Trim(vsGrid.TextMatrix(2, 2)) != "" && Strings.Trim(vsGrid.TextMatrix(1, 1)) == "")
								{
									// if only the second has data
									strTemp += " AND OName = '" + Strings.Trim(vsGrid.TextMatrix(2, 2)) + "'";
								}
								else if ((Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(2, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(2, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2)) && FCConvert.CBool(Strings.Trim(FCConvert.ToString(vsGrid.TextMatrix(3, 1) != "")))))
								{
									// both fields have different data
									strTemp += " AND OName > '" + Strings.Trim(vsGrid.TextMatrix(2, 1)) + "     ' AND OName < '" + Strings.Trim(vsGrid.TextMatrix(2, 2)) + "zzzzz'";
								}
							}
							// 4 - Account
							if (Strings.Trim(vsGrid.TextMatrix(3, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(3, 2)) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(vsGrid.TextMatrix(3, 1)) != "" && Strings.Trim(vsGrid.TextMatrix(3, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(3, 1)) == Strings.Trim(vsGrid.TextMatrix(3, 2))))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									strTemp += " AND ActualAccountNumber = " + Strings.Trim(vsGrid.TextMatrix(3, 1));
								}
								else if (Strings.Trim(vsGrid.TextMatrix(3, 2)) != "" && Strings.Trim(vsGrid.TextMatrix(3, 1)) == "")
								{
									// if only the second has data
									strTemp += " AND ActualAccountNumber = " + Strings.Trim(vsGrid.TextMatrix(3, 2));
								}
								else if ((Strings.Trim(vsGrid.TextMatrix(3, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(3, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(3, 1)) == Strings.Trim(vsGrid.TextMatrix(3, 2)) && FCConvert.CBool(Strings.Trim(FCConvert.ToString(vsGrid.TextMatrix(3, 1) != "")))))
								{
									// both fields have different data
									strTemp += " AND ActualAccountNumber >= " + Strings.Trim(vsGrid.TextMatrix(3, 1)) + " AND ActualAccountNumber <= " + Strings.Trim(vsGrid.TextMatrix(3, 2));
								}
							}
							break;
						}
					case 2:
						{
							// Name
							if (Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(1, 2)) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" && Strings.Trim(vsGrid.TextMatrix(1, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(1, 1)) == Strings.Trim(vsGrid.TextMatrix(1, 2))))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(1, 1)) & "'"
									// kgk 07-30-2012   strTemp = strTemp & " AND OName > '" & Trim(vsGrid.TextMatrix(1, 1)) & "     ' AND OName < '" & Trim(vsGrid.TextMatrix(1, 1)) & "zzzzz'"
									strTemp += " AND pOwn.Name > '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "     ' AND pOwn.Name < '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "zzzzz'";
								}
								else if (Strings.Trim(vsGrid.TextMatrix(1, 2)) != "" && Strings.Trim(vsGrid.TextMatrix(1, 1)) == "")
								{
									// if only the second has data
									strTemp += " AND OName = '" + Strings.Trim(vsGrid.TextMatrix(1, 2)) + "'";
								}
								else if ((Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(1, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(1, 1)) == Strings.Trim(vsGrid.TextMatrix(1, 2)) && FCConvert.CBool(Strings.Trim(FCConvert.ToString(vsGrid.TextMatrix(1, 1) != "")))))
								{
									// both fields have different data
									strTemp += " AND OName > '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "     ' AND OName < '" + Strings.Trim(vsGrid.TextMatrix(1, 2)) + "zzzzz'";
								}
							}
							// Account
							if (Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(2, 2)) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" && Strings.Trim(vsGrid.TextMatrix(2, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(2, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2))))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									strTemp += " AND ActualAccountNumber = " + Strings.Trim(vsGrid.TextMatrix(2, 1));
								}
								else if (Strings.Trim(vsGrid.TextMatrix(2, 2)) != "" && Strings.Trim(vsGrid.TextMatrix(2, 1)) == "")
								{
									// if only the second has data
									strTemp += " AND ActualAccountNumber = " + Strings.Trim(vsGrid.TextMatrix(2, 2));
								}
								else if ((Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(2, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(2, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2)) && FCConvert.CBool(Strings.Trim(FCConvert.ToString(vsGrid.TextMatrix(2, 1) != "")))))
								{
									// both fields have different data
									strTemp += " AND ActualAccountNumber >= " + Strings.Trim(vsGrid.TextMatrix(2, 1)) + " AND ActualAccountNumber <= " + Strings.Trim(vsGrid.TextMatrix(2, 2));
								}
							}
							break;
						}
					case 1:
					case 3:
						{
							// Post Cards / Mailer
							// 2 - Name
							if (Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(1, 2)) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" && Strings.Trim(vsGrid.TextMatrix(1, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(1, 1)) == Strings.Trim(vsGrid.TextMatrix(1, 2))))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(2, 1)) & "'"
									strTemp += " AND OName > '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "     ' AND OName < '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "zzzzz'";
								}
								else if (Strings.Trim(vsGrid.TextMatrix(1, 2)) != "" && Strings.Trim(vsGrid.TextMatrix(1, 1)) == "")
								{
									// if only the second has data
									strTemp += " AND OName = '" + Strings.Trim(vsGrid.TextMatrix(1, 2)) + "'";
								}
								else if ((Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(1, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(1, 1)) == Strings.Trim(vsGrid.TextMatrix(1, 2)) && FCConvert.CBool(Strings.Trim(FCConvert.ToString(vsGrid.TextMatrix(1, 1) != "")))))
								{
									// both fields have different data
									strTemp += " AND OName > '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "     ' AND OName < '" + Strings.Trim(vsGrid.TextMatrix(1, 2)) + "zzzzz'";
								}
							}
							// 3 - Account
							if (Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(2, 2)) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" && Strings.Trim(vsGrid.TextMatrix(2, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(2, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2))))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									strTemp += " AND ActualAccountNumber = " + Strings.Trim(vsGrid.TextMatrix(2, 1));
								}
								else if (Strings.Trim(vsGrid.TextMatrix(2, 2)) != "" && Strings.Trim(vsGrid.TextMatrix(2, 1)) == "")
								{
									// if only the second has data
									strTemp += " AND ActualAccountNumber = " + Strings.Trim(vsGrid.TextMatrix(2, 2));
								}
								else if ((Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(2, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(2, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2)) && FCConvert.CBool(Strings.Trim(FCConvert.ToString(vsGrid.TextMatrix(2, 1) != "")))))
								{
									// both fields have different data
									strTemp += " AND ActualAccountNumber >= " + Strings.Trim(vsGrid.TextMatrix(2, 1)) + " AND ActualAccountNumber <= " + Strings.Trim(vsGrid.TextMatrix(2, 2));
								}
							}
							break;
						}
					case 4:
						{
							// OutPrinting File
							// Name
							if (Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(1, 2)) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" && Strings.Trim(vsGrid.TextMatrix(1, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(1, 1)) == Strings.Trim(vsGrid.TextMatrix(1, 2))))
								{
									// if the first field has data and the second does not OR they both have the same thing
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(2, 1)) & "'"
									strTemp += " AND OName > '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "     ' AND OName < '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "zzzzz'";
								}
								else if (Strings.Trim(vsGrid.TextMatrix(1, 2)) != "" && Strings.Trim(vsGrid.TextMatrix(1, 1)) == "")
								{
									// if only the second has data
									// strTemp = strTemp & " AND Name1 = '" & Trim(vsGrid.TextMatrix(2, 2)) & "'"
									strTemp += " AND OName > '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "     ' AND OName < '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "zzzzz'";
								}
								else if ((Strings.Trim(vsGrid.TextMatrix(1, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(1, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(1, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2)) && FCConvert.CBool(Strings.Trim(FCConvert.ToString(vsGrid.TextMatrix(1, 1) != "")))))
								{
									// both fields have different data
									strTemp += " AND OName > '" + Strings.Trim(vsGrid.TextMatrix(1, 1)) + "     ' AND OName < '" + Strings.Trim(vsGrid.TextMatrix(1, 2)) + "zzzzz'";
								}
							}
							// Account
							if (Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(2, 2)) != "")
							{
								// one of the fields has data in it
								if ((Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" && Strings.Trim(vsGrid.TextMatrix(2, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(2, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2))))
								{
									// if the first field has data and the second does not OR they both have the same thing that is not ""
									strTemp += " AND ActualAccountNumber = " + Strings.Trim(vsGrid.TextMatrix(2, 1));
								}
								else if (Strings.Trim(vsGrid.TextMatrix(2, 2)) != "" && Strings.Trim(vsGrid.TextMatrix(2, 1)) == "")
								{
									// if only the second has data
									strTemp += " AND ActualAccountNumber = " + Strings.Trim(vsGrid.TextMatrix(2, 2));
								}
								else if ((Strings.Trim(vsGrid.TextMatrix(2, 1)) != "" || Strings.Trim(vsGrid.TextMatrix(2, 2)) == "") || (Strings.Trim(vsGrid.TextMatrix(2, 1)) == Strings.Trim(vsGrid.TextMatrix(2, 2)) && FCConvert.CBool(Strings.Trim(FCConvert.ToString(vsGrid.TextMatrix(2, 1) != "")))))
								{
									// both fields have different data
									strTemp += " AND ActualAccountNumber >= " + Strings.Trim(vsGrid.TextMatrix(2, 1)) + " AND ActualAccountNumber <= " + Strings.Trim(vsGrid.TextMatrix(2, 2));
								}
							}
							break;
						}
				}
				//end switch
				if (Conversion.Val(txtMinimumAmount.Text) != 0)
				{
					if (boolWater)
					{
						strMinimum = " (WPrinOwed - WPrinPaid) >= " + FCConvert.ToString(FCConvert.ToDouble(txtMinimumAmount.Text));
						strLMinimum = " (Principal - PrinPaid) >= " + FCConvert.ToString(FCConvert.ToDouble(txtMinimumAmount.Text));
					}
					else
					{
						strMinimum = " (SPrinOwed - SPrinPaid) >= " + FCConvert.ToString(FCConvert.ToDouble(txtMinimumAmount.Text));
						strLMinimum = " (Principal - PrinPaid) >= " + FCConvert.ToString(FCConvert.ToDouble(txtMinimumAmount.Text));
					}
				}
				else
				{
					if (boolWater)
					{
						strMinimum = " (WPrinOwed - WPrinPaid) > 0";
						strLMinimum = " (Principal - PrinPaid) > 0";
					}
					else
					{
						strMinimum = " (SPrinOwed - SPrinPaid) > 0";
						strLMinimum = " (Principal - PrinPaid) > 0";
					}
				}
				// Create the body of the SQL string
				if (cmbOrder.Text == "Account")
				{
					strOrder = "ORDER BY ActualAccountNumber ";
				}
				else
				{
					strOrder = "ORDER BY BookNumber, Sequence";
				}
				if (boolWater)
				{
					if (intAction == 4)
					{
						strMain = "SELECT * FROM (SELECT Bill, AccountKey, ActualAccountNumber FROM Bill WHERE Service <> 'S' " + strTemp + " AND " + strMinimum + " AND WLienRecordNumber = 0 AND " + strMinimum + " AND BillingRateKey IN" + strRateKeyList;
						strMain += " UNION SELECT Bill, AccountKey, ActualAccountNumber FROM Bill INNER JOIN Lien ON Bill.WLienRecordNumber = Lien.ID WHERE Service <> 'S' " + strTemp + " AND " + strLMinimum + " AND RateKey IN" + strRateKeyList + ") AS qTmp1 ";
					}
					else
					{
						strMain = "SELECT * FROM (SELECT Distinct AccountKey, ActualAccountNumber FROM Bill WHERE Service <> 'S' " + strTemp + " AND " + strMinimum + " AND WLienRecordNumber = 0 AND " + strMinimum + " AND BillingRateKey IN" + strRateKeyList;
						strMain += " UNION SELECT Distinct AccountKey, ActualAccountNumber FROM Bill INNER JOIN Lien ON Bill.WLienRecordNumber = Lien.ID WHERE Service <> 'S' " + strTemp + " AND " + strLMinimum + " AND RateKey IN" + strRateKeyList + ") AS qTmp1 ";
					}
				}
				else
				{
					if (intAction == 4)
					{
						strMain = "SELECT * FROM (SELECT Bill, AccountKey, ActualAccountNumber FROM Bill WHERE Service <> 'W' " + strTemp + " AND " + strMinimum + " AND SLienRecordNumber = 0 AND BillingRateKey IN" + strRateKeyList;
						strMain += " UNION SELECT Bill, AccountKey, ActualAccountNumber FROM Bill INNER JOIN Lien ON Bill.SLienRecordNumber = Lien.ID WHERE Service <> 'W' " + strTemp + " AND " + strLMinimum + " AND RateKey IN" + strRateKeyList + ") AS qTmp1 ";
					}
					else
					{
						strMain = "SELECT * FROM (SELECT Distinct AccountKey, ActualAccountNumber FROM Bill WHERE Service <> 'W' " + strTemp + " AND " + strMinimum + " AND SLienRecordNumber = 0 AND BillingRateKey IN" + strRateKeyList;
						strMain += " UNION SELECT Distinct AccountKey, ActualAccountNumber FROM Bill INNER JOIN Lien ON Bill.SLienRecordNumber = Lien.ID WHERE Service <> 'W' " + strTemp + " AND " + strLMinimum + " AND RateKey IN" + strRateKeyList + ") AS qTmp1 ";
					}
				}
				strMain = "SELECT * FROM (" + strMain + ") AS Temp INNER JOIN MeterTable ON Temp.AccountKey = MeterTable.AccountKey WHERE MeterNumber = 1 " + strOrder;
				BuildSQL = strMain;
				return BuildSQL;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building SQL", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildSQL;
		}

		private void FillGrid_2(short intType)
		{
			FillGrid(ref intType);
		}

		private void FillGrid(ref short intType)
		{
			// this will fill the grid with the questions and potential answers for the user to choose from
			string strTemp = "";
			switch (intType)
			{
				case 0:
					{
						// Labels
						// this will clear the grid
						vsGrid.Rows = 1;
						vsGrid.Rows = 4;
						// change the backcolor of the last column to gray
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, vsGrid.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, vsGrid.Rows - 1, 1, Color.White);
						// show the array titles
						vsGrid.TextMatrix(0, 0, "Criteria");
						vsGrid.TextMatrix(0, 1, "Beginning");
						vsGrid.TextMatrix(0, 2, "Ending");
						vsGrid.TextMatrix(1, 0, "Type");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 1, 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
						// .TextMatrix(2, 0) = "Year"
						// .TextMatrix(2, 1) = FormatYear(gstrLastYearBilled)
						vsGrid.TextMatrix(2, 0, "Name");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 2, 3, 2, Color.White);
						vsGrid.TextMatrix(3, 0, "Account");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 2, 3, 2, Color.White);
						// this is so that the type will merge together
						vsGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeRestrictRows;
						vsGrid.MergeRow(0, true);
						vsReturnAddress.Visible = false;
						vsReturnAddress.Rows = 5;
						break;
					}
				case 1:
					{
						// Post Card
						// this will clear the grid
						vsGrid.Rows = 1;
						vsGrid.Rows = 5;
						// change the backcolor of the last column to gray
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, vsGrid.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, vsGrid.Rows - 1, 1, Color.White);
						// show the array titles
						vsGrid.TextMatrix(0, 0, "Criteria");
						vsGrid.TextMatrix(0, 1, "Beginning");
						vsGrid.TextMatrix(0, 2, "Ending");
						// .TextMatrix(1, 0) = "Year"
						// .TextMatrix(1, 1) = FormatYear(gstrLastYearBilled)
						vsGrid.TextMatrix(1, 0, "Name");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, 1, 2, Color.White);
						vsGrid.TextMatrix(2, 0, "Account");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 2, 2, 2, Color.White);
						vsGrid.TextMatrix(3, 0, "Message");
						vsGrid.TextMatrix(4, 0, "Use Return Address");
						vsGrid.TextMatrix(4, 1, "Yes");
						vsReturnAddress.Visible = true;
						vsReturnAddress.TextMatrix(0, 0, "Return Address");
						vsReturnAddress.Rows = 5;
						// fills in default data
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress1", ref strTemp);
						vsReturnAddress.TextMatrix(1, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress2", ref strTemp);
						vsReturnAddress.TextMatrix(2, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress3", ref strTemp);
						vsReturnAddress.TextMatrix(3, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress4", ref strTemp);
						vsReturnAddress.TextMatrix(4, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeMessage", ref strTemp);
						vsGrid.TextMatrix(3, 1, strTemp);
						// this is so that the type will merge together
						vsGrid.MergeCells = FCGrid.MergeCellsSettings.flexMergeRestrictRows;
						vsGrid.MergeRow(0, true);
						break;
					}
				case 2:
					{
						// Forms
						// this will clear the grid
						vsGrid.Rows = 1;
						vsGrid.Rows = 9;
						// .rows = 10
						// change the backcolor of the last column to gray
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, vsGrid.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						// show the array titles
						vsGrid.TextMatrix(0, 0, "Criteria");
						vsGrid.TextMatrix(0, 1, "Beginning");
						vsGrid.TextMatrix(0, 2, "Ending");
						vsGrid.TextMatrix(1, 0, "Name");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, 1, 2, Color.White);
						vsGrid.TextMatrix(2, 0, "Account");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 2, 2, 2, Color.White);
						vsGrid.TextMatrix(3, 0, "Mail Date");
						vsGrid.TextMatrix(4, 0, "Interest Department");
						// the dept that handles the interest collection
						vsGrid.TextMatrix(5, 0, "Interest Department Phone");
						vsGrid.TextMatrix(6, 0, "Matured Year Department");
						// the dept that handles the matured lien
						vsGrid.TextMatrix(7, 0, "Matured Year Department Phone");
						// .TextMatrix(8, 0) = "High Year"                'this is to see how far back the sub report will show account balances for
						// .TextMatrix(9, 0) = "Last Year Matured"        'this is to output the last year that has matured
						vsGrid.TextMatrix(8, 0, "Use Return Address");
						// kgk 12-16-2011 trout-772  Add return address
						vsGrid.TextMatrix(8, 1, "Yes");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, vsGrid.Rows - 1, 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
						// fills in default data
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeIntDept", ref strTemp);
						vsGrid.TextMatrix(4, 1, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeIntDeptPhone", ref strTemp);
						vsGrid.TextMatrix(5, 1, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeMatDept", ref strTemp);
						vsGrid.TextMatrix(6, 1, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeMatDeptPhone", ref strTemp);
						vsGrid.TextMatrix(7, 1, strTemp);
						// kgk 12-16-2011 trout-772  Add return address to header   vsReturnAddress.Visible = False
						vsReturnAddress.Visible = true;
						vsReturnAddress.TextMatrix(0, 0, "Return Address");
						vsReturnAddress.Rows = 6;
						// fills in default data
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress1", ref strTemp);
						vsReturnAddress.TextMatrix(1, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress2", ref strTemp);
						vsReturnAddress.TextMatrix(2, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress3", ref strTemp);
						vsReturnAddress.TextMatrix(3, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress4", ref strTemp);
						vsReturnAddress.TextMatrix(4, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress5", ref strTemp);
						vsReturnAddress.TextMatrix(5, 0, strTemp);
						break;
					}
				case 3:
					{
						// Mailer
						// this will clear the grid
						vsGrid.Rows = 1;
						vsGrid.Rows = 6;
						// change the backcolor of the last column to gray
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, vsGrid.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						// show the array titles
						vsGrid.TextMatrix(0, 0, "Criteria");
						vsGrid.TextMatrix(0, 1, "Beginning");
						vsGrid.TextMatrix(0, 2, "Ending");
						// .TextMatrix(1, 0) = "Year"
						// .TextMatrix(1, 1) = FormatYear(gstrLastYearBilled)
						vsGrid.TextMatrix(1, 0, "Name");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, 1, 2, Color.White);
						vsGrid.TextMatrix(2, 0, "Account");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 2, 2, 2, Color.White);
						vsGrid.TextMatrix(3, 0, "Show Map and Lot?");
						// only show this when it is mailer or post card
						vsGrid.TextMatrix(3, 1, "Yes");
						vsGrid.TextMatrix(4, 0, "Use Return Address");
						vsGrid.TextMatrix(4, 1, "Yes");
						// .TextMatrix(6, 0) = "Return Address"
						// .TextMatrix(7, 0) = "Return Address"
						// .TextMatrix(8, 0) = "Return Address"
						// .TextMatrix(9, 0) = "Select Payment"
						vsGrid.TextMatrix(5, 0, "Mailing Date");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, 1, 5, 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
						vsReturnAddress.Visible = true;
						vsReturnAddress.TextMatrix(0, 0, "Return Address");
						vsReturnAddress.Rows = 5;
						// fills in default data
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress1", ref strTemp);
						vsReturnAddress.TextMatrix(1, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress2", ref strTemp);
						vsReturnAddress.TextMatrix(2, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress3", ref strTemp);
						vsReturnAddress.TextMatrix(3, 0, strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "ReminderNoticeReturnAddress4", ref strTemp);
						vsReturnAddress.TextMatrix(4, 0, strTemp);
						break;
					}
				case 4:
					{
						// OutPrinting File
						// this will clear the grid
						vsGrid.Rows = 1;
						vsGrid.Rows = 4;
						// change the backcolor of the last column to gray
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, vsGrid.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						// show the array titles
						vsGrid.TextMatrix(0, 0, "Criteria");
						vsGrid.TextMatrix(0, 1, "Beginning");
						vsGrid.TextMatrix(0, 2, "Ending");
						// .TextMatrix(1, 0) = "Year"
						// .TextMatrix(1, 1) = FormatYear(gstrLastYearBilled)
						vsGrid.TextMatrix(1, 0, "Name");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 2, 1, 2, Color.White);
						vsGrid.TextMatrix(2, 0, "Account");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, 2, 2, 2, Color.White);
						vsGrid.TextMatrix(3, 0, "Interest Date");
						vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, 1, 3, 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
						vsReturnAddress.Visible = false;
						vsReturnAddress.Rows = 5;
						break;
					}
			}
			//end switch
			// this will set the height of the grid
			//FC:FINAL:DDU:#i885 - align grids
			if (!vsReturnAddress.Visible)
			{
				vsGrid.Height = fraRN.Height - 50;
				//(vsGrid.Rows * vsGrid.RowHeight(0)) + 70;
			}
			else
			{
				//FC:FINAL:DDU:#i921 - align second grid as well
				vsGrid.Height = fraRN.Height / 2 - 30;
				vsReturnAddress.Top = vsGrid.Top + vsGrid.Height + 20;
				vsReturnAddress.Height = fraRN.Height - vsReturnAddress.Top - 20;
			}
		}

		private void FillStringArray()
		{
			// this will fill the string array of choices that will dropdown in the boxes
			string strRK = "";
			string strNonLienYears = "";
			string strLabels;
			int lngLastLienedYear = 0;
			clsDRWrapper rsYear = new clsDRWrapper();
			FCUtils.EraseSafe(strList);
			// clear the list
			// fill the years array
			// rsYear.OpenRecordset "SELECT DISTINCT BillingYear FROM BillingMaster", strUTDatabase
			// If Not rsYear.EndOfFile Then
			// rsYear.MoveLast
			// Do Until rsYear.BeginningOfFile
			// strRK = strRK & FormatYear(rsYear.Fields("BillingRateKey")) & "|"
			// rsYear.MovePrevious
			// Loop
			// End If
			// fill the years array with non liened years only
			// rsYear.OpenRecordset "SELECT Top 1 RateKey FROM LienRec INNER JOIN BillingMaster ON LienRec.LienRecordNumber = BillingMaster.LienRecordNumber ORDER BY BillingRateKey desc", strUTDatabase
			// If Not rsYear.EndOfFile Then
			// lngLastLienedYear = rsYear.Fields("BillingRateKey")
			// Else
			// lngLastLienedYear = 0
			// End If
			rsYear.OpenRecordset("SELECT DISTINCT BillingRateKey FROM Bill ORDER BY BillingRateKey desc", modExtraModules.strUTDatabase);
			if (!rsYear.EndOfFile())
			{
				rsYear.MoveLast();
				while (!rsYear.BeginningOfFile())
				{
					//Application.DoEvents();
					if (rsYear.Get_Fields_Int32("BillingRateKey") > lngLastLienedYear)
					{
						strNonLienYears += rsYear.Get_Fields_Int32("BillingRateKey") + "|";
						// FormatYear(rsYear.Fields("BillingRateKey")) & "|"
					}
					rsYear.MovePrevious();
				}
			}
			// this should take off the last '|'
			if (strRK.Length > 0)
				strRK = Strings.Left(strRK, strRK.Length - 1);
			// strLabels = "#0;Avery 4013|#1;Avery 4014|#2;Avery 5160,5260,5970|Avery 5161,5261,5661|Avery 5162,5262,5662|Avery 5163,5263,5663"
			strLabels = BuildLabelsComboString();
			// fill the array
			// labels
			strList[1, 0] = strLabels;
			// Label Types
			// strList(2, 0) = strrk              'Year
			strList[2, 0] = "";
			// Name Range
			strList[3, 0] = "";
			// Account Range
			// Post Cards
			// strList(1, 1) = strNonLienYears    'Year
			strList[1, 1] = "";
			// Name Range
			strList[2, 1] = "";
			// Account Range
			strList[3, 1] = "";
			// Message
			strList[4, 1] = "#0;Yes|#1;No";
			// Return Address Question
			strLabels = "#0;Plain Paper (1 per page)|#1;Plain Paper (2 per page)|#2;Full Page Notice";
			// Forms
			strList[1, 2] = "";
			// Name Range
			strList[2, 2] = "";
			// Account Range
			strList[3, 2] = "";
			// Mail Date
			strList[4, 2] = "";
			// Interest Department
			strList[5, 2] = "";
			// Interest Department Phone
			strList[6, 2] = "";
			// Matured Year Department
			strList[7, 2] = "";
			// Matured Year Department Phone
			// strList(8, 2) = strNonLienYears     'High Year
			// strList(9, 2) = strRK               'Last Year Matured
			strList[8, 2] = "#0;Yes|#1;No";
			// Return Address
			// Mailer
			// strList(1, 3) = strNonLienYears    'Year
			strList[1, 3] = "";
			// Name Range
			strList[2, 3] = "";
			// Account Range
			strList[3, 3] = "#0;Yes|#1;No";
			// Show Map Lot?
			strList[4, 3] = "#0;Yes|#1;No";
			// Return Address
			strList[5, 3] = "";
			// Mailing Date
			// OutPrinting File
			// strList(1, 4) = strNonLienYears    'Year
			strList[1, 4] = "";
			// Name Range
			strList[2, 4] = "";
			// Account Range
			strList[3, 4] = "";
			// strList(4, 4) = "#0;First|#1;Second|#2;Third|#3;Fourth" 'Which Payment
		}

		private void ShowAllFrames()
		{
			vsGrid.Visible = true;
			fraGrid.Visible = true;
			fraRN.Visible = true;
			fraType.Visible = true;
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				bool boolOK;
				// this function will check to make sure that the user answered the required questions correctly
				vsGrid.Select(0, 0);
				if (chkSendTo[0].CheckState == Wisej.Web.CheckState.Unchecked && chkSendTo[1].CheckState == Wisej.Web.CheckState.Unchecked)
				{
					// MsgBox "Please select to whom the notice be sent.", vbExclamation, "Invalid Data"
					// MsgBox "Please select whom to send the notice to.", vbExclamation, "Invalid Data"
					MessageBox.Show("Please select the notice recipient.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					ValidateAnswers = false;
					chkSendTo[0].Focus();
					return ValidateAnswers;
				}
				switch (intAction)
				{
					case 0:
						{
							// Labels
							if (vsGrid.TextMatrix(1, 1) != "")
							{
								ValidateAnswers = true;
							}
							else
							{
								ValidateAnswers = false;
								MessageBox.Show("Please select the type of label to print.", "Data Input", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return ValidateAnswers;
							}
							break;
						}
					case 1:
						{
							// Post Cards
							ValidateAnswers = true;
							break;
						}
					case 2:
						{
							// Forms
							ValidateAnswers = true;
							if (vsGrid.TextMatrix(3, 1) != "")
							{
								if (vsGrid.TextMatrix(4, 1) != "")
								{
									if (vsGrid.TextMatrix(5, 1) != "")
									{
										if (vsGrid.TextMatrix(6, 1) != "")
										{
											if (vsGrid.TextMatrix(7, 1) != "")
											{
												// If vsGrid.TextMatrix(8, 1) <> "" Then
												// If vsGrid.TextMatrix(9, 1) <> "" Then
												// ValidateAnswers = True
												// Else
												// last year matured
												// ValidateAnswers = False
												// MsgBox "Please enter the last year matured.", vbExclamation, "Invalid Data"
												// vsGrid.Select 9, 1
												// End If
												// Else
												// high year
												// ValidateAnswers = False
												// MsgBox "Please enter the highest year that is to be shown.", vbExclamation, "Invalid Data"
												// vsGrid.Select 8, 1
												// End If
											}
											else
											{
												// matured year dept phone
												ValidateAnswers = false;
												MessageBox.Show("Please enter the phone number for the matured year department.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
												vsGrid.Select(7, 1);
											}
										}
										else
										{
											// matured year dept
											ValidateAnswers = false;
											MessageBox.Show("Please enter the name for the matured year department.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
											vsGrid.Select(6, 1);
										}
									}
									else
									{
										// interest dept phone
										ValidateAnswers = false;
										MessageBox.Show("Please enter the phone number for the department that collects interest.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										vsGrid.Select(5, 1);
									}
								}
								else
								{
									// Interest Department
									ValidateAnswers = false;
									MessageBox.Show("Please enter the name for the department that collects interest.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									vsGrid.Select(4, 1);
								}
							}
							else
							{
								// mail date
								ValidateAnswers = false;
								MessageBox.Show("Please enter a valid date into the mail Date field.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								vsGrid.Select(3, 1);
							}
							break;
						}
					case 3:
						{
							// Mailer
							if (vsGrid.TextMatrix(5, 1) != "")
							{
								// date
								if (Information.IsDate(vsGrid.TextMatrix(5, 1)))
								{
									ValidateAnswers = true;
								}
								else
								{
									MessageBox.Show("Please enter a valid date into the Date Field.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									vsGrid.Select(5, 1);
								}
							}
							else
							{
								MessageBox.Show("Please enter a value into the Date Field.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								vsGrid.Select(5, 1);
							}
							break;
						}
					case 4:
						{
							// OutPrinting File
							if (vsGrid.TextMatrix(3, 1) != "")
							{
								// date
								if (Information.IsDate(vsGrid.TextMatrix(3, 1)))
								{
									ValidateAnswers = true;
								}
								else
								{
									MessageBox.Show("Please enter a valid date into the Interest Date Field.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									vsGrid.Select(3, 1);
								}
							}
							else
							{
								MessageBox.Show("Please enter a value into the Interest Date Field.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								vsGrid.Select(3, 1);
							}
							break;
						}
				}
				//end switch
				return ValidateAnswers;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Selection", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateAnswers;
		}

		private void vsGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (intAction == 0)
			{
				switch (vsGrid.GetFlexRowIndex(e.RowIndex))
				{
					case 1:
						{
							if (vsGrid.ComboIndex >= 0)
							{
								intLabelType = FCConvert.ToInt32(vsGrid.ComboData(vsGrid.ComboIndex));
							}
							break;
						}
				}
				//end switch
			}
		}

		private void vsReturnAddress_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsReturnAddress.EditMaxLength = 35;
		}

		private void vsReturnAddress_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsReturnAddress.Row > 0)
			{
				vsReturnAddress.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsReturnAddress.EditCell();
			}
			else
			{
				vsReturnAddress.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsReturnAddress_RowColChange(object sender, System.EventArgs e)
		{
			if (vsReturnAddress.Row > 0)
			{
				vsReturnAddress.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsReturnAddress.EditCell();
			}
			else
			{
				vsReturnAddress.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsbMinAmount_ValueChanged(object sender, System.EventArgs e)
		{
			// this allows the scroll bar to adjust the value in the text box
			//if (vsbMinAmount.Value == 0)
			//{
			//    if (FCConvert.ToDouble(txtMinimumAmount.Text)) <= 0)
			//    {
			//        txtMinimumAmount.Text = "0.00";
			//    }
			//    else
			//    {
			//        txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text)) - 0.1, "#,##0.00");
			//        vsbMinAmount.Value = 1;
			//    }
			//}
			//else if (vsbMinAmount.Value == 2)
			//{
			//    txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text)) + 0.1, "#,##0.00");
			//    vsbMinAmount.Value = 1;
			//}
		}

		private void txtMinimumAmount_Enter(object sender, System.EventArgs e)
		{
			txtMinimumAmount.SelectionStart = 0;
			txtMinimumAmount.SelectionLength = txtMinimumAmount.Text.Length;
		}

		private void txtMinimumAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
			{
			}
			else if (KeyAscii == Keys.Delete)
			{
				if (Strings.InStr(1, txtMinimumAmount.Text, ".", CompareConstants.vbBinaryCompare) > 0)
				{
					KeyAscii = (Keys)0;
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMinimumAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMinimumAmount.Text) > 0)
			{
				txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text), "#,##0.00");
			}
			else
			{
				txtMinimumAmount.Text = "0.00";
			}
		}

		private string BuildLabelsComboString()
		{
			string BuildLabelsComboString = "";
			int counter;
			string strTemp;
			// strLabels = "#0;Avery 4013|#1;Avery 4014|#2;Avery 5160,5260,5970|#3;Avery 5161,5261,5661|#4;Avery 5162,5262,5662|#5;Avery 5163,5263,5663|#6;Avery 4030"
			// hide labels that are not supported by BD because of the 5 lines I need to print for an address
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE5066)
				{
					labLabelTypes.Set_Visible(counter, false);
					break;
				}
			}
			strTemp = "";
			// fill combo box with all available types of labels
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				//Application.DoEvents();
				if (labLabelTypes.Get_Visible(counter))
				{
					strTemp += "#" + FCConvert.ToString(labLabelTypes.Get_ID(counter)) + ";" + labLabelTypes.Get_Caption(counter) + "|";
				}
				else
				{
					if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256)
					{
						strTemp += "#" + FCConvert.ToString(labLabelTypes.Get_ID(counter)) + ";" + labLabelTypes.Get_Caption(counter) + "|";
					}
				}
			}
			strTemp = Strings.Left(strTemp, strTemp.Length - 1);
			BuildLabelsComboString = strTemp;
			return BuildLabelsComboString;
		}
	}
}
