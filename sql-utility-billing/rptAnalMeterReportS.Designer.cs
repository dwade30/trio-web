﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptAnalMeterReportS.
	/// </summary>
	partial class rptAnalMeterReportS
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAnalMeterReportS));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMeterSize = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblConsumption = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldConsumption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMeterSize = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooterTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalConsumption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDisclaimer = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDisclaimer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCount,
				this.fldConsumption,
				this.fldMeterSize,
				this.fldCode
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblMeterSize,
				this.lnHeader,
				this.lblCount,
				this.lblConsumption,
				this.lblReportType,
				this.Label1
			});
			this.GroupHeader1.Height = 0.6458333F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooterTitle,
				this.fldTotalConsumption,
				this.lnTotals,
				this.fldTotalCount,
				this.lblDisclaimer
			});
			this.GroupFooter1.Height = 0.6875F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Meter Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblMeterSize
			// 
			this.lblMeterSize.Height = 0.1875F;
			this.lblMeterSize.HyperLink = null;
			this.lblMeterSize.Left = 1F;
			this.lblMeterSize.Name = "lblMeterSize";
			this.lblMeterSize.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 0";
			this.lblMeterSize.Text = "Meter Size";
			this.lblMeterSize.Top = 0.4375F;
			this.lblMeterSize.Width = 2F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0.5F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.625F;
			this.lnHeader.Width = 5.5F;
			this.lnHeader.X1 = 0.5F;
			this.lnHeader.X2 = 6F;
			this.lnHeader.Y1 = 0.625F;
			this.lnHeader.Y2 = 0.625F;
			// 
			// lblCount
			// 
			this.lblCount.Height = 0.1875F;
			this.lblCount.HyperLink = null;
			this.lblCount.Left = 4F;
			this.lblCount.Name = "lblCount";
			this.lblCount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCount.Text = "Count";
			this.lblCount.Top = 0.4375F;
			this.lblCount.Width = 1F;
			// 
			// lblConsumption
			// 
			this.lblConsumption.Height = 0.1875F;
			this.lblConsumption.HyperLink = null;
			this.lblConsumption.Left = 5F;
			this.lblConsumption.Name = "lblConsumption";
			this.lblConsumption.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblConsumption.Text = "Consumption";
			this.lblConsumption.Top = 0.4375F;
			this.lblConsumption.Width = 1F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.1875F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.lblReportType.Text = "-  -  -  -  Sewer  -  -  -  -";
			this.lblReportType.Top = 0.25F;
			this.lblReportType.Width = 7.5F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.Label1.Text = "Code";
			this.Label1.Top = 0.4375F;
			this.Label1.Width = 0.4375F;
			// 
			// fldCount
			// 
			this.fldCount.Height = 0.1875F;
			this.fldCount.Left = 4F;
			this.fldCount.Name = "fldCount";
			this.fldCount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCount.Text = null;
			this.fldCount.Top = 0F;
			this.fldCount.Width = 1F;
			// 
			// fldConsumption
			// 
			this.fldConsumption.Height = 0.1875F;
			this.fldConsumption.Left = 5F;
			this.fldConsumption.Name = "fldConsumption";
			this.fldConsumption.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldConsumption.Text = null;
			this.fldConsumption.Top = 0F;
			this.fldConsumption.Width = 1F;
			// 
			// fldMeterSize
			// 
			this.fldMeterSize.Height = 0.1875F;
			this.fldMeterSize.Left = 1F;
			this.fldMeterSize.Name = "fldMeterSize";
			this.fldMeterSize.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left; ddo-char-set: 0";
			this.fldMeterSize.Text = null;
			this.fldMeterSize.Top = 0F;
			this.fldMeterSize.Width = 3F;
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 0.5F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCode.Text = null;
			this.fldCode.Top = 0F;
			this.fldCode.Width = 0.4375F;
			// 
			// lblFooterTitle
			// 
			this.lblFooterTitle.Height = 0.1875F;
			this.lblFooterTitle.HyperLink = null;
			this.lblFooterTitle.Left = 1F;
			this.lblFooterTitle.Name = "lblFooterTitle";
			this.lblFooterTitle.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblFooterTitle.Text = "Total:";
			this.lblFooterTitle.Top = 0.125F;
			this.lblFooterTitle.Width = 1F;
			// 
			// fldTotalConsumption
			// 
			this.fldTotalConsumption.Height = 0.1875F;
			this.fldTotalConsumption.Left = 5F;
			this.fldTotalConsumption.Name = "fldTotalConsumption";
			this.fldTotalConsumption.OutputFormat = resources.GetString("fldTotalConsumption.OutputFormat");
			this.fldTotalConsumption.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotalConsumption.Text = null;
			this.fldTotalConsumption.Top = 0.125F;
			this.fldTotalConsumption.Width = 1F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 4F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Width = 2F;
			this.lnTotals.X1 = 4F;
			this.lnTotals.X2 = 6F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 4F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.OutputFormat = resources.GetString("fldTotalCount.OutputFormat");
			this.fldTotalCount.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotalCount.Text = null;
			this.fldTotalCount.Top = 0.125F;
			this.fldTotalCount.Width = 1F;
			// 
			// lblDisclaimer
			// 
			this.lblDisclaimer.Height = 0.1875F;
			this.lblDisclaimer.HyperLink = null;
			this.lblDisclaimer.Left = 0F;
			this.lblDisclaimer.Name = "lblDisclaimer";
			this.lblDisclaimer.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblDisclaimer.Text = null;
			this.lblDisclaimer.Top = 0.4375F;
			this.lblDisclaimer.Width = 7.5F;
			// 
			// rptAnalMeterReportS
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDisclaimer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConsumption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMeterSize;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMeterSize;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblConsumption;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalConsumption;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDisclaimer;
	}
}
