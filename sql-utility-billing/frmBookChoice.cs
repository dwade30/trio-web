﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBookChoice.
	/// </summary>
	public partial class frmBookChoice : BaseForm
	{
		public frmBookChoice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBookChoice InstancePtr
		{
			get
			{
				return (frmBookChoice)Sys.GetInstance(typeof(frmBookChoice));
			}
		}

		protected frmBookChoice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/21/2006              *
		// ********************************************************
		string strSql;
		clsDRWrapper rsBE;
		// Billing Edit recordset
		bool FormLvlUp;
		int intFormType;
		// vbPorter upgrade warning: BookArray As int()	OnWrite(string)
		int[] BookArray = null;
		// holds the books to be billed
		int lngRateKey;
		string strRateKeyList;
		string strDateRanges;
		// trouts-227 5.5.2017 rate key selection by date range
		// vbPorter upgrade warning: dtBillDate As DateTime	OnWrite(string, DateTime)
		DateTime dtBillDate;
		double dblStartSeq;
		double dblEndSeq;
		int intLabelType;
		// trout-1153 03.27.2017 Acct Label pass parms added
		bool blnLabelAddr;
		string strLabelOrderBy;
		// vbPorter upgrade warning: blnLabelWS1 As bool	OnWriteFCConvert.ToInt16(
		bool blnLabelWS1;
		// vbPorter upgrade warning: blnLabelWS2 As bool	OnWriteFCConvert.ToInt16(
		bool blnLabelWS2;
		bool blnLabelDuplicates;
		bool blnLabelShowNoMeterSetDate;
		DateTime dtLabelSetDate;
		bool blnLabelShortSlip;
		int lngGridColCheck;
		int lngGridColBook;
		int lngGridColDesc;
		int lngGridColStatus;
		int lngGridColClearedDate;
		int lngGridColDataDate;
		int lngGridColCalculatedDate;
		int lngGridColBilledEditDate;
		int lngGridColBilledDate;
		string strService;
		// vbPorter upgrade warning: strPassRKList As string	OnWrite(string, double)
		// vbPorter upgrade warning: blnPassShortSlip As bool	OnWrite(VB.OptionButton)
		// vbPorter upgrade warning: intPassLabelType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: blnPassLabelAddr As bool	OnWrite(VB.OptionButton)
		// vbPorter upgrade warning: blnPassLabelWS1 As short	OnWrite(VB.OptionButton)	OnRead(bool)
		// vbPorter upgrade warning: blnPassLabelWS2 As short	OnWrite(VB.OptionButton)	OnRead(bool)
		// vbPorter upgrade warning: blnPassLabelDuplicates As bool	OnWrite(CheckState)
		public void Init(short intType, int lngPassRK = 0, bool blnShowModal = false, string strPassRKList = "", string strW = "", bool blnPassShortSlip = true, DateTime? dtPassSetDate = null/* DateTime dtPassSetDate = DateTime.Now */, bool blnPassShowNoMtrSetDate = false, int intPassLabelType = 0, bool blnPassLabelAddr = true, string strPassOrderBy = "", short blnPassLabelWS1 = 0, short blnPassLabelWS2 = 0, bool blnPassLabelDuplicates = true, string strPassDateRanges = "")
		{
			intFormType = intType;
			lngRateKey = lngPassRK;
			strRateKeyList = strPassRKList;
			strDateRanges = strPassDateRanges;
			strService = strW;
			intLabelType = intPassLabelType;
			// trout-1153 03.27.2017 Acct Label pass parms added
			blnLabelAddr = blnPassLabelAddr;
			strLabelOrderBy = strPassOrderBy;
			blnLabelWS1 = FCConvert.ToBoolean(blnPassLabelWS1);
			blnLabelWS2 = FCConvert.ToBoolean(blnPassLabelWS2);
			blnLabelDuplicates = blnPassLabelDuplicates;
			dtLabelSetDate = dtPassSetDate ?? DateTime.Now;
			blnLabelShowNoMeterSetDate = blnPassShowNoMtrSetDate;
			blnLabelShortSlip = blnPassShortSlip;
			if (blnShowModal)
			{
				//FC:FINAL:RPU: #i915 - Expand the form
				this.Width = App.MainForm.MdiClient.Width;
				this.Height = App.MainForm.MdiClient.Height;
				this.Show(FCForm.FormShowEnum.Modal);
			}
			else
			{
				this.Show(App.MainForm);
			}
		}

		private void frmBookChoice_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
				return;
			Format_Grid();
			Fill_Grid();
			// populates flex grid
			FormLvlUp = false;
			vsBEChoice.Focus();
		}

		private void cmdProcess_Click(object sender, System.EventArgs e)
		{
			try
			{
                bool formUnloaded = false;
				// On Error GoTo ERROR_HANDLER
				int i;
				short ct = 0;
				string strOrderBy = "";
				string strPrinter = "";
				int NumFonts = 0;
				// variables used to find a printer font for the printer chosen
				bool boolUseFont = false;
				int intCPI = 0;
				int x;
				string strFont = "";
				int intReturn;
				string strSql = "";
				// string that holds book SQL statement
				string strLabelSQL = "";
				// complete SQL statement
				int counter;
				// counter used in building the SQL statement for labels
				string strOldPrinter = "";
				// fills the array with the selected books from the BillingEditChoice form
				for (i = 1; i <= vsBEChoice.Rows - 1; i++)
				{
					if (vsBEChoice.TextMatrix(i, lngGridColCheck) != "")
					{
						if (FCConvert.ToDouble(vsBEChoice.TextMatrix(i, lngGridColCheck)) == -1)
						{
							ct += 1;
							Array.Resize(ref BookArray, ct + 1);
							BookArray[ct] = FCConvert.ToInt32(vsBEChoice.TextMatrix(i, lngGridColBook));
						}
					}
				}
				if (ct > 0)
				{
					if (intFormType == 1)
					{
                        // Data Entry
                        //FC:FINAL:SBE - #3820 - Workaround:unload form before showing new form, to avoid unnecessary validating event
                        this.Unload();
                        FCUtils.ApplicationUpdate(App.MainForm);
                        formUnloaded = true;
						frmDataEntry.InstancePtr.Init(ref BookArray[1]);
						// this only allows one book
					}
					else if (intFormType == 2)
					{
						// Calculate
						frmCalculation.InstancePtr.Init(true, ct, ref BookArray);
					}
					else if (intFormType == 3)
					{
						// Billed Edit
						frmCalculation.InstancePtr.Init(false, ct, ref BookArray);
					}
					else if (intFormType == 4)
					{
						// Create Bills
						frmBillsPreview.InstancePtr.Init(ct, ref BookArray, FCConvert.CBool(cmbInitial.Text == "Initial"), ref lngRateKey);
						frmRateRecChoice.InstancePtr.Unload();
					}
					else if (intFormType == 5)
					{
						// Print Bills
						strSql = "";
						if (ct > 0)
						{
							if (ct == 1)
							{
								strSql = "Book = " + FCConvert.ToString(BookArray[1]);
							}
							else
							{
								strSql = "Book IN (";
								for (i = 1; i <= ct; i++)
								{
									strSql += FCConvert.ToString(BookArray[i]) + ",";
								}
								if (strSql != string.Empty)
								{
									strSql = Strings.Left(strSql, strSql.Length - 1) + ")";
								}
							}
						}

						frmBillPrintOptions.InstancePtr.Init(ref lngRateKey, ref strSql);
						frmRateRecChoice.InstancePtr.Unload();
					}
					else if (intFormType == 6)
					{
						// Transfer Report
						dtBillDate = FCConvert.ToDateTime(modCoreysUTCode.GetBillingDateFromRate(lngRateKey));
						rptBillTransferReportMaster.InstancePtr.Init(ref BookArray, ref lngRateKey, ref dtBillDate, strDateRanges);
						frmRateRecChoice.InstancePtr.Unload();
					}
					else if (intFormType == 7)
					{
						// Billed List
						frmRateRecChoice.InstancePtr.Unload();
					}
					else if (intFormType == 8)
					{
						// Tracker Reference: 14181
						// Non-Billed Records
						dtBillDate = FCConvert.ToDateTime(modCoreysUTCode.GetBillingDateFromRate(lngRateKey));
						rptBillNonTransferReportMaster.InstancePtr.Init(ref BookArray, ref lngRateKey, ref dtBillDate);
						frmRateRecChoice.InstancePtr.Unload();
					}
					else if (intFormType == 10)
					{
						// Print Meter Reading Slips
						strOrderBy = strLabelOrderBy;
						Information.Err().Clear();
						try
						{
							CommonDialog1.ShowSave();
						}
						catch
						{
						}
						if (Information.Err().Number == 0)
						{
							strPrinter = FCGlobal.Printer.DeviceName;
						}
						else
						{
							return;
						}
						// If frmSetupMeterReports.optShort Then
						if (blnLabelShortSlip)
						{
							// trout-1153 03.27.2017 Acct Label pass parms added
							if (modPrinterFunctions.PrintXsForAlignment("The X should have printed on the K of the line titled:" + "\r\n" + "FROM PREVIOUS BOOK", 36, 1, strPrinter) == DialogResult.Cancel)
							{
								return;
							}
							else
							{
								rptShortMeterReadingSlip.InstancePtr.Init("S", ref strOrderBy, ref strPrinter, ct, ref BookArray);
							}
						}
						else
						{
							if (modPrinterFunctions.PrintXsForAlignment("The Xs should have printed with the middle X on the line of the consumption field.", 30, 3, strPrinter) == DialogResult.Cancel)
							{
								return;
							}
							else
							{
								rptLongMeterReadingSlip.InstancePtr.Init("S", ref strOrderBy, ref strPrinter, ct, ref BookArray);
							}
						}
					}
					else if (intFormType == 11)
					{
						// Print Meter Exchange List
						strOrderBy = strLabelOrderBy;

						rptMeterExchangeList.InstancePtr.Init("S", ref strOrderBy, dtLabelSetDate, blnLabelShowNoMeterSetDate, ct, ref BookArray);
					}
					else if (intFormType == 12)
					{
						if (strLabelOrderBy == "N")
						{
							// trout-1153 03.27.2017 Acct Label pass parms added
							strOrderBy = " ORDER BY Name";
						}
						else if (strLabelOrderBy == "L")
						{
							strOrderBy = " ORDER BY StreetName, StreetNumber";
						}
						else
						{
							strOrderBy = " ORDER BY UTBook, AccountNumber";
							// " ORDER BY AccountNumber"
						}
						// Get printer for label printing
						Information.Err().Clear();

						if (Information.Err().Number == 0)
						{
							strPrinter = FCGlobal.Printer.DeviceName;
						}
						else
						{
							return;
						}
						// if we are only reporting on selected books then build the sql string to find the records we want
						if (Information.UBound(BookArray, 1) == 1)
						{
							strSql = " = " + FCConvert.ToString(BookArray[1]);
						}
						else
						{
							for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
							{
								strSql += FCConvert.ToString(BookArray[counter]) + ",";
							}
							strSql = "IN (" + Strings.Left(strSql, strSql.Length - 1) + ")";
						}

						if (blnLabelWS2)
						{
							// trout-1153 03.27.2017 Acct Label pass parms added
							if (blnLabelDuplicates)
							{
								// kgk                    strLabelSQL = "SELECT * FROM Master WHERE Key IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE BookNumber " & strSQL & " AND Left(Service,1) <> 'W')" & strOrderBy
								strLabelSQL = "SELECT Master.AccountNumber, StreetName, StreetNumber, Apt, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID WHERE Master.ID IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE BookNumber " + strSql + " AND Left(Service,1) <> 'W')" + strOrderBy;
							}
							else
							{
								// kgk                    strLabelSQL = "SELECT AccountNumber, Name, StreetName, StreetNumber, Apt, BAddress1, BAddress2, BAddress3, BCity, BState, BZip, BZip4 FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL & " AND Left(Service,1) <> 'W' " & strOrderBy
								strLabelSQL = "SELECT AccountNumber, StreetName, StreetNumber, Apt, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID) WHERE BookNumber " + strSql + " AND Left(Service,1) <> 'W' " + strOrderBy;
							}
						}
						else if (blnLabelWS1)
						{
							if (blnLabelDuplicates)
							{
								// kgk                    strLabelSQL = "SELECT * FROM Master WHERE Key IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE BookNumber " & strSQL & " AND Left(Service,1) <> 'S')" & strOrderBy
								strLabelSQL = "SELECT Master.AccountNumber, StreetName, StreetNumber, Apt, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID WHERE Master.ID IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE BookNumber " + strSql + " AND Left(Service,1) <> 'S')" + strOrderBy;
							}
							else
							{
								// kgk                    strLabelSQL = "SELECT AccountNumber, Name, StreetName, StreetNumber, Apt, BAddress1, BAddress2, BAddress3, BCity, BState, BZip, BZip4 FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL & " AND Left(Service,1) <> 'S' " & strOrderBy
								strLabelSQL = "SELECT AccountNumber, StreetName, StreetNumber, Apt, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID) WHERE BookNumber " + strSql + " AND Left(Service,1) <> 'S' " + strOrderBy;
							}
						}
						else
						{
							// If frmSetupMeterReports.chkDuplicateLabels = vbUnchecked Then
							if (blnLabelDuplicates)
							{
								// kgk                    strLabelSQL = "SELECT * FROM Master WHERE Key IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE BookNumber " & strSQL & ")" & strOrderBy
								strLabelSQL = "SELECT Master.AccountNumber, StreetName, StreetNumber, Apt, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID WHERE Master.ID IN (SELECT DISTINCT AccountKey FROM MeterTable WHERE BookNumber " + strSql + ")" + strOrderBy;
							}
							else
							{
								// kgk                    strLabelSQL = "SELECT AccountNumber, Name, StreetName, StreetNumber, Apt, BAddress1, BAddress2, BAddress3, BCity, BState, BZip, BZip4 FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL & strOrderBy
								strLabelSQL = "SELECT AccountNumber, StreetName, StreetNumber, Apt, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID) WHERE BookNumber " + strSql + strOrderBy;
							}
						}
						// Find a printer font for the printer we are using
						NumFonts = FCGlobal.Printer.FontCount;
						boolUseFont = false;
						intCPI = 10;
						for (x = 0; x <= NumFonts - 1; x++)
						{
							strFont = FCGlobal.Printer.Fonts[x];
							if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
							{
								strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
								if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
								{
									boolUseFont = true;
									strFont = FCGlobal.Printer.Fonts[x];
									break;
								}
							}
						}
						// x
						// SHOW THE REPORT
						if (!boolUseFont)
							strFont = "";

						if (blnLabelAddr == true)
						{
							// trout-1153 03.27.2017 kjr Label options not working when books selected
							rptAccountLabels.InstancePtr.Init(ref strLabelSQL, "ADDRESSLABELS", intLabelType, ref strPrinter, ref strFont);
						}
						else
						{
							rptAccountLabels.InstancePtr.Init(ref strLabelSQL, "LOCATIONLABELS", intLabelType, ref strPrinter, ref strFont);
						}
					}
					else if (intFormType == 13)
					{
						// Extracting Data to Remote Reader
						modTSCONSUM.CreateExtractForReader(ref BookArray);
					}
					else if (intFormType == 14)
					{
						// Load Previous Readings
						if (Information.UBound(BookArray, 1) == 1)
						{
							strSql = " = " + FCConvert.ToString(BookArray[1]);
						}
						else
						{
							for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
							{
								strSql += FCConvert.ToString(BookArray[counter]) + ",";
							}
							strSql = "IN (" + Strings.Left(strSql, strSql.Length - 1) + ")";
						}
						frmLoadPreviousReadingFigures.InstancePtr.strSQL = strSql;
						frmLoadPreviousReadingFigures.InstancePtr.Show(App.MainForm);
					}
					else if (intFormType == 15)
					{
						// Create Extract File
						modTSCONSUM.CreateTSCONSUMExtractFile(ref BookArray);
					}
					else if (intFormType == 16)
					{
						modProcessTWUTXX41File.ProcessTWUTXX41File(ref ct, ref BookArray);
					}
					else if (intFormType == 17)
					{
						rptDisconnectEditReport.InstancePtr.Init("S", DateAndTime.DateValue(frmSetupDisconnectEditReport.InstancePtr.txtNoticeDate.Text), DateAndTime.DateValue(frmSetupDisconnectEditReport.InstancePtr.txtShutOffDate.Text), ct, ref BookArray, FCConvert.ToInt32(Conversion.Val(frmSetupDisconnectEditReport.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectEditReport.InstancePtr.lngRowRateKey, frmSetupDisconnectEditReport.InstancePtr.lngColData))), Conversion.Val(frmSetupDisconnectEditReport.InstancePtr.txtMinimumAmt.Text));
					}
					else if (intFormType == 18)
					{
						if (frmSetupDisconnectNotices.InstancePtr.cmbNewMailer.Text == "Full Page Plain Paper")
						{
							rptDisconnectNoticeOriginalPlainPaper2.InstancePtr.Init("B", DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtNoticeDate.Text), DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtShutOffDate.Text), FCConvert.ToDecimal(frmSetupDisconnectNotices.InstancePtr.txtMinimumAmount.Text), frmSetupDisconnectNotices.InstancePtr.chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, frmSetupDisconnectNotices.InstancePtr.chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtName.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress1.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress2.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress3.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress4.Text), 0, ref BookArray, frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowPhone, frmSetupDisconnectNotices.InstancePtr.lngColData), FCConvert.ToInt32(Conversion.Val(frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowRateKey, frmSetupDisconnectNotices.InstancePtr.lngColData))), frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowHours, frmSetupDisconnectNotices.InstancePtr.lngColData), frmSetupDisconnectNotices.InstancePtr.txtDefaultText.Text, frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowMuniName, frmSetupDisconnectNotices.InstancePtr.lngColData), Conversion.Val(frmSetupDisconnectNotices.InstancePtr.txtReconFeeReg.Text), Conversion.Val(frmSetupDisconnectNotices.InstancePtr.txtReconFeeAH.Text), frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowGovtContact, frmSetupDisconnectNotices.InstancePtr.lngColData), frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowGovtPhone, frmSetupDisconnectNotices.InstancePtr.lngColData), FCConvert.CBool(frmSetupDisconnectNotices.InstancePtr.chkIncludeDepositText.CheckState == Wisej.Web.CheckState.Checked));
						}
						else if (frmSetupDisconnectNotices.InstancePtr.cmbNewMailer.Text == "Original Plain Paper")
						{
							rptDisconnectNoticeOriginalPlainPaper.InstancePtr.Init("B", DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtNoticeDate.Text), DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtShutOffDate.Text), FCConvert.ToDecimal(frmSetupDisconnectNotices.InstancePtr.txtMinimumAmount.Text), frmSetupDisconnectNotices.InstancePtr.chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, frmSetupDisconnectNotices.InstancePtr.chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtName.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress1.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress2.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress3.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress4.Text), ct, ref BookArray, FCConvert.ToInt32(Conversion.Val(frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowRateKey, frmSetupDisconnectNotices.InstancePtr.lngColData))));
						}
						else if (frmSetupDisconnectNotices.InstancePtr.cmbNewMailer.Text == "Original Mailer")
						{
							Information.Err().Clear();
	
							try
							{
								MDIParent.InstancePtr.CommonDialog1.ShowSave();
							}
							catch
							{
							}
							/*? On Error GoTo 0 */
							if (Information.Err().Number == 0)
							{
								strPrinter = FCGlobal.Printer.DeviceName;
							}
							else
							{
								return;
							}
							NumFonts = FCGlobal.Printer.FontCount;
							boolUseFont = false;
							intCPI = 10;
							for (x = 0; x <= NumFonts - 1; x++)
							{
								strFont = FCGlobal.Printer.Fonts[x];
								if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
								{
									strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
									if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
									{
										boolUseFont = true;
										strFont = FCGlobal.Printer.Fonts[x];
										break;
									}
								}
							}
							// x
							// SHOW THE REPORT
							if (!boolUseFont)
								strFont = "";
							if (modPrinterFunctions.PrintXsForAlignment("The X should have printed at the top right of your mailer directly above the right line of the FROM box.", 38, 1, strPrinter) == DialogResult.Yes)
							{
								rptDisconnectNoticeOriginalMailer.InstancePtr.Init("B", DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtNoticeDate.Text), DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtShutOffDate.Text), FCConvert.ToDecimal(frmSetupDisconnectNotices.InstancePtr.txtMinimumAmount.Text), frmSetupDisconnectNotices.InstancePtr.chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, frmSetupDisconnectNotices.InstancePtr.chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtName.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress1.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress2.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress3.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress4.Text), ct, ref BookArray, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowRateKey, frmSetupDisconnectNotices.InstancePtr.lngColData))));
							}
						}
						else if (frmSetupDisconnectNotices.InstancePtr.cmbNewMailer.Text == "Outprint File")
						{
							modUTLien.DisconnectOutPrint(ref BookArray);
						}
						else if (frmSetupDisconnectNotices.InstancePtr.cmbNewMailer.Text == "Optional Mailer 7in")
						{
							Information.Err().Clear();
							strOldPrinter = FCGlobal.Printer.DeviceName;

							try
							{
								MDIParent.InstancePtr.CommonDialog1.ShowSave();
							}
							catch
							{
							}
							if (Information.Err().Number == 0)
							{
								strPrinter = FCGlobal.Printer.DeviceName;
							}
							else
							{
								return;
							}
							NumFonts = FCGlobal.Printer.FontCount;
							intCPI = 10;
							for (x = 0; x <= NumFonts - 1; x++)
							{
								strFont = FCGlobal.Printer.Fonts[x];
								if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
								{
									strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
									if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
									{
										boolUseFont = true;
										strFont = FCGlobal.Printer.Fonts[x];
										break;
									}
								}
							}
							// x
							// SHOW THE REPORT
							if (!boolUseFont)
								strFont = "";
							if (modPrinterFunctions.PrintXsForAlignment("X's should have printed at the top of the address box of your mailer.", 1, 46, strPrinter) == DialogResult.OK)
							{
								// StartProcess = True
								rptDisconnectNoticeMailer7InchMF.InstancePtr.Init("B", DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtNoticeDate.Text), DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtShutOffDate.Text), FCConvert.ToDecimal(frmSetupDisconnectNotices.InstancePtr.txtMinimumAmount.Text), frmSetupDisconnectNotices.InstancePtr.chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, frmSetupDisconnectNotices.InstancePtr.chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtName.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress1.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress2.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress3.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress4.Text), ct, ref BookArray, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowRateKey, frmSetupDisconnectNotices.InstancePtr.lngColData))));
							}
						}
						else
						{
							Information.Err().Clear();

							try
							{
								MDIParent.InstancePtr.CommonDialog1.ShowSave();
							}
							catch
							{
							}
							/*? On Error GoTo 0 */
							if (Information.Err().Number == 0)
							{
								strPrinter = FCGlobal.Printer.DeviceName;
							}
							else
							{
								return;
							}
							NumFonts = FCGlobal.Printer.FontCount;
							boolUseFont = false;
							intCPI = 10;
							for (x = 0; x <= NumFonts - 1; x++)
							{
								strFont = FCGlobal.Printer.Fonts[x];
								if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
								{
									strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
									if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
									{
										boolUseFont = true;
										strFont = FCGlobal.Printer.Fonts[x];
										break;
									}
								}
							}
							// x
							// SHOW THE REPORT
							if (!boolUseFont)
								strFont = "";
							if (modPrinterFunctions.PrintXsForAlignment("The X should have printed at the top right of your mailer directly above the right line of the FROM box.", 38, 1, strPrinter) == DialogResult.Yes)
							{
								rptDisconnectNoticeNewMailer.InstancePtr.Init("B", DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtNoticeDate.Text), DateAndTime.DateValue(frmSetupDisconnectNotices.InstancePtr.txtShutOffDate.Text), FCConvert.ToDecimal(frmSetupDisconnectNotices.InstancePtr.txtMinimumAmount.Text), frmSetupDisconnectNotices.InstancePtr.chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, frmSetupDisconnectNotices.InstancePtr.chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtName.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress1.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress2.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress3.Text), Strings.Trim(frmSetupDisconnectNotices.InstancePtr.txtAddress4.Text), ct, ref BookArray, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(frmSetupDisconnectNotices.InstancePtr.vsDefaults.TextMatrix(frmSetupDisconnectNotices.InstancePtr.lngRowRateKey, frmSetupDisconnectNotices.InstancePtr.lngColData))));
							}
						}
					}
					else if (intFormType == 20)
					{
						// This is just to show the books w/o any selection, just a place for the user to view them
					}
					else if (intFormType == 21)
					{
						dblStartSeq = Conversion.Val(txtStartSeq.Text);
						dblEndSeq = Conversion.Val(txtEndSeq.Text);
						// Meter Reading List
						rptMeterReadingList.InstancePtr.Init(ref BookArray, dblStartSeq, dblEndSeq);
					}
					else if (intFormType == 25)
					{
						// Analysis Reports
						strSql = "";
						for (i = 1; i <= ct; i++)
						{
							strSql += " Book = " + FCConvert.ToString(BookArray[i]) + " OR";
						}
						if (strSql != string.Empty)
						{
							strSql = Strings.Mid(strSql, 1, strSql.Length - 2);
						}
						frmAnalysisReports.InstancePtr.Init(ref strSql, ref strRateKeyList);
					}
					else if (intFormType == 30)
					{
						// Account Listings
						strSql = "";
						for (i = 1; i <= ct; i++)
						{
							strSql += " BookNumber = " + FCConvert.ToString(BookArray[i]) + " OR";
						}
						if (strSql != string.Empty)
						{
							strSql = Strings.Mid(strSql, 1, strSql.Length - 2);
						}
						frmAccountListing.InstancePtr.Init(ref strSql);
						// pass the book list to the next form
					}
					else if (intFormType == 35)
					{
						// Email Bills
						strSql = "";
						for (i = 1; i <= ct; i++)
						{
							strSql += " Bill.Book = " + FCConvert.ToString(BookArray[i]) + " OR";
						}
						if (strSql != string.Empty)
						{
							strSql = Strings.Mid(strSql, 1, strSql.Length - 2);
						}
						frmBillEmailOptions.InstancePtr.Init(ref lngRateKey, ref strSql);
						frmRateRecChoice.InstancePtr.Unload();
					}
					else if (intFormType == 36)
					{
						// kgk 10-14-2011  trout-733  Invoice Cloud Export
						// Export Bills to PDF for Invoice Cloud
						strSql = "";
						for (i = 1; i <= ct; i++)
						{
							strSql += " Bill.Book = " + FCConvert.ToString(BookArray[i]) + " OR";
						}
						if (strSql != string.Empty)
						{
							strSql = Strings.Mid(strSql, 1, strSql.Length - 2);
						}
						frmBillInvCldExpOptions.InstancePtr.Init(ref lngRateKey, ref strSql);
						frmRateRecChoice.InstancePtr.Unload();
					}
					else if ((intFormType >= 42 && intFormType <= 43) || (intFormType >= 50 && intFormType <= 52))
					{
						//42 'Print 30DN
						//43 'Print Lien
						//50 'CMF 30 DN
						//51 'CMF Lien
						//52 'CMF Lien Mat

						strSql = "";

						for (i = 0; i < ct; i++)
						{
							strSql += " Book = " + FCConvert.ToString(BookArray[i]) + " OR";
						}

						if (strSql != string.Empty)
						{
							strSql = Strings.Mid(strSql, 1, strSql.Length - 2);
						}

						frmRateRecChoice.InstancePtr.Unload();

						frmRateRecChoice.InstancePtr.Init(FCConvert.ToInt16(intFormType - 40), strSql);
					}
					else if (intFormType == 41 || (intFormType >= 44 && intFormType <= 49))
					{ 
						// 41 'Print Lien Edit
						// 44 'Print Lien Mat
						// 45 'Apply 30 DN ''Not sure why this is not used
						// 46 'Transfer To Lien
						// 47 'Lien Mat Fee
						strSql = "";
						for (i = 1; i <= ct; i++)
						{
							strSql += " Bill.Book = " + FCConvert.ToString(BookArray[i]) + " OR";
						}
						if (strSql != string.Empty)
						{
							strSql = Strings.Mid(strSql, 1, strSql.Length - 2);
						}
						frmRateRecChoice.InstancePtr.Unload();
						// kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
						frmRateRecChoice.InstancePtr.Init(FCConvert.ToInt16(intFormType - 40), strSql);
						// frmRateRecChoice
					}
					else if (intFormType == 55)
					{
						// kk04242014 trouts-64  Reset bills by Book(s)
						if (ct == 1)
						{
							strSql = "= " + FCConvert.ToString(BookArray[1]);
						}
						else
						{
							for (i = 1; i <= ct; i++)
							{
								strSql += FCConvert.ToString(BookArray[i]) + ",";
							}
							strSql = "IN (" + Strings.Left(strSql, strSql.Length - 1) + ")";
						}
						modUTBilling.ResetCurrentBilling(ref strSql);
					}
					else if (intFormType == 60)
					{
						// Update Sequence
						frmSequenceBook.InstancePtr.Init(ref BookArray);
					}
					else if (intFormType == 70)
					{
						// Consumption History Report
						frmConsumptionHistoryReport.InstancePtr.Init(ref BookArray, ref strRateKeyList, ref strService, strDateRanges);
					}
					FormLvlUp = false;
                    if (!formUnloaded)
                    {
                        this.Unload();
                    }
                }
                else
				{
					MessageBox.Show("Please select a valid book.", "Select Book", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Processing Book List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdProcess_Click()
		{
			cmdProcess_Click(cmdProcess, new System.EventArgs());
		}

		private void frmBookChoice_Load(object sender, System.EventArgs e)
		{

			lngGridColCheck = 0;
			lngGridColBook = 1;
			lngGridColDesc = 2;
			lngGridColStatus = 3;
			lngGridColClearedDate = 4;
			lngGridColDataDate = 5;
			lngGridColCalculatedDate = 6;
			lngGridColBilledEditDate = 7;
			lngGridColBilledDate = 8;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Select Books";
			mnuProcessBooks_Click(null, EventArgs.Empty);
		}

		private void frmBookChoice_Resize(object sender, System.EventArgs e)
		{
			Format_Grid();
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{

		}

		private void frmBookChoice_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				FormLvlUp = true;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			SelectAllBooks_2(true);
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			cmdProcess_Click();
		}

		private void mnuProcessBooks_Click(object sender, System.EventArgs e)
		{
			switch (intFormType)
			{
				case 6:
				case 7:
				case 8:
				case 11:
				case 12:
				case 13:
				case 14:
				case 15:
				case 17:
				case 18:
				case 21:
				case 25:
				case 30:
				case 70:
				case 35:
					{
						mnuFileSelectAll.Enabled = true;
						cmdFileSelectAll.Enabled = true;
						break;
					}
				default:
					{
						mnuFileSelectAll.Enabled = false;
						cmdFileSelectAll.Enabled = false;
						break;
					}
			}
			//end switch
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			FormLvlUp = true;
			this.Unload();
		}

		private void Fill_Grid()
		{
			int x;
			// temporary variable
			clsDRWrapper rsBill = new clsDRWrapper();
			int LastIndex;
			rsBE = new clsDRWrapper();
			strSql = "SELECT * FROM BOOK";
			// builds the SQL statement to select all records
			rsBE.OpenRecordset(strSql);
			// selects all the records
			vsBEChoice.Rows = 1;
			if (rsBE.EndOfFile() != true && rsBE.BeginningOfFile() != true)
			{
				// if not at the begining or the end of the recordset
				while (!rsBE.EndOfFile())
				{
					vsBEChoice.AddItem("");
					x = vsBEChoice.Rows - 1;
					vsBEChoice.TextMatrix(x, lngGridColBook, Strings.Format(rsBE.Get_Fields_Int32("BookNumber"), "0000"));
					// fills the grid
					vsBEChoice.TextMatrix(x, lngGridColDesc, FCConvert.ToString(rsBE.Get_Fields_String("Description")));
					if (intFormType == 20)
					{
						//FC:FINAL:CHN - issue #1111: remove timestamps.
						// old format was : "MM/dd/yyyy  hh:mm:SS tt"
						vsBEChoice.TextMatrix(x, lngGridColClearedDate, Strings.Format(FCConvert.ToString(rsBE.Get_Fields("CDate")), "MM/dd/yyyy"));
						vsBEChoice.TextMatrix(x, lngGridColDataDate, Strings.Format(rsBE.Get_Fields("DDate"), "MM/dd/yyyy"));
						vsBEChoice.TextMatrix(x, lngGridColCalculatedDate, Strings.Format(FCConvert.ToString(rsBE.Get_Fields("XDate")), "MM/dd/yyyy"));
						vsBEChoice.TextMatrix(x, lngGridColBilledEditDate, Strings.Format(rsBE.Get_Fields("EDate"), "MM/dd/yyyy"));
						vsBEChoice.TextMatrix(x, lngGridColBilledDate, Strings.Format(rsBE.Get_Fields("BDate"), "MM/dd/yyyy"));
					}
					else
					{
						vsBEChoice.TextMatrix(x, lngGridColClearedDate, Strings.Format(rsBE.Get_Fields("CDate"), "MM/dd/yyyy"));
						vsBEChoice.TextMatrix(x, lngGridColDataDate, Strings.Format(rsBE.Get_Fields("DDate"), "MM/dd/yyyy"));
						vsBEChoice.TextMatrix(x, lngGridColCalculatedDate, Strings.Format(rsBE.Get_Fields("XDate"), "MM/dd/yyyy"));
						vsBEChoice.TextMatrix(x, lngGridColBilledEditDate, Strings.Format(rsBE.Get_Fields("EDate"), "MM/dd/yyyy"));
						vsBEChoice.TextMatrix(x, lngGridColBilledDate, Strings.Format(rsBE.Get_Fields("BDate"), "MM/dd/yyyy"));
					}
					if (fecherFoundation.FCUtils.IsNull(rsBE.Get_Fields_String("CurrentStatus")) == false)
					{
						if (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "C")
						{
							vsBEChoice.TextMatrix(x, lngGridColStatus, "C - Cleared");
						}
						else if (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "D")
						{
							vsBEChoice.TextMatrix(x, lngGridColStatus, "D - Data Entry");
						}
						else if (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "X")
						{
							vsBEChoice.TextMatrix(x, lngGridColStatus, "X - Calculated");
							// kk03302015 trouts-11  Change Billed Edit to Calc and Edit
						}
						else if (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "E")
						{
							vsBEChoice.TextMatrix(x, lngGridColStatus, "E - Calc and Edit");
						}
						else if (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "W")
						{
							vsBEChoice.TextMatrix(x, lngGridColStatus, "W - Water Billed");
						}
						else if (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "S")
						{
							vsBEChoice.TextMatrix(x, lngGridColStatus, "S - Sewer Billed");
						}
						else if (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "B")
						{
							vsBEChoice.TextMatrix(x, lngGridColStatus, "B - Billed");
						}
					}
					// set the color of the row
					switch (intFormType)
					{
						case 1:
						case 16:
							{
								// DE
								if ((Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "C") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "D") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "X") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "E"))
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
								}
								else
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								}
								break;
							}
						case 2:
							{
								// Calc
								if ((Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "D") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "X") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "E"))
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
								}
								else
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								}
								break;
							}
						case 3:
							{
								// BE
								if ((Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "X") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "E"))
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
								}
								else
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								}
								break;
							}
						case 4:
							{
								// Create Bills
								if ((Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "E") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "W") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "S"))
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
								}
								else
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								}
								lblInitial.Visible = true;
								cmbInitial.Visible = true;
								break;
							}
						case 5:
						case 36:
							{
								// Print Bills, Outprinting File   'kgk 10-14-2011 Added Invoice Cloud Export
								if (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "B")
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
								}
								else
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								}
								// fraInitial.Visible = True
								break;
							}
						case 8:
							{
								// Non-Billed Report
								vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								break;
							}
						case 10:
						case 11:
						case 12:
						case 13:
						case 14:
						case 15:
						case 17:
						case 18:
							{
								vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								// Tracker Reference: 14178
								break;
							}
						case 21:
							{
								vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								fraSequence.Visible = true;
								break;
							}
						case 55:
							{
								// kk04242014  trouts-64
								if ((Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "D") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "X") || (Strings.Left(FCConvert.ToString(rsBE.Get_Fields_String("CurrentStatus")), 1) == "E"))
								{
									// kk01112016 trouts-175  Take out Cleared status to match selection code
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
								}
								else
								{
									vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								}
								break;
							}
						default:
							{
								// all rows will be white
								vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, x, 0, x, vsBEChoice.Cols - 1, Color.White);
								break;
							}
					}
					//end switch
					if (!rsBE.EndOfFile())
						rsBE.MoveNext();
					// if not the end of the recordset, go to the next
				}
				//FC:FINAL:AAKV - use anchor instead
				//SetGridHeight_2(false);
			}
		}

		private void vsBEChoice_AfterUserResize()
		{
			bool boolScrollBar = false;
			int lngColWidths = 0;
			int intCols;
			// check to see if there is a scroll bar at the bottom, if so, then adjust the height of the grid
			for (intCols = 0; intCols <= vsBEChoice.Cols - 1; intCols++)
			{
				lngColWidths += vsBEChoice.ColWidth(intCols);
			}
			boolScrollBar = FCConvert.CBool(lngColWidths > vsBEChoice.Width);
			//FC:FINAL:AAKV - use anchor instead
			//SetGridHeight(ref boolScrollBar);
		}

		private void vsBEChoice_ClickEvent(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTest = "";
				string strTest2 = "";
				string strTest3 = "";
				// so the user can only click the box of the book that they want billed edited
				if (vsBEChoice.Col == lngGridColCheck)
				{
					// If .Editable = False Then
					// .Editable = True
					// End If
					if (intFormType == 1)
					{
						// data entry
						// instead of selecting this book with a checkmark, just accept
						// the book and go to the data entry screen
						if ((Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == "C") || (Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == "D") || (Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == "X") || (Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == "E"))
						{
							vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(-1));
							if (Information.IsDate(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate)))
							{
								dtBillDate = FCConvert.ToDateTime(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate));
							}
							cmdProcess_Click();
							return;
						}
						else
						{
							vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(0));
							// clear the cell
							return;
						}
					}
					else if (intFormType == 60)
					{
						vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(-1));
						cmdProcess_Click();
						return;
					}
					else if (intFormType == 2)
					{
						// calculate and edit
						strTest = "D";
						strTest2 = "X";
						strTest3 = "E";
					}
					else if (intFormType == 3)
					{
						// billed edit
						strTest = "X";
						strTest2 = "E";
						strTest3 = "Z";
					}
					else if (intFormType == 4)
					{
						// bills
						strTest = "E";
						strTest2 = "Z";
						// nothing
						strTest3 = "Z";
					}
					else if ((intFormType == 5) || (intFormType == 6) || (intFormType == 7) || (intFormType == 10) || (intFormType == 11) || (intFormType == 12) || (intFormType == 13) || (intFormType == 14) || (intFormType == 15) || (intFormType == 17) || (intFormType == 18) || (intFormType == 21) || (intFormType == 25) || (intFormType == 30) || (intFormType == 35) || (intFormType == 36) || (intFormType >= 40 && intFormType <= 52) || (intFormType >= 101 && intFormType <= 107) || (intFormType == 70))
					{
						if (vsBEChoice.Col == lngGridColCheck)
						{
							if (Conversion.Val(vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col)) == 0)
							{
								if (Information.IsDate(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate)))
								{
									dtBillDate = FCConvert.ToDateTime(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate));
								}
								vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(-1));
							}
							else
							{
								vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(0));
							}
						}
						return;
					}
					else if (intFormType == 8)
					{
						// Tracker Reference: 14181
						if (vsBEChoice.Col == lngGridColCheck)
						{
							if (Conversion.Val(vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col)) == 0)
							{
								if (Information.IsDate(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate)))
								{
									// kk03232015 Check for valid bill date
									dtBillDate = FCConvert.ToDateTime(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate));
								}
								vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(-1));
							}
							else
							{
								vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(0));
							}
						}
						return;
					}
					else if (intFormType == 16)
					{
						if ((Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == "C") || (Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == "D") || (Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == "X") || (Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == "E"))
						{
							// only allow Cleared and Data Entered Books through   'kk 12032013  Add X & E back in
							if (Conversion.Val(vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col)) == 0)
							{
								if (Information.IsDate(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate)))
								{
									dtBillDate = FCConvert.ToDateTime(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate));
								}
								else
								{
									dtBillDate = DateAndTime.DateValue(FCConvert.ToString(0));
								}
								vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColCheck, FCConvert.ToString(-1));
							}
							else
							{
								vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColCheck, FCConvert.ToString(0));
							}
							return;
						}
						else
						{
							vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColCheck, FCConvert.ToString(0));
							// clear the cell
							return;
						}
					}
					else if (intFormType == 55)
					{
						// Reset Bills   'kk04302014
						strTest = "D";
						strTest2 = "X";
						strTest3 = "E";
					}
					if (vsBEChoice.Col == lngGridColCheck)
					{
						if (Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == strTest || Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == strTest2 || Strings.Left(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColStatus), 1) == strTest3)
						{
							if (Conversion.Val(vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col)) == 0)
							{
								if (Information.IsDate(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate)))
								{
									dtBillDate = FCConvert.ToDateTime(vsBEChoice.TextMatrix(vsBEChoice.Row, lngGridColBilledDate));
								}
								vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(-1));
							}
							else
							{
								vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(0));
							}
							return;
						}
						else
						{
							vsBEChoice.TextMatrix(vsBEChoice.Row, vsBEChoice.Col, FCConvert.ToString(0));
							return;
						}
					}
				}
				else
				{
					// .Editable = False
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Selecting Book", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
		}

		private void Format_Grid()
		{
			int FGWidth = 0;
			FGWidth = vsBEChoice.WidthOriginal;
			vsBEChoice.Cols = 9;
			if (intFormType == 20)
			{
				vsBEChoice.ColDataType(lngGridColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
				// sets col 0 checkboxes
				vsBEChoice.ColWidth(lngGridColCheck, FCConvert.ToInt32(FGWidth * 0.0));
				// check boxes
				vsBEChoice.ColWidth(lngGridColBook, FCConvert.ToInt32(FGWidth * 0.08));
				// book number
				vsBEChoice.ColWidth(lngGridColDesc, FCConvert.ToInt32(FGWidth * 0.18));
				// description
				vsBEChoice.ColWidth(lngGridColStatus, FCConvert.ToInt32(FGWidth * 0.13));
				// status
				vsBEChoice.ColWidth(lngGridColClearedDate, FCConvert.ToInt32(FGWidth * 0.12));
				// C - status date
				vsBEChoice.ColWidth(lngGridColDataDate, FCConvert.ToInt32(FGWidth * 0.12));
				// D - status date
				vsBEChoice.ColWidth(lngGridColCalculatedDate, FCConvert.ToInt32(FGWidth * 0.12));
				// X - status date
				vsBEChoice.ColWidth(lngGridColBilledEditDate, FCConvert.ToInt32(FGWidth * 0.12));
				// E - status date
				vsBEChoice.ColWidth(lngGridColBilledDate, FCConvert.ToInt32(FGWidth * 0.12));
				// B - status date
				vsBEChoice.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
				vsBEChoice.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
				vsBEChoice.ExtendLastCol = false;
				mnuProcess.Visible = false;
				cmdProcess.Visible = false;
				Seperator.Visible = false;
				this.Text = "Book Status";
			}
			else
			{
				vsBEChoice.ColDataType(lngGridColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
				// sets col 0 checkboxes
				vsBEChoice.ColWidth(lngGridColCheck, FCConvert.ToInt32(FGWidth * 0.04));
				// check boxes
				vsBEChoice.ColWidth(lngGridColBook, FCConvert.ToInt32(FGWidth * 0.06));
				// book number
				vsBEChoice.ColWidth(lngGridColDesc, FCConvert.ToInt32(FGWidth * 0.15));
				// description
				vsBEChoice.ColWidth(lngGridColStatus, FCConvert.ToInt32(FGWidth * 0.13));
				// status
				vsBEChoice.ColWidth(lngGridColClearedDate, FCConvert.ToInt32(FGWidth * 0.12));
				// C - status date
				vsBEChoice.ColWidth(lngGridColDataDate, FCConvert.ToInt32(FGWidth * 0.12));
				// D - status date
				vsBEChoice.ColWidth(lngGridColCalculatedDate, FCConvert.ToInt32(FGWidth * 0.12));
				// X - status date
				vsBEChoice.ColWidth(lngGridColBilledEditDate, FCConvert.ToInt32(FGWidth * 0.12));
				// E - status date
				vsBEChoice.ColWidth(lngGridColBilledDate, FCConvert.ToInt32(FGWidth * 0.12));
				// B - status date
			}
			vsBEChoice.TextMatrix(0, lngGridColBook, "Book");
			// fills the column titles
			vsBEChoice.TextMatrix(0, lngGridColDesc, "Description");
			vsBEChoice.TextMatrix(0, lngGridColStatus, "Status");
			vsBEChoice.TextMatrix(0, lngGridColClearedDate, "Cleared");
			vsBEChoice.TextMatrix(0, lngGridColDataDate, "Data");
			vsBEChoice.TextMatrix(0, lngGridColCalculatedDate, "Calc");
			vsBEChoice.TextMatrix(0, lngGridColBilledEditDate, "Bill Edit");
			vsBEChoice.TextMatrix(0, lngGridColBilledDate, "Billed");
			//vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, vsBEChoice.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsBEChoice.Height = this.Height;
		}

		private void vsBEChoice_DblClick(object sender, System.EventArgs e)
		{
			SelectAllBooks();
		}

		private void SelectAllBooks_2(bool boolOverride)
		{
			SelectAllBooks(boolOverride);
		}

		private void SelectAllBooks(bool boolOverride = false)
		{
			int lngMR;
			int lngMC;
			int lngCT;
			lngMR = vsBEChoice.MouseRow;
			lngMC = vsBEChoice.MouseCol;
			// Select Case intFormType
			// Case 2, 6, 7, 11, 12, 13, 14, 15, 17, 18, 25, 30
			if ((lngMR <= 0 && lngMC <= 0) || boolOverride)
			{
				// this is a double click on the top row and the first col which will select all of the rows
				for (lngCT = 1; lngCT <= vsBEChoice.Rows - 1; lngCT++)
				{
					if ((intFormType == 6) || (intFormType == 8) || (intFormType == 10) || (intFormType == 11) || (intFormType == 12) || (intFormType == 13) || (intFormType == 14) || (intFormType == 15) || (intFormType == 17) || (intFormType == 18) || (intFormType == 25) || (intFormType == 30) || (intFormType == 35) || (intFormType >= 41 && intFormType <= 52) || (intFormType == 70))
					{
						vsBEChoice.TextMatrix(lngCT, lngGridColCheck, FCConvert.ToString(-1));
					}
					else
					{
						//FC:FINAL:AAKV - exception cleared
						//if (vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCT, 0) != Color.White)
						if (ColorTranslator.FromOle(vsBEChoice.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCT, 0)) != Color.White)
						{
							vsBEChoice.TextMatrix(lngCT, lngGridColCheck, FCConvert.ToString(-1));
						}
					}
				}
			}
			// end Select
		}

		private void SetGridHeight_2(bool boolScrollBar)
		{
			SetGridHeight(ref boolScrollBar);
		}

		private void SetGridHeight(ref bool boolScrollBar)
		{
			int lngHt;
			int lngAddition;
			// MAL@20080805: Changed the way the height is calculated
			// Tracker Reference: 14181
			// With vsBEChoice
			// If boolScrollBar Then
			// lngAddition = 250
			// Else
			// lngAddition = 75
			// End If
			// 
			// lngHt = (.RowHeight(0) * .rows) + lngAddition   'calculates the total height of the flexgrid
			// 
			// If lngHt < Me.Height * 0.84 Then
			// .Height = lngHt  'if area used is less then start, decrease the unused grey area
			// Else
			// .Height = Me.Height * 0.84
			// End If
			// End With
			if (vsBEChoice.Rows > 1)
			{
				if ((vsBEChoice.Rows * vsBEChoice.RowHeight(0)) + 70 > (this.Height * 0.75))
				{
					// vsRate.Height = fraRate.Height * 0.85
					vsBEChoice.Height = (20 * vsBEChoice.RowHeight(0)) + 70;
					vsBEChoice.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
				else
				{
					vsBEChoice.Height = (vsBEChoice.Rows * vsBEChoice.RowHeight(0)) + 70;
					vsBEChoice.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
			}
		}
		// Private Sub vsBEChoice_RowColChange()
		// If vsBEChoice.Col > 0 Then
		// Select Case intFormType
		// Case 1 'Data Entry
		// vsBEChoice.Editable = flexEDNone
		// Case 2 'Calculate
		// vsBEChoice.Editable = flexEDNone
		// Case 3 'Billed Edit
		// vsBEChoice.Editable = flexEDNone
		// Case 4 'Create Bills
		// vsBEChoice.Editable = flexEDNone
		// Case 5 'Print Bills
		// vsBEChoice.Editable = flexEDNone
		// Case 6 'Transfer Report
		// vsBEChoice.Editable = flexEDNone
		// Case 7 'Billed List
		// vsBEChoice.Editable = flexEDNone
		// Case 10 'Print Meter Reading Slips
		// vsBEChoice.Editable = flexEDNone
		// Case 11 'Print Meter Exchange List
		// vsBEChoice.Editable = flexEDNone
		// Case 12 'Print Account Labels
		// vsBEChoice.Editable = flexEDNone
		// Case 13 'Extracting Data to Remote Reader
		// vsBEChoice.Editable = flexEDNone
		// Case 14 'Load Previous Readings
		// vsBEChoice.Editable = flexEDNone
		// Case 15 'Create Extract File
		// vsBEChoice.Editable = flexEDNone
		// Case 16
		// vsBEChoice.Editable = flexEDNone
		// Case 17
		// vsBEChoice.Editable = flexEDNone
		// Case 18
		// vsBEChoice.Editable = flexEDNone
		// Case 20
		// This is just to show the books w/o any selection, just a place for the user to view them
		// vsBEChoice.Editable = flexEDNone
		// Case 25
		// Analysis Reports
		// vsBEChoice.Editable = flexEDNone
		// Case 30
		// Account Listings
		// vsBEChoice.Editable = flexEDNone
		//
		// End Select
		// Else
		// vsBEChoice.Editable = flexEDKbdMouse
		// End If
		// End Sub
	}
}
