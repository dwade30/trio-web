﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arMeterCheck.
	/// </summary>
	public partial class arMeterCheck : BaseSectionReport
	{
		public arMeterCheck()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			// FC:FINAL:VGE - i911 Title fixed
			this.Name = "Meter Error Report";
		}

		public static arMeterCheck InstancePtr
		{
			get
			{
				return (arMeterCheck)Sys.GetInstance(typeof(arMeterCheck));
			}
		}

		protected arMeterCheck _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arMeterCheck	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/11/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/01/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsDataW = new clsDRWrapper();
		//clsDRWrapper rsDataS = new clsDRWrapper();
		int lngCountW;
		int lngCountS;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				rsData.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, MeterTable.BookNumber, Digits, BillingStatus, MeterNumber, Sequence, Service, Size, Multiplier, Combine, WaterCategory, SewerCategory " + "FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID) INNER JOIN Book ON MeterTable.BookNumber = Book.BookNumber WHERE ISNULL(Service,'') = '' OR Digits = 0 OR Sequence = 0 OR MeterTable.BookNumber = 0 OR Size = 0 OR Multiplier = 0 OR (Service <> 'W' AND SewerCategory = 0) OR (Service <> 'S' AND WaterCategory = 0) OR (MeterNumber > 1 AND NOT (Combine = 'C' OR Combine = 'B')) OR (CurrentStatus = 'B' AND (BillingStatus <> 'B' AND BillingStatus <> 'C')) ORDER BY AccountNumber, MeterNumber", modExtraModules.strUTDatabase);
				FillHeaderLabels();
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("There are no meters that are missing any data.", "No Meters", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
			rsData.MoveNext();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					// This will find all of the meters that are missing some data
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
					fldBook.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BookNumber"));
					fldDigits.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Digits"));
					fldStatus.Text = rsData.Get_Fields_String("BillingStatus");
					fldMeterNumber.Text = FCConvert.ToString(rsData.Get_Fields_Int32("MeterNumber"));
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					fldName.Text = FCConvert.ToString(rsData.Get_Fields("OwnerName"));
					// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
					fldSeq.Text = FCConvert.ToString(rsData.Get_Fields("Sequence"));
					fldService.Text = rsData.Get_Fields_String("Service");
					fldSize.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Size"));
					fldUnits.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Multiplier"));
					fldCombine.Text = rsData.Get_Fields_String("Combine");
					fldCategory.Text = "W-" + rsData.Get_Fields_Int32("WaterCategory") + "  S-" + rsData.Get_Fields_Int32("SewerCategory");
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void arMeterCheck_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arMeterCheck properties;
			//arMeterCheck.Caption	= "Meter Error Report";
			//arMeterCheck.Icon	= "arMeterCheck.dsx":0000";
			//arMeterCheck.Left	= 0;
			//arMeterCheck.Top	= 0;
			//arMeterCheck.Width	= 11880;
			//arMeterCheck.Height	= 8160;
			//arMeterCheck.WindowState	= 2;
			//arMeterCheck.SectionData	= "arMeterCheck.dsx":058A;
			//End Unmaped Properties
		}
	}
}
