﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Diagnostics;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReprintCMForm.
	/// </summary>
	partial class frmReprintCMForm : BaseForm
	{
		public fecherFoundation.FCFrame fraForm;
		public fecherFoundation.FCComboBox cmbForm;
		public fecherFoundation.FCLabel lblType;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCFrame fraCMF;
		public FCGrid vsPayments;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileClearAll;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelectToBottom;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelectAll;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileReprint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReprintCMForm));
			this.fraForm = new fecherFoundation.FCFrame();
			this.cmbForm = new fecherFoundation.FCComboBox();
			this.lblType = new fecherFoundation.FCLabel();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.fraCMF = new fecherFoundation.FCFrame();
			this.vsPayments = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClearAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSelectToBottom = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSelectAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileReprint = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileReprint = new fecherFoundation.FCButton();
			this.cmdFileClearAll = new fecherFoundation.FCButton();
			this.cmdFileSelectToBottom = new fecherFoundation.FCButton();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraForm)).BeginInit();
			this.fraForm.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCMF)).BeginInit();
			this.fraCMF.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileReprint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClearAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectToBottom)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileReprint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 615);
			this.BottomPanel.Size = new System.Drawing.Size(763, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraForm);
			this.ClientArea.Controls.Add(this.fraCMF);
			this.ClientArea.Size = new System.Drawing.Size(763, 555);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Controls.Add(this.cmdFileSelectToBottom);
			this.TopPanel.Controls.Add(this.cmdFileClearAll);
			this.TopPanel.Size = new System.Drawing.Size(763, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClearAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectToBottom, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(300, 30);
			this.HeaderText.Text = "Reprin Certified Mail Form";
			// 
			// fraForm
			// 
			this.fraForm.AppearanceKey = "groupBoxNoBorders";
			this.fraForm.Controls.Add(this.cmbForm);
			this.fraForm.Controls.Add(this.lblType);
			this.fraForm.Controls.Add(this.lblInstructions);
			this.fraForm.Location = new System.Drawing.Point(0, 0);
			this.fraForm.Name = "fraForm";
			this.fraForm.Size = new System.Drawing.Size(613, 173);
			this.fraForm.TabIndex = 0;
			// 
			// cmbForm
			// 
			this.cmbForm.AutoSize = false;
			this.cmbForm.BackColor = System.Drawing.SystemColors.Window;
			this.cmbForm.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbForm.FormattingEnabled = true;
			this.cmbForm.Location = new System.Drawing.Point(198, 120);
			this.cmbForm.Name = "cmbForm";
			this.cmbForm.Size = new System.Drawing.Size(343, 40);
			this.cmbForm.TabIndex = 2;
			this.cmbForm.DropDown += new System.EventHandler(this.cmbForm_DropDown);
			this.cmbForm.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbForm_KeyDown);
			// 
			// lblType
			// 
			this.lblType.Location = new System.Drawing.Point(30, 134);
			this.lblType.Name = "lblType";
			this.lblType.Size = new System.Drawing.Size(106, 18);
			this.lblType.TabIndex = 1;
			this.lblType.Text = "MAIL FORM TYPE";
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(379, 69);
			this.lblInstructions.TabIndex = 0;
			// 
			// fraCMF
			// 
			this.fraCMF.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraCMF.Controls.Add(this.vsPayments);
			this.fraCMF.Location = new System.Drawing.Point(30, 191);
			this.fraCMF.Name = "fraCMF";
			this.fraCMF.Size = new System.Drawing.Size(708, 343);
			this.fraCMF.TabIndex = 1;
			this.fraCMF.Text = "Certified Mail Form Accounts";
			this.fraCMF.Visible = false;
			// 
			// vsPayments
			// 
			this.vsPayments.AllowSelection = false;
			this.vsPayments.AllowUserToResizeColumns = false;
			this.vsPayments.AllowUserToResizeRows = false;
			this.vsPayments.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsPayments.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsPayments.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsPayments.BackColorBkg = System.Drawing.Color.Empty;
			this.vsPayments.BackColorFixed = System.Drawing.Color.Empty;
			this.vsPayments.BackColorSel = System.Drawing.Color.Empty;
			this.vsPayments.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsPayments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsPayments.ColumnHeadersHeight = 30;
			this.vsPayments.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsPayments.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsPayments.DragIcon = null;
			this.vsPayments.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsPayments.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsPayments.FixedCols = 0;
			this.vsPayments.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsPayments.FrozenCols = 0;
			this.vsPayments.GridColor = System.Drawing.Color.Empty;
			this.vsPayments.GridColorFixed = System.Drawing.Color.Empty;
			this.vsPayments.Location = new System.Drawing.Point(20, 30);
			this.vsPayments.Name = "vsPayments";
			this.vsPayments.ReadOnly = true;
			this.vsPayments.RowHeadersVisible = false;
			this.vsPayments.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsPayments.RowHeightMin = 0;
			this.vsPayments.Rows = 1;
			this.vsPayments.ScrollTipText = null;
			this.vsPayments.ShowColumnVisibilityMenu = false;
			this.vsPayments.Size = new System.Drawing.Size(661, 293);
			this.vsPayments.StandardTab = true;
			this.vsPayments.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsPayments.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsPayments.TabIndex = 0;
			this.vsPayments.CurrentCellChanged += new System.EventHandler(this.vsPayments_RowColChange);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileClearAll,
				this.mnuFileSelectToBottom,
				this.mnuFileSelectAll,
				this.mnuFileSeperator2,
				this.mnuFileReprint,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileClearAll
			// 
			this.mnuFileClearAll.Index = 0;
			this.mnuFileClearAll.Name = "mnuFileClearAll";
			this.mnuFileClearAll.Text = "Clear Selection";
			this.mnuFileClearAll.Click += new System.EventHandler(this.mnuFileClearAll_Click);
			// 
			// mnuFileSelectToBottom
			// 
			this.mnuFileSelectToBottom.Index = 1;
			this.mnuFileSelectToBottom.Name = "mnuFileSelectToBottom";
			this.mnuFileSelectToBottom.Text = "Select Current Row To Bottom";
			this.mnuFileSelectToBottom.Click += new System.EventHandler(this.mnuFileSelectToBottom_Click);
			// 
			// mnuFileSelectAll
			// 
			this.mnuFileSelectAll.Index = 2;
			this.mnuFileSelectAll.Name = "mnuFileSelectAll";
			this.mnuFileSelectAll.Text = "Select All";
			this.mnuFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 3;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuFileReprint
			// 
			this.mnuFileReprint.Index = 4;
			this.mnuFileReprint.Name = "mnuFileReprint";
			this.mnuFileReprint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileReprint.Text = "Process";
			this.mnuFileReprint.Click += new System.EventHandler(this.mnuFileReprint_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 5;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 6;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileReprint
			// 
			this.cmdFileReprint.AppearanceKey = "acceptButton";
			this.cmdFileReprint.Location = new System.Drawing.Point(288, 17);
			this.cmdFileReprint.Name = "cmdFileReprint";
			this.cmdFileReprint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileReprint.Size = new System.Drawing.Size(107, 48);
			this.cmdFileReprint.TabIndex = 0;
			this.cmdFileReprint.Text = "Process";
			this.cmdFileReprint.Click += new System.EventHandler(this.mnuFileReprint_Click);
			// 
			// cmdFileClearAll
			// 
			this.cmdFileClearAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClearAll.AppearanceKey = "toolbarButton";
			this.cmdFileClearAll.Location = new System.Drawing.Point(611, 29);
			this.cmdFileClearAll.Name = "cmdFileClearAll";
			this.cmdFileClearAll.Size = new System.Drawing.Size(115, 24);
			this.cmdFileClearAll.TabIndex = 1;
			this.cmdFileClearAll.Text = "Clear Selection";
			this.cmdFileClearAll.Click += new System.EventHandler(this.mnuFileClearAll_Click);
			// 
			// cmdFileSelectToBottom
			// 
			this.cmdFileSelectToBottom.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectToBottom.AppearanceKey = "toolbarButton";
			this.cmdFileSelectToBottom.Location = new System.Drawing.Point(400, 29);
			this.cmdFileSelectToBottom.Name = "cmdFileSelectToBottom";
			this.cmdFileSelectToBottom.Size = new System.Drawing.Size(205, 24);
			this.cmdFileSelectToBottom.TabIndex = 2;
			this.cmdFileSelectToBottom.Text = "SelectCurretn Row To Bottom";
			this.cmdFileSelectToBottom.Click += new System.EventHandler(this.mnuFileSelectToBottom_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(314, 29);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(80, 24);
			this.cmdFileSelectAll.TabIndex = 3;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// frmReprintCMForm
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(763, 723);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmReprintCMForm";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reprint Certified Mail Form";
			this.Load += new System.EventHandler(this.frmReprintCMForm_Load);
			this.Activated += new System.EventHandler(this.frmReprintCMForm_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReprintCMForm_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReprintCMForm_KeyPress);
			this.Resize += new System.EventHandler(this.frmReprintCMForm_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraForm)).EndInit();
			this.fraForm.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraCMF)).EndInit();
			this.fraCMF.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileReprint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClearAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectToBottom)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileReprint;
		private FCButton cmdFileClearAll;
		private FCButton cmdFileSelectToBottom;
		private FCButton cmdFileSelectAll;
	}
}
