//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Diagnostics;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	partial class frmCustomLabels : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkFormPrintOptions;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCCheckBox chkFormPrintOptions_0;
		public fecherFoundation.FCCheckBox chkFormPrintOptions_1;
		public fecherFoundation.FCCheckBox chkFormPrintOptions_2;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCLabel lblFormPrint;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCComboBox cmbFormType;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCFrame fraType;
		public fecherFoundation.FCCheckBox chkCondensed;
		public fecherFoundation.FCComboBox cmbLabelType;
		public fecherFoundation.FCLabel lblDescription;
		public fecherFoundation.FCLabel Label3;
		public Wisej.Web.ImageList ImageList1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomLabels));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.fraWhere = new fecherFoundation.FCFrame();
			this.vsWhere = new fecherFoundation.FCGrid();
			this.cmdClear = new fecherFoundation.FCButton();
			this.fraSort = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCDraggableListBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.fraFields = new fecherFoundation.FCFrame();
			this.chkFormPrintOptions_0 = new fecherFoundation.FCCheckBox();
			this.chkFormPrintOptions_1 = new fecherFoundation.FCCheckBox();
			this.chkFormPrintOptions_2 = new fecherFoundation.FCCheckBox();
			this.lstFields = new fecherFoundation.FCDraggableListBox();
			this.lblFormPrint = new fecherFoundation.FCLabel();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.Label3 = new fecherFoundation.FCLabel();
			this.lblFormType = new Wisej.Web.Label();
			this.cmbFormType = new fecherFoundation.FCComboBox();
			this.vsLayout = new fecherFoundation.FCGrid();
			this.fraType = new fecherFoundation.FCFrame();
			this.chkCondensed = new fecherFoundation.FCCheckBox();
			this.cmbLabelType = new fecherFoundation.FCComboBox();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.ImageList1 = new Wisej.Web.ImageList(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
			this.fraWhere.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
			this.fraSort.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
			this.fraFields.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
			this.fraType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCondensed)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(960, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraWhere);
			this.ClientArea.Controls.Add(this.fraSort);
			this.ClientArea.Controls.Add(this.fraFields);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(960, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(960, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(84, 30);
			this.HeaderText.Text = "Lien Certified Mail Forms";
			// 
			// fraWhere
			// 
			this.fraWhere.AppearanceKey = "groupBoxNoBorders";
			this.fraWhere.Controls.Add(this.vsWhere);
			this.fraWhere.Location = new System.Drawing.Point(31, 468);
			this.fraWhere.Name = "fraWhere";
			this.fraWhere.Size = new System.Drawing.Size(775, 182);
			this.fraWhere.TabIndex = 5;
			this.fraWhere.Text = "Select Search Criteria";
			this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
			// 
			// vsWhere
			// 
			this.vsWhere.AllowSelection = false;
			this.vsWhere.AllowUserToResizeColumns = false;
			this.vsWhere.AllowUserToResizeRows = false;
			this.vsWhere.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsWhere.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsWhere.BackColorBkg = System.Drawing.Color.Empty;
			this.vsWhere.BackColorFixed = System.Drawing.Color.Empty;
			this.vsWhere.BackColorSel = System.Drawing.Color.Empty;
			this.vsWhere.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsWhere.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsWhere.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsWhere.ColumnHeadersHeight = 30;
			this.vsWhere.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsWhere.ColumnHeadersVisible = false;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsWhere.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsWhere.DragIcon = null;
			this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsWhere.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsWhere.ExtendLastCol = true;
			this.vsWhere.FixedRows = 0;
			this.vsWhere.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsWhere.FrozenCols = 0;
			this.vsWhere.GridColor = System.Drawing.Color.Empty;
			this.vsWhere.GridColorFixed = System.Drawing.Color.Empty;
			this.vsWhere.Location = new System.Drawing.Point(0, 30);
			this.vsWhere.Name = "vsWhere";
			this.vsWhere.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsWhere.RowHeightMin = 0;
			this.vsWhere.Rows = 0;
			this.vsWhere.ScrollTipText = null;
			this.vsWhere.ShowColumnVisibilityMenu = false;
			this.vsWhere.ShowFocusCell = false;
			this.vsWhere.Size = new System.Drawing.Size(735, 128);
			this.vsWhere.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsWhere.TabIndex = 6;
			this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
			this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
			this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
			this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.AppearanceKey = "toolbarButton";
			this.cmdClear.Location = new System.Drawing.Point(786, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(142, 24);
			this.cmdClear.TabIndex = 7;
			this.cmdClear.Text = "Clear Search Criteria";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// fraSort
			// 
			this.fraSort.Controls.Add(this.lstSort);
			this.fraSort.Location = new System.Drawing.Point(266, 279);
			this.fraSort.Name = "fraSort";
			this.fraSort.Size = new System.Drawing.Size(220, 171);
			this.fraSort.TabIndex = 3;
			this.fraSort.Text = "Fields To Sort By";
			this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
			// 
			// lstSort
			// 
			this.lstSort.Appearance = 0;
			this.lstSort.BackColor = System.Drawing.SystemColors.Window;
			this.lstSort.CheckBoxes = true;
			this.lstSort.Location = new System.Drawing.Point(20, 30);
			this.lstSort.MultiSelect = 0;
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(180, 117);
			this.lstSort.Sorted = false;
			this.lstSort.Style = 1;
			this.lstSort.TabIndex = 4;
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.Location = new System.Drawing.Point(386, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(100, 48);
			this.cmdPrint.TabIndex = 1;
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// fraFields
			// 
			this.fraFields.Controls.Add(this.chkFormPrintOptions_0);
			this.fraFields.Controls.Add(this.chkFormPrintOptions_1);
			this.fraFields.Controls.Add(this.chkFormPrintOptions_2);
			this.fraFields.Controls.Add(this.lstFields);
			this.fraFields.Controls.Add(this.lblFormPrint);
			this.fraFields.Location = new System.Drawing.Point(31, 279);
			this.fraFields.Name = "fraFields";
			this.fraFields.Size = new System.Drawing.Size(210, 171);
			this.fraFields.TabIndex = 0;
			this.fraFields.Text = "Print Specific";
			this.fraFields.Visible = false;
			this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
			// 
			// chkFormPrintOptions_0
			// 
			this.chkFormPrintOptions_0.Checked = true;
			this.chkFormPrintOptions_0.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkFormPrintOptions_0.Location = new System.Drawing.Point(20, 55);
			this.chkFormPrintOptions_0.Name = "chkFormPrintOptions_0";
			this.chkFormPrintOptions_0.Size = new System.Drawing.Size(75, 27);
			this.chkFormPrintOptions_0.TabIndex = 23;
			this.chkFormPrintOptions_0.Text = "Owner";
			// 
			// chkFormPrintOptions_1
			// 
			this.chkFormPrintOptions_1.Checked = true;
			this.chkFormPrintOptions_1.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkFormPrintOptions_1.Location = new System.Drawing.Point(20, 92);
			this.chkFormPrintOptions_1.Name = "chkFormPrintOptions_1";
			this.chkFormPrintOptions_1.Size = new System.Drawing.Size(149, 27);
			this.chkFormPrintOptions_1.TabIndex = 22;
			this.chkFormPrintOptions_1.Text = "Mortgage Holder";
			// 
			// chkFormPrintOptions_2
			// 
			this.chkFormPrintOptions_2.Checked = true;
			this.chkFormPrintOptions_2.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkFormPrintOptions_2.Location = new System.Drawing.Point(20, 131);
			this.chkFormPrintOptions_2.Name = "chkFormPrintOptions_2";
			this.chkFormPrintOptions_2.Size = new System.Drawing.Size(113, 27);
			this.chkFormPrintOptions_2.TabIndex = 21;
			this.chkFormPrintOptions_2.Text = "New Owner";
			// 
			// lstFields
			// 
			this.lstFields.Appearance = 0;
			this.lstFields.BackColor = System.Drawing.SystemColors.Window;
			this.lstFields.Location = new System.Drawing.Point(20, 175);
			this.lstFields.MultiSelect = 0;
			this.lstFields.Name = "lstFields";
			this.lstFields.Size = new System.Drawing.Size(170, 34);
			this.lstFields.Sorted = false;
			this.lstFields.TabIndex = 2;
			this.lstFields.Visible = false;
			this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
			// 
			// lblFormPrint
			// 
			this.lblFormPrint.Location = new System.Drawing.Point(20, 30);
			this.lblFormPrint.Name = "lblFormPrint";
			this.lblFormPrint.Size = new System.Drawing.Size(56, 18);
			this.lblFormPrint.TabIndex = 24;
			this.lblFormPrint.Text = "PRINT";
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.Label3);
			this.Frame1.Controls.Add(this.lblFormType);
			this.Frame1.Controls.Add(this.cmbFormType);
			this.Frame1.Controls.Add(this.vsLayout);
			this.Frame1.Controls.Add(this.fraType);
			this.Frame1.Location = new System.Drawing.Point(0, 0);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(931, 273);
			this.Frame1.TabIndex = 10;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(31, 229);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(511, 33);
			this.Label3.TabIndex = 13;
			this.Label3.Text = "THIS IS A PRE-DEFINED REPORT MAKING SOME SECTIONS ON THIS FORM INACCESSIBLE";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblFormType
			// 
			this.lblFormType.Location = new System.Drawing.Point(500, 44);
			this.lblFormType.Name = "lblFormType";
			this.lblFormType.Size = new System.Drawing.Size(90, 16);
			this.lblFormType.TabIndex = 20;
			this.lblFormType.Text = "Mail Form Type";
			// 
			// cmbFormType
			// 
			this.cmbFormType.AutoSize = false;
			this.cmbFormType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFormType.FormattingEnabled = true;
			this.cmbFormType.Location = new System.Drawing.Point(610, 30);
			this.cmbFormType.Name = "cmbFormType";
			this.cmbFormType.Size = new System.Drawing.Size(310, 40);
			this.cmbFormType.TabIndex = 19;
			this.cmbFormType.DropDown += new System.EventHandler(this.cmbFormType_DropDown);
			this.cmbFormType.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbFormType_KeyDown);
			// 
			// vsLayout
			// 
			this.vsLayout.AllowSelection = false;
			this.vsLayout.AllowUserToResizeColumns = false;
			this.vsLayout.AllowUserToResizeRows = false;
			this.vsLayout.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsLayout.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsLayout.BackColorBkg = System.Drawing.Color.Empty;
			this.vsLayout.BackColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.BackColorSel = System.Drawing.Color.Empty;
			this.vsLayout.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsLayout.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsLayout.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsLayout.ColumnHeadersHeight = 30;
			this.vsLayout.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.vsLayout.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsLayout.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsLayout.DragIcon = null;
			this.vsLayout.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
			this.vsLayout.FixedCols = 0;
			this.vsLayout.FixedRows = 0;
			this.vsLayout.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.FrozenCols = 0;
			this.vsLayout.GridColor = System.Drawing.Color.Empty;
			this.vsLayout.GridColorFixed = System.Drawing.Color.Empty;
			this.vsLayout.Location = new System.Drawing.Point(30, 30);
			this.vsLayout.Name = "vsLayout";
			this.vsLayout.ReadOnly = true;
			this.vsLayout.RowHeadersVisible = false;
			this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsLayout.RowHeightMin = 0;
			this.vsLayout.Rows = 4;
			this.vsLayout.ScrollTipText = null;
			this.vsLayout.ShowColumnVisibilityMenu = false;
			this.vsLayout.ShowFocusCell = false;
			this.vsLayout.Size = new System.Drawing.Size(450, 185);
			this.vsLayout.StandardTab = true;
			this.vsLayout.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsLayout.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsLayout.TabIndex = 15;
			this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
			this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
			// 
			// fraType
			// 
			this.fraType.Controls.Add(this.chkCondensed);
			this.fraType.Controls.Add(this.cmbLabelType);
			this.fraType.Controls.Add(this.lblDescription);
			this.fraType.Location = new System.Drawing.Point(500, 30);
			this.fraType.Name = "fraType";
			this.fraType.Size = new System.Drawing.Size(390, 185);
			this.fraType.TabIndex = 14;
			this.fraType.Text = "Label Type";
			this.fraType.Visible = false;
			// 
			// chkCondensed
			// 
			this.chkCondensed.Location = new System.Drawing.Point(20, 138);
			this.chkCondensed.Name = "chkCondensed";
			this.chkCondensed.Size = new System.Drawing.Size(164, 27);
			this.chkCondensed.TabIndex = 20;
			this.chkCondensed.Text = "Condensed Labels";
			// 
			// cmbLabelType
			// 
			this.cmbLabelType.AutoSize = false;
			this.cmbLabelType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbLabelType.FormattingEnabled = true;
			this.cmbLabelType.Location = new System.Drawing.Point(20, 30);
			this.cmbLabelType.Name = "cmbLabelType";
			this.cmbLabelType.Size = new System.Drawing.Size(275, 40);
			this.cmbLabelType.TabIndex = 16;
			this.cmbLabelType.SelectedIndexChanged += new System.EventHandler(this.cmbLabelType_SelectedIndexChanged);
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(20, 90);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(364, 44);
			this.lblDescription.TabIndex = 17;
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1
			});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClear,
				this.mnuFileSeperator2,
				this.mnuPrint,
				this.mnuSP1,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear Search Criteria";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 1;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 2;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Process";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 3;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// frmCustomLabels
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(960, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmCustomLabels";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Labels";
			this.Load += new System.EventHandler(this.frmCustomLabels_Load);
			this.Activated += new System.EventHandler(this.frmCustomLabels_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomLabels_KeyDown);
			this.Resize += new System.EventHandler(this.frmCustomLabels_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
			this.fraWhere.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
			this.fraSort.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
			this.fraFields.ResumeLayout(false);
			this.fraFields.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFormPrintOptions_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
			this.fraType.ResumeLayout(false);
			this.fraType.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkCondensed)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private Label lblFormType;
	}
}