﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptReminderForm.
	/// </summary>
	partial class srptReminderForm
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptReminderForm));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.lblHeaderYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFooterPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFooterInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFooterTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblFooterTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblYear,
				this.lblPrincipal,
				this.lblInterest,
				this.lblTotal,
				this.lblTax,
				this.lblDate
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeaderYear,
				this.lblHeaderPrin,
				this.lblHeaderInt,
				this.lblHeaderTotal,
				this.lblHeaderTax,
				this.lblHeaderDate
			});
			this.ReportHeader.Height = 0.1770833F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooter,
				this.lblFooterPrincipal,
				this.lblFooterInterest,
				this.lblFooterTotal,
				this.lnTotal,
				this.lblFooterTax
			});
			this.ReportFooter.Height = 0.2291667F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// lblHeaderYear
			// 
			this.lblHeaderYear.Height = 0.16F;
			this.lblHeaderYear.HyperLink = null;
			this.lblHeaderYear.Left = 0.06944445F;
			this.lblHeaderYear.Name = "lblHeaderYear";
			this.lblHeaderYear.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
			this.lblHeaderYear.Text = "Bill";
			this.lblHeaderYear.Top = 0F;
			this.lblHeaderYear.Width = 1.111111F;
			// 
			// lblHeaderPrin
			// 
			this.lblHeaderPrin.Height = 0.16F;
			this.lblHeaderPrin.HyperLink = null;
			this.lblHeaderPrin.Left = 2.3125F;
			this.lblHeaderPrin.Name = "lblHeaderPrin";
			this.lblHeaderPrin.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
			this.lblHeaderPrin.Text = "Principal";
			this.lblHeaderPrin.Top = 0F;
			this.lblHeaderPrin.Width = 1.125F;
			// 
			// lblHeaderInt
			// 
			this.lblHeaderInt.Height = 0.16F;
			this.lblHeaderInt.HyperLink = null;
			this.lblHeaderInt.Left = 4.0625F;
			this.lblHeaderInt.Name = "lblHeaderInt";
			this.lblHeaderInt.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
			this.lblHeaderInt.Text = "Interest";
			this.lblHeaderInt.Top = 0F;
			this.lblHeaderInt.Width = 1.125F;
			// 
			// lblHeaderTotal
			// 
			this.lblHeaderTotal.Height = 0.16F;
			this.lblHeaderTotal.HyperLink = null;
			this.lblHeaderTotal.Left = 4.9375F;
			this.lblHeaderTotal.Name = "lblHeaderTotal";
			this.lblHeaderTotal.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
			this.lblHeaderTotal.Text = "Total";
			this.lblHeaderTotal.Top = 0F;
			this.lblHeaderTotal.Width = 1.125F;
			// 
			// lblHeaderTax
			// 
			this.lblHeaderTax.Height = 0.16F;
			this.lblHeaderTax.HyperLink = null;
			this.lblHeaderTax.Left = 3.1875F;
			this.lblHeaderTax.Name = "lblHeaderTax";
			this.lblHeaderTax.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
			this.lblHeaderTax.Text = "Tax";
			this.lblHeaderTax.Top = 0F;
			this.lblHeaderTax.Width = 1.125F;
			// 
			// lblHeaderDate
			// 
			this.lblHeaderDate.Height = 0.16F;
			this.lblHeaderDate.HyperLink = null;
			this.lblHeaderDate.Left = 1.1875F;
			this.lblHeaderDate.Name = "lblHeaderDate";
			this.lblHeaderDate.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
			this.lblHeaderDate.Text = "Date";
			this.lblHeaderDate.Top = 0F;
			this.lblHeaderDate.Width = 1.125F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.16F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 0.06944445F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblYear.Text = "0.00";
			this.lblYear.Top = 0F;
			this.lblYear.Width = 1.111111F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.16F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 2.3125F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblPrincipal.Text = "0.00";
			this.lblPrincipal.Top = 0F;
			this.lblPrincipal.Width = 1.125F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.16F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 4.0625F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblInterest.Text = "0.00";
			this.lblInterest.Top = 0F;
			this.lblInterest.Width = 1.125F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.16F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 4.9375F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblTotal.Text = "0.00";
			this.lblTotal.Top = 0F;
			this.lblTotal.Width = 1.125F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.16F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 3.1875F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblTax.Text = "0.00";
			this.lblTax.Top = 0F;
			this.lblTax.Width = 1.125F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.16F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1.1875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblDate.Text = "MM/DD/YYYY";
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.125F;
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.16F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 0.06944445F;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Courier New\'; font-weight: bold; text-align: right";
			this.lblFooter.Text = "Total:";
			this.lblFooter.Top = 0.05F;
			this.lblFooter.Width = 1.111111F;
			// 
			// lblFooterPrincipal
			// 
			this.lblFooterPrincipal.Height = 0.16F;
			this.lblFooterPrincipal.HyperLink = null;
			this.lblFooterPrincipal.Left = 2.3125F;
			this.lblFooterPrincipal.Name = "lblFooterPrincipal";
			this.lblFooterPrincipal.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblFooterPrincipal.Text = "0.00";
			this.lblFooterPrincipal.Top = 0.0625F;
			this.lblFooterPrincipal.Width = 1.125F;
			// 
			// lblFooterInterest
			// 
			this.lblFooterInterest.Height = 0.16F;
			this.lblFooterInterest.HyperLink = null;
			this.lblFooterInterest.Left = 4.0625F;
			this.lblFooterInterest.Name = "lblFooterInterest";
			this.lblFooterInterest.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblFooterInterest.Text = "0.00";
			this.lblFooterInterest.Top = 0.0625F;
			this.lblFooterInterest.Width = 1.125F;
			// 
			// lblFooterTotal
			// 
			this.lblFooterTotal.Height = 0.16F;
			this.lblFooterTotal.HyperLink = null;
			this.lblFooterTotal.Left = 4.9375F;
			this.lblFooterTotal.Name = "lblFooterTotal";
			this.lblFooterTotal.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblFooterTotal.Text = "0.00";
			this.lblFooterTotal.Top = 0.0625F;
			this.lblFooterTotal.Width = 1.125F;
			// 
			// lnTotal
			// 
			this.lnTotal.Height = 0F;
			this.lnTotal.Left = 0.125F;
			this.lnTotal.LineWeight = 1F;
			this.lnTotal.Name = "lnTotal";
			this.lnTotal.Top = 0F;
			this.lnTotal.Width = 5.9375F;
			this.lnTotal.X1 = 0.125F;
			this.lnTotal.X2 = 6.0625F;
			this.lnTotal.Y1 = 0F;
			this.lnTotal.Y2 = 0F;
			// 
			// lblFooterTax
			// 
			this.lblFooterTax.Height = 0.16F;
			this.lblFooterTax.HyperLink = null;
			this.lblFooterTax.Left = 3.1875F;
			this.lblFooterTax.Name = "lblFooterTax";
			this.lblFooterTax.Style = "font-family: \'Courier New\'; text-align: right";
			this.lblFooterTax.Text = "0.00";
			this.lblFooterTax.Top = 0.0625F;
			this.lblFooterTax.Width = 1.125F;
			// 
			// srptReminderForm
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderDate;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTax;
	}
}
