﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;

namespace TWUT0000
{
    /// <summary>
    /// Summary description for frmCalculation.
    /// </summary>
    partial class frmCalculation : BaseForm
    {
        public fecherFoundation.FCComboBox cmbDetail;
        public fecherFoundation.FCLabel lblDetail;
        public fecherFoundation.FCCheckBox chkShowMapLot;
        public fecherFoundation.FCCheckBox chkShowSubtotal;
        public FCGrid vsCalc;
        public fecherFoundation.FCLabel lblInformation;
        //public AxvsElasticLightLib.AxvsElasticLight vsElasticLight2;
        public fecherFoundation.FCToolStripMenuItem mnuProcess;
        public fecherFoundation.FCToolStripMenuItem mnuFilePrintSummary;
        public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
        public fecherFoundation.FCToolStripMenuItem mnuSummary;
        public fecherFoundation.FCToolStripMenuItem Seperator;
        public fecherFoundation.FCToolStripMenuItem mnuQuit;
        private Wisej.Web.ToolTip ToolTip1;


        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
           
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalculation));
            this.cmbDetail = new fecherFoundation.FCComboBox();
            this.lblDetail = new fecherFoundation.FCLabel();
            this.chkShowMapLot = new fecherFoundation.FCCheckBox();
            this.chkShowSubtotal = new fecherFoundation.FCCheckBox();
            this.vsCalc = new fecherFoundation.FCGrid();
            this.lblInformation = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintSummary = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSummary = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdFilePrint = new fecherFoundation.FCButton();
            this.cmdFilePrintSummary = new fecherFoundation.FCButton();
            this.cmdSummary = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowSubtotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsCalc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSummary)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFilePrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(678, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbDetail);
            this.ClientArea.Controls.Add(this.lblDetail);
            this.ClientArea.Controls.Add(this.chkShowMapLot);
            this.ClientArea.Controls.Add(this.chkShowSubtotal);
            this.ClientArea.Controls.Add(this.vsCalc);
            this.ClientArea.Controls.Add(this.lblInformation);
            this.ClientArea.Size = new System.Drawing.Size(678, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSummary);
            this.TopPanel.Controls.Add(this.cmdFilePrintSummary);
            this.TopPanel.Size = new System.Drawing.Size(678, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrintSummary, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSummary, 0);
            // 
            // HeaderText
            // 
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbDetail
            // 
            this.cmbDetail.Items.AddRange(new object[] {
            "Book Info",
            "Detail",
            "Meter Info"});
            this.cmbDetail.Location = new System.Drawing.Point(160, 30);
            this.cmbDetail.Name = "cmbDetail";
            this.cmbDetail.Size = new System.Drawing.Size(153, 40);
            this.cmbDetail.Text = "Meter Info";
            this.ToolTip1.SetToolTip(this.cmbDetail, null);
            this.cmbDetail.SelectedIndexChanged += new System.EventHandler(this.optDetail_CheckedChanged);
            // 
            // lblDetail
            // 
            this.lblDetail.AutoSize = true;
            this.lblDetail.Location = new System.Drawing.Point(30, 44);
            this.lblDetail.Name = "lblDetail";
            this.lblDetail.Size = new System.Drawing.Size(84, 16);
            this.lblDetail.TabIndex = 1;
            this.lblDetail.Text = "SELECTION";
            this.ToolTip1.SetToolTip(this.lblDetail, null);
            // 
            // chkShowMapLot
            // 
            this.chkShowMapLot.Checked = true;
            this.chkShowMapLot.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkShowMapLot.ForeColor = System.Drawing.Color.Black;
            this.chkShowMapLot.Location = new System.Drawing.Point(393, 71);
            this.chkShowMapLot.Name = "chkShowMapLot";
            this.chkShowMapLot.Size = new System.Drawing.Size(181, 23);
            this.chkShowMapLot.TabIndex = 7;
            this.chkShowMapLot.Text = "Show Map/Lot on Reports";
            this.ToolTip1.SetToolTip(this.chkShowMapLot, null);
            // 
            // chkShowSubtotal
            // 
            this.chkShowSubtotal.Checked = true;
            this.chkShowSubtotal.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkShowSubtotal.ForeColor = System.Drawing.Color.Black;
            this.chkShowSubtotal.Location = new System.Drawing.Point(393, 24);
            this.chkShowSubtotal.Name = "chkShowSubtotal";
            this.chkShowSubtotal.Size = new System.Drawing.Size(188, 23);
            this.chkShowSubtotal.TabIndex = 6;
            this.chkShowSubtotal.Text = "Show Subtotals on Reports";
            this.ToolTip1.SetToolTip(this.chkShowSubtotal, null);
            this.chkShowSubtotal.CheckedChanged += new System.EventHandler(this.chkShowSubtotal_CheckedChanged);
            // 
            // vsCalc
            // 
            this.vsCalc.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom)
            | Wisej.Web.AnchorStyles.Left)
            | Wisej.Web.AnchorStyles.Right)));
            this.vsCalc.Cols = 12;
            this.vsCalc.ColumnHeadersHeight = 60;
            this.vsCalc.FixedRows = 2;
            this.vsCalc.Location = new System.Drawing.Point(30, 118);
            this.vsCalc.Name = "vsCalc";
            this.vsCalc.Size = new System.Drawing.Size(628, 360);
            this.vsCalc.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.vsCalc, null);
            this.vsCalc.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsCalc_MouseMoveEvent);
            this.vsCalc.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.vsCalc_AfterCollapse);
            this.vsCalc.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vsCalc_AfterCollapse);
            // 
            // lblInformation
            // 
            this.lblInformation.AutoSize = true;
            this.lblInformation.Location = new System.Drawing.Point(8, 49);
            this.lblInformation.Name = "lblInformation";
            this.lblInformation.Size = new System.Drawing.Size(4, 15);
            this.lblInformation.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.lblInformation, null);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrintSummary,
            this.mnuFilePrint,
            this.mnuSummary,
            this.Seperator,
            this.mnuQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePrintSummary
            // 
            this.mnuFilePrintSummary.Index = 0;
            this.mnuFilePrintSummary.Name = "mnuFilePrintSummary";
            this.mnuFilePrintSummary.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFilePrintSummary.Text = "Print Summary";
            this.mnuFilePrintSummary.Click += new System.EventHandler(this.mnuFilePrintSummary_Click);
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 1;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePrint.Text = "Print Summary and Detail";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuSummary
            // 
            this.mnuSummary.Enabled = false;
            this.mnuSummary.Index = 2;
            this.mnuSummary.Name = "mnuSummary";
            this.mnuSummary.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuSummary.Text = "Show Summary";
            this.mnuSummary.Click += new System.EventHandler(this.mnuSummary_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 3;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuQuit
            // 
            this.mnuQuit.Index = 4;
            this.mnuQuit.Name = "mnuQuit";
            this.mnuQuit.Text = "Exit";
            this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
            // 
            // cmdFilePrint
            // 
            this.cmdFilePrint.AppearanceKey = "acceptButton";
            this.cmdFilePrint.Location = new System.Drawing.Point(209, 30);
            this.cmdFilePrint.Name = "cmdFilePrint";
            this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFilePrint.Size = new System.Drawing.Size(227, 48);
            this.cmdFilePrint.Text = "Print Summary and Detail";
            this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // cmdFilePrintSummary
            // 
            this.cmdFilePrintSummary.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrintSummary.Location = new System.Drawing.Point(546, 29);
            this.cmdFilePrintSummary.Name = "cmdFilePrintSummary";
            this.cmdFilePrintSummary.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdFilePrintSummary.Size = new System.Drawing.Size(104, 24);
            this.cmdFilePrintSummary.TabIndex = 1;
            this.cmdFilePrintSummary.Text = "Print Summary";
            this.cmdFilePrintSummary.Click += new System.EventHandler(this.mnuFilePrintSummary_Click);
            // 
            // cmdSummary
            // 
            this.cmdSummary.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSummary.Enabled = false;
            this.cmdSummary.Location = new System.Drawing.Point(432, 29);
            this.cmdSummary.Name = "cmdSummary";
            this.cmdSummary.Shortcut = Wisej.Web.Shortcut.F8;
            this.cmdSummary.Size = new System.Drawing.Size(108, 24);
            this.cmdSummary.TabIndex = 2;
            this.cmdSummary.Text = "Show Summary";
            this.cmdSummary.Click += new System.EventHandler(this.mnuSummary_Click);
            // 
            // frmCalculation
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(678, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.Color.FromArgb(192, 192, 192);
            this.KeyPreview = true;
            this.Name = "frmCalculation";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Calculate & Edit";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmCalculation_Load);
            this.Activated += new System.EventHandler(this.frmCalculation_Activated);
            this.Resize += new System.EventHandler(this.frmCalculation_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCalculation_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowSubtotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsCalc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSummary)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private System.ComponentModel.IContainer components;
        private FCButton cmdFilePrint;
        private FCButton cmdFilePrintSummary;
        private FCButton cmdSummary;
    }
}
