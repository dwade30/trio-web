﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptBillInfo.
	/// </summary>
	public partial class srptBillInfo : FCSectionReport
	{
		public srptBillInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptBillInfo InstancePtr
		{
			get
			{
				return (srptBillInfo)Sys.GetInstance(typeof(srptBillInfo));
			}
		}

		protected srptBillInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsPaymentInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptBillInfo	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/20/2006              *
		// ********************************************************
		public int lngBillKey;
		public bool blnLienBill;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsPaymentInfo = new clsDRWrapper();
		bool blnShowWater;
		bool blnShowSewer;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsPaymentInfo.MoveNext();
				eArgs.EOF = rsPaymentInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			rsInfo.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
			}
			else
			{
				this.Cancel();
				return;
			}
			if (blnLienBill)
			{
				if ((FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "B" || FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "W") && FCConvert.ToInt32(rsInfo.Get_Fields_Int32("WLienRecordNumber")) != 0)
				{
					blnShowWater = true;
				}
				else
				{
					blnShowWater = false;
				}
				if ((FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "B" || FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "S") && FCConvert.ToInt32(rsInfo.Get_Fields_Int32("SLienRecordNumber")) != 0)
				{
					blnShowSewer = true;
				}
				else
				{
					blnShowSewer = false;
				}
			}
			else
			{
				if ((FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "B" || FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "W") && rsInfo.Get_Fields_Double("WPrinOwed") == rsInfo.Get_Fields_Double("WPrinPaid") && rsInfo.Get_Fields_Double("WIntOwed") - rsInfo.Get_Fields_Double("WIntAdded") == rsInfo.Get_Fields_Double("WIntPaid") && rsInfo.Get_Fields_Double("WTaxOwed") == rsInfo.Get_Fields_Double("WTaxPaid") && rsInfo.Get_Fields_Double("WCostOwed") - rsInfo.Get_Fields_Double("WCostAdded") == rsInfo.Get_Fields_Double("WCostPaid"))
				{
					blnShowWater = true;
				}
				else
				{
					blnShowWater = false;
				}
				if ((FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "B" || FCConvert.ToString(rsInfo.Get_Fields_String("Service")) == "S") && rsInfo.Get_Fields_Double("SPrinOwed") == rsInfo.Get_Fields_Double("SPrinPaid") && rsInfo.Get_Fields_Double("SIntOwed") - rsInfo.Get_Fields_Double("SIntAdded") == rsInfo.Get_Fields_Double("SIntPaid") && rsInfo.Get_Fields_Double("STaxOwed") == rsInfo.Get_Fields_Double("STaxPaid") && rsInfo.Get_Fields_Double("SCostOwed") - rsInfo.Get_Fields_Double("SCostAdded") == rsInfo.Get_Fields_Double("SCostPaid"))
				{
					blnShowSewer = true;
				}
				else
				{
					blnShowSewer = false;
				}
			}
			if (blnShowWater && blnShowSewer)
			{
				rsPaymentInfo.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(Lien,0) = 0 AND BillKey = " + FCConvert.ToString(lngBillKey));
			}
			else if (blnShowWater)
			{
				rsPaymentInfo.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(Lien,0) = 0 AND Service = 'W' AND BillKey = " + FCConvert.ToString(lngBillKey));
			}
			else
			{
				rsPaymentInfo.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(Lien,0) = 0 AND Service = 'S' AND BillKey = " + FCConvert.ToString(lngBillKey));
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsPaymentInfo.EndOfFile())
			{
				fldPaymentDate.Text = Strings.Format(rsPaymentInfo.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy");
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCode.Text = FCConvert.ToString(rsPaymentInfo.Get_Fields("Code"));
				fldReceipt.Text = Strings.Format(rsPaymentInfo.Get_Fields_Int32("ReceiptNumber"), "00000");
				if (FCConvert.ToString(rsPaymentInfo.Get_Fields_String("Service")) == "W")
				{
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					fldWaterPrincipalPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields("Principal"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
					fldWaterTaxPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields("Tax"), "#,##0.00");
					fldWaterInterestPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields_Decimal("PreLienInterest") + rsPaymentInfo.Get_Fields_Decimal("CurrentInterest"), "#,##0.00");
					fldWaterCostsPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields_Decimal("LienCost"), "#,##0.00");
					fldSewerPrincipalPayment.Text = "";
					fldSewerTaxPayment.Text = "";
					fldSewerInterestPayment.Text = "";
					fldSewerCostsPayment.Text = "";
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					fldSewerPrincipalPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields("Principal"), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
					fldSewerTaxPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields("Tax"), "#,##0.00");
					fldSewerInterestPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields_Decimal("PreLienInterest") + rsPaymentInfo.Get_Fields_Decimal("CurrentInterest"), "#,##0.00");
					fldSewerCostsPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields_Decimal("LienCost"), "#,##0.00");
					fldWaterPrincipalPayment.Text = "";
					fldWaterTaxPayment.Text = "";
					fldWaterInterestPayment.Text = "";
					fldWaterCostsPayment.Text = "";
				}
			}
			else
			{
				fldPaymentDate.Text = "";
				// kgk  08-10-2012  Stop showing blank/ghost payments
				fldCode.Text = "";
				fldReceipt.Text = "";
				fldSewerPrincipalPayment.Text = "";
				fldSewerTaxPayment.Text = "";
				fldSewerInterestPayment.Text = "";
				fldSewerCostsPayment.Text = "";
				fldWaterPrincipalPayment.Text = "";
				fldWaterTaxPayment.Text = "";
				fldWaterInterestPayment.Text = "";
				fldWaterCostsPayment.Text = "";
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("CreationDate"), "MM/dd/yyyy");
			fldReference.Text = "Original Bill";
			if (blnShowWater)
			{
				fldWaterPrincipal.Text = Strings.Format(rsInfo.Get_Fields_Double("WPrinOwed"), "#,##0.00");
				fldWaterTax.Text = Strings.Format(rsInfo.Get_Fields_Double("WTaxOwed"), "#,##0.00");
				fldWaterInterest.Text = Strings.Format(rsInfo.Get_Fields_Double("WIntOwed"), "#,##0.00");
				fldWaterCosts.Text = Strings.Format(rsInfo.Get_Fields_Double("WCostOwed"), "#,##0.00");
			}
			else
			{
				fldWaterPrincipal.Text = "";
				fldWaterTax.Text = "";
				fldWaterInterest.Text = "";
				fldWaterCosts.Text = "";
			}
			if (blnShowSewer)
			{
				fldSewerPrincipal.Text = Strings.Format(rsInfo.Get_Fields_Double("SPrinOwed"), "#,##0.00");
				fldSewerTax.Text = Strings.Format(rsInfo.Get_Fields_Double("STaxOwed"), "#,##0.00");
				fldSewerInterest.Text = Strings.Format(rsInfo.Get_Fields_Double("SIntOwed"), "#,##0.00");
				fldSewerCosts.Text = Strings.Format(rsInfo.Get_Fields_Double("SCostOwed"), "#,##0.00");
			}
			else
			{
				fldSewerPrincipal.Text = "";
				fldSewerTax.Text = "";
				fldSewerInterest.Text = "";
				fldSewerCosts.Text = "";
			}
		}

		private void srptBillInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptBillInfo properties;
			//srptBillInfo.Icon	= "srptBillInfo.dsx":0000";
			//srptBillInfo.Left	= 0;
			//srptBillInfo.Top	= 0;
			//srptBillInfo.Width	= 11880;
			//srptBillInfo.Height	= 8595;
			//srptBillInfo.StartUpPosition	= 3;
			//srptBillInfo.SectionData	= "srptBillInfo.dsx":058A;
			//End Unmaped Properties
		}

		private void srptBillInfo_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
