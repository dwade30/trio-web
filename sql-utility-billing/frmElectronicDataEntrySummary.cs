﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmElectronicDataEntrySummary.
	/// </summary>
	public partial class frmElectronicDataEntrySummary : BaseForm
	{
		public frmElectronicDataEntrySummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmElectronicDataEntrySummary InstancePtr
		{
			get
			{
				return (frmElectronicDataEntrySummary)Sys.GetInstance(typeof(frmElectronicDataEntrySummary));
			}
		}

		protected frmElectronicDataEntrySummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               09/17/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/06/2006              *
		// *
		// This form will show the results of the electronic data *
		// entry process and have an option for the user to get a *
		// report of the results                                  *
		// ********************************************************
		int BookCol;
		int SequenceCol;
		int CurrentCol;
		int NameCol;
		int lngNoMatchForRecord;
		// No Match for the reading in the meter files
		int lngNotUpdatedRecords;
		// Records that were not use to update
		int lngNotUpdatedMeters;
		// Meters Not Updated
		int lngUpdatedRecords;
		// Records that were used to update
		int lngUpdatedMeters;
		// Meters Updated
		int lngTotalMeters;
		// Total Meters in selected books
		int lngTotalRecords;
		// Total Records in Input file
		int lngDuplicateMeters;
		// Duplicate Meter Book/Seq
		int lngDuplicateRecords;
		// Duplicate Readings
		public string strBookSQL = "";

		private void frmElectronicDataEntrySummary_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmElectronicDataEntrySummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmElectronicDataEntrySummary properties;
			//frmElectronicDataEntrySummary.FillStyle	= 0;
			//frmElectronicDataEntrySummary.ScaleWidth	= 9045;
			//frmElectronicDataEntrySummary.ScaleHeight	= 7410;
			//frmElectronicDataEntrySummary.LinkTopic	= "Form2";
			//frmElectronicDataEntrySummary.LockControls	= true;
			//frmElectronicDataEntrySummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				BookCol = 0;
				SequenceCol = 1;
				CurrentCol = 2;
				NameCol = 3;
				vsNoMatch.TextMatrix(0, BookCol, "Book");
				vsNoMatch.TextMatrix(0, SequenceCol, "Seq");
				vsNoMatch.TextMatrix(0, CurrentCol, "Current");
				vsNotUpdated.TextMatrix(0, BookCol, "Book");
				vsNotUpdated.TextMatrix(0, SequenceCol, "Seq");
				vsDuplicates.TextMatrix(0, BookCol, "Book");
				vsDuplicates.TextMatrix(0, SequenceCol, "Seq");
				vsDuplicates.TextMatrix(0, CurrentCol, "Current");
				vsDuplicates.TextMatrix(0, NameCol, "Name");
				vsNoMatch.ColWidth(BookCol, FCConvert.ToInt32(vsNoMatch.WidthOriginal * 0.25));
				vsNoMatch.ColWidth(SequenceCol, FCConvert.ToInt32(vsNoMatch.WidthOriginal * 0.25));
				vsNoMatch.ColWidth(CurrentCol, 100);
				vsNotUpdated.ColWidth(BookCol, FCConvert.ToInt32(vsNotUpdated.WidthOriginal * 0.5));
				vsNotUpdated.ColWidth(SequenceCol, 100);
				vsDuplicates.ColWidth(BookCol, FCConvert.ToInt32(vsNoMatch.WidthOriginal * 0.25));
				vsDuplicates.ColWidth(SequenceCol, FCConvert.ToInt32(vsNoMatch.WidthOriginal * 0.25));
				vsDuplicates.ColWidth(CurrentCol, FCConvert.ToInt32(vsNoMatch.WidthOriginal * 0.5));
				vsNoMatch.ColAlignment(BookCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsNoMatch.ColAlignment(SequenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsNotUpdated.ColAlignment(BookCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsNotUpdated.ColAlignment(SequenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDuplicates.ColAlignment(BookCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDuplicates.ColAlignment(SequenceCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsNoMatch.ColAlignment(CurrentCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsDuplicates.ColAlignment(CurrentCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				// reset variables
				lngTotalMeters = 0;
				lngTotalRecords = 0;
				lngDuplicateMeters = 0;
				lngDuplicateRecords = 0;
				lngNoMatchForRecord = 0;
				lngNotUpdatedRecords = 0;
				lngNotUpdatedMeters = 0;
				lngUpdatedRecords = 0;
				lngUpdatedMeters = 0;
				// kk01282015 trouts-136  Reset the grids
				vsNoMatch.Rows = 1;
				vsNotUpdated.Rows = 1;
				vsDuplicates.Rows = 1;
				// fill all the information
				FillNoMatchGrid();
				FillNotUpdatedGrid();
				FillDuplicatesGrid();
				// fill the summary screen
				txtTotalMeters.Text = Strings.Format(lngTotalMeters, "#,##0");
				txtTotalRecords.Text = Strings.Format(lngTotalRecords, "#,##0");
				txtDuplicateMeters.Text = Strings.Format(lngDuplicateMeters, "#,##0");
				txtDuplicateReadings.Text = Strings.Format(lngDuplicateRecords, "#,##0");
				txtNoMatchRecords.Text = Strings.Format(lngNoMatchForRecord, "#,##0");
				txtRecordsWithDupMeters.Text = Strings.Format(lngNotUpdatedRecords, "#,##0");
				txtNotUpdatedMeters.Text = Strings.Format(lngNotUpdatedMeters, "#,##0");
				txtMetersUpdated.Text = Strings.Format(lngUpdatedMeters, "#,##0");
				txtRecordsUpdated.Text = Strings.Format(lngUpdatedRecords, "#,##0");
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmElectronicDataEntrySummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// Clear Variables
			FCUtils.EraseSafe(modProcessTWUTXX41File.Statics.mtdMeters);
		}

		private void frmElectronicDataEntrySummary_Resize(object sender, System.EventArgs e)
		{
			FormatGrid();
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptElectronicDataEntrySummary.InstancePtr);
			this.Hide();
		}

		private void mnuProcessPrint_Click(object sender, System.EventArgs e)
		{
			rptElectronicDataEntrySummary.InstancePtr.PrintReport(true);
			rptElectronicDataEntrySummary.InstancePtr.Unload();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void FillNoMatchGrid()
		{
			int counter;
			for (counter = 0; counter <= Information.UBound(modProcessTWUTXX41File.Statics.mtdMeters, 1) - 1; counter++)
			{
				if (modProcessTWUTXX41File.Statics.mtdMeters[counter].MatchFound == false)
				{
					vsNoMatch.Rows += 1;
					vsNoMatch.TextMatrix(vsNoMatch.Rows - 1, BookCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter].BookNumber, "0000"));
					vsNoMatch.TextMatrix(vsNoMatch.Rows - 1, SequenceCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter].Sequence, "0000"));
					vsNoMatch.TextMatrix(vsNoMatch.Rows - 1, CurrentCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter].CurrentReading, "#,##0"));
					lngNoMatchForRecord += 1;
					// These records did not match a meter
				}
				else if (modProcessTWUTXX41File.Statics.mtdMeters[counter].Updated)
				{
					lngUpdatedRecords += 1;
					// These records were updated successfully
				}
				else
				{
					lngNotUpdatedRecords += 1;
					// These RECORDS were not updated correctly but had a match
				}
				lngTotalRecords += 1;
				// total records
			}
		}

		private void FillNotUpdatedGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int counter;
				string strSQL = "";
				clsDRWrapper rsInfo = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				int lngCT;
				bool boolFound = false;
				bool boolUpdated = false;
				rsInfo.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber " + strBookSQL, modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true, rsInfo.RecordCount(), true);
				while (!rsInfo.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					// Total number of meters
					lngTotalMeters += 1;
					boolFound = false;
					boolUpdated = false;
					//Application.DoEvents();
					// look for the meter (book/seq) in the array
					for (lngCT = 0; lngCT <= Information.UBound(modProcessTWUTXX41File.Statics.mtdMeters, 1); lngCT++)
					{
						//Application.DoEvents();
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						if (modProcessTWUTXX41File.Statics.mtdMeters[lngCT].BookNumber == FCConvert.ToInt32(rsInfo.Get_Fields_Int32("BookNumber")) && modProcessTWUTXX41File.Statics.mtdMeters[lngCT].Sequence == FCConvert.ToInt32(rsInfo.Get_Fields("Sequence")))
						{
							boolFound = true;
							boolUpdated = modProcessTWUTXX41File.Statics.mtdMeters[lngCT].Updated;
							break;
						}
					}
					if (boolFound)
					{
						// do nothing, this meter was processed
						if (boolUpdated)
						{
							lngUpdatedMeters += 1;
						}
						else
						{
							lngNotUpdatedMeters += 1;
						}
						// kk 12032013 trouts-65  Reset Bill.NoBill flag if not set on account master/meter
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							if (!FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("NoBill")))
							{
								rsBill.Execute("UPDATE Bill SET NoBill = 0 WHERE MeterKey = " + rsInfo.Get_Fields_Int32("ID") + " AND BillNumber = 0", "twut0000.vb1");
							}
						}
					}
					else
					{
						// This will show the meters that were not found
						vsNotUpdated.Rows += 1;
						vsNotUpdated.TextMatrix(vsNotUpdated.Rows - 1, BookCol, Strings.Format(rsInfo.Get_Fields_Int32("BookNumber"), "0000"));
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						vsNotUpdated.TextMatrix(vsNotUpdated.Rows - 1, SequenceCol, Strings.Format(rsInfo.Get_Fields("Sequence"), "0000"));
						lngNotUpdatedMeters += 1;
						// kk 11042013 trouts-55  Bangor request - don't No Bill if not in Billing file
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							rsBill.Execute("UPDATE Bill SET NoBill = 1 WHERE MeterKey = " + rsInfo.Get_Fields_Int32("ID") + " AND BillNumber = 0", "twut0000.vb1");
						}
					}
					rsInfo.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				// strSQL = "("
				// For counter = 0 To UBound(mtdMeters) - 1
				// If mtdMeters(counter).MatchFound = True Then
				// strSQL = strSQL & "(BookNumber = " & mtdMeters(counter).BookNumber & " AND Sequence = " & mtdMeters(counter).Sequence & ") OR "
				// End If
				// Next
				// strSQL = Left(strSQL, Len(strSQL) - 4) & ")"
				// 
				// rsInfo.OpenRecordset "SELECT * FROM MeterTable WHERE BookNumber " & strBookSQL & " AND MeterKey NOT IN (SELECT MeterKey FROM MeterTable WHERE " & strSQL & ") ORDER BY BookNumber, Sequence"
				// If rsInfo.EndOfFile <> True And rsInfo.BeginningOfFile <> True Then
				// Do
				// vsNotUpdated.rows = vsNotUpdated.rows + 1
				// vsNotUpdated.TextMatrix(vsNotUpdated.rows - 1, BookCol) = Format(rsInfo.Fields("BookNumber"), "0000")
				// vsNotUpdated.TextMatrix(vsNotUpdated.rows - 1, SequenceCol) = Format(rsInfo.Fields("Sequence"), "0000")
				// rsInfo.MoveNext
				// Loop While rsInfo.EndOfFile <> True
				// lngNotUpdatedRecords = rsInfo.RecordCount
				// End If
				// 
				// rsInfo.OpenRecordset "SELECT * FROM MeterTable WHERE BookNumber " & strBookSQL
				// If rsInfo.EndOfFile <> True And rsInfo.BeginningOfFile <> True Then
				// lngTotalMeters = rsInfo.RecordCount
				// End If
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Not Updated Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillDuplicatesGrid()
		{
			int counter;
			int counter2;
			bool blnShownFirstDuplicate = false;
			clsDRWrapper rsInfo = new clsDRWrapper();
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking Duplicates", true, Information.UBound(modProcessTWUTXX41File.Statics.mtdMeters, 1));
			for (counter = 0; counter <= Information.UBound(modProcessTWUTXX41File.Statics.mtdMeters, 1) - 1; counter++)
			{
				frmWait.InstancePtr.IncrementProgress();
				//Application.DoEvents();
				blnShownFirstDuplicate = false;
				for (counter2 = counter + 1; counter2 <= Information.UBound(modProcessTWUTXX41File.Statics.mtdMeters, 1) - 1; counter2++)
				{
					//Application.DoEvents();
					if (modProcessTWUTXX41File.Statics.mtdMeters[counter].BookNumber == modProcessTWUTXX41File.Statics.mtdMeters[counter2].BookNumber && modProcessTWUTXX41File.Statics.mtdMeters[counter].Sequence == modProcessTWUTXX41File.Statics.mtdMeters[counter2].Sequence)
					{
						rsInfo.OpenRecordset("SELECT OwnerPartyID FROM Master INNER JOIN MeterTable ON MeterTable.AccountKey = Master.ID WHERE MeterTable.BookNumber = " + FCConvert.ToString(modProcessTWUTXX41File.Statics.mtdMeters[counter].BookNumber) + " AND Sequence = " + FCConvert.ToString(modProcessTWUTXX41File.Statics.mtdMeters[counter].Sequence));
						if (!blnShownFirstDuplicate)
						{
							blnShownFirstDuplicate = true;
							vsDuplicates.Rows += 1;
							vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, BookCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter].BookNumber, "0000"));
							vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, SequenceCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter].Sequence, "0000"));
							vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, CurrentCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter].CurrentReading, "#,##0"));
							if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
							{
								vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, NameCol, FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
							}
							else
							{
								vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, NameCol, "UNKNOWN");
							}
							lngDuplicateMeters += 1;
						}
						vsDuplicates.Rows += 1;
						vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, BookCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter2].BookNumber, "0000"));
						vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, SequenceCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter2].Sequence, "0000"));
						vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, CurrentCol, Strings.Format(modProcessTWUTXX41File.Statics.mtdMeters[counter2].CurrentReading, "#,##0"));
						if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
						{
							vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, NameCol, FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
						}
						else
						{
							vsDuplicates.TextMatrix(vsDuplicates.Rows - 1, NameCol, "UNKNOWN");
						}
						// lngDuplicateMeters = lngDuplicateMeters + 1
					}
				}
			}
			frmWait.InstancePtr.Unload();
		}

		private void FormatGrid()
		{
			int lngWid = 0;
			lngWid = vsDuplicates.WidthOriginal;
			vsDuplicates.ColWidth(BookCol, FCConvert.ToInt32(lngWid * 0.15));
			vsDuplicates.ColWidth(SequenceCol, FCConvert.ToInt32(lngWid * 0.15));
			vsDuplicates.ColWidth(CurrentCol, FCConvert.ToInt32(lngWid * 0.15));
			lngWid = vsNoMatch.WidthOriginal;
			vsNoMatch.ColWidth(BookCol, FCConvert.ToInt32(lngWid * 0.25));
			vsNoMatch.ColWidth(SequenceCol, FCConvert.ToInt32(lngWid * 0.25));
			vsNoMatch.ColWidth(CurrentCol, FCConvert.ToInt32(lngWid * 0.4));
			lngWid = vsNotUpdated.WidthOriginal;
			vsNotUpdated.ColWidth(BookCol, FCConvert.ToInt32(lngWid * 0.5));
			vsNotUpdated.ColWidth(SequenceCol, FCConvert.ToInt32(lngWid * 0.45));
		}
	}
}
