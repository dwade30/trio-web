//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Receipting;
using Wisej.Web;


namespace TWUT0000
{
	public class cUTCommandHandler
	{

		//=========================================================


		public delegate void MenuChangedEventHandler(string strMenuName);
		public event MenuChangedEventHandler MenuChanged;
		private CommandDispatcher commandDispatcher;

		private void ChangeMenu(string strMenuName)
		{
			if (this.MenuChanged != null) this.MenuChanged(strMenuName);
		}

		public cUTCommandHandler(CommandDispatcher commandDispatcher)
		{
			this.commandDispatcher = commandDispatcher;
		}

		// vbPorter upgrade warning: intCode As short	OnWrite(int)
		public void ExecuteMenuCommand(string strMenuName, int intCode)
		{
			switch (intCode) {
					// *********** Main Menu
				
				case (int)UT_Commands.Account_Meter_Update:
				{					
					if (!frmAccountMaster.InstancePtr.Visible)
                    {
                        commandDispatcher.Send(new ViewUTAccount());
                        //frmGetMasterAccount.InstancePtr.Init(0);
					} else {
						MessageBox.Show("There is already an account master screen open.", "Already Open", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					break;
				}
				case (int)UT_Commands.Menu_BillingProcess:
				{
					if (modUTBilling.CheckForUTAccountsSetup()) {
						// Billing Process
						ChangeMenu("Billing");
					} else {
						MessageBox.Show("Before continuing, please setup your UT Accounts in File Maintenance > Customize.", "Missing Valid Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					break;
				}
				case (int)UT_Commands.Menu_CollectionProcess:
				{
					ChangeMenu("Collections");
					break;
				}
				case (int)UT_Commands.Menu_Printing:
				{
					ChangeMenu("Listing");
					break;
				}
				case (int)UT_Commands.TableFileSetup:
				{
					frmTableFileSetup.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.ExtractForRemoteReader:
				{
					frmBookChoice.InstancePtr.Init(13);
					break;
				}
				case (int)UT_Commands.Menu_FileMaintenance:
				{
					ChangeMenu("File");
					break;
				}
				case (int)UT_Commands.Menu_Main:
				{
					ChangeMenu("UT");
					// ****** Billing Menu
					break;
				}
				case (int)UT_Commands.ClearBooks:
				{
					frmClearBooks.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.DataEntry:
				{
					frmBookChoice.InstancePtr.Init(1);
					break;
				}
				case (int)UT_Commands.CalculateEdit:
				{
					frmBookChoice.InstancePtr.Init(2);
					break;
				}
				case (int)UT_Commands.CreateBillRecords:
				{
					frmRateRecChoice.InstancePtr.Unload();
					frmRateRecChoice.InstancePtr.Init(101);
					break;
				}
				case (int)UT_Commands.PrintBills:
				{
					frmRateRecChoice.InstancePtr.Unload();
					frmRateRecChoice.InstancePtr.Init(102);
					break;
				}
				case (int)UT_Commands.ReprintBillCreationReport:
				{
					frmRateRecChoice.InstancePtr.Unload();
					frmRateRecChoice.InstancePtr.Init(106);
					break;
				}
				case (int)UT_Commands.PrintNotBilledReport:
				{
					frmRateRecChoice.InstancePtr.Unload();
					frmRateRecChoice.InstancePtr.Init(108);
					break;
				}
				case (int)UT_Commands.CreateExtractFile:
				{
					frmCreateExtractFile.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.ElectronicDataEntry:
				{
					frmBookChoice.InstancePtr.Init(16);
					break;
				}
				case (int)UT_Commands.BookStatus:
				{
					frmBookChoice.InstancePtr.Init(20);
					break;
				}
				case (int)UT_Commands.FinalBilling:
				{
					frmLoadBack.InstancePtr.Init(1);
					break;
				}
				case (int)UT_Commands.EmailBills:
				{
					frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
					frmRateRecChoice.InstancePtr.Init(104);
					break;
				}
				case (int)UT_Commands.InvoiceCloudExport:
				{
					frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
					frmRateRecChoice.InstancePtr.Init(111);
					break;
				}
				case (int)UT_Commands.CreateAutoPayFile:
				{
					frmAutoPay.InstancePtr.Init(false);
					// ********** collections
					break;
				}
				case (int)UT_Commands.AccountStatus:
				{
					commandDispatcher.Send(new MakeUtilityBillingTransaction
					{
						CorrelationId = Guid.NewGuid(),
						Id = Guid.NewGuid(),
						AccountNumber = 0,
						StartedFromCashReceipts = false,
						ShowPaymentScreen = false,
						AllowBatch = false
					});
					//	if (! frmUTCLStatus.InstancePtr.Visible) {
					//	frmGetMasterAccount.InstancePtr.Init(1, true);
					//} else {
					//	MessageBox.Show("There is already an account status screen open.", "Already Open", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					//	return;
					//}
					break;
				}
				case (int)UT_Commands.PaymentsAdjustments:
				{
					commandDispatcher.Send(new MakeUtilityBillingTransaction
					{
						CorrelationId = Guid.NewGuid(),
						Id = Guid.NewGuid(),
						AccountNumber = 0,
						StartedFromCashReceipts = false,
						ShowPaymentScreen = true,
						AllowBatch = false
					});
					//	if (! frmUTCLStatus.InstancePtr.Visible) {
					//	frmGetMasterAccount.InstancePtr.Init(2);
					//} else {
					//	MessageBox.Show("There is already an account status screen open.", "Already Open", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					//	return;
					//}
					break;
				}
				case (int)UT_Commands.DailyAuditReport:
				{
					frmUTDailyAudit.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.LoadOfBackInformation:
				{
					frmLoadBack.InstancePtr.Init(0);
					break;
				}
				case (int)UT_Commands.MortgageHolder:
				{
					frmGetMortgage.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.Menu_LienProcess:
				{
					ChangeMenu("Lien");
					break;
				}
				case (int)UT_Commands.ChargeInterest:
				{
					modGlobalFunctions.AddCYAEntry("UT", "Calculated Interest On Demand","","","","");
					modUTBilling.CalculateInterestAtBilling("",DateTime.Today);
					// ******** Lien
					break;
				}
				case (int)UT_Commands.LienEditReport:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(41);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
						frmRateRecChoice.InstancePtr.Init(1);
					}
					break;
				}
				case (int)UT_Commands.Menu_30DayNoticeProcess:
				{
					ChangeMenu("30DN");
					break;
				}
				case (int)UT_Commands.Menu_LienNotice:
				{
					ChangeMenu("LienNotice");
					break;
				}
				case (int)UT_Commands.Menu_LienMaturity:
				{
					ChangeMenu("Maturity");
					break;
				}
				case (int)UT_Commands.EditLienBookPage:
				{
					frmEditBookPage.InstancePtr.Init(0);
					break;
				}
				case (int)UT_Commands.EditLDNBookPage:
				{
					frmEditBookPage.InstancePtr.Init(1);
					break;
				}
				case (int)UT_Commands.Reverse30DayNoticeFee:
				{
					frmRemove30DNFee.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.RemoveFromLien:
				{
					frmRemoveLien.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.ReverseLienMaturityFee:
				{
					frmRemoveLienMatFee.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.LienDateChart:
				{
					frmUTLienDates.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.TaxAcquired:
				{
					frmTaxAcquiredList.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.UpdateBillAddresses:
				{
					frmUpdateAddresses.InstancePtr.Show(App.MainForm);
					// ********  Printing Menu
					break;
				}
				case (int)UT_Commands.AccountListing:
				{
					frmBookChoice.InstancePtr.Init(30);
					break;
				}
				case (int)UT_Commands.StatusLists:
				{
					frmUTStatusList.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.AccountLabels:
				{
					frmSetupMeterReports.InstancePtr.strMeterReportType = "L";
					frmSetupMeterReports.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.DeletedAccountsSummary:
				{
					arDeletedAccountSummary.InstancePtr.Init();
					break;
				}
				case (int)UT_Commands.MeterReports:
				{
					ChangeMenu("PrintMeter");
					break;
				}
				case (int)UT_Commands.AnalysisReports:
				{
					frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
					frmRateRecChoice.InstancePtr.Init(25);
					break;
				}
				case (int)UT_Commands.LienDischargeNotices:
				{
					frmUTLienDischargeGetAcct.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.DisconnectEditReport:
				{
					frmSetupDisconnectEditReport.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.DisconnectNotices:
				{
					frmSetupDisconnectNotices.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.RateTableListing:
				{
					if (DialogResult.No==MessageBox.Show("Would you like to print the condensed rate table listing?", "Rate Table Listing", MessageBoxButtons.YesNo, MessageBoxIcon.Question)) {
						frmReportViewer.InstancePtr.Init( rptRateTables.InstancePtr);
					} else {
						frmReportViewer.InstancePtr.Init( rptRateTablesCond.InstancePtr);
					}
					break;
				}
				case (int)UT_Commands.RateKeyListing:
				{
					rptRateKeyListing.InstancePtr.Init("");
					break;
				}
				case (int)UT_Commands.LoadBackReport:
				{
					frmReportViewer.InstancePtr.Init(rptLoadbackReportMaster.InstancePtr);
					break;
				}
				case (int)UT_Commands.RedisplayDailyAudit:
				{
					frmUTRPTViewer.InstancePtr.Init("LastUTDailyAudit", "Redisplay Utility Daily Audit", 1);
					break;
				}
				case (int)UT_Commands.ReminderNotices:
				{
					frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
					frmRateRecChoice.InstancePtr.Init(30);
					break;
				}
				case (int)UT_Commands.BookPageReport:
				{
					frmUTBookPageReport.InstancePtr.Init(0);
					break;
				}
				case (int)UT_Commands.TaxAcquiredReport:
				{
					frmReportViewer.InstancePtr.Init( rptTaxAcquiredProperties.InstancePtr);
					break;
				}
				case (int)UT_Commands.CommentsReport:
				{
					rptCommentsReport.InstancePtr.Init(false);
					break;
				}
				case (int)UT_Commands.PaymentActivityReport:
				{
					frmUTPmtActivityReport.InstancePtr.Show(App.MainForm);
					// ******    File maint menu
					break;
				}
				case (int)UT_Commands.Customize:
				{
					frmUTCustomize.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.CheckDBStructure:
				{
					modMain.NewVersion(true);
					break;
				}
				case (int)UT_Commands.Menu_MeterFunctions:
				{
					ChangeMenu("MeterFunctions");
					break;
				}
				case (int)UT_Commands.PurgePaidBills:
				{
					frmPurgePaidBills.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.ReprintPurgeReport:
				{
					frmReprintPurgeReport.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.EditCustomBillTypes:
				{
					clsDRWrapper rsSettings = new/*AsNew*/ clsDRWrapper();
					rsSettings.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
					if (!rsSettings.EndOfFile()) {
						if (fecherFoundation.Conversion.Val(FCConvert.ToString(rsSettings.Get_Fields("DefaultBillFormat")))>0) {
							frmCustomBill.InstancePtr.Init("UT","Custom Bill" ,  Convert.ToInt32(fecherFoundation.Conversion.Val(FCConvert.ToString(rsSettings.Get_Fields("DefaultBillFormat")))));
						} else {
							frmCustomBill.InstancePtr.Init("UT");
						}
					} else {
						frmCustomBill.InstancePtr.Init("UT");
					}
					break;
				}
				case (int)UT_Commands.ConvertConsumptionFile:
				{
					clsDRWrapper rsInfo = new/*AsNew*/ clsDRWrapper();
					rsInfo.OpenRecordset("SELECT * FROM UtilityBilling");
					if (rsInfo.EndOfFile()!=true && rsInfo.BeginningOfFile()!=true) {
						switch (rsInfo.Get_Fields("ExtractFileType")) {
							
							case "A":
							{
								modTSCONSUM.ConvertAquaAmericaExtractFile();
								break;
							}
							case "B":
							{
								// kk 01182013 trout-896  Add Badger Extract/Import
								modTSCONSUM.ConvertBadgerExtractFile();
								break;
							}
							case "D":
							{
								modTSCONSUM.ConvertTRIODOSExtractFile();
								break;
							}
							case "E":
							{
								// MAL@20071019
								modTSCONSUM.ConvertEZReaderExtractFile();
								break;
							}
							case "F":
							{
								modTSCONSUM.ConvertFarmingtonXRefExtractFile();
								break;
							}
							case "G":
							{
								// kk 03122013 trouts-5  Bangor Water District
								modTSCONSUM.ConvertBangorExtractFile();
								break;
							}
							case "H":
							{
								// kjr 09082016 trout-1245 Hampden
								modTSCONSUM.ConvertHampdenExtractFile();
								break;
							}
							case "I":
							{
								modTSCONSUM.ConvertNewFarmingtonXRefExtractFile();
								break;
							}
							case "L":
							{
								modTSCONSUM.ConvertLisbonExtractFile();
								break;
							}
							case "M":
							{
								// MAL@20080925 ; Millinocket
								modTSCONSUM.ConvertMillinocketExtractFile();
								break;
							}
							case "N":
							{
								modTSCONSUM.ConvertNorthernDataXRefExtractFile();
								break;
							}
							case "O":
							{
								modTSCONSUM.ConvertOaklandExtractFile();
								break;
							}
							case "P":
							{
								// Prescott book/seq files do not need to be converted
								MessageBox.Show("Prescott users do not need to convert thier TSUTXX41.DAT files.  It should already be in the correct format.", "No Conversion Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
								// ConvertPrescottExtractFile
								break;
							}
							case "Q":
							{
								// Prescott account/meter # file
								modTSCONSUM.ConvertPrescottExtractFile();
								break;
							}
							case "R":
							{
								modTSCONSUM.ConvertRVSExtractFile(); // RVS
								break;
							}
							case "S":
							{
								// kk11092018 trouts-263  Sensus Analytics/VFlex
								MessageBox.Show("VFlex/Sensus Analytics users do not need to convert thier TSUTXX41.DAT files.  It should already be in the correct format.", "No Conversion Needed", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							case "T":
							{
								modTSCONSUM.ConvertTiSalesExtractFile();
								break;
							}
							case "V":
							{
								// MAL@20070925
								modTSCONSUM.ConvertRVSWindowsExtractFile();
								break;
							}
							case "W":
							{
								modTSCONSUM.ConvertTRIOWindowsExtractFile();
								break;
							}
							case "X":
							{
								modTSCONSUM.ConvertTRIODOSXRefExtractFile();
								break;
							}
							case "Y":
							{
								modTSCONSUM.ConvertTRIOWindowsXRefExtractFile();
								break;
							}
							case "Z":
							{
								modTSCONSUM.ConvertBelfastExtractFile();
								break;
							}
							case "8":
							{
								// kk09022016 Add Veazie import for Orono
								modTSCONSUM.ConvertVeazieExtractFile();
								break;
							}
							case "9":
							{
								// kgk trout-754  09-22-2011  Dover-Foxcroft
								modTSCONSUM.ConvertDoverFoxcroftExtractFile();
								// code freeze trouts-279
								break;
							}
							case "7":
							{
								// kk01072014 trout-998 Topsham Sewer; kk01222014 trout-1027 Brunswick Sewer
								modTSCONSUM.ConvertBTWDConsumptionFile();
								break;
							}
							default: {
								MessageBox.Show("You must select an export file type in the File Maintenance > Customize screen before you may continue with this process.", "Invalid Extract Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
								break;
							}
						} //end switch
					} else {
						MessageBox.Show("You must select an export file type in the File Maintenance > Customize screen before you may continue with this process.", "Invalid Extract Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}


					break;
				}
				case (int)UT_Commands.CreateDatabaseExtract:
				{
					frmCreateDatabaseExtract.InstancePtr.Show(App.MainForm);					
					break;
				}
				case (int)UT_Commands.RateKeyUpdate:
				{
					frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
					frmRateRecChoice.InstancePtr.Init(200);
					break;
				}
				case (int)UT_Commands.ClearCertifiedMailTable:
				{
					modMain.ClearUTCMFNumbers();
					break;
				}
				case (int)UT_Commands.EditBillInformation:
				{
					frmUTEditBillInfo.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.ResetBills:
				{
					frmBookChoice.InstancePtr.Init(55);
					break;
				}
				case (int)UT_Commands.ResetFinalBills:
				{
					modUTBilling.ResetFinalBilling();
					break;
				}
				case (int)UT_Commands.RemoveFromTaxAcquired:
				{
					frmTaxAcquiredRemoval.InstancePtr.Init();
					break;
				}
				case (int)UT_Commands.SplitUTBills:
				{
					frmRateRecChoice.InstancePtr.Unload();
					frmRateRecChoice.InstancePtr.Init(110);
					break;
				}
				case (int)UT_Commands.PermanentlyDeleteAccount:
				{
					DeleteAcctPermanently();
					break;
				}
				case (int)UT_Commands.SyncWithREAccounts:
				{
					modStormwater.SyncWithREforStormwater(false);
					break;
				}
				case (int)UT_Commands.SetupAutoPay:
                {
                    var achBankForm = new frmACHBankInformation();
                    achBankForm.Show(App.MainForm);					
					// ****** Meter Functions
					break;
				}
				case (int)UT_Commands.LoadPreviousReadings:
				{
					frmBookChoice.InstancePtr.Init(14);
					break;
				}
				case (int)UT_Commands.MeterChangeOut:
				{
					frmMeterChangeOut.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.UpdateSequence:
				{
					frmBookChoice.InstancePtr.Init(60);
					// ****** Print 30 DN menu
					break;
				}
				case (int)UT_Commands.Print30DayNotices:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(42);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
						frmRateRecChoice.InstancePtr.Init(2);
					}
					break;
				}
				case (int)UT_Commands.ThirtyDN_CertifiedMailFormLabels:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(50);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
						frmRateRecChoice.InstancePtr.Init(10);
					}
					break;
				}
				case (int)UT_Commands.ApplyDemandFees:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(45);
					} else {
						frmApplyDemandFees.InstancePtr.Init("");
					}
					break;
				}
				case (int)UT_Commands.ThirtyDN_ReprintWaterLabels:
				{
					frmReportViewer.InstancePtr.Init(null ,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUT30DayLabels1.RDF"), "Reprint Water Labels");
					break;
				}
				case (int)UT_Commands.ThirtyDN_ReprintSewerLabels:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUT30DayLabels1.RDF"), "Reprint Sewer Labels");
					break;
				}
				case (int)UT_Commands.ThirtyDN_ReprintWCMForms:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUT30DayForms1.RDF"), "Reprint Water CFM Forms");
					break;
				}
				case (int)UT_Commands.ThirtyDN_ReprintSCMForms:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUT30DayForms1.RDF"), "Reprint Sewer CFM Forms");
					break;
				}
				case (int)UT_Commands.ReprintWaterDemandList:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTDemandFeesList1.RDF"), "Reprint Water Demand Fees List");
					break;
				}
				case (int)UT_Commands.ReprintSewerDemandList:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTDemandFeesList1.RDF"), "Reprint Sewer Demand Fees List");
					// ********** lien maturity
					break;
				}
				case (int)UT_Commands.PrintLienMaturityNotices:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(44);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for liens only
						frmRateRecChoice.InstancePtr.Init(4);
					}
					break;
				}
				case (int)UT_Commands.LMN_CertifiedMailFormsLabels:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(52);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for liens only
						frmRateRecChoice.InstancePtr.Init(12);
					}
					break;
				}
				case (int)UT_Commands.ApplyMaturityFees:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(47);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for liens only
						frmRateRecChoice.InstancePtr.Init(7);
					}
					break;
				}
				case (int)UT_Commands.LMN_ReprintWaterLabels:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false , "Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTLienMaturityLabels1.RDF"), "Reprint Water Labels");
					break;
				}
				case (int)UT_Commands.LMN_ReprintSewerLabels:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages", false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTLienMaturityLabels1.RDF"), "Reprint Sewer Labels");
					break;
				}
				case (int)UT_Commands.LMN_ReprintWaterCMForms:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTLienMaturityForms1.RDF"), "Reprint Water CFM Forms");
					break;
				}
				case (int)UT_Commands.LMN_ReprintSewerCMForms:
				{
					frmReportViewer.InstancePtr.Init(null,"",0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTLienMaturityForms1.RDF"), "Reprint Sewer CFM Forms");
					break;
				}
				case (int)UT_Commands.ReprintWaterMatFeeList:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTLienMaturityList.RDF"), "Reprint Water Maturity Fee List");
					break;
				}
				case (int)UT_Commands.ReprintSewerMatFeeList:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTLienMaturityList1.RDF"), "Reprint Sewer Maturity Fee List");
					break;
				}
				case (int)UT_Commands.PrintBankruptcyReport:
				{
					frmUTRPTViewer.InstancePtr.Init("LastUTBankruptcy", "Reprint Bankruptcy Report", 11);
					frmUTRPTViewer.InstancePtr.Show(App.MainForm);
					// *********** lien notice
					break;
				}
				case (int)UT_Commands.PrintLienNotices:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(43);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
						frmRateRecChoice.InstancePtr.Init(3);
					}
					break;
				}
				case (int)UT_Commands.LN_CertifiedMailFormsLabels:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(51);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
						frmRateRecChoice.InstancePtr.Init(11);
					}
					break;
				}
				case (int)UT_Commands.TransferBillsToLienStatus:
				{
					if (modUTCalculations.Statics.gboolUseBookForLien) {
						frmBookChoice.InstancePtr.Init(46);
					} else {
						frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for liens only
						frmRateRecChoice.InstancePtr.Init(6);
					}
					break;
				}
				case (int)UT_Commands.LN_ReprintWaterLabels:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTTransferToLienLabels1.RDF"), "Reprint Water Labels");
					break;
				}
				case (int)UT_Commands.LN_ReprintSewerLabels:
				{
					frmReportViewer.InstancePtr.Init(null,"" ,0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTTransferToLienLabels1.RDF"), "Reprint Sewer Labels");
					break;
				}
				case (int)UT_Commands.LN_ReprintWaterCMForms:
				{
					frmReportViewer.InstancePtr.Init(null, "",0 ,false ,false ,"Pages" ,false , System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTTransferToLienForms1.RDF"), "Reprint Water CFM Forms");
					break;
				}
				case (int)UT_Commands.LN_ReprintSewerCMForms:
				{
					frmReportViewer.InstancePtr.Init(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTTransferToLienForms1.RDF"), "Reprint Sewer CFM Forms");
					break;
				}
				case (int)UT_Commands.LN_ReprintWaterTransferList:
				{
					frmReportViewer.InstancePtr.Init(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTTranfserToLien1.RDF"), "Reprint Water Transfer List");
					break;
				}
				case (int)UT_Commands.LN_ReprintSewerTransferList:
				{
					frmReportViewer.InstancePtr.Init(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTTranfserToLien1.RDF"), "Reprint Sewer Transfer List");
					break;
				}
				case (int)UT_Commands.MeterReadingSlips:
				{
					frmSetupMeterReports.InstancePtr.strMeterReportType = "S";
					frmSetupMeterReports.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.MeterExchangeList:
				{
					frmSetupMeterReports.InstancePtr.strMeterReportType = "E";
					frmSetupMeterReports.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.MeterCountByCategory:
				{
					frmReportViewer.InstancePtr.Init(arMeterCount.InstancePtr);
					break;
				}
				case (int)UT_Commands.MeterListWRateTables:
				{
					arRateTableSummary.InstancePtr.Init();
					break;
				}
				case (int)UT_Commands.MeterErrorReport:
				{
					//////Application.DoEvents();
					frmReportViewer.InstancePtr.Init( arMeterCheck.InstancePtr);
					break;
				}
				case (int)UT_Commands.HighestConsumptionReport:
				{
					frmHighestConsumptionReport.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.MeterChangeoutHistory:
				{
					frmChangeOutHistoryReport.InstancePtr.Show(App.MainForm);
					break;
				}
				case (int)UT_Commands.MeterReadingList:
				{
					frmBookChoice.InstancePtr.Init(21);
					break;
				}
				case (int)UT_Commands.ConsumptionHistoryReport:
				{
					frmRateRecChoice.InstancePtr.Unload(); // kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
					frmRateRecChoice.InstancePtr.Init(250);

					break;
				}
                case (int)UT_Commands.Menu_Custom:
                    ChangeMenu("Custom");
                    break;
                case (int) UT_Commands.Custom_Oakland_BadAccounts:
                {
                    frmOaklandBadNumbers.InstancePtr.Init();
                    break;
                }
                case (int) UT_Commands.Custom_MechanicFalls_Consumption:
                {
                    CustomConsumptionCalculation();
                    break;
                }
                case (int)UT_Commands.Custom_MechanicFalls_ConsumptionUpdate:
                    frmMFSConsumptionUpdate.InstancePtr.Init();
                    break;
                case (int)UT_Commands.Custom_Lisbon_SummerConsumptionCalc:
                    frmCustomLisbonConsumption.InstancePtr.Show(App.MainForm);
                    break;
                case (int)UT_Commands.Custom_Lisbon_UpdateWinterBilling:
                    frmCustomLisbonUpdateWinterBilling.InstancePtr.Show(App.MainForm);
                    break;
                case (int)UT_Commands.Custom_Norway_ZeroBillsByBook:
                    var zeroBills = new frmCustomNorwayZeroBills();
                    zeroBills.Show(App.MainForm);
                    break;
                case (int)UT_Commands.Custom_BrunswickSewer_ImportExceptionList:
                    frmImportExceptionList.InstancePtr.Init();
                    break;
            } //end switch
		}

		private void ExitApplication()
		{
            try
            {
                modBlockEntry.WriteYY();
                App.MainForm.OpenModule("TWGNENTY");
                FCUtils.CloseFormsOnProject();
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Unloading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

		}

		private void DeleteAcctPermanently()
		{
			object lngAcct = 0;
			clsDRWrapper rsTemp = new/*AsNew*/ clsDRWrapper();
			clsDRWrapper rsDelete = new/*AsNew*/ clsDRWrapper();
			clsDRWrapper rsBill = new/*AsNew*/ clsDRWrapper();
			int lngCode;
			int lngKey = 0;

			try
			{	// On Error GoTo ErrorHandler

				if (frmInput.InstancePtr.Init(ref lngAcct, "Delete Account", "Enter Account to Delete", 1440, false,  modGlobalConstants.InputDTypes.idtWholeNumber, "", true)) {
					rsTemp.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = "+FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);

					if (!rsTemp.EndOfFile()) {
						if (rsTemp.Get_Fields("Deleted")==true) {
							if (MessageBox.Show("This will permanently delete the data for this account"+"\n"+"You will not be able to get the data back"+"\n"+"Are you sure you want to delete this account?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)==DialogResult.Yes) {
								lngKey = Convert.ToInt32(rsTemp.Get_Fields("ID"));
								modGlobalFunctions.AddCYAEntry("UT", "Permanently Deleted", "Account "+FCConvert.ToString(lngAcct),"","","");


								// Delete all but Bill, Lien, Discharge and Master
								rsDelete.Execute("DELETE FROM CMFNumbers WHERE ActualAccountNumber = "+FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
								rsDelete.Execute("DELETE FROM Comments WHERE Account = "+FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
								// kgk  ???DELETE FROM RE???     .Execute "DELETE FROM SRMaster WHERE AccountNumber = " & lngAcct, strREDatabase
								rsDelete.Execute("DELETE FROM CommentRec WHERE AccountKey = "+FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
								rsDelete.Execute("DELETE FROM FinalBilled WHERE AccountKey = "+FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
								rsDelete.Execute("DELETE FROM MeterConsumption WHERE AccountKey = "+FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
								rsDelete.Execute("DELETE FROM MeterTable WHERE AccountKey = "+FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
								rsDelete.Execute("DELETE FROM PaymentRec WHERE AccountKey = "+FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
								rsDelete.Execute("DELETE FROM PreviousOwner WHERE AccountKey = "+FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);

								// Loop through and delete Bills and attached records
								rsDelete.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = "+FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
								if (rsDelete.RecordCount()>0) {
									rsDelete.MoveFirst();
									while (!rsDelete.EndOfFile()) {
										if (rsDelete.Get_Fields("WLienRecordNumber")>0) {
											rsBill.Execute("DELETE FROM Lien WHERE ID = "+rsDelete.Get_Fields("WLienRecordNumber"), modExtraModules.strUTDatabase);
										}

										if (rsDelete.Get_Fields("SLienRecordNumber")>0) {
											rsBill.Execute("DELETE FROM Lien WHERE ID = "+rsDelete.Get_Fields("SLienRecordNumber"), modExtraModules.strUTDatabase);
										}

										rsBill.Execute("DELETE FROM DischargeNeeded WHERE BillKey = "+rsDelete.Get_Fields("ID"), modExtraModules.strUTDatabase);
										rsBill.Execute("DELETE FROM DischargeNeededArchive WHERE BillKey = "+rsDelete.Get_Fields("ID"), modExtraModules.strUTDatabase);

										rsDelete.MoveNext();
									}
								}


								rsTemp.Delete();
								rsTemp.Update(); // kk07142014 trouts-105  Permanent Delete not working

								MessageBox.Show("Account Deleted", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;

							}
						} else {
							MessageBox.Show("Account has not been flagged as deleted."+"\r\n"+"This must be done before an account can be purged.", "Unable to Continue", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					} else {
						MessageBox.Show("Account "+FCConvert.ToString(lngAcct)+" could not be found", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}

				}
				return;
			}
			catch (Exception ex)
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)+"  "+fecherFoundation.Information.Err(ex).Description+"\n"+"In DeleteAcctPermanently", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        private void CustomConsumptionCalculation()
        {
            // - "AutoDim"
            clsDRWrapper rsConsumptionInfo = new clsDRWrapper();
            clsDRWrapper rsInfo = new clsDRWrapper();
            // vbPorter upgrade warning: lngReading As int	OnWrite(int, double)
            int[] lngReading = new int[4 + 1];
            int counter;
            int lngPrev = 0;
            int lngCurr = 0;
            int lngAdj = 0;
            int lngDigits = 0;
            int intReadings = 0;
            // vbPorter upgrade warning: lngAverage As int	OnWriteFCConvert.ToDouble(
            int lngAverage = 0;
            int lngHighConsumption = 0;
            bool blnHasCurrent = false;
            // MAL@20071022
            DateTime dtCurrReadDate = default(DateTime);
            FCUtils.EraseSafe(modCustomPrograms.Statics.cciInfo);
            modCustomPrograms.Statics.lngAcctCounter = 0;
            rsInfo.OpenRecordset("SELECT * FROM MeterTable");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
            {
                modCustomPrograms.Statics.lngAcctCounter = 1;
                do
                {
                    for (counter = 0; counter <= 4; counter++)
                    {
                        lngReading[counter] = 0;
                    }
                    intReadings = 0;
                    counter = 0;
                    if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("CurrentReading")) != -1)
                    {
                        // kk 110812 trout-884 Change from 0 to -1 for No Read
                        lngPrev = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("PreviousReading"));
                        lngCurr = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("CurrentReading"));
                        lngDigits = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("Digits"));
                        // MAL@20071022: Added to track current reading date for later use
                        blnHasCurrent = true;
                        dtCurrReadDate = (DateTime)rsInfo.Get_Fields_DateTime("CurrentReadingDate");
                        if (lngCurr > lngPrev)
                        {
                            lngReading[counter] = lngCurr - lngPrev;
                            counter += 1;
                            intReadings += 1;
                        }
                        else
                        {
                            switch (modExtraModules.Statics.glngTownReadingUnits)
                            {
                                case 100000:
                                    {
                                        lngAdj = 5;
                                        break;
                                    }
                                case 10000:
                                    {
                                        lngAdj = 4;
                                        break;
                                    }
                                case 1000:
                                    {
                                        lngAdj = 3;
                                        break;
                                    }
                                case 100:
                                    {
                                        lngAdj = 2;
                                        break;
                                    }
                                case 10:
                                    {
                                        lngAdj = 1;
                                        break;
                                    }
                                case 1:
                                    {
                                        lngAdj = 0;
                                        break;
                                    }
                                default:
                                    {
                                        lngAdj = 0;
                                        break;
                                    }
                            }
                            //end switch
                            // this must be a rolled over meter
                            lngReading[counter] = FCConvert.ToInt32(lngCurr + ((Math.Pow(10, (lngDigits - lngAdj))) - lngPrev));
                            counter += 1;
                            intReadings += 1;
                        }
                    }
                    rsConsumptionInfo.OpenRecordset("SELECT * FROM MeterConsumption WHERE AccountKey = " + rsInfo.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsInfo.Get_Fields_Int32("ID") + " ORDER BY BillDate DESC");
                    if (rsConsumptionInfo.EndOfFile() != true && rsConsumptionInfo.BeginningOfFile() != true)
                    {
                        do
                        {
                            if (counter < 4)
                            {
                                // MAL@20071022: Found that the current consumption was being duplicated in calcs
                                if (blnHasCurrent && rsConsumptionInfo.Get_Fields_DateTime("BillDate").ToOADate() > dtCurrReadDate.ToOADate())
                                {
                                    // Skip This Record - Already Have this Information
                                }
                                else
                                {
                                    lngReading[counter] = FCConvert.ToInt32(rsConsumptionInfo.Get_Fields_Int32("Consumption"));
                                    counter += 1;
                                    intReadings += 1;
                                }
                            }
                            else
                            {
                                break;
                            }
                            rsConsumptionInfo.MoveNext();
                        }
                        while (rsConsumptionInfo.EndOfFile() != true);
                    }
                    lngHighConsumption = 0;
                    for (counter = 0; counter <= intReadings - 1; counter++)
                    {
                        if (lngHighConsumption < lngReading[counter])
                        {
                            lngHighConsumption = lngReading[counter];
                        }
                    }
                    if (intReadings == 4)
                    {
                        lngAverage = FCConvert.ToInt32(FCConvert.ToDouble(lngReading[0] + lngReading[1] + lngReading[2] + lngReading[3] - lngHighConsumption) / (intReadings - 1));
                        rsInfo.Edit();
                        rsInfo.Set_Fields("SewerAmount2", lngAverage);
                        rsInfo.Update();
                        Array.Resize(ref modCustomPrograms.Statics.cciInfo, modCustomPrograms.Statics.lngAcctCounter - 1 + 1);
                        modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngAccountKey = rsInfo.Get_Fields_Int32("AccountKey");
                        modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngMeterKey = rsInfo.Get_Fields_Int32("MeterNumber");
                        modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngConsumptionOverride = lngAverage;
                    }
                    else
                    {
                        Array.Resize(ref modCustomPrograms.Statics.cciInfo, modCustomPrograms.Statics.lngAcctCounter - 1 + 1);
                        modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngAccountKey = rsInfo.Get_Fields_Int32("AccountKey");
                        modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngMeterKey = rsInfo.Get_Fields_Int32("MeterNumber");
                        modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngConsumptionOverride = -1;
                    }
                    modCustomPrograms.Statics.lngAcctCounter += 1;
                    rsInfo.MoveNext();
                }
                while (rsInfo.EndOfFile() != true);
                modCustomPrograms.Statics.lngAcctCounter -= 1;
            }
            if (modCustomPrograms.Statics.lngAcctCounter > 0)
            {
                frmReportViewer.InstancePtr.Init(rptCustomConsumptionCalculationReport.InstancePtr);
            }
            MessageBox.Show("Consumption Overrides Set", "Calculations Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

    }
}
