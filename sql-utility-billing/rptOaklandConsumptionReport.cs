﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using GrapeCity.ActiveReports;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptOaklandConsumptionReport.
	/// </summary>
	public partial class rptOaklandConsumptionReport : BaseSectionReport
	{
		public rptOaklandConsumptionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Consumption Report";
		}

		public static rptOaklandConsumptionReport InstancePtr
		{
			get
			{
				return (rptOaklandConsumptionReport)Sys.GetInstance(typeof(rptOaklandConsumptionReport));
			}
		}

		protected rptOaklandConsumptionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOaklandConsumptionReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		string strListType;
		bool blnFirstRecord;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord == true)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				if (strListType == "A")
				{
					strListType = "B";
					eArgs.EOF = false;
				}
				else if (strListType == "B")
				{
					strListType = "C";
					eArgs.EOF = false;
				}
				else if (strListType == "C")
				{
					strListType = "D";
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			strListType = "A";
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (strListType == "A")
			{
				subConsumptionList.Report = new srptOaklandDisketteNoMatch();
			}
			else if (strListType == "B")
			{
				subConsumptionList.Report = new srptOaklandMetersWithNoUpdate();
			}
			else if (strListType == "C")
			{
				subConsumptionList.Report = new srptOaklandZeroReadings();
			}
			else if (strListType == "D")
			{
				subConsumptionList.Report = new srptOaklandReadings();
			}
			else
			{
				subConsumptionList.Report = null;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptOaklandConsumptionReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptOaklandConsumptionReport properties;
			//rptOaklandConsumptionReport.Caption	= "Consumption Report";
			//rptOaklandConsumptionReport.Icon	= "rptOaklandConsumptionReport.dsx":0000";
			//rptOaklandConsumptionReport.Left	= 0;
			//rptOaklandConsumptionReport.Top	= 0;
			//rptOaklandConsumptionReport.Width	= 11880;
			//rptOaklandConsumptionReport.Height	= 8595;
			//rptOaklandConsumptionReport.StartUpPosition	= 3;
			//rptOaklandConsumptionReport.SectionData	= "rptOaklandConsumptionReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
