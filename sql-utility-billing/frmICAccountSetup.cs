﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmICAccountSetup.
	/// </summary>
	public partial class frmICAccountSetup : BaseForm
	{
		public frmICAccountSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmICAccountSetup InstancePtr
		{
			get
			{
				return (frmICAccountSetup)Sys.GetInstance(typeof(frmICAccountSetup));
			}
		}

		protected frmICAccountSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/03/2007              *
		// ********************************************************
		bool boolDirty;
		// this will keep track of new codes being added and not saved
		bool boolLockType;
		bool boolLoaded;
		int intCurCode;
		bool boolLeftArrow;
		bool boolAccountBox;
		string strComboList = "";
		bool boolChangingRows;
		bool boolSetBMV;
		// this will get set then the click event should not fire
		bool boolTRIOOverride;
		bool boolDoNotChangeType;
		clsGridAccount clsAcct = new clsGridAccount();
		clsGridAccount clsAltCashAccount = new clsGridAccount();
		bool boolSaving;
		int CategoryCol;
		int TitleCol;
		int AbbrevCol;
		int AccountCol;
		int YearCol;
		int DefaultCol;
		int KeyCol;
		int ProjCol;
		int PercentCol;
		bool blnIsDeleted;
		// MAL@20071010
		private void FormatGrid()
		{
			clsDRWrapper rsProjects = new clsDRWrapper();
			string strComboList = "";
			// sets all of the widths, datatypes and titles for the grid
			int Width = 0;
			vsReceipt.Cols = 9;
			vsReceipt.EditMaxLength = 20;
			Width = vsReceipt.WidthOriginal;
			vsReceipt.ColWidth(CategoryCol, FCConvert.ToInt32(Width * 0.08));
			// Category Number
			vsReceipt.ColWidth(TitleCol, FCConvert.ToInt32(Width * 0.22));
			// Title
			vsReceipt.ColWidth(AbbrevCol, FCConvert.ToInt32(Width * 0.15));
			// Abbrev Title
			vsReceipt.ColWidth(AccountCol, FCConvert.ToInt32(Width * 0.19));
			// Account
			vsReceipt.ColWidth(YearCol, FCConvert.ToInt32(Width * 0.08));
			// Year
			// hidden column
			vsReceipt.ColWidth(KeyCol, 0);
			// Primary Key in table
			vsReceipt.ColHidden(KeyCol, true);
			vsReceipt.ColWidth(PercentCol, FCConvert.ToInt32(Width * 0.08));
            vsReceipt.ExtendLastCol = true;
			// Percent
			// check box for the Year column
			vsReceipt.ColDataType(YearCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsReceipt.ColDataType(PercentCol, FCGrid.DataTypeSettings.flexDTBoolean);
			// alignment
			vsReceipt.ColAlignment(CategoryCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
            vsReceipt.ColAlignment(DefaultCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
            // default format for the Default Dollar column
            // .ColFormat(5) = "#,##0.00"
            // bold the header
            vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsReceipt.Cols - 1, true);
			//vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsReceipt.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// sets the titles for the flexgrid
			vsReceipt.TextMatrix(0, CategoryCol, "Cat");
			vsReceipt.TextMatrix(0, TitleCol, "Screen Title");
			vsReceipt.TextMatrix(0, AbbrevCol, "Report Title");
			vsReceipt.TextMatrix(0, AccountCol, "Account");
			vsReceipt.TextMatrix(0, YearCol, "Year");
			vsReceipt.TextMatrix(0, DefaultCol, "Default $");
			vsReceipt.TextMatrix(0, ProjCol, "Proj");
			vsReceipt.TextMatrix(0, PercentCol, "%");
			// this will make two cols and the second col has a length of 0
			cmbCode.Cols = 2;
			cmbCode.ColWidth(1, 0);
		}

		private void cmbCode_ChangeEdit(object sender, System.EventArgs e)
		{
            if (cmbCode.IsCurrentCellInEditMode)
            {
                ProcessType();
                cmbCode.Focus();
                cmbCode.EditCell();
            }
		}

		private void cmbCode_ClickEvent(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_ComboDropDown(object sender, System.EventArgs e)
		{
			cmbCode.ComboList = strComboList;
		}

		private void cmbCode_DblClick(object sender, System.EventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void cmbCode_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						ProcessType();
						if (RestrictedCode())
						{
						}
						else
						{
							if (txtTitle.Enabled && txtTitle.Visible)
							{
								txtTitle.Focus();
							}
							else if (cmbType.Visible && cmbType.Enabled)
							{
								cmbType.Focus();
							}
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCode_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						ProcessType();
						if (RestrictedCode())
						{
						}
						else
						{
							if (txtTitle.Enabled && txtTitle.Visible)
							{
								txtTitle.Focus();
							}
							else if (cmbType.Visible && cmbType.Enabled)
							{
								cmbType.Focus();
							}
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			ProcessType();
		}

		private void cmbCode_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			ProcessType();
		}

		private void cmbType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolDoNotChangeType)
			{
				ProcessType();
			}
		}

		private void cmbType_DoubleClick(object sender, System.EventArgs e)
		{
			if (!boolDoNotChangeType)
			{
				ProcessType();
			}
		}

		private void cmbType_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbType.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbType_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will force the combobox to open with a spacebar
						if (modAPIsConst.SendMessageByNum(cmbType.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbType.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.J:
					{
						if (Shift == 3)
						{
							KeyCode = (Keys)0;
							//if (MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Bevel==sbrInset) {
							//	MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Text = "";
							//	MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Bevel = sbrNoBevel;
							//} else {
							//	MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Text = "Software Designer : "+modEncrypt.ToggleEncryptCode("н");
							//	MDIParent.InstancePtr.StatusBar1.Panels[2 - 1].Bevel = sbrInset;
							//}
						}
						break;
					}
			}
			//end switch
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			// this goes back to the original state where the user has to choose the type
			mnuFileBack.Enabled = true;
			mnuFileForward.Enabled = true;
			txtCode.Text = "";
			ShowFrame(ref fraReceiptList);
			fraNewType.Visible = false;
			if (cmbType.Visible && cmbType.Enabled)
			{
				cmbType.Focus();
			}
			// cmbCode.SetFocus
		}

		public void cmdCancel_Click()
		{
			cmdCancel_Click(cmdCancel, new System.EventArgs());
		}

		private void cmdCancel_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short FindNextComboIndex(ref short intCD)
		{
			short FindNextComboIndex = 0;
			// this will return the combo index on the next type
			int intCT;
			FindNextComboIndex = 0;
			for (intCT = 0; intCT <= cmbCode.ComboCount - 1; intCT++)
			{
				if (Conversion.Val(cmbCode.ComboItem(intCT)) == intCD)
				{
					FindNextComboIndex = FCConvert.ToInt16(intCT);
					return FindNextComboIndex;
				}
			}
			return FindNextComboIndex;
		}

		private void frmICAccountSetup_Activated(object sender, System.EventArgs e)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				intErr = 1;
				if (modMain.FormExist(this))
					return;
				if (!boolLoaded)
				{
					intErr = 2;
					boolDirty = false;
					intErr = 4;
					//fraReceiptList.Left = FCConvert.ToInt32((this.Width - fraReceiptList.Width) / 2.0);
					//fraReceiptList.Top = FCConvert.ToInt32((this.Height - fraReceiptList.Height) / 3.0);
					fraReceiptList.Visible = true;
					intErr = 5;
					lblDefaultAccount.Text = "Alternate cash account:";
					intErr = 6;
					FormatGrid();
					intErr = 9;
					FillCodeCombo();
					intErr = 10;
					// this should show the first type
					if (cmbType.Items.Count > 0)
					{
						cmbType.SelectedIndex = 0;
					}
					if (cmbType.Visible && cmbType.Enabled)
					{
						cmbType.Focus();
					}
					boolLoaded = true;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Activate - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmICAccountSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// vbPorter upgrade warning: lngTemp As int	OnWrite(string)
			int lngTemp = 0;
			int intIndex;
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Escape:
					{
						// exit
						KeyCode = (Keys)0;
						if (fraNewType.Visible)
						{
							cmdCancel_Click();
						}
						else
						{
							this.Unload();
							return;
						}
						break;
					}
				case Keys.F9:
					{
						if (Shift == 3)
						{
							// this will be CTRL-SHIFT-F1
							KeyCode = (Keys)0;
							lngTemp = FCConvert.ToInt32(Interaction.InputBox("Please enter the code supplied to you by a TRIO staff member", "Save Override Code"/*, null, -1, -1*/));
							if (modModDayCode.CheckWinModCode(lngTemp, "CR"))
							{
								boolTRIOOverride = true;
								MessageBox.Show("TRIO Save override code has been activated.", "Override Activated", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							else
							{
								boolTRIOOverride = false;
								MessageBox.Show("TRIO Save override code has not been activated.", "Override Not Activated", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						break;
					}
			}
			//end switch
			if (Strings.UCase(this.ActiveControl.GetName()) == Strings.UCase("vsReceipt"))
			{
				if (vsReceipt.Col == 3 && (KeyCode < Keys.F1 || KeyCode > Keys.F12))
				{
					modNewAccountBox.CheckFormKeyDown(vsReceipt, vsReceipt.Row, vsReceipt.Col, KeyCode, Shift, vsReceipt.EditSelStart, vsReceipt.EditText, vsReceipt.EditSelLength);
				}
			}
		}

		private void frmICAccountSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmICAccountSetup properties;
			//frmICAccountSetup.FillStyle	= 0;
			//frmICAccountSetup.ScaleWidth	= 9045;
			//frmICAccountSetup.ScaleHeight	= 6420;
			//frmICAccountSetup.LinkTopic	= "Form1";
			//End Unmaped Properties
			// this will size the form to the MDIParent
			CategoryCol = 0;
			TitleCol = 1;
			AbbrevCol = 2;
			AccountCol = 3;
			YearCol = 4;
			DefaultCol = 5;
			KeyCol = 6;
			ProjCol = 7;
			PercentCol = 8;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			vsReceipt.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			clsAcct.GRID7Light = vsReceipt;
			clsAltCashAccount.GRID7Light = txtDefaultAccount;
			clsAcct.AccountCol = FCConvert.ToInt16(AccountCol);
			clsAcct.DefaultAccountType = "R";
			clsAltCashAccount.DefaultAccountType = "G";
			if (modGlobalConstants.Statics.gboolBD)
			{
				modValidateAccount.SetBDFormats();
				modNewAccountBox.GetFormats();
			}
			vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}
		// vbPorter upgrade warning: intTypeCode As short	OnWriteFCConvert.ToInt32(
		private void ShowType(int intTypeCode = 0, int lngResCode = 0, bool boolTryOnce = false)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsType = new clsDRWrapper();
				int intCT;
				string strTemp = "";
				intErr = 0;
				rsType.DefaultDB = modExtraModules.strUTDatabase;
				intErr = 1;
				if (intTypeCode == 0)
				{
					// new type or blank to start
					intErr = 2;
					vsReceipt.Rows = 1;
					// clears grid
					vsReceipt.Rows = 7;
					txtTitle.Text = "";
					boolLockType = false;
					lblAccountTitle.Text = "";
					ColorGrid_2(false);
				}
				else
				{
					// edit or examine a code that is already created
					intErr = 3;
					lblAccountTitle.Text = "";
					rsType.OpenRecordset("SELECT * FROM tblIConnectAccountInfo WHERE TypeCode = " + FCConvert.ToString(intTypeCode), modExtraModules.strUTDatabase);
					if (rsType.EndOfFile() != true && rsType.BeginningOfFile() != true)
					{
						intErr = 4;
						strTemp = FCConvert.ToString(rsType.Get_Fields_String("TypeTitle"));
						txtTitle.Text = strTemp;
						intErr = 16;
						vsReceipt.TextMatrix(1, CategoryCol, FCConvert.ToString(1));
						vsReceipt.TextMatrix(1, TitleCol, FCConvert.ToString(rsType.Get_Fields_String("Title1")));
						vsReceipt.TextMatrix(1, AbbrevCol, FCConvert.ToString(rsType.Get_Fields_String("Title1Abbrev")));
						vsReceipt.TextMatrix(1, AccountCol, FCConvert.ToString(rsType.Get_Fields_String("Account1")));
						vsReceipt.TextMatrix(1, YearCol, FCConvert.ToString(rsType.Get_Fields_Boolean("Year1")));
						vsReceipt.TextMatrix(1, DefaultCol, FCConvert.ToString(rsType.Get_Fields_Double("DefaultAmount1")));
						intErr = 17;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee1")))
						{
							vsReceipt.TextMatrix(1, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(1, PercentCol, FCConvert.ToString(0));
						}
						intErr = 18;
						if (Strings.Left(vsReceipt.TextMatrix(1, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(1, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 19;
						vsReceipt.TextMatrix(2, CategoryCol, FCConvert.ToString(2));
						vsReceipt.TextMatrix(2, TitleCol, FCConvert.ToString(rsType.Get_Fields_String("Title2")));
						vsReceipt.TextMatrix(2, AbbrevCol, FCConvert.ToString(rsType.Get_Fields_String("Title2Abbrev")));
						vsReceipt.TextMatrix(2, AccountCol, FCConvert.ToString(rsType.Get_Fields_String("Account2")));
						vsReceipt.TextMatrix(2, YearCol, FCConvert.ToString(rsType.Get_Fields_Boolean("Year2")));
						vsReceipt.TextMatrix(2, DefaultCol, FCConvert.ToString(rsType.Get_Fields_Double("DefaultAmount2")));
						intErr = 20;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee2")))
						{
							vsReceipt.TextMatrix(2, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(2, PercentCol, FCConvert.ToString(0));
						}
						intErr = 21;
						if (Strings.Left(vsReceipt.TextMatrix(2, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(2, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 22;
						vsReceipt.TextMatrix(3, CategoryCol, FCConvert.ToString(3));
						vsReceipt.TextMatrix(3, TitleCol, FCConvert.ToString(rsType.Get_Fields_String("Title3")));
						vsReceipt.TextMatrix(3, AbbrevCol, FCConvert.ToString(rsType.Get_Fields_String("Title3Abbrev")));
						vsReceipt.TextMatrix(3, AccountCol, FCConvert.ToString(rsType.Get_Fields_String("Account3")));
						vsReceipt.TextMatrix(3, YearCol, FCConvert.ToString(rsType.Get_Fields_Boolean("Year3")));
						vsReceipt.TextMatrix(3, DefaultCol, FCConvert.ToString(rsType.Get_Fields_Double("DefaultAmount3")));
						intErr = 23;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee3")))
						{
							vsReceipt.TextMatrix(3, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(3, PercentCol, FCConvert.ToString(0));
						}
						intErr = 24;
						if (Strings.Left(vsReceipt.TextMatrix(3, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(3, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 25;
						vsReceipt.TextMatrix(4, CategoryCol, FCConvert.ToString(4));
						vsReceipt.TextMatrix(4, TitleCol, FCConvert.ToString(rsType.Get_Fields_String("Title4")));
						vsReceipt.TextMatrix(4, AbbrevCol, FCConvert.ToString(rsType.Get_Fields_String("Title4Abbrev")));
						vsReceipt.TextMatrix(4, AccountCol, FCConvert.ToString(rsType.Get_Fields_String("Account4")));
						vsReceipt.TextMatrix(4, YearCol, FCConvert.ToString(rsType.Get_Fields_Boolean("Year4")));
						vsReceipt.TextMatrix(4, DefaultCol, FCConvert.ToString(rsType.Get_Fields_Double("DefaultAmount4")));
						intErr = 26;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee4")))
						{
							vsReceipt.TextMatrix(4, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(4, PercentCol, FCConvert.ToString(0));
						}
						intErr = 27;
						if (Strings.Left(vsReceipt.TextMatrix(4, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(4, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 28;
						vsReceipt.TextMatrix(5, CategoryCol, FCConvert.ToString(5));
						vsReceipt.TextMatrix(5, TitleCol, FCConvert.ToString(rsType.Get_Fields_String("Title5")));
						vsReceipt.TextMatrix(5, AbbrevCol, FCConvert.ToString(rsType.Get_Fields_String("Title5Abbrev")));
						vsReceipt.TextMatrix(5, AccountCol, FCConvert.ToString(rsType.Get_Fields_String("Account5")));
						vsReceipt.TextMatrix(5, YearCol, FCConvert.ToString(rsType.Get_Fields_Boolean("Year5")));
						vsReceipt.TextMatrix(5, DefaultCol, FCConvert.ToString(rsType.Get_Fields_Double("DefaultAmount5")));
						intErr = 29;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee5")))
						{
							vsReceipt.TextMatrix(5, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(5, PercentCol, FCConvert.ToString(0));
						}
						intErr = 30;
						if (Strings.Left(vsReceipt.TextMatrix(5, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(5, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 31;
						vsReceipt.TextMatrix(6, CategoryCol, FCConvert.ToString(6));
						vsReceipt.TextMatrix(6, TitleCol, FCConvert.ToString(rsType.Get_Fields_String("Title6")));
						vsReceipt.TextMatrix(6, AbbrevCol, FCConvert.ToString(rsType.Get_Fields_String("Title6Abbrev")));
						vsReceipt.TextMatrix(6, AccountCol, FCConvert.ToString(rsType.Get_Fields_String("Account6")));
						vsReceipt.TextMatrix(6, YearCol, FCConvert.ToString(rsType.Get_Fields_Boolean("Year6")));
						vsReceipt.TextMatrix(6, DefaultCol, FCConvert.ToString(rsType.Get_Fields_Double("DefaultAmount6")));
						intErr = 32;
						if (FCConvert.ToBoolean(rsType.Get_Fields_Boolean("PercentageFee6")))
						{
							vsReceipt.TextMatrix(6, PercentCol, FCConvert.ToString(-1));
						}
						else
						{
							vsReceipt.TextMatrix(6, PercentCol, FCConvert.ToString(0));
						}
						intErr = 33;
						if (Strings.Left(vsReceipt.TextMatrix(6, AccountCol), 1) != "G" && Strings.Left(vsReceipt.TextMatrix(6, AccountCol), 1) != "M")
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, Information.RGB(255, 255, 255));
						}
						intErr = 34;
						txtDefaultAccount.TextMatrix(0, 0, FCConvert.ToString(rsType.Get_Fields_String("DefaultAccount")));
						if ((intTypeCode >= 90 && intTypeCode <= 92) || (intTypeCode == 890) || (intTypeCode == 891))
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, Color.White);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, Color.White);
						}
						else
						{
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 2, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 3, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 5, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
							vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 6, YearCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
						}
						intCurCode = intTypeCode;
						cmbCode.TextMatrix(0, 0, FCConvert.ToString(intTypeCode));
						// For intCT = 0 To cmbType.ListCount - 1
						// If Val(Left(cmbType.List(cmbType.ListIndex), 3)) = intTypeCode Then
						// boolDoNotChangeType = True
						// cmbType.ListIndex = intCT
						// boolDoNotChangeType = False
						// End If
						// Next
						intErr = 35;
						// disable the boxes that the user should not be touching
						ColorGrid_2(true);
					}
					else
					{
					}
					boolDirty = false;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Type - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmICAccountSetup_Resize(object sender, System.EventArgs e)
		{
			if (fraNewType.Visible == true)
			{
				ShowFrame(ref fraNewType);
			}
			else
			{
				// ShowFrame fraDefaultAccount
				ShowFrame(ref fraReceiptList);
			}
			//vsReceipt.Height = (vsReceipt.RowHeight(0) * vsReceipt.Rows) + 70;
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (boolDirty)
			{
				if (Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3)) != 0)
				{
					intTemp = MessageBox.Show("Do you want to save Code " + Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3) + "?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					// If Val(cmbCode.ComboItem(cmbCode.ComboIndex)) <> 0 Then
					// intTemp = MsgBox("Do you want to save Code " & cmbCode.ComboItem(cmbCode.ComboIndex) & "?", vbYesNoCancel + vbQuestion, "Save Changes")
					if (intTemp == DialogResult.Yes)
					{
						// yes
						// save it and exit
						SaveCode();
					}
					else if (intTemp == DialogResult.No)
					{
						// no
						// exit w/o saving
					}
					else if (intTemp == DialogResult.Cancel)
					{
						// cancel
						// go back to the screen and
						e.Cancel = true;
						return;
					}
				}
			}
			boolLoaded = false;
			//MDIParent.InstancePtr.Show();
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int i;
			int intCode = 0;
			bool boolNoSave = false;
			// this will make sure the validation of the alt cash account is done
			if (Strings.Trim(txtDefaultAccount.TextMatrix(0, 0)) != "" && Strings.Left(txtDefaultAccount.TextMatrix(0, 0), 1) != "M" && Strings.InStr(1, txtDefaultAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
			{
				txtDefaultAccount_Validating(sender, new System.ComponentModel.CancelEventArgs());
			}
			if (!boolNoSave)
			{
				intCode = intCurCode;
				// saves the current Type Code
				// MAL@20071107: Add successful message
				// SaveCode
				if (SaveCode())
				{
					MessageBox.Show("Save Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// set the combo box to the last code
					for (i = 0; i <= cmbType.Items.Count - 1; i++)
					{
						if (intCode == Conversion.Val(Strings.Left(cmbType.Items[i].ToString(), 3)))
						{
							intCurCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[i].ToString(), 3))));
							cmbType.SelectedIndex = i;
							break;
						}
					}
				}
				else
				{
					MessageBox.Show("This type was not saved.", "Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				MessageBox.Show("This type was not saved.", "Fund Mismatch", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				if (txtDefaultAccount.Enabled && txtDefaultAccount.Visible)
				{
					txtDefaultAccount.Focus();
				}
			}
		}

		private void mnuProcessSaveExit_Click(object sender, System.EventArgs e)
		{
			int intTempCode = 0;
			int intCT;
			bool boolNoSave = false;
			// this will make sure the validation of the alt cash account is done
			if (Strings.Trim(txtDefaultAccount.TextMatrix(0, 0)) != "" && Strings.Left(txtDefaultAccount.TextMatrix(0, 0), 1) != "M" && Strings.InStr(1, txtDefaultAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0)
			{
				txtDefaultAccount_Validating(sender, new System.ComponentModel.CancelEventArgs());
			}
			if (!boolNoSave)
			{
				intTempCode = intCurCode;
				if (SaveCode())
				{
					// this will check to see if the code is still in the combobox, if it is then it will set
					for (intCT = 0; intCT <= cmbType.Items.Count - 1; intCT++)
					{
						if (intTempCode == Conversion.Val(Strings.Left(cmbType.Items[intCT].ToString(), 3)))
						{
							intCurCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[intCT].ToString(), 3))));
							break;
						}
						else
						{
							intCurCode = -1;
						}
					}
					vsReceipt.EditText = "";
					// this will stop the edittext from bleeding into the next type
					//Application.DoEvents();
					MoveForward();
				}
				else
				{
					MessageBox.Show("This type was not saved correctly.", "Unsuccessful Save", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			else
			{
				MessageBox.Show("This type was not saved.", "Fund Mismatch", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				if (txtDefaultAccount.Enabled && txtDefaultAccount.Visible)
				{
					txtDefaultAccount.Focus();
				}
			}
			// cmbCode.Cell(FCGrid.CellPropertySettings.flexcpText, 0, 0) = cmbCode.ComboItem((cmbCode.ComboIndex + 1) Mod cmbCode.ComboCount)
		}

		private void txtCode_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void txtCode_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// this will not allow characters into the textbox '8,13
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
			{
				// the numbers, backspace and return
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillCodeCombo()
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill all of the types into cmbType
				int Number = 0;
				string Name = "";
				string strTemp = "";
				clsDRWrapper rsTemp = new clsDRWrapper();
				int intCT = 0;
				string[] strTypeDescriptions = new string[999 + 1];
				rsTemp.DefaultDB = modExtraModules.strUTDatabase;
				cmbType.Clear();
				FCUtils.EraseSafe(strTypeDescriptions);
				// Only load types that apply to town's service
				rsTemp.OpenRecordset("SELECT * FROM tblIConnectAccountInfo WHERE Active = 1 ORDER BY TypeCode");
				if (rsTemp.RecordCount() > 0)
				{
					intCurCode = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("TypeCode"));
					do
					{
						// this fills all of the types into the combobox
						Number = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("TypeCode"));
						Name = FCConvert.ToString(rsTemp.Get_Fields_String("TypeTitle"));
						strTypeDescriptions[Number] = Name;
						strTemp = modMain.PadToString_6(ref Number, 3) + " - " + Name;
						cmbType.AddItem(strTemp);
						intCT += 1;
						rsTemp.MoveNext();
					}
					while (!rsTemp.EndOfFile());
				}
				else
				{
					MessageBox.Show("No types were loaded.", "No Receipt Types", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fill Combo Error - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtDefaultAccount_Change()
		{
			boolDirty = true;
		}

		private void txtDefaultAccount_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void txtDefaultAccount_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtDefaultAccount.TextMatrix(0, 0));
				if (strAcct != "")
				{
					txtDefaultAccount.TextMatrix(0, 0, strAcct);
				}
			}
		}

		private void txtDefaultAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// this will check to see if all of the accounts for this type are from the same fund as this account
			string strAcct;
			int lngFund = 0;
			int intCT;
			strAcct = txtDefaultAccount.TextMatrix(0, 0);
			if (Strings.Trim(strAcct + " ") != "" && Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) == 0)
			{
				lngFund = modBudgetaryAccounting.GetFundFromAccount(strAcct, true);
				if (lngFund > 0)
				{
					for (intCT = 1; intCT <= 6; intCT++)
					{
						if (vsReceipt.TextMatrix(intCT, AccountCol) != "")
						{
							if (lngFund != modBudgetaryAccounting.GetFundFromAccount(vsReceipt.TextMatrix(intCT, AccountCol), true))
							{
								e.Cancel = true;
								MessageBox.Show("All of the accounts in category 1 - 6 must be from the same fund as the Alternate Cash Account.", "Incorrect Fund", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								break;
							}
						}
					}
				}
				else if (lngFund == -1)
				{
					// this is a bad account so blank the acct box out
					MessageBox.Show("Invalid account or the system could not find the fund for this account.", "Bad Account / Missing Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					e.Cancel = true;
					txtDefaultAccount.TextMatrix(0, 0, "");
				}
			}
			else
			{
				// empty let it slide
				txtDefaultAccount.TextMatrix(0, 0, "");
			}
		}

		private void txtTitle_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtTitle_Enter(object sender, System.EventArgs e)
		{
			boolAccountBox = false;
		}

		private void vsReceipt_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsReceipt.Col == AccountCol)
			{
				vsReceipt.EditMaxLength = 23;
			}
		}

		private void vsReceipt_ChangeEdit(object sender, System.EventArgs e)
		{
            if (vsReceipt.IsCurrentCellInEditMode)
            {
                boolDirty = true;
                if (vsReceipt.Col == AccountCol)
                {
                    // if this is the account col then
                    if (modGlobalConstants.Statics.gboolBD)
                    {
                        lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsReceipt.EditText);
                    }
                }
            }
		}

		private void vsReceipt_ClickEvent(object sender, System.EventArgs e)
		{
			int lngRow = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCode = 0;
				// If cmbCode.ComboIndex <> -1 Then
				// intCode = Val(Left$(cmbCode.ComboItem(cmbCode.ComboIndex), 3))
				// End If
				if (cmbType.SelectedIndex != -1)
				{
					intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
				}
				lngRow = vsReceipt.Row;
				if (!RestrictedCode())
				{
					if (vsReceipt.Col == TitleCol)
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
					}
					else if (vsReceipt.Col == AbbrevCol)
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
					}
					else if (vsReceipt.Col == AccountCol)
					{
						// account box row
					}
					else if (vsReceipt.Col == YearCol)
					{
					}
					else if (vsReceipt.Col == DefaultCol)
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsReceipt.EditMaxLength = 15;
						vsReceipt.EditCell();
					}
					else if (vsReceipt.Col == ProjCol)
					{
					}
					else if (vsReceipt.Col == PercentCol)
					{
						if (FCConvert.ToDouble(vsReceipt.TextMatrix(lngRow, PercentCol)) == -1)
						{
							vsReceipt.TextMatrix(lngRow, PercentCol, FCConvert.ToString(0));
						}
						else
						{
							vsReceipt.TextMatrix(lngRow, PercentCol, FCConvert.ToString(-1));
						}
						// vsReceipt.Editable = True
					}
					else
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				else
				{
					if (vsReceipt.Col == YearCol)
					{
						if (Strings.Left(vsReceipt.TextMatrix(lngRow, AccountCol), 1) == "G")
						{
							if (lngRow == 1 && (intCode == 90 || intCode == 91 || intCode == 92 || intCode == 890 || intCode == 891))
							{
								// always make the pricinpal of a RE or PP receipt selected
								vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(-1));
							}
							else
							{
								if (Conversion.Val(vsReceipt.TextMatrix(lngRow, vsReceipt.Col)) == -1)
								{
									vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(0));
								}
								else
								{
									vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(-1));
								}
							}
						}
						else if (Strings.Left(vsReceipt.TextMatrix(lngRow, AccountCol), 1) == "M" && lngRow == 1 && (intCode == 90 || intCode == 91 || intCode == 92 || intCode == 890 || intCode == 891))
						{
							if (FCConvert.ToDouble(vsReceipt.TextMatrix(lngRow, vsReceipt.Col)) == -1)
							{
								vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(0));
							}
							else
							{
								vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(-1));
							}
						}
						else
						{
							vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(0));
						}
						// If Left$(vsReceipt.TextMatrix(lngRow, 3), 1) = "G" Then
						// vsReceipt.Editable = True
						// Else
						// vsReceipt.Editable = False
						// If vsReceipt.TextMatrix(lngRow, 4) = True Then vsReceipt.TextMatrix(lngRow, 4) = False
						// End If
					}
					else if (vsReceipt.Col == AccountCol)
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						// vsReceipt.Editable = False
						// If Trim(vsReceipt.TextMatrix(lngRow, vsReceipt.Col) & " ") <> "" Then
						// txtAcct(lngRow).Text = vsReceipt.TextMatrix(lngRow, vsReceipt.Col)
						// Else
						// txtAcct(lngRow).Text = txtAcct(lngRow).Default
						// End If
						// txtAcct(lngRow).Left = vsReceipt.Left + vsReceipt.ColWidth(0) + vsReceipt.ColWidth(1) + vsReceipt.ColWidth(2)
						// txtAcct(lngRow).Top = vsReceipt.Top + (lngRow * vsReceipt.RowHeight(0))
						// txtAcct(lngRow).Tag = lngRow
						// txtAcct(lngRow).Visible = True
						// txtAcct(lngRow).SetFocus
					}
					else
					{
						vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Grid Click Error - " + FCConvert.ToString(lngRow), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsReceipt_Enter(object sender, System.EventArgs e)
		{
			int lngRow;
			// this is the row that was clicked on
			lngRow = vsReceipt.Row;
			// txtAcct(6).Visible = False
			// txtAcct(1).Visible = False
			// txtAcct(2).Visible = False
			// txtAcct(3).Visible = False
			// txtAcct(4).Visible = False
			// txtAcct(5).Visible = False
			if (!boolAccountBox)
			{
				// vsReceipt.Select 1, 1
				vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			if (RestrictedCode())
			{
				if (vsReceipt.Col == DefaultCol)
				{
					vsReceipt.EditCell();
				}
				// If vsReceipt.Col = 3 Then
				// SetGridFormat vsReceipt, vsReceipt.Row, vsReceipt.Col, False
				// End If
			}
			else
			{
				// vsReceipt.EditCell
			}
			boolAccountBox = false;
		}

		private void vsReceipt_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (vsReceipt.Col == DefaultCol && e.KeyCode == Keys.Left)
			{
				boolLeftArrow = true;
			}
			else
			{
				boolLeftArrow = false;
			}
			if (vsReceipt.Col == YearCol && FCConvert.ToInt32(e.KeyCode) == 32)
			{
				vsReceipt_ClickEvent(sender, new EventArgs());
			}
		}

		private void vsReceipt_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (vsReceipt.Col == DefaultCol)
			{
				if (RestrictedCode())
				{
					// no default values set for restriced types
					KeyAscii = 0;
				}
				else
				{
					if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii == 46) || (KeyAscii == 45))
					{
					}
					else
					{
						KeyAscii = 0;
					}
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsReceipt_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCode = 0;
				int lngRow;
				int lngCol;
				lngRow = vsReceipt.Row;
				lngCol = vsReceipt.Col;
				if (!boolChangingRows)
				{
					boolChangingRows = true;
					if (cmbType.SelectedIndex != -1)
					{
						intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
					}
					if (RestrictedCode())
					{
						// show the account description in the label at the bottom of the page
						if (Strings.Trim(vsReceipt.TextMatrix(lngRow, AccountCol)) != "")
						{
							if (Strings.InStr(1, vsReceipt.TextMatrix(lngRow, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.InStr(1, vsReceipt.TextMatrix(lngRow, AccountCol), "--", CompareConstants.vbBinaryCompare) == 0 && Strings.Right(vsReceipt.TextMatrix(lngRow, AccountCol), 1) != "-")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsReceipt.TextMatrix(lngRow, AccountCol));
								}
							}
							else
							{
								lblAccountTitle.Text = "";
							}
						}
						else
						{
							lblAccountTitle.Text = "";
						}
						if (vsReceipt.Col == CategoryCol || vsReceipt.Col == TitleCol || vsReceipt.Col == AbbrevCol)
						{
							vsReceipt.Col = 3;
						}
						else if (vsReceipt.Col == AccountCol)
						{
							if (lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
							// SetGridFormat vsReceipt, vsReceipt.Row, vsReceipt.Col, True
							// .Editable = False
							// txtAcct(lngRow).Text = .TextMatrix(lngRow, .Col)
							// txtAcct(lngRow).Left = .Left + .ColWidth(0) + .ColWidth(1) + .ColWidth(2)
							// txtAcct(lngRow).Top = .Top + (lngRow * .RowHeight(0))
							// txtAcct(lngRow).Tag = lngRow
							// txtAcct(lngRow).Visible = True
							// txtAcct(lngRow).Enabled = True
							// txtAcct(lngRow).SetFocus
						}
						else if (vsReceipt.Col == YearCol)
						{
							// .Editable = True
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							if (lngRow == 1 && (intCode == 90 || intCode == 91 || intCode == 92 || intCode == 890 || intCode == 891))
							{
								// always make the pricinpal of a RE or PP receipt selected
								vsReceipt.TextMatrix(lngRow, vsReceipt.Col, FCConvert.ToString(-1));
							}
							// If Left$(.TextMatrix(lngRow, 3), 1) = "G" Then
							// .Editable = flexEDKbdMouse
							// .EditCell
							// Else
							// .Editable = False
							// If .TextMatrix(lngRow, 4) <> "" Then
							// If .TextMatrix(lngRow, 4) = True Then .TextMatrix(lngRow, 4) = False
							// End If
							// If boolLeftArrow Then
							// .Col = 3
							// Else
							// .Col = 5
							// .Editable = flexEDKbdMouse
							// .EditCell
							// End If
							// End If
						}
						else if (vsReceipt.Col == DefaultCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							// .EditMaxLength = 15
							// .EditCell
						}
						else if (vsReceipt.Col == ProjCol)
						{
							if (vsReceipt.ColHidden(ProjCol) == false)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else if (vsReceipt.Col == PercentCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						if (vsReceipt.Col == vsReceipt.RightCol - 1 && lngRow == vsReceipt.BottomRow)
						{
							vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
						}
						else
						{
							vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
						}
						if (vsReceipt.Col == AccountCol)
						{
							// If txtAcct(lngRow).Visible Then
							// txtAcct(lngRow).SetFocus
							// End If
						}
					}
					else
					{
						// show the account description in the label at the bottom of the page
						if (Strings.Trim(vsReceipt.TextMatrix(lngRow, AccountCol)) != "")
						{
							if (Strings.InStr(1, vsReceipt.TextMatrix(lngRow, AccountCol), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.InStr(1, vsReceipt.TextMatrix(lngRow, AccountCol), "--", CompareConstants.vbBinaryCompare) == 0 && Strings.Right(vsReceipt.TextMatrix(lngRow, AccountCol), 1) != "-")
							{
								if (modGlobalConstants.Statics.gboolBD)
								{
									lblAccountTitle.Text = modAccountTitle.ReturnAccountDescription(vsReceipt.TextMatrix(lngRow, AccountCol));
								}
							}
							else
							{
								lblAccountTitle.Text = "";
							}
						}
						else
						{
							lblAccountTitle.Text = "";
						}
						if (vsReceipt.Col == TitleCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						else if (vsReceipt.Col == AbbrevCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						else if (vsReceipt.Col == AccountCol)
						{
							if (lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								modNewAccountBox.SetGridFormat(vsReceipt, vsReceipt.Row, vsReceipt.Col, true);
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else if (vsReceipt.Col == YearCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						else if (vsReceipt.Col == DefaultCol)
						{
							if (lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsReceipt.EditMaxLength = 15;
								vsReceipt.EditCell();
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else if (vsReceipt.Col == ProjCol)
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						else if (vsReceipt.Col == PercentCol)
						{
							if (lngRow > 0)
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							}
							else
							{
								vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
							}
						}
						else
						{
							vsReceipt.Editable = FCGrid.EditableSettings.flexEDNone;
						}
						// If .Col = 3 Then
						// If txtAcct(lngRow).Visible Then
						// txtAcct(lngRow).SetFocus
						// End If
						// End If
					}
					if (vsReceipt.Col >= vsReceipt.RightCol - 1 && lngRow == vsReceipt.BottomRow)
					{
						vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						vsReceipt.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
					}
					boolChangingRows = false;
				}
				else
				{
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Validation Error (Row Column Change)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowFrame(ref FCPanel fraFrame)
		{
			// this will place the frame in the middle of the form
			//fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			//fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private bool SaveCode(int lngForcedResCode = 0)
		{
			bool SaveCode = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will save all of the information, as long as it has a title
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsMultiSave = new clsDRWrapper();
				clsDRWrapper rsRb = new clsDRWrapper();
				int intCT;
				int lngResCode;
				bool[] boolYear = new bool[7 + 1];
				SaveCode = true;
				boolSaving = true;
				if (cmbType.Visible)
				{
					cmbType.Focus();
				}
				boolSaving = false;
				if (Strings.Trim(txtTitle.Text) == "")
				{
					MessageBox.Show("Please enter a valid Title.", "Missing Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					SaveCode = false;
					if (txtTitle.Enabled && txtTitle.Visible)
					{
						txtTitle.Focus();
					}
					return SaveCode;
				}
				if (!CheckPercentages())
				{
					MessageBox.Show("The percentages entered do not add up to 100%.", "Incorrect Percentage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					SaveCode = false;
					return SaveCode;
				}
				if (Strings.Trim(txtTitle.Text) != "")
				{
					rsSave.OpenRecordset("SELECT * FROM tblIConnectAccountInfo WHERE TypeCode = " + FCConvert.ToString(intCurCode), modExtraModules.strUTDatabase);
					if (rsSave.BeginningOfFile() != true && rsSave.EndOfFile() != true)
					{
						rsSave.Edit();
					}
					else
					{
						if (lngForcedResCode != 0)
						{
							rsSave.AddNew();
							rsSave.Set_Fields("TypeCode", intCurCode);
						}
						else
						{
							switch (MessageBox.Show("Type Code not found would you like to add it?", "New Receipt Type", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
							{
								case DialogResult.No:
									{
										SaveCode = false;
										return SaveCode;
									}
								case DialogResult.Yes:
									{
										rsSave.AddNew();
										rsSave.Set_Fields("TypeCode", intCurCode);
										break;
									}
							}
							//end switch
						}
					}
					rsSave.Set_Fields("TypeTitle", Strings.Trim(txtTitle.Text));
					if (Strings.Trim(vsReceipt.TextMatrix(1, TitleCol)) != "")
					{
						// And .TextMatrix(1, 3) <> "" Then
						if (!CheckStandardAccountType_2(Strings.Trim(vsReceipt.TextMatrix(1, AccountCol))))
						{
							MessageBox.Show("Invalid standard account number for fee 1.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
						rsSave.Set_Fields("Title1", vsReceipt.TextMatrix(1, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(1, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title1Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(1, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title1Abbrev", vsReceipt.TextMatrix(1, AbbrevCol));
						}
						rsSave.Set_Fields("Account1", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(1, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(1, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount1", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(1, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount1", 0);
						}
						boolYear[1] = !FCConvert.CBool(rsSave.Get_Fields_Boolean("Year1") == FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(1, YearCol)) == true));
						rsSave.Set_Fields("Year1", FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(1, YearCol)) == true));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account1")))
						{
							MessageBox.Show("Invalid account number for fee 1.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Account1", "");
						rsSave.Set_Fields("DefaultAmount1", 0);
						rsSave.Set_Fields("Year1", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(2, TitleCol)) != "")
					{
						// And .TextMatrix(2, 3) <> "" Then
						rsSave.Set_Fields("Title2", vsReceipt.TextMatrix(2, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(2, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title2Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(2, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title2Abbrev", vsReceipt.TextMatrix(2, AbbrevCol));
						}
						rsSave.Set_Fields("Account2", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(2, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(2, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount2", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(2, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount2", 0);
						}
						boolYear[2] = !FCConvert.CBool(rsSave.Get_Fields_Boolean("Year2") == FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(2, YearCol)) == true));
						rsSave.Set_Fields("Year2", FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(2, YearCol)) == true));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account2")))
						{
							MessageBox.Show("Invalid account number for fee 2.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Account2", "");
						rsSave.Set_Fields("DefaultAmount2", 0);
						rsSave.Set_Fields("Year2", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(3, TitleCol)) != "")
					{
						// And .TextMatrix(3, 3) <> "" Then
						rsSave.Set_Fields("Title3", vsReceipt.TextMatrix(3, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(3, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title3Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(3, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title3Abbrev", vsReceipt.TextMatrix(3, AbbrevCol));
						}
						boolYear[3] = !FCConvert.CBool(rsSave.Get_Fields_Boolean("Year3") == FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(3, YearCol)) == true));
						rsSave.Set_Fields("Account3", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(3, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(3, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount3", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(3, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount3", 0);
						}
						rsSave.Set_Fields("Year3", FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(3, YearCol)) == true));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account3")))
						{
							MessageBox.Show("Invalid account number for fee 3.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Account3", "");
						rsSave.Set_Fields("DefaultAmount3", 0);
						rsSave.Set_Fields("Year3", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(4, TitleCol)) != "")
					{
						// And .TextMatrix(4, 3) <> "" Then
						rsSave.Set_Fields("Title4", vsReceipt.TextMatrix(4, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(4, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title4Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(4, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title4Abbrev", vsReceipt.TextMatrix(4, AbbrevCol));
						}
						rsSave.Set_Fields("Account4", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(4, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(4, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount4", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(4, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount4", 0);
						}
						boolYear[4] = !FCConvert.CBool(rsSave.Get_Fields_Boolean("Year4") == FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(4, YearCol)) == true));
						rsSave.Set_Fields("Year4", FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(4, YearCol)) == true));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account4")))
						{
							MessageBox.Show("Invalid account number for fee 4.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Account4", "");
						rsSave.Set_Fields("DefaultAmount4", 0);
						rsSave.Set_Fields("Year4", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(5, TitleCol)) != "")
					{
						// And .TextMatrix(5, 3) <> "" Then
						rsSave.Set_Fields("Title5", vsReceipt.TextMatrix(5, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(5, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title5Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(5, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title5Abbrev", vsReceipt.TextMatrix(5, AbbrevCol));
						}
						rsSave.Set_Fields("Account5", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(5, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(5, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount5", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(5, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount5", 0);
						}
						boolYear[5] = !FCConvert.CBool(rsSave.Get_Fields_Boolean("Year5") == FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(5, YearCol)) == true));
						rsSave.Set_Fields("Year5", FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(5, YearCol)) == true));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account5")))
						{
							MessageBox.Show("Invalid account number for fee 5.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Account5", "");
						rsSave.Set_Fields("DefaultAmount5", 0);
						rsSave.Set_Fields("Year5", false);
					}
					if (Strings.Trim(vsReceipt.TextMatrix(6, TitleCol)) != "")
					{
						// And .TextMatrix(6, 3) <> "" Then
						rsSave.Set_Fields("Title6", vsReceipt.TextMatrix(6, TitleCol));
						if (Strings.Trim(vsReceipt.TextMatrix(6, AbbrevCol)) == "")
						{
							rsSave.Set_Fields("Title6Abbrev", Strings.Trim(Strings.Left(vsReceipt.TextMatrix(6, TitleCol) + "        ", 8)));
						}
						else
						{
							rsSave.Set_Fields("Title6Abbrev", vsReceipt.TextMatrix(6, AbbrevCol));
						}
						rsSave.Set_Fields("Account6", Strings.Trim(RemoveUnderscore(vsReceipt.TextMatrix(6, AccountCol))));
						if (Conversion.Val(vsReceipt.TextMatrix(6, DefaultCol)) != 0)
						{
							rsSave.Set_Fields("DefaultAmount6", FCUtils.Round(FCConvert.ToDouble(vsReceipt.TextMatrix(6, DefaultCol)), 2));
						}
						else
						{
							rsSave.Set_Fields("DefaultAmount6", 0);
						}
						boolYear[6] = !FCConvert.CBool(rsSave.Get_Fields_Boolean("Year6") == FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(6, YearCol)) == true));
						rsSave.Set_Fields("Year6", FCConvert.CBool(FCConvert.CBool(vsReceipt.TextMatrix(6, YearCol)) == true));
						if (!modValidateAccount.AccountValidate(rsSave.Get_Fields_String("Account6")))
						{
							MessageBox.Show("Invalid account number for fee 6.  You will not be able to use this fee until this is corrected.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SaveCode = false;
							return SaveCode;
						}
					}
					else
					{
						rsSave.Set_Fields("Account6", "");
						rsSave.Set_Fields("DefaultAmount6", 0);
						rsSave.Set_Fields("Year6", false);
					}
					if (boolYear[1] || boolYear[2] || boolYear[3] || boolYear[4] || boolYear[5] || boolYear[6])
					{
						modGlobalFunctions.AddCYAEntry_728("CR", "Changing the year flag in Type:" + FCConvert.ToString(intCurCode), " 1 : " + FCConvert.ToString(boolYear[1]) + " - " + " 2 : " + FCConvert.ToString(boolYear[2]), "3 : " + FCConvert.ToString(boolYear[3]) + " 4 : " + FCConvert.ToString(boolYear[4]), "5 : " + FCConvert.ToString(boolYear[5]) + " 6 : " + FCConvert.ToString(boolYear[6]), "Values : " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year1"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year2"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year3"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year4"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year5"))) + " " + FCConvert.ToString(FCConvert.CBool(rsSave.Get_Fields_Boolean("Year6"))));
					}
					if (txtDefaultAccount.TextMatrix(0, 0) != "" && Strings.InStr(1, txtDefaultAccount.TextMatrix(0, 0), "_", CompareConstants.vbBinaryCompare) == 0 && Strings.Trim(txtDefaultAccount.TextMatrix(0, 0)) != "M")
					{
						rsSave.Set_Fields("DefaultAccount", txtDefaultAccount.TextMatrix(0, 0));
					}
					else
					{
						rsSave.Set_Fields("DefaultAccount", "");
					}
					if (rsSave.Update())
					{
						boolDirty = false;
					}
				}
				//Application.DoEvents();
				FillCodeCombo();
				if (txtCode.Visible && txtCode.Enabled)
				{
					txtCode.Focus();
				}
				return SaveCode;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Code", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveCode;
		}

		private void MoveBack()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				CheckForSave();
				if (cmbType.SelectedIndex <= 0)
				{
					// it is at the beginning
					cmbType.SelectedIndex = cmbType.Items.Count - 1;
				}
				else
				{
					cmbType.SelectedIndex = cmbType.SelectedIndex - 1;
				}
				intCurCode = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbType.Items[cmbType.SelectedIndex].ToString())));
				if (txtTitle.Enabled && txtTitle.Visible)
				{
					txtTitle.Focus();
				}
				else if (cmbType.Visible && cmbType.Enabled)
				{
					cmbType.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Move Back", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void MoveForward()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intIndex;
				CheckForSave();
				// this will find the code index
				for (intIndex = 0; intIndex <= cmbType.Items.Count - 1; intIndex++)
				{
					if (intCurCode == Conversion.Val(Strings.Left(cmbType.Items[intIndex].ToString(), 3)))
					{
						break;
					}
				}
				if (intIndex < cmbType.Items.Count - 1)
				{
					// this will get the next index
					cmbType.SelectedIndex = intIndex + 1 % cmbType.Items.Count;
				}
				else if (cmbType.Items.Count > 0)
				{
					cmbType.SelectedIndex = 0;
				}
				else
				{
					cmbType.SelectedIndex = -1;
				}
				if (txtTitle.Enabled && txtTitle.Visible)
				{
					txtTitle.Focus();
				}
				else if (cmbType.Visible && cmbType.Enabled)
				{
					cmbType.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Move Forward", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckForSave()
		{
			bool CheckForSave = false;
			// vbPorter upgrade warning: intTemp As short, int --> As DialogResult
			DialogResult intTemp;
			CheckForSave = true;
			if (boolDirty)
			{
				if (Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3)) != 0)
				{
					if (intCurCode > 0)
					{
						intTemp = MessageBox.Show("Do you want to save the changes in Code " + FCConvert.ToString(intCurCode) + "?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
						if (intTemp == DialogResult.Yes)
						{
							// yes
							// save it and exit
							SaveCode();
						}
						else if (intTemp == DialogResult.No)
						{
							// no
							// do nothing
						}
					}
					else
					{
						// CheckForSave = False
					}
				}
				else
				{
				}
			}
			boolDirty = false;
			return CheckForSave;
		}

		private bool RestrictedCode(int lngCode = 0)
		{
			bool RestrictedCode = false;
			// this will return true if this is a restricted code
			int intTypeCode;
			if (cmbType.SelectedIndex != -1 || lngCode != 0)
			{
				// If cmbCode.ComboIndex <> -1 Then
				if (lngCode != 0)
				{
					intTypeCode = lngCode;
				}
				else
				{
					intTypeCode = FCConvert.ToInt16(intCurCode);
				}
				if ((intTypeCode >= 90 && intTypeCode <= 99) || (intTypeCode >= 190 && intTypeCode <= 199) || (intTypeCode >= 290 && intTypeCode <= 299) || (intTypeCode >= 390 && intTypeCode <= 399) || (intTypeCode >= 490 && intTypeCode <= 499) || (intTypeCode >= 590 && intTypeCode <= 599) || (intTypeCode >= 690 && intTypeCode <= 699) || (intTypeCode >= 790 && intTypeCode <= 799) || (intTypeCode >= 800 && intTypeCode <= 899) || (intTypeCode >= 900 && intTypeCode <= 999))
				{
					RestrictedCode = true;
				}
				else
				{
					RestrictedCode = false;
				}
			}
			else
			{
				RestrictedCode = true;
			}
			return RestrictedCode;
		}

		private void ProcessType()
		{
			int intTypeCode = 0;
			bool boolCancel;
			int intTemp;
			clsDRWrapper rsTemp;
			int lngResCode = 0;
			boolCancel = false;
			if (cmbType.SelectedIndex != -1)
			{
				intTypeCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
				if (CheckForSave())
				{
					intCurCode = intTypeCode;
					ShowType(intCurCode, lngResCode);
                    //FC:FINAL:AM: reset the dirty flag
                    boolDirty = false;
				}
			}
		}

		private bool CheckPercentages()
		{
			bool CheckPercentages = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return true if the percentage amounts add up to 100%
				// and return false otherwise
				// Make sure that all the values are in the grid rather than in the .edittext
				double dblPercent;
				int intRow;
				dblPercent = 0;
				for (intRow = 1; intRow <= 6; intRow++)
				{
					if (Conversion.Val(vsReceipt.TextMatrix(intRow, PercentCol)) == -1)
					{
						// if it is checked
						dblPercent += FCConvert.ToDouble(vsReceipt.TextMatrix(intRow, DefaultCol));
					}
					else
					{
						// next
					}
				}
				if (FCUtils.Round(dblPercent, 4) == 100 || !CheckPercentages)
				{
					CheckPercentages = true;
				}
				else
				{
					CheckPercentages = false;
				}
				return CheckPercentages;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Percentages", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckPercentages;
		}

		private void vsReceipt_StartEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// If (intCurCode = 90 Or intCurCode = 91 Or intCurCode = 890 Or intCurCode = 891) And Col = 4 And Row = 1 Then
			if (vsReceipt.Col == YearCol || vsReceipt.Col == PercentCol)
			{
				e.Cancel = true;
			}
		}

		private void vsReceipt_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsReceipt.GetFlexRowIndex(e.RowIndex);
			int col = vsReceipt.GetFlexColIndex(e.ColumnIndex);
			// this will make sure the format is correct
			if (col == AccountCol)
			{
				if (row == 1)
				{
					if (modGlobalConstants.Statics.gboolBD)
					{
						e.Cancel = !CheckStandardAccountType(vsReceipt.EditText, boolSaving);
						if (intCurCode == 90 || intCurCode == 91 || intCurCode == 890 || intCurCode == 891)
						{
							vsReceipt.TextMatrix(row, YearCol, FCConvert.ToString(true));
							// set the year flag
						}
					}
					else
					{
						if (FCConvert.CBool(Strings.Left(vsReceipt.EditText, 1) != "M"))
						{
							vsReceipt.EditText = "";
						}
					}
				}
			}
			else if (col == DefaultCol)
			{
				// This will check to see if the percentage col is checked
				// if so then format this to a long so the 100% is easier to
				// match instead of with decimals and rounding will be less of an issue
				if (Conversion.Val(vsReceipt.TextMatrix(row, PercentCol)) == -1)
				{
					vsReceipt.EditText = Strings.Format(vsReceipt.EditText, "#0.00");
				}
				else
				{
					vsReceipt.EditText = Strings.Format(vsReceipt.EditText, "#,##0.00");
				}
			}
		}

		private void ColorGrid_2(bool boolRestricted)
		{
			ColorGrid(ref boolRestricted);
		}

		private void ColorGrid(ref bool boolRestricted)
		{
			if (boolRestricted)
			{
				// color the grid with grayed out columns
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 6, AbbrevCol, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 6, AbbrevCol, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			}
			else
			{
				// set the grid back to white
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 1, 6, AbbrevCol, Color.White);
				vsReceipt.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 1, 1, 6, AbbrevCol, modGlobalConstants.Statics.TRIOCOLORBLACK);
			}
		}

		private string RemoveUnderscore(string strAcct)
		{
			string RemoveUnderscore = "";
			// this function will remove the underscores from any M account and will
			// return "" if there is any underscores for all other accounts
			if (Strings.Trim(strAcct) != "")
			{
				if (Strings.Left(strAcct, 1) != "M")
				{
					if (Strings.InStr(1, strAcct, "_", CompareConstants.vbBinaryCompare) != 0)
					{
						RemoveUnderscore = "";
					}
					else
					{
						RemoveUnderscore = strAcct;
					}
				}
				else
				{
					RemoveUnderscore = strAcct.Replace("_", "");
				}
			}
			else
			{
				RemoveUnderscore = "";
			}
			return RemoveUnderscore;
		}

		private bool CheckStandardAccountType_2(string strAccount, bool boolSave = false)
		{
			return CheckStandardAccountType(strAccount, boolSave);
		}

		private bool CheckStandardAccountType(string strAccount, bool boolSave = false)
		{
			bool CheckStandardAccountType = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intTypeCode = 0;
				int lngResCode;
				string strLedgerPiece = "";
				string strSALedgerPiece = "";
				clsDRWrapper rsBD = new clsDRWrapper();
				if (modGlobalConstants.Statics.gboolBD && !boolSave)
				{
					if (Strings.Left(strAccount, 1) == "G")
					{
						if (cmbType.SelectedIndex != -1)
						{
							intTypeCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbType.Items[cmbType.SelectedIndex].ToString(), 3))));
						}
						rsBD.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
						if (rsBD.EndOfFile() != true && rsBD.BeginningOfFile() != true)
						{
							switch (intTypeCode)
							{
								case 90:
									{
										// RE Payment
										rsBD.FindFirstRecord("Code", "RR");
										break;
									}
								case 91:
									{
										// Lien Payment
										rsBD.FindFirstRecord("Code", "LR");
										break;
									}
								case 92:
									{
										// PP Payment
										rsBD.FindFirstRecord("Code", "PR");
										break;
									}
								case 93:
									{
										// Water
										rsBD.FindFirstRecord("Code", "WR");
										break;
									}
								case 94:
									{
										// Sewer
										rsBD.FindFirstRecord("Code", "SR");
										break;
									}
								case 95:
									{
										rsBD.FindFirstRecord("Code", "SL");
										break;
									}
								case 96:
									{
										rsBD.FindFirstRecord("Code", "WL");
										break;
									}
								case 97:
									{
										rsBD.FindFirstRecord("Code", "SS");
										break;
									}
								case 890:
									{
										rsBD.FindFirstRecord("Code", "TR");
										break;
									}
								case 891:
									{
										rsBD.FindFirstRecord("Code", "LV");
										break;
									}
								default:
									{
										CheckStandardAccountType = true;
										return CheckStandardAccountType;
									}
							}
							//end switch
							if (!rsBD.NoMatch)
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								if (FCConvert.ToString(rsBD.Get_Fields("Account")) != "")
								{
									// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
									strSALedgerPiece = FCConvert.ToString(rsBD.Get_Fields("Account"));
								}
								else
								{
									strSALedgerPiece = "";
									MessageBox.Show("Error in Standard Accounts.  Please make sure that the Standard Account that you were looking for is setup in Budgetary.", "Standard Accounts Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
							else
							{
								strSALedgerPiece = "";
								MessageBox.Show("Error in Standard Accounts.  Please make sure that the Standard Account that you were looking for is setup in Budgetary.", "Standard Accounts Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							strLedgerPiece = GetLedgerAcctPiece(strAccount);
							if (strSALedgerPiece == strLedgerPiece)
							{
								CheckStandardAccountType = true;
							}
							else
							{
								MessageBox.Show("Please use the General Ledger Standard Account (" + strSALedgerPiece + ") for this field.", "Standard Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								CheckStandardAccountType = false;
							}
						}
						else
						{
							MessageBox.Show("Error in Standard Accounts.  Please make sure that the Standard Account that you were looking for is setup in Budgetary.", "Standard Accounts Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							CheckStandardAccountType = false;
						}
					}
					else if (Strings.Left(strAccount, 1) != "M")
					{
						if (intCurCode >= 90 && intCurCode <= 96)
						{
							MessageBox.Show("Please use a General Ledger account for this field.", "Standard Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							CheckStandardAccountType = false;
						}
						else
						{
							CheckStandardAccountType = true;
						}
					}
					else
					{
						CheckStandardAccountType = true;
					}
				}
				else
				{
					CheckStandardAccountType = true;
				}
				return CheckStandardAccountType;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Standard Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckStandardAccountType;
		}

		public string GetLedgerAcctPiece(string Acct)
		{
			string GetLedgerAcctPiece = "";
			if (Strings.Left(Acct, 1) == "G")
			{
				GetLedgerAcctPiece = Strings.Mid(Acct, 4 + FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))));
			}
			else
			{
				GetLedgerAcctPiece = "";
			}
			return GetLedgerAcctPiece;
		}

		private void mnuFileBack_Click(object sender, System.EventArgs e)
		{
			MoveBack();
		}

		private void mnuFileForward_Click(object sender, System.EventArgs e)
		{
			MoveForward();
		}
	}
}
