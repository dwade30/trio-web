﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReminderMailer.
	/// </summary>
	public partial class frmReminderMailer : BaseForm
	{
		public frmReminderMailer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.fraText = new System.Collections.Generic.List<fecherFoundation.FCFrame>();
			this.txtNotPastDue = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.fraText.AddControlArrayElement(fraText_0, 0);
			this.txtNotPastDue.AddControlArrayElement(txtNotPastDue_1, 1);
			this.txtNotPastDue.AddControlArrayElement(txtNotPastDue_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReminderMailer InstancePtr
		{
			get
			{
				return (frmReminderMailer)Sys.GetInstance(typeof(frmReminderMailer));
			}
		}

		protected frmReminderMailer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/01/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/02/2006              *
		// ********************************************************
		string strSQL;
		string strPayment;
		DateTime dtDate;
		string strYear;
		public bool boolWater;
		bool boolLienedRecords;

		public void Init(ref string strPassSQL, ref string strPassYear, string strPassPayment, DateTime dtPassDate, ref bool boolPassWater, ref bool boolPassLienedRecords)
		{
			// collect date fronm the last form
			strSQL = strPassSQL;
			strPayment = strPassPayment;
			dtDate = dtPassDate;
			strYear = strPassYear;
			boolWater = boolPassWater;
			boolLienedRecords = boolPassLienedRecords;
			// setup the labels
			SetupLabels();
			// show this form
			this.Show(App.MainForm);
		}

		private void frmReminderMailer_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuFileExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmReminderMailer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReminderMailer properties;
			//frmReminderMailer.FillStyle	= 0;
			//frmReminderMailer.ScaleWidth	= 9045;
			//frmReminderMailer.ScaleHeight	= 6930;
			//frmReminderMailer.LinkTopic	= "Form2";
			//frmReminderMailer.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strTemp = "";
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Reminder Notices - Mailer";
			// get this information from the registry to default the boxes
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "REMINDERMAILERNOTPAST1", ref strTemp);
			txtNotPastDue[0].Text = strTemp;
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "REMINDERMAILERNOTPAST2", ref strTemp);
			txtNotPastDue[1].Text = strTemp;
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "REMINDERMAILERPAST", ref strTemp);
		}

		private void frmReminderMailer_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			// this will clear the textboxes out
			txtNotPastDue[0].Text = "";
			txtNotPastDue[1].Text = "";
			// txtPastDue.Text = ""
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuFileExit_Click()
		{
			mnuFileExit_Click(mnuFileExit, new System.EventArgs());
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will call the report initialize routine
				string strNotMessage;
				string strMessage = "";
				string strPreMessage;
				frmWait.InstancePtr.Init("Please Wait...");
				strPreMessage = txtNotPastDue[0].Text;
				strNotMessage = txtNotPastDue[1].Text;
				// save this information into the registry
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "REMINDERMAILERNOTPAST1", strPreMessage);
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "REMINDERMAILERNOTPAST2", strNotMessage);
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "REMINDERMAILERPAST", strMessage);
				rptReminderMailer.InstancePtr.Init(ref strSQL, "******  Utility - Reminder Notice  ******", ref strPayment, ref strNotMessage, ref strMessage, ref strPreMessage, ref dtDate, FCConvert.CBool(chkShowPending.CheckState == Wisej.Web.CheckState.Checked), ref boolWater, ref boolLienedRecords);
				frmReportViewer.InstancePtr.Init(rptReminderMailer.InstancePtr);
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupLabels()
		{
			// this routine will make the labels look similar to what will be seen on the notice
			lblNotHeader.Text = "******  Sewer - Reminder Notice  ******";
			lblNotMessage.Text = "This is to remind you that your Utility bill is still due.";
			lblNotAmountDue.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   ###,###.##";
		}

		private void txtNotPastDue_KeyDown(short Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						if (Index == 0)
						{
							txtNotPastDue[1].Focus();
						}
						else
						{
						}
						break;
					}
			}
			//end switch
		}

		private void txtNotPastDue_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			short index = txtNotPastDue.GetIndex((FCTextBox)sender);
			txtNotPastDue_KeyDown(index, sender, e);
		}
	}
}
