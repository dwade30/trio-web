﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUpdateAddresses.
	/// </summary>
	public partial class frmUpdateAddresses : BaseForm
	{
		public frmUpdateAddresses()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUpdateAddresses InstancePtr
		{
			get
			{
				return (frmUpdateAddresses)Sys.GetInstance(typeof(frmUpdateAddresses));
			}
		}

		protected frmUpdateAddresses _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsPrev = new clsDRWrapper();
		bool boolLoaded;
		clsDRWrapper rsUT = new clsDRWrapper();
		int lngBillingYear;
		string strRateKey = "";
		DateTime datBillDate;
		int lngColCheckBox;
		int lngColAccount;
		int lngColName;
		int lngColAddress1;
		int lngColAddress2;
		int lngColBillKey;
		int lngColHidden;

		private void cboRateKeys_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboRateKeys.SelectedIndex >= 0)
			{
				if (!FillGrid())
				{
					// Unload Me
				}
			}
		}

		private void frmUpdateAddresses_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			if (!boolLoaded)
			{
				boolLoaded = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				this.Text = "Update Bill Addresses";
				lblValidateInstruction.Text = "If these account changes are correct, Press F12 to advance.";
				//Application.DoEvents();
				LoadRateKeys();
				ShowGridFrame();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			}
			this.Refresh();
		}

		private void frmUpdateAddresses_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUpdateAddresses properties;
			//frmUpdateAddresses.FillStyle	= 0;
			//frmUpdateAddresses.ScaleWidth	= 9045;
			//frmUpdateAddresses.ScaleHeight	= 7185;
			//frmUpdateAddresses.LinkTopic	= "Form2";
			//frmUpdateAddresses.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			lngColCheckBox = 0;
			lngColAccount = 1;
			lngColName = 2;
			lngColAddress1 = 3;
			lngColAddress2 = 4;
			lngColBillKey = 5;
			lngColHidden = 6;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FCUtils.EraseSafe(modUTLien.Statics.arrDemand);
		}

		private void frmUpdateAddresses_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileSelect_Click(object sender, System.EventArgs e)
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngColCheckBox, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelect_Click()
		{
			mnuFileSelect_Click(mnuFileSelect, new System.EventArgs());
		}

		private void mnuFileUnselect_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void LoadRateKeys()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				cboRateKeys.Clear();
				rsInfo.OpenRecordset("SELECT * FROM RateKeys WHERE RateType <> 'L' ORDER BY BillDate DESC");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						//FC:FINAL:CHN - issue #1199: Remove number in dropdown list.
						cboRateKeys.AddItem(Strings.Format(rsInfo.Get_Fields("RateKey"), "00000") + "  " + rsInfo.Get_Fields_String("RateType") + "   " + Strings.Format(rsInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "   " + rsInfo.Get_Fields_String("Description"));
						// cboRateKeys.AddItem(Strings.Format(rsInfo.Get_Fields_Int32("RateKey"), "00000") + "  " + rsInfo.Get_Fields_String("RateType") + "   " + Strings.Format(rsInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "   " + rsInfo.Get_Fields_String("Description"));
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				else
				{
					MessageBox.Show("No valid rate records to process.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Rate Keys", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmUpdateAddresses_Resize(object sender, System.EventArgs e)
		{
			FormatGrid_2(true);
			ShowGridFrame();
		}

		private void ShowGridFrame()
		{
			//int lngHeight;
			//int counter;
			// this will show/center the frame with the grid on it
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
			//lngHeight = 0;
			//for (counter = 0; counter <= vsDemand.Rows - 1; counter++)
			//{
			//    lngHeight += vsDemand.RowHeight(counter);
			//}
			//if (lngHeight > fraGrid.Height - lblValidateInstruction.Height - 500)
			//{
			//    vsDemand.Height = fraGrid.Height - lblValidateInstruction.Height - 500;
			//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsDemand.Height = lngHeight + 70;
			//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void FormatGrid_2(bool boolResize)
		{
			FormatGrid(boolResize);
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 7;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngColCheckBox, FCConvert.ToInt32(wid * 0.1));
			// Checkbox
			vsDemand.ColWidth(lngColAccount, FCConvert.ToInt32(wid * 0.1));
			// Acct
			vsDemand.ColWidth(lngColName, FCConvert.ToInt32(wid * 0.25));
			// Name
			vsDemand.ColWidth(lngColAddress1, FCConvert.ToInt32(wid * 0.25));
			// Bill Address
			vsDemand.ColWidth(lngColAddress2, FCConvert.ToInt32(wid * 0.25));
			// Master Address
			vsDemand.ColWidth(lngColBillKey, 0);
			// Hidden Key Field (Bill Key)
			vsDemand.ColWidth(lngColHidden, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColDataType(lngColCheckBox, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColAddress1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColAddress2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.TextMatrix(0, lngColAccount, "Account");
			vsDemand.TextMatrix(0, lngColName, "Name");
			vsDemand.TextMatrix(0, lngColAddress1, "Bill Address");
			vsDemand.TextMatrix(0, lngColAddress2, "Master Address");
		}

		private bool FillGrid()
		{
			bool FillGrid = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL;
				int lngIndex;
				double dblXInt = 0;
				int lngYear;
				string strTemp = "";
				string strOldAddress = "";
				string strNewAddress = "";
				int lngHeight;
				int counter;
				lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboRateKeys.Text, 5))));
				datBillDate = DateAndTime.DateValue(Strings.Mid(cboRateKeys.Text, 12, 10));
				FillGrid = true;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				vsDemand.Rows = 1;
				// This function will go through any accounts that have had an address change
				// since the last sale or commitment and then update the address on the last bill
				// strSQL = "SELECT * FROM BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount WHERE Name1 = RSName AND BillingType = 'RE' AND LienRecordNumber = 0 AND (Address1 <> RSAddr1 OR Address2 <> RSAddr2 OR Left(Address3, 3) <> Left(RSAddr3, 3)) AND (TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) > 0 AND RateKey > 0 AND RSCard = 1 AND BillingYear = " & lngYear & " ORDER BY Account"   ' OR Address3 <> RSAddr3
				strSQL = "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.Key WHERE OName <> OwnerName AND WLienRecordNumber = 0 AND SLienRecordNumber = 0 AND (SPrinOwed + WPrinOwed - SPrinPaid - WPrinPaid) > 0 AND BillNumber = " + FCConvert.ToString(lngYear) + " ORDER BY AccountNumber";
				// AND (Bill.BAddress1 <> Master.BAddress1  OR Bill.BAddress2 <> Master.BAddress2 OR Bill.BAddress3 <> Master.BAddress3 OR Bill.BCity <> Master.BCity OR Bill.BState <> Master.BState OR Bill.BZip <> Master.BZip OR Bill.BZip4 <> Master.BZip4)
				// this is a list of account from non liened records that have a different address
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				rsUT.OpenRecordset("SELECT * FROM Master");
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("There are no accounts eligible to have addresses updated.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FillGrid = false;
					return FillGrid;
				}
				lngIndex = -1;
				while (!rsData.EndOfFile())
				{
					//Application.DoEvents();
					dblXInt = 0;
					double dblXtraInt = DateTime.Today.ToOADate();
					double dblXtraInt2 = DateTime.Today.ToOADate();
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					if ((modUTCalculations.CalculateAccountUT(rsData, rsData.Get_Fields("AccountNumber"), ref dblXtraInt, true, ref dblXInt) + modUTCalculations.CalculateAccountUT(rsData, rsData.Get_Fields("AccountNumber"), ref dblXtraInt2, false, ref dblXInt)) <= 0)
					{
						// skip this account
						goto SKIP;
					}
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("UseREAccount")))
					{
						rsPrev.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + rsData.Get_Fields_Int32("REAccount") + " AND Name = '" + modUTStatusList.FixQuotes(rsData.Get_Fields_String("OName")) + "' AND DateCreated > #" + FCConvert.ToString(datBillDate) + "# ORDER BY DateCreated desc", modExtraModules.strREDatabase);
					}
					else
					{
						// TODO Get_Fields: Field [Key] not found!! (maybe it is an alias?)
						rsPrev.OpenRecordset("SELECT * FROM PreviousOwner WHERE AccountKey = " + rsData.Get_Fields("Key") + " AND OName = '" + modUTStatusList.FixQuotes(rsData.Get_Fields_String("OName")) + "' AND LastUpdated > #" + FCConvert.ToString(datBillDate) + "# ORDER BY LastUpdated desc", modExtraModules.strUTDatabase);
					}
					if (rsPrev.EndOfFile())
					{
						// no previous owner records so no need to show this bill
						goto SKIP;
					}
					// This will show what is different
					strTemp = "";
					// TODO Get_Fields: Field [Bill.OAddress1] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OAddress1"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address1"))))
					{
						if (strTemp != "")
							strTemp += "  ";
						// TODO Get_Fields: Field [Bill.OAddress1] not found!! (maybe it is an alias?)
						strTemp = "Address 1 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OAddress1"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address1"))) + "'";
					}
					// TODO Get_Fields: Field [Bill.OAddress2] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OAddress2"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address2"))))
					{
						if (strTemp != "")
							strTemp += "  ";
						// TODO Get_Fields: Field [Bill.OAddress2] not found!! (maybe it is an alias?)
						strTemp += "Address 2 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OAddress2"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address2"))) + "'";
					}
					if (rsData.Get_Fields_Boolean("UseREAccount") != true)
					{
						// TODO Get_Fields: Field [Bill.OAddress3] not found!! (maybe it is an alias?)
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OAddress3"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address3"))))
						{
							if (strTemp != "")
								strTemp += "  ";
							// TODO Get_Fields: Field [Bill.OAddress3] not found!! (maybe it is an alias?)
							strTemp += "Address 3 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OAddress3"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Address3"))) + "'";
						}
					}
					// TODO Get_Fields: Field [Bill.OCity] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OCity"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("City"))))
					{
						if (strTemp != "")
							strTemp += "  ";
						// TODO Get_Fields: Field [Bill.OCity] not found!! (maybe it is an alias?)
						strTemp = "Address 1 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OCity"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("City"))) + "'";
					}
					// TODO Get_Fields: Field [Bill.OState] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OState"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("State"))))
					{
						if (strTemp != "")
							strTemp += "  ";
						// TODO Get_Fields: Field [Bill.OState] not found!! (maybe it is an alias?)
						strTemp += "Address 2 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OState"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("State"))) + "'";
					}
					// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip"))))
					{
						if (strTemp != "")
							strTemp += "  ";
						// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
						strTemp += "Address 3 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip"))) + "'";
					}
					// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip4"))) != Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip4"))))
					{
						if (strTemp != "")
							strTemp += "  ";
						// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
						strTemp += "Address 3 : '" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip4"))) + "' - '" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip4"))) + "'";
					}
					if (strTemp != "")
					{
						// add a row/element
						vsDemand.AddItem("");
						Array.Resize(ref modUTLien.Statics.arrDemand, vsDemand.Rows - 1 + 1);
						// this will make sure that there are enough elements to use
						lngIndex = vsDemand.Rows - 2;
						modUTLien.Statics.arrDemand[lngIndex].Used = true;
						modUTLien.Statics.arrDemand[lngIndex].Processed = false;
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAccount, FCConvert.ToString(rsData.Get_Fields("AccountNumber")));
						// account number
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						modUTLien.Statics.arrDemand[lngIndex].Account = FCConvert.ToInt32(rsData.Get_Fields("AccountNumber"));
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColName, FCConvert.ToString(rsData.Get_Fields_String("OName")));
						// name
						modUTLien.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("OName"));
						strOldAddress = "";
						// TODO Get_Fields: Field [Bill.OAddress1] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsData.Get_Fields("Bill.OAddress1")) != "")
						{
							// TODO Get_Fields: Field [Bill.OAddress1] not found!! (maybe it is an alias?)
							strOldAddress += rsData.Get_Fields("Bill.OAddress1");
						}
						// TODO Get_Fields: Field [Bill.OAddress2] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsData.Get_Fields("Bill.OAddress2")) != "")
						{
							if (strOldAddress == "")
							{
								// TODO Get_Fields: Field [Bill.OAddress2] not found!! (maybe it is an alias?)
								strOldAddress += rsData.Get_Fields("Bill.OAddress2");
							}
							else
							{
								// TODO Get_Fields: Field [Bill.OAddress2] not found!! (maybe it is an alias?)
								strOldAddress += "\r\n" + rsData.Get_Fields("Bill.OAddress2");
							}
						}
						// TODO Get_Fields: Field [Bill.OAddress3] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsData.Get_Fields("Bill.OAddress3")) != "")
						{
							if (strOldAddress == "")
							{
								// TODO Get_Fields: Field [Bill.OAddress3] not found!! (maybe it is an alias?)
								strOldAddress += rsData.Get_Fields("Bill.OAddress3");
							}
							else
							{
								// TODO Get_Fields: Field [Bill.OAddress3] not found!! (maybe it is an alias?)
								strOldAddress += "\r\n" + rsData.Get_Fields("Bill.OAddress3");
							}
						}
						// TODO Get_Fields: Field [Bill.OCity] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [Bill.OState] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsData.Get_Fields("Bill.OCity")) != "" || FCConvert.ToString(rsData.Get_Fields("Bill.OState")) != "" || FCConvert.ToString(rsData.Get_Fields("Bill.OZip")) != "" || FCConvert.ToString(rsData.Get_Fields("Bill.OZip4")) != "")
						{
							if (strOldAddress == "")
							{
								// TODO Get_Fields: Field [Bill.OState] not found!! (maybe it is an alias?)
								if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OState"))) != "")
								{
									// TODO Get_Fields: Field [Bill.OCity] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [Bill.OState] not found!! (maybe it is an alias?)
									strOldAddress += Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OCity"))) + ", " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OState")))) + " ";
								}
								else
								{
									// TODO Get_Fields: Field [Bill.OCity] not found!! (maybe it is an alias?)
									strOldAddress += Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OCity"))) + " ";
								}
								// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
								if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip"))) != "" && Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip4"))) != "")
								{
									// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
									strOldAddress += Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip4")));
								}
								// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
								else if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip"))) != "")
								{
									// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
									strOldAddress += Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip")));
								}
							}
							else
							{
								// TODO Get_Fields: Field [Bill.OState] not found!! (maybe it is an alias?)
								if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OState"))) != "")
								{
									// TODO Get_Fields: Field [Bill.OCity] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [Bill.OState] not found!! (maybe it is an alias?)
									strOldAddress += "\r\n" + Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OCity"))) + ", " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OState")))) + " ";
								}
								else
								{
									// TODO Get_Fields: Field [Bill.OCity] not found!! (maybe it is an alias?)
									strOldAddress += "\r\n" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OCity"))) + " ";
								}
								// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
								if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip"))) != "" && Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip4"))) != "")
								{
									// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
									// TODO Get_Fields: Field [Bill.OZip4] not found!! (maybe it is an alias?)
									strOldAddress += Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip"))) + "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip4")));
								}
								// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
								else if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip"))) != "")
								{
									// TODO Get_Fields: Field [Bill.OZip] not found!! (maybe it is an alias?)
									strOldAddress += Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Bill.OZip")));
								}
							}
						}
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAddress1, strOldAddress);
						modUTLien.Statics.arrDemand[lngIndex].OldAddress = strOldAddress;
						strNewAddress = "";
						if (FCConvert.ToString(rsPrev.Get_Fields_String("Address1")) != "")
						{
							strNewAddress += rsPrev.Get_Fields_String("Address1");
						}
						if (FCConvert.ToString(rsPrev.Get_Fields_String("Address2")) != "")
						{
							if (strNewAddress == "")
							{
								strNewAddress += rsPrev.Get_Fields_String("Address2");
							}
							else
							{
								strNewAddress += "\r\n" + rsPrev.Get_Fields_String("Address2");
							}
						}
						if (rsData.Get_Fields_Boolean("UseREAccount") != true)
						{
							if (FCConvert.ToString(rsPrev.Get_Fields_String("Address3")) != "")
							{
								if (strNewAddress == "")
								{
									strNewAddress += rsPrev.Get_Fields_String("Address3");
								}
								else
								{
									strNewAddress += "\r\n" + rsPrev.Get_Fields_String("Address3");
								}
							}
						}
						if (FCConvert.ToString(rsPrev.Get_Fields_String("City")) != "" || FCConvert.ToString(rsPrev.Get_Fields_String("State")) != "" || FCConvert.ToString(rsPrev.Get_Fields_String("Zip")) != "" || FCConvert.ToString(rsPrev.Get_Fields_String("Zip4")) != "")
						{
							if (strNewAddress == "")
							{
								if (Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("State"))) != "")
								{
									strNewAddress += Strings.Trim(Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("State")))) + " ";
								}
								else
								{
									strNewAddress += Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("City"))) + " ";
								}
								if (Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip"))) != "" && Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip4"))) != "")
								{
									strNewAddress += Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip"))) + "-" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip4")));
								}
								else if (Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip"))) != "")
								{
									strNewAddress += Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip")));
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("State"))) != "")
								{
									strNewAddress += "\r\n" + Strings.Trim(Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("City"))) + ", " + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("State")))) + " ";
								}
								else
								{
									strNewAddress += "\r\n" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("City"))) + " ";
								}
								if (Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip"))) != "" && Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip4"))) != "")
								{
									strNewAddress += Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip"))) + "-" + Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip4")));
								}
								else if (Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip"))) != "")
								{
									strNewAddress += Strings.Trim(FCConvert.ToString(rsPrev.Get_Fields_String("Zip")));
								}
							}
						}
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAddress2, strNewAddress);
						modUTLien.Statics.arrDemand[lngIndex].NewAddress = strNewAddress;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColBillKey, FCConvert.ToString(rsData.Get_Fields("Bill")));
						// billkey
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColHidden, strTemp);
					}
					SKIP:
					;
					rsData.MoveNext();
				}
				if (vsDemand.Rows == 1)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("There are no accounts eligible to have addresses updated.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FillGrid = false;
					return FillGrid;
				}
				vsDemand.AutoSizeMode = FCGrid.AutoSizeSettings.flexAutoSizeRowHeight;
				vsDemand.AutoSize(0, vsDemand.Cols - 1);
				// check all of the accounts
				mnuFileSelect_Click();
				//lngHeight = 0;
				//for (counter = 0; counter <= vsDemand.Rows - 1; counter++)
				//{
				//    //Application.DoEvents();
				//    lngHeight += vsDemand.RowHeight(counter);
				//}
				//if (lngHeight > fraGrid.Height - lblValidateInstruction.Height - 500)
				//{
				//    vsDemand.Height = fraGrid.Height - lblValidateInstruction.Height - 500;
				//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//    vsDemand.Height = lngHeight + 70;
				//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				frmWait.InstancePtr.Unload();
				return FillGrid;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillGrid;
		}

		private void ApplyAddressChanges()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				clsDRWrapper rsUT = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				clsDRWrapper rsMaster = new clsDRWrapper();
				frmWait.InstancePtr.Init("Processing..." + "\r\n" + "Applying Changes");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					//Application.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, lngColCheckBox)) == -1)
					{
						// check to see if the check box if checked
						rsUT.OpenRecordset("SELECT * FROM Bill WHERE Bill = " + vsDemand.TextMatrix(lngCT, lngColBillKey), modExtraModules.strUTDatabase);
						rsMaster.OpenRecordset("SELECT * FROM Master WHERE Key = " + rsUT.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
						if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("UseREAccount")))
						{
							rsPrev.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + rsMaster.Get_Fields_Int32("REAccount") + " AND Name = '" + rsUT.Get_Fields_String("OName") + "' AND DateCreated > #" + FCConvert.ToString(datBillDate) + "# ORDER BY DateCreated desc", modExtraModules.strREDatabase);
						}
						else
						{
							rsPrev.OpenRecordset("SELECT * FROM PreviousOwner WHERE AccountKey = " + rsUT.Get_Fields_Int32("AccountKey") + " AND OName = '" + rsUT.Get_Fields_String("OName") + "' AND LastUpdated > #" + FCConvert.ToString(datBillDate) + "# ORDER BY LastUpdated desc", modExtraModules.strUTDatabase);
						}
						if (rsPrev.EndOfFile())
						{
							MessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, lngColAccount) + ".  No address change was applied.", "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, lngColBillKey), MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
						else
						{
							modUTLien.Statics.arrDemand[FindDemandIndex_2(rsUT.Get_Fields_Int32("ActualAccountNumber"))].Processed = true;
							rsUT.Edit();
							rsUT.Set_Fields("OAddress1", rsPrev.Get_Fields_String("Address1"));
							rsUT.Set_Fields("OAddress2", rsPrev.Get_Fields_String("Address2"));
							if (rsMaster.Get_Fields_Boolean("UseREAccount") != true)
							{
								rsUT.Set_Fields("OAddress3", rsPrev.Get_Fields_String("Address3"));
							}
							else
							{
								rsUT.Set_Fields("OAddress3", "");
							}
							rsUT.Set_Fields("OCity", rsPrev.Get_Fields_String("City"));
							rsUT.Set_Fields("OState", rsPrev.Get_Fields_String("State"));
							rsUT.Set_Fields("OZip", rsPrev.Get_Fields_String("Zip"));
							rsUT.Set_Fields("OZip4", rsPrev.Get_Fields_String("Zip4"));
							rsUT.Update();
							lngDemandCount += 1;
						}
					}
					else
					{
						// not selected...do nothing
					}
				}
				frmWait.InstancePtr.Unload();
				MessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", "Addresses Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// print the list of accounts that have been affected
				rptUpdatedAddressList.InstancePtr.Init(ref lngDemandCount, cboRateKeys.Items[cboRateKeys.SelectedIndex].ToString());
				frmReportViewer.InstancePtr.Init(rptUpdatedAddressList.InstancePtr);
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int FindDemandIndex_2(int lngAcct)
		{
			return FindDemandIndex(ref lngAcct);
		}

		private int FindDemandIndex(ref int lngAcct)
		{
			int FindDemandIndex = 0;
			int lngCT;
			for (lngCT = 0; lngCT <= Information.UBound(modUTLien.Statics.arrDemand, 1); lngCT++)
			{
				//Application.DoEvents();
				if (lngAcct == modUTLien.Statics.arrDemand[lngCT].Account)
				{
					FindDemandIndex = lngCT;
					break;
				}
			}
			return FindDemandIndex;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			ApplyAddressChanges();
			this.Unload();
		}

		private void vsDemand_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// this should not allow an account that already has demand fees to be checked
			if (vsDemand.Col == 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(vsDemand.Row, lngColHidden)) != 0)
				{
					vsDemand.TextMatrix(vsDemand.Row, lngColCheckBox, FCConvert.ToString(0));
				}
			}
		}

		private void vsDemand_DblClick(object sender, System.EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == lngColCheckBox)
			{
				mnuFileSelect_Click();
			}
		}

		private void vsDemand_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsDemand[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = vsDemand.GetFlexRowIndex(e.RowIndex);
			lngMC = vsDemand.GetFlexColIndex(e.ColumnIndex);
			if (lngMR > 0 && lngMC > 1)
			{
				//ToolTip1.SetToolTip(vsDemand, Strings.Trim(vsDemand.TextMatrix(lngMR, lngColHidden)));
				cell.ToolTipText = Strings.Trim(vsDemand.TextMatrix(lngMR, lngColHidden));
			}
		}

		private void vsDemand_RowColChange(object sender, System.EventArgs e)
		{
			if (vsDemand.Col == lngColCheckBox)
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngColCheckBox, FCConvert.ToString(0));
			}
		}
	}
}
