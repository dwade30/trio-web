﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptLoadbackReport.
	/// </summary>
	public partial class srptLoadbackReport : FCSectionReport
	{
		public srptLoadbackReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static srptLoadbackReport InstancePtr
		{
			get
			{
				return (srptLoadbackReport)Sys.GetInstance(typeof(srptLoadbackReport));
			}
		}

		protected srptLoadbackReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLoadbackReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/08/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/08/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngNumOfAccts;
		bool boolLien;
		double[] dblTotals = new double[5 + 1];
		int lngYear;
		string strWS = "";
		bool boolWater;
		bool blnFirstRecord;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsData.MoveNext();
				eArgs.EOF = rsData.EndOfFile();
			}
			if (eArgs.EOF == false)
			{
				this.Fields["Binder"].Value = rsData.Get_Fields_Int32("BillingRateKey");
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
			// catch the esc key and unload the report
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			boolLien = FCConvert.CBool(Strings.Trim(Strings.Right(FCConvert.ToString(this.UserData), 1)) == "L");
			boolWater = FCConvert.CBool(Strings.Trim(Strings.Left(FCConvert.ToString(this.UserData), 1)) == "W");
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			if (!boolLien)
			{
				rsData.OpenRecordset("SELECT * FROM Bill WHERE " + strWS + "LienRecordNumber = 0 AND ISNULL(LoadBack,0) = 1 ORDER BY BillingRateKey, ActualAccountNumber, Bill.ID", modExtraModules.strUTDatabase);
				if (boolWater)
				{
					lblReportType.Text = "Water Non Lien Loadbacks";
				}
				else
				{
					lblReportType.Text = "Sewer Non Lien Loadbacks";
				}
				lblPrin.Text = "Principal";
				lblTax.Text = "Tax";
				lblCost.Text = "Costs";
				lblPLI.Text = "";
			}
			else
			{
				rsData.OpenRecordset("SELECT * FROM Bill WHERE " + strWS + "LienRecordNumber <> 0 AND ISNULL(LoadBack,0) = 1 ORDER BY BillingRateKey, ActualAccountNumber, Bill.ID", modExtraModules.strUTDatabase);
				if (boolWater)
				{
					lblReportType.Text = "Water Lien Loadbacks";
				}
				else
				{
					lblReportType.Text = "Sewer Lien Loadbacks";
				}
				lblPrin.Text = "Principal";
				lblTax.Text = "Tax";
				lblCost.Text = "Costs";
				lblPLI.Text = "PLI";
			}
			if (rsData.EndOfFile())
			{
				// MsgBox "There are no accounts that have been loaded back.", vbOKOnly, "No Accounts Found"
				this.Close();
				//this.Unload();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				BindFields();
				lngNumOfAccts += 1;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Detail Format Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (lngNumOfAccts == 1)
				{
					fldFooterTotal.Text = "Total for 1 account.";
				}
				else
				{
					fldFooterTotal.Text = "Total for " + FCConvert.ToString(lngNumOfAccts) + " accounts.";
				}
				// show totals
				fldFooterPrin.Text = Strings.Format(dblTotals[1], "#,##0.00");
				fldFooterTax.Text = Strings.Format(dblTotals[2], "#,##0.00");
				fldFooterCost.Text = Strings.Format(dblTotals[3], "#,##0.00");
				if (boolLien)
				{
					fldFooterPLI.Text = Strings.Format(dblTotals[4], "#,##0.00");
					fldFooterPLI.Visible = true;
				}
				else
				{
					fldFooterPLI.Text = "";
				}
				fldFooterCHGINT.Text = Strings.Format(dblTotals[0], "#,##0.00");
				fldFooterTaxTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[3] + dblTotals[4], "#,##0.00");
				// If lngNumOfAccts = 1 Then
				// fldFooterTotal.Text = "Total for 1 account."
				// Else
				// fldFooterTotal.Text = "Total for " & lngNumOfAccts & " accounts."
				// End If
				// 
				// show totals
				// fldFooterPrin.Text = Format(dblTotals(1), "#,##0.00")
				// fldFooterTax.Text = Format(dblTotals(2), "#,##0.00")
				// fldFooterCost.Text = Format(dblTotals(3), "#,##0.00")
				// If boolLien Then
				// fldFooterPLI.Text = Format(dblTotals(4), "#,##0.00")
				// fldFooterPLI.Visible = True
				// Else
				// fldFooterPLI.Text = ""
				// End If
				// 
				// fldFooterCHGINT.Text = Format(dblTotals(0), "#,##0.00")
				// fldFooterTaxTotal.Text = Format(dblTotals(0) + dblTotals(1) + dblTotals(2) + dblTotals(3) + dblTotals(4), "#,##0.00")
				FCUtils.EraseSafe(dblTotals);
				lngNumOfAccts = 0;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Group Format Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			int lngAccount = 0;
			clsDRWrapper rsLien = new clsDRWrapper();
			DUPLICATEBILL:
			;
            try
            {
                if (!rsData.EndOfFile())
                {
                    // us the correct new information
                    lngAccount = FCConvert.ToInt32(rsData.Get_Fields_Int32("ActualAccountNumber"));
                    lngYear = FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingRateKey"));
                    // check the next record so see if it is the same
                    rsData.MoveNext();
                    if (!rsData.EndOfFile())
                    {
                        if (FCConvert.ToInt32(rsData.Get_Fields_Int32("ActualAccountNumber")) == lngAccount &&
                            FCConvert.ToInt32(rsData.Get_Fields_Int32("BillingRateKey")) == lngYear)
                        {
                            // if the next account is the same then skip it by staying on this account and starting over
                            goto DUPLICATEBILL;
                        }
                    }

                    // move back to the record at hand
                    rsData.MovePrevious();
                    // If GroupHeader1.GroupValue <> lngYear Then
                    // GroupHeader1.GroupValue = lngYear
                    // SetupGroupFooter
                    // Erase dblTotals
                    // lngNumOfAccts = 0
                    // End If
                    fldAcct.Text = FCConvert.ToString(lngAccount);
                    fldName.Text = rsData.Get_Fields_String("OName");
                    if (FCConvert.ToString(rsData.Get_Fields_String("OName2")) != "")
                    {
                        lblName.Text = lblName.Text + " & " + rsData.Get_Fields_String("OName2");
                    }

                    fldRK.Text = lngYear.ToString();
                    if (!boolLien)
                    {
                        fldPrin.Text = Strings.Format(rsData.Get_Fields(strWS + "PrinOwed"), "#,##0.00");
                        fldTax.Text = Strings.Format(rsData.Get_Fields(strWS + "TaxOwed"), "#,##0.00");
                        fldCost.Text = Strings.Format(rsData.Get_Fields(strWS + "CostOwed"), "#,##0.00");
                        fldPLI.Text = "";
                        // Format(.Fields("IntOwed"), "#,##0.00")
                        fldCHGINT.Text = Strings.Format(rsData.Get_Fields(strWS + "IntAdded") * -1, "#,##0.00");
                        fldTotal.Text = Strings.Format(
                            rsData.Get_Fields(strWS + "PrinOwed") + rsData.Get_Fields(strWS + "TaxOwed") +
                            rsData.Get_Fields(strWS + "CostOwed") + rsData.Get_Fields(strWS + "IntOwed") -
                            rsData.Get_Fields(strWS + "IntAdded"), "#,##0.00");
                        dblTotals[1] += FCConvert.ToDouble(fldPrin.Text);
                        dblTotals[2] += FCConvert.ToDouble(fldTax.Text);
                        dblTotals[3] += FCConvert.ToDouble(fldCost.Text);
                        // dblTotals(4) = dblTotals(4) + CDbl(fldPLI.Text)
                        dblTotals[0] += FCConvert.ToDouble(fldCHGINT.Text);
                    }
                    else
                    {
                        rsLien.OpenRecordset(
                            "SELECT * FROM Lien WHERE ID = " + rsData.Get_Fields(strWS + "LienRecordNumber"),
                            modExtraModules.strUTDatabase);
                        if (!rsLien.EndOfFile())
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            fldPrin.Text = Strings.Format(rsLien.Get_Fields("Principal"), "#,##0.00");
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            fldTax.Text = Strings.Format(rsLien.Get_Fields("Tax"), "#,##0.00");
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            fldCost.Text = Strings.Format(rsLien.Get_Fields("Costs"), "#,##0.00");
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            fldPLI.Text = Strings.Format(rsLien.Get_Fields("Interest"), "#,##0.00");
                            // TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
                            fldCHGINT.Text = Strings.Format(rsLien.Get_Fields("IntAdded") * -1, "#,##0.00");
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
                            fldTotal.Text =
                                Strings.Format(
                                    rsLien.Get_Fields("Principal") + rsLien.Get_Fields("Tax") +
                                    rsLien.Get_Fields("Interest") + rsLien.Get_Fields("Costs") -
                                    rsLien.Get_Fields("IntAdded"), "#,##0.00");
                        }
                        else
                        {
                            fldPrin.Text = "0.00";
                            fldTax.Text = "0.00";
                            fldCost.Text = "0.00";
                            fldPLI.Text = "0.00";
                        }

                        dblTotals[0] += FCConvert.ToDouble(fldCHGINT.Text);
                        dblTotals[1] += FCConvert.ToDouble(fldPrin.Text);
                        dblTotals[2] += FCConvert.ToDouble(fldTax.Text);
                        dblTotals[3] += FCConvert.ToDouble(fldCost.Text);
                        dblTotals[4] += FCConvert.ToDouble(fldPLI.Text);
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                //ERROR_HANDLER: ;
                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				rsLien.Dispose();
            }
		}

		private void SetupGroupFooter()
		{
			if (lngNumOfAccts == 1)
			{
				fldFooterTotal.Text = "Total for 1 account.";
			}
			else
			{
				fldFooterTotal.Text = "Total for " + FCConvert.ToString(lngNumOfAccts) + " accounts.";
			}
			// show totals
			fldFooterPrin.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldFooterTax.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldFooterCost.Text = Strings.Format(dblTotals[3], "#,##0.00");
			if (!boolLien)
			{
				fldFooterPLI.Text = Strings.Format(dblTotals[4], "#,##0.00");
				fldFooterPLI.Visible = true;
			}
			else
			{
				fldFooterPLI.Text = "";
			}
			fldFooterCHGINT.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldFooterTaxTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[3] + dblTotals[4], "#,##0.00");
		}

		private void srptLoadbackReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptLoadbackReport properties;
			//srptLoadbackReport.Caption	= "Account Detail";
			//srptLoadbackReport.Icon	= "srptUTLoadbackReport.dsx":0000";
			//srptLoadbackReport.Left	= 0;
			//srptLoadbackReport.Top	= 0;
			//srptLoadbackReport.Width	= 11880;
			//srptLoadbackReport.Height	= 8595;
			//srptLoadbackReport.StartUpPosition	= 3;
			//srptLoadbackReport.SectionData	= "srptUTLoadbackReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
