﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arUTAgedLists.
	/// </summary>
	partial class arUTAgedLists
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arUTAgedLists));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld30Day = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld60Day = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld90Day = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldTotalCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal30Day = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal60Day = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal90Day = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbl30Day = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbl60Day = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbl90Day = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLiens = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld30Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld60Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld90Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal30Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal60Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal90Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl30Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl60Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl90Day)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLiens)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanGrow = false;
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldName,
				this.fldCurrent,
				this.fld30Day,
				this.fld60Day,
				this.lblLocation,
				this.fldLocation,
				this.lblMapLot,
				this.fldMapLot,
				this.fld90Day,
				this.fldLien,
				this.fldTotal
			});
			this.Detail.Height = 0.3645833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.75F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.75F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.75F;
			// 
			// fldCurrent
			// 
			this.fldCurrent.Height = 0.1875F;
			this.fldCurrent.Left = 3.3125F;
			this.fldCurrent.Name = "fldCurrent";
			this.fldCurrent.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCurrent.Text = "0.00";
			this.fldCurrent.Top = 0F;
			this.fldCurrent.Width = 1.1875F;
			// 
			// fld30Day
			// 
			this.fld30Day.Height = 0.1875F;
			this.fld30Day.Left = 4.5F;
			this.fld30Day.Name = "fld30Day";
			this.fld30Day.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fld30Day.Text = "0.00";
			this.fld30Day.Top = 0F;
			this.fld30Day.Width = 1.125F;
			// 
			// fld60Day
			// 
			this.fld60Day.Height = 0.1875F;
			this.fld60Day.Left = 5.625F;
			this.fld60Day.Name = "fld60Day";
			this.fld60Day.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fld60Day.Text = "0.00";
			this.fld60Day.Top = 0F;
			this.fld60Day.Width = 1.125F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 1.875F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'";
			this.lblLocation.Text = "Location:";
			this.lblLocation.Top = 0.1875F;
			this.lblLocation.Width = 0.75F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 2.625F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0.1875F;
			this.fldLocation.Width = 5.25F;
			// 
			// lblMapLot
			// 
			this.lblMapLot.Height = 0.1875F;
			this.lblMapLot.HyperLink = null;
			this.lblMapLot.Left = 0F;
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Style = "font-family: \'Tahoma\'";
			this.lblMapLot.Text = "Map Lot:";
			this.lblMapLot.Top = 0.1875F;
			this.lblMapLot.Width = 0.625F;
			// 
			// fldMapLot
			// 
			this.fldMapLot.CanGrow = false;
			this.fldMapLot.Height = 0.1875F;
			this.fldMapLot.Left = 0.625F;
			this.fldMapLot.MultiLine = false;
			this.fldMapLot.Name = "fldMapLot";
			this.fldMapLot.Style = "font-family: \'Tahoma\'";
			this.fldMapLot.Text = null;
			this.fldMapLot.Top = 0.1875F;
			this.fldMapLot.Width = 1.25F;
			// 
			// fld90Day
			// 
			this.fld90Day.Height = 0.1875F;
			this.fld90Day.Left = 6.75F;
			this.fld90Day.Name = "fld90Day";
			this.fld90Day.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fld90Day.Text = "0.00";
			this.fld90Day.Top = 0F;
			this.fld90Day.Width = 1.125F;
			// 
			// fldLien
			// 
			this.fldLien.Height = 0.1875F;
			this.fldLien.Left = 7.875F;
			this.fldLien.Name = "fldLien";
			this.fldLien.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldLien.Text = "0.00";
			this.fldLien.Top = 0F;
			this.fldLien.Width = 1.0625F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 8.9375F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = "0.00";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.1875F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalCurrent,
				this.fldTotal30Day,
				this.fldTotal60Day,
				this.lnTotals,
				this.lblTotals,
				this.fldTotal90Day,
				this.fldTotalLien,
				this.fldTotalTotal
			});
			this.ReportFooter.Height = 0.3645833F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldTotalCurrent
			// 
			this.fldTotalCurrent.Height = 0.1875F;
			this.fldTotalCurrent.Left = 3.1875F;
			this.fldTotalCurrent.Name = "fldTotalCurrent";
			this.fldTotalCurrent.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCurrent.Text = "0.00";
			this.fldTotalCurrent.Top = 0.0625F;
			this.fldTotalCurrent.Width = 1.3125F;
			// 
			// fldTotal30Day
			// 
			this.fldTotal30Day.Height = 0.1875F;
			this.fldTotal30Day.Left = 4.5F;
			this.fldTotal30Day.Name = "fldTotal30Day";
			this.fldTotal30Day.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal30Day.Text = "0.00";
			this.fldTotal30Day.Top = 0.0625F;
			this.fldTotal30Day.Width = 1.125F;
			// 
			// fldTotal60Day
			// 
			this.fldTotal60Day.Height = 0.1875F;
			this.fldTotal60Day.Left = 5.625F;
			this.fldTotal60Day.Name = "fldTotal60Day";
			this.fldTotal60Day.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal60Day.Text = "0.00";
			this.fldTotal60Day.Top = 0.0625F;
			this.fldTotal60Day.Width = 1.125F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 2.5625F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0F;
			this.lnTotals.Width = 7.625F;
			this.lnTotals.X1 = 2.5625F;
			this.lnTotals.X2 = 10.1875F;
			this.lnTotals.Y1 = 0F;
			this.lnTotals.Y2 = 0F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.1875F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0.0625F;
			this.lblTotals.Width = 2.8125F;
			// 
			// fldTotal90Day
			// 
			this.fldTotal90Day.Height = 0.1875F;
			this.fldTotal90Day.Left = 6.75F;
			this.fldTotal90Day.Name = "fldTotal90Day";
			this.fldTotal90Day.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal90Day.Text = "0.00";
			this.fldTotal90Day.Top = 0.0625F;
			this.fldTotal90Day.Width = 1.125F;
			// 
			// fldTotalLien
			// 
			this.fldTotalLien.Height = 0.1875F;
			this.fldTotalLien.Left = 7.875F;
			this.fldTotalLien.Name = "fldTotalLien";
			this.fldTotalLien.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalLien.Text = "0.00";
			this.fldTotalLien.Top = 0.0625F;
			this.fldTotalLien.Width = 1.0625F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 8.9375F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0.0625F;
			this.fldTotalTotal.Width = 1.1875F;
			// 
			// PageHeader
			// 
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lblReportType,
				this.lblAccount,
				this.lblCurrent,
				this.lbl30Day,
				this.lbl60Day,
				this.lbl90Day,
				this.lblLiens,
				this.lblTotal,
				this.lblName,
				this.lnHeader
			});
			this.PageHeader.Height = 1.020833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Collection Status List";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 10.125F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 9F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.125F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 9F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.25F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 3.5F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.5F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblReportType.Text = "Report Type";
			this.lblReportType.Top = 0.1875F;
			this.lblReportType.Width = 10.125F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.1875F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.8125F;
			this.lblAccount.Width = 0.5F;
			// 
			// lblCurrent
			// 
			this.lblCurrent.Height = 0.375F;
			this.lblCurrent.HyperLink = null;
			this.lblCurrent.Left = 3.5F;
			this.lblCurrent.Name = "lblCurrent";
			this.lblCurrent.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblCurrent.Text = "Current Amount Due";
			this.lblCurrent.Top = 0.625F;
			this.lblCurrent.Width = 1F;
			// 
			// lbl30Day
			// 
			this.lbl30Day.Height = 0.1875F;
			this.lbl30Day.HyperLink = null;
			this.lbl30Day.Left = 4.5F;
			this.lbl30Day.Name = "lbl30Day";
			this.lbl30Day.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lbl30Day.Text = "30 Day";
			this.lbl30Day.Top = 0.8125F;
			this.lbl30Day.Width = 1.125F;
			// 
			// lbl60Day
			// 
			this.lbl60Day.Height = 0.1875F;
			this.lbl60Day.HyperLink = null;
			this.lbl60Day.Left = 5.625F;
			this.lbl60Day.Name = "lbl60Day";
			this.lbl60Day.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lbl60Day.Text = "60 Day";
			this.lbl60Day.Top = 0.8125F;
			this.lbl60Day.Width = 1.125F;
			// 
			// lbl90Day
			// 
			this.lbl90Day.Height = 0.1875F;
			this.lbl90Day.HyperLink = null;
			this.lbl90Day.Left = 6.75F;
			this.lbl90Day.Name = "lbl90Day";
			this.lbl90Day.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lbl90Day.Text = "90 Day";
			this.lbl90Day.Top = 0.8125F;
			this.lbl90Day.Width = 1.125F;
			// 
			// lblLiens
			// 
			this.lblLiens.Height = 0.1875F;
			this.lblLiens.HyperLink = null;
			this.lblLiens.Left = 7.875F;
			this.lblLiens.Name = "lblLiens";
			this.lblLiens.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblLiens.Text = "Liens";
			this.lblLiens.Top = 0.8125F;
			this.lblLiens.Width = 1.0625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 8.9375F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.8125F;
			this.lblTotal.Width = 1.1875F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.75F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.8125F;
			this.lblName.Width = 0.5F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0.0625F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 1F;
			this.lnHeader.Width = 10.0625F;
			this.lnHeader.X1 = 0.0625F;
			this.lnHeader.X2 = 10.125F;
			this.lnHeader.Y1 = 1F;
			this.lnHeader.Y2 = 1F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// arUTAgedLists
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld30Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld60Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld90Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal30Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal60Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal90Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl30Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl60Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl90Day)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLiens)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld30Day;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld60Day;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld90Day;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLien;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal30Day;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal60Day;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal90Day;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalLien;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl30Day;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl60Day;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl90Day;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLiens;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
