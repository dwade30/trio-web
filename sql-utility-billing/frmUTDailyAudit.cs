﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTDailyAudit.
	/// </summary>
	public partial class frmUTDailyAudit : BaseForm
	{
		public frmUTDailyAudit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTDailyAudit InstancePtr
		{
			get
			{
				return (frmUTDailyAudit)Sys.GetInstance(typeof(frmUTDailyAudit));
			}
		}

		protected frmUTDailyAudit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/06/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/16/2007              *
		// ********************************************************
		private void cmdDone_Click(object sender, System.EventArgs e)
		{
			// show the report modally so that the closeout will not happen until after the report is finished
			if (cmbAudit.SelectedIndex == 0)
			{
				// Audit Preview
				// this will look for payments that are not closed out yet
				modMain.Statics.glngCurrentCloseOut = 0;
				if (cmbREPP.Text == "Water" || cmbREPP.Text == "Stormwater")
				{
					// the user wants a real estate audit
					rptUTDailyAuditMaster.InstancePtr.StartUp(false, cmbAlignment.Text == "Landscape", 0);
				}
				else if (cmbREPP.Text == "Sewer")
				{
					// the user wants a personal property audit
					rptUTDailyAuditMaster.InstancePtr.StartUp(false, cmbAlignment.Text == "Landscape", 1);
				}
				else
				{
					// both RE and PP
					rptUTDailyAuditMaster.InstancePtr.StartUp(false, cmbAlignment.Text == "Landscape", 2);
				}
				// audit preview
				frmReportViewer.InstancePtr.Init(rptUTDailyAuditMaster.InstancePtr, "", 0, false, false, "Pages", cmbAlignment.Text == "Landscape");
			}
			else if (cmbAudit.SelectedIndex == 1)
			{
				// Daily Audit
				// closeout the payments (only in UT)
				modMain.Statics.glngCurrentCloseOut = modMain.UTCloseOut();
				// both Water and Sewer and close out all of the records
				rptUTDailyAuditMaster.InstancePtr.StartUp(true, cmbAlignment.Text == "Landscape", 2, true, true);
				// rptUTDailyAuditMaster.PrintReport
				modDuplexPrinting.DuplexPrintReport(rptUTDailyAuditMaster.InstancePtr, "", false, cmbAlignment.Text == "Landscape");
				rptUTDailyAuditMaster.InstancePtr.Unload();
				this.Unload();
			}
			else
			{
				// nothing else
			}
		}

		public void cmdDone_Click()
		{
			cmdDone_Click(cmdDone, new System.EventArgs());
		}

		private void frmUTDailyAudit_Activated(object sender, System.EventArgs e)
		{
			if (modGlobalConstants.Statics.gboolCR)
			{
				// this will turn off the audit report if the user has CR and will only allow
				// the user to preview the CL report not actually close out the payments
				if (cmbAudit.Items.Contains("Audit Report and Close Out"))
				{
					cmbAudit.Items.Remove("Audit Report and Close Out");
				}
				ToolTip1.SetToolTip(cmbAudit, "The audit will be run when the TRIO Cash Receipting daily audit is run.");
			}
			else
			{
				//optAudit[1].Enabled = true;
				if (!cmbAudit.Items.Contains("Audit Report and Close Out"))
				{
					cmbAudit.Items.Insert(1, "Audit Report and Close Out");
				}
				ToolTip1.SetToolTip(cmbAudit, "");
			}
			if (modUTStatusPayments.Statics.TownService == "W")
			{
				if (!cmbREPP.Items.Contains("Water"))
				{
					cmbREPP.Items.Insert(0, "Water");
				}
				if (cmbREPP.Items.Contains("Sewer"))
				{
					cmbREPP.Items.Remove("Sewer");
				}
				if (cmbREPP.Items.Contains("Both"))
				{
					cmbREPP.Items.Remove("Both");
				}
				cmbREPP.Text = "Water";
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				if (cmbREPP.Items.Contains("Water"))
				{
					cmbREPP.Items.Remove("Water");
				}
				if (!cmbREPP.Items.Contains("Sewer"))
				{
					cmbREPP.Items.Insert(1, "Sewer");
				}
				if (cmbREPP.Items.Contains("Both"))
				{
					cmbREPP.Items.Remove("Both");
				}
				cmbREPP.Text = "Sewer";
			}
			else if (modUTStatusPayments.Statics.TownService == "B")
			{
				if (!cmbREPP.Items.Contains("Water"))
				{
					cmbREPP.Items.Insert(0, "Water");
				}
				if (!cmbREPP.Items.Contains("Sewer"))
				{
					cmbREPP.Items.Insert(1, "Sewer");
				}
				if (!cmbREPP.Items.Contains("Both"))
				{
					cmbREPP.Items.Insert(2, "Both");
				}
			}
			ShowFrame(ref fraPreAudit);
		}

		private void frmUTDailyAudit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTDailyAudit properties;
			//frmUTDailyAudit.FillStyle	= 0;
			//frmUTDailyAudit.ScaleWidth	= 9045;
			//frmUTDailyAudit.ScaleHeight	= 6930;
			//frmUTDailyAudit.LinkTopic	= "Form2";
			//frmUTDailyAudit.LockControls	= true;
			//frmUTDailyAudit.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDef = new clsDRWrapper();
				rsDef.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!rsDef.EndOfFile())
				{
					modUTCalculations.Statics.gboolShowMapLotInUTAudit = FCConvert.CBool(rsDef.Get_Fields_Boolean("ShowMapLotInUTAudit"));
				}
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				// kk trouts-6 03012013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					if (cmbREPP.Items.Contains("Water"))
					{
						cmbREPP.Items.Remove("Water");
						cmbREPP.Items.Insert(0, "Stormwater");
					}
				}
				frmUTDailyAudit_Activated(null, EventArgs.Empty);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Daily Audit Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmUTDailyAudit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// Catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowFrame(ref FCFrame fraFrame)
		{
			// this will place the frame in the middle of the form
			//fraFrame.Top = FCConvert.ToInt32((this.Height - fraFrame.Height) / 3.0);
			//fraFrame.Left = FCConvert.ToInt32((this.Width - fraFrame.Width) / 2.0);
			fraFrame.Visible = true;
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			cmdDone_Click();
		}

		private void optAudit_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				cmbREPP.Visible = true;
			}
			else
			{
				cmbREPP.Visible = false;
			}
		}

		private void optAudit_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbAudit.SelectedIndex;
			optAudit_CheckedChanged(index, sender, e);
		}
	}
}
