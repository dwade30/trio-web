﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arMeterCountNoCat.
	/// </summary>
	public partial class arMeterCountNoCat : BaseSectionReport
	{
		public arMeterCountNoCat()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Meters Missing Category";
		}

		public static arMeterCountNoCat InstancePtr
		{
			get
			{
				return (arMeterCountNoCat)Sys.GetInstance(typeof(arMeterCountNoCat));
			}
		}

		protected arMeterCountNoCat _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arMeterCountNoCat	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ****************************************************      ****
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/17/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/17/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsDataW = new clsDRWrapper();
		//clsDRWrapper rsDataS = new clsDRWrapper();
		int lngCountW;
		int lngCountS;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			rsData.OpenRecordset("SELECT AccountNumber, pBill.FullNameLF AS Name, WaterCategory, SewerCategory, Service FROM  MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID WHERE (SewerCategory = 0 AND Service <> 'W') or (WaterCategory = 0 AND Service <> 'S')", modExtraModules.strUTDatabase);
			if (rsData.EndOfFile())
			{
				EndReport();
			}
		}

		private void EndReport()
		{
			// this routine will hide everything
			lblHeader.Visible = false;
			lblAccount.Visible = false;
			lblDesc.Visible = false;
			lblWaterHeader.Visible = false;
			lblSewerHeader.Visible = false;
			Line2.Visible = false;
			GroupHeader1.Height = 0;
			fldAcct.Visible = false;
			fldDesc.Visible = false;
			fldW.Visible = false;
			fldS.Visible = false;
			Detail.Height = 0;
			Line1.Visible = false;
			fldTotalCount.Visible = false;
			fldTotalS.Visible = false;
			fldTotalW.Visible = false;
			GroupFooter1.Height = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			if (!rsData.EndOfFile())
			{
				// this counts all of the meter records that have the same catagory as the current metersize
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
				fldDesc.Text = rsData.Get_Fields_String("Name");
				if ((FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterCategory")) == 0 && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "S") && (FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerCategory")) == 0 && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "W"))
				{
					fldS.Text = "1";
					lngCountS += 1;
					fldW.Text = "1";
					lngCountW += 1;
				}
				else if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerCategory")) == 0 && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "W")
				{
					fldS.Text = "1";
					fldW.Text = "0";
					lngCountS += 1;
				}
				else if (FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterCategory")) == 0 && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "S")
				{
					fldW.Text = "1";
					fldS.Text = "0";
					lngCountW += 1;
				}
				rsData.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalW.Text = lngCountW.ToString();
			fldTotalS.Text = lngCountS.ToString();
		}

		private void arMeterCountNoCat_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arMeterCountNoCat properties;
			//arMeterCountNoCat.Caption	= "Meters Missing Category";
			//arMeterCountNoCat.Icon	= "arMeterCountNoCat.dsx":0000";
			//arMeterCountNoCat.Left	= 0;
			//arMeterCountNoCat.Top	= 0;
			//arMeterCountNoCat.Width	= 11700;
			//arMeterCountNoCat.Height	= 7500;
			//arMeterCountNoCat.WindowState	= 2;
			//arMeterCountNoCat.SectionData	= "arMeterCountNoCat.dsx":058A;
			//End Unmaped Properties
		}
	}
}
