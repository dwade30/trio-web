﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTEditBillInfo.
	/// </summary>
	public partial class frmUTEditBillInfo : BaseForm
	{
		public frmUTEditBillInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.txtGetAccountNumber.AllowOnlyNumericInput();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTEditBillInfo InstancePtr
		{
			get
			{
				return (frmUTEditBillInfo)Sys.GetInstance(typeof(frmUTEditBillInfo));
			}
		}

		protected frmUTEditBillInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/15/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/17/2006              *
		// ********************************************************
		int lngYear;
		bool boolResultsScreen;
		// this is true when the result grid is being shown
		bool boolSearch;
		string strSearchString;
		bool boolDirty;
		int lngLastRow;
		string strCopyName1;
		string strCopyName2;
		string strCopyAddr1;
		string strCopyAddr2;
		string strCopyAddr3;
		string strCopyCity;
		string strCopyState;
		string strCopyZip;
		string strCopyZip4;
		string strCopyTName1;
		string strCopyTName2;
		string strCopyTAddr1;
		string strCopyTAddr2;
		string strCopyTAddr3;
		string strCopyTCity;
		string strCopyTState;
		string strCopyTZip;
		string strCopyTZip4;
		string strCopyMapLot;
		string strCopyBookPage;
		string strCopyStreetNumber = "";
		string strCopyStreetName;
		string strCopyBook;
		string strCopyRef1 = "";
		bool boolCopyUseRef1;
		// these constants are for the grid columns
		// edit grid
		int lngColEDYear;
		int lngColEDName1;
		int lngColEDName2;
		int lngColEDBillkey;
		// Dim lngColMapLot                As Long
		// search grid
		int lngColAcct;
		int lngColName;
		int lngColLocationNumber;
		int lngColLocation;
		int lngColMapLot;
		int lngColBookPage;
		int lngColStreetNumber;
		int lngColStreetName;
		int lngRowOwner;
		int lngRowSecondOwner;
		int lngRowAddress1;
		int lngRowAddress2;
		int lngRowAddress3;
		int lngRowCity;
		int lngRowState;
		int lngRowZip;
		int lngRowZip4;
		int lngRowTenant;
		int lngRowTenant2;
		int lngRowTAddress1;
		int lngRowTAddress2;
		int lngRowTAddress3;
		int lngRowTCity;
		int lngRowTState;
		int lngRowTZip;
		int lngRowTZip4;
		int lngRowMapLot;
		int lngRowBookPage;
		int lngRowStreetNumber;
		int lngRowStreetName;
		int lngRowBook;
		// Dim lngRowRef1                  As Long
		// Dim lngRowUseRef1OnLien         As Long
		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			if (boolResultsScreen)
			{
				vsSearch.Visible = false;
				lblSearchListInstruction.Visible = false;
				fraSearchCriteria.Visible = true;
				fraSearch.Visible = true;
				fraGetAccount.Visible = true;
				boolResultsScreen = false;
				mnuFileSave.Enabled = true;
				mnuProcessGetAccount.Enabled = true;
				cmdProcessGetAccount.Enabled = true;
				mnuProcessSearch.Enabled = true;
				cmdProcessSearch.Enabled = true;
			}
			else
			{
				cmbSearchType.Text = "Name";
				txtSearch.Text = "";
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int holdaccount = 0;
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsRE = new clsDRWrapper();
				string strSQL = "";
				int intAns;
				lblSearchListInstruction.Visible = false;
				TRYAGAIN:
				;
				if (Conversion.Val(txtGetAccountNumber.Text) != 0)
				{
					holdaccount = FCConvert.ToInt32(FCConvert.ToDouble(txtGetAccountNumber.Text));
					intError = 1;
					modUTStatusPayments.Statics.lngCurrentAccountUT = holdaccount;
					strSQL = "SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT)) + " AND BillStatus = 'B'";
					intError = 2;
					rsCL.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					intError = 3;
					if (rsCL.EndOfFile() != true && rsCL.BeginningOfFile() != true)
					{
						// Create Queries on the database for the next screen to use
						ShowEditAccount(ref modUTStatusPayments.Statics.lngCurrentAccountUT);
					}
					else
					{
						MessageBox.Show("The account selected does not have any billing records.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return;
					}
					intError = 50;
					// set the last account field in the registry to the current account
					if (cmbRE.Text == "Real Estate")
					{
						intError = 51;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "RELastAccountNumber", modMain.PadToString_6(ref StaticSettings.TaxCollectionValues.LastAccountRE, 6));
					}
					else
					{
						intError = 52;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "PPLastAccountNumber", modMain.PadToString_6(ref StaticSettings.TaxCollectionValues.LastAccountPP, 6));
					}
					intError = 53;
					mnuFileSave.Enabled = true;
					mnuProcessGetAccount.Enabled = true;
					cmdProcessGetAccount.Enabled = true;
					mnuProcessSearch.Enabled = true;
					cmdProcessSearch.Enabled = true;
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a valid account number.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get Account Error - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Unload();
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdGetAccountNumber, new System.EventArgs());
		}

		private void cmdSearch_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngSelection = 0;
				string strAcctList = "";
				string strLeftOverAccts = "";
				string SQL = "";
				clsDRWrapper rs = new clsDRWrapper();
				clsDRWrapper rsBook = new clsDRWrapper();
				clsDRWrapper rsUTAccounts = new clsDRWrapper();
				string strList = "";
				string strBook = "";
				string strLastName = "";
				string strDoubleSearch = "";
				string strMasterQry;
				// find out which search option has been checked
				if (cmbSearchType.Text == "Name")
				{
					lngSelection = 0;
				}
				else if (cmbSearchType.Text == "Street Name")
				{
					lngSelection = 1;
				}
				else if (cmbSearchType.Text == "Map / Lot")
				{
					lngSelection = 2;
					//} else if (optSearchType[3].Checked==true) { //FC:FINAL:DDU - there's no 3 index anywhere
					//	lngSelection = 3;
				}
				else
				{
					MessageBox.Show("You must select a field to search by from the 'Search By' box.", "Search By", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				// make sure that the user typed search criteria in
				if (Strings.Trim(txtSearch.Text) == "")
				{
					MessageBox.Show("You must type a search criteria in the white box.", "Search Criteria", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
				//Application.DoEvents();

                strMasterQry =
                    "SELECT Master.*, DeedName1 AS OwnerName, DeedName2 AS SecondOwnerName, pBill.FullNameLF AS Name, pBill2.FullNameLF AS Name2 " +
                    "FROM Master INNER JOIN " + modMain.Statics.strDbCP +
                    "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP +
                    "PartyNameView pBill ON pBill.ID = Master.BillingPartyID " + "LEFT JOIN " +
                    modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID LEFT JOIN " +
                    modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID";
                switch (lngSelection)
				{
					case 0:
						{
							// Name
							// check for the type of account to look for Real Estate or Personal Property
							SQL = "SELECT * FROM Bill INNER JOIN (SELECT ID AS BK FROM Bill WHERE BillStatus = 'B' AND ((OName >= '" + txtSearch.Text + "  ' AND OName < '" + txtSearch.Text + "ZZZ') OR (OName2 >= '" + txtSearch.Text + "  ' AND OName2 < '" + txtSearch.Text + "ZZZ'))) AS List ON Bill.ID = List.BK ORDER BY OName";
							// this will create a string of accounts that will be added in the grid as well as the RE matches (for old collection files with that name)
							rsUTAccounts.OpenRecordset(SQL, modExtraModules.strUTDatabase);
							strAcctList = ",";
							while (!rsUTAccounts.EndOfFile())
							{
								strAcctList += rsUTAccounts.Get_Fields_Int32("ActualAccountNumber") + ",";
								rsUTAccounts.MoveNext();
							}

							SQL = "SELECT *, AccountNumber AS Account, OwnerName AS OName FROM (" + strMasterQry + ") AS qMstr WHERE (Name >= '" + txtSearch.Text + "' AND Name < '" + txtSearch.Text + "ZZZ') OR (SecondOwnerName >= '" + txtSearch.Text + "' AND SecondOwnerName < '" + txtSearch.Text + "ZZZ') ORDER BY Name, AccountNumber";
							break;
						}
					case 1:
						{
							// Street Name
							rsUTAccounts.OpenRecordset("SELECT * FROM Bill WHERE Bill = 0", modExtraModules.strUTDatabase);
							SQL = "SELECT *, AccountNumber AS Account, OwnerName AS OName FROM (" + strMasterQry + ") AS qMstr WHERE (StreetName >= '" + txtSearch.Text + "' AND StreetName < '" + txtSearch.Text + "ZZZ') ORDER BY StreetName, StreetNumber, AccountNumber";
							break;
						}
					case 2:
						{
							// Map Lot
							rsUTAccounts.OpenRecordset("SELECT * FROM Bill WHERE Bill = 0", modExtraModules.strUTDatabase);
							SQL = "SELECT *, AccountNumber AS Account, OwnerName AS OName FROM (" + strMasterQry + ") AS qMstr WHERE (MapLot >= '" + txtSearch.Text + "' AND MapLot < '" + txtSearch.Text + "ZZZ') ORDER BY MapLot, AccountNumber";
							break;
						}
				}
				//end switch
				rs.OpenRecordset(SQL, modExtraModules.strUTDatabase);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching", true, rs.RecordCount(), true);
					this.Refresh();
					rs.MoveLast();
					rs.MoveFirst();
					// clear the listbox
					vsSearch.Rows = 1;
					FormatSearchGrid();
					// fill the listbox with all the records returned
					while (!rs.EndOfFile())
					{
						frmWait.InstancePtr.IncrementProgress();
						//Application.DoEvents();
						if (lngSelection == 0)
						{
							// is this from CL and RE
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (rs.Get_Fields("Account") != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
							{
								vsSearch.AddItem("");
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields("Account")));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, strBook);
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rs.Get_Fields_String("StreetName")));
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocationNumber, Strings.Trim(FCConvert.ToString(rs.Get_Fields("StreetNumber"))));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MAPLOT"))));
								if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("OName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("OName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
								{
									// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rs.Get_Fields_String("OName") + " & " + rs.Get_Fields("SecondOwnerName"));
								}
								else
								{
									// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rs.Get_Fields_String("OName") + ", " + rs.Get_Fields("SecondOwnerName"));
								}
							}
						}
						else
						{
							// data from RE
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							if (rs.Get_Fields("Account") != Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct), 6)))
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								strList = rs.Get_Fields("Account") + "\t" + rs.Get_Fields_String("Name") + "\t" + Strings.Trim(FCConvert.ToString(rs.Get_Fields("StreetNumber"))) + "\t" + rs.Get_Fields_String("StreetName") + "\t" + strBook + "\t" + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MAPLOT")));
								vsSearch.AddItem("");
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rs.Get_Fields("Account")));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, strBook);
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rs.Get_Fields_String("StreetName")));
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocationNumber, Strings.Trim(FCConvert.ToString(rs.Get_Fields("StreetNumber"))));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("MAPLOT"))));
								if (lngSelection == 0)
								{
									if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("Name"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("Name"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
									{
										// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
										vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsUTAccounts.Get_Fields_String("Name") + " & " + rsUTAccounts.Get_Fields("SecondOwnerName"));
									}
									else
									{
										// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
										vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsUTAccounts.Get_Fields("SecondOwnerName") + ", " + rsUTAccounts.Get_Fields_String("Name"));
									}
								}
								else
								{
									if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("Name"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("Name"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
									{
										// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
										vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rs.Get_Fields_String("Name") + " & " + rs.Get_Fields("SecondOwnerName"));
									}
									else
									{
										// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
										vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rs.Get_Fields("SecondOwnerName") + ", " + rs.Get_Fields_String("Name"));
									}
								}
								// vsSearch.TextMatrix(vsSearch.rows - 1, lngColName) = Trim(.Fields("Name") & " " & .Fields("RSSECOWNER"))
							}
						}
						rs.MoveNext();
					}
					// now check all of the billing records to see if any did not match the master record,
					// if they did not, then add a row for that bill as well
					rsUTAccounts.MoveFirst();
					while (!rsUTAccounts.EndOfFile())
					{
						rs.FindFirstRecord("Account", rsUTAccounts.Get_Fields_Int32("ActualAccountNumber"));
						if (!rs.NoMatch)
						{
							if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName")))) == Strings.Trim(Strings.UCase(FCConvert.ToString(rs.Get_Fields_String("Name")))) || Strings.Trim(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName")))) == Strings.Trim(Strings.UCase(strLastName)))
							{
								// if this bill has the same name as its master account or has the
								// same name as the last bill then do nothing
							}
							else
							{
								// add the row
								vsSearch.AddItem("");
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsUTAccounts.Get_Fields_Int32("ActualAccountNumber")));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, FCConvert.ToString(rsUTAccounts.Get_Fields_String("BookPage")));
								if (Conversion.Val(rsUTAccounts.Get_Fields_String("Location")) != 0)
								{
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Strings.Right(Strings.Trim(FCConvert.ToString(rsUTAccounts.Get_Fields_String("Location"))), Strings.Trim(FCConvert.ToString(rsUTAccounts.Get_Fields_String("Location"))).Length - Strings.InStr(1, Strings.Trim(FCConvert.ToString(rsUTAccounts.Get_Fields_String("Location"))), " ", CompareConstants.vbBinaryCompare)));
								}
								else
								{
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsUTAccounts.Get_Fields_String("Location")));
								}
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocationNumber, FCConvert.ToString(Conversion.Val(rsUTAccounts.Get_Fields_String("Location"))));
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rsUTAccounts.Get_Fields_String("MapLot"))));
								if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
								{
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsUTAccounts.Get_Fields_String("OName") + " & " + rsUTAccounts.Get_Fields_String("OName2"));
								}
								else
								{
									vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsUTAccounts.Get_Fields_String("OName2") + ", " + rsUTAccounts.Get_Fields_String("OName"));
								}
								// highlight the added rows that are only past owners
								vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
								strLastName = FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName"));
							}
						}
						else if (Strings.Trim(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName")))) != Strings.Trim(Strings.UCase(strLastName)))
						{
							// add the row
							vsSearch.AddItem("");
							vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAcct, FCConvert.ToString(rsUTAccounts.Get_Fields_Int32("ActualAccountNumber")));
							vsSearch.TextMatrix(vsSearch.Rows - 1, lngColBookPage, FCConvert.ToString(rsUTAccounts.Get_Fields_String("BookPage")));
							if (Conversion.Val(rsUTAccounts.Get_Fields_String("Location")) != 0)
							{
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, Strings.Right(Strings.Trim(FCConvert.ToString(rsUTAccounts.Get_Fields_String("Location"))), Strings.Trim(FCConvert.ToString(rsUTAccounts.Get_Fields_String("Location"))).Length - Strings.InStr(1, Strings.Trim(FCConvert.ToString(rsUTAccounts.Get_Fields_String("Location"))), " ", CompareConstants.vbBinaryCompare)));
							}
							else
							{
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, FCConvert.ToString(rsUTAccounts.Get_Fields_String("Location")));
							}
							vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocationNumber, FCConvert.ToString(Conversion.Val(rsUTAccounts.Get_Fields_String("Location"))));
							vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, Strings.Trim(FCConvert.ToString(rsUTAccounts.Get_Fields_String("MapLot"))));
							if ((Strings.CompareString(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName"))), ">=", Strings.UCase(txtSearch.Text) + "   ") && Strings.CompareString(Strings.UCase(FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName"))), "<=", Strings.UCase(txtSearch.Text + "ZZZ"))) || lngSelection != 0)
							{
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsUTAccounts.Get_Fields_String("OName") + " & " + rsUTAccounts.Get_Fields_String("OName2"));
							}
							else
							{
								vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, rsUTAccounts.Get_Fields_String("OName2") + ", " + rsUTAccounts.Get_Fields_String("OName"));
							}
							// highlight the added rows that are only past owners
							vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
							strLastName = FCConvert.ToString(rsUTAccounts.Get_Fields_String("OName"));
						}
						rsUTAccounts.MoveNext();
					}
					// set the height of the grid
					if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.HeightOriginal - vsSearch.TopOriginal) - 1000)
					{
						vsSearch.HeightOriginal = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
						vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
					}
					else
					{
						vsSearch.HeightOriginal = (this.HeightOriginal - vsSearch.TopOriginal) - 1000;
						vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
					}
					vsSearch.Visible = true;
					fraSearch.Visible = false;
					fraGetAccount.Visible = false;
					fraSearchCriteria.Visible = false;
					boolResultsScreen = true;
					mnuProcessGetAccount.Enabled = false;
					cmdProcessGetAccount.Enabled = false;
					mnuProcessSearch.Enabled = false;
					cmdProcessSearch.Enabled = false;
					if (lngSelection == 0)
					{
						// force the name order
						vsSearch.Col = lngColName;
						vsSearch.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					}
					frmWait.InstancePtr.Unload();
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No results were matched to the criteria.  Please try again.", "No RE Results", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Open Recordset ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Unload();
			}
		}

		public void cmdSearch_Click()
		{
			cmdSearch_Click(cmdSearch, new System.EventArgs());
		}

		private void frmUTEditBillInfo_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorTag
				// If FormExist(Me) Then Exit Sub
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				lblInstructions1.Text = "Please enter the account number or hit Enter to view the account shown";
				if (modMain.Statics.boolRE)
				{
					// set the menu options on for RE
					if (modExtraModules.IsThisCR())
					{
						if (modUTUseCR.Statics.gboolShowLastCLAccountInCR)
						{
							txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
						}
					}
					else
					{
						txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountRE);
					}
					cmbRE.Text = "Real Estate";
				}
				else
				{
					if (modExtraModules.IsThisCR())
					{
						if (modUTUseCR.Statics.gboolShowLastCLAccountInCR)
						{
							txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
						}
					}
					else
					{
						txtGetAccountNumber.Text = FCConvert.ToString(StaticSettings.TaxCollectionValues.LastAccountPP);
					}
					cmbRE.Text = "Personal Property";
				}
				ShowSearch();
				txtGetAccountNumber.SelectionStart = 0;
				txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
				if (txtGetAccountNumber.Visible && txtGetAccountNumber.Enabled)
				{
					txtGetAccountNumber.Focus();
				}
				FCGlobal.Screen.MousePointer = 0;
				//Application.DoEvents();
				return;
			}
			catch
			{
				// ErrorTag:
				FCGlobal.Screen.MousePointer = 0;
			}
		}

		private void frmUTEditBillInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (fraEditInfo.Visible)
			{
				switch (KeyCode)
				{
					case Keys.C:
						{
							if (Shift == 2)
							{
								CopyInformation();
							}
							break;
						}
					case Keys.V:
						{
							if (Shift == 2)
							{
								PasteInformation();
							}
							break;
						}
				}
				//end switch
			}
		}

		private void frmUTEditBillInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				if (vsSearch.Visible == true)
				{
					vsSearch.Visible = false;
					fraSearchCriteria.Visible = true;
					fraSearch.Visible = true;
					fraGetAccount.Visible = true;
					lblSearchListInstruction.Visible = false;
					boolSearch = false;
				}
				else if (fraEditInfo.Visible)
				{
					fraEditInfo.Visible = false;
					fraSearch.Visible = true;
					fraSearchCriteria.Visible = true;
					fraGetAccount.Visible = true;
					lblSearchListInstruction.Visible = false;
					boolSearch = false;
				}
				else
				{
					KeyAscii = (Keys)0;
					this.Unload();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmUTEditBillInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTEditBillInfo properties;
			//frmUTEditBillInfo.ScaleWidth	= 9045;
			//frmUTEditBillInfo.ScaleHeight	= 7305;
			//frmUTEditBillInfo.LinkTopic	= "Form1";
			//frmUTEditBillInfo.LockControls	= true;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				// this just sets the columns constant values
				// edit grid
				lngColEDBillkey = 0;
				lngColEDYear = 1;
				lngColEDName1 = 2;
				lngColEDName2 = 3;
				// search grid
				lngColAcct = 0;
				lngColName = 1;
				lngColLocationNumber = 2;
				lngColLocation = 3;
				lngColBookPage = 4;
				lngColMapLot = 5;
				lngRowOwner = 0;
				lngRowSecondOwner = 1;
				lngRowAddress1 = 2;
				lngRowAddress2 = 3;
				lngRowAddress3 = 4;
				lngRowCity = 5;
				lngRowState = 6;
				lngRowZip = 7;
				lngRowZip4 = 8;
				lngRowTenant = 9;
				lngRowTenant2 = 10;
				lngRowTAddress1 = 11;
				lngRowTAddress2 = 12;
				lngRowTAddress3 = 13;
				lngRowTCity = 14;
				lngRowTState = 15;
				lngRowTZip = 16;
				lngRowTZip4 = 17;
				lngRowMapLot = 18;
				lngRowBookPage = 19;
				lngRowStreetNumber = 20;
				lngRowStreetName = 21;
				lngRowBook = 22;
				// lngRowRef1 = 9
				// lngRowUseRef1OnLien = 10
				strSearchString = "";
				if (modMain.Statics.boolRE)
				{
					cmbSearchType.Clear();
					cmbSearchType.Items.Add("Name");
					cmbSearchType.Items.Add("Street Name");
					cmbSearchType.Items.Add("Map / Lot");
				}
				else
				{
					cmbSearchType.Clear();
					cmbSearchType.Items.Add("Name");
					cmbSearchType.Items.Add("Street Name");
					cmbSearchType.Text = "Name";
				}
				modGlobalFunctions.SetTRIOColors(this);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Form Load Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDirty)
			{
				switch (MessageBox.Show("Changes have been made, would you like to save before exiting?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information))
				{
					case DialogResult.Yes:
						{
							SaveInformation();
							break;
						}
					case DialogResult.No:
						{
							// exit w/o doing anything
							break;
						}
					case DialogResult.Cancel:
						{
							e.Cancel = true;
							break;
						}
				}
				//end switch
			}
		}

		private void frmUTEditBillInfo_Resize(object sender, System.EventArgs e)
		{
			if (fraSearch.Visible == true && vsSearch.Visible == false)
			{
				ShowSearch();
			}
			else if (vsSearch.Visible == true)
			{
				FormatSearchGrid();
				// set the height of the grid
				if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.HeightOriginal - vsSearch.TopOriginal) - 1000)
				{
					vsSearch.HeightOriginal = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
				else
				{
					vsSearch.HeightOriginal = Math.Abs((this.HeightOriginal - vsSearch.TopOriginal) - 1000);
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
			}
			else
			{
				AdjustGridHeight();
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInformation();
		}

		private void mnuProcessClearLienElig_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsE = new clsDRWrapper();
				rsE.OpenRecordset("SELECT * FROM Bill WHERE ID = " + vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey), modExtraModules.strUTDatabase);
				if (!rsE.EndOfFile())
				{
					rsE.Edit();
					if (FCConvert.ToInt32(rsE.Get_Fields_Int32("SLienRecordNumber")) == 0)
					{
						rsE.Set_Fields("SLienStatusEligibility", 0);
						rsE.Set_Fields("SLienProcessStatus", 0);
					}
					if (FCConvert.ToInt32(rsE.Get_Fields_Int32("WLienRecordNumber")) == 0)
					{
						rsE.Set_Fields("WLienStatusEligibility", 0);
						rsE.Set_Fields("WLienProcessStatus", 0);
					}
					rsE.Update();
					MessageBox.Show("Lien eligibiliy cleared.", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Clearing Lien Eligibility", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsEditInfo_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			boolDirty = true;
		}

		private void vsEditInfo_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void vsEditInfo_Enter(object sender, System.EventArgs e)
		{
			if (vsEditInfo.Col > 0)
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsEditInfo.EditCell();
			}
			else
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			vsEditInfo.Row = 1;
			vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsEditInfo_KeyDownEdit(object sender, KeyEventArgs e)
		{
			// Select Case KeyCode
			// Case vbKeyC
			// If Shift = 2 And Col > 0 Then
			// KeyCode = 0
			// CopyInformation Row
			// End If
			// End Select
		}

		private void vsEditInfo_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			int lngMR;
			lngMR = vsEditInfo.MouseRow;
			//FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
			if (e.ColumnIndex > -1 && e.RowIndex > -1)
			{
				DataGridViewCell cell = vsEditInfo[e.ColumnIndex, e.RowIndex];
				if (lngMR == lngRowAddress1 || lngMR == lngRowAddress2 || lngMR == lngRowAddress3)
				{
					cell.ToolTipText = "";
				}
				else if (lngMR == lngRowBookPage)
				{
					cell.ToolTipText = "Book and Page";
				}
				else if (lngMR == lngRowMapLot)
				{
					cell.ToolTipText = "Map and Lot";
				}
				else if (lngMR == lngRowOwner)
				{
					cell.ToolTipText = "";
					// Case lngRowRef1
					// .ToolTipText = "Reference 1"
				}
				else if (lngMR == lngRowSecondOwner)
				{
					cell.ToolTipText = "";
				}
				else if (lngMR == lngRowStreetName)
				{
					cell.ToolTipText = "";
				}
				else if (lngMR == lngRowBook)
				{
					cell.ToolTipText = "Book or Route";
					// Case lngRowStreetNumber
					// .ToolTipText = ""
					// Case lngRowUseRef1OnLien
					// .ToolTipText = "Use the Ref 1 field instead of the Book and Page fields on 30 DN and Liens."
				}
				else
				{
					cell.ToolTipText = "";
				}
			}
		}

		private void vsEditInfo_RowColChange(object sender, System.EventArgs e)
		{
			if (vsEditInfo.Col > 0)
			{
				if (vsEditInfo.Row == lngRowTState || vsEditInfo.Row == lngRowState)
				{
					vsEditInfo.EditMaxLength = 2;
				}
				else if (vsEditInfo.Row == lngRowZip || vsEditInfo.Row == lngRowTZip)
				{
					vsEditInfo.EditMaxLength = 5;
				}
				else if (vsEditInfo.Row == lngRowZip4 || vsEditInfo.Row == lngRowTZip4)
				{
					vsEditInfo.EditMaxLength = 4;
				}
				else
				{
					vsEditInfo.EditMaxLength = 50;
				}
				// Select Case .Row
				// Case lngRowUseRef1OnLien
				// .ComboList = "1;Yes|2;No"
				// Case Else
				// .ComboList = ""
				// End Select
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsEditInfo.EditCell();
			}
			else
			{
				vsEditInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsEditInfo.Row == vsEditInfo.Rows - 1)
			{
				vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsEditInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsSearch_AfterUserResize(object sender, EventArgs e)
		{
			int intCT;
			int lngWid = 0;
			for (intCT = 0; intCT <= vsSearch.Cols - 1; intCT++)
			{
				lngWid += vsSearch.ColWidth(intCT);
			}
			if (lngWid > vsSearch.Width)
			{
				vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
				// set the height of the grid
				if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.HeightOriginal - vsSearch.TopOriginal) - 1000)
				{
					vsSearch.HeightOriginal = ((vsSearch.Rows + 1) * vsSearch.RowHeight(0));
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
				}
				else
				{
					vsSearch.HeightOriginal = (this.HeightOriginal - vsSearch.TopOriginal) - 1000;
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
				}
			}
			else
			{
				// set the height of the grid
				if ((vsSearch.Rows * vsSearch.RowHeight(0)) + 70 < (this.HeightOriginal - vsSearch.TopOriginal) - 1000 )
				{
					vsSearch.HeightOriginal = (vsSearch.Rows * vsSearch.RowHeight(0)) + 70;
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
				else
				{
					vsSearch.HeightOriginal = (this.HeightOriginal - vsSearch.TopOriginal) - 1000;
					vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
			}
		}

		private void vsSearch_BeforeSort(object sender, DataGridViewCellMouseEventArgs e)
		{
			if (vsSearch.Col == lngColLocation || vsSearch.Col == lngColLocationNumber)
			{
				if (vsSearch.Rows > 1)
				{
					vsSearch.Select(1, lngColLocationNumber, 1, lngColLocation);
				}
			}
		}

		private void vsSearch_ClickEvent(object sender, System.EventArgs e)
		{
			txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
		}

		private void vsSearch_DblClick(object sender, System.EventArgs e)
		{
			if (vsSearch.Row > 0)
			{
				txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
				cmdGetAccountNumber_Click();
			}
		}

		private void vsSearch_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// captures the return key to accept the account highlighted in the listbox
			if (e.KeyCode == Keys.Return)
			{
				cmdGetAccountNumber_Click();
			}
		}

		private void mnuProcessClearSearch_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuProcessGetAccount_Click(object sender, System.EventArgs e)
		{
			if (fraEditInfo.Visible)
			{
				// this is a save and exit
				SaveInformation();
                //FC:FINAL:AKV:#3751 - Leave form open after saving
                //this.Unload();
			}
			else
			{
				if (Strings.Trim(txtSearch.Text) != "")
				{
					cmdSearch_Click();
				}
				else
				{
					cmdGetAccountNumber_Click();
				}
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSearch_Click(object sender, System.EventArgs e)
		{
			if (fraGetAccount.Visible)
			{
				cmdSearch_Click();
			}
			else
			{
				// go back to that screen
				if (boolDirty)
				{
					switch (MessageBox.Show("Changes have been made.  Would you like to save the information?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								SaveInformation();
								break;
							}
						case DialogResult.No:
							{
								// do nothing
								break;
							}
						case DialogResult.Cancel:
							{
								return;
							}
					}
					//end switch
				}
				mnuProcessGetAccount.Text = "Process";
				cmdProcessGetAccount.Text = "Process";
				cmdProcessGetAccount.Width = 100;
				mnuProcessClearSearch.Visible = true;
				cmdProcessClearSearch.Visible = true;
				fraSearch.Visible = true;
				fraGetAccount.Visible = true;
				fraSearchCriteria.Visible = true;
				fraEditInfo.Visible = false;
			}
		}

		private void optPP_CheckedChanged(object sender, System.EventArgs e)
		{
			if (modExtraModules.IsThisCR() && !modUTUseCR.Statics.gboolShowLastCLAccountInCR)
			{
			}
			else
			{
				txtGetAccountNumber.Text = FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT);
			}
		}

		private void optRE_CheckedChanged(object sender, System.EventArgs e)
		{
			if (modExtraModules.IsThisCR() && !modUTUseCR.Statics.gboolShowLastCLAccountInCR)
			{
			}
			else
			{
				txtGetAccountNumber.Text = FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT);
			}
		}

		private void txtGetAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdGetAccountNumber_Click();
			}
		}

		private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii < Keys.D0 || KeyAscii > Keys.D9)
			{
				if (KeyAscii == Keys.Back)
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Return)
			{
				KeyCode = (Keys)0;
				cmdSearch_Click();
			}
		}

		private void ShowSearch()
		{
			// this will align the Search Screen and Show it
			boolSearch = true;
			//fraSearch.Left = FCConvert.ToInt32((this.Width - fraSearch.Width) / 2.0);
			//fraSearch.Top = FCConvert.ToInt32((this.Height - fraSearch.Height) / 2.0);
			fraSearch.Visible = true;
		}

		private void vsSearch_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsSearch[e.ColumnIndex, e.RowIndex];
            int lngR;
			int lngC;
			lngC = vsSearch.GetFlexColIndex(e.ColumnIndex);
			lngR = vsSearch.GetFlexRowIndex(e.RowIndex);
			// vssearch.ToolTipText =
		}

		private void vsSearch_RowColChange(object sender, System.EventArgs e)
		{
			if (Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6)) != 0)
			{
				txtGetAccountNumber.Text = Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAcct), 6);
			}
		}

		private void FormatSearchGrid()
		{
			int lngWid;
			vsSearch.Cols = 6;
			// set the height of the grid
			vsSearch.Height = FCConvert.ToInt32(this.Height * 0.8);
			vsSearch.Width = FCConvert.ToInt32(this.Width * 0.9);
			vsSearch.Left = FCConvert.ToInt32(((this.Width - vsSearch.Width) / 2.0));
			vsSearch.Top = FCConvert.ToInt32(this.Height * 0.1);
			vsSearch.ExtendLastCol = true;
			lngWid = vsSearch.WidthOriginal;
			// set the column widths
			if (modMain.Statics.boolRE)
			{
				vsSearch.ColWidth(lngColAcct, FCConvert.ToInt32(lngWid * 0.09));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.32));
				vsSearch.ColWidth(lngColLocationNumber, FCConvert.ToInt32(lngWid * 0.06));
				vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.31));
				vsSearch.ColWidth(lngColBookPage, 0);
				vsSearch.ColWidth(lngColMapLot, FCConvert.ToInt32(lngWid * 0.18));
			}
			else
			{
				vsSearch.ColWidth(lngColAcct, FCConvert.ToInt32(lngWid * 0.09));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.4));
				vsSearch.ColWidth(lngColLocationNumber, FCConvert.ToInt32(lngWid * 0.06));
				vsSearch.ColWidth(lngColLocation, FCConvert.ToInt32(lngWid * 0.36));
				vsSearch.ColWidth(lngColBookPage, 0);
				vsSearch.ColWidth(lngColMapLot, 0);
			}
			vsSearch.ColAlignment(lngColAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColLocationNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColBookPage, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.ColAlignment(lngColMapLot, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSearch.TextMatrix(0, lngColAcct, "Account");
			vsSearch.TextMatrix(0, lngColName, "Name");
			vsSearch.TextMatrix(0, lngColLocationNumber, "#");
			vsSearch.TextMatrix(0, lngColLocation, "Street");
			// vsSearch.TextMatrix(0, lngColBookPage) = "Book/Page"
			// vsSearch.TextMatrix(0, lngColMapLot) = "Map/Lot"
			lblSearchListInstruction.Text = "Select an account by double clicking or pressing 'Enter' while the account is highlighted";
			lblSearchListInstruction.Top = 30;
			//lblSearchListInstruction.Width = lngWid;
			lblSearchListInstruction.AutoSize = true;
			lblSearchListInstruction.Left = vsSearch.Left;
			lblSearchListInstruction.Visible = true;
		}

		private void ShowEditAccount(ref int lngShowAccount)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will show the years that this account has been billed
				// and let the user edit the name and address information
				clsDRWrapper rsAcct = new clsDRWrapper();
				vsYearInfo.Rows = 1;
				mnuFileSave.Visible = true;
				mnuProcessGetAccount.Text = "Save and Exit";
				cmdProcessGetAccount.Text = "Save and Exit";
				cmdProcessGetAccount.Width = 132;
				mnuProcessClearSearch.Visible = false;
				cmdProcessClearSearch.Visible = false;
				FormatGrids();
				rsAcct.OpenRecordset("SELECT Bill.ID AS Bill, BillingRateKey, OName, OName2 FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE Bill.AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngShowAccount)) + " AND BillStatus = 'B' ORDER BY BillingRateKey desc", modExtraModules.strUTDatabase);
				if (!rsAcct.EndOfFile())
				{
					while (!rsAcct.EndOfFile())
					{
						vsYearInfo.AddItem("");
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDBillkey, FCConvert.ToString(rsAcct.Get_Fields("Bill")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDYear, FCConvert.ToString(rsAcct.Get_Fields_Int32("BillingRateKey")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDName1, FCConvert.ToString(rsAcct.Get_Fields_String("OName")));
						vsYearInfo.TextMatrix(vsYearInfo.Rows - 1, lngColEDName2, FCConvert.ToString(rsAcct.Get_Fields_String("OName2")));
						rsAcct.MoveNext();
					}
					vsYearInfo.Select(1, 0, 1, vsYearInfo.Cols - 1);
					EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
					fraEditInfo.Visible = true;
					fraSearch.Visible = false;
					vsSearch.Visible = false;
					fraEditInfo.Text = "Edit Account " + FCConvert.ToString(lngShowAccount) + " Information";
				}
				else
				{
					MessageBox.Show("Cannot load account #" + FCConvert.ToString(lngShowAccount) + ".", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				AdjustGridHeight();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - ", "Error Showing Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatGrids()
		{
			// this will format the two grids vsYearInfo and vsEditinfo
			int wid = 0;
			wid = vsYearInfo.WidthOriginal;
			vsYearInfo.ExtendLastCol = true;
			vsYearInfo.ColWidth(lngColEDBillkey, 0);
			vsYearInfo.ColWidth(lngColEDYear, FCConvert.ToInt32(wid * 0.15));
			vsYearInfo.ColWidth(lngColEDName1, FCConvert.ToInt32(wid * 0.4));
			vsYearInfo.ColWidth(lngColEDName2, FCConvert.ToInt32(wid * 0.4));
			vsYearInfo.TextMatrix(0, lngColEDYear, "Bill");
			vsYearInfo.TextMatrix(0, lngColEDName1, "Owner");
			vsYearInfo.TextMatrix(0, lngColEDName2, "Second Owner");
			vsEditInfo.Cols = 2;
			vsEditInfo.Rows = 23;
			vsEditInfo.ColWidth(lngColEDBillkey, FCConvert.ToInt32(wid * 0.3));
			vsEditInfo.ColWidth(lngColEDYear, FCConvert.ToInt32(wid * 0.65));
			vsEditInfo.TextMatrix(lngRowOwner, 0, "Owner");
			vsEditInfo.TextMatrix(lngRowSecondOwner, 0, "Second Owner");
			vsEditInfo.TextMatrix(lngRowAddress1, 0, "Address 1");
			vsEditInfo.TextMatrix(lngRowAddress2, 0, "Address 2");
			vsEditInfo.TextMatrix(lngRowAddress3, 0, "Address 3");
			vsEditInfo.TextMatrix(lngRowCity, 0, "City");
			vsEditInfo.TextMatrix(lngRowState, 0, "State");
			vsEditInfo.TextMatrix(lngRowZip, 0, "Zip");
			vsEditInfo.TextMatrix(lngRowZip4, 0, "Zip4");
			vsEditInfo.TextMatrix(lngRowTenant, 0, "Tenant");
			vsEditInfo.TextMatrix(lngRowTenant2, 0, "Second Tenant");
			vsEditInfo.TextMatrix(lngRowTAddress1, 0, "Address 1");
			vsEditInfo.TextMatrix(lngRowTAddress2, 0, "Address 2");
			vsEditInfo.TextMatrix(lngRowTAddress3, 0, "Address 3");
			vsEditInfo.TextMatrix(lngRowTCity, 0, "City");
			vsEditInfo.TextMatrix(lngRowTState, 0, "State");
			vsEditInfo.TextMatrix(lngRowTZip, 0, "Zip");
			vsEditInfo.TextMatrix(lngRowTZip4, 0, "Zip4");
			vsEditInfo.TextMatrix(lngRowMapLot, 0, "Map Lot");
			vsEditInfo.TextMatrix(lngRowBookPage, 0, "Book Page");
			vsEditInfo.TextMatrix(lngRowStreetNumber, 0, "Street Number");
			vsEditInfo.TextMatrix(lngRowStreetName, 0, "Street Name");
			vsEditInfo.TextMatrix(lngRowBook, 0, "Book or Route");
		}
		// vbPorter upgrade warning: lngBK As int	OnWrite(string)
		private void EditBillInformation(int lngBK)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will show the information for the bill that has been selected
				clsDRWrapper rsAcct = new clsDRWrapper();
				rsAcct.OpenRecordset("SELECT OName, OName2, OAddress1, OAddress2, OAddress3, OCity, OState, OZip, OZip4, " + "BName, BName2, BAddress1, BAddress2, BAddress3, BCity, BState, BZip, BZip4, " + "Bill.MapLot, Bill.BookPage, Location, Bill.Book, SLienRecordNumber, WLienRecordNumber " + "FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE Bill.ID = " + FCConvert.ToString(lngBK) + " ORDER BY BillingRateKey desc", modExtraModules.strUTDatabase);
				if (!rsAcct.EndOfFile())
				{
					vsEditInfo.TextMatrix(lngRowOwner, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OName")));
					vsEditInfo.TextMatrix(lngRowSecondOwner, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OName2")));
					vsEditInfo.TextMatrix(lngRowAddress1, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OAddress1")));
					vsEditInfo.TextMatrix(lngRowAddress2, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OAddress2")));
					vsEditInfo.TextMatrix(lngRowAddress3, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OAddress3")));
					vsEditInfo.TextMatrix(lngRowCity, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OCity")));
					vsEditInfo.TextMatrix(lngRowState, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OState")));
					vsEditInfo.TextMatrix(lngRowZip, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OZip")));
					vsEditInfo.TextMatrix(lngRowZip4, 1, FCConvert.ToString(rsAcct.Get_Fields_String("OZip4")));
					vsEditInfo.TextMatrix(lngRowTenant, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BName")));
					vsEditInfo.TextMatrix(lngRowTenant2, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BName2")));
					vsEditInfo.TextMatrix(lngRowTAddress1, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BAddress1")));
					vsEditInfo.TextMatrix(lngRowTAddress2, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BAddress2")));
					vsEditInfo.TextMatrix(lngRowTAddress3, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BAddress3")));
					vsEditInfo.TextMatrix(lngRowTCity, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BCity")));
					vsEditInfo.TextMatrix(lngRowTState, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BState")));
					vsEditInfo.TextMatrix(lngRowTZip, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BZip")));
					vsEditInfo.TextMatrix(lngRowTZip4, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BZip4")));
					vsEditInfo.TextMatrix(lngRowMapLot, 1, FCConvert.ToString(rsAcct.Get_Fields_String("MapLot")));
					vsEditInfo.TextMatrix(lngRowBookPage, 1, FCConvert.ToString(rsAcct.Get_Fields_String("BookPage")));
					vsEditInfo.TextMatrix(lngRowStreetName, 1, FCConvert.ToString(rsAcct.Get_Fields_String("Location")));
					// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
					vsEditInfo.TextMatrix(lngRowBook, 1, FCConvert.ToString(rsAcct.Get_Fields("Book")));
					if (FCConvert.ToInt32(rsAcct.Get_Fields_Int32("SLienRecordNumber")) == 0 || FCConvert.ToInt32(rsAcct.Get_Fields_Int32("WLienRecordNumber")) == 0)
					{
						mnuProcessClearLienElig.Visible = true;
						cmdProcessClearLienElig.Visible = true;
					}
					else
					{
						mnuProcessClearLienElig.Visible = false;
						cmdProcessClearLienElig.Visible = false;
					}
					// .TextMatrix(lngRowRef1, 1) = rsAcct.Fields("Ref1")
					// If rsAcct.Fields("ShowRef1") Then
					// .TextMatrix(lngRowUseRef1OnLien, 1) = "Yes"
					// Else
					// .TextMatrix(lngRowUseRef1OnLien, 1) = "No"
					// End If
				}
				else
				{
					mnuProcessClearLienElig.Visible = false;
					cmdProcessClearLienElig.Visible = false;
					vsEditInfo.TextMatrix(lngRowOwner, 1, "");
					vsEditInfo.TextMatrix(lngRowSecondOwner, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress1, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress2, 1, "");
					vsEditInfo.TextMatrix(lngRowAddress3, 1, "");
					vsEditInfo.TextMatrix(lngRowCity, 1, "");
					vsEditInfo.TextMatrix(lngRowState, 1, "");
					vsEditInfo.TextMatrix(lngRowZip, 1, "");
					vsEditInfo.TextMatrix(lngRowZip4, 1, "");
					vsEditInfo.TextMatrix(lngRowTenant, 1, "");
					vsEditInfo.TextMatrix(lngRowTenant2, 1, "");
					vsEditInfo.TextMatrix(lngRowTAddress1, 1, "");
					vsEditInfo.TextMatrix(lngRowTAddress2, 1, "");
					vsEditInfo.TextMatrix(lngRowTAddress3, 1, "");
					vsEditInfo.TextMatrix(lngRowTCity, 1, "");
					vsEditInfo.TextMatrix(lngRowTState, 1, "");
					vsEditInfo.TextMatrix(lngRowTZip, 1, "");
					vsEditInfo.TextMatrix(lngRowTZip4, 1, "");
					vsEditInfo.TextMatrix(lngRowMapLot, 1, "");
					vsEditInfo.TextMatrix(lngRowBookPage, 1, "");
					// .TextMatrix(lngRowStreetNumber, 1) = ""
					vsEditInfo.TextMatrix(lngRowStreetName, 1, "");
					vsEditInfo.TextMatrix(lngRowBook, 1, "");
					// .TextMatrix(lngRowRef1, 1) = ""
					// .TextMatrix(lngRowUseRef1OnLien, 1) = ""
				}
				boolDirty = false;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Editing Bill Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsYearInfo_Enter(object sender, System.EventArgs e)
		{
			vsYearInfo.Row = 1;
			vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsYearInfo_RowColChange(object sender, System.EventArgs e)
		{
			if (vsYearInfo.Row > 0 && lngLastRow != vsYearInfo.Row)
			{
				if (boolDirty)
				{
					switch (MessageBox.Show("Changes have been made.  Would you like to save the information?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								SaveInformation();
								EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
								break;
							}
						case DialogResult.No:
							{
								EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
								break;
							}
						case DialogResult.Cancel:
							{
								// do nothing
								break;
							}
					}
					//end switch
				}
				else
				{
					EditBillInformation(FCConvert.ToInt32(vsYearInfo.TextMatrix(vsYearInfo.Row, lngColEDBillkey)));
				}
				lngLastRow = vsYearInfo.Row;
			}
			if (vsYearInfo.Row == vsYearInfo.Rows - 1)
			{
				vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsYearInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void SaveInformation()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strName1 = "";
				string strName2 = "";
				string strAddr1 = "";
				string strAddr2 = "";
				string strAddr3 = "";
				string strCity = "";
				string strState = "";
				string strZip = "";
				string strZip4 = "";
				string strTName1 = "";
				string strTName2 = "";
				string strTAddr1 = "";
				string strTAddr2 = "";
				string strTAddr3 = "";
				string strTCity = "";
				string strTState = "";
				string strTZip = "";
				string strTZip4 = "";
				string strMapLot = "";
				string strBookPage = "";
				// Dim lngStreetNumber     As Long
				string strStreetName = "";
				string strBook = "";
				// Dim strRef1             As String
				// Dim boolUseRef1         As Boolean
				clsDRWrapper rsAcct = new clsDRWrapper();
				vsEditInfo.Select(0, 0);
				// use lngLastRow to get the from from the grid, this is the last row selected and is the way that I will find the billkey
				if (lngLastRow > 0)
				{
					rsAcct.OpenRecordset("SELECT * FROM Bill WHERE ID = " + vsYearInfo.TextMatrix(lngLastRow, lngColEDBillkey), modExtraModules.strUTDatabase);
					if (!rsAcct.EndOfFile())
					{
						// this will save the information into the correct bill
						strName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowOwner, 1));
						strName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowSecondOwner, 1));
						strAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress1, 1));
						strAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress2, 1));
						strAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress3, 1));
						strCity = Strings.Trim(vsEditInfo.TextMatrix(lngRowCity, 1));
						strState = Strings.Trim(vsEditInfo.TextMatrix(lngRowState, 1));
						strZip = Strings.Trim(vsEditInfo.TextMatrix(lngRowZip, 1));
						strZip4 = Strings.Trim(vsEditInfo.TextMatrix(lngRowZip4, 1));
						strTName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTenant, 1));
						strTName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTenant2, 1));
						strTAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTAddress1, 1));
						strTAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTAddress2, 1));
						strTAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTAddress3, 1));
						strTCity = Strings.Trim(vsEditInfo.TextMatrix(lngRowTCity, 1));
						strTState = Strings.Trim(vsEditInfo.TextMatrix(lngRowTState, 1));
						strTZip = Strings.Trim(vsEditInfo.TextMatrix(lngRowTZip, 1));
						strTZip4 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTZip4, 1));
						strMapLot = Strings.Trim(vsEditInfo.TextMatrix(lngRowMapLot, 1));
						strBookPage = Strings.Trim(vsEditInfo.TextMatrix(lngRowBookPage, 1));
						// lngStreetNumber = Val("0" & Trim(vsEditInfo.TextMatrix(lngRowStreetNumber, 1)))
						strStreetName = Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetName, 1));
						strBook = Strings.Trim(vsEditInfo.TextMatrix(lngRowBook, 1));
						// strRef1 = Trim(vsEditInfo.TextMatrix(lngRowRef1, 1))
						// boolUseRef1 = CBool(Left(vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1), 1) = "Y")
						rsAcct.Edit();
						rsAcct.Set_Fields("OName", strName1);
						rsAcct.Set_Fields("OName2", strName2);
						rsAcct.Set_Fields("OAddress1", strAddr1);
						rsAcct.Set_Fields("OAddress2", strAddr2);
						rsAcct.Set_Fields("OAddress3", strAddr3);
						rsAcct.Set_Fields("OCity", strCity);
						rsAcct.Set_Fields("OState", strState);
						rsAcct.Set_Fields("OZip", strZip);
						rsAcct.Set_Fields("OZip4", strZip4);
						rsAcct.Set_Fields("BName", strTName1);
						rsAcct.Set_Fields("BName2", strTName2);
						rsAcct.Set_Fields("BAddress1", strTAddr1);
						rsAcct.Set_Fields("BAddress2", strTAddr2);
						rsAcct.Set_Fields("BAddress3", strTAddr3);
						rsAcct.Set_Fields("BCity", strTCity);
						rsAcct.Set_Fields("BState", strTState);
						rsAcct.Set_Fields("BZip", strTZip);
						rsAcct.Set_Fields("BZip4", strTZip4);
						rsAcct.Set_Fields("MapLot", strMapLot);
						rsAcct.Set_Fields("BookPage", strBookPage);
						// rsAcct.Fields("StreetNumber") = lngStreetNumber
						rsAcct.Set_Fields("Location", strStreetName);
						rsAcct.Set_Fields("Book", strBook);
						// rsAcct.Fields("Ref1") = strRef1
						// rsAcct.Fields("ShowRef1") = boolUseRef1
						rsAcct.Update();
						vsYearInfo.TextMatrix(lngLastRow, lngColEDName1, strName1);
						vsYearInfo.TextMatrix(lngLastRow, lngColEDName2, strName2);
						modGlobalFunctions.AddCYAEntry_26("UT", "Editing Bill information.", "Bill = " + vsYearInfo.TextMatrix(lngLastRow, lngColEDBillkey));
					}
					boolDirty = false;
				}
				MessageBox.Show("Save Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Bill", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CopyInformation(int lngRow = -1)
		{
			// this will copy the information into the virtual clipboard from the grid
			// Select Case lngRow
			// Case -1
			strCopyName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowOwner, 1));
			strCopyName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowSecondOwner, 1));
			strCopyAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress1, 1));
			strCopyAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress2, 1));
			strCopyAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowAddress3, 1));
			strCopyCity = Strings.Trim(vsEditInfo.TextMatrix(lngRowCity, 1));
			strCopyState = Strings.Trim(vsEditInfo.TextMatrix(lngRowState, 1));
			strCopyZip = Strings.Trim(vsEditInfo.TextMatrix(lngRowZip, 1));
			strCopyZip4 = Strings.Trim(vsEditInfo.TextMatrix(lngRowZip4, 1));
			strCopyTName1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTenant, 1));
			strCopyTName2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTenant2, 1));
			strCopyTAddr1 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTAddress1, 1));
			strCopyTAddr2 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTAddress2, 1));
			strCopyTAddr3 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTAddress3, 1));
			strCopyTCity = Strings.Trim(vsEditInfo.TextMatrix(lngRowTCity, 1));
			strCopyTState = Strings.Trim(vsEditInfo.TextMatrix(lngRowTState, 1));
			strCopyTZip = Strings.Trim(vsEditInfo.TextMatrix(lngRowTZip, 1));
			strCopyTZip4 = Strings.Trim(vsEditInfo.TextMatrix(lngRowTZip4, 1));
			strCopyMapLot = Strings.Trim(vsEditInfo.TextMatrix(lngRowMapLot, 1));
			strCopyBookPage = Strings.Trim(vsEditInfo.TextMatrix(lngRowBookPage, 1));
			// strCopyStreetNumber = Val("0" & Trim(vsEditInfo.TextMatrix(lngRowStreetNumber, 1)))
			strCopyStreetName = Strings.Trim(vsEditInfo.TextMatrix(lngRowStreetName, 1));
			strCopyBook = Strings.Trim(vsEditInfo.TextMatrix(lngRowBook, 1));
			// strCopyRef1 = Trim(vsEditInfo.TextMatrix(lngRowRef1, 1))
			// boolCopyUseRef1 = CBool(Left(vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1), 1) = "Y")
			// Case 0
			// strCopyName1 = Trim(vsEditInfo.EditSelText)
			// Case 1
			// strCopyName2 = Trim(vsEditInfo.EditSelText)
			// Case 2
			// strCopyAddr1 = Trim(vsEditInfo.EditSelText)
			// Case 3
			// strCopyAddr2 = Trim(vsEditInfo.EditSelText)
			// Case 4
			// strCopyAddr3 = Trim(vsEditInfo.EditSelText)
			// End Select
		}

		private void PasteInformation()
		{
			// this will paste the information into the grid from the virtual clipboard
			if (strCopyName1 != "" && (strCopyName2 != "" || strCopyAddr1 != "" || strCopyAddr2 != "" || strCopyAddr3 != ""))
			{
				vsEditInfo.TextMatrix(lngRowOwner, 1, strCopyName1);
				vsEditInfo.TextMatrix(lngRowSecondOwner, 1, strCopyName2);
				vsEditInfo.TextMatrix(lngRowAddress1, 1, strCopyAddr1);
				vsEditInfo.TextMatrix(lngRowAddress2, 1, strCopyAddr2);
				vsEditInfo.TextMatrix(lngRowAddress3, 1, strCopyAddr3);
				vsEditInfo.TextMatrix(lngRowCity, 1, strCopyCity);
				vsEditInfo.TextMatrix(lngRowState, 1, strCopyState);
				vsEditInfo.TextMatrix(lngRowZip, 1, strCopyZip);
				vsEditInfo.TextMatrix(lngRowZip4, 1, strCopyZip4);
				vsEditInfo.TextMatrix(lngRowTenant, 1, strCopyTName1);
				vsEditInfo.TextMatrix(lngRowTenant2, 1, strCopyTName2);
				vsEditInfo.TextMatrix(lngRowTAddress1, 1, strCopyTAddr1);
				vsEditInfo.TextMatrix(lngRowTAddress2, 1, strCopyTAddr2);
				vsEditInfo.TextMatrix(lngRowTAddress3, 1, strCopyTAddr3);
				vsEditInfo.TextMatrix(lngRowTCity, 1, strCopyTCity);
				vsEditInfo.TextMatrix(lngRowTState, 1, strCopyTState);
				vsEditInfo.TextMatrix(lngRowTZip, 1, strCopyTZip);
				vsEditInfo.TextMatrix(lngRowTZip4, 1, strCopyTZip4);
				vsEditInfo.TextMatrix(lngRowMapLot, 1, strCopyMapLot);
				vsEditInfo.TextMatrix(lngRowBookPage, 1, strCopyBookPage);
				// vsEditInfo.TextMatrix(lngRowStreetNumber, 1) = strCopyStreetNumber
				vsEditInfo.TextMatrix(lngRowStreetName, 1, strCopyStreetName);
				vsEditInfo.TextMatrix(lngRowBook, 1, strCopyBook);
				// vsEditInfo.TextMatrix(lngRowRef1, 1) = strCopyRef1
				// If boolCopyUseRef1 Then
				// vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1) = "Yes"
				// Else
				// vsEditInfo.TextMatrix(lngRowUseRef1OnLien, 1) = "No"
				// End If
			}
		}

		private void AdjustGridHeight()
		{
			//vsEditInfo.Height = (fraEditInfo.Height - vsEditInfo.Top) - 200 / 1440;
			//vsEditInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			// .Height = (.rows * .RowHeight(0)) + (.RowHeight(0) * 0.15)
			//if ((vsYearInfo.Rows * vsYearInfo.RowHeight(0)) + (vsYearInfo.RowHeight(0) * 0.15) + vsYearInfo.Top > vsEditInfo.Top - 100 / 1440)
			//{
			//    vsYearInfo.Height = vsEditInfo.Top - 100 / 1440 - vsYearInfo.Top;
			//    vsYearInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsYearInfo.Height = FCConvert.ToInt32((vsYearInfo.Rows * vsYearInfo.RowHeight(0)) + (vsYearInfo.RowHeight(0) * 0.15));
			//    vsYearInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}
	}
}
