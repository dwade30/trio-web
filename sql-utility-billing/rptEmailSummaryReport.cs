﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptEmailSummaryReport.
	/// </summary>
	public partial class rptEmailSummaryReport : BaseSectionReport
	{
		public rptEmailSummaryReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static rptEmailSummaryReport InstancePtr
		{
			get
			{
				return (rptEmailSummaryReport)Sys.GetInstance(typeof(rptEmailSummaryReport));
			}
		}

		protected rptEmailSummaryReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptEmailSummaryReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rs = new clsDRWrapper();
		public string strSQL = "";
		bool blnFirstRecord;
		// vbPorter upgrade warning: curWaterTotal As Decimal	OnWrite(short, Decimal)
		Decimal curWaterTotal;
		// vbPorter upgrade warning: curSewerTotal As Decimal	OnWrite(short, Decimal)
		Decimal curSewerTotal;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rs.MoveNext();
				eArgs.EOF = rs.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			curWaterTotal = 0;
			curSewerTotal = 0;
			rs.OpenRecordset(strSQL, "TWUT0000.vb1");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: curWater As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curWater;
			// vbPorter upgrade warning: curSewer As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curSewer;
			curWater = FCConvert.ToDecimal(rs.Get_Fields_Double("WPrinOwed") + rs.Get_Fields_Double("WTaxOwed"));
			curSewer = FCConvert.ToDecimal(rs.Get_Fields_Double("SPrinOwed") + rs.Get_Fields_Double("STaxOwed"));
			fldAcct.Text = FCConvert.ToString(rs.Get_Fields_Int32("ActualAccountNumber"));
			fldName.Text = rs.Get_Fields_String("BName");
			fldWater.Text = Strings.Format(curWater, "#,##0.00");
			fldSewer.Text = Strings.Format(curSewer, "#,##0.00");
			fldTotal.Text = Strings.Format(curWater + curSewer, "#,##0.00");
			curWaterTotal += curWater;
			curSewerTotal += curSewer;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldWTotal.Text = Strings.Format(curWaterTotal, "#,##0.00");
			fldSTotal.Text = Strings.Format(curSewerTotal, "#,##0.00");
			fldOverallTotal.Text = Strings.Format(curWaterTotal + curSewerTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptEmailSummaryReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptEmailSummaryReport properties;
			//rptEmailSummaryReport.Caption	= "ActiveReport1";
			//rptEmailSummaryReport.Left	= 0;
			//rptEmailSummaryReport.Top	= 0;
			//rptEmailSummaryReport.Width	= 20280;
			//rptEmailSummaryReport.Height	= 11115;
			//rptEmailSummaryReport.StartUpPosition	= 3;
			//rptEmailSummaryReport.SectionData	= "rptEmailSummaryReport.dsx":0000;
			//End Unmaped Properties
		}

		private void rptEmailSummaryReport_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
