﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmClearBooks.
	/// </summary>
	partial class frmClearBooks : BaseForm
	{
		public FCGrid vsBook;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblError;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuClearBooks;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmClearBooks));
			this.vsBook = new fecherFoundation.FCGrid();
			this.cmdClear = new fecherFoundation.FCButton();
			this.lblError = new fecherFoundation.FCLabel();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClearBooks = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdClear);
			this.BottomPanel.Location = new System.Drawing.Point(0, 572);
			this.BottomPanel.Size = new System.Drawing.Size(650, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsBook);
			this.ClientArea.Controls.Add(this.lblError);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(650, 512);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(650, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(146, 30);
			this.HeaderText.Text = "Clear Books";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// vsBook
			// 
			this.vsBook.AllowSelection = false;
			this.vsBook.AllowUserToResizeColumns = false;
			this.vsBook.AllowUserToResizeRows = false;
			this.vsBook.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsBook.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsBook.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsBook.BackColorBkg = System.Drawing.Color.Empty;
			this.vsBook.BackColorFixed = System.Drawing.Color.Empty;
			this.vsBook.BackColorSel = System.Drawing.Color.Empty;
			this.vsBook.Cols = 8;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsBook.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsBook.ColumnHeadersHeight = 30;
			this.vsBook.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsBook.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsBook.DragIcon = null;
			this.vsBook.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsBook.ExtendLastCol = true;
			this.vsBook.FixedCols = 0;
			this.vsBook.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsBook.FrozenCols = 0;
			this.vsBook.GridColor = System.Drawing.Color.Empty;
			this.vsBook.GridColorFixed = System.Drawing.Color.Empty;
			this.vsBook.Location = new System.Drawing.Point(30, 106);
			this.vsBook.Name = "vsBook";
			this.vsBook.ReadOnly = true;
			this.vsBook.RowHeadersVisible = false;
			this.vsBook.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsBook.RowHeightMin = 0;
			this.vsBook.Rows = 2;
			this.vsBook.ScrollTipText = null;
			this.vsBook.ShowColumnVisibilityMenu = false;
			this.vsBook.Size = new System.Drawing.Size(592, 389);
			this.vsBook.StandardTab = true;
			this.vsBook.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsBook.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsBook.TabIndex = 3;
			this.ToolTip1.SetToolTip(this.vsBook, null);
			this.vsBook.Click += new System.EventHandler(this.vsBook_ClickEvent);
			this.vsBook.DoubleClick += new System.EventHandler(this.vsBook_DblClick);
			// 
			// cmdClear
			// 
			this.cmdClear.AppearanceKey = "acceptButton";
			this.cmdClear.ForeColor = System.Drawing.Color.White;
			this.cmdClear.Location = new System.Drawing.Point(254, 30);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdClear.Size = new System.Drawing.Size(116, 48);
			this.cmdClear.TabIndex = 1;
			this.cmdClear.Text = "Clear Books";
			this.ToolTip1.SetToolTip(this.cmdClear, "Clear Selected Books");
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// lblError
			// 
			this.lblError.Location = new System.Drawing.Point(30, 68);
			this.lblError.Name = "lblError";
			this.lblError.Size = new System.Drawing.Size(368, 18);
			this.lblError.TabIndex = 2;
			this.lblError.Text = " ONLY SELECTIONS WITH A STATUS OF (B)ILLED CAN BE CLEARED";
			this.ToolTip1.SetToolTip(this.lblError, null);
			this.lblError.Visible = false;
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(566, 18);
			this.lblInstructions.TabIndex = 0;
			this.lblInstructions.Text = " SELECT THE BOOKS TO BE CLEARED BY CHECKING THE BOXES TO THE LEFT OF THE BOOK NUM" + "BER";
			this.ToolTip1.SetToolTip(this.lblInstructions, null);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuClearBooks,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuClearBooks
			// 
			this.mnuClearBooks.Index = 0;
			this.mnuClearBooks.Name = "mnuClearBooks";
			this.mnuClearBooks.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuClearBooks.Text = "Clear Books";
			this.mnuClearBooks.Click += new System.EventHandler(this.mnuClearBooks_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmClearBooks
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(650, 680);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmClearBooks";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Clear Books";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmClearBooks_Load);
			this.Activated += new System.EventHandler(this.frmClearBooks_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmClearBooks_KeyPress);
			this.Resize += new System.EventHandler(this.frmClearBooks_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
