﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arMeterDataExtract.
	/// </summary>
	public partial class arMeterDataExtract : BaseSectionReport
	{
		public arMeterDataExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Meters Missing Category";
		}

		public static arMeterDataExtract InstancePtr
		{
			get
			{
				return (arMeterDataExtract)Sys.GetInstance(typeof(arMeterDataExtract));
			}
		}

		protected arMeterDataExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arMeterDataExtract	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/26/2007              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/03/2007              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngCountW;
		int lngCountS;

		public void Init(ref string strPassSQL)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (strPassSQL != "")
				{
					rsData.OpenRecordset("SELECT * FROM MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key WHERE " + strPassSQL, modExtraModules.strUTDatabase);
				}
				else
				{
					rsData.OpenRecordset("SELECT * FROM MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key ", modExtraModules.strUTDatabase);
				}
				if (!rsData.EndOfFile())
				{
					frmReportViewer.InstancePtr.Init(this);
				}
				else
				{
					MessageBox.Show("No records returned.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (rsData.EndOfFile())
			{
				EndReport();
			}
		}

		private void EndReport()
		{
			// this routine will hide everything
			lblHeader.Visible = false;
			lblAccount.Visible = false;
			lblDesc.Visible = false;
			lblWaterHeader.Visible = false;
			lblSewerHeader.Visible = false;
			Line2.Visible = false;
			GroupHeader1.Height = 0;
			fldAcct.Visible = false;
			fldDesc.Visible = false;
			fldW.Visible = false;
			fldS.Visible = false;
			Detail.Height = 0;
			Line1.Visible = false;
			fldTotalCount.Visible = false;
			fldTotalS.Visible = false;
			fldTotalW.Visible = false;
			GroupFooter1.Height = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			if (!rsData.EndOfFile())
			{
				// this counts all of the meter records that have the same catagory as the current metersize
				//FC:FINAL:MSH - can't implicitly convert from int to string
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
				fldDesc.Text = rsData.Get_Fields_String("Name");
				if ((FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterCategory")) == 0 && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "S") && (FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerCategory")) == 0 && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "W"))
				{
					fldS.Text = "1";
					lngCountS += 1;
					fldW.Text = "1";
					lngCountW += 1;
				}
				else if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerCategory")) == 0 && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "W")
				{
					fldS.Text = "1";
					fldW.Text = "0";
					lngCountS += 1;
				}
				else if (FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterCategory")) == 0 && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "S")
				{
					fldW.Text = "1";
					fldS.Text = "0";
					lngCountW += 1;
				}
				rsData.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalW.Text = lngCountW.ToString();
			fldTotalS.Text = lngCountS.ToString();
		}

		private void arMeterDataExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arMeterDataExtract properties;
			//arMeterDataExtract.Caption	= "Meters Missing Category";
			//arMeterDataExtract.Icon	= "arMeterDataExtract.dsx":0000";
			//arMeterDataExtract.Left	= 0;
			//arMeterDataExtract.Top	= 0;
			//arMeterDataExtract.Width	= 11880;
			//arMeterDataExtract.Height	= 8010;
			//arMeterDataExtract.WindowState	= 2;
			//arMeterDataExtract.SectionData	= "arMeterDataExtract.dsx":058A;
			//End Unmaped Properties
		}
	}
}
