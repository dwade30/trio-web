﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDisconnectNoticeMailer7InchMF.
	/// </summary>
	partial class rptDisconnectNoticeMailer7InchMF
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDisconnectNoticeMailer7InchMF));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldBookSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNoticeDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldShutOffDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAsterisk = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblExtraText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoticeDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldShutOffDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExtraText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBookSequence,
				this.fldAccountNumber,
				this.lblText,
				this.fldNoticeDate,
				this.fldShutOffDate,
				this.fldWaterTotal,
				this.lblAsterisk,
				this.fldName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.Field2,
				this.Field3,
				this.lblExtraText,
				this.fldLocation,
				this.fldAddress5
			});
			this.Detail.Height = 7F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldBookSequence
			// 
			this.fldBookSequence.Height = 0.1875F;
			this.fldBookSequence.Left = 3.4375F;
			this.fldBookSequence.Name = "fldBookSequence";
			this.fldBookSequence.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldBookSequence.Text = null;
			this.fldBookSequence.Top = 0.5F;
			this.fldBookSequence.Width = 1F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 6.875F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAccountNumber.Text = "Acct#";
			this.fldAccountNumber.Top = 1.5F;
			this.fldAccountNumber.Width = 0.875F;
			// 
			// lblText
			// 
			this.lblText.Height = 0.1875F;
			this.lblText.Left = 0F;
			this.lblText.Name = "lblText";
			this.lblText.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.lblText.Text = null;
			this.lblText.Top = 5.4375F;
			this.lblText.Width = 8F;
			// 
			// fldNoticeDate
			// 
			this.fldNoticeDate.Height = 0.1875F;
			this.fldNoticeDate.Left = 6.875F;
			this.fldNoticeDate.Name = "fldNoticeDate";
			this.fldNoticeDate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldNoticeDate.Text = null;
			this.fldNoticeDate.Top = 0.8125F;
			this.fldNoticeDate.Width = 0.9375F;
			// 
			// fldShutOffDate
			// 
			this.fldShutOffDate.Height = 0.1875F;
			this.fldShutOffDate.Left = 3.375F;
			this.fldShutOffDate.Name = "fldShutOffDate";
			this.fldShutOffDate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldShutOffDate.Text = null;
			this.fldShutOffDate.Top = 2.375F;
			this.fldShutOffDate.Width = 1.1875F;
			// 
			// fldWaterTotal
			// 
			this.fldWaterTotal.Height = 0.1875F;
			this.fldWaterTotal.Left = 6.875F;
			this.fldWaterTotal.Name = "fldWaterTotal";
			this.fldWaterTotal.Style = "font-family: \'Roman 12cpi\'; text-align: right; ddo-char-set: 1";
			this.fldWaterTotal.Text = null;
			this.fldWaterTotal.Top = 1.125F;
			this.fldWaterTotal.Width = 0.9375F;
			// 
			// lblAsterisk
			// 
			this.lblAsterisk.Height = 0.1875F;
			this.lblAsterisk.HyperLink = null;
			this.lblAsterisk.Left = 7.75F;
			this.lblAsterisk.Name = "lblAsterisk";
			this.lblAsterisk.Style = "font-family: \'Roman 12cpi\'; font-size: 10pt; ddo-char-set: 1";
			this.lblAsterisk.Text = "*";
			this.lblAsterisk.Top = 1.125F;
			this.lblAsterisk.Width = 0.1875F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldName.Text = null;
			this.fldName.Top = 1F;
			this.fldName.Width = 3.4375F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 1F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress1.Text = null;
			this.fldAddress1.Top = 1.1875F;
			this.fldAddress1.Width = 3.4375F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 1F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress2.Text = null;
			this.fldAddress2.Top = 1.375F;
			this.fldAddress2.Width = 3.4375F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 1F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress3.Text = null;
			this.fldAddress3.Top = 1.5625F;
			this.fldAddress3.Width = 3.4375F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 1F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress4.Text = null;
			this.fldAddress4.Top = 1.75F;
			this.fldAddress4.Width = 3.4375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 2.65625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Field2.Text = "$25.00";
			this.Field2.Top = 5.125F;
			this.Field2.Width = 0.9375F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 3.09375F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.Field3.Text = "$50.00";
			this.Field3.Top = 5.25F;
			this.Field3.Width = 0.9375F;
			// 
			// lblExtraText
			// 
			this.lblExtraText.CanGrow = false;
			this.lblExtraText.Height = 0.65625F;
			this.lblExtraText.Left = 5.875F;
			this.lblExtraText.Name = "lblExtraText";
			this.lblExtraText.Style = "font-family: \'Roman 12cpi\'; text-align: left; ddo-char-set: 1";
			this.lblExtraText.Text = null;
			this.lblExtraText.Top = 1.6875F;
			this.lblExtraText.Width = 2F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 0.53125F;
			this.fldLocation.MultiLine = false;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Roman 12cpi\'; white-space: nowrap; ddo-char-set: 1";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0.5F;
			this.fldLocation.Width = 2.8125F;
			// 
			// fldAddress5
			// 
			this.fldAddress5.Height = 0.1875F;
			this.fldAddress5.Left = 1F;
			this.fldAddress5.Name = "fldAddress5";
			this.fldAddress5.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress5.Text = null;
			this.fldAddress5.Top = 1.9375F;
			this.fldAddress5.Width = 3.4375F;
			// 
			// rptDisconnectNoticeMailer7InchMF
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoticeDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldShutOffDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExtraText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoticeDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldShutOffDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAsterisk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblExtraText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
