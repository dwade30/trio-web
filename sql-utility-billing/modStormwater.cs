﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Diagnostics;

namespace TWUT0000
{
	public class modStormwater
	{
		//=========================================================
		// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		// XXXXXXXXXXXXXXXXXXX  SYNC WITH RE FOR STORMWATER BILLING       XXXXXXXXXXXXXXXX
		// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
		public struct AcctSyncRec
		{
			public int UTAcctNum;
			public int REAcctNum;
			public int REOwnID1;
			public int REOwnID2;
			public int UTOwnID1;
			public int UTOwnID2;
			public string Location;

            public string DeedName1;

            public string DeedName2;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public AcctSyncRec(int unusedParam)
			{
				this.UTAcctNum = 0;
				this.REAcctNum = 0;
				this.REOwnID1 = 0;
				this.REOwnID2 = 0;
				this.UTOwnID1 = 0;
				this.UTOwnID2 = 0;
				this.Location = string.Empty;
                this.DeedName1 = string.Empty;
                this.DeedName2 = string.Empty;
            }
		};

		public static void SyncWithREforStormwater(bool modalDialog)
		{
			clsDRWrapper rsRE = new clsDRWrapper();
			clsDRWrapper rsRECnt = new clsDRWrapper();
			clsDRWrapper rsUT = new clsDRWrapper();
			int lngRecCnt = 0;
			string strStreetName = "";
			string strStreetNum = "";
			int lngXBook = 0;
			bool boolChgOwner = false;
			string strTemp = "";
			int REOwn1ID = 0;
			int REOwn2ID = 0;
			int UTOwn1ID = 0;
			int UTOwn2ID = 0;
			int lngUpdCnt = 0;
			int lngMultiCnt = 0;
            string REDeedName1 = "";
            string REDeedName2 = "";
			rptUTREAcctSync rptUTRESync = null;
			rsUT.OpenRecordset("SELECT MAX(BookNumber) AS BookNumber FROM Book WHERE BookNumber >= 9000 AND BookNumber < 9100", modExtraModules.strUTDatabase);
			if (!rsUT.IsFieldNull("BookNumber") && FCConvert.ToInt32(rsUT.Get_Fields_Int32("BookNumber")) != 0)
			{
				lngXBook = rsUT.Get_Fields_Int32("BookNumber") + 1;
			}
			else
			{
				lngXBook = 9000;
			}
			rsUT.OpenRecordset("SELECT * FROM Book WHERE BookNumber = -1", modExtraModules.strUTDatabase);
			rsUT.AddNew();
			rsUT.Set_Fields("BookNumber", lngXBook);
			rsUT.Set_Fields("Description", "Added from Real Estate");
			rsUT.Set_Fields("CurrentStatus", "CE");
			rsUT.Set_Fields("CDate", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
			rsUT.Set_Fields("DDate", Strings.Format(DateAndTime.DateValue(FCConvert.ToString(0)), "MM/dd/yyyy"));
			rsUT.Set_Fields("XDate", Strings.Format(DateAndTime.DateValue(FCConvert.ToString(0)), "MM/dd/yyyy"));
			rsUT.Set_Fields("EDate", Strings.Format(DateAndTime.DateValue(FCConvert.ToString(0)), "MM/dd/yyyy"));
			rsUT.Set_Fields("BDate", Strings.Format(DateAndTime.DateValue(FCConvert.ToString(0)), "MM/dd/yyyy"));
			rsUT.Set_Fields("DateUpdated", Strings.Format(DateAndTime.DateValue(FCConvert.ToString(0)), "MM/dd/yyyy"));
			rsUT.Update();
			rsRE.OpenRecordset("SELECT RSAccount, RSLocNumAlph, RSLocApt, RSLocStreet, RSMapLot,DeedName1, DeedName2, OwnerPartyID, SecOwnerPartyID, " + "TaxAcquired, TADate, InBankruptcy FROM Master WHERE RSCard = 1 AND RSDeleted = 0 AND ISNULL(RSLocStreet, '') <> '' ORDER BY RSLocStreet, RSLocNumAlph", modExtraModules.strREDatabase);
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Synchronizing RE Data", true, rsRE.RecordCount(), true);
			while (!rsRE.EndOfFile())
			{
				frmWait.InstancePtr.IncrementProgress();
				//Application.DoEvents();
				REOwn1ID = FCConvert.ToInt32(rsRE.Get_Fields_Int32("OwnerPartyID"));
                REDeedName1 = rsRE.Get_Fields_String("DeedName1");
                REDeedName2 = rsRE.Get_Fields_String("DeedName2");
				if (!rsRE.IsFieldNull("SecOwnerPartyID") && FCConvert.ToInt32(rsRE.Get_Fields_Int32("SecOwnerPartyID")) != 0)
				{
					REOwn2ID = FCConvert.ToInt32(rsRE.Get_Fields_Int32("SecOwnerPartyID"));
				}
				else
				{
					REOwn2ID = 0;
				}
				strStreetName = modGlobalFunctions.EscapeQuotes(rsRE.Get_Fields_String("RSLocStreet"));
				strStreetNum = FCConvert.ToString(rsRE.Get_Fields_String("RSLocNumAlph"));
				if (Strings.Right(strStreetName, 1) == ".")
				{
					strStreetName = Strings.Left(strStreetName, strStreetName.Length - 1);
				}
				if (strStreetNum != "")
				{
					strTemp = " AND StreetNumber = '" + rsRE.Get_Fields_String("RSLocNumAlph") + "'";
				}
				else
				{
					strTemp = " AND StreetNumber = ''";
				}
				// xx rsUT.OpenRecordset "SELECT * FROM Master WHERE StreetName LIKE '" & strStreetName & "%'" & strTemp, strUTDatabase
				rsUT.OpenRecordset("SELECT * FROM Master WHERE StreetName = '" + strStreetName + "'" + strTemp, modExtraModules.strUTDatabase);
				if (rsUT.RecordCount() != 0)
				{
					rsRECnt.OpenRecordset("SELECT Count(ID) as Cnt FROM Master WHERE RSCard = 1 AND RSDeleted = 0 AND RSLocStreet = '" + modGlobalFunctions.EscapeQuotes(rsRE.Get_Fields_String("RSLocStreet")) + "' AND RSLocNumAlph = '" + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLocNumAlph"))) + "'", modExtraModules.strREDatabase);
					// TODO Get_Fields: Field [Cnt] not found!! (maybe it is an alias?)
					if (rsRECnt.Get_Fields("Cnt") > 1)
					{
						//Debug.WriteLine(Support.TabLayout(rsRE.Get_Fields("RSLocStreet")+"  "+Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("RSLocNumAlph")))+"  " + Strings.StrP(rsRECnt.Get_Fields("Cnt"))));
						// skip these but add to another report!
						lngMultiCnt += 1;
						Array.Resize(ref Statics.MultiREAcct, lngMultiCnt + 1);
						Statics.MultiREAcct[lngMultiCnt].REAcctNum = FCConvert.ToInt32(rsRE.Get_Fields_Int32("RSAccount"));
						Statics.MultiREAcct[lngMultiCnt].REOwnID1 = FCConvert.ToInt32(rsRE.Get_Fields_Int32("OwnerPartyID"));
						Statics.MultiREAcct[lngMultiCnt].REOwnID2 = FCConvert.ToInt32(rsRE.Get_Fields_Int32("SecOwnerPartyID"));
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						Statics.MultiREAcct[lngMultiCnt].UTAcctNum = FCConvert.ToInt32(rsUT.Get_Fields("AccountNumber"));
						Statics.MultiREAcct[lngMultiCnt].UTOwnID1 = FCConvert.ToInt32(rsUT.Get_Fields_Int32("OwnerPartyID"));
						Statics.MultiREAcct[lngMultiCnt].UTOwnID2 = FCConvert.ToInt32(rsUT.Get_Fields_Int32("SecondOwnerPartyID"));
						Statics.MultiREAcct[lngMultiCnt].Location = Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLocNumAlph"))) + " " + rsRE.Get_Fields_String("RSLocStreet");
                        Statics.MultiREAcct[lngMultiCnt].DeedName1 = REDeedName1;
                        Statics.MultiREAcct[lngMultiCnt].DeedName2 = REDeedName2;
                    }
					else
					{
						while (!rsUT.EndOfFile())
						{
							boolChgOwner = false;
							UTOwn1ID = 0;
							UTOwn2ID = 0;
							rsUT.Edit();
							if (CopyOwnerFromRE_6(REOwn1ID, rsUT.Get_Fields_Int32("OwnerPartyID")))
							{
								UTOwn1ID = FCConvert.ToInt32(rsUT.Get_Fields_Int32("OwnerPartyID"));
								rsUT.Set_Fields("OwnerPartyID", REOwn1ID);
								boolChgOwner = true;
							}
							if (REOwn2ID != 0 || (!rsUT.IsFieldNull("SecondOwnerPartyID") && FCConvert.ToInt32(rsUT.Get_Fields_Int32("SecondOwnerPartyID")) != 0))
							{
								if (CopyOwnerFromRE_6(REOwn2ID, rsUT.Get_Fields_Int32("SecondOwnerPartyID")))
								{
									UTOwn2ID = FCConvert.ToInt32(rsUT.Get_Fields_Int32("SecondOwnerPartyID"));
									rsUT.Set_Fields("SecondOwnerPartyID", REOwn2ID);
									boolChgOwner = true;
								}
							}
                            rsUT.Set_Fields("DeedName1", REDeedName1);
                            rsUT.Set_Fields("DeedName2",REDeedName2);
							rsUT.Set_Fields("MapLot", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot"))));
							rsUT.Set_Fields("REAccount", rsRE.Get_Fields_Int32("RSAccount"));
							rsUT.Update();
							if (boolChgOwner)
							{
								// ADD ACCOUNT INFO TO ARRAY FOR REPORT
								lngUpdCnt += 1;
								Array.Resize(ref Statics.AcctSyncRpt, lngUpdCnt + 1);
								Statics.AcctSyncRpt[lngUpdCnt].REAcctNum = FCConvert.ToInt32(rsRE.Get_Fields_Int32("RSAccount"));
								Statics.AcctSyncRpt[lngUpdCnt].REOwnID1 = REOwn1ID;
								Statics.AcctSyncRpt[lngUpdCnt].REOwnID2 = REOwn2ID;
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								Statics.AcctSyncRpt[lngUpdCnt].UTAcctNum = FCConvert.ToInt32(rsUT.Get_Fields("AccountNumber"));
								Statics.AcctSyncRpt[lngUpdCnt].UTOwnID1 = UTOwn1ID;
								Statics.AcctSyncRpt[lngUpdCnt].UTOwnID2 = UTOwn2ID;
								Statics.AcctSyncRpt[lngUpdCnt].Location = Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLocNumAlph"))) + " " + rsRE.Get_Fields_String("RSLocStreet");
                                Statics.AcctSyncRpt[lngUpdCnt].DeedName1 = REDeedName1;
                                Statics.AcctSyncRpt[lngUpdCnt].DeedName2 = REDeedName2;
                            }
							rsUT.MoveNext();
						}
					}
				}
				else
				{
					// No exact match, keep searching or just create one?
					if (Strings.UCase(Strings.Right(strStreetName, 6)) == "STREET")
					{
						// STREET -> ST
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 4);
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 3)) == " ST")
					{
						// ST -> STREET
						strStreetName = Strings.Trim(strStreetName) + "REET";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 5)) == "DRIVE")
					{
						// DRIVE -> DR
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 3);
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 3)) == " DR")
					{
						// DR -> DRIVE
						strStreetName = Strings.Trim(strStreetName) + "IVE";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 5)) == "PLACE")
					{
						// PLACE -> PL
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 3);
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 3)) == " PL")
					{
						// PL -> PLACE
						strStreetName = Strings.Trim(strStreetName) + "ACE";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 5)) == "TRAIL")
					{
						// TRAIL -> TR
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 3);
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 3)) == " TR")
					{
						// TR -> TRAIL
						strStreetName = Strings.Trim(strStreetName) + "AIL";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 6)) == "AVENUE")
					{
						// AVENUE -> AVE
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 3);
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 4)) == " AVE")
					{
						// AVE -> AVENUE
						strStreetName = Strings.Trim(strStreetName) + "NUE";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 6)) == "CIRCLE")
					{
						// CIRCLE -> CIR
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 3);
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 4)) == " CIR")
					{
						// CIR -> CIRCLE
						strStreetName = Strings.Trim(strStreetName) + "CLE";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 7)) == "ESTATES")
					{
						// ESTATES -> EST
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 4);
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 4)) == " EST")
					{
						// EST -> ESTATES
						strStreetName = Strings.Trim(strStreetName) + "ATES";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 4)) == "ROAD")
					{
						// ROAD -> RD
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 3) + "D";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 2)) == "RD")
					{
						// RD -> ROAD
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 1) + "OAD";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 4)) == "LANE")
					{
						// LANE -> LN
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 3) + "N";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 2)) == "LN")
					{
						// LN -> LANE
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 1) + "ANE";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 5)) == "COURT")
					{
						// COURT -> CT
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 4) + "T";
					}
					else if (Strings.UCase(Strings.Right(strStreetName, 2)) == "CT")
					{
						// CT -> COURT
						strStreetName = Strings.Left(strStreetName, strStreetName.Length - 1) + "OURT";
					}
					else
					{
						strStreetName = strStreetName;
					}
					// xx rsUT.OpenRecordset "SELECT * FROM Master WHERE StreetName LIKE '" & strStreetName & "%'" & strTemp, "twut0000.vb1"
					rsUT.OpenRecordset("SELECT * FROM Master WHERE StreetName = '" + strStreetName + "'" + strTemp, "twut0000.vb1");
					if (rsUT.RecordCount() != 0)
					{
						rsRECnt.OpenRecordset("SELECT Count(ID) as Cnt FROM Master WHERE RSCard = 1 AND RSDeleted = 0 AND RSLocStreet = '" + modGlobalFunctions.EscapeQuotes(rsRE.Get_Fields_String("RSLocStreet")) + "' AND RSLocNumAlph = '" + Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLocNumAlph"))) + "'", modExtraModules.strREDatabase);
						// TODO Get_Fields: Field [Cnt] not found!! (maybe it is an alias?)
						if (rsRECnt.Get_Fields("Cnt") > 1)
						{
							//Debug.WriteLine(Support.TabLayout(rsRE.Get_Fields("RSLocStreet")+"  "+Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("RSLocNumAlph")))+"  " + Strings.StrP(rsRECnt.Get_Fields("Cnt"))));
							// skip these but add to another report!
							lngMultiCnt += 1;
							Array.Resize(ref Statics.MultiREAcct, lngMultiCnt + 1);
							Statics.MultiREAcct[lngMultiCnt].REAcctNum = FCConvert.ToInt32(rsRE.Get_Fields_Int32("RSAccount"));
							Statics.MultiREAcct[lngMultiCnt].REOwnID1 = FCConvert.ToInt32(rsRE.Get_Fields_Int32("OwnerPartyID"));
							Statics.MultiREAcct[lngMultiCnt].REOwnID2 = FCConvert.ToInt32(rsRE.Get_Fields_Int32("SecOwnerPartyID"));
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							Statics.MultiREAcct[lngMultiCnt].UTAcctNum = FCConvert.ToInt32(rsUT.Get_Fields("AccountNumber"));
							Statics.MultiREAcct[lngMultiCnt].UTOwnID1 = FCConvert.ToInt32(rsUT.Get_Fields_Int32("OwnerPartyID"));
							Statics.MultiREAcct[lngMultiCnt].UTOwnID2 = FCConvert.ToInt32(rsUT.Get_Fields_Int32("SecondOwnerPartyID"));
							Statics.MultiREAcct[lngMultiCnt].Location = Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLocNumAlph"))) + " " + rsRE.Get_Fields_String("RSLocStreet");
                            Statics.MultiREAcct[lngMultiCnt].DeedName1 = rsRE.Get_Fields_String("DeedName1");
                            Statics.MultiREAcct[lngMultiCnt].DeedName2 = rsRE.Get_Fields_String("DeedName2");
                        }
						else
						{
							while (!rsUT.EndOfFile())
							{
								boolChgOwner = false;
								UTOwn1ID = 0;
								UTOwn2ID = 0;
								rsUT.Edit();
								if (CopyOwnerFromRE_6(REOwn1ID, rsUT.Get_Fields_Int32("OwnerPartyID")))
								{
									UTOwn1ID = FCConvert.ToInt32(rsUT.Get_Fields_Int32("OwnerPartyID"));
									rsUT.Set_Fields("OwnerPartyID", REOwn1ID);
									boolChgOwner = true;
								}
								if (REOwn2ID != 0 || (!rsUT.IsFieldNull("SecondOwnerPartyID") && FCConvert.ToInt32(rsUT.Get_Fields_Int32("SecondOwnerPartyID")) != 0))
								{
									if (CopyOwnerFromRE_6(REOwn2ID, rsUT.Get_Fields_Int32("SecondOwnerPartyID")))
									{
										UTOwn2ID = FCConvert.ToInt32(rsUT.Get_Fields_Int32("SecondOwnerPartyID"));
										rsUT.Set_Fields("SecondOwnerPartyID", REOwn2ID);
										boolChgOwner = true;
									}
								}
                                rsUT.Set_Fields("DeedName1",REDeedName1);
                                rsUT.Set_Fields("DeedName2",REDeedName2);
								rsUT.Set_Fields("MapLot", Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot"))));
								rsUT.Set_Fields("REAccount", rsRE.Get_Fields_Int32("RSAccount"));
								rsUT.Update();
								if (boolChgOwner)
								{
									// ADD ACCOUNT INFO TO ARRAY FOR REPORT
									lngUpdCnt += 1;
									Array.Resize(ref Statics.AcctSyncRpt, lngUpdCnt + 1);
									Statics.AcctSyncRpt[lngUpdCnt].REAcctNum = FCConvert.ToInt32(rsRE.Get_Fields_Int32("RSAccount"));
									Statics.AcctSyncRpt[lngUpdCnt].REOwnID1 = REOwn1ID;
									Statics.AcctSyncRpt[lngUpdCnt].REOwnID2 = REOwn2ID;
									// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
									Statics.AcctSyncRpt[lngUpdCnt].UTAcctNum = FCConvert.ToInt32(rsUT.Get_Fields("AccountNumber"));
									Statics.AcctSyncRpt[lngUpdCnt].UTOwnID1 = UTOwn1ID;
									Statics.AcctSyncRpt[lngUpdCnt].UTOwnID2 = UTOwn2ID;
									Statics.AcctSyncRpt[lngUpdCnt].Location = Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("RSLocNumAlph"))) + " " + rsRE.Get_Fields_String("RSLocStreet");
                                    Statics.AcctSyncRpt[lngUpdCnt].DeedName1 = REDeedName1;
                                    Statics.AcctSyncRpt[lngUpdCnt].DeedName2 = REDeedName2;
                                }
								rsUT.MoveNext();
							}
						}
					}
					else
					{
						// No match found, create a new account in the New book
						AddAccountFromRE(ref rsRE, ref lngXBook);
						lngRecCnt += 1;
						Debug.WriteLine(FCConvert.ToString(lngRecCnt) + " " + rsRE.Get_Fields_String("RSLocNumAlph") + " _ " + rsRE.Get_Fields_String("RSLocStreet"));
					}
				}
				rsRE.MoveNext();
			}
			frmWait.InstancePtr.Unload();
			if (lngUpdCnt > 0)
			{
				// run the report of Account Name Changes
				rptUTREAcctSync.InstancePtr.Init(1, modalDialog);
				rptUTREAcctSync.InstancePtr.Unload();
			}
			if (lngMultiCnt > 0)
			{
				// run the report of Skipped Account Name Changes
				rptUTREAcctSync.InstancePtr.Init(2, modalDialog);
				rptUTREAcctSync.InstancePtr.Unload();
			}
			if (lngRecCnt > 0)
			{
				// run the report of Accounts in the new book
				arAddedAcctListing.InstancePtr.Init(ref lngXBook);
			}
			FCUtils.EraseSafe(Statics.AcctSyncRpt);
			FCUtils.EraseSafe(Statics.MultiREAcct);
		}

		private static void AddAccountFromRE(ref clsDRWrapper rsRE, ref int lngBook)
		{
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsMeter = new clsDRWrapper();
			int lngAcctNum = 0;
			int lngSeqNum = 0;
			int lngAcctKey = 0;
			rsMaster.OpenRecordset("SELECT MAX(Sequence) AS Sequence FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(lngBook), modExtraModules.strUTDatabase);
			if (!rsMaster.IsFieldNull("BookNumber") && FCConvert.ToInt32(rsMaster.Get_Fields_Int32("BookNumber")) != 0)
			{
				// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
				lngSeqNum = rsMaster.Get_Fields("Sequence") + 1;
			}
			else
			{
				lngSeqNum = 1;
			}
			rsMaster.OpenRecordset("SELECT MAX(AccountNumber) AS AccountNumber FROM Master WHERE AccountNumber >= 3000 AND AccountNumber < 10000", modExtraModules.strUTDatabase);
			// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			if (FCConvert.ToString(rsMaster.Get_Fields("AccountNumber")) != "" && FCConvert.ToInt32(rsMaster.Get_Fields("AccountNumber")) != 0)
			{
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				lngAcctNum = rsMaster.Get_Fields("AccountNumber") + 1;
			}
			else
			{
				lngAcctNum = 3000;
			}
			rsMaster.OpenRecordset("SELECT * FROM Master WHERE ID = -1", modExtraModules.strUTDatabase);
			rsMaster.AddNew();
			rsMaster.Set_Fields("AccountNumber", lngAcctNum);
			rsMaster.Set_Fields("BillingPartyID", rsRE.Get_Fields_Int32("OwnerPartyID"));
			rsMaster.Set_Fields("SecondBillingPartyID", rsRE.Get_Fields_Int32("SecOwnerPartyID"));
			rsMaster.Set_Fields("SewerCategory", "");
			rsMaster.Set_Fields("WaterCategory", 1);
			// Default to residential
			rsMaster.Set_Fields("WaterAccount", "");
			rsMaster.Set_Fields("REAccount", rsRE.Get_Fields_Int32("RSAccount"));
			rsMaster.Set_Fields("OwnerPartyID", rsRE.Get_Fields_Int32("OwnerPartyID"));
			rsMaster.Set_Fields("SecondOwnerPartyID", rsRE.Get_Fields_Int32("SecOwnerPartyID"));
            rsMaster.Set_Fields("DeedName1",rsRE.Get_Fields_String("DeedName1"));
            rsMaster.Set_Fields("DeedName2",rsRE.Get_Fields_String("DeedName2"));
			rsMaster.Set_Fields("DateOfChange", DateAndTime.DateValue(FCConvert.ToString(0)));
			rsMaster.Set_Fields("Comment", "");
			rsMaster.Set_Fields("MapLot", rsRE.Get_Fields_String("RSMapLot"));
			rsMaster.Set_Fields("Code", 0);
			rsMaster.Set_Fields("Deleted", 0);
			rsMaster.Set_Fields("Directions", "");
			rsMaster.Set_Fields("BillMessage", "");
			rsMaster.Set_Fields("DataEntry", "");
			rsMaster.Set_Fields("UseREAccount", 0);
			rsMaster.Set_Fields("UseMortgageHolder", 0);
			rsMaster.Set_Fields("SameBillOwner", 1);
			rsMaster.Set_Fields("BookPage", "");
			rsMaster.Set_Fields("FinalBill", 0);
			rsMaster.Set_Fields("NoBill", 0);
			rsMaster.Set_Fields("Deposit", 0);
			rsMaster.Set_Fields("Apt", "");
			rsMaster.Set_Fields("StreetNumber", rsRE.Get_Fields_String("RSLocNumAlph"));
			rsMaster.Set_Fields("StreetName", rsRE.Get_Fields_String("RSLocStreet"));
			rsMaster.Set_Fields("TaxAcquired", rsRE.Get_Fields_Boolean("TaxAcquired"));
			rsMaster.Set_Fields("TADate", rsRE.Get_Fields_DateTime("TADate"));
			rsMaster.Set_Fields("Telephone", "");
			rsMaster.Set_Fields("Email", "");
			rsMaster.Set_Fields("InBankruptcy", rsRE.Get_Fields_Boolean("InBankruptcy"));
			rsMaster.Set_Fields("RefAccountNumber", "");
			rsMaster.Set_Fields("Book", 0);
			rsMaster.Set_Fields("Page", 0);
			rsMaster.Set_Fields("ResCode", 0);
			rsMaster.Set_Fields("UTBook", lngBook);
			rsMaster.Set_Fields("EmailBill", 0);
			rsMaster.Set_Fields("WBillToOwner", 1);
			rsMaster.Set_Fields("SBillToOwner", 1);
			rsMaster.Set_Fields("ImpervSurfArea", 0);
			rsMaster.Update();
			lngAcctKey = FCConvert.ToInt32(rsMaster.Get_Fields_Int32("ID"));
			rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE ID = -1", modExtraModules.strUTDatabase);
			rsMeter.AddNew();
			rsMeter.Set_Fields("MeterNumber", 1);
			rsMeter.Set_Fields("AccountKey", lngAcctKey);
			rsMeter.Set_Fields("BookNumber", lngBook);
			rsMeter.Set_Fields("Sequence", lngSeqNum);
			rsMeter.Set_Fields("SerialNumber", "");
			rsMeter.Set_Fields("Size", "1");
			rsMeter.Set_Fields("Service", "W");
			rsMeter.Set_Fields("Frequency", 1);
			rsMeter.Set_Fields("Digits", 4);
			rsMeter.Set_Fields("Multiplier", 1);
			rsMeter.Set_Fields("Combine", "N");
			rsMeter.Set_Fields("ReplacementConsumption", 0);
			rsMeter.Set_Fields("ReplacementDate", DateAndTime.DateValue(FCConvert.ToString(0)));
			rsMeter.Set_Fields("Location", "");
			rsMeter.Set_Fields("AdjustDescription", "");
			rsMeter.Set_Fields("WaterCharged", 0);
			rsMeter.Set_Fields("SewerCharged", 0);
			rsMeter.Set_Fields("WaterPercent", 100);
			rsMeter.Set_Fields("WaterTaxPercent", 0);
			rsMeter.Set_Fields("WaterType1", 2);
			rsMeter.Set_Fields("WaterType2", 0);
			rsMeter.Set_Fields("WaterType3", 0);
			rsMeter.Set_Fields("WaterType4", 0);
			rsMeter.Set_Fields("WaterType5", 0);
			rsMeter.Set_Fields("WaterAmount1", 0);
			rsMeter.Set_Fields("WaterAmount2", 0);
			rsMeter.Set_Fields("WaterAmount3", 0);
			rsMeter.Set_Fields("WaterAmount4", 0);
			rsMeter.Set_Fields("WaterAmount5", 0);
			rsMeter.Set_Fields("WaterKey1", 99);
			rsMeter.Set_Fields("WaterKey2", 0);
			rsMeter.Set_Fields("WaterKey3", 0);
			rsMeter.Set_Fields("WaterKey4", 0);
			rsMeter.Set_Fields("WaterKey5", 0);
			rsMeter.Set_Fields("WaterAdjustKey", 0);
			rsMeter.Set_Fields("WaterAdjustAmount", 0);
			rsMeter.Set_Fields("WaterConsumption", 0);
			rsMeter.Set_Fields("WaterConsumptionOverride", 0);
			rsMeter.Set_Fields("SewerPercent", 0);
			rsMeter.Set_Fields("SewerTaxPercent", 0);
			rsMeter.Set_Fields("SewerType1", 0);
			rsMeter.Set_Fields("SewerType2", 0);
			rsMeter.Set_Fields("SewerType3", 0);
			rsMeter.Set_Fields("SewerType4", 0);
			rsMeter.Set_Fields("SewerType5", 0);
			rsMeter.Set_Fields("SewerAmount1", 0);
			rsMeter.Set_Fields("SewerAmount2", 0);
			rsMeter.Set_Fields("SewerAmount3", 0);
			rsMeter.Set_Fields("SewerAmount4", 0);
			rsMeter.Set_Fields("SewerAmount5", 0);
			rsMeter.Set_Fields("SewerKey1", 0);
			rsMeter.Set_Fields("SewerKey2", 0);
			rsMeter.Set_Fields("SewerKey3", 0);
			rsMeter.Set_Fields("SewerKey4", 0);
			rsMeter.Set_Fields("SewerKey5", 0);
			rsMeter.Set_Fields("SewerAdjustKey", 0);
			rsMeter.Set_Fields("SewerAdjustAmount", 0);
			rsMeter.Set_Fields("SewerConsumption", 0);
			rsMeter.Set_Fields("SewerConsumptionOverride", 0);
			rsMeter.Set_Fields("PreviousReading", 0);
			rsMeter.Set_Fields("CurrentReading", -1);
			rsMeter.Set_Fields("WCat", 1);
			rsMeter.Set_Fields("SCat", 0);
			rsMeter.Set_Fields("BackFlow", 0);
			rsMeter.Set_Fields("Comment", "Stormwater");
			rsMeter.Set_Fields("FinalBilled", 0);
			rsMeter.Set_Fields("NoBill", 0);
			rsMeter.Set_Fields("BillingStatus", "C");
			rsMeter.Set_Fields("UseRate1", 1);
			rsMeter.Set_Fields("UseRate2", 0);
			rsMeter.Set_Fields("UseRate3", 0);
			rsMeter.Set_Fields("UseRate4", 0);
			rsMeter.Set_Fields("UseRate5", 0);
			rsMeter.Set_Fields("UseAdjustRate", 0);
			rsMeter.Set_Fields("Radio", 0);
			rsMeter.Set_Fields("IncludeInExtract", 0);
			rsMeter.Set_Fields("BilledConsumption", 0);
			rsMeter.Set_Fields("ConsumptionOfChangedOutMeter", 0);
			rsMeter.Set_Fields("NegativeConsumption", 0);
			rsMeter.Set_Fields("Final", 0);
			rsMeter.Set_Fields("RadioAccessType", 0);
			rsMeter.Set_Fields("PrevChangeOut", 0);
			rsMeter.Update();
		}

		private static bool CopyOwnerFromRE_6(int lngREOwn, int lngUTOwn)
		{
			return CopyOwnerFromRE(ref lngREOwn, ref lngUTOwn);
		}

		private static bool CopyOwnerFromRE(ref int lngREOwn, ref int lngUTOwn)
		{
			bool CopyOwnerFromRE = false;
			clsDRWrapper rsCP = new clsDRWrapper();
			string strName = "";
			string strFName = "";
			string strLName = "";
			string strMName = "";
			string strDesig = "";
			string strAddr1 = "";
			string strAddr2 = "";
			string strAddr3 = "";
			string strCity = "";
			string strState = "";
			CopyOwnerFromRE = true;
			if (lngREOwn == lngUTOwn)
			{
				CopyOwnerFromRE = false;
			}
			else
			{
				rsCP.OpenRecordset("SELECT * FROM PartyAndAddressView WHERE ID = " + FCConvert.ToString(lngREOwn), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					strName = FCConvert.ToString(rsCP.Get_Fields_String("FullNameLF"));
					strFName = FCConvert.ToString(rsCP.Get_Fields_String("FirstName"));
					strLName = FCConvert.ToString(rsCP.Get_Fields_String("LastName"));
					strMName = FCConvert.ToString(rsCP.Get_Fields_String("MiddleName"));
					strDesig = FCConvert.ToString(rsCP.Get_Fields_String("Designation"));
					strAddr1 = FCConvert.ToString(rsCP.Get_Fields_String("Address1"));
					strAddr2 = FCConvert.ToString(rsCP.Get_Fields_String("Address2"));
					strAddr3 = FCConvert.ToString(rsCP.Get_Fields_String("Address3"));
					strCity = FCConvert.ToString(rsCP.Get_Fields_String("City"));
					strState = FCConvert.ToString(rsCP.Get_Fields_String("State"));
					rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(lngUTOwn), "CentralParties");
					if (!rsCP.EndOfFile())
					{
						if ((Strings.UCase(strName) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("FullNameLF")))) || (Strings.UCase(strLName) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("LastName"))) && Strings.UCase(strFName) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("FirstName"))) && Strings.UCase(strDesig) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("Designation"))) && (Strings.UCase(strMName) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("MiddleName"))) || strMName == "" || FCConvert.ToString(rsCP.Get_Fields_String("MiddleName")) == "")))
						{
							rsCP.OpenRecordset("SELECT * FROM Addresses WHERE PartyID = " + FCConvert.ToString(lngUTOwn), "CentralParties");
							if (!rsCP.EndOfFile())
							{
								if (Strings.UCase(strAddr1) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("Address1"))) && Strings.UCase(strAddr2) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("Address2"))) && Strings.UCase(strAddr3) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("Address3"))) && Strings.UCase(strCity) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("City"))) && Strings.UCase(strState) == Strings.UCase(FCConvert.ToString(rsCP.Get_Fields_String("State"))))
								{
									CopyOwnerFromRE = false;
								}
							}
							else
							{
								CopyOwnerFromRE = false;
							}
						}
					}
				}
				else
				{
					CopyOwnerFromRE = false;
				}
			}
			return CopyOwnerFromRE;
		}

		public static void ForceStormwaterBills(ref int[] BookArray)
		{
			// vbPorter upgrade warning: BillingCode As object	OnWrite(object, string)
			object BillingCode;
			int intTemp;
			// - "AutoDim"
			// kk 01292014 trouts-68  Need to create the bill records when there bill isn't created by Electronic Data Entry/Data Entry
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAccount = new clsDRWrapper();
				clsDRWrapper rsMeter = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				string strSQL;
				string strBookSQL = "";
				int counter;
				// build the sql string to get the books we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					strBookSQL = "= " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					strBookSQL = "";
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strBookSQL += FCConvert.ToString(BookArray[counter]) + ",";
					}
					strBookSQL = "IN (" + Strings.Left(strBookSQL, strBookSQL.Length - 1) + ")";
				}
				strSQL = "SELECT * FROM Metertable WHERE BookNumber " + strBookSQL + " AND Combine = 'N' ORDER BY Sequence";
				rsMeter.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				while (!rsMeter.EndOfFile())
				{
					//Application.DoEvents();
					// find the billkey for this meter --- 'kk07082015 trouts-149  Add a check for ImpervSurfArea >= 0
					rsAccount.OpenRecordset("SELECT * FROM Master WHERE ImpervSurfArea >= 0 AND ID = " + rsMeter.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
					if (rsAccount.RecordCount() > 0)
					{
						if (!FCConvert.ToBoolean(rsAccount.Get_Fields_Boolean("Deleted")))
						{
							rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsMeter.Get_Fields_Int32("ID") + " AND BillStatus <> 'B'", modExtraModules.strUTDatabase);
							// If (boolCreateNew And rsBill.RecordCount = 0) Or (boolAddBillAsNeeded And rsBill.RecordCount <= 0) Then
							if (rsBill.RecordCount() == 0)
							{
								rsBill.AddNew();
								rsBill.Set_Fields("NoBill", true);
								// If we got here then the account is not in the readings/billing file
								rsBill.Set_Fields("BillNumber", 0);
								rsBill.Set_Fields("BillingRateKey", 0);
								rsBill.Set_Fields("AccountKey", rsMeter.Get_Fields_Int32("AccountKey"));
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								rsBill.Set_Fields("ActualAccountNumber", rsAccount.Get_Fields("AccountNumber"));
								rsBill.Set_Fields("Book", rsMeter.Get_Fields_Int32("BookNumber"));
								if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("MeterNumber")) == 1)
								{
									rsBill.Set_Fields("MeterKey", rsMeter.Get_Fields_Int32("ID"));
								}
								rsBill.Set_Fields("BillStatus", "D");
								if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_String("Service")) == false)
								{
									rsBill.Set_Fields("Service", rsMeter.Get_Fields_String("Service"));
								}
								else
								{
									rsBill.Set_Fields("Service", "B");
								}
								// Service = .Fields("Service")
								if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_String("BillingCode")) == false)
								{
									rsBill.Set_Fields("CombinationCode", rsMeter.Get_Fields_String("BillingCode"));
								}
								else
								{
									rsBill.Set_Fields("CombinationCode", "2");
								}
								if (fecherFoundation.FCUtils.IsNull(rsBill.Get_Fields_String("CombinationCode")) == false)
								{
									BillingCode = rsBill.Get_Fields_String("CombinationCode");
								}
								else
								{
									BillingCode = "";
								}
								// .Fields("CurDate") = mtdMeters(lngMeterCounter).ReadingDate    'kk 09052013 trout-986   datDefaultReading
								// .Fields("CurReading") = mtdMeters(lngMeterCounter).CurrentReading * (rsMeter.Fields("Multiplier") / glngTownReadingUnits)
								// .Fields("CurCode") = "A"
								// If IsDate(rsMeter.Fields("PreviousReadingDate")) Then
								// .Fields("PrevDate") = rsMeter.Fields("PreviousReadingDate")
								// End If
								// .Fields("PrevReading") = Val(rsMeter.Fields("PreviousReading"))
								// .Fields("PrevCode") = Trim$(rsMeter.Fields("PreviousCode") & " ")
								// .Fields("Consumption") = CalculateMeterConsumption(Val(.Fields("PrevReading")), Val(.Fields("CurReading")), rsMeter.Fields("Digits"))
								rsBill.Set_Fields("CurDate", DateAndTime.DateValue(FCConvert.ToString(0)));
								rsBill.Set_Fields("CurReading", -1);
								rsBill.Set_Fields("CurCode", "");
								if (Information.IsDate(rsMeter.Get_Fields("PreviousReadingDate")))
								{
									rsBill.Set_Fields("PrevDate", rsMeter.Get_Fields_DateTime("PreviousReadingDate"));
								}
								rsBill.Set_Fields("PrevReading", FCConvert.ToString(Conversion.Val(rsMeter.Get_Fields_Int32("PreviousReading"))));
								rsBill.Set_Fields("PrevCode", Strings.Trim(rsMeter.Get_Fields_String("PreviousCode") + " "));
								rsBill.Set_Fields("Consumption", 0);
								rsBill.Set_Fields("WDEAdjustAmount", 0);
								rsBill.Set_Fields("SDEAdjustAmount", 0);
								for (intTemp = 1; intTemp <= 5; intTemp++)
								{
									// rate table information
									if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intTemp))) == false)
									{
										rsBill.Set_Fields("WRT" + intTemp, rsMeter.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intTemp)));
									}
									else
									{
										rsBill.Set_Fields("WRT" + intTemp, 0);
									}
									if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intTemp))) == false)
									{
										rsBill.Set_Fields("SRT" + intTemp, rsMeter.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intTemp)));
									}
									else
									{
										rsBill.Set_Fields("SRT" + intTemp, 0);
									}
								}
								rsBill.Set_Fields("WLienStatusEligibility", 0);
								rsBill.Set_Fields("WLienProcessStatus", 0);
								rsBill.Set_Fields("WLienRecordNumber", 0);
								rsBill.Set_Fields("WCombinationLienKey", 0);
								rsBill.Set_Fields("SLienStatusEligibility", 0);
								rsBill.Set_Fields("SLienProcessStatus", 0);
								rsBill.Set_Fields("SLienRecordNumber", 0);
								rsBill.Set_Fields("SCombinationLienKey", 0);
								rsBill.Set_Fields("WIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
								rsBill.Set_Fields("WIntOwed", 0);
								rsBill.Set_Fields("WIntAdded", 0);
								rsBill.Set_Fields("WCostOwed", 0);
								rsBill.Set_Fields("WCostAdded", 0);
								rsBill.Set_Fields("WPrinPaid", 0);
								rsBill.Set_Fields("WTaxPaid", 0);
								rsBill.Set_Fields("WIntPaid", 0);
								rsBill.Set_Fields("WCostPaid", 0);
								rsBill.Set_Fields("SIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
								rsBill.Set_Fields("SIntOwed", 0);
								rsBill.Set_Fields("SIntAdded", 0);
								rsBill.Set_Fields("SCostOwed", 0);
								rsBill.Set_Fields("SCostAdded", 0);
								rsBill.Set_Fields("SPrinPaid", 0);
								rsBill.Set_Fields("STaxPaid", 0);
								rsBill.Set_Fields("SIntPaid", 0);
								rsBill.Set_Fields("SCostPaid", 0);
								rsBill.Set_Fields("Copies", 0);
								rsBill.Set_Fields("Loadback", 0);
								rsBill.Set_Fields("Final", 0);
								rsBill.Set_Fields("SHasOverride", 0);
								rsBill.Set_Fields("WHasOverride", 0);
								rsBill.Set_Fields("SDemandGroupID", 0);
								rsBill.Set_Fields("WDemandGroupID", 0);
								rsBill.Set_Fields("SendEBill", 0);
								rsBill.Set_Fields("WinterBill", 0);
								rsBill.Set_Fields("SUploaded", 0);
								rsBill.Set_Fields("WUploaded", 0);
								// add the final bill dates        'kk05232016 trouts-168  Set final bill info on SW only acct so the bill after final is correct
								if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("Final")))
								{
									rsBill.Set_Fields("FinalEndDate", rsMeter.Get_Fields_DateTime("FinalEndDate"));
									rsBill.Set_Fields("FinalStartDate", rsMeter.Get_Fields_DateTime("FinalStartDate"));
									rsBill.Set_Fields("FinalBillDate", rsMeter.Get_Fields_DateTime("FinalBillDate"));
								}
								rsBill.Update();
								// kk05232016 trouts-168  Update the billing status and the book status
								rsMeter.Set_Fields("BillingStatus", rsBill.Get_Fields_String("BillingStatus"));
								rsMeter.Update();
								rsAccount.Execute("UPDATE Book SET CurrentStatus = 'DE', DDate = '" + Strings.Format(DateTime.Now, "MM/dd/yyyy") + "' WHERE BookNumber = " + rsMeter.Get_Fields_Int32("BookNumber"), modExtraModules.strUTDatabase);
							}
						}
					}
					rsMeter.MoveNext();
				}
				// XXXXXXXXXXXXXXXXXXXXXXXXXXXXX
				// rsAcct.OpenRecordset "SELECT *, MeterTable.ID AS MeterKey FROM MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID WHERE ISNULL(Master.Deleted,0) = 0 AND BookNumber " & strSQL
				// While Not rsAcct.EndOfFile
				// if
				// rsAcct.MoveNext
				// Wend
				return;
			}
			catch
			{
				
				MessageBox.Show("Error Creating Bills on Stormwater only accounts.");
			}
		}

		public class StaticVariables
		{
			public AcctSyncRec[] AcctSyncRpt = null;
			public AcctSyncRec[] MultiREAcct = null;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
