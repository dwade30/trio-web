﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmAnalysisReports.
	/// </summary>
	public partial class frmAnalysisReports : BaseForm
	{
		public frmAnalysisReports()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAnalysisReports InstancePtr
		{
			get
			{
				return (frmAnalysisReports)Sys.GetInstance(typeof(frmAnalysisReports));
			}
		}

		protected frmAnalysisReports _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/12/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/28/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		string strBookList;
		string strRKList;
		public string strPassRNFORMSQL = "";

		public void Init(ref string strPassBookList, ref string strPassRKList)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will pass a list of the rate keys that will determine which accounts to use
				strBookList = strPassBookList;
				strRKList = strPassRKList;
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmAnalysisReports_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!boolLoaded)
				{
					boolLoaded = true;
					this.Text = "Analysis Reports";
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmAnalysisReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmAnalysisReports properties;
			//frmAnalysisReports.FillStyle	= 0;
			//frmAnalysisReports.ScaleWidth	= 5880;
			//frmAnalysisReports.ScaleHeight	= 4275;
			//frmAnalysisReports.LinkTopic	= "Form2";
			//frmAnalysisReports.LockControls	= true;
			//frmAnalysisReports.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				if (modUTStatusPayments.Statics.TownService == "W")
				{
					//optWS[0].Enabled = true;
					//optWS[1].Enabled = false;
					//optWS[2].Enabled = false;
					//optWS[0].Checked = true;
					cmbWS.Clear();
					cmbWS.Items.Add("Water");
					cmbWS.Text = "Water";
				}
				else if (modUTStatusPayments.Statics.TownService == "S")
				{
					//optWS[1].Enabled = true;
					//optWS[0].Enabled = false;
					//optWS[2].Enabled = false;
					//optWS[1].Checked = true;
					cmbWS.Clear();
					cmbWS.Items.Add("Sewer");
					cmbWS.Text = "Sewer";
				}
				else if (modUTStatusPayments.Statics.TownService == "B")
				{
					//optWS[0].Enabled = true;
					//optWS[1].Enabled = true;
					//optWS[2].Enabled = true;
					//optWS[2].Checked = true;
					cmbWS.Clear();
					cmbWS.Items.Add("Water");
					cmbWS.Items.Add("Sewer");
					cmbWS.Items.Add("Both");
					cmbWS.Text = "Both";
				}
				LoadDefaults();
				// kk trouts-6 03012013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					lblWaterHeader.Text = "Stormwater";
					//optWS[0].Text = "Stormwater";
					if (cmbWS.Items.Contains("Water"))
					{
						cmbWS.Items.Remove("Water");
						cmbWS.Items.Insert(0, "Stormwater");
					}
				}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Reminder Notices", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Unload();
			}
		}

		private void frmAnalysisReports_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void fraType_DoubleClick(object sender, System.EventArgs e)
		{
			// select all of the options
			chkBillingSummary.CheckState = Wisej.Web.CheckState.Checked;
			if (modUTStatusPayments.Statics.TownService != "S")
			{
				chkDollarAmountsW.CheckState = Wisej.Web.CheckState.Checked;
				chkConsumptionsW.CheckState = Wisej.Web.CheckState.Checked;
				chkBillCountW.CheckState = Wisej.Web.CheckState.Checked;
				chkMeterReportW.CheckState = Wisej.Web.CheckState.Checked;
				chkSalesTaxW.CheckState = Wisej.Web.CheckState.Checked;
			}
			if (modUTStatusPayments.Statics.TownService != "W")
			{
				chkDollarAmountsS.CheckState = Wisej.Web.CheckState.Checked;
				chkConsumptionsS.CheckState = Wisej.Web.CheckState.Checked;
				chkBillCountS.CheckState = Wisej.Web.CheckState.Checked;
				chkMeterReportS.CheckState = Wisej.Web.CheckState.Checked;
				chkSalesTaxS.CheckState = Wisej.Web.CheckState.Checked;
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intType = 0;
				if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
				{
					// Water
					intType = 0;
				}
				else if (cmbWS.Text == "Sewer")
				{
					// Sewer
					intType = 1;
				}
				else
				{
					// Both
					intType = 2;
				}
				// If InStr(1, ",", strBookList) <> 0 Then
				// strBookList = " IN (" & strBookList & ")"
				// Else
				// strBookList = " = " & strBookList
				// End If
				// call to the master report
				rptAnalysisReportsMaster.InstancePtr.Init(BuildSQL(), FCConvert.CBool(chkBillingSummary.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkDollarAmountsW.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkConsumptionsW.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkBillCountW.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkMeterReportW.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkSalesTaxW.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkDollarAmountsS.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkConsumptionsS.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkBillCountS.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkMeterReportS.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(chkSalesTaxS.CheckState == Wisej.Web.CheckState.Checked), FCConvert.CBool(cmbOrientation.Text == "Landscape"), ref strBookList, intType, false, false, strRKList);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Master Report Call", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will build the query to use for all of the analysis reports
				// by creating a query that is a list of meters/bills to include
				string strTemp;
				strTemp = "";
				BuildSQL = strTemp;
				return BuildSQL;
			}
			catch (Exception ex)
			{
				
				BuildSQL = "";
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building SQL", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildSQL;
		}

		private void optWS_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			chkBillingSummary.Enabled = true;
			switch (Index)
			{
				case 0:
					{
						// Water
						chkDollarAmountsW.Enabled = true;
						chkConsumptionsW.Enabled = true;
						chkBillCountW.Enabled = true;
						chkMeterReportW.Enabled = true;
						chkSalesTaxW.Enabled = true;
						chkDollarAmountsS.Enabled = false;
						chkConsumptionsS.Enabled = false;
						chkBillCountS.Enabled = false;
						chkMeterReportS.Enabled = false;
						chkSalesTaxS.Enabled = false;
						chkDollarAmountsS.CheckState = Wisej.Web.CheckState.Unchecked;
						chkConsumptionsS.CheckState = Wisej.Web.CheckState.Unchecked;
						chkBillCountS.CheckState = Wisej.Web.CheckState.Unchecked;
						chkMeterReportS.CheckState = Wisej.Web.CheckState.Unchecked;
						chkSalesTaxS.CheckState = Wisej.Web.CheckState.Unchecked;
						break;
					}
				case 1:
					{
						// Sewer
						chkDollarAmountsS.Enabled = true;
						chkConsumptionsS.Enabled = true;
						chkBillCountS.Enabled = true;
						chkMeterReportS.Enabled = true;
						chkSalesTaxS.Enabled = true;
						chkDollarAmountsW.Enabled = false;
						chkConsumptionsW.Enabled = false;
						chkBillCountW.Enabled = false;
						chkMeterReportW.Enabled = false;
						chkSalesTaxW.Enabled = false;
						chkDollarAmountsW.CheckState = Wisej.Web.CheckState.Unchecked;
						chkConsumptionsW.CheckState = Wisej.Web.CheckState.Unchecked;
						chkBillCountW.CheckState = Wisej.Web.CheckState.Unchecked;
						chkMeterReportW.CheckState = Wisej.Web.CheckState.Unchecked;
						chkSalesTaxW.CheckState = Wisej.Web.CheckState.Unchecked;
						break;
					}
				case 2:
					{
						// Both
						chkDollarAmountsW.Enabled = true;
						chkConsumptionsW.Enabled = true;
						chkBillCountW.Enabled = true;
						chkMeterReportW.Enabled = true;
						chkSalesTaxW.Enabled = true;
						chkDollarAmountsS.Enabled = true;
						chkConsumptionsS.Enabled = true;
						chkBillCountS.Enabled = true;
						chkMeterReportS.Enabled = true;
						chkSalesTaxS.Enabled = true;
						break;
					}
			}
			//end switch
		}

		private void optWS_CheckedChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:DDU:#i869 - set index based on combobox text
			//int index = cmbWS.SelectedIndex;
			int index;
			if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
			{
				index = 0;
			}
			else if (cmbWS.Text == "Sewer")
			{
				index = 1;
			}
			else
			{
				index = 2;
			}
			optWS_CheckedChanged(index, sender, e);
		}

		private void LoadDefaults()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDef = new clsDRWrapper();
				rsDef.OpenRecordset("SELECT * FROM DefaultAnalysisReports", modExtraModules.strUTDatabase);
				if (rsDef.EndOfFile())
				{
					// If there is no record then create one and exit this routine
					rsDef.AddNew();
					rsDef.Update(false);
					return;
				}
				if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("BillingSummaryS")))
				{
					chkBillingSummary.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkBillingSummary.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (modUTStatusPayments.Statics.TownService != "W")
				{
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("DollarAmountS")))
					{
						chkDollarAmountsS.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDollarAmountsS.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("ConsumptionS")))
					{
						chkConsumptionsS.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkConsumptionsS.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("BillCountS")))
					{
						chkBillCountS.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkBillCountS.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("MeterReportS")))
					{
						chkMeterReportS.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkMeterReportS.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("SalesTaxS")))
					{
						chkSalesTaxS.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkSalesTaxS.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				if (modUTStatusPayments.Statics.TownService != "S")
				{
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("DollarAmountW")))
					{
						chkDollarAmountsW.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDollarAmountsW.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("ConsumptionW")))
					{
						chkConsumptionsW.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkConsumptionsW.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("BillCountW")))
					{
						chkBillCountW.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkBillCountW.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("MeterReportW")))
					{
						chkMeterReportW.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkMeterReportW.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsDef.Get_Fields_Boolean("SalesTaxW")))
					{
						chkSalesTaxW.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkSalesTaxW.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Defaults", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
