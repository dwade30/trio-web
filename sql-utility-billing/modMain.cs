﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Diagnostics;
using System.Runtime.InteropServices;
using fecherFoundation.DataBaseLayer;
using fecherFoundation.VisualBasicLayer;
using modUTStatusPayments = Global.modUTFunctions;
using System.IO;
using Microsoft.VisualBasic.ApplicationServices;
using SharedApplication.Messaging;
using TWSharedLibrary;
using TWSharedLibrary.Data;
using clsDRWrapper = Global.clsDRWrapper;

namespace TWUT0000
{
	public class modMain
	{
		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}

		public class StaticVariables
		{
            //=========================================================
            // ********************************************************
            // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
            // *
            // WRITTEN BY     :               Jim Bertolino           *
            // DATE           :               03/19/2003              *
            // *
            // MODIFIED BY    :               Jim Bertolino           *
            // LAST UPDATED   :               02/15/2007              *
            // ********************************************************
            public bool moduleInitialized = false;
            public string CityTown = "";
			// town or city?
			public int lngCurrentMeterKey;
			public string strPrintSQL = "";
			// this string is used to pass a generated
			// SQL string into the AR viewer control
			public int gintBasis;
			// this is a value of either 360 or 365 depending on how the department calculates interest
			public bool boolRE;
			// this is for the mortgage holder screens
			public string gstrLastYearBilled = "";
			public bool gboolDefaultPaymentsToAuto;
			public bool gboolUseSigFile;
			public int glngCurrentCloseOut;
			public bool gboolSBillToOwner;
			public bool gboolWBillToOwner;
			public double[] arrTAPrincipalW = new double[10000 + 1];
			// These are for the Tax Acquired Listing
			public double[] arrTALienPrincipalW = new double[10000 + 1];
			// The first number is a variation of the year and the second is the town code
			public double[] arrTAPrincipalS = new double[10000 + 1];
			// These are for the Tax Acquired Listing
			public double[] arrTALienPrincipalS = new double[10000 + 1];
			// The first number is a variation of the year and the second is the town code
			public double gdblCMFAdjustment;
			// adjusts the CMF
			public double gdblLabelsAdjustmentDMH;
			// adjustments for dot matrix horizontal position of labels
			public double gdblLabelsAdjustmentDMV;
			// adjustments for dot matrix vertical position of labels
			public double gdblLabelsAdjustment;
			// laster adjustment
			public double gdblLienAdjustmentTop;
			// adjusts the top margin of the Lien
			public double gdblLienAdjustmentBottom;
			// adjusts the bottom margin of the Lien   'kk04222014  trocl-1156/trout-1082
			public double gdblLienAdjustmentSide;
			// kk02082016 trout-1197 Allow user to adjust side margins
			// Public gdblLienTopRightBoxW         As Double           'kk02082016 trout-1197 Removed the Top Right Box settings 'kk08072015 trocl-1250 Allow user to define the top right box
			// Public gdblLienTopRightBoxH         As Double
			public double gdblLienSigAdjust;
			// kk02012016 trut-1197  Actually do something with the signature adjustment
			// kk06122014 trout-1082  Change globals for LDN from gdblLDNAdjustment... to gdblUTLDNAdjustment... to fix conflict with CL in CR
			// Moved definitions to modUTCalculations
			// Public gdblLDNAdjustmentTop       As Double           'adjusts the top margin of the Lien Discharge Notice
			// Public gdblLDNAdjustmentBottom    As Double           'adjusts the bottom margin of the Lien Discharge Notice   'kk04222014  trocl-1156/trout-1082
			public double gdblReminderFormAdjustH;
			public double gdblReminderFormAdjustV;
			public DateTime gdtUTStatusListAsOfDate;
			// variables for Status Lists
			public bool gboolUTUseAsOfDate;
			public bool gboolUTSLDateRange;
			public DateTime gdtUTSLPaymentDate1;
			public DateTime gdtUTSLPaymentDate2;
			public bool boolSubReport;
			public bool gboolPendingRI;
			// if this is true, then frmReceiptInput will act differently
			public bool gboolPendingTransactions;
			// if this is true, then the pending RI option will show, the daily audit options will be different, ect
			// MAL@20071011
			public bool boolMultiLine;
			public bool boolUseOldCHGINTDate;
			public int lngCHGINT;
			// kgk 02-27-2012  Add full db paths for joins
			public string strDbRE = string.Empty;
			public string strDbPP = string.Empty;
			public string strDbUT = string.Empty;
			public string strDbGNC = string.Empty;
			public string strDbGNS = string.Empty;
			public string strDbCP = string.Empty;
			public int DefaultWKey;
			// default key/account number for water and sewer
			public int DefaultSKey;
			public string DefaultWAccount = "";
			public string DefaultSAccount = "";
			// these variables are from modGNbas
			public bool boolLoadHide;
			// kgk 08-08-2012  Move stored procedures to SQL
			public string gstrBillingMasterYear = "";
			public string gstrLienEditReportQuery = "";
			public string gstr30DayNoticeQuery = "";
			public string gstrProcessLienQuery = "";
			public string gstrLienMaturityQuery = "";
			public cPartyController gCPCtlr = new cPartyController();
		}

		public const int UTSECACCOUNTMETERUPDATE = 1;
		public const int UTSECBILLINGPROCESS = 2;
		public const int UTSECCOLLECTIONPROCESS = 3;
		public const int UTSECVIEWSTATUS = 8;
		public const int UTSECPAYMENTSADJUSTMENTS = 9;
		public const int UTSECDAILYAUDITREPORT = 10;
		public const int UTSECLOADOFBACKINFORMATION = 11;
		public const int UTSECMORTGAGEHOLDER = 12;
		public const int UTSECLIENPROCESS = 13;
		public const int UTSECCHARGEINTEREST = 14;
		public const int UTSECTABLEFILESETUP = 4;
		public const int UTSECEXTRACTFORREMOTEREADER = 5;
		public const int UTSECFILEMAINTENANCE = 6;
		public const int UTSECCUSTOMIZE = 15;
		public const int UTSECLOADPREVREADINGS = 16;
		public const int UTSECPURGEPAIDBILLS = 17;
		public const int UTSECREPRINTPURGEREPORT = 18;
		public const int UTSECMETERCHANGEOUT = 19;
		public const int UTSECEDITCUSTOMBILLTYPES = 20;
		public const int UTSECCONVERTCONSUMPTIONFILE = 21;
		public const int UTSECCREATEDATABASEEXTRACT = 22;
		public const int UTSECCOPYEXTRACTTODISK = 23;
		public const int UTSECRATEKEYUPDATE = 24;
		public const int UTSECCLEARCERTFIEDMAILTABLE = 25;
		public const int UTSECRESETBILLS = 26;
		public const int UTSECRESETFINALBILLS = 27;
		public const int UTSECREMOVEFROMTAXACQUIRED = 28;
		public const int UTSECSPLITBILLS = 29;
		public const int UTSECPRINTING = 7;
		public const int UTSECEDITBILLINFORMATION = 30;
		public const int UTSECPERMDELETEACCOUNT = 31;
		public const int UTSECAUTOPOSTJOURNAL = 32;
		// kk01282015 trouts-129  Automatic posting of journals
		[DllImport("user32")]
		public static extern int SetWindowPos(int hwnd, int hWndInsertAfter, int x, int Y, int cx, int cy, int wFlags);

		public struct MeterKey
		{
			// this data type is used in frmMaster
			public int Key;
			// this is the key to the meter in the MeterTable in the
			public int MeterNumber;
			public int AccountKey;
			public int BookNumber;
			// vbPorter upgrade warning: Sequence As int	OnWrite(string, short)
			public int Sequence;
			public string SerialNumber;
			public bool RadioAccess;
			public int RadioAccessType;
			// 0 - manual, 1 - Automated, Non-radio 2 - Automated, Radio
			public int Size;
			public string Service;
			public int Frequency;
			public int Digits;
			public int Multiplier;
			public string Combine;
			public int ReplacementConsumption;
			// vbPorter upgrade warning: ReplacementDate As DateTime	OnWrite(short, DateTime)
			public DateTime ReplacementDate;
			public string Location;
			public bool IncludeInExtract;
			public string XRef1;
			public string XRef2;
			public string Longitude;
			public string Latitude;
			public string MXUNumber;
			public string ReaderCode;
			public bool UseRate1;
			public bool UseRate2;
			public bool UseRate3;
			public bool UseRate4;
			public bool UseRate5;
			// this will be 0 = None, 1 - Flat, 2 - Units, 4 - Consumption
			public short RateType1;
			public short RateType2;
			public short RateType3;
			public short RateType4;
			public short RateType5;
			public bool UseAdjustRate;
			// water related fields
			public int WaterPercent;
			public int WaterTaxPercent;
			public double WaterAmount1;
			public double WaterAmount2;
			public double WaterAmount3;
			public double WaterAmount4;
			public double WaterAmount5;
			public int WaterKey1;
			public int WaterKey2;
			public int WaterKey3;
			public int WaterKey4;
			public int WaterKey5;
			public string WaterAccount1;
			public string WaterAccount2;
			public string WaterAccount3;
			public string WaterAccount4;
			public string WaterAccount5;
			// vbPorter upgrade warning: WaterType1 As int	OnWrite(string, short)
			public int WaterType1;
			// 1-Consumption 2-Flat 3-Unit
			// vbPorter upgrade warning: WaterType2 As int	OnWrite(string, short)
			public int WaterType2;
			// vbPorter upgrade warning: WaterType3 As int	OnWrite(string, short)
			public int WaterType3;
			// vbPorter upgrade warning: WaterType4 As int	OnWrite(string, short)
			public int WaterType4;
			// vbPorter upgrade warning: WaterType5 As int	OnWrite(string, short)
			public int WaterType5;
			public double WaterAdjustAmount;
			public int WaterAdjustKey;
			public string WaterAdjustAccount;
			public string WaterAdjustDescription;
			public int WaterConsumption;
			public int WaterConsumptionOverride;
			// sewer related fields
			// SewerRT1                        As Long
			// SewerRT2                        As Long
			// SewerRT3                        As Long
			// SewerRT4                        As Long
			public int SewerRT5;
			public int SewerPercent;
			public int SewerTaxPercent;
			public double SewerAmount1;
			public double SewerAmount2;
			public double SewerAmount3;
			public double SewerAmount4;
			public double SewerAmount5;
			public int SewerKey1;
			public int SewerKey2;
			public int SewerKey3;
			public int SewerKey4;
			public int SewerKey5;
			public string SewerAccount1;
			public string SewerAccount2;
			public string SewerAccount3;
			public string SewerAccount4;
			public string SewerAccount5;
			// vbPorter upgrade warning: SewerType1 As int	OnWrite(string, short)
			public int SewerType1;
			// vbPorter upgrade warning: SewerType2 As int	OnWrite(string, short)
			public int SewerType2;
			// vbPorter upgrade warning: SewerType3 As int	OnWrite(string, short)
			public int SewerType3;
			// vbPorter upgrade warning: SewerType4 As int	OnWrite(string, short)
			public int SewerType4;
			// vbPorter upgrade warning: SewerType5 As int	OnWrite(string, short)
			public int SewerType5;
			public double SewerAdjustAmount;
			public int SewerAdjustKey;
			public string SewerAdjustAccount;
			public string SewerAdjustDescription;
			public int SewerConsumption;
			public int SewerConsumptionOverride;
			public int PreviousReading;
			// vbPorter upgrade warning: PreviousReadingDate As DateTime	OnWrite(short, DateTime)
			public DateTime PreviousReadingDate;
			public string PreviousCode;
			public int CurrentReading;
			// vbPorter upgrade warning: CurrentReadingDate As DateTime	OnWrite(short, DateTime)
			public DateTime CurrentReadingDate;
			public string CurrentCode;
			// vbPorter upgrade warning: DateOfChange As DateTime	OnWrite(DateTime, short)
			public DateTime DateOfChange;
			public string Remote;
			public string WCat;
			public string SCat;
			public bool BackFlow;
			// vbPorter upgrade warning: BackFlowDate As DateTime	OnWrite(short, DateTime)
			public DateTime BackFlowDate;
			// vbPorter upgrade warning: SetDate As DateTime	OnWrite(short, DateTime)
			public DateTime SetDate;
			public string BillingCode;
			public string Comment;
			public bool FinalBilled;
			public bool NoBill;
			public string BillingStatus;
			public bool NegativeConsumption;
			public bool Used;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public MeterKey(int unusedParam)
			{
				this.Key = 0;
				this.MeterNumber = 0;
				this.AccountKey = 0;
				this.BookNumber = 0;
				this.Sequence = 0;
				this.SerialNumber = string.Empty;
				this.RadioAccess = false;
				this.RadioAccessType = 0;
				this.Size = 0;
				;
				this.Service = string.Empty;
				this.Frequency = 0;
				this.Digits = 0;
				this.Multiplier = 0;
				this.Combine = string.Empty;
				this.ReplacementConsumption = 0;
				this.ReplacementDate = default(DateTime);
				this.Location = string.Empty;
				this.IncludeInExtract = false;
				this.XRef1 = string.Empty;
				this.XRef2 = string.Empty;
				this.Longitude = string.Empty;
				this.Latitude = string.Empty;
				this.MXUNumber = string.Empty;
				this.ReaderCode = string.Empty;
				this.UseRate1 = false;
				this.UseRate2 = false;
				this.UseRate3 = false;
				this.UseRate4 = false;
				this.UseRate5 = false;
				this.RateType1 = 0;
				this.RateType2 = 0;
				this.RateType3 = 0;
				this.RateType4 = 0;
				this.RateType5 = 0;
				this.UseAdjustRate = false;
				this.WaterPercent = 0;
				this.WaterTaxPercent = 0;
				this.WaterAmount1 = 0;
				this.WaterAmount2 = 0;
				this.WaterAmount3 = 0;
				this.WaterAmount4 = 0;
				this.WaterAmount5 = 0;
				this.WaterKey1 = 0;
				this.WaterKey2 = 0;
				this.WaterKey3 = 0;
				this.WaterKey4 = 0;
				this.WaterKey5 = 0;
				this.WaterAccount1 = string.Empty;
				this.WaterAccount2 = string.Empty;
				this.WaterAccount3 = string.Empty;
				this.WaterAccount4 = string.Empty;
				this.WaterAccount5 = string.Empty;
				this.WaterType1 = 0;
				this.WaterType2 = 0;
				this.WaterType3 = 0;
				this.WaterType4 = 0;
				this.WaterType5 = 0;
				this.WaterAdjustAmount = 0;
				this.WaterAdjustKey = 0;
				this.WaterAdjustAccount = string.Empty;
				this.WaterAdjustDescription = string.Empty;
				this.WaterConsumption = 0;
				this.WaterConsumptionOverride = 0;
				this.SewerRT5 = 0;
				this.SewerPercent = 0;
				this.SewerTaxPercent = 0;
				this.SewerAmount1 = 0;
				this.SewerAmount2 = 0;
				this.SewerAmount3 = 0;
				this.SewerAmount4 = 0;
				this.SewerAmount5 = 0;
				this.SewerKey1 = 0;
				this.SewerKey2 = 0;
				this.SewerKey3 = 0;
				this.SewerKey4 = 0;
				this.SewerKey5 = 0;
				this.SewerAccount1 = string.Empty;
				this.SewerAccount2 = string.Empty;
				this.SewerAccount3 = string.Empty;
				this.SewerAccount4 = string.Empty;
				this.SewerAccount5 = string.Empty;
				this.SewerType1 = 0;
				this.SewerType2 = 0;
				this.SewerType3 = 0;
				this.SewerType4 = 0;
				this.SewerType5 = 0;
				this.SewerAdjustAmount = 0;
				this.SewerAdjustKey = 0;
				this.SewerAdjustAccount = string.Empty;
				this.SewerAdjustDescription = string.Empty;
				this.SewerConsumption = 0;
				this.SewerConsumptionOverride = 0;
				this.PreviousReading = 0;
				this.PreviousReadingDate = default(DateTime);
				this.PreviousCode = string.Empty;
				this.CurrentReading = 0;
				this.CurrentReadingDate = default(DateTime);
				this.CurrentCode = string.Empty;
				this.DateOfChange = default(DateTime);
				this.Remote = string.Empty;
				this.WCat = string.Empty;
				this.SCat = string.Empty;
				this.BackFlow = false;
				this.BackFlowDate = default(DateTime);
				this.SetDate = default(DateTime);
				this.BillingCode = string.Empty;
				this.Comment = string.Empty;
				this.FinalBilled = false;
				this.NoBill = false;
				this.BillingStatus = string.Empty;
				this.NegativeConsumption = false;
				this.Used = false;
			}
		};

		public const string DEFAULTDATABASE = "TWUT0000.vb1";

		public static void InitializeUtilityBillingStatics()
		{
			if (TWSharedLibrary.Data.DataReader.Statics.ConnectionPools == null ||
			    String.IsNullOrWhiteSpace(TWSharedLibrary.Data.DataReader.Statics.ConnectionPools.DefaultMegaGroup))
			{
				DataConfigurator.LoadSQLConfig();
			}

			if (!modGlobalFunctions.LoadSQLConfig("TWUT0000.VB1"))
			{
				MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}

			clsDRWrapper WWK = new clsDRWrapper();
			clsDRWrapper rsCreateTable = new clsDRWrapper();
			string strTemp = "";

			Statics.strDbRE = rsCreateTable.Get_GetFullDBName(modExtraModules.strREDatabase) + ".dbo.";
			Statics.strDbPP = rsCreateTable.Get_GetFullDBName(modExtraModules.strPPDatabase) + ".dbo.";
			Statics.strDbUT = rsCreateTable.Get_GetFullDBName(modExtraModules.strUTDatabase) + ".dbo.";
			Statics.strDbGNC = rsCreateTable.Get_GetFullDBName("CentralData") + ".dbo.";
			Statics.strDbGNS = rsCreateTable.Get_GetFullDBName("SystemSettings") + ".dbo.";
			Statics.strDbCP = rsCreateTable.Get_GetFullDBName("CentralParties") + ".dbo.";

			modGlobalFunctions.GetGlobalRegistrySetting();
			modReplaceWorkFiles.EntryFlagFile();
			frmWait.InstancePtr.Init("Checking Database" + "\r\n" + "Please Wait...", true, 10);
			modGlobalConstants.Statics.clsSecurityClass = new clsTrioSecurity();
			modGlobalConstants.Statics.clsSecurityClass.Init("UT", "SystemSettings");
			modGlobalConstants.Statics.gstrUserID = modGlobalConstants.Statics.clsSecurityClass.Get_OpID();
			// save the user name
			CheckDatabaseStructure();
			if (NewVersion_2(false))
			{
				// update the system if there are any changes
				modReplaceWorkFiles.GetGlobalVariables();
				frmWait.InstancePtr.IncrementProgress();
				modGlobalFunctions.LoadTRIOColors();
				// this will load the variables that hold the TRIO color values
				frmWait.InstancePtr.IncrementProgress();
				modGlobalFunctions.UpdateUsedModules();
				if (!modGlobalConstants.Statics.gboolUT)
				{
					MessageBox.Show("Utility Billing has expired.  Please call TRIO!", "Module Expired",
						MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
				}

				frmWait.InstancePtr.IncrementProgress();

				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\",
					"UTLastAccountNumber", ref strTemp);
				if (Strings.Trim(strTemp) != "")
				{
					modUTStatusPayments.Statics.lngCurrentAccountUT =
						FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				}

				frmWait.InstancePtr.IncrementProgress();
				frmWait.InstancePtr.IncrementProgress();
				modNewAccountBox.GetFormats();
				if (modGlobalConstants.Statics.gboolBD)
				{
					modAccountTitle.SetAccountFormats();
				}

				WWK.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (WWK.EndOfFile() != true && WWK.BeginningOfFile() != true)
				{
					
					// find the user settings location
					modGlobalConstants.Statics.gstrCityTown = FCConvert.ToString(WWK.Get_Fields_String("CityTown"));
				}

				// kgk 02-28-2012  Moved Invoice Cloud config from UT to GN database
				WWK.OpenRecordset(
					"SELECT EBillVendorID, EBillBillerID, EBillBillerPswd FROM EBillingConfiguration WHERE EBillVendorID = 'ICL'",
					"SystemSettings", 0, false, 0, false, "", false);
				if (WWK.ErrorNumber == 0 && WWK.RecordCount() > 0)
				{
					modGlobalConstants.Statics.gboolInvCld =
						FCConvert.CBool(WWK.Get_Fields_String("EBillBillerID") != "");
				}
				else
				{
					modGlobalConstants.Statics.gboolInvCld = false;
				}

				frmWait.InstancePtr.IncrementProgress();

				modUTCalculations.UTSetup();

				modMain.Statics.boolRE = false;
				// this is not RE/BL/CL
			}
			else
			{
				// new version failed and I do not want the user to enter
				MessageBox.Show("Fields were not added to the database.  Make sure that everyone is out of TRIO and try again.", "Error Adding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Application.Exit();
			}
		}

		public static void Main(CommandDispatcher commandDispatcher)
		{
			int lngErrCode = 0;
			try
			{
				InitializeUtilityBillingStatics();

				MDIParent.InstancePtr.Init(commandDispatcher);
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Utility Billing - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				Application.Exit();
			}
		}
		// vbPorter upgrade warning: 'Return' As DateTime	OnWrite(string)
		private static DateTime FindLastDay(ref string NewDate)
		{
			DateTime FindLastDay = System.DateTime.Now;
			DateTime xdate;
			// returns the last day of the current month
			xdate = (DateAndTime.DateAdd("m", 1, FCConvert.ToDateTime(NewDate)));
			xdate = DateAndTime.DateAdd("d", -1, xdate);
			FindLastDay = FCConvert.ToDateTime(Strings.Format(xdate, "MM/dd/yyyy"));
			return FindLastDay;
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmClearBooks, frmTableFileSetup, frmDataEntry, frmCalculation, frmCalculationSummary, frmBillsPreview, frmGetMasterAccount, frmUTGetDate, frmBookChoice, frmEditRateRecordTitles, frmCreateDatabaseExtract, frmLoadPreviousReadingFigures, frmCreateExtractFile, frmLienMaturityFees, frmElectronicDataEntrySummary, frmMeterChangeOut, frmPurgePaidBills, frmReprintPurgeReport, frmSetupDisconnectEditReport, frmSetupDisconnectNotices, frmSetupMeterReports, frmUTDateChange, frmLoadValidAccounts, frmICAccountSetup, frmCustomLisbonConsumption, frmCustomLisbonUpdateWinterBilling, frmViewDocuments, frmAddDocument, frmScanDocument, frmSequenceBook, frmConsumptionHistoryReport, frmUpdateAddresses, frmBillEmailOptions, frmEditCentralParties, frmCentralPartySearch, frmComment, frmSelectBankNumber, frmSelectPostingJournal)
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			// this checks to see if the form is already loaded and returns true if it is false otherwise
			foreach (Form frm in Application.OpenForms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}
		// vbPorter upgrade warning: lngValue As int	OnWrite(int, double, string)
		// vbPorter upgrade warning: intLength As short	OnWrite(short, string, double)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string PadToString_6(ref int lngValue, short intLength)
		{
			return PadToString(lngValue, intLength);
		}

		public static string PadToString_8(int lngValue, short intLength)
		{
			return PadToString(lngValue, intLength);
		}

		public static string PadToString(int lngValue, short intLength)
		{
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(lngValue) == true)
				lngValue = 0;
			if ((intLength - lngValue.ToString().Length) > 0)
			{
				PadToString = Strings.StrDup(intLength - lngValue.ToString().Length, "0") + FCConvert.ToString(lngValue);
			}
			else
			{
				PadToString = lngValue.ToString();
			}
			return PadToString;
		}

		public static void CloseChildForms()
		{
			//FC:FINAL:DSE #925 Cannot change a collection inside a foreach statement
			//foreach (FCForm frm in Application.OpenForms)
			//{
			//    if (frm.Name != "MDIParent")
			//    {
			//        if (!boolLoadHide) frm.Unload();
			//    }
			//}
			FCUtils.CloseFormsOnProject(Statics.boolLoadHide);
		}

		public static bool NewVersion_2(bool boolOverride)
		{
			return NewVersion( boolOverride);
		}

		public static bool NewVersion( bool boolOverride)
		{
			bool NewVersion = false;
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// if it is new version then check the general entry table and make sure that all the correct fields are there
				clsDRWrapper rsVersion = new clsDRWrapper();
				clsDRWrapper rsAdjust = new clsDRWrapper();
				bool boolUpdate;
				int lngMajor;
				int lngMinor;
				int lngRevsion;
				bool boolExists;
				bool boolNoEntryError;
				bool boolUpdateGlobalRU;
				if (boolOverride)
				{
					frmWait.InstancePtr.Init("Checking Database" + "\r\n" + "Please Wait...", true, 10);
				}
				if (boolOverride)
				{
					frmWait.InstancePtr.IncrementProgress();
					// Run function to add records to Account table
					// .OpenRecordset "SELECT * FROM tblIConnectAccountInfo", strUTDatabase
					// If .RecordCount > 0 Then
					// Do Nothing - table already populated
					// Else
					// Call InsertDefaultIConnectAccounts
					// End If
					// Run initial function to add records to the table
					// MAL@20081118: Changed to check accounts every time
					// Tracker Reference: 16194
					// .OpenRecordset "SELECT * FROM tblIConnectInfo", strUTDatabase
					// If .RecordCount > 0 Then
					// Do Nothing - table already populated
					// Else
					InsertIConnectRecords();
					// End If
					FixMasterBookNumbers();
				}
				modCoreysUTCode.MakeUpdatesToDatabase_2(true);
				frmWait.InstancePtr.Unload();
				NewVersion = true;
				return NewVersion;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Update Utility Billing Table - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return NewVersion;
		}

		public static int UTCloseOut()
		{
			int UTCloseOut = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will close out all of the payments
				string strSQL;
				clsDRWrapper rsUpdate = new clsDRWrapper();
				// this will find the next close out number
				rsUpdate.OpenRecordset("SELECT * FROM CloseOut", modExtraModules.strUTDatabase);
				rsUpdate.AddNew();
				rsUpdate.Set_Fields("Type", "UT");
				rsUpdate.Update();
				UTCloseOut = FCConvert.ToInt32(rsUpdate.Get_Fields_Int32("ID"));
				// update all of the payments with this number
				strSQL = "UPDATE PaymentRec SET DailyCloseOut = " + FCConvert.ToString(UTCloseOut) + " WHERE DailyCloseOut = 0";
				if (rsUpdate.Execute(strSQL, modExtraModules.strUTDatabase))
				{
					MessageBox.Show("Records Updated.", "Update Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show("Records not Updated.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				return UTCloseOut;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In UT Close Out", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UTCloseOut;
		}

		public static string TypeTitleText_2(int lngType)
		{
			return TypeTitleText(ref lngType);
		}

		public static string TypeTitleText(ref int lngType)
		{
			string TypeTitleText = "";
			// this function will return the type text of the type code passed in
			switch (lngType)
			{
				case 0:
					{
						// none
						TypeTitleText = "";
						break;
					}
				case 1:
					{
						// Consumption
						TypeTitleText = "Consumption";
						break;
					}
				case 2:
					{
						// Flat
						TypeTitleText = "Flat";
						break;
					}
				case 3:
					{
						// Units
						TypeTitleText = "Units";
						break;
					}
				case 4:
					{
						// Negative Consumption
						TypeTitleText = "Negative Consumption";
						break;
					}
			}
			//end switch
			return TypeTitleText;
		}

		public static string GetBulkMailerString()
		{
			string GetBulkMailerString = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBulk = new clsDRWrapper();
				int intCT;
				rsBulk.OpenRecordset("SELECT * FROM GlobalVariables", "SystemSettings");
				if (!rsBulk.EndOfFile())
				{
					for (intCT = 1; intCT <= 5; intCT++)
					{
						// TODO Get_Fields: Field [BulkMail] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsBulk.Get_Fields("BulkMail" + FCConvert.ToString(intCT))) != "")
						{
							// TODO Get_Fields: Field [BulkMail] not found!! (maybe it is an alias?)
							GetBulkMailerString = GetBulkMailerString + rsBulk.Get_Fields("BulkMail" + FCConvert.ToString(intCT)) + "\r\n";
						}
					}
				}
				else
				{
					GetBulkMailerString = "";
				}
				return GetBulkMailerString;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Getting Bulk Mailing Text", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetBulkMailerString;
		}

		public static bool NewOwnerUT(ref clsDRWrapper rsData, ref clsDRWrapper rsUT, ref bool boolUTMatch/*= false */)
		{
			bool NewOwnerUT = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will find out if there has been a new owner since the billing date
				clsDRWrapper rsSR = new clsDRWrapper();
				clsDRWrapper rsMaster = new clsDRWrapper();
				DateTime dtDate;

				rsMaster.OpenRecordset("SELECT BillDate FROM RateKeys WHERE ID = " + rsData.Get_Fields_Int32("BillingRateKey"), modExtraModules.strUTDatabase);
				if (!rsMaster.EndOfFile())
				{
					dtDate = (DateTime)rsMaster.Get_Fields_DateTime("BillDate");
				}
				else
				{
					dtDate = rsData.Get_Fields_DateTime("CreationDate");
					// default to the record creation date
				}
				NewOwnerUT = false;
				rsUT.FindFirstRecord("ID", rsData.Get_Fields_Int32("AccountKey"));
				if (!rsUT.NoMatch)
				{
					boolUTMatch = true;
					if (rsUT.Get_Fields_Boolean("UseREAccount") && rsUT.Get_Fields_Int32("REAccount") != 0)
					{
						rsMaster.OpenRecordset("SELECT DeedName1 FROM Master WHERE RSAccount = " + rsUT.Get_Fields_Int32("REAccount"), modExtraModules.strREDatabase);
						if (!rsMaster.EndOfFile())
						{
                            if (rsMaster.Get_Fields_String("DeedName1").Trim().ToLower() != rsData.Get_Fields_String("OName").Trim().ToLower())
							{
								rsSR.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + rsUT.Get_Fields_Int32("REAccount") + " AND SaleDate > '" + FCConvert.ToString(dtDate) + "' ORDER BY SaleDate Desc", modExtraModules.strREDatabase);
								if (!rsSR.EndOfFile())
								{
									NewOwnerUT = true;
									// this is a new owner so exit
									return NewOwnerUT;
								}
								rsSR.OpenRecordset("SELECT * FROM PreviousOwner WHERE Account = " + rsUT.Get_Fields_Int32("REAccount") + " AND SaleDate > '" + FCConvert.ToString(dtDate) + "'", modExtraModules.strREDatabase);
								if (!rsSR.EndOfFile())
								{
									NewOwnerUT = true;
									// this is a new owner so exit
									return NewOwnerUT;
								}
							}
						}
						else
						{
							// boolNameMatch = True
						}
					}
					else
					{					
                        if (rsUT.Get_Fields_String("DeedName1").Trim().ToLower() != rsData.Get_Fields_String("OName").Trim().ToLower())						
						{
							rsSR.OpenRecordset("SELECT * FROM PreviousOwner WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND SaleDate > '" + FCConvert.ToString(dtDate) + "'", modExtraModules.strUTDatabase);
							if (!rsSR.EndOfFile())
							{
								NewOwnerUT = true;
							}
						}
					}
				}
				else
				{
					boolUTMatch = false;
				}
				return NewOwnerUT;
			}
			catch
			{
				
				NewOwnerUT = false;
			}
			return NewOwnerUT;
		}

		public static bool CreateTransferTaxToLienBDEntry_6(double dblTotalPrin, int lngYear, bool boolWater)
		{
			return CreateTransferTaxToLienBDEntry(ref dblTotalPrin, ref lngYear, ref boolWater);
		}

		public static bool CreateTransferTaxToLienBDEntry_18(double dblTotalPrin, int lngYear, bool boolWater)
		{
			return CreateTransferTaxToLienBDEntry(ref dblTotalPrin, ref lngYear, ref boolWater);
		}

		public static bool CreateTransferTaxToLienBDEntry(ref double dblTotalPrin, ref int lngYear, ref bool boolWater)
		{
			bool CreateTransferTaxToLienBDEntry = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will create an entry in the BD database that will move monies from
				// the taxes receivable account to the lien receivable account
				clsDRWrapper rsTax = new clsDRWrapper();
				clsDRWrapper Master = new clsDRWrapper();
				clsDRWrapper rsEntries = new clsDRWrapper();
				string strSAccountNumber = "";
				string strWAccountNumber = "";
				string strSLAccountNumber = "";
				string strWLAccountNumber = "";
				string strCRSAccountNumber = "";
				string strCRWAccountNumber = "";
				string strCRSLAccountNumber = "";
				string strCRWLAccountNumber = "";
				int intFund = 0;
				string strTableName;
				int lngJournal = 0;
				clsBudgetaryPosting tPostInfo;
				// kk01282015 trouts-129  Automatic posting journals
				clsPostingJournalInfo tJournalInfo;
				CreateTransferTaxToLienBDEntry = true;
				if (!modGlobalConstants.Statics.gboolBD)
				{
					return CreateTransferTaxToLienBDEntry;
				}
				// Find Receivable Account Number
				Master.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
				if (!Master.EndOfFile())
				{
					if (boolWater)
					{
						Master.FindFirstRecord("Code", "WR");
						if (!Master.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strWAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
						}
						Master.FindFirstRecord("Code", "WL");
						if (!Master.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strWLAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
						}
					}
					else
					{
						Master.FindFirstRecord("Code", "SR");
						if (!Master.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strSAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
						}
						Master.FindFirstRecord("Code", "SL");
						if (!Master.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strSLAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
						}
					}
				}
				if (modGlobalConstants.Statics.gboolCR)
				{
					if (!boolWater)
					{
						Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 94", modExtraModules.strCRDatabase);
						if (!Master.EndOfFile())
						{
							if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
							{
								strCRSAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear, "00");
							}
							else
							{
								strCRSAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
							}
						}
						Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 95", modExtraModules.strCRDatabase);
						if (!Master.EndOfFile())
						{
							if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
							{
								strCRSLAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear, "00");
							}
							else
							{
								strCRSLAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
							}
						}
					}
					else
					{
						Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 96", modExtraModules.strCRDatabase);
						if (!Master.EndOfFile())
						{
							if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
							{
								strCRWLAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear, "00");
							}
							else
							{
								strCRWLAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
							}
						}
						Master.OpenRecordset("SELECT * FROM Type WHERE Typecode = 93", modExtraModules.strCRDatabase);
						if (!Master.EndOfFile())
						{
							if (FCConvert.ToBoolean(Master.Get_Fields_Boolean("Year1")))
							{
								strCRWAccountNumber = Strings.Left(FCConvert.ToString(Master.Get_Fields_String("Account1")), FCConvert.ToString(Master.Get_Fields_String("Account1")).Length - 2) + Strings.Format(lngYear, "00");
							}
							else
							{
								strCRWAccountNumber = FCConvert.ToString(Master.Get_Fields_String("Account1"));
							}
						}
					}
				}
				else
				{
					// must find fund from the UT Customize screen for towns with UT+BD but not CR
					intFund = 1;
					// get the fund and account from UT and use 00 for year
					if (boolWater)
					{
						strCRWAccountNumber = "G " + Strings.Format(intFund, Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + "-" + strWAccountNumber + "-00";
						strCRWLAccountNumber = "G " + Strings.Format(intFund, Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + "-" + strWLAccountNumber + "-00";
					}
					else
					{
						strCRSAccountNumber = "G " + Strings.Format(intFund, Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + "-" + strSAccountNumber + "-00";
						strCRSLAccountNumber = "G " + Strings.Format(intFund, Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + "-" + strSLAccountNumber + "-00";
					}
				}
				// kk07302015 trout-927  Add checks for std accounts being all zeros
				if (boolWater)
				{
					if (Strings.Trim(strWAccountNumber) == "" || Conversion.Val(strWAccountNumber) == 0)
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter the Water Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
					if (Strings.Trim(strWLAccountNumber) == "" || Conversion.Val(strWLAccountNumber) == 0)
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter the Water Lien Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
					if (Strings.Trim(strCRWAccountNumber) == "")
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter the Water Receivable Account in Cash Receipts.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
					if (Strings.Trim(strCRWLAccountNumber) == "")
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter the Water Lien Receivable Account in Cash Receipts.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
					if (Conversion.Val(Strings.Mid(strCRWLAccountNumber, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + 4), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) != Conversion.Val(strWLAccountNumber))
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Your Water Lien Receivable Account in Cash Receipts and Budgetary do not match.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
				}
				else
				{
					if (Strings.Trim(strSAccountNumber) == "" || Conversion.Val(strSAccountNumber) == 0)
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter the Sewer Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
					if (Strings.Trim(strSLAccountNumber) == "" || Conversion.Val(strSLAccountNumber) == 0)
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter the Sewer Lien Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
					if (Strings.Trim(strCRSAccountNumber) == "")
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter the Sewer Receivable Account in Cash Receipts.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
					if (Strings.Trim(strCRSLAccountNumber) == "")
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter the Sewer Lien Receivable Account in Cash Receipts.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
					if (Conversion.Val(Strings.Mid(strCRSLAccountNumber, FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 1, 2)) + 4), FCConvert.ToInt32(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2))))) != Conversion.Val(strSLAccountNumber))
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Your Sewer Lien Receivable Account in Cash Receipts and Budgetary do not match.", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTransferTaxToLienBDEntry = false;
						return CreateTransferTaxToLienBDEntry;
					}
				}
				// Add Journal
				// ***********************************************************************************************************
				strTableName = "";
				// get journal number
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					// add entries to incomplete journals table
					strTableName = "Temp";
				}
				else
				{
					Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngJournal = Master.Get_Fields("JournalNumber") + 1;
					}
					else
					{
						lngJournal = 1;
					}
					Master.AddNew();
					Master.Set_Fields("JournalNumber", lngJournal);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					if (boolWater)
					{
						Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Water LN");
					}
					else
					{
						Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Sewer LN");
					}
					Master.Set_Fields("Type", "GJ");
					Master.Set_Fields("Period", DateTime.Today.Month);
					Master.Update();
					Master.Reset();
					modBudgetaryAccounting.UnlockJournal();
				}
				// Add Journal Entries
				// ************************************************************************************************************
				rsEntries.OpenRecordset("SELECT * FROM " + strTableName + "JournalEntries WHERE ID = 0", "TWBD0000.vb1");
				Master.OpenRecordset("SELECT * FROM UtilityBilling");
				if (boolWater)
				{
					// Add Water Entries
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
					rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Water LN");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					// intFund = GetFundFromAccount(Master.Fields("WaterPrincipalAccount"))    'get the fund from the principal account
					rsEntries.Set_Fields("Account", strCRWAccountNumber);
					// "G " & Format(intFund, String(Val(Mid$(Ledger, 1, 2)), "0")) & "-" & Format(Val(strWAccountNumber), String(Val(Mid$(Ledger, 3, 2)), "0")) & "-" & Format(lngYear, String(Val(Mid$(Ledger, 3, 2)), "0"))
					rsEntries.Set_Fields("Amount", dblTotalPrin);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", DateTime.Today.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
					rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Water LN");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					// intFund = GetFundFromAccount(Master.Fields("WaterPrincipalAccount"))
					rsEntries.Set_Fields("Account", strCRWLAccountNumber);
					// "G " & Format(intFund, String(Val(Mid$(Ledger, 1, 2)), "0")) & "-" & Format(Val(strWAccountNumber), String(Val(Mid$(Ledger, 3, 2)), "0")) & "-" & Format(lngYear, String(Val(Mid$(Ledger, 3, 2)), "0"))
					rsEntries.Set_Fields("Amount", dblTotalPrin * -1);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", DateTime.Today.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
				}
				else
				{
					// Add Sewer Entries
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
					rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Sewer LN");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					rsEntries.Set_Fields("Account", strCRSAccountNumber);
					rsEntries.Set_Fields("Amount", dblTotalPrin * -1);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", DateTime.Today.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
					rsEntries.AddNew();
					rsEntries.Set_Fields("Type", "G");
					rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
					rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Sewer LN");
					rsEntries.Set_Fields("JournalNumber", lngJournal);
					rsEntries.Set_Fields("Account", strCRSLAccountNumber);
					rsEntries.Set_Fields("Amount", dblTotalPrin);
					rsEntries.Set_Fields("WarrantNumber", 0);
					rsEntries.Set_Fields("Period", DateTime.Today.Month);
					rsEntries.Set_Fields("RCB", "R");
					rsEntries.Set_Fields("Status", "E");
					rsEntries.Update();
				}
				frmWait.InstancePtr.Unload();
				// kk01282015 trouts-129  Add automatic posting of journals
				if (lngJournal == 0)
				{
					MessageBox.Show("Unable to complete the journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					if (modSecurityValidation.ValidPermissions(null, UTSECAUTOPOSTJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptUTLiens))
					{
						if (MessageBox.Show("The entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							tJournalInfo = new clsPostingJournalInfo();
							tJournalInfo.JournalNumber = lngJournal;
							tJournalInfo.JournalType = "GJ";
							tJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
							tJournalInfo.CheckDate = "";
							tPostInfo = new clsBudgetaryPosting();
							tPostInfo.AddJournal(tJournalInfo);
							tPostInfo.AllowPreview = true;
							tPostInfo.PageBreakBetweenJournalsOnReport = true;
							tPostInfo.WaitForReportToEnd = true;
							tPostInfo.PostJournals();
							tJournalInfo = null;
							tPostInfo = null;
						}
					}
					else
					{
						MessageBox.Show("The Budgetary journal entries were created in journal #" + FCConvert.ToString(lngJournal) + ".", "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				return CreateTransferTaxToLienBDEntry;
			}
			catch (Exception ex)
			{
				
				CreateTransferTaxToLienBDEntry = false;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateTransferTaxToLienBDEntry;
		}

		public static bool CreateUTJournalEntry(ref string strBillList)
		{
			bool CreateUTJournalEntry = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsEntries = new clsDRWrapper();
				clsDRWrapper rsBillInfo = new clsDRWrapper();
				clsDRWrapper Master = new clsDRWrapper();
				string strTableName;
				int lngJournal = 0;
				string strSAccountNumber = "";
				string strWAccountNumber = "";
				string strSTAAccountNum = "";
				string strWTAAccountNum = "";
				string strSQL = "";
				double[] dblSFundAmount = new double[99 + 1];
				double[] dblWFundAmount = new double[99 + 1];
				int intFund = 0;
				bool blnIsTA;
				clsBudgetaryPosting tPostInfo;
				// kk01282015 trouts-129  Add automatic posting of journals
				clsPostingJournalInfo tJournalInfo;
				CreateUTJournalEntry = true;
				// Find Receivable Account Number
				Master.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
				if (!Master.EndOfFile())
				{
					Master.FindFirstRecord("Code", "SR");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strSAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "WR");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strWAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "ST");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strSTAAccountNum = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "WT");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strWTAAccountNum = FCConvert.ToString(Master.Get_Fields("Account"));
					}
				}
				// kk07302015 trout-927  Add checks for std accounts being all zeros
				if (modUTStatusPayments.Statics.TownService != "W")
				{
					if (Strings.Trim(strSAccountNumber) == "" || Conversion.Val(strSAccountNumber) == 0)
					{
						MessageBox.Show("Please enter the Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateUTJournalEntry = false;
						return CreateUTJournalEntry;
					}
				}
				if (modUTStatusPayments.Statics.TownService != "S")
				{
					if (Strings.Trim(strWAccountNumber) == "" || Conversion.Val(strWAccountNumber) == 0)
					{
						MessageBox.Show("Please enter the Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateUTJournalEntry = false;
						return CreateUTJournalEntry;
					}
				}
				strTableName = "";
				// get journal number
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					// add entries to incomplete journals table
					strTableName = "Temp";
				}
				else
				{
					Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngJournal = Master.Get_Fields("JournalNumber") + 1;
					}
					else
					{
						lngJournal = 1;
					}
					Master.AddNew();
					Master.Set_Fields("JournalNumber", lngJournal);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Bill Run");
					Master.Set_Fields("Type", "GJ");
					Master.Set_Fields("Period", DateTime.Today.Month);
					Master.Update();
					Master.Reset();
					modBudgetaryAccounting.UnlockJournal();
				}
				rsEntries.OpenRecordset("SELECT * FROM " + strTableName + "JournalEntries WHERE ID = 0", "TWBD0000.vb1");
				if (modUTStatusPayments.Statics.TownService != "S")
				{
					// Add Water Entries
					// MAL@20080310: Change to exclude Tax Acquired Accounts
					// Tracker Reference: 12615
					strSQL = "SELECT SUM(Amount) as Amt, AccountNumber FROM Breakdown WHERE BillKey IN (" + strBillList + ") AND Service = 'W' AND ISNULL(TaxAcquired,0) = 0 GROUP BY AccountNumber";
					rsBillInfo.OpenRecordset(strSQL);
					while (!rsBillInfo.EndOfFile())
					{
						rsEntries.AddNew();
						rsEntries.Set_Fields("Type", "G");
						rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
						rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Water");
						rsEntries.Set_Fields("JournalNumber", lngJournal);
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						intFund = modBudgetaryAccounting.GetFundFromAccount_2(rsBillInfo.Get_Fields("AccountNumber"));
						if (intFund > 0)
						{
							// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
							dblWFundAmount[intFund] += Conversion.Val(rsBillInfo.Get_Fields("Amt"));
						}
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						rsEntries.Set_Fields("Account", rsBillInfo.Get_Fields("AccountNumber"));
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						rsEntries.Set_Fields("Amount", rsBillInfo.Get_Fields("Amt") * -1);
						rsEntries.Set_Fields("WarrantNumber", 0);
						rsEntries.Set_Fields("Period", DateTime.Today.Month);
						rsEntries.Set_Fields("RCB", "R");
						rsEntries.Set_Fields("Status", "E");
						rsEntries.Update();
						rsBillInfo.MoveNext();
					}
					for (intFund = 1; intFund <= 99; intFund++)
					{
						if (dblWFundAmount[intFund] != 0)
						{
							// Water Receivable Entry
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT Water - Receive");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							rsEntries.Set_Fields("Account", "G " + Strings.Format(intFund.ToString(), Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "-" + strWAccountNumber + "-00");
							rsEntries.Set_Fields("Amount", dblWFundAmount[intFund]);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
					}
					// Add Water Entries
					// MAL@20080310: Add separate entry for Tax Acquired Accounts
					// Tracker Reference: 12615
					FCUtils.EraseSafe(dblWFundAmount);
					strSQL = "SELECT SUM(Amount) as Amt, AccountNumber FROM Breakdown WHERE BillKey IN (" + strBillList + ") AND Service = 'W' AND ISNULL(TaxAcquired,0) = 1 GROUP BY AccountNumber";
					rsBillInfo.OpenRecordset(strSQL);
					while (!rsBillInfo.EndOfFile())
					{
						rsEntries.AddNew();
						rsEntries.Set_Fields("Type", "G");
						rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
						rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Water TA");
						rsEntries.Set_Fields("JournalNumber", lngJournal);
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						intFund = modBudgetaryAccounting.GetFundFromAccount_2(rsBillInfo.Get_Fields("AccountNumber"));
						if (intFund > 0)
						{
							// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
							dblWFundAmount[intFund] += Conversion.Val(rsBillInfo.Get_Fields("Amt"));
						}
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						rsEntries.Set_Fields("Account", rsBillInfo.Get_Fields("AccountNumber"));
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						rsEntries.Set_Fields("Amount", rsBillInfo.Get_Fields("Amt") * -1);
						rsEntries.Set_Fields("WarrantNumber", 0);
						rsEntries.Set_Fields("Period", DateTime.Today.Month);
						rsEntries.Set_Fields("RCB", "R");
						rsEntries.Set_Fields("Status", "E");
						rsEntries.Update();
						rsBillInfo.MoveNext();
					}
					for (intFund = 1; intFund <= 99; intFund++)
					{
						if (dblWFundAmount[intFund] != 0)
						{
							// Water Receivable Entry
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT Water - TA Receive");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							rsEntries.Set_Fields("Account", "G " + Strings.Format(intFund.ToString(), Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "-" + strWTAAccountNum + "-00");
							rsEntries.Set_Fields("Amount", dblWFundAmount[intFund]);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
					}
				}
				if (modUTStatusPayments.Statics.TownService != "W")
				{
					// Add Sewer Entries
					strSQL = "SELECT SUM(Amount) as Amt, AccountNumber FROM Breakdown WHERE BillKey IN (" + strBillList + ") AND Service = 'S' AND ISNULL(TaxAcquired,0) = 0 GROUP BY AccountNumber";
					rsBillInfo.OpenRecordset(strSQL);
					while (!rsBillInfo.EndOfFile())
					{
						rsEntries.AddNew();
						rsEntries.Set_Fields("Type", "G");
						rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
						rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Sewer");
						rsEntries.Set_Fields("JournalNumber", lngJournal);
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						intFund = modBudgetaryAccounting.GetFundFromAccount_2(rsBillInfo.Get_Fields("AccountNumber"));
						if (intFund > 0)
						{
							// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
							dblSFundAmount[intFund] += Conversion.Val(rsBillInfo.Get_Fields("Amt"));
						}
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						rsEntries.Set_Fields("Account", rsBillInfo.Get_Fields("AccountNumber"));
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						rsEntries.Set_Fields("Amount", rsBillInfo.Get_Fields("Amt") * -1);
						rsEntries.Set_Fields("WarrantNumber", 0);
						rsEntries.Set_Fields("Period", DateTime.Today.Month);
						rsEntries.Set_Fields("RCB", "R");
						rsEntries.Set_Fields("Status", "E");
						rsEntries.Update();
						rsBillInfo.MoveNext();
					}
					for (intFund = 1; intFund <= 99; intFund++)
					{
						if (dblSFundAmount[intFund] != 0)
						{
							// Sewer Receivable Entry
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT Sewer - Receive");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							rsEntries.Set_Fields("Account", "G " + Strings.Format(intFund.ToString(), Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "-" + strSAccountNumber + "-00");
							rsEntries.Set_Fields("Amount", dblSFundAmount[intFund]);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
					}
					// Add Sewer Entries - TaxAcquired
					// Tracker Reference: 12615
					FCUtils.EraseSafe(dblSFundAmount);
					strSQL = "SELECT SUM(Amount) as Amt, AccountNumber FROM Breakdown WHERE BillKey IN (" + strBillList + ") AND Service = 'S' AND ISNULL(TaxAcquired,0) = 1 GROUP BY AccountNumber";
					rsBillInfo.OpenRecordset(strSQL);
					while (!rsBillInfo.EndOfFile())
					{
						rsEntries.AddNew();
						rsEntries.Set_Fields("Type", "G");
						rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
						rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT - Sewer TA");
						rsEntries.Set_Fields("JournalNumber", lngJournal);
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						intFund = modBudgetaryAccounting.GetFundFromAccount_2(rsBillInfo.Get_Fields("AccountNumber"));
						if (intFund > 0)
						{
							// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
							dblSFundAmount[intFund] += Conversion.Val(rsBillInfo.Get_Fields("Amt"));
						}
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						rsEntries.Set_Fields("Account", rsBillInfo.Get_Fields("AccountNumber"));
						// TODO Get_Fields: Field [Amt] not found!! (maybe it is an alias?)
						rsEntries.Set_Fields("Amount", rsBillInfo.Get_Fields("Amt") * -1);
						rsEntries.Set_Fields("WarrantNumber", 0);
						rsEntries.Set_Fields("Period", DateTime.Today.Month);
						rsEntries.Set_Fields("RCB", "R");
						rsEntries.Set_Fields("Status", "E");
						rsEntries.Update();
						rsBillInfo.MoveNext();
					}
					for (intFund = 1; intFund <= 99; intFund++)
					{
						if (dblSFundAmount[intFund] != 0)
						{
							// Sewer Receivable Entry
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields(strTableName + "JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT Sewer - TA Receive");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							rsEntries.Set_Fields("Account", "G " + Strings.Format(intFund.ToString(), Strings.StrDup(FCConvert.ToInt32(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2))), "0")) + "-" + strSTAAccountNum + "-00");
							rsEntries.Set_Fields("Amount", dblSFundAmount[intFund]);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
					}
				}
				// kk01282015 trouts-129  Add automatic posting of journals
				frmWait.InstancePtr.Unload();
				if (lngJournal == 0)
				{
					MessageBox.Show("Unable to complete the journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					if (modSecurityValidation.ValidPermissions(null, UTSECAUTOPOSTJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptUTBills))
					{
						if (MessageBox.Show("The entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							tJournalInfo = new clsPostingJournalInfo();
							tJournalInfo.JournalNumber = lngJournal;
							tJournalInfo.JournalType = "GJ";
							tJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
							tJournalInfo.CheckDate = "";
							tPostInfo = new clsBudgetaryPosting();
							tPostInfo.AddJournal(tJournalInfo);
							tPostInfo.AllowPreview = true;
							tPostInfo.PageBreakBetweenJournalsOnReport = true;
							tPostInfo.WaitForReportToEnd = true;
							tPostInfo.PostJournals();
							tJournalInfo = null;
							tPostInfo = null;
						}
					}
					else
					{
						MessageBox.Show("The Budgetary journal entries were created in journal #" + FCConvert.ToString(lngJournal) + ".", "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				return CreateUTJournalEntry;
			}
			catch (Exception ex)
			{
				
				CreateUTJournalEntry = false;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Journal", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateUTJournalEntry;
		}

		public static void ResetBookStatus_2(int lngBook, bool boolResetMeters = true, string strStatus = "DE")
		{
			ResetBookStatus(lngBook, boolResetMeters, strStatus);
		}

		public static void ResetBookStatus(int lngBook, bool boolResetMeters = true, string strStatus = "DE")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBook = new clsDRWrapper();
				clsDRWrapper rsM = new clsDRWrapper();
				if (lngBook != 0)
				{
					rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(lngBook), modExtraModules.strUTDatabase);
					if (!rsBook.EndOfFile())
					{
						if ((Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "B") || (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "C"))
						{
							boolResetMeters = false;
						}
						else
						{
							// if the book is not cleared or billed then reset the status
							rsBook.Edit();
							rsBook.Set_Fields("CurrentStatus", strStatus);
							rsBook.Update();
						}
					}
					if (boolResetMeters)
					{
						rsM.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(lngBook), modExtraModules.strUTDatabase);
						while (!rsM.EndOfFile())
						{
							if ((Strings.Left(FCConvert.ToString(rsM.Get_Fields_String("BillingStatus")), 1) == "B") || (Strings.Left(FCConvert.ToString(rsM.Get_Fields_String("BillingStatus")), 1) == "C"))
							{
							}
							else
							{
								// if the book is not cleared or billed then reset the status
								rsM.Edit();
								rsM.Set_Fields("BillingStatus", Strings.Left(strStatus, 1));
								rsM.Update();
							}
							rsM.MoveNext();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Resetting Book Status - " + FCConvert.ToString(lngBook), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void GetMasterAccount(int lngAccountKey, int lngAccountNumber)
		{
			frmAccountMaster.InstancePtr.Init(lngAccountKey, lngAccountNumber);
		}

		private static void CheckActualAccountNumber()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAAN = new clsDRWrapper();
				rsAAN.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = 0 and AccountKey <> 0", modExtraModules.strUTDatabase);
				while (!rsAAN.EndOfFile())
				{
					rsAAN.Edit();
					rsAAN.Set_Fields("ActualAccountNumber", modUTStatusPayments.GetAccountNumber_2(rsAAN.Get_Fields_Int32("AccountKey")));
					rsAAN.Update();
					rsAAN.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Check Account Numbers", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ClearUTCMFNumbers(bool boolOverride = false)
		{
			clsDRWrapper rsVersion = new clsDRWrapper();
			if (boolOverride)
			{
				rsVersion.Execute("DELETE FROM CMFNumbers", modExtraModules.strUTDatabase);
				modGlobalFunctions.AddCYAEntry_26("UT", "Clearing CMF Numbers Table", "Override");
			}
			else
			{
				switch (MessageBox.Show("Are you sure that you would like to clear the Certified Mail Form Table?", "Clear Table", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							rsVersion.Execute("DELETE FROM CMFNumbers", modExtraModules.strUTDatabase);
							modGlobalFunctions.AddCYAEntry_8("UT", "Clearing CMF Numbers Table");
							MessageBox.Show("Table cleared.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							break;
						}
					default:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
		}

		private static void FillDefaultFreeReports()
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTest = new clsDRWrapper();
				clsDRWrapper rsFill = new clsDRWrapper();
				string strBuild;
				intErr = 1;
				rsTest.OpenRecordset("SELECT * FROM SavedFreeReports WHERE LEFT(ReportName,7) = 'DEFAULT'", modExtraModules.strUTDatabase);
				intErr = 2;
				while (!rsTest.EndOfFile())
				{
					intErr = 3;
					// delete the default records and replace it with this one
					rsTest.Delete();
					rsTest.MoveNext();
				}
				intErr = 4;
				// Create the default notices
				rsFill.OpenRecordset("SELECT * FROM SavedFreeReports", modExtraModules.strUTDatabase);
				// 30 DN
				intErr = 5;
				rsFill.AddNew();
				rsFill.Set_Fields("ReportName", "DEFAULT30 Day Notice");
				rsFill.Set_Fields("Type", "30DAYNOTICEW");
				strBuild = "\r\n";
				strBuild += "<NAMEADDRESS>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "    You are hereby notified that you are obligated to pay within 30 days after this notice is served or mailed, as the case may be, a rate, toll, rent or other charge in the amount of <TOTALDUE> plus interest and costs as provided by statute.  A lien is claimed by the <UTILITYTITLE>, County of <COUNTY>, State of <STATE>, on the following described real estate to secure the payment of said rate, toll, rent or other charge." + "\r\n";
				strBuild += "\r\n";
				strBuild += "Real estate being described as follows:" + "\r\n";
				strBuild += "    Real estate located at:  <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "    Map and Lot:             <MAPLOT>" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "    Map and lot numbers refer to such numbers as found on tax maps of the <CITYTOWNOF> <MUNI>, prepared by:  <PREPARER> and dated <PREPAREDATE>, on file at <CITYTOWNOF> <MUNI> municipal offices." + "\r\n";
				}
				else
				{
					strBuild += "    Map and lot numbers refer to such numbers as found on tax maps of the <MUNI>, prepared by:  <PREPARER> and dated <PREPAREDATE>, on file at <MUNI> municipal offices." + "\r\n";
				}
				strBuild += "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "Note: The <CITYTOWNOF> <MUNI> is willing to arrange installment payments of the outstanding debt." + "\r\n";
				}
				else
				{
					strBuild += "Note: The <MUNI> is willing to arrange installment payments of the outstanding debt." + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "In witness whereof, I have hereunto set my hand this <MAILDATE>." + "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 6;
				rsFill.AddNew();
				rsFill.Set_Fields("ReportName", "DEFAULT30 Day Notice");
				rsFill.Set_Fields("Type", "30DAYNOTICES");
				strBuild = "\r\n";
				strBuild += "<NAMEADDRESS>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "    You are hereby notified that you are obligated to pay within 30 days after this notice is served or mailed, as the case may be, a rate, toll, rent or other charge in the amount of <TOTALDUE> plus interest and costs as provided by statute.  A lien is claimed by the <UTILITYTITLE>, County of <COUNTY>, State of <STATE>, on the following described real estate to secure the payment of said rate, toll, rent or other charge." + "\r\n";
				strBuild += "\r\n";
				strBuild += "Real estate being described as follows:" + "\r\n";
				strBuild += "    Real estate located at:  <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "    Map and Lot:             <MAPLOT>" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "    Map and lot numbers refer to such numbers as found on tax maps of the <CITYTOWNOF> <MUNI>, prepared by:  <PREPARER> and dated <PREPAREDATE>, on file at <CITYTOWNOF> <MUNI> municipal offices." + "\r\n";
					strBuild += "\r\n";
					strBuild += "Note: The <CITYTOWNOF> <MUNI> is willing to arrange installment payments of the outstanding debt." + "\r\n";
				}
				else
				{
					strBuild += "    Map and lot numbers refer to such numbers as found on tax maps of the <MUNI>, prepared by:  <PREPARER> and dated <PREPAREDATE>, on file at <MUNI> municipal offices." + "\r\n";
					strBuild += "\r\n";
					strBuild += "Note: The <MUNI> is willing to arrange installment payments of the outstanding debt." + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "In witness whereof, I have hereunto set my hand this <MAILDATE>." + "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				// Lien Notices
				intErr = 9;
				rsFill.AddNew();
				rsFill.Set_Fields("ReportName", "DEFAULTUtility Lien Certificate");
				rsFill.Set_Fields("Type", "LIENPROCESSW");
				strBuild = "\r\n";
				strBuild += "<ADDRESSBAR>" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "    I, <COLLECTOR>, Treasurer for the <CITYTOWNOF> of <MUNI>, a municipal corporation located in the County of <COUNTY>, State of <STATE>, hereby give you notice that a rate, toll, rent or other charge in the amount of <PRINCIPAL> has been assessed, and was committed to me for collection on <COMMITMENT>, against real estate in said <CITYTOWNOF> of <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:" + "\r\n";
				}
				else
				{
					strBuild += "    I, <COLLECTOR>, Treasurer for the <MUNI>, located in the County of <COUNTY>, State of <STATE>, hereby give you notice that a rate, toll, rent or other charge in the amount of <PRINCIPAL> has been assessed, and was committed to me for collection on <COMMITMENT>, against real estate in said <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:" + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "Real Estate located at:    <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Map Lot Description:       <MAPLOT>" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "Map and Lot numbers refer to such numbers as found on tax maps of the <CITYTOWNOF> of <MUNI>, prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <CITYTOWNOF> of <MUNI> municipal office." + "\r\n";
				}
				else
				{
					strBuild += "Map and Lot numbers refer to such numbers as found on tax maps of the <MUNI>, prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <MUNI> municipal office." + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "I give you further notice that said rate, toll, rent or other charge, together with interest in the amount of <INTEREST>, which has been added to and has become part of said rate, toll, rent or other charge, remains unpaid;  That a lien is claimed on said real estate, above described, to secure the payment of said rate, toll, rent or other charge; that proper demand for payment of said rate, toll, rent or other charge has been made in accordance with <LEGALDESCRIPTION>." + "\r\n";
				// kk 01242013 trout-817  'Title 36, Section 942, revised statutes of 1964, as amended." & vbCrLf
				strBuild += "\r\n";
				strBuild += "<SIGNATURELINE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "NOTICE: The municipality has policy under 36 M.R.S.A. Section 906 to apply all payments to the oldest outstanding rate, toll, rent or other charge obligation.  If you are uncertain of the status on this property, contact the Treasurer." + "\r\n";
				strBuild += "NOTICE: Partial payments do not waive a lien." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<COUNTY>,  SS. State of <STATE>  <MUNI>, <STATE>  <FILINGDATE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Then personally appeared the above named <COLLECTOR>, Treasurer, and acknowledged the foregoing instrument to be his free act and deed in his said capacity." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                                    Before Me,__________________" + "\r\n";
				strBuild += "                                    <SIGNER>" + "\r\n";
				strBuild += "                                    <JOP>" + "\r\n";
				strBuild += "                                    <COMMISSIONEXPIRATION>" + "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 10;
				rsFill.AddNew();
				rsFill.Set_Fields("ReportName", "DEFAULTUtility Lien Certificate");
				rsFill.Set_Fields("Type", "LIENPROCESSS");
				strBuild = "\r\n";
				strBuild += "<ADDRESSBAR>" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "    I, <COLLECTOR>, Treasurer for the <CITYTOWNOF> of <MUNI>, a municipal corporation located in the County of <COUNTY>, State of <STATE>, hereby give you notice that a rate, toll, rent or other charge in the amount of <PRINCIPAL> has been assessed, and was committed to me for collection on <COMMITMENT>, against real estate in said <CITYTOWNOF> of <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:" + "\r\n";
				}
				else
				{
					strBuild += "    I, <COLLECTOR>, Treasurer for <MUNI>, located in the County of <COUNTY>, State of <STATE>, hereby give you notice that a rate, toll, rent or other charge in the amount of <PRINCIPAL> has been assessed, and was committed to me for collection on <COMMITMENT>, against real estate in said <MUNI>, and against <OWNER> as owner(s) thereof, said real estate being described as follows:" + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "Real Estate located at:    <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Map Lot Description:       <MAPLOT>" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					strBuild += "Map and Lot numbers refer to such numbers as found on tax maps of the <CITYTOWNOF> of <MUNI>, prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <CITYTOWNOF> of <MUNI> municipal office." + "\r\n";
				}
				else
				{
					strBuild += "Map and Lot numbers refer to such numbers as found on tax maps of the <MUNI>, prepared by: <PREPARER> and dated <PREPAREDATE>, on file at the <MUNI> municipal office." + "\r\n";
				}
				strBuild += "\r\n";
				strBuild += "I give you further notice that said rate, toll, rent or other charge, together with interest in the amount of <INTEREST>, which has been added to and has become part of said rate, toll, rent or other charge, remains unpaid;  That a lien is claimed on said real estate, above described, to secure the payment of said rate, toll, rent or other charge; that proper demand for payment of said rate, toll, rent or other charge has been made in accordance with Title 38, Section 1208." + "\r\n";
				// , revised statutes of 1964, as amended
				strBuild += "\r\n";
				strBuild += "<SIGNATURELINE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "NOTICE: The municipality has policy under 38 M.R.S.A. Section 1208 to apply all payments to the oldest outstanding rate, toll, rent or other charge obligation.  If you are uncertain of the status on this property, contact the Treasurer." + "\r\n";
				strBuild += "NOTICE: Partial payments do not waive a lien." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<COUNTY>,  SS. State of <STATE>  <MUNI>, <STATE>  <FILINGDATE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "Then personally appeared the above named <COLLECTOR>, Treasurer, and acknowledged the foregoing instrument to be his/her free act and deed in his/her said capacity." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                                    Before Me,__________________" + "\r\n";
				strBuild += "                                    <SIGNER>" + "\r\n";
				strBuild += "                                    <JOP>" + "\r\n";
				strBuild += "                                    <COMMISSIONEXPIRATION>" + "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 15;
				// Lien Maturity
				rsFill.AddNew();
				rsFill.Set_Fields("ReportName", "DEFAULTLien Maturity Notice");
				rsFill.Set_Fields("Type", "LIENMATURITYW");
				strBuild = "\r\n";
				strBuild += "                                                    <MAILDATE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "IMPORTANT: DO NOT DISREGARD THIS NOTICE.  YOU WILL LOSE YOUR PROPERTY UNLESS YOU PAY YOUR WATER RATE, TOLL, RENT OR OTHER CHARGE, INTEREST AND COSTS." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<ADDRESSBAR>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "You are the party named on a water lien certificate filed on <LIENFILINGDATE>, and recorded in <BOOKPAGE> in the <COUNTY> County Registry of Deeds.  This filing has created a water lien mortgage on the real estate described therein." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                Map & Lot:  <MAPLOT>" + "\r\n";
				strBuild += "                Location:   <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "On <FORECLOSUREDATE>, the water lien mortgage will be foreclosed and your right to recover your property by paying the rate, toll, rent or other charges together with interest and costs that are owed will expire." + "\r\n";
				strBuild += "\r\n";
				strBuild += "IF THE WATER LIEN FORECLOSES, THE <ORGANIZATION> WILL OWN YOUR PROPERTY." + "\r\n";
				strBuild += "\r\n";
				strBuild += "If you cannot pay the amount that you owe, please contact me to discuss this notice." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                  *************************************" + "\r\n";
				strBuild += "                  * IF YOU ARE A DEBTOR IN BANKRUPTCY,*" + "\r\n";
				strBuild += "                  * THIS NOTICE DOES NOT APPLY TO YOU.*" + "\r\n";
				strBuild += "                  *************************************" + "\r\n";
				strBuild += "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 20;
				rsFill.AddNew();
				rsFill.Set_Fields("ReportName", "DEFAULTLien Maturity Notice");
				rsFill.Set_Fields("Type", "LIENMATURITYS");
				strBuild = "\r\n";
				strBuild += "                                                    <MAILDATE>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "IMPORTANT: DO NOT DISREGARD THIS NOTICE.  YOU WILL LOSE YOUR PROPERTY UNLESS YOU PAY YOUR SEWER RATE, TOLL, RENT OR OTHER CHARGE, INTEREST AND COSTS." + "\r\n";
				strBuild += "\r\n";
				strBuild += "<ADDRESSBAR>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "You are the party named on a sewer lien certificate filed on <LIENFILINGDATE>, and recorded in <BOOKPAGE> in the <COUNTY> County Registry of Deeds.  This filing has created a sewer lien mortgage on the real estate described therein." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                Map & Lot:  <MAPLOT>" + "\r\n";
				strBuild += "                Location:   <LOCATION>" + "\r\n";
				strBuild += "\r\n";
				strBuild += "On <FORECLOSUREDATE>, the sewer lien mortgage will be foreclosed and your right to recover your property by paying the rate, toll, rent or other charges together with interest and costs that are owed will expire." + "\r\n";
				strBuild += "\r\n";
				strBuild += "IF THE SEWER LIEN FORECLOSES, THE <ORGANIZATION> WILL OWN YOUR PROPERTY." + "\r\n";
				strBuild += "\r\n";
				strBuild += "If you cannot pay the amount that you owe, please contact me to discuss this notice." + "\r\n";
				strBuild += "\r\n";
				strBuild += "                  *************************************" + "\r\n";
				strBuild += "                  * IF YOU ARE A DEBTOR IN BANKRUPTCY,*" + "\r\n";
				strBuild += "                  * THIS NOTICE DOES NOT APPLY TO YOU.*" + "\r\n";
				strBuild += "                  *************************************" + "\r\n";
				strBuild += "\r\n";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 54;
				// Sewer Reminder Notice
				TRY_AGAIN6:
				;
				intErr = 55;
				rsTest.FindFirstRecord("ReportName", "DEFAULTReminder Notice Form");
				// kk08122014 trocls-49  ' "'DEFAULTReminder Notice Form'"
				if (rsTest.NoMatch)
				{
					// none there...keep going
				}
				else
				{
					intErr = 51;
					// delete the current record and replace it with this one
					rsTest.Delete();
					goto TRY_AGAIN6;
				}
				intErr = 56;
				// create the default Reminder Notice
				rsFill.OpenRecordset("SELECT * FROM SavedFreeReports", DEFAULTDATABASE);
				rsFill.AddNew();
				intErr = 57;
				rsFill.Set_Fields("ReportName", "DEFAULTReminder Notice Form");
				rsFill.Set_Fields("Type", "RNFORM");
				strBuild = "\r\n";
				strBuild += "Be aware that these amounts are as of <MAILDATE>.  Please call the <INTDEPT> at <INTDEPTPHONE> for current amount due, before sending a payment." + "\r\n";
				// strBuild = strBuild & vbCrLf & "If you owe additional taxes for the year <LASTYEARMAT> or earlier, you will also be required to pay an additional fee not shown on this statement."
				strBuild += "  For lien information, please contact the <MATDEPT> at <MATDEPTPHONE>." + "\r\n";
				strBuild += "\r\n" + "If you are making payments under a valid signed workout agreement with the Town, please continue to make your regular payments." + "\r\n";
				strBuild += "\r\n" + "Thank You.";
				rsFill.Set_Fields("ReportText", strBuild);
				rsFill.Update();
				intErr = 58;
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Resetting Default Reports - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static bool UpdateUTAccountwithREInfo_6(int lngAcctKey, int lngREAccount)
		{
			return UpdateUTAccountwithREInfo(ref lngAcctKey, ref lngREAccount);
		}

		public static bool UpdateUTAccountwithREInfo_8(int lngAcctKey, int lngREAccount)
		{
			return UpdateUTAccountwithREInfo(ref lngAcctKey, ref lngREAccount);
		}

		public static bool UpdateUTAccountwithREInfo(ref int lngAcctKey, ref int lngREAccount)
		{
			bool UpdateUTAccountwithREInfo = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRE = new clsDRWrapper();
				clsDRWrapper rsUT = new clsDRWrapper();
				UpdateUTAccountwithREInfo = true;
				rsRE.OpenRecordset("SELECT * FROM Master WHERE RSCard = 1 AND RSAccount = " + FCConvert.ToString(lngREAccount), modExtraModules.strREDatabase);
				if (!rsRE.EndOfFile())
				{
					rsUT.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngAcctKey), modExtraModules.strUTDatabase);
					if (!rsUT.EndOfFile())
					{
						rsUT.Edit();
						rsUT.Set_Fields("OwnerPartyID", rsRE.Get_Fields_Int32("OwnerPartyID"));
						rsUT.Set_Fields("SecondOwnerPartyID", rsRE.Get_Fields_Int32("SecOwnerPartyID"));
                        rsUT.Set_Fields("DeedName1", rsRE.Get_Fields_String("DeedName1"));
                        rsUT.Set_Fields("DeedName2", rsRE.Get_Fields_String("DeedName2"));
						rsUT.Set_Fields("StreetNumber", rsRE.Get_Fields_String("RSLOCNUMALPH"));
						// End If
						rsUT.Set_Fields("StreetName", rsRE.Get_Fields_String("RSLOCSTREET"));
						rsUT.Set_Fields("Apt", rsRE.Get_Fields_String("RSLOCAPT"));
						if (FCConvert.ToBoolean(rsUT.Get_Fields_Boolean("SameBillOwner")))
						{
							rsUT.Set_Fields("BillingPartyID", rsUT.Get_Fields_Int32("OwnerPartyID"));
							rsUT.Set_Fields("SecondBillingPartyID", rsUT.Get_Fields_Int32("SecondOwnerPartyID"));
						}
						rsUT.Update();
					}
				}
				return UpdateUTAccountwithREInfo;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating Info - " + FCConvert.ToString(modUTStatusPayments.GetUTAccountNumber(lngAcctKey)), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateUTAccountwithREInfo;
		}

		public static void UpdateAllUTAccountsWithREInfo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rs = new clsDRWrapper();
				int intCounter;
				string[] strProblemAccts = null;
				int intCT;
				string strMessage = "";
				rs.OpenRecordset("SELECT ID, AccountNumber, REAccount FROM Master WHERE ISNULL(UseREAccount,0) = 1 AND ISNULL(Deleted,0) = 0", modExtraModules.strUTDatabase);
				intCounter = 0;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating RE Data", true, rs.RecordCount(), true);
				while (!rs.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					if (!UpdateUTAccountwithREInfo_8(rs.Get_Fields_Int32("ID"), rs.Get_Fields_Int32("REAccount")))
					{
						Array.Resize(ref strProblemAccts, intCounter + 1);
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						strProblemAccts[intCounter] = FCConvert.ToString(rs.Get_Fields("AccountNumber"));
						intCounter += 1;
					}
					rs.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				if (intCounter > 0)
				{
					strMessage = "Cound not update RE location information for account(s) ";
					for (intCT = 0; intCT <= intCounter - 1; intCT++)
					{
						//Application.DoEvents();
						strMessage += "\r\n" + strProblemAccts[intCT];
					}
					strMessage += "\r\n" + "Please verify that the information is correct.";
					MessageBox.Show(strMessage, "Error Updaing RE Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating RE Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static int GetCategory_8(bool boolWater, int lngAcctKey)
		{
			return GetCategory(ref boolWater, ref lngAcctKey);
		}

		public static int GetCategory(ref bool boolWater, ref int lngAcctKey)
		{
			int GetCategory = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will find the category number for the account and return it
				clsDRWrapper rsCat = new clsDRWrapper();
				rsCat.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngAcctKey), modExtraModules.strUTDatabase);
				if (!rsCat.EndOfFile())
				{
					if (boolWater)
					{
						GetCategory = FCConvert.ToInt32(rsCat.Get_Fields_Int32("WaterCategory"));
					}
					else
					{
						GetCategory = FCConvert.ToInt32(rsCat.Get_Fields_Int32("SewerCategory"));
					}
				}
				else
				{
					GetCategory = 0;
				}
				return GetCategory;
			}
			catch
			{
				
				GetCategory = 0;
			}
			return GetCategory;
		}

		private static void FixRoundingProblem()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This routine will check all the values in the DB that could have a rounding problem
				clsDRWrapper rs = new clsDRWrapper();
				string strField;
				strField = "WPrinOwed";
				rs.Execute("UPDATE Bill SET " + strField + " = (((" + strField + " * 100) \\ 1) /100)", modExtraModules.strUTDatabase);
				strField = "SPrinOwed";
				rs.Execute("UPDATE Bill SET " + strField + " = (((" + strField + " * 100) \\ 1) /100)", modExtraModules.strUTDatabase);
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adjusting Decimals", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static bool CheckDefaultFundForAccount_2(bool boolWater, string strAcct, string strDefaultOverride = "")
		{
			return CheckDefaultFundForAccount(ref boolWater, ref strAcct, strDefaultOverride);
		}

		public static bool CheckDefaultFundForAccount(ref bool boolWater, ref string strAcct, string strDefaultOverride = "")
		{
			bool CheckDefaultFundForAccount = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAcct = new clsDRWrapper();
				int lngFund;
				int lngDefault;
				string strDef = "";
				if (strDefaultOverride == "")
				{
					rsAcct.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
					if (!rsAcct.EndOfFile())
					{
						if (boolWater)
						{
							strDef = FCConvert.ToString(rsAcct.Get_Fields_String("WaterPrincipalAccount"));
						}
						else
						{
							strDef = FCConvert.ToString(rsAcct.Get_Fields_String("SewerPrincipalAccount"));
						}
					}
				}
				else
				{
					strDef = strDefaultOverride;
				}
				lngDefault = modBudgetaryAccounting.GetFundFromAccount(strDef);
				lngFund = modBudgetaryAccounting.GetFundFromAccount(strAcct);
				CheckDefaultFundForAccount = FCConvert.CBool(lngFund == lngDefault);
				return CheckDefaultFundForAccount;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Account Fund", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckDefaultFundForAccount;
		}

		private static void UpdateBillDates()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will update all of the bill dates from the rate record that it is associated with
				clsDRWrapper rsRK = new clsDRWrapper();
				clsDRWrapper rsB = new clsDRWrapper();
				rsRK.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Bill Dates", true, rsRK.RecordCount(), true);
				while (!rsRK.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					if (Information.IsDate(rsRK.Get_Fields("BillDate")))
					{
						//Application.DoEvents();
						rsB.Execute("UPDATE Bill SET BillDate = '" + rsRK.Get_Fields_DateTime("BillDate") + "' WHERE BillingRateKey = " + rsRK.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
					}
					rsRK.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Account Fund", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Public Sub CheckCompactDate()
		// Dim rsCompactDates As New clsDRWrapper
		//
		// If Not rsCompactDates.UpdateDatabaseTable("CompactDates", "twgn0000.vb1") Then
		// do nothing
		// Else
		// rsCompactDates.OpenRecordset "SELECT * FROM CompactDates", "twgn0000.vb1"
		// If rsCompactDates.EndOfFile <> True And rsCompactDates.BeginningOfFile <> True Then
		// If IsDate(rsCompactDates.Fields(Mid(App.EXEName, 3, 2) & "Date")) Then
		// If Abs(DateDiff("d", Date, rsCompactDates.Fields(Mid(App.EXEName, 3, 2) & "Date"))) > 7 Then
		// MsgBox "You have not compacted your database in over a week.  Please try to compact it when you get a chance.", vbExclamation, "Compact Database"
		// End If
		// Else
		// rsCompactDates.Edit
		// rsCompactDates.Fields(Mid(App.EXEName, 3, 2) & "Date") = Date
		// rsCompactDates.Update False
		// End If
		// Else
		// rsCompactDates.AddNew
		// rsCompactDates.Fields(Mid(App.EXEName, 3, 2) & "Date") = Date
		// rsCompactDates.Update False
		// End If
		// End If
		// End Sub
		public static void FixMasterBookNumbers()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsB = new clsDRWrapper();
				rsB.Execute("UPDATE Master SET UTBook = MeterTable.BookNumber FROM MeterTable INNER JOIN Master ON MeterTable.Accountkey = Master.ID WHERE (UTBook <> BookNumber OR ISNULL(UTBook, 0) = 0) AND MeterNumber = 1", modExtraModules.strUTDatabase);
				// rsB.OpenRecordset "SELECT * FROM MeterTable INNER JOIN Master ON MeterTable.Accountkey = Master.ID WHERE (UTBook <> BookNumber OR ISNULL(UTBook, 0) = 0) AND MeterNumber = 1", strUTDatabase
				// Do Until rsB.EndOfFile
				// rsB.Edit
				// rsB.Fields("UTBook") = rsB.Fields("BookNumber")
				// rsB.Update
				// 
				// rsB.MoveNext
				// Loop
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Master Book", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void ConvertOldConsumptions()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsLoad = new clsDRWrapper();
				clsDRWrapper rsCheck = new clsDRWrapper();
				clsDRWrapper rsM = new clsDRWrapper();
				int lngMeterKey = 0;
				int lngPrev = 0;
				int lngCurr = 0;
				int lngDigits = 0;
				bool boolNegMeter = false;
				int lngAccountKey = 0;
				int lngAdj = 0;
				int lngCT = 0;
				rsLoad.OpenRecordset("SELECT * FROM Bill", modExtraModules.strUTDatabase);
				rsSave.OpenRecordset("SELECT * FROM MeterConsumption WHERE ID = 0", modExtraModules.strUTDatabase);
				rsCheck.OpenRecordset("SELECT * FROM MeterConsumption", modExtraModules.strUTDatabase);
				if (rsCheck.RecordCount() < 10)
				{
					while (!rsLoad.EndOfFile())
					{
						//Application.DoEvents();
						if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("CurReading")) != 0)
						{
							rsCheck.FindFirstRecord2("MeterKey,BillDate", rsLoad.Get_Fields_Int32("MeterKey") + "," + rsLoad.Get_Fields_DateTime("CurDate"), ",");
							if (rsCheck.NoMatch)
							{
								lngCT += 1;
								lngAccountKey = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("AccountKey"));
								lngMeterKey = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("MeterKey"));
								lngPrev = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("PrevReading"));
								lngCurr = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("CurReading"));
								boolNegMeter = false;
								// rsLoad.Fields("NegativeConsumption")
								if (lngCT >= 100)
								{
									//Application.DoEvents();
									frmWait.InstancePtr.IncrementProgress();
									lngCT = 0;
								}
								rsSave.AddNew();
								rsM.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(lngMeterKey), modExtraModules.strUTDatabase);
								if (!rsM.EndOfFile())
								{
									lngDigits = FCConvert.ToInt32(rsM.Get_Fields_Int32("Digits"));
									rsSave.Set_Fields("MeterNumber", rsM.Get_Fields_Int32("MeterNumber"));
								}
								else
								{
									lngDigits = 1;
									rsSave.Set_Fields("MeterNumber", 1);
								}
								rsSave.Set_Fields("AccountKey", lngAccountKey);
								rsSave.Set_Fields("MeterKey", lngMeterKey);
								rsSave.Set_Fields("Begin", lngPrev);
								rsSave.Set_Fields("End", lngCurr);
								if (lngCurr >= lngPrev)
								{
									rsSave.Set_Fields("Consumption", lngCurr - lngPrev);
								}
								else
								{
									switch (modExtraModules.Statics.glngTownReadingUnits)
									{
										case 100000:
											{
												lngAdj = 5;
												break;
											}
										case 10000:
											{
												lngAdj = 4;
												break;
											}
										case 1000:
											{
												lngAdj = 3;
												break;
											}
										case 100:
											{
												lngAdj = 2;
												break;
											}
										case 10:
											{
												lngAdj = 1;
												break;
											}
										case 1:
											{
												lngAdj = 0;
												break;
											}
										default:
											{
												lngAdj = 0;
												break;
											}
									}
									//end switch
									// this must be a rolled over meter
									rsSave.Set_Fields("Consumption", lngCurr + ((Math.Pow(10, (lngDigits - lngAdj))) - lngPrev));
								}
								rsSave.Set_Fields("NegativeMeter", boolNegMeter);
								rsSave.Set_Fields("BillDate", rsLoad.Get_Fields_DateTime("CurDate"));
								rsSave.Update();
							}
						}
						rsLoad.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Meter Consumption Table", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void SetPLIPaidFieldAndPayments()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will search through each lien record and find records that have
				// paid more interest than thier charged interest field, then it will take the
				// excess paid interest out of the InterestPaid field and put it in the PLIPaid fiel
				// in order to track PLI better from the LienRec itself
				clsDRWrapper rsLN = new clsDRWrapper();
				clsDRWrapper rsPay = new clsDRWrapper();
				double dblPLIPaid = 0;
				double dblIntPaid = 0;
				double dblIntCharged = 0;
				double dblTotPaymentInt = 0;
				rsLN.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
				// WHERE InterestPaid > (InterestCharged * -1)", strCLDatabase
				while (!rsLN.EndOfFile())
				{
					//Application.DoEvents();
					// If rsLN.Fields("LienKey") = 55 Then
					// MsgBox "Stop"
					// End If
					// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
					dblIntCharged = rsLN.Get_Fields("IntAdded") * -1;
					// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
					dblIntPaid = rsLN.Get_Fields("IntPaid") + rsLN.Get_Fields("PLIPaid");
					// If rsLN.Fields("LienRecordNumber") = 48 Then Stop
					if (dblIntPaid > dblIntCharged)
					{
						dblPLIPaid = (dblIntPaid - dblIntCharged);
						dblIntPaid -= (dblIntPaid - dblIntCharged);
						if (dblPLIPaid >= 0)
						{
							rsLN.Edit();
							rsLN.Set_Fields("IntPaid", dblIntPaid);
							rsLN.Set_Fields("PLIPaid", dblPLIPaid);
							rsLN.Update();
							// set the payment amounts
							rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE Code <> 'I' AND BillKey = " + rsLN.Get_Fields_Int32("ID") + " AND ISNULL(Lien,0) = 1 AND Service = 'S'", modExtraModules.strUTDatabase);
							while (!rsPay.EndOfFile())
							{
								//Application.DoEvents();
								dblTotPaymentInt = FCConvert.ToDouble(rsPay.Get_Fields_Decimal("PreLienInterest") + rsPay.Get_Fields_Decimal("CurrentInterest"));
								if (dblTotPaymentInt != 0)
								{
									rsPay.Edit();
									if (dblPLIPaid > 0)
									{
										if (dblPLIPaid > dblTotPaymentInt)
										{
											rsPay.Set_Fields("PreLienInterest", dblTotPaymentInt);
											rsPay.Set_Fields("CurrentInterest", 0);
											dblPLIPaid -= dblTotPaymentInt;
										}
										else
										{
											rsPay.Set_Fields("PreLienInterest", dblPLIPaid);
											rsPay.Set_Fields("CurrentInterest", dblTotPaymentInt - dblPLIPaid);
											dblPLIPaid = 0;
											dblIntPaid -= (dblTotPaymentInt - dblPLIPaid);
										}
									}
									else
									{
										rsPay.Set_Fields("PreLienInterest", 0);
										rsPay.Set_Fields("CurrentInterest", dblTotPaymentInt);
									}
									rsPay.Update();
								}
								rsPay.MoveNext();
							}
						}
					}
					else
					{
						rsLN.Edit();
						rsLN.Set_Fields("PLIPaid", 0);
						rsLN.Update();
						// set the payment amounts
						rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE Code <> 'I' AND BillKey = " + rsLN.Get_Fields_Int32("ID") + " AND ISNULL(Lien,0) = 1 AND Service = 'S'", modExtraModules.strUTDatabase);
						while (!rsPay.EndOfFile())
						{
							//Application.DoEvents();
							rsPay.Edit();
							if (rsPay.Get_Fields_Decimal("PreLienInterest") != 0 || rsPay.Get_Fields_Decimal("CurrentInterest") != 0)
							{
								dblTotPaymentInt = FCConvert.ToDouble(rsPay.Get_Fields_Decimal("PreLienInterest") + rsPay.Get_Fields_Decimal("CurrentInterest"));
								rsPay.Set_Fields("PreLienInterest", 0);
								rsPay.Set_Fields("CurrentInterest", dblTotPaymentInt);
								rsPay.Update();
							}
							rsPay.MoveNext();
						}
					}
					rsLN.MoveNext();
				}
				rsLN.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(Lien,0) = 0 AND PreLienInterest <> 0", modExtraModules.strUTDatabase);
				while (!rsLN.EndOfFile())
				{
					//Application.DoEvents();
					rsLN.Edit();
					rsLN.Set_Fields("CurrentInterest", rsLN.Get_Fields_Decimal("CurrentInterest") + rsLN.Get_Fields_Decimal("PreLienInterest"));
					rsLN.Set_Fields("PreLienInterest", 0);
					rsLN.Update();
					rsLN.MoveNext();
				}
				rsLN.OpenRecordset("SELECT * FROM PaymentRec WHERE (Code = 'L' OR Code = 'X' OR Code = 'S') AND PreLienInterest <> 0 AND PreLienInterest = CurrentInterest *-1", modExtraModules.strUTDatabase);
				while (!rsLN.EndOfFile())
				{
					//Application.DoEvents();
					rsLN.Edit();
					rsLN.Set_Fields("CurrentInterest", 0);
					rsLN.Set_Fields("PreLienInterest", 0);
					rsLN.Update();
					rsLN.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting PLI Payment Amounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// Public Sub FillBillSequenceField()
		// On Error GoTo ERROR_HANDLER
		// this routine will fill the sequence field of each bill with the
		// main meters sequence for sorting purposes on reminder notices
		// Dim rsBS                As New clsDRWrapper
		// Dim rsBill              As New clsDRWrapper
		//
		// rsBill.OpenRecordset "SELECT * FROM Bill WHERE val(Seq & ' ') = 0 OR Seq = NULL", strUTDatabase
		// Do Until rsBill.EndOfFile
		// If rsBill.Fields("MeterKey") > 0 Then
		// rsBS.OpenRecordset "SELECT * FROM MeterTable WHERE MeterKey = " & rsBill.Fields("MeterKey"), strUTDatabase
		// If Not rsBS.EndOfFile Then
		// rsBill.Edit
		// rsBill.Fields("Seq") = rsBS.Fields("Sequence")
		// rsBill.Update
		// End If
		// End If
		// rsBill.MoveNext
		// Loop
		//
		// Exit Sub
		
		// Unload frmWait
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Fill Bill Sequence Field"
		// End Sub
		private static void SetMeterRadioField()
		{
			clsDRWrapper rsM = new clsDRWrapper();
			rsM.OpenRecordset("SELECT * FROM MeterTable WHERE ISNULL(Radio,0) = 1", modExtraModules.strUTDatabase);
			while (!rsM.EndOfFile())
			{
				rsM.Edit();
				rsM.Set_Fields("RadioAccessType", 1);
				rsM.Update();
				rsM.MoveNext();
			}
		}

		public static void BackupUTDatabase(bool boolShowMSG = true, string strBK = "TWUTArchive")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsExecute = new clsDRWrapper();
				int intCounter;
				int intMax = 0;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// CHECK TO SEE IF THE ARCHIVE FOLDER DOES ACTUALLY EXIST
				if (Directory.Exists("Backup"))
				{
				}
				else
				{
					Directory.CreateDirectory("Backup");
				}
				if (File.Exists("TWUT0000.vb1"))
				{
					TRYAGAIN:
					;
					intMax = 4;
					for (intCounter = 1; intCounter <= intMax; intCounter++)
					{
						//Application.DoEvents();
						if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\Backup\\" + strBK + FCConvert.ToString(intCounter) + ".vb1"))
						{
						}
						else
						{
							// UT DATABASE DOES EXIST SO TRY AND COPY IT OVER TO THE ARCHIVE FOLDER
							modGlobalFunctions.AddCYAEntry_26("UT", "Backup UT Database", "Copy Database to Archive");
							File.Copy("TWUT0000.vb1", FCFileSystem.Statics.UserDataFolder + "\\Backup\\" + strBK + FCConvert.ToString(intCounter) + ".vb1", true);
							if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\Backup\\" + strBK + FCConvert.ToString(intCounter) + ".vb1"))
							{
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
								if (boolShowMSG)
								{
									MessageBox.Show("Backup of UT Database completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
							}
							if (intCounter == intMax)
							{
								File.Delete(FCFileSystem.Statics.UserDataFolder + "\\Backup\\" + strBK + "1.vb1");
							}
							else
							{
								if (File.Exists(FCFileSystem.Statics.UserDataFolder + "\\Backup\\" + strBK + FCConvert.ToString(intCounter + 1) + ".vb1"))
								{
									File.Delete(FCFileSystem.Statics.UserDataFolder + "\\Backup\\" + strBK + FCConvert.ToString(intCounter + 1) + ".vb1");
								}
							}
							return;
						}
					}
					if (intCounter > intMax)
					{
						File.Delete(FCFileSystem.Statics.UserDataFolder + "\\Backup\\" + strBK + "1.vb1");
						goto TRYAGAIN;
					}
				}
				else
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
					if (boolShowMSG)
					{
						MessageBox.Show("Utility Database could not be found. Backup of information could not be completed.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Backing Up UT Database", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void FixUseREMortgageHolders()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.Execute("UPDATE Master SET UseMortgageHolder = true WHERE UseREAccount = true", "TWUT0000.vb1");
		}

		public static void CorrectConsumptionHistory()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.Execute("UPDATE MeterConsumption SET Consumption = 0 WHERE Begin = End", "TWUT0000.vb1");
		}

		public static void SetRateKeyDates_6(string strService, string strType, string strRateKeys = "")
		{
			SetRateKeyDates(ref strService, ref strType, strRateKeys);
		}

		public static void SetRateKeyDates_8(string strService, string strType, string strRateKeys = "")
		{
			SetRateKeyDates(ref strService, ref strType, strRateKeys);
		}

		public static void SetRateKeyDates(ref string strService, ref string strType, string strRateKeys = "")
		{
			clsDRWrapper rsValidate = new clsDRWrapper();
			clsDRWrapper rsRateKeys = new clsDRWrapper();
			string strRateKeyList = "";
			if (strType == "3")
			{
				rsValidate.OpenRecordset("SELECT * FROM " + strService + "Control_30DayNotice");
				if (rsValidate.EndOfFile() != true && rsValidate.BeginningOfFile() != true)
				{
					strRateKeyList = Strings.Trim(FCConvert.ToString(rsValidate.Get_Fields_String("RateKeyList")));
					if (Strings.Right(strRateKeyList, 1) == ",")
					{
						strRateKeyList = Strings.Left(strRateKeyList, strRateKeyList.Length - 1);
					}
					rsRateKeys.OpenRecordset("SELECT * FROM RateKeys WHERE ID IN (" + strRateKeyList + ")", "TWUT0000.vb1");
					if (rsRateKeys.EndOfFile() != true && rsRateKeys.BeginningOfFile() != true)
					{
						do
						{
							rsRateKeys.Edit();
							rsRateKeys.Set_Fields("30DNDate" + strService, rsValidate.Get_Fields_DateTime("MailDate"));
							rsRateKeys.Update();
							rsRateKeys.MoveNext();
						}
						while (rsRateKeys.EndOfFile() != true);
					}
				}
			}
			else if (strType == "L")
			{
				rsValidate.OpenRecordset("SELECT * FROM " + strService + "Control_LienProcess");
				if (rsValidate.EndOfFile() != true && rsValidate.BeginningOfFile() != true)
				{
					strRateKeyList = strRateKeys;
					if (Strings.Right(strRateKeyList, 1) == ",")
					{
						strRateKeyList = Strings.Left(strRateKeyList, strRateKeyList.Length - 1);
					}
					rsRateKeys.OpenRecordset("SELECT * FROM RateKeys WHERE ID IN (" + strRateKeyList + ")", "TWUT0000.vb1");
					if (rsRateKeys.EndOfFile() != true && rsRateKeys.BeginningOfFile() != true)
					{
						do
						{
							rsRateKeys.Edit();
							rsRateKeys.Set_Fields("LienDate" + strService, rsValidate.Get_Fields_DateTime("FilingDate"));
							rsRateKeys.Update();
							rsRateKeys.MoveNext();
						}
						while (rsRateKeys.EndOfFile() != true);
					}
				}
			}
			else if (strType == "M")
			{
				rsValidate.OpenRecordset("SELECT * FROM " + strService + "Control_LienMaturity");
				if (rsValidate.EndOfFile() != true && rsValidate.BeginningOfFile() != true)
				{
					strRateKeyList = strRateKeys;
					if (Strings.Right(strRateKeyList, 1) == ",")
					{
						strRateKeyList = Strings.Left(strRateKeyList, strRateKeyList.Length - 1);
					}
					rsRateKeys.OpenRecordset("SELECT * FROM RateKeys WHERE ID IN (" + strRateKeyList + ")", "TWUT0000.vb1");
					if (rsRateKeys.EndOfFile() != true && rsRateKeys.BeginningOfFile() != true)
					{
						do
						{
							rsRateKeys.Edit();
							rsRateKeys.Set_Fields("MaturityDate" + strService, rsValidate.Get_Fields_DateTime("MailDate"));
							rsRateKeys.Update();
							rsRateKeys.MoveNext();
						}
						while (rsRateKeys.EndOfFile() != true);
					}
				}
			}
		}

		public static void GeneralFixRoutine()
		{
			clsDRWrapper rsLien = new clsDRWrapper();
			clsDRWrapper rsPayment = new clsDRWrapper();
			Decimal curPreLienIntTotal;
			// vbPorter upgrade warning: curCurrentIntTotal As Decimal	OnWrite(short, double)
			Decimal curCurrentIntTotal;
			// vbPorter upgrade warning: curSCurrentIntTotal As Decimal	OnWriteFCConvert.ToDouble(
			Decimal curSCurrentIntTotal;
			// vbPorter upgrade warning: curPreLienIntRemaining As Decimal	OnWrite(Decimal, short)
			Decimal curPreLienIntRemaining;
			Decimal curCurrentIntRemaining;
			Decimal curSCurrentIntRemaining;
			// vbPorter upgrade warning: curPreLienIntPaidPaymentTotal As Decimal	OnWrite(short, Decimal)
			Decimal curPreLienIntPaidPaymentTotal;
			// vbPorter upgrade warning: curCurrentIntPaidPaymentTotal As Decimal	OnWrite(short, Decimal)
			Decimal curCurrentIntPaidPaymentTotal;
			// vbPorter upgrade warning: curSCurrentIntPaidPaymentTotal As Decimal	OnWrite(short, Decimal)
			Decimal curSCurrentIntPaidPaymentTotal;
			Decimal curPreLienIntPaidLienTotal;
			Decimal curCurrentIntPaidLienTotal;
			Decimal curSCurrentIntPaidLienTotal;
			// vbPorter upgrade warning: curTotalIntPayment As Decimal	OnWrite(double, Decimal)
			Decimal curTotalIntPayment;
			// vbPorter upgrade warning: curCorrectPreLienIntPaidTotal As Decimal	OnWrite(short, Decimal)
			Decimal curCorrectPreLienIntPaidTotal;
			// vbPorter upgrade warning: curCorrectCurrentIntPaidTotal As Decimal	OnWrite(short, Decimal)
			Decimal curCorrectCurrentIntPaidTotal;
			Decimal curSCorrectCurrentIntPaidTotal;
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			// vbPorter upgrade warning: curSCurrentIntPaymentTotal As Decimal	OnWrite(short, Decimal)
			Decimal curSCurrentIntPaymentTotal;
			// vbPorter upgrade warning: curCurrentIntPaymentTotal As Decimal	OnWrite(short, Decimal)
			Decimal curCurrentIntPaymentTotal;
			clsDRWrapper rsCheck = new clsDRWrapper();
			rsCheck.OpenRecordset("SELECT * FROM UtilityBilling", "TWUT0000.vb1");
			if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				return;
			}
			if (rsCheck.Get_Fields_Boolean("RanGeneralFix") != true)
			{
				FCFileSystem.FileOpen(1, "LienFix.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.WriteLine(1, "Fix perfomed on " + FCConvert.ToString(DateTime.Now));
				frmWait.InstancePtr.lblMessage.Text = "Checking Liens" + "\r\n" + "Please Wait...";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				rsLien.OpenRecordset("SELECT * FROM Lien", "TWUT0000.vb1");
				if (rsLien.EndOfFile() != true && rsLien.BeginningOfFile() != true)
				{
					frmWait.InstancePtr.prgProgress.Maximum = rsLien.RecordCount();
					do
					{
						//Application.DoEvents();
						// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
						curPreLienIntTotal = FCConvert.ToDecimal(rsLien.Get_Fields("Interest"));
						// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						curCurrentIntTotal = rsLien.Get_Fields("IntAdded") * -1;
						// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
						curPreLienIntPaidLienTotal = FCConvert.ToDecimal(rsLien.Get_Fields("PLIPaid"));
						// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
						curCurrentIntPaidLienTotal = FCConvert.ToDecimal(rsLien.Get_Fields("IntPaid"));
						curPreLienIntPaidPaymentTotal = 0;
						curCurrentIntPaidPaymentTotal = 0;
						rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsLien.Get_Fields_Int32("ID") + " AND ISNULL(Lien,0) = 1 AND Code <> 'I' ORDER BY ActualSystemDate", "TWUT0000.vb1");
						if (rsPayment.EndOfFile() != true && rsPayment.BeginningOfFile() != true)
						{
							rsAccountInfo.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsPayment.Get_Fields_Int32("AccountKey"), "TWUT0000.vb1");
							do
							{
								curPreLienIntPaidPaymentTotal += rsPayment.Get_Fields_Decimal("PreLienInterest");
								curCurrentIntPaidPaymentTotal += rsPayment.Get_Fields_Decimal("CurrentInterest");
								rsPayment.MoveNext();
							}
							while (rsPayment.EndOfFile() != true);
							if (curPreLienIntTotal < curPreLienIntPaidLienTotal || curCurrentIntTotal < curCurrentIntPaidLienTotal || curPreLienIntPaidLienTotal != curPreLienIntPaidPaymentTotal || curCurrentIntPaidLienTotal != curCurrentIntPaidPaymentTotal)
							{
								curPreLienIntRemaining = curPreLienIntTotal;
								curCurrentIntRemaining = curCurrentIntTotal;
								curCorrectPreLienIntPaidTotal = 0;
								curCorrectCurrentIntPaidTotal = 0;
								rsPayment.MoveFirst();
								do
								{
									//Application.DoEvents();
									curTotalIntPayment = FCConvert.ToDecimal(rsPayment.Get_Fields_Decimal("PreLienInterest") + rsPayment.Get_Fields_Decimal("CurrentInterest"));
									if (curTotalIntPayment > 0)
									{
										if (curTotalIntPayment < curPreLienIntRemaining)
										{
											rsPayment.Edit();
											if (rsPayment.Get_Fields_Decimal("PreLienInterest") != curTotalIntPayment || rsPayment.Get_Fields_Decimal("CurrentInterest") != 0)
											{
												// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
												FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("RateKey") + "   Old PLI Paid: " + rsPayment.Get_Fields_Decimal("PreLienInterest") + "   New PLI Paid: " + FCConvert.ToString(curTotalIntPayment) + "   Old Cur Int Paid: " + rsPayment.Get_Fields_Decimal("CurrentInterest") + "   New Cur Int Paid: 0.00");
											}
											rsPayment.Set_Fields("PreLienInterest", curTotalIntPayment);
											rsPayment.Set_Fields("CurrentInterest", 0);
											rsPayment.Update();
											curPreLienIntRemaining -= curTotalIntPayment;
											curCorrectPreLienIntPaidTotal += curTotalIntPayment;
										}
										else
										{
											if (curPreLienIntRemaining > 0)
											{
												rsPayment.Edit();
												if (rsPayment.Get_Fields_Decimal("PreLienInterest") != curPreLienIntRemaining || rsPayment.Get_Fields_Decimal("CurrentInterest") != (curTotalIntPayment - curPreLienIntRemaining))
												{
													// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
													FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("RateKey") + "   Old PLI Paid: " + rsPayment.Get_Fields_Decimal("PreLienInterest") + "   New PLI Paid: " + FCConvert.ToString(curPreLienIntRemaining) + "   Old Cur Int Paid: " + rsPayment.Get_Fields_Decimal("CurrentInterest") + "   New Cur Int Paid: " + FCConvert.ToString(curTotalIntPayment - curPreLienIntRemaining));
												}
												rsPayment.Set_Fields("PreLienInterest", curPreLienIntRemaining);
												curTotalIntPayment -= curPreLienIntRemaining;
												curCorrectPreLienIntPaidTotal += curPreLienIntRemaining;
												curPreLienIntRemaining = 0;
												rsPayment.Set_Fields("CurrentInterest", curTotalIntPayment);
												rsPayment.Update();
												curCurrentIntRemaining -= curTotalIntPayment;
												curCorrectCurrentIntPaidTotal += curTotalIntPayment;
											}
											else
											{
												rsPayment.Edit();
												if (rsPayment.Get_Fields_Decimal("PreLienInterest") != 0 || rsPayment.Get_Fields_Decimal("CurrentInterest") != curTotalIntPayment)
												{
													// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
													FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("RateKey") + "   Old PLI Paid: " + rsPayment.Get_Fields_Decimal("PreLienInterest") + "   New PLI Paid: " + "0.00" + "   Old Cur Int Paid: " + rsPayment.Get_Fields_Decimal("CurrentInterest") + "   New Cur Int Paid: " + FCConvert.ToString(curTotalIntPayment));
												}
												rsPayment.Set_Fields("PreLienInterest", 0);
												rsPayment.Set_Fields("CurrentInterest", curTotalIntPayment);
												rsPayment.Update();
												curCurrentIntRemaining -= curTotalIntPayment;
												curCorrectCurrentIntPaidTotal += curTotalIntPayment;
											}
										}
									}
									else if (curTotalIntPayment == 0)
									{
										if (rsPayment.Get_Fields_Decimal("PreLienInterest") != 0 && rsPayment.Get_Fields_Decimal("CurrentInterest") != 0)
										{
											rsPayment.Edit();
											// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
											FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("RateKey") + "   Old PLI Paid: " + rsPayment.Get_Fields_Decimal("PreLienInterest") + "   New PLI Paid: " + "0.00" + "   Old Cur Int Paid: " + rsPayment.Get_Fields_Decimal("CurrentInterest") + "   New Cur Int Paid: " + "0.00");
											rsPayment.Set_Fields("PreLienInterest", 0);
											rsPayment.Set_Fields("CurrentInterest", 0);
											rsPayment.Update();
										}
									}
									else
									{
										rsPayment.Edit();
										if (rsPayment.Get_Fields_Decimal("PreLienInterest") != 0 || rsPayment.Get_Fields_Decimal("CurrentInterest") != curTotalIntPayment)
										{
											// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
											FCFileSystem.WriteLine(1, "Adjusted Payment for Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("RateKey") + "   Old PLI Paid: " + rsPayment.Get_Fields_Decimal("PreLienInterest") + "   New PLI Paid: " + "0.00" + "   Old Cur Int Paid: " + rsPayment.Get_Fields_Decimal("CurrentInterest") + "   New Cur Int Paid: " + FCConvert.ToString(curTotalIntPayment));
										}
										rsPayment.Set_Fields("PreLienInterest", 0);
										rsPayment.Set_Fields("CurrentInterest", curTotalIntPayment);
										curCorrectCurrentIntPaidTotal += curTotalIntPayment;
										rsPayment.Update();
									}
									rsPayment.MoveNext();
								}
								while (rsPayment.EndOfFile() != true);
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
								FCFileSystem.WriteLine(1, "Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("RateKey") + "   Old PLI Paid: " + rsLien.Get_Fields("PLIPaid") + "   New PLI Paid: " + FCConvert.ToString(curCorrectPreLienIntPaidTotal) + "   Old Cur Int Paid: " + rsLien.Get_Fields("IntPaid") + "   New Cur Int Paid: " + FCConvert.ToString(curCorrectCurrentIntPaidTotal));
								rsLien.Edit();
								rsLien.Set_Fields("PLIPaid", curCorrectPreLienIntPaidTotal);
								rsLien.Set_Fields("IntPaid", curCorrectCurrentIntPaidTotal);
								rsLien.Update();
							}
						}
						rsLien.MoveNext();
						frmWait.InstancePtr.IncrementProgress();
					}
					while (rsLien.EndOfFile() != true);
				}
				FCFileSystem.FileClose(1);
				// Bill Portion
				FCFileSystem.FileOpen(1, "BillFix.txt", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.WriteLine(1, "Fix perfomed on " + FCConvert.ToString(DateTime.Now));
				frmWait.InstancePtr.lblMessage.Text = "Checking Bills" + "\r\n" + "Please Wait...";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				rsLien.OpenRecordset("SELECT * FROM Bill", "TWUT0000.vb1");
				if (rsLien.EndOfFile() != true && rsLien.BeginningOfFile() != true)
				{
					frmWait.InstancePtr.prgProgress.Maximum = rsLien.RecordCount();
					do
					{
						//Application.DoEvents();
						curSCurrentIntTotal = FCConvert.ToDecimal(rsLien.Get_Fields_Double("SIntOwed") - rsLien.Get_Fields_Double("SIntAdded"));
						curCurrentIntTotal = FCConvert.ToDecimal(rsLien.Get_Fields_Double("WIntOwed") - rsLien.Get_Fields_Double("WIntAdded"));
						curSCurrentIntPaidLienTotal = FCConvert.ToDecimal(rsLien.Get_Fields_Double("SIntPaid"));
						curCurrentIntPaidLienTotal = FCConvert.ToDecimal(rsLien.Get_Fields_Double("WIntPaid"));
						curSCurrentIntPaidPaymentTotal = 0;
						curCurrentIntPaidPaymentTotal = 0;
						curSCurrentIntPaymentTotal = 0;
						curCurrentIntPaymentTotal = 0;
						rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsLien.Get_Fields_Int32("ID") + " AND ISNULL(Lien,0) = 0 AND ReceiptNumber >= 0 ORDER BY ActualSystemDate", "TWUT0000.vb1");
						if (rsPayment.EndOfFile() != true && rsPayment.BeginningOfFile() != true)
						{
							rsAccountInfo.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsPayment.Get_Fields_Int32("AccountKey"), "TWUT0000.vb1");
							do
							{
								//Application.DoEvents();
								if (rsPayment.Get_Fields_Decimal("PreLienInterest") != 0)
								{
									// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
									FCFileSystem.WriteLine(1, "Pre Lien Interest Moved to Current Interest on Payment for Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("BillingRateKey") + "   PLI Paid: " + rsPayment.Get_Fields_Decimal("PreLienInterest"));
									rsPayment.Edit();
									rsPayment.Set_Fields("CurrentInterest", rsPayment.Get_Fields_Decimal("CurrentInterest") + rsPayment.Get_Fields_Decimal("PreLienInterest"));
									rsPayment.Set_Fields("PreLienInterest", 0);
									rsPayment.Update();
								}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								if (FCConvert.ToString(rsPayment.Get_Fields("Code")) == "I")
								{
									if (FCConvert.ToString(rsPayment.Get_Fields_String("service")) == "S")
									{
										curSCurrentIntPaymentTotal += rsPayment.Get_Fields_Decimal("CurrentInterest");
									}
									else
									{
										curCurrentIntPaymentTotal += rsPayment.Get_Fields_Decimal("CurrentInterest");
									}
								}
								else
								{
									if (FCConvert.ToString(rsPayment.Get_Fields_String("service")) == "S")
									{
										curSCurrentIntPaidPaymentTotal += rsPayment.Get_Fields_Decimal("CurrentInterest");
									}
									else
									{
										curCurrentIntPaidPaymentTotal += rsPayment.Get_Fields_Decimal("CurrentInterest");
									}
								}
								rsPayment.MoveNext();
							}
							while (rsPayment.EndOfFile() != true);
							if (curSCurrentIntPaidLienTotal != curSCurrentIntPaidPaymentTotal || curCurrentIntPaidLienTotal != curCurrentIntPaidPaymentTotal)
							{
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								FCFileSystem.WriteLine(1, "Adjusted Int Paid for Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("BillingRateKey") + "   Old Sewer Int Paid: " + rsLien.Get_Fields_Double("SIntPaid") + "   New Sewer Int Paid: " + FCConvert.ToString(curSCurrentIntPaidPaymentTotal) + "   Old Water Int Paid: " + rsLien.Get_Fields_Double("WIntPaid") + "   New Cur Int Paid: " + FCConvert.ToString(curCurrentIntPaidPaymentTotal));
								rsLien.Edit();
								rsLien.Set_Fields("SIntPaid", curSCurrentIntPaidPaymentTotal);
								rsLien.Set_Fields("WIntPaid", curCurrentIntPaidPaymentTotal);
								rsLien.Update();
							}
							else
							{
							}
							if (FCConvert.ToDecimal(rsLien.Get_Fields_Double("SIntAdded")) != curSCurrentIntPaymentTotal || FCConvert.ToDecimal(rsLien.Get_Fields_Double("WIntAdded")) != curCurrentIntPaymentTotal)
							{
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								FCFileSystem.WriteLine(1, "Adjusted Int Owed for Account: " + rsAccountInfo.Get_Fields("AccountNumber") + "   Bill: " + rsLien.Get_Fields_Int32("BillingRateKey") + "   Old Sewer Int Owed: " + rsLien.Get_Fields_Double("SIntAdded") + "   New Sewer Int Owed: " + FCConvert.ToString(curSCurrentIntPaymentTotal) + "   Old Water Int Owed: " + rsLien.Get_Fields_Double("WIntAdded") + "   New Water Int Owed: " + FCConvert.ToString(curCurrentIntPaymentTotal));
								rsLien.Edit();
								rsLien.Set_Fields("SIntAdded", curSCurrentIntPaymentTotal);
								rsLien.Set_Fields("WIntAdded", curCurrentIntPaymentTotal);
								rsLien.Update();
							}
							else
							{
							}
						}
						frmWait.InstancePtr.IncrementProgress();
						rsLien.MoveNext();
					}
					while (rsLien.EndOfFile() != true);
				}
				FCFileSystem.FileClose(1);
				rsCheck.Edit();
				rsCheck.Set_Fields("RanGeneralFix", true);
				rsCheck.Update();
				frmWait.InstancePtr.lblMessage.Text = "Checking Database" + "\r\n" + "Please Wait...";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.prgProgress.Maximum = 10;
				frmWait.InstancePtr.prgProgress.Value = 2;
				frmWait.InstancePtr.Refresh();
			}
		}

		public static void CheckBillDates()
		{
			// MAL@20070917: Check that all bill and lien records have a Bill Date
			// Call Reference: 117062 & 114949
			clsDRWrapper rsBill = new clsDRWrapper();
			int lngRateKey = 0;
			string strRateDate = "";
			frmWait.InstancePtr.Init("Please Wait " + "\r\n" + "Record Update in Progress", true, 5000);
			// Bill Table
			rsBill.OpenRecordset("SELECT * FROM Bill WHERE BillDate IS NULL AND (BillNumber <> 0 AND BillingRateKey <> 0)", modExtraModules.strUTDatabase);
			if (rsBill.RecordCount() > 0)
			{
				while (!rsBill.EndOfFile())
				{
					//Application.DoEvents();
					lngRateKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("BillingRateKey"));
					strRateDate = GetRateKeyBillDate(lngRateKey);
					if (strRateDate == "")
					{
						// Do Nothing
					}
					else
					{
						rsBill.Edit();
						rsBill.Set_Fields("BillDate", strRateDate);
						rsBill.Update();
					}
					frmWait.InstancePtr.IncrementProgress();
					rsBill.MoveNext();
				}
			}
			// Lien Table
			rsBill.OpenRecordset("SELECT * FROM Lien WHERE DateCreated IS NULL", "TWUT0000.vb1");
			if (rsBill.RecordCount() > 0)
			{
				while (!rsBill.EndOfFile())
				{
					//Application.DoEvents();
					lngRateKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("RateKey"));
					strRateDate = GetRateKeyBillDate(lngRateKey);
					if (strRateDate == "")
					{
						// Do Nothing
					}
					else
					{
						rsBill.Edit();
						rsBill.Set_Fields("DateCreated", strRateDate);
						rsBill.Update();
					}
					rsBill.MoveNext();
				}
			}
			frmWait.InstancePtr.Unload();
		}

		public static string GetRateKeyBillDate(int lngRateKey)
		{
			string GetRateKeyBillDate = "";
			// MAL@20070917: Created as part of the fill-in function for Bill Dates
			// Call Reference: 117062 & 114949
			clsDRWrapper rsRate = new clsDRWrapper();
			string strReturn = "";
			rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey));
			if (rsRate.RecordCount() > 0)
			{
				if (fecherFoundation.FCUtils.IsEmptyDateTime(rsRate.Get_Fields_DateTime("BillDate")))
				{
					strReturn = "";
				}
				else
				{
					strReturn = FCConvert.ToString(rsRate.Get_Fields_DateTime("BillDate"));
				}
			}
			else
			{
				strReturn = "";
			}
			GetRateKeyBillDate = strReturn;
			return GetRateKeyBillDate;
		}

		public static bool HasMoreThanOneOwner(int lngAccountKey, string strReportType, string strSQL = "")
		{
			bool HasMoreThanOneOwner = false;
			// MAL@20070926: Moved to be Public function to determine if more than one owner exists for bills being liened
			bool blnResult = false;
			bool blnContinue = false;
			string strLastName = "";
			clsDRWrapper rsBill = new clsDRWrapper();
			if (strReportType == "30DN")
			{
				rsBill.OpenRecordset("SELECT * FROM (" + Statics.gstr30DayNoticeQuery + ") AS qTmpZ WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else if (strReportType == "LIEN")
			{
				rsBill.OpenRecordset("SELECT * FROM (" + Statics.gstrProcessLienQuery + ") AS qTmpZ WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else if (strReportType == "LIENMAT")
			{
				rsBill.OpenRecordset("SELECT * FROM (" + Statics.gstrLienMaturityQuery + ") AS qTmpZ WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else if (strReportType == "SUMRPT")
			{
				rsBill.OpenRecordset("SELECT * FROM (" + strSQL + ") AS qTmpZ WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else if (strReportType == "LIENEDIT")
			{
				Debug.WriteLine(lngAccountKey);
				rsBill.OpenRecordset("SELECT * FROM (" + Statics.gstrLienEditReportQuery + ") AS qTmpZ WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else
			{
				blnContinue = false;
			}
			if (blnContinue)
			{
				if (rsBill.RecordCount() > 0)
				{
					rsBill.MoveFirst();
					strLastName = FCConvert.ToString(rsBill.Get_Fields_String("OName"));
					rsBill.MoveNext();
					if (rsBill.EndOfFile())
					{
						blnResult = false;
					}
					else
					{
						while (!rsBill.EndOfFile())
						{
							if (FCConvert.ToString(rsBill.Get_Fields_String("OName")) != strLastName)
							{
								blnResult = true;
								break;
							}
							else
							{
								blnResult = false;
							}
							rsBill.MoveNext();
						}
					}
				}
				else
				{
					blnResult = false;
				}
			}
			rsBill.Reset();
			rsBill = null;
			HasMoreThanOneOwner = blnResult;
			return HasMoreThanOneOwner;
		}

		public static void GetLatestOwnerInformation_59022(int lngAccountKey, string strReportType, ref string strLastOwner, string strLastOwner2, string strLastAddress, string strLastAddress2, string strLastAddress3, string strLastCity, string strLastState, string strLastZip, string strSQL = "")
		{
			GetLatestOwnerInformation(lngAccountKey, strReportType, ref strLastOwner, ref strLastOwner2, ref strLastAddress, ref strLastAddress2, ref strLastAddress3, ref strLastCity, ref strLastState, ref strLastZip, strSQL);
		}

		public static void GetLatestOwnerInformation(int lngAccountKey, string strReportType, ref string strLastOwner, ref string strLastOwner2, ref string strLastAddress, ref string strLastAddress2, ref string strLastAddress3, ref string strLastCity, ref string strLastState, ref string strLastZip, string strSQL = "")
		{
			// MAL@20070926: Added as Public routine to use throughout the lien process (was 30DN only)
			bool blnContinue = false;
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			DateTime dtLastDate;
			int lngBillKey;
			// XXXXXX THESE ARE STORED QUERIES! NEED TO REWRITE
			if (strReportType == "30DN")
			{
				rsBill.OpenRecordset("SELECT Max(BillDate) as LastBillDate FROM (" + Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else if (strReportType == "LIEN")
			{
				rsBill.OpenRecordset("SELECT Max(BillDate) as LastBillDate FROM (" + Statics.gstrProcessLienQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else if (strReportType == "LIENMAT")
			{
				rsBill.OpenRecordset("SELECT Max(BillDate) as LastBillDate FROM (" + Statics.gstrLienMaturityQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else if (strReportType == "SUMRPT")
			{
				rsBill.OpenRecordset("SELECT Max(BillDate) as LastBillDate FROM (" + strSQL + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else if (strReportType == "LIENEDIT")
			{
				rsBill.OpenRecordset("SELECT Max(BillDate) as LastBillDate FROM (" + Statics.gstrLienEditReportQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				blnContinue = true;
			}
			else
			{
				blnContinue = false;
			}
			if (blnContinue)
			{
				if (rsBill.RecordCount() > 0)
				{
					// TODO Get_Fields: Field [LastBillDate] not found!! (maybe it is an alias?)
					dtLastDate = (DateTime)rsBill.Get_Fields("LastBillDate");
					// lngBillKey = .Fields("Bill")
					// XXXXXX THESE ARE STORED QUERIES!  NEED TO REWRITE
					if (strReportType == "30DN")
					{
						rsInfo.OpenRecordset("SELECT * FROM (" + Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillDate = '" + FCConvert.ToString(dtLastDate) + "'", modExtraModules.strUTDatabase);
						blnContinue = true;
					}
					else if (strReportType == "LIEN")
					{
						rsInfo.OpenRecordset("SELECT * FROM (" + Statics.gstrProcessLienQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillDate = '" + FCConvert.ToString(dtLastDate) + "'", modExtraModules.strUTDatabase);
						blnContinue = true;
					}
					else if (strReportType == "LIENMAT")
					{
						rsInfo.OpenRecordset("SELECT * FROM (" + Statics.gstrLienMaturityQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillDate = '" + FCConvert.ToString(dtLastDate) + "'", modExtraModules.strUTDatabase);
						blnContinue = true;
					}
					else if (strReportType == "SUMRPT")
					{
						rsInfo.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillDate = '" + FCConvert.ToString(dtLastDate) + "'", modExtraModules.strUTDatabase);
						blnContinue = true;
					}
					else if (strReportType == "LIENEDIT")
					{
						rsInfo.OpenRecordset("SELECT * FROM (" + Statics.gstrLienEditReportQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillDate = '" + FCConvert.ToString(dtLastDate) + "'", modExtraModules.strUTDatabase);
						blnContinue = true;
					}
					else
					{
						blnContinue = false;
					}
					if (rsInfo.RecordCount() > 0)
					{
						strLastOwner = FCConvert.ToString(rsInfo.Get_Fields_String("OName"));
						strLastOwner2 = FCConvert.ToString(rsInfo.Get_Fields_String("OName2"));
						strLastAddress = FCConvert.ToString(rsInfo.Get_Fields_String("OAddress1"));
						strLastAddress2 = FCConvert.ToString(rsInfo.Get_Fields_String("OAddress2"));
						strLastAddress3 = FCConvert.ToString(rsInfo.Get_Fields_String("OAddress3"));
						strLastCity = FCConvert.ToString(rsInfo.Get_Fields_String("OCity"));
						strLastState = FCConvert.ToString(rsInfo.Get_Fields_String("OState"));
						if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("OZip4"))) != "")
						{
							strLastZip = rsInfo.Get_Fields_String("OZip") + "-" + rsInfo.Get_Fields_String("OZip4");
						}
						else
						{
							strLastZip = FCConvert.ToString(rsInfo.Get_Fields_String("OZip"));
						}
					}
				}
			}
		}
		//FC:FINAL:AM: not used
		//private static void AddIndextoBillTable()
		//{
		//	DAO.Database db;
		//	TableDef tdf = new TableDef();
		//	DAO.Index idx = new DAO.Index();
		//	int intCount;
		//	bool blnAcctExists = false;
		//	bool blnBillExists = false;
		//	db = OpenDatabase(modExtraModules.strUTDatabase, false, false, ";PWD="+modGlobalConstants.DATABASEPASSWORD);
		//	tdf = db.TableDefs("Bill");
		//	foreach (int idx_foreach in tdf.Indexes) {
		//		idx = idx_foreach;
		//		if (idx.Name=="ActualAccountNumber") {
		//			blnAcctExists = true;
		//		} else if (idx.Name=="BillNumber") {
		//			blnBillExists = true;
		//		}
		//		idx = null;
		//	} // idx
		//	if (!blnAcctExists) {
		//		idx = tdf.CreateIndex("ActualAccountNumber");
		//		idx.Primary = false;
		//		idx.Unique = false;
		//		idx.Required = true;
		//		((DAO.IndexFields) idx.Fields).Append(idx.CreateField("ActualAccountNumber"));
		//		tdf.Indexes.Append(idx);
		//		idx = null;
		//	}
		//	if (!blnBillExists) {
		//		idx = tdf.CreateIndex("BillNumber");
		//		idx.Primary = false;
		//		idx.Unique = false;
		//		idx.Required = true;
		//		((DAO.IndexFields) idx.Fields).Append(idx.CreateField("BillNumber"));
		//		tdf.Indexes.Append(idx);
		//		idx = null;
		//	}
		//}
		private static void AddSecurityPermissions()
		{
			// Tracker Reference: 11808
			clsDRWrapper rsTable = new clsDRWrapper();
			clsDRWrapper rsPerm = new clsDRWrapper();
			int lngID;
			string strPerm = "";
			rsTable.OpenRecordset("SELECT * FROM PermissionSetup WHERE ModuleName = 'UT' AND FunctionID = 31", "SystemSettings");
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
				rsTable.Set_Fields("ModuleName", "UT");
				rsTable.Set_Fields("ParentFunction", "File Maintenance");
				rsTable.Set_Fields("ChildFunction", "   Permanently Delete Account");
				rsTable.Set_Fields("FunctionID", 31);
				rsTable.Set_Fields("UseViewOnly", false);
				rsTable.Set_Fields("Constant", "UTSECPERMDELETEACCOUNT");
				rsTable.Update();
			}
			rsTable.OpenRecordset("SELECT UserID, Permission FROM PermissionsTable WHERE ModuleName = 'UT' AND FunctionID >=16 AND FunctionID <=31", "SystemSettings");
			if (rsTable.RecordCount() > 0)
			{
				rsTable.MoveFirst();
				while (!rsTable.EndOfFile())
				{
					strPerm = FCConvert.ToString(rsTable.Get_Fields_String("Permission"));
					// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
					rsPerm.OpenRecordset("SELECT * FROM PermissionsTable WHERE UserID = " + rsTable.Get_Fields("UserID") + " AND ModuleName = 'UT' AND FunctionID = 31", "SystemSettings");
					if (rsPerm.RecordCount() == 0)
					{
						rsPerm.AddNew();
						// TODO Get_Fields: Check the table for the column [UserID] and replace with corresponding Get_Field method
						rsPerm.Set_Fields("UserID", rsTable.Get_Fields("UserID"));
						rsPerm.Set_Fields("ModuleName", "UT");
						rsPerm.Set_Fields("FunctionID", 31);
						rsPerm.Set_Fields("Permission", strPerm);
						rsPerm.Update();
					}
					rsTable.MoveNext();
				}
			}
		}

		private static void GeneralFixPastDueAmounts()
		{
			// Tracker Reference: 12128
			bool blnContinue = false;
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsLien = new clsDRWrapper();
			clsDRWrapper rsPayment = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			int lngAccountKey;
			int lngBillKey = 0;
			int lngSLienKey = 0;
			int lngWLienKey = 0;
			string strBillService = "";
			string strService = "";
			double dblWPrinOwed_Bill = 0;
			double dblWTaxOwed_Bill = 0;
			double dblWIntOwed_Bill = 0;
			double dblWCostOwed_Bill = 0;
			double dblWPLIOwed_Bill = 0;
			double dblWMatOwed_Bill = 0;
			double dblWIntAdded_Bill = 0;
			double dblWCostAdded_Bill;
			double dblWPrinPaid_Bill = 0;
			double dblWTaxPaid_Bill = 0;
			double dblWIntPaid_Bill = 0;
			double dblWCostPaid_Bill = 0;
			double dblWPLIPaid_Bill = 0;
			double dblWMatPaid_Bill;
			double dblWPrinPaid_Pymt = 0;
			double dblWTaxPaid_Pymt = 0;
			double dblWIntPaid_Pymt = 0;
			double dblWIntChg_Pymt = 0;
			double dblWCostPaid_Pymt = 0;
			double dblWPLIPaid_Pymt = 0;
			double dblWCostChg_Pymt = 0;
			double dblWMatPaid_Pymt;
			double dblWMatChg_Pymt = 0;
			double dblSPrinOwed_Bill = 0;
			double dblSTaxOwed_Bill = 0;
			double dblSIntOwed_Bill = 0;
			double dblSCostOwed_Bill = 0;
			double dblSPLIOwed_Bill = 0;
			double dblSMatOwed_Bill = 0;
			double dblSIntAdded_Bill = 0;
			double dblSCostAdded_Bill;
			double dblSPrinPaid_Bill = 0;
			double dblSTaxPaid_Bill = 0;
			double dblSIntPaid_Bill = 0;
			double dblSCostPaid_Bill = 0;
			double dblSPLIPaid_Bill = 0;
			double dblSMatPaid_Bill;
			double dblSPrinPaid_Pymt = 0;
			double dblSTaxPaid_Pymt = 0;
			double dblSIntPaid_Pymt = 0;
			double dblSIntChg_Pymt = 0;
			double dblSCostPaid_Pymt = 0;
			double dblSPLIPaid_Pymt = 0;
			double dblSCostChg_Pymt = 0;
			double dblSMatPaid_Pymt;
			double dblSMatChg_Pymt = 0;
			double dblSTotalOwed = 0;
			double dblSTotalPaid = 0;
			double dblWTotalOwed = 0;
			double dblWTotalPaid = 0;
			double dblSTotal;
			double dblWTotal;
			rsTemp.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
			blnContinue = !FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("GeneralFixPastDueAmts"));
			strService = FCConvert.ToString(rsTemp.Get_Fields_String("Service"));
			frmWait.InstancePtr.Init("Checking Payment Amounts", true);
			if (blnContinue)
			{
				frmWait.InstancePtr.lblMessage.Text = "Checking Non-Lien Payments" + "\r\n" + "Please Wait...";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				// Non-Liened Records
				rsBill.OpenRecordset("SELECT * FROM Bill WHERE (WLienRecordNumber = 0 AND SLienRecordNumber = 0) ORDER BY ActualAccountNumber, ID", modExtraModules.strUTDatabase);
				if (rsBill.RecordCount() > 0)
				{
					frmWait.InstancePtr.prgProgress.Maximum = rsBill.RecordCount();
					rsBill.MoveFirst();
					while (!rsBill.EndOfFile())
					{
						//Application.DoEvents();
						lngBillKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
						lngAccountKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("AccountKey"));
						strBillService = FCConvert.ToString(rsBill.Get_Fields_String("Service"));
						// Reset Values
						dblSPrinPaid_Pymt = 0;
						dblSTaxPaid_Pymt = 0;
						dblSIntPaid_Pymt = 0;
						dblSCostPaid_Pymt = 0;
						dblSIntChg_Pymt = 0;
						dblWPrinPaid_Pymt = 0;
						dblWTaxPaid_Pymt = 0;
						dblWIntPaid_Pymt = 0;
						dblWCostPaid_Pymt = 0;
						dblWIntChg_Pymt = 0;
						dblSCostChg_Pymt = 0;
						dblWCostChg_Pymt = 0;
						if (strService == "B" || strService == "S")
						{
							dblSPrinOwed_Bill = rsBill.Get_Fields_Double("SPrinOwed");
							dblSTaxOwed_Bill = rsBill.Get_Fields_Double("STaxOwed");
							dblSIntOwed_Bill = rsBill.Get_Fields_Double("SIntOwed");
							dblSIntAdded_Bill = rsBill.Get_Fields_Double("SIntAdded");
							dblSCostOwed_Bill = rsBill.Get_Fields_Double("SCostOwed") - rsBill.Get_Fields_Double("SCostAdded");
							dblSTotalOwed = dblSPrinOwed_Bill + dblSTaxOwed_Bill + dblSIntOwed_Bill - dblSIntAdded_Bill + dblSCostOwed_Bill;
							dblSPrinPaid_Bill = rsBill.Get_Fields_Double("SPrinPaid");
							dblSTaxPaid_Bill = rsBill.Get_Fields_Double("STaxPaid");
							dblSIntPaid_Bill = rsBill.Get_Fields_Double("SIntPaid");
							dblSCostPaid_Bill = rsBill.Get_Fields_Double("SCostPaid");
							dblSTotalPaid = dblSPrinPaid_Bill + dblSTaxPaid_Bill + dblSIntPaid_Bill + dblSCostPaid_Bill;
							dblSTotal = dblSTotalOwed - dblSTotalPaid;
							// Get Charged Interest to Compare
							rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = 'S' AND ISNULL(Lien,0) = 0 AND (Reference = 'CHGINT' Or Reference = 'Interest')", modExtraModules.strUTDatabase);
							if (rsPayment.RecordCount() > 0)
							{
								if (rsPayment.RecordCount() > 1)
								{
									while (!rsPayment.EndOfFile())
									{
										dblSIntChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
										rsPayment.MoveNext();
									}
								}
								else
								{
									dblSIntChg_Pymt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
								}
							}
							// Get Charged Costs (Demand Fees) to Compare
							rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = 'S' AND ISNULL(Lien,0) = 0 AND (Code = '3' Or Code = 'L')", modExtraModules.strUTDatabase);
							if (rsPayment.RecordCount() > 0)
							{
								if (rsPayment.RecordCount() > 1)
								{
									while (!rsPayment.EndOfFile())
									{
										dblSCostChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
										rsPayment.MoveNext();
									}
								}
								else
								{
									dblSCostChg_Pymt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
								}
							}
							rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = 'S' AND ISNULL(Lien,0) = 0 AND Reference <> 'CHGINT' AND Reference <> 'Interest' AND Code <> '3' AND Code <> 'L'", modExtraModules.strUTDatabase);
							if (rsPayment.RecordCount() > 0)
							{
								rsPayment.MoveFirst();
								while (!rsPayment.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
									dblSPrinPaid_Pymt += rsPayment.Get_Fields("Principal");
									// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
									dblSTaxPaid_Pymt += rsPayment.Get_Fields("Tax");
									dblSIntPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									if (FCConvert.ToString(rsPayment.Get_Fields("Code")) != "X")
									{
										dblSCostPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
									}
									rsPayment.MoveNext();
								}
							}
							else
							{
								// Do Nothing - No Payments, Nothing to Fix
							}
							if (dblSCostChg_Pymt < 0)
							{
								dblSCostChg_Pymt = (dblSCostChg_Pymt * -1);
							}
							// Principal ; Only Change the Paid Amount, Should Not Change the Charged Amount
							if (dblSPrinPaid_Bill == dblSPrinPaid_Pymt)
							{
								// Principal Payments Match - Move On
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("SPrinPaid", dblSPrinPaid_Pymt);
								rsBill.Update();
							}
							// Tax
							if (dblSTaxPaid_Bill == dblSTaxPaid_Pymt)
							{
								// Tax Payments Match - Move On
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("STaxPaid", dblSTaxPaid_Pymt);
								rsBill.Update();
							}
							// Costs
							if (dblSCostOwed_Bill == dblSCostChg_Pymt)
							{
								// Do Nothing - They're Equal
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("SCostAdded", (dblSCostChg_Pymt * -1));
								rsBill.Update();
							}
							if (dblSCostPaid_Bill == dblSCostPaid_Pymt)
							{
								// Cost Payments Match - Move On
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("SCostPaid", dblSCostPaid_Pymt);
								rsBill.Update();
							}
							// Charged Interest ; Check All Pieces (Paid and Charged)
							if (dblSIntAdded_Bill != dblSIntChg_Pymt)
							{
								rsBill.Edit();
								rsBill.Set_Fields("SIntAdded", dblSIntChg_Pymt);
								rsBill.Update();
							}
							// Check Payments
							if (dblSIntPaid_Bill == dblSIntPaid_Pymt)
							{
								// Equal - No Changes Needed
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("SIntPaid", dblSIntPaid_Pymt);
								rsBill.Update();
							}
						}
						else
						{
							// Do Nothing - Nothing is Due & No Credit Amount
						}
						if (strService == "B" || strService == "W")
						{
							dblWPrinOwed_Bill = rsBill.Get_Fields_Double("WPrinOwed");
							dblWTaxOwed_Bill = rsBill.Get_Fields_Double("WTaxOwed");
							dblWIntOwed_Bill = rsBill.Get_Fields_Double("WIntOwed");
							dblWIntAdded_Bill = rsBill.Get_Fields_Double("WIntAdded");
							dblWCostOwed_Bill = rsBill.Get_Fields_Double("WCostOwed") - rsBill.Get_Fields_Double("WCostAdded");
							dblWTotalOwed = dblWPrinOwed_Bill + dblWTaxOwed_Bill + dblWIntOwed_Bill + dblWCostOwed_Bill;
							dblWPrinPaid_Bill = rsBill.Get_Fields_Double("WPrinPaid");
							dblWTaxPaid_Bill = rsBill.Get_Fields_Double("WTaxPaid");
							dblWIntPaid_Bill = rsBill.Get_Fields_Double("WIntPaid");
							dblWCostPaid_Bill = rsBill.Get_Fields_Double("WCostPaid");
							dblWTotalPaid = dblWPrinPaid_Bill + dblWTaxPaid_Bill + dblWIntPaid_Bill + dblWCostPaid_Bill;
							dblWTotal = dblWTotalOwed - dblWTotalPaid;
							// Get Charged Interest to Compare
							rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = 'W' AND ISNULL(Lien,0) = 0 AND Reference = 'CHGINT'", modExtraModules.strUTDatabase);
							if (rsPayment.RecordCount() > 0)
							{
								if (rsPayment.RecordCount() > 1)
								{
									while (!rsPayment.EndOfFile())
									{
										dblWIntChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
										rsPayment.MoveNext();
									}
								}
								else
								{
									dblWIntChg_Pymt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
								}
							}
							// Get Charged Costs (Demand Fees) to Compare
							rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = 'W' AND ISNULL(Lien,0) = 0 AND (Code = '3' Or Code = 'L')", modExtraModules.strUTDatabase);
							if (rsPayment.RecordCount() > 0)
							{
								if (rsPayment.RecordCount() > 1)
								{
									while (!rsPayment.EndOfFile())
									{
										dblWCostChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
										rsPayment.MoveNext();
									}
								}
								else
								{
									dblWCostChg_Pymt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
								}
							}
							rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Service = 'W' AND ISNULL(Lien,0) = 0 AND Reference <> 'CHGINT' AND Code <> '3' AND Code <> 'L'", modExtraModules.strUTDatabase);
							if (rsPayment.RecordCount() > 0)
							{
								rsPayment.MoveFirst();
								while (!rsPayment.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
									dblWPrinPaid_Pymt += rsPayment.Get_Fields("Principal");
									// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
									dblWTaxPaid_Pymt += rsPayment.Get_Fields("Tax");
									dblWIntPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									if (FCConvert.ToString(rsPayment.Get_Fields("Code")) != "X")
									{
										dblWCostPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
									}
									rsPayment.MoveNext();
								}
							}
							else
							{
								// Do Nothing - No Payments, Nothing to Fix
							}
							if (dblWCostChg_Pymt < 0)
							{
								dblWCostChg_Pymt = (dblWCostChg_Pymt * -1);
							}
							if (dblWPrinPaid_Bill == dblWPrinPaid_Pymt)
							{
								// Principal Payments Match - Move On
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("WPrinPaid", dblWPrinPaid_Pymt);
								rsBill.Update();
							}
							// Tax
							if (dblWTaxPaid_Bill == dblWTaxPaid_Pymt)
							{
								// Tax Payments Match - Move On
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("WTaxPaid", dblWTaxPaid_Pymt);
								rsBill.Update();
							}
							// Costs
							if (dblWCostOwed_Bill == dblWCostChg_Pymt)
							{
								// Do Nothing - They're Equal
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("WCostAdded", (dblWCostChg_Pymt * -1));
								rsBill.Update();
							}
							if (dblWCostPaid_Bill == dblWCostPaid_Pymt)
							{
								// Cost Payments Match - Move On
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("WCostPaid", dblWCostPaid_Pymt);
								rsBill.Update();
							}
							// Charged Interest ; Check All Pieces (Paid and Charged)
							if (dblWIntAdded_Bill != dblWIntChg_Pymt)
							{
								rsBill.Edit();
								rsBill.Set_Fields("WIntAdded", dblWIntChg_Pymt);
								rsBill.Update();
							}
							// Check Payments
							if (dblWIntPaid_Bill == dblWIntPaid_Pymt)
							{
								// Equal - No Changes Needed
							}
							else
							{
								rsBill.Edit();
								rsBill.Set_Fields("WIntPaid", dblWIntPaid_Pymt);
								rsBill.Update();
							}
						}
						else
						{
							// Do Nothing - Nothing Due & No Credit Amount
						}
						frmWait.InstancePtr.IncrementProgress();
						rsBill.MoveNext();
					}
				}
				// Liened Bills
				frmWait.InstancePtr.lblMessage.Text = "Checking Lien Payments" + "\r\n" + "Please Wait...";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.Refresh();
				rsBill.OpenRecordset("SELECT * FROM Bill WHERE (WLienRecordNumber > 0 OR SLienRecordNumber > 0) ORDER BY ActualAccountNumber, ID", modExtraModules.strUTDatabase);
				if (rsBill.RecordCount() > 0)
				{
					frmWait.InstancePtr.prgProgress.Maximum = rsBill.RecordCount();
					rsBill.MoveFirst();
					while (!rsBill.EndOfFile())
					{
						//Application.DoEvents();
						lngBillKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
						lngSLienKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("SLienRecordNumber"));
						lngWLienKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("WLienRecordNumber"));
						lngAccountKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("AccountKey"));
						strBillService = FCConvert.ToString(rsBill.Get_Fields_String("Service"));
						// Reset Values
						dblSPrinPaid_Pymt = 0;
						dblSTaxPaid_Pymt = 0;
						dblSIntPaid_Pymt = 0;
						dblSCostPaid_Pymt = 0;
						dblSPLIPaid_Pymt = 0;
						dblWPrinPaid_Pymt = 0;
						dblWTaxPaid_Pymt = 0;
						dblWIntPaid_Pymt = 0;
						dblWCostPaid_Pymt = 0;
						dblWPLIPaid_Pymt = 0;
						dblWIntChg_Pymt = 0;
						dblSIntChg_Pymt = 0;
						dblWMatPaid_Pymt = 0;
						dblWMatChg_Pymt = 0;
						dblSMatPaid_Pymt = 0;
						dblSMatChg_Pymt = 0;
						if (strService == "B" || strService == "S")
						{
							rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngSLienKey), modExtraModules.strUTDatabase);
							if (rsLien.RecordCount() > 0)
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								dblSPrinOwed_Bill = rsLien.Get_Fields("Principal");
								// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
								dblSTaxOwed_Bill = rsLien.Get_Fields("Tax");
								// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
								dblSIntOwed_Bill = rsLien.Get_Fields("IntAdded") * -1;
								// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
								dblSPLIOwed_Bill = rsLien.Get_Fields("Interest");
								// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
								dblSCostOwed_Bill = rsLien.Get_Fields("Costs");
								// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
								dblSMatOwed_Bill = rsLien.Get_Fields("MaturityFee") * -1;
								dblSTotalOwed = dblSPrinOwed_Bill + dblSTaxOwed_Bill + dblSIntOwed_Bill + dblSPLIOwed_Bill + dblSCostOwed_Bill + dblSMatOwed_Bill;
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								dblSPrinPaid_Bill = rsLien.Get_Fields("PrinPaid");
								// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
								dblSTaxPaid_Bill = rsLien.Get_Fields("TaxPaid");
								// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
								dblSIntPaid_Bill = rsLien.Get_Fields("IntPaid");
								// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
								dblSPLIPaid_Bill = rsLien.Get_Fields("PLIPaid");
								dblSCostPaid_Bill = rsLien.Get_Fields_Double("CostPaid");
								dblSTotalPaid = dblSPrinPaid_Bill + dblSTaxPaid_Bill + dblSIntPaid_Bill + dblSPLIPaid_Bill + dblSCostPaid_Bill;
								dblSTotal = dblSTotalOwed - dblSTotalPaid;
								rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngSLienKey) + " AND Service = 'S' AND ISNULL(Lien,0) = 1 AND (Reference = 'CHGINT' OR Reference = 'Interest')", modExtraModules.strUTDatabase);
								if (rsPayment.RecordCount() > 0)
								{
									if (rsPayment.RecordCount() > 1)
									{
										while (!rsPayment.EndOfFile())
										{
											dblSIntChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
											rsPayment.MoveNext();
										}
									}
									else
									{
										dblSIntChg_Pymt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
									}
								}
								// Get Added Maturity Fees to Compare
								rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngSLienKey) + " AND Service = 'S' AND ISNULL(Lien,0) = 1 AND (Code = 'L' OR Code = '3')", modExtraModules.strUTDatabase);
								if (rsPayment.RecordCount() > 0)
								{
									if (rsPayment.RecordCount() > 1)
									{
										while (!rsPayment.EndOfFile())
										{
											dblSMatChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
											rsPayment.MoveNext();
										}
									}
									else
									{
										dblSMatChg_Pymt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
									}
								}
								rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngSLienKey) + " AND Service = 'S' AND ISNULL(Lien,0) = 1 AND Code <> '3' AND Code <> 'L' AND Code <> 'I'", modExtraModules.strUTDatabase);
								if (rsPayment.RecordCount() > 0)
								{
									rsPayment.MoveFirst();
									while (!rsPayment.EndOfFile())
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblSPrinPaid_Pymt += rsPayment.Get_Fields("Principal");
										// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
										dblSTaxPaid_Pymt += rsPayment.Get_Fields("Tax");
										dblSIntPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
										dblSPLIPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest"));
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										if (FCConvert.ToString(rsPayment.Get_Fields("Code")) != "X")
										{
											dblSCostPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
										}
										rsPayment.MoveNext();
									}
									// Principal ; Only Change the Paid Amount, Should Not Change the Charged Amount
									if (dblSPrinPaid_Bill == dblSPrinPaid_Pymt)
									{
										// Principal Payments Match - Move On
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("PrinPaid", dblSPrinPaid_Pymt);
										rsLien.Update();
									}
									// Maturity Fees
									if (dblSMatOwed_Bill != (dblSMatChg_Pymt * -1))
									{
										rsLien.Edit();
										rsLien.Set_Fields("MaturityFee", dblSMatChg_Pymt);
										rsLien.Update();
									}
									// Costs ; Check All Pieces (Paid and Charged)
									if (dblSCostPaid_Bill == dblSCostPaid_Pymt)
									{
										// Cost Payments Match - Move On
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("CostPaid", dblSCostPaid_Pymt);
										rsLien.Update();
									}
									// Charged Interest ; Check All Pieces (Paid and Charged)
									if (dblSIntOwed_Bill != (dblSIntChg_Pymt * -1))
									{
										rsLien.Edit();
										rsLien.Set_Fields("IntAdded", dblSIntChg_Pymt);
										rsLien.Update();
									}
									// Check Payments
									if (dblSIntPaid_Bill == dblSIntPaid_Pymt)
									{
										// Equal - No Changes Needed
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("IntPaid", dblSIntPaid_Pymt);
										rsLien.Update();
									}
									// Pre-Lien Interest ; Check All Pieces (Paid and Charged)
									if (dblSPLIPaid_Bill == dblSPLIPaid_Pymt)
									{
										// Interest Payments Match - Move On
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("PLIPaid", dblSPLIPaid_Pymt);
										rsLien.Update();
									}
									// Tax ; Check All Pieces (Paid and Charged)
									if (dblSTaxPaid_Bill == dblSTaxPaid_Pymt)
									{
										// Tax Payments Match - Move On
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("TaxPaid", dblSTaxPaid_Pymt);
										rsLien.Update();
									}
								}
								else
								{
									// Do Nothing - No Payments, Nothing to Fix
								}
							}
							else
							{
								// Do Nothing - Lien Record Does Not Exist
							}
						}
						if (strService == "B" || strService == "W")
						{
							rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngWLienKey), modExtraModules.strUTDatabase);
							if (rsLien.RecordCount() > 0)
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								dblWPrinOwed_Bill = rsLien.Get_Fields("Principal");
								// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
								dblWTaxOwed_Bill = rsLien.Get_Fields("Tax");
								// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
								dblWIntOwed_Bill = rsLien.Get_Fields("IntAdded");
								// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
								dblWPLIOwed_Bill = rsLien.Get_Fields("Interest");
								// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
								dblWCostOwed_Bill = rsLien.Get_Fields("Costs");
								// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
								dblWMatOwed_Bill = rsLien.Get_Fields("MaturityFee") * -1;
								dblWTotalOwed = dblWPrinOwed_Bill + dblWTaxOwed_Bill + dblWIntOwed_Bill + dblWPLIOwed_Bill + dblWCostOwed_Bill + dblWMatOwed_Bill;
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								dblWPrinPaid_Bill = rsLien.Get_Fields("PrinPaid");
								// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
								dblWTaxPaid_Bill = rsLien.Get_Fields("TaxPaid");
								// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
								dblWIntPaid_Bill = rsLien.Get_Fields("IntPaid");
								// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
								dblWPLIPaid_Bill = rsLien.Get_Fields("PLIPaid");
								dblWCostPaid_Bill = rsLien.Get_Fields_Double("CostPaid");
								dblWTotalPaid = dblWPrinPaid_Bill + dblWTaxPaid_Bill + dblWIntPaid_Bill + dblWPLIPaid_Bill + dblWCostPaid_Bill;
								dblWTotal = dblWTotalOwed - dblWTotalPaid;
								rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngWLienKey) + " AND Service = 'W' AND ISNULL(Lien,0) = 1 AND Reference = 'CHGINT'", modExtraModules.strUTDatabase);
								if (rsPayment.RecordCount() > 0)
								{
									if (rsPayment.RecordCount() > 1)
									{
										while (!rsPayment.EndOfFile())
										{
											dblWIntChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
											rsPayment.MoveNext();
										}
									}
									else
									{
										dblWIntChg_Pymt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
									}
								}
								// Get Added Maturity Fees to Compare
								rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngWLienKey) + " AND Service = 'W' AND ISNULL(Lien,0) = 1 AND (Code = 'L' OR Code = '3')", modExtraModules.strUTDatabase);
								if (rsPayment.RecordCount() > 0)
								{
									if (rsPayment.RecordCount() > 1)
									{
										while (!rsPayment.EndOfFile())
										{
											dblWMatChg_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
											rsPayment.MoveNext();
										}
									}
									else
									{
										dblWMatChg_Pymt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
									}
								}
								rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngWLienKey) + " AND Service = 'W' AND ISNULL(Lien,0) = 1 AND Code <> '3' AND Code <> 'L' AND Code <> 'I'", modExtraModules.strUTDatabase);
								if (rsPayment.RecordCount() > 0)
								{
									rsPayment.MoveFirst();
									while (!rsPayment.EndOfFile())
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblWPrinPaid_Pymt += rsPayment.Get_Fields("Principal");
										// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
										dblWTaxPaid_Pymt += rsPayment.Get_Fields("Tax");
										dblWIntPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
										dblWPLIPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest"));
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										if (FCConvert.ToString(rsPayment.Get_Fields("Code")) != "X")
										{
											dblWCostPaid_Pymt += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
										}
										rsPayment.MoveNext();
									}
									// Principal ; Only Change the Paid Amount, Should Not Change the Charged Amount
									if (dblWPrinPaid_Bill == dblWPrinPaid_Pymt)
									{
										// Principal Payments Match - Move On
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("PrinPaid", dblWPrinPaid_Pymt);
										rsLien.Update();
									}
									// Maturity Fees
									if (dblWMatOwed_Bill != (dblWMatChg_Pymt * -1))
									{
										rsLien.Edit();
										rsLien.Set_Fields("MaturityFee", dblWMatChg_Pymt);
										rsLien.Update();
									}
									// Costs ; Check All Pieces (Paid and Charged)
									if (dblWCostPaid_Bill == dblWCostPaid_Pymt)
									{
										// Cost Payments Match - Move On
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("CostPaid", dblWCostPaid_Pymt);
										rsLien.Update();
									}
									// Charged Interest ; Check All Pieces (Paid and Charged)
									if (dblWIntOwed_Bill != (dblWIntChg_Pymt * -1))
									{
										rsLien.Edit();
										rsLien.Set_Fields("IntAdded", dblWIntChg_Pymt);
										rsLien.Update();
									}
									// Check Payments
									if (dblWIntPaid_Bill == dblWIntPaid_Pymt)
									{
										// Equal - No Changes Needed
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("IntPaid", dblWIntPaid_Pymt);
										rsLien.Update();
									}
									// Pre-Lien Interest
									if (dblWPLIPaid_Bill == dblWPLIPaid_Pymt)
									{
										// Interest Payments Match - Move On
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("PLIPaid", dblWPLIPaid_Pymt);
										rsLien.Update();
									}
									// Tax ; Check All Pieces (Paid and Charged)
									if (dblWTaxPaid_Bill == dblWTaxPaid_Pymt)
									{
										// Tax Payments Match - Move On
									}
									else
									{
										rsLien.Edit();
										rsLien.Set_Fields("TaxPaid", dblWTaxPaid_Pymt);
										rsLien.Update();
									}
								}
								else
								{
									// Do Nothing - No Payments, Nothing to Fix
								}
							}
							else
							{
								// Do Nothing - Lien Record Does Not Exist
							}
						}
						frmWait.InstancePtr.IncrementProgress();
						rsBill.MoveNext();
					}
				}
				rsTemp.Edit();
				rsTemp.Set_Fields("GeneralFixPastDueAmts", true);
				rsTemp.Update();
				frmWait.InstancePtr.lblMessage.Text = "Checking Database" + "\r\n" + "Please Wait...";
				frmWait.InstancePtr.prgProgress.Value = 0;
				frmWait.InstancePtr.prgProgress.Maximum = 10;
				frmWait.InstancePtr.prgProgress.Value = 2;
				frmWait.InstancePtr.Refresh();
			}
			else
			{
				// This Has Already Been Run - Don't Run It Again
			}
		}

		public static void UpdateMeterCategories()
		{
			// Tracker Reference: 12701
			clsDRWrapper rsMeter = new clsDRWrapper();
			clsDRWrapper rsAccount = new clsDRWrapper();
			int lngKey = 0;
			int lngSCat = 0;
			int lngWCat = 0;
			rsAccount.OpenRecordset("SELECT * FROM Master", modExtraModules.strUTDatabase);
			if (rsAccount.RecordCount() > 0)
			{
				rsAccount.MoveFirst();
				while (!rsAccount.EndOfFile())
				{
					lngKey = FCConvert.ToInt32(rsAccount.Get_Fields_Int32("ID"));
					lngSCat = FCConvert.ToInt32(rsAccount.Get_Fields_Int32("SewerCategory"));
					lngWCat = FCConvert.ToInt32(rsAccount.Get_Fields_Int32("WaterCategory"));
					rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
					if (rsMeter.RecordCount() > 0)
					{
						rsMeter.MoveFirst();
						while (!rsMeter.EndOfFile())
						{
							rsMeter.Edit();
							rsMeter.Set_Fields("SCat", lngSCat);
							rsMeter.Set_Fields("WCat", lngWCat);
							rsMeter.Update();
							rsMeter.MoveNext();
						}
					}
					rsAccount.MoveNext();
				}
			}
		}

		public static void UpdateWaterCosts20080320()
		{
			// Tracker Reference: 12857
			clsDRWrapper rsBill = new clsDRWrapper();
			rsBill.OpenRecordset("SELECT * FROM Bill WHERE WCostAdded <> 0 AND WLienStatusEligibility <= 1", modExtraModules.strUTDatabase);
			if (rsBill.RecordCount() > 0)
			{
				rsBill.MoveFirst();
				while (!rsBill.EndOfFile())
				{
					rsBill.Edit();
					rsBill.Set_Fields("WCostAdded", 0);
					rsBill.Update();
					rsBill.MoveNext();
				}
			}
			rsBill.OpenRecordset("SELECT * FROM Bill WHERE SCostAdded <> 0 AND SLienStatusEligibility <= 1", modExtraModules.strUTDatabase);
			if (rsBill.RecordCount() > 0)
			{
				rsBill.MoveFirst();
				while (!rsBill.EndOfFile())
				{
					rsBill.Edit();
					rsBill.Set_Fields("SCostAdded", 0);
					rsBill.Update();
					rsBill.MoveNext();
				}
			}
		}

		public static void ClearBogusCHGINTRecords()
		{
			// Tracker Reference: 13228
			clsDRWrapper rsPymt = new clsDRWrapper();
			rsPymt.OpenRecordset("SELECT * FROM PaymentRec WHERE Reference = 'CHGINT' AND ReceiptNumber = -1", modExtraModules.strUTDatabase);
			if (rsPymt.RecordCount() > 0)
			{
				rsPymt.MoveFirst();
				while (!rsPymt.EndOfFile())
				{
					rsPymt.Delete();
					rsPymt.MoveNext();
				}
			}
		}

		private static void InsertIConnectRecords()
		{
			// Tracker Reference: 10680
			clsDRWrapper rsMaster = new clsDRWrapper();
			clsDRWrapper rsI = new clsDRWrapper();
			string strName = "";
			rsI.OpenRecordset("SELECT * FROM tblIConnectInfo", modExtraModules.strUTDatabase);
			rsMaster.OpenRecordset("SELECT m.ID,m.SameBillOwner,m.AccountNumber,m.BillMessage,pOwn.FullNameLF AS OwnerName,pBill.FullNameLF AS Name " + "FROM Master m INNER JOIN " + Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.PartyID = m.OwnerPartyID INNER JOIN " + Statics.strDbCP + "PartyAndAddressView pBill ON pBill.PartyID = m.BillingPartyID " + "WHERE ISNULL(Deleted,0) = 0", modExtraModules.strUTDatabase);
			if (rsMaster.RecordCount() > 0)
			{
				rsMaster.MoveFirst();
				while (!rsMaster.EndOfFile())
				{
					//Application.DoEvents();
					if (rsMaster.Get_Fields_Boolean("SameBillOwner") == true)
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						strName = RemoveSpecialCharacters(rsMaster.Get_Fields("OwnerName"), true);
					}
					else
					{
						strName = RemoveSpecialCharacters(rsMaster.Get_Fields_String("Name"), true);
					}
					if (strName != "")
					{
						// MAL@20081118: Change to check if account exists and add if necessary
						// Tracker Reference: 16194
						rsI.MoveFirst();
						rsI.FindFirstRecord("AccountKey", rsMaster.Get_Fields_Int32("ID"));
						if (rsI.NoMatch)
						{
							rsI.AddNew();
							rsI.Set_Fields("AccountKey", rsMaster.Get_Fields_Int32("ID"));
							rsI.Set_Fields("IConnectAccountKey", 0);
							if (rsMaster.Get_Fields_Boolean("SameBillOwner") == true)
							{
								rsI.Set_Fields("OwnerTenant", "O");
							}
							else
							{
								rsI.Set_Fields("OwnerTenant", "T");
							}
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							rsI.Set_Fields("WebPIN", rsMaster.Get_Fields("AccountNumber"));
							rsI.Set_Fields("AcceptEmail", false);
							rsI.Set_Fields("AcceptEbill", "P");
							rsI.Set_Fields("SpecialMessage", rsMaster.Get_Fields_String("BillMessage"));
							rsI.Set_Fields("GraphFlag", "D");
							rsI.Update();
						}
					}
					rsMaster.MoveNext();
				}
			}
		}

		private static void InsertDefaultIConnectAccounts()
		{
			// Tracker Reference: 10680
			clsDRWrapper rsAcct = new clsDRWrapper();
			string strService = "";
			int intC;
			rsAcct.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
			if (rsAcct.RecordCount() > 0)
			{
				strService = FCConvert.ToString(rsAcct.Get_Fields_String("Service"));
			}
			rsAcct.Reset();
			rsAcct.OpenRecordset("SELECT * FROM tblIConnectAccountInfo", modExtraModules.strUTDatabase);
			// 93 - Utility Water Payment
			rsAcct.AddNew();
			rsAcct.Set_Fields("TypeCode", 93);
			rsAcct.Set_Fields("TypeTitle", "Utility - Water Payment");
			rsAcct.Set_Fields("Title1", "Principal");
			rsAcct.Set_Fields("Title1Abbrev", "Prin");
			rsAcct.Set_Fields("Account1", "");
			rsAcct.Set_Fields("DefaultAmount1", 0);
			rsAcct.Set_Fields("Title2", "Interest");
			rsAcct.Set_Fields("Title2Abbrev", "Int");
			rsAcct.Set_Fields("Account2", "");
			rsAcct.Set_Fields("DefaultAmount2", 0);
			rsAcct.Set_Fields("Title3", "Discount");
			rsAcct.Set_Fields("Title3Abbrev", "Disc");
			rsAcct.Set_Fields("Account3", "");
			rsAcct.Set_Fields("DefaultAmount3", 0);
			rsAcct.Set_Fields("Title4", "Costs");
			rsAcct.Set_Fields("Title4Abbrev", "Costs");
			rsAcct.Set_Fields("Account4", "");
			rsAcct.Set_Fields("DefaultAmount4", 0);
			rsAcct.Set_Fields("Title5", "Tax");
			rsAcct.Set_Fields("Title5Abbrev", "Tax");
			rsAcct.Set_Fields("Account5", "");
			rsAcct.Set_Fields("DefaultAmount5", 0);
			rsAcct.Set_Fields("Title6", "Abatement");
			rsAcct.Set_Fields("Title6Abbrev", "Abate");
			rsAcct.Set_Fields("Account6", "");
			rsAcct.Set_Fields("DefaultAmount6", 0);
			rsAcct.Set_Fields("Year1", false);
			rsAcct.Set_Fields("Year2", false);
			rsAcct.Set_Fields("Year3", false);
			rsAcct.Set_Fields("Year4", false);
			rsAcct.Set_Fields("Year5", false);
			rsAcct.Set_Fields("Year6", false);
			rsAcct.Set_Fields("PercentageFee1", false);
			rsAcct.Set_Fields("PercentageFee2", false);
			rsAcct.Set_Fields("PercentageFee3", false);
			rsAcct.Set_Fields("PercentageFee4", false);
			rsAcct.Set_Fields("PercentageFee5", false);
			rsAcct.Set_Fields("PercentageFee6", false);
			rsAcct.Set_Fields("DefaultAccount", "");
			rsAcct.Set_Fields("Active", FCConvert.CBool(strService == "W" || strService == "B"));
			rsAcct.Update();
			// 94 - Utility Sewer Payment
			rsAcct.AddNew();
			rsAcct.Set_Fields("TypeCode", 94);
			rsAcct.Set_Fields("TypeTitle", "Utility - Sewer Payment");
			rsAcct.Set_Fields("Title1", "Principal");
			rsAcct.Set_Fields("Title1Abbrev", "Prin");
			rsAcct.Set_Fields("Account1", "");
			rsAcct.Set_Fields("DefaultAmount1", 0);
			rsAcct.Set_Fields("Title2", "Interest");
			rsAcct.Set_Fields("Title2Abbrev", "Int");
			rsAcct.Set_Fields("Account2", "");
			rsAcct.Set_Fields("DefaultAmount2", 0);
			rsAcct.Set_Fields("Title3", "Discount");
			rsAcct.Set_Fields("Title3Abbrev", "Disc");
			rsAcct.Set_Fields("Account3", "");
			rsAcct.Set_Fields("DefaultAmount3", 0);
			rsAcct.Set_Fields("Title4", "Costs");
			rsAcct.Set_Fields("Title4Abbrev", "Costs");
			rsAcct.Set_Fields("Account4", "");
			rsAcct.Set_Fields("DefaultAmount4", 0);
			rsAcct.Set_Fields("Title5", "Tax");
			rsAcct.Set_Fields("Title5Abbrev", "Tax");
			rsAcct.Set_Fields("Account5", "");
			rsAcct.Set_Fields("DefaultAmount5", 0);
			rsAcct.Set_Fields("Title6", "Abatement");
			rsAcct.Set_Fields("Title6Abbrev", "Abate");
			rsAcct.Set_Fields("Account6", "");
			rsAcct.Set_Fields("DefaultAmount6", 0);
			rsAcct.Set_Fields("Year1", false);
			rsAcct.Set_Fields("Year2", false);
			rsAcct.Set_Fields("Year3", false);
			rsAcct.Set_Fields("Year4", false);
			rsAcct.Set_Fields("Year5", false);
			rsAcct.Set_Fields("Year6", false);
			rsAcct.Set_Fields("PercentageFee1", false);
			rsAcct.Set_Fields("PercentageFee2", false);
			rsAcct.Set_Fields("PercentageFee3", false);
			rsAcct.Set_Fields("PercentageFee4", false);
			rsAcct.Set_Fields("PercentageFee5", false);
			rsAcct.Set_Fields("PercentageFee6", false);
			rsAcct.Set_Fields("DefaultAccount", "");
			rsAcct.Set_Fields("Active", FCConvert.CBool(strService == "S" || strService == "B"));
			rsAcct.Update();
			// 95 - Utility Sewer Lien
			rsAcct.AddNew();
			rsAcct.Set_Fields("TypeCode", 95);
			rsAcct.Set_Fields("TypeTitle", "Utility - Sewer Lien");
			rsAcct.Set_Fields("Title1", "Principal");
			rsAcct.Set_Fields("Title1Abbrev", "Prin");
			rsAcct.Set_Fields("Account1", "");
			rsAcct.Set_Fields("DefaultAmount1", 0);
			rsAcct.Set_Fields("Title2", "Interest");
			rsAcct.Set_Fields("Title2Abbrev", "Int");
			rsAcct.Set_Fields("Account2", "");
			rsAcct.Set_Fields("DefaultAmount2", 0);
			rsAcct.Set_Fields("Title3", "Lien Interest");
			rsAcct.Set_Fields("Title3Abbrev", "Lien Int");
			rsAcct.Set_Fields("Account3", "");
			rsAcct.Set_Fields("DefaultAmount3", 0);
			rsAcct.Set_Fields("Title4", "Costs");
			rsAcct.Set_Fields("Title4Abbrev", "Costs");
			rsAcct.Set_Fields("Account4", "");
			rsAcct.Set_Fields("DefaultAmount4", 0);
			rsAcct.Set_Fields("Title5", "Tax");
			rsAcct.Set_Fields("Title5Abbrev", "Tax");
			rsAcct.Set_Fields("Account5", "");
			rsAcct.Set_Fields("DefaultAmount5", 0);
			rsAcct.Set_Fields("Title6", "Abatement");
			rsAcct.Set_Fields("Title6Abbrev", "Abate");
			rsAcct.Set_Fields("Account6", "");
			rsAcct.Set_Fields("DefaultAmount6", 0);
			rsAcct.Set_Fields("Year1", false);
			rsAcct.Set_Fields("Year2", false);
			rsAcct.Set_Fields("Year3", false);
			rsAcct.Set_Fields("Year4", false);
			rsAcct.Set_Fields("Year5", false);
			rsAcct.Set_Fields("Year6", false);
			rsAcct.Set_Fields("PercentageFee1", false);
			rsAcct.Set_Fields("PercentageFee2", false);
			rsAcct.Set_Fields("PercentageFee3", false);
			rsAcct.Set_Fields("PercentageFee4", false);
			rsAcct.Set_Fields("PercentageFee5", false);
			rsAcct.Set_Fields("PercentageFee6", false);
			rsAcct.Set_Fields("DefaultAccount", "");
			rsAcct.Set_Fields("Active", FCConvert.CBool(strService == "S" || strService == "B"));
			rsAcct.Update();
			// 96 - Utility Water Lien
			rsAcct.AddNew();
			rsAcct.Set_Fields("TypeCode", 96);
			rsAcct.Set_Fields("TypeTitle", "Utility - Water Lien");
			rsAcct.Set_Fields("Title1", "Principal");
			rsAcct.Set_Fields("Title1Abbrev", "Prin");
			rsAcct.Set_Fields("Account1", "");
			rsAcct.Set_Fields("DefaultAmount1", 0);
			rsAcct.Set_Fields("Title2", "Interest");
			rsAcct.Set_Fields("Title2Abbrev", "Int");
			rsAcct.Set_Fields("Account2", "");
			rsAcct.Set_Fields("DefaultAmount2", 0);
			rsAcct.Set_Fields("Title3", "Lien Interest");
			rsAcct.Set_Fields("Title3Abbrev", "Lien Int");
			rsAcct.Set_Fields("Account3", "");
			rsAcct.Set_Fields("DefaultAmount3", 0);
			rsAcct.Set_Fields("Title4", "Costs");
			rsAcct.Set_Fields("Title4Abbrev", "Costs");
			rsAcct.Set_Fields("Account4", "");
			rsAcct.Set_Fields("DefaultAmount4", 0);
			rsAcct.Set_Fields("Title5", "Tax");
			rsAcct.Set_Fields("Title5Abbrev", "Tax");
			rsAcct.Set_Fields("Account5", "");
			rsAcct.Set_Fields("DefaultAmount5", 0);
			rsAcct.Set_Fields("Title6", "Abatement");
			rsAcct.Set_Fields("Title6Abbrev", "Abate");
			rsAcct.Set_Fields("Account6", "");
			rsAcct.Set_Fields("DefaultAmount6", 0);
			rsAcct.Set_Fields("Year1", false);
			rsAcct.Set_Fields("Year2", false);
			rsAcct.Set_Fields("Year3", false);
			rsAcct.Set_Fields("Year4", false);
			rsAcct.Set_Fields("Year5", false);
			rsAcct.Set_Fields("Year6", false);
			rsAcct.Set_Fields("PercentageFee1", false);
			rsAcct.Set_Fields("PercentageFee2", false);
			rsAcct.Set_Fields("PercentageFee3", false);
			rsAcct.Set_Fields("PercentageFee4", false);
			rsAcct.Set_Fields("PercentageFee5", false);
			rsAcct.Set_Fields("PercentageFee6", false);
			rsAcct.Set_Fields("DefaultAccount", "");
			rsAcct.Set_Fields("Active", FCConvert.CBool(strService == "W" || strService == "B"));
			rsAcct.Update();
			// 893 - Tax Acquired Water Payment
			rsAcct.AddNew();
			rsAcct.Set_Fields("TypeCode", 893);
			rsAcct.Set_Fields("TypeTitle", "Tax Acquired Water Payment");
			rsAcct.Set_Fields("Title1", "Principal");
			rsAcct.Set_Fields("Title1Abbrev", "Prin");
			rsAcct.Set_Fields("Account1", "");
			rsAcct.Set_Fields("DefaultAmount1", 0);
			rsAcct.Set_Fields("Title2", "Interest");
			rsAcct.Set_Fields("Title2Abbrev", "Int");
			rsAcct.Set_Fields("Account2", "");
			rsAcct.Set_Fields("DefaultAmount2", 0);
			rsAcct.Set_Fields("Title3", "Discount");
			rsAcct.Set_Fields("Title3Abbrev", "Disc");
			rsAcct.Set_Fields("Account3", "");
			rsAcct.Set_Fields("DefaultAmount3", 0);
			rsAcct.Set_Fields("Title4", "Costs");
			rsAcct.Set_Fields("Title4Abbrev", "Costs");
			rsAcct.Set_Fields("Account4", "");
			rsAcct.Set_Fields("DefaultAmount4", 0);
			rsAcct.Set_Fields("Title5", "Tax");
			rsAcct.Set_Fields("Title5Abbrev", "Tax");
			rsAcct.Set_Fields("Account5", "");
			rsAcct.Set_Fields("DefaultAmount5", 0);
			rsAcct.Set_Fields("Title6", "Abatement");
			rsAcct.Set_Fields("Title6Abbrev", "Abate");
			rsAcct.Set_Fields("Account6", "");
			rsAcct.Set_Fields("DefaultAmount6", 0);
			rsAcct.Set_Fields("Year1", false);
			rsAcct.Set_Fields("Year2", false);
			rsAcct.Set_Fields("Year3", false);
			rsAcct.Set_Fields("Year4", false);
			rsAcct.Set_Fields("Year5", false);
			rsAcct.Set_Fields("Year6", false);
			rsAcct.Set_Fields("PercentageFee1", false);
			rsAcct.Set_Fields("PercentageFee2", false);
			rsAcct.Set_Fields("PercentageFee3", false);
			rsAcct.Set_Fields("PercentageFee4", false);
			rsAcct.Set_Fields("PercentageFee5", false);
			rsAcct.Set_Fields("PercentageFee6", false);
			rsAcct.Set_Fields("DefaultAccount", "");
			rsAcct.Set_Fields("Active", FCConvert.CBool(strService == "W" || strService == "B"));
			rsAcct.Update();
			// 894 - Tax Acquired Sewer Payment
			rsAcct.AddNew();
			rsAcct.Set_Fields("TypeCode", 894);
			rsAcct.Set_Fields("TypeTitle", "Tax Acquired Sewer Payment");
			rsAcct.Set_Fields("Title1", "Principal");
			rsAcct.Set_Fields("Title1Abbrev", "Prin");
			rsAcct.Set_Fields("Account1", "");
			rsAcct.Set_Fields("DefaultAmount1", 0);
			rsAcct.Set_Fields("Title2", "Interest");
			rsAcct.Set_Fields("Title2Abbrev", "Int");
			rsAcct.Set_Fields("Account2", "");
			rsAcct.Set_Fields("DefaultAmount2", 0);
			rsAcct.Set_Fields("Title3", "Discount");
			rsAcct.Set_Fields("Title3Abbrev", "Disc");
			rsAcct.Set_Fields("Account3", "");
			rsAcct.Set_Fields("DefaultAmount3", 0);
			rsAcct.Set_Fields("Title4", "Costs");
			rsAcct.Set_Fields("Title4Abbrev", "Costs");
			rsAcct.Set_Fields("Account4", "");
			rsAcct.Set_Fields("DefaultAmount4", 0);
			rsAcct.Set_Fields("Title5", "Tax");
			rsAcct.Set_Fields("Title5Abbrev", "Tax");
			rsAcct.Set_Fields("Account5", "");
			rsAcct.Set_Fields("DefaultAmount5", 0);
			rsAcct.Set_Fields("Title6", "Abatement");
			rsAcct.Set_Fields("Title6Abbrev", "Abate");
			rsAcct.Set_Fields("Account6", "");
			rsAcct.Set_Fields("DefaultAmount6", 0);
			rsAcct.Set_Fields("Year1", false);
			rsAcct.Set_Fields("Year2", false);
			rsAcct.Set_Fields("Year3", false);
			rsAcct.Set_Fields("Year4", false);
			rsAcct.Set_Fields("Year5", false);
			rsAcct.Set_Fields("Year6", false);
			rsAcct.Set_Fields("PercentageFee1", false);
			rsAcct.Set_Fields("PercentageFee2", false);
			rsAcct.Set_Fields("PercentageFee3", false);
			rsAcct.Set_Fields("PercentageFee4", false);
			rsAcct.Set_Fields("PercentageFee5", false);
			rsAcct.Set_Fields("PercentageFee6", false);
			rsAcct.Set_Fields("DefaultAccount", "");
			rsAcct.Set_Fields("Active", FCConvert.CBool(strService == "S" || strService == "B"));
			rsAcct.Update();
			// 895 - Tax Acquired Sewer Lien Payment
			rsAcct.AddNew();
			rsAcct.Set_Fields("TypeCode", 895);
			rsAcct.Set_Fields("TypeTitle", "Tax Acquired Sewer Lien Payment");
			rsAcct.Set_Fields("Title1", "Principal");
			rsAcct.Set_Fields("Title1Abbrev", "Prin");
			rsAcct.Set_Fields("Account1", "");
			rsAcct.Set_Fields("DefaultAmount1", 0);
			rsAcct.Set_Fields("Title2", "Interest");
			rsAcct.Set_Fields("Title2Abbrev", "Int");
			rsAcct.Set_Fields("Account2", "");
			rsAcct.Set_Fields("DefaultAmount2", 0);
			rsAcct.Set_Fields("Title3", "Lien Interest");
			rsAcct.Set_Fields("Title3Abbrev", "Lien Int");
			rsAcct.Set_Fields("Account3", "");
			rsAcct.Set_Fields("DefaultAmount3", 0);
			rsAcct.Set_Fields("Title4", "Costs");
			rsAcct.Set_Fields("Title4Abbrev", "Costs");
			rsAcct.Set_Fields("Account4", "");
			rsAcct.Set_Fields("DefaultAmount4", 0);
			rsAcct.Set_Fields("Title5", "Tax");
			rsAcct.Set_Fields("Title5Abbrev", "Tax");
			rsAcct.Set_Fields("Account5", "");
			rsAcct.Set_Fields("DefaultAmount5", 0);
			rsAcct.Set_Fields("Title6", "Abatement");
			rsAcct.Set_Fields("Title6Abbrev", "Abate");
			rsAcct.Set_Fields("Account6", "");
			rsAcct.Set_Fields("DefaultAmount6", 0);
			rsAcct.Set_Fields("Year1", false);
			rsAcct.Set_Fields("Year2", false);
			rsAcct.Set_Fields("Year3", false);
			rsAcct.Set_Fields("Year4", false);
			rsAcct.Set_Fields("Year5", false);
			rsAcct.Set_Fields("Year6", false);
			rsAcct.Set_Fields("PercentageFee1", false);
			rsAcct.Set_Fields("PercentageFee2", false);
			rsAcct.Set_Fields("PercentageFee3", false);
			rsAcct.Set_Fields("PercentageFee4", false);
			rsAcct.Set_Fields("PercentageFee5", false);
			rsAcct.Set_Fields("PercentageFee6", false);
			rsAcct.Set_Fields("DefaultAccount", "");
			rsAcct.Set_Fields("Active", FCConvert.CBool(strService == "S" || strService == "B"));
			rsAcct.Update();
			// 896 - Tax Acquired Water Lien Payment
			rsAcct.AddNew();
			rsAcct.Set_Fields("TypeCode", 896);
			rsAcct.Set_Fields("TypeTitle", "Tax Acquired Water Lien Payment");
			rsAcct.Set_Fields("Title1", "Principal");
			rsAcct.Set_Fields("Title1Abbrev", "Prin");
			rsAcct.Set_Fields("Account1", "");
			rsAcct.Set_Fields("DefaultAmount1", 0);
			rsAcct.Set_Fields("Title2", "Interest");
			rsAcct.Set_Fields("Title2Abbrev", "Int");
			rsAcct.Set_Fields("Account2", "");
			rsAcct.Set_Fields("DefaultAmount2", 0);
			rsAcct.Set_Fields("Title3", "Lien Interest");
			rsAcct.Set_Fields("Title3Abbrev", "Lien Int");
			rsAcct.Set_Fields("Account3", "");
			rsAcct.Set_Fields("DefaultAmount3", 0);
			rsAcct.Set_Fields("Title4", "Costs");
			rsAcct.Set_Fields("Title4Abbrev", "Costs");
			rsAcct.Set_Fields("Account4", "");
			rsAcct.Set_Fields("DefaultAmount4", 0);
			rsAcct.Set_Fields("Title5", "Tax");
			rsAcct.Set_Fields("Title5Abbrev", "Tax");
			rsAcct.Set_Fields("Account5", "");
			rsAcct.Set_Fields("DefaultAmount5", 0);
			rsAcct.Set_Fields("Title6", "Abatement");
			rsAcct.Set_Fields("Title6Abbrev", "Abate");
			rsAcct.Set_Fields("Account6", "");
			rsAcct.Set_Fields("DefaultAmount6", 0);
			rsAcct.Set_Fields("Year1", false);
			rsAcct.Set_Fields("Year2", false);
			rsAcct.Set_Fields("Year3", false);
			rsAcct.Set_Fields("Year4", false);
			rsAcct.Set_Fields("Year5", false);
			rsAcct.Set_Fields("Year6", false);
			rsAcct.Set_Fields("PercentageFee1", false);
			rsAcct.Set_Fields("PercentageFee2", false);
			rsAcct.Set_Fields("PercentageFee3", false);
			rsAcct.Set_Fields("PercentageFee4", false);
			rsAcct.Set_Fields("PercentageFee5", false);
			rsAcct.Set_Fields("PercentageFee6", false);
			rsAcct.Set_Fields("DefaultAccount", "");
			rsAcct.Set_Fields("Active", FCConvert.CBool(strService == "W" || strService == "B"));
			rsAcct.Update();
		}

		public static string RemoveSpecialCharacters(string strPass, bool blnRemoveSpaces)
		{
			string RemoveSpecialCharacters = "";
			// MAL@20080731: Checks for characters in string such as "." and "'"
			string strResult;
			strResult = strPass;
			// Commas
			strResult = strResult.Replace(",", "");
			// Periods
			strResult = strResult.Replace(".", "");
			// Apostrophes
			strResult = strResult.Replace("'", "");
			// Back Slash
			strResult = strResult.Replace("\\", "");
			// Forward Slash
			strResult = strResult.Replace("/", "");
			if (blnRemoveSpaces)
			{
				strResult = strResult.Replace(" ", "");
			}
			RemoveSpecialCharacters = strResult;
			return RemoveSpecialCharacters;
		}

		private static void RemoveExtraSpacesinPaymentRecords()
		{
			// Tracker Reference: 15469
			clsDRWrapper rsPymt = new clsDRWrapper();
			rsPymt.OpenRecordset("SELECT * FROM PaymentRec", modExtraModules.strUTDatabase);
			if (rsPymt.RecordCount() > 0)
			{
				rsPymt.MoveFirst();
				while (!rsPymt.EndOfFile())
				{
					rsPymt.Edit();
					rsPymt.Set_Fields("Reference", Strings.Trim(FCConvert.ToString(rsPymt.Get_Fields_String("Reference"))));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					rsPymt.Set_Fields("Code", Strings.Trim(FCConvert.ToString(rsPymt.Get_Fields("Code"))));
					rsPymt.Update();
					rsPymt.MoveNext();
				}
			}
		}

		public static void AutoSizeComboBoxDropDown(FCComboBox cmb)
		{
			// MAL@20081016: Get largest width necessary for combobox
			// Tracker Reference: 15615
			// Original Code from www.vbrad.com/article.aspx?id=9
			// Parameters: cmb - ComboBox control/object to perform the Autosize on
			float CurrentEntryWidth = 0;
			float PixelLength = 0;
			int x;
			Font oFormFont;
			int iScaleMode;
			// find the longest string in the list portion of the combobox
			//FC:FINAL:MSH - Issue #928: parent of comboBox is Panel in our case, so cmb.Parent as BaseForm will be null.
			// We must find and save form of comboBox and work with it.
			BaseForm parentForm = cmb.FindForm() as BaseForm;
			// temporarily set the form font to the combo box font
			// First cache the font
			oFormFont = parentForm.Font;
			// now set the combo box font to the form font
			parentForm.Font = cmb.Font;
			// temporarily change the ScaleMode of the form to Pixel
			// first cache the ScaleMode
			iScaleMode = FCConvert.ToInt32(parentForm.ScaleMode);
			// now set the ScaleMode to Pixel
			parentForm.ScaleMode = ScaleModeConstants.vbPixels;
			// find out the length in pixels of the longest string in the combo box
			for (x = 0; x <= cmb.Items.Count - 1; x++)
			{
				CurrentEntryWidth = parentForm.TextWidth(cmb.Items[x].ToString());
				if (CurrentEntryWidth > PixelLength)
				{
					PixelLength = CurrentEntryWidth;
				}
			}
			// then add 10 pixels for a good measure (actually, to account for combobox margins)
			SetComboBoxDropDownWidth_8(cmb.Handle.ToInt32(), FCConvert.ToInt32(PixelLength + 10));
			// reset the ScaleMode to its original value
			parentForm.ScaleMode = (ScaleModeConstants)iScaleMode;
			// reset to the original form font
			parentForm.Font = oFormFont;
		}

		public static void SetComboBoxDropDownWidth_8(int hwnd, int WidthPx)
		{
			SetComboBoxDropDownWidth(ref hwnd, ref WidthPx);
		}

		public static void SetComboBoxDropDownWidth(ref int hwnd, ref int WidthPx)
		{
			// Parameters: hWnd - handle to the ComboBox (gotten through ComboBox.hWnd)
			// WidthPx - width of the list portion of the combo box in pixels
			modAPIsConst.SendMessageByNum(hwnd, modAPIsConst.CB_SETDROPPEDWIDTH, FCConvert.ToInt16(WidthPx), 0);
		}

		private static void ClearOldReplacementConsumption()
		{
			clsDRWrapper rsMeter = new clsDRWrapper();
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsCheck = new clsDRWrapper();
			rsCheck.OpenRecordset("SELECT * FROM UtilityBilling", "TWUT0000.vb1");
			if (rsCheck.EndOfFile() != true && rsCheck.BeginningOfFile() != true)
			{
				// do nothing
			}
			else
			{
				return;
			}
			//FC:FINAL:DSE Resolve compiler errors
			//if (rsCheck.Get_Fields_DateTime("RanClearOldReplacementConsumption") != true)
			if (rsCheck.Get_Fields_DateTime("RanClearOldReplacementConsumption").ToOADate() != -1)
			{
				rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE ReplacementConsumption <> 0");
				if (rsMeter.EndOfFile() != true && rsMeter.BeginningOfFile() != true)
				{
					do
					{
						rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsMeter.Get_Fields_Int32("ID") + "AND BillStatus = 'B' ORDER BY BillDate DESC");
						if (rsBill.EndOfFile() != true && rsBill.BeginningOfFile() != true)
						{
							if (rsBill.Get_Fields_DateTime("BillDate") > rsMeter.Get_Fields_DateTime("ReplacementDate"))
							{
								rsMeter.Edit();
								rsMeter.Set_Fields("ReplacementConsumption", 0);
								rsMeter.Update();
							}
						}
						rsMeter.MoveNext();
					}
					while (rsMeter.EndOfFile() != true);
				}
				rsCheck.Edit();
				rsCheck.Set_Fields("RanClearOldReplacementConsumption", true);
				rsCheck.Update();
			}
		}

		private static void DefaultRateKeyDueDate()
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM RateKeys", "twut0000.vb1");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
					if (!Information.IsDate(rs.Get_Fields("DueDate")))
					{
						rs.Edit();
						rs.Set_Fields("DueDate", DateAndTime.DateAdd("d", -1, (DateTime)rs.Get_Fields_DateTime("IntStart")));
						rs.Update();
					}
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}
		// vbPorter upgrade warning: 'Return' As int	OnWrite(string, short)
		public static int DetermineOldestRateKey(ref string strRateKeyList)
		{
			int DetermineOldestRateKey = 0;
			string[] strArr = null;
			int intCT;
			clsDRWrapper rsRateKeys = new clsDRWrapper();
			DateTime datOldest;
			datOldest = DateTime.Today;
			if (strRateKeyList.Length > 0)
			{
				rsRateKeys.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
				strArr = Strings.Split(Strings.Mid(Strings.Trim(strRateKeyList), 2, Strings.Trim(strRateKeyList).Length - 2), ",", -1, CompareConstants.vbBinaryCompare);
				DetermineOldestRateKey = FCConvert.ToInt32(strArr[0]);
				if (rsRateKeys.FindFirstRecord("ID", strArr[0]))
				{
					datOldest = (DateTime)rsRateKeys.Get_Fields_DateTime("BillDate");
				}
				for (intCT = 0; intCT <= Information.UBound(strArr, 1); intCT++)
				{
					if (rsRateKeys.FindFirstRecord("ID", strArr[intCT]))
					{
						if (datOldest.ToOADate() > rsRateKeys.Get_Fields_DateTime("BillDate").ToOADate())
						{
							DetermineOldestRateKey = FCConvert.ToInt32(strArr[intCT]);
							datOldest = (DateTime)rsRateKeys.Get_Fields_DateTime("BillDate");
						}
					}
				}
			}
			else
			{
				DetermineOldestRateKey = -1;
			}
			return DetermineOldestRateKey;
		}
		// trout-718 08-02-2011 kgk  After adding the SBillToOwner and WBillToOwner fields to the
		// account Master table, set the values from the default settings
		private static void InitializeBillToField(ref string strService)
		{
			clsDRWrapper rsMst = new clsDRWrapper();
			clsDRWrapper rsUT = new clsDRWrapper();
			bool boolBillOwner = false;
			rsUT.OpenRecordset("SELECT * FROM UtilityBilling", "twut0000.vb1");
			if (!rsUT.EndOfFile())
			{
				boolBillOwner = FCConvert.ToBoolean(rsUT.Get_Fields(strService + "BillToOwner"));
			}
			else
			{
				boolBillOwner = false;
			}
			rsMst.OpenRecordset("SELECT * FROM Master", "twut0000.vb1");
			while (!rsMst.EndOfFile())
			{
				rsMst.Edit();
				rsMst.Set_Fields(strService + "BillToOwner", boolBillOwner);
				rsMst.Update();
				rsMst.MoveNext();
			}
		}

		private static void CheckDatabaseStructure()
		{
			cUtilityBillingDB utDB = new cUtilityBillingDB();
			if (!utDB.CheckVersion())
			{
				cVersionInfo tVer;
				tVer = utDB.GetVersion();
				MessageBox.Show("Error checking database." + "\r\n" + tVer.VersionString);
			}
			cSystemSettings sysSet = new cSystemSettings();
			sysSet.CheckVersion();
		}

		public static double Round(double value, int numdecimalplaces = 0)
		{
			return FCUtils.Round(value, numdecimalplaces);
		}
	}
}
