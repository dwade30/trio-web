﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptBillNonTransferReport.
	/// </summary>
	public partial class rptBillNonTransferReport : BaseSectionReport
	{
		public rptBillNonTransferReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Created Bills Report";
		}

		public static rptBillNonTransferReport InstancePtr
		{
			get
			{
				return (rptBillNonTransferReport)Sys.GetInstance(typeof(rptBillNonTransferReport));
			}
		}

		protected rptBillNonTransferReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillNonTransferReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/31/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngBillCount;
		int lngBook;
		// vbPorter upgrade warning: lngRKey As int	OnWrite(string)
		int lngRKey;
		double[] dblTotalSumAmount = new double[6 + 1];
		// vbPorter upgrade warning: dtIntDate As DateTime	OnWrite(string)
		DateTime dtIntDate;
		// vbPorter upgrade warning: lngOrder As int	OnWrite(string)
		int lngOrder;
		string strBook = "";
		// vbPorter upgrade warning: strData As string()	OnRead(DateTime, int, string)
		string[] strData = null;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				strData = Strings.Split(FCConvert.ToString(this.UserData), "|", -1, CompareConstants.vbTextCompare);
				dtIntDate = FCConvert.ToDateTime(strData[2]);
				lngRKey = FCConvert.ToInt32(strData[1]);
				lngOrder = FCConvert.ToInt32(strData[3]);
				if (lngOrder == 0)
				{
					lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(strData[0])));
					lblReportType.Text = "Rate Key = " + FCConvert.ToString(lngRKey) + "\r\n" + "Book = " + FCConvert.ToString(lngBook);
				}
				else
				{
					strBook = strData[0];
					lblReportType.Text = "Rate Key = " + FCConvert.ToString(lngRKey) + "\r\n" + "Book = " + strBook;
				}
				// force this report to be landscape
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				;
				// this loads the data
				if (LoadAccounts())
				{
					// Continue
				}
				else
				{
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Sub Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (lngOrder == 0)
			{
				FillTotals();
			}
			else
			{
				GroupFooter1.Visible = false;
			}
		}

		private bool LoadAccounts()
		{
			bool LoadAccounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				string strFields;
				string strFieldsNoBill;
				string strSeq = "";
				strFields =
					"AccountNumber, DeedName1 AS Name, DeedName1 AS OwnerName, TotalWBillAmount, TotalSBillAmount, Bill.Book, Sequence, Consumption, WFlatAmount, WUnitsAmount, WConsumptionAmount, WAdjustAmount, SAdjustAmount, WDEAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount, WMiscAmount, SMiscAmount, WTax, STax, Bill.AccountKey AS AcctKey, (WPrinPaid + WTaxPaid + WIntPaid + WCostPaid + SPrinPaid + STaxPaid + SIntPaid + SCostPaid) AS TotPaid, WHasOverride, SHasOverride, Bill.BillStatus, Master.Deleted, MeterTable.ID AS MeterKey, Bill.NoBill, Bill.Final, Master.MapLot, Master.StreetName, Master.StreetNumber, Bill.ID AS BillKey ";
				strFieldsNoBill =
					"AccountNumber, DeedName1 AS Name, DeedName1 AS OwnerName, 0 AS TotalWBillAmount, 0 AS TotalSBillAmount, MeterTable.BookNumber, Sequence, 0 AS Consumption, 0 AS WFlatAmount, 0 AS WUnitsAmount, 0 AS WConsumptionAmount, 0 AS WAdjustAmount, 0 AS SAdjustAmount, 0 AS WDEAdjustAmount, 0 AS SDEAdjustAmount, 0 AS SFlatAmount, 0 AS SUnitsAmount, 0 AS SConsumptionAmount, 0 AS WMiscAmount, 0 AS SMiscAmount, 0 AS WTax, 0 AS STax, MeterTable.AccountKey AS AcctKey, 0 AS TotPaid, 0 AS WHasOverride, 0 AS SHasOverride, MeterTable.BillingStatus, Master.Deleted, MeterTable.ID AS MeterKey, MeterTable.NoBill, MeterTable.Final, Master.MapLot, Master.StreetName, Master.StreetNumber, 0 AS BillKey ";
				switch (lngOrder)
				{
					case 0:
						{
							// Book/Seq
							strSeq = " ORDER BY Bill.Book, MeterTable.Sequence, AccountNumber";
							break;
						}
					case 1:
						{
							// Account Number
							strSeq = " ORDER BY AccountNumber";
							break;
						}
					case 2:
						{
							// Name
							strSeq = " ORDER BY Name";
							break;
						}
					case 3:
						{
							// Location
							strSeq = " ORDER BY StreetName, StreetNumber";
							break;
						}
					case 4:
						{
							// Map Lot
							strSeq = " ORDER BY Master.MapLot";
							break;
						}
				}
				//end switch
				if (lngOrder == 0)
				{
					rsData.OpenRecordset("SELECT " + strFields + " FROM (Bill LEFT OUTER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pB ON Master.BillingPartyID = pB.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pO ON Master.OwnerPartyID = pO.ID WHERE BillingRateKey = " + FCConvert.ToString(lngRKey) + " AND Bill.Book = " + lngBook + " AND (Bill.BillStatus <> 'B' OR TotalWBillAmount + TotalSBillAmount = 0) " +
					                     "UNION ALL " +
					                     "SELECT " + strFieldsNoBill + " FROM (MeterTable LEFT OUTER JOIN Bill ON Bill.MeterKey = MeterTable.ID AND (Bill.BillingRateKey = " + FCConvert.ToString(lngRKey) + " OR Bill.BillingRateKey = 0)) INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pB ON Master.BillingPartyID = pB.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pO ON Master.OwnerPartyID = pO.ID WHERE MeterTable.BookNumber = " + lngBook + " AND ISNULL(ActualAccountNumber,'0') = '0' AND Combine = 'N' " +
					                     "ORDER BY MeterTable.Sequence, AccountNumber", modExtraModules.strUTDatabase);


					//XXXX DELETE ME XXXX rsData.OpenRecordset("SELECT " + strFields + " FROM (Bill LEFT OUTER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pB ON Master.BillingPartyID = pB.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pO ON Master.OwnerPartyID = pO.ID WHERE BillingRateKey = " + FCConvert.ToString(lngRKey) + " AND Bill.Book = " + FCConvert.ToString(lngBook) + " AND (Bill.BillStatus <> 'B' OR TotalWBillAmount + TotalSBillAmount = 0) ORDER BY MeterTable.Sequence, ActualAccountNumber", modExtraModules.strUTDatabase);
				}
				else
				{
					rsData.OpenRecordset("SELECT " + strFields + " FROM (Bill LEFT OUTER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pB ON Master.BillingPartyID = pB.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pO ON Master.OwnerPartyID = pO.ID WHERE BillingRateKey = " + FCConvert.ToString( lngRKey) + " AND Bill.Book IN (" + strBook + ") AND Combine = 'N' AND (Bill.BillStatus <> 'B' OR TotalWBillAmount + TotalSBillAmount = 0) " +
					                     "UNION ALL " +
					                     "SELECT " + strFieldsNoBill + " FROM (MeterTable LEFT OUTER JOIN Bill ON Bill.MeterKey = MeterTable.ID AND (Bill.BillingRateKey = " + FCConvert.ToString(lngRKey) + " OR Bill.BillingRateKey = 0)) INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pB ON Master.BillingPartyID = pB.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pO ON Master.OwnerPartyID = pO.ID WHERE MeterTable.BookNumber IN (" + strBook + ") AND ISNULL(ActualAccountNumber,'0') = '0' AND Combine = 'N' " + 
					                     strSeq, modExtraModules.strUTDatabase);


					//XXXX DELETE ME XXXX rsData.OpenRecordset("SELECT " + strFields + " FROM (Bill LEFT OUTER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pB ON Master.BillingPartyID = pB.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pO ON Master.OwnerPartyID = pO.ID WHERE BillingRateKey = " + FCConvert.ToString(lngRKey) + " AND Bill.Book = " + strBook + " AND Combine = 'N' AND (Bill.BillStatus <> 'B' OR TotalWBillAmount + TotalSBillAmount = 0)" + strSeq, modExtraModules.strUTDatabase);
				}
				if (!rsData.EndOfFile())
				{
					LoadAccounts = true;
					if (lngOrder == 0)
					{
						lblReportType.Text = "Book : " + FCConvert.ToString(lngBook);
					}
					else
					{
						lblReportType.Text = "Books : " + strBook;
					}
				}
				else
				{
					LoadAccounts = false;
				}
				return LoadAccounts;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadAccounts;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblAmount = 0;
				double dblWCurInt = 0;
				double dblWCHGInt = 0;
				double dblSCurInt = 0;
				double dblSCHGInt = 0;
				double dblTotal = 0;
				double dblPaid = 0;
				if (!rsData.EndOfFile())
				{
					dblSCurInt = 0;
					dblSCHGInt = 0;
					dblWCurInt = 0;
					dblWCHGInt = 0;
					dblPaid = 0;
					fldReason.Text = GetNoBillReason();
					// If rsData.Fields("AccountNumber") = 16 Then Stop
					//FC:FINAL:MSH - can't implicitly convert from in to string
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					fldAcctNum.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
					if (modUTCalculations.Statics.gboolShowOwner)
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						fldName.Text = FCConvert.ToString(rsData.Get_Fields("OwnerName"));
					}
					else
					{
						fldName.Text = FCConvert.ToString(rsData.Get_Fields_String("Name"));
					}
					// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
					fldBook.Text = FCConvert.ToString(rsData.Get_Fields("Book"));
					if (FCConvert.CBool(rsData.Get_Fields_Boolean("WHasOverride")) == true || FCConvert.CBool(rsData.Get_Fields_Boolean("SHasOverride")) == true)
					{
						fldBook.Text = "*" + fldBook.Text;
					}
					// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
					fldSeq.Text = FCConvert.ToString(rsData.Get_Fields("Sequence"));
					fldCons.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Consumption"));
					fldRegular.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WUnitsAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WConsumptionAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SFlatAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SUnitsAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SConsumptionAmount")), "#,##0.00");
					fldMisc.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WMiscAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SMiscAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WAdjustAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SAdjustAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("WDEAdjustAmount")) + FCConvert.ToDouble(rsData.Get_Fields_Decimal("SDEAdjustAmount")), "#,##0.00");
					// TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
					fldTax.Text = Strings.Format(FCConvert.ToDouble(rsData.Get_Fields("WTax")) + FCConvert.ToDouble(rsData.Get_Fields("STax")), "#,##0.00");
					// TODO Get_Fields: Field [TotPaid] not found!! (maybe it is an alias?)
					dblPaid = FCConvert.ToDouble(rsData.Get_Fields("TotPaid"));
					// TODO Get_Fields: Field [AcctKey] not found!! (maybe it is an alias?)
					dblTotal = modUTCalculations.CalculateAccountUTTotal_3(rsData.Get_Fields("AcctKey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblWCurInt, ref dblWCHGInt, ref dtIntDate);
					// TODO Get_Fields: Field [AcctKey] not found!! (maybe it is an alias?)
					dblTotal += modUTCalculations.CalculateAccountUTTotal_3(rsData.Get_Fields("AcctKey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblSCurInt, ref dblSCHGInt, ref dtIntDate);
					if (dblTotal != 0)
					{
						fldPastDue.Text = Strings.Format((dblTotal - (dblWCurInt + dblWCHGInt + dblSCurInt + dblSCHGInt) - FCConvert.ToDouble(fldRegular.Text) - FCConvert.ToDouble(fldTax.Text) - FCConvert.ToDouble(fldMisc.Text)) + dblPaid, "#,##0.00");
						// remove the totals from this bill and the interest amounts
						fldInterest.Text = Strings.Format(dblWCurInt + dblWCHGInt + dblSCurInt + dblSCHGInt, "#,##0.00");
						// If fldInterest.Text < 0 Then
						// MsgBox "Stop"
						// End If
					}
					else
					{
						fldPastDue.Text = "0.00";
						fldInterest.Text = "0.00";
					}
					// dblAmount = rsData.Fields("TotalWBillAmount") + rsData.Fields("TotalSBillAmount")   'get the total bill amount
					dblAmount = FCConvert.ToDouble(fldRegular.Text) + FCConvert.ToDouble(fldTax.Text) + FCConvert.ToDouble(fldMisc.Text) + FCConvert.ToDouble(fldPastDue.Text) + FCConvert.ToDouble(fldInterest.Text);
					fldAmount.Text = Strings.Format(dblAmount, "#,##0.00");
					// fill the local (group) totals
					dblTotalSumAmount[0] += Conversion.Val(fldCons.Text);
					dblTotalSumAmount[1] += FCConvert.ToDouble(fldRegular.Text);
					dblTotalSumAmount[2] += FCConvert.ToDouble(fldMisc.Text);
					dblTotalSumAmount[3] += FCConvert.ToDouble(fldTax.Text);
					dblTotalSumAmount[4] += FCConvert.ToDouble(fldPastDue.Text);
					dblTotalSumAmount[5] += FCConvert.ToDouble(fldInterest.Text);
					dblTotalSumAmount[6] += dblAmount;
					lngBillCount += 1;
					// update the master totals
					rptBillNonTransferReportMaster.InstancePtr.dblTotalSumAmount0 += Conversion.Val(fldCons.Text);
					rptBillNonTransferReportMaster.InstancePtr.dblTotalSumAmount1 += FCConvert.ToDouble(fldRegular.Text);
					rptBillNonTransferReportMaster.InstancePtr.dblTotalSumAmount2 += FCConvert.ToDouble(fldMisc.Text);
					rptBillNonTransferReportMaster.InstancePtr.dblTotalSumAmount3 += FCConvert.ToDouble(fldTax.Text);
					rptBillNonTransferReportMaster.InstancePtr.dblTotalSumAmount4 += FCConvert.ToDouble(fldPastDue.Text);
					rptBillNonTransferReportMaster.InstancePtr.dblTotalSumAmount5 += FCConvert.ToDouble(fldInterest.Text);
					rptBillNonTransferReportMaster.InstancePtr.dblTotalSumAmount6 += FCConvert.ToDouble(fldAmount.Text);
					rptBillNonTransferReportMaster.InstancePtr.lngBillCount += 1;
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fldTotalCons.Text = Strings.Format(dblTotalSumAmount[0], "#,##0");
				fldTotalRegular.Text = Strings.Format(dblTotalSumAmount[1], "#,##0.00");
				fldTotalMisc.Text = Strings.Format(dblTotalSumAmount[2], "#,##0.00");
				fldTotalTax.Text = Strings.Format(dblTotalSumAmount[3], "#,##0.00");
				fldTotalPastDue.Text = Strings.Format(dblTotalSumAmount[4], "#,##0.00");
				fldTotalInterest.Text = Strings.Format(dblTotalSumAmount[5], "#,##0.00");
				fldTotalAmount.Text = Strings.Format(dblTotalSumAmount[6], "#,##0.00");
				if (lngBillCount == 1)
				{
					fldFooter.Text = "Book: " + FCConvert.ToString(lngBook) + "\r\n" + "1 bill";
				}
				else
				{
					fldFooter.Text = "Book: " + FCConvert.ToString(lngBook) + "\r\n" + FCConvert.ToString(lngBillCount) + " bills";
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Totals", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string GetNoBillReason()
		{
			string GetNoBillReason = "";
			string strResult = "";
			if (rsData.Get_Fields_Boolean("NoBill") == true)
			{
				strResult = "No Bill";
			}
			else if (rsData.Get_Fields_Boolean("Final") == true)
			{
				strResult = "Final Bill";
			}
			else if (fecherFoundation.FCUtils.IsNull(rsData.Get_Fields_Int32("MeterKey")))
			{
				strResult = "No Meter";
			}
			else if (rsData.Get_Fields_Boolean("Deleted") == true)
			{
				strResult = "Deleted";
			}
			else if (AccountIsFinalBill())
			{
				strResult = "Final Bill";
			}
			else if (rsData.Get_Fields_Decimal("TotalWBillAmount") + rsData.Get_Fields_Decimal("TotalSBillAmount") == 0)
			{
				strResult = "Zero Bill";
			}
			else if (AccountIsNoBill())
			{
				strResult = "No Bill";
			}
			else
			{
				strResult = "Unknown";
			}
			GetNoBillReason = strResult;
			return GetNoBillReason;
		}

		private bool AccountIsNoBill()
		{
			bool AccountIsNoBill = false;
			bool blnResult = false;
            using (clsDRWrapper rsAcct = new clsDRWrapper())
            {
                // TODO Get_Fields: Field [AcctKey] not found!! (maybe it is an alias?)
                rsAcct.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsData.Get_Fields("AcctKey"),
                    modExtraModules.strUTDatabase);
                if (rsAcct.RecordCount() > 0)
                {
                    blnResult = FCConvert.ToBoolean(rsAcct.Get_Fields_Boolean("NoBill"));
                }
                else
                {
                    blnResult = false;
                }

                AccountIsNoBill = blnResult;
            }

            return AccountIsNoBill;
		}

		private bool AccountIsFinalBill()
		{
			bool AccountIsFinalBill = false;
			bool blnResult = false;
            using (clsDRWrapper rsAcct = new clsDRWrapper())
            {
                // TODO Get_Fields: Field [AcctKey] not found!! (maybe it is an alias?)
                rsAcct.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsData.Get_Fields("AcctKey"),
                    modExtraModules.strUTDatabase);
                if (rsAcct.RecordCount() > 0)
                {
                    blnResult = FCConvert.ToBoolean(rsAcct.Get_Fields_Boolean("FinalBill"));
                }
                else
                {
                    blnResult = false;
                }

                AccountIsFinalBill = blnResult;
            }

            return AccountIsFinalBill;
		}

		
	}
}
