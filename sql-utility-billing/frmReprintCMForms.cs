﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Diagnostics;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReprintCMForm.
	/// </summary>
	public partial class frmReprintCMForm : BaseForm
	{
		public frmReprintCMForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReprintCMForm InstancePtr
		{
			get
			{
				return (frmReprintCMForm)Sys.GetInstance(typeof(frmReprintCMForm));
			}
		}

		protected frmReprintCMForm _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/02/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/04/2004              *
		// ********************************************************
		int lngType;
		bool boolLoaded;
		int lngGRIDCOLTree;
		int lngGridColAccount;
		int lngGRIDCOLYear;
		int lngGridColName;
		int lngGridColLocation;
		int lngGRIDCOLMailNumber;
		int lngGridColBillKey;
		int lngGRIDCOLHidden;

		public void Init(ref int lngPIT)
		{
			// this will set the type
			lngType = lngPIT;
			Debug.Assert(false);
			// XXXXX This form has collections code, is it used?
			Debug.Assert(true);
			this.Show(App.MainForm);
		}

		private void cmbForm_DropDown(object sender, System.EventArgs e)
		{
			// this will set the width of the dropdown to see the whole string
			modAPIsConst.SendMessageByNum(this.cmbForm.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cmbForm_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						KeyCode = (Keys)0;
						if (modAPIsConst.SendMessageByNum(cmbForm.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbForm.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
						}
						break;
					}
			}
			//end switch
		}

		private void frmReprintCMForm_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
			}
			else
			{
				boolLoaded = true;
				lblInstructions.Text = "Select the type of mailer that this will be printed on and select the accounts that you would like to reprint.  Press F12 to continue.";
				FillFormCombo();
				FillPaymentGrid();
			}
		}

		private void frmReprintCMForm_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmReprintCMForm_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReprintCMForm properties;
			//frmReprintCMForm.FillStyle	= 0;
			//frmReprintCMForm.ScaleWidth	= 9045;
			//frmReprintCMForm.ScaleHeight	= 7305;
			//frmReprintCMForm.LinkTopic	= "Form2";
			//frmReprintCMForm.LockControls	= true;
			//frmReprintCMForm.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lngGRIDCOLTree = 0;
			lngGridColAccount = 1;
			lngGRIDCOLYear = 2;
			lngGridColName = 3;
			lngGridColLocation = 4;
			lngGRIDCOLMailNumber = 5;
			lngGridColBillKey = 6;
			lngGRIDCOLHidden = 7;
		}

		private void frmReprintCMForm_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmReprintCMForm_Resize(object sender, System.EventArgs e)
		{
			FormatPaymentGrid();
		}

		private void mnuFileClearAll_Click(object sender, System.EventArgs e)
		{
			ClearAll();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void FillFormCombo()
		{
			try
			{
				cmbForm.AddItem("Merit Certified Mailer (Laser Format)");
				cmbForm.ItemData(cmbForm.NewIndex, 0);
				cmbForm.SelectedIndex = 0;
				return;
			}
			catch (Exception ex)
			{
				//ERROR_HANDLER: ;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Form Combo Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatPaymentGrid()
		{
			// this will format the payment grid and set the widths of rows, header titles ect
			vsPayments.Cols = 8;
			vsPayments.Rows = 1;
			vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsPayments.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsPayments.Cols - 1, true);
			// .Cell(FCGrid.CellPropertySettings.flexcpFontSize, 0, 0, 0, .Cols - 1) = 12
			vsPayments.ExtendLastCol = true;
			// set the column headers
			vsPayments.TextMatrix(0, lngGridColAccount, "Account");
			vsPayments.TextMatrix(0, lngGRIDCOLYear, "Year");
			vsPayments.TextMatrix(0, lngGridColName, "Name");
			vsPayments.TextMatrix(0, lngGridColLocation, "Location");
			vsPayments.TextMatrix(0, lngGRIDCOLMailNumber, "Mail Number");
			vsPayments.TextMatrix(0, lngGridColBillKey, "");
			vsPayments.TextMatrix(0, lngGRIDCOLHidden, "");
			// alignment and other formatting
			vsPayments.ColAlignment(lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGRIDCOLYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGridColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGridColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGRIDCOLMailNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGridColBillKey, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngGRIDCOLHidden, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsPayments.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsPayments.Cols - 1, true);
			vsPayments.ColDataType(lngGRIDCOLTree, FCGrid.DataTypeSettings.flexDTBoolean);
			// SetPaymentGridHeight
			fraCMF.Visible = true;
		}

		private void SetColWidths()
		{
			int lngWid = 0;
			// set the widths
			lngWid = vsPayments.Width;
			vsPayments.Cols = 8;
			vsPayments.ColWidth(lngGRIDCOLTree, FCConvert.ToInt32(lngWid * 0.04));
			// Check Box
			vsPayments.ColWidth(lngGridColAccount, FCConvert.ToInt32(lngWid * 0.1));
			// Account
			vsPayments.ColWidth(lngGRIDCOLYear, FCConvert.ToInt32(lngWid * 0.1));
			// Year
			vsPayments.ColWidth(lngGridColName, FCConvert.ToInt32(lngWid * 0.22));
			// Name
			vsPayments.ColWidth(lngGridColLocation, FCConvert.ToInt32(lngWid * 0.22));
			// Location
			vsPayments.ColWidth(lngGRIDCOLMailNumber, FCConvert.ToInt32(lngWid * 0.19));
			// Mail Number
			vsPayments.ColWidth(lngGridColBillKey, 0);
			// BillKey
			vsPayments.ColWidth(lngGRIDCOLHidden, 0);
		}

		private void FillPaymentGrid()
		{
			// this will fill the payment grid with the lien payments of the account that was entered
			clsDRWrapper rsPay = new clsDRWrapper();
			string strAccount = "";
			string strSQL;
			int lngEligible = 0;
			switch (lngType)
			{
				case 0:
					{
						// 30 Day Notices
						lngEligible = 2;
						break;
					}
				case 1:
					{
						// Lien Transfer Process
						lngEligible = 4;
						break;
					}
				case 2:
					{
						// Lien Maturity
						lngEligible = 5;
						break;
					}
			}
			//end switch
			// XXXXXX
			strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = " + FCConvert.ToString(lngEligible) + " AND CertifiedMailNumber <> ''";
			vsPayments.Rows = 1;
			rsPay.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			while (!rsPay.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				strAccount = "\t" + rsPay.Get_Fields("Account") + "\t" + rsPay.Get_Fields_Int32("BillNumber") + "\t" + rsPay.Get_Fields("StreetNumber") + rsPay.Get_Fields_String("StreetName") + "\t" + rsPay.Get_Fields_String("CertifiedMailNumber") + "\t" + rsPay.Get_Fields_Int32("BillKey");
				vsPayments.AddItem("");
				// strAccount
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGridColAccount, FCConvert.ToString(rsPay.Get_Fields("Account")));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGRIDCOLYear, FCConvert.ToString(rsPay.Get_Fields_Int32("BillNumber")));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGridColName, FCConvert.ToString(rsPay.Get_Fields_String("Name1")));
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGridColLocation, rsPay.Get_Fields("StreetNumber") + " " + rsPay.Get_Fields_String("StreetName"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGRIDCOLMailNumber, FCConvert.ToString(rsPay.Get_Fields_String("CertifiedMailNumber")));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngGridColBillKey, FCConvert.ToString(rsPay.Get_Fields_Int32("BillKey")));
				rsPay.MoveNext();
			}
			SetPaymentGridHeight();
			fraCMF.Visible = true;
		}

		private void SetPaymentGridHeight()
		{
			// this will adjust the height of the grid in the frame after a resize or a change of the amount of rows in the grid
			int lngFrameHeight;
			int lngMaxHeight;
			lngMaxHeight = fraCMF.Height - 700;
			if ((vsPayments.Rows * vsPayments.RowHeight(0)) + 70 > lngMaxHeight)
			{
				vsPayments.Height = lngMaxHeight;
				vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			}
			else
			{
				vsPayments.Height = (vsPayments.Rows * vsPayments.RowHeight(0)) + 70;
				vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
			SetColWidths();
			vsPayments.Refresh();
		}

		private void SelectAll()
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsPayments.Rows - 1; lngCT++)
			{
				vsPayments.TextMatrix(lngCT, lngGRIDCOLTree, FCConvert.ToString(-1));
			}
		}

		private void ClearAll()
		{
			int lngCT;
			for (lngCT = 1; lngCT <= vsPayments.Rows - 1; lngCT++)
			{
				vsPayments.TextMatrix(lngCT, lngGRIDCOLTree, FCConvert.ToString(0));
			}
		}

		private void mnuFileReprint_Click(object sender, System.EventArgs e)
		{
			ReprintCertifiedMailForms();
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			SelectAll();
		}

		private void ReprintCertifiedMailForms()
		{
			// this function will ask the user which printer to use then show the Certified Mail Forms
			bool boolOK;
			int lngCT;
			string strAccounts;
			string strSQL = "";
			string strPrinterName = "";
			int NumFonts = 0;
			bool boolUseFont = false;
			string strFont = "";
			int intCPI = 0;
			int X;
			// check to see if an account is selected...if one is, then call the report
			boolOK = false;
			strAccounts = "";
			// Find all the accounts that are selected
			for (lngCT = 1; lngCT <= vsPayments.Rows - 1; lngCT++)
			{
				if (Conversion.Val(vsPayments.TextMatrix(lngCT, lngGRIDCOLTree)) == -1)
				{
					boolOK = true;
					strAccounts += vsPayments.TextMatrix(lngCT, lngGridColBillKey) + ", ";
				}
			}
			if (boolOK)
			{
				// if accounts were selected
				/*? On Error Resume Next  */
				//MDIParent.InstancePtr.CommonDialog1.ShowPrinter();
				// show the printer dialog box
				if (Information.Err().Number == 0)
				{
					//strPrinterName = FCGlobal.Printer.DeviceName;
					// set the name of the printer
					NumFonts = FCGlobal.Printer.FontCount;
					// check the number of fonts in the selected printer
					boolUseFont = false;
					intCPI = 10;
					for (X = 0; X <= NumFonts - 1; X++)
					{
						// find the forst font that is 10 CPI
						strFont = FCGlobal.Printer.Fonts[X];
						;
						if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
						{
							strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
							if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
							{
								boolUseFont = true;
								strFont = FCGlobal.Printer.Fonts[X];
								;
								break;
							}
						}
					}
					// X
					if (!boolUseFont)
					{
						strFont = "";
					}
					strAccounts = Strings.Left(strAccounts, strAccounts.Length - 2);
					// get rid of the last space and the last comma
					strSQL = "SELECT * FROM (BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount) WHERE BillKey IN (" + strAccounts + ")";
					// create the SQL string
					// initialize the report
					rptCustomForms.InstancePtr.Init(ref strSQL, "FORMS", cmbForm.SelectedIndex, ref strPrinterName, ref strFont, 20 + lngType, false);
				}
				else
				{
					// no accounts were selected so prompt the user
					MessageBox.Show("Please select an account(s) to reprint.", "No Accounts Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuFileSelectToBottom_Click(object sender, System.EventArgs e)
		{
			// this will select the current row and all of the others to the bottom
			int lngRW;
			for (lngRW = vsPayments.Row; lngRW <= vsPayments.Rows - 1; lngRW++)
			{
				if (vsPayments.Row > 0)
				{
					vsPayments.TextMatrix(lngRW, lngGRIDCOLTree, FCConvert.ToString(-1));
				}
			}
		}

		private void vsPayments_RowColChange(object sender, System.EventArgs e)
		{
			switch (vsPayments.Col)
			{
				case 0:
					{
						vsPayments.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsPayments.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}
	}
}
