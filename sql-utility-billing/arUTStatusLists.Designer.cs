﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arUTStatusLists.
	/// </summary>
	partial class arUTStatusLists
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arUTStatusLists));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFakeHeader1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFakeHeader2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFakeHeader4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFakeHeader5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFakeHeader6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFakeHeader7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFakeHeader8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFakeHeader3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOutstanding = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblGroupHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillNum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLienRecord = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.srptSLAllActivityDetailOB = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.fldTADate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroupPayments = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroupTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroupPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblGroupTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGroupTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroupInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroupCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroupTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRTError = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSpecialTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSpecialText = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblSummary1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSummary1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSummaryHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSummaryPaymentType12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnSubtotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOutstanding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienRecord)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTADate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRTError)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSpecialTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSpecialText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldName,
				this.fldAccount,
				this.fldBillDate,
				this.fldTaxDue,
				this.fldPaymentReceived,
				this.fldDue,
				this.lblLocation,
				this.fldLocation,
				this.lblMapLot,
				this.fldMapLot,
				this.fldPrincipal,
				this.fldTax,
				this.fldInterest,
				this.fldCosts,
				this.fldLienRecord,
				this.fldBook,
				this.srptSLAllActivityDetailOB,
				this.fldTADate,
				this.fldBillNum
			});
			this.Detail.Height = 0.7604167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalTaxDue,
				this.fldTotalPaymentReceived,
				this.fldTotalDue,
				this.lnTotals,
				this.lblTotals,
				this.lblRTError,
				this.lblSpecialTotal,
				this.lblSpecialText,
				this.lblSummary,
				this.Line1,
				this.lblSummary1,
				this.fldSummary1,
				this.lblSummaryHeader,
				this.lblSummaryPaymentType1,
				this.lblSummaryPaymentType2,
				this.lblSummaryPaymentType3,
				this.lblSummaryPaymentType4,
				this.lblSummaryPaymentType5,
				this.lblSummaryPaymentType6,
				this.lblSummaryPaymentType7,
				this.lblSummaryPaymentType8,
				this.lblSummaryPaymentType9,
				this.lblSummaryPaymentType10,
				this.lblSummaryPaymentType11,
				this.lblSummaryTotal1,
				this.lblSummaryTotal2,
				this.lblSummaryTotal3,
				this.lblSummaryTotal4,
				this.lblSummaryTotal5,
				this.lblSummaryTotal6,
				this.lblSummaryTotal7,
				this.lblSummaryTotal8,
				this.lblSummaryTotal9,
				this.lblSummaryTotal10,
				this.lblSummaryTotal11,
				this.lblSumPrin1,
				this.lblSumPrin2,
				this.lblSumPrin3,
				this.lblSumPrin4,
				this.lblSumPrin5,
				this.lblSumPrin6,
				this.lblSumPrin7,
				this.lblSumPrin8,
				this.lblSumPrin9,
				this.lblSumPrin10,
				this.lblSumPrin11,
				this.lblSumInt1,
				this.lblSumInt2,
				this.lblSumInt3,
				this.lblSumInt4,
				this.lblSumInt5,
				this.lblSumInt6,
				this.lblSumInt7,
				this.lblSumInt8,
				this.lblSumInt9,
				this.lblSumInt10,
				this.lblSumInt11,
				this.lblSumCost1,
				this.lblSumCost2,
				this.lblSumCost3,
				this.lblSumCost4,
				this.lblSumCost5,
				this.lblSumCost6,
				this.lblSumCost7,
				this.lblSumCost8,
				this.lblSumCost9,
				this.lblSumCost10,
				this.lblSumCost11,
				this.lblSumHeaderType,
				this.lblSumHeaderTotal,
				this.lblSumHeaderPrin,
				this.lblSumHeaderInt,
				this.lblSumHeaderCost,
				this.lblSumTax1,
				this.lblSumTax2,
				this.lblSumTax3,
				this.lblSumTax4,
				this.lblSumTax5,
				this.lblSumTax6,
				this.lblSumTax7,
				this.lblSumTax8,
				this.lblSumTax9,
				this.lblSumTax10,
				this.lblSumTax11,
				this.lblSumHeaderTax,
				this.lnSummaryTotal,
				this.Line2,
				this.fldTotalPrincipal,
				this.fldTotalTax,
				this.fldTotalInterest,
				this.fldTotalCost,
				this.lblSummaryPaymentType12,
				this.lblSummaryPaymentType13,
				this.lblSummaryTotal12,
				this.lblSummaryTotal13,
				this.lblSumPrin12,
				this.lblSumPrin13,
				this.lblSumInt12,
				this.lblSumInt13,
				this.lblSumCost12,
				this.lblSumCost13,
				this.lblSumTax12,
				this.lblSumTax13,
				this.lnSubtotal
			});
			this.ReportFooter.Height = 7F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblReportType,
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lblFakeHeader1,
				this.lblFakeHeader2,
				this.lblFakeHeader4,
				this.lblFakeHeader5,
				this.lblFakeHeader6,
				this.lblFakeHeader7,
				this.lblFakeHeader8,
				this.lblFakeHeader3
			});
			this.PageHeader.Height = 1F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lnHeader,
				this.lblAccount,
				this.lblBillDate,
				this.lblTaxDue,
				this.lblPaymentReceived,
				this.lblDue,
				this.lblPrincipal,
				this.lblTax,
				this.lblInterest,
				this.lblCost,
				this.lblOutstanding,
				this.lblGroupHeader,
				this.Binder,
				this.lblBook,
				this.lblBillNum
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.5625F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldGroupPayments,
				this.fldGroupTotal,
				this.fldGroupPrincipal,
				this.lblGroupTotals,
				this.fldGroupTax,
				this.fldGroupInterest,
				this.fldGroupCost,
				this.fldGroupTaxDue,
				this.Line3
			});
			this.GroupFooter1.Height = 0.3020833F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.8125F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblReportType.Text = "Report Type";
			this.lblReportType.Top = 0.1875F;
			this.lblReportType.Width = 10.125F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Collection Status List";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 10.125F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 9.125F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 9.125F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.25F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 3.5F;
			// 
			// lblFakeHeader1
			// 
			this.lblFakeHeader1.Height = 0.375F;
			this.lblFakeHeader1.HyperLink = null;
			this.lblFakeHeader1.Left = 2.875F;
			this.lblFakeHeader1.Name = "lblFakeHeader1";
			this.lblFakeHeader1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblFakeHeader1.Text = "Original Amount Due";
			this.lblFakeHeader1.Top = 0.625F;
			this.lblFakeHeader1.Visible = false;
			this.lblFakeHeader1.Width = 1F;
			// 
			// lblFakeHeader2
			// 
			this.lblFakeHeader2.Height = 0.375F;
			this.lblFakeHeader2.HyperLink = null;
			this.lblFakeHeader2.Left = 4F;
			this.lblFakeHeader2.Name = "lblFakeHeader2";
			this.lblFakeHeader2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblFakeHeader2.Text = "Payment / Adjustments";
			this.lblFakeHeader2.Top = 0.625F;
			this.lblFakeHeader2.Visible = false;
			this.lblFakeHeader2.Width = 1F;
			// 
			// lblFakeHeader4
			// 
			this.lblFakeHeader4.Height = 0.1875F;
			this.lblFakeHeader4.HyperLink = null;
			this.lblFakeHeader4.Left = 5F;
			this.lblFakeHeader4.Name = "lblFakeHeader4";
			this.lblFakeHeader4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblFakeHeader4.Text = "Total Due";
			this.lblFakeHeader4.Top = 0.8125F;
			this.lblFakeHeader4.Visible = false;
			this.lblFakeHeader4.Width = 1.1875F;
			// 
			// lblFakeHeader5
			// 
			this.lblFakeHeader5.Height = 0.1875F;
			this.lblFakeHeader5.HyperLink = null;
			this.lblFakeHeader5.Left = 6.1875F;
			this.lblFakeHeader5.Name = "lblFakeHeader5";
			this.lblFakeHeader5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblFakeHeader5.Text = "Principal";
			this.lblFakeHeader5.Top = 0.8125F;
			this.lblFakeHeader5.Visible = false;
			this.lblFakeHeader5.Width = 1.125F;
			// 
			// lblFakeHeader6
			// 
			this.lblFakeHeader6.Height = 0.1875F;
			this.lblFakeHeader6.HyperLink = null;
			this.lblFakeHeader6.Left = 7.3125F;
			this.lblFakeHeader6.Name = "lblFakeHeader6";
			this.lblFakeHeader6.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblFakeHeader6.Text = "Tax";
			this.lblFakeHeader6.Top = 0.8125F;
			this.lblFakeHeader6.Visible = false;
			this.lblFakeHeader6.Width = 1.0625F;
			// 
			// lblFakeHeader7
			// 
			this.lblFakeHeader7.Height = 0.1875F;
			this.lblFakeHeader7.HyperLink = null;
			this.lblFakeHeader7.Left = 8.375F;
			this.lblFakeHeader7.Name = "lblFakeHeader7";
			this.lblFakeHeader7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblFakeHeader7.Text = "Interest";
			this.lblFakeHeader7.Top = 0.8125F;
			this.lblFakeHeader7.Visible = false;
			this.lblFakeHeader7.Width = 1F;
			// 
			// lblFakeHeader8
			// 
			this.lblFakeHeader8.Height = 0.1875F;
			this.lblFakeHeader8.HyperLink = null;
			this.lblFakeHeader8.Left = 9.375F;
			this.lblFakeHeader8.Name = "lblFakeHeader8";
			this.lblFakeHeader8.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblFakeHeader8.Text = "Costs";
			this.lblFakeHeader8.Top = 0.8125F;
			this.lblFakeHeader8.Visible = false;
			this.lblFakeHeader8.Width = 0.75F;
			// 
			// lblFakeHeader3
			// 
			this.lblFakeHeader3.Height = 0.1875F;
			this.lblFakeHeader3.HyperLink = null;
			this.lblFakeHeader3.Left = 5F;
			this.lblFakeHeader3.Name = "lblFakeHeader3";
			this.lblFakeHeader3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblFakeHeader3.Text = "Outstanding";
			this.lblFakeHeader3.Top = 0.625F;
			this.lblFakeHeader3.Visible = false;
			this.lblFakeHeader3.Width = 5.125F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.5625F;
			this.lnHeader.Width = 10.125F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 10.125F;
			this.lnHeader.Y1 = 0.5625F;
			this.lnHeader.Y2 = 0.5625F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.0625F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.375F;
			this.lblAccount.Width = 0.5F;
			// 
			// lblBillDate
			// 
			this.lblBillDate.Height = 0.1875F;
			this.lblBillDate.HyperLink = null;
			this.lblBillDate.Left = 1.8125F;
			this.lblBillDate.Name = "lblBillDate";
			this.lblBillDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblBillDate.Text = "Bill Date";
			this.lblBillDate.Top = 0.375F;
			this.lblBillDate.Width = 0.875F;
			// 
			// lblTaxDue
			// 
			this.lblTaxDue.Height = 0.375F;
			this.lblTaxDue.HyperLink = null;
			this.lblTaxDue.Left = 2.875F;
			this.lblTaxDue.Name = "lblTaxDue";
			this.lblTaxDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTaxDue.Text = "Original Amount Due";
			this.lblTaxDue.Top = 0.1875F;
			this.lblTaxDue.Width = 1F;
			// 
			// lblPaymentReceived
			// 
			this.lblPaymentReceived.Height = 0.375F;
			this.lblPaymentReceived.HyperLink = null;
			this.lblPaymentReceived.Left = 4F;
			this.lblPaymentReceived.Name = "lblPaymentReceived";
			this.lblPaymentReceived.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPaymentReceived.Text = "Payment / Adjustments";
			this.lblPaymentReceived.Top = 0.1875F;
			this.lblPaymentReceived.Width = 1F;
			// 
			// lblDue
			// 
			this.lblDue.Height = 0.1875F;
			this.lblDue.HyperLink = null;
			this.lblDue.Left = 5F;
			this.lblDue.Name = "lblDue";
			this.lblDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDue.Text = "Total Due";
			this.lblDue.Top = 0.375F;
			this.lblDue.Width = 1.1875F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 6.1875F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0.375F;
			this.lblPrincipal.Width = 1.125F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 7.3125F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.375F;
			this.lblTax.Width = 1.0625F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 8.375F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0.375F;
			this.lblInterest.Width = 1F;
			// 
			// lblCost
			// 
			this.lblCost.Height = 0.1875F;
			this.lblCost.HyperLink = null;
			this.lblCost.Left = 9.375F;
			this.lblCost.Name = "lblCost";
			this.lblCost.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblCost.Text = "Costs";
			this.lblCost.Top = 0.375F;
			this.lblCost.Width = 0.75F;
			// 
			// lblOutstanding
			// 
			this.lblOutstanding.Height = 0.1875F;
			this.lblOutstanding.HyperLink = null;
			this.lblOutstanding.Left = 5F;
			this.lblOutstanding.Name = "lblOutstanding";
			this.lblOutstanding.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblOutstanding.Text = "Outstanding";
			this.lblOutstanding.Top = 0.1875F;
			this.lblOutstanding.Width = 5.125F;
			// 
			// lblGroupHeader
			// 
			this.lblGroupHeader.Height = 0.1875F;
			this.lblGroupHeader.HyperLink = null;
			this.lblGroupHeader.Left = 0.1875F;
			this.lblGroupHeader.Name = "lblGroupHeader";
			this.lblGroupHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblGroupHeader.Text = null;
			this.lblGroupHeader.Top = 0F;
			this.lblGroupHeader.Width = 10.125F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.1875F;
			this.Binder.Left = 0F;
			this.Binder.Name = "Binder";
			this.Binder.Text = null;
			this.Binder.Top = 0.1875F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.5F;
			// 
			// lblBook
			// 
			this.lblBook.Height = 0.1875F;
			this.lblBook.HyperLink = null;
			this.lblBook.Left = 0.875F;
			this.lblBook.Name = "lblBook";
			this.lblBook.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblBook.Text = "Book";
			this.lblBook.Top = 0.375F;
			this.lblBook.Width = 0.4375F;
			// 
			// lblBillNum
			// 
			this.lblBillNum.Height = 0.1875F;
			this.lblBillNum.HyperLink = null;
			this.lblBillNum.Left = 1.375F;
			this.lblBillNum.Name = "lblBillNum";
			this.lblBillNum.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblBillNum.Text = "Bill";
			this.lblBillNum.Top = 0.375F;
			this.lblBillNum.Width = 0.375F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.875F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 6F;
			// 
			// fldAccount
			// 
			this.fldAccount.CanGrow = false;
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.MultiLine = false;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right; white-space: nowrap";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.8125F;
			// 
			// fldBillDate
			// 
			this.fldBillDate.CanGrow = false;
			this.fldBillDate.Height = 0.1875F;
			this.fldBillDate.Left = 1.8125F;
			this.fldBillDate.Name = "fldBillDate";
			this.fldBillDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldBillDate.Text = null;
			this.fldBillDate.Top = 0F;
			this.fldBillDate.Width = 0.875F;
			// 
			// fldTaxDue
			// 
			this.fldTaxDue.CanGrow = false;
			this.fldTaxDue.Height = 0.1875F;
			this.fldTaxDue.Left = 2.75F;
			this.fldTaxDue.Name = "fldTaxDue";
			this.fldTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTaxDue.Text = "0.00";
			this.fldTaxDue.Top = 0F;
			this.fldTaxDue.Width = 1.125F;
			// 
			// fldPaymentReceived
			// 
			this.fldPaymentReceived.CanGrow = false;
			this.fldPaymentReceived.Height = 0.1875F;
			this.fldPaymentReceived.Left = 3.875F;
			this.fldPaymentReceived.Name = "fldPaymentReceived";
			this.fldPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentReceived.Text = "0.00";
			this.fldPaymentReceived.Top = 0F;
			this.fldPaymentReceived.Width = 1.125F;
			// 
			// fldDue
			// 
			this.fldDue.CanGrow = false;
			this.fldDue.Height = 0.1875F;
			this.fldDue.Left = 5F;
			this.fldDue.Name = "fldDue";
			this.fldDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldDue.Text = "0.00";
			this.fldDue.Top = 0F;
			this.fldDue.Width = 1.1875F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 0F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'";
			this.lblLocation.Text = "Location:";
			this.lblLocation.Top = 0.375F;
			this.lblLocation.Width = 0.75F;
			// 
			// fldLocation
			// 
			this.fldLocation.CanGrow = false;
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 0.75F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0.375F;
			this.fldLocation.Width = 6.125F;
			// 
			// lblMapLot
			// 
			this.lblMapLot.Height = 0.1875F;
			this.lblMapLot.HyperLink = null;
			this.lblMapLot.Left = 0F;
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Style = "font-family: \'Tahoma\'";
			this.lblMapLot.Text = "Map Lot:";
			this.lblMapLot.Top = 0.5625F;
			this.lblMapLot.Width = 0.75F;
			// 
			// fldMapLot
			// 
			this.fldMapLot.CanGrow = false;
			this.fldMapLot.Height = 0.1875F;
			this.fldMapLot.Left = 0.75F;
			this.fldMapLot.MultiLine = false;
			this.fldMapLot.Name = "fldMapLot";
			this.fldMapLot.Style = "font-family: \'Tahoma\'";
			this.fldMapLot.Text = null;
			this.fldMapLot.Top = 0.5625F;
			this.fldMapLot.Width = 0.875F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.CanGrow = false;
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 6.1875F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = "0.00";
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 1.125F;
			// 
			// fldTax
			// 
			this.fldTax.CanGrow = false;
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 7.3125F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTax.Text = "0.00";
			this.fldTax.Top = 0F;
			this.fldTax.Width = 1.0625F;
			// 
			// fldInterest
			// 
			this.fldInterest.CanGrow = false;
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 8.375F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldInterest.Text = "0.00";
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 1F;
			// 
			// fldCosts
			// 
			this.fldCosts.CanGrow = false;
			this.fldCosts.Height = 0.1875F;
			this.fldCosts.Left = 9.375F;
			this.fldCosts.Name = "fldCosts";
			this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCosts.Text = "0.00";
			this.fldCosts.Top = 0F;
			this.fldCosts.Width = 0.75F;
			// 
			// fldLienRecord
			// 
			this.fldLienRecord.CanGrow = false;
			this.fldLienRecord.Height = 0.1875F;
			this.fldLienRecord.Left = 0.8125F;
			this.fldLienRecord.Name = "fldLienRecord";
			this.fldLienRecord.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldLienRecord.Text = "*";
			this.fldLienRecord.Top = 0F;
			this.fldLienRecord.Visible = false;
			this.fldLienRecord.Width = 0.125F;
			// 
			// fldBook
			// 
			this.fldBook.CanGrow = false;
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0.875F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldBook.Text = null;
			this.fldBook.Top = 0F;
			this.fldBook.Width = 0.4375F;
			// 
			// srptSLAllActivityDetailOB
			// 
			this.srptSLAllActivityDetailOB.CloseBorder = false;
			this.srptSLAllActivityDetailOB.Height = 0.125F;
			this.srptSLAllActivityDetailOB.Left = 0F;
			this.srptSLAllActivityDetailOB.Name = "srptSLAllActivityDetailOB";
			this.srptSLAllActivityDetailOB.Report = null;
			this.srptSLAllActivityDetailOB.Top = 0.625F;
			this.srptSLAllActivityDetailOB.Visible = false;
			this.srptSLAllActivityDetailOB.Width = 10.125F;
			// 
			// fldTADate
			// 
			this.fldTADate.Height = 0.1875F;
			this.fldTADate.Left = 5.75F;
			this.fldTADate.Name = "fldTADate";
			this.fldTADate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTADate.Text = null;
			this.fldTADate.Top = 0F;
			this.fldTADate.Visible = false;
			this.fldTADate.Width = 1.25F;
			// 
			// fldBillNum
			// 
			this.fldBillNum.Height = 0.1875F;
			this.fldBillNum.Left = 1.375F;
			this.fldBillNum.Name = "fldBillNum";
			this.fldBillNum.Style = "font-family: \'Tahoma\'";
			this.fldBillNum.Text = null;
			this.fldBillNum.Top = 0F;
			this.fldBillNum.Width = 0.375F;
			// 
			// fldGroupPayments
			// 
			this.fldGroupPayments.Height = 0.1875F;
			this.fldGroupPayments.Left = 3.875F;
			this.fldGroupPayments.Name = "fldGroupPayments";
			this.fldGroupPayments.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldGroupPayments.Text = "0.00";
			this.fldGroupPayments.Top = 0.0625F;
			this.fldGroupPayments.Width = 1.125F;
			// 
			// fldGroupTotal
			// 
			this.fldGroupTotal.Height = 0.1875F;
			this.fldGroupTotal.Left = 5F;
			this.fldGroupTotal.Name = "fldGroupTotal";
			this.fldGroupTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldGroupTotal.Text = "0.00";
			this.fldGroupTotal.Top = 0.0625F;
			this.fldGroupTotal.Width = 1.1875F;
			// 
			// fldGroupPrincipal
			// 
			this.fldGroupPrincipal.Height = 0.1875F;
			this.fldGroupPrincipal.Left = 6.1875F;
			this.fldGroupPrincipal.Name = "fldGroupPrincipal";
			this.fldGroupPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldGroupPrincipal.Text = "0.00";
			this.fldGroupPrincipal.Top = 0.0625F;
			this.fldGroupPrincipal.Width = 1.125F;
			// 
			// lblGroupTotals
			// 
			this.lblGroupTotals.Height = 0.1875F;
			this.lblGroupTotals.HyperLink = null;
			this.lblGroupTotals.Left = 0.1875F;
			this.lblGroupTotals.Name = "lblGroupTotals";
			this.lblGroupTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblGroupTotals.Text = "Total:";
			this.lblGroupTotals.Top = 0.0625F;
			this.lblGroupTotals.Width = 1.8125F;
			// 
			// fldGroupTax
			// 
			this.fldGroupTax.Height = 0.1875F;
			this.fldGroupTax.Left = 7.3125F;
			this.fldGroupTax.Name = "fldGroupTax";
			this.fldGroupTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldGroupTax.Text = "0.00";
			this.fldGroupTax.Top = 0.0625F;
			this.fldGroupTax.Width = 1.0625F;
			// 
			// fldGroupInterest
			// 
			this.fldGroupInterest.Height = 0.1875F;
			this.fldGroupInterest.Left = 8.375F;
			this.fldGroupInterest.Name = "fldGroupInterest";
			this.fldGroupInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldGroupInterest.Text = "0.00";
			this.fldGroupInterest.Top = 0.0625F;
			this.fldGroupInterest.Width = 1F;
			// 
			// fldGroupCost
			// 
			this.fldGroupCost.Height = 0.1875F;
			this.fldGroupCost.Left = 9.375F;
			this.fldGroupCost.Name = "fldGroupCost";
			this.fldGroupCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldGroupCost.Text = "0.00";
			this.fldGroupCost.Top = 0.0625F;
			this.fldGroupCost.Width = 0.75F;
			// 
			// fldGroupTaxDue
			// 
			this.fldGroupTaxDue.Height = 0.1875F;
			this.fldGroupTaxDue.Left = 2.75F;
			this.fldGroupTaxDue.Name = "fldGroupTaxDue";
			this.fldGroupTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldGroupTaxDue.Text = "0.00";
			this.fldGroupTaxDue.Top = 0.0625F;
			this.fldGroupTaxDue.Width = 1.125F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 2.5F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 7.625F;
			this.Line3.X1 = 2.5F;
			this.Line3.X2 = 10.125F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// fldTotalTaxDue
			// 
			this.fldTotalTaxDue.Height = 0.1875F;
			this.fldTotalTaxDue.Left = 2.5625F;
			this.fldTotalTaxDue.Name = "fldTotalTaxDue";
			this.fldTotalTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTaxDue.Text = "0.00";
			this.fldTotalTaxDue.Top = 0.0625F;
			this.fldTotalTaxDue.Width = 1.3125F;
			// 
			// fldTotalPaymentReceived
			// 
			this.fldTotalPaymentReceived.Height = 0.1875F;
			this.fldTotalPaymentReceived.Left = 3.875F;
			this.fldTotalPaymentReceived.Name = "fldTotalPaymentReceived";
			this.fldTotalPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentReceived.Text = "0.00";
			this.fldTotalPaymentReceived.Top = 0.0625F;
			this.fldTotalPaymentReceived.Width = 1.125F;
			// 
			// fldTotalDue
			// 
			this.fldTotalDue.Height = 0.1875F;
			this.fldTotalDue.Left = 5F;
			this.fldTotalDue.Name = "fldTotalDue";
			this.fldTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalDue.Text = "0.00";
			this.fldTotalDue.Top = 0.0625F;
			this.fldTotalDue.Width = 1.1875F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 2.5625F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0F;
			this.lnTotals.Width = 7.625F;
			this.lnTotals.X1 = 2.5625F;
			this.lnTotals.X2 = 10.1875F;
			this.lnTotals.Y1 = 0F;
			this.lnTotals.Y2 = 0F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.1875F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0.0625F;
			this.lblTotals.Width = 1.8125F;
			// 
			// lblRTError
			// 
			this.lblRTError.Height = 0.3125F;
			this.lblRTError.HyperLink = null;
			this.lblRTError.Left = 0F;
			this.lblRTError.Name = "lblRTError";
			this.lblRTError.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblRTError.Text = "  ";
			this.lblRTError.Top = 6.6875F;
			this.lblRTError.Width = 7F;
			// 
			// lblSpecialTotal
			// 
			this.lblSpecialTotal.Height = 0.1875F;
			this.lblSpecialTotal.HyperLink = null;
			this.lblSpecialTotal.Left = 5.3125F;
			this.lblSpecialTotal.Name = "lblSpecialTotal";
			this.lblSpecialTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSpecialTotal.Text = null;
			this.lblSpecialTotal.Top = 0.3125F;
			this.lblSpecialTotal.Width = 2.8125F;
			// 
			// lblSpecialText
			// 
			this.lblSpecialText.Height = 0.1875F;
			this.lblSpecialText.HyperLink = null;
			this.lblSpecialText.Left = 2.3125F;
			this.lblSpecialText.Name = "lblSpecialText";
			this.lblSpecialText.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.lblSpecialText.Text = null;
			this.lblSpecialText.Top = 0.3125F;
			this.lblSpecialText.Width = 3F;
			// 
			// lblSummary
			// 
			this.lblSummary.Height = 0.1875F;
			this.lblSummary.HyperLink = null;
			this.lblSummary.Left = 0.3125F;
			this.lblSummary.Name = "lblSummary";
			this.lblSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblSummary.Text = "Balance Due";
			this.lblSummary.Top = 3.5F;
			this.lblSummary.Width = 2F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.3125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3.6875F;
			this.Line1.Width = 2F;
			this.Line1.X1 = 0.3125F;
			this.Line1.X2 = 2.3125F;
			this.Line1.Y1 = 3.6875F;
			this.Line1.Y2 = 3.6875F;
			// 
			// lblSummary1
			// 
			this.lblSummary1.Height = 0.1875F;
			this.lblSummary1.HyperLink = null;
			this.lblSummary1.Left = 0.3125F;
			this.lblSummary1.Name = "lblSummary1";
			this.lblSummary1.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblSummary1.Text = null;
			this.lblSummary1.Top = 3.6875F;
			this.lblSummary1.Width = 1F;
			// 
			// fldSummary1
			// 
			this.fldSummary1.Height = 0.1875F;
			this.fldSummary1.Left = 1.3125F;
			this.fldSummary1.Name = "fldSummary1";
			this.fldSummary1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummary1.Text = "0.00";
			this.fldSummary1.Top = 3.6875F;
			this.fldSummary1.Width = 1F;
			// 
			// lblSummaryHeader
			// 
			this.lblSummaryHeader.Height = 0.1875F;
			this.lblSummaryHeader.HyperLink = null;
			this.lblSummaryHeader.Left = 1.75F;
			this.lblSummaryHeader.Name = "lblSummaryHeader";
			this.lblSummaryHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblSummaryHeader.Text = "Payment Summary";
			this.lblSummaryHeader.Top = 0.625F;
			this.lblSummaryHeader.Width = 7.25F;
			// 
			// lblSummaryPaymentType1
			// 
			this.lblSummaryPaymentType1.Height = 0.1875F;
			this.lblSummaryPaymentType1.HyperLink = null;
			this.lblSummaryPaymentType1.Left = 1.75F;
			this.lblSummaryPaymentType1.MultiLine = false;
			this.lblSummaryPaymentType1.Name = "lblSummaryPaymentType1";
			this.lblSummaryPaymentType1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType1.Text = null;
			this.lblSummaryPaymentType1.Top = 1F;
			this.lblSummaryPaymentType1.Width = 2.5F;
			// 
			// lblSummaryPaymentType2
			// 
			this.lblSummaryPaymentType2.Height = 0.1875F;
			this.lblSummaryPaymentType2.HyperLink = null;
			this.lblSummaryPaymentType2.Left = 1.75F;
			this.lblSummaryPaymentType2.MultiLine = false;
			this.lblSummaryPaymentType2.Name = "lblSummaryPaymentType2";
			this.lblSummaryPaymentType2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType2.Text = null;
			this.lblSummaryPaymentType2.Top = 1.1875F;
			this.lblSummaryPaymentType2.Width = 2.5F;
			// 
			// lblSummaryPaymentType3
			// 
			this.lblSummaryPaymentType3.Height = 0.1875F;
			this.lblSummaryPaymentType3.HyperLink = null;
			this.lblSummaryPaymentType3.Left = 1.75F;
			this.lblSummaryPaymentType3.MultiLine = false;
			this.lblSummaryPaymentType3.Name = "lblSummaryPaymentType3";
			this.lblSummaryPaymentType3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType3.Text = null;
			this.lblSummaryPaymentType3.Top = 1.375F;
			this.lblSummaryPaymentType3.Width = 2.5F;
			// 
			// lblSummaryPaymentType4
			// 
			this.lblSummaryPaymentType4.Height = 0.1875F;
			this.lblSummaryPaymentType4.HyperLink = null;
			this.lblSummaryPaymentType4.Left = 1.75F;
			this.lblSummaryPaymentType4.MultiLine = false;
			this.lblSummaryPaymentType4.Name = "lblSummaryPaymentType4";
			this.lblSummaryPaymentType4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType4.Text = null;
			this.lblSummaryPaymentType4.Top = 1.5625F;
			this.lblSummaryPaymentType4.Width = 2.5F;
			// 
			// lblSummaryPaymentType5
			// 
			this.lblSummaryPaymentType5.Height = 0.1875F;
			this.lblSummaryPaymentType5.HyperLink = null;
			this.lblSummaryPaymentType5.Left = 1.75F;
			this.lblSummaryPaymentType5.MultiLine = false;
			this.lblSummaryPaymentType5.Name = "lblSummaryPaymentType5";
			this.lblSummaryPaymentType5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType5.Text = null;
			this.lblSummaryPaymentType5.Top = 1.75F;
			this.lblSummaryPaymentType5.Width = 2.5F;
			// 
			// lblSummaryPaymentType6
			// 
			this.lblSummaryPaymentType6.Height = 0.1875F;
			this.lblSummaryPaymentType6.HyperLink = null;
			this.lblSummaryPaymentType6.Left = 1.75F;
			this.lblSummaryPaymentType6.MultiLine = false;
			this.lblSummaryPaymentType6.Name = "lblSummaryPaymentType6";
			this.lblSummaryPaymentType6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType6.Text = null;
			this.lblSummaryPaymentType6.Top = 1.9375F;
			this.lblSummaryPaymentType6.Width = 2.5F;
			// 
			// lblSummaryPaymentType7
			// 
			this.lblSummaryPaymentType7.Height = 0.1875F;
			this.lblSummaryPaymentType7.HyperLink = null;
			this.lblSummaryPaymentType7.Left = 1.75F;
			this.lblSummaryPaymentType7.MultiLine = false;
			this.lblSummaryPaymentType7.Name = "lblSummaryPaymentType7";
			this.lblSummaryPaymentType7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType7.Text = null;
			this.lblSummaryPaymentType7.Top = 2.125F;
			this.lblSummaryPaymentType7.Width = 2.5F;
			// 
			// lblSummaryPaymentType8
			// 
			this.lblSummaryPaymentType8.Height = 0.1875F;
			this.lblSummaryPaymentType8.HyperLink = null;
			this.lblSummaryPaymentType8.Left = 1.75F;
			this.lblSummaryPaymentType8.MultiLine = false;
			this.lblSummaryPaymentType8.Name = "lblSummaryPaymentType8";
			this.lblSummaryPaymentType8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType8.Text = null;
			this.lblSummaryPaymentType8.Top = 2.3125F;
			this.lblSummaryPaymentType8.Width = 2.5F;
			// 
			// lblSummaryPaymentType9
			// 
			this.lblSummaryPaymentType9.Height = 0.1875F;
			this.lblSummaryPaymentType9.HyperLink = null;
			this.lblSummaryPaymentType9.Left = 1.75F;
			this.lblSummaryPaymentType9.MultiLine = false;
			this.lblSummaryPaymentType9.Name = "lblSummaryPaymentType9";
			this.lblSummaryPaymentType9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType9.Text = null;
			this.lblSummaryPaymentType9.Top = 2.5F;
			this.lblSummaryPaymentType9.Width = 2.5F;
			// 
			// lblSummaryPaymentType10
			// 
			this.lblSummaryPaymentType10.Height = 0.1875F;
			this.lblSummaryPaymentType10.HyperLink = null;
			this.lblSummaryPaymentType10.Left = 1.75F;
			this.lblSummaryPaymentType10.MultiLine = false;
			this.lblSummaryPaymentType10.Name = "lblSummaryPaymentType10";
			this.lblSummaryPaymentType10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType10.Text = null;
			this.lblSummaryPaymentType10.Top = 2.6875F;
			this.lblSummaryPaymentType10.Width = 2.5F;
			// 
			// lblSummaryPaymentType11
			// 
			this.lblSummaryPaymentType11.Height = 0.1875F;
			this.lblSummaryPaymentType11.HyperLink = null;
			this.lblSummaryPaymentType11.Left = 1.75F;
			this.lblSummaryPaymentType11.MultiLine = false;
			this.lblSummaryPaymentType11.Name = "lblSummaryPaymentType11";
			this.lblSummaryPaymentType11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType11.Text = null;
			this.lblSummaryPaymentType11.Top = 2.875F;
			this.lblSummaryPaymentType11.Width = 2.5F;
			// 
			// lblSummaryTotal1
			// 
			this.lblSummaryTotal1.Height = 0.1875F;
			this.lblSummaryTotal1.HyperLink = null;
			this.lblSummaryTotal1.Left = 8F;
			this.lblSummaryTotal1.Name = "lblSummaryTotal1";
			this.lblSummaryTotal1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal1.Text = null;
			this.lblSummaryTotal1.Top = 1F;
			this.lblSummaryTotal1.Width = 1F;
			// 
			// lblSummaryTotal2
			// 
			this.lblSummaryTotal2.Height = 0.1875F;
			this.lblSummaryTotal2.HyperLink = null;
			this.lblSummaryTotal2.Left = 8F;
			this.lblSummaryTotal2.Name = "lblSummaryTotal2";
			this.lblSummaryTotal2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal2.Text = null;
			this.lblSummaryTotal2.Top = 1.1875F;
			this.lblSummaryTotal2.Width = 1F;
			// 
			// lblSummaryTotal3
			// 
			this.lblSummaryTotal3.Height = 0.1875F;
			this.lblSummaryTotal3.HyperLink = null;
			this.lblSummaryTotal3.Left = 8F;
			this.lblSummaryTotal3.Name = "lblSummaryTotal3";
			this.lblSummaryTotal3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal3.Text = null;
			this.lblSummaryTotal3.Top = 1.375F;
			this.lblSummaryTotal3.Width = 1F;
			// 
			// lblSummaryTotal4
			// 
			this.lblSummaryTotal4.Height = 0.1875F;
			this.lblSummaryTotal4.HyperLink = null;
			this.lblSummaryTotal4.Left = 8F;
			this.lblSummaryTotal4.Name = "lblSummaryTotal4";
			this.lblSummaryTotal4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal4.Text = null;
			this.lblSummaryTotal4.Top = 1.5625F;
			this.lblSummaryTotal4.Width = 1F;
			// 
			// lblSummaryTotal5
			// 
			this.lblSummaryTotal5.Height = 0.1875F;
			this.lblSummaryTotal5.HyperLink = null;
			this.lblSummaryTotal5.Left = 8F;
			this.lblSummaryTotal5.Name = "lblSummaryTotal5";
			this.lblSummaryTotal5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal5.Text = null;
			this.lblSummaryTotal5.Top = 1.75F;
			this.lblSummaryTotal5.Width = 1F;
			// 
			// lblSummaryTotal6
			// 
			this.lblSummaryTotal6.Height = 0.1875F;
			this.lblSummaryTotal6.HyperLink = null;
			this.lblSummaryTotal6.Left = 8F;
			this.lblSummaryTotal6.Name = "lblSummaryTotal6";
			this.lblSummaryTotal6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal6.Text = null;
			this.lblSummaryTotal6.Top = 1.9375F;
			this.lblSummaryTotal6.Width = 1F;
			// 
			// lblSummaryTotal7
			// 
			this.lblSummaryTotal7.Height = 0.1875F;
			this.lblSummaryTotal7.HyperLink = null;
			this.lblSummaryTotal7.Left = 8F;
			this.lblSummaryTotal7.Name = "lblSummaryTotal7";
			this.lblSummaryTotal7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal7.Text = null;
			this.lblSummaryTotal7.Top = 2.125F;
			this.lblSummaryTotal7.Width = 1F;
			// 
			// lblSummaryTotal8
			// 
			this.lblSummaryTotal8.Height = 0.1875F;
			this.lblSummaryTotal8.HyperLink = null;
			this.lblSummaryTotal8.Left = 8F;
			this.lblSummaryTotal8.Name = "lblSummaryTotal8";
			this.lblSummaryTotal8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal8.Text = null;
			this.lblSummaryTotal8.Top = 2.3125F;
			this.lblSummaryTotal8.Width = 1F;
			// 
			// lblSummaryTotal9
			// 
			this.lblSummaryTotal9.Height = 0.1875F;
			this.lblSummaryTotal9.HyperLink = null;
			this.lblSummaryTotal9.Left = 8F;
			this.lblSummaryTotal9.Name = "lblSummaryTotal9";
			this.lblSummaryTotal9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal9.Text = null;
			this.lblSummaryTotal9.Top = 2.5F;
			this.lblSummaryTotal9.Width = 1F;
			// 
			// lblSummaryTotal10
			// 
			this.lblSummaryTotal10.Height = 0.1875F;
			this.lblSummaryTotal10.HyperLink = null;
			this.lblSummaryTotal10.Left = 8F;
			this.lblSummaryTotal10.Name = "lblSummaryTotal10";
			this.lblSummaryTotal10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal10.Text = null;
			this.lblSummaryTotal10.Top = 2.6875F;
			this.lblSummaryTotal10.Width = 1F;
			// 
			// lblSummaryTotal11
			// 
			this.lblSummaryTotal11.Height = 0.1875F;
			this.lblSummaryTotal11.HyperLink = null;
			this.lblSummaryTotal11.Left = 8F;
			this.lblSummaryTotal11.Name = "lblSummaryTotal11";
			this.lblSummaryTotal11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal11.Text = null;
			this.lblSummaryTotal11.Top = 2.875F;
			this.lblSummaryTotal11.Width = 1F;
			// 
			// lblSumPrin1
			// 
			this.lblSumPrin1.Height = 0.1875F;
			this.lblSumPrin1.HyperLink = null;
			this.lblSumPrin1.Left = 4.25F;
			this.lblSumPrin1.Name = "lblSumPrin1";
			this.lblSumPrin1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin1.Text = null;
			this.lblSumPrin1.Top = 1F;
			this.lblSumPrin1.Width = 1F;
			// 
			// lblSumPrin2
			// 
			this.lblSumPrin2.Height = 0.1875F;
			this.lblSumPrin2.HyperLink = null;
			this.lblSumPrin2.Left = 4.25F;
			this.lblSumPrin2.Name = "lblSumPrin2";
			this.lblSumPrin2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin2.Text = null;
			this.lblSumPrin2.Top = 1.1875F;
			this.lblSumPrin2.Width = 1F;
			// 
			// lblSumPrin3
			// 
			this.lblSumPrin3.Height = 0.1875F;
			this.lblSumPrin3.HyperLink = null;
			this.lblSumPrin3.Left = 4.25F;
			this.lblSumPrin3.Name = "lblSumPrin3";
			this.lblSumPrin3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin3.Text = null;
			this.lblSumPrin3.Top = 1.375F;
			this.lblSumPrin3.Width = 1F;
			// 
			// lblSumPrin4
			// 
			this.lblSumPrin4.Height = 0.1875F;
			this.lblSumPrin4.HyperLink = null;
			this.lblSumPrin4.Left = 4.25F;
			this.lblSumPrin4.Name = "lblSumPrin4";
			this.lblSumPrin4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin4.Text = null;
			this.lblSumPrin4.Top = 1.5625F;
			this.lblSumPrin4.Width = 1F;
			// 
			// lblSumPrin5
			// 
			this.lblSumPrin5.Height = 0.1875F;
			this.lblSumPrin5.HyperLink = null;
			this.lblSumPrin5.Left = 4.25F;
			this.lblSumPrin5.Name = "lblSumPrin5";
			this.lblSumPrin5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin5.Text = null;
			this.lblSumPrin5.Top = 1.75F;
			this.lblSumPrin5.Width = 1F;
			// 
			// lblSumPrin6
			// 
			this.lblSumPrin6.Height = 0.1875F;
			this.lblSumPrin6.HyperLink = null;
			this.lblSumPrin6.Left = 4.25F;
			this.lblSumPrin6.Name = "lblSumPrin6";
			this.lblSumPrin6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin6.Text = null;
			this.lblSumPrin6.Top = 1.9375F;
			this.lblSumPrin6.Width = 1F;
			// 
			// lblSumPrin7
			// 
			this.lblSumPrin7.Height = 0.1875F;
			this.lblSumPrin7.HyperLink = null;
			this.lblSumPrin7.Left = 4.25F;
			this.lblSumPrin7.Name = "lblSumPrin7";
			this.lblSumPrin7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin7.Text = null;
			this.lblSumPrin7.Top = 2.125F;
			this.lblSumPrin7.Width = 1F;
			// 
			// lblSumPrin8
			// 
			this.lblSumPrin8.Height = 0.1875F;
			this.lblSumPrin8.HyperLink = null;
			this.lblSumPrin8.Left = 4.25F;
			this.lblSumPrin8.Name = "lblSumPrin8";
			this.lblSumPrin8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin8.Text = null;
			this.lblSumPrin8.Top = 2.3125F;
			this.lblSumPrin8.Width = 1F;
			// 
			// lblSumPrin9
			// 
			this.lblSumPrin9.Height = 0.1875F;
			this.lblSumPrin9.HyperLink = null;
			this.lblSumPrin9.Left = 4.25F;
			this.lblSumPrin9.Name = "lblSumPrin9";
			this.lblSumPrin9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin9.Text = null;
			this.lblSumPrin9.Top = 2.5F;
			this.lblSumPrin9.Width = 1F;
			// 
			// lblSumPrin10
			// 
			this.lblSumPrin10.Height = 0.1875F;
			this.lblSumPrin10.HyperLink = null;
			this.lblSumPrin10.Left = 4.25F;
			this.lblSumPrin10.Name = "lblSumPrin10";
			this.lblSumPrin10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin10.Text = null;
			this.lblSumPrin10.Top = 2.6875F;
			this.lblSumPrin10.Width = 1F;
			// 
			// lblSumPrin11
			// 
			this.lblSumPrin11.Height = 0.1875F;
			this.lblSumPrin11.HyperLink = null;
			this.lblSumPrin11.Left = 4.25F;
			this.lblSumPrin11.Name = "lblSumPrin11";
			this.lblSumPrin11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin11.Text = null;
			this.lblSumPrin11.Top = 2.875F;
			this.lblSumPrin11.Width = 1F;
			// 
			// lblSumInt1
			// 
			this.lblSumInt1.Height = 0.1875F;
			this.lblSumInt1.HyperLink = null;
			this.lblSumInt1.Left = 6.25F;
			this.lblSumInt1.Name = "lblSumInt1";
			this.lblSumInt1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt1.Text = null;
			this.lblSumInt1.Top = 1F;
			this.lblSumInt1.Width = 1F;
			// 
			// lblSumInt2
			// 
			this.lblSumInt2.Height = 0.1875F;
			this.lblSumInt2.HyperLink = null;
			this.lblSumInt2.Left = 6.25F;
			this.lblSumInt2.Name = "lblSumInt2";
			this.lblSumInt2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt2.Text = null;
			this.lblSumInt2.Top = 1.1875F;
			this.lblSumInt2.Width = 1F;
			// 
			// lblSumInt3
			// 
			this.lblSumInt3.Height = 0.1875F;
			this.lblSumInt3.HyperLink = null;
			this.lblSumInt3.Left = 6.25F;
			this.lblSumInt3.Name = "lblSumInt3";
			this.lblSumInt3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt3.Text = null;
			this.lblSumInt3.Top = 1.375F;
			this.lblSumInt3.Width = 1F;
			// 
			// lblSumInt4
			// 
			this.lblSumInt4.Height = 0.1875F;
			this.lblSumInt4.HyperLink = null;
			this.lblSumInt4.Left = 6.25F;
			this.lblSumInt4.Name = "lblSumInt4";
			this.lblSumInt4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt4.Text = null;
			this.lblSumInt4.Top = 1.5625F;
			this.lblSumInt4.Width = 1F;
			// 
			// lblSumInt5
			// 
			this.lblSumInt5.Height = 0.1875F;
			this.lblSumInt5.HyperLink = null;
			this.lblSumInt5.Left = 6.25F;
			this.lblSumInt5.Name = "lblSumInt5";
			this.lblSumInt5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt5.Text = null;
			this.lblSumInt5.Top = 1.75F;
			this.lblSumInt5.Width = 1F;
			// 
			// lblSumInt6
			// 
			this.lblSumInt6.Height = 0.1875F;
			this.lblSumInt6.HyperLink = null;
			this.lblSumInt6.Left = 6.25F;
			this.lblSumInt6.Name = "lblSumInt6";
			this.lblSumInt6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt6.Text = null;
			this.lblSumInt6.Top = 1.9375F;
			this.lblSumInt6.Width = 1F;
			// 
			// lblSumInt7
			// 
			this.lblSumInt7.Height = 0.1875F;
			this.lblSumInt7.HyperLink = null;
			this.lblSumInt7.Left = 6.25F;
			this.lblSumInt7.Name = "lblSumInt7";
			this.lblSumInt7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt7.Text = null;
			this.lblSumInt7.Top = 2.125F;
			this.lblSumInt7.Width = 1F;
			// 
			// lblSumInt8
			// 
			this.lblSumInt8.Height = 0.1875F;
			this.lblSumInt8.HyperLink = null;
			this.lblSumInt8.Left = 6.25F;
			this.lblSumInt8.Name = "lblSumInt8";
			this.lblSumInt8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt8.Text = null;
			this.lblSumInt8.Top = 2.3125F;
			this.lblSumInt8.Width = 1F;
			// 
			// lblSumInt9
			// 
			this.lblSumInt9.Height = 0.1875F;
			this.lblSumInt9.HyperLink = null;
			this.lblSumInt9.Left = 6.25F;
			this.lblSumInt9.Name = "lblSumInt9";
			this.lblSumInt9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt9.Text = null;
			this.lblSumInt9.Top = 2.5F;
			this.lblSumInt9.Width = 1F;
			// 
			// lblSumInt10
			// 
			this.lblSumInt10.Height = 0.1875F;
			this.lblSumInt10.HyperLink = null;
			this.lblSumInt10.Left = 6.25F;
			this.lblSumInt10.Name = "lblSumInt10";
			this.lblSumInt10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt10.Text = null;
			this.lblSumInt10.Top = 2.6875F;
			this.lblSumInt10.Width = 1F;
			// 
			// lblSumInt11
			// 
			this.lblSumInt11.Height = 0.1875F;
			this.lblSumInt11.HyperLink = null;
			this.lblSumInt11.Left = 6.25F;
			this.lblSumInt11.Name = "lblSumInt11";
			this.lblSumInt11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt11.Text = null;
			this.lblSumInt11.Top = 2.875F;
			this.lblSumInt11.Width = 1F;
			// 
			// lblSumCost1
			// 
			this.lblSumCost1.Height = 0.1875F;
			this.lblSumCost1.HyperLink = null;
			this.lblSumCost1.Left = 7.25F;
			this.lblSumCost1.Name = "lblSumCost1";
			this.lblSumCost1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost1.Text = null;
			this.lblSumCost1.Top = 1F;
			this.lblSumCost1.Width = 0.75F;
			// 
			// lblSumCost2
			// 
			this.lblSumCost2.Height = 0.1875F;
			this.lblSumCost2.HyperLink = null;
			this.lblSumCost2.Left = 7.25F;
			this.lblSumCost2.Name = "lblSumCost2";
			this.lblSumCost2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost2.Text = null;
			this.lblSumCost2.Top = 1.1875F;
			this.lblSumCost2.Width = 0.75F;
			// 
			// lblSumCost3
			// 
			this.lblSumCost3.Height = 0.1875F;
			this.lblSumCost3.HyperLink = null;
			this.lblSumCost3.Left = 7.25F;
			this.lblSumCost3.Name = "lblSumCost3";
			this.lblSumCost3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost3.Text = null;
			this.lblSumCost3.Top = 1.375F;
			this.lblSumCost3.Width = 0.75F;
			// 
			// lblSumCost4
			// 
			this.lblSumCost4.Height = 0.1875F;
			this.lblSumCost4.HyperLink = null;
			this.lblSumCost4.Left = 7.25F;
			this.lblSumCost4.Name = "lblSumCost4";
			this.lblSumCost4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost4.Text = null;
			this.lblSumCost4.Top = 1.5625F;
			this.lblSumCost4.Width = 0.75F;
			// 
			// lblSumCost5
			// 
			this.lblSumCost5.Height = 0.1875F;
			this.lblSumCost5.HyperLink = null;
			this.lblSumCost5.Left = 7.25F;
			this.lblSumCost5.Name = "lblSumCost5";
			this.lblSumCost5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost5.Text = null;
			this.lblSumCost5.Top = 1.75F;
			this.lblSumCost5.Width = 0.75F;
			// 
			// lblSumCost6
			// 
			this.lblSumCost6.Height = 0.1875F;
			this.lblSumCost6.HyperLink = null;
			this.lblSumCost6.Left = 7.25F;
			this.lblSumCost6.Name = "lblSumCost6";
			this.lblSumCost6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost6.Text = null;
			this.lblSumCost6.Top = 1.9375F;
			this.lblSumCost6.Width = 0.75F;
			// 
			// lblSumCost7
			// 
			this.lblSumCost7.Height = 0.1875F;
			this.lblSumCost7.HyperLink = null;
			this.lblSumCost7.Left = 7.25F;
			this.lblSumCost7.Name = "lblSumCost7";
			this.lblSumCost7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost7.Text = null;
			this.lblSumCost7.Top = 2.125F;
			this.lblSumCost7.Width = 0.75F;
			// 
			// lblSumCost8
			// 
			this.lblSumCost8.Height = 0.1875F;
			this.lblSumCost8.HyperLink = null;
			this.lblSumCost8.Left = 7.25F;
			this.lblSumCost8.Name = "lblSumCost8";
			this.lblSumCost8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost8.Text = null;
			this.lblSumCost8.Top = 2.3125F;
			this.lblSumCost8.Width = 0.75F;
			// 
			// lblSumCost9
			// 
			this.lblSumCost9.Height = 0.1875F;
			this.lblSumCost9.HyperLink = null;
			this.lblSumCost9.Left = 7.25F;
			this.lblSumCost9.Name = "lblSumCost9";
			this.lblSumCost9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost9.Text = null;
			this.lblSumCost9.Top = 2.5F;
			this.lblSumCost9.Width = 0.75F;
			// 
			// lblSumCost10
			// 
			this.lblSumCost10.Height = 0.1875F;
			this.lblSumCost10.HyperLink = null;
			this.lblSumCost10.Left = 7.25F;
			this.lblSumCost10.Name = "lblSumCost10";
			this.lblSumCost10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost10.Text = null;
			this.lblSumCost10.Top = 2.6875F;
			this.lblSumCost10.Width = 0.75F;
			// 
			// lblSumCost11
			// 
			this.lblSumCost11.Height = 0.1875F;
			this.lblSumCost11.HyperLink = null;
			this.lblSumCost11.Left = 7.25F;
			this.lblSumCost11.Name = "lblSumCost11";
			this.lblSumCost11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost11.Text = null;
			this.lblSumCost11.Top = 2.875F;
			this.lblSumCost11.Width = 0.75F;
			// 
			// lblSumHeaderType
			// 
			this.lblSumHeaderType.Height = 0.1875F;
			this.lblSumHeaderType.HyperLink = null;
			this.lblSumHeaderType.Left = 1.75F;
			this.lblSumHeaderType.MultiLine = false;
			this.lblSumHeaderType.Name = "lblSumHeaderType";
			this.lblSumHeaderType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSumHeaderType.Text = null;
			this.lblSumHeaderType.Top = 0.8125F;
			this.lblSumHeaderType.Width = 2.5F;
			// 
			// lblSumHeaderTotal
			// 
			this.lblSumHeaderTotal.Height = 0.1875F;
			this.lblSumHeaderTotal.HyperLink = null;
			this.lblSumHeaderTotal.Left = 8F;
			this.lblSumHeaderTotal.Name = "lblSumHeaderTotal";
			this.lblSumHeaderTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderTotal.Text = null;
			this.lblSumHeaderTotal.Top = 0.8125F;
			this.lblSumHeaderTotal.Width = 1F;
			// 
			// lblSumHeaderPrin
			// 
			this.lblSumHeaderPrin.Height = 0.1875F;
			this.lblSumHeaderPrin.HyperLink = null;
			this.lblSumHeaderPrin.Left = 4.25F;
			this.lblSumHeaderPrin.Name = "lblSumHeaderPrin";
			this.lblSumHeaderPrin.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderPrin.Text = null;
			this.lblSumHeaderPrin.Top = 0.8125F;
			this.lblSumHeaderPrin.Width = 1F;
			// 
			// lblSumHeaderInt
			// 
			this.lblSumHeaderInt.Height = 0.1875F;
			this.lblSumHeaderInt.HyperLink = null;
			this.lblSumHeaderInt.Left = 6.25F;
			this.lblSumHeaderInt.Name = "lblSumHeaderInt";
			this.lblSumHeaderInt.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderInt.Text = null;
			this.lblSumHeaderInt.Top = 0.8125F;
			this.lblSumHeaderInt.Width = 1F;
			// 
			// lblSumHeaderCost
			// 
			this.lblSumHeaderCost.Height = 0.1875F;
			this.lblSumHeaderCost.HyperLink = null;
			this.lblSumHeaderCost.Left = 7.25F;
			this.lblSumHeaderCost.Name = "lblSumHeaderCost";
			this.lblSumHeaderCost.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderCost.Text = null;
			this.lblSumHeaderCost.Top = 0.8125F;
			this.lblSumHeaderCost.Width = 0.75F;
			// 
			// lblSumTax1
			// 
			this.lblSumTax1.Height = 0.1875F;
			this.lblSumTax1.HyperLink = null;
			this.lblSumTax1.Left = 5.25F;
			this.lblSumTax1.Name = "lblSumTax1";
			this.lblSumTax1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax1.Text = null;
			this.lblSumTax1.Top = 1F;
			this.lblSumTax1.Width = 1F;
			// 
			// lblSumTax2
			// 
			this.lblSumTax2.Height = 0.1875F;
			this.lblSumTax2.HyperLink = null;
			this.lblSumTax2.Left = 5.25F;
			this.lblSumTax2.Name = "lblSumTax2";
			this.lblSumTax2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax2.Text = null;
			this.lblSumTax2.Top = 1.1875F;
			this.lblSumTax2.Width = 1F;
			// 
			// lblSumTax3
			// 
			this.lblSumTax3.Height = 0.1875F;
			this.lblSumTax3.HyperLink = null;
			this.lblSumTax3.Left = 5.25F;
			this.lblSumTax3.Name = "lblSumTax3";
			this.lblSumTax3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax3.Text = null;
			this.lblSumTax3.Top = 1.375F;
			this.lblSumTax3.Width = 1F;
			// 
			// lblSumTax4
			// 
			this.lblSumTax4.Height = 0.1875F;
			this.lblSumTax4.HyperLink = null;
			this.lblSumTax4.Left = 5.25F;
			this.lblSumTax4.Name = "lblSumTax4";
			this.lblSumTax4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax4.Text = null;
			this.lblSumTax4.Top = 1.5625F;
			this.lblSumTax4.Width = 1F;
			// 
			// lblSumTax5
			// 
			this.lblSumTax5.Height = 0.1875F;
			this.lblSumTax5.HyperLink = null;
			this.lblSumTax5.Left = 5.25F;
			this.lblSumTax5.Name = "lblSumTax5";
			this.lblSumTax5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax5.Text = null;
			this.lblSumTax5.Top = 1.75F;
			this.lblSumTax5.Width = 1F;
			// 
			// lblSumTax6
			// 
			this.lblSumTax6.Height = 0.1875F;
			this.lblSumTax6.HyperLink = null;
			this.lblSumTax6.Left = 5.25F;
			this.lblSumTax6.Name = "lblSumTax6";
			this.lblSumTax6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax6.Text = null;
			this.lblSumTax6.Top = 1.9375F;
			this.lblSumTax6.Width = 1F;
			// 
			// lblSumTax7
			// 
			this.lblSumTax7.Height = 0.1875F;
			this.lblSumTax7.HyperLink = null;
			this.lblSumTax7.Left = 5.25F;
			this.lblSumTax7.Name = "lblSumTax7";
			this.lblSumTax7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax7.Text = null;
			this.lblSumTax7.Top = 2.125F;
			this.lblSumTax7.Width = 1F;
			// 
			// lblSumTax8
			// 
			this.lblSumTax8.Height = 0.1875F;
			this.lblSumTax8.HyperLink = null;
			this.lblSumTax8.Left = 5.25F;
			this.lblSumTax8.Name = "lblSumTax8";
			this.lblSumTax8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax8.Text = null;
			this.lblSumTax8.Top = 2.3125F;
			this.lblSumTax8.Width = 1F;
			// 
			// lblSumTax9
			// 
			this.lblSumTax9.Height = 0.1875F;
			this.lblSumTax9.HyperLink = null;
			this.lblSumTax9.Left = 5.25F;
			this.lblSumTax9.Name = "lblSumTax9";
			this.lblSumTax9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax9.Text = null;
			this.lblSumTax9.Top = 2.5F;
			this.lblSumTax9.Width = 1F;
			// 
			// lblSumTax10
			// 
			this.lblSumTax10.Height = 0.1875F;
			this.lblSumTax10.HyperLink = null;
			this.lblSumTax10.Left = 5.25F;
			this.lblSumTax10.Name = "lblSumTax10";
			this.lblSumTax10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax10.Text = null;
			this.lblSumTax10.Top = 2.6875F;
			this.lblSumTax10.Width = 1F;
			// 
			// lblSumTax11
			// 
			this.lblSumTax11.Height = 0.1875F;
			this.lblSumTax11.HyperLink = null;
			this.lblSumTax11.Left = 5.25F;
			this.lblSumTax11.Name = "lblSumTax11";
			this.lblSumTax11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax11.Text = null;
			this.lblSumTax11.Top = 2.875F;
			this.lblSumTax11.Width = 1F;
			// 
			// lblSumHeaderTax
			// 
			this.lblSumHeaderTax.Height = 0.1875F;
			this.lblSumHeaderTax.HyperLink = null;
			this.lblSumHeaderTax.Left = 5.25F;
			this.lblSumHeaderTax.Name = "lblSumHeaderTax";
			this.lblSumHeaderTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderTax.Text = null;
			this.lblSumHeaderTax.Top = 0.8125F;
			this.lblSumHeaderTax.Width = 1F;
			// 
			// lnSummaryTotal
			// 
			this.lnSummaryTotal.Height = 0F;
			this.lnSummaryTotal.Left = 4.25F;
			this.lnSummaryTotal.LineWeight = 1F;
			this.lnSummaryTotal.Name = "lnSummaryTotal";
			this.lnSummaryTotal.Top = 3.25F;
			this.lnSummaryTotal.Width = 4.75F;
			this.lnSummaryTotal.X1 = 4.25F;
			this.lnSummaryTotal.X2 = 9F;
			this.lnSummaryTotal.Y1 = 3.25F;
			this.lnSummaryTotal.Y2 = 3.25F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.75F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.8125F;
			this.Line2.Width = 7.25F;
			this.Line2.X1 = 1.75F;
			this.Line2.X2 = 9F;
			this.Line2.Y1 = 0.8125F;
			this.Line2.Y2 = 0.8125F;
			// 
			// fldTotalPrincipal
			// 
			this.fldTotalPrincipal.Height = 0.1875F;
			this.fldTotalPrincipal.Left = 6.1875F;
			this.fldTotalPrincipal.Name = "fldTotalPrincipal";
			this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrincipal.Text = "0.00";
			this.fldTotalPrincipal.Top = 0.0625F;
			this.fldTotalPrincipal.Width = 1.125F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 7.3125F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTax.Text = "0.00";
			this.fldTotalTax.Top = 0.0625F;
			this.fldTotalTax.Width = 1.0625F;
			// 
			// fldTotalInterest
			// 
			this.fldTotalInterest.Height = 0.1875F;
			this.fldTotalInterest.Left = 8.375F;
			this.fldTotalInterest.Name = "fldTotalInterest";
			this.fldTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalInterest.Text = "0.00";
			this.fldTotalInterest.Top = 0.0625F;
			this.fldTotalInterest.Width = 1F;
			// 
			// fldTotalCost
			// 
			this.fldTotalCost.Height = 0.1875F;
			this.fldTotalCost.Left = 9.375F;
			this.fldTotalCost.Name = "fldTotalCost";
			this.fldTotalCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCost.Text = "0.00";
			this.fldTotalCost.Top = 0.0625F;
			this.fldTotalCost.Width = 0.75F;
			// 
			// lblSummaryPaymentType12
			// 
			this.lblSummaryPaymentType12.Height = 0.1875F;
			this.lblSummaryPaymentType12.HyperLink = null;
			this.lblSummaryPaymentType12.Left = 1.75F;
			this.lblSummaryPaymentType12.MultiLine = false;
			this.lblSummaryPaymentType12.Name = "lblSummaryPaymentType12";
			this.lblSummaryPaymentType12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType12.Text = null;
			this.lblSummaryPaymentType12.Top = 3.0625F;
			this.lblSummaryPaymentType12.Width = 2.5F;
			// 
			// lblSummaryPaymentType13
			// 
			this.lblSummaryPaymentType13.Height = 0.1875F;
			this.lblSummaryPaymentType13.HyperLink = null;
			this.lblSummaryPaymentType13.Left = 1.75F;
			this.lblSummaryPaymentType13.MultiLine = false;
			this.lblSummaryPaymentType13.Name = "lblSummaryPaymentType13";
			this.lblSummaryPaymentType13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType13.Text = null;
			this.lblSummaryPaymentType13.Top = 3.25F;
			this.lblSummaryPaymentType13.Width = 2.5F;
			// 
			// lblSummaryTotal12
			// 
			this.lblSummaryTotal12.Height = 0.1875F;
			this.lblSummaryTotal12.HyperLink = null;
			this.lblSummaryTotal12.Left = 8F;
			this.lblSummaryTotal12.Name = "lblSummaryTotal12";
			this.lblSummaryTotal12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal12.Text = null;
			this.lblSummaryTotal12.Top = 3.0625F;
			this.lblSummaryTotal12.Width = 1F;
			// 
			// lblSummaryTotal13
			// 
			this.lblSummaryTotal13.Height = 0.1875F;
			this.lblSummaryTotal13.HyperLink = null;
			this.lblSummaryTotal13.Left = 8F;
			this.lblSummaryTotal13.Name = "lblSummaryTotal13";
			this.lblSummaryTotal13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal13.Text = null;
			this.lblSummaryTotal13.Top = 3.25F;
			this.lblSummaryTotal13.Width = 1F;
			// 
			// lblSumPrin12
			// 
			this.lblSumPrin12.Height = 0.1875F;
			this.lblSumPrin12.HyperLink = null;
			this.lblSumPrin12.Left = 4.25F;
			this.lblSumPrin12.Name = "lblSumPrin12";
			this.lblSumPrin12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin12.Text = null;
			this.lblSumPrin12.Top = 3.0625F;
			this.lblSumPrin12.Width = 1F;
			// 
			// lblSumPrin13
			// 
			this.lblSumPrin13.Height = 0.1875F;
			this.lblSumPrin13.HyperLink = null;
			this.lblSumPrin13.Left = 4.25F;
			this.lblSumPrin13.Name = "lblSumPrin13";
			this.lblSumPrin13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin13.Text = null;
			this.lblSumPrin13.Top = 3.25F;
			this.lblSumPrin13.Width = 1F;
			// 
			// lblSumInt12
			// 
			this.lblSumInt12.Height = 0.1875F;
			this.lblSumInt12.HyperLink = null;
			this.lblSumInt12.Left = 6.25F;
			this.lblSumInt12.Name = "lblSumInt12";
			this.lblSumInt12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt12.Text = null;
			this.lblSumInt12.Top = 3.0625F;
			this.lblSumInt12.Width = 1F;
			// 
			// lblSumInt13
			// 
			this.lblSumInt13.Height = 0.1875F;
			this.lblSumInt13.HyperLink = null;
			this.lblSumInt13.Left = 6.25F;
			this.lblSumInt13.Name = "lblSumInt13";
			this.lblSumInt13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt13.Text = null;
			this.lblSumInt13.Top = 3.25F;
			this.lblSumInt13.Width = 1F;
			// 
			// lblSumCost12
			// 
			this.lblSumCost12.Height = 0.1875F;
			this.lblSumCost12.HyperLink = null;
			this.lblSumCost12.Left = 7.25F;
			this.lblSumCost12.Name = "lblSumCost12";
			this.lblSumCost12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost12.Text = null;
			this.lblSumCost12.Top = 3.0625F;
			this.lblSumCost12.Width = 0.75F;
			// 
			// lblSumCost13
			// 
			this.lblSumCost13.Height = 0.1875F;
			this.lblSumCost13.HyperLink = null;
			this.lblSumCost13.Left = 7.25F;
			this.lblSumCost13.Name = "lblSumCost13";
			this.lblSumCost13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost13.Text = null;
			this.lblSumCost13.Top = 3.25F;
			this.lblSumCost13.Width = 0.75F;
			// 
			// lblSumTax12
			// 
			this.lblSumTax12.Height = 0.1875F;
			this.lblSumTax12.HyperLink = null;
			this.lblSumTax12.Left = 5.25F;
			this.lblSumTax12.Name = "lblSumTax12";
			this.lblSumTax12.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax12.Text = null;
			this.lblSumTax12.Top = 3.0625F;
			this.lblSumTax12.Width = 1F;
			// 
			// lblSumTax13
			// 
			this.lblSumTax13.Height = 0.1875F;
			this.lblSumTax13.HyperLink = null;
			this.lblSumTax13.Left = 5.25F;
			this.lblSumTax13.Name = "lblSumTax13";
			this.lblSumTax13.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax13.Text = null;
			this.lblSumTax13.Top = 3.25F;
			this.lblSumTax13.Width = 1F;
			// 
			// lnSubtotal
			// 
			this.lnSubtotal.Height = 0F;
			this.lnSubtotal.Left = 2.5625F;
			this.lnSubtotal.LineWeight = 1F;
			this.lnSubtotal.Name = "lnSubtotal";
			this.lnSubtotal.Top = 3.625F;
			this.lnSubtotal.Width = 1.3125F;
			this.lnSubtotal.X1 = 2.5625F;
			this.lnSubtotal.X2 = 3.875F;
			this.lnSubtotal.Y1 = 3.625F;
			this.lnSubtotal.Y2 = 3.625F;
			// 
			// arUTStatusLists
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFakeHeader3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOutstanding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienRecord)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTADate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRTError)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSpecialTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSpecialText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienRecord;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptSLAllActivityDetailOB;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTADate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillNum;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRTError;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSpecialTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSpecialText;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummary1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderTax;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSummaryTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost13;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax12;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax13;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSubtotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFakeHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFakeHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFakeHeader4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFakeHeader5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFakeHeader6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFakeHeader7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFakeHeader8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFakeHeader3;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOutstanding;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGroupHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillNum;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupPayments;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGroupTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
	}
}
