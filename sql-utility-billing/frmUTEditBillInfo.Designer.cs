﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTEditBillInfo.
	/// </summary>
	partial class frmUTEditBillInfo : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRE;
		public fecherFoundation.FCLabel lblRE;
		public fecherFoundation.FCComboBox cmbSearchType;
		public fecherFoundation.FCLabel lblSearchType;
		public fecherFoundation.FCGrid vsSearch;
		public fecherFoundation.FCPanel fraSearch;
		public fecherFoundation.FCFrame fraGetAccount;
		public fecherFoundation.FCButton cmdGetAccountNumber;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCLabel lblInstructions1;
		public fecherFoundation.FCFrame fraSearchCriteria;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCButton cmdSearch;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCLabel lblLastAccount;
		public fecherFoundation.FCTextBox txtHold;
		public fecherFoundation.FCFrame fraEditInfo;
		public FCGrid vsEditInfo;
		public FCGrid vsYearInfo;
		public fecherFoundation.FCLabel lblSearchListInstruction;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessClearLienElig;
		public fecherFoundation.FCToolStripMenuItem mnuProcessClearSearch;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessGetAccount;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTEditBillInfo));
            this.cmbRE = new fecherFoundation.FCComboBox();
            this.lblRE = new fecherFoundation.FCLabel();
            this.cmbSearchType = new fecherFoundation.FCComboBox();
            this.lblSearchType = new fecherFoundation.FCLabel();
            this.vsSearch = new fecherFoundation.FCGrid();
            this.txtHold = new fecherFoundation.FCTextBox();
            this.fraSearch = new fecherFoundation.FCPanel();
            this.fraSearchCriteria = new fecherFoundation.FCFrame();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtSearch = new fecherFoundation.FCTextBox();
            this.cmdSearch = new fecherFoundation.FCButton();
            this.cmdClear = new fecherFoundation.FCButton();
            this.fraGetAccount = new fecherFoundation.FCFrame();
            this.cmdGetAccountNumber = new fecherFoundation.FCButton();
            this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
            this.lblInstructions1 = new fecherFoundation.FCLabel();
            this.lblLastAccount = new fecherFoundation.FCLabel();
            this.fraEditInfo = new fecherFoundation.FCFrame();
            this.vsEditInfo = new fecherFoundation.FCGrid();
            this.vsYearInfo = new fecherFoundation.FCGrid();
            this.lblSearchListInstruction = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessClearLienElig = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessClearSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessGetAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdProcessGetAccount = new fecherFoundation.FCButton();
            this.cmdProcessClearSearch = new fecherFoundation.FCButton();
            this.cmdProcessSearch = new fecherFoundation.FCButton();
            this.cmdProcessClearLienElig = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
            this.vsSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearch)).BeginInit();
            this.fraSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearchCriteria)).BeginInit();
            this.fraSearchCriteria.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraGetAccount)).BeginInit();
            this.fraGetAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEditInfo)).BeginInit();
            this.fraEditInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsEditInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsYearInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessGetAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearLienElig)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessGetAccount);
            this.BottomPanel.Location = new System.Drawing.Point(0, 790);
            this.BottomPanel.Size = new System.Drawing.Size(998, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.lblSearchListInstruction);
            this.ClientArea.Controls.Add(this.fraSearch);
            this.ClientArea.Controls.Add(this.fraEditInfo);
            this.ClientArea.Controls.Add(this.vsSearch);
            this.ClientArea.Size = new System.Drawing.Size(998, 730);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdProcessClearLienElig);
            this.TopPanel.Controls.Add(this.cmdProcessSearch);
            this.TopPanel.Controls.Add(this.cmdProcessClearSearch);
            this.TopPanel.Size = new System.Drawing.Size(998, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessClearSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessSearch, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessClearLienElig, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(225, 30);
            this.HeaderText.Text = "Edit Bill Information";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbRE
            // 
            this.cmbRE.Items.AddRange(new object[] {
            "Real Estate",
            "Personal Property"});
            this.cmbRE.Location = new System.Drawing.Point(251, 116);
            this.cmbRE.Name = "cmbRE";
            this.cmbRE.Size = new System.Drawing.Size(199, 40);
            this.cmbRE.TabIndex = 15;
            this.cmbRE.Text = "Real Estate";
            this.ToolTip1.SetToolTip(this.cmbRE, null);
            this.cmbRE.Visible = false;
            // 
            // lblRE
            // 
            this.lblRE.AutoSize = true;
            this.lblRE.Location = new System.Drawing.Point(20, 130);
            this.lblRE.Name = "lblRE";
            this.lblRE.Size = new System.Drawing.Size(91, 15);
            this.lblRE.TabIndex = 16;
            this.lblRE.Text = "REAL ESTATE";
            this.ToolTip1.SetToolTip(this.lblRE, null);
            this.lblRE.Visible = false;
            // 
            // cmbSearchType
            // 
            this.cmbSearchType.Items.AddRange(new object[] {
            "Name",
            "Street Name",
            "Map / Lot"});
            this.cmbSearchType.Location = new System.Drawing.Point(170, 30);
            this.cmbSearchType.Name = "cmbSearchType";
            this.cmbSearchType.Size = new System.Drawing.Size(280, 40);
            this.cmbSearchType.TabIndex = 1;
            this.cmbSearchType.Text = "Name";
            this.ToolTip1.SetToolTip(this.cmbSearchType, null);
            // 
            // lblSearchType
            // 
            this.lblSearchType.AutoSize = true;
            this.lblSearchType.Location = new System.Drawing.Point(20, 44);
            this.lblSearchType.Name = "lblSearchType";
            this.lblSearchType.Size = new System.Drawing.Size(95, 15);
            this.lblSearchType.TabIndex = 3;
            this.lblSearchType.Text = "SEARCH TYPE";
            this.ToolTip1.SetToolTip(this.lblSearchType, null);
            // 
            // vsSearch
            // 
            this.vsSearch.Cols = 5;
            this.vsSearch.Controls.Add(this.txtHold);
            this.vsSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsSearch.FixedCols = 0;
            this.vsSearch.Location = new System.Drawing.Point(651, 14);
            this.vsSearch.Name = "vsSearch";
            this.vsSearch.RowHeadersVisible = false;
            this.vsSearch.Rows = 1;
            this.vsSearch.ShowFocusCell = false;
            this.vsSearch.Size = new System.Drawing.Size(114, 56);
            this.vsSearch.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.vsSearch, null);
            this.vsSearch.Visible = false;
            this.vsSearch.CurrentCellChanged += new System.EventHandler(this.vsSearch_RowColChange);
            this.vsSearch.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsSearch_MouseMoveEvent);
            this.vsSearch.ColumnHeaderMouseClick += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsSearch_BeforeSort);
            this.vsSearch.Click += new System.EventHandler(this.vsSearch_ClickEvent);
            this.vsSearch.DoubleClick += new System.EventHandler(this.vsSearch_DblClick);
            this.vsSearch.Resize += new System.EventHandler(this.vsSearch_AfterUserResize);
            this.vsSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.vsSearch_KeyDownEvent);
            // 
            // txtHold
            // 
            this.txtHold.BackColor = System.Drawing.SystemColors.Window;
            this.txtHold.Location = new System.Drawing.Point(-23, -12);
            this.txtHold.Name = "txtHold";
            this.txtHold.Size = new System.Drawing.Size(132, 40);
            this.txtHold.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtHold, null);
            this.txtHold.Visible = false;
            // 
            // fraSearch
            // 
            this.fraSearch.AppearanceKey = "groupBoxNoBorders";
            this.fraSearch.Controls.Add(this.fraSearchCriteria);
            this.fraSearch.Controls.Add(this.fraGetAccount);
            this.fraSearch.Controls.Add(this.lblLastAccount);
            this.fraSearch.Name = "fraSearch";
            this.fraSearch.Size = new System.Drawing.Size(532, 535);
            this.fraSearch.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.fraSearch, null);
            this.fraSearch.Visible = false;
            // 
            // fraSearchCriteria
            // 
            this.fraSearchCriteria.Controls.Add(this.Frame4);
            this.fraSearchCriteria.Controls.Add(this.cmbSearchType);
            this.fraSearchCriteria.Controls.Add(this.lblSearchType);
            this.fraSearchCriteria.Location = new System.Drawing.Point(20, 286);
            this.fraSearchCriteria.Name = "fraSearchCriteria";
            this.fraSearchCriteria.Size = new System.Drawing.Size(470, 248);
            this.fraSearchCriteria.TabIndex = 1;
            this.fraSearchCriteria.Text = "Search";
            this.ToolTip1.SetToolTip(this.fraSearchCriteria, null);
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.txtSearch);
            this.Frame4.Controls.Add(this.cmdSearch);
            this.Frame4.Controls.Add(this.cmdClear);
            this.Frame4.Location = new System.Drawing.Point(20, 90);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(274, 136);
            this.Frame4.TabIndex = 2;
            this.Frame4.Text = "Search For";
            this.ToolTip1.SetToolTip(this.Frame4, null);
            // 
            // txtSearch
            // 
            this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearch.Location = new System.Drawing.Point(20, 30);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(234, 40);
            this.ToolTip1.SetToolTip(this.txtSearch, null);
            this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
            // 
            // cmdSearch
            // 
            this.cmdSearch.AppearanceKey = "actionButton";
            this.cmdSearch.ForeColor = System.Drawing.Color.White;
            this.cmdSearch.Location = new System.Drawing.Point(176, 82);
            this.cmdSearch.Name = "cmdSearch";
            this.cmdSearch.Size = new System.Drawing.Size(78, 40);
            this.cmdSearch.TabIndex = 2;
            this.cmdSearch.Text = "Search";
            this.ToolTip1.SetToolTip(this.cmdSearch, null);
            this.cmdSearch.Visible = false;
            this.cmdSearch.Click += new System.EventHandler(this.cmdSearch_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.AppearanceKey = "actionButton";
            this.cmdClear.ForeColor = System.Drawing.Color.White;
            this.cmdClear.Location = new System.Drawing.Point(20, 82);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(141, 40);
            this.cmdClear.TabIndex = 1;
            this.cmdClear.Text = "Clear Search";
            this.ToolTip1.SetToolTip(this.cmdClear, null);
            this.cmdClear.Visible = false;
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // fraGetAccount
            // 
            this.fraGetAccount.Controls.Add(this.cmdGetAccountNumber);
            this.fraGetAccount.Controls.Add(this.cmbRE);
            this.fraGetAccount.Controls.Add(this.lblRE);
            this.fraGetAccount.Controls.Add(this.txtGetAccountNumber);
            this.fraGetAccount.Controls.Add(this.lblInstructions1);
            this.fraGetAccount.Location = new System.Drawing.Point(20, 30);
            this.fraGetAccount.Name = "fraGetAccount";
            this.fraGetAccount.Size = new System.Drawing.Size(470, 236);
            this.fraGetAccount.TabIndex = 2;
            this.fraGetAccount.Text = "Last Account Accessed";
            this.ToolTip1.SetToolTip(this.fraGetAccount, null);
            // 
            // cmdGetAccountNumber
            // 
            this.cmdGetAccountNumber.AppearanceKey = "actionButton";
            this.cmdGetAccountNumber.ForeColor = System.Drawing.Color.White;
            this.cmdGetAccountNumber.Location = new System.Drawing.Point(332, 176);
            this.cmdGetAccountNumber.Name = "cmdGetAccountNumber";
            this.cmdGetAccountNumber.Size = new System.Drawing.Size(118, 40);
            this.cmdGetAccountNumber.TabIndex = 14;
            this.cmdGetAccountNumber.Text = "Get Account";
            this.ToolTip1.SetToolTip(this.cmdGetAccountNumber, null);
            this.cmdGetAccountNumber.Visible = false;
            this.cmdGetAccountNumber.Click += new System.EventHandler(this.cmdGetAccountNumber_Click);
            // 
            // txtGetAccountNumber
            // 
            this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtGetAccountNumber.Location = new System.Drawing.Point(251, 30);
            this.txtGetAccountNumber.Name = "txtGetAccountNumber";
            this.txtGetAccountNumber.Size = new System.Drawing.Size(199, 40);
            this.txtGetAccountNumber.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtGetAccountNumber, null);
            this.txtGetAccountNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtGetAccountNumber_KeyDown);
            this.txtGetAccountNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGetAccountNumber_KeyPress);
            // 
            // lblInstructions1
            // 
            this.lblInstructions1.Location = new System.Drawing.Point(20, 30);
            this.lblInstructions1.Name = "lblInstructions1";
            this.lblInstructions1.Size = new System.Drawing.Size(161, 66);
            this.lblInstructions1.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.lblInstructions1, null);
            // 
            // lblLastAccount
            // 
            this.lblLastAccount.AutoSize = true;
            this.lblLastAccount.Location = new System.Drawing.Point(330, 11);
            this.lblLastAccount.Name = "lblLastAccount";
            this.lblLastAccount.Size = new System.Drawing.Size(4, 14);
            this.lblLastAccount.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.lblLastAccount, null);
            // 
            // fraEditInfo
            // 
            this.fraEditInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraEditInfo.AppearanceKey = "groupBoxNoBorders";
            this.fraEditInfo.Controls.Add(this.vsEditInfo);
            this.fraEditInfo.Controls.Add(this.vsYearInfo);
            this.fraEditInfo.Location = new System.Drawing.Point(30, 30);
            this.fraEditInfo.Name = "fraEditInfo";
            this.fraEditInfo.Size = new System.Drawing.Size(949, 697);
            this.fraEditInfo.TabIndex = 1;
            this.fraEditInfo.Text = "Edit Account Information";
            this.ToolTip1.SetToolTip(this.fraEditInfo, null);
            this.fraEditInfo.Visible = false;
            // 
            // vsEditInfo
            // 
            this.vsEditInfo.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsEditInfo.ColumnHeadersVisible = false;
            this.vsEditInfo.ExtendLastCol = true;
            this.vsEditInfo.FixedCols = 0;
            this.vsEditInfo.FixedRows = 0;
            this.vsEditInfo.Location = new System.Drawing.Point(0, 353);
            this.vsEditInfo.Name = "vsEditInfo";
            this.vsEditInfo.RowHeadersVisible = false;
            this.vsEditInfo.Rows = 10;
            this.vsEditInfo.ShowFocusCell = false;
            this.vsEditInfo.Size = new System.Drawing.Size(942, 325);
            this.ToolTip1.SetToolTip(this.vsEditInfo, null);
            this.vsEditInfo.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsEditInfo_KeyDownEdit);
            this.vsEditInfo.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsEditInfo_AfterEdit);
            this.vsEditInfo.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsEditInfo_ChangeEdit);
            this.vsEditInfo.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsEditInfo_MouseMoveEvent);
            this.vsEditInfo.CurrentCellChanged += new System.EventHandler(this.vsEditInfo_RowColChange);
            this.vsEditInfo.Enter += new System.EventHandler(this.vsEditInfo_Enter);
            // 
            // vsYearInfo
            // 
            this.vsYearInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsYearInfo.Cols = 4;
            this.vsYearInfo.FixedCols = 0;
            this.vsYearInfo.Location = new System.Drawing.Point(0, 30);
            this.vsYearInfo.Name = "vsYearInfo";
            this.vsYearInfo.RowHeadersVisible = false;
            this.vsYearInfo.Rows = 1;
            this.vsYearInfo.ShowFocusCell = false;
            this.vsYearInfo.Size = new System.Drawing.Size(942, 300);
            this.vsYearInfo.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.vsYearInfo, null);
            this.vsYearInfo.CurrentCellChanged += new System.EventHandler(this.vsYearInfo_RowColChange);
            this.vsYearInfo.Enter += new System.EventHandler(this.vsYearInfo_Enter);
            // 
            // lblSearchListInstruction
            // 
            this.lblSearchListInstruction.Location = new System.Drawing.Point(30, 30);
            this.lblSearchListInstruction.Name = "lblSearchListInstruction";
            this.lblSearchListInstruction.Size = new System.Drawing.Size(515, 22);
            this.ToolTip1.SetToolTip(this.lblSearchListInstruction, null);
            this.lblSearchListInstruction.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessClearLienElig,
            this.mnuProcessClearSearch,
            this.mnuProcessSearch,
            this.mnuFileSave,
            this.mnuProcessGetAccount,
            this.mnuProcessSeperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessClearLienElig
            // 
            this.mnuProcessClearLienElig.Index = 0;
            this.mnuProcessClearLienElig.Name = "mnuProcessClearLienElig";
            this.mnuProcessClearLienElig.Text = "Clear Lien Eligibility";
            this.mnuProcessClearLienElig.Visible = false;
            this.mnuProcessClearLienElig.Click += new System.EventHandler(this.mnuProcessClearLienElig_Click);
            // 
            // mnuProcessClearSearch
            // 
            this.mnuProcessClearSearch.Index = 1;
            this.mnuProcessClearSearch.Name = "mnuProcessClearSearch";
            this.mnuProcessClearSearch.Text = "Clear Search";
            this.mnuProcessClearSearch.Click += new System.EventHandler(this.mnuProcessClearSearch_Click);
            // 
            // mnuProcessSearch
            // 
            this.mnuProcessSearch.Index = 2;
            this.mnuProcessSearch.Name = "mnuProcessSearch";
            this.mnuProcessSearch.Text = "Search";
            this.mnuProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 3;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Visible = false;
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuProcessGetAccount
            // 
            this.mnuProcessGetAccount.Index = 4;
            this.mnuProcessGetAccount.Name = "mnuProcessGetAccount";
            this.mnuProcessGetAccount.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessGetAccount.Text = "Process";
            this.mnuProcessGetAccount.Click += new System.EventHandler(this.mnuProcessGetAccount_Click);
            // 
            // mnuProcessSeperator
            // 
            this.mnuProcessSeperator.Index = 5;
            this.mnuProcessSeperator.Name = "mnuProcessSeperator";
            this.mnuProcessSeperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 6;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcessGetAccount
            // 
            this.cmdProcessGetAccount.AppearanceKey = "acceptButton";
            this.cmdProcessGetAccount.Location = new System.Drawing.Point(234, 30);
            this.cmdProcessGetAccount.Name = "cmdProcessGetAccount";
            this.cmdProcessGetAccount.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessGetAccount.Size = new System.Drawing.Size(100, 48);
            this.cmdProcessGetAccount.Text = "Process";
            this.cmdProcessGetAccount.Click += new System.EventHandler(this.mnuProcessGetAccount_Click);
            // 
            // cmdProcessClearSearch
            // 
            this.cmdProcessClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessClearSearch.Location = new System.Drawing.Point(878, 29);
            this.cmdProcessClearSearch.Name = "cmdProcessClearSearch";
            this.cmdProcessClearSearch.Size = new System.Drawing.Size(92, 24);
            this.cmdProcessClearSearch.TabIndex = 1;
            this.cmdProcessClearSearch.Text = "Clear Search";
            this.cmdProcessClearSearch.Click += new System.EventHandler(this.mnuProcessClearSearch_Click);
            // 
            // cmdProcessSearch
            // 
            this.cmdProcessSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessSearch.ImageSource = "button-search";
            this.cmdProcessSearch.Location = new System.Drawing.Point(814, 29);
            this.cmdProcessSearch.Name = "cmdProcessSearch";
            this.cmdProcessSearch.Size = new System.Drawing.Size(81, 24);
            this.cmdProcessSearch.TabIndex = 2;
            this.cmdProcessSearch.Text = "Search";
            this.cmdProcessSearch.Click += new System.EventHandler(this.mnuProcessSearch_Click);
            // 
            // cmdProcessClearLienElig
            // 
            this.cmdProcessClearLienElig.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessClearLienElig.Location = new System.Drawing.Point(674, 29);
            this.cmdProcessClearLienElig.Name = "cmdProcessClearLienElig";
            this.cmdProcessClearLienElig.Size = new System.Drawing.Size(134, 24);
            this.cmdProcessClearLienElig.TabIndex = 3;
            this.cmdProcessClearLienElig.Text = "Clear Lien Eligibility";
            this.cmdProcessClearLienElig.Visible = false;
            this.cmdProcessClearLienElig.Click += new System.EventHandler(this.mnuProcessClearLienElig_Click);
            // 
            // frmUTEditBillInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(998, 898);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmUTEditBillInfo";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit Bill Information";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmUTEditBillInfo_Load);
            this.Activated += new System.EventHandler(this.frmUTEditBillInfo_Activated);
            this.Resize += new System.EventHandler(this.frmUTEditBillInfo_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTEditBillInfo_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTEditBillInfo_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
            this.vsSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraSearch)).EndInit();
            this.fraSearch.ResumeLayout(false);
            this.fraSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSearchCriteria)).EndInit();
            this.fraSearchCriteria.ResumeLayout(false);
            this.fraSearchCriteria.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraGetAccount)).EndInit();
            this.fraGetAccount.ResumeLayout(false);
            this.fraGetAccount.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdGetAccountNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEditInfo)).EndInit();
            this.fraEditInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsEditInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsYearInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessGetAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessClearLienElig)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessGetAccount;
		private FCButton cmdProcessSearch;
		private FCButton cmdProcessClearSearch;
		private FCButton cmdProcessClearLienElig;
	}
}
