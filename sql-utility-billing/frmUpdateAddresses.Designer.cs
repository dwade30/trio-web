﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUpdateAddresses.
	/// </summary>
	partial class frmUpdateAddresses : BaseForm
	{
		public fecherFoundation.FCPanel fraGrid;
		public fecherFoundation.FCGrid vsDemand;
		public fecherFoundation.FCLabel lblValidateInstruction;
		public fecherFoundation.FCComboBox cboRateKeys;
		public fecherFoundation.FCLabel lblRateKey;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelect;
		public fecherFoundation.FCToolStripMenuItem mnuFileUnselect;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator1;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUpdateAddresses));
			this.fraGrid = new fecherFoundation.FCPanel();
			this.vsDemand = new fecherFoundation.FCGrid();
			this.lblValidateInstruction = new fecherFoundation.FCLabel();
			this.cboRateKeys = new fecherFoundation.FCComboBox();
			this.lblRateKey = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSelect = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileUnselect = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdFileUnselect = new fecherFoundation.FCButton();
			this.cmdFileSelect = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).BeginInit();
			this.fraGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileUnselect)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelect)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 568);
			this.BottomPanel.Size = new System.Drawing.Size(670, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraGrid);
			this.ClientArea.Controls.Add(this.cboRateKeys);
			this.ClientArea.Controls.Add(this.lblRateKey);
			this.ClientArea.Size = new System.Drawing.Size(670, 508);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSelect);
			this.TopPanel.Controls.Add(this.cmdFileUnselect);
			this.TopPanel.Size = new System.Drawing.Size(670, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileUnselect, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelect, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(255, 30);
			this.HeaderText.Text = "Update Bill Addresses";
			// 
			// fraGrid
			// 
			this.fraGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraGrid.AppearanceKey = "groupBoxNoBorders";
			this.fraGrid.Controls.Add(this.vsDemand);
			this.fraGrid.Controls.Add(this.lblValidateInstruction);
			this.fraGrid.Location = new System.Drawing.Point(0, 90);
			this.fraGrid.Name = "fraGrid";
			this.fraGrid.Size = new System.Drawing.Size(640, 415);
			this.fraGrid.TabIndex = 2;
			this.fraGrid.Visible = false;
			// 
			// vsDemand
			// 
			this.vsDemand.AllowSelection = false;
			this.vsDemand.AllowUserToResizeColumns = false;
			this.vsDemand.AllowUserToResizeRows = false;
			this.vsDemand.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsDemand.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDemand.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDemand.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDemand.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.BackColorSel = System.Drawing.Color.Empty;
			this.vsDemand.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDemand.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsDemand.ColumnHeadersHeight = 30;
			this.vsDemand.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDemand.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsDemand.DragIcon = null;
			this.vsDemand.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDemand.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.vsDemand.ExtendLastCol = true;
			this.vsDemand.FixedCols = 0;
			this.vsDemand.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.FrozenCols = 0;
			this.vsDemand.GridColor = System.Drawing.Color.Empty;
			this.vsDemand.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.Location = new System.Drawing.Point(30, 60);
			this.vsDemand.Name = "vsDemand";
			this.vsDemand.ReadOnly = true;
			this.vsDemand.RowHeadersVisible = false;
			this.vsDemand.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDemand.RowHeightMin = 0;
			this.vsDemand.Rows = 1;
			this.vsDemand.ScrollTipText = null;
			this.vsDemand.ShowColumnVisibilityMenu = false;
			this.vsDemand.Size = new System.Drawing.Size(590, 335);
			this.vsDemand.StandardTab = true;
			this.vsDemand.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDemand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDemand.TabIndex = 1;
			this.vsDemand.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDemand_AfterEdit);
			this.vsDemand.CurrentCellChanged += new System.EventHandler(this.vsDemand_RowColChange);
			this.vsDemand.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDemand_MouseMoveEvent);
			this.vsDemand.DoubleClick += new System.EventHandler(this.vsDemand_DblClick);
			// 
			// lblValidateInstruction
			// 
			this.lblValidateInstruction.Location = new System.Drawing.Point(30, 0);
			this.lblValidateInstruction.Name = "lblValidateInstruction";
			this.lblValidateInstruction.Size = new System.Drawing.Size(596, 30);
			this.lblValidateInstruction.TabIndex = 0;
			this.lblValidateInstruction.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cboRateKeys
			// 
			this.cboRateKeys.AutoSize = false;
			this.cboRateKeys.BackColor = System.Drawing.SystemColors.Window;
			this.cboRateKeys.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboRateKeys.FormattingEnabled = true;
			this.cboRateKeys.Location = new System.Drawing.Point(189, 30);
			this.cboRateKeys.Name = "cboRateKeys";
			this.cboRateKeys.Size = new System.Drawing.Size(423, 40);
			this.cboRateKeys.TabIndex = 1;
			this.cboRateKeys.SelectedIndexChanged += new System.EventHandler(this.cboRateKeys_SelectedIndexChanged);
			// 
			// lblRateKey
			// 
			this.lblRateKey.Location = new System.Drawing.Point(30, 44);
			this.lblRateKey.Name = "lblRateKey";
			this.lblRateKey.Size = new System.Drawing.Size(145, 18);
			this.lblRateKey.TabIndex = 0;
			this.lblRateKey.Text = "RATE RECORD";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSelect,
				this.mnuFileUnselect,
				this.mnuFileSeperator1,
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileSelect
			// 
			this.mnuFileSelect.Index = 0;
			this.mnuFileSelect.Name = "mnuFileSelect";
			this.mnuFileSelect.Text = "Select All";
			this.mnuFileSelect.Click += new System.EventHandler(this.mnuFileSelect_Click);
			// 
			// mnuFileUnselect
			// 
			this.mnuFileUnselect.Index = 1;
			this.mnuFileUnselect.Name = "mnuFileUnselect";
			this.mnuFileUnselect.Text = "Unselect All";
			this.mnuFileUnselect.Click += new System.EventHandler(this.mnuFileUnselect_Click);
			// 
			// mnuFileSeperator1
			// 
			this.mnuFileSeperator1.Index = 2;
			this.mnuFileSeperator1.Name = "mnuFileSeperator1";
			this.mnuFileSeperator1.Text = "-";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 3;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 5;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(293, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// cmdFileUnselect
			// 
			this.cmdFileUnselect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileUnselect.AppearanceKey = "toolbarButton";
			this.cmdFileUnselect.Location = new System.Drawing.Point(552, 29);
			this.cmdFileUnselect.Name = "cmdFileUnselect";
			this.cmdFileUnselect.Size = new System.Drawing.Size(90, 24);
			this.cmdFileUnselect.TabIndex = 1;
			this.cmdFileUnselect.Text = "Unselect All";
			this.cmdFileUnselect.Click += new System.EventHandler(this.mnuFileUnselect_Click);
			// 
			// cmdFileSelect
			// 
			this.cmdFileSelect.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelect.AppearanceKey = "toolbarButton";
			this.cmdFileSelect.Location = new System.Drawing.Point(474, 29);
			this.cmdFileSelect.Name = "cmdFileSelect";
			this.cmdFileSelect.Size = new System.Drawing.Size(72, 24);
			this.cmdFileSelect.TabIndex = 2;
			this.cmdFileSelect.Text = "Select All";
			this.cmdFileSelect.Click += new System.EventHandler(this.mnuFileSelect_Click);
			// 
			// frmUpdateAddresses
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(670, 676);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUpdateAddresses";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Update Bill Addresses";
			this.Load += new System.EventHandler(this.frmUpdateAddresses_Load);
			this.Activated += new System.EventHandler(this.frmUpdateAddresses_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUpdateAddresses_KeyPress);
			this.Resize += new System.EventHandler(this.frmUpdateAddresses_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).EndInit();
			this.fraGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileUnselect)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelect)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdFileSelect;
		private FCButton cmdFileUnselect;
	}
}
