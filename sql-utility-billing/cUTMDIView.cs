//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;


namespace TWUT0000
{
	public class cUTMDIView
	{

        //=========================================================
        public bool customMenu = false;

        private cUTCommandHandler utcHandler;
		private cGenericCollection gcollCurrentMenu = new/*AsNew*/ cGenericCollection();
		private cMenuController menuController = new/*AsNew*/ cMenuController();
		private string strCurrentMenuName = "";
		private string strCurrentFormCaption = "";
		private string strPanelMessage;
		private string strBaseCaption;
		private string strApplicationName;
		private bool boolShowDemand;

		public delegate void MenuChangedEventHandler();
		public event MenuChangedEventHandler MenuChanged;
		public delegate void PanelChangedEventHandler();
		public event PanelChangedEventHandler PanelChanged;

		public cUTMDIView() : base()
		{
			strBaseCaption = "TRIO Software";
			strApplicationName = "Utility Billing";
			clsDRWrapper rsTemp = new/*AsNew*/ clsDRWrapper();
			utcHandler = new cUTCommandHandler(StaticSettings.GlobalCommandDispatcher);
			rsTemp.OpenRecordset("SELECT RegularIntMethod FROM UtilityBilling", "UtilityBilling");
			if (!rsTemp.EndOfFile()) {
				if (FCConvert.ToString(rsTemp.Get_Fields("RegularIntMethod"))=="D") {
					boolShowDemand = true;
				} else {
					boolShowDemand = false;
				}
			}
		}

		private void utcHandler_MenuChanged(string strMenuName)
		{
			SetMenu(strMenuName);
		}

		public void Reset()
		{
			SetMenu("UT");
		}

		public string PanelMessage
		{
			set
			{
				strPanelMessage = value;
				if (this.PanelChanged != null) this.PanelChanged();
			}

			get
			{
				return strPanelMessage;				
			}
		}

		public string CaptionPrefix
		{
			set
			{
				strBaseCaption = value;
			}
		}



		public string CurrentFormCaption
		{
			get
			{
					string CurrentFormCaption = "";
				CurrentFormCaption = strCurrentFormCaption;
				return CurrentFormCaption;
			}
		}

		public string CurrentMenuName
		{
			get
			{
					string CurrentMenuName = "";
				CurrentMenuName = strCurrentMenuName;
				return CurrentMenuName;
			}
		}

		public cGenericCollection CurrentMenu
		{
			get
			{
					cGenericCollection CurrentMenu = null;
				CurrentMenu = gcollCurrentMenu;
				return CurrentMenu;
			}
		}

		private string GenerateCaption(string strCaption)
		{
			string GenerateCaption = "";
			string strTemp = "";
			if (fecherFoundation.Strings.Trim(strBaseCaption)!="") {
				strTemp = fecherFoundation.Strings.Trim(strBaseCaption+" - "+strApplicationName+"   "+strCaption);
			} else {
				strTemp = fecherFoundation.Strings.Trim(strApplicationName+"   "+fecherFoundation.Strings.Trim(strCaption));
			}
			strCurrentFormCaption = strTemp;
			GenerateCaption = strTemp;
			return GenerateCaption;
		}

		public void ExecuteMenuCommand(string strMenu, int lngCode)
		{
			utcHandler.ExecuteMenuCommand(strMenu, (short)lngCode);
		}

		public void SetMenu(string strMenuName)
		{
			if (fecherFoundation.Strings.LCase(strMenuName)!=fecherFoundation.Strings.LCase(strCurrentMenuName)) {
				if (fecherFoundation.Strings.LCase(strCurrentMenuName)=="checkrec") {
					PanelMessage = "";
				}
			}
			switch (fecherFoundation.Strings.LCase(strMenuName)) {
				
				case "ut":
				case "main":
				{
					gcollCurrentMenu = GetMainMenu();
					break;
				}
				case "billing":
				{
					gcollCurrentMenu = GetBillingMenu();
					break;
				}
				case "collections":
				{
					gcollCurrentMenu = GetCollectionsMenu();
					break;
				}
				case "file":
				{
					gcollCurrentMenu = GetFileMenu();
					break;
				}
				case "printing":
				{
					gcollCurrentMenu = GetListingMenu();
					break;
				}
				case "meterfunctions":
				{
					gcollCurrentMenu = GetMeterFunctionsMenu();
					break;
				}
				case "30dn":
				{
					gcollCurrentMenu = Get30DNMenu();
					break;
				}
				case "lien":
				{
					gcollCurrentMenu = GetLienMenu();
					break;
				}
				case "maturity":
				{
					gcollCurrentMenu = GetLienMaturityMenu();
					break;
				}
				case "liennotice":
				{
					gcollCurrentMenu = GetLienNoticeMenu();
					break;
				}
				case "printmeter":
				{
					gcollCurrentMenu = GetPrintMeterMenu();
					break;
				}
                case "custom":
                    gcollCurrentMenu = GetCustomMenu();
                    break;
			} //end switch
			if (this.MenuChanged != null) this.MenuChanged();
		}

		private cGenericCollection GetMainMenu()
		{
			cGenericCollection GetMainMenu = null;
			int CurRow;
			string strTemp = "";
			int lngFCode;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();

			strCurrentMenuName = "UT";
			GenerateCaption("[Main]");
			CurRow = 1;
			cMenuChoice tItem;
			gcollMenu.ClearList();
			// Call gcollMenu.AddItem(menuController.CreateMenuItem("UT_Menu_File", 15, "File Maintenance", FILEMAINTENANCE, "M", "", CurRow))
			gcollMenu.AddItem( menuController.CreateMenuItem("UT_Account_Meter_Update", (int)UT_Commands.Account_Meter_Update, "Account/Meter Update", modMain.UTSECACCOUNTMETERUPDATE, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("UT_Menu_Billing_Process", (int)UT_Commands.Menu_BillingProcess, "Billing Process", modMain.UTSECBILLINGPROCESS, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("UT_Menu_Collection_Process", (int)UT_Commands.Menu_CollectionProcess, "Collection Process", modMain.UTSECCOLLECTIONPROCESS, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("UT_Menu_Printing", (int)UT_Commands.Menu_Printing, "Printing", modMain.UTSECPRINTING, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("UT_Table_File_Setup", (int)UT_Commands.TableFileSetup, "Table File Setup", modMain.UTSECEXTRACTFORREMOTEREADER, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("UT_Extract_For_Remote_Reader", (int)UT_Commands.ExtractForRemoteReader, "Extract for Remote Reader", modMain.UTSECEXTRACTFORREMOTEREADER, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("UT_Menu_File_Maintenance", (int)UT_Commands.Menu_FileMaintenance, "File Maintenance", modMain.UTSECFILEMAINTENANCE, "M", "", ref CurRow));            
            var custMenu = GetCustomMenu();
            if (custMenu.ItemCount() > 0)
            {
                gcollMenu.AddItem(menuController.CreateMenuItem("", (int)UT_Commands.Menu_Custom, "Custom", 0, "", "", ref CurRow));
            }
            GetMainMenu = gcollMenu;
			return GetMainMenu;
		}

		private cGenericCollection GetCollectionsMenu()
		{
			cGenericCollection GetCollectionsMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			cMenuChoice tItem;
			strCurrentMenuName = "Collections";
			GenerateCaption("[Collections]");
			CurRow = 1;
			gcollMenu.ClearList();
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.AccountStatus, "Status", modMain.UTSECVIEWSTATUS, "", "", ref CurRow));
			if (!modGlobalConstants.Statics.gboolCR) {
				tItem = menuController.CreateMenuItem("", (int)UT_Commands.PaymentsAdjustments, "Payments & Adjustments", modMain.UTSECPAYMENTSADJUSTMENTS, "", "", ref CurRow);
				if (modGlobalConstants.Statics.gboolCR) {
					tItem.Enabled = false;
				}
				gcollMenu.AddItem( tItem);
			}

            cMenuChoice mItem = null;
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.DailyAuditReport, "Daily Audit Report", modMain.UTSECDAILYAUDITREPORT, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LoadOfBackInformation, "Load of Back Information", modMain.UTSECLOADOFBACKINFORMATION, "", "", ref CurRow));
            mItem = menuController.CreateMenuItem("", (int) UT_Commands.MortgageHolder, "Mortgage Holder",
                modMain.UTSECMORTGAGEHOLDER, "", "", ref CurRow);
            if (mItem.Enabled)
            {
                gcollMenu.AddItem(mItem);
            }

            mItem = menuController.CreateMenuItem("", (int) UT_Commands.Menu_LienProcess, "Lien Process",
                modMain.UTSECLIENPROCESS, "", "", ref CurRow);
            if (mItem.Enabled)
            {
                gcollMenu.AddItem(mItem);
            }

            if (boolShowDemand) {
				gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ChargeInterest, "Charge Interest", modMain.UTSECCHARGEINTEREST, "", "", ref CurRow));
			}			
           
			GetCollectionsMenu = gcollMenu;
			return GetCollectionsMenu;
		}

		private cGenericCollection GetBillingMenu()
		{
			cGenericCollection GetBillingMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			cMenuChoice tItem;
			strCurrentMenuName = "Billing";
			GenerateCaption("[Billing]");
			CurRow = 1;
			gcollMenu.ClearList();
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ClearBooks, "Clear Book(s)", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.DataEntry, "Data Entry", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.CalculateEdit, "Calculate & Edit", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.CreateBillRecords, "Create Bill Records", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.PrintBills, "Print Bills", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ReprintBillCreationReport, "Reprint Bill Creation Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.PrintNotBilledReport, "Print Not Billed Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.CreateExtractFile, "Create Extract File", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ElectronicDataEntry, "Electronic Data Entry", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.BookStatus, "Book Status", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.FinalBilling, "Final Billing", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.EmailBills, "E-Mail Bills", 0, "", "", ref CurRow));
			if (modUTCalculations.Statics.gUTSettings.AllowAutoPay) {
				gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.CreateAutoPayFile, "Create Auto-Pay File", 0, "", "", ref CurRow));
			}

			if (modGlobalConstants.Statics.gboolInvCld) {
				gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.InvoiceCloudExport, "Invoice Cloud Export", 0, "", "", ref CurRow));
			}			
			GetBillingMenu = gcollMenu;
			return GetBillingMenu;
		}

		private cGenericCollection GetListingMenu()
		{
			cGenericCollection GetListingMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			strCurrentMenuName = "Listing";
			GenerateCaption("[Printing]");
			CurRow = 1;
			gcollMenu.ClearList();
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.AccountListing, "Account Listing", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.StatusLists, "Status Lists", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.AccountLabels, "Account Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.DeletedAccountsSummary, "Deleted Accounts Summary", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterReports, "Meter Reports", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.AnalysisReports, "Analysis Reports", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LienDischargeNotices, "Lien Discharge Notices", 0, "", "", ref CurRow));
			if (!(modUTFunctions.Statics.TownService == "S")) {
				gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.DisconnectEditReport, "Disconnect Edit Report", 0, "", "", ref CurRow));
				gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.DisconnectNotices, "Disconnect Notices", 0, "", "", ref CurRow));
			}
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.RateTableListing, "Rate Table Listing", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.RateKeyListing, "Rate Key Listing", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LoadBackReport, "Loadback Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.RedisplayDailyAudit, "Redisplay Daily Audit", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ReminderNotices, "Reminder Notices", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.BookPageReport, "Book Page Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.TaxAcquiredReport, "Tax Acquired Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.CommentsReport, "Comments Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.PaymentActivityReport, "Payment Activity Report", 0, "", "", ref CurRow));			
			GetListingMenu = gcollMenu;
			return GetListingMenu;            
		}

		public cGenericCollection GetFileMenu()
		{
			cGenericCollection GetFileMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			strCurrentMenuName = "File";
			GenerateCaption("[File Maintenance]");
			CurRow = 1;
			gcollMenu.ClearList();

			
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.Customize, "Customize", modMain.UTSECCUSTOMIZE, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.CheckDBStructure, "Check Database Structure", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.Menu_MeterFunctions, "Meter Functions", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.PurgePaidBills, "Purge Paid Bills", modMain.UTSECPURGEPAIDBILLS, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ReprintPurgeReport, "Reprint Purge Report", modMain.UTSECREPRINTPURGEREPORT, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.EditCustomBillTypes, "Edit Custom Bill Types", modMain.UTSECEDITCUSTOMBILLTYPES, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ConvertConsumptionFile, "Convert Consumption File", modMain.UTSECCONVERTCONSUMPTIONFILE, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.CreateDatabaseExtract, "Create Database Extract", modMain.UTSECCREATEDATABASEEXTRACT, "", "", ref CurRow));			
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.RateKeyUpdate, "Rate Key Update", modMain.UTSECRATEKEYUPDATE, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ClearCertifiedMailTable, "Clear Certified Mail Table", modMain.UTSECCLEARCERTFIEDMAILTABLE, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.EditBillInformation, "Edit Bill Information", modMain.UTSECEDITBILLINFORMATION, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ResetBills, "Reset Bills", modMain.UTSECRESETBILLS, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ResetFinalBills, "Reset Final Bills", modMain.UTSECRESETFINALBILLS, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.RemoveFromTaxAcquired, "Remove From Tax Acquired", modMain.UTSECREMOVEFROMTAXACQUIRED, "", "", ref CurRow));
			if (modUTCalculations.Statics.gboolSplitBills) {
				gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.SplitUTBills, "Split Bills", modMain.UTSECSPLITBILLS, "", "", ref CurRow));
			}
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.PermanentlyDeleteAccount, "Permanently Delete Account", modMain.UTSECPERMDELETEACCOUNT, "", "", ref CurRow));
			if (modUTCalculations.Statics.gUTSettings.AllowAutoPay) {
				gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.SetupAutoPay, "Setup Auto-Pay/ACH", 0, "", "", ref CurRow));
			}
			if (fecherFoundation.Strings.UCase(modGlobalConstants.Statics.MuniName)=="BANGOR") {
				gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.SyncWithREAccounts, "Sync w/ RE Accounts", modMain.UTSECPERMDELETEACCOUNT, "", "", ref CurRow));
			}			
			GetFileMenu = gcollMenu;
			return GetFileMenu;
		}

		public cGenericCollection GetMeterFunctionsMenu()
		{
			cGenericCollection GetMeterFunctionsMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			strCurrentMenuName = "MeterFunctions";
			GenerateCaption("[Meter Functions]");
			CurRow = 1;
			gcollMenu.ClearList();

			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LoadPreviousReadings, "Load Prev Readings", modMain.UTSECLOADPREVREADINGS, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterChangeOut, "Meter Change Out", modMain.UTSECMETERCHANGEOUT, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.UpdateSequence, "Update Sequence", modMain.UTSECCOLLECTIONPROCESS, "", "", ref CurRow));			
			GetMeterFunctionsMenu = gcollMenu;
			return GetMeterFunctionsMenu;
		}

		public cGenericCollection Get30DNMenu()
		{
			cGenericCollection Get30DNMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			strCurrentMenuName = "30DN";
			GenerateCaption("[30 Day Notice Process]");
			CurRow = 1;
			gcollMenu.ClearList();

			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.Print30DayNotices, "Print 30 Day Notices", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ThirtyDN_CertifiedMailFormLabels, "Certified Mailers & Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ApplyDemandFees, "Apply Demand Fees", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ThirtyDN_ReprintWaterLabels, "Reprint Water Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ThirtyDN_ReprintSewerLabels, "Reprint Sewer Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ThirtyDN_ReprintWCMForms, "Reprint Water CM Forms", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ThirtyDN_ReprintSCMForms, "Reprint Sewer CM Forms", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ReprintWaterDemandList, "Reprint Water Demand List", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ReprintSewerDemandList, "Reprint Sewer Demand List", 0, "", "", ref CurRow));
			
			Get30DNMenu = gcollMenu;
			return Get30DNMenu;
		}

		public cGenericCollection GetLienMenu()
		{
			cGenericCollection GetLienMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			strCurrentMenuName = "Lien";
			GenerateCaption("[Lien]");
			CurRow = 1;
			gcollMenu.ClearList();

			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LienEditReport, "Lien Edit Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.Menu_30DayNoticeProcess, "30 Day Notice Process", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.Menu_LienNotice, "Lien Process", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.Menu_LienMaturity, "Lien Maturity Process", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.EditLienBookPage, "Edit Lien Book / Page", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.EditLDNBookPage, "Edit LDN Book / Page", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.Reverse30DayNoticeFee, "Reverse 30 Day Notice Fee", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.RemoveFromLien, "Remove From Lien", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ReverseLienMaturityFee, "Reverse Lien Maturity Fee", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LienDateChart, "Lien Date Chart", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.TaxAcquired, "Tax Acquired", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.UpdateBillAddresses, "Update Bill Addresses", 0, "", "", ref CurRow));
			
			GetLienMenu = gcollMenu;
			return GetLienMenu;
		}

		public cGenericCollection GetLienMaturityMenu()
		{
			cGenericCollection GetLienMaturityMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			strCurrentMenuName = "Maturity";
			GenerateCaption("[Lien Maturity Process]");
			CurRow = 1;
			gcollMenu.ClearList();

			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.PrintLienMaturityNotices, "Print Lien Maturity Notices", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LMN_CertifiedMailFormsLabels, "Certified Mailers & Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ApplyMaturityFees, "Apply Maturity Fees", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LMN_ReprintWaterLabels, "Reprint Water Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LMN_ReprintSewerLabels, "Reprint Sewer Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LMN_ReprintWaterCMForms, "Reprint Water CM Forms", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LMN_ReprintSewerCMForms, "Reprint Sewer CM Forms", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ReprintWaterMatFeeList, "Reprint Water Mat Fee List", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ReprintSewerMatFeeList, "Reprint Sewer Mat Fee List", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.PrintBankruptcyReport, "Print Bankruptcy Report", 0, "", "", ref CurRow));
			
			GetLienMaturityMenu = gcollMenu;
			return GetLienMaturityMenu;
		}

		public cGenericCollection GetLienNoticeMenu()
		{
			cGenericCollection GetLienNoticeMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			strCurrentMenuName = "LIENNOTICE";
			GenerateCaption("[Lien Notice Process]");
			CurRow = 1;
			gcollMenu.ClearList();

			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.PrintLienNotices, "Print Lien Notices", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LN_CertifiedMailFormsLabels, "Certified Mailers & Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.TransferBillsToLienStatus, "Transfer Bills to Lien Status", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LN_ReprintWaterLabels, "Reprint Water Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LN_ReprintSewerLabels, "Reprint Sewer Labels", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LN_ReprintWaterCMForms, "Reprint Water CM Forms", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LN_ReprintWaterTransferList, "Reprint Water Transfer List", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.LN_ReprintSewerTransferList, "Reprint Sewer Transfer List", 0, "", "", ref CurRow));

			GetLienNoticeMenu = gcollMenu;
			return GetLienNoticeMenu;
		}

		public cGenericCollection GetPrintMeterMenu()
		{
			cGenericCollection GetPrintMeterMenu = null;
			int CurRow;
			cGenericCollection gcollMenu = new/*AsNew*/ cGenericCollection();
			strCurrentMenuName = "PrintMeter";
			GenerateCaption("[Print Meter]");
			CurRow = 1;
			gcollMenu.ClearList();

			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterReadingSlips, "Meter Reading Slips", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterExchangeList, "Meter Exchange List", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterCountByCategory, "Meter Count by Category", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterListWRateTables, "Meter List w/ Rate Tables", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterErrorReport, "Meter Error Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.HighestConsumptionReport, "Highest Consumption Report", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterChangeoutHistory, "Meter Change Out History", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.MeterReadingList, "Meter Reading List", 0, "", "", ref CurRow));
			gcollMenu.AddItem( menuController.CreateMenuItem("", (int)UT_Commands.ConsumptionHistoryReport, "Consumption History Report", 0, "", "", ref CurRow));
			
			GetPrintMeterMenu = gcollMenu;
			return GetPrintMeterMenu;
		}

        public cGenericCollection GetCustomMenu()
        {
            cGenericCollection gcollMenu = new cGenericCollection();
            strCurrentMenuName = "Custom";
            GenerateCaption("[Custom Programs]");
            int CurRow = 1;
            cMenuChoice tItem;
            gcollMenu.ClearList();
            //gcollMenu.AddItem(menuController.CreateMenuItem("UT_Account_Meter_Update", (int)UT_Commands.Account_Meter_Update, "Account/Meter Update", modMain.UTSECACCOUNTMETERUPDATE, "", "", ref CurRow));
            switch (modGlobalConstants.Statics.MuniName.ToLower())
            {
                case "mechanic falls sanitary":
                    gcollMenu.AddItem(menuController.CreateMenuItem("", (int)UT_Commands.Custom_MechanicFalls_Consumption, "Custom Consumption Calculation", 0, "", "", ref CurRow));
                    gcollMenu.AddItem(menuController.CreateMenuItem("", (int)UT_Commands.Custom_MechanicFalls_ConsumptionUpdate, "Custom Reading Update", 0, "", "", ref CurRow));
                    break;
                case "oakland":
                    gcollMenu.AddItem(menuController.CreateMenuItem("", (int)UT_Commands.Custom_Oakland_BadAccounts, "Bad Account Numbers", 0, "", "", ref CurRow));
                    break;
                case "lisbon":
                    gcollMenu.AddItem(menuController.CreateMenuItem("", (int)UT_Commands.Custom_Lisbon_SummerConsumptionCalc, "Summer Consumption Calculation", 0, "", "", ref CurRow));
                    gcollMenu.AddItem(menuController.CreateMenuItem("", (int)UT_Commands.Custom_Lisbon_UpdateWinterBilling, "Update Winter Billing", 0, "", "", ref CurRow));
                    break;
                case "brunswick sewer district":
                    gcollMenu.AddItem(menuController.CreateMenuItem("", (int)UT_Commands.Custom_BrunswickSewer_ImportExceptionList, "Import Exception List", 0, "", "", ref CurRow));
                    break;
                case "norway water":
                case "norway":
                    gcollMenu.AddItem(menuController.CreateMenuItem("", (int)UT_Commands.Custom_Norway_ZeroBillsByBook, "Zero Bills by Book", 0, "", "", ref CurRow));
                    break;
            }
            return gcollMenu;
        }
    }
}
