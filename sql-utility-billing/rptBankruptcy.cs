﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptBankruptcy.
	/// </summary>
	public partial class rptBankruptcy : BaseSectionReport
	{
		public static rptBankruptcy InstancePtr
		{
			get
			{
				return (rptBankruptcy)Sys.GetInstance(typeof(rptBankruptcy));
			}
		}

		protected rptBankruptcy _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		public rptBankruptcy()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.Name = "Bankruptcy Report";
        }
		// nObj = 1
		//   0	rptBankruptcy	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/16/2007              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/16/2007              *
		// ********************************************************
		string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		int lngTotalMortgageHolders;

		public void Init(string pstrSQL, bool modalDialog)
		{
			// this sub will set the SQL string
			strSQL = pstrSQL;
			this.Run(true);
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (strSQL != "")
			{
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				lngTotalMortgageHolders = rsData.RecordCount();
			}
			if (lngTotalMortgageHolders == 0)
			{
				// hide the headers if there are no records
				lblAcct.Visible = false;
				lblName.Visible = false;
				lblAddress.Visible = false;
			}
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			float lngHt;
			intRow = 0;
			lngHt = 270 / 1440F;
			if (Strings.Trim(fldAddress1.Text) != "")
			{
				fldAddress1.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress1.Top = 0;
			}
			if (Strings.Trim(fldAddress2.Text) != "")
			{
				fldAddress2.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress2.Top = 0;
			}
			if (Strings.Trim(fldAddress3.Text) != "")
			{
				fldAddress3.Top = lngHt * intRow;
				intRow += 1;
			}
			else
			{
				fldAddress3.Top = 0;
			}
			this.Detail.Height = intRow * lngHt + 200 / 1440f;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				if (!rsData.EndOfFile())
				{
					//FC:FINAL:MSH - can't implicitly convert from int to string
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					fldName.Text = FCConvert.ToString(rsData.Get_Fields("OwnerName"));
					fldAddress1.Text = rsData.Get_Fields_String("OAddress1");
					fldAddress2.Text = rsData.Get_Fields_String("OAddress2");
					fldAddress3.Text = rsData.Get_Fields_String("OAddress3");
					fldAddress4.Text = rsData.Get_Fields_String("OState") + " " + rsData.Get_Fields_String("OZip");
					// If Trim(rsData.Fields("OZip4")) <> "" Then
					// fldAddress4.Text = fldAddress4.Text & "-" & rsData.Fields("OZip4")
					// End If
					SetupFields();
					// move to the next record in the query
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error in Bankruptcy Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastUTBankruptcy");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastUTBankruptcy1.RDF"));
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblFooter.Text = "There were " + FCConvert.ToString(lngTotalMortgageHolders) + " accounts not processed.";
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		public void Save_Report()
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastUTBankruptcy");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastUTBankruptcy1.RDF"));
			}
			// Unload Me
		}

		private void rptBankruptcy_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptBankruptcy properties;
			//rptBankruptcy.Caption	= "Bankruptcy Report";
			//rptBankruptcy.Icon	= "rptBankruptcyReport.dsx":0000";
			//rptBankruptcy.Left	= 0;
			//rptBankruptcy.Top	= 0;
			//rptBankruptcy.Width	= 28680;
			//rptBankruptcy.Height	= 15690;
			//rptBankruptcy.StartUpPosition	= 3;
			//rptBankruptcy.SectionData	= "rptBankruptcyReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
