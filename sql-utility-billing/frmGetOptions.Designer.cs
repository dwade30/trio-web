﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmGetOptions.
	/// </summary>
	partial class frmGetOptions : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOption1;
		public fecherFoundation.FCLabel lbltion1;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCLabel lblDescription;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSaveContinue;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGetOptions));
			this.cmbOption1 = new fecherFoundation.FCComboBox();
			this.lbltion1 = new fecherFoundation.FCLabel();
			this.cmdOK = new fecherFoundation.FCButton();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.lblDescription = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveContinue = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 261);
			this.BottomPanel.Size = new System.Drawing.Size(424, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbOption1);
			this.ClientArea.Controls.Add(this.lbltion1);
			this.ClientArea.Controls.Add(this.cmdOK);
			this.ClientArea.Controls.Add(this.cmdCancel);
			this.ClientArea.Controls.Add(this.lblDescription);
			this.ClientArea.Size = new System.Drawing.Size(424, 201);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(424, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// cmbOption1
			// 
			this.cmbOption1.AutoSize = false;
			this.cmbOption1.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOption1.FormattingEnabled = true;
			this.cmbOption1.Location = new System.Drawing.Point(142, 80);
			this.cmbOption1.Name = "cmbOption1";
			this.cmbOption1.Size = new System.Drawing.Size(202, 40);
			this.cmbOption1.TabIndex = 2;
			// 
			// lbltion1
			// 
			this.lbltion1.AutoSize = true;
			this.lbltion1.Location = new System.Drawing.Point(30, 94);
			this.lbltion1.Name = "lbltion1";
			this.lbltion1.Size = new System.Drawing.Size(55, 15);
			this.lbltion1.TabIndex = 1;
			this.lbltion1.Text = "OPTION";
			// 
			// cmdOK
			// 
			this.cmdOK.AppearanceKey = "actionButton";
			this.cmdOK.ForeColor = System.Drawing.Color.White;
			this.cmdOK.Location = new System.Drawing.Point(30, 140);
			this.cmdOK.Name = "cmdOK";
			this.cmdOK.Size = new System.Drawing.Size(82, 40);
			this.cmdOK.TabIndex = 3;
			this.cmdOK.Text = "OK";
			this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(126, 140);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(89, 40);
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// lblDescription
			// 
			this.lblDescription.Location = new System.Drawing.Point(30, 30);
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Size = new System.Drawing.Size(373, 36);
			this.lblDescription.TabIndex = 0;
			this.lblDescription.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuSaveContinue,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSaveContinue
			// 
			this.mnuSaveContinue.Index = 0;
			this.mnuSaveContinue.Name = "mnuSaveContinue";
			this.mnuSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveContinue.Text = "Save & Continue";
			this.mnuSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(126, 31);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(181, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Save  & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
            // 
            // frmGetOptions
            // 
            this.AcceptButton = this.cmdSaveContinue;
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(424, 369);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmGetOptions";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "";
			this.Load += new System.EventHandler(this.frmGetOptions_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmGetOptions_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveContinue;
	}
}
