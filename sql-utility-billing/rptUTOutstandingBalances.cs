﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTOutstandingBalances.
	/// </summary>
	public partial class rptUTOutstandingBalances : BaseSectionReport
	{
		public rptUTOutstandingBalances()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outstanding Balance Report";
		}

		public static rptUTOutstandingBalances InstancePtr
		{
			get
			{
				return (rptUTOutstandingBalances)Sys.GetInstance(typeof(rptUTOutstandingBalances));
			}
		}

		protected rptUTOutstandingBalances _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUTOutstandingBalances	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/02/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/03/2004              *
		// ********************************************************
		string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[8 + 1];
		// 0 - Original, 1 - Payment, 2 - ?, 3 - Total Due, 4 - Prin, 5 - Tax, 6 - Int, 7 - Cost
		bool boolRTError;
		int lngCount;
		bool boolStarted;
		double dblPDTotal;
		bool boolAdjustedSummary;
		bool boolSummaryOnly;
		// this is for the summaries at the bottom
		double[] dblYearTotals = new double[2000 + 1];
		// billingyear - 19800
		double[,] dblPayments = new double[10 + 1, 6 + 1];
		// 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
		// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs, 4 - Total, 5 - Tax
		bool boolWater;
		string strWS = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				if (boolStarted)
				{
					eArgs.EOF = true;
				}
				else
				{
				}
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 0;
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "W";
			}
			boolSummaryOnly = FCConvert.CBool(frmUTStatusList.InstancePtr.chkSummaryOnly.CheckState == Wisej.Web.CheckState.Checked);
			if (boolSummaryOnly)
			{
				lblName.Visible = false;
				lblAccount.Visible = false;
			}
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolRTError = false;
			SetupFields();
			// Moves/shows the correct fields into the right places
			SetReportHeader();
			// Sets the titles and moves labels in the header
			boolStarted = false;
			strSQL = BuildSQL();
			// Generates the SQL String
			rsData.OpenRecordset(strSQL, modMain.DEFAULTDATABASE);
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause;
			string strREPPBill = "";
			string strREPPPayment = "";
			int intCT;
			string strSupp = "";
			if (rptUTOutstandingBalancesAll.InstancePtr.intSuppReportType == 1)
			{
				strSupp = " AND BillingYear MOD 10 > 1 ";
			}
			strWhereClause = "WHERE " + strWS + "LienRecordNumber = 0 And Service = 'B' OR Service = '" + strWS + "')" + strSupp;
			// this will query the rest of the criteria for the report except for the balance due
			for (intCT = 0; intCT <= frmUTStatusList.InstancePtr.vsWhere.Rows - 2; intCT++)
			{
				if (frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) != "" || frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2) != "")
				{
					switch (intCT)
					{
						case 0:
							{
								// Account Number
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										if (Conversion.Val(Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1))) == Conversion.Val(Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2))))
										{
											strTemp += "Account = " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
										}
										else
										{
											strTemp += "(Account BETWEEN " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " AND " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2) + ")";
										}
									}
									else
									{
										strTemp += "Account <= " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								}
								else
								{
									strTemp += "Account >= " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
								break;
							}
						case 2:
							{
								// Tax Year
								if (Strings.Trim(strTemp) != "")
								{
									strTemp += " AND ";
								}
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) != "")
								{
									if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										strTemp += "(BillingYear BETWEEN " + modExtraModules.FormatYear(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) + " AND " + modExtraModules.FormatYear(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) + ")";
									}
									else
									{
										strTemp += "BillingYear = " + modExtraModules.FormatYear(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1));
									}
								}
								else
								{
									strTemp += "BillingYear = " + modExtraModules.FormatYear(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2));
								}
								break;
							}
						default:
							{
								break;
								break;
							}
					}
					//end switch
				}
			}
			if (Strings.Trim(strTemp) != "")
			{
				strWhereClause += " AND " + strTemp;
			}
			if (modMain.Statics.gboolUTUseAsOfDate)
			{
				strTemp = "SELECT * FROM BillingMaster " + strWhereClause + " ORDER BY Name1, Account, BillNumber";
			}
			else
			{
				strTemp = "SELECT * FROM OutstandingBalance " + strWhereClause + " ORDER BY Name1, Account, BillNumber";
			}
			BuildSQL = strTemp;
			return BuildSQL;
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2;
			int lngHt;
			if (boolSummaryOnly)
			{
				lblBillDate.Visible = false;
				fldBillDate.Visible = false;
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldPaymentReceived.Visible = false;
				fldTaxDue.Visible = false;
				fldDue.Visible = false;
				lnHeader.Visible = false;
				lnTotals.Visible = false;
				fldType.Visible = false;
				Detail.Height = 0;
				return;
			}
			intRow = 2;
			lngHt = 270;
			lblBillDate.Visible = true;
			fldBillDate.Visible = true;
			// if the year is not shown, then make the name field smaller
			fldName.Width = fldBillDate.Left - (fldType.Left + fldType.Width);
		}

		private void BindFields()
		{
			var rsPayment = new clsDRWrapper();
			var rsRate = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill the information into the fields
                //clsDRWrapper rsPayment = new clsDRWrapper();
                //clsDRWrapper rsRE = new clsDRWrapper();
                //clsDRWrapper rsCalLien;
                //clsDRWrapper rsRate = new clsDRWrapper();
                string strTemp = "";
                double dblTotalPayment;
                double dblTotalAbate;
                double dblTotalRefundAbate;
                double dblCurInt;
                double dblLienCurInt;
                DateTime dtPaymentDate;
                bool boolREInfo;
                double dblPaymentRecieved;
                double dblXtraInt = 0;
                double dblTotalDue;
                TRYAGAIN: ;
                dblPaymentRecieved = 0;
                fldDue.Text = "";
                fldTaxDue.Text = "";
                fldPaymentReceived.Text = "";
                fldAccount.Text = "";
                fldBillDate.Text = "";
                fldName.Text = "";
                fldType.Text = "";
                if (rsData.EndOfFile())
                {
                    return;
                }

                lngCount += 1;
                // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                fldAccount.Text = rsData.Get_Fields_String("Account");
                fldBillDate.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillingRateKey"));
                fldName.Text = modUTStatusList.GetStatusName_6(rsData,
                    FCConvert.ToInt16(frmUTStatusList.InstancePtr.cmbNameOption.SelectedIndex));
                // rsData.Fields("Name1")
                fldTaxDue.Text =
                    Strings.Format(
                        rsData.Get_Fields_Decimal("TaxDue1") + rsData.Get_Fields_Decimal("TaxDue2") +
                        rsData.Get_Fields_Decimal("TaxDue3") + rsData.Get_Fields_Decimal("TaxDue4"), "#,##0.00");
                if (modMain.Statics.gboolUTUseAsOfDate)
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("Account") +
                                            " AND Year = " + rsData.Get_Fields_Int32("BillingYear") +
                                            " AND BillKey = " + rsData.Get_Fields_Int32("Billkey") +
                                            " AND RecordedTransactionDate <= #" +
                                            FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "#");
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
                    rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("Account") +
                                            " AND Year = " + rsData.Get_Fields_Int32("BillingYear") +
                                            " AND BillKey = " + rsData.Get_Fields_Int32("Billkey"));
                }

                while (!rsPayment.EndOfFile())
                {
                    // 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
                    // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
                    if (frmUTStatusList.InstancePtr.boolFullStatusAmounts)
                    {
                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        dblPaymentRecieved +=
                            FCConvert.ToDouble(rsPayment.Get_Fields("Principal") +
                                               rsPayment.Get_Fields_Decimal("CurrentInterest") +
                                               rsPayment.Get_Fields_Decimal("PreLienInterest") +
                                               rsPayment.Get_Fields_Decimal("LienCost"));
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "P")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(6, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(8, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "X")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(9, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(10, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(2, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(1, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "S")
                        {
                            // put these figures in the X category because there are going away
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(9, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(3, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(4, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(5, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(0, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(7, rsPayment.Get_Fields("Principal"),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")),
                                FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")),
                                rsPayment.Get_Fields("Tax"));
                        }
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "P")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(6, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if ((Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "X") ||
                                 (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "S"))
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(9, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(8, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(10, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(2, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(1, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(3, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(4, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(5, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(0, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            AddToPaymentArray_728(7, rsPayment.Get_Fields("Principal"), 0, 0, 0,
                                rsPayment.Get_Fields("Tax"));
                            // , rsPayment.Fields("PreLienInterest"), rsPayment.Fields("CurrentInterest"), rsPayment.Fields("LienCost")
                        }

                        // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                        // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                        dblPaymentRecieved += rsPayment.Get_Fields("Principal") + rsPayment.Get_Fields("Tax");
                        // + rsPayment.Fields("LienCost")
                    }

                    rsPayment.MoveNext();
                }

                if (frmUTStatusList.InstancePtr.boolFullStatusAmounts)
                {
                    // if the current interest is checked then calculate it and display it
                    dblTotalDue = modUTCalculations.CalculateAccountUT(rsData, modMain.Statics.gdtUTStatusListAsOfDate,
                        ref dblXtraInt, boolWater);
                }
                else
                {
                    dblXtraInt = 0;
                }

                fldPaymentReceived.Text = Strings.Format(dblPaymentRecieved - dblXtraInt, "#,##0.00");
                fldDue.Text = Strings.Format(FCConvert.ToDouble(fldTaxDue.Text) - (dblPaymentRecieved - dblXtraInt),
                    "#,##0.00");
                if (rptUTOutstandingBalancesAll.InstancePtr.intSuppReportType == 1)
                {
                    rsRate.OpenRecordset("SELECT * FROM RateRec WHERE RateKey = " + rsData.Get_Fields_Int32("RateKey"),
                        modExtraModules.strUTDatabase);
                    if (!rsRate.EndOfFile())
                    {
                        if (FCConvert.ToString(rsRate.Get_Fields_String("RateType")) != "S")
                        {
                            ReversePaymentsFromStatusArray(ref rsPayment);
                            rsData.MoveNext();
                            goto TRYAGAIN;
                        }
                    }
                    else
                    {
                        ReversePaymentsFromStatusArray(ref rsPayment);
                        rsData.MoveNext();
                        goto TRYAGAIN;
                    }
                }

                if (FCConvert.ToDouble(fldDue.Text) == 0)
                {
                    // if this account is not outstanding, then it must not be used
                    ReversePaymentsFromStatusArray(ref rsPayment);
                    rsData.MoveNext();
                    lngCount -= 1;
                    goto TRYAGAIN;
                }

                if (FCConvert.ToString(rsData.Get_Fields_String("BillingType")) == "RE")
                {
                    fldType.Text = "R";
                }
                else
                {
                    fldType.Text = "P";
                }

                if (modMain.Statics.boolSubReport)
                {
                    rptUTOutstandingBalancesAll.InstancePtr.dblTotalsPrin += FCConvert.ToDouble(fldTaxDue.Text);
                    rptUTOutstandingBalancesAll.InstancePtr.dblTotalsPay += dblPaymentRecieved - dblXtraInt;
                    rptUTOutstandingBalancesAll.InstancePtr.lngCount += 1;
                }

                fldPrincipal.Text =
                    FCConvert.ToString(rsData.Get_Fields(strWS + "PrinOwed") - rsData.Get_Fields(strWS + "PrinPaid"));
                fldTax.Text =
                    FCConvert.ToString(rsData.Get_Fields(strWS + "TaxOwed") - rsData.Get_Fields(strWS + "TaxPaid"));
                fldInterest.Text =
                    FCConvert.ToString(rsData.Get_Fields(strWS + "IntOwed") - rsData.Get_Fields(strWS + "IntPaid"));
                fldCosts.Text = FCConvert.ToString(rsData.Get_Fields(strWS + "CostOwed") -
                                                   rsData.Get_Fields(strWS + "CostAdded") -
                                                   rsData.Get_Fields(strWS + "CostPaid"));
                dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
                dblTotals[1] += dblPaymentRecieved - dblXtraInt;
                dblTotals[3] += FCConvert.ToDouble(fldDue.Text);
                dblTotals[4] += FCConvert.ToDouble(fldPrincipal.Text);
                dblTotals[5] += FCConvert.ToDouble(fldTax.Text);
                dblTotals[6] += FCConvert.ToDouble(fldInterest.Text);
                dblTotals[7] += FCConvert.ToDouble(fldCosts.Text);
                // keep track for the year totals
                dblYearTotals[rsData.Get_Fields_Int32("BillingRateKey")] += FCConvert.ToDouble(fldDue.Text);
                // move to the next record in the query
                rsData.MoveNext();
                return;
            }
            catch (Exception ex)
            {

                frmWait.InstancePtr.Unload();
                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error In BindFields", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				rsRate.Dispose();
				rsPayment.Dispose();
            }
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will fill in the totals line at the bottom of the report
				//clsDRWrapper rsSum = new clsDRWrapper();
				string strSUM = "";
				int intSumRows;
				int intCT;
				fldTotalTaxDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
				fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], "#,##0.00");
				fldTotalDue.Text = Strings.Format(dblTotals[3], "#,##0.00");
				fldTotalPrincipal.Text = Strings.Format(dblTotals[4], "#,##0.00");
				fldTotalTax.Text = Strings.Format(dblTotals[5], "#,##0.00");
				fldTotalInterest.Text = Strings.Format(dblTotals[6], "#,##0.00");
				fldTotalCost.Text = Strings.Format(dblTotals[7], "#,##0.00");
				if (lngCount > 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Accounts:";
				}
				else if (lngCount == 1)
				{
					lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Account:";
				}
				else
				{
					lblTotals.Text = "No Non Lien Accounts";
				}
				// this will setup the payment summary
				SetupTotalSummary();
				// Load Summary List
				intSumRows = 1;
				for (intCT = 0; intCT <= Information.UBound(dblYearTotals, 1) - 1; intCT++)
				{
					if (dblYearTotals[intCT] != 0)
					{
						AddSummaryRow_18(intSumRows, dblYearTotals[intCT], intCT + 19800);
						intSumRows += 1;
					}
				}
				// strSUM = "SELECT BillingYear AS Year, SUM(TaxDue1 + TaxDue2 + TaxDue3 + TaxDue4 - PrincipalPaid) AS Due FROM (" & strSQL & ") GROUP BY BillingYear"
				// 
				// intSumRows = 1
				// 
				// rsSum.OpenRecordset strSUM, strCLDatabase
				// If rsSum.EndOfFile Then
				// intSumRows = 1
				// Else
				// Do Until rsSum.EndOfFile
				// AddSummaryRow intSumRows, rsSum.Fields("Due"), rsSum.Fields("Year")
				// intSumRows = intSumRows + 1
				// rsSum.MoveNext
				// Loop
				// End If
				// create the total fields and fill them
				// add a field
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
				obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				obNew.Name = "fldSummaryTotal";
				obNew.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
				obNew.Left = fldSummary1.Left;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				obNew.Width = fldSummary1.Width;
				obNew.Font = lblSummary1.Font;
				obNew.Text = Strings.Format(dblPDTotal, "#,##0.00");
				ReportFooter.Controls.Add(obNew);
				// add a label
				GrapeCity.ActiveReports.SectionReportModel.Label obLabel;
				obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
				obLabel.Name = "lblPerDiemTotal";
				obLabel.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLabel.Left = lblSummary1.Left;
				obLabel.Font = lblSummary1.Font;
				obLabel.Text = "Total";
				ReportFooter.Controls.Add(obLabel);
				// add a line
				GrapeCity.ActiveReports.SectionReportModel.Line obLine;
				obLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
				obLine.Name = "lnFooterSummaryTotal";
				obLine.X1 = fldSummary1.Left;
				obLine.X2 = fldSummary1.Left + fldSummary1.Width;
				obLine.Y1 = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
				obLine.Y2 = obLine.Y1;
				ReportFooter.Controls.Add(obLine);
				ReportFooter.Height = lblSummary1.Top + (intSumRows * lblSummary1.Height);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Summary Creation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
			for (intCT = 0; intCT <= frmUTStatusList.InstancePtr.vsWhere.Rows - 2; intCT++)
			{
				if (frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) != "" || frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2) != "")
				{
					switch (intCT)
					{
						case 0:
							{
								// Account Number
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += "Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
										}
										else
										{
											strTemp += "Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
										}
									}
									else
									{
										strTemp += "Below Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								}
								else
								{
									strTemp += "Above Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
								break;
							}
						case 2:
							{
								// Tax Year
								if (Strings.Trim(strTemp) != "")
								{
									strTemp += ";";
								}
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) != "")
								{
									if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										strTemp += " Tax Year: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
									else
									{
										strTemp += " Tax Year: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
									}
								}
								else
								{
									strTemp += " Tax Year: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
								}
								break;
							}
						default:
							{
								break;
								break;
							}
					}
					//end switch
				}
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List" + "\r\n" + "Outstanding Non Liened Accounts";
			}
			else
			{
				lblReportType.Text = strTemp + "\r\n" + "Outstanding Non Liened Accounts";
			}
			if (modMain.Statics.gboolUTUseAsOfDate)
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As of: " + Strings.Format(modMain.Statics.gdtUTStatusListAsOfDate, "MM/dd/yyyy");
			}
		}
		// vbPorter upgrade warning: intRNum As short	OnWriteFCConvert.ToInt32(
		private void AddSummaryRow_18(int intRNum, double dblAmount, int lngYear)
		{
			AddSummaryRow(ref intRNum, ref dblAmount, ref lngYear);
		}

		private void AddSummaryRow(ref int intRNum, ref double dblAmount, ref int lngYear)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will add another per diem line in the report footer
				if (intRNum == 1)
				{
					fldSummary1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					fldSummary1.Text = Strings.Format(dblAmount, "#,##0.00");
					lblSummary1.Text = FCConvert.ToString(lngYear);
					dblPDTotal += dblAmount;
				}
				else
				{
					// add a field
					GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldSummary" + FCConvert.ToString(intRNum);
					obNew.Top = fldSummary1.Top + ((intRNum - 1) * fldSummary1.Height);
					obNew.Left = fldSummary1.Left;
					obNew.Width = fldSummary1.Width;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = Strings.Format(dblAmount, "#,##0.00");
					ReportFooter.Controls.Add(obNew);
					dblPDTotal += dblAmount;
					// add a label
					GrapeCity.ActiveReports.SectionReportModel.Label obLabel;
					obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
					obLabel.Name = "lblSummary" + FCConvert.ToString(intRNum);
					obLabel.Top = lblSummary1.Top + ((intRNum - 1) * lblSummary1.Height);
					obLabel.Left = lblSummary1.Left;
					obLabel.Font = fldSummary1.Font;
					// this sets the font to the same as the field that is already created
					obLabel.Text = FCConvert.ToString(lngYear);
					ReportFooter.Controls.Add(obLabel);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Summary Row", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupTotalSummary()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the summary labels at the bottom of the page
				// and hide/show the labels when needed
				int intCT;
				int intRow;
				// this will keep track of the row  that I am adding values to
				string strDesc = "";
				double[] dblTotal = new double[5 + 1];
				// fill in the titles
				lblSumHeaderType.Text = "Type";
				lblSumHeaderPrin.Text = "Principal";
				lblSumHeaderTax.Text = "Tax";
				lblSumHeaderInt.Text = "Interest";
				lblSumHeaderCost.Text = "Costs";
				lblSumHeaderTotal.Text = "Total";
				intRow = 1;
				// start at the first row
				for (intCT = 0; intCT <= 10; intCT++)
				{
					// this will fill the totals element
					dblPayments[intCT, 4] = dblPayments[intCT, 0] + dblPayments[intCT, 1] + dblPayments[intCT, 2] + dblPayments[intCT, 3] + dblPayments[intCT, 5];
				}
				for (intCT = 0; intCT <= 10; intCT++)
				{
					if (dblPayments[intCT, 4] != 0)
					{
						switch (intCT)
						{
							case 0:
								{
									strDesc = "3 - 30 DN Costs";
									break;
								}
							case 1:
								{
									strDesc = "A - Abatement";
									break;
								}
							case 2:
								{
									strDesc = "C - Correction";
									break;
								}
							case 3:
								{
									strDesc = "D - Discount";
									break;
								}
							case 4:
								{
									strDesc = "I - Interest Charged";
									break;
								}
							case 5:
								{
									strDesc = "L - Lien Costs";
									break;
								}
							case 6:
								{
									strDesc = "P - Payment";
									break;
								}
							case 7:
								{
									strDesc = "R - Refunded Abatement";
									break;
								}
							case 8:
								{
									strDesc = "U - Tax Club";
									break;
								}
							case 9:
								{
									strDesc = "X - DOS Correction";
									break;
								}
							case 10:
								{
									strDesc = "Y - Prepayment";
									break;
								}
						}
						//end switch
						FillSummaryLine(ref intRow, ref strDesc, ref dblPayments[intCT, 0], ref dblPayments[intCT, 1], ref dblPayments[intCT, 2], ref dblPayments[intCT, 3], ref dblPayments[intCT, 5], ref dblPayments[intCT, 4]);
						dblTotal[0] += dblPayments[intCT, 0];
						// this will total all of the seperated payments for the total line
						dblTotal[1] += dblPayments[intCT, 1];
						dblTotal[2] += dblPayments[intCT, 2];
						dblTotal[3] += dblPayments[intCT, 3];
						dblTotal[4] += dblPayments[intCT, 4];
						dblTotal[5] += dblPayments[intCT, 5];
						intRow += 1;
					}
				}
				// show the total line
				FillSummaryLine_6(intRow, "Total", dblTotal[0], dblTotal[1], dblTotal[2], dblTotal[3], dblTotal[5], dblTotal[4]);
				SetSummaryTotalLine(ref intRow);
				for (intCT = intRow + 1; intCT <= 11; intCT++)
				{
					HideSummaryRow(ref intCT);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Summary Table", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void SetSummaryTotalLine(ref int intRw)
		{
			switch (intRw)
			{
				case 1:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal1.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal1.Top;
						break;
					}
				case 2:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal2.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal2.Top;
						break;
					}
				case 3:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal3.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal3.Top;
						break;
					}
				case 4:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal4.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal4.Top;
						break;
					}
				case 5:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal5.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal5.Top;
						break;
					}
				case 6:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal6.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal6.Top;
						break;
					}
				case 7:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal7.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal7.Top;
						break;
					}
				case 8:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal8.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal8.Top;
						break;
					}
				case 9:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal9.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal9.Top;
						break;
					}
				case 10:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal10.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal10.Top;
						break;
					}
				case 11:
					{
						lnSummaryTotal.Y1 = lblSummaryTotal11.Top;
						lnSummaryTotal.Y2 = lblSummaryTotal11.Top;
						break;
					}
			}
			//end switch
		}
		// vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void FillSummaryLine_6(int intRw, string strDescription, double dblPrin, double dblPLI, double dblCurInt, double dblCosts, double dblTax, double dblTotal)
		{
			FillSummaryLine(ref intRw, ref strDescription, ref dblPrin, ref dblPLI, ref dblCurInt, ref dblCosts, ref dblTax, ref dblTotal);
		}

		private void FillSummaryLine(ref int intRw, ref string strDescription, ref double dblPrin, ref double dblPLI, ref double dblCurInt, ref double dblCosts, ref double dblTax, ref double dblTotal)
		{
			// this routine will fill in the line summary row
			switch (intRw)
			{
				case 1:
					{
						lblSummaryPaymentType1.Text = strDescription;
						lblSumPrin1.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax1.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt1.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost1.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal1.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 2:
					{
						lblSummaryPaymentType2.Text = strDescription;
						lblSumPrin2.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax2.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt2.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost2.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal2.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 3:
					{
						lblSummaryPaymentType3.Text = strDescription;
						lblSumPrin3.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax3.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt3.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost3.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal3.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 4:
					{
						lblSummaryPaymentType4.Text = strDescription;
						lblSumPrin4.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax4.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt4.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost4.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal4.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 5:
					{
						lblSummaryPaymentType5.Text = strDescription;
						lblSumPrin5.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax5.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt5.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost5.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal5.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 6:
					{
						lblSummaryPaymentType6.Text = strDescription;
						lblSumPrin6.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax6.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt6.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost6.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal6.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 7:
					{
						lblSummaryPaymentType7.Text = strDescription;
						lblSumPrin7.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax7.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt7.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost7.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal7.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 8:
					{
						lblSummaryPaymentType8.Text = strDescription;
						lblSumPrin8.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax8.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt8.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost8.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal8.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 9:
					{
						lblSummaryPaymentType9.Text = strDescription;
						lblSumPrin9.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax9.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt9.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost9.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal9.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 10:
					{
						lblSummaryPaymentType10.Text = strDescription;
						lblSumPrin10.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax10.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt10.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost10.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal10.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
				case 11:
					{
						lblSummaryPaymentType11.Text = strDescription;
						lblSumPrin11.Text = Strings.Format(dblPrin, "#,##0.00");
						lblSumTax11.Text = Strings.Format(dblTax, "#,##0.00");
						lblSumInt11.Text = Strings.Format(dblPLI + dblCurInt, "#,##0.00");
						lblSumCost11.Text = Strings.Format(dblCosts, "#,##0.00");
						lblSummaryTotal11.Text = Strings.Format(dblTotal, "#,##0.00");
						break;
					}
			}
			//end switch
		}

		private void AddToPaymentArray_728(int lngIndex, double dblPrin, double dblPLI, double dblCurInt, double dblCost, double dblTax)
		{
			AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblPLI, ref dblCurInt, ref dblCost, ref dblTax);
		}

		private void AddToPaymentArray(ref int lngIndex, ref double dblPrin, ref double dblPLI, ref double dblCurInt, ref double dblCost, ref double dblTax)
		{
			dblPayments[lngIndex, 0] += dblPrin;
			dblPayments[lngIndex, 1] += dblPLI;
			dblPayments[lngIndex, 2] += dblCurInt;
			dblPayments[lngIndex, 3] += dblCost;
			dblPayments[lngIndex, 5] += dblTax;
		}
		// vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
		private void HideSummaryRow(ref int intRw)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				for (intCT = intRw; intCT <= 11; intCT++)
				{
					switch (intRw)
					{
						case 1:
							{
								lblSummaryPaymentType1.Visible = false;
								lblSumPrin1.Visible = false;
								lblSumTax1.Visible = false;
								lblSumInt1.Visible = false;
								lblSumCost1.Visible = false;
								lblSummaryTotal1.Visible = false;
								break;
							}
						case 2:
							{
								lblSummaryPaymentType2.Visible = false;
								lblSumPrin2.Visible = false;
								lblSumTax2.Visible = false;
								lblSumInt2.Visible = false;
								lblSumCost2.Visible = false;
								lblSummaryTotal2.Visible = false;
								break;
							}
						case 3:
							{
								lblSummaryPaymentType3.Visible = false;
								lblSumPrin3.Visible = false;
								lblSumTax3.Visible = false;
								lblSumInt3.Visible = false;
								lblSumCost3.Visible = false;
								lblSummaryTotal3.Visible = false;
								break;
							}
						case 4:
							{
								lblSummaryPaymentType4.Visible = false;
								lblSumPrin4.Visible = false;
								lblSumTax4.Visible = false;
								lblSumInt4.Visible = false;
								lblSumCost4.Visible = false;
								lblSummaryTotal4.Visible = false;
								break;
							}
						case 5:
							{
								lblSummaryPaymentType5.Visible = false;
								lblSumPrin5.Visible = false;
								lblSumTax5.Visible = false;
								lblSumInt5.Visible = false;
								lblSumCost5.Visible = false;
								lblSummaryTotal5.Visible = false;
								break;
							}
						case 6:
							{
								lblSummaryPaymentType6.Visible = false;
								lblSumPrin6.Visible = false;
								lblSumTax6.Visible = false;
								lblSumInt6.Visible = false;
								lblSumCost6.Visible = false;
								lblSummaryTotal6.Visible = false;
								break;
							}
						case 7:
							{
								lblSummaryPaymentType7.Visible = false;
								lblSumPrin7.Visible = false;
								lblSumTax7.Visible = false;
								lblSumInt7.Visible = false;
								lblSumCost7.Visible = false;
								lblSummaryTotal7.Visible = false;
								break;
							}
						case 8:
							{
								lblSummaryPaymentType8.Visible = false;
								lblSumPrin8.Visible = false;
								lblSumTax8.Visible = false;
								lblSumInt8.Visible = false;
								lblSumCost8.Visible = false;
								lblSummaryTotal8.Visible = false;
								break;
							}
						case 9:
							{
								lblSummaryPaymentType9.Visible = false;
								lblSumPrin9.Visible = false;
								lblSumTax9.Visible = false;
								lblSumInt9.Visible = false;
								lblSumCost9.Visible = false;
								lblSummaryTotal9.Visible = false;
								break;
							}
						case 10:
							{
								lblSummaryPaymentType10.Visible = false;
								lblSumPrin10.Visible = false;
								lblSumTax10.Visible = false;
								lblSumInt10.Visible = false;
								lblSumCost10.Visible = false;
								lblSummaryTotal10.Visible = false;
								break;
							}
						case 11:
							{
								lblSummaryPaymentType11.Visible = false;
								lblSumPrin11.Visible = false;
								lblSumTax11.Visible = false;
								lblSumInt11.Visible = false;
								lblSumCost11.Visible = false;
								lblSummaryTotal11.Visible = false;
								break;
							}
					}
					//end switch
				}
				if (!boolAdjustedSummary)
				{
					SetYearSummaryTop_2(lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 100 / 1440f);
					boolAdjustedSummary = true;
				}
				// set the size of the report footer depending on how many rows have been used
				// and move the error label to 300 pixels after the summary list
				// lblRTError.Top = lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 300
				// ReportFooter.Height = lblRTError.Top + lblRTError.Height + 100
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Hiding Summary Rows", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetYearSummaryTop_2(float lngTop)
		{
			SetYearSummaryTop(ref lngTop);
		}

		private void SetYearSummaryTop(ref float lngTop)
		{
			// this will start the year summary at the right height
			lblSummary.Top = lngTop;
			Line1.Y1 = lngTop + lblSummary.Height;
			Line1.Y2 = lngTop + lblSummary.Height;
			lblSummary1.Top = lngTop + lblSummary.Height;
			fldSummary1.Top = lngTop + lblSummary.Height;
		}

		private void ReversePaymentsFromStatusArray(ref clsDRWrapper rsRev)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (rsRev.RecordCount() != 0)
				{
					rsRev.MoveFirst();
					while (!rsRev.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (Strings.UCase(rsRev.Get_Fields("Code")) == "P")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(6, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if ((Strings.UCase(rsRev.Get_Fields("Code")) == "X") || (Strings.UCase(rsRev.Get_Fields("Code")) == "S"))
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(9, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (Strings.UCase(rsRev.Get_Fields("Code")) == "U")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(8, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(rsRev.Get_Fields("Code")) == "Y")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(10, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(rsRev.Get_Fields("Code")) == "C")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(2, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(rsRev.Get_Fields("Code")) == "A")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(1, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), FCConvert.ToDouble(rsRev.Get_Fields("Tax") * -1));
						}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(rsRev.Get_Fields("Code")) == "D")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(3, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(rsRev.Get_Fields("Code")) == "I")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(4, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(rsRev.Get_Fields("Code")) == "L")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(5, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(rsRev.Get_Fields("Code")) == "3")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(0, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), rsRev.Get_Fields("Tax") * -1);
						}
															// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
															else if (Strings.UCase(rsRev.Get_Fields("Code")) == "R")
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							AddToPaymentArray_728(7, rsRev.Get_Fields("Principal") * -1, FCConvert.ToDouble(rsRev.Get_Fields_Decimal("PreLienInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("CurrentInterest") * -1), FCConvert.ToDouble(rsRev.Get_Fields_Decimal("LienCost") * -1), FCConvert.ToDouble(rsRev.Get_Fields("Tax") * -1));
						}
						rsRev.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Reversing Payment Counts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void rptUTOutstandingBalances_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUTOutstandingBalances properties;
			//rptUTOutstandingBalances.Caption	= "Outstanding Balance Report";
			//rptUTOutstandingBalances.Icon	= "rptUTOutstandingBalance.dsx":0000";
			//rptUTOutstandingBalances.Left	= 0;
			//rptUTOutstandingBalances.Top	= 0;
			//rptUTOutstandingBalances.Width	= 11880;
			//rptUTOutstandingBalances.Height	= 8595;
			//rptUTOutstandingBalances.StartUpPosition	= 3;
			//rptUTOutstandingBalances.SectionData	= "rptUTOutstandingBalance.dsx":058A;
			//End Unmaped Properties
		}
	}
}
