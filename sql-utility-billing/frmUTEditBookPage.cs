﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmEditBookPage.
	/// </summary>
	public partial class frmEditBookPage : BaseForm
	{
		public frmEditBookPage()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
            //FC:FINAL:BSE:#3859 allow only numeric input in textbox
            txtBook.AllowOnlyNumericInput();
            txtPage.AllowOnlyNumericInput();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEditBookPage InstancePtr
		{
			get
			{
				return (frmEditBookPage)Sys.GetInstance(typeof(frmEditBookPage));
			}
		}

		protected frmEditBookPage _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/04/2005              *
		// ********************************************************
		clsDRWrapper rsLien = new clsDRWrapper();
		bool boolDirty;
		bool boolLoaded;
		string strWS = "";
		int intType;

		public void Init(short intPassType = 0)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// defaults to saving the lien book and page
				intType = intPassType;
				switch (intType)
				{
					case 0:
						{
							this.Text = "Edit Lien Book / Page";
							break;
						}
					case 1:
						{
							this.Text = "Edit LDN Book / Page";
							break;
						}
				}
				//end switch
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// load the info from this account
				// this will load the lien record's book and page info
				if (cmbYear.SelectedIndex >= 0)
				{
					switch (intType)
					{
						case 0:
							{
								rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strUTDatabase);
								if (!rsLien.EndOfFile())
								{
									// this will load the info and unlock the fields
									// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
									txtBook.Text = FCConvert.ToString(rsLien.Get_Fields("Book"));
									// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
									txtPage.Text = FCConvert.ToString(rsLien.Get_Fields("Page"));
									rsLien.OpenRecordset("SELECT * FROM Bill WHERE " + strWS + "LienRecordNumber = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strUTDatabase);
									if (!rsLien.EndOfFile())
									{
										// this will change the name to be correct
										lblName.Text = FCConvert.ToString(rsLien.Get_Fields_String("OName"));
									}
									LockFields_2(false);
								}
								else
								{
									MessageBox.Show("Could not find lien record " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)) + ".", "Load Lien Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								break;
							}
						case 1:
							{
								rsLien.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strUTDatabase);
								if (!rsLien.EndOfFile())
								{
									// this will load the info and unlock the fields
									// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
									txtBook.Text = FCConvert.ToString(Conversion.Val(rsLien.Get_Fields("Book")));
									// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
									txtPage.Text = FCConvert.ToString(Conversion.Val(rsLien.Get_Fields("Page")));
									rsLien.OpenRecordset("SELECT * FROM Bill WHERE " + strWS + "LienRecordNumber = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strUTDatabase);
									if (!rsLien.EndOfFile())
									{
										// this will change the name to be correct
										lblName.Text = FCConvert.ToString(rsLien.Get_Fields_String("OName"));
									}
									LockFields_2(false);
								}
								else
								{
									MessageBox.Show("Could not find a discharge notice for lien record " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)) + ".", "Load Lien Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
								break;
							}
					}
					//end switch
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Load Lien Record Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmbYear_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbYear.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void frmEditBookPage_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
			}
		}

		private void frmEditBookPage_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmEditBookPage_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditBookPage properties;
			//frmEditBookPage.FillStyle	= 0;
			//frmEditBookPage.ScaleWidth	= 3885;
			//frmEditBookPage.ScaleHeight	= 2445;
			//frmEditBookPage.LinkTopic	= "Form2";
			//frmEditBookPage.LockControls	= true;
			//frmEditBookPage.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			// this will set the size of the form to the smaller TRIO size (1/2 normal)
			modGlobalFunctions.SetTRIOColors(this);
			// this will set all the TRIO colors
			if (modUTStatusPayments.Statics.TownService == "B")
			{
				strWS = "W";
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = true;
			}
			else if (modUTStatusPayments.Statics.TownService == "W")
			{
				strWS = "W";
				cmbWS.Clear();
				cmbWS.Items.Add("Water");
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = false;
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				strWS = "S";
				cmbWS.Clear();
				cmbWS.Items.Add("Sewer");
				cmbWS.Text = "Sewer";
				//optWS[0].Enabled = false;
				//optWS[1].Enabled = true;
				//optWS[1].Checked = true;
			}
		}

		private void frmEditBookPage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int lngCT;
			if (boolDirty)
			{
				switch (MessageBox.Show("Changes have been made.  Would you like to save them before you exit?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							// save the changes and exit
							SaveInfo();
							break;
						}
					case DialogResult.No:
						{
							// do not save the changes and exit
							break;
						}
					case DialogResult.Cancel:
						{
							// do not save and cancel the exit
							e.Cancel = true;
							break;
						}
				}
				//end switch
			}
			if (e.Cancel)
			{
				// do nothing
			}
			else
			{
				boolLoaded = false;
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFileGetAccount_Click(object sender, System.EventArgs e)
		{
			// get the account information
			if (boolDirty)
			{
				switch (MessageBox.Show("Changes have been made.  Would you like to save now?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							// save the changes
							SaveInfo();
							// get the account years that are liened
							if (Conversion.Val(txtAccount.Text) > 0)
							{
								GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
							}
							break;
						}
					case DialogResult.No:
						{
							// do not save the changes and exit
							// get the account years that are liened
							if (Conversion.Val(txtAccount.Text) > 0)
							{
								GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
							}
							break;
						}
					case DialogResult.Cancel:
						{
							// do nothing...the user cancelled
							break;
						}
				}
				//end switch
			}
			else
			{
				// get the account years that are liened
				if (Conversion.Val(txtAccount.Text) > 0)
				{
					GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
				}
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveInfo();
			this.Unload();
		}
		// vbPorter upgrade warning: lngLienAccount As int	OnWriteFCConvert.ToDouble(
		private void GetLienYears_2(int lngLienAccount)
		{
			GetLienYears(ref lngLienAccount);
		}

		private void GetLienYears(ref int lngLienAccount)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the year combo with all of the years that this account has liens
				// and it will clear out the book and page fields
				clsDRWrapper rsMaster = new clsDRWrapper();
				if (cmbWS.Text == "Water")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				cmbYear.Clear();
				rsMaster.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngLienAccount), modExtraModules.strUTDatabase);
				if (!rsMaster.EndOfFile())
				{
					lngLienAccount = FCConvert.ToInt32(rsMaster.Get_Fields_Int32("ID"));
					// rsLien.OpenRecordset "SELECT * FROM Lien INNER JOIN Bill ON Lien.LienKey = Bill." & strWS & "LienRecordNumber WHERE AccountKey = " & lngLienAccount & " AND " & strWS & "CombinationLienKey = Bill ORDER BY RateKey desc", strUTDatabase
					// kk01292014 trouts-70   rsLien.OpenRecordset "SELECT * FROM (SELECT OName, Lien.ID AS LienKey, Lien.DateCreated FROM Lien INNER JOIN Bill ON Lien.ID = Bill." & strWS & "LienRecordNumber) AS LB INNER JOIN RateKeys ON LB.RateKey = RateKeys.ID WHERE AccountKey = " & lngLienAccount & " AND " & strWS & "CombinationLienKey = LB.BillKey ORDER BY LB.RateKey desc", strUTDatabase
					rsLien.OpenRecordset("SELECT Bill.ID AS BillKey, " + strWS + "CombinationLienKey, AccountKey, OName, Lien.DateCreated, Lien.ID AS LienKey, Lien.DateCreated, RateKey, RateKeys.BillDate FROM Lien INNER JOIN Bill ON Lien.ID = Bill." + strWS + "LienRecordNumber INNER JOIN RateKeys ON RateKey = RateKeys.ID WHERE AccountKey = " + FCConvert.ToString(lngLienAccount) + " AND " + strWS + "CombinationLienKey = Bill.ID ORDER BY RateKey", modExtraModules.strUTDatabase);
					if (rsLien.RecordCount() > 0)
					{
						while (!rsLien.EndOfFile())
						{
							// cmbYear.AddItem Format(rsLien.Fields("DateCreated"), "MM/dd/yyyy") & " - Lien : " & rsLien.Fields("LienKey")
							if (Information.IsDate(rsLien.Get_Fields("BillDate")))
							{
								cmbYear.AddItem(Strings.Format(rsLien.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + " - Lien : " + rsLien.Get_Fields_Int32("LienKey"));
							}
							else
							{
								cmbYear.AddItem(Strings.Format(rsLien.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy") + " - Lien : " + rsLien.Get_Fields_Int32("LienKey"));
							}
							cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsLien.Get_Fields_Int32("LienKey")));
							rsLien.MoveNext();
						}
						rsLien.MoveFirst();
						lblName.Text = FCConvert.ToString(rsLien.Get_Fields_String("OName"));
						LockFields_8(true, false);
					}
					else
					{
						MessageBox.Show("This account does not have any liened years.", "No Liened Years", MessageBoxButtons.OK, MessageBoxIcon.Information);
						lblName.Text = "";
						txtBook.Text = "";
						txtPage.Text = "";
						LockFields_2(true);
					}
					boolDirty = false;
				}
				else
				{
					MessageBox.Show("Cannot find account " + FCConvert.ToString(lngLienAccount) + ".", "Missing Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveInfo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save the book page information into the lien record
				switch (intType)
				{
					case 0:
						{
							// save this lien record's book and page
							if (cmbYear.SelectedIndex >= 0)
							{
								rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)));
								if (!rsLien.EndOfFile())
								{
									rsLien.Edit();
									// this will save the info, then clear and lock the fields
									rsLien.Set_Fields("Book", FCConvert.ToString(Conversion.Val(txtBook.Text)));
									rsLien.Set_Fields("Page", FCConvert.ToString(Conversion.Val(txtPage.Text)));
									rsLien.Update();
									LockFields_2(false);
								}
								else
								{
									MessageBox.Show("Could not find lien record " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)) + ".", "Load Lien Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
							break;
						}
					case 1:
						{
							// Save the book and page to the Lien Discharge Notice
							if (cmbYear.SelectedIndex >= 0)
							{
								rsLien.OpenRecordset("SELECT * FROM DischargeNeeded WHERE LienKey = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strUTDatabase);
								if (!rsLien.EndOfFile())
								{
									rsLien.Edit();
									// this will save the info, then clear and lock the fields
									rsLien.Set_Fields("Book", FCConvert.ToString(Conversion.Val(txtBook.Text)));
									rsLien.Set_Fields("Page", FCConvert.ToString(Conversion.Val(txtPage.Text)));
									rsLien.Update();
									LockFields_2(false);
								}
								else
								{
									MessageBox.Show("Could not find lien discharge notice " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)) + ".", "Load Lien Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
							break;
						}
				}
				//end switch
				// clear the fields
				lblName.Text = "";
				txtAccount.Text = "";
				cmbYear.Clear();
				txtBook.Text = "";
				txtPage.Text = "";
				// clear slate
				boolDirty = false;
				// lock the fields until another valid account is entered
				LockFields_2(true);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Lien", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LockFields_2(bool boolLock, bool boolYear = true)
		{
			LockFields(ref boolLock, boolYear);
		}

		private void LockFields_8(bool boolLock, bool boolYear = true)
		{
			LockFields(ref boolLock, boolYear);
		}

		private void LockFields(ref bool boolLock, bool boolYear = true)
		{
			if (boolYear)
			{
				cmbYear.Enabled = !boolLock;
			}
			else
			{
				cmbYear.Enabled = true;
			}
			txtBook.Enabled = !boolLock;
			txtPage.Enabled = !boolLock;
		}

		private void optWS_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (boolDirty)
			{
				switch (MessageBox.Show("Changes have been made.  Would you like to save now?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							// save the changes
							SaveInfo();
							// get the account years that are liened
							if (Conversion.Val(txtAccount.Text) > 0)
							{
								GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
							}
							break;
						}
					case DialogResult.No:
						{
							// do not save the changes and exit
							// get the account years that are liened
							if (Conversion.Val(txtAccount.Text) > 0)
							{
								GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
							}
							break;
						}
					case DialogResult.Cancel:
						{
							// do nothing...the user cancelled
							break;
						}
				}
				//end switch
			}
			else
			{
				// get the account years that are liened
				if (Conversion.Val(txtAccount.Text) > 0)
				{
					GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
				}
			}
		}

		private void optWS_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbWS.SelectedIndex;
			optWS_CheckedChanged(index, sender, e);
		}

		private void txtAccount_Enter(object sender, System.EventArgs e)
		{
			if (txtAccount.Text != "")
			{
				txtAccount.SelectionStart = 0;
				txtAccount.SelectionLength = txtAccount.Text.Length;
			}
		}

		private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
                        //FC:FINAL:AM:#3856 - select next control instead to avoid multiple validation messages
                        //txtAccount_Validate(false);
                        Support.SendKeys("{TAB}");
						break;
					}
			}
			//end switch
		}

		private void txtAccount_Validating_2(bool Cancel)
		{
			txtAccount_Validating(new object(), new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// get the account information
			if (boolDirty)
			{
				switch (MessageBox.Show("Changes have been made.  Would you like to save now?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							// save the changes
							SaveInfo();
							// get the account years that are liened
							if (Conversion.Val(txtAccount.Text) > 0)
							{
								GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
							}
							break;
						}
					case DialogResult.No:
						{
							// do not save the changes and exit
							// get the account years that are liened
							if (Conversion.Val(txtAccount.Text) > 0)
							{
								GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
							}
							break;
						}
					case DialogResult.Cancel:
						{
							// do nothing...the user cancelled
							break;
						}
				}
				//end switch
			}
			else
			{
				// get the account years that are liened
				if (Conversion.Val(txtAccount.Text) > 0)
				{
					GetLienYears_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text)));
				}
			}
		}

		public void txtAccount_Validate(bool Cancel)
		{
			txtAccount_Validating(txtAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtBook_Enter(object sender, System.EventArgs e)
		{
			if (txtBook.Text != "")
			{
				txtBook.SelectionStart = 0;
				txtBook.SelectionLength = txtBook.Text.Length;
			}
		}

		private void txtBook_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back))
			{
				if (txtBook.Enabled)
				{
					boolDirty = true;
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPage_Enter(object sender, System.EventArgs e)
		{
			if (txtPage.Text != "")
			{
				txtPage.SelectionStart = 0;
				txtPage.SelectionLength = txtPage.Text.Length;
			}
		}

		private void txtPage_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back))
			{
				if (txtPage.Enabled)
				{
					boolDirty = true;
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
