﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCustomLisbonUpdateWinterBilling.
	/// </summary>
	public partial class frmCustomLisbonUpdateWinterBilling : BaseForm
	{
		public frmCustomLisbonUpdateWinterBilling()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomLisbonUpdateWinterBilling InstancePtr
		{
			get
			{
				return (frmCustomLisbonUpdateWinterBilling)Sys.GetInstance(typeof(frmCustomLisbonUpdateWinterBilling));
			}
		}

		protected frmCustomLisbonUpdateWinterBilling _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void cboRateKeys_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cboRateKeys.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 450, 0);
		}

		private void frmCustomLisbonUpdateWinterBilling_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCustomLisbonUpdateWinterBilling_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomLisbonUpdateWinterBilling properties;
			//frmCustomLisbonUpdateWinterBilling.FillStyle	= 0;
			//frmCustomLisbonUpdateWinterBilling.ScaleWidth	= 3885;
			//frmCustomLisbonUpdateWinterBilling.ScaleHeight	= 2355;
			//frmCustomLisbonUpdateWinterBilling.LinkTopic	= "Form2";
			//frmCustomLisbonUpdateWinterBilling.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			FillRateKeyCombo();
		}

		private void frmCustomLisbonUpdateWinterBilling_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void FillRateKeyCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				cboRateKeys.Clear();
				rsInfo.OpenRecordset("SELECT * FROM RateKeys WHERE RateType <> 'L' ORDER BY BillDate DESC");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						cboRateKeys.AddItem(Strings.Format(rsInfo.Get_Fields_Int32("RateKey"), "00000") + "  " + rsInfo.Get_Fields_String("RateType") + "   " + Strings.Format(rsInfo.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "   " + rsInfo.Get_Fields_String("Description"));
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Rate Keys", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM Bill WHERE BillingRateKey = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboRateKeys.Text, 5))));
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					rs.Edit();
					if (cmbWinter.SelectedIndex == 0)
					{
						rs.Set_Fields("WinterBill", true);
					}
					else
					{
						rs.Set_Fields("WinterBill", false);
					}
					rs.Update();
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			MessageBox.Show("Process Complete!", "Winter Billing Flag Set", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
	}
}
