﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptRateKeyListing.
	/// </summary>
	partial class rptRateKeyListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRateKeyListing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRateKeyNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldStart = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblStartDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldEnd = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblEndDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblIntDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRateType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRateTupe = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fld30DNW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl30DNW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLienW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLienW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMatW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMatW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblHeaderW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fld30DNS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl30DNS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLienS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLienS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMatS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMatS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldInterestRate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblIntRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateKeyNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStart)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEnd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateTupe)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld30DNW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl30DNW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLienW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMatW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMatW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld30DNS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl30DNS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLienS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMatS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMatS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterestRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldRateKeyNumber,
				this.fldDescription,
				this.fldBillDate,
				this.lblBillDate,
				this.fldStart,
				this.lblStartDate,
				this.fldEnd,
				this.lblEndDate,
				this.fldInt,
				this.lblIntDate,
				this.fldRateType,
				this.lblRateTupe,
				this.fld30DNW,
				this.lbl30DNW,
				this.fldLienW,
				this.lblLienW,
				this.fldMatW,
				this.lblMatW,
				this.Line1,
				this.lblHeaderW,
				this.Line2,
				this.fld30DNS,
				this.lbl30DNS,
				this.fldLienS,
				this.lblLienS,
				this.fldMatS,
				this.lblMatS,
				this.lblHeaderS,
				this.Line3,
				this.fldInterestRate,
				this.lblIntRate
			});
			this.Detail.Height = 1.3125F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber
			});
			this.PageHeader.Height = 0.4375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Rate Key Listing";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.0625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.5F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6.5F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1F;
			// 
			// fldRateKeyNumber
			// 
			this.fldRateKeyNumber.Height = 0.1875F;
			this.fldRateKeyNumber.Left = 0F;
			this.fldRateKeyNumber.Name = "fldRateKeyNumber";
			this.fldRateKeyNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldRateKeyNumber.Text = null;
			this.fldRateKeyNumber.Top = 0.0625F;
			this.fldRateKeyNumber.Width = 0.8125F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 0.9375F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldDescription.Text = null;
			this.fldDescription.Top = 0.0625F;
			this.fldDescription.Width = 4.1875F;
			// 
			// fldBillDate
			// 
			this.fldBillDate.Height = 0.1875F;
			this.fldBillDate.Left = 2F;
			this.fldBillDate.Name = "fldBillDate";
			this.fldBillDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldBillDate.Text = null;
			this.fldBillDate.Top = 0.25F;
			this.fldBillDate.Width = 1F;
			// 
			// lblBillDate
			// 
			this.lblBillDate.Height = 0.1875F;
			this.lblBillDate.HyperLink = null;
			this.lblBillDate.Left = 0.9375F;
			this.lblBillDate.Name = "lblBillDate";
			this.lblBillDate.Style = "font-family: \'Tahoma\'";
			this.lblBillDate.Text = "Bill Date:";
			this.lblBillDate.Top = 0.25F;
			this.lblBillDate.Width = 1.0625F;
			// 
			// fldStart
			// 
			this.fldStart.Height = 0.1875F;
			this.fldStart.Left = 2F;
			this.fldStart.Name = "fldStart";
			this.fldStart.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldStart.Text = null;
			this.fldStart.Top = 0.4375F;
			this.fldStart.Width = 1F;
			// 
			// lblStartDate
			// 
			this.lblStartDate.Height = 0.1875F;
			this.lblStartDate.HyperLink = null;
			this.lblStartDate.Left = 0.9375F;
			this.lblStartDate.Name = "lblStartDate";
			this.lblStartDate.Style = "font-family: \'Tahoma\'";
			this.lblStartDate.Text = "Start Date:";
			this.lblStartDate.Top = 0.4375F;
			this.lblStartDate.Width = 1.0625F;
			// 
			// fldEnd
			// 
			this.fldEnd.Height = 0.1875F;
			this.fldEnd.Left = 2F;
			this.fldEnd.Name = "fldEnd";
			this.fldEnd.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldEnd.Text = null;
			this.fldEnd.Top = 0.625F;
			this.fldEnd.Width = 1F;
			// 
			// lblEndDate
			// 
			this.lblEndDate.Height = 0.1875F;
			this.lblEndDate.HyperLink = null;
			this.lblEndDate.Left = 0.9375F;
			this.lblEndDate.Name = "lblEndDate";
			this.lblEndDate.Style = "font-family: \'Tahoma\'";
			this.lblEndDate.Text = "End Date:";
			this.lblEndDate.Top = 0.625F;
			this.lblEndDate.Width = 1.0625F;
			// 
			// fldInt
			// 
			this.fldInt.Height = 0.1875F;
			this.fldInt.Left = 2F;
			this.fldInt.Name = "fldInt";
			this.fldInt.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldInt.Text = null;
			this.fldInt.Top = 0.8125F;
			this.fldInt.Width = 1F;
			// 
			// lblIntDate
			// 
			this.lblIntDate.Height = 0.1875F;
			this.lblIntDate.HyperLink = null;
			this.lblIntDate.Left = 0.9375F;
			this.lblIntDate.Name = "lblIntDate";
			this.lblIntDate.Style = "font-family: \'Tahoma\'";
			this.lblIntDate.Text = "Interest Date:";
			this.lblIntDate.Top = 0.8125F;
			this.lblIntDate.Width = 1.0625F;
			// 
			// fldRateType
			// 
			this.fldRateType.Height = 0.1875F;
			this.fldRateType.Left = 6.375F;
			this.fldRateType.Name = "fldRateType";
			this.fldRateType.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldRateType.Text = null;
			this.fldRateType.Top = 0.0625F;
			this.fldRateType.Width = 1F;
			// 
			// lblRateTupe
			// 
			this.lblRateTupe.Height = 0.1875F;
			this.lblRateTupe.HyperLink = null;
			this.lblRateTupe.Left = 5.3125F;
			this.lblRateTupe.Name = "lblRateTupe";
			this.lblRateTupe.Style = "font-family: \'Tahoma\'";
			this.lblRateTupe.Text = "Rate Type:";
			this.lblRateTupe.Top = 0.0625F;
			this.lblRateTupe.Width = 1.0625F;
			// 
			// fld30DNW
			// 
			this.fld30DNW.Height = 0.1875F;
			this.fld30DNW.Left = 4.3125F;
			this.fld30DNW.Name = "fld30DNW";
			this.fld30DNW.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fld30DNW.Text = null;
			this.fld30DNW.Top = 0.5F;
			this.fld30DNW.Width = 1F;
			// 
			// lbl30DNW
			// 
			this.lbl30DNW.Height = 0.1875F;
			this.lbl30DNW.HyperLink = null;
			this.lbl30DNW.Left = 3.25F;
			this.lbl30DNW.Name = "lbl30DNW";
			this.lbl30DNW.Style = "font-family: \'Tahoma\'";
			this.lbl30DNW.Text = "30 DN Date:";
			this.lbl30DNW.Top = 0.5F;
			this.lbl30DNW.Width = 1.0625F;
			// 
			// fldLienW
			// 
			this.fldLienW.Height = 0.1875F;
			this.fldLienW.Left = 4.3125F;
			this.fldLienW.Name = "fldLienW";
			this.fldLienW.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldLienW.Text = null;
			this.fldLienW.Top = 0.6875F;
			this.fldLienW.Width = 1F;
			// 
			// lblLienW
			// 
			this.lblLienW.Height = 0.1875F;
			this.lblLienW.HyperLink = null;
			this.lblLienW.Left = 3.25F;
			this.lblLienW.Name = "lblLienW";
			this.lblLienW.Style = "font-family: \'Tahoma\'";
			this.lblLienW.Text = "Lien Date:";
			this.lblLienW.Top = 0.6875F;
			this.lblLienW.Width = 1.0625F;
			// 
			// fldMatW
			// 
			this.fldMatW.Height = 0.1875F;
			this.fldMatW.Left = 4.3125F;
			this.fldMatW.Name = "fldMatW";
			this.fldMatW.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldMatW.Text = null;
			this.fldMatW.Top = 0.875F;
			this.fldMatW.Width = 1F;
			// 
			// lblMatW
			// 
			this.lblMatW.Height = 0.1875F;
			this.lblMatW.HyperLink = null;
			this.lblMatW.Left = 3.25F;
			this.lblMatW.Name = "lblMatW";
			this.lblMatW.Style = "font-family: \'Tahoma\'";
			this.lblMatW.Text = "Maturity Date:";
			this.lblMatW.Top = 0.875F;
			this.lblMatW.Width = 1.0625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.25F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.25F;
			this.Line1.Width = 7F;
			this.Line1.X1 = 0.25F;
			this.Line1.X2 = 7.25F;
			this.Line1.Y1 = 1.25F;
			this.Line1.Y2 = 1.25F;
			// 
			// lblHeaderW
			// 
			this.lblHeaderW.Height = 0.1875F;
			this.lblHeaderW.HyperLink = null;
			this.lblHeaderW.Left = 3.25F;
			this.lblHeaderW.Name = "lblHeaderW";
			this.lblHeaderW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblHeaderW.Text = "Water";
			this.lblHeaderW.Top = 0.3125F;
			this.lblHeaderW.Width = 2.0625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.25F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.5F;
			this.Line2.Width = 2.0625F;
			this.Line2.X1 = 3.25F;
			this.Line2.X2 = 5.3125F;
			this.Line2.Y1 = 0.5F;
			this.Line2.Y2 = 0.5F;
			// 
			// fld30DNS
			// 
			this.fld30DNS.Height = 0.1875F;
			this.fld30DNS.Left = 6.4375F;
			this.fld30DNS.Name = "fld30DNS";
			this.fld30DNS.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fld30DNS.Text = null;
			this.fld30DNS.Top = 0.5F;
			this.fld30DNS.Width = 1F;
			// 
			// lbl30DNS
			// 
			this.lbl30DNS.Height = 0.1875F;
			this.lbl30DNS.HyperLink = null;
			this.lbl30DNS.Left = 5.375F;
			this.lbl30DNS.Name = "lbl30DNS";
			this.lbl30DNS.Style = "font-family: \'Tahoma\'";
			this.lbl30DNS.Text = "30 DN Date:";
			this.lbl30DNS.Top = 0.5F;
			this.lbl30DNS.Width = 1.0625F;
			// 
			// fldLienS
			// 
			this.fldLienS.Height = 0.1875F;
			this.fldLienS.Left = 6.4375F;
			this.fldLienS.Name = "fldLienS";
			this.fldLienS.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldLienS.Text = null;
			this.fldLienS.Top = 0.6875F;
			this.fldLienS.Width = 1F;
			// 
			// lblLienS
			// 
			this.lblLienS.Height = 0.1875F;
			this.lblLienS.HyperLink = null;
			this.lblLienS.Left = 5.375F;
			this.lblLienS.Name = "lblLienS";
			this.lblLienS.Style = "font-family: \'Tahoma\'";
			this.lblLienS.Text = "Lien Date:";
			this.lblLienS.Top = 0.6875F;
			this.lblLienS.Width = 1.0625F;
			// 
			// fldMatS
			// 
			this.fldMatS.Height = 0.1875F;
			this.fldMatS.Left = 6.4375F;
			this.fldMatS.Name = "fldMatS";
			this.fldMatS.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldMatS.Text = null;
			this.fldMatS.Top = 0.875F;
			this.fldMatS.Width = 1F;
			// 
			// lblMatS
			// 
			this.lblMatS.Height = 0.1875F;
			this.lblMatS.HyperLink = null;
			this.lblMatS.Left = 5.375F;
			this.lblMatS.Name = "lblMatS";
			this.lblMatS.Style = "font-family: \'Tahoma\'";
			this.lblMatS.Text = "Maturity Date:";
			this.lblMatS.Top = 0.875F;
			this.lblMatS.Width = 1.0625F;
			// 
			// lblHeaderS
			// 
			this.lblHeaderS.Height = 0.1875F;
			this.lblHeaderS.HyperLink = null;
			this.lblHeaderS.Left = 5.375F;
			this.lblHeaderS.Name = "lblHeaderS";
			this.lblHeaderS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblHeaderS.Text = "Sewer";
			this.lblHeaderS.Top = 0.3125F;
			this.lblHeaderS.Width = 2.0625F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 5.375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.5F;
			this.Line3.Width = 2.0625F;
			this.Line3.X1 = 5.375F;
			this.Line3.X2 = 7.4375F;
			this.Line3.Y1 = 0.5F;
			this.Line3.Y2 = 0.5F;
			// 
			// fldInterestRate
			// 
			this.fldInterestRate.Height = 0.1875F;
			this.fldInterestRate.Left = 2F;
			this.fldInterestRate.Name = "fldInterestRate";
			this.fldInterestRate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldInterestRate.Text = null;
			this.fldInterestRate.Top = 1F;
			this.fldInterestRate.Width = 1.25F;
			// 
			// lblIntRate
			// 
			this.lblIntRate.Height = 0.1875F;
			this.lblIntRate.HyperLink = null;
			this.lblIntRate.Left = 0.9375F;
			this.lblIntRate.Name = "lblIntRate";
			this.lblIntRate.Style = "font-family: \'Tahoma\'";
			this.lblIntRate.Text = "Interest Rate:";
			this.lblIntRate.Top = 1F;
			this.lblIntRate.Width = 1.0625F;
			// 
			// rptRateKeyListing
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateKeyNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStart)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEnd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateTupe)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld30DNW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl30DNW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLienW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMatW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMatW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld30DNS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl30DNS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLienS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMatS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMatS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterestRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblIntRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRateKeyNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStart;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStartDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEnd;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEndDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIntDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRateType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRateTupe;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld30DNW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl30DNW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLienW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMatW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMatW;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderW;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld30DNS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl30DNS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLienS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMatS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMatS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderS;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterestRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblIntRate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
