﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.ComponentModel;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmDataEntry.
	/// </summary>
	partial class frmDataEntry : BaseForm
	{
		public fecherFoundation.FCFrame Frame4;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCCheckBox chkNoBill;
		public Global.T2KOverTypeBox txtReading;
		public Global.T2KOverTypeBox txtConsumption;
		public Global.T2KOverTypeBox txtReadingCode;
		public Global.T2KOverTypeBox txtOverrideWC;
		public Global.T2KOverTypeBox txtOverrideSC;
		public Global.T2KOverTypeBox txtOverrideWA;
		public Global.T2KOverTypeBox txtOverrideSA;
		public FCGrid vsAdjust;
		public fecherFoundation.FCLabel lblChangeOut;
		public fecherFoundation.FCLabel lblCombinedMeter;
		public fecherFoundation.FCLabel lblDEUnitsWarning;
		public fecherFoundation.FCLabel lblPreviousReadingText;
		public fecherFoundation.FCLabel lblPreviousReading;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCLabel lblOvrdWater;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label28;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCTextBox txtPrevReading;
		public fecherFoundation.FCCheckBox chkHistory;
		public fecherFoundation.FCCheckBox chkUnits;
		public fecherFoundation.FCCheckBox chkFlat;
		public fecherFoundation.FCFrame Frame5;
		public FCGrid vsHistory;
		public FCGrid vsRate;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label25;
		public fecherFoundation.FCComboBox cmbSeq;
		public Global.T2KOverTypeBox txtAccount;
		public Global.T2KOverTypeBox txtBillTo;
		public Global.T2KOverTypeBox txtOwner;
		public Global.T2KOverTypeBox txtLocation;
		public Global.T2KOverTypeBox txtSerial;
		public Global.T2KOverTypeBox txtRemote;
		public fecherFoundation.FCLabel lblBookNumber;
		public fecherFoundation.FCLabel lblBookNumberTitle;
		public fecherFoundation.FCLabel Label29;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileMaster;
		public fecherFoundation.FCToolStripMenuItem mnuPrevious;
		public fecherFoundation.FCToolStripMenuItem mnuNext;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDataEntry));
			this.Frame4 = new fecherFoundation.FCFrame();
			this.txtDate = new Global.T2KDateBox();
			this.chkNoBill = new fecherFoundation.FCCheckBox();
			this.txtReading = new Global.T2KOverTypeBox();
			this.txtConsumption = new Global.T2KOverTypeBox();
			this.txtReadingCode = new Global.T2KOverTypeBox();
			this.txtOverrideWC = new Global.T2KOverTypeBox();
			this.txtOverrideSC = new Global.T2KOverTypeBox();
			this.txtOverrideWA = new Global.T2KOverTypeBox();
			this.txtOverrideSA = new Global.T2KOverTypeBox();
			this.vsAdjust = new fecherFoundation.FCGrid();
			this.lblChangeOut = new fecherFoundation.FCLabel();
			this.lblCombinedMeter = new fecherFoundation.FCLabel();
			this.lblDEUnitsWarning = new fecherFoundation.FCLabel();
			this.lblPreviousReadingText = new fecherFoundation.FCLabel();
			this.lblPreviousReading = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label21 = new fecherFoundation.FCLabel();
			this.lblOvrdWater = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label28 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.txtPrevReading = new fecherFoundation.FCTextBox();
			this.chkHistory = new fecherFoundation.FCCheckBox();
			this.chkUnits = new fecherFoundation.FCCheckBox();
			this.chkFlat = new fecherFoundation.FCCheckBox();
			this.Frame5 = new fecherFoundation.FCFrame();
			this.vsHistory = new fecherFoundation.FCGrid();
			this.vsRate = new fecherFoundation.FCGrid();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label25 = new fecherFoundation.FCLabel();
			this.cmbSeq = new fecherFoundation.FCComboBox();
			this.txtAccount = new Global.T2KOverTypeBox();
			this.txtBillTo = new Global.T2KOverTypeBox();
			this.txtOwner = new Global.T2KOverTypeBox();
			this.txtLocation = new Global.T2KOverTypeBox();
			this.txtSerial = new Global.T2KOverTypeBox();
			this.txtRemote = new Global.T2KOverTypeBox();
			this.lblBookNumber = new fecherFoundation.FCLabel();
			this.lblBookNumberTitle = new fecherFoundation.FCLabel();
			this.Label29 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileMaster = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrevious = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNext = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdFileMaster = new fecherFoundation.FCButton();
			this.cmdPrevious = new fecherFoundation.FCButton();
			this.cmdNext = new fecherFoundation.FCButton();
			this.customProperties = new Wisej.Web.Ext.CustomProperties.CustomProperties(this.components);
			//FC:FINAL:MSH - issue #967: create JavaScript and assign it to textboxes to allow enter only number values
			this.javaScript1 = new Wisej.Web.JavaScript(components);
			clientEventKeyPress_checkService = new JavaScript.ClientEvent();
			clientEventKeyPress_checkService.Event = "keypress";
			clientEventKeyPress_checkService.JavaScript = "Sewer_Water_CheckService(this, e)";
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
			this.Frame4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNoBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReadingCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverrideWC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverrideSC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverrideWA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverrideSA)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsAdjust)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHistory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFlat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
			this.Frame5.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsHistory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillTo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSerial)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemote)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMaster)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNext)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame5);
			this.ClientArea.Controls.Add(this.Frame4);
			this.ClientArea.Controls.Add(this.chkHistory);
			this.ClientArea.Controls.Add(this.chkUnits);
			this.ClientArea.Controls.Add(this.chkFlat);
			this.ClientArea.Controls.Add(this.cmbSeq);
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.txtBillTo);
			this.ClientArea.Controls.Add(this.txtOwner);
			this.ClientArea.Controls.Add(this.txtLocation);
			this.ClientArea.Controls.Add(this.txtSerial);
			this.ClientArea.Controls.Add(this.txtRemote);
			this.ClientArea.Controls.Add(this.lblBookNumber);
			this.ClientArea.Controls.Add(this.lblBookNumberTitle);
			this.ClientArea.Controls.Add(this.Label29);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label3);
			this.ClientArea.Controls.Add(this.Label4);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.txtPrevReading);
			this.ClientArea.Size = new System.Drawing.Size(1078, 702);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNext);
			this.TopPanel.Controls.Add(this.cmdPrevious);
			this.TopPanel.Controls.Add(this.cmdFileMaster);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileMaster, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdPrevious, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNext, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.ForeColor = System.Drawing.Color.FromName("@windowText");
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(128, 30);
			this.HeaderText.Text = "Data Entry";
			// 
			// Frame4
			// 
			this.Frame4.AppearanceKey = "groupBoxNoBorders";
			this.Frame4.Controls.Add(this.txtDate);
			this.Frame4.Controls.Add(this.chkNoBill);
			this.Frame4.Controls.Add(this.txtReading);
			this.Frame4.Controls.Add(this.txtConsumption);
			this.Frame4.Controls.Add(this.txtReadingCode);
			this.Frame4.Controls.Add(this.txtOverrideWC);
			this.Frame4.Controls.Add(this.txtOverrideSC);
			this.Frame4.Controls.Add(this.txtOverrideWA);
			this.Frame4.Controls.Add(this.txtOverrideSA);
			this.Frame4.Controls.Add(this.vsAdjust);
			this.Frame4.Controls.Add(this.lblChangeOut);
			this.Frame4.Controls.Add(this.lblCombinedMeter);
			this.Frame4.Controls.Add(this.lblDEUnitsWarning);
			this.Frame4.Controls.Add(this.lblPreviousReadingText);
			this.Frame4.Controls.Add(this.lblPreviousReading);
			this.Frame4.Controls.Add(this.Label13);
			this.Frame4.Controls.Add(this.Label21);
			this.Frame4.Controls.Add(this.lblOvrdWater);
			this.Frame4.Controls.Add(this.Label15);
			this.Frame4.Controls.Add(this.Label12);
			this.Frame4.Controls.Add(this.Label14);
			this.Frame4.Controls.Add(this.Label28);
			this.Frame4.Controls.Add(this.Label9);
			this.Frame4.Controls.Add(this.Label10);
			this.Frame4.Controls.Add(this.Label11);
			this.Frame4.Location = new System.Drawing.Point(0, 290);
			this.Frame4.Name = "Frame4";
			this.Frame4.Size = new System.Drawing.Size(1050, 314);
			this.Frame4.TabIndex = 29;
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(199, 90);
			this.txtDate.Mask = "##/##/####";
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(143, 40);
			this.txtDate.TabIndex = 0;
			this.txtDate.Text = "  /  /";
			//FC:FINAL:MSH - issue #967: assign JavaScript to textbox to allow enter only number values
			this.txtDate.TextChanged += new System.EventHandler(this.txtDate_Change);
			this.txtDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDate_Validate);
			// 
			// chkNoBill
			// 
			this.chkNoBill.Location = new System.Drawing.Point(30, 0);
			this.chkNoBill.Name = "chkNoBill";
			this.chkNoBill.Size = new System.Drawing.Size(106, 27);
			this.chkNoBill.TabIndex = 8;
			this.chkNoBill.TabStop = false;
			this.chkNoBill.Text = "No Charge";
			this.chkNoBill.CheckedChanged += new System.EventHandler(this.chkNoBill_CheckedChanged);
			//FC:FINAL:CHN - issue #963: not active style of text
			this.chkNoBill.ForeColor = this.Label29.ForeColor;
			this.chkNoBill.CheckedForeColor = this.Label29.ForeColor;
			// 
			// txtReading
			// 
			this.txtReading.AutoSize = false;
			this.txtReading.LinkItem = null;
			this.txtReading.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReading.LinkTopic = null;
			this.txtReading.Location = new System.Drawing.Point(199, 140);
			this.txtReading.MaxLength = 10;
			this.txtReading.Name = "txtReading";
			this.txtReading.Size = new System.Drawing.Size(143, 40);
			//FC:FINAL:MSH - Issue #940: in original app txtReading has 0 tabindex and it will be focused after showing form
			this.txtReading.TabIndex = 2;
			this.txtReading.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReading_KeyPressEvent);
			this.txtReading.Enter += new System.EventHandler(this.txtReading_Enter);
			this.txtReading.Leave += new System.EventHandler(this.txtReading_Leave);
			this.txtReading.TextChanged += new System.EventHandler(this.txtReading_Change);
			this.txtReading.Validating += new System.ComponentModel.CancelEventHandler(this.txtReading_Validate);
			// 
			// txtConsumption
			// 
			this.txtConsumption.Locked = true;
			this.txtConsumption.AutoSize = false;
			this.txtConsumption.LinkItem = null;
			this.txtConsumption.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtConsumption.LinkTopic = null;
			this.txtConsumption.Location = new System.Drawing.Point(199, 190);
			this.txtConsumption.MaxLength = 10;
			this.txtConsumption.Name = "txtConsumption";
			this.txtConsumption.Size = new System.Drawing.Size(143, 40);
			this.txtConsumption.TabIndex = 3;
			this.txtConsumption.TabStop = false;
			this.txtConsumption.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtConsumption_KeyPressEvent);
			this.txtConsumption.TextChanged += new System.EventHandler(this.txtConsumption_Change);
			this.txtConsumption.Validating += new System.ComponentModel.CancelEventHandler(this.txtConsumption_Validate);
			// 
			// txtReadingCode
			// 
			this.txtReadingCode.Locked = true;
			this.txtReadingCode.AutoSize = false;
			this.txtReadingCode.LinkItem = null;
			this.txtReadingCode.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReadingCode.LinkTopic = null;
			this.txtReadingCode.Location = new System.Drawing.Point(152, 140);
			this.txtReadingCode.MaxLength = 1;
			this.txtReadingCode.Name = "txtReadingCode";
			this.txtReadingCode.Size = new System.Drawing.Size(40, 40);
			this.txtReadingCode.TabIndex = 1;
			this.txtReadingCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtReadingCode_KeyPressEvent);
			this.txtReadingCode.DoubleClick += new System.EventHandler(this.txtReadingCode_DblClick);
			this.txtReadingCode.TextChanged += new System.EventHandler(this.txtReadingCode_Change);
			this.txtReadingCode.Validating += new System.ComponentModel.CancelEventHandler(this.txtReadingCode_Validate);
			// 
			// txtOverrideWC
			// 
			this.txtOverrideWC.AutoSize = false;
			this.txtOverrideWC.LinkItem = null;
			this.txtOverrideWC.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOverrideWC.LinkTopic = null;
			this.txtOverrideWC.Location = new System.Drawing.Point(467, 140);
			this.txtOverrideWC.MaxLength = 7;
			this.txtOverrideWC.Name = "txtOverrideWC";
			this.txtOverrideWC.Size = new System.Drawing.Size(100, 40);
			this.txtOverrideWC.TabIndex = 4;
			//FC:FINAL:MSH - issue #987: assign Javascript to check service value and cancel entering values in appropriate textbox
			this.javaScript1.GetJavaScriptEvents(this.txtOverrideWC).Add(this.clientEventKeyPress_checkService);
			this.txtOverrideWC.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOverrideWC_KeyPressEvent);
			this.txtOverrideWC.TextChanged += new System.EventHandler(this.txtOverrideWC_Change);
			// 
			// txtOverrideSC
			// 
			this.txtOverrideSC.AutoSize = false;
			this.txtOverrideSC.LinkItem = null;
			this.txtOverrideSC.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOverrideSC.LinkTopic = null;
			this.txtOverrideSC.Location = new System.Drawing.Point(467, 190);
			this.txtOverrideSC.MaxLength = 7;
			this.txtOverrideSC.Name = "txtOverrideSC";
			this.txtOverrideSC.Size = new System.Drawing.Size(100, 40);
			this.txtOverrideSC.TabIndex = 9;
			//FC:FINAL:MSH - issue #987: assign Javascript to check service value and cancel entering values in appropriate textbox
			this.javaScript1.GetJavaScriptEvents(this.txtOverrideSC).Add(this.clientEventKeyPress_checkService);
			this.txtOverrideSC.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOverrideSC_KeyPressEvent);
			this.txtOverrideSC.TextChanged += new System.EventHandler(this.txtOverrideSC_Change);
			// 
			// txtOverrideWA
			// 
			this.txtOverrideWA.AutoSize = false;
			this.txtOverrideWA.LinkItem = null;
			this.txtOverrideWA.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOverrideWA.LinkTopic = null;
			this.txtOverrideWA.Location = new System.Drawing.Point(577, 140);
			this.txtOverrideWA.MaxLength = 7;
			this.txtOverrideWA.Name = "txtOverrideWA";
			this.txtOverrideWA.Size = new System.Drawing.Size(100, 40);
			this.txtOverrideWA.TabIndex = 8;
			//FC:FINAL:MSH - issue #987: assign Javascript to check service value and cancel entering values in appropriate textbox
			this.javaScript1.GetJavaScriptEvents(this.txtOverrideWA).Add(this.clientEventKeyPress_checkService);
			this.txtOverrideWA.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOverrideWA_KeyPressEvent);
			this.txtOverrideWA.TextChanged += new System.EventHandler(this.txtOverrideWA_Change);
			// 
			// txtOverrideSA
			// 
			this.txtOverrideSA.AutoSize = false;
			this.txtOverrideSA.LinkItem = null;
			this.txtOverrideSA.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOverrideSA.LinkTopic = null;
			this.txtOverrideSA.Location = new System.Drawing.Point(577, 190);
			this.txtOverrideSA.MaxLength = 7;
			this.txtOverrideSA.Name = "txtOverrideSA";
			this.txtOverrideSA.Size = new System.Drawing.Size(100, 40);
			this.txtOverrideSA.TabIndex = 10;
			//FC:FINAL:MSH - issue #987: assign Javascript to check service value and cancel entering values in appropriate textbox
			this.javaScript1.GetJavaScriptEvents(this.txtOverrideSA).Add(this.clientEventKeyPress_checkService);
			this.txtOverrideSA.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOverrideSA_KeyPressEvent);
			this.txtOverrideSA.TextChanged += new System.EventHandler(this.txtOverrideSA_Change);
			// 
			// vsAdjust
			// 
			this.vsAdjust.AllowSelection = false;
			this.vsAdjust.AllowUserToResizeColumns = false;
			this.vsAdjust.AllowUserToResizeRows = false;
			this.vsAdjust.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsAdjust.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAdjust.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAdjust.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAdjust.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAdjust.BackColorSel = System.Drawing.Color.Empty;
			this.vsAdjust.Cols = 5;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAdjust.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsAdjust.ColumnHeadersHeight = 30;
			this.vsAdjust.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAdjust.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsAdjust.DragIcon = null;
			this.vsAdjust.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAdjust.FixedCols = 0;
			this.vsAdjust.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAdjust.FrozenCols = 0;
			this.vsAdjust.GridColor = System.Drawing.Color.Empty;
			this.vsAdjust.GridColorFixed = System.Drawing.Color.Empty;
			this.vsAdjust.Location = new System.Drawing.Point(707, 90);
			this.vsAdjust.Name = "vsAdjust";
			this.vsAdjust.ReadOnly = true;
			this.vsAdjust.RowHeadersVisible = false;
			this.vsAdjust.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAdjust.RowHeightMin = 0;
			this.vsAdjust.Rows = 5;
			this.vsAdjust.ScrollTipText = null;
			this.vsAdjust.ShowColumnVisibilityMenu = false;
			//FC:FINAL:CHN - issue #966: Expand Grid.
			this.vsAdjust.Size = new System.Drawing.Size(327, 200);
			// was (327, 140)
			this.vsAdjust.StandardTab = true;
			this.vsAdjust.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsAdjust.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsAdjust.TabIndex = 18;
			this.vsAdjust.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsAdjust_CellChanged);
			this.vsAdjust.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsAdjust_AfterEdit);
			this.vsAdjust.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsAdjust_ValidateEdit);
			this.vsAdjust.CurrentCellChanged += new System.EventHandler(this.vsAdjust_RowColChange);
			this.vsAdjust.Enter += new System.EventHandler(this.vsAdjust_Enter);
			// 
			// lblChangeOut
			// 
			this.lblChangeOut.Location = new System.Drawing.Point(30, 300);
			this.lblChangeOut.Name = "lblChangeOut";
			this.lblChangeOut.Size = new System.Drawing.Size(598, 15);
			this.lblChangeOut.TabIndex = 50;
			this.lblChangeOut.Text = "THE ORIGINAL METER WAS CHANGED.  THE ORIGINAL ONSUMPTION AMOUNT WILL BE ADDED";
			// 
			// lblCombinedMeter
			// 
			this.lblCombinedMeter.Location = new System.Drawing.Point(30, 275);
			this.lblCombinedMeter.Name = "lblCombinedMeter";
			this.lblCombinedMeter.Size = new System.Drawing.Size(684, 15);
			this.lblCombinedMeter.TabIndex = 49;
			this.lblCombinedMeter.Text = "THIS METER IS A COMBINED METER. CALCULATED CONSUMPTION AMOUNT WILL BE ADDED TO TH" + "E PRIMARY METER";
			this.lblCombinedMeter.Visible = false;
			// 
			// lblDEUnitsWarning
			// 
			this.lblDEUnitsWarning.Location = new System.Drawing.Point(30, 250);
			this.lblDEUnitsWarning.Name = "lblDEUnitsWarning";
			this.lblDEUnitsWarning.Size = new System.Drawing.Size(434, 15);
			this.lblDEUnitsWarning.TabIndex = 48;
			this.lblDEUnitsWarning.Text = "ALL READINGS SHOULD BE ENTERED IN UNITS OF 100 CUBIC FEET";
			this.lblDEUnitsWarning.Visible = false;
			// 
			// lblPreviousReadingText
			// 
			this.lblPreviousReadingText.Location = new System.Drawing.Point(349, 10);
			this.lblPreviousReadingText.Name = "lblPreviousReadingText";
			this.lblPreviousReadingText.Size = new System.Drawing.Size(96, 16);
			this.lblPreviousReadingText.TabIndex = 45;
			// 
			// lblPreviousReading
			// 
			this.lblPreviousReading.Location = new System.Drawing.Point(190, 10);
			this.lblPreviousReading.Name = "lblPreviousReading";
			this.lblPreviousReading.Size = new System.Drawing.Size(128, 16);
			this.lblPreviousReading.TabIndex = 44;
			this.lblPreviousReading.Text = "PREVIOUS READING";
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(707, 65);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(327, 16);
			this.Label13.TabIndex = 42;
			this.Label13.Text = "ADJUSTMENTS";
			this.Label13.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label21
			// 
			this.Label21.Location = new System.Drawing.Point(396, 204);
			this.Label21.Name = "Label21";
			this.Label21.Size = new System.Drawing.Size(50, 16);
			this.Label21.TabIndex = 41;
			this.Label21.Text = "SEWER";
			// 
			// lblOvrdWater
			// 
			this.lblOvrdWater.Location = new System.Drawing.Point(396, 154);
			this.lblOvrdWater.Name = "lblOvrdWater";
			this.lblOvrdWater.Size = new System.Drawing.Size(65, 16);
			this.lblOvrdWater.TabIndex = 40;
			this.lblOvrdWater.Text = "WATER";
			// 
			// Label15
			// 
			this.Label15.Location = new System.Drawing.Point(492, 104);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(45, 15);
			this.Label15.TabIndex = 39;
			this.Label15.Text = "CONS";
			this.Label15.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(396, 65);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(281, 16);
			this.Label12.TabIndex = 38;
			this.Label12.Text = "OVERRIDE";
			this.Label12.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label14
			// 
			//FC:FINAL:CHN - issue #964: Fix label positions.
			this.Label14.Location = new System.Drawing.Point(600, 104);
			// was: (522, 104)
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(54, 15);
			this.Label14.TabIndex = 37;
			this.Label14.Text = "AMOUNT";
			this.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label28
			// 
			this.Label28.Location = new System.Drawing.Point(30, 204);
			this.Label28.Name = "Label28";
			this.Label28.Size = new System.Drawing.Size(100, 16);
			this.Label28.TabIndex = 33;
			this.Label28.Text = "CONSUMPTION";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(30, 65);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(312, 16);
			this.Label9.TabIndex = 32;
			this.Label9.Text = "CURRENT";
			this.Label9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(30, 104);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(84, 16);
			this.Label10.TabIndex = 31;
			this.Label10.Text = "DATE";
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(30, 154);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(84, 16);
			this.Label11.TabIndex = 30;
			this.Label11.Text = "READING";
			// 
			// txtPrevReading
			// 
			this.txtPrevReading.AutoSize = false;
			this.txtPrevReading.BackColor = System.Drawing.SystemColors.Window;
			this.txtPrevReading.LinkItem = null;
			this.txtPrevReading.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPrevReading.LinkTopic = null;
			this.txtPrevReading.Location = new System.Drawing.Point(30, 30);
			this.txtPrevReading.Name = "txtPrevReading";
			this.txtPrevReading.Size = new System.Drawing.Size(66, 40);
			this.txtPrevReading.TabIndex = 28;
			this.txtPrevReading.TabStop = false;
			this.txtPrevReading.Visible = false;
			// 
			// chkHistory
			// 
			this.chkHistory.Location = new System.Drawing.Point(872, 130);
			this.chkHistory.Name = "chkHistory";
			this.chkHistory.Size = new System.Drawing.Size(137, 27);
			this.chkHistory.TabIndex = 12;
			this.chkHistory.Text = "Display History";
			this.chkHistory.CheckedChanged += new System.EventHandler(this.chkHistory_CheckedChanged);
			//FC:FINAL:CHN - issue #963: not active style of text
			this.chkHistory.ForeColor = this.Label29.ForeColor;
			this.chkHistory.CheckedForeColor = this.Label29.ForeColor;
			// 
			// chkUnits
			// 
			this.chkUnits.Checked = true;
			this.chkUnits.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkUnits.Location = new System.Drawing.Point(872, 80);
			this.chkUnits.Name = "chkUnits";
			this.chkUnits.Size = new System.Drawing.Size(125, 27);
			this.chkUnits.TabIndex = 11;
			this.chkUnits.Text = "Stop on Units";
			//FC:FINAL:CHN - issue #963: not active style of text
			this.chkUnits.ForeColor = this.Label29.ForeColor;
			this.chkUnits.CheckedForeColor = this.Label29.ForeColor;
			// 
			// chkFlat
			// 
			this.chkFlat.Checked = true;
			this.chkFlat.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkFlat.Location = new System.Drawing.Point(872, 30);
			this.chkFlat.Name = "chkFlat";
			this.chkFlat.Size = new System.Drawing.Size(155, 27);
			this.chkFlat.TabIndex = 10;
			this.chkFlat.Text = "Stop on Flat Rate";
			//FC:FINAL:CHN - issue #963: not active style of text
			this.chkFlat.ForeColor = this.Label29.ForeColor;
			this.chkFlat.CheckedForeColor = this.Label29.ForeColor;
			// 
			// Frame5
			// 
			this.Frame5.AppearanceKey = "groupBoxNoBorders";
			this.Frame5.Controls.Add(this.vsHistory);
			this.Frame5.Controls.Add(this.vsRate);
			this.Frame5.Controls.Add(this.Label8);
			this.Frame5.Controls.Add(this.Label25);
			this.Frame5.Location = new System.Drawing.Point(0, 624);
			this.Frame5.Name = "Frame5";
			this.Frame5.Size = new System.Drawing.Size(1050, 369);
			this.Frame5.TabIndex = 26;
			// 
			// vsHistory
			// 
			this.vsHistory.AllowSelection = false;
			this.vsHistory.AllowUserToResizeColumns = false;
			this.vsHistory.AllowUserToResizeRows = false;
			this.vsHistory.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsHistory.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsHistory.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsHistory.BackColorBkg = System.Drawing.Color.Empty;
			this.vsHistory.BackColorFixed = System.Drawing.Color.Empty;
			this.vsHistory.BackColorSel = System.Drawing.Color.Empty;
			this.vsHistory.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsHistory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsHistory.ColumnHeadersHeight = 30;
			this.vsHistory.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsHistory.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsHistory.DragIcon = null;
			this.vsHistory.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsHistory.ExtendLastCol = true;
			this.vsHistory.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsHistory.FrozenCols = 0;
			this.vsHistory.GridColor = System.Drawing.Color.Empty;
			this.vsHistory.GridColorFixed = System.Drawing.Color.Empty;
			this.vsHistory.Location = new System.Drawing.Point(542, 43);
			this.vsHistory.Name = "vsHistory";
			this.vsHistory.ReadOnly = true;
			this.vsHistory.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsHistory.RowHeightMin = 0;
			this.vsHistory.Rows = 1;
			this.vsHistory.ScrollTipText = null;
			this.vsHistory.ShowColumnVisibilityMenu = false;
			this.vsHistory.Size = new System.Drawing.Size(492, 314);
			this.vsHistory.StandardTab = true;
			this.vsHistory.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsHistory.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsHistory.TabIndex = 46;
			this.vsHistory.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vsHistory_AfterCollapse);
			this.vsHistory.DoubleClick += new System.EventHandler(this.vsHistory_DblClick);
			// 
			// vsRate
			// 
			this.vsRate.AllowSelection = false;
			this.vsRate.AllowUserToResizeColumns = false;
			this.vsRate.AllowUserToResizeRows = false;
			this.vsRate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left)));
			this.vsRate.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsRate.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsRate.BackColorBkg = System.Drawing.Color.Empty;
			this.vsRate.BackColorFixed = System.Drawing.Color.Empty;
			this.vsRate.BackColorSel = System.Drawing.Color.Empty;
			this.vsRate.Cols = 3;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsRate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsRate.ColumnHeadersHeight = 30;
			this.vsRate.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsRate.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsRate.DragIcon = null;
			this.vsRate.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsRate.FixedCols = 0;
			this.vsRate.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsRate.FrozenCols = 0;
			this.vsRate.GridColor = System.Drawing.Color.Empty;
			this.vsRate.GridColorFixed = System.Drawing.Color.Empty;
			this.vsRate.Location = new System.Drawing.Point(30, 43);
			this.vsRate.Name = "vsRate";
			this.vsRate.ReadOnly = true;
			this.vsRate.RowHeadersVisible = false;
			this.vsRate.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsRate.RowHeightMin = 0;
			this.vsRate.Rows = 6;
			this.vsRate.ScrollTipText = null;
			this.vsRate.ShowColumnVisibilityMenu = false;
			this.vsRate.Size = new System.Drawing.Size(492, 234);
			this.vsRate.StandardTab = true;
			this.vsRate.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsRate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsRate.TabIndex = 35;
			this.vsRate.CurrentCellChanged += new System.EventHandler(this.vsRate_RowColChange);
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(542, 13);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(492, 16);
			this.Label8.TabIndex = 36;
			this.Label8.Text = "HISTORY";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label25
			// 
			this.Label25.Location = new System.Drawing.Point(30, 13);
			this.Label25.Name = "Label25";
			this.Label25.Size = new System.Drawing.Size(490, 16);
			this.Label25.TabIndex = 27;
			this.Label25.Text = "RATE TABLE";
			this.Label25.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// cmbSeq
			// 
			this.cmbSeq.AutoSize = false;
			this.cmbSeq.BackColor = System.Drawing.SystemColors.Window;
			this.cmbSeq.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSeq.FormattingEnabled = true;
			this.cmbSeq.Location = new System.Drawing.Point(127, 30);
			this.cmbSeq.Name = "cmbSeq";
			this.cmbSeq.Size = new System.Drawing.Size(398, 40);
			this.cmbSeq.TabIndex = 6;
			this.cmbSeq.SelectedIndexChanged += new System.EventHandler(this.cmbSeq_SelectedIndexChanged);
			this.cmbSeq.DropDown += new System.EventHandler(this.cmbSeq_DropDown);
			this.cmbSeq.KeyPress += new Wisej.Web.KeyPressEventHandler(this.cmbSeq_KeyPress);
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.LinkItem = null;
			this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAccount.LinkTopic = null;
			this.txtAccount.Location = new System.Drawing.Point(127, 80);
			//FC:FINAL:MSH - issue #956: restore missing property(in original field locked by default)
			this.txtAccount.Locked = true;
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(398, 40);
			this.txtAccount.TabIndex = 13;
			this.txtAccount.TabStop = false;
			// 
			// txtBillTo
			// 
			this.txtBillTo.AutoSize = false;
			this.txtBillTo.LinkItem = null;
			this.txtBillTo.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtBillTo.LinkTopic = null;
			this.txtBillTo.Location = new System.Drawing.Point(127, 130);
			//FC:FINAL:MSH - issue #956: restore missing property(in original field locked by default)
			this.txtBillTo.Locked = true;
			this.txtBillTo.Name = "txtBillTo";
			this.txtBillTo.Size = new System.Drawing.Size(398, 40);
			this.txtBillTo.TabIndex = 14;
			this.txtBillTo.TabStop = false;
			// 
			// txtOwner
			// 
			this.txtOwner.AutoSize = false;
			this.txtOwner.LinkItem = null;
			this.txtOwner.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtOwner.LinkTopic = null;
			this.txtOwner.Location = new System.Drawing.Point(127, 180);
			//FC:FINAL:MSH - issue #956: restore missing property(in original field locked by default)
			this.txtOwner.Locked = true;
			this.txtOwner.Name = "txtOwner";
			this.txtOwner.Size = new System.Drawing.Size(398, 40);
			this.txtOwner.TabIndex = 15;
			this.txtOwner.TabStop = false;
			// 
			// txtLocation
			// 
			this.txtLocation.AutoSize = false;
			this.txtLocation.LinkItem = null;
			this.txtLocation.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtLocation.LinkTopic = null;
			this.txtLocation.Location = new System.Drawing.Point(127, 230);
			//FC:FINAL:MSH - issue #956: restore missing property(in original field locked by default)
			this.txtLocation.Locked = true;
			this.txtLocation.Name = "txtLocation";
			this.txtLocation.Size = new System.Drawing.Size(398, 40);
			this.txtLocation.TabIndex = 16;
			this.txtLocation.TabStop = false;
			// 
			// txtSerial
			// 
			this.txtSerial.AutoSize = false;
			this.txtSerial.LinkItem = null;
			this.txtSerial.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSerial.LinkTopic = null;
			this.txtSerial.Location = new System.Drawing.Point(654, 80);
			//FC:FINAL:MSH - issue #956: restore missing property(in original field locked by default)
			this.txtSerial.Locked = true;
			this.txtSerial.Name = "txtSerial";
			this.txtSerial.Size = new System.Drawing.Size(188, 40);
			this.txtSerial.TabIndex = 17;
			this.txtSerial.TabStop = false;
			// 
			// txtRemote
			// 
			this.txtRemote.AutoSize = false;
			this.txtRemote.LinkItem = null;
			this.txtRemote.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRemote.LinkTopic = null;
			this.txtRemote.Location = new System.Drawing.Point(654, 130);
			//FC:FINAL:MSH - issue #956: restore missing property(in original field locked by default)
			this.txtRemote.Locked = true;
			this.txtRemote.Name = "txtRemote";
			this.txtRemote.Size = new System.Drawing.Size(188, 40);
			this.txtRemote.TabIndex = 18;
			this.txtRemote.TabStop = false;
			// 
			// lblBookNumber
			// 
			this.lblBookNumber.Location = new System.Drawing.Point(600, 50);
			this.lblBookNumber.Name = "lblBookNumber";
			this.lblBookNumber.Size = new System.Drawing.Size(36, 16);
			this.lblBookNumber.TabIndex = 43;
			// 
			// lblBookNumberTitle
			// 
			this.lblBookNumberTitle.Location = new System.Drawing.Point(555, 50);
			this.lblBookNumberTitle.Name = "lblBookNumberTitle";
			this.lblBookNumberTitle.Size = new System.Drawing.Size(44, 15);
			this.lblBookNumberTitle.TabIndex = 34;
			this.lblBookNumberTitle.Text = "BOOK";
			// 
			// Label29
			// 
			this.Label29.Location = new System.Drawing.Point(30, 44);
			this.Label29.Name = "Label29";
			this.Label29.Size = new System.Drawing.Size(66, 16);
			this.Label29.TabIndex = 25;
			this.Label29.Text = "SEQUENCE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 94);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(66, 16);
			this.Label1.TabIndex = 24;
			this.Label1.Text = "ACCOUNT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 144);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(66, 16);
			this.Label2.TabIndex = 23;
			this.Label2.Text = "TENANT";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 194);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(66, 16);
			this.Label3.TabIndex = 22;
			this.Label3.Text = "OWNER";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 244);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(64, 16);
			this.Label4.TabIndex = 21;
			this.Label4.Text = "LOCATION";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(555, 94);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(64, 15);
			this.Label6.TabIndex = 20;
			this.Label6.Text = "SERIAL#";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(555, 144);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(64, 15);
			this.Label7.TabIndex = 19;
			this.Label7.Text = "REMOTE#";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileMaster,
				this.mnuPrevious,
				this.mnuNext,
				this.mnuSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFileMaster
			// 
			this.mnuFileMaster.Index = 0;
			this.mnuFileMaster.Name = "mnuFileMaster";
			this.mnuFileMaster.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuFileMaster.Text = "Show Account/Meter Screen";
			this.mnuFileMaster.Click += new System.EventHandler(this.mnuFileMaster_Click);
			// 
			// mnuPrevious
			// 
			this.mnuPrevious.Enabled = false;
			this.mnuPrevious.Index = 1;
			this.mnuPrevious.Name = "mnuPrevious";
			this.mnuPrevious.Shortcut = Wisej.Web.Shortcut.F7;
			this.mnuPrevious.Text = "Move Previous";
			this.mnuPrevious.Click += new System.EventHandler(this.mnuPrevious_Click);
			// 
			// mnuNext
			// 
			this.mnuNext.Enabled = false;
			this.mnuNext.Index = 2;
			this.mnuNext.Name = "mnuNext";
			this.mnuNext.Shortcut = Wisej.Web.Shortcut.F8;
			this.mnuNext.Text = "Move Next";
			this.mnuNext.Click += new System.EventHandler(this.mnuNext_Click);
			// 
			// mnuSave
			// 
			this.mnuSave.Enabled = false;
			this.mnuSave.Index = 3;
			this.mnuSave.Name = "mnuSave";
			this.mnuSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSave.Text = "Save & Advance";
			this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 5;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Enabled = false;
			this.cmdSave.Location = new System.Drawing.Point(364, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(156, 48);
			this.cmdSave.TabIndex = 51;
			this.cmdSave.Text = "Save & Advance";
			this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
			// 
			// cmdFileMaster
			// 
			this.cmdFileMaster.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileMaster.AppearanceKey = "toolbarButton";
			this.cmdFileMaster.Location = new System.Drawing.Point(658, 29);
			this.cmdFileMaster.Name = "cmdFileMaster";
			this.cmdFileMaster.Shortcut = Wisej.Web.Shortcut.F6;
			this.cmdFileMaster.Size = new System.Drawing.Size(190, 24);
			this.cmdFileMaster.TabIndex = 52;
			this.cmdFileMaster.Text = "Show Account/Meter Screen";
			this.cmdFileMaster.Click += new System.EventHandler(this.mnuFileMaster_Click);
			// 
			// cmdPrevious
			// 
			this.cmdPrevious.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdPrevious.AppearanceKey = "toolbarButton";
			this.cmdPrevious.Enabled = false;
			this.cmdPrevious.Location = new System.Drawing.Point(854, 29);
			this.cmdPrevious.Name = "cmdPrevious";
			this.cmdPrevious.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdPrevious.Size = new System.Drawing.Size(106, 24);
			this.cmdPrevious.TabIndex = 53;
			this.cmdPrevious.Text = "Move Previous";
			this.cmdPrevious.Click += new System.EventHandler(this.mnuPrevious_Click);
			// 
			// cmdNext
			// 
			this.cmdNext.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNext.AppearanceKey = "toolbarButton";
			this.cmdNext.Enabled = false;
			this.cmdNext.Location = new System.Drawing.Point(966, 29);
			this.cmdNext.Name = "cmdNext";
			this.cmdNext.Shortcut = Wisej.Web.Shortcut.F8;
			this.cmdNext.Size = new System.Drawing.Size(84, 24);
			this.cmdNext.TabIndex = 54;
			this.cmdNext.Text = "Move Next";
			this.cmdNext.Click += new System.EventHandler(this.mnuNext_Click);
			// 
			// frmDataEntry
			// 
			this.BackColor = System.Drawing.SystemColors.Menu;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = -2147483644;
			this.ForeColor = System.Drawing.SystemColors.Control;
			this.KeyPreview = true;
			this.Name = "frmDataEntry";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Data Entry";
			this.Load += new System.EventHandler(this.frmDataEntry_Load);
			this.Activated += new System.EventHandler(this.frmDataEntry_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmDataEntry_KeyPress);
			this.Resize += new System.EventHandler(this.frmDataEntry_Resize);
			this.Appear += new EventHandler(FrmDataEntry_Appear);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
			this.Frame4.ResumeLayout(false);
			this.Frame4.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkNoBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtReadingCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverrideWC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverrideSC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverrideWA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOverrideSA)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsAdjust)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkHistory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkFlat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
			this.Frame5.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsHistory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtBillTo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSerial)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtRemote)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileMaster)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNext)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private IContainer components;
		private FCButton cmdSave;
		private FCButton cmdFileMaster;
		private FCButton cmdPrevious;
		private FCButton cmdNext;
		private JavaScript javaScript1;
		private Wisej.Web.JavaScript.ClientEvent clientEventKeyPress_checkService;
		private Wisej.Web.Ext.CustomProperties.CustomProperties customProperties;
	}
}
