﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmRemoveLien.
	/// </summary>
	partial class frmRemoveLien : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public fecherFoundation.FCFrame fraAccount;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCLabel lblYearTitle;
		public fecherFoundation.FCFrame fraPayments;
		public FCGrid vsPayments;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileRemove;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRemoveLien));
			this.cmbWS = new fecherFoundation.FCComboBox();
			this.lblWS = new fecherFoundation.FCLabel();
			this.fraAccount = new fecherFoundation.FCFrame();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.lblAccountTitle = new fecherFoundation.FCLabel();
			this.lblYearTitle = new fecherFoundation.FCLabel();
			this.fraPayments = new fecherFoundation.FCFrame();
			this.vsPayments = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileRemove = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileRemove = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccount)).BeginInit();
			this.fraAccount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPayments)).BeginInit();
			this.fraPayments.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRemove)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileRemove);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(695, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraAccount);
			this.ClientArea.Controls.Add(this.fraPayments);
			this.ClientArea.Size = new System.Drawing.Size(695, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(695, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(394, 30);
			this.HeaderText.Text = "Remove Account From Lien Status";
			// 
			// cmbWS
			// 
			this.cmbWS.AutoSize = false;
			this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWS.FormattingEnabled = true;
			this.cmbWS.Items.AddRange(new object[] {
				"Water",
				"Sewer"
			});
			this.cmbWS.Location = new System.Drawing.Point(177, 30);
			this.cmbWS.Name = "cmbWS";
			this.cmbWS.Size = new System.Drawing.Size(224, 40);
			this.cmbWS.TabIndex = 1;
			this.cmbWS.Text = "Water";
			this.cmbWS.SelectedIndexChanged += new System.EventHandler(this.optWS_CheckedChanged);
			// 
			// lblWS
			// 
			this.lblWS.AutoSize = true;
			this.lblWS.Location = new System.Drawing.Point(20, 44);
			this.lblWS.Name = "lblWS";
			this.lblWS.Size = new System.Drawing.Size(107, 15);
			this.lblWS.TabIndex = 0;
			this.lblWS.Text = "WATER / SEWER";
			// 
			// fraAccount
			// 
			this.fraAccount.Controls.Add(this.txtAccount);
			this.fraAccount.Controls.Add(this.cmbWS);
			this.fraAccount.Controls.Add(this.cmbYear);
			this.fraAccount.Controls.Add(this.lblWS);
			this.fraAccount.Controls.Add(this.lblAccountTitle);
			this.fraAccount.Controls.Add(this.lblYearTitle);
			this.fraAccount.Location = new System.Drawing.Point(30, 30);
			this.fraAccount.Name = "fraAccount";
			this.fraAccount.Size = new System.Drawing.Size(421, 209);
			this.fraAccount.TabIndex = 0;
			this.fraAccount.Text = "Account / Year";
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.LinkItem = null;
			this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAccount.LinkTopic = null;
			this.txtAccount.Location = new System.Drawing.Point(177, 90);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(224, 40);
			this.txtAccount.TabIndex = 3;
			this.txtAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAccount_KeyPress);
			this.txtAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccount_Validating);
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(177, 150);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(224, 40);
			this.cmbYear.TabIndex = 5;
			this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
			this.cmbYear.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbYear_KeyDown);
			// 
			// lblAccountTitle
			// 
			this.lblAccountTitle.Location = new System.Drawing.Point(20, 104);
			this.lblAccountTitle.Name = "lblAccountTitle";
			this.lblAccountTitle.Size = new System.Drawing.Size(74, 14);
			this.lblAccountTitle.TabIndex = 2;
			this.lblAccountTitle.Text = "ACCOUNT";
			// 
			// lblyearTtitle
			// 
			this.lblYearTitle.Location = new System.Drawing.Point(20, 164);
			this.lblYearTitle.Name = "lblyearTtitle";
			this.lblYearTitle.Size = new System.Drawing.Size(74, 18);
			this.lblYearTitle.TabIndex = 4;
			this.lblYearTitle.Text = "RATE KEY";
			// 
			// fraPayments
			// 
			this.fraPayments.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraPayments.Controls.Add(this.vsPayments);
			this.fraPayments.Location = new System.Drawing.Point(30, 255);
			this.fraPayments.Name = "fraPayments";
			this.fraPayments.Size = new System.Drawing.Size(642, 241);
			this.fraPayments.TabIndex = 1;
			this.fraPayments.Text = "Lien Payments";
			this.fraPayments.Visible = false;
			// 
			// vsPayments
			// 
			this.vsPayments.AllowSelection = false;
			this.vsPayments.AllowUserToResizeColumns = false;
			this.vsPayments.AllowUserToResizeRows = false;
			this.vsPayments.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsPayments.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsPayments.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsPayments.BackColorBkg = System.Drawing.Color.Empty;
			this.vsPayments.BackColorFixed = System.Drawing.Color.Empty;
			this.vsPayments.BackColorSel = System.Drawing.Color.Empty;
			this.vsPayments.Cols = 6;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsPayments.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsPayments.ColumnHeadersHeight = 30;
			this.vsPayments.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsPayments.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsPayments.DragIcon = null;
			this.vsPayments.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsPayments.FixedCols = 0;
			this.vsPayments.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsPayments.FrozenCols = 0;
			this.vsPayments.GridColor = System.Drawing.Color.Empty;
			this.vsPayments.GridColorFixed = System.Drawing.Color.Empty;
			this.vsPayments.Location = new System.Drawing.Point(20, 30);
			this.vsPayments.Name = "vsPayments";
			this.vsPayments.ReadOnly = true;
			this.vsPayments.RowHeadersVisible = false;
			this.vsPayments.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsPayments.RowHeightMin = 0;
			this.vsPayments.Rows = 1;
			this.vsPayments.ScrollTipText = null;
			this.vsPayments.ShowColumnVisibilityMenu = false;
			this.vsPayments.Size = new System.Drawing.Size(602, 193);
			this.vsPayments.StandardTab = true;
			this.vsPayments.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsPayments.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsPayments.TabIndex = 0;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileRemove,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileRemove
			// 
			this.mnuFileRemove.Index = 0;
			this.mnuFileRemove.Name = "mnuFileRemove";
			this.mnuFileRemove.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileRemove.Text = "Process";
			this.mnuFileRemove.Click += new System.EventHandler(this.mnuFileRemove_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileRemove
			// 
			this.cmdFileRemove.AppearanceKey = "acceptButton";
			this.cmdFileRemove.Location = new System.Drawing.Point(296, 30);
			this.cmdFileRemove.Name = "cmdFileRemove";
			this.cmdFileRemove.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileRemove.Size = new System.Drawing.Size(100, 48);
			this.cmdFileRemove.TabIndex = 0;
			this.cmdFileRemove.Text = "Process";
			this.cmdFileRemove.Click += new System.EventHandler(this.mnuFileRemove_Click);
			// 
			// frmRemoveLien
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(695, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmRemoveLien";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Remove Account From Lien Status";
			this.Load += new System.EventHandler(this.frmRemoveLien_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRemoveLien_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRemoveLien_KeyPress);
			this.Resize += new System.EventHandler(this.frmRemoveLien_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccount)).EndInit();
			this.fraAccount.ResumeLayout(false);
			this.fraAccount.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPayments)).EndInit();
			this.fraPayments.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileRemove)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileRemove;
	}
}
