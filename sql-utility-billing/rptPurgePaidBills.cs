﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptPurgePaidBills.
	/// </summary>
	public partial class rptPurgePaidBills : BaseSectionReport
	{
		public rptPurgePaidBills()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Purged Paid Bills List";
		}

		public static rptPurgePaidBills InstancePtr
		{
			get
			{
				return (rptPurgePaidBills)Sys.GetInstance(typeof(rptPurgePaidBills));
			}
		}

		protected rptPurgePaidBills _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsLienBills.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptPurgePaidBills	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/20/2006              *
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int lngTotal;
		clsDRWrapper rsLienBills = new clsDRWrapper();
		bool blnLienRecordPrinted;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				eArgs.EOF = false;
				blnFirstRecord = false;
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "S")
				{
					blnLienRecordPrinted = false;
					// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
					rsLienBills.OpenRecordset("SELECT * FROM Bill WHERE SLienRecordNumber = " + rsInfo.Get_Fields("RecordNumber"));
				}
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				else if (rsInfo.Get_Fields("Type") == "W")
				{
					blnLienRecordPrinted = false;
					// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
					rsLienBills.OpenRecordset("SELECT * FROM Bill WHERE WLienRecordNumber = " + rsInfo.Get_Fields("RecordNumber"));
				}
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "B" || (FCConvert.ToString(rsInfo.Get_Fields("Type")) != "B" && blnLienRecordPrinted == true))
				{
					rsInfo.MoveNext();
					eArgs.EOF = rsInfo.EndOfFile();
					if (!eArgs.EOF)
					{
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "S")
						{
							// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
							rsLienBills.OpenRecordset("SELECT * FROM Bill WHERE SLienRecordNumber = " + rsInfo.Get_Fields("RecordNumber"));
						}
						// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
						else if (rsInfo.Get_Fields("Type") == "W")
						{
							// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
							rsLienBills.OpenRecordset("SELECT * FROM Bill WHERE WLienRecordNumber = " + rsInfo.Get_Fields("RecordNumber"));
						}
					}
				}
				else
				{
					rsLienBills.MoveNext();
					eArgs.EOF = false;
				}
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = rsInfo.Get_Fields_Int32("AccountKey");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			clsDRWrapper rsDelete = new clsDRWrapper();
			clsDRWrapper rsPaymentDelete = new clsDRWrapper();
			string strFilename;
			rsInfo.MoveFirst();
			if (rsInfo.EndOfFile() != true)
			{
				do
				{
					//Application.DoEvents();
					// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
					if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "B")
					{
						// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
						rsPaymentDelete.Execute("DELETE FROM PaymentRec WHERE ISNULL(Lien,0) = 0 and BillKey = " + rsInfo.Get_Fields("RecordNumber"), modExtraModules.strUTDatabase);
						// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
						rsDelete.Execute("DELETE FROM Bill WHERE ID = " + rsInfo.Get_Fields("RecordNumber"), modExtraModules.strUTDatabase);
					}
					else
					{
						// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
						rsDelete.OpenRecordset("SELECT * FROM Bill WHERE WlienRecordNumber = " + rsInfo.Get_Fields("RecordNumber") + " OR SlienRecordNumber = " + rsInfo.Get_Fields("RecordNumber"), modExtraModules.strUTDatabase);
						if (rsDelete.EndOfFile() != true)
						{
							do
							{
								//FC:FINAL:DDU:#i988 - don't delete when ID = 0, because it continues to infinity and gives error
								if (FCConvert.ToInt32(rsDelete.Get_Fields_Int32("ID")) == 0)
								{
									break;
								}
								rsPaymentDelete.Execute("DELETE FROM PaymentRec WHERE ISNULL(Lien,0) = 0 and BillKey = " + rsDelete.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
								rsDelete.Delete();
								rsDelete.MoveNext();
							}
							while (rsDelete.EndOfFile() != true);
						}
						// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
						rsPaymentDelete.Execute("DELETE FROM PaymentRec WHERE ISNULL(Lien,0) = 1 and BillKey = " + rsInfo.Get_Fields("RecordNumber"), modExtraModules.strUTDatabase);
						// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
						rsDelete.Execute("DELETE FROM Lien WHERE ID = " + rsInfo.Get_Fields("RecordNumber"), modExtraModules.strUTDatabase);
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			//FC:FINAL:MSH - incorrect date format (same with internal issue #914)
			strFilename = "UTPP" + Strings.Format(DateTime.Today, "MMddyy") + Strings.Format(DateAndTime.TimeOfDay, "hhmmss") + ".rdf";
			this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, strFilename));
			IncrementReports(ref strFilename);
			frmWait.InstancePtr.Unload();
			//Application.DoEvents();
			MessageBox.Show("Bills have been deleted", "Bills Deleted", MessageBoxButtons.OK, MessageBoxIcon.Information);
			rsDelete.Dispose();
			rsPaymentDelete.Dispose();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strBillInfoQry;
			string strWaterLienInfoQry;
			string strSewerLienInfoQry;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			lngTotal = 0;
			Label9.Text = modGlobalConstants.Statics.MuniName;
			Label11.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label8.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			// k    rsInfo.CreateStoredProcedure "BillInfo", "SELECT Bill as RecordNumber, CreationDate as BillDate, 'B' as Type, AccountKey FROM Bill WHERE BillStatus = 'B' AND (((Service = 'B' or Service = 'S') and SPrinOwed = SPrinPaid and STaxOwed = STaxPaid and SIntOwed - SIntAdded = SIntPaid and SCostOwed - SCostAdded = SCostPaid) or ((Service = 'B' or Service = 'W') and WPrinOwed = WPrinPaid and WTaxOwed = WTaxPaid and WIntOwed - WIntAdded = WIntPaid and WCostOwed - WCostAdded = WCostPaid)) AND CreationDate <= #" & CDate(frmPurgePaidBills.txtDate.Text) & "#"
			strBillInfoQry = "SELECT ID as RecordNumber, CreationDate as BillDate, 'B' as Type, AccountKey FROM Bill WHERE BillStatus = 'B' AND (((Service = 'B' or Service = 'S') and SPrinOwed = SPrinPaid and STaxOwed = STaxPaid and SIntOwed - SIntAdded = SIntPaid and SCostOwed - SCostAdded = SCostPaid) or ((Service = 'B' or Service = 'W') and WPrinOwed = WPrinPaid and WTaxOwed = WTaxPaid and WIntOwed - WIntAdded = WIntPaid and WCostOwed - WCostAdded = WCostPaid)) AND CreationDate <= '1/1/1900' AND CreationDate <= '" + FCConvert.ToString(DateAndTime.DateValue(frmPurgePaidBills.InstancePtr.txtDate.Text)) + "'";
			// k    rsInfo.CreateStoredProcedure "WaterLienInfo", "SELECT LienKey as RecordNumber, DateCreated as BillDate, 'W' as Type, AccountKey FROM Lien INNER JOIN Bill ON Bill.WLienRecordNumber = Lien.LienKey WHERE BillStatus = 'B' AND Bill = WCombinationLienKey AND WLienRecordNumber <> 0 AND DateCreated <= #" & CDate(frmPurgePaidBills.txtDate.Text) & "# AND (Lien.Principal = Lien.PrinPaid and Lien.Tax = Lien.TaxPaid and Lien.Interest = Lien.PLIPaid and (Lien.IntAdded * -1) = Lien.IntPaid and Lien.Costs = Lien.CostPaid)"
			// kk11062014 trout-1119  Process skips liens with maturity fee added
			strWaterLienInfoQry = "SELECT Lien.ID as RecordNumber, DateCreated as BillDate, 'W' as Type, AccountKey FROM Lien INNER JOIN Bill ON Bill.WLienRecordNumber = Lien.ID WHERE BillStatus = 'B' AND Bill.ID = WCombinationLienKey AND WLienRecordNumber <> 0 AND DateCreated <= '" + FCConvert.ToString(DateAndTime.DateValue(frmPurgePaidBills.InstancePtr.txtDate.Text)) + "' AND (Lien.Principal = Lien.PrinPaid and Lien.Tax = Lien.TaxPaid and Lien.Interest = Lien.PLIPaid and (Lien.IntAdded * -1) = Lien.IntPaid and (Lien.MaturityFee * -1) + Lien.Costs = Lien.CostPaid)";
			// k    rsInfo.CreateStoredProcedure "SewerLienInfo", "SELECT LienKey as RecordNumber, DateCreated as BillDate, 'S' as Type, AccountKey FROM Lien INNER JOIN Bill ON Bill.SLienRecordNumber = Lien.LienKey WHERE BillStatus = 'B' AND Bill = SCombinationLienKey AND SLienRecordNumber <> 0 AND DateCreated <= #" & CDate(frmPurgePaidBills.txtDate.Text) & "# AND (Lien.Principal = Lien.PrinPaid and Lien.Tax = Lien.TaxPaid and Lien.Interest = Lien.PLIPaid and (Lien.IntAdded * -1) = Lien.IntPaid and Lien.Costs = Lien.CostPaid)"
			// kk11062014 trout-1119  Process skips liens with maturity fee added
			strSewerLienInfoQry = "SELECT Lien.ID as RecordNumber, DateCreated as BillDate, 'S' as Type, AccountKey FROM Lien INNER JOIN Bill ON Bill.SLienRecordNumber = Lien.ID WHERE BillStatus = 'B' AND Bill.ID = SCombinationLienKey AND SLienRecordNumber <> 0 AND DateCreated <= '" + FCConvert.ToString(DateAndTime.DateValue(frmPurgePaidBills.InstancePtr.txtDate.Text)) + "' AND (Lien.Principal = Lien.PrinPaid and Lien.Tax = Lien.TaxPaid and Lien.Interest = Lien.PLIPaid and (Lien.IntAdded * -1) = Lien.IntPaid and (Lien.MaturityFee * -1) + Lien.Costs = Lien.CostPaid)";
			rsInfo.OpenRecordset("SELECT * FROM (SELECT * FROM (" + strBillInfoQry + ") AS q1 UNION ALL SELECT * FROM (" + strWaterLienInfoQry + ") AS q2 UNION ALL SELECT * FROM (" + strSewerLienInfoQry + ") AS q3) AS qTmp ORDER BY AccountKey, BillDate");
			//FC:FINAL:CHN - issue #1213: Error during process.
			TWUT0000.frmPurgePaidBills.InstancePtr.Close();
			// Close after use data from form (txtDate.Text).
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				;
				this.PrintWidth = 15100 / 1440f;
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				return;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
				if (FCConvert.ToString(rsInfo.Get_Fields("Type")) == "B")
				{
					// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
					srptBillInfo.InstancePtr.lngBillKey = FCConvert.ToInt32(rsInfo.Get_Fields("RecordNumber"));
					srptBillInfo.InstancePtr.blnLienBill = false;
					subBillInfo.Report = srptBillInfo.InstancePtr;
				}
				else
				{
					if (rsLienBills.EndOfFile() != true)
					{
						srptBillInfo.InstancePtr.lngBillKey = FCConvert.ToInt32(rsLienBills.Get_Fields_Int32("ID"));
						srptBillInfo.InstancePtr.blnLienBill = true;
						subBillInfo.Report = srptBillInfo.InstancePtr;
					}
					else
					{
						// TODO Get_Fields: Field [RecordNumber] not found!! (maybe it is an alias?)
						srptLienInfo.InstancePtr.lngBillKey = FCConvert.ToInt32(rsInfo.Get_Fields("RecordNumber"));
						subBillInfo.Report = srptLienInfo.InstancePtr;
						blnLienRecordPrinted = true;
					}
				}
			}
			catch (Exception ex)
			{
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			rsAccountInfo.OpenRecordset("SELECT AccountNumber, pBill.FullNameLF AS Name, StreetNumber, StreetName, Apt FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID WHERE Master.ID = " + rsInfo.Get_Fields_Int32("AccountKey"));
			if (rsAccountInfo.EndOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccountNumber.Text = Strings.Format(rsAccountInfo.Get_Fields("AccountNumber"), "000000");
				fldName.Text = rsAccountInfo.Get_Fields_String("Name");
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields("StreetNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("StreetNumber")) + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName")));
				}
				else
				{
					fldLocation.Text = Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName")));
				}
			}
			else
			{
				fldAccountNumber.Text = Strings.Format("0", "000000");
				fldName.Text = "UNKNOWN";
				fldLocation.Text = "UNKNOWN";
			}
			rsAccountInfo.Dispose();
		}

		private void IncrementReports(ref string strFile)
		{
			clsDRWrapper rs = new clsDRWrapper();
			rs.OpenRecordset("SELECT * FROM PurgeReports ORDER BY ReportOrder");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					if (rs.Get_Fields_Int32("ReportOrder") < 9)
					{
						rs.Edit();
						rs.Set_Fields("ReportOrder", rs.Get_Fields_Int32("ReportOrder") + 1);
						rs.Update();
					}
					else
					{
						System.IO.File.Delete("/rpt/" + rs.Get_Fields_String("FileName"));
						rs.Delete();
					}
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			rs.AddNew();
			rs.Set_Fields("ReportOrder", 1);
			rs.Set_Fields("FileName", strFile);
			rs.Set_Fields("PurgeDate", DateAndTime.DateValue(frmPurgePaidBills.InstancePtr.txtDate.Text));
			rs.Update();
			rs.Dispose();
		}

		private void rptPurgePaidBills_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptPurgePaidBills properties;
			//rptPurgePaidBills.Caption	= "Purged Paid Biils List";
			//rptPurgePaidBills.Icon	= "rptPurgePaidBills.dsx":0000";
			//rptPurgePaidBills.Left	= 0;
			//rptPurgePaidBills.Top	= 0;
			//rptPurgePaidBills.Width	= 11880;
			//rptPurgePaidBills.Height	= 8595;
			//rptPurgePaidBills.StartUpPosition	= 3;
			//rptPurgePaidBills.SectionData	= "rptPurgePaidBills.dsx":058A;
			//End Unmaped Properties
		}

		private void rptPurgePaidBills_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
