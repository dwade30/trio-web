﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptLienMaturityFees.
	/// </summary>
	public partial class rptLienMaturityFees : BaseSectionReport
	{
		public rptLienMaturityFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Maturity Fees";
		}

		public static rptLienMaturityFees InstancePtr
		{
			get
			{
				return (rptLienMaturityFees)Sys.GetInstance(typeof(rptLienMaturityFees));
			}
		}

		protected rptLienMaturityFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLienMaturityFees	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/11/2004              *
		// ********************************************************
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		double dblDemandFee;
		bool boolDone;
		double dblTotalDemand;
		double dblTotalCMF;
		bool boolWater;

		public void Init(ref int lngPassTotalAccounts, ref double dblPassDemand, ref bool boolPassWater)
		{
			lngTotalAccounts = lngPassTotalAccounts;
			dblDemandFee = dblPassDemand;
			boolWater = boolPassWater;
			// Me.Show
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!modUTLien.Statics.arrDemand[lngCount].Used && boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				if (boolWater)
				{
					modGlobalFunctions.IncrementSavedReports("LastWUTLienMaturityList");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTLienMaturityList1.RDF"));
				}
				else
				{
					modGlobalFunctions.IncrementSavedReports("LastSUTLienMaturityList");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTLienMaturityList1.RDF"));
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngCount = 0;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			NEXTACCOUNT:
			;
			if (!boolDone)
			{
				if (!modUTLien.Statics.arrDemand[lngCount].Processed)
				{
					lngCount += 1;
					if (lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1))
					{
						boolDone = true;
					}
					goto NEXTACCOUNT;
				}
				// With frmApplyDemandFees.vsDemand
				// fldAcct.Text = .TextMatrix(lngCount, 1)
				// fldName.Text = .TextMatrix(lngCount, 2)
				// fldDemand.Text = Format(dblDemandFee, "#,##0.00")
				// fldCMF.Text = Format(CDbl(.TextMatrix(lngCount, 3)) - dblDemandFee, "#,##0.00")
				// fldTotal.Text = .TextMatrix(lngCount, 3)
				// 
				// dblTotalDemand = dblTotalDemand + dblDemandFee
				// dblTotalCMF = dblTotalCMF + CDbl(.TextMatrix(lngCount, 3)) - dblDemandFee
				// move to the next record in the grid that is checked
				// Do Until .TextMatrix(lngCount, 0) = -1 Or lngCount >= .rows
				// lngCount = lngCount + 1
				// Loop
				// 
				// If lngCount >= .rows Then
				// boolDone = True
				// End If
				fldAcct.Text = modUTLien.Statics.arrDemand[lngCount].Account.ToString();
				fldName.Text = modUTLien.Statics.arrDemand[lngCount].Name;
				fldDemand.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].Fee, "#,##0.00");
				if (modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee != 0)
				{
					// MAL@20080527: Corrected to remove the DemandFee from the CMF total
					// Tracker Reference: 13749
					fldCMF.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee, "#,##0.00");
					// fldCMF.Text = Format(arrDemand(lngCount).CertifiedMailFee - arrDemand(lngCount).Fee, "#,##0.00")
					fldTotal.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee + modUTLien.Statics.arrDemand[lngCount].Fee, "#,##0.00");
					// fldTotal.Text = Format(arrDemand(lngCount).CertifiedMailFee, "#,##0.00")
					dblTotalDemand += modUTLien.Statics.arrDemand[lngCount].Fee;
					dblTotalCMF += modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee;
					// dblTotalCMF = dblTotalCMF + arrDemand(lngCount).CertifiedMailFee - arrDemand(lngCount).Fee
				}
				else
				{
					fldCMF.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee, "#,##0.00");
					fldTotal.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].Fee, "#,##0.00");
					dblTotalDemand += modUTLien.Statics.arrDemand[lngCount].Fee;
					// dblTotalCMF = dblTotalCMF + arrDemand(lngCount).CertifiedMailFee
				}
				// move to the next record in the grid that is checked
				do
				{
					lngCount += 1;
				}
				while (!(modUTLien.Statics.arrDemand[lngCount].Processed || lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1)));
				if (lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1))
				{
					boolDone = true;
				}
				// End With
			}
			else
			{
				// clear the fields
				fldAcct.Text = "";
				fldName.Text = "";
				fldDemand.Text = "";
				fldCMF.Text = "";
				fldTotal.Text = "";
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmFreeReport.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			lblTotalCMF.Text = Strings.Format(dblTotalCMF, "#,##0.00");
			lblTotalDemand.Text = Strings.Format(dblTotalDemand, "#,##0.00");
			lblTotalTotal.Text = Strings.Format(dblTotalCMF + dblTotalDemand, "#,##0.00");
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void rptLienMaturityFees_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLienMaturityFees properties;
			//rptLienMaturityFees.Caption	= "Lien Maturity Fees";
			//rptLienMaturityFees.Icon	= "rptLienMaturityFees.dsx":0000";
			//rptLienMaturityFees.Left	= 0;
			//rptLienMaturityFees.Top	= 0;
			//rptLienMaturityFees.Width	= 11880;
			//rptLienMaturityFees.Height	= 8595;
			//rptLienMaturityFees.StartUpPosition	= 3;
			//rptLienMaturityFees.SectionData	= "rptLienMaturityFees.dsx":058A;
			//End Unmaped Properties
		}
	}
}
