﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmTransferTaxToLien.
	/// </summary>
	public partial class frmTransferTaxToLien : BaseForm
	{
		public frmTransferTaxToLien()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTransferTaxToLien InstancePtr
		{
			get
			{
				return (frmTransferTaxToLien)Sys.GetInstance(typeof(frmTransferTaxToLien));
			}
		}

		protected frmTransferTaxToLien _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/08/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsValidate = new clsDRWrapper();
		bool boolLoaded;
		double dblFilingFee;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		// vbPorter upgrade warning: dtMailDate As DateTime	OnWrite(string)
		DateTime dtMailDate;
		int lngRK;
		string strRK;
		bool boolWater;
		string strWS = "";
		int lngLastAccountKey;
		bool boolCombinedBills;
		DateTime dtDateCheck;
		string strBookList;
		int lngColCheck;
		int lngColAcct;
		int lngColName;
		int lngColNotices;
		int lngColCosts;
		int lngColBill;
		int lngColProcess;
		int lngColAccountKey;
		int lngColPrin;
		int lngColTax;
		int lngColInt;
		int lngColDemand;
		string strRateKeys = "";
		bool boolChargeForNewOwner;
		bool boolSendToNewOwner;
		clsDRWrapper rsUT = new clsDRWrapper();

		public void Init(string strPassRK, bool boolPassWater, string strPassBookList)
		{
			boolWater = boolPassWater;
			strBookList = strPassBookList;
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			lngColCheck = 0;
			lngColAcct = 1;
			lngColName = 2;
			lngColNotices = 3;
			lngColCosts = 4;
			lngColBill = 5;
			lngColProcess = 6;
			lngColAccountKey = 7;
			lngColPrin = 8;
			lngColTax = 9;
			lngColInt = 10;
			lngColDemand = 11;
			lblInstruction.Text = "To transfer to liened status:" + "\r\n" + "1. Check the box beside the account" + "\r\n" + "2. Select 'Process' or press F12";
			lblValidateInstruction.Text = "If these values are correct, Press F12 to advance.  If not, then please rerun your Tax Lien Certificates";
			SetAct(0);
			FillValidateGrid();
			if (FillDemandGrid())
			{
				boolLoaded = true;
			}
			else
			{
				boolLoaded = false;
				this.Unload();
			}
			strRK = strPassRK;
			// this is the rate key list to select from
			this.Show(App.MainForm);
		}

		private void frmTransferTaxToLien_Activated(object sender, System.EventArgs e)
		{
			lngRK = frmRateRecChoice.InstancePtr.lngRateRecNumber;
			strRK = frmRateRecChoice.InstancePtr.strRateKeyList;
			if (Strings.Trim(strRK) != "")
			{
				if (!boolLoaded)
				{
					FormatGrid_2(true);
					ShowGridFrame();
					this.Text = "Transfer Tax to Lien";
				}
			}
			else
			{
				MessageBox.Show("The rate record selected is 0.  Please choose another rate record.", "Rate Record Violation", MessageBoxButtons.OK, MessageBoxIcon.Information);
				frmRateRecChoice.InstancePtr.Unload();
				// kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
				frmRateRecChoice.InstancePtr.Init(61);
				this.Unload();
			}
		}

		private void frmTransferTaxToLien_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmTransferTaxToLien_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTransferTaxToLien properties;
			//frmTransferTaxToLien.FillStyle	= 0;
			//frmTransferTaxToLien.ScaleWidth	= 9045;
			//frmTransferTaxToLien.ScaleHeight	= 7035;
			//frmTransferTaxToLien.LinkTopic	= "Form2";
			//frmTransferTaxToLien.LockControls	= true;
			//frmTransferTaxToLien.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmTransferTaxToLien_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			// unload frmRateRecChoice if need be
			//Form frm = null;
			foreach (Form frm in fecherFoundation.FCGlobal.Statics.Forms)
			{
				if (frm.Name == "frmRateRecChoice")
				{
					frmRateRecChoice.InstancePtr.Unload();
					break;
				}
			}
		}

		private void frmTransferTaxToLien_Resize(object sender, System.EventArgs e)
		{
			if (fraGrid.Visible)
			{
				ShowGridFrame();
				// this sets the height of the grid
				if (((vsDemand.Rows * vsDemand.RowHeight(0)) + 70) > fraGrid.Height - vsDemand.Top - 400)
				{
					vsDemand.Height = fraGrid.Height - vsDemand.Top - 400;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
				else
				{
					vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
			}
			else
			{
				ShowValidateFrame();
			}
			FormatGrid_2(true);
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			int lngCT = 0;
			if (vsDemand.Rows > 1)
			{
				lngCT = 1;
				while (!(lngCT >= vsDemand.Rows))
				{
					vsDemand.TextMatrix(lngCT, 0, "");
					lngCT += 1;
				}
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowValidateFrame()
		{
			// this will show/center the frame with the grid on it
			//FC:FINAL:RPU:#i997 - These were fixed in Designer
			//fraValidate.Top = FCConvert.ToInt32((this.Height - fraValidate.Height) / 3.0);
			//fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
			fraValidate.Visible = true;
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//FC:FINAL:RPU:#i997 - These were fixed in Designer
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
		}

		private void FormatGrid_2(bool boolResize)
		{
			FormatGrid(ref boolResize);
		}

		private void FormatGrid(ref bool boolResize)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Cols = 12;
				vsDemand.Rows = 1;
			}
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngColCheck, FCConvert.ToInt32(wid * 0.1));
			// Checkbox
			vsDemand.ColWidth(lngColAcct, FCConvert.ToInt32(wid * 0.15));
			// Acct
			vsDemand.ColWidth(lngColName, FCConvert.ToInt32(wid * 0.45));
			// Name
			vsDemand.ColWidth(lngColNotices, FCConvert.ToInt32(wid * 0.15));
			// Total Notices Sent
			vsDemand.ColWidth(lngColCosts, FCConvert.ToInt32(wid * 0.1));
			// Total Lien Costs
			vsDemand.ColWidth(lngColBill, 0);
			// Hidden Key Field
			vsDemand.ColWidth(lngColProcess, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColWidth(lngColAccountKey, 0);
			// holds the billing year
			vsDemand.ColWidth(lngColPrin, 0);
			// Total for Prin
			vsDemand.ColWidth(lngColTax, 0);
			// Total for Tax
			vsDemand.ColWidth(lngColInt, 0);
			// Total for Interest
			vsDemand.ColWidth(lngColDemand, 0);
			// Total for Demand
			vsDemand.ColFormat(lngColCosts, "#,##0.00");
			vsDemand.ColDataType(lngColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngColAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColNotices, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColCosts, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, lngColAcct, "Account");
			vsDemand.TextMatrix(0, lngColName, "Name");
			vsDemand.TextMatrix(0, lngColNotices, "Notices");
			vsDemand.TextMatrix(0, lngColCosts, "Amount");
			vsValidate.Cols = 3;
			wid = vsValidate.WidthOriginal;
			vsValidate.ColWidth(0, FCConvert.ToInt32(wid * 0.6));
			// Question
			vsValidate.ColWidth(1, FCConvert.ToInt32(wid * 0.32));
			// Answer
			vsValidate.ColWidth(2, 0);
			// Hidden field
			vsValidate.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.TextMatrix(0, 1, "Parameters");
			vsValidate.TextMatrix(0, 2, "Value");
		}

		private bool FillDemandGrid()
		{
			bool FillDemandGrid = false;
			int intCT = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL;
				clsDRWrapper rsCombineBills = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				double dblTotalDue = 0;
				double dblBillDue = 0;
				double dblBillInt;
				double dblBillPrin;
				double dblBillTax;
				double dblBillCost;
				double dblInt;
				double dblXInt = 0;
				double dblPrin;
				double dblCost;
				double dblTax;
				double dblPayments;
				double dblDemand = 0;
				// Dim dblCertMailFee          As Double
				int lngBillCount = 0;
				double dblBillCurInt;
				double dblMinimum = 0;
				bool blnContinue = false;
				double dblBalanceDue = 0;
				double dblBillChgInt;
				// MAL@20080506
				int lngCT;
				FormatGrid_2(false);
				lngLastAccountKey = 0;
				boolCombinedBills = false;
				FillDemandGrid = true;
				intCT = 1;
				// frmWait.Init "Please Wait..." & vbCrLf & "Loading Info"
				intCT = 2;
				// reset the grid
				vsDemand.Rows = 1;
				strSQL = SetupSQL();
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				rsUT.OpenRecordset("SELECT * FROM Master", modExtraModules.strUTDatabase);
				if (rsData.EndOfFile())
				{
					intCT = 4;
					frmWait.InstancePtr.Unload();
					MessageBox.Show("There are no accounts eligible to transfer to lien.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FillDemandGrid = false;
					return FillDemandGrid;
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Records", true, rsData.RecordCount(), true);
				intCT = 5;
				TRYAGAIN:
				;
				dblPrin = 0;
				dblInt = 0;
				dblCost = 0;
				dblTax = 0;
				dblBillPrin = 0;
				dblBillTax = 0;
				dblBillInt = 0;
				dblBillCost = 0;
				dblBillCurInt = 0;
				dblBillChgInt = 0;
				// MAL@20080506
				while (!rsData.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					if (rsData.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						return FillDemandGrid;
					}
					// test to make sure this is a primary accounts
					if (boolCombinedBills)
					{
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey")) == lngLastAccountKey)
						{
							// get the next record
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								frmWait.InstancePtr.Unload();
								return FillDemandGrid;
							}
							goto TRYAGAIN;
						}
						else
						{
							// set this back since it is a new account
							lngLastAccountKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey"));
							boolCombinedBills = false;
						}
					}
					else
					{
						// set this back since it is a new account
						lngLastAccountKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey"));
						boolCombinedBills = false;
					}
					// If rsData.Fields("ActualAccountNumber") = 135 Then
					// MsgBox "Stop"
					// End If
					lngBillCount = 0;
					// test to see if they are eligible anymore
					rsCombineBills.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrProcessLienQuery + ") AS qTmpY WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
					if (rsCombineBills.RecordCount() > 1)
					{
						dblPrin = 0;
						dblInt = 0;
						dblCost = 0;
						dblTax = 0;
						dblBillPrin = 0;
						dblBillTax = 0;
						dblBillInt = 0;
						dblBillCost = 0;
						rsCombineBills.MoveFirst();
						boolCombinedBills = true;
						while (!rsCombineBills.EndOfFile())
						{
							// this will calculate the principal, interest and total due
							dblBillInt = 0;
							dblBillChgInt = 0;
							if (FCConvert.ToInt32(rsCombineBills.Get_Fields(strWS + "LienRecordNumber")) != 0)
							{
								// This should not be here...this is the transfer TO lien
								// Skip this part
								// rsTemp.OpenRecordset "SELECT * FROM Lien WHERE LienKey = " & rsCombineBills.Fields(strWS & "LienRecordNumber"), strUTDatabase
								// If Not rsTemp.EndOfFile Then
								// dblBillDue = CalculateAccountUTLien(rsTemp, CDate(dtDateCheck), dblBillInt, boolWater)
								// End If
								dblBillDue = 0;
							}
							else
							{
								// if a blank date is returned, then this line will crash
								double dblPerDiem = 0;
								bool boolForcePerDiem = false;
								bool blnForceBillIntDate = false;
								DateTime dtDate = DateAndTime.DateValue(FCConvert.ToString(dtDateCheck));
								dblBillDue = modUTCalculations.CalculateAccountUT(rsCombineBills, ref dtDate, ref dblBillInt, boolWater, ref dblBillPrin, ref dblBillChgInt, ref dblBillCost, false, false, DateTime.Now, ref dblBillTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
							}
							rsCombineBills.MoveNext();
							if (dblBillDue > 0)
							{
								// count this bill
								lngBillCount += 1;
								dblInt += dblBillChgInt;
								// + dblBillInt MAL@20080506: Changed to include already charged interest ; Tracker Reference: 13300
								dblPrin += dblBillPrin;
								dblTax += dblBillTax;
								dblCost += dblBillCost;
								dblTotalDue += dblBillDue;
							}
						}
						if (lngBillCount > 1)
						{
							boolCombinedBills = true;
						}
						else
						{
							boolCombinedBills = false;
						}
					}
					else
					{
						dblCost = (rsData.Get_Fields(strWS + "CostOwed") - rsData.Get_Fields(strWS + "CostAdded") - rsData.Get_Fields(strWS + "CostPaid"));
						lngBillCount = 1;
						boolCombinedBills = false;
					}
					if (!boolCombinedBills)
					{
						// this will calculate the principal, interest and total due on a single bill
						if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) != 0)
						{
							// rsTemp.OpenRecordset "SELECT * FROM Lien WHERE LienKey = " & rsData.Fields(strWS & "LienRecordNumber"), strUTDatabase
							// dblInt = 0
							// If Not rsTemp.EndOfFile Then
							// dblTotalDue = CalculateAccountUTLien(rsTemp, CDate(dtDateCheck), dblInt, boolWater)
							// End If
						}
						else
						{
							// if a blank date is returned, then this line will crash
							dblXInt = 0;
							double dblPerDiem = 0;
							bool boolForcePerDiem = false;
							bool blnForceBillIntDate = false;
							DateTime dtDate = DateAndTime.DateValue(FCConvert.ToString(dtDateCheck));
							dblTotalDue = modUTCalculations.CalculateAccountUT(rsData, ref dtDate, ref dblXInt, boolWater, ref dblPrin, ref dblInt, ref dblDemand, false, false, DateTime.Now, ref dblTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
							dblTotalDue -= dblInt;
						}
					}
					// MAL@20070910: Get Total Balance Due - If Greater then a requested minimum then include it
					// MAL@20070927: Moved to a different part in the function to get the correct total due
					if (frmFreeReport.InstancePtr.dblMinimumAmount > 0)
					{
						dblMinimum = frmFreeReport.InstancePtr.dblMinimumAmount;
					}
					else
					{
						dblMinimum = 0;
					}
					dblBalanceDue = dblTotalDue;
					blnContinue = (dblBalanceDue >= dblMinimum && dblBalanceDue > 0);
					// MAL@20080422: Added check for balance due of 0 ; Tracker Reference: 12837
					if (!blnContinue)
					{
						lngLastAccountKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey"));
						rsData.MoveNext();
						goto TRYAGAIN;
					}
					else
					{
						// add the primary to the grid
						vsDemand.AddItem("");
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAcct, FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"))));
						// account number
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAccountKey, FCConvert.ToString(rsData.Get_Fields_Int32("AccountKey")));
						// account number
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColName, FCConvert.ToString(rsData.Get_Fields_String("OName")));
						// name
						// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColNotices, FCConvert.ToString(rsData.Get_Fields("Copies")));
						// number of copies
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColPrin, FCConvert.ToString(FCUtils.Round(dblPrin, 2)));
						// principal amount
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColTax, FCConvert.ToString(dblTax));
						// tax amount
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColInt, FCConvert.ToString(dblInt));
						// interest amount
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColDemand, FCConvert.ToString(dblDemand));
						intCT = 6;
						// amount
						// If rsData.Fields(strWS & "CostAdded") = 0 Then
						// highlight the row and set the value = zero
						// intCT = 7
						// .Cell(FCGrid.CellPropertySettings.flexcpForeColor, .rows - 1, 0, .rows - 1, .Cols - 1) = vbRed
						// .TextMatrix(.rows - 1, lngColCosts) = 0
						// If boolChargeCert Then
						// .TextMatrix(.rows - 1, lngColProcess) = 0
						// Else
						// this will allow no charges to be added
						// .TextMatrix(.rows - 1, lngColProcess) = 0
						// End If
						// Else
						intCT = 8;
						// gonna have to figure out the amount
						if (boolChargeCert)
						{
							if (boolChargeMort)
							{
								if (boolChargeForNewOwner)
								{
									// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
									vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString((rsData.Get_Fields("Copies") * dblCertMailFee) + dblFilingFee + dblCost));
									// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
								}
								else
								{
									bool boolUTMatch = false;
                                    //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
									//if (boolSendToNewOwner && modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
									if (boolSendToNewOwner & modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
									{
										// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
										vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString(((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee + dblCost));
										// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
										vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString((rsData.Get_Fields("Copies") * dblCertMailFee) + dblFilingFee + dblCost));
										// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
									}
								}
							}
							else
							{
								// may have to add the new owner charge here...
								bool boolUTMatch = false;
                                //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
								//if (boolChargeForNewOwner && modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
								if (boolChargeForNewOwner & modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
								{
									vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString((dblCertMailFee * 2) + dblFilingFee + dblCost));
									// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
								}
								else
								{
									vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString(dblCertMailFee + dblFilingFee + dblCost));
									// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
								}
							}
						}
						else
						{
							if (boolChargeMort)
							{
								if (boolChargeForNewOwner)
								{
									// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
									vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString(((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee + dblCost));
									// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
								}
								else
								{
									bool boolUTMatch = false;
                                    //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
									//if (boolSendToNewOwner && modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
									if (boolSendToNewOwner & modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
									{
										// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
										vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString(((rsData.Get_Fields("Copies") - 2) * dblCertMailFee) + dblFilingFee + dblCost));
										// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
										vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString(((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee + dblCost));
										// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
									}
								}
							}
							else
							{
								bool boolUTMatch = false;
                                //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
								//if (boolChargeForNewOwner && modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
								if (boolChargeForNewOwner & modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
								{
									vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString(dblCertMailFee + dblFilingFee + dblCost));
									// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
								}
								else
								{
									vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCosts, FCConvert.ToString(dblFilingFee + dblCost));
									// (rsData.Fields(strWS & "CostOwed") - rsData.Fields(strWS & "CostAdded") - rsData.Fields(strWS & "CostPaid"))
								}
							}
						}
						// End If
						intCT = 20;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngColBill, FCConvert.ToString(rsData.Get_Fields("Bill")));
						// billkey
						rsData.MoveNext();
					}
				}
				intCT = 21;
				// check all of the accounts
				mnuFileSelectAll_Click();
				intCT = 25;
				if (((vsDemand.Rows * vsDemand.RowHeight(0)) + 70) > fraGrid.Height - vsDemand.Top - 400)
				{
					vsDemand.Height = fraGrid.Height - vsDemand.Top - 400;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
					intCT = 30;
				}
				else
				{
					intCT = 31;
					vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
				intCT = 32;
				frmWait.InstancePtr.Unload();
				return FillDemandGrid;
			}
			catch (Exception ex)
			{
				
				FillDemandGrid = false;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Transfer List - " + FCConvert.ToString(intCT), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillDemandGrid;
		}

		private void FillValidateGrid()
		{
			// this will fill the validate grid with the information from the Control_30DayNotice table
			// which is the information that was used to print the 30 Day Notices the last time
			vsValidate.Rows = 1;
			rsValidate.OpenRecordset("SELECT * FROM " + strWS + "Control_LienProcess");
			if (rsValidate.EndOfFile())
			{
				MessageBox.Show("Please return to and run 'Print Tax Lien'.", "No Control Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Unload();
			}
			else
			{
				// vsValidate.AddItem "Billing Year" & vbTab & .Fields("BillingYear")
				dtMailDate = FCConvert.ToDateTime(Strings.Format(rsValidate.Get_Fields_DateTime("FilingDate"), "MM/dd/yyyy"));
				dtDateCheck = dtMailDate;
				vsValidate.AddItem("Interest/Mailing Date" + "\t" + Strings.Format(dtMailDate, "MM/dd/yyyy"));
				// vsValidate.AddItem "Recorded Date" & vbTab & Format(.Fields("DateCreated"), "MM/dd/yyyy")
				// vsValidate.AddItem "Certified Mail Fee" & vbTab & Format(.Fields("CertMailFee"), "#,##0.00")
				dblFilingFee = rsValidate.Get_Fields_Double("FilingFee");
				vsValidate.AddItem("Filing Fee" + "\t" + Strings.Format(dblFilingFee, "#,##0.00"));
				boolChargeCert = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("PayCertMailFee"));
				boolChargeMort = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder"));
				if (boolChargeCert)
				{
					vsValidate.AddItem("Certified Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					dblCertMailFee = rsValidate.Get_Fields_Double("CertMailFee");
					vsValidate.AddItem("Charge for Cert Mail Fee?" + "\t" + "Yes");
				}
				else
				{
					if (boolChargeMort)
					{
						vsValidate.AddItem("Certified Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
						dblCertMailFee = rsValidate.Get_Fields_Double("CertMailFee");
					}
					else
					{
						vsValidate.AddItem("Certified Mail Fee" + "\t" + "0.00");
						dblCertMailFee = 0;
					}
					vsValidate.AddItem("Charge for Cert Mail Fee?" + "\t" + "No");
				}
				if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToMortHolder")))
				{
					if (boolChargeMort)
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "Yes");
					}
					else
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
					}
				}
				else
				{
					vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
				}
				if (FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")) != "")
				{
					if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 2) == "Ye")
					{
						if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 8) == "Yes, cha")
						{
							vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, charge CMF.");
							boolChargeForNewOwner = true;
							boolSendToNewOwner = true;
						}
						else
						{
							vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, with no charge.");
							boolChargeForNewOwner = false;
							boolSendToNewOwner = true;
						}
					}
					else
					{
						vsValidate.AddItem("Send to New Owner" + "\t" + "No");
						boolSendToNewOwner = false;
					}
				}
				else
				{
					vsValidate.AddItem("Send to New Owner" + "\t" + "No");
					boolSendToNewOwner = false;
				}
			}
			// set the height of the grid
			if (vsValidate.Rows * vsValidate.RowHeight(0) > fraValidate.Height - 300)
			{
				vsValidate.Height = fraValidate.Height - 300;
				vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			}
			else
			{
				vsValidate.Height = vsValidate.Rows * vsValidate.RowHeight(0) + 70;
				vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
		}

		private void FindDupLiens()
		{
			int lngCT;
			bool boolBadAcct;
			clsDRWrapper rsTemp = new clsDRWrapper();
			// kgk 05-25-11  trout-725  check to see if any accounts already had a lien created with this rate key (like billing process)
			boolBadAcct = false;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				rsTemp.OpenRecordset("SELECT * FROM Lien INNER JOIN Bill ON Lien.ID = Bill." + strWS + "LienRecordNumber WHERE Lien.RateKey = " + FCConvert.ToString(lngRK) + " AND Bill.AccountKey = " + vsDemand.TextMatrix(lngCT, lngColAccountKey), modExtraModules.strUTDatabase);
				if (!rsTemp.EndOfFile())
				{
					vsDemand.TextMatrix(lngCT, lngColProcess, FCConvert.ToString(-1));
					// This will keep the row from being processed
					vsDemand.TextMatrix(lngCT, lngColCheck, FCConvert.ToString(0));
					vsDemand.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngCT, 0, lngCT, vsDemand.Cols - 1, Color.Red);
					boolBadAcct = true;
					// Flag to show message one time
				}
			}
			// kgk 05-25-11  trout-725 Show the message one time
			if (boolBadAcct)
			{
				MessageBox.Show("One or more accounts already have a lien with this rate key and will not be transferred.", "Duplicate Rate Key", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void SetAct(short intAct)
		{
			// this will change all of the menu options
			switch (intAct)
			{
				case 0:
					{
						ShowValidateFrame();
						fraGrid.Visible = false;
						mnuFileSave.Text = "Process";
						cmdFileSelectAll.Visible = false;
						intAction = 0;
						break;
					}
				case 1:
					{
						FindDupLiens();
						// kgk 05-25-11  trout-725
						ShowGridFrame();
						fraValidate.Visible = false;
						cmdFileSelectAll.Visible = true;
						mnuFileSave.Text = "Process";
						intAction = 1;
						break;
					}
			}
			//end switch
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						// this will say that the user accepts the parameters shown
						SetAct(1);
						break;
					}
				case 1:
					{
						// Transfer Tax to Lien
						TransferTaxToLien(boolWater);
						this.Unload();
						break;
					}
			}
			//end switch
		}

		private void TransferTaxToLien(bool boolWater)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				string strAcctList;
				clsDRWrapper rsEligibility = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				int lngLRN = 0;
				int lngPrimaryBill = 0;
				double dblPrin = 0;
				double dblTotalTransfer = 0;
				// DJW@01092013 TROUT-760 Added check to not run process if there are no accounts
				if (vsDemand.Rows == 1)
				{
					MessageBox.Show("No accounts to process.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Records");
				strAcctList = "";
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					//Application.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, lngColCheck)) == -1)
					{
						// check to see if the check box is checked
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, lngColProcess)) >= 0)
						{
							// then what?
							rsData.FindFirstRecord("Bill", vsDemand.TextMatrix(lngCT, 5));
							if (rsData.NoMatch)
							{
								MessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, lngColAcct) + ".  No lien record was created.", "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, lngColBill), MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
							else
							{
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								strAcctList += rsData.Get_Fields("Bill") + ",";
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								lngPrimaryBill = FCConvert.ToInt32(rsData.Get_Fields("Bill"));
								dblPrin = 0;
								lngLRN = CreateLienRecord_6(lngCT, rsValidate.Get_Fields_DateTime("FilingDate"), boolWater, ref dblPrin);
								dblTotalTransfer += dblPrin;
								// this will accumulate the total transfer of principal for the BD entry made
								rsEligibility.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrProcessLienQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(Conversion.Val(vsDemand.TextMatrix(lngCT, lngColAccountKey))) + " ORDER BY OName, BillingRateKey", modExtraModules.strUTDatabase);
								while (!rsEligibility.EndOfFile())
								{
									//Application.DoEvents();
									// this will set all bills to the correct eligibility
									// k                       rsEligibility.Edit
									// rsEligibility.Fields(strWS & "CombinationLienKey") = lngPrimaryBill 'this primary bill will have Bill = CombinationLienKey
									// rsEligibility.Fields(strWS & "LienRecordNumber") = lngLRN           'all bills will have the
									// rsEligibility.Fields(strWS & "IntPaidDate") = rsValidate.Fields("FilingDate")
									// rsEligibility.Fields(strWS & "WhetherBilledBefore") = "L"
									// rsEligibility.Fields(strWS & "LienProcessStatus") = 4
									// rsEligibility.Fields(strWS & "LienStatusEligibility") = 5
									// rsEligibility.Update
									if (strWS != "W")
									{
										// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
										rsTemp.Execute("UPDATE Bill SET SCombinationLienKey = " + FCConvert.ToString(lngPrimaryBill) + ", SLienRecordNumber = " + FCConvert.ToString(lngLRN) + ", SIntPaidDate = '" + rsValidate.Get_Fields_DateTime("FilingDate") + "', SLienProcessStatus = 4, SLienStatusEligibility = 5 WHERE ID = " + rsEligibility.Get_Fields("Bill"), modExtraModules.strUTDatabase);
									}
									else
									{
										// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
										rsTemp.Execute("UPDATE Bill SET WCombinationLienKey = " + FCConvert.ToString(lngPrimaryBill) + ", WLienRecordNumber = " + FCConvert.ToString(lngLRN) + ", WIntPaidDate = '" + rsValidate.Get_Fields_DateTime("FilingDate") + "', WLienProcessStatus = 4, WLienStatusEligibility = 5 WHERE ID = " + rsEligibility.Get_Fields("Bill"), modExtraModules.strUTDatabase);
									}
									rsEligibility.MoveNext();
								}
								lngDemandCount += 1;
							}
						}
						else
						{
							// not eligible...do nothing
						}
					}
					else
					{
						// not selected...do nothing
					}
				}
				if (Strings.Right(strAcctList, 2) == ", ")
				{
					strAcctList = Strings.Left(strAcctList, strAcctList.Length - 2);
				}
				// kk03312016 trout-927  Change this so the Process doesn't complete if CreateTransfer... fails
				// If gboolBD Then
				// create the automatic entry for BD
				// AddCYAEntry "UT", "Sent Transfer Tax To Lien Entry To BD", CStr(dblTotalTransfer), "Filing Date :" & rsValidate.Fields("FilingDate"), "Rate Key: " & lngRK, "boolWater = " & boolWater
				// CreateTransferTaxToLienBDEntry dblTotalTransfer, Year(rsValidate.Fields("FilingDate")), boolWater
				// End If
				if (modMain.CreateTransferTaxToLienBDEntry_6(dblTotalTransfer, FCConvert.ToInt32(rsValidate.Get_Fields_DateTime("FilingDate").Year), boolWater))
				{
					if (boolWater)
					{
						modMain.SetRateKeyDates_8("W", "L", strRateKeys);
					}
					else
					{
						modMain.SetRateKeyDates_8("S", "L", strRateKeys);
					}
					// create a report with these accounts shown and save it as the transfer tax to lien report
					// MAL@20080505: Change to pass filing date to the report
					// Tracker Reference: 13300
					// arLienTransferReport.Init strAcctList, boolWater
					arLienTransferReport.InstancePtr.Init(strAcctList, boolWater, dtMailDate);
					frmReportViewer.InstancePtr.Init(arLienTransferReport.InstancePtr);
					frmWait.InstancePtr.Unload();
					// show the user how many accounts were affected
					MessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", "Liened Accounts Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					// Failed the journal entry
					// do not set the acquired field and do not show any report
					MessageBox.Show("The Budgetary journal entries were not created and the accounts have not been transferred.", "Error Updating", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error During Transfer", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(mnuFileSelectAll, new System.EventArgs());
		}

		private int CreateLienRecord_6(int lngRow, DateTime dtLienDate, bool boolWater, ref double dblReturnPrin)
		{
			return CreateLienRecord(ref lngRow, ref dtLienDate, ref boolWater, ref dblReturnPrin);
		}

		private int CreateLienRecord(ref int lngRow, ref DateTime dtLienDate, ref bool boolWater, ref double dblReturnPrin)
		{
			int CreateLienRecord = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will use rsData which is set to the correct record
				clsDRWrapper rsLien = new clsDRWrapper();
				double dblCMFee;
				clsDRWrapper rsTransfer = new clsDRWrapper();
				// this will actually create the payment record
				rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = 0", modExtraModules.strUTDatabase);
				rsLien.AddNew();
				rsLien.Set_Fields("IntAdded", 0.0);
				rsLien.Set_Fields("PrinPaid", 0.0);
				rsLien.Set_Fields("TaxPaid", 0.0);
				rsLien.Set_Fields("PLIPaid", 0.0);
				rsLien.Set_Fields("IntPaid", 0.0);
				rsLien.Set_Fields("CostPaid", 0.0);
				rsLien.Set_Fields("MaturityFee", 0.0);
				rsLien.Set_Fields("PrintedLDN", false);
				rsLien.Set_Fields("LastUpdatedDate", DateAndTime.DateValue(FCConvert.ToString(0)));
				rsLien.Set_Fields("Uploaded", false);
				rsLien.Set_Fields("Principal", vsDemand.TextMatrix(lngRow, lngColPrin));
				rsLien.Set_Fields("Tax", vsDemand.TextMatrix(lngRow, lngColTax));
				rsLien.Set_Fields("Interest", FCConvert.ToDouble(vsDemand.TextMatrix(lngRow, lngColInt)));
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblReturnPrin = FCConvert.ToDouble(rsLien.Get_Fields("Principal"));
				if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("PayCertMailFee")))
				{
					if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder")))
					{
						// I think I need to check about the new owner too...
						// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
						dblCMFee = rsValidate.Get_Fields_Double("CertMailFee") * rsData.Get_Fields("Copies");
					}
					else
					{
						dblCMFee = rsValidate.Get_Fields_Double("CertMailFee");
					}
				}
				else
				{
					if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder")))
					{
						// I think I need to check about the new owner too...
						// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
						dblCMFee = rsValidate.Get_Fields_Double("CertMailFee") * (rsData.Get_Fields("Copies") - 1);
					}
					else
					{
						dblCMFee = 0;
					}
				}
				rsLien.Set_Fields("Costs", vsDemand.TextMatrix(lngRow, lngColCosts));
				// + dblCMFee + vsDemand.TextMatrix(lngRow, lngColDemand)
				rsLien.Set_Fields("RateKey", lngRK);
				rsLien.Set_Fields("DateCreated", DateTime.Today);
				rsLien.Set_Fields("IntPaidDate", rsValidate.Get_Fields_DateTime("FilingDate"));
				rsLien.Set_Fields("Status", "A");
				rsLien.Set_Fields("Water", boolWater);
				rsLien.Update();
				CreateLienRecord = FCConvert.ToInt32(rsLien.Get_Fields_Int32("ID"));
				return CreateLienRecord;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Lien Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateLienRecord;
		}
		// kgk 05-26-11 trout-725  Added this to disable rows that are ineligible
		private void vsDemand_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsDemand.Col == lngColCheck && Conversion.Val(vsDemand.TextMatrix(vsDemand.Row, lngColProcess)) == -1)
			{
				vsDemand.TextMatrix(vsDemand.Row, lngColCheck, FCConvert.ToString(0));
			}
		}

		private void vsDemand_DblClick(object sender, System.EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click();
			}
		}

		private void vsDemand_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsDemand[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = vsDemand.GetFlexRowIndex(e.RowIndex);
			lngMC = vsDemand.GetFlexColIndex(e.ColumnIndex);
			if (lngMR > 0 && lngMC >= 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(lngMR, 6)) == -1)
				{
					//ToolTip1.SetToolTip(vsDemand, "A Lien Record has already been created for this account.  This account is not eligible.");
					cell.ToolTipText =  "A Lien Record has already been created for this account.  This account is not eligible.";
				}
				else
				{
					if (vsDemand.TextMatrix(lngMR, 7) != "")
					{
                        //ToolTip1.SetToolTip(vsDemand, "");
                        cell.ToolTipText = "";
						// vsDemand.TextMatrix(lngMR, 7)
					}
					else
					{
                        //ToolTip1.SetToolTip(vsDemand, "");
                        cell.ToolTipText = "";
					}
				}
			}
		}

		private void vsDemand_RowColChange(object sender, System.EventArgs e)
		{
			if (Conversion.Val(vsDemand.TextMatrix(vsDemand.Row, lngColProcess)) != -1)
			{
				// kgk trout-725
				switch (vsDemand.Col)
				{
					case 0:
						{
							vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsDemand.EditCell();
							break;
						}
					default:
						{
							vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
				}
				//end switch
			}
			else
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private string SetupSQL()
		{
			string SetupSQL = "";
			// this will return the SQL statement for this batch of reports
			string strWhereClause = "";
			int intCT;
			clsDRWrapper rsRateKeys = new clsDRWrapper();
			if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Account")
			{
				// range of accounts
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
					}
				}
			}
			else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
			{
				// range of names
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
					}
					else
					{
						// first full second empty
						strWhereClause = " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "'";
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "'";
					}
					else
					{
						// both empty
						strWhereClause = "";
					}
				}
			}
			else
			{
				strWhereClause = "";
			}
			if (Strings.Trim(strBookList) != "" && Strings.Trim(strBookList) != "()")
			{
				strWhereClause += " AND " + strBookList;
			}
			// If Trim(strRK) <> "" Then
			// strWhereClause = strWhereClause & " AND BillingRateKey IN " & strRK
			// End If
			// create the string for the minimum amount
			// MAL@20070910: Commented out because single bills were not necessarily above the minimum but total due was
			// Changed to handle the inclusion of these accounts based on total balance
			// If frmFreeReport.dblMinimumAmount > 0 Then
			// strWhereClause = strWhereClause & " AND (" & strWS & "PrinOwed + " & strWS & "TaxOwed - " & strWS & "PrinPaid - " & strWS & "TaxPaid) > " & frmFreeReport.dblMinimumAmount
			// End If
			// k    rsData.CreateStoredProcedure "ProcessLienQuery", "SELECT * From Bill WHERE BillStatus = 'B' AND (" & strWS & "LienStatusEligibility = 4 OR " & strWS & "LienStatusEligibility = 3) AND " & strWS & "LienProcessStatus = 3 AND " & strWS & "LienRecordNumber = 0 " & strWhereClause & " ORDER BY ActualAccountNumber, BillingRateKey desc"
			modMain.Statics.gstrProcessLienQuery = "SELECT *, ID AS Bill From Bill WHERE BillStatus = 'B' AND (" + strWS + "LienStatusEligibility = 4 OR " + strWS + "LienStatusEligibility = 3) AND " + strWS + "LienProcessStatus = 3 AND " + strWS + "LienRecordNumber = 0 " + strWhereClause;
			// k      & " ORDER BY ActualAccountNumber, BillingRateKey desc"
			// SetupSQL = "SELECT * FROM ProcessLienQuery"
			SetupSQL = modMain.Statics.gstrProcessLienQuery + " ORDER BY ActualAccountNumber, BillingRateKey desc";
			strRateKeys = "";
			rsRateKeys.OpenRecordset("SELECT DISTINCT BillingRateKey FROM (" + modMain.Statics.gstrProcessLienQuery + ") AS qTmpY", "TWUT0000.vb1");
			if (rsRateKeys.EndOfFile() != true && rsRateKeys.BeginningOfFile() != true)
			{
				do
				{
					strRateKeys += rsRateKeys.Get_Fields_Int32("BillingRateKey") + ", ";
					rsRateKeys.MoveNext();
				}
				while (rsRateKeys.EndOfFile() != true);
			}
			if (strRateKeys != "")
			{
				strRateKeys = Strings.Left(strRateKeys, strRateKeys.Length - 2);
			}
			return SetupSQL;
		}
	}
}
