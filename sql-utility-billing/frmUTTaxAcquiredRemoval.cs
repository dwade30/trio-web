﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmTaxAcquiredRemoval.
	/// </summary>
	public partial class frmTaxAcquiredRemoval : BaseForm
	{
		public frmTaxAcquiredRemoval()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxAcquiredRemoval InstancePtr
		{
			get
			{
				return (frmTaxAcquiredRemoval)Sys.GetInstance(typeof(frmTaxAcquiredRemoval));
			}
		}

		protected frmTaxAcquiredRemoval _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/03/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		bool boolChargeForNewOwner;
		DateTime dtMailDate;
		clsDRWrapper rsRE = new clsDRWrapper();
		int lngBillingYear;
		string strYearList = "";
		int lngValidateRowYear;
		int lngGridColTaxAcquired;
		int lngGridColAccount;
		int lngGridColName;
		int lngGridColLocation;
		int lngGridColOSTaxYear;
		int lngGridColOSTaxTotal;
		int lngGridColBillKey;

		public void Init()
		{
			ShowGridFrame();
			SetAct();
		}

		private void chkUpdateRE_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intAnswer As short, int --> As DialogResult
			DialogResult intAnswer;
			if ((modGlobalConstants.Statics.gblnAutoUpdateRETaxAcq && chkUpdateRE.CheckState == Wisej.Web.CheckState.Unchecked) || (!modGlobalConstants.Statics.gblnAutoUpdateRETaxAcq && chkUpdateRE.CheckState == Wisej.Web.CheckState.Checked))
			{
				intAnswer = MessageBox.Show("Are you sure you wish to override the default setting?", "Override Default?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intAnswer == DialogResult.Yes)
				{
					// Do Nothing
				}
				else
				{
					if (chkUpdateRE.CheckState == Wisej.Web.CheckState.Checked)
					{
						chkUpdateRE.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					else
					{
						chkUpdateRE.CheckState = Wisej.Web.CheckState.Checked;
					}
				}
			}
		}

		private void frmTaxAcquiredRemoval_Activated(object sender, System.EventArgs e)
		{
			this.Text = "Tax Acquired Removal";
			lblInstructionHeader.Visible = true;
			// False
			lblInstruction.Visible = true;
			lblInstruction.Text = "To Manually Remove Account From Tax Aquired:" + "\r\n" + "     1. Check the box beside the account" + "\r\n" + "     2. Select 'Save' or press F12";
			if (modGlobalConstants.Statics.gblnAutoUpdateRETaxAcq)
			{
				chkUpdateRE.CheckState = Wisej.Web.CheckState.Checked;
				chkUpdateRE.Enabled = true;
			}
			else
			{
				chkUpdateRE.CheckState = Wisej.Web.CheckState.Unchecked;
				chkUpdateRE.Enabled = false;
			}
			//Application.DoEvents();
		}

		private void frmTaxAcquiredRemoval_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaxAcquiredRemoval properties;
			//frmTaxAcquiredRemoval.FillStyle	= 0;
			//frmTaxAcquiredRemoval.ScaleWidth	= 9225;
			//frmTaxAcquiredRemoval.ScaleHeight	= 7455;
			//frmTaxAcquiredRemoval.LinkTopic	= "Form2";
			//frmTaxAcquiredRemoval.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			lngGridColTaxAcquired = 0;
			lngGridColBillKey = 1;
			lngGridColAccount = 2;
			lngGridColName = 3;
			lngGridColLocation = 4;
			lngGridColOSTaxYear = 5;
			lngGridColOSTaxTotal = 6;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmTaxAcquiredRemoval_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmTaxAcquiredRemoval_Resize(object sender, System.EventArgs e)
		{
			FormatGrid_2(true);
			ShowGridFrame();
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
			if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.HeightOriginal - lblInstruction.HeightOriginal - 500)
			{
				//vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500 / 1440;
				vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			}
			else
			{
				//vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70 / 1440;
				vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
		}

		private void FormatGrid_2(bool boolResize)
		{
			FormatGrid(boolResize);
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 7;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngGridColTaxAcquired, FCConvert.ToInt32(wid * 0.05));
			// checkbox
			vsDemand.ColWidth(lngGridColBillKey, 0);
			// BillKey
			vsDemand.ColWidth(lngGridColAccount, FCConvert.ToInt32(wid * 0.1));
			// Acct
			vsDemand.ColWidth(lngGridColName, FCConvert.ToInt32(wid * 0.5));
			// Account
			vsDemand.ColWidth(lngGridColLocation, FCConvert.ToInt32(wid * 0.3));
			// Location
			vsDemand.ColWidth(lngGridColOSTaxYear, 0);
			vsDemand.ColWidth(lngGridColOSTaxTotal, 0);
			vsDemand.ColFormat(lngGridColOSTaxYear, "#,##0.00");
			vsDemand.ColFormat(lngGridColOSTaxTotal, "#,##0.00");
			vsDemand.ColDataType(lngGridColTaxAcquired, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColOSTaxYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColOSTaxTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, lngGridColTaxAcquired, "TA");
			vsDemand.TextMatrix(0, lngGridColAccount, "Acct");
			vsDemand.TextMatrix(0, lngGridColName, "Name");
			vsDemand.TextMatrix(0, lngGridColLocation, "Location");
			vsDemand.TextMatrix(0, lngGridColOSTaxTotal, "Total Due");
		}

		private void FillDemandGrid()
		{
			lngGridColTaxAcquired = 0;
			lngGridColBillKey = 1;
			lngGridColAccount = 2;
			lngGridColName = 3;
			lngGridColLocation = 4;
			lngGridColOSTaxYear = 5;
			lngGridColOSTaxTotal = 6;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				string strSQL = "";
				int lngIndex;
				double dblXInt;
				double dblBillAmount;
				double dblTotalAmount;
				clsDRWrapper rsLN = new clsDRWrapper();
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				vsDemand.Rows = 1;
				rsRE.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, StreetNumber, StreetName FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID WHERE ISNULL(TaxAcquired,0) = 1 ORDER BY AccountNumber", modExtraModules.strUTDatabase);
				if (rsRE.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("There are no accounts set to Tax Acquired.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Unload();
				}
				lngIndex = -1;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Accounts", true, rsRE.RecordCount(), true);
				while (!rsRE.EndOfFile())
				{
					//Application.DoEvents();
					// add a row/element
					vsDemand.AddItem("");
					// ReDim Preserve arrDemand(.rows - 1) 'this will make sure that there are enough elements to use
					// lngIndex = .rows - 2
					// arrDemand(lngIndex).Used = True
					// arrDemand(lngIndex).Processed = False
					// 
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColAccount, rsRE.Get_Fields("AccountNumber"));
					// account number
					// arrDemand(lngIndex).Account = rsRE.Fields("RSAccount")
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColName, rsRE.Get_Fields("OwnerName"));
					// name
					// arrDemand(lngIndex).Name = rsRE.Fields("RSName")
					// .TextMatrix(.rows - 1, lngGridColBillKey) = rsData.Fields("BillKey")    'billkey
					// arrDemand(lngIndex).BillKey = rsData.Fields("BillKey")
					// If Val(rsData.Fields("StreetNumber")) <> 0 Then
					// .TextMatrix(.rows - 1, lngGridColLocation) = Trim(rsData.Fields("StreetNumber") & " " & rsData.Fields("StreetName"))   'location
					// ElseIf Trim(rsData.Fields("StreetName")) <> "" Then
					// .TextMatrix(.rows - 1, lngGridColLocation) = Trim(rsData.Fields("StreetName"))   'location
					// Else
					// if there is nothing in the bill then show the RE Master info
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("StreetNumber"))) != "")
					{
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(rsRE.Get_Fields("StreetNumber") + " " + rsRE.Get_Fields_String("Streetname")));
						// location
					}
					else
					{
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(rsRE.Get_Fields_String("Streetname")));
						// location
					}
					// End If
					// amounts
					// .TextMatrix(.rows - 1, lngGridColOSTaxYear) = dblBillAmount
					// dblTotalAmount = CalculateAccountTotal(rsData.Fields("Account"), True)
					// .TextMatrix(.rows - 1, lngGridColOSTaxTotal) = dblTotalAmount
					SKIP:
					;
					rsRE.MoveNext();
				}
				if (vsDemand.Rows <= 1)
				{
					frmWait.InstancePtr.Unload();
					this.Unload();
				}
				else
				{
					this.Show(App.MainForm);
				}
				// check all of the accounts
				// mnuFileSelectAll_Click
				if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.Height - lblInstruction.Height - 500 / 1440f)
				{
					//vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500 / 1440;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
				else
				{
					//vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70 / 1440;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetAct()
		{
			// this will change all of the menu options
			FillDemandGrid();
			ShowGridFrame();
			mnuFileSelectAll.Visible = true;
			mnuFilePrint.Text = "Save";
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			// apply demand fees
			if (ApplyDemandFees())
			{
				this.Unload();
			}
		}

		private bool ApplyDemandFees()
		{
			bool ApplyDemandFees = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngCount = 0;
				clsDRWrapper rsMast = new clsDRWrapper();
				string strInits = "";
				clsDRWrapper rsReal = new clsDRWrapper();
				TRYAGAIN:
				;
				if (MessageBox.Show("Are you sure that you would like to remove these accounts from Tax Acquired status manually?", "Manual Removal From Tax Acquired Status", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) == DialogResult.Yes)
				{
					strInits = Interaction.InputBox("Please enter your initials here", "Enter Initials"/*, null, -1, -1*/);
					if (Strings.Trim(strInits) == "" || Strings.Trim(strInits).Length < 2)
					{
						MessageBox.Show("Please enter valid initials.", "Invalid Initials", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						goto TRYAGAIN;
					}
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Tax Acquired");
					for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
					{
						// for each account in the grid
						//Application.DoEvents();
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
						{
							// check to see if the check box if checked
							// kgk                rsRE.FindFirstRecord "AccountNumber", vsDemand.TextMatrix(lngCT, lngGridColAccount)
							rsMast.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + vsDemand.TextMatrix(lngCT, lngGridColAccount), modExtraModules.strUTDatabase);
							if (!rsMast.EndOfFile())
							{
								// k If Not rsRE.NoMatch Then
								rsMast.Edit();
								rsMast.Set_Fields("TaxAcquired", false);
								rsMast.Update();
								lngCount += 1;
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								modGlobalFunctions.AddCYAEntry_62("UT", "Manually Removed Account From Tax Acquired Status", strInits, FCConvert.ToString(rsMast.Get_Fields("AccountNumber")));
								// MAL@20080812: Update RE Account automatically
								// Tracker Reference: 14035
								if (chkUpdateRE.CheckState == Wisej.Web.CheckState.Checked && rsRE.Get_Fields_Boolean("UseREAccount") == true)
								{
									rsReal.OpenRecordset("SELECT * FROM Master WHERE RSAccount = " + rsRE.Get_Fields_Int32("REAccount") + " AND RSCard = 1", modExtraModules.strREDatabase);
									if (rsReal.RecordCount() > 0)
									{
										if (rsReal.Get_Fields_Boolean("TaxAcquired") == true)
										{
											// Don't Update if already removed
											rsReal.Edit();
											rsReal.Set_Fields("TaxAcquired", false);
											rsReal.Update();
											modGlobalFunctions.AddCYAEntry_62("RE", "Manually Removed Account From Tax Acquired Status", strInits, FCConvert.ToString(rsRE.Get_Fields_Int32("RSAccount")));
										}
									}
								}
							}
						}
					}
					frmWait.InstancePtr.Unload();
					if (lngCount != 1)
					{
						MessageBox.Show(FCConvert.ToString(lngCount) + " accounts were affected.", "Tax Acquired Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show(FCConvert.ToString(lngCount) + " account was affected.", "Tax Acquired Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					if (modGlobalConstants.Statics.gboolBD)
					{
						MessageBox.Show("The Budgetary journal entries were not created for these accounts, they will have to be created manually.", "Journal Entries", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					this.Unload();
					//MDIParent.InstancePtr.Focus();
				}
				return ApplyDemandFees;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Affecting Status", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ApplyDemandFees;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(mnuFileSelectAll, new System.EventArgs());
		}

		private void vsDemand_DblClick(object sender, System.EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click();
			}
		}

		private void vsDemand_RowColChange(object sender, System.EventArgs e)
		{
			if (vsDemand.Col == lngGridColTaxAcquired)
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngGridColTaxAcquired, FCConvert.ToString(0));
			}
		}
	}
}
