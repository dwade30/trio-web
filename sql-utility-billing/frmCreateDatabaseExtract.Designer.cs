﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCreateDatabaseExtract.
	/// </summary>
	partial class frmCreateDatabaseExtract : BaseForm
	{
		public fecherFoundation.FCCheckBox chkFinalBilled;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCCheckBox chkOwnerName;
		public fecherFoundation.FCCheckBox chkMapLot;
		public fecherFoundation.FCCheckBox chkCategory;
		public fecherFoundation.FCCheckBox chkMailingAddress;
		public fecherFoundation.FCCheckBox chkLocation;
		public fecherFoundation.FCCheckBox chkName;
		public fecherFoundation.FCCheckBox chkAccountNumber;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateDatabaseExtract));
			this.chkFinalBilled = new fecherFoundation.FCCheckBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.chkOwnerName = new fecherFoundation.FCCheckBox();
			this.chkMapLot = new fecherFoundation.FCCheckBox();
			this.chkCategory = new fecherFoundation.FCCheckBox();
			this.chkMailingAddress = new fecherFoundation.FCCheckBox();
			this.chkLocation = new fecherFoundation.FCCheckBox();
			this.chkName = new fecherFoundation.FCCheckBox();
			this.chkAccountNumber = new fecherFoundation.FCCheckBox();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkFinalBilled)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMailingAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 410);
			this.BottomPanel.Size = new System.Drawing.Size(605, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdProcessSave);
			this.ClientArea.Controls.Add(this.chkFinalBilled);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Size = new System.Drawing.Size(605, 350);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(605, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(282, 30);
			this.HeaderText.Text = "Create Database Extract";
			// 
			// chkFinalBilled
			// 
			this.chkFinalBilled.Location = new System.Drawing.Point(30, 228);
			this.chkFinalBilled.Name = "chkFinalBilled";
			this.chkFinalBilled.Size = new System.Drawing.Size(237, 27);
			this.chkFinalBilled.TabIndex = 8;
			this.chkFinalBilled.Text = "Include Final Billed Accounts";
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.chkOwnerName);
			this.Frame1.Controls.Add(this.chkMapLot);
			this.Frame1.Controls.Add(this.chkCategory);
			this.Frame1.Controls.Add(this.chkMailingAddress);
			this.Frame1.Controls.Add(this.chkLocation);
			this.Frame1.Controls.Add(this.chkName);
			this.Frame1.Controls.Add(this.chkAccountNumber);
			this.Frame1.Location = new System.Drawing.Point(30, 30);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(411, 178);
			this.Frame1.TabIndex = 0;
			this.Frame1.Text = "Information To Extract";
			// 
			// chkOwnerName
			// 
			this.chkOwnerName.Location = new System.Drawing.Point(185, 104);
			this.chkOwnerName.Name = "chkOwnerName";
			this.chkOwnerName.Size = new System.Drawing.Size(124, 27);
			this.chkOwnerName.TabIndex = 7;
			this.chkOwnerName.Text = "Owner Name";
			// 
			// chkMapLot
			// 
			this.chkMapLot.Location = new System.Drawing.Point(185, 67);
			this.chkMapLot.Name = "chkMapLot";
			this.chkMapLot.Size = new System.Drawing.Size(86, 27);
			this.chkMapLot.TabIndex = 6;
			this.chkMapLot.Text = "Map Lot";
			// 
			// chkCategory
			// 
			this.chkCategory.Location = new System.Drawing.Point(185, 30);
			this.chkCategory.Name = "chkCategory";
			this.chkCategory.Size = new System.Drawing.Size(94, 27);
			this.chkCategory.TabIndex = 5;
			this.chkCategory.Text = "Category";
			// 
			// chkMailingAddress
			// 
			this.chkMailingAddress.Location = new System.Drawing.Point(20, 141);
			this.chkMailingAddress.Name = "chkMailingAddress";
			this.chkMailingAddress.Size = new System.Drawing.Size(144, 27);
			this.chkMailingAddress.TabIndex = 4;
			this.chkMailingAddress.Text = "Mailing Address";
			// 
			// chkLocation
			// 
			this.chkLocation.Location = new System.Drawing.Point(20, 104);
			this.chkLocation.Name = "chkLocation";
			this.chkLocation.Size = new System.Drawing.Size(89, 27);
			this.chkLocation.TabIndex = 3;
			this.chkLocation.Text = "Location";
			// 
			// chkName
			// 
			this.chkName.Location = new System.Drawing.Point(20, 67);
			this.chkName.Name = "chkName";
			this.chkName.Size = new System.Drawing.Size(71, 27);
			this.chkName.TabIndex = 2;
			this.chkName.Text = "Name";
			// 
			// chkAccountNumber
			// 
			this.chkAccountNumber.Location = new System.Drawing.Point(20, 30);
			this.chkAccountNumber.Name = "chkAccountNumber";
			this.chkAccountNumber.Size = new System.Drawing.Size(149, 27);
			this.chkAccountNumber.TabIndex = 1;
			this.chkAccountNumber.Text = "Account Number";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(30, 275);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(117, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmCreateDatabaseExtract
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(605, 518);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCreateDatabaseExtract";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Create Database Extract";
			this.Load += new System.EventHandler(this.frmCreateDatabaseExtract_Load);
			this.Activated += new System.EventHandler(this.frmCreateDatabaseExtract_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCreateDatabaseExtract_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkFinalBilled)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			this.Frame1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMailingAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}
