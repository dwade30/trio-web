﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptReminderMailer.
	/// </summary>
	public partial class rptReminderMailer : BaseSectionReport
	{
		public rptReminderMailer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Notice - Mailer";
		}

		public static rptReminderMailer InstancePtr
		{
			get
			{
				return (rptReminderMailer)Sys.GetInstance(typeof(rptReminderMailer));
			}
		}

		protected rptReminderMailer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsRate.Dispose();
				rsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderMailer	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/07/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/31/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsRate = new clsDRWrapper();
		clsDRWrapper rsTemp = new clsDRWrapper();
		string strHeader;
		string strPayment;
		string strNotPastDue;
		string strPastDue;
		string strPreMessage;
		DateTime dtDate;
		bool boolDeleteLastPage;
		int intPer;
		bool boolReturnAddress;
		string strYear = "";
		bool boolShowPendingAmount;
		string strBulkMailer = "";
		bool boolWater;
		bool boolCombineService;
		int lngIndex;
		bool boolNoSummary;
		bool boolSendTenant;
		bool boolSendOwner;
		bool boolSecondCopy;
		bool boolLienedRecords;

		public void Init(ref string strSQL, string strPassHeader, ref string strPassPayment, ref string strPassNotPastDue, ref string strPassPastDue, ref string strPassPreMessage, ref DateTime dtMailingDate, bool boolShowPending, ref bool boolPassWater, ref bool boolPassLienedRecords)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strMod;
				//clsReportPrinterFunctions rpfFont = new clsReportPrinterFunctions();
				string strPrinterFont;
				int const_PrintToolID;
				int lngCT;
				frmWait.InstancePtr.Unload();
				boolShowPendingAmount = boolShowPending;
				boolReturnAddress = FCConvert.CBool(frmReminderNotices.InstancePtr.vsGrid.TextMatrix(4, 1) == "Yes");
				boolLienedRecords = boolPassLienedRecords;
				// strYear = strPassYear
				boolWater = boolPassWater;
				boolCombineService = FCConvert.CBool(frmReminderNotices.InstancePtr.chkCombineService.CheckState == Wisej.Web.CheckState.Checked);
				boolSendTenant = FCConvert.CBool(frmReminderNotices.InstancePtr.chkSendTo[0].CheckState == Wisej.Web.CheckState.Checked);
				boolSendOwner = FCConvert.CBool(frmReminderNotices.InstancePtr.chkSendTo[1].CheckState == Wisej.Web.CheckState.Checked);
				// reset the summary counter
				FCUtils.EraseSafe(modReminderNoticeSummary.Statics.arrReminderSummaryList);
				lngIndex = 0;
				if (frmReminderNotices.InstancePtr.chkBulkMailing.CheckState == Wisej.Web.CheckState.Checked)
				{
					strBulkMailer = modMain.GetBulkMailerString();
				}
				else
				{
					strBulkMailer = "";
				}
				// get a printer font
				//rpfFont.ChoosePrinter(this.Document.Printer);
				//strPrinterFont = rpfFont.GetFont(this.Document.Printer.PrinterName, 10);
				// set the printer font
				// rpfFont.SetReportFontsByTag Me, "TEXT", strPrinterFont
				// this will turn the print button off
				const_PrintToolID = 9950;
				//for(lngCT=0; lngCT<=this.Toolbar.Tools.Count-1; lngCT++) {
				//	if ("Print..."==this.Toolbar.Tools(lngCT).Caption) {
				//		this.Toolbar.Tools(lngCT).ID = const_PrintToolID;
				//		this.Toolbar.Tools(lngCT).Enabled = true;
				//	}
				//} // lngCT
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Report");
				// use the info that is passed into the report
				strHeader = strPassHeader;
				// strRateKeyList = strPassRK
				strPayment = strPassPayment;
				strNotPastDue = strPassNotPastDue;
				strPastDue = strPassPastDue;
				strPreMessage = strPassPreMessage;
				dtDate = dtMailingDate;
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				strMod = "Utility";
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					boolNoSummary = true;
					MessageBox.Show("No matching records were found.", strMod + " Reminder Mailer", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					// this will reset the values used
					// If frmReminderNotices.optPeriod(1).Value Then
					// period 1
					// intPer = 1
					// strPayment = "first "
					// ElseIf frmReminderNotices.optPeriod(2).Value Then
					// period 2
					// intPer = 2
					// strPayment = "second "
					// ElseIf frmReminderNotices.optPeriod(3).Value Then
					// period 3
					// intPer = 3
					// strPayment = "third "
					// ElseIf frmReminderNotices.optPeriod(4).Value Then
					// intPer = 4
					// strPayment = "fourth "
					// Else
					intPer = 4;
					strPayment = "";
					// End If
					// keep going
					rsRate.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
					// If boolCombineService Then
					// rsTemp.OpenRecordset "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.Key WHERE ActualAccountNumber = " & rsData.Fields("ActualAccountNumber"), strUTDatabase
					// Else
					// If boolWater Then
					// rsTemp.OpenRecordset "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.Key WHERE Service <> 'S' AND ActualAccountNumber = " & rsData.Fields("ActualAccountNumber"), strUTDatabase
					// Else
					// rsTemp.OpenRecordset "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.Key WHERE Service <> 'W' AND ActualAccountNumber = " & rsData.Fields("ActualAccountNumber"), strUTDatabase
					// End If
					// End If
					this.PageSettings.PaperHeight = 5.5F;
                    //FC:FINAL:AM:#4131 - don't change the location of the label
					//lblMailingAddress4.Top = ((4 * 1440) - 256) / 1440f;
					// this sits on the bottom of the form to make sure that the detail section is still large enough
					this.PageSettings.PaperWidth = (8.5F);
					// Me.Zoom = -1
					// Me.Show , MDIParent
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Mailers", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
			if (boolDeleteLastPage)
			{
				this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
				//this.Document.Pages.Commit();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
			if (!boolNoSummary)
			{
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                modReminderNoticeSummary.Statics.intRPTReminderSummaryType = 3;
				frmReportViewer.InstancePtr.Init(rptReminderSummary.InstancePtr);
				//rptReminderSummary.InstancePtr.Show(App.MainForm);
			}
		}
		//private void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		//{
		//	bool bReturn;
		//	// vbPorter upgrade warning: intReturn As short --> As int	OnWrite(DialogResult)
		//	int intReturn = 0;
		//	string vbPorterVar = ""/*Tool.Caption*/;
		//	if (vbPorterVar=="Print...")
		//	{
		//		if (this.Document.Pages.Count>1) {
		//			intReturn = FCConvert.ToInt32(DialogResult.No;
		//			this.Document.Printer.FromPage = 1;
		//			this.Document.Printer.ToPage = 1;
		//			while (intReturn== FCConvert.ToInt32(DialogResult.No) {
		//				this.PrintReport(false);
		//				intReturn = FCConvert.ToInt32(MessageBox.Show("Is the mailer correctly aligned?"+"\r\n"+"Choose No to re-align, Yes to continue.", "Align Mailer", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
		//			}
		//			if (intReturn!= FCConvert.ToInt32(DialogResult.Cancel) {
		//				this.Document.Printer.FromPage = 2;
		//				this.Document.Printer.ToPage = this.Document.Pages.Count;
		//				this.PrintReport(false);
		//			}
		//		} else {
		//			this.PrintReport(false);
		//		}
		//	}
		//}
		private void Detail_Format(object sender, EventArgs e)
		{
			TRYAGAIN:
			;
			if (!rsData.EndOfFile())
			{
				if (boolCombineService)
				{
					rsTemp.OpenRecordset("SELECT AccountKey, ActualAccountNumber, SUM(TotalOwed) as TotalPOwed, SUM(TotalOwedT) as TotalTOwed, SUM(TotalPaid) as TotalPPaid, SUM(TotalPaidT) as TotalTPaid FROM (SELECT AccountKey, ActualAccountNumber, SUM(WPrinOwed) as TotalOwed, SUM(WTaxOwed) as TotalOwedT, SUM(WPrinPaid) as TotalPaid, SUM(WTaxPaid) as TotalPaidT FROM Bill WHERE Service <> 'S' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND WLienRecordNumber = 0 GROUP BY AccountKey, ActualAccountNumber UNION SELECT AccountKey, ActualAccountNumber, SUM(SPrinOwed) as TotalOwed, SUM(STaxOwed) as TotalOwedT, SUM(SPrinPaid) as TotalPaid, SUM(STaxPaid) as TotalPaidT FROM Bill WHERE Service <> 'W' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND SLienRecordNumber = 0 GROUP BY AccountKey, ActualAccountNumber " + "UNION SELECT AccountKey, ActualAccountNumber, SUM(Principal) AS TotalOwed, SUM(Tax) AS TotalOwedT, SUM(PrinPaid) as TotalPaid, SUM(TaxPaid) as TotalPaidT FROM Bill INNER JOIN Lien ON Bill.WLienRecordNumber = Lien.ID WHERE Service <> 'S' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND WCombinationLienKey = Bill.ID GROUP BY AccountKey, ActualAccountNumber UNION SELECT AccountKey, ActualAccountNumber, SUM(Principal) AS TotalOwed, SUM(Tax) AS TotalOwedT, SUM(PrinPaid) as TotalPaid, SUM(TaxPaid) as TotalPaidT FROM Bill INNER JOIN Lien ON Bill.SLienRecordNumber = Lien.ID WHERE Service <> 'W' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND SCombinationLienKey = Bill.ID GROUP BY AccountKey, ActualAccountNumber) AS qTmp GROUP BY AccountKey, ActualAccountNumber");
				}
				else
				{
					if (boolWater)
					{
						rsTemp.OpenRecordset("SELECT AccountKey, ActualAccountNumber, SUM(TotalOwed) as TotalPOwed, SUM(TotalOwedT) as TotalTOwed, SUM(TotalPaid) as TotalPPaid, SUM(TotalPaidT) as TotalTPaid FROM (SELECT AccountKey, ActualAccountNumber, SUM(WPrinOwed) as TotalOwed, SUM(WTaxOwed) as TotalOwedT, SUM(WPrinPaid) as TotalPaid, SUM(WTaxPaid) as TotalPaidT FROM Bill WHERE Service <> 'S' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND WLienRecordNumber = 0 GROUP BY AccountKey, ActualAccountNumber UNION SELECT AccountKey, ActualAccountNumber, SUM(Principal) AS TotalOwed, SUM(Tax) AS TotalOwedT, SUM(PrinPaid) as TotalPaid, SUM(TaxPaid) as TotalPaidT FROM Bill INNER JOIN Lien ON Bill.WLienRecordNumber = Lien.ID WHERE Service <> 'S' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND WCombinationLienKey = Bill.ID GROUP BY AccountKey, ActualAccountNumber) AS qTmp GROUP BY AccountKey, ActualAccountNumber");
					}
					else
					{
						rsTemp.OpenRecordset("SELECT AccountKey, ActualAccountNumber, SUM(TotalOwed) as TotalPOwed, SUM(TotalOwedT) as TotalTOwed, SUM(TotalPaid) as TotalPPaid, SUM(TotalPaidT) as TotalTPaid FROM (SELECT AccountKey, ActualAccountNumber, SUM(SPrinOwed) as TotalOwed, SUM(STaxOwed) as TotalOwedT, SUM(SPrinPaid) as TotalPaid, SUM(STaxPaid) as TotalPaidT FROM Bill WHERE Service <> 'W' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND SLienRecordNumber = 0 GROUP BY AccountKey, ActualAccountNumber UNION SELECT AccountKey, ActualAccountNumber, SUM(Principal) AS TotalOwed, SUM(Tax) AS TotalOwedT, SUM(PrinPaid) as TotalPaid, SUM(TaxPaid) as TotalPaidT FROM Bill INNER JOIN Lien ON Bill.SLienRecordNumber = Lien.ID WHERE Service <> 'W' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND SCombinationLienKey = Bill.ID GROUP BY AccountKey, ActualAccountNumber) AS qTmp GROUP BY AccountKey, ActualAccountNumber");
					}
				}
				PrintPostCards();
				if (!boolSecondCopy)
				{
					rsData.MoveNext();
				}
			}
		}

		private void PrintPostCards()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill in the fields for post card
				int intCTRLIndex;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string str5 = "";
				double dblDue = 0;
				double dblPaid = 0;
				double dblPeriodDue = 0;
				int intRatePer;
				double dblPrinToPeriod = 0;
				double dblPrinAfterPer;
				double[] dblAbateAmt = new double[4 + 1];
				bool boolAbatementPayment = false;
				clsDRWrapper rsMaster = new clsDRWrapper();
				double dblTaxDue = 0;
				double dblTaxPaid = 0;
				GETNEXTACCOUNT:
				;
				// .OpenRecordset "SELECT * FROM Master WHERE AccountNumber = " & rsData.Fields("ActualAccountNumber"), strUTDatabase
				rsMaster.OpenRecordset(modUTStatusPayments.UTMasterQuery(rsData.Get_Fields_Int32("AccountKey")), modExtraModules.strUTDatabase);
				if (boolSendTenant && !boolSecondCopy)
				{
					boolSecondCopy = false;
					if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name2"))) != "")
					{
						str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name"))) + " and " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name2")));
					}
					else
					{
						str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name")));
					}
					// this will fill the address form the master screen for the mail address
					str2 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress1")));
					str3 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress2")));
					str4 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress3")));
					str5 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BCity"))) + " " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BState"))) + " " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BZip")));
					// kgk  If Trim(.Fields("BZip4")) <> "" Then
					// str5 = str5 & "-" & Trim(.Fields("BZip4"))
					// End If
					if (boolSendOwner)
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name"))) != Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName"))))
						{
							boolSecondCopy = true;
						}
					}
				}
				else if (boolSendOwner || boolSecondCopy)
				{
					boolSecondCopy = false;
					// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("SecondOwnerName"))) != "")
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
						str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName"))) + " and " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("SecondOwnerName")));
					}
					else
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName")));
					}
					// this will fill the address form the master screen for the mail address
					str2 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress1")));
					str3 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress2")));
					str4 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress3")));
					str5 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OCity"))) + " " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OState"))) + " " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip")));
					// kgk  If Trim(.Fields("OZip4")) <> "" Then
					// str5 = str5 & "-" & Trim(.Fields("OZip4"))
					// End If
				}
				// condense the labels if some are blank
				if (Strings.Trim(str4) == string.Empty)
				{
					str4 = str5;
					str5 = "";
				}
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					str4 = str5;
					str5 = "";
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					str4 = str5;
					str5 = "";
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					str4 = str5;
					str5 = "";
				}
				lblBulk.Text = strBulkMailer;
				// TODO Get_Fields: Field [TotalPOwed] not found!! (maybe it is an alias?)
				dblDue = rsTemp.Get_Fields("TotalPOwed");
				// TODO Get_Fields: Field [TotalPPaid] not found!! (maybe it is an alias?)
				dblPaid = rsTemp.Get_Fields("TotalPPaid");
				// TODO Get_Fields: Field [TotalTOwed] not found!! (maybe it is an alias?)
				dblTaxDue = rsTemp.Get_Fields("TotalTOwed");
				// TODO Get_Fields: Field [TotalTPaid] not found!! (maybe it is an alias?)
				dblTaxPaid = rsTemp.Get_Fields("TotalTPaid");
				if (boolReturnAddress)
				{
					// Return Address
					lblReturnAddress1.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(1, 0);
					lblReturnAddress2.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(2, 0);
					lblReturnAddress3.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(3, 0);
					lblReturnAddress4.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(4, 0);
				}
				// Account Number
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				lblAccount.Text = "Account: " + rsMaster.Get_Fields("AccountNumber");
				// Name
				lblName.Text = str1;
				// Mailing Address
				lblMailingAddress1.Text = str2;
				lblMailingAddress2.Text = str3;
				lblMailingAddress3.Text = str4;
				lblMailingAddress4.Text = str5;
				// Message
				// find out if the account has outstanding taxes
				if ((!boolAbatementPayment && FCUtils.Round((dblDue - dblPeriodDue), 2) <= FCUtils.Round(dblPaid, 2)) || (boolAbatementPayment && dblPrinToPeriod > 0))
				{
					// check the taxes due before the upcoming period and compare it to the taxes already paid
					// this account has no outstanding taxes
					if (boolCombineService)
					{
						// kk trouts-6 03012013  Change Water to Stormwater for Bangor
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							lblHeader.Text = "******  Stormwater and Sewer - Reminder Notice  ******";
						}
						else
						{
							lblHeader.Text = "******  Water and Sewer - Reminder Notice  ******";
						}
						lblPreMessage.Text = "This is to remind you that your water and sewer bill is due on " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
					}
					else
					{
						if (boolWater)
						{
							// kk trouts-6 03012013  Change Water to Stormwater for Bangor
							if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
							{
								lblHeader.Text = "******  Stormwater - Reminder Notice  ******";
							}
							else
							{
								lblHeader.Text = "******  Water - Reminder Notice  ******";
							}
							lblPreMessage.Text = "This is to remind you that your water bill is due on " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
						}
						else
						{
							lblHeader.Text = "******  Sewer - Reminder Notice  ******";
							lblPreMessage.Text = "This is to remind you that your sewer bill is due on " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
						}
					}
					if (boolAbatementPayment)
					{
						lblAmount.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   $" + Strings.Format(dblPrinToPeriod, "#,##0.00");
					}
					else
					{
						if (dblPaid > 0)
						{
							if (dblDue >= dblPaid)
							{
								lblAmount.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   $" + Strings.Format(dblDue - dblPaid, "#,##0.00");
							}
							else
							{
								lblAmount.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   $" + Strings.Format(0, "#,##0.00");
							}
						}
						else
						{
							lblAmount.Text = "Amount Due " + Strings.Format(dtDate, "MM/dd/yyyy") + "   $" + Strings.Format(dblDue, "#,##0.00");
						}
					}
					lblMessage.Text = strNotPastDue;
				}
				else
				{
					// outstanding taxes due
					if (boolCombineService)
					{
						// kk trouts-6 03012013  Change Water to Stormwater for Bangor
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							lblHeader.Text = "******  Stormwater and Sewer - Reminder Notice  ******";
						}
						else
						{
							lblHeader.Text = "******  Water and Sewer - Reminder Notice  ******";
						}
						lblPreMessage.Text = "This is to remind you that you have past due amounts on your water and/or sewer bill as of " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
					}
					else
					{
						if (boolWater)
						{
							// kk trouts-6 03012013  Change Water to Stormwater for Bangor
							if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
							{
								lblHeader.Text = "******  Stormwater - Reminder Notice  ******";
							}
							else
							{
								lblHeader.Text = "******  Water - Reminder Notice  ******";
							}
							lblPreMessage.Text = "This is to remind you that you have past due amounts on your water bill as of " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
						}
						else
						{
							lblHeader.Text = "******  Sewer - Reminder Notice  ******";
							lblPreMessage.Text = "This is to remind you that you have past due amounts on your sewer bill as of " + Strings.Format(dtDate, "MM/dd/yyyy") + "." + "\r\n" + strPreMessage;
						}
					}
					lblMessage.Text = strPastDue + "\r\n";
					lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Past Due", 10, false) + modGlobalFunctions.PadStringWithSpaces("$" + Strings.Format((dblDue - dblPaid) - dblPeriodDue, "#,##0.00"), 15) + "\r\n";
					lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Tax", 10, false) + modGlobalFunctions.PadStringWithSpaces("$" + Strings.Format(dblTaxDue - dblTaxPaid, "#,##0.00"), 15) + "\r\n";
					lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Interest", 10, false) + modGlobalFunctions.PadStringWithSpaces("Call for Total.", 15) + "\r\n";
					lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Due", 10, false) + modGlobalFunctions.PadStringWithSpaces("$" + Strings.Format(dblPeriodDue, "#,##0.00"), 15) + "\r\n";
					lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces(" ", 10, false) + modGlobalFunctions.PadStringWithSpaces("------------", 15) + "\r\n";
					if (boolShowPendingAmount)
					{
						lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Total Due", 10, false) + modGlobalFunctions.PadStringWithSpaces("$" + Strings.Format(dblDue - dblPaid, "#,##0.00"), 15) + "\r\n";
					}
					else
					{
						lblMessage.Text = lblMessage.Text + "                    " + modGlobalFunctions.PadStringWithSpaces("Total Due", 10, false) + modGlobalFunctions.PadStringWithSpaces("Call for Total.", 15) + "\r\n";
					}
					lblAmount.Text = "";
				}
				Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
				// fill the struct
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = FCConvert.ToInt32(rsMaster.Get_Fields("AccountNumber"));
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = str1;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str2;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr2 = str3;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr3 = str4;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr4 = str5;
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Year = 0;
				// strYear
				modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblDue - dblPaid;
				lngIndex += 1;
				rsMaster.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Printing Mailer", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void rptReminderMailer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderMailer properties;
			//rptReminderMailer.Caption	= "Reminder Notice - Mailer";
			//rptReminderMailer.Icon	= "rptReminderMailer.dsx":0000";
			//rptReminderMailer.Left	= 0;
			//rptReminderMailer.Top	= 0;
			//rptReminderMailer.Width	= 11880;
			//rptReminderMailer.Height	= 8595;
			//rptReminderMailer.StartUpPosition	= 3;
			//rptReminderMailer.SectionData	= "rptReminderMailer.dsx":058A;
			//End Unmaped Properties
		}
	}
}
