﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using SharedApplication.Extensions;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmRateRecChoice.
	/// </summary>
	public partial class frmRateRecChoice : BaseForm
	{
		public frmRateRecChoice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.txtBillingDate = new System.Collections.Generic.List<T2KDateBox>();
			this.txtDueDate = new System.Collections.Generic.List<T2KDateBox>();
			this.lblStartDate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblEndDate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.txtRange = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.chkPrint = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.txtBillingDate.AddControlArrayElement(txtBillingDate_0, 0);
			this.txtBillingDate.AddControlArrayElement(txtBillingDate_1, 1);
			this.txtDueDate.AddControlArrayElement(txtDueDate_0, 0);
			this.txtDueDate.AddControlArrayElement(txtDueDate_1, 1);
			this.lblStartDate.AddControlArrayElement(lblStartDate_1, 1);
			this.lblStartDate.AddControlArrayElement(lblStartDate_0, 0);
			this.lblEndDate.AddControlArrayElement(lblEndDate_1, 1);
			this.lblEndDate.AddControlArrayElement(lblEndDate_0, 0);
			this.txtRange.AddControlArrayElement(txtRange_1, 1);
			this.txtRange.AddControlArrayElement(txtRange_0, 0);
			this.chkPrint.AddControlArrayElement(chkPrint_3, 3);
			this.chkPrint.AddControlArrayElement(chkPrint_2, 2);
			this.chkPrint.AddControlArrayElement(chkPrint_1, 1);
			this.chkPrint.AddControlArrayElement(chkPrint_0, 0);
            this.txtFilterEnd.KeyDown += TxtFilterEnd_KeyDown;
            this.txtFilterEndDate.KeyDown += TxtFilterEndDate_KeyDown;
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

        private void TxtFilterEndDate_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                ApplyFilter();
            }
        }

        private void TxtFilterEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                ApplyFilter();
            }
        }

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmRateRecChoice InstancePtr
		{
			get
			{
				return (frmRateRecChoice)Sys.GetInstance(typeof(frmRateRecChoice));
			}
		}

		protected frmRateRecChoice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/06/2006              *
		// ********************************************************
		public int intRateType;
		// this will be set by the form calling this form and will tell which form this should be setup for
		public int lngRateRecNumber;
		public bool boolAddedRK;
		// the audit info screen will set this and this will automatically set the first  key
		int intLienedAccounts;
		clsDRWrapper rsRateRec = new clsDRWrapper();
		bool boolLoaded;
		int intAction;
		int lngBillDateCol;
		// these are the column numbers for each variable so that they can be
		int lngBillingDateCol;
		// moved with minimal effort, since I know they will be (haha)
		int lngDueDate1Col;
		int lngStatusCol;
		int lngDescriptionCol;
		public int lngHiddenCol;
		int lngLienCol;
		bool boolFormatting;
		bool boolUnloadMe;
		public string strRateKeyList = "";
		public string strBookList = string.Empty;
		public string strDateRanges = "";
		// trouts-227 select rate keys by date range
		string strDefaultRateKeyList;
		// if this has a list of rate keys in it then the LoadGrid routine will automatically set those to checked
		bool boolFromDisconnect;
		bool boolIsRightClick;
		// Used to determine if right-click
		// trouts-217 Add filter options
		DateTime dtFilterBillDateStart;
		DateTime dtFilterBillDateEnd;
		DateTime dtFilterStartDateStart;
		DateTime dtFilterStartDateEnd;
		DateTime dtFilterIntDateStart;
		DateTime dtFilterIntDateEnd;
		string strFilterDesc = "";
		string strFilterDescOption = "";
		int lngFilterRateKeyStart;
		int lngFilterRateKeyEnd;
		int currentMouseRow = -1;
		int currentMouseCol = -1;
		string strLabelQuery = "";
		// vbPorter upgrade warning: intPassRateType As short	OnWriteFCConvert.ToInt32(
		public void Init(short intPassRateType, string strPassBookString = "", bool boolPassFromDisconnect = false, string strPassRKList = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				intRateType = intPassRateType;
				strBookList = strPassBookString;
				boolFromDisconnect = boolPassFromDisconnect;
				strDefaultRateKeyList = strPassRKList;
				if (boolLoaded)
				{
					boolFormatting = true;
					// this will make sure that the form knows that I am formatting it instead of the user changing the size of the form
					modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
					// set the size of the form
					//Application.DoEvents();
					FormatGrid();
					// format the grid
					SetAct_2(0);
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
					boolFormatting = false;
				}

                ClearAllFilters();
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + Information.Err(ex).Number + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this function will determine from intRateType which report is calling it and will
			// create the SQL string accordingly then pass it back
			string strWS = "";
			string strWhereLeader;
			if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			strWhereLeader = " AND ";
			switch (intRateType)
			{
			// ******************************** Lien Process *********************************
				case 0:
					{
						// this means that nothing was setup
						BuildSQL = "SELECT * FROM RateKeys WHERE ID = 0";
						// this should produce 0 records
						break;
					}
				case 1:
					{
						// Lien Edit Report
						if (cmbLien.Text == "All Records")
						{
							// all accounts
							BuildSQL = "SELECT * FROM RateKeys";
							strWhereLeader = " WHERE ";
						}
						else if (cmbLien.Text == "Pre Lien Only")
						{
							// pre lien
							BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						}
						else if (cmbLien.Text == "Liened Only")
						{
							// liened accounts
							this.Text = "Select Lien Rate Record";
							BuildSQL = "SELECT * FROM RateKeys WHERE RateType = 'L'";
						}
						else if (cmbLien.Text == "30 Day Notice")
						{
							// eligible for 30 day notices
							BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						}
						else if (cmbLien.Text == "Lien")
						{
							// eligible for liens
							BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						}
						else if (cmbLien.Text == "Lien Maturity")
						{
							// eligible for lien maturity
							this.Text = "Select Lien Rate Record";
							BuildSQL = "SELECT * FROM RateKeys WHERE RateType = 'L'";
						}
						break;
					}
				case 2:
					{
						// 30 Day Process
						modUTStatusList.Statics.strReportType = "30DAYNOTICE" + strWS;
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						break;
					}
				case 3:
					{
						// Tax Lien Process
						modUTStatusList.Statics.strReportType = "LIENPROCESS" + strWS;
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						break;
					}
				case 4:
					{
						// Lien Maturity Notice
						modUTStatusList.Statics.strReportType = "LIENMATURITY" + strWS;
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType = 'L'";
						break;
					}
				case 5:
					{
						// Apply Demand Fees
						this.Text = "Select Rate Record To Apply Fees";
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						break;
					}
				case 6:
					{
						// Transfer Tax To Lien (Lien Record)
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType = 'L'";
						break;
					}
				case 61:
					{
						// Transfer Tax To Lien (Regular Records)
						this.Text = "Select Rate Records To Transfer";
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						break;
					}
				case 7:
					{
						// Apply Lien Maturity Notice Fees
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType = 'L'";
						// ********************************    Labels    *********************************
						break;
					}
				case 10:
					{
						// Demand Labels
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						break;
					}
				case 11:
					{
						// Lien Process Labels
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						break;
					}
				case 12:
					{
						// Lien Maturity Labels
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType = 'L'";
						// ********************************  Mail Forms  *********************************
						break;
					}
				case 20:
					{
						// Demand Mail Forms
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						break;
					}
				case 21:
					{
						// Lien Process Forms
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						break;
					}
				case 22:
					{
						// Lien Maturity Forms
						this.Text = "Select Lien Rate Record";
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType = 'L'";
						break;
					}
				case 25:
					{
						// Analysis Reports
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						// *******************************Reminder Notices********************************
						break;
					}
				case 30:
					{
						// Reminder Notices
						if (cmbReminderLien.Text == "Regular")
						{
							BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						}
						else
						{
							BuildSQL = "SELECT * FROM RateKeys WHERE RateType = 'L'";
						}
						// *******************************  Period Report  *******************************
						break;
					}
				case 50:
					{
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						// ******************************Reverse 30 DN Fees*******************************
						break;
					}
				case 60:
					{
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						GetRateKeyList();
						// *******************************   Load Back    ********************************
						break;
					}
				case 100:
					{
						// this is to select a ratekey for the load back function
						BuildSQL = "SELECT * FROM RateKeys";
						strWhereLeader = " WHERE ";
						// *******************************    Billing     ********************************
						break;
					}
				case 101:
					{
						// Create Bills
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 102:
					{
						// Print Bills
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 103:
					{
						// Outprinting File
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 104:
					{
						// Email Bills
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 106:
					{
						// Print Transfer List
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 107:
					{
						// Billed List
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 108:
					{
						// Print Non-Billed Accounts
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 110:
					{
						// Split Bills
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 111:
					{
						// kgk 10-14-2011  Export Bills for Invoice Cloud
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 200:
					{
						// this is the edit type
						BuildSQL = "SELECT * FROM RateKeys WHERE (RateType = 'R' OR RateType = 'L' OR RateType = 'S')";
						break;
					}
				case 201:
					{
						// Final Billed
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				case 250:
					{
						// Consumption History Report
						BuildSQL = "SELECT * FROM RateKeys WHERE RateType <> 'L'";
						break;
					}
				default:
					{
						BuildSQL = "SELECT * FROM RateKeys WHERE ID = 0";
						// this should produce 0 records
						break;
					}
			}
			//end switch
			if (intRateType != 200)
			{
				if (intRateType != 1 && intRateType != 6 && intRateType != 4 && intRateType != 7 && intRateType != 12 && intRateType != 22 && (intRateType != 30 || cmbReminderLien.Text != "Lien"))
				{
					BuildSQL = BuildSQL + strWhereLeader + "(RateType = 'R' OR RateType = 'S')";
				}
			}
			BuildSQL = BuildSQL + " ORDER BY ID desc";
			return BuildSQL;
		}

		private void cmbReportDetail_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cmbReportDetail_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbReportDetail.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmdRIClose_Click(object sender, System.EventArgs e)
		{
			fraRateInfo.Visible = false;
		}

		private void frmRateRecChoice_Activated(object sender, System.EventArgs e)
		{
			if (boolUnloadMe || intRateType == -1)
			{
				this.Unload();
				return;
			}
			if (!boolLoaded)
			{
				boolLoaded = true;
				// show that the form was loaded
				boolFormatting = true;
				txtMailDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				if (modUTStatusPayments.Statics.TownService == "W")
				{
					cmbWS.Clear();
					cmbWS.Items.Add("Water");
					cmbWS.Text = "Water";
				}
				else if (modUTStatusPayments.Statics.TownService == "B")
				{
					cmbWS.Clear();
					cmbWS.Items.Add("Water");
					cmbWS.Items.Add("Sewer");
					cmbWS.Text = "Water";
					if (boolFromDisconnect)
					{
						cmbWS.Text = "Water";
					}
					else
					{
						cmbWS.Text = "Sewer";
					}
				}
				else if (modUTStatusPayments.Statics.TownService == "S")
				{
					cmbWS.Clear();
					cmbWS.Items.Add("Sewer");
					cmbWS.Text = "Sewer";
				}
				if (!fcFramePrint.Visible)
				{
					SetAct_8(0, true);
					// load the grid
					ChangeRateKeys();
				}
                if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
                {
                    //optWS[0].Text = "Stormwater";
                    if (cmbWS.Items.Contains("Water"))
                    {
                        cmbWS.Items.Remove("Water");
                        cmbWS.Items.Insert(0, "Stormwater");
                    }
                }
            }
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			boolFormatting = false;
			// this will only show the option if it is transfer tax to lien, loadback or creating bills
			if (intRateType == 6 || intRateType == 100 || intRateType == 101)
			{
				mnuFileAddRateKey.Visible = true;
				cmdFileAddRateKey.Visible = true;
			}
			else
			{
				mnuFileAddRateKey.Visible = false;
				cmdFileAddRateKey.Visible = false;
			}
			// kk11032014 trout-1050  Add option to Select All rates for Consumption History Report
			if (intRateType == 250)
			{
				mnuFileSelectAll.Visible = true;
			}
			else
			{
				mnuFileSelectAll.Visible = false;
			}
			// FC:FINAL:VGE - #i1148 Disabling fraDateRange if it was enabled before.
			if (fraDateRange.Visible)
			{
				fraDateRange.Visible = false;
				fraDateRange.Enabled = false;
			}
		}

		private void frmRateRecChoice_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void frmRateRecChoice_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
                        if (!fraFilter.Visible || (this.ActiveControl.Name.ToLower() != "txtfilterenddate" &&
                                                   this.ActiveControl.Name.ToLower() != "txtfilterend"))
                        {
                            KeyCode = (Keys) 0;
                            Support.SendKeys("{Tab}", false);
                        }

                        break;
					}
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						if (fraRateInfo.Visible)
						{
							fraRateInfo.Visible = false;
						}
                        else if (fraFilter.Visible)

                        {
                            fraFilter.Visible = false;
                        }
						else
						{
							this.Unload();
						}
						break;
					}
				case Keys.F10:
					{
						// turn off the F10 key
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmRateRecChoice_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetTRIOColors(this);
			// set the TRIO colors for the controls on this form
			// this will set the default column numbers
			lngBillDateCol = 2;
			lngBillingDateCol = 3;
			lngDueDate1Col = 4;
			lngStatusCol = 5;
			lngDescriptionCol = 6;
			lngHiddenCol = 1;
			lngLienCol = 7;
			boolFormatting = true;
			// this will make sure that the form knows that I am formatting it instead of the user changing the size of the form
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			FormatGrid();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			if ((intRateType >= 10 && intRateType <= 12) || (intRateType >= 20 && intRateType <= 22))
			{
				fraRate.Visible = false;
				fraQuestions.Visible = false;
				fcFramePrint.Visible = true;
				//FC:FINAL:DDU:#1126 - changed location of frame
				fcFramePrint.Location = new Point(30, 30);
				fraPrintOptions.Visible = false;
				fcFrameWS.Visible = false;
				mnuDateRanges.Visible = false;
				// trouts-227
				cmdDateRanges.Visible = false;
			}
			else
			{
				switch (intRateType)
				{
					case 100:
					case 201:
					case 110:
						{
							fcFrameWS.Visible = false;
							fraPrintOptions.Visible = false;
							break;
						}
					case 25:
					case 101:
					case 102:
					case 103:
					case 106:
					case 107:
					case 200:
						{
							fcFrameWS.Visible = true;
							fraPrintOptions.Visible = false;
							break;
						}
					default:
						{
							fcFrameWS.Visible = false;
							fraPrintOptions.Visible = false;
							// trouts-227 kjr 04.25.17 add date range frame
							fraDateRange.Visible = false;
							fraDateRange.Enabled = false;
							fraRate.Enabled = true;
							break;
						}
				}
				//end switch
			}
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				//optWS[0].Text = "Stormwater";
				if (cmbWS.Items.Contains("Water"))
				{
					cmbWS.Items.Remove("Water");
					cmbWS.Items.Insert(0, "Stormwater");
				}
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			boolFormatting = false;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			boolUnloadMe = false;
			intAction = 0;
		}

		private void SetAct_2(short intAct, bool boolQuery = true)
		{
			SetAct(ref intAct, boolQuery);
		}

		private void SetAct_8(short intAct, bool boolQuery = true)
		{
			SetAct(ref intAct, boolQuery);
		}

		private void SetAct(ref short intAct, bool boolQuery = true)
		{
			int intErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				int intTemp;
				// this will set up the grid and show/hide all of the frames depending on intRateType
				// set the cursor to the hourglass
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				intErrCode = 1;
				if (intRateType == 1)
				{
					// pre lien edit report
					intErrCode = 2;
					fraQuestions.Visible = false;
					cmbLien.Clear();
					cmbLien.Items.Add("All Records");
					cmbLien.Items.Add("Pre Lien Only");
					cmbLien.Items.Add("Liened Only");
					cmbLien.Items.Add("30 Day Notice");
					cmbLien.Items.Add("Lien");
					cmbLien.Items.Add("Lien Maturity");
					cmbLien.Text = "All Records";
					fraPrintOptions.Visible = true;
					cmbExtraLines.Clear();
					cmbExtraLines.AddItem("0");
					cmbExtraLines.AddItem("1");
					cmbExtraLines.AddItem("2");
					cmbExtraLines.AddItem("3");
					cmbExtraLines.AddItem("4");
					cmbExtraLines.AddItem("5");
					cmbExtraLines.AddItem("6");
					cmbExtraLines.Text = "0";
					intErrCode = 3;
					lblEligible.Visible = true;
					//lnEligible.Visible = true;
					intErrCode = 4;
					FillReportDetailCombo();
					intErrCode = 5;
					intErrCode = 6;
					fraPreLienEdit.Visible = true;
					fraPreLienEdit.Enabled = true;
					intErrCode = 7;
					// set the default mail date from the last time this was entered
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "LastLienEditReportDate", ref strTemp);
					intErrCode = 8;
					if (Strings.Trim(strTemp) != "")
					{
						if (Information.IsDate(strTemp))
						{
							txtMailDate.Text = strTemp;
						}
						else
						{
							// do nothing
						}
					}
					else
					{
						// do nothing
					}
					// this is to show the Initial or Reprint message
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					// show the questions - need to shift this to the left so that fraWS can fit
					fraQuestions.Visible = true;
					fraQuestions.Enabled = true;
					intErrCode = 10;
					fraRate.Visible = true;
					fraRate.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;
					//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
					intErrCode = 14;
					fraRange.Visible = true;
					fraRange.Enabled = true;
				}
				else if (intRateType == 2)
				{
					fraPrintOptions.Visible = false;
					fraQuestions.Visible = false;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					cmbLien.Clear();
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					intErrCode = 12;
					// position the grid
					fraRate.Visible = true;
					fraRate.Enabled = true;
					fraRange.Visible = true;
					fraRange.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;
					//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
				}
				else if (intRateType == 6)
				{
					// MAL@20070917: Changed so that range and minimum amounts don't show for transfer to lien process
					fraPrintOptions.Visible = false;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					fraQuestions.Visible = false;
					cmbLien.Clear();
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					intErrCode = 12;
					// position the grid
					fraRate.Visible = true;
					fraRate.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;
					intErrCode = 13;
					intErrCode = 14;
					fraRange.Visible = false;
					fraRange.Enabled = false;
				}
				else if ((intRateType >= 3 && intRateType <= 5) || (intRateType >= 7 && intRateType <= 9) || (intRateType == 61))
				{
					// Case 3 To 5, 7 To 10, 61  'kk11202014 trouts-111 Changed to remove Labels
					// Case 3 To 10, 61          'MAL@20070917: Changed to remove transfer to lien from range
					fraPrintOptions.Visible = false;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					fraQuestions.Visible = false;
					cmbLien.Clear();
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					intErrCode = 12;
					// position the grid
					fraRate.Visible = true;
					fraRate.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;
					//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
					intErrCode = 13;
					intErrCode = 14;
					fraRange.Visible = true;
					fraRange.Enabled = true;
				}
				else if ((intRateType == 100) || (intRateType == 201))
				{
					cmbLien.Clear();
					intErrCode = 15;
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					intErrCode = 16;
					// position the grid
					fraRate.Visible = true;
					fraRate.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;
					//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
					intErrCode = 17;
					// hide the questions
					fraQuestions.Visible = true;
					fraQuestions.Enabled = true;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					intErrCode = 18;
					// hide the reprint frame
					fraPreLienEdit.Visible = false;
				}
				else if ((intRateType >= 101 && intRateType <= 107) || (intRateType == 25) || (intRateType == 110) || (intRateType == 111))
				{
					cmbLien.Clear();
					intErrCode = 10115;
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					intErrCode = 10116;
					// position the grid
					fraRate.Visible = true;
					fraRate.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;
					//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
					intErrCode = 10117;
					// hide the questions
					fraQuestions.Visible = false;
					fcFrameWS.Visible = false;
					intErrCode = 10118;
					// hide the reprint frame
					fraPreLienEdit.Visible = false;
				}
				else if (intRateType == 30)
				{
					cmbLien.Clear();
					cmbLien.Items.Add("Liened Only");
					cmbLien.Items.Add("30 Day Notice");
					cmbLien.Items.Add("Lien");
					cmbLien.Items.Add("Lien Maturity");
					cmbLien.Text = "Liened Only";
					intErrCode = 120;
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					intErrCode = 121;
					cmbLien.Clear();
					intErrCode = 122;
					// position the grid
					fraRate.Visible = true;
					fraRate.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;
					//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
					intErrCode = 123;
					fcFramePrint.Visible = false;
					//FC:FINAL:DDU:#1126 - changed location of frame
					fcFramePrint.Location = new Point(351, 88);
					fraPrintOptions.Visible = false;
					fcFrameWS.Visible = true;
					// show the questions
					fraQuestions.Visible = false;
					fraQuestions.Enabled = true;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					fcFrameReminderLien.Visible = true;
					fcFrameReminderLien.Left = fraQuestions.Left;
					cmbReminderLien.Enabled = true;
					intErrCode = 124;
					// hide the reprint frame
					fraPreLienEdit.Visible = false;
				}
				else if ((intRateType == 50) || (intRateType == 60))
				{
					// period report, reverse 30 DN
					cmbLien.Clear();
					cmbLien.Items.Add("Liened Only");
					cmbLien.Items.Add("30 Day Notice");
					cmbLien.Items.Add("Lien");
					cmbLien.Items.Add("Lien Maturity");
					cmbLien.Text = "Liened Only";
					intErrCode = 130;
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					intErrCode = 131;
					cmbLien.Clear();
					intErrCode = 132;
					// position the grid
					fraRate.Visible = true;
					fraRate.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;
					//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
					intErrCode = 133;
					fcFramePrint.Visible = false;
					//FC:FINAL:DDU:#1126 - changed location of frame
					fcFramePrint.Location = new Point(351, 88);
					fraPrintOptions.Visible = false;
					// show the questions
					fcFrameWS.Visible = true;
					fraQuestions.Visible = true;
					fraQuestions.Enabled = true;
					intErrCode = 134;
					if (intRateType == 50)
					{
						fcFramePeriod.Visible = true;
					}
				}
				else if ((intRateType == 10) || (intRateType == 20))
				{
					// kk11202014 trouts-111  Added 10 (Labels) here
					// Labels
					// CMF
					intErrCode = 19;
					cmbLien.Clear();
					cmbLien.Items.Add("Liened Only");
					cmbLien.Items.Add("30 Day Notice");
					cmbLien.Items.Add("Lien");
					cmbLien.Items.Add("Lien Maturity");
					cmbLien.Text = "Liened Only";
					intErrCode = 20;
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					intErrCode = 21;
					cmbLien.Clear();
					intErrCode = 22;

					fraRate.Visible = true;
					fraRate.Enabled = true;
					mnuDateRanges.Visible = true;
					cmdDateRanges.Visible = true;

					intErrCode = 23;

					fraQuestions.Visible = false;
					fraQuestions.Enabled = false;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					intErrCode = 24;
					fraPreLienEdit.Visible = false;
					// kk11202014 trouts-111 Don't show the range for labels/cmf because we're using the range from the notices
					fraRange.Visible = false;
					// True
					fraRange.Enabled = false;
					// True
				}
				else
				{
					// Labels
					// CMF
					intErrCode = 19;
					cmbLien.Clear();
					cmbLien.Items.Add("Liened Only");
					cmbLien.Items.Add("30 Day Notice");
					cmbLien.Items.Add("Lien");
					cmbLien.Items.Add("Lien Maturity");
					cmbLien.Text = "Liened Only";
					intErrCode = 20;
					lblEligible.Visible = false;
					//lnEligible.Visible = false;
					intErrCode = 21;
					cmbLien.Clear();
					intErrCode = 22;
					// position the grid
					switch (intRateType)
					{
						case 2:
							{
								// 30 Day Process
								fraRate.Visible = true;
								fraRate.Enabled = true;
								mnuDateRanges.Visible = true;
								// trouts-227
								cmdDateRanges.Visible = true;
								//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
								break;
							}
						default:
							{
								fraRate.Visible = true;
								fraRate.Enabled = true;
								mnuDateRanges.Visible = true;
								cmdDateRanges.Visible = true;
								//fraDateRange.Font = new Font(fraDateRange.Font.FontFamily, fraDateRange.Font.Size, FontStyle.Bold);
								break;
							}
					}
					//end switch
					intErrCode = 23;

					fraQuestions.Visible = false;
					fraQuestions.Enabled = false;
					fcFrameWS.Visible = true;
					cmbWS.Enabled = true;
					intErrCode = 24;
					fraPreLienEdit.Visible = false;
				}
				// hide/show the menu options
				mnuFileSave.Visible = true;
				intErrCode = 27;
				if (boolQuery)
				{
					//Application.DoEvents();
					CreateBillMasterYearQuery();
				}
				intErrCode = 28;
				// set the cursor back to the default
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				return;
			}
			catch (Exception ex)
			{
				
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Setup Error - " + FCConvert.ToString(intErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmRateRecChoice_Resize(object sender, System.EventArgs e)
		{
			if (!boolFormatting)
			{
				if (!boolLoaded)
				{
					FormatGrid();
				}
				// SetAct intAction        'this will move everything to the correct place after the resize
				// set the height of the grid after resizing
				if (fcFramePrint.Visible)
				{
					fcFrameWS.Visible = false;
				}
				if (vsRate.Rows > 1)
				{
					if (vsRate.Rows == 2 && intRateType != 6)
					{
						// this only has one rate record, so select it automatically
						vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
					}
				}
				switch (intRateType)
				{
					case 1:
						{
							break;
						}
					default:
						{
							// if it is not a lien edit report then resize fraQuestions to the smaller version
							// fraWS.Visible = True
							// fraWS.Enabled = True
							cmbLien.Clear();
							lblEligible.Visible = false;
							//lnEligible.Visible = false;
							break;
						}
				}
				//end switch
			}
			//FC:FINAL:SBE - #1311 - center filter frame to parent container
			fraFilter.CenterToContainer(fraFilter.Parent);
		}

		private void LoadGrid()
		{
			string strTemp = "";
			int lngRK = 0;
			int lngRW;
			bool boolReClick = false;
			int[] arrRateKey = null;
			int lngCT;
			int lngClicked = 0;
			if (vsRate.Rows > 1)
			{
				for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(lngCT, 0)) == -1)
					{
						lngClicked += 1;
						Array.Resize(ref arrRateKey, lngClicked + 1);
						arrRateKey[lngClicked] = FCConvert.ToInt32(Math.Round(Conversion.Val(vsRate.TextMatrix(lngCT, lngHiddenCol))));
						boolReClick = true;
					}
				}
			}
			// this will take a recordset with records and fill the grid with it
			vsRate.Rows = 1;
			vsRate.Cols = 8;
			while (!rsRateRec.EndOfFile())
			{
				vsRate.AddItem("");
				vsRate.TextMatrix(vsRate.Rows - 1, lngBillingDateCol, Strings.Format(rsRateRec.Get_Fields("Start"), "MM/dd/yyyy"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngDueDate1Col, Strings.Format(rsRateRec.Get_Fields("IntStart"), "MM/dd/yyyy"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngDescriptionCol, rsRateRec.Get_Fields_String("Description"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngHiddenCol, rsRateRec.Get_Fields_Int32("ID"));
				vsRate.TextMatrix(vsRate.Rows - 1, lngLienCol, rsRateRec.Get_Fields_String("RateType"));
				if (FCConvert.ToString(rsRateRec.Get_Fields_String("RateType")) == "L")
				{
					// change the backcolor to grey
					vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Rows - 1, 0, vsRate.Rows - 1, vsRate.Cols - 1, Information.RGB(230, 230, 230));
				}
				else
				{
					vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Rows - 1, 0, vsRate.Rows - 1, vsRate.Cols - 1, Color.White);
				}
				if (Information.IsDate(rsRateRec.Get_Fields("BillDate")))
				{
					vsRate.TextMatrix(vsRate.Rows - 1, lngBillDateCol, FCConvert.ToString(rsRateRec.Get_Fields_DateTime("BillDate")));
				}
				else
				{
					vsRate.TextMatrix(vsRate.Rows - 1, lngBillDateCol, "");
					//vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Rows - 1, lngBillDateCol, modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				}
				rsRateRec.MoveNext();
			}
			if (boolReClick)
			{
				// if there was rows checked before, then recheck them
				for (lngClicked = 1; lngClicked <= Information.UBound(arrRateKey, 1); lngClicked++)
				{
					for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
					{
						if (arrRateKey[lngClicked] == Conversion.Val(vsRate.TextMatrix(lngCT, lngHiddenCol)))
						{
							vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
							break;
						}
					}
				}
			}
			if (intRateType == 6)
			{
				vsRate.AddItem("\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New");
			}
			if (Strings.Trim(strDefaultRateKeyList) != "")
			{
				strTemp = strDefaultRateKeyList;
				do
				{
					//Application.DoEvents();
					// find the first comma and take the val before it
					lngRK = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strTemp, Strings.InStr(1, strTemp, ",", CompareConstants.vbBinaryCompare) - 1))));
					// remove that key and comma from the list
					strTemp = Strings.Right(strTemp, strTemp.Length - Strings.InStr(1, strTemp, ",", CompareConstants.vbBinaryCompare));
					// find the rate key in the list and check it
					for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
					{
						if (Conversion.Val(vsRate.TextMatrix(lngRW, lngHiddenCol)) == lngRK)
						{
							vsRate.TextMatrix(lngRW, 0, FCConvert.ToString(-1));
							break;
						}
					}
				}
				while (!(Strings.Trim(strTemp) == ""));
			}
			if (intRateType == 101)
			{
				if (boolAddedRK)
				{
					if (vsRate.Rows > 1)
					{
						vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
						// select the newest key
					}
					boolAddedRK = false;
				}
			}
			// this sets the height
			if (vsRate.Rows > 1)
			{
				// this only has one rate record, so select it automatically
				if (vsRate.Rows == 2 || (intRateType == 6 && vsRate.Rows == 3))
				{
					vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
				}
                ApplyRateFilters();
			}
		}

		private void CreateBillMasterYearQuery()
		{
			clsDRWrapper rsCreate = new clsDRWrapper();
			string strWhereClause = "";
			int intCT;
			string strWS = "";
			if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			if (intRateType == 4 || intRateType == 7 || intRateType == 12 || intRateType == 22)
			{
				// this will inner join the lien rec
				for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
					{
						if (Strings.Trim(strWhereClause) == "")
						{
							strWhereClause = "(Lien.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol) + " OR BillingRateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
						}
						else
						{
							strWhereClause += " OR Lien.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol) + " OR BillingRateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
						}
					}
				}
				if (Strings.Trim(strWhereClause) != "")
				{
					strWhereClause += ")";
					//Application.DoEvents();
					// kgk rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT * FROM (SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.Key) AS Main INNER JOIN Lien ON Main." & strWS & "LienRecordNumber = Lien.LienKey WHERE " & strWhereClause, strUTDatabase
					// XXXXXX
					modMain.Statics.gstrBillingMasterYear = "SELECT * FROM (SELECT SIntAdded, SCostOwed, SCostAdded, SPrinPaid, SIntPaid, SCostPaid, SPrinOwed, STaxOwed, SIntOwed, WIntAdded, WCostOwed, WCostAdded, WPrinPaid, WIntPaid, WCostPaid, WPrinOwed, WTaxOwed, WIntOwed, BillingRateKey, AccountKey, InBankruptcy, Bill.ID AS Bill, BillStatus, SLienStatusEligibility, SLienRecordNumber, WLienStatusEligibility, WLienRecordNumber, LienProcessExclusion, ActualAccountNumber, BillDate FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID) AS Main INNER JOIN Lien ON Main." + strWS + "LienRecordNumber = Lien.ID WHERE " + strWhereClause;
				}
				else
				{
					//Application.DoEvents();
					// kgk rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT * FROM Bill INNER JOIN Lien ON Bill." & strWS & "LienRecordNumber = Lien.LienKey"
					modMain.Statics.gstrBillingMasterYear = "SELECT Bill.*, Bill.ID AS Bill, Lien.ID AS LienKey, Lien.IntPaidDate, Lien.Principal, Lien.Tax, Lien.Interest, Lien.IntAdded, Lien.Costs, Lien.PrinPaid, Lien.TaxPaid, Lien.PLIPaid, Lien.IntPaid, Lien.CostPaid, Lien.MaturityFee, Lien.DateCreated, Lien.Status, Lien.RateKey, Lien.Water " + "FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID";
				}
			}
			else
			{
				for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
					{
						if (Strings.Trim(strWhereClause) == "")
						{
							// MAL@20070918: Found another error testing another process
							strWhereClause = "(RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
							// strWhereClause = strWhereClause & " AND (RateKey = " & vsRate.TextMatrix(intCT, lngHiddenCol)
							strRateKeyList = " (" + vsRate.TextMatrix(intCT, lngHiddenCol) + ",";
						}
						else
						{
							strWhereClause += " OR RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
							strRateKeyList += vsRate.TextMatrix(intCT, lngHiddenCol) + ",";
						}
					}
				}
				if (Strings.Trim(strWhereClause) != "")
				{
					strRateKeyList = Strings.Left(strRateKeyList, strRateKeyList.Length - 1);
					// take the end comma off
					strWhereClause += ")";
					// add a parenthesis
					strRateKeyList += ")";
				}
				// MAL@20070918
				if (Strings.Trim(strWhereClause) != "")
				{
					strWhereClause = " WHERE " + strWhereClause;
				}
				//Application.DoEvents();
				if (intLienedAccounts == 2)
				{
					// MAL@20070918: Found this error testing another process
					// rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT " & strSelect & " FROM Lien INNER JOIN Bill ON Lien.LienKey = Bill." & strWS & "LienRecordNumber WHERE " & Right(strRateKeyList, Len(strRateKeyList) - 4), strUTDatabase
					// kgk rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT " & strSelect & " FROM Lien INNER JOIN Bill ON Lien.LienKey = Bill." & strWS & "LienRecordNumber" & strWhereClause, strUTDatabase
					// XXXXXX
					modMain.Statics.gstrBillingMasterYear = "SELECT Bill.*, Bill.ID AS Bill, Lien.ID AS LienKey, Lien.IntPaidDate, Lien.Principal, Lien.Tax, Lien.Interest, Lien.IntAdded, Lien.Costs, Lien.PrinPaid, Lien.TaxPaid, Lien.PLIPaid, Lien.IntPaid, Lien.CostPaid, Lien.MaturityFee, Lien.DateCreated, Lien.Status, Lien.RateKey, Lien.Water " + "FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID";
				}
				else
				{
					if (Strings.Trim(strRateKeyList) != "()" && Strings.Trim(strRateKeyList) != "" && Strings.Trim(strRateKeyList) != "(Add New)")
					{
						// kgk rsCreate.CreateStoredProcedure "BillingMasterYear", "SELECT * FROM Bill WHERE BillingRateKey IN " & strRateKeyList, strUTDatabase
						modMain.Statics.gstrBillingMasterYear = "SELECT *, Bill.ID AS Bill FROM Bill WHERE BillingRateKey IN " + strRateKeyList;
					}
					else
					{
						// MsgBox "No rate key was selected.", vbExclamation, "Select Rate Key"
					}
				}
			}
		}

		private void FormatGrid()
		{
			// this sub will set the format of the grid
			int wid = 0;
			vsRate.Cols = 8;
			vsRate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsRate.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSortShow;
			// set the widths
			wid = vsRate.WidthOriginal;
			vsRate.ColWidth(0, FCConvert.ToInt32(wid * 0.07));
			vsRate.ColWidth(lngBillDateCol, FCConvert.ToInt32(wid * 0.18));
			// Bill Date
			vsRate.ColWidth(lngBillingDateCol, FCConvert.ToInt32(wid * 0.18));
			// Start Date
			vsRate.ColWidth(lngDueDate1Col, FCConvert.ToInt32(wid * 0.18));
			// Int Start Date
			vsRate.ColWidth(lngStatusCol, FCConvert.ToInt32(wid * 0.0));
			// Status
			vsRate.ColWidth(lngDescriptionCol, FCConvert.ToInt32(wid * 0.28));
			// Description
			vsRate.ColWidth(lngHiddenCol, FCConvert.ToInt32(wid * 0.09));
			// Rate Key
			vsRate.ColWidth(lngLienCol, 0);
			// set the alignment values
			vsRate.ColAlignment(lngBillDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngBillingDateCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngDueDate1Col, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngStatusCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsRate.ColAlignment(lngDescriptionCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// set the formatting
			vsRate.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsRate.ColFormat(lngBillDateCol, "MM/dd/yyyy");
			vsRate.ColDataType(lngBillDateCol, FCGrid.DataTypeSettings.flexDTDate);
			// Set data type for correct sorting
			vsRate.ColFormat(lngBillingDateCol, "MM/dd/yyyy");
			vsRate.ColDataType(lngBillingDateCol, FCGrid.DataTypeSettings.flexDTDate);
			vsRate.ColFormat(lngDueDate1Col, "MM/dd/yyyy");
			vsRate.ColDataType(lngDueDate1Col, FCGrid.DataTypeSettings.flexDTDate);
			vsRate.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, 5, true);
			//vsRate.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//FC:FINAL:CHN - issue #1037: order by key is not correct
			vsRate.ColDataType(lngBillDateCol - 1, FCGrid.DataTypeSettings.flexDTLong);
			// CHN: key column is before (lngBillDateCol).
			// set the header titles
			vsRate.TextMatrix(0, 0, "Select");
			vsRate.TextMatrix(0, lngBillDateCol, "Billing Date");
			vsRate.TextMatrix(0, lngBillingDateCol, "PerStart Date");
			vsRate.TextMatrix(0, lngDueDate1Col, "Int Start Date");
			vsRate.TextMatrix(0, lngStatusCol, "Status");
			vsRate.TextMatrix(0, lngHiddenCol, "Key");
			vsRate.TextMatrix(0, lngDescriptionCol, "Description");
		}

		private void fraDateRange_DragDrop(object sender, Wisej.Web.DragEventArgs e)
		{
		}

		private void mnuDateRanges_Click(object sender, System.EventArgs e)
		{
			// trouts-227 kjr 04.25.17 enable date range frame
			if (!fraDateRange.Visible)
			{
				fraDateRange.Visible = true;
				fraDateRange.Enabled = true;
				fraDateRange.BringToFront();
				// needed for Lien Edit Rpt where frame doesn't fit
				fraRate.Enabled = false;
				fraRate.Visible = false;
				fraPreLienEdit.Top = 328;
				fraRange.Top = 328;
				fraPrintOptions.Top = 478;
				mnuFileClear_Click();
			}
		}

		private void mnuFileAddRateKey_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDef = new clsDRWrapper();
				// vbPorter upgrade warning: dtCommit As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtCommit;
				// vbPorter upgrade warning: dtLienDate As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtLienDate;
				// vbPorter upgrade warning: dtBillDate As DateTime	OnWrite(DateTime, short)
				DateTime dtBillDate;
				string strWS = "";
				this.Hide();
				if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				int lngPassRateKey = 0;
				switch (intRateType)
				{
					case 6:
						{
							// this will send the default information and set the type of rate rec to create
							// find the default commitment date
							rsDef.OpenRecordset("SELECT * FROM RateKeys WHERE RateType = 'R' ORDER BY BillDate desc");
							if (rsDef.EndOfFile())
							{
								dtCommit = DateTime.FromOADate(0);
							}
							else
							{
								rsDef.FindFirstRecord("BillDate", "1899/12/30", "<>");
								if (rsDef.NoMatch)
								{
									dtCommit = DateTime.FromOADate(0);
								}
								else
								{
									dtCommit = (DateTime)rsDef.Get_Fields_DateTime("BillDate");
								}
							}
							// find the lien date
							rsDef.OpenRecordset("SELECT * FROM " + strWS + "Control_LienProcess");
							if (!rsDef.EndOfFile())
							{
								//FC:FINAL:MSH - if rsDef.Get_Fields("FilingDate") will be equal to an empty string will be throwed an exception (same with i.issue #1002)
								if (FCConvert.ToDateTime(rsDef.Get_Fields_DateTime("FilingDate") as object).ToOADate() != DateTime.MinValue.ToOADate())
								{
									dtLienDate = (DateTime)rsDef.Get_Fields_DateTime("FilingDate");
								}
								else
								{
									dtLienDate = DateTime.FromOADate(0);
								}
							}
							else
							{
								dtLienDate = DateTime.FromOADate(0);
							}
							if (dtLienDate.ToOADate() != 0)
							{
								dtBillDate = DateAndTime.DateAdd("D", 1, dtLienDate);
							}
							else
							{
								dtBillDate = DateTime.FromOADate(0);
							}
							frmUTAuditInfo.InstancePtr.Init(10, dtLienDate, dtBillDate, DateTime.Now, ref lngPassRateKey);
							break;
						}
					case 100:
						{
							frmUTAuditInfo.InstancePtr.Init(1, DateTime.Now, DateTime.Now, DateTime.Now, ref lngPassRateKey);
							// load back
							break;
						}
					case 101:
						{
							frmUTAuditInfo.InstancePtr.Init(101, DateTime.Now, DateTime.Now, DateTime.Now, ref lngPassRateKey);
							// load back
							break;
						}
					case 201:
						{
							frmUTAuditInfo.InstancePtr.Init(2, DateTime.Now, DateTime.Now, DateTime.Now, ref lngPassRateKey);
							// final billed
							break;
						}
					default:
						{
							frmUTAuditInfo.InstancePtr.Init(0, DateTime.Now, DateTime.Now, DateTime.Now, ref lngPassRateKey);
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuFileAddRateKey_Click()
		{
			mnuFileAddRateKey_Click(mnuFileAddRateKey, new System.EventArgs());
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			int lngCT = 0;
			if (vsRate.Rows > 1)
			{
				lngCT = 1;
				while (!(lngCT >= vsRate.Rows))
				{
					vsRate.TextMatrix(lngCT, 0, "");
					lngCT += 1;
				}
			}
		}

		public void mnuFileClear_Click()
		{
			mnuFileClear_Click(mnuFileClear, new System.EventArgs());
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSql = "";
				bool boolLienRR = false;
				bool boolWater;
				int lngCT;
				string strRateKeyList = "";
				// check to see if this is an add ratekey row
				if (intRateType == 6)
				{
					if (Conversion.Val(vsRate.TextMatrix(vsRate.Rows - 1, 0)) == -1)
					{
						// this is trying to add a row
						mnuFileAddRateKey_Click();
						return;
					}
				}
				if (cmbLien.Text == "All Records")
				{
					// all accounts
					intLienedAccounts = 0;
				}
				else if (cmbLien.Text == "Pre Lien Only")
				{
					// pre lien
					intLienedAccounts = 1;
				}
				else if (cmbLien.Text == "Liened Only")
				{
					// liened accounts
					intLienedAccounts = 2;
				}
				else if (cmbLien.Text == "30 Day Notice")
				{
					// eligible for 30 day notices
					InitialSettings_2("30DAYNOTICE");
					intLienedAccounts = 3;
				}
				else if (cmbLien.Text == "Lien")
				{
					// eligible for liens
					intLienedAccounts = 4;
					InitialSettings_2("LIENPROCESS");
				}
				else if (cmbLien.Text == "Lien Maturity")
				{
					// eligible for lien maturity
					intLienedAccounts = 5;
					InitialSettings_2("LIENMATURITY");
				}
				boolWater = FCConvert.CBool(cmbWS.Text == "Water" || cmbWS.Text == "Stormwater");
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (!fcFramePrint.Visible)
				{
					// this will check to see if the print type is being determined
					// if it is not then process normally
					if (vsRate.Enabled && vsRate.Visible)
					{
						vsRate.Select(0, 0);
					}
					if (fraDateRange.Enabled == true)
					{
						CheckDateRanges();
						// trouts-227 5.5.2017 Date Range selection option
					}
					if (GetRateKey(ref boolLienRR) == 0)
					{
						if (fraDateRange.Enabled == true)
						{
							MessageBox.Show("No rate records were found within the date range(s) you selected.", "Select Rate Record By Date Range", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("You must select a rate record before you may continue.", "Select Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						return;
					}
					//Application.DoEvents();
					CreateBillMasterYearQuery();
					// this will load the next screen depending on how the user got here
					switch (intRateType)
					{
						case 0:
							{
								// this is nothing
								MessageBox.Show("The Rate Type was not setup.", "Rate Rec Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								// do nothing else...if this happens then there is a bug
								break;
							}
						case 1:
							{
								// lien process report
								if (ValidateAnswers())
								{
									this.Hide();
									//Application.DoEvents();
									string strCommaDelRKList = "";
									strRateKeyList = RateKeyList(ref strCommaDelRKList);
									// MAL@20080212: Tracker Reference: 12308
									arLienProcessEditReport.InstancePtr.StartLienEditReportQuery(ref intLienedAccounts, ref boolWater, strRateKeyList, strBookList, strDateRanges);
									//Application.DoEvents();
									frmReportViewer.InstancePtr.Init(arLienProcessEditReport.InstancePtr);
								}
								break;
							}
						case 2:
							{
								// 30 day notice
								if (ValidateAnswers())
								{
									this.Hide();
									SaveRateKeyList();
									modRhymalReporting.Statics.strFreeReportType = "30DAYNOTICE";
									string strCommaDelRKList = "";
									frmFreeReport.InstancePtr.strRateKeyList = RateKeyList(ref strCommaDelRKList);
									frmFreeReport.InstancePtr.strBookList = strBookList;
									frmFreeReport.InstancePtr.Show(App.MainForm);
									//Application.DoEvents();
									frmFreeReport.InstancePtr.Focus();
								}
								break;
							}
						case 3:
							{
								// lien process
								if (ValidateAnswers())
								{
									this.Hide();
									modRhymalReporting.Statics.strFreeReportType = "LIENPROCESS";
									string strCommaDelRKList = "";
									frmFreeReport.InstancePtr.strRateKeyList = RateKeyList(ref strCommaDelRKList);
									frmFreeReport.InstancePtr.strBookList = strBookList;
									frmFreeReport.InstancePtr.Show(App.MainForm);
									//Application.DoEvents();
									frmFreeReport.InstancePtr.Focus();
								}
								break;
							}
						case 4:
							{
								// lien maturity
								if (ValidateAnswers())
								{
									this.Hide();
									modRhymalReporting.Statics.strFreeReportType = "LIENMATURITY";
									string strCommaDelRKList = "";
									frmFreeReport.InstancePtr.strRateKeyList = RateKeyList(ref strCommaDelRKList);
									frmFreeReport.InstancePtr.strBookList = strBookList;
									frmFreeReport.InstancePtr.Show(App.MainForm);
									//Application.DoEvents();
									frmFreeReport.InstancePtr.Focus();
								}
								break;
							}
						case 5:
							{
								// apply demand fees
								frmApplyDemandFees.InstancePtr.Init(strBookList);
								break;
							}
						case 6:
							{
								// transfer tax to lien
								if (ValidateAnswers())
								{
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									// this will hold the lien record
									modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "LastBilledRateKey", lngRateRecNumber.ToString());
									// this is new to not ask for the regular keys and get the last selection from the 30DN selection
									RateKeyList(ref strDefaultRateKeyList);
									if (Strings.Trim(strDefaultRateKeyList) != "")
									{
										strRateKeyList = "(" + Strings.Left(strDefaultRateKeyList, strDefaultRateKeyList.Length - 1) + ")";
										this.Hide();
										string strCommaDelRKList = "";
										frmTransferTaxToLien.InstancePtr.Init(RateKeyList(ref strCommaDelRKList), FCConvert.CBool(cmbWS.Text == "Water" || cmbWS.Text == "Stormwater"), "(" + strBookList + ")");
									}
									else
									{
									}
									// now we need to get the regular rate records that will be used
									// intRateType = 61        'change to regular rate records
									// boolLoaded = False
									// Form_Activate
									// ChangeRateKeys
								}
								break;
							}
						case 61:
							{
								if (ValidateAnswers())
								{
									this.Hide();
									string strCommaDelRKList = "";
									frmTransferTaxToLien.InstancePtr.Init(RateKeyList(ref strCommaDelRKList), FCConvert.CBool(cmbWS.Text == "Water" || cmbWS.Text == "Stormwater"), strBookList);
								}
								break;
							}
						case 7:
							{
								// apply lien maturity notice
								if (ValidateAnswers())
								{
									this.Hide();
									// lngRateRecNumber = GetRateKey(boolLienRR)
									RateKeyList(ref strDefaultRateKeyList);
									if (Strings.Trim(strDefaultRateKeyList) != "")
									{
										strRateKeyList = "(" + Strings.Left(strDefaultRateKeyList, strDefaultRateKeyList.Length - 1) + ")";
									}
									if (modMain.FormExist(frmLienMaturityFees.InstancePtr))
									{
										frmLienMaturityFees.InstancePtr.Unload();
									}
									frmLienMaturityFees.InstancePtr.Init(ref strBookList, ref strRateKeyList);
								}
								break;
							}
						case 10:
							{
								// Demand Labels
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									strSql = BuildLabelSQL(ref intRateType);
									frmCustomLabels.InstancePtr.Init(strSql, intRateType, boolWater, strBookList);
								}
								break;
							}
						case 11:
							{
								// Lien Process Labels
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									strSql = BuildLabelSQL(ref intRateType);
									frmCustomLabels.InstancePtr.Init(strSql, intRateType, boolWater, strBookList);
								}
								break;
							}
						case 12:
							{
								// Lien Maturity Labels
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									strSql = BuildLabelSQL(ref intRateType);
									frmCustomLabels.InstancePtr.Init(strSql, intRateType, boolWater, strBookList);
								}
								break;
							}
						case 20:
							{
								// Demand Forms
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									strSql = BuildLabelSQL(ref intRateType);
									frmCustomLabels.InstancePtr.Init(strSql, intRateType, boolWater, strBookList);
								}
								break;
							}
						case 21:
							{
								// Lien Process Forms
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									strSql = BuildLabelSQL(ref intRateType);
									frmCustomLabels.InstancePtr.Init(strSql, intRateType, boolWater, strBookList);
								}
								break;
							}
						case 22:
							{
								// Lien Maturity Forms
								if (ValidateAnswers())
								{
									this.Hide();
									// Call Reference: 117148
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									strSql = BuildLabelSQL(ref intRateType);
									frmCustomLabels.InstancePtr.Init(strSql, intRateType, boolWater, strBookList);
								}
								break;
							}
						case 25:
							{
								// Analysis Reports
								if (ValidateAnswers())
								{
									this.Hide();
									RateKeyList(ref strDefaultRateKeyList);
									if (Strings.Trim(strDefaultRateKeyList) != "")
									{
										strRateKeyList = "(" + Strings.Left(strDefaultRateKeyList, strDefaultRateKeyList.Length - 1) + ")";
									}
									frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Reminder Information");
									frmBookChoice.InstancePtr.Init(25, 0, false, strRateKeyList);
									frmWait.InstancePtr.Unload();
								}
								break;
							}
						case 30:
							{
								// Reminder Notices
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									strSql = BuildLabelSQL(ref intRateType);
									// MAL@20070925: Call Reference 117179
									RateKeyList(ref strDefaultRateKeyList);
									if (Strings.Trim(strDefaultRateKeyList) != "")
									{
										strRateKeyList = "(" + Strings.Left(strDefaultRateKeyList, strDefaultRateKeyList.Length - 1) + ")";
									}
									frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Reminder Information");
									frmReminderNotices.InstancePtr.Init(ref strRateKeyList, cmbWS.Text == "Water" || cmbWS.Text == "Stormwater", FCConvert.CBool(cmbReminderLien.Text == "Lien"));
									frmWait.InstancePtr.Unload();
								}
								break;
							}
						case 50:
							{
								// If ValidateAnswers Then
								// Me.Hide
								// lngRateRecNumber = GetRateKey(boolLienRR)
								// strSQL = BuildLabelSQL(intRateType)
								// 
								// frmWait.Init "Please Wait..." & vbCrLf & "Loading Report Information"
								// rptOutstandingPeriodBalances.strRateKeyList = strRateKeyList
								// frmReportViewer.Init rptOutstandingPeriodBalances
								// Unload frmWait
								// End If
								break;
							}
						case 60:
							{
								// If ValidateAnswers Then
								// Me.Hide
								// lngRateRecNumber = GetRateKey(boolLienRR)
								// strSQL = BuildLabelSQL(intRateType)
								// 
								// frmWait.Init "Please Wait..." & vbCrLf & "Loading Notice Information"
								// rptOutstandingPeriodBalances.strRateKeyList = strRateKeyList
								// frmReverseDemandFees.Init strRateKeyList
								// Unload frmWait
								// End If
								break;
							}
						case 100:
						case 201:
							{
								// load back or final billed
								if (ValidateAnswers())
								{
									this.Hide();
									lngRateRecNumber = GetRateKey(ref boolLienRR);
									frmLoadBack.InstancePtr.Show(App.MainForm);
									// . Init lngRateRecNumber, boolLienRR
									frmLoadBack.InstancePtr.LoadRateKeys();
									for (lngCT = 0; lngCT <= frmLoadBack.InstancePtr.cboRateKeys.Items.Count - 1; lngCT++)
									{
										if (Conversion.Val(frmLoadBack.InstancePtr.cboRateKeys.Items[lngCT].ToString()) == lngRateRecNumber)
										{
											frmLoadBack.InstancePtr.cboRateKeys.SelectedIndex = lngCT;
										}
									}
								}
								break;
							}
						case 101:
							{
								// Create Bills
								lngRateRecNumber = GetRateKey(ref boolLienRR);
								frmBookChoice.InstancePtr.Init(4, lngRateRecNumber);
								break;
							}
						case 102:
							{
								// Print Bills
								lngRateRecNumber = GetRateKey(ref boolLienRR);
								frmBookChoice.InstancePtr.Init(5, lngRateRecNumber);
								break;
							}
						case 104:
							{
								// Email Bills
								lngRateRecNumber = GetRateKey(ref boolLienRR);
								frmBookChoice.InstancePtr.Init(35, lngRateRecNumber);
								break;
							}
						case 106:
							{
								// Transfer Report
								lngRateRecNumber = GetRateKey(ref boolLienRR);
								frmBookChoice.InstancePtr.Init(6, lngRateRecNumber, false, "", "", true, DateTime.Now, false, 0, true, "", 0, 0, true, strDateRanges);
								break;
							}
						case 107:
							{
								// Billed List
								lngRateRecNumber = GetRateKey(ref boolLienRR);
								frmBookChoice.InstancePtr.Init(7, lngRateRecNumber);
								break;
							}
						case 108:
							{
								// Not Billed List
								lngRateRecNumber = GetRateKey(ref boolLienRR);
								frmBookChoice.InstancePtr.Init(8, lngRateRecNumber);
								break;
							}
						case 110:
							{
								// Split Bills
								lngRateRecNumber = GetRateKey(ref boolLienRR);
								modUTBilling.SplitBills(ref lngRateRecNumber);
								break;
							}
						case 111:
							{
								// kgk 10-14-2011  Export Bills to PDF for invoice cloud
								lngRateRecNumber = GetRateKey(ref boolLienRR);
								frmBookChoice.InstancePtr.Init(36, lngRateRecNumber);
								break;
							}
						case 200:
							{
								if (ValidateAnswers())
								{
									this.Hide();
									bool boolType = false;
									lngRateRecNumber = GetRateKey(ref boolType);
									frmUTAuditInfo.InstancePtr.Init(100, DateTime.Now, DateTime.Now, DateTime.Now, ref lngRateRecNumber);
									// this will allow the user to edit the rate key
								}
								break;
							}
						case 250:
							{
								// Consumption History Report
								RateKeyList(ref strDefaultRateKeyList);
								if (boolWater)
								{
									frmBookChoice.InstancePtr.Init(70, 0, false, strDefaultRateKeyList, "W", true, DateTime.Now, false, 0, true, "", 0, 0, true, strDateRanges);
								}
								else
								{
									frmBookChoice.InstancePtr.Init(70, 0, false, strDefaultRateKeyList, "S", true, DateTime.Now, false, 0, true, "", 0, 0, true, strDateRanges);
								}
								break;
							}
						default:
							{
								// this is nothing
								MessageBox.Show("The Rate Type was not setup.", "Rate Rec Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								// do nothing else...if this happens then there is a bug
								break;
							}
					}
					//end switch
					// End If
				}
				else
				{
					if (cmbPrint.Text == "Labels")
					{
						PrintTypeSetup(0);
					}
					else
					{
						PrintTypeSetup(1);
					}
				}
				switch (intRateType)
				{
					case 2:
						{
							// this will save the rate key list to the control record for 30 DN notices
							SaveRateKeyList();
							break;
						}
				}
				//end switch
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// kk11032014 trout-1050  Add Select All menu item
			int lngCT;
			if (vsRate.Rows > 1)
			{
				for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
				{
                    if (vsRate.RowHidden(lngCT) == false)
                    {
                        vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
                    }
                }
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(mnuFileSelectAll, new System.EventArgs());
		}

		private void optLien_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// update the rate grid
			ChangeRateKeys();
		}

		private void optLien_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbLien.SelectedIndex;
			optLien_CheckedChanged(index, sender, e);
		}

		private void optPrint_KeyDown(int Index, object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						if (fcFramePrint.Visible)
						{
							KeyCode = (Keys)0;
							PrintTypeSetup(Index);
						}
						break;
					}
			}
			//end switch
		}

		private void optPrint_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int index = cmbPrint.SelectedIndex;
			optPrint_KeyDown(index, sender, e);
		}

		private void PrintTypeSetup(int intIndex)
		{
			if (!boolFormatting)
			{
				fcFramePrint.Visible = false;
				//FC:FINAL:DDU:#1126 - changed location of frame
				fcFramePrint.Location = new Point(351, 88);
				if (intIndex == 1)
				{
					// this will set the rate type to forms rather than labels
					intRateType += 10;
				}
				boolFormatting = true;
				SetAct_2(0);
				boolFormatting = false;
				ChangeRateKeys();
			}
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// All Accounts
						txtRange[0].Enabled = false;
						txtRange[1].Enabled = false;
						break;
					}
				case 1:
					{
						// Range by Name
						txtRange[0].Enabled = true;
						txtRange[1].Enabled = true;
						txtRange[0].MaxLength = 20;
						txtRange[1].MaxLength = 20;
						if (txtRange[0].Enabled && txtRange[0].Visible)
						{
							txtRange[0].Focus();
						}
						break;
					}
				case 2:
					{
						// Range by Account Number
						txtRange[0].Enabled = true;
						txtRange[1].Enabled = true;
						txtRange[0].MaxLength = 10;
						txtRange[1].MaxLength = 10;
						if (txtRange[0].Enabled && txtRange[0].Visible)
						{
							txtRange[0].Focus();
						}
						break;
					}
			}
			//end switch
			txtRange[0].Text = "";
			txtRange[1].Text = "";
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void optReminderLien_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (intRateType == 30)
			{
				ChangeRateKeys();
			}
		}

		private void optReminderLien_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReminderLien.SelectedIndex;
			optReminderLien_CheckedChanged(index, sender, e);
		}

		private void optWS_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// this will select either 0 - Water or 1 - Sewer
			ChangeRateKeys();
		}

		private void optWS_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbWS.SelectedIndex;
			optWS_CheckedChanged(index, sender, e);
		}

		private void txtMailDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// set the default mail date from the last time this was entered
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "LastLienEditReportDate", txtMailDate.Text);
		}

		private void txtMinimumAmount_Enter(object sender, System.EventArgs e)
		{
			txtMinimumAmount.SelectionStart = 0;
			txtMinimumAmount.SelectionLength = txtMinimumAmount.Text.Length;
		}

		private void txtMinimumAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
			{
			}
			else if (KeyAscii == Keys.Delete)
			{
				if (Strings.InStr(1, txtMinimumAmount.Text, ".", CompareConstants.vbBinaryCompare) > 0)
				{
					KeyAscii = (Keys)0;
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMinimumAmount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMinimumAmount.Text) > 0)
			{
				txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text), "#,##0.00");
			}
			else
			{
				txtMinimumAmount.Text = "0.00";
			}
		}

		private void txtRange_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			bool boolText;
			boolText = FCConvert.CBool(cmbRange.Text == "Range by Name");
			if (boolText)
			{
				// this will allow all characters
				switch (KeyAscii)
				{
					case Keys.Right:
						{
							// stop single quotes
							KeyAscii = (Keys)0;
							break;
						}
				}
				//end switch
			}
			else
			{
				// this will only allow numbers
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtRange_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			short index = txtRange.GetIndex((FCTextBox)sender);
			txtRange_KeyPress(index, sender, e);
		}

		private void txtFilterStart_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (FCConvert.ToString(fraFilter.Tag) == "RateKey")
			{
				// RateKey - numbers only; Descr - Allow all
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtFilterEnd_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// RateKey - numbers only
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsbMinAmount_ValueChanged(object sender, System.EventArgs e)
		{
			// this allows the scroll bar to adjust the value in the text box
			//if (vsbMinAmount.Value == 0)
			//{
			//    if (FCConvert.ToDouble(txtMinimumAmount.Text)) <= 0)
			//    {
			//        txtMinimumAmount.Text = "0.00";
			//    }
			//    else
			//    {
			//        txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text)) - 0.1, "#,##0.00");
			//        vsbMinAmount.Value = 1;
			//    }
			//}
			//else if (vsbMinAmount.Value == 2)
			//{
			//    txtMinimumAmount.Text = Strings.Format(FCConvert.ToDouble(txtMinimumAmount.Text)) + 0.1, "#,##0.00");
			//    vsbMinAmount.Value = 1;
			//}
		}

		private void vsRate_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsRate.Col == lngBillDateCol)
			{
				vsRate.EditMask = "##/##/####";
				vsRate.EditMaxLength = 10;
			}
		}

		private void vsRate_BeforeMouseDown(object sender, MouseEventArgs e)
		{
			// MAL@20070904: Added event to check for right-click
			boolIsRightClick = e.Button == MouseButtons.Right;
		}

		private void vsRate_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			int lngRW;
			int intTemp = 0;
			if (vsRate.Col == 0)
			{
				switch (intRateType)
				{
					case 100:
					case 101:
					case 102:
					case 106:
					case 107:
					case 108:
					case 200:
					case 201:
					case 111:
						{
							// kk03232015 trouts-122  Add 108 - Non-billed report single rate selection
							if (vsRate.Row > 0 && vsRate.Row <= vsRate.Rows - 1)
							{
								for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
								{
									// this does not have the extra add row feature
									// this will clear any other selection in the grid so the user can only choose one
									if (lngRW != vsRate.Row)
									{
										vsRate.TextMatrix(lngRW, 0, FCConvert.ToString(0));
										vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0, "");
									}
									else
									{
										// and it will show the picture if chosen
										// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0) = MDIParent.ImageList1.ListImages(5).Picture
									}
								}
							}
							break;
						}
					case 6:
						{
							// , 7, 12, 22 'MAL@20070914: Add ability for multiple selections
							if ((vsRate.Row > 0 && vsRate.Row < vsRate.Rows - 1) || intRateType == 7 || intRateType == 22)
							{
								if (intRateType == 7 || intRateType == 22)
								{
									intTemp = 1;
								}
								else
								{
									intTemp = 2;
								}
								for (lngRW = 1; lngRW <= vsRate.Rows - intTemp; lngRW++)
								{
									// this will clear any other selection in the grid so the user can only choose one
									if (lngRW != vsRate.Row)
									{
										vsRate.TextMatrix(lngRW, 0, FCConvert.ToString(0));
										vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0, "");
									}
									else
									{
										// and it will show the picture if chosen
										// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, lngRW, 0) = MDIParent.ImageList1.ListImages(5).Picture
									}
								}
							}
							else if (vsRate.Row == vsRate.Rows - 1 && intRateType == 6)
							{
								if (!boolFormatting)
								{
									mnuFileAddRateKey_Click();
								}
							}
							break;
						}
					default:
						{
							if (vsRate.Row > 0)
							{
								if (Conversion.Val(vsRate.TextMatrix(vsRate.Row, 0)) == -1)
								{
									// vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, Row, 0) = MDIParent.ImageList1.ListImages(5).Picture
									vsRate.Cell(FCGrid.CellPropertySettings.flexcpPictureAlignment, vsRate.Row, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
								}
								else
								{
									vsRate.Cell(FCGrid.CellPropertySettings.flexcpPicture, vsRate.Row, 0, "");
								}
							}
							break;
						}
				}
				//end switch
			}
		}

		private void vsRate_ClickEvent(object sender, System.EventArgs e)
        {
            bool boolDateFilter = false;

			if (vsRate.Col == 0 && currentMouseRow > 0)
			{
				// MAL@20070904: Check for Right or Left Click
				if (boolIsRightClick)
				{
					boolIsRightClick = false;
					// Reset value
				}
				else
				{
					if (Conversion.Val(vsRate.TextMatrix(currentMouseRow, vsRate.Col)) == 0)
					{
						vsRate.TextMatrix(currentMouseRow, vsRate.Col, FCConvert.ToString(-1));
					}
					else
					{
						vsRate.TextMatrix(currentMouseRow, vsRate.Col, FCConvert.ToString(0));
					}
				}
			}
            //FC:FINAL:AM:#3744 - in VB6 the event is not triggered for left click
            //else if (currentMouseRow == 0 && currentMouseCol != 0)
            else if (currentMouseRow == 0 && currentMouseCol != 0 && boolIsRightClick)
            {
				if (currentMouseCol == lngHiddenCol)
				{
					// Setup to filter by range of rate keys
					fraFilter.Text = "Filter by Rate Key";
					fraFilter.Tag = "RateKey";
					if (lngFilterRateKeyStart != 0 || lngFilterRateKeyEnd != 0)
					{
						txtFilterStart.Text = FCConvert.ToString(lngFilterRateKeyStart);
						txtFilterEnd.Text = FCConvert.ToString(lngFilterRateKeyEnd);
					}
					else
					{
						txtFilterStart.Text = "";
						txtFilterEnd.Text = "";
					}
					lblFilterStart.Visible = true;
					lblFilterEnd.Visible = true;
					txtFilterStartDate.Visible = false;
					txtFilterEndDate.Visible = false;
					txtFilterStart.Visible = true;
					txtFilterEnd.Visible = true;
					cmbFilterContains.Visible = false;
                    lblFilterContains.Visible = false;
                    lblFilterBy.Text = "Rate Key:";
					lblFilterBy.Visible = true;
				}
				else if (currentMouseCol == lngBillDateCol)
				{
					// Setup to filter by range of Bill Dates
					fraFilter.Text = "Filter by Billing Date";
					fraFilter.Tag = "BillDate";
					if (dtFilterBillDateStart.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() || dtFilterBillDateEnd.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
					{
						txtFilterStartDate.Text = Strings.Format(dtFilterBillDateStart, "MM/dd/yyyy");
						txtFilterEndDate.Text = Strings.Format(dtFilterBillDateEnd, "MM/dd/yyyy");
					}
					else
					{
						txtFilterStartDate.Text = "";
						txtFilterEndDate.Text = "";
					}
					lblFilterStart.Visible = true;
					lblFilterEnd.Visible = true;
					txtFilterStartDate.Visible = true;
					txtFilterEndDate.Visible = true;
					txtFilterStart.Visible = false;
					txtFilterEnd.Visible = false;
					cmbFilterContains.Visible = false;
                    lblFilterContains.Visible = false;
					lblFilterBy.Text = "Billing Date:";
					lblFilterBy.Visible = true;
                    boolDateFilter = true;
                }
				else if (currentMouseCol == lngBillingDateCol)
				{
					// Setup to filter by range of Billing Dates (Start Dates)
					fraFilter.Text = "Filter by Period Start Date";
					fraFilter.Tag = "StartDate";
					if (dtFilterStartDateStart.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() || dtFilterStartDateEnd.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
					{
						txtFilterStartDate.Text = Strings.Format(dtFilterStartDateStart, "MM/dd/yyyy");
						txtFilterEndDate.Text = Strings.Format(dtFilterStartDateEnd, "MM/dd/yyyy");
					}
					else
					{
						txtFilterStartDate.Text = "";
						txtFilterEndDate.Text = "";
					}
					lblFilterStart.Visible = true;
					lblFilterEnd.Visible = true;
					txtFilterStartDate.Visible = true;
					txtFilterEndDate.Visible = true;
					txtFilterStart.Visible = false;
					txtFilterEnd.Visible = false;
					cmbFilterContains.Visible = false;
                    lblFilterContains.Visible = false;
                    lblFilterBy.Text = "Period Start Date:";
					lblFilterBy.Visible = true;
                    boolDateFilter = true;
                }
				else if (currentMouseCol == lngDueDate1Col)
				{
					// Setup to filter by range of Due Dates
					fraFilter.Text = "Filter by Interest Start Date";
					fraFilter.Tag = "IntDate";
					if (dtFilterIntDateStart.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() || dtFilterIntDateEnd.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
					{
						txtFilterStartDate.Text = Strings.Format(dtFilterIntDateStart, "MM/dd/yyyy");
						txtFilterEndDate.Text = Strings.Format(dtFilterIntDateEnd, "MM/dd/yyyy");
					}
					else
					{
						txtFilterStartDate.Text = "";
						txtFilterEndDate.Text = "";
					}
					lblFilterStart.Visible = true;
					lblFilterEnd.Visible = true;
					txtFilterStartDate.Visible = true;
					txtFilterEndDate.Visible = true;
					txtFilterStart.Visible = false;
					txtFilterEnd.Visible = false;
					cmbFilterContains.Visible = false;
                    lblFilterContains.Visible = false;
                    lblFilterBy.Text = "Interest Start Date:";
					lblFilterBy.Visible = true;
                    boolDateFilter = true;
                }
				else if (currentMouseCol == lngDescriptionCol)
				{
					// Setup to Text filter on Description
					fraFilter.Text = "Filter by Description";
					fraFilter.Tag = "Descr";
					if (Strings.Trim(strFilterDesc) != "")
					{
						txtFilterStart.Text = Strings.Trim(strFilterDesc);
						if (FCConvert.CBool(strFilterDescOption == "C"))
						{
							cmbFilterContains.SelectedIndex = 0;
						}
						else if (FCConvert.CBool(strFilterDescOption == "N"))
						{
							cmbFilterContains.SelectedIndex = 1;
						}
					}
					else
					{
						txtFilterStart.Text = "";
						cmbFilterContains.SelectedIndex = 0;
					}
					lblFilterStart.Visible = false;
					lblFilterEnd.Visible = false;
					txtFilterStartDate.Visible = false;
					txtFilterEndDate.Visible = false;
					txtFilterStart.Visible = true;
					txtFilterEnd.Visible = false;
					cmbFilterContains.Visible = true;
                    lblFilterContains.Visible = true;
                    lblFilterBy.Text = "Description:";
					lblFilterBy.Visible = true;
				}
				fraFilter.Visible = true;
                if (boolDateFilter)
                {
                    txtFilterStartDate.Focus();
                }
                else
                {
                    txtFilterStart.Focus();
                }
                //FC:FINAL:SBE - #1311 - bring filter frame to the top
                fraFilter.BringToFront();
                fraFilter.Height = this.cmbFilterContains.Visible ? 230 : 170;
                fraFilter.Update();
			}
		}

		private void vsRate_DblClick(object sender, System.EventArgs e)
		{
			int lngCT;
			int lngMR = 0;
			int lngMC = 0;
			if ((intRateType == 1) || (intRateType == 2) || (intRateType == 3) || (intRateType >= 10 && intRateType <= 100) || (intRateType == 250))
			{
				lngMR = vsRate.MouseRow;
				lngMC = vsRate.MouseCol;
				if (lngMR == 0 && lngMC == 0)
				{
					mnuFileSelectAll_Click();
				}
			}
		}

		private void vsRate_Enter(object sender, System.EventArgs e)
		{
			if (vsRate.Rows > 1)
			{
				vsRate.Select(0, 0);
			}
			vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void vsRate_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			//FC:FINAL:MSH - Issue #900: saving mouse indexes of selected cell, because after the work of FCGrid.OnCellClick value of MouseRow will be 
			// increased by 1. This happens because table will be scrolled if we will select cell, which not completely visible, and value of 
			// e.RowIndex in FCGrid.OnCellClick will be greater than necessary
			currentMouseRow = vsRate.MouseRow;
			currentMouseCol = vsRate.MouseCol;
			//FC:FINAL:MSH - issue #1310: wrong value after converting button to int, so the comparing is always wrong
			//if (FCConvert.ToInt32(e.Button == 2 && currentMouseRow > 0)
			if (e.Button == MouseButtons.Right && currentMouseRow > 0)
			{
				ShowRateInfo_18(currentMouseRow, currentMouseCol, FCConvert.ToInt32(Conversion.Val(vsRate.TextMatrix(currentMouseRow, lngHiddenCol))));
			}
            //FC:FINAL:SBE - #1311 - right click will not trigger the click event for column header. Force trigger of event
            else if (e.Button == MouseButtons.Right && currentMouseRow == 0)
            {
                vsRate_ClickEvent(sender, EventArgs.Empty);
            }
        }

        private void vsRate_RowColChange(object sender, System.EventArgs e)
		{
			if (vsRate.Col == 0)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			else if (vsRate.Col == lngBillDateCol)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			else if (vsRate.Col == vsRate.Cols - 1)
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
				if (vsRate.Row == vsRate.Rows - 1)
				{
					vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
			}
			else
			{
				vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsRate.Col == 5 && vsRate.Row == vsRate.Rows - 1)
			{
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool ValidateAnswers()
		{
			bool ValidateAnswers = false;
			// this function will check to make sure that all of the answers that are given are valid
			// including the rate tables being checked and the ranges being correct
			int intCT;
			bool boolFound;
			ValidateAnswers = true;
			boolFound = false;
			for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
			{
				//Application.DoEvents();
				if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
				{
					boolFound = true;
					if (vsRate.TextMatrix(intCT, lngBillDateCol) == "")
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please fill the bill date field in for the selected rate table.", "Rate Table Selection", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						ValidateAnswers = false;
						SetAct_2(0);
						//Application.DoEvents();
						if (vsRate.Visible && vsRate.Enabled)
						{
							vsRate.Focus();
							vsRate.Select(intCT, lngBillDateCol);
							vsRate.EditCell();
						}
						return ValidateAnswers;
					}
				}
			}
			if (!boolFound)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Please select a rate table.", "Rate Table Selection", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				ValidateAnswers = false;
				SetAct_2(0);
				vsRate.Focus();
				return ValidateAnswers;
			}
			if (cmbRange.Text == "Range by Name")
			{
				if (Strings.CompareString(txtRange[0].Text, ">", txtRange[1].Text))
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a value in the second section of the range that is greater than the first.", "Range Criteria", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ValidateAnswers = false;
					SetAct_2(2);
					txtRange[1].Focus();
					return ValidateAnswers;
				}
			}
			if (cmbRange.Text == "Range by Account")
			{
				if (Conversion.Val(txtRange[0].Text) > Conversion.Val(txtRange[1].Text))
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a value in the second section of the range that is greater than the first.", "Range Criteria", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ValidateAnswers = false;
					SetAct_2(2);
					txtRange[1].Focus();
					return ValidateAnswers;
				}
			}
			if (intRateType == 1)
			{
				if (txtMailDate.Text != "")
				{
					if (!Information.IsDate(txtMailDate.Text))
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Please enter a valid date in the mail date field.", "Mail Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						ValidateAnswers = false;
						SetAct_2(2);
						txtMailDate.Focus();
						return ValidateAnswers;
					}
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a value in the mail date field.", "Mail Date", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ValidateAnswers = false;
					SetAct_2(2);
					txtMailDate.Focus();
					return ValidateAnswers;
				}
				if (cmbReportDetail.SelectedIndex < 0)
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Please enter a value in the report detail field.", "Report Detail", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					ValidateAnswers = false;
					SetAct_2(2);
					cmbReportDetail.Focus();
					return ValidateAnswers;
				}
			}
			return ValidateAnswers;
		}

		private void vsRate_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - issue #1002: save and use correct indexes of the cell
			int row = vsRate.GetFlexRowIndex(e.RowIndex);
			int col = vsRate.GetFlexColIndex(e.ColumnIndex);
			if (intRateType == 6 && row == vsRate.Rows - 1)
			{
				// skip the validation of this row
			}
			else
			{
				if (col == lngBillDateCol)
				{
					// is it the right column
					if (Information.IsDate(vsRate.EditText))
					{
						// is this a valid date
						// save it in the database
						rsRateRec.FindFirstRecord("ID", vsRate.TextMatrix(row, lngHiddenCol));
						if (rsRateRec.NoMatch)
						{
							MessageBox.Show("Error finding rate record.  Please reload the rate records.", "Rate Record Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
							SetAct_2(0);
						}
						else
						{
							rsRateRec.Edit();
							rsRateRec.Set_Fields("BillDate", Strings.Format(vsRate.EditText, "MM/dd/yyyy"));
							rsRateRec.Update();
							//FC:FINAL:DDU:#i1078 - don't change backcolor
							//vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Row, vsRate.Col, vsRate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsRate.Row, vsRate.Col - 1));
						}
					}
					else
					{
						//FC:FINAL:MSH - issue #1002: capitalize date format in message
						MessageBox.Show("Please enter a valid date in the Bill Date field. (MM/DD/YYYY)", "Bill Date Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
						// If Val(vsRate.TextMatrix(Row, 0)) = -1 Then
						e.Cancel = true;
						// End If
					}
				}
			}
		}

		private void ChangeRateKeys()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				int lngRW;
				// show the Rate Keys
				rsRateRec.OpenRecordset(BuildSQL(), modExtraModules.strUTDatabase);
				if (rsRateRec.EndOfFile() != true)
				{
					// show the rate keys and size the grid
					if (intAction != 2)
					{
						LoadGrid();
						if (intRateType == 6)
						{
							modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "LastLienEditReportDate", ref strTemp);
							if (Conversion.Val(strTemp) != 0)
							{
								// find this RK and check it
								for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
								{
									if (Conversion.Val(vsRate.TextMatrix(lngRW, lngBillDateCol)) == Conversion.Val(strTemp))
									{
										vsRate.TextMatrix(lngRW, 0, FCConvert.ToString(-1));
									}
								}
							}
						}
					}
				}
				else
				{
					if (intRateType == 6)
					{
						vsRate.Rows = 1;
						vsRate.AddItem("\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New" + "\t" + "Add New");
						// this sets the height
						if (vsRate.Rows > 1)
						{
							if (vsRate.Rows == 2 || (intRateType == 6 && vsRate.Rows == 3))
							{
								// this only has one rate record, so select it automatically
								boolFormatting = true;
								// this will make sure that it does not automatically go to the rate screen
								vsRate.TextMatrix(1, 0, FCConvert.ToString(-1));
								boolFormatting = false;
							}
						}
					}
					else
					{
						MessageBox.Show("There are no rate records that match the criteria.", "No Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
						//optLien[0].Checked = true;
						cmbLien.Text = "All Records";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Changing RK", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillReportDetailCombo()
		{
			// this will fill the combo box with the different options for the detail level of the report
			cmbReportDetail.Clear();
			cmbReportDetail.AddItem("Account Information Only");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 0);
			cmbReportDetail.AddItem("Show Mortgage Holders");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 1);
			cmbReportDetail.AddItem("Show Mortgage Holders and Address");
			cmbReportDetail.ItemData(cmbReportDetail.NewIndex, 2);
			cmbReportDetail.SelectedIndex = 0;
		}

		private int GetRateKey(ref bool boolType)
		{
			int GetRateKey = 0;
			// this will check the grid and return whatever rate key is selected
			int lngRW;
			for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
			{
				if (Conversion.Val(vsRate.TextMatrix(lngRW, 0)) == -1)
				{
					boolType = FCConvert.CBool(vsRate.TextMatrix(lngRW, lngLienCol) == "L");
					GetRateKey = FCConvert.ToInt32(Math.Round(Conversion.Val(vsRate.TextMatrix(lngRW, lngHiddenCol))));
					break;
				}
			}
			return GetRateKey;
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		private string BuildLabelSQL(ref int intType)
		{
			string BuildLabelSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the SQL statement for this batch of reports
				string strWhereClause = "";
				int intCT;
				clsDRWrapper rsData = new clsDRWrapper();
				string strWS = "";
				if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				for (intCT = 1; intCT <= vsRate.Rows - 1; intCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(intCT, 0)) == -1)
					{
						switch (intType)
						{
							case 12:
							case 22:
								{
									if (Strings.Trim(strWhereClause) == "")
									{
										strWhereClause = " AND (Lien.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol) + " OR Bill.BillingRateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
									}
									else
									{
										strWhereClause += " OR Lien.RateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol) + " OR Bill.BillingRateKey = " + vsRate.TextMatrix(intCT, lngHiddenCol);
									}
									break;
								}
							default:
								{
									if (Strings.Trim(strWhereClause) == "")
									{
										strWhereClause = " AND BillingRateKey IN (" + vsRate.TextMatrix(intCT, lngHiddenCol);
									}
									else
									{
										strWhereClause += ", " + vsRate.TextMatrix(intCT, lngHiddenCol);
									}
									break;
								}
						}
						//end switch
					}
				}
				if (strWhereClause != "")
				{
					strWhereClause += ")";
				}
				if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Account")
				{
					// range of accounts
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " AND AccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND AccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause = " AND AccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " AND AccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// both empty
							strWhereClause = "";
						}
					}
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
				{
					// range of names
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   ' AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
						}
						else
						{
							// first full second empty
							strWhereClause = " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   '";
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
						}
						else
						{
							// both empty
							strWhereClause = "";
						}
					}
				}
				else
				{
				}
				switch (intType)
				{
					case 12:
					case 22:
						{
							strWhereClause += " AND Principal > Lien.PrinPaid ";
							break;
						}
					default:
						{
							strWhereClause += " AND (" + strWS + "PrinOwed) > " + strWS + "PrinPaid ";
							break;
						}
				}

				switch (intType)
				{
					case 10:
					case 20:
						{
							// 30 Day Labels
							// kgk rsData.CreateStoredProcedure "LabelQuery", "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE BillStatus = 'B' AND (" & strWS & "LienProcessStatus = 1 OR " & strWS & "LienProcessStatus = 2) " & strWhereClause, strUTDatabase
							strLabelQuery = "SELECT Bill.*, Bill.ID AS Bill, Master.AccountNumber, p.FullNameLF AS OwnerName FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.OwnerPartyID WHERE BillStatus = 'B' AND (" + strWS + "LienProcessStatus = 1 OR " + strWS + "LienProcessStatus = 2) " + strWhereClause;
							break;
						}
					case 11:
					case 21:
						{
							// Transfer Tax To Lien Labels
							// kgk rsData.CreateStoredProcedure "LabelQuery", "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE BillStatus = 'B' AND (" & strWS & "LienProcessStatus = 3) " & strWhereClause, strUTDatabase
							strLabelQuery = "SELECT Bill.*, Bill.ID AS Bill, Master.AccountNumber, p.FullNameLF AS OwnerName FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.OwnerPartyID WHERE BillStatus = 'B' AND (" + strWS + "LienProcessStatus = 3) " + strWhereClause;
							break;
						}
					case 12:
					case 22:
						{
							// Lien Maturity Labels
							// kgk rsData.CreateStoredProcedure "LabelQuery", "SELECT * FROM Lien INNER JOIN (Bill INNER JOIN Master ON Bill.AccountKey = Master.ID) ON Lien.ID = Bill." & strWS & "LienRecordNumber WHERE BillStatus = 'B' AND (" & strWS & "LienProcessStatus = 5) " & strWhereClause, strUTDatabase
							strLabelQuery = "SELECT Bill.*, Bill.ID AS Bill, Master.AccountNumber, p.FullNameLF AS OwnerName, Lien.RateKey FROM Lien INNER JOIN (Bill INNER JOIN Master ON Bill.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.OwnerPartyID) ON Lien.ID = Bill." + strWS + "LienRecordNumber WHERE BillStatus = 'B' AND (" + strWS + "LienProcessStatus = 5) " + strWhereClause;
							break;
						}
					case 30:
						{
							// Reminder Notice Labels
							// kgk rsData.CreateStoredProcedure "LabelQuery", "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE BillStatus = 'B' " & strWhereClause, strUTDatabase
							strLabelQuery = "SELECT Bill.*, Bill.ID AS Bill, Master.AccountNumber, p.FullNameLF AS OwnerName FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.OwnerPartyID WHERE BillStatus = 'B' " + strWhereClause;
							break;
						}
				}
				//end switch
				// , SUM(" & strWS & "PrinOwed) as TotalPrinOwed, SUM(" & strWS & "TaxOwed) as TotalTaxOwed, SUM(" & strWS & "IntOwed) as TotalIntOwed, SUM(" & strWS & "IntAdded) as TotalIntAdded, SUM(" & strWS & "CostOwed) as TotalCostOwed, SUM(" & strWS & "PrinPaid) as TotalPrinPaid, SUM(" & strWS & "TaxPaid) as TotalTaxPaid, SUM(" & strWS & "IntPaid) as TotalIntPaid, SUM(" & strWS & "CostPaid) as TotalCostPaid
				if (FCConvert.ToDecimal(txtMinimumAmount.Text) > 0)
				{
					BuildLabelSQL = "SELECT TOP(100) PERCENT * FROM (SELECT * FROM (" + strLabelQuery + ") AS qTmp WHERE AccountKey IN (SELECT AccountKey FROM LabelQuery GROUP BY AccountKey HAVING SUM((" + strWS + "PrinOwed + " + strWS + "TaxOwed + " + strWS + "IntOwed - " + strWS + "IntAdded + " + strWS + "CostOwed) - (" + strWS + "PrinPaid + " + strWS + "TaxPaid + " + strWS + "IntPaid + " + strWS + "CostPaid)) >= " + FCConvert.ToString(FCConvert.ToDecimal(txtMinimumAmount.Text)) + "))";
				}
				else
				{
					BuildLabelSQL = "SELECT TOP(100) PERCENT * FROM (" + strLabelQuery + ") AS qTmp";
				}
				return BuildLabelSQL;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Build Label SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			return BuildLabelSQL;
		}

		private void SetToolTips()
		{
			// this procedure will set all of the tool tips
			// for each of the controls on the form
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWriteFCConvert.ToDouble(
		private void ShowRateInfo_18(int cRow, int cCol, int lngRateKey)
		{
			ShowRateInfo(ref cRow, ref cCol, ref lngRateKey);
		}

		private void ShowRateInfo(ref int cRow, ref int cCol, ref int lngRateKey)
		{
			int lngErrCode = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will show a box with the rate information in it
				// as soon as the box loses focus, I will make it disappear
				clsDRWrapper rsRI = new clsDRWrapper();
				clsDRWrapper rsLR = new clsDRWrapper();
				int i;
				int RK;
				int LRN;
				double dblPercent = 0;
				lngErrCode = 1;
				RK = lngRateKey;
				lngErrCode = 2;
				rsRI.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(RK), modExtraModules.strUTDatabase);
				// format grid
				vsRateInfo.Cols = 1;
				vsRateInfo.Cols = 2;
				vsRateInfo.Rows = 10;
				vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				fraRateInfo.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				lngErrCode = 4;
				vsRateInfo.ColWidth(0, FCConvert.ToInt32(vsRateInfo.WidthOriginal * 0.45));
				vsRateInfo.ColWidth(1, FCConvert.ToInt32(vsRateInfo.WidthOriginal * 0.5));
				vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsRateInfo.ExtendLastCol = true;
				vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				lngErrCode = 5;
				if (rsRI.BeginningOfFile() != true && rsRI.EndOfFile() != true)
				{
					// show the first set of information (rate information and period info)--------------
					vsRateInfo.TextMatrix(0, 0, "RateKey");
					vsRateInfo.TextMatrix(1, 0, "Rate Type");
					vsRateInfo.TextMatrix(2, 0, "");
					vsRateInfo.TextMatrix(3, 0, "Creation Date");
					vsRateInfo.TextMatrix(4, 0, "Start Date");
					vsRateInfo.TextMatrix(5, 0, "End Date");
					vsRateInfo.TextMatrix(6, 0, "Interest Start Date");
					// kk trouts-6 03012013  Change Water to Stormwater for Bangor
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						vsRateInfo.TextMatrix(7, 0, "Stormwater Interest Rate");
					}
					else
					{
						vsRateInfo.TextMatrix(7, 0, "Water Interest Rate");
					}
					vsRateInfo.TextMatrix(8, 0, "Sewer Interest Rate");
					vsRateInfo.TextMatrix(9, 0, "Description");
					lngErrCode = 6;
					vsRateInfo.TextMatrix(0, 1, FCConvert.ToString(rsRI.Get_Fields_Int32("ID")));
					lngErrCode = 7;
					vsRateInfo.TextMatrix(1, 1, FCConvert.ToString(rsRI.Get_Fields_String("RateType")));
					lngErrCode = 8;
					vsRateInfo.TextMatrix(2, 1, "");
					lngErrCode = 9;
					vsRateInfo.TextMatrix(3, 1, Strings.Format(rsRI.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy"));
					lngErrCode = 10;
					vsRateInfo.TextMatrix(4, 1, Strings.Format(rsRI.Get_Fields_DateTime("Start"), "MM/dd/yyyy"));
					lngErrCode = 11;
					// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
					vsRateInfo.TextMatrix(5, 1, Strings.Format(rsRI.Get_Fields("End"), "MM/dd/yyyy"));
					lngErrCode = 12;
					vsRateInfo.TextMatrix(6, 1, Strings.Format(rsRI.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy"));
					lngErrCode = 13;
					if (Conversion.Val(rsRI.Get_Fields_Double("WIntRate")) == 0)
					{
						vsRateInfo.TextMatrix(7, 1, "0.00%");
					}
					else
					{
						// MAL@20070904: Change to not round the percent
						if (rsRI.Get_Fields_Double("WIntRate") < 1)
						{
							dblPercent = (rsRI.Get_Fields_Double("WIntRate") * 100);
						}
						else
						{
							dblPercent = rsRI.Get_Fields_Double("WIntRate");
						}
						vsRateInfo.TextMatrix(7, 1, Strings.Format(dblPercent, "#0.00") + "%");
						// .TextMatrix(7, 1) = Format(rsRI.Fields("WIntRate"), "#0.00") & "%"
					}
					lngErrCode = 14;
					if (Conversion.Val(rsRI.Get_Fields_Double("SIntRate")) == 0)
					{
						vsRateInfo.TextMatrix(8, 1, "0.00%");
					}
					else
					{
						// MAL@20070904: Change to not round the percent
						if (rsRI.Get_Fields_Double("SIntRate") < 1)
						{
							dblPercent = (rsRI.Get_Fields_Double("SIntRate") * 100);
						}
						else
						{
							dblPercent = rsRI.Get_Fields_Double("SIntRate");
						}
						vsRateInfo.TextMatrix(8, 1, Strings.Format(dblPercent, "#0.00") + "%");
						// .TextMatrix(8, 1) = Format(rsRI.Fields("SIntRate"), "#0.00") & "%"
					}
					lngErrCode = 15;
					vsRateInfo.TextMatrix(9, 1, Strings.Trim(FCConvert.ToString(rsRI.Get_Fields_String("Description"))));
				}
				// show the frame-----------------------------------------------------
				fraRateInfo.Text = "Rate Information - Rate Key: " + FCConvert.ToString(RK);
				fraRateInfo.Visible = true;
				if (cmdRIClose.Visible)
				{
					cmdRIClose.Focus();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void InitialSettings_2(string strReportType)
		{
			InitialSettings(ref strReportType);
		}

		private void InitialSettings(ref string strReportType)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will reset the LienStatusEligibility field of the bills
				// to the previous and check to see if it is still eligibly for this step
				clsDRWrapper rsInitial = new clsDRWrapper();
				clsDRWrapper rsInitialLien = null;
				clsDRWrapper rsUT = null;
				string strSql;
				string strAccountList;
				string strNoNEligibleAccounts;
				string strBankruptcyAccounts;
				bool boolEligible;
				int lngMaxAccount;
				int lngCount;
				string strTemp = "";
				string strWS = "";
				if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				// Debug.Print Time & " - Start Initial Settings"
				strSql = "";
				strAccountList = "";
				strNoNEligibleAccounts = "";
				strBankruptcyAccounts = "";
				lngMaxAccount = 0;
				boolEligible = false;
				// this will set all of the accounts that are currently eligible back to the last eligibility
				if (Strings.UCase(strReportType) == "30DAYNOTICE")
				{
					strSql = "UPDATE Bill SET " + strWS + "LienStatusEligibility = 0 WHERE BillStatus = 'B' AND " + strWS + "LienProcessStatus = 1 ";
				}
				else if (Strings.UCase(strReportType) == "LIENPROCESS")
				{
					strSql = "UPDATE Bill SET " + strWS + "LienStatusEligibility = 2 WHERE BillStatus = 'B' AND " + strWS + "LienProcessStatus = 3 ";
				}
				else if (Strings.UCase(strReportType) == "LIENMATURITY")
				{
					strSql = "UPDATE Bill SET " + strWS + "LienStatusEligibility = 4 WHERE BillStatus = 'B' AND " + strWS + "LienProcessStatus = 5 ";
				}
				if (rsInitial.Execute(strSql, modExtraModules.strUTDatabase))
				{
					// Check all of the accounts that have a certain eligibility to recalculate that eligibility (check to see if the account was paid off)
					if (Strings.UCase(strReportType) == "30DAYNOTICE")
					{
						// if initial run
						strSql = "SELECT *, (Total" + strWS + "BillAmount) AS Principal FROM Bill WHERE BillStatus = 'B' AND " + strWS + "LienStatusEligibility = 0";
					}
					else if (Strings.UCase(strReportType) == "LIENPROCESS")
					{
						strSql = "SELECT *, (Total" + strWS + "BillAmount) AS Principal FROM Bill WHERE BillStatus = 'B' AND " + strWS + "LienStatusEligibility = 2";
					}
					else if (Strings.UCase(strReportType) == "LIENMATURITY")
					{
						rsInitialLien = new clsDRWrapper();
						rsUT = new clsDRWrapper();
						CreateBillMasterYearQuery();
						// kk 11212013  trouts-44   gstrBillingMasterYear is not initialized
						strSql = "SELECT * FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE " + strWS + "LienStatusEligibility = 4";
						rsInitialLien.OpenRecordset("SELECT * FROM Lien");
						// this will store the Lien Records
						rsUT.OpenRecordset("SELECT * FROM Master", modExtraModules.strUTDatabase);
					}
					// Debug.Print Time & " - Find SQL"
					rsInitial.OpenRecordset(strSql);
					// select all of the accounts that are eligible to have thier eligibility recalculated
					if (rsInitial.EndOfFile() != true)
					{
						lngMaxAccount = rsInitial.RecordCount();
					}
					else
					{
						lngCount = 0;
					}
					lngCount = 0;
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Calculating Eligibility", true, lngMaxAccount, true);
					this.Refresh();
					// Debug.Print Time & " - Open Recordset"
					while (!rsInitial.EndOfFile())
					{
						// cycle through them and find the accounts that are still eligible
						frmWait.InstancePtr.IncrementProgress();
						//Application.DoEvents();
						boolEligible = false;
						if (Strings.UCase(strReportType) == "30DAYNOTICE")
						{
							// check in the Bill to see if there is any outstanding balances
							if (rsInitial.Get_Fields_Double("Principal") - rsInitial.Get_Fields(strWS + "IntAdded") + rsInitial.Get_Fields(strWS + "CostOwed") - rsInitial.Get_Fields(strWS + "CostAdded") > rsInitial.Get_Fields(strWS + "PrinPaid") + rsInitial.Get_Fields(strWS + "IntPaid") + rsInitial.Get_Fields(strWS + "CostPaid"))
							{
								boolEligible = true;
							}
							else
							{
								boolEligible = false;
							}
						}
						else if (Strings.UCase(strReportType) == "LIENPROCESS")
						{
							// check in the Bill to see if there is any outstanding balances
							if (rsInitial.Get_Fields_Double("Principal") - rsInitial.Get_Fields(strWS + "IntAdded") + rsInitial.Get_Fields(strWS + "CostOwed") - rsInitial.Get_Fields(strWS + "CostAdded") > rsInitial.Get_Fields(strWS + "PrinPaid") + rsInitial.Get_Fields(strWS + "IntPaid") + rsInitial.Get_Fields(strWS + "CostPaid"))
							{
								boolEligible = true;
							}
							else
							{
								boolEligible = false;
							}
						}
						else if (Strings.UCase(strReportType) == "LIENMATURITY")
						{
							// check in the LienRec to see if there is any outstanding balances
							if (rsInitialLien.FindFirstRecord("ID", rsInitial.Get_Fields(strWS + "LienRecordNumber")))
							{
								// "Bill." &
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
								if (rsInitialLien.Get_Fields("Principal") + rsInitialLien.Get_Fields("IntAdded") + rsInitialLien.Get_Fields("Costs") + rsInitialLien.Get_Fields("MaturityFee") > rsInitialLien.Get_Fields("PrinPaid") + rsInitialLien.Get_Fields("IntPaid") + rsInitialLien.Get_Fields_Double("CostPaid"))
								{
									// kgk strTemp = "Key = " & rsInitial.Fields("AccountKey")
									// kgk If rsUT.FindFirstRecord(, , strTemp) Then
									if (rsUT.FindFirstRecord("ID", rsInitial.Get_Fields_Int32("AccountKey")))
									{
										// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
										if (modUTStatusPayments.IsAccountTaxAcquired_7(rsUT.Get_Fields("AccountNumber")))
										{
											// check to see if this account is in bankruptcy
											boolEligible = false;
											// if it is then it is not eligible for lien maturity
											// strBankruptcyAccounts = strBankruptcyAccounts & rsInitial.Fields("Account") & ","
											strBankruptcyAccounts += rsInitial.Get_Fields_Int32("ActualAccountNumber") + ",";
										}
										else
										{
											boolEligible = true;
											// if not then it is eligible
										}
									}
									else
									{
										boolEligible = false;
										// if you can not find the account in the RE database, then it is not eligible
									}
								}
								else
								{
									boolEligible = false;
								}
							}
							else
							{
								boolEligible = false;
							}
						}
						if (boolEligible)
						{
							strAccountList += rsInitial.Get_Fields_Int32("ID");
							// add this billkey to the account list
						}
						else
						{
							strNoNEligibleAccounts += rsInitial.Get_Fields_Int32("AccountKey") + ",";
						}
						if (strAccountList.Length > 0)
						{
							if (Strings.Right(strAccountList, 1) == ",")
							{
								strAccountList = Strings.Left(strAccountList, strAccountList.Length - 1);
							}
							if (Strings.UCase(strReportType) == "30DAYNOTICE")
							{
								strSql = "UPDATE Bill SET " + strWS + "LienStatusEligibility = 1 WHERE ID = " + strAccountList;
							}
							else if (Strings.UCase(strReportType) == "LIENPROCESS")
							{
								strSql = "UPDATE Bill SET " + strWS + "LienStatusEligibility = 3 WHERE ID = " + strAccountList;
							}
							else if (Strings.UCase(strReportType) == "LIENMATURITY")
							{
								strSql = "UPDATE Bill SET " + strWS + "LienStatusEligibility = 5 WHERE ID = " + strAccountList;
							}
							// Debug.Print Time & " - Updating Eligibility"
							if (strAccountList != "")
							{
								rsInitial.Execute(strSql, modExtraModules.strUTDatabase);
								// set the account status eligibility
							}
							// Debug.Print Time & " - Updating Eligibility Finished"
							strAccountList = "";
							// this will reset the acocunt list
						}
						// move to the next record
						rsInitial.MoveNext();
					}
					rsInitial.Reset();
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Setting Eligibility");
					// this will take the comma off of the end of the string if there is one
					if (strBankruptcyAccounts.Length > 0)
					{
						strBankruptcyAccounts = Strings.Left(strBankruptcyAccounts, strBankruptcyAccounts.Length - 1);
						frmWait.InstancePtr.Unload();
						// MsgBox "The following is a list of accounts that will not print Lien Maturity Notices due to bankruptcy.", vbInformation, "Bankrupt Accounts"
						// MsgBox "Some accounts will not be processed due to bankruptcy.  Run the 'Print Bankruptcy Report' to see the accounts not eligible for Lien Maturity.", vbInformation, "Bankrupt Accounts"
						// this is where I actually run and save the report, It can be shown later from the Lien Maturity Menu
						// rptBankruptcy.Init "SELECT * FROM Bill INNER JOIN Master ON Bill.Account = Master.RSAccount WHERE BillKey IN (" & strBankruptcyAccounts & ")"
						// rptBankruptcy.Run True
					}
					// If Len(strAccountList) > 0 Then
					// strAccountList = Left$(strAccountList, Len(strAccountList) - 1)
					// 
					// Select Case UCase(strReportType)
					// Case "30DAYNOTICE"
					// strSQL = "UPDATE Bill SET LienStatusEligibility = 1 WHERE BillKey IN (" & strAccountList & ")"
					// Case "LIENPROCESS"
					// strSQL = "UPDATE Bill SET LienStatusEligibility = 3 WHERE BillKey IN (" & strAccountList & ")"
					// Case "LIENMATURITY"
					// strSQL = "UPDATE Bill SET LienStatusEligibility = 5 WHERE BillKey IN (" & strAccountList & ")"
					// End Select
					// 
					// rsInitial.Execute strSQL            'set the account status eligibility
					// End If
					if (strNoNEligibleAccounts.Length > 0)
					{
						strNoNEligibleAccounts = Strings.Left(strNoNEligibleAccounts, strNoNEligibleAccounts.Length - 1);
					}
				}
				else
				{
					MessageBox.Show("An ERROR occured while updating the database.", "Lien Status Eligibility", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Settings", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string RateKeyList(ref string strCommaDelRKList)
		{
			string RateKeyList = "";
			// this function will return the string of rate keys
			string strTemp = "";
			int lngCT;
			for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
			{
				if (Conversion.Val(vsRate.TextMatrix(lngCT, 0)) == -1)
				{
					if (strTemp == "")
					{
						strTemp = "(" + vsRate.TextMatrix(lngCT, lngHiddenCol);
					}
					else
					{
						strTemp += "," + vsRate.TextMatrix(lngCT, lngHiddenCol);
					}
					strCommaDelRKList += vsRate.TextMatrix(lngCT, lngHiddenCol) + ",";
				}
			}
			if (strTemp != "")
			{
				strTemp += ")";
			}
			RateKeyList = strTemp;
			return RateKeyList;
		}

		private void GetRateKeyList()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will get the default rate key list and store it in the strDefaultRateKeyList
				clsDRWrapper rsDRKL = new clsDRWrapper();
				string strWS = "";
				if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				rsDRKL.OpenRecordset("SELECT * FROM " + strWS + "Control_30DayNotice", modExtraModules.strUTDatabase);
				if (!rsDRKL.EndOfFile())
				{
					if (Strings.Trim(FCConvert.ToString(rsDRKL.Get_Fields_String("RateKeyList"))) != "")
					{
						strDefaultRateKeyList = Strings.Trim(FCConvert.ToString(rsDRKL.Get_Fields_String("RateKeyList")));
					}
					else
					{
						strDefaultRateKeyList = "";
					}
				}
				else
				{
					strDefaultRateKeyList = "";
				}
				return;
			}
			catch
			{
				
				strDefaultRateKeyList = "";
			}
		}

		private void SaveRateKeyList()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will get the default rate key list and store it in the strDefaultRateKeyList
				clsDRWrapper rsDRKL = new clsDRWrapper();
				string strTemp = "";
				string strWS = "";
				if (cmbWS.Text == "Water" || cmbWS.Text == "Stormwater")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				// fill the list with a comma delimited string
				RateKeyList(ref strTemp);
				// fill in the control table
				rsDRKL.OpenRecordset("SELECT * FROM " + strWS + "Control_30DayNotice", modExtraModules.strUTDatabase);
				if (!rsDRKL.EndOfFile())
				{
					rsDRKL.Edit();
				}
				else
				{
					rsDRKL.AddNew();
				}
				rsDRKL.Set_Fields("RateKeyList", Strings.Trim(strTemp));
				rsDRKL.Update();
				return;
			}
			catch
			{
				
				strDefaultRateKeyList = "";
			}
		}

		private void CheckDateRanges()
		{
			// Reads through Rate keys to auto (de-)select based on date ranges
			int lngCT;
			DateTime dtBillDateStart = default(DateTime);
			DateTime dtBillDateEnd = default(DateTime);
			DateTime dtDueDateStart = default(DateTime);
			DateTime dtDueDateEnd = default(DateTime);
			if (txtBillingDate[0].Text != "" && Strings.Mid(txtBillingDate[0].Text, 1, 2) != "00")
			{
				dtBillDateStart = DateAndTime.DateValue(txtBillingDate[0].Text);
			}
			if (txtBillingDate[1].Text != "" && Strings.Mid(txtBillingDate[1].Text, 1, 2) != "00")
			{
				dtBillDateEnd = DateAndTime.DateValue(txtBillingDate[1].Text);
			}
			if (txtDueDate[0].Text != "" && Strings.Mid(txtDueDate[0].Text, 1, 2) != "00")
			{
				dtDueDateStart = DateAndTime.DateValue(txtDueDate[0].Text);
			}
			if (txtDueDate[1].Text != "" && Strings.Mid(txtDueDate[1].Text, 1, 2) != "00")
			{
				dtDueDateEnd = DateAndTime.DateValue(txtDueDate[1].Text);
			}
			if (dtBillDateStart.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() && dtBillDateEnd.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
			{
				for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
				{
					if (DateAndTime.DateValue(vsRate.TextMatrix(lngCT, lngBillDateCol)).ToOADate() >= dtBillDateStart.ToOADate() && DateAndTime.DateValue(vsRate.TextMatrix(lngCT, lngBillDateCol)).ToOADate() <= dtBillDateEnd.ToOADate())
					{
						// check date ranges
						vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
					}
					else
					{
						vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(0));
					}
				}
				strDateRanges = "Bill Dates: " + Strings.Format(dtBillDateStart, "MM/dd/yyyy") + " - " + Strings.Format(dtBillDateEnd, "MM/dd/yyyy");
			}
			if (dtDueDateStart.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() && dtDueDateEnd.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
			{
				for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
				{
					if (DateAndTime.DateValue(vsRate.TextMatrix(lngCT, lngDueDate1Col)).ToOADate() >= dtDueDateStart.ToOADate() && DateAndTime.DateValue(vsRate.TextMatrix(lngCT, lngDueDate1Col)).ToOADate() <= dtDueDateEnd.ToOADate())
					{
						// check date ranges
						vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
					}
					else
					{
						vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(0));
					}
				}
				if (!(strDateRanges == ""))
				{
					strDateRanges += " Due Dates: " + Strings.Format(dtDueDateStart, "MM/dd/yyyy") + " - " + Strings.Format(dtDueDateEnd, "MM/dd/yyyy");
				}
				else
				{
					strDateRanges = "Due Dates: " + Strings.Format(dtDueDateStart, "MM/dd/yyyy") + " - " + Strings.Format(dtDueDateEnd, "MM/dd/yyyy");
				}
			}
		}

		private void cmdCancelFilter_Click(object sender, System.EventArgs e)
		{
			fraFilter.Visible = false;
		}

		private void cmdClearFilter_Click(object sender, System.EventArgs e)
		{
			string vbPorterVar = FCConvert.ToString(fraFilter.Tag);
			if (vbPorterVar == "RateKey")
			{
				// Clear the rate key filter values
				lngFilterRateKeyStart = 0;
				lngFilterRateKeyEnd = 0;
			}
			else if (vbPorterVar == "BillDate")
			{
				// Clear the Bill Date filter values
				dtFilterBillDateStart = DateAndTime.DateValue(FCConvert.ToString(0));
				dtFilterBillDateEnd = DateAndTime.DateValue(FCConvert.ToString(0));
			}
			else if (vbPorterVar == "StartDate")
			{
				// Clear the Start Date filter values
				dtFilterStartDateStart = DateAndTime.DateValue(FCConvert.ToString(0));
				dtFilterStartDateEnd = DateAndTime.DateValue(FCConvert.ToString(0));
			}
			else if (vbPorterVar == "IntDate")
			{
				// Clear the Due Date filter values
				dtFilterIntDateStart = DateAndTime.DateValue(FCConvert.ToString(0));
				dtFilterIntDateEnd = DateAndTime.DateValue(FCConvert.ToString(0));
			}
			else if (vbPorterVar == "Descr")
			{
				// Clear the Description filter values
				strFilterDesc = "";
				strFilterDescOption = "";
			}
			ApplyRateFilters();
			fraFilter.Visible = false;
		}

		private void cmdApplyFilter_Click(object sender, System.EventArgs e)
		{
			ApplyFilter();
		}

        private void ApplyFilter()
        {
            bool boolValidFilter;
            boolValidFilter = false;
            if (FCConvert.ToString(fraFilter.Tag) == "RateKey")
            {
                // Validate the Rate Key selections
                if (Strings.Trim(txtFilterStart.Text) == "")
                {
                    MessageBox.Show("Please enter a valid start rate number.", "Invalid Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (FCConvert.ToInt32(FCConvert.ToDouble(txtFilterStart.Text)) <= 0)
                {
                    MessageBox.Show("Please enter a valid start rate number.", "Invalid Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (Strings.Trim(txtFilterEnd.Text) == "")
                {
                    MessageBox.Show("Please enter a valid end rate number.", "Invalid Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (FCConvert.ToInt32(FCConvert.ToDouble(txtFilterEnd.Text)) <= 0)
                {
                    MessageBox.Show("Please enter a valid end rate number.", "Invalid Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (FCConvert.ToInt32(FCConvert.ToDouble(txtFilterStart.Text)) > FCConvert.ToInt32(FCConvert.ToDouble(txtFilterEnd.Text)))
                {
                    MessageBox.Show("End rate number must be larger than start rate number.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    boolValidFilter = true;
                }
            }
            else if (FCConvert.ToString(fraFilter.Tag) == "BillDate" || FCConvert.ToString(fraFilter.Tag) == "StartDate" || FCConvert.ToString(fraFilter.Tag) == "IntDate")
            {
                // Validate the date selections
                if (!txtFilterStartDate.Text.IsDate())
                {
                    MessageBox.Show("Please enter a valid start date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (DateAndTime.DateValue(txtFilterStartDate.Text).ToOADate() == DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
                {
                    MessageBox.Show("Please enter a valid start date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (!txtFilterEndDate.Text.IsDate())
                {
                    MessageBox.Show("Please enter a valid end date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (DateAndTime.DateValue(txtFilterEndDate.Text).ToOADate() == DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
                {
                    MessageBox.Show("Please enter a valid end date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else if (DateAndTime.DateValue(txtFilterStartDate.Text).ToOADate() > DateAndTime.DateValue(txtFilterEndDate.Text).ToOADate())
                {
                    MessageBox.Show("End date must be later than start date.", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    boolValidFilter = true;
                }
            }
            else if (fraFilter.Tag == "Descr")
            {
                // Validate the Description selections
                if (Strings.Trim(txtFilterStart.Text) == "")
                {
                    MessageBox.Show("Please enter a valid filter string.", "Invalid Rate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    boolValidFilter = true;
                }
            }
            if (boolValidFilter)
            {
                string vbPorterVar = FCConvert.ToString(fraFilter.Tag);
                if (vbPorterVar == "RateKey")
                {
                    // Apply the rate key filter values
                    lngFilterRateKeyStart = FCConvert.ToInt32(FCConvert.ToDouble(txtFilterStart.Text));
                    lngFilterRateKeyEnd = FCConvert.ToInt32(FCConvert.ToDouble(txtFilterEnd.Text));
                }
                else if (vbPorterVar == "BillDate")
                {
                    // Apply the Bill Date filter values
                    dtFilterBillDateStart = DateAndTime.DateValue(txtFilterStartDate.Text);
                    dtFilterBillDateEnd = DateAndTime.DateValue(txtFilterEndDate.Text);
                }
                else if (vbPorterVar == "StartDate")
                {
                    // Apply the Start Date filter values
                    dtFilterStartDateStart = DateAndTime.DateValue(txtFilterStartDate.Text);
                    dtFilterStartDateEnd = DateAndTime.DateValue(txtFilterEndDate.Text);
                }
                else if (vbPorterVar == "IntDate")
                {
                    // Apply the Due Date filter values
                    dtFilterIntDateStart = DateAndTime.DateValue(txtFilterStartDate.Text);
                    dtFilterIntDateEnd = DateAndTime.DateValue(txtFilterEndDate.Text);
                }
                else if (vbPorterVar == "Descr")
                {
                    // Clear the Description filter values
                    strFilterDesc = txtFilterStart.Text;
                    if (cmbFilterContains.SelectedIndex == 0)
                    {
                        strFilterDescOption = "C";
                    }
                    else
                    {
                        strFilterDescOption = "N";
                    }
                }
                ApplyRateFilters();
                fraFilter.Visible = false;
            }
        }
		private void ApplyRateFilters()
		{
			int lngCT;
			bool boolRowFiltered = false;
			bool boolUseRateKey;
			bool boolUseBillDate;
			bool boolUseStartDate;
			bool boolUseIntDate;
			bool boolUseDescr;
			int tmpLong = 0;
			DateTime tmpDate;
			string tmpStr = "";
			boolUseRateKey = FCConvert.CBool(lngFilterRateKeyStart > 0 && lngFilterRateKeyEnd > 0);
			boolUseBillDate = FCConvert.CBool(dtFilterBillDateStart.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() && dtFilterBillDateEnd.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate());
			boolUseStartDate = FCConvert.CBool(dtFilterStartDateStart.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() && dtFilterStartDateEnd.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate());
			boolUseIntDate = FCConvert.CBool(dtFilterIntDateStart.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() && dtFilterIntDateEnd.ToOADate() > DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate());
			boolUseDescr = FCConvert.CBool(Strings.Trim(strFilterDesc) != "");
			if (boolUseRateKey)
			{
				vsRate.TextMatrix(0, lngHiddenCol, "Key *");
			}
			else
			{
				vsRate.TextMatrix(0, lngHiddenCol, "Key");
			}
			if (boolUseBillDate)
			{
				vsRate.TextMatrix(0, lngBillDateCol, "Billing Date *");
			}
			else
			{
				vsRate.TextMatrix(0, lngBillDateCol, "Billing Date");
			}
			if (boolUseStartDate)
			{
				vsRate.TextMatrix(0, lngBillingDateCol, "PerStart Date *");
			}
			else
			{
				vsRate.TextMatrix(0, lngBillingDateCol, "PerStart Date");
			}
			if (boolUseIntDate)
			{
				vsRate.TextMatrix(0, lngDueDate1Col, "Int Start Date *");
			}
			else
			{
				vsRate.TextMatrix(0, lngDueDate1Col, "Int Start Date");
			}
			if (boolUseDescr)
			{
				vsRate.TextMatrix(0, lngDescriptionCol, "Description *");
			}
			else
			{
				vsRate.TextMatrix(0, lngDescriptionCol, "Description");
			}
			for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
			{
				boolRowFiltered = false;
				if (boolUseRateKey)
				{
					tmpLong = FCConvert.ToInt32(FCConvert.ToDouble(vsRate.TextMatrix(lngCT, lngHiddenCol)));
					if (tmpLong < lngFilterRateKeyStart || tmpLong > lngFilterRateKeyEnd)
					{
						boolRowFiltered = true;
					}
				}
				if (!boolRowFiltered && boolUseBillDate)
				{
					tmpDate = DateAndTime.DateValue(vsRate.TextMatrix(lngCT, lngBillDateCol));
					if (tmpDate.ToOADate() < dtFilterBillDateStart.ToOADate() || tmpDate.ToOADate() > dtFilterBillDateEnd.ToOADate())
					{
						boolRowFiltered = true;
					}
				}
				if (!boolRowFiltered && boolUseStartDate)
				{
					tmpDate = DateAndTime.DateValue(vsRate.TextMatrix(lngCT, lngBillingDateCol));
					if (tmpDate.ToOADate() < dtFilterStartDateStart.ToOADate() || tmpDate.ToOADate() > dtFilterStartDateEnd.ToOADate())
					{
						boolRowFiltered = true;
					}
				}
				if (!boolRowFiltered && boolUseIntDate)
				{
					tmpDate = DateAndTime.DateValue(vsRate.TextMatrix(lngCT, lngDueDate1Col));
					if (tmpDate.ToOADate() < dtFilterIntDateStart.ToOADate() || tmpDate.ToOADate() > dtFilterIntDateEnd.ToOADate())
					{
						boolRowFiltered = true;
					}
				}
				if (!boolRowFiltered && boolUseDescr)
				{
					tmpStr = vsRate.TextMatrix(lngCT, lngDescriptionCol);
					if (strFilterDescOption == "C")
					{
						// Keep anything that Contains the string
						if (Strings.InStr(1, tmpStr, strFilterDesc, CompareConstants.vbTextCompare) <= 0)
						{
							boolRowFiltered = true;
						}
					}
					else if (strFilterDescOption == "N")
					{
						// Keep anything that Does Not Contain the string
						if (Strings.InStr(1, tmpStr, strFilterDesc, CompareConstants.vbTextCompare) > 0)
						{
							boolRowFiltered = true;
						}
					}
				}
				if (boolRowFiltered)
				{
					// Hide and Uncheck filtered rows
					vsRate.RowHidden(lngCT, true);
					vsRate.TextMatrix(lngCT, 0, FCConvert.ToString(0));
				}
				else
				{
					vsRate.RowHidden(lngCT, false);
					// Unhide non-filtered rows
				}
			}
		}

		private void cmbPrint_SelectedIndexChanged(object sender, System.EventArgs e)
		{

		}

        private void cmdClearAllFilters_Click(object sender, EventArgs e)
        {
            ClearAllFilters();
            ApplyRateFilters();
        }

        private void ClearAllFilters()
        {
            lngFilterRateKeyStart = 0;
            lngFilterRateKeyEnd = 0;
            dtFilterBillDateStart = DateTime.FromOADate(0);
            dtFilterBillDateEnd = DateTime.FromOADate(0);
            dtFilterStartDateStart = DateTime.FromOADate(0);
            dtFilterStartDateEnd = DateTime.FromOADate(0);
            strFilterDesc = "";
            strFilterDescOption = "";
            dtFilterIntDateStart = DateTime.FromOADate(0);
            dtFilterIntDateEnd = DateTime.FromOADate(0);
        }
    }
}
