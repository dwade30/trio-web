//Fecher vbPorter - Version 1.0.0.90

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class clsACHFileInfo
	{

		//=========================================================

		private string strFileName = "";
		private string strEffectiveEntryDate = "";
		private bool boolBalanced;
		private bool boolRegular;
		private bool boolPreNote;
		private bool boolForcePrenote;

		// Private tACHSetup                         As New clsACHSetup
		private clsACHCompanyInfo coInfo = new/*AsNew*/ clsACHCompanyInfo();
		private clsACHInfo destInfo = new/*AsNew*/ clsACHInfo();
		private clsACHOriginatorInfo origInfo = new/*AsNew*/ clsACHOriginatorInfo();

		public bool ForcePreNote
		{
			get
			{
					bool ForcePreNote = false;
				ForcePreNote = boolForcePrenote;
				return ForcePreNote;
			}

			set
			{
				boolForcePrenote = value;
			}
		}



		public bool Regular
		{
			set
			{
				boolRegular = value;
			}

			get
			{
					bool Regular = false;
				Regular = boolRegular;
				return Regular;
			}
		}



		public bool PreNote
		{
			set
			{
				boolPreNote = value;
			}

			get
			{
					bool PreNote = false;
				PreNote = boolPreNote;
				return PreNote;
			}
		}



		public clsACHOriginatorInfo OriginInfo
		{
            get { return origInfo; }
        }

		public clsACHInfo DestinationInfo
        {
            get { return destInfo; }
        }
		

		public clsACHCompanyInfo CompanyInfo
        {
            get { return coInfo; }
        }

		public string FileName
		{
			get
			{
					string FileName = "";
				FileName = strFileName;
				return FileName;
			}

			set
			{
				strFileName = value;
			}
		}



		public string EffectiveEntryDate
		{
			get
			{
					string EffectiveEntryDate = "";
				EffectiveEntryDate = strEffectiveEntryDate;
				return EffectiveEntryDate;
			}

			set
			{
				strEffectiveEntryDate = value;
			}
		}



		public bool Balanced
		{
			get
			{
					bool Balanced = false;
				Balanced = boolBalanced;
				return Balanced;
			}

			set
			{
				boolBalanced = value;
			}
		}



	}
}
