﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTOutstandingBalances.
	/// </summary>
	partial class rptUTOutstandingBalances
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUTOutstandingBalances));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOutstanding = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblSummary1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSummary1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSummaryHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryPaymentType11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryTotal11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumPrin11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumInt11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumCost11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblSumTax1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumTax11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSumHeaderTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOutstanding)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldType,
				this.fldName,
				this.fldBillDate,
				this.fldTaxDue,
				this.fldPaymentReceived,
				this.fldDue,
				this.fldPrincipal,
				this.fldTax,
				this.fldInterest,
				this.fldCosts
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalTaxDue,
				this.fldTotalPaymentReceived,
				this.fldTotalDue,
				this.lblTotals,
				this.lblSummary,
				this.Line1,
				this.lblSummary1,
				this.fldSummary1,
				this.lblSummaryHeader,
				this.lblSummaryPaymentType1,
				this.lblSummaryPaymentType2,
				this.lblSummaryPaymentType3,
				this.lblSummaryPaymentType4,
				this.lblSummaryPaymentType5,
				this.lblSummaryPaymentType6,
				this.lblSummaryPaymentType7,
				this.lblSummaryPaymentType8,
				this.lblSummaryPaymentType9,
				this.lblSummaryPaymentType10,
				this.lblSummaryPaymentType11,
				this.lblSummaryTotal1,
				this.lblSummaryTotal2,
				this.lblSummaryTotal3,
				this.lblSummaryTotal4,
				this.lblSummaryTotal5,
				this.lblSummaryTotal6,
				this.lblSummaryTotal7,
				this.lblSummaryTotal8,
				this.lblSummaryTotal9,
				this.lblSummaryTotal10,
				this.lblSummaryTotal11,
				this.lblSumPrin1,
				this.lblSumPrin2,
				this.lblSumPrin3,
				this.lblSumPrin4,
				this.lblSumPrin5,
				this.lblSumPrin6,
				this.lblSumPrin7,
				this.lblSumPrin8,
				this.lblSumPrin9,
				this.lblSumPrin10,
				this.lblSumPrin11,
				this.lblSumInt1,
				this.lblSumInt2,
				this.lblSumInt3,
				this.lblSumInt4,
				this.lblSumInt5,
				this.lblSumInt6,
				this.lblSumInt7,
				this.lblSumInt8,
				this.lblSumInt9,
				this.lblSumInt10,
				this.lblSumInt11,
				this.lblSumCost1,
				this.lblSumCost2,
				this.lblSumCost3,
				this.lblSumCost4,
				this.lblSumCost5,
				this.lblSumCost6,
				this.lblSumCost7,
				this.lblSumCost8,
				this.lblSumCost9,
				this.lblSumCost10,
				this.lblSumCost11,
				this.lblSumHeaderType,
				this.lblSumHeaderTotal,
				this.lblSumHeaderPrin,
				this.lblSumHeaderInt,
				this.lblSumHeaderCost,
				this.fldTotalPrincipal,
				this.fldTotalTax,
				this.fldTotalInterest,
				this.fldTotalCost,
				this.lnTotals,
				this.lblSumTax1,
				this.lblSumTax2,
				this.lblSumTax3,
				this.lblSumTax4,
				this.lblSumTax5,
				this.lblSumTax6,
				this.lblSumTax7,
				this.lblSumTax8,
				this.lblSumTax9,
				this.lblSumTax10,
				this.lblSumTax11,
				this.lblSumHeaderTax,
				this.lnSummaryTotal,
				this.Line2
			});
			this.ReportFooter.Height = 3.489583F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lnHeader,
				this.lblAccount,
				this.lblBillDate,
				this.lblTaxDue,
				this.lblPaymentReceived,
				this.lblDue,
				this.lblReportType,
				this.lblName,
				this.lblPrincipal,
				this.lblTax,
				this.lblInterest,
				this.lblCost,
				this.lblOutstanding
			});
			this.PageHeader.Height = 1.197917F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Outstanding Status List";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 10.5F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 9F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.5F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 9F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.5F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 1.1875F;
			this.lnHeader.Width = 10.5F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 10.5F;
			this.lnHeader.Y1 = 1.1875F;
			this.lnHeader.Y2 = 1.1875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 1F;
			this.lblAccount.Width = 0.5625F;
			// 
			// lblBillDate
			// 
			this.lblBillDate.Height = 0.1875F;
			this.lblBillDate.HyperLink = null;
			this.lblBillDate.Left = 2.9375F;
			this.lblBillDate.Name = "lblBillDate";
			this.lblBillDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblBillDate.Text = "Bill Date";
			this.lblBillDate.Top = 1F;
			this.lblBillDate.Width = 0.75F;
			// 
			// lblTaxDue
			// 
			this.lblTaxDue.Height = 0.375F;
			this.lblTaxDue.HyperLink = null;
			this.lblTaxDue.Left = 3.9375F;
			this.lblTaxDue.Name = "lblTaxDue";
			this.lblTaxDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTaxDue.Text = "Original Tax";
			this.lblTaxDue.Top = 0.8125F;
			this.lblTaxDue.Width = 0.75F;
			// 
			// lblPaymentReceived
			// 
			this.lblPaymentReceived.Height = 0.375F;
			this.lblPaymentReceived.HyperLink = null;
			this.lblPaymentReceived.Left = 4.75F;
			this.lblPaymentReceived.Name = "lblPaymentReceived";
			this.lblPaymentReceived.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPaymentReceived.Text = "Payment / Adjustments";
			this.lblPaymentReceived.Top = 0.8125F;
			this.lblPaymentReceived.Width = 0.9375F;
			// 
			// lblDue
			// 
			this.lblDue.Height = 0.375F;
			this.lblDue.HyperLink = null;
			this.lblDue.Left = 5.9375F;
			this.lblDue.Name = "lblDue";
			this.lblDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDue.Text = "Amount Due";
			this.lblDue.Top = 0.8125F;
			this.lblDue.Width = 0.75F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.625F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblReportType.Text = "Report Type";
			this.lblReportType.Top = 0.1875F;
			this.lblReportType.Width = 10.5F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.8125F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name ----";
			this.lblName.Top = 1F;
			this.lblName.Width = 1F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 6.75F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 1F;
			this.lblPrincipal.Width = 0.9375F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 7.75F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 1F;
			this.lblTax.Width = 0.9375F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 8.75F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 1F;
			this.lblInterest.Width = 0.9375F;
			// 
			// lblCost
			// 
			this.lblCost.Height = 0.1875F;
			this.lblCost.HyperLink = null;
			this.lblCost.Left = 9.6875F;
			this.lblCost.Name = "lblCost";
			this.lblCost.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblCost.Text = "Costs";
			this.lblCost.Top = 1F;
			this.lblCost.Width = 0.8125F;
			// 
			// lblOutstanding
			// 
			this.lblOutstanding.Height = 0.1875F;
			this.lblOutstanding.HyperLink = null;
			this.lblOutstanding.Left = 6.75F;
			this.lblOutstanding.Name = "lblOutstanding";
			this.lblOutstanding.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblOutstanding.Text = "Outstanding";
			this.lblOutstanding.Top = 0.8125F;
			this.lblOutstanding.Width = 3.75F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.5625F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.1875F;
			this.fldType.Left = 0.5625F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fldType.Text = null;
			this.fldType.Top = 0F;
			this.fldType.Width = 0.25F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.8125F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.125F;
			// 
			// fldBillDate
			// 
			this.fldBillDate.Height = 0.1875F;
			this.fldBillDate.Left = 2.9375F;
			this.fldBillDate.Name = "fldBillDate";
			this.fldBillDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldBillDate.Text = null;
			this.fldBillDate.Top = 0F;
			this.fldBillDate.Width = 0.75F;
			// 
			// fldTaxDue
			// 
			this.fldTaxDue.Height = 0.1875F;
			this.fldTaxDue.Left = 3.6875F;
			this.fldTaxDue.Name = "fldTaxDue";
			this.fldTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTaxDue.Text = "0.00";
			this.fldTaxDue.Top = 0F;
			this.fldTaxDue.Width = 1F;
			// 
			// fldPaymentReceived
			// 
			this.fldPaymentReceived.Height = 0.1875F;
			this.fldPaymentReceived.Left = 4.6875F;
			this.fldPaymentReceived.Name = "fldPaymentReceived";
			this.fldPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPaymentReceived.Text = "0.00";
			this.fldPaymentReceived.Top = 0F;
			this.fldPaymentReceived.Width = 1F;
			// 
			// fldDue
			// 
			this.fldDue.Height = 0.1875F;
			this.fldDue.Left = 5.6875F;
			this.fldDue.Name = "fldDue";
			this.fldDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldDue.Text = "0.00";
			this.fldDue.Top = 0F;
			this.fldDue.Width = 1F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 6.6875F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = "0.00";
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 1F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 7.6875F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTax.Text = "0.00";
			this.fldTax.Top = 0F;
			this.fldTax.Width = 1F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 8.6875F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldInterest.Text = "0.00";
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 1F;
			// 
			// fldCosts
			// 
			this.fldCosts.Height = 0.1875F;
			this.fldCosts.Left = 9.6875F;
			this.fldCosts.Name = "fldCosts";
			this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCosts.Text = "0.00";
			this.fldCosts.Top = 0F;
			this.fldCosts.Width = 0.8125F;
			// 
			// fldTotalTaxDue
			// 
			this.fldTotalTaxDue.Height = 0.1875F;
			this.fldTotalTaxDue.Left = 3.6875F;
			this.fldTotalTaxDue.Name = "fldTotalTaxDue";
			this.fldTotalTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTaxDue.Text = "0.00";
			this.fldTotalTaxDue.Top = 0F;
			this.fldTotalTaxDue.Width = 1F;
			// 
			// fldTotalPaymentReceived
			// 
			this.fldTotalPaymentReceived.Height = 0.1875F;
			this.fldTotalPaymentReceived.Left = 4.6875F;
			this.fldTotalPaymentReceived.Name = "fldTotalPaymentReceived";
			this.fldTotalPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentReceived.Text = "0.00";
			this.fldTotalPaymentReceived.Top = 0F;
			this.fldTotalPaymentReceived.Width = 1F;
			// 
			// fldTotalDue
			// 
			this.fldTotalDue.Height = 0.1875F;
			this.fldTotalDue.Left = 5.6875F;
			this.fldTotalDue.Name = "fldTotalDue";
			this.fldTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalDue.Text = "0.00";
			this.fldTotalDue.Top = 0F;
			this.fldTotalDue.Width = 1F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 2F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0F;
			this.lblTotals.Width = 1.6875F;
			// 
			// lblSummary
			// 
			this.lblSummary.Height = 0.1875F;
			this.lblSummary.HyperLink = null;
			this.lblSummary.Left = 1.75F;
			this.lblSummary.Name = "lblSummary";
			this.lblSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblSummary.Text = "Non Lien Summary";
			this.lblSummary.Top = 2.8125F;
			this.lblSummary.Width = 2F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.75F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 3F;
			this.Line1.Width = 2F;
			this.Line1.X1 = 1.75F;
			this.Line1.X2 = 3.75F;
			this.Line1.Y1 = 3F;
			this.Line1.Y2 = 3F;
			// 
			// lblSummary1
			// 
			this.lblSummary1.Height = 0.1875F;
			this.lblSummary1.HyperLink = null;
			this.lblSummary1.Left = 1.75F;
			this.lblSummary1.Name = "lblSummary1";
			this.lblSummary1.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblSummary1.Text = null;
			this.lblSummary1.Top = 3F;
			this.lblSummary1.Width = 1F;
			// 
			// fldSummary1
			// 
			this.fldSummary1.Height = 0.1875F;
			this.fldSummary1.Left = 2.75F;
			this.fldSummary1.Name = "fldSummary1";
			this.fldSummary1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummary1.Text = "0.00";
			this.fldSummary1.Top = 3F;
			this.fldSummary1.Width = 1F;
			// 
			// lblSummaryHeader
			// 
			this.lblSummaryHeader.Height = 0.1875F;
			this.lblSummaryHeader.HyperLink = null;
			this.lblSummaryHeader.Left = 1.75F;
			this.lblSummaryHeader.Name = "lblSummaryHeader";
			this.lblSummaryHeader.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblSummaryHeader.Text = "Payment Summary";
			this.lblSummaryHeader.Top = 0.25F;
			this.lblSummaryHeader.Width = 7.25F;
			// 
			// lblSummaryPaymentType1
			// 
			this.lblSummaryPaymentType1.Height = 0.1875F;
			this.lblSummaryPaymentType1.HyperLink = null;
			this.lblSummaryPaymentType1.Left = 1.75F;
			this.lblSummaryPaymentType1.MultiLine = false;
			this.lblSummaryPaymentType1.Name = "lblSummaryPaymentType1";
			this.lblSummaryPaymentType1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType1.Text = null;
			this.lblSummaryPaymentType1.Top = 0.625F;
			this.lblSummaryPaymentType1.Width = 2.5F;
			// 
			// lblSummaryPaymentType2
			// 
			this.lblSummaryPaymentType2.Height = 0.1875F;
			this.lblSummaryPaymentType2.HyperLink = null;
			this.lblSummaryPaymentType2.Left = 1.75F;
			this.lblSummaryPaymentType2.MultiLine = false;
			this.lblSummaryPaymentType2.Name = "lblSummaryPaymentType2";
			this.lblSummaryPaymentType2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType2.Text = null;
			this.lblSummaryPaymentType2.Top = 0.8125F;
			this.lblSummaryPaymentType2.Width = 2.5F;
			// 
			// lblSummaryPaymentType3
			// 
			this.lblSummaryPaymentType3.Height = 0.1875F;
			this.lblSummaryPaymentType3.HyperLink = null;
			this.lblSummaryPaymentType3.Left = 1.75F;
			this.lblSummaryPaymentType3.MultiLine = false;
			this.lblSummaryPaymentType3.Name = "lblSummaryPaymentType3";
			this.lblSummaryPaymentType3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType3.Text = null;
			this.lblSummaryPaymentType3.Top = 1F;
			this.lblSummaryPaymentType3.Width = 2.5F;
			// 
			// lblSummaryPaymentType4
			// 
			this.lblSummaryPaymentType4.Height = 0.1875F;
			this.lblSummaryPaymentType4.HyperLink = null;
			this.lblSummaryPaymentType4.Left = 1.75F;
			this.lblSummaryPaymentType4.MultiLine = false;
			this.lblSummaryPaymentType4.Name = "lblSummaryPaymentType4";
			this.lblSummaryPaymentType4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType4.Text = null;
			this.lblSummaryPaymentType4.Top = 1.1875F;
			this.lblSummaryPaymentType4.Width = 2.5F;
			// 
			// lblSummaryPaymentType5
			// 
			this.lblSummaryPaymentType5.Height = 0.1875F;
			this.lblSummaryPaymentType5.HyperLink = null;
			this.lblSummaryPaymentType5.Left = 1.75F;
			this.lblSummaryPaymentType5.MultiLine = false;
			this.lblSummaryPaymentType5.Name = "lblSummaryPaymentType5";
			this.lblSummaryPaymentType5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType5.Text = null;
			this.lblSummaryPaymentType5.Top = 1.375F;
			this.lblSummaryPaymentType5.Width = 2.5F;
			// 
			// lblSummaryPaymentType6
			// 
			this.lblSummaryPaymentType6.Height = 0.1875F;
			this.lblSummaryPaymentType6.HyperLink = null;
			this.lblSummaryPaymentType6.Left = 1.75F;
			this.lblSummaryPaymentType6.MultiLine = false;
			this.lblSummaryPaymentType6.Name = "lblSummaryPaymentType6";
			this.lblSummaryPaymentType6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType6.Text = null;
			this.lblSummaryPaymentType6.Top = 1.5625F;
			this.lblSummaryPaymentType6.Width = 2.5F;
			// 
			// lblSummaryPaymentType7
			// 
			this.lblSummaryPaymentType7.Height = 0.1875F;
			this.lblSummaryPaymentType7.HyperLink = null;
			this.lblSummaryPaymentType7.Left = 1.75F;
			this.lblSummaryPaymentType7.MultiLine = false;
			this.lblSummaryPaymentType7.Name = "lblSummaryPaymentType7";
			this.lblSummaryPaymentType7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType7.Text = null;
			this.lblSummaryPaymentType7.Top = 1.75F;
			this.lblSummaryPaymentType7.Width = 2.5F;
			// 
			// lblSummaryPaymentType8
			// 
			this.lblSummaryPaymentType8.Height = 0.1875F;
			this.lblSummaryPaymentType8.HyperLink = null;
			this.lblSummaryPaymentType8.Left = 1.75F;
			this.lblSummaryPaymentType8.MultiLine = false;
			this.lblSummaryPaymentType8.Name = "lblSummaryPaymentType8";
			this.lblSummaryPaymentType8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType8.Text = null;
			this.lblSummaryPaymentType8.Top = 1.9375F;
			this.lblSummaryPaymentType8.Width = 2.5F;
			// 
			// lblSummaryPaymentType9
			// 
			this.lblSummaryPaymentType9.Height = 0.1875F;
			this.lblSummaryPaymentType9.HyperLink = null;
			this.lblSummaryPaymentType9.Left = 1.75F;
			this.lblSummaryPaymentType9.MultiLine = false;
			this.lblSummaryPaymentType9.Name = "lblSummaryPaymentType9";
			this.lblSummaryPaymentType9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType9.Text = null;
			this.lblSummaryPaymentType9.Top = 2.125F;
			this.lblSummaryPaymentType9.Width = 2.5F;
			// 
			// lblSummaryPaymentType10
			// 
			this.lblSummaryPaymentType10.Height = 0.1875F;
			this.lblSummaryPaymentType10.HyperLink = null;
			this.lblSummaryPaymentType10.Left = 1.75F;
			this.lblSummaryPaymentType10.MultiLine = false;
			this.lblSummaryPaymentType10.Name = "lblSummaryPaymentType10";
			this.lblSummaryPaymentType10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType10.Text = null;
			this.lblSummaryPaymentType10.Top = 2.3125F;
			this.lblSummaryPaymentType10.Width = 2.5F;
			// 
			// lblSummaryPaymentType11
			// 
			this.lblSummaryPaymentType11.Height = 0.1875F;
			this.lblSummaryPaymentType11.HyperLink = null;
			this.lblSummaryPaymentType11.Left = 1.75F;
			this.lblSummaryPaymentType11.MultiLine = false;
			this.lblSummaryPaymentType11.Name = "lblSummaryPaymentType11";
			this.lblSummaryPaymentType11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSummaryPaymentType11.Text = null;
			this.lblSummaryPaymentType11.Top = 2.5F;
			this.lblSummaryPaymentType11.Width = 2.5F;
			// 
			// lblSummaryTotal1
			// 
			this.lblSummaryTotal1.Height = 0.1875F;
			this.lblSummaryTotal1.HyperLink = null;
			this.lblSummaryTotal1.Left = 8F;
			this.lblSummaryTotal1.Name = "lblSummaryTotal1";
			this.lblSummaryTotal1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal1.Text = null;
			this.lblSummaryTotal1.Top = 0.625F;
			this.lblSummaryTotal1.Width = 1F;
			// 
			// lblSummaryTotal2
			// 
			this.lblSummaryTotal2.Height = 0.1875F;
			this.lblSummaryTotal2.HyperLink = null;
			this.lblSummaryTotal2.Left = 8F;
			this.lblSummaryTotal2.Name = "lblSummaryTotal2";
			this.lblSummaryTotal2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal2.Text = null;
			this.lblSummaryTotal2.Top = 0.8125F;
			this.lblSummaryTotal2.Width = 1F;
			// 
			// lblSummaryTotal3
			// 
			this.lblSummaryTotal3.Height = 0.1875F;
			this.lblSummaryTotal3.HyperLink = null;
			this.lblSummaryTotal3.Left = 8F;
			this.lblSummaryTotal3.Name = "lblSummaryTotal3";
			this.lblSummaryTotal3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal3.Text = null;
			this.lblSummaryTotal3.Top = 1F;
			this.lblSummaryTotal3.Width = 1F;
			// 
			// lblSummaryTotal4
			// 
			this.lblSummaryTotal4.Height = 0.1875F;
			this.lblSummaryTotal4.HyperLink = null;
			this.lblSummaryTotal4.Left = 8F;
			this.lblSummaryTotal4.Name = "lblSummaryTotal4";
			this.lblSummaryTotal4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal4.Text = null;
			this.lblSummaryTotal4.Top = 1.1875F;
			this.lblSummaryTotal4.Width = 1F;
			// 
			// lblSummaryTotal5
			// 
			this.lblSummaryTotal5.Height = 0.1875F;
			this.lblSummaryTotal5.HyperLink = null;
			this.lblSummaryTotal5.Left = 8F;
			this.lblSummaryTotal5.Name = "lblSummaryTotal5";
			this.lblSummaryTotal5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal5.Text = null;
			this.lblSummaryTotal5.Top = 1.375F;
			this.lblSummaryTotal5.Width = 1F;
			// 
			// lblSummaryTotal6
			// 
			this.lblSummaryTotal6.Height = 0.1875F;
			this.lblSummaryTotal6.HyperLink = null;
			this.lblSummaryTotal6.Left = 8F;
			this.lblSummaryTotal6.Name = "lblSummaryTotal6";
			this.lblSummaryTotal6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal6.Text = null;
			this.lblSummaryTotal6.Top = 1.5625F;
			this.lblSummaryTotal6.Width = 1F;
			// 
			// lblSummaryTotal7
			// 
			this.lblSummaryTotal7.Height = 0.1875F;
			this.lblSummaryTotal7.HyperLink = null;
			this.lblSummaryTotal7.Left = 8F;
			this.lblSummaryTotal7.Name = "lblSummaryTotal7";
			this.lblSummaryTotal7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal7.Text = null;
			this.lblSummaryTotal7.Top = 1.75F;
			this.lblSummaryTotal7.Width = 1F;
			// 
			// lblSummaryTotal8
			// 
			this.lblSummaryTotal8.Height = 0.1875F;
			this.lblSummaryTotal8.HyperLink = null;
			this.lblSummaryTotal8.Left = 8F;
			this.lblSummaryTotal8.Name = "lblSummaryTotal8";
			this.lblSummaryTotal8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal8.Text = null;
			this.lblSummaryTotal8.Top = 1.9375F;
			this.lblSummaryTotal8.Width = 1F;
			// 
			// lblSummaryTotal9
			// 
			this.lblSummaryTotal9.Height = 0.1875F;
			this.lblSummaryTotal9.HyperLink = null;
			this.lblSummaryTotal9.Left = 8F;
			this.lblSummaryTotal9.Name = "lblSummaryTotal9";
			this.lblSummaryTotal9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal9.Text = null;
			this.lblSummaryTotal9.Top = 2.125F;
			this.lblSummaryTotal9.Width = 1F;
			// 
			// lblSummaryTotal10
			// 
			this.lblSummaryTotal10.Height = 0.1875F;
			this.lblSummaryTotal10.HyperLink = null;
			this.lblSummaryTotal10.Left = 8F;
			this.lblSummaryTotal10.Name = "lblSummaryTotal10";
			this.lblSummaryTotal10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal10.Text = null;
			this.lblSummaryTotal10.Top = 2.3125F;
			this.lblSummaryTotal10.Width = 1F;
			// 
			// lblSummaryTotal11
			// 
			this.lblSummaryTotal11.Height = 0.1875F;
			this.lblSummaryTotal11.HyperLink = null;
			this.lblSummaryTotal11.Left = 8F;
			this.lblSummaryTotal11.Name = "lblSummaryTotal11";
			this.lblSummaryTotal11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSummaryTotal11.Text = null;
			this.lblSummaryTotal11.Top = 2.5F;
			this.lblSummaryTotal11.Width = 1F;
			// 
			// lblSumPrin1
			// 
			this.lblSumPrin1.Height = 0.1875F;
			this.lblSumPrin1.HyperLink = null;
			this.lblSumPrin1.Left = 4.25F;
			this.lblSumPrin1.Name = "lblSumPrin1";
			this.lblSumPrin1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin1.Text = null;
			this.lblSumPrin1.Top = 0.625F;
			this.lblSumPrin1.Width = 1F;
			// 
			// lblSumPrin2
			// 
			this.lblSumPrin2.Height = 0.1875F;
			this.lblSumPrin2.HyperLink = null;
			this.lblSumPrin2.Left = 4.25F;
			this.lblSumPrin2.Name = "lblSumPrin2";
			this.lblSumPrin2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin2.Text = null;
			this.lblSumPrin2.Top = 0.8125F;
			this.lblSumPrin2.Width = 1F;
			// 
			// lblSumPrin3
			// 
			this.lblSumPrin3.Height = 0.1875F;
			this.lblSumPrin3.HyperLink = null;
			this.lblSumPrin3.Left = 4.25F;
			this.lblSumPrin3.Name = "lblSumPrin3";
			this.lblSumPrin3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin3.Text = null;
			this.lblSumPrin3.Top = 1F;
			this.lblSumPrin3.Width = 1F;
			// 
			// lblSumPrin4
			// 
			this.lblSumPrin4.Height = 0.1875F;
			this.lblSumPrin4.HyperLink = null;
			this.lblSumPrin4.Left = 4.25F;
			this.lblSumPrin4.Name = "lblSumPrin4";
			this.lblSumPrin4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin4.Text = null;
			this.lblSumPrin4.Top = 1.1875F;
			this.lblSumPrin4.Width = 1F;
			// 
			// lblSumPrin5
			// 
			this.lblSumPrin5.Height = 0.1875F;
			this.lblSumPrin5.HyperLink = null;
			this.lblSumPrin5.Left = 4.25F;
			this.lblSumPrin5.Name = "lblSumPrin5";
			this.lblSumPrin5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin5.Text = null;
			this.lblSumPrin5.Top = 1.375F;
			this.lblSumPrin5.Width = 1F;
			// 
			// lblSumPrin6
			// 
			this.lblSumPrin6.Height = 0.1875F;
			this.lblSumPrin6.HyperLink = null;
			this.lblSumPrin6.Left = 4.25F;
			this.lblSumPrin6.Name = "lblSumPrin6";
			this.lblSumPrin6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin6.Text = null;
			this.lblSumPrin6.Top = 1.5625F;
			this.lblSumPrin6.Width = 1F;
			// 
			// lblSumPrin7
			// 
			this.lblSumPrin7.Height = 0.1875F;
			this.lblSumPrin7.HyperLink = null;
			this.lblSumPrin7.Left = 4.25F;
			this.lblSumPrin7.Name = "lblSumPrin7";
			this.lblSumPrin7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin7.Text = null;
			this.lblSumPrin7.Top = 1.75F;
			this.lblSumPrin7.Width = 1F;
			// 
			// lblSumPrin8
			// 
			this.lblSumPrin8.Height = 0.1875F;
			this.lblSumPrin8.HyperLink = null;
			this.lblSumPrin8.Left = 4.25F;
			this.lblSumPrin8.Name = "lblSumPrin8";
			this.lblSumPrin8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin8.Text = null;
			this.lblSumPrin8.Top = 1.9375F;
			this.lblSumPrin8.Width = 1F;
			// 
			// lblSumPrin9
			// 
			this.lblSumPrin9.Height = 0.1875F;
			this.lblSumPrin9.HyperLink = null;
			this.lblSumPrin9.Left = 4.25F;
			this.lblSumPrin9.Name = "lblSumPrin9";
			this.lblSumPrin9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin9.Text = null;
			this.lblSumPrin9.Top = 2.125F;
			this.lblSumPrin9.Width = 1F;
			// 
			// lblSumPrin10
			// 
			this.lblSumPrin10.Height = 0.1875F;
			this.lblSumPrin10.HyperLink = null;
			this.lblSumPrin10.Left = 4.25F;
			this.lblSumPrin10.Name = "lblSumPrin10";
			this.lblSumPrin10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin10.Text = null;
			this.lblSumPrin10.Top = 2.3125F;
			this.lblSumPrin10.Width = 1F;
			// 
			// lblSumPrin11
			// 
			this.lblSumPrin11.Height = 0.1875F;
			this.lblSumPrin11.HyperLink = null;
			this.lblSumPrin11.Left = 4.25F;
			this.lblSumPrin11.Name = "lblSumPrin11";
			this.lblSumPrin11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumPrin11.Text = null;
			this.lblSumPrin11.Top = 2.5F;
			this.lblSumPrin11.Width = 1F;
			// 
			// lblSumInt1
			// 
			this.lblSumInt1.Height = 0.1875F;
			this.lblSumInt1.HyperLink = null;
			this.lblSumInt1.Left = 6.25F;
			this.lblSumInt1.Name = "lblSumInt1";
			this.lblSumInt1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt1.Text = null;
			this.lblSumInt1.Top = 0.625F;
			this.lblSumInt1.Width = 1F;
			// 
			// lblSumInt2
			// 
			this.lblSumInt2.Height = 0.1875F;
			this.lblSumInt2.HyperLink = null;
			this.lblSumInt2.Left = 6.25F;
			this.lblSumInt2.Name = "lblSumInt2";
			this.lblSumInt2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt2.Text = null;
			this.lblSumInt2.Top = 0.8125F;
			this.lblSumInt2.Width = 1F;
			// 
			// lblSumInt3
			// 
			this.lblSumInt3.Height = 0.1875F;
			this.lblSumInt3.HyperLink = null;
			this.lblSumInt3.Left = 6.25F;
			this.lblSumInt3.Name = "lblSumInt3";
			this.lblSumInt3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt3.Text = null;
			this.lblSumInt3.Top = 1F;
			this.lblSumInt3.Width = 1F;
			// 
			// lblSumInt4
			// 
			this.lblSumInt4.Height = 0.1875F;
			this.lblSumInt4.HyperLink = null;
			this.lblSumInt4.Left = 6.25F;
			this.lblSumInt4.Name = "lblSumInt4";
			this.lblSumInt4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt4.Text = null;
			this.lblSumInt4.Top = 1.1875F;
			this.lblSumInt4.Width = 1F;
			// 
			// lblSumInt5
			// 
			this.lblSumInt5.Height = 0.1875F;
			this.lblSumInt5.HyperLink = null;
			this.lblSumInt5.Left = 6.25F;
			this.lblSumInt5.Name = "lblSumInt5";
			this.lblSumInt5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt5.Text = null;
			this.lblSumInt5.Top = 1.375F;
			this.lblSumInt5.Width = 1F;
			// 
			// lblSumInt6
			// 
			this.lblSumInt6.Height = 0.1875F;
			this.lblSumInt6.HyperLink = null;
			this.lblSumInt6.Left = 6.25F;
			this.lblSumInt6.Name = "lblSumInt6";
			this.lblSumInt6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt6.Text = null;
			this.lblSumInt6.Top = 1.5625F;
			this.lblSumInt6.Width = 1F;
			// 
			// lblSumInt7
			// 
			this.lblSumInt7.Height = 0.1875F;
			this.lblSumInt7.HyperLink = null;
			this.lblSumInt7.Left = 6.25F;
			this.lblSumInt7.Name = "lblSumInt7";
			this.lblSumInt7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt7.Text = null;
			this.lblSumInt7.Top = 1.75F;
			this.lblSumInt7.Width = 1F;
			// 
			// lblSumInt8
			// 
			this.lblSumInt8.Height = 0.1875F;
			this.lblSumInt8.HyperLink = null;
			this.lblSumInt8.Left = 6.25F;
			this.lblSumInt8.Name = "lblSumInt8";
			this.lblSumInt8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt8.Text = null;
			this.lblSumInt8.Top = 1.9375F;
			this.lblSumInt8.Width = 1F;
			// 
			// lblSumInt9
			// 
			this.lblSumInt9.Height = 0.1875F;
			this.lblSumInt9.HyperLink = null;
			this.lblSumInt9.Left = 6.25F;
			this.lblSumInt9.Name = "lblSumInt9";
			this.lblSumInt9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt9.Text = null;
			this.lblSumInt9.Top = 2.125F;
			this.lblSumInt9.Width = 1F;
			// 
			// lblSumInt10
			// 
			this.lblSumInt10.Height = 0.1875F;
			this.lblSumInt10.HyperLink = null;
			this.lblSumInt10.Left = 6.25F;
			this.lblSumInt10.Name = "lblSumInt10";
			this.lblSumInt10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt10.Text = null;
			this.lblSumInt10.Top = 2.3125F;
			this.lblSumInt10.Width = 1F;
			// 
			// lblSumInt11
			// 
			this.lblSumInt11.Height = 0.1875F;
			this.lblSumInt11.HyperLink = null;
			this.lblSumInt11.Left = 6.25F;
			this.lblSumInt11.Name = "lblSumInt11";
			this.lblSumInt11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumInt11.Text = null;
			this.lblSumInt11.Top = 2.5F;
			this.lblSumInt11.Width = 1F;
			// 
			// lblSumCost1
			// 
			this.lblSumCost1.Height = 0.1875F;
			this.lblSumCost1.HyperLink = null;
			this.lblSumCost1.Left = 7.25F;
			this.lblSumCost1.Name = "lblSumCost1";
			this.lblSumCost1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost1.Text = null;
			this.lblSumCost1.Top = 0.625F;
			this.lblSumCost1.Width = 0.75F;
			// 
			// lblSumCost2
			// 
			this.lblSumCost2.Height = 0.1875F;
			this.lblSumCost2.HyperLink = null;
			this.lblSumCost2.Left = 7.25F;
			this.lblSumCost2.Name = "lblSumCost2";
			this.lblSumCost2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost2.Text = null;
			this.lblSumCost2.Top = 0.8125F;
			this.lblSumCost2.Width = 0.75F;
			// 
			// lblSumCost3
			// 
			this.lblSumCost3.Height = 0.1875F;
			this.lblSumCost3.HyperLink = null;
			this.lblSumCost3.Left = 7.25F;
			this.lblSumCost3.Name = "lblSumCost3";
			this.lblSumCost3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost3.Text = null;
			this.lblSumCost3.Top = 1F;
			this.lblSumCost3.Width = 0.75F;
			// 
			// lblSumCost4
			// 
			this.lblSumCost4.Height = 0.1875F;
			this.lblSumCost4.HyperLink = null;
			this.lblSumCost4.Left = 7.25F;
			this.lblSumCost4.Name = "lblSumCost4";
			this.lblSumCost4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost4.Text = null;
			this.lblSumCost4.Top = 1.1875F;
			this.lblSumCost4.Width = 0.75F;
			// 
			// lblSumCost5
			// 
			this.lblSumCost5.Height = 0.1875F;
			this.lblSumCost5.HyperLink = null;
			this.lblSumCost5.Left = 7.25F;
			this.lblSumCost5.Name = "lblSumCost5";
			this.lblSumCost5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost5.Text = null;
			this.lblSumCost5.Top = 1.375F;
			this.lblSumCost5.Width = 0.75F;
			// 
			// lblSumCost6
			// 
			this.lblSumCost6.Height = 0.1875F;
			this.lblSumCost6.HyperLink = null;
			this.lblSumCost6.Left = 7.25F;
			this.lblSumCost6.Name = "lblSumCost6";
			this.lblSumCost6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost6.Text = null;
			this.lblSumCost6.Top = 1.5625F;
			this.lblSumCost6.Width = 0.75F;
			// 
			// lblSumCost7
			// 
			this.lblSumCost7.Height = 0.1875F;
			this.lblSumCost7.HyperLink = null;
			this.lblSumCost7.Left = 7.25F;
			this.lblSumCost7.Name = "lblSumCost7";
			this.lblSumCost7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost7.Text = null;
			this.lblSumCost7.Top = 1.75F;
			this.lblSumCost7.Width = 0.75F;
			// 
			// lblSumCost8
			// 
			this.lblSumCost8.Height = 0.1875F;
			this.lblSumCost8.HyperLink = null;
			this.lblSumCost8.Left = 7.25F;
			this.lblSumCost8.Name = "lblSumCost8";
			this.lblSumCost8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost8.Text = null;
			this.lblSumCost8.Top = 1.9375F;
			this.lblSumCost8.Width = 0.75F;
			// 
			// lblSumCost9
			// 
			this.lblSumCost9.Height = 0.1875F;
			this.lblSumCost9.HyperLink = null;
			this.lblSumCost9.Left = 7.25F;
			this.lblSumCost9.Name = "lblSumCost9";
			this.lblSumCost9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost9.Text = null;
			this.lblSumCost9.Top = 2.125F;
			this.lblSumCost9.Width = 0.75F;
			// 
			// lblSumCost10
			// 
			this.lblSumCost10.Height = 0.1875F;
			this.lblSumCost10.HyperLink = null;
			this.lblSumCost10.Left = 7.25F;
			this.lblSumCost10.Name = "lblSumCost10";
			this.lblSumCost10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost10.Text = null;
			this.lblSumCost10.Top = 2.3125F;
			this.lblSumCost10.Width = 0.75F;
			// 
			// lblSumCost11
			// 
			this.lblSumCost11.Height = 0.1875F;
			this.lblSumCost11.HyperLink = null;
			this.lblSumCost11.Left = 7.25F;
			this.lblSumCost11.Name = "lblSumCost11";
			this.lblSumCost11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumCost11.Text = null;
			this.lblSumCost11.Top = 2.5F;
			this.lblSumCost11.Width = 0.75F;
			// 
			// lblSumHeaderType
			// 
			this.lblSumHeaderType.Height = 0.1875F;
			this.lblSumHeaderType.HyperLink = null;
			this.lblSumHeaderType.Left = 1.75F;
			this.lblSumHeaderType.MultiLine = false;
			this.lblSumHeaderType.Name = "lblSumHeaderType";
			this.lblSumHeaderType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; white-space: nowrap";
			this.lblSumHeaderType.Text = null;
			this.lblSumHeaderType.Top = 0.4375F;
			this.lblSumHeaderType.Width = 2.5F;
			// 
			// lblSumHeaderTotal
			// 
			this.lblSumHeaderTotal.Height = 0.1875F;
			this.lblSumHeaderTotal.HyperLink = null;
			this.lblSumHeaderTotal.Left = 8F;
			this.lblSumHeaderTotal.Name = "lblSumHeaderTotal";
			this.lblSumHeaderTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderTotal.Text = null;
			this.lblSumHeaderTotal.Top = 0.4375F;
			this.lblSumHeaderTotal.Width = 1F;
			// 
			// lblSumHeaderPrin
			// 
			this.lblSumHeaderPrin.Height = 0.1875F;
			this.lblSumHeaderPrin.HyperLink = null;
			this.lblSumHeaderPrin.Left = 4.25F;
			this.lblSumHeaderPrin.Name = "lblSumHeaderPrin";
			this.lblSumHeaderPrin.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderPrin.Text = null;
			this.lblSumHeaderPrin.Top = 0.4375F;
			this.lblSumHeaderPrin.Width = 1F;
			// 
			// lblSumHeaderInt
			// 
			this.lblSumHeaderInt.Height = 0.1875F;
			this.lblSumHeaderInt.HyperLink = null;
			this.lblSumHeaderInt.Left = 6.25F;
			this.lblSumHeaderInt.Name = "lblSumHeaderInt";
			this.lblSumHeaderInt.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderInt.Text = null;
			this.lblSumHeaderInt.Top = 0.4375F;
			this.lblSumHeaderInt.Width = 1F;
			// 
			// lblSumHeaderCost
			// 
			this.lblSumHeaderCost.Height = 0.1875F;
			this.lblSumHeaderCost.HyperLink = null;
			this.lblSumHeaderCost.Left = 7.25F;
			this.lblSumHeaderCost.Name = "lblSumHeaderCost";
			this.lblSumHeaderCost.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderCost.Text = null;
			this.lblSumHeaderCost.Top = 0.4375F;
			this.lblSumHeaderCost.Width = 0.75F;
			// 
			// fldTotalPrincipal
			// 
			this.fldTotalPrincipal.Height = 0.1875F;
			this.fldTotalPrincipal.Left = 6.6875F;
			this.fldTotalPrincipal.Name = "fldTotalPrincipal";
			this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrincipal.Text = "0.00";
			this.fldTotalPrincipal.Top = 0F;
			this.fldTotalPrincipal.Width = 1F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 7.6875F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTax.Text = "0.00";
			this.fldTotalTax.Top = 0F;
			this.fldTotalTax.Width = 1F;
			// 
			// fldTotalInterest
			// 
			this.fldTotalInterest.Height = 0.1875F;
			this.fldTotalInterest.Left = 8.6875F;
			this.fldTotalInterest.Name = "fldTotalInterest";
			this.fldTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalInterest.Text = "0.00";
			this.fldTotalInterest.Top = 0F;
			this.fldTotalInterest.Width = 1F;
			// 
			// fldTotalCost
			// 
			this.fldTotalCost.Height = 0.1875F;
			this.fldTotalCost.Left = 9.6875F;
			this.fldTotalCost.Name = "fldTotalCost";
			this.fldTotalCost.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCost.Text = "0.00";
			this.fldTotalCost.Top = 0F;
			this.fldTotalCost.Width = 0.8125F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 2.875F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0F;
			this.lnTotals.Width = 7.625F;
			this.lnTotals.X1 = 2.875F;
			this.lnTotals.X2 = 10.5F;
			this.lnTotals.Y1 = 0F;
			this.lnTotals.Y2 = 0F;
			// 
			// lblSumTax1
			// 
			this.lblSumTax1.Height = 0.1875F;
			this.lblSumTax1.HyperLink = null;
			this.lblSumTax1.Left = 5.25F;
			this.lblSumTax1.Name = "lblSumTax1";
			this.lblSumTax1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax1.Text = null;
			this.lblSumTax1.Top = 0.625F;
			this.lblSumTax1.Width = 1F;
			// 
			// lblSumTax2
			// 
			this.lblSumTax2.Height = 0.1875F;
			this.lblSumTax2.HyperLink = null;
			this.lblSumTax2.Left = 5.25F;
			this.lblSumTax2.Name = "lblSumTax2";
			this.lblSumTax2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax2.Text = null;
			this.lblSumTax2.Top = 0.8125F;
			this.lblSumTax2.Width = 1F;
			// 
			// lblSumTax3
			// 
			this.lblSumTax3.Height = 0.1875F;
			this.lblSumTax3.HyperLink = null;
			this.lblSumTax3.Left = 5.25F;
			this.lblSumTax3.Name = "lblSumTax3";
			this.lblSumTax3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax3.Text = null;
			this.lblSumTax3.Top = 1F;
			this.lblSumTax3.Width = 1F;
			// 
			// lblSumTax4
			// 
			this.lblSumTax4.Height = 0.1875F;
			this.lblSumTax4.HyperLink = null;
			this.lblSumTax4.Left = 5.25F;
			this.lblSumTax4.Name = "lblSumTax4";
			this.lblSumTax4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax4.Text = null;
			this.lblSumTax4.Top = 1.1875F;
			this.lblSumTax4.Width = 1F;
			// 
			// lblSumTax5
			// 
			this.lblSumTax5.Height = 0.1875F;
			this.lblSumTax5.HyperLink = null;
			this.lblSumTax5.Left = 5.25F;
			this.lblSumTax5.Name = "lblSumTax5";
			this.lblSumTax5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax5.Text = null;
			this.lblSumTax5.Top = 1.375F;
			this.lblSumTax5.Width = 1F;
			// 
			// lblSumTax6
			// 
			this.lblSumTax6.Height = 0.1875F;
			this.lblSumTax6.HyperLink = null;
			this.lblSumTax6.Left = 5.25F;
			this.lblSumTax6.Name = "lblSumTax6";
			this.lblSumTax6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax6.Text = null;
			this.lblSumTax6.Top = 1.5625F;
			this.lblSumTax6.Width = 1F;
			// 
			// lblSumTax7
			// 
			this.lblSumTax7.Height = 0.1875F;
			this.lblSumTax7.HyperLink = null;
			this.lblSumTax7.Left = 5.25F;
			this.lblSumTax7.Name = "lblSumTax7";
			this.lblSumTax7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax7.Text = null;
			this.lblSumTax7.Top = 1.75F;
			this.lblSumTax7.Width = 1F;
			// 
			// lblSumTax8
			// 
			this.lblSumTax8.Height = 0.1875F;
			this.lblSumTax8.HyperLink = null;
			this.lblSumTax8.Left = 5.25F;
			this.lblSumTax8.Name = "lblSumTax8";
			this.lblSumTax8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax8.Text = null;
			this.lblSumTax8.Top = 1.9375F;
			this.lblSumTax8.Width = 1F;
			// 
			// lblSumTax9
			// 
			this.lblSumTax9.Height = 0.1875F;
			this.lblSumTax9.HyperLink = null;
			this.lblSumTax9.Left = 5.25F;
			this.lblSumTax9.Name = "lblSumTax9";
			this.lblSumTax9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax9.Text = null;
			this.lblSumTax9.Top = 2.125F;
			this.lblSumTax9.Width = 1F;
			// 
			// lblSumTax10
			// 
			this.lblSumTax10.Height = 0.1875F;
			this.lblSumTax10.HyperLink = null;
			this.lblSumTax10.Left = 5.25F;
			this.lblSumTax10.Name = "lblSumTax10";
			this.lblSumTax10.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax10.Text = null;
			this.lblSumTax10.Top = 2.3125F;
			this.lblSumTax10.Width = 1F;
			// 
			// lblSumTax11
			// 
			this.lblSumTax11.Height = 0.1875F;
			this.lblSumTax11.HyperLink = null;
			this.lblSumTax11.Left = 5.25F;
			this.lblSumTax11.Name = "lblSumTax11";
			this.lblSumTax11.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumTax11.Text = null;
			this.lblSumTax11.Top = 2.5F;
			this.lblSumTax11.Width = 1F;
			// 
			// lblSumHeaderTax
			// 
			this.lblSumHeaderTax.Height = 0.1875F;
			this.lblSumHeaderTax.HyperLink = null;
			this.lblSumHeaderTax.Left = 5.25F;
			this.lblSumHeaderTax.Name = "lblSumHeaderTax";
			this.lblSumHeaderTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblSumHeaderTax.Text = null;
			this.lblSumHeaderTax.Top = 0.4375F;
			this.lblSumHeaderTax.Width = 1F;
			// 
			// lnSummaryTotal
			// 
			this.lnSummaryTotal.Height = 0F;
			this.lnSummaryTotal.Left = 4.25F;
			this.lnSummaryTotal.LineWeight = 1F;
			this.lnSummaryTotal.Name = "lnSummaryTotal";
			this.lnSummaryTotal.Top = 2.5F;
			this.lnSummaryTotal.Width = 4.75F;
			this.lnSummaryTotal.X1 = 4.25F;
			this.lnSummaryTotal.X2 = 9F;
			this.lnSummaryTotal.Y1 = 2.5F;
			this.lnSummaryTotal.Y2 = 2.5F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.75F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.4375F;
			this.Line2.Width = 7.25F;
			this.Line2.X1 = 1.75F;
			this.Line2.X2 = 9F;
			this.Line2.Y1 = 0.4375F;
			this.Line2.Y2 = 0.4375F;
			// 
			// rptUTOutstandingBalances
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOutstanding)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummary1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummary1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryPaymentType11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryTotal11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumPrin11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumInt11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumCost11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumTax11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSumHeaderTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummary1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummary1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryPaymentType11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryTotal11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumPrin11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumInt11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumCost11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumTax11;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSumHeaderTax;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSummaryTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOutstanding;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
