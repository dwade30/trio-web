﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmPreviousOwner.
	/// </summary>
	public partial class frmPreviousOwner : BaseForm
	{
		public frmPreviousOwner()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.Label8 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label8.AddControlArrayElement(Label8_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPreviousOwner InstancePtr
		{
			get
			{
				return (frmPreviousOwner)Sys.GetInstance(typeof(frmPreviousOwner));
			}
		}

		protected frmPreviousOwner _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/08/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/07/2006              *
		// ********************************************************
		int lngOwnerID;
		int lngAccount;
		int lngColAccount;
		int lngColName;
		int lngColRecordKey;
		int lngColSaleDate;

		private void frmPreviousOwner_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmPreviousOwner_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPreviousOwner properties;
			//frmPreviousOwner.ScaleWidth	= 6555;
			//frmPreviousOwner.ScaleHeight	= 4950;
			//frmPreviousOwner.LinkTopic	= "Form1";
			//frmPreviousOwner.LockControls	= true;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ErrorHandler
				lngColAccount = 0;
				lngColName = 3;
				lngColRecordKey = 1;
				lngColSaleDate = 2;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				// Label9.ForeColor = TRIOCOLORBLUE
				// lblAccount.ForeColor = TRIOCOLORBLUE
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmPreviousOwner_Resize(object sender, System.EventArgs e)
		{
			SetGridHeight();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intResp As short, int --> As DialogResult
			DialogResult intResp;
			clsDRWrapper clsExecute = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				intResp = MessageBox.Show("This will permanently remove this previous owner information." + "\r\n" + "Do you wish to continue?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intResp == DialogResult.Yes)
				{
					clsExecute.Execute("DELETE FROM PreviousOwner WHERE ID = " + FCConvert.ToString(lngOwnerID), modExtraModules.strUTDatabase);
					this.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In delete");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (fraPreviousOwners.Visible)
				{
					fraPreviousOwners.Visible = false;
					fraEdit.Visible = true;
				}
				lngOwnerID = -2;
				ClearInfo();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In mnuNew_Click", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper clsSave = new clsDRWrapper();
			// vbPorter upgrade warning: dttemp As DateTime	OnWrite(string)
			DateTime dttemp;
			try
			{
				// On Error GoTo ErrorHandler
				if (lngOwnerID == -2)
				{
					clsSave.OpenRecordset("select * from previousowner where id = 0", modExtraModules.strUTDatabase);
				}
				else
				{
					clsSave.OpenRecordset("select * from previousowner where id = " + FCConvert.ToString(lngOwnerID), modExtraModules.strUTDatabase);
				}
				if (!clsSave.EndOfFile())
				{
					if (lngOwnerID != -2)
					{
						clsSave.Edit();
					}
				}
				else
				{
					if (lngAccount <= 0)
					{
						MessageBox.Show("Must have a valid account number to save", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					clsSave.AddNew();
					clsSave.Set_Fields("AccountKey", lngAccount);
					// clsSave.Fields("datecreated") = Format(Date, "MM/dd/yyyy")
					modGlobalFunctions.AddCYAEntry_80("UT", "Manually added a previous owner", "Account : " + FCConvert.ToString(modUTStatusPayments.GetAccountNumber(ref lngAccount)), "AutoID = " + clsSave.Get_Fields_Int32("id"));
				}
				clsSave.Set_Fields("OName", Strings.Trim(txtName.Text));
				clsSave.Set_Fields("OName2", Strings.Trim(txtSecOwner.Text));
				clsSave.Set_Fields("address1", Strings.Trim(txtAddress1.Text));
				clsSave.Set_Fields("address2", Strings.Trim(txtAddress2.Text));
				clsSave.Set_Fields("city", Strings.Trim(txtCity.Text));
				clsSave.Set_Fields("state", Strings.Trim(txtState.Text));
				clsSave.Set_Fields("zip", Strings.Trim(txtZip.Text));
				clsSave.Set_Fields("zip4", Strings.Trim(txtZip4.Text));
				if (Information.IsDate(T2KSaleDate.Text))
				{
					dttemp = FCConvert.ToDateTime(Strings.Format(T2KSaleDate.Text, "MM/dd/yyyy"));
					if (dttemp.ToOADate() == 0)
					{
						clsSave.Set_Fields("saledate", 0);
					}
					else
					{
						clsSave.Set_Fields("saledate", Strings.Format(dttemp, "MM/dd/yyyy"));
					}
				}
				else
				{
					clsSave.Set_Fields("saledate", 0);
				}
				clsSave.Update();
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In save and exit");
			}
		}

		public void Init(ref int lngNewAccount, int lngAutoID = 0)
		{
			try
			{
				// On Error GoTo ErrorHandler
				lngAccount = modUTStatusPayments.GetAccountKeyUT(ref lngNewAccount);
				lblAccount.Text = FCConvert.ToString(lngNewAccount);
				//FC:FINAL:SGA - #i893 - force form Load event
				this.LoadForm();
				if (lngAutoID == 0)
				{
					// if no ID is passed then show all records and let the user select
					lngOwnerID = 0;
					if (FillGrid())
					{
						fraPreviousOwners.Visible = true;
						fraEdit.Visible = false;
					}
					else
					{
						// else adding a new record
						mnuNew_Click();
					}
				}
				else
				{
					lngOwnerID = lngAutoID;
					if (lngOwnerID > 0)
					{
						FillOwnerInfo();
						lngAccount = 0;
						fraEdit.Visible = true;
						fraPreviousOwners.Visible = false;
					}
					else
					{
						// new
						mnuNew_Click();
					}
					//Application.DoEvents();
					if (lngAccount == 0 && lngOwnerID <= 0)
					{
						this.Unload();
					}
					return;
				}
				this.Show(FCForm.FormShowEnum.Modal);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillOwnerInfo()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			DateTime dttemp;
			try
			{
				// On Error GoTo ErrorHandler
				clsLoad.OpenRecordset("select * from previousowner where id = " + FCConvert.ToString(lngOwnerID), modExtraModules.strUTDatabase);
				if (!clsLoad.EndOfFile())
				{
					// lblAccount.Caption = clsLoad.Fields("AccountKey")
					txtName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("OName"));
					txtSecOwner.Text = FCConvert.ToString(clsLoad.Get_Fields_String("OName2"));
					txtAddress1.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Address1"));
					txtAddress2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Address2"));
					txtCity.Text = FCConvert.ToString(clsLoad.Get_Fields_String("City"));
					txtState.Text = FCConvert.ToString(clsLoad.Get_Fields_String("State"));
					txtZip.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Zip"));
					txtZip4.Text = FCConvert.ToString(clsLoad.Get_Fields_String("Zip4"));
					if (Information.IsDate(clsLoad.Get_Fields("SaleDate")))
					{
						dttemp = (DateTime)clsLoad.Get_Fields_DateTime("SaleDate");
						if (dttemp.ToOADate() != 0)
						{
							T2KSaleDate.Text = Strings.Format(dttemp, "MM/dd/yyyy");
						}
					}
					// mebTelephone.Text = clsLoad.Fields("telephone")
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In fill owner info", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ClearInfo()
		{
			txtName.Text = "";
			txtSecOwner.Text = "";
			txtAddress1.Text = "";
			txtAddress2.Text = "";
			txtCity.Text = "";
			txtState.Text = "";
			txtZip.Text = "";
			txtZip4.Text = "";
			T2KSaleDate.Text = "";
		}

		private bool FillGrid()
		{
			bool FillGrid = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with the records from the previousowner table for this account
				clsDRWrapper rsP = new clsDRWrapper();
				FillGrid = true;
				vsGrid.Rows = 1;
				vsGrid.Cols = 4;
				vsGrid.TextMatrix(0, lngColAccount, "Acct");
				vsGrid.TextMatrix(0, lngColName, "Name");
				vsGrid.TextMatrix(0, lngColRecordKey, "Key");
				vsGrid.TextMatrix(0, lngColSaleDate, "Sale Date");
				rsP.OpenRecordset("SELECT * FROM PreviousOwner WHERE AccountKey = " + FCConvert.ToString(lngAccount) + " ORDER BY SaleDate desc", modExtraModules.strUTDatabase);
				if (rsP.EndOfFile())
				{
					FillGrid = false;
					return FillGrid;
				}
				while (!rsP.EndOfFile())
				{
					vsGrid.AddItem("");
					vsGrid.TextMatrix(vsGrid.Rows - 1, lngColAccount, FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsP.Get_Fields_Int32("AccountKey"))));
					vsGrid.TextMatrix(vsGrid.Rows - 1, lngColName, FCConvert.ToString(rsP.Get_Fields_String("OName")));
					vsGrid.TextMatrix(vsGrid.Rows - 1, lngColRecordKey, FCConvert.ToString(rsP.Get_Fields_Int32("ID")));
					vsGrid.TextMatrix(vsGrid.Rows - 1, lngColSaleDate, Strings.Format(rsP.Get_Fields_DateTime("SaleDate"), "MM/dd/yyyy"));
					rsP.MoveNext();
				}
				SetGridHeight();
				return FillGrid;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillGrid;
		}

		private void SetGridHeight()
		{
			// set the height
			int lngHt;
			int lngW;
			lngW = vsGrid.WidthOriginal;
			lngHt = fraPreviousOwners.Height - (2 * vsGrid.Top);
			if (lngHt <= (vsGrid.RowHeight(0) * vsGrid.Rows) + 70)
			{
				vsGrid.Height = lngHt;
			}
			else
			{
				vsGrid.Height = (vsGrid.RowHeight(0) * vsGrid.Rows) + 70;
			}
			vsGrid.ColWidth(lngColAccount, FCConvert.ToInt32(lngW * 0.11));
			vsGrid.ColWidth(lngColName, FCConvert.ToInt32(lngW * 0.63));
			vsGrid.ColWidth(lngColRecordKey, 0);
			vsGrid.ColWidth(lngColSaleDate, FCConvert.ToInt32(lngW * 0.24));
		}

		private void vsGrid_DblClick(object sender, System.EventArgs e)
		{
			// select the previous owner record
			int lngNewAccount = modUTStatusPayments.GetAccountNumber(ref lngAccount);
			this.Init(ref lngNewAccount, FCConvert.ToInt32(Conversion.Val(vsGrid.TextMatrix(vsGrid.Row, lngColRecordKey))));
		}
	}
}
