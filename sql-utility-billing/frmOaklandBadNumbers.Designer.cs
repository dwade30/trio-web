﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmOaklandBadNumbers.
	/// </summary>
	partial class frmOaklandBadNumbers : BaseForm
	{
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCFrame fraBadAccounts;
		public FCGrid vsGrid;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label9;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOaklandBadNumbers));
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.fraBadAccounts = new fecherFoundation.FCFrame();
			this.vsGrid = new fecherFoundation.FCGrid();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBadAccounts)).BeginInit();
			this.fraBadAccounts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 416);
			this.BottomPanel.Size = new System.Drawing.Size(601, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.fraBadAccounts);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Size = new System.Drawing.Size(601, 356);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(601, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(376, 30);
			this.HeaderText.Text = "Add/Rewiew Bad Serial Numbers";
			// 
			// txtAccount
			// 
			this.txtAccount.MaxLength = 15;
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.LinkItem = null;
			this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAccount.LinkTopic = null;
			this.txtAccount.Location = new System.Drawing.Point(240, 30);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(87, 40);
			this.txtAccount.TabIndex = 1;
			// 
			// fraBadAccounts
			// 
			this.fraBadAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraBadAccounts.Controls.Add(this.vsGrid);
			this.fraBadAccounts.Location = new System.Drawing.Point(30, 110);
			this.fraBadAccounts.Name = "fraBadAccounts";
			this.fraBadAccounts.Size = new System.Drawing.Size(523, 229);
			this.fraBadAccounts.TabIndex = 3;
			this.fraBadAccounts.Text = "Bad Serial Numbers";
			this.fraBadAccounts.Visible = false;
			// 
			// vsGrid
			// 
			this.vsGrid.AllowSelection = false;
			this.vsGrid.AllowUserToResizeColumns = false;
			this.vsGrid.AllowUserToResizeRows = false;
			this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrid.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsGrid.ColumnHeadersHeight = 30;
			this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsGrid.DragIcon = null;
			this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrid.ExtendLastCol = true;
			this.vsGrid.FixedCols = 0;
			this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.FrozenCols = 0;
			this.vsGrid.GridColor = System.Drawing.Color.Empty;
			this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.Location = new System.Drawing.Point(20, 30);
			this.vsGrid.Name = "vsGrid";
			this.vsGrid.ReadOnly = true;
			this.vsGrid.RowHeadersVisible = false;
			this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrid.RowHeightMin = 0;
			this.vsGrid.Rows = 1;
			this.vsGrid.ScrollTipText = null;
			this.vsGrid.ShowColumnVisibilityMenu = false;
			this.vsGrid.Size = new System.Drawing.Size(478, 178);
			this.vsGrid.StandardTab = true;
			this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsGrid.TabIndex = 0;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 80);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(208, 23);
			this.Label1.TabIndex = 2;
			this.Label1.Text = "PRESS <ENTER> TO ADD TO LIST";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(30, 44);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(179, 23);
			this.Label9.TabIndex = 0;
			this.Label9.Text = "ENTER SERIAL NUMBER";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuDelete,
				this.mnuSepar2,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 0;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete this Serial Number";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 1;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 2;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(386, 31);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(177, 24);
			this.cmdDelete.TabIndex = 1;
			this.cmdDelete.Text = "Delete this serial number";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// frmOaklandBadNumbers
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(601, 524);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmOaklandBadNumbers";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Add/Review Bad Serial Numbers";
			this.Load += new System.EventHandler(this.frmOaklandBadNumbers_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmOaklandBadNumbers_KeyDown);
			this.Resize += new System.EventHandler(this.frmOaklandBadNumbers_Resize);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraBadAccounts)).EndInit();
			this.fraBadAccounts.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdDelete;
	}
}
