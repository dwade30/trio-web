﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using System.Drawing;
using System.Linq;
using SharedApplication.UtilityBilling.Receipting.Interfaces;
using SharedApplication;
using SharedApplication.AccountGroups;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Enums;
using SharedApplication.UtilityBilling.Receipting;
using TWSharedLibrary;
using DataGridViewCell = Wisej.Web.DataGridViewCell;
using DataGridViewCellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle;
using DataGridViewCellFormattingEventArgs = Wisej.Web.DataGridViewCellFormattingEventArgs;
using DataGridViewCellMouseEventArgs = Wisej.Web.DataGridViewCellMouseEventArgs;
using DataGridViewCellValidatingEventArgs = Wisej.Web.DataGridViewCellValidatingEventArgs;
using DataGridViewRowEventArgs = Wisej.Web.DataGridViewRowEventArgs;
using DialogResult = Wisej.Web.DialogResult;
using KeyEventArgs = Wisej.Web.KeyEventArgs;
using KeyPressEventArgs = Wisej.Web.KeyPressEventArgs;
using Keys = Wisej.Web.Keys;
using MessageBox = Wisej.Web.MessageBox;
using MessageBoxButtons = Wisej.Web.MessageBoxButtons;
using MessageBoxIcon = Wisej.Web.MessageBoxIcon;
using MouseButtons = Wisej.Web.MouseButtons;

namespace Global
{
	/// <summary>
	/// Summary description for frmUBPaymentStatus.
	/// </summary>
	public partial class frmUBPaymentStatus : BaseForm, IView<IUtilityBillingPaymentViewModel>
	{
		private IUtilityBillingPaymentViewModel viewModel { get; set; }
		private GlobalColorSettings globalColorSettings;
		private bool cancelling = true;

		public frmUBPaymentStatus()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

        private void InitializeComponentEx()
		{
            this.imgNote.ImageSource = "imgnote?color=#707884";
			this.cmdDisc = new System.Collections.Generic.List<fecherFoundation.FCButton>();
			this.Label1 = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.Label1.AddControlArrayElement(Label1_8, FCConvert.ToInt16(8));
			this.Label1.AddControlArrayElement(Label1_4, FCConvert.ToInt16(4));
			this.Label1.AddControlArrayElement(Label1_9, FCConvert.ToInt16(9));
			this.Label1.AddControlArrayElement(Label1_11, FCConvert.ToInt16(11));
			this.Label1.AddControlArrayElement(Label1_1, FCConvert.ToInt16(1));
			this.Label1.AddControlArrayElement(Label1_2, FCConvert.ToInt16(2));
			this.Label1.AddControlArrayElement(Label1_10, FCConvert.ToInt16(10));
			this.Label1.AddControlArrayElement(Label1_3, FCConvert.ToInt16(3));
			this.Label1.AddControlArrayElement(Label1_12, FCConvert.ToInt16(12));

			vsRateInfo.RowHeight(-1, 400);
			vsPeriod.RowHeight(-1, 550);
			vsPeriod.CellBorderStyle = DataGridViewCellBorderStyle.None;

            txtWaterTotalTotal.DoubleClick += TxtWaterTotalTotal_DoubleClick;
            txtSewerTotalTotal.DoubleClick += TxtSewerTotalTotal_DoubleClick;
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

        private void TxtSewerTotalTotal_DoubleClick(object sender, EventArgs e)
        {
			if (viewModel.IsPaymentScreen & !viewModel.DisableAutoPaymentInsert)
            {
                AddSewerTotalToPay(txtSewerTotalTotal.Text.ToDecimalValue());
            }
		}

        private void TxtWaterTotalTotal_DoubleClick(object sender, EventArgs e)
        {
			if (viewModel.IsPaymentScreen & !viewModel.DisableAutoPaymentInsert)
            {
                AddWaterTotalToPay(txtWaterTotalTotal.Text.ToDecimalValue());
            }
		}

        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmUBPaymentStatus InstancePtr
		{
			get
			{
				return (frmUBPaymentStatus)Sys.GetInstance(typeof(frmUBPaymentStatus));
			}
		}

		protected frmUBPaymentStatus _InstancePtr = null;

		bool boolDoNotSetService;
		
		public bool blnSBill;
		public int lngGRIDCOLTree;
		public int lngGRIDCOLYear;
		public int lngGRIDColBillNumber;
		public int lngGridColBillKey;
		public int lngGRIDColDate;
		public int lngGRIDColRef;
		public int lngGRIDColService;
		public int lngGRIDColPaymentCode;
		public int lngGRIDColPrincipal;
		public int lngGRIDColPTC;
		public int lngGRIDColTax;
		public int lngGRIDColInterest;
		public int lngGRIDColCosts;
		public int lngGRIDColTotal;
		public int lngGRIDColLineCode;
		public int lngGRIDColPending;
		public int lngGRIDColPaymentKey;
		public int lngGRIDColCHGINTNumber;
		public int lngGRIDColPerDiem;
		public int lngPayGridColBill;
		public int lngPayGridColDate;
		public int lngPayGridColRef;
		public int lngPayGridColService;
		public int lngPayGridColCode;
		public int lngPayGridColCDAC;
		public int lngPayGridColPrincipal;
		public int lngPayGridColTax;
		public int lngPayGridColInterest;
		public int lngPayGridColCosts;
		public int lngPayGridColArrayIndex;
		public int lngPayGridColKeyNumber;
		public int lngPayGridColCHGINTNumber;
		public int lngPayGridColTotal;
		public int OwnerPartyID;

		clsGridAccount clsAcct = new clsGridAccount();
		string strStatusString = "";
		string strPaymentString = "";
		string strSupplemntalBillString;
		string strDemandBillString;

		decimal sumPrin;
		decimal sumTax;
		decimal sumInt;
		decimal sumCost;
		decimal sumTotal;

		private UtilityType currentServiceSelected;

		public frmUBPaymentStatus(IUtilityBillingPaymentViewModel viewModel, GlobalColorSettings globalColorSettings) : this()
		{
			lngGRIDCOLTree = 0;
			lngGRIDCOLYear = 2;
            lngGRIDColBillNumber = 1;
			lngGRIDColDate = 3;
			lngGRIDColRef = 4;
			lngGRIDColService = 5;
			lngGRIDColPaymentCode = 6;
			lngGRIDColPrincipal = 7;
			lngGridColBillKey = 8;
			lngGRIDColPTC = 9;
			lngGRIDColTax = 10;
			lngGRIDColInterest = 11;
			lngGRIDColCosts = 12;
			lngGRIDColTotal = 13;
			lngGRIDColLineCode = 14;
			lngGRIDColPaymentKey = 15;
			lngGRIDColCHGINTNumber = 16;
			lngGRIDColPerDiem = 17;
			lngGRIDColPending = 18;

			lngPayGridColBill = 0;
			lngPayGridColDate = 1;
			lngPayGridColRef = 2;
			lngPayGridColService = 3;
			lngPayGridColCode = 4;
			lngPayGridColCDAC = 5;
			lngPayGridColPrincipal = 6;
			lngPayGridColTax = 7;
			lngPayGridColInterest = 8;
			lngPayGridColCosts = 9;
			lngPayGridColArrayIndex = 10;
			lngPayGridColKeyNumber = 11;
			lngPayGridColCHGINTNumber = 12;
			lngPayGridColTotal = 13;

			this.ViewModel = viewModel;
			this.globalColorSettings = globalColorSettings;

			clsAcct.GRID7Light = txtAcctNumber;
			clsAcct.DefaultAccountType = "G";
			clsAcct.ToolTipText = "Hit Shift-F2 to get a valid accounts list.";

			lblWaterHeading.ForeColor = ColorTranslator.FromOle(globalColorSettings.TRIOCOLORBLUE);
			lblSewerHeading.ForeColor = ColorTranslator.FromOle(globalColorSettings.TRIOCOLORBLUE);

			if (viewModel.ClientName.Left(6).ToUpper() == "BANGOR")
			{
				lblWaterHeading.Text = "Stormwater";
			}

			lblGrpInfo.ForeColor = ColorTranslator.FromOle(globalColorSettings.TRIOCOLORRED);
			
			strStatusString = "Go To Status";
			strPaymentString = "Go To Payments/Adj";
			strSupplemntalBillString = "Lien";
			strDemandBillString = "Dmd";

            SGRID.AddExpandButton();
            WGRID.AddExpandButton();

			txtAcctNumber.ExtendLastCol = true;

			this.Resize += new System.EventHandler(this.frmUBPaymentStatus_Resize);
			viewModel.AccountChanged += ViewModel_AccountChanged;
			viewModel.PendingPaymentAdded += Viewmodel_PendingPaymentAdded;
            Format_PaymentGrid();
           
		}

		private void Viewmodel_PendingPaymentAdded(object sender, UBPayment payment)
		{
			AddPaymentRow(payment);
		}

		private void ViewModel_AccountChanged(object sender, EventArgs e)
		{
			SetupView();
		}

		public IUtilityBillingPaymentViewModel ViewModel
		{
			get => viewModel;
			set => viewModel = value;
		}

		private void StartUp()
		{
			try
			{
                this.Text = "Utility Status";
				WGRID.Rows = 1;
				SGRID.Rows = 1;
				WGRID.Redraw = false;
				SGRID.Redraw = false;
				
				FillInUTInfo();

				var bills = viewModel.Bills(UtilityType.All);

				if (bills.Count > 0)
				{
                    waterGridPanel.Visible = false;
                    sewerGridPanel.Visible = false;
					foreach (var bill in bills)
					{
						Console.WriteLine(bill.BillNumber);
						FillGrid(bill);
					}
				}

				if (WGRID.Rows > 1)
				{
					WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, WGRID.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					Reset_Sum();
					GRID_Coloring(1, WGRID);
					WGRID.Select(0, lngGRIDCOLYear, 0, lngGRIDColTotal);
					WGRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
					Create_Account_Totals(UtilityType.Water);
					WGRID.Redraw = true;
                    waterGridPanel.Visible = true;
					viewModel.VisibleService = UtilityType.Water;
					WGRID.Refresh();
				}

				if (SGRID.Rows > 1)
				{
					SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 3, SGRID.Rows - 1, 3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
					Reset_Sum();
					GRID_Coloring(1, SGRID);
					SGRID.Select(0, lngGRIDCOLYear, 0, lngGRIDColTotal);
					SGRID.CellBorder(Color.Black, 0, 0, 0, 1, 0, 1);
					Create_Account_Totals(UtilityType.Sewer);
					SGRID.Redraw = true;
                    sewerGridPanel.Visible = true;
					if (WGRID.Rows <= 1)
					{
                        viewModel.VisibleService = UtilityType.Sewer;
					}
					else
					{
                        viewModel.VisibleService = UtilityType.All;
					}
					SGRID.Refresh();
				}

				Format_Grids(viewModel.VisibleService);
				SetHeadingLabels();
				Refresh();
				if ((viewModel.GroupInfo?.Comment?.Trim() ?? "") != "")
					MessageBox.Show(viewModel.GroupInfo?.Comment?.Trim(), "Group " + viewModel.GroupInfo?.GroupNumber + " Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

				WGRID.Outline(1);
				SGRID.Outline(1);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.InnerException.ToString());
			}
		}

		public void SetupView()
		{
			if (viewModel.StartedFromCashReceipts || viewModel.IsCashReceiptsActive())
			{
				cmdProcessGoTo.Enabled = false;
				cmdProcessGoTo.Visible = false;
			}

			if (viewModel.Customer == null)
			{
				viewModel.Cancel();
				frmWait.InstancePtr.Unload();
				MessageBox.Show("No billing records exist for this account.", "No Billing Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				Close();
				return;
			}

			StartUp();
            lblAcctComment.Visible = HasAccountComment();
			if (viewModel.IsPaymentScreen)
            {
                BottomPanel.Visible = true;
				FillUBServiceCombo();
				FillUBCodeCombo();
				boolDoNotSetService = true;
				ResetUBPaymentFrame(true);
				boolDoNotSetService = false;
				fraPayment.Visible = true;
				fraStatusLabels.Visible = false;
				Format_PaymentGrid();
				Format_UBvsPeriod();

				if (txtReference.Enabled && txtReference.Visible)
				{
					txtReference.Focus();
				}
				
				cmdSavePayments.Enabled = true;
				cmdPaymentSaveExit.Enabled = true;
			}
			else
			{
				fraPayment.Visible = false;
                BottomPanel.Visible = false;
            }

			if (viewModel.IsPaymentScreen)
			{
				mnuPayment.Visible = true;
				cmdProcessGoTo.Text = strStatusString;
				cmdProcessGoTo.Width = 96;
				vsPayments.Visible = true;
				lblPaymentInfo.Visible = true;

				if (viewModel.NegativeBillsExist(UtilityType.All))
				{
					MessageBox.Show(
						"You have some bills with negative balances.  Press F11 to apply the appropriate entries.",
						"Negative Bill Balances", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				lblPaymentInfo.Visible = false;
				mnuPayment.Visible = false;
				cmdProcessGoTo.Text = strPaymentString;
				cmdProcessGoTo.Width = 142;
			}
       
			frmWait.InstancePtr.Unload();

			if (viewModel.AccountNote != null)
			{
				if (!viewModel.AccountNote.Text.IsNullOrWhiteSpace())
                {
				    if (viewModel.PopUpComment())
				    {
					    MessageBox.Show(
						    "Account " + viewModel.Customer.AccountNumber + " note." + "\r\n" +
						    viewModel.AccountNote.Text.Trim(), "Account Note");
				    }
				    imgNote.Visible = true;
				    mnuFileEditNote.Visible = true;
                }
                else
                {
                    imgNote.Visible = false;
                }
            }
			else
			{
				imgNote.Visible = false;
            }
		}

		private void FillUBServiceCombo()
		{
			UtilityType services = viewModel.AccountServices();

			cmbService.Clear();
			if (services == UtilityType.None)
			{
				services = viewModel.TownServices;
			}

			if (services == UtilityType.All)
			{
				cmbService.AddItem("Both");
				cmbService.AddItem("Water");
				cmbService.AddItem("Sewer");
			}
			else if (services == UtilityType.Water)
			{
				cmbService.AddItem("Water");
				if (viewModel.HasSewerBill())
				{
					cmbService.AddItem("Sewer");
					cmbService.AddItem("Both");
				}
			}
			else if (services == UtilityType.Sewer)
			{
				cmbService.AddItem("Sewer");
				if (viewModel.HasWaterBill())
				{
					cmbService.AddItem("Water");
					cmbService.AddItem("Both");
				}
			}
			else
			{
				cmbService.AddItem("Both");
				cmbService.AddItem("Water");
				cmbService.AddItem("Sewer");
			}
		}

		private void FillUBCodeCombo()
		{
			cmbCode.Clear();
			cmbCode.AddItem("Y   - PrePayment");
			cmbCode.AddItem("A   - Abatement");
			cmbCode.AddItem("C   - Correction");
			cmbCode.AddItem("P   - Regular Payment");
			cmbCode.AddItem("R   - Refunded Abatement");
		}

		public void ResetUBPaymentFrame(bool resetService)
		{
			ResetUBComboBoxes(resetService);

			txtReference.Text = "";
			txtPrincipal.Text = Strings.Format(0, "#,##0.00");
			txtInterest.Text = Strings.Format(0, "#,##0.00");
			txtCosts.Text = Strings.Format(0, "#,##0.00");
			txtTax.Text = Strings.Format(0, "#,##0.00");
			txtComments.Text = "";
			if (!viewModel.ResetTransactionDate)
			{
				txtTransactionDate.Text = Strings.Format(viewModel.EffectiveDate, "MM/dd/yyyy");
				viewModel.ResetTransactionDate = true;
			}

			if (!viewModel.IsCashReceiptsActive() && !viewModel.BudgetaryExists)
			{
				if (!txtCash.Enabled)
				{
					txtCash.Enabled = true;
				}
				txtComments.Focus();
				txtCash.Focus();
				txtCD.Focus();
				txtCash.Enabled = false;
				txtCash.Text = "Y";
				txtCD.Text = "Y";
			}
			else
			{
				if (viewModel.StartedFromCashReceipts)
				{
					txtCD.Text = "Y";
					txtCash.Text = "Y";
				}
				else
				{
					txtCD.Text = "N";
					txtCash.Text = "N";
				}
				txtCash.Enabled = true;
				txtCash.Focus();
				txtComments.Focus();
				txtCash.Enabled = false;
				txtCD.Focus();
			}
		}

		private void SetCodeCombo(string code)
		{
			for (int counter = 0; counter <= cmbCode.Items.Count - 1; counter++)
			{
				if (code == cmbCode.Items[counter].ToString().Left(1))
				{
					cmbCode.SelectedIndex = counter;
					break;
				}
			}
		}

		private void SetBillNumberCombo(string code)
		{
			for (int counter = 0; counter <= cmbBillNumber.Items.Count - 1; counter++)
			{
				if (code == cmbBillNumber.Items[counter].ToString().Replace("*", ""))
				{
					cmbBillNumber.SelectedIndex = counter;
					break;
				}
			}
		}

		public void ResetUBComboBoxes(bool resetService)
		{
			txtCD.Text = "Y";
			txtCash.Text = "Y";

			if (resetService)
			{
				if (cmbService.Items.Count > 0)
				{
					cmbService.SelectedIndex = 0;
				}
			}

			FillUBBillNumber(viewModel.VisibleService);
			if (cmbBillNumber.Items.Count == 2)
			{
				SetBillNumberCombo("0");
				SetCodeCombo("Y");
			}
			else
			{
				SetBillNumberCombo("Auto");
				SetCodeCombo("P");
			}
		}

		public void FillUBBillNumber(UtilityType service)
		{
			cmbBillNumber.Clear();

			foreach (var bill in viewModel.Bills(service))
			{
				if (bill.UtilityDetails.Any(x => !x.IsLiened))
				{
					cmbBillNumber.AddItem(bill.BillNumber.ToString());
				}
			}


			if (!viewModel.PrepaymentExists(service))
			{
				cmbBillNumber.AddItem("0*");
			}

			cmbBillNumber.AddItem("Auto");
			SetBillNumberCombo("Auto");
		}

		public void Format_UBvsPeriod()
		{
			int wid = 0;
			wid = vsPeriod.WidthOriginal;
			vsPeriod.ColWidth(0, FCConvert.ToInt32(wid * 0.34));
			vsPeriod.ColWidth(1, FCConvert.ToInt32(wid * 0.61));
			vsPeriod.ColWidth(2, 0);
			vsPeriod.ColWidth(3, 0);
			vsPeriod.ColWidth(4, 0);
		}

		public bool UTYearCodeValidate()
		{
			if (cmbBillNumber.Items.Count > 0)
			{
				if (cmbBillNumber.Text.Left(4) == "Auto")
				{
					if (cmbCode.Text.Left(1) != "P" && cmbCode.Text.Left(1) != "C")
					{
						return false;
					}
				}
			}

			return true;
		}

		private void cmbCode_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strTemp;

			strTemp = Strings.Left(cmbCode.Text, 1);
			if (UTYearCodeValidate())
			{
				if ((strTemp == "A") || (strTemp == "R"))
				{
					txtCash.Text = "N";
					txtCD.Text = "N";
					txtAcctNumber.TextMatrix(0, 0, viewModel.GetAbatementAccount(strTemp == "W" ? UtilityType.Water : UtilityType.Sewer, cmbBillNumber.Text.Replace("*", "").ToIntegerValue()));
					InputBoxAdjustment(false);
				}
				else if (strTemp == "P")
				{
					txtCash.Text = "Y";
					txtCD.Text = "Y";
					txtAcctNumber.TextMatrix(0, 0, "");
					txtAcctNumber.Enabled = false;
					InputBoxAdjustment(true);
				}
				else if (strTemp == "Y")
				{
					if (Strings.Right(cmbBillNumber.Text, 1) != "*" && Conversion.Val(cmbBillNumber.Text) != 0 && txtReference.Text != "REVERSE")
					{
						SetCodeCombo("P");
					}
					else
					{
						txtCash.Text = "Y";
						txtCD.Text = "Y";
						txtAcctNumber.TextMatrix(0, 0, "");
						txtAcctNumber.Enabled = false;
					}
					InputBoxAdjustment(true);
				}
				else if (strTemp == "C")
				{
					InputBoxAdjustment(false);
				}
			}
			else
			{
				SetCodeCombo("P");
			}
		}

		private void cmbCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);

			switch (KeyCode)
			{
				case Keys.Left:
					{
						cmbService.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
			}
		}


		private void cmbService_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolDoNotSetService)
			{
				FillUBBillNumber(cmbService.Text.Left(1) == "B" ? UtilityType.All : cmbService.Text.Left(1) == "W" ? UtilityType.Water : UtilityType.Sewer);
				if (cmbService.Text.Left(1) != "B")
				{
					if (cmbBillNumber.Items.Count <= 2)
					{
						SetBillNumberCombo("0");
						SetCodeCombo("Y");
					}
					else
					{
						SetBillNumberCombo("Auto");
					}

					cmbBillNumber_Click();
				}
				else
				{
					SetBillNumberCombo("Auto");
					txtCash.Text = "Y";
					//vsPeriod.TextMatrix(0, 1, Strings.Format(FCConvert.ToString(Conversion.Val(WGRID.TextMatrix(WGRID.Rows - 1, lngGRIDColTotal)) + Conversion.Val(SGRID.TextMatrix(SGRID.Rows - 1, lngGRIDColTotal))), "#,##0.00"));
                    vsPeriod.TextMatrix(0, 1,
                        (txtSewerTotalTotal.Text.ToDecimalValue() +
                         txtWaterTotalTotal.Text.ToDecimalValue()).FormatAsCurrencyNoSymbol());
				}
			}
		}

		private void cmbService_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						txtReference.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						cmbCode.Focus();
						KeyCode = (Keys)0;
						break;
					}
			}
		}

		private void cmbBillNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
            if (!viewModel.IsPaymentScreen)
            {
                return;
            }
			
			UtilityType service = FindPaymentType();
			
			if (cmbBillNumber.Text == "Auto")
			{
				if (modUTFunctions.UTYearCodeValidate(cmbBillNumber.Text,cmbCode.Text))
				{
					SetCodeCombo("P");
					vsPeriod.TextMatrix(0, 0, "Total Due:");
					if (cmbService.Text.Left(1) == "B")
					{
						//vsPeriod.TextMatrix(0, 1, Strings.Format(FCConvert.ToString(Conversion.Val(WGRID.TextMatrix(WGRID.Rows - 1, lngGRIDColTotal)) + Conversion.Val(SGRID.TextMatrix(SGRID.Rows - 1, lngGRIDColTotal))), "#,##0.00"));
                        vsPeriod.TextMatrix(0, 1,
                            (txtSewerTotalTotal.Text.ToDecimalValue() +
                            txtWaterTotalTotal.Text.ToDecimalValue()).FormatAsCurrencyNoSymbol());
                    }
					else
					{
						if (service == UtilityType.Water)
						{
							//.TextMatrix(0, 1, Strings.Format(WGRID.TextMatrix(WGRID.Rows - 1, lngGRIDColTotal), "#,##0.00"));
                            vsPeriod.TextMatrix(0, 1, txtWaterTotalTotal.Text.ToDecimalValue().FormatAsCurrencyNoSymbol());
						}
						else
						{
							//vsPeriod.TextMatrix(0, 1, Strings.Format(SGRID.TextMatrix(SGRID.Rows - 1, lngGRIDColTotal), "#,##0.00"));
                            vsPeriod.TextMatrix(0, 1, txtSewerTotalTotal.Text.ToDecimalValue().FormatAsCurrencyNoSymbol());
						}
					}
					vsPeriod.TextMatrix(0, 2, "");
					vsPeriod.TextMatrix(0, 3, "");
					vsPeriod.TextMatrix(0, 4, "");
				}
				else
				{
					cmbBillNumber.SelectedIndex = -1;
				}
			}
			else
			{
				var billNumber = cmbBillNumber.Text.Replace("*", "");
				var bill = viewModel.GetBill(billNumber);

				if (billNumber == "0")
				{
					SetCodeCombo("Y");
				}
				else if (cmbCode.Text == "Y")
				{
					SetCodeCombo("P");
				}

				SetService(service);

				decimal balance = 0;
				if (bill != null)
				{
					var utility = viewModel.GetUtilityDetails(bill, service);

					balance = utility.BalanceDue;
				}

				vsPeriod.TextMatrix(0, 0, "Total Due:");
				vsPeriod.TextMatrix(0, 1, Strings.Format(balance, "#,##0.00"));
			}
		}

		public void cmbBillNumber_Click()
		{
			cmbBillNumber_SelectedIndexChanged(cmbBillNumber, new System.EventArgs());
		}

		private void cmbBillNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
						{
							txtTransactionDate.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Return:
					{
						break;
					}
			}
		}

		private void cmdRIClose_Click(object sender, System.EventArgs e)
		{
			fraRateInfo.Visible = false;
		}

		public void cmdRIClose_Click()
		{
			cmdRIClose_Click(cmdRIClose, new System.EventArgs());
		}


		private void frmUBPaymentStatus_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void frmUBPaymentStatus_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			int lngRows = 0;
			switch (KeyCode)
			{
				case Keys.Return:
					{
						break;
					}
				case Keys.Escape:
					{
						if (fraRateInfo.Visible == true)
						{
							// close the rate info frame
							KeyCode = (Keys)0;
							cmdRIClose_Click();
						}
						else
						{
							KeyCode = (Keys)0;
							CloseForm();
						}
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Insert:
					{
						KeyCode = (Keys)0;
						if (viewModel.DisableAutoPaymentInsert)
						{
							return;
						}
						if (viewModel.IsPaymentScreen)
						{
							AutoInsertAmount();
                            
							//string strSvc = Strings.Left(cmbService.Text, 1);

							//if (cmbBillNumber.Text == "Auto")
							//{
							//	if (strSvc != "B")
							//	{
							//		if (strSvc == "W")
							//		{
							//			lngRows = WGRID.Rows - 1;
							//		}
							//		else
							//		{
							//			lngRows = SGRID.Rows - 1;
							//		}
							//		CreateUTOppositionLine(lngRows, strSvc == "W" ? UtilityType.Water : UtilityType.Sewer);
							//	}
							//	else
							//	{
							//		txtInterest.Text = vsPeriod.TextMatrix(0, 1);
							//	}
							//}
							//else
							//{
							//	if (strSvc == "B")
							//	{
							//		if (viewModel.PayWaterFirst)
							//		{
							//			strSvc = "W";
							//		}
							//		else
							//		{
							//			strSvc = "S";
							//		}
							//	}

							//	if (strSvc == "W")
							//	{
							//		CreateUTOppositionLine(NextSpacerLine(WGRID.Row, WGRID) - 1, UtilityType.Water, false);
							//	}
							//	else
							//	{
							//		CreateUTOppositionLine(NextSpacerLine(SGRID.Row, SGRID) - 1, UtilityType.Sewer, false);
							//	}
							//}
						}
						break;
					}
			}
		}

        public void AutoInsertAmount()
        {
			SetCodeCombo("P");
            txtCD.Text = "Y";
            txtCash.Text = "Y";
            txtReference.Text = "";
            txtComments.Text = "";
            decimal totalAmount = 0;
            var currentServiceType = GetCurrentService();
            if (cmbBillNumber.Text.ToLower() == "auto")
            {
                if (currentServiceType == UtilityType.All || currentServiceType == UtilityType.Sewer)
                {
                    totalAmount += txtSewerTotalTotal.Text.ToDecimalValue();
                }

                if (currentServiceType == UtilityType.All || currentServiceType == UtilityType.Water)
                {
                    totalAmount += txtWaterTotalTotal.Text.ToDecimalValue();
                }
            }
            else
            {
                var billNumber = cmbBillNumber.Text.Replace("*", "");
                var bill = viewModel.GetBill(billNumber);

                switch (currentServiceType)
                {
					case UtilityType.Water:
                        totalAmount = bill.UtilityDetails.Where(d => d.Service == UtilityType.Water)
                            .Sum(d => d.BalanceDue);
                        break;
					case UtilityType.Sewer:
                        totalAmount = bill.UtilityDetails.Where(d => d.Service == UtilityType.Sewer)
                            .Sum(d => d.BalanceDue);
					    break;
					default:
                        totalAmount = bill.UtilityDetails.Sum(d => d.BalanceDue);
					    break;
                }
                
            }

            txtPrincipal.Text =  "0.00";
            txtTax.Text = "0.00";
            txtInterest.Text = totalAmount.FormatAsCurrencyNoSymbol();
            txtCosts.Text = "0.00";
		
        }

        private UtilityType GetCurrentService()
        {
            switch (cmbService.Text.ToLower())
            {
				case "water":
                    return UtilityType.Water;
				case "sewer":
                    return UtilityType.Sewer;
            }

            return UtilityType.All;
        }
		public void CreateUTOppositionLine(int Row, UtilityType service, bool boolReversal = false)
		{
			int Parent = 0;
			int lngBill = 0;
			string strCode = "";
			string strPaymentCode = "";
			string strService = "";
			decimal dblP = 0;
			decimal dblTax = 0;
			decimal dblInt = 0;
			decimal dblCost = 0;

			viewModel.ChargedInterestToReverse = null;

			FCGrid grid = SetGrid(service);
		
			if (Row > 0)
			{
				var payment = viewModel.GetPayment(grid.TextMatrix(Row, lngGRIDColPaymentKey).ToIntegerValue());
				if (boolReversal && payment == null)
				{
					MessageBox.Show("You can't reverse charged interest.  If you need this removed you must reverse the payment record that caused the charged interest to be created.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}

				var boolGrandTotal = grid.RowOutlineLevel(Row) == 0 || (grid.RowOutlineLevel(Row) == 2 && grid.TextMatrix(Row, lngGRIDColRef).Trim() == "Total");
				
				if (boolGrandTotal)
				{
					Parent = Row;
					
					if (Row == grid.Rows - 1)
					{
						SetBillNumberCombo("Auto");
					}
					else
					{
						SetBillNumberCombo(grid.TextMatrix(LastParentRow(Row, grid), lngGRIDColBillNumber).Trim());
					}
					strCode = "=";
					strPaymentCode = "P";
					strService = "A";
				}
				else
				{
					Parent = LastParentRow(Row, grid);

					lngBill = grid.TextMatrix(Parent, lngGRIDColBillNumber).ToIntegerValue();
					SetBillNumberCombo(lngBill.ToString());
					strCode = grid.TextMatrix(Row, lngGRIDColLineCode);
					strPaymentCode = grid.TextMatrix(Row, lngGRIDColPaymentCode);
					strService = grid.TextMatrix(Row, lngGRIDColService);

					if (strService == "")
						strService = "A";
				}

				if (!boolReversal)
				{
					txtCD.Text = "Y";
					txtCash.Text = "Y";
				}
				else
				{
					if (payment != null)
					{
						if (payment.CashDrawer == "Y" && payment.DailyCloseOut == 0)
						{
							txtCD.Text = "Y";
							txtCash.Text = "Y";
						}
						else
						{
							txtCD.Text = "N";
							if (payment.GeneralLedger == "N")
							{
								txtCash.Text = "N";
								txtAcctNumber.TextMatrix(0, 0, payment.BudgetaryAccountNumber);
							}
							else
							{
								txtCash.Text = "Y";
							}
						}

						viewModel.ReversalEffectiveDate = payment.EffectiveInterestDate.Value;
					}
				}

				txtTransactionDate.Text = DateTime.Today.ToString("MM/dd/yyyy");
				if (grid.TextMatrix(Row, lngGRIDColRef).Trim() != "Total" && grid.TextMatrix(Row, lngGRIDColRef).Trim() != "CURINT" && grid.RowOutlineLevel(Row) != 0)
				{
					txtReference.Text = "REVERSE";
				}

				if (strPaymentCode == "P")
				{
					if (boolGrandTotal)
					{
						SetCodeCombo("P");
					}
					else
					{
						SetCodeCombo("C");
					}

					if (!boolGrandTotal)
					{
						if (viewModel.ChargedInterestExistsOnUtility(payment.BillId, service))
						{
							if (viewModel.GetUtilityDetails(lngBill, service).PreviousInterestDate == null)
							{
								MessageBox.Show("This transaction contains no previous interest date information, please correct manually.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}

							if (!viewModel.LatestPaymentOnUtility(payment, service))
							{
								MessageBox.Show(
									"Only the last payment applied can be reversed when interest was charged.", "ERROR",
									MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
						}

						if (payment.ChargedInterestRecord != null)
						{
							viewModel.ChargedInterestToReverse = payment.ChargedInterestRecord;
						}
					}
				}
				else
				{
					SetCodeCombo(strPaymentCode);
				}

				if (strCode == "=" || strCode == "-1")
				{
					dblP = grid.TextMatrix(Row, lngGRIDColPrincipal).ToDecimalValue();
					dblTax = grid.TextMatrix(Row, lngGRIDColTax).ToDecimalValue();
					dblInt = grid.TextMatrix(Row, lngGRIDColInterest).ToDecimalValue();
					dblCost = grid.TextMatrix(Row, lngGRIDColCosts).ToDecimalValue();
					if (strPaymentCode != "C")
					{
						dblInt = dblP + dblInt + dblCost + dblTax;
						dblTax = 0;
						dblP = 0;
						dblCost = 0;
					}
					txtComments.Text = "";
				}
				else
				{
					dblP = grid.TextMatrix(Row, lngGRIDColPrincipal).ToDecimalValue() * -1;
					dblTax = grid.TextMatrix(Row, lngGRIDColTax).ToDecimalValue() * -1;
					dblInt = grid.TextMatrix(Row, lngGRIDColInterest).ToDecimalValue() * -1;
					dblCost = grid.TextMatrix(Row, lngGRIDColCosts).ToDecimalValue() * -1;
					if (strPaymentCode == "Y" && !boolReversal)
					{
						dblInt = dblP + dblInt + dblCost;
						dblP = 0;
						dblCost = 0;
					}
					txtComments.Text = "";
				}
				
				if (strCode == "=")
				{
					if (!boolGrandTotal)
					{
						for (int intCT = 1; intCT <= vsPayments.Rows - 1; intCT++)
						{
							if (vsPayments.TextMatrix(intCT, lngPayGridColBill).Replace("*", "").Replace("-", "") == lngBill.ToString())
							{
								if (vsPayments.TextMatrix(intCT, lngPayGridColCode) == "P")
								{
									dblP -= vsPayments.TextMatrix(intCT, lngPayGridColPrincipal).ToDecimalValue();
									dblTax -= vsPayments.TextMatrix(intCT, lngPayGridColTax).ToDecimalValue();
									dblInt -= vsPayments.TextMatrix(intCT, lngPayGridColInterest).ToDecimalValue();
									dblCost -= vsPayments.TextMatrix(intCT, lngPayGridColCosts).ToDecimalValue();
								}
								else
								{
									dblP += vsPayments.TextMatrix(intCT, lngPayGridColPrincipal).ToDecimalValue();
									dblTax += vsPayments.TextMatrix(intCT, lngPayGridColTax).ToDecimalValue();
									dblInt += vsPayments.TextMatrix(intCT, lngPayGridColInterest).ToDecimalValue();
									dblCost += vsPayments.TextMatrix(intCT, lngPayGridColCosts).ToDecimalValue();
								}
							}
						}
					}
				}

				txtPrincipal.Text = Strings.Format(dblP, "#,##0.00");
				txtTax.Text = Strings.Format(dblTax, "#,##0.00");
				txtInterest.Text = Strings.Format(dblInt, "#,##0.00");
				txtCosts.Text = Strings.Format(dblCost, "#,##0.00");

				if (boolReversal || cmbCode.Text == "Y")
				{
					SavePayment();
					ResetUBPaymentFrame(true);
				}
			}
		}

		private void frmUBPaymentStatus_Resize(object sender, System.EventArgs e)
		{
			Format_Grids(viewModel.VisibleService);
			Format_PaymentGrid();
			if (fraRateInfo.Visible == true)
			{
				fraRateInfo.Left = FCConvert.ToInt32((this.Width - this.fraRateInfo.Width) / 3.0);
				vsRateInfo.SendToBack();
				fraRateInfo.CenterToContainer(this.ClientArea);
			}

			SetHeadingLabels();
		}
		
		private void CreateMasterLine(UBBill bill, UBBillUtilityDetails utility, FCGrid grid)
		{
			try
			{
				DateTime billDate;

				billDate = bill.BillDate ?? DateTime.Today;
				
				Add(0, grid);

				grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentKey, bill.Id);
				grid.TextMatrix(grid.Rows - 1, lngGridColBillKey, bill.Id);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColBillNumber, bill.BillNumber);
				if (bill.BillNumber == 0)
				{
					grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, "No RK");
				}
				else
				{
					grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, Strings.Format(billDate, "MM/dd/yy"));
				}
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPending, " ");

				if (utility.IsIn30DayNoticeStatus)
				{
					grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, "Dmd #" + utility.DemandGroupId);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, grid.TextMatrix(grid.Rows - 1, lngGRIDColDate) + "**");
					grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, grid.Rows - 1, lngGRIDColRef, true);
				}
				else if (bill.IsLien)
				{
					grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, "Lien #" + bill.Id);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, grid.TextMatrix(grid.Rows - 1, lngGRIDColDate) + "*");
					grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, grid.Rows - 1, lngGRIDColRef, true);
				}
				else if (utility.IsLiened)
				{
					grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, "Lien");
				}
				else
				{
					grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, "Original");
				}
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPrincipal, utility.PrincipalOwed);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColTax, utility.TaxOwed);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColInterest, utility.IntOwed);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColCosts, utility.CostOwed);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPTC, utility.PrincipalOwed + utility.CostOwed + utility.TaxOwed);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColTotal, utility.PrincipalOwed + utility.CostOwed + utility.TaxOwed + utility.IntOwed);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPerDiem, utility.PerDiem);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColLineCode, "+");
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Master Line", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillGrid(UBBill bill)
		{
            foreach (var type in bill.UtilityDetails.Select(x => x.Service).Distinct())
			{
                
                var utilityInfo = SetGridAndPanel(type);
                FCGrid grid = utilityInfo.utilityGrid;
                var utilityPanel = utilityInfo.utilityPanel;
				foreach (var utility in bill.UtilityDetails.Where(x => x.Service == type))
				{
					//grid.Visible = true;
                    utilityPanel.Visible = true;
					string nameString = "";
					decimal currentInterest = 0;

					CreateMasterLine(bill, utility, grid);
					var parentLine = grid.Rows - 1;

					if (utility.BillOwner)
					{
						if (bill.Oname2.Trim() != "")
						{
							nameString = bill.Oname.Trim() + " & " + bill.Oname2.Trim();
						}
						else
						{
							nameString = bill.Oname.Trim();
						}
					}
					else
					{
						if (bill.Bname2.Trim() != "")
						{
							nameString = bill.Bname.Trim() + " & " + bill.Bname2.Trim();
						}
						else
						{
							nameString = bill.Bname?.Trim();
						}
					}

					if (Strings.Trim(nameString) != Strings.Trim(lblOwnersName.Text) && bill.BillNumber != 0 && nameString != "")
					{
						Add(1, grid);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, "Billed To:");
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode, nameString);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode + 1, nameString);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode + 2, nameString);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode + 3, nameString);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode + 4, nameString);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode + 5, nameString);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode + 6, nameString);
						grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, grid.Rows - 1, lngGRIDColPaymentCode, grid.Rows - 1, lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						grid.MergeRow(grid.Rows - 1, true, lngGRIDColPaymentCode, lngGRIDColPaymentCode + 6);
						grid.MergeRow(grid.Rows - 1, true, lngGRIDColPaymentCode + 3, lngGRIDColPaymentCode + 6);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPending, " ");
					}

					PaymentRecords(utility, grid);

					currentInterest = utility.CurrentInterest - utility.PendingChargedInterest;
					if (currentInterest != 0)
					{
						Add(1, grid);
						grid.TextMatrix(grid.Rows - 1, lngGRIDCOLYear, "");
						grid.TextMatrix(grid.Rows - 1, lngGRIDColBillNumber, "");
						grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, "");
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPending, " ");
						if (currentInterest < 0)
						{
							grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, "CURINT");
						}
						else
						{
							grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, "EARNINT");
						}
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPrincipal, 0);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColPTC, 0);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColTax, 0);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColInterest, currentInterest);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColCosts, 0);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColTotal, currentInterest);
						grid.TextMatrix(grid.Rows - 1, lngGRIDColLineCode, "-");
					}

					Add(1, grid);
					int currentRow = grid.Rows - 1;
					grid.TextMatrix(currentRow, lngGRIDColPending, " ");
					Add(1, grid);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColPending, " ");
					grid.TextMatrix(currentRow, lngGRIDColLineCode, "=");

					if (utility.IsLiened)
					{
						grid.TextMatrix(currentRow, lngGRIDColRef, "Lien #" + utility.LienId);

						grid.TextMatrix(currentRow, lngGRIDColPrincipal, "0.00");
						grid.TextMatrix(currentRow, lngGRIDColPTC, "0.00");
						grid.TextMatrix(currentRow, lngGRIDColTax, " 0.00");
						grid.TextMatrix(currentRow, lngGRIDColInterest, "  0.00");
						grid.TextMatrix(currentRow, lngGRIDColCosts, "   0.00");
						grid.TextMatrix(currentRow, lngGRIDColTotal, "    0.00");
						grid.TextMatrix(currentRow, lngGRIDColLineCode, "=");

						grid.TextMatrix(currentRow + 1, lngGRIDColPrincipal, "0.00");
						grid.TextMatrix(currentRow + 1, lngGRIDColPTC, "0.00");
						grid.TextMatrix(currentRow + 1, lngGRIDColTax, "0.00");
						grid.TextMatrix(currentRow + 1, lngGRIDColInterest, "0.00");
						grid.TextMatrix(currentRow + 1, lngGRIDColCosts, "0.00");
						grid.TextMatrix(currentRow + 1, lngGRIDColTotal, "0.00");
						grid.TextMatrix(currentRow + 1, lngGRIDColDate, grid.TextMatrix(parentLine, lngGRIDColDate));
						grid.TextMatrix(currentRow + 1, lngGRIDColPaymentKey, grid.TextMatrix(parentLine, lngGRIDColPaymentKey));
						
						SwapRows(parentLine, NextSpacerLine(parentLine, grid), grid);
					}
					else
					{
						grid.TextMatrix(currentRow, lngGRIDColRef, "Total");
						
						int l = LastParentRow(currentRow, grid);
						decimal sum = 0;

						for (int j = lngGRIDColPrincipal; j <= lngGRIDColTotal; j++)
						{
							sum = 0;
							for (int sumRow = l; sumRow <= currentRow - 1; sumRow++)
							{
								if (grid.TextMatrix(sumRow, j).Trim() != "")
								{
									if (grid.TextMatrix(sumRow, lngGRIDColLineCode) == "+")
									{
										sum = Math.Round(sum + grid.TextMatrix(sumRow, j).ToDecimalValue(), 2, MidpointRounding.AwayFromZero);
									}
									else if (grid.TextMatrix(sumRow, lngGRIDColLineCode) == "-")
									{
										sum = Math.Round(sum - grid.TextMatrix(sumRow, j).ToDecimalValue(), 2, MidpointRounding.AwayFromZero);
									}
									else if (FCConvert.ToBoolean(grid.TextMatrix(sumRow, lngGRIDColLineCode)) == true)
									{
										sum = Math.Round(grid.TextMatrix(sumRow, j).ToDecimalValue(), 2, MidpointRounding.AwayFromZero);
									}
								}
							}
							grid.TextMatrix(currentRow, j, FCConvert.ToString(sum));
							grid.TextMatrix(currentRow + 1, j, FCConvert.ToString(sum));
						}

						if (bill.BillNumber == 0)
						{
							grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, "No RK");
						}
						else
						{
							grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, Strings.Format(bill.BillDate ?? DateTime.Today, "MM/dd/yy"));
						}

						if (bill.IsLien)
						{
							grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentKey, FCConvert.ToString(bill.Id * -1));
							grid.TextMatrix(grid.Rows - 1, lngGridColBillKey, FCConvert.ToString(bill.Id * -1));
						}
						else
						{
							grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentKey, FCConvert.ToString(bill.Id));
							grid.TextMatrix(grid.Rows - 1, lngGridColBillKey, FCConvert.ToString(bill.Id));
						}
						
						if (grid.TextMatrix(l, lngGRIDColRef) == "Original" || grid.TextMatrix(l, lngGRIDColRef).Left(3) == strDemandBillString.Left(3) || grid.TextMatrix(l, lngGRIDColRef).Left(4) == strSupplemntalBillString.Left(4))
						{
							SwapRows(l, NextSpacerLine(l, grid), grid);
						}
					}
				}
            }

			//if (SGRID.Visible)
            if (sewerGridPanel.Visible)
			{
				SGRID.Select(0, 0);
				SGRID.Refresh();
			}

			if (WGRID.Visible)
			{
				WGRID.Select(0, 0);
				WGRID.Refresh();
			}
		}

		private void PaymentRecords(UBBillUtilityDetails utility, FCGrid grid)
        {
            var payments = viewModel.UtilityPayments(utility);
			foreach (var payment in payments)
			{
				Add(1, grid);

				grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, payment.RecordedTransactionDate.Value.FormatAndPadShortDate());
				grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, payment.Reference);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColService, utility.ServiceCode);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode, payment.Code);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPrincipal, payment.Principal);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColTax, payment.Tax);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColInterest, payment.InterestTotal);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColCosts, payment.Cost);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPTC, payment.Principal + payment.Tax + payment.Cost);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColTotal, payment.PaymentTotal);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColLineCode, "-");
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentKey, payment.Id);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColCHGINTNumber, payment.ChargedInterestRecord?.Id ?? 0);
				grid.TextMatrix(grid.Rows - 1, lngGRIDColPending, payment.Pending ? "*" : " ");

				if (payment.ChargedInterestRecord != null)
				{
					Add(1, grid);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColDate, payment.ChargedInterestRecord.RecordedTransactionDate.Value.FormatAndPadShortDate());
					grid.TextMatrix(grid.Rows - 1, lngGRIDColRef, payment.ChargedInterestRecord.Reference);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColService, utility.ServiceCode);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentCode, payment.ChargedInterestRecord.Code);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColPrincipal, payment.ChargedInterestRecord.Principal);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColTax, payment.ChargedInterestRecord.Tax);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColInterest, payment.ChargedInterestRecord.InterestTotal);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColCosts, payment.ChargedInterestRecord.Cost);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColPTC, payment.ChargedInterestRecord.Principal + payment.ChargedInterestRecord.Tax + payment.ChargedInterestRecord.Cost);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColTotal, payment.ChargedInterestRecord.PaymentTotal);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColLineCode, "-");
					grid.TextMatrix(grid.Rows - 1, lngGRIDColPaymentKey, payment.ChargedInterestRecord.Id);
					grid.TextMatrix(grid.Rows - 1, lngGRIDColCHGINTNumber, "0");
					grid.TextMatrix(grid.Rows - 1, lngGRIDColPending, payment.ChargedInterestRecord.Pending ? "*" : " ");
				}
			}
		}

		private void FormatGrid(bool isWide, UtilityType service)
		{
			
            var utilityInfo = SetGridAndPanel(service);
            var grid = utilityInfo.utilityGrid;
            var utilityPanel = utilityInfo.utilityPanel;
			if (isWide)
			{
                utilityPanel.Width = (this.Width * .94).ToInteger();
                utilityPanel.Left = 30;
            }
			else
			{
                utilityPanel.Width = (ClientArea.Width * .47).ToInteger();
				if (service == UtilityType.Water)
				{
                    utilityPanel.Left = (((ClientArea.Width / 2.0) - utilityPanel.Width) / 2).ToInteger();
                }
				else
				{
                    utilityPanel.Left = ((((ClientArea.Width / 2.0) - utilityPanel.Width) / 2) + (ClientArea.Width / 2.0))
                        .ToInteger();
                }
			}

			grid.Columns[lngGRIDColTotal - 1].DefaultCellStyle.ForeColor = Color.FromArgb(5, 204, 71);
			grid.Columns[lngGRIDColTotal - 1].DefaultCellStyle.Font = new Font("default", 16F, FontStyle.Bold, GraphicsUnit.Pixel);

			grid.Cols = lngGRIDColPending + 1;
			if (viewModel.IsPaymentScreen)
			{
				fraPayment.Visible = true;
				fraStatusLabels.Visible = false;
                utilityPanel.Top = lblPaymentInfo.Top + lblPaymentInfo.Height + lblWaterHeading.Height;
            }
			else
			{
				fraPayment.Visible = false;
				fraStatusLabels.Visible = true;
                utilityPanel.Top = fraStatusLabels.Top + fraStatusLabels.Height + lblWaterHeading.Height;
			}

			grid.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;

			grid.TextMatrix(0, lngGRIDCOLYear, "Year");
			grid.TextMatrix(0, lngGRIDColBillNumber, "Bill");
			grid.TextMatrix(0, lngGRIDColDate, "Date");
			grid.TextMatrix(0, lngGRIDColRef, "Ref");
			grid.TextMatrix(0, lngGRIDColService, "P");
			grid.TextMatrix(0, lngGRIDColPaymentCode, "C");
			grid.TextMatrix(0, lngGRIDColPrincipal, "Principal");
			grid.TextMatrix(0, lngGRIDColPTC, "PTC");
			grid.TextMatrix(0, lngGRIDColTax, "Tax");
			grid.TextMatrix(0, lngGRIDColInterest, "Interest");
			grid.TextMatrix(0, lngGRIDColCosts, "Costs");
			grid.TextMatrix(0, lngGRIDColTotal, "Total");

            grid.ColHidden(lngGRIDCOLYear, true);
            grid.ColHidden(lngGridColBillKey, true);
            grid.ColHidden(lngGRIDCOLTree);
			AutoSize_GRID(service, isWide);

			grid.ColFormat(lngGRIDColDate, "MM/dd/yy");
			grid.ColFormat(lngGRIDColPrincipal, "#,##0.00");
			grid.ColFormat(lngGRIDColPTC, "#,##0.00");
			grid.ColFormat(lngGRIDColTax, "#,##0.00");
			grid.ColFormat(lngGRIDColInterest, "#,##0.00");
			grid.ColFormat(lngGRIDColCosts, "#,##0.00");
			grid.ColFormat(lngGRIDColTotal, "#,##0.00");

			grid.ColAlignment(lngGRIDCOLYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			grid.ColAlignment(lngGRIDColBillNumber, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			grid.ColAlignment(lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			grid.ColAlignment(lngGRIDColRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			grid.ColAlignment(lngGRIDColPrincipal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.ColAlignment(lngGRIDColPTC, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.ColAlignment(lngGRIDColTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.ColAlignment(lngGRIDColInterest, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.ColAlignment(lngGRIDColCosts, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.ColAlignment(lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPrincipal, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPTC, 0, lngGRIDColTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColRef, 0, lngGRIDColPaymentCode, FCGrid.AlignmentSettings.flexAlignRightCenter);
			grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColDate, 0, lngGRIDColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			grid.BackColorBkg = ColorTranslator.FromOle(globalColorSettings.TRIOCOLORGRAYBACKGROUND);
			grid.BackColorFixed = ColorTranslator.FromOle(globalColorSettings.TRIOCOLORGRAYBACKGROUND);

			SetHeadingLabels();
            if (ViewModel.IsPaymentScreen)
            {
                var spaceBetweenPanels = 10;
				utilityPanel.Height = ClientArea.Height - utilityPanel.Top - BottomPanel.Height - fraPayment.Height - spaceBetweenPanels;
				fraPayment.Top = utilityPanel.Top + utilityPanel.Height + spaceBetweenPanels;
			}
            else
            {
                utilityPanel.Height = ClientArea.Height - utilityPanel.Top - BottomPanel.Height;
            }
			
			grid.Refresh();
		}

		private void Format_Grids(UtilityType service)
		{
			try
			{
				if (service == UtilityType.All)
				{
					FormatGrid(false, UtilityType.Water);
					FormatGrid(false, UtilityType.Sewer);
				}
				else
				{
					FormatGrid(true, service);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Formatting Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Add(int lvl, FCGrid grid)
		{
			grid.Rows = grid.Rows + 1;

			lvl += 1;

			if (lvl == 2)
			{
				grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, grid.Rows - 1, lngGRIDColRef, grid.Rows - 1, grid.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, grid.Rows - 1, lngGRIDCOLYear, grid.Rows - 1, grid.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
			}

			grid.RowOutlineLevel(grid.Rows - 1, FCConvert.ToInt16(lvl));
			if (lvl <= 1)
				grid.IsSubtotal(grid.Rows - 1, true);
			
			grid.Refresh();
		}

		private void ResizeGridColumns(bool forceWide, UtilityType service)
        {
            var grid = SetGrid(service);
			int wid = grid.WidthOriginal;
            grid.ColHidden(lngGRIDCOLYear, true);
            grid.ColHidden(lngGridColBillKey, true);
            grid.ColHidden(lngGRIDCOLTree, true);
            int principalWidth = 0;
            int taxWidth = 0;
            int costWidth = 0;
            int interestWidth = 0;
            int totalsWidth = 0;
            int ptcWidth = 0;
            int billWidth = 0;
            int dateWidth = 0;
            int refWidth = 0;
            int paymentCodeWidth = 0;
            int pendingWidth = 0;
            int ptcLeft = 0;
            int principalLeft = 0;
            int taxLeft = 0;
            int interestLeft = 0;
            int costLeft = 0;
            int totalLeft = 0;
			if (!forceWide)
			{
                billWidth = (wid * .17).ToInteger();
                dateWidth = (wid * .2).ToInteger();
                ptcWidth = (wid * .18).ToInteger();
                interestWidth = (wid * .17).ToInteger();
                totalsWidth = (wid * .19).ToInteger();
                pendingWidth = (wid * .05).ToInteger();
            }
			else
			{
                billWidth = (wid * .075).ToInteger();
                dateWidth = (wid * .12).ToInteger();
                refWidth = (wid * .09).ToInteger();
                paymentCodeWidth = (wid * .04).ToInteger();
                principalWidth = (wid * .12).ToInteger();
                taxWidth = (wid * .11).ToInteger();
                interestWidth = (wid * .11).ToInteger();
                costWidth = (wid * 0.11).ToInteger();
                totalsWidth = (wid * .12).ToInteger();
                pendingWidth = (wid * .02).ToInteger();
            }

            grid.ColWidth(lngGRIDColPTC, ptcWidth);
            grid.ColWidth(lngGRIDColTax,taxWidth);
            grid.ColWidth(lngGRIDColPrincipal, principalWidth);
            grid.ColWidth(lngGRIDColInterest, interestWidth);
            grid.ColWidth(lngGRIDColCosts, costWidth);
            grid.ColWidth(lngGRIDColTotal, totalsWidth);
            grid.ColWidth(lngGRIDColBillNumber, billWidth);
            grid.ColWidth(lngGRIDColDate, dateWidth);
            grid.ColWidth(lngGRIDColRef, refWidth);
            grid.ColWidth(lngGRIDColPaymentCode, paymentCodeWidth);
            grid.ColWidth(lngGRIDColService, 0);
            grid.ColWidth(lngGRIDColLineCode, 0);
            grid.ColWidth(lngGRIDColPaymentKey, 0);
            grid.ColWidth(lngGRIDColCHGINTNumber, 0);
            grid.ColWidth(lngGRIDColPerDiem, 0);
            grid.ColWidth(lngGRIDColPending, pendingWidth);

			//transform to pixels. FCGrid uses twips

            ptcWidth = ptcWidth / 15;
            principalWidth = principalWidth / 15;
            taxWidth = taxWidth / 15;
            interestWidth = interestWidth / 15;
            costWidth = costWidth / 15;
            totalsWidth = totalsWidth / 15;
            billWidth = billWidth / 15;
            dateWidth = dateWidth / 15;
            refWidth = refWidth / 15;
            paymentCodeWidth = paymentCodeWidth / 15;

            int offset = 12 + grid.Left; // offset is number found through experiment.  Width of expansion control perhaps?

            ptcLeft = billWidth + dateWidth + refWidth + paymentCodeWidth + offset;
            principalLeft = ptcLeft;
            taxLeft = principalLeft + principalWidth + ptcWidth ;
            interestLeft = taxLeft + taxWidth;
            costLeft = interestLeft + interestWidth;
            totalLeft = costLeft + costWidth;


			if (service == UtilityType.Water)
            {
                txtWaterTotalPrincipal.Width = principalWidth;
                txtWaterTotalTax.Width = taxWidth;
                txtWaterTotalInterest.Width = interestWidth;
                txtWaterTotalCosts.Width = interestWidth;
				txtWaterTotalPrincipal.Left = principalLeft;
                txtWaterTotalPTC.Left = ptcLeft;
                txtWaterTotalCosts.Left = costLeft;
                txtWaterTotalInterest.Left = interestLeft;
                txtWaterTotalTax.Left = taxLeft;
                txtWaterTotalTotal.Left = totalLeft;
                txtWaterTotalTotal.Width = totalsWidth;
				txtWaterTotalTax.Visible = taxWidth != 0;
                txtWaterTotalCosts.Visible = costWidth != 0;
                txtWaterTotalPTC.Visible = ptcWidth != 0;
                txtWaterTotalPrincipal.Visible = ptcWidth == 0;
                txtWaterAccountTotalsAsOf.Width = principalLeft - txtWaterAccountTotalsAsOf.Left;
                txtWaterShortAccountTotalsAsOf.Width = principalLeft - txtWaterShortAccountTotalsAsOf.Left;
                txtWaterAccountTotalsAsOf.Visible = txtWaterTotalPrincipal.Visible;
                txtWaterShortAccountTotalsAsOf.Visible = !txtWaterAccountTotalsAsOf.Visible;
            }
            else
            {
                txtSewerTotalPrincipal.Width = principalWidth;
                txtSewerTotalPTC.Width = ptcWidth;
                txtSewerTotalTax.Width = taxWidth;
                txtSewerTotalInterest.Width = interestWidth;
                txtSewerTotalCosts.Width = interestWidth ;
                txtSewerTotalTotal.Width = totalsWidth;
                txtSewerTotalPrincipal.Left = principalLeft;
                txtSewerTotalPTC.Left = ptcLeft;
                txtSewerTotalCosts.Left = costLeft;
                txtSewerTotalInterest.Left = interestLeft;
                txtSewerTotalTax.Left = taxLeft;
                txtSewerTotalTotal.Left = totalLeft;
                txtSewerTotalTax.Visible = taxWidth != 0;
                txtSewerTotalCosts.Visible = costWidth != 0;
                txtSewerTotalPTC.Visible = ptcWidth != 0;
                txtSewerTotalPrincipal.Visible = ptcWidth == 0;
                txtSewerAccountTotalsAsOf.Width = principalLeft - txtSewerAccountTotalsAsOf.Left;
                txtSewerShortAccountTotalsAsOf.Width = principalLeft - txtSewerShortAccountTotalsAsOf.Left;
                txtSewerAccountTotalsAsOf.Visible = txtSewerTotalPrincipal.Visible;
                txtSewerShortAccountTotalsAsOf.Visible = !txtSewerAccountTotalsAsOf.Visible;
			}
		}

		private void AutoSize_GRID(UtilityType service, bool boolForcedWide = false)
		{
			for (int j = 0; j < lngGRIDColPending - 1; j++)
			{
				WGRID.Columns[j].Visible = true;
				SGRID.Columns[j].Visible = true;
			}
			if (viewModel.TownServices == UtilityType.All)
			{
				if (!boolForcedWide)
				{
					ResizeGridColumns(false, UtilityType.Water);
					ResizeGridColumns(false, UtilityType.Sewer);
				}
				else if (service == UtilityType.Water)
				{
					ResizeGridColumns(true, UtilityType.Water);
				}
				else
				{
					ResizeGridColumns(true, UtilityType.Sewer);
				}
			}
			else
			{
				if (service == UtilityType.Water)
				{
					ResizeGridColumns(true, UtilityType.Water);
				}
				else
				{
					ResizeGridColumns(true,UtilityType.Sewer);
				}
			}
		}

		private void lblAcctComment_Click(object sender, System.EventArgs e)
		{
			EditCommentRec();
		}

		private void lblSewerHeading_DoubleClick(object sender, System.EventArgs e)
		{
			UtilityType service = UtilityType.Sewer;

			if (waterGridPanel.Visible)
			{
				Format_Grids(UtilityType.Sewer);
				viewModel.VisibleService = UtilityType.Sewer;
				mnuFileSwitch.Visible = true;
				mnuFileShowSplitView.Visible = true;
				service = UtilityType.Sewer;
                waterGridPanel.Visible = false; 
            }
			else if (WGRID.Rows > 1)
			{
				mnuFileSwitch_Click();
				Format_Grids(UtilityType.Water);
				mnuFileSwitch.Visible = true;
				mnuFileShowSplitView.Visible = true;
				service = UtilityType.Water;
				//SGRID.Visible = false;
                sewerGridPanel.Visible = false;
            }
			else
			{
				//SGRID.Visible = true;
                sewerGridPanel.Visible = true;
                waterGridPanel.Visible = false;
            }

			SetService(service);
			SetHeadingLabels();

			WGRID.Refresh();
			SGRID.Refresh();
		}

		private void lblWaterHeading_DoubleClick(object sender, System.EventArgs e)
		{
			UtilityType service = UtilityType.Water;

            if (sewerGridPanel.Visible)
			{
				Format_Grids(UtilityType.Water);
				viewModel.VisibleService = UtilityType.Water;
				mnuFileSwitch.Visible = true;
				mnuFileShowSplitView.Visible = true;
				service = UtilityType.Water;
                sewerGridPanel.Visible = false;
            }
			else if (SGRID.Rows > 1)
			{
				mnuFileSwitch_Click();
				Format_Grids(UtilityType.Sewer);
				mnuFileSwitch.Visible = true;
				mnuFileShowSplitView.Visible = true;
				service = UtilityType.Sewer;
                waterGridPanel.Visible = false;
            }
			else
			{
                sewerGridPanel.Visible = true;
                waterGridPanel.Visible = false;
            }

			SetService(service);
			SetHeadingLabels();

			WGRID.Refresh();
			SGRID.Refresh();
		}

		private void mnuFileComment_Click(object sender, System.EventArgs e)
		{
			EditCommentRec();
			
		}

		private void EditCommentRec()
		{
			int temp = viewModel.Master.ID;
			frmUTComment.InstancePtr.Init(ref temp);
			viewModel.UpdateCommentRec();

            lblAcctComment.Visible = HasAccountComment();
        }

        private bool HasAccountComment()
        {
            if ((viewModel.AccountCommentRec?.Text ?? "") != "")
            {
                return true;
            }
            else
            {
                return false;
            }
		}

		private void mnuFileConsumptionReport_Click(object sender, System.EventArgs e)
		{
			int temp = viewModel.Master.AccountNumber ?? 0;
			rptConsumptionHistory.InstancePtr.Init(ref temp, ref temp);
		}

		private void mnuFileOptionsLoadValidAccount_Click(object sender, System.EventArgs e)
		{
			if (viewModel.BudgetaryExists)
			{
				txtAcctNumber.TextMatrix(0, 0, frmLoadValidAccounts.InstancePtr.Init(txtAcctNumber.Text));
				txtAcctNumber.EditText = txtAcctNumber.TextMatrix(0, 0);
			}
		}

		private void mnuFilePrintRateInfo_Click(object sender, System.EventArgs e)
		{
			arUBStatusRateInfoReport.InstancePtr.Init(this, vsRateInfo.Rows, vsRateInfo.Cols, viewModel.VisibleService == UtilityType.Water);
			frmReportViewer.InstancePtr.Init(arUBStatusRateInfoReport.InstancePtr);
		}

		private void mnuFileShowMeterInfo_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Unload();
			arUTMeterDetail.InstancePtr.Init(viewModel.Master.ID);
		}

		private void mnuFileShowSplitView_Click(object sender, System.EventArgs e)
		{
			Format_Grids(UtilityType.All);
			mnuFileSwitch.Visible = false;
			mnuFileShowSplitView.Visible = false;
            sewerGridPanel.Visible = true;
            waterGridPanel.Visible = true;
			SetHeadingLabels();
		}

		private void mnuFileSwitch_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (viewModel.VisibleService == UtilityType.Water)
				{
					if (SGRID.Rows > 1)
                    {
                        waterGridPanel.Visible = false;
                        sewerGridPanel.Visible = true;
						viewModel.VisibleService = UtilityType.Sewer;
						mnuFileSwitch.Text = "Show Water Bills";
						Format_Grids(UtilityType.Sewer);
					}
					else
					{
						mnuFileSwitch.Visible = false;
					}
				}
				else
				{
					if (WGRID.Rows > 1)
					{
                        sewerGridPanel.Visible = false;
                        waterGridPanel.Visible = true;
						viewModel.VisibleService = UtilityType.Water;
						mnuFileSwitch.Text = "Show Sewer Bills";
						Format_Grids(UtilityType.Water);
					}
					else
					{
						mnuFileSwitch.Visible = false;
					}
				}
				SetHeadingLabels();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Switching Service", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuFileSwitch_Click()
		{
			mnuFileSwitch_Click(mnuFileSwitch, new System.EventArgs());
		}

		private void SGRID_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			SGRID_Collapsed(this.SGRID.GetFlexRowIndex(e.RowIndex), false);
		}

		private void SGRID_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			SGRID_Collapsed(this.SGRID.GetFlexRowIndex(e.RowIndex), true);
		}

		private void txtAcctNumber_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (!viewModel.ValidManualAccount(txtAcctNumber.EditText, FindPaymentType()))
			{
				MessageBox.Show("This account is either invalid or the account's fund does not match the fund for this bill type.", "Invalid Account / Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void txtTax_Enter(object sender, System.EventArgs e)
		{
			txtTax.SelectionStart = 0;
			txtTax.SelectionLength = txtTax.Text.Length;
		}

		private void txtTax_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtTax.SelectionStart == 0)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtTax.SelectionStart == txtTax.Text.Length)
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtTax_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii == Keys.Back) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Delete)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtTax.Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtTax.SelectionStart < lngDecPlace && txtTax.SelectionLength + txtTax.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else if (KeyAscii == Keys.Execute)
			{
				// plus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtTax.Text) == 0)
				{
					txtTax.Text = "0.00";
				}
				txtTax.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtTax.Text)), "#,##0.00");
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = txtTax.Text.Length;
			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				// c, C - this will clear the box
				KeyAscii = (Keys)0;
				txtTax.Text = "0.00";
				txtTax.SelectionStart = 0;
				txtTax.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTax_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				if (txtTax.Text != "")
				{
					if (FCConvert.ToDecimal(txtTax.Text) > 99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTax.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtTax.Text) < -99999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtTax.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtTax.Text = Strings.Format(FCConvert.ToDecimal(txtTax.Text), "#,##0.00");
					}
				}
				else
				{
					txtTax.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void WGRID_RowCollapsed(object sender, DataGridViewRowEventArgs e)
		{
			WGRID_Collapsed(this.WGRID.GetFlexRowIndex(e.RowIndex), false);
		}

		private void WGRID_RowExpanded(object sender, DataGridViewRowEventArgs e)
		{
			WGRID_Collapsed(this.WGRID.GetFlexRowIndex(e.RowIndex), true);
		}

		private void GridClick(UtilityType service)
		{
			FCGrid grid = SetGrid(service);

			SetService(service);
			if (grid.RowOutlineLevel(grid.Row) == 1)
			{
				FillUBBillNumber(service);
				SetBillNumberCombo("Auto");
				SetBillNumberCombo(grid.TextMatrix(grid.Row, lngGRIDColBillNumber));
			}
			else if (WGRID.RowOutlineLevel(grid.Row) == 0)
			{
				if ("Auto" == Strings.Trim(cmbBillNumber.Text))
				{
					cmbBillNumber_Click();
				}
				else
				{
					SetBillNumberCombo("Auto");
				}
			}
		}

		private void WGRID_ClickEvent(object sender, System.EventArgs e)
		{
			GridClick(UtilityType.Water);
		}

		private void WGRID_Collapsed(int row, bool isCollapsed)
		{
			GridCollapse(UtilityType.Water);
		}

		private FCGrid SetGrid(UtilityType service)
		{
			if (service == UtilityType.Water)
			{
				return WGRID;
			}
			else
			{
				return SGRID;
			}
		}

        private (FCGrid utilityGrid, Wisej.Web.TableLayoutPanel utilityPanel) SetGridAndPanel(UtilityType service)
        {
            if (service == UtilityType.Water)
            {
                return (utilityGrid: WGRID,utilityPanel: waterGridPanel);
            }
            else
            {
                return (utilityGrid:SGRID,utilityPanel: sewerGridPanel);
            }
        }

		private void GridDoubleClick(UtilityType service)
		{
			FCGrid grid = SetGrid(service);

			int mRow = grid.MouseRow;
			int mCol = grid.MouseCol;
			int cRow = grid.Row;
			if (mRow > 0 && mCol > 0)
			{
				if (grid.RowOutlineLevel(cRow) == 1)
				{
					if (grid.Col == lngGRIDColTotal || grid.Col == lngGRIDColPending)
					{
						// do nothing
					}
					else if (grid.Col == 1)
					{
						// set the year
						SetBillNumberCombo(grid.TextMatrix(grid.Row, grid.Col).Trim());
						SetService(service);
					}
					// expand the rows
					if (grid.IsCollapsed(grid.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						grid.IsCollapsed(grid.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
					}
					else
					{
						grid.IsCollapsed(grid.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					}
				}
				else if (grid.RowOutlineLevel(cRow) == 2 && fraPayment.Visible == true && (grid.TextMatrix(cRow, lngGRIDColRef) != "CHGINT" && grid.TextMatrix(cRow, lngGRIDColRef) != "EARNINT" && grid.TextMatrix(cRow, lngGRIDColRef) != "CNVRSN"))
				{
					if (grid.TextMatrix(LastParentRow(cRow, grid), lngGridColBillKey) != "")
					{
						var utility = viewModel.GetUtilityDetails(
							grid.TextMatrix(LastParentRow(cRow, grid), lngGRIDColBillNumber).ToIntegerValue(),
							service);

						if (!utility.IsLiened)
						{
							if (grid.TextMatrix(cRow, lngGRIDColRef) != "Billed To:")
							{
								if (Strings.Trim(grid.TextMatrix(cRow, lngGRIDColRef)) != "Total" && Strings.Trim(grid.TextMatrix(cRow, lngGRIDColRef)) != "CURINT")
								{
									if (MessageBox.Show("Are you sure that you would like to reverse this payment?", "Reverse Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
									{
										CreateUTOppositionLine(cRow, service, true);
									}
								}
								else
								{
									CreateUTOppositionLine(cRow, service);
								}
							}
						}
					}
					else
					{
						if (viewModel.DisableAutoPaymentInsert)
						{
							return;
						}
						CreateUTOppositionLine(cRow, service, false);
					}
				}
				else if (grid.RowOutlineLevel(cRow) == 0)
				{
					if (viewModel.DisableAutoPaymentInsert)
					{
						return;
					}
					CreateUTOppositionLine(cRow, service, true);
				}
			}
		}

		private void WGRID_DblClick(object sender, System.EventArgs e)
		{
			GridDoubleClick(UtilityType.Water);
		}

		private void WGRID_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void WGRID_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			GridKeyPress(UtilityType.Sewer, e);
		}

		private void GridMouseDown(UtilityType service, DataGridViewCellMouseEventArgs e)
		{
			FCGrid grid = SetGrid(service);
			currentServiceSelected = service;

			int lngMRow = 0;
			int lngBK = 0;
			lngMRow = grid.MouseRow;

			if (e.Button == MouseButtons.Right && lngMRow >= 0)
			{
				if (grid.RowOutlineLevel(lngMRow) == 1)
				{
					grid.Select(lngMRow, 1);
					var bill = viewModel.GetBill(Math.Abs(grid.TextMatrix(lngMRow, lngGRIDColPaymentKey)
						.ToIntegerValue()));

					if (bill != null)
					{
						ShowRateInfo(bill, service);
					}
				}
				else if (grid.RowOutlineLevel(lngMRow) == 2)
				{
					var payment = viewModel.GetPayment(Math.Abs(grid.TextMatrix(lngMRow, lngGRIDColPaymentKey)
						.ToIntegerValue()));
					if (payment != null)
					{
						ShowPaymentInfo(payment);
					}
				}
			}
		}

		private void WGRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			GridMouseDown(UtilityType.Water, e);
		}

		private void WGRID_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			GridMouseMove(UtilityType.Water, e);
		}

		public int LastParentRow(int CurRow, FCGrid grid)
		{
			int l = 0;
			int curLvl = 0;
			if (CurRow > 0)
			{
				l = CurRow;
				curLvl = grid.RowOutlineLevel(l);
				if (curLvl > 1)
				{
					l -= 1;
					while (grid.RowOutlineLevel(l) > 1)
					{
						l -= 1;
					}
				}
			}
			return l;
		}

		private void SwapRows(int x, int y, FCGrid Grid)
		{
			int intCol;
			string strTemp = "";
			for (intCol = lngGRIDColRef; intCol <= Grid.Cols - 1; intCol++)
			{
				strTemp = Grid.TextMatrix(x, intCol);
				Grid.TextMatrix(x, intCol, Grid.TextMatrix(y, intCol));
				Grid.TextMatrix(y, intCol, strTemp);
			}
		}

		private int NextSpacerLine(int x, FCGrid grid, string strCheck = "=")
		{
			return grid.FindRow(strCheck, x, lngGRIDColLineCode) + 1;
		}

		private void GRID_Coloring(int l, FCGrid grid)
		{
			int lngTemp;
			lngTemp = NextSpacerLine(l, grid);
			if (lngTemp > 0)
			{
				grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngTemp, 0, lngTemp, grid.Cols - 1, ColorTranslator.ToOle(Color.FromArgb(244, 247, 249)));
				grid.Select(lngTemp - 1, lngGRIDColPrincipal, lngTemp - 1, lngGRIDColTotal);
				grid.CellBorder(Color.Blue, 0, 1, 0, 0, 0, 0);
				grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngTemp - 1, 1, lngTemp - 1, grid.Cols - 1, true);
				if (grid.TextMatrix(lngTemp - 1, lngGRIDColPrincipal).ToDecimalValue() != 0)
				{
					sumPrin += grid.TextMatrix(lngTemp - 1, lngGRIDColPrincipal).ToDecimalValue();
				}
				if (grid.TextMatrix(lngTemp - 1, lngGRIDColInterest).ToDecimalValue() != 0)
				{
					sumInt += grid.TextMatrix(lngTemp - 1, lngGRIDColInterest).ToDecimalValue();
				}
				if (grid.TextMatrix(lngTemp - 1, lngGRIDColTax).ToDecimalValue() != 0)
				{
					sumTax += grid.TextMatrix(lngTemp - 1, lngGRIDColTax).ToDecimalValue();
				}
				if (grid.TextMatrix(lngTemp - 1, lngGRIDColCosts).ToDecimalValue() != 0)
				{
					sumCost += grid.TextMatrix(lngTemp - 1, lngGRIDColCosts).ToDecimalValue();
				}
				if (grid.TextMatrix(lngTemp - 1, lngGRIDColTotal).ToDecimalValue() != 0)
				{
					sumTotal += grid.TextMatrix(lngTemp - 1, lngGRIDColTotal).ToDecimalValue();
				}
				GRID_Coloring(lngTemp + 1, grid);
			}
			else
			{
				grid.Select(0, 1);
			}
		}

		
		private void imgNote_DoubleClick(object sender, System.EventArgs e)
		{
            EditNote();
        }

        private void ShowNote()
        {
            if (viewModel.AccountNote != null)
            {
                if (!viewModel.AccountNote.Text.IsNullOrWhiteSpace())
                {
                    imgNote.Visible = true;
                }
                else
                {
                    imgNote.Visible = false;
                }
            }
            else
            {
                imgNote.Visible = false;
            }
        }
		private void EditNote()
		{
			viewModel.EditNote();
            ShowNote();
        }

		private void mnuFileAccountInfo_Click(object sender, System.EventArgs e)
		{
			ShowAccountInfo();
		}

		private void mnuFileEditNote_Click(object sender, System.EventArgs e)
		{
            EditNote();
        }

		

		private void mnuFileDischarge_Click(object sender, System.EventArgs e)
		{
			try
			{
				UtilityType service;
				int lienId = 0;

				if (mnuFileShowSplitView.Visible)
				{
					service = viewModel.VisibleService;
				}
				else
				{
					service = currentServiceSelected;
				}

				if (service == UtilityType.Sewer)
				{
					int lngRow = SGRID.Row;
					if (lngRow > 0)
					{
						if (lngRow < SGRID.Rows)
						{
							lngRow += 1;
						}
						if (Strings.Left(SGRID.TextMatrix(LastParentRow(lngRow, SGRID), lngGRIDColRef), 4) == "Lien")
						{
							lienId = SGRID.TextMatrix(LastParentRow(lngRow, SGRID), lngGridColBillKey).ToIntegerValue();
						}
					}

					if (lienId > 0)
					{
						var discharge = viewModel.LienDischarge(lienId);
						if (discharge != null)
						{
							frmUTLienDischargeNotice.InstancePtr.Init((DateTime)discharge.DatePaid, false, lienId);
						}
						else
						{
							frmUTLienDischargeNotice.InstancePtr.Init(viewModel.EffectiveDate.Date, false, lienId);
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Lien Discharge Notice Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileMortgageHolderInfo_Click(object sender, System.EventArgs e)
		{
			if ((viewModel.Master.UseREAccount ?? false) && (viewModel.Master.UseMortgageHolder ?? false) && (viewModel.Master.REAccount ?? 0) != 0)
			{
				frmMortgageHolder.InstancePtr.Init((int)viewModel.Master.REAccount, 2, true);
			}
			else
			{
				frmMortgageHolder.InstancePtr.Init(FCConvert.ToInt32(Conversion.Val(lblAccount.Text)), 3, false);
			}
			frmMortgageHolder.InstancePtr.Show(App.MainForm);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			arUBPaymentStatusDetail.InstancePtr.SetPaymentForm(this);
			frmReportViewer.InstancePtr.Init(arUBPaymentStatusDetail.InstancePtr);
		}

		private void DeleteAllPendingPayments()
		{
			viewModel.RemoveAllPendingPayments();

			vsPayments.Rows = 1;
			StartUp();
			CheckAllPending();
		}

		public void CheckAllPending()
		{
			foreach (var bill in viewModel.Bills(UtilityType.All).Where(x => x.BillNumber != 0))
			{
				foreach (var utility in bill.UtilityDetails)
				{
					FCGrid grid = SetGrid(utility.Service);

					int billRow = grid.FindRow(bill.BillNumber, 1, lngGRIDColBillNumber);
					bool pending = viewModel.PendingPaymentsExistForSelectedBill(bill.BillNumber.ToString());

					grid.TextMatrix(billRow, lngGRIDColPending, pending ? "*" : " ");
					grid.TextMatrix(grid.FindRow("=", billRow, lngGRIDColLineCode), lngGRIDColPending, pending ? "*" : " ");
					grid.TextMatrix(grid.FindRow("=", billRow, lngGRIDColLineCode) + 1, lngGRIDColPending, pending ? "*" : " ");
				}
			}

			if (viewModel.TownServices == UtilityType.All || viewModel.TownServices == UtilityType.Sewer)
			{
				SGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPending, SGRID.Rows - 1, lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}

			if (viewModel.TownServices == UtilityType.All || viewModel.TownServices == UtilityType.Water)
			{
				WGRID.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, lngGRIDColPending, WGRID.Rows - 1, lngGRIDColPending, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}

			txtTotalPendingDue.Text = Strings.Format(viewModel.TotalPendingPaymentsAmount(viewModel.TownServices), "#,##0.00");
		}

		private void mnuPaymentClearList_Click(object sender, System.EventArgs e)
		{
			DeleteAllPendingPayments();
		}

		private void mnuPaymentClearPayment_Click(object sender, System.EventArgs e)
		{
			ResetUBPaymentFrame(true);
		}

		private void mnuPaymentSave_Click(object sender, System.EventArgs e)
		{
			SavePayment();
		}

		private bool SavePayment()
		{
			try
			{
				bool excessAmount = false;
				string billNumber = cmbBillNumber.Items[cmbBillNumber.SelectedIndex].ToString();
				string code = cmbCode.Items[cmbCode.SelectedIndex].ToString().Left(1);
				var paymentType = FindPaymentType();

				if (billNumber.ToUpper() == "AUTO")
                {
                    if (viewModel.PendingPaymentsExist())
                    {
                        MessageBox.Show("You may not make an auto payment because pending payments exist.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
				}

				if (viewModel.PendingPaymentsExistForSelectedBill(billNumber))
				{
					MessageBox.Show("Payments already exists from this bill.", "Existing Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (code == "P" && txtInterest.Text.ToDecimalValue() < 0)
				{
					MessageBox.Show("Payments may not contain negative values.  If you need to adjust amounts on a bill you may do a correction instead.", "Negative Payments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return false;
				}

				if (txtCD.Text == "N" && txtCash.Text == "N")
				{
					if (!viewModel.ValidManualAccount(txtAcctNumber.Text, paymentType))
					{
						MessageBox.Show("This account is either invalid or the account's fund does not match the fund for this bill type.", "Invalid Account / Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return false;
					}
				}

				if (!viewModel.IsPaymentScreen)
				{
					return false;
				}

				if (txtPrincipal.Text == "")
					txtPrincipal.Text = "0.00";
				if (txtTax.Text == "")
					txtTax.Text = "0.00";
				if (txtInterest.Text == "")
					txtInterest.Text = "0.00";
				if (txtCosts.Text == "")
					txtCosts.Text = "0.00";

				if (txtPrincipal.Text.ToDecimalValue() != 0 || txtTax.Text.ToDecimalValue() != 0 || txtInterest.Text.ToDecimalValue() != 0 || txtCosts.Text.ToDecimalValue() != 0)
				{
					if (code != "")
					{
						if (code == "P" || code == "C")
						{
							excessAmount = ViewModel.PaymentGreaterThanOwed(
								billNumber, cmbService.Text.Left(1) == "B" ? UtilityType.All : paymentType,
								txtPrincipal.Text.ToDecimalValue() + txtTax.Text.ToDecimalValue() +
								txtInterest.Text.ToDecimalValue() + txtCosts.Text.ToDecimalValue());
						
							if (excessAmount)
							{
								if (MessageBox.Show("The payment entered is greater than owed, would you like to continue?", "Payment Amount", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
								{
									return false;
								}
							}
						}

						viewModel.AddPendingPayment(billNumber, cmbService.Text.Left(1) == "B" ? UtilityType.All : paymentType, code, txtReference.Text.Trim(), txtCD.Text, txtCash.Text, txtAcctNumber.TextMatrix(0, 0), txtComments.Text.Trim(), txtTransactionDate.Text.Trim() == "" ? (DateTime?)null : txtTransactionDate.Text.ToDate(), txtPrincipal.Text.ToDecimalValue(), txtTax.Text.ToDecimalValue(), txtInterest.Text.ToDecimalValue(), txtCosts.Text.ToDecimalValue(), txtReference.Text.ToUpper().Trim() == "REVERS" || cmbCode.Text.Left(1) == "C" || cmbCode.Text.Left(1) == "A" || cmbCode.Text.Left(1) == "R");
					}
					else
					{
						MessageBox.Show("No Code entered!", "Missing Code ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						return false;
					}
				}
				else
				{
					if (vsPayments.Rows == 1)
					{
						if (viewModel.NegativeBillsExist(cmbService.Text.Left(1) == "B" ? UtilityType.All : paymentType))
						{
							if (MessageBox.Show("Apply the negative account values to bills with outstanding balances?", "Negative Account Balances", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								viewModel.AdjustNegativeBillsToAddPayment(cmbService.Text.Left(1) == "B" ? UtilityType.All : paymentType);
							}
						}
						else
						{
							MessageBox.Show("There are no values in the payment fields.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
					}
				}

				ResetUBPaymentFrame(false);
				CheckAllPending();
				StartUp();

				return true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Payment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return false;
			}
		}

		private void mnuPaymentSaveExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				if (!viewModel.IsPaymentScreen)
				{
					return;
				}

				if (txtPrincipal.Text.ToDecimalValue() != 0 || txtTax.Text.ToDecimalValue() != 0 || txtInterest.Text.ToDecimalValue() != 0)
				{
					if (!SavePayment())
					{
						return;
					}
				}

				FillTransactionInformation();

				viewModel.CompleteTransaction();
				cancelling = false;
				this.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving/Exit Payment", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTransactionInformation()
		{
			viewModel.Transaction.Bills = viewModel.Bills(UtilityType.All);
		}

		private void mnuProcessChangeAccount_Click(object sender, System.EventArgs e)
		{
			bool boolGoBack = false;
			if (vsPayments.Rows > 1)
			{
				if (MessageBox.Show("Are you sure that you would like to leave this screen?  Any payments that you have added this session will be lost unless saved.", "Change Account", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					boolGoBack = true;
				}
			}
			else
			{
				boolGoBack = true;
			}
			if (boolGoBack)
			{
				cancelling = false;
				Close();
				viewModel.GoToSearchScreen();
			}
		}

		private void mnuProcessEffective_Click(object sender, System.EventArgs e)
		{
			if (viewModel.PendingPaymentsExist())
			{
				MessageBox.Show("You may not change the effective date with existing pending transactions.", "Unable to Change Effective Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			else
			{
				object strDate = viewModel.EffectiveDate.ToString("MM/dd/yyyy");
				if (frmInput.InstancePtr.Init(ref strDate, "Date Change", "Please input the new effective date and then press OK.", intDataType: modGlobalConstants.InputDTypes.idtDate, strInitValue: viewModel.EffectiveDate.ToString("MM/dd/yyyy")))
				{
					DateTime newEffectiveDate;
					if (DateTime.TryParse(strDate.ToString(), out newEffectiveDate))
					{
						viewModel.EffectiveDate = newEffectiveDate;
					}

					StartUp();
				}
			}
		}

		private void Create_Account_Totals(UtilityType service)
		{
            var totalText = "";
            if (Math.Round((sumPrin + sumInt + sumCost + sumTax), 2) == Math.Round(sumTotal, 2))
			{
                totalText = sumTotal.FormatAsMoney();
            }
			else
			{
                totalText = "ERROR(" + sumTotal + ")";
            }
            if (service == UtilityType.Water)
            {
                txtWaterAccountTotalsAsOf.Text =
                    "Account Totals as of " + viewModel.EffectiveDate.ToString("MM/dd/yyyy");
                txtWaterShortAccountTotalsAsOf.Text = "Totals as of " + viewModel.EffectiveDate.ToString("MM/dd/yyyy");
                txtWaterTotalPrincipal.Text = sumPrin.FormatAsMoney();
                txtWaterTotalTax.Text = sumTax.FormatAsMoney();
                txtWaterTotalInterest.Text = sumPrin.FormatAsMoney();
                txtWaterTotalCosts.Text = sumCost.FormatAsMoney();
                txtWaterTotalTotal.Text = totalText;
                txtWaterTotalPTC.Text = (sumPrin + sumCost + sumTax).FormatAsMoney();
            }
            else
            {
                txtSewerAccountTotalsAsOf.Text =
                    "Account Totals as of " + viewModel.EffectiveDate.ToString("MM/dd/yyyy");
                txtSewerShortAccountTotalsAsOf.Text = "Totals as of " + viewModel.EffectiveDate.ToString("MM/dd/yyyy");
                txtSewerTotalPrincipal.Text = sumPrin.FormatAsMoney();
                txtSewerTotalTax.Text = sumTax.FormatAsMoney();
                txtSewerTotalInterest.Text = sumInt.FormatAsMoney();
                txtSewerTotalCosts.Text = sumCost.FormatAsMoney();
                txtSewerTotalTotal.Text = totalText;
                txtSewerTotalPTC.Text = (sumPrin + sumCost + sumTax).FormatAsMoney();
            }
		}

		private void Reset_Sum()
		{
			sumPrin = 0;
			sumInt = 0;
			sumTax = 0;
			sumCost = 0;
			sumTotal = 0;
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			CloseForm();
		}

		private void CloseForm()
		{
			Close();
		}

		private void Format_PaymentGrid()
		{
			int wid = 0;
			vsPayments.Cols = 14;
			wid = vsPayments.WidthOriginal;
			vsPayments.ColWidth(lngPayGridColBill, FCConvert.ToInt32(wid * 0.04));
			// Year
			vsPayments.ColWidth(lngPayGridColDate, FCConvert.ToInt32(wid * 0.12));
			// Date
			vsPayments.ColWidth(lngPayGridColRef, FCConvert.ToInt32(wid * 0.09));
			// Reference
			vsPayments.ColWidth(lngPayGridColService, FCConvert.ToInt32(wid * 0.04));
			// Service
			vsPayments.ColWidth(lngPayGridColCode, FCConvert.ToInt32(wid * 0.05));
			// Code
			vsPayments.ColWidth(lngPayGridColCDAC, FCConvert.ToInt32(wid * 0.05));
			// CD / Affect Cash
			vsPayments.ColWidth(lngPayGridColPrincipal, FCConvert.ToInt32(wid * 0.13));
			// Principal
			vsPayments.ColWidth(lngPayGridColTax, FCConvert.ToInt32(wid * 0.113));
			// Tax
			vsPayments.ColWidth(lngPayGridColInterest, FCConvert.ToInt32(wid * 0.12));
			// Interest
			vsPayments.ColWidth(lngPayGridColCosts, FCConvert.ToInt32(wid * 0.113));
			// Costs
			vsPayments.ColWidth(lngPayGridColArrayIndex, 0);
			// Array Index
			vsPayments.ColWidth(lngPayGridColKeyNumber, 0);
			// ID Number from AutoNumberfield in DB
			vsPayments.ColWidth(lngPayGridColCHGINTNumber, 0);
			// CHGINT Number
			vsPayments.ColWidth(lngPayGridColTotal, FCConvert.ToInt32(wid * 0.13));
			// Total
			vsPayments.ColAlignment(lngPayGridColBill, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColRef, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColService, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColCode, FCGrid.AlignmentSettings.flexAlignLeftCenter);

			vsPayments.ColAlignment(lngPayGridColPrincipal, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColTax, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColInterest, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColCosts, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngPayGridColTotal, FCGrid.AlignmentSettings.flexAlignLeftCenter);

			// all the headers
			vsPayments.ExtendLastCol = true;
			vsPayments.TextMatrix(0, lngPayGridColBill, "Bill");
			vsPayments.TextMatrix(0, lngPayGridColDate, "Date");
			vsPayments.TextMatrix(0, lngPayGridColRef, "Ref");
			vsPayments.TextMatrix(0, lngPayGridColService, "Svc");
			vsPayments.TextMatrix(0, lngPayGridColCode, "Code");
			vsPayments.TextMatrix(0, lngPayGridColCDAC, "Cash");
			vsPayments.TextMatrix(0, lngPayGridColPrincipal, "Principal");
			vsPayments.TextMatrix(0, lngPayGridColTax, "Tax");
			vsPayments.TextMatrix(0, lngPayGridColInterest, "Interest");
			vsPayments.TextMatrix(0, lngPayGridColCosts, "Costs");
			vsPayments.TextMatrix(0, lngPayGridColTotal, "Total");
		}

		private void mnuProcessGoTo_Click(object sender, System.EventArgs e)
		{
			if (!viewModel.StartedFromCashReceipts)
			{
				if (viewModel.IsPaymentScreen)
				{
					GoToStatus();
				}
				else
				{
					GoToPayment();
				}
			}
        }

		private void txtAcctNumber_Enter(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text, 1) == "R" || txtAcctNumber.Enabled == false)
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtAcctNumber_KeyDownEvent(object sender, KeyEventArgs e)
		{
			// left = 37
			// right = 39
			Keys KeyCode = e.KeyCode;
			switch (FCConvert.ToInt32(KeyCode))
			{
				case 37:
					{
						if (txtAcctNumber.EditSelStart == 0)
						{
							if (txtCash.Enabled == true)
							{
								if (txtCash.Visible && txtCash.Enabled)
								{
									txtCash.Focus();
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
							KeyCode = 0;
						}
						break;
					}
				case 39:
					{
						if (txtAcctNumber.EditSelStart == txtAcctNumber.Text.Length - 1)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = 0;
						}
						break;
					}
			}
		}

		private void txtCash_DoubleClick(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text, 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
			else
			{
				if (txtCash.Text == "N")
				{
					txtCash.Text = "Y";
				}
				else
				{
					if (Strings.Left(cmbService.Text, 1) != "B")
					{
						txtCash.Text = "N";
					}
					else
					{
						MessageBox.Show("Please select a service before selecting Non Cash.", "Invalid Service", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				CheckCash();
			}
		}

		public void txtCash_DblClick()
		{
			txtCash_DoubleClick(txtCash, new System.EventArgs());
		}

		private void txtCash_Enter(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text, 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtCash_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// left = 37
			// right = 39
			switch (KeyCode)
			{
				case Keys.Left:
					{
						if (txtCD.Visible && txtCD.Enabled)
						{
							txtCD.Focus();
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						if (txtCash.Text == "N")
						{
							if (txtAcctNumber.Visible && txtPrincipal.Enabled)
							{
								txtAcctNumber.Focus();
							}
						}
						else
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						txtCash_DblClick();
						break;
					}
				case Keys.N:
					{
						if (Strings.Left(cmbService.Text, 1) != "B")
						{
							txtCash.Text = "N";
						}
						else
						{
							MessageBox.Show("Please select a service before selecting Non Cash.", "Invalid Service", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						break;
					}
				case Keys.Y:
					{
						txtCash.Text = "Y";
						break;
					}
				default:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			CheckCash();
		}

		private void txtCash_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.N:
				case Keys.Y:
				case Keys.Space:
				case Keys.Back:
					{
						// do nothing
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCD_DoubleClick(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text.ToString(), 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
			else
			{
				if (txtCD.Text == "N")
				{
					txtCD.Text = "Y";
				}
				else
				{
					txtCD.Text = "N";
				}
				CheckCD();
			}
		}

		public void txtCD_DblClick()
		{
			txtCD_DoubleClick(txtCD, new System.EventArgs());
		}

		private void txtCD_Enter(object sender, System.EventArgs e)
		{
			if (Strings.Left(cmbCode.Text, 1) == "A" || Strings.Left(cmbCode.Text, 1) == "R")
			{
				if (txtPrincipal.Visible && txtPrincipal.Enabled)
				{
					txtPrincipal.Focus();
				}
			}
		}

		private void txtCD_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						cmbCode.Focus();
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtCD.Text == "N")
						{
							if (!modGlobalConstants.Statics.gboolCR && !modGlobalConstants.Statics.gboolBD)
							{
								txtCash.Enabled = false;
							}
							else
							{
								txtCash.Enabled = true;
							}
							if (txtCash.Visible && txtCash.Enabled)
							{
								txtCash.Focus();
							}
						}
						else
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
						}
						KeyCode = (Keys)0;
						break;
					}
				case Keys.Space:
					{
						txtCD_DblClick();
						break;
					}
				case Keys.N:
					{
						txtCD.Text = "N";
						break;
					}
				case Keys.Y:
					{
						txtCD.Text = "Y";
						break;
					}
				default:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			CheckCD();
		}

		private void txtCD_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.N:
				case Keys.Y:
				case Keys.Space:
				case Keys.Back:
					{
						break;
					}
				default:
					{
						KeyAscii = (Keys)0;
						break;
					}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtComments_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtComments.SelectionStart == 0)
						{
							if (txtCosts.Visible && txtCosts.Enabled)
							{
								txtCosts.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtComments.SelectionStart == txtComments.Text.Length)
						{
							vsPayments.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtCosts_Enter(object sender, System.EventArgs e)
		{
			txtCosts.SelectionStart = 0;
			txtCosts.SelectionLength = txtCosts.Text.Length;
		}

		private void txtCosts_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtCosts.SelectionStart == 0)
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtCosts.SelectionStart == txtCosts.Text.Length)
						{
							txtComments.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtCosts_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if (Strings.Left(cmbCode.Text, 1) == "R")
			{
				KeyAscii = (Keys)0;
			}
			else
			{
				if ((KeyAscii == Keys.Back) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
				{
					// do nothing
				}
				else if (KeyAscii == Keys.Delete)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, txtCosts.Text, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (txtCosts.SelectionStart < lngDecPlace && txtCosts.SelectionLength + txtCosts.SelectionStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = (Keys)0;
						}
					}
				}
				else if (KeyAscii == Keys.Execute)
				{
					// plus
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtCosts.Text) == 0)
					{
						txtCosts.Text = "0.00";
					}
					txtCosts.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtCosts.Text)), "#,##0.00");
					txtCosts.SelectionStart = 0;
					txtCosts.SelectionLength = txtCosts.Text.Length;
				}
				else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
				{
					// c, C - this will clear the box
					KeyAscii = (Keys)0;
					txtCosts.Text = "0.00";
					txtCosts.SelectionStart = 0;
					txtCosts.SelectionLength = 4;
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCosts_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				if (txtCosts.Text != "")
				{
					if (FCConvert.ToDecimal(txtCosts.Text) > 9999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $9,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtCosts.Text = Strings.Format(9999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtCosts.Text) < -9999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-9,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtCosts.Text = Strings.Format(-9999.99, "#,##0.00");
					}
					else
					{
						txtCosts.Text = Strings.Format(FCConvert.ToDecimal(txtCosts.Text), "#,##0.00");
					}
				}
				else
				{
					txtCosts.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtInterest_Enter(object sender, System.EventArgs e)
		{
			txtInterest.SelectionStart = 0;
			txtInterest.SelectionLength = txtInterest.Text.Length;
		}

		private void txtInterest_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtInterest.SelectionStart == 0)
						{
							if (txtPrincipal.Visible && txtPrincipal.Enabled)
							{
								txtPrincipal.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtInterest.SelectionStart == txtInterest.Text.Length)
						{
							if (txtCosts.Visible && txtCosts.Enabled)
							{
								txtCosts.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtInterest_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if (Strings.Left(cmbCode.Text, 1) == "R" || Strings.Left(cmbCode.Text, 1) == "A")
			{
				switch (KeyAscii)
				{
					case Keys.NumPad3:
					case Keys.C:
					case Keys.Back:
						{
							// c, C, 0 - this will clear the box
							KeyAscii = (Keys)0;
							txtInterest.Text = "0.00";
							txtInterest.SelectionStart = 0;
							txtInterest.SelectionLength = 4;
							break;
						}
				}
				//end switch
			}
			else
			{
				if ((KeyAscii == Keys.Back) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
				{
					// do nothing
				}
				else if (KeyAscii == Keys.Delete)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, txtInterest.Text, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (txtInterest.SelectionStart < lngDecPlace && txtInterest.SelectionLength + txtInterest.SelectionStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = (Keys)0;
						}
					}
				}
				else if (KeyAscii == Keys.Execute)
				{
					// plus
					KeyAscii = (Keys)0;
					if (Conversion.Val(txtInterest.Text) == 0)
					{
						txtInterest.Text = "0.00";
					}
					txtInterest.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtInterest.Text)), "#,##0.00");
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = txtInterest.Text.Length;
				}
				else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
				{
					// c, C - this will clear the box
					KeyAscii = (Keys)0;
					txtInterest.Text = "0.00";
					txtInterest.SelectionStart = 0;
					txtInterest.SelectionLength = 4;
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtInterest_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				if (txtInterest.Text != "")
				{
					if (FCConvert.ToDecimal(txtInterest.Text) > 9999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $9,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInterest.Text = Strings.Format(9999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtInterest.Text) < -9999999.99m)
					{
						MessageBox.Show("This field cannot contain a value exceeding $-9,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtInterest.Text = Strings.Format(-9999999.99, "#,##0.00");
					}
					else
					{
						txtInterest.Text = Strings.Format(FCConvert.ToDecimal(txtInterest.Text), "#,##0.00");
					}
				}
				else
				{
					txtInterest.Text = "0.00";
				}
				return;
			}
			catch (Exception ex)
			{
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtPrincipal_Enter(object sender, System.EventArgs e)
		{
			txtPrincipal.SelectionStart = 0;
			txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
		}

		private void txtPrincipal_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtPrincipal.SelectionStart == 0)
						{
							if (txtCD.Text == "N")
							{
								if (txtCash.Text == "N")
								{
									txtAcctNumber.Focus();
								}
								else
								{
									txtAcctNumber.Enabled = false;
									if (txtCash.Visible && txtCash.Enabled)
									{
										txtCash.Focus();
									}
								}
							}
							else
							{
								if (txtCD.Visible && txtCD.Enabled)
								{
									txtCD.Focus();
								}
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						// right = 39
						if (txtPrincipal.SelectionStart == txtPrincipal.Text.Length)
						{
							txtTax.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void txtPrincipal_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii == Keys.Back) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Delete)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtPrincipal.Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtPrincipal.SelectionStart < lngDecPlace && txtPrincipal.SelectionLength + txtPrincipal.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else if (KeyAscii == Keys.Execute)
			{
				// plus
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtPrincipal.Text) == 0)
				{
					txtPrincipal.Text = "0.00";
				}
				txtPrincipal.Text = Strings.Format(Math.Abs(FCConvert.ToDecimal(txtPrincipal.Text)), "#,##0.00");
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = txtPrincipal.Text.Length;
			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				// c, C - this will clear the box
				KeyAscii = (Keys)0;
				txtPrincipal.Text = "0.00";
				txtPrincipal.SelectionStart = 0;
				txtPrincipal.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtPrincipal_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				if (txtPrincipal.Text != "")
				{
					if (FCConvert.ToDecimal(txtPrincipal.Text) > FCConvert.ToDecimal(99999999.99))
					{
						MessageBox.Show("This field cannot contain a value exceeding $99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrincipal.Text = Strings.Format(99999999.99, "#,##0.00");
					}
					else if (FCConvert.ToDecimal(txtPrincipal.Text) < FCConvert.ToDecimal(-99999999.99))
					{
						MessageBox.Show("This field cannot contain a value exceeding $-99,999,999.99.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtPrincipal.Text = Strings.Format(-99999999.99, "#,##0.00");
					}
					else
					{
						txtPrincipal.Text = Strings.Format(FCConvert.ToDecimal(txtPrincipal.Text), "#,##0.00");
					}
				}
				else
				{
					txtPrincipal.Text = "0.00";
				}
			}
			catch (Exception ex)
			{
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtReference_Enter(object sender, System.EventArgs e)
		{
			txtReference.BackColor = Color.White;
		}

		private void txtReference_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			bool boolTest = false;
			switch (KeyCode)
			{
				case Keys.Left:
					{
						// left = 37
						if (txtReference.SelectionStart == 0)
						{
							if (txtTransactionDate.Visible && txtTransactionDate.Enabled)
							{
								txtTransactionDate.Focus();
							}
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Right:
					{
						if (txtReference.SelectionStart == txtReference.Text.Length)
						{
							cmbService.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.Return:
					{
						boolTest = false;
						txtReference_Validate(ref boolTest);
						if (boolTest)
						{
						}
						else
						{
							txtInterest.Focus();
							KeyCode = (Keys)0;
						}
						break;
					}
			}
		}

		private void txtReference_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// catches reserved words
			if (txtReference.Text.Trim().ToUpper() == "CHGINT" || txtReference.Text.Trim().ToUpper() == "CNVRSN" || txtReference.Text.Trim().ToUpper() == "REVRSE" || txtReference.Text.Trim().ToUpper() == "EARNINT" || txtReference.Text.Trim().ToUpper() == "Interest")
			{
				MessageBox.Show(Strings.UCase(Strings.Trim(txtReference.Text)) + " is a reserved reference string.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Information);
				e.Cancel = true;
			}
		}

		public void txtReference_Validate(ref bool Cancel)
		{
			txtReference_Validating(txtReference, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtTotalPendingDue_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// this will not allow any input to this text box
			KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTransactionDate_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (e.KeyValue == 37)
			{
				// left = 37
				if (txtTransactionDate.SelStart == 0)
				{
					cmbBillNumber.Focus();
					//FC:TODO:AM
					//e.KeyCode = 0;
				}
			}
			else if (e.KeyValue == 39)
			{
				// right = 39
				if (txtTransactionDate.SelStart == txtTransactionDate.Text.Length)
				{
					txtReference.Focus();
					//e.KeyCode = 0;
				}
			}
			else if (e.KeyCode == Keys.Return)
			{
				if (txtReference.Visible && txtReference.Enabled)
				{
					txtReference.Focus();
				}
				else
				{
					Support.SendKeys("{tab}", false);
				}
			}
			else if ((e.KeyCode == Keys.Back) || (e.KeyCode == Keys.Delete) || (e.KeyValue >= 48 && e.KeyValue <= 57) || (e.KeyValue == 191) || (e.KeyValue == 111) || (e.KeyValue >= 96 && e.KeyValue <= 105))
			{
				// backspace, delete, 0-9, "/", numberpad "/", 0-9 all can pass
			}
			else
			{
				//e.KeyCode = 0;
			}
		}

		private void txtTransactionDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(Strings.Left(txtTransactionDate.Text, 2)) > 12)
			{
				e.Cancel = true;
				MessageBox.Show("Please use the date format MM/dd/yyyy.", "Invalid Date Format", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void vsPayments_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int l;
			if (e.KeyCode == Keys.Delete)
			{
				if (vsPayments.Rows > 1)
				{
					if (MessageBox.Show("Are you sure that you would like to delete this payment?", "Delete Payment Record", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "AUTO" || vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "OVERPAY" || vsPayments.TextMatrix(vsPayments.Row, lngPayGridColRef) == "PREPAY-A")
						{
							for (l = vsPayments.Rows - 1; l >= 1; l--)
							{
								if (vsPayments.TextMatrix(l, lngPayGridColRef) == "AUTO" || vsPayments.TextMatrix(l, lngPayGridColRef) == "OVERPAY" || vsPayments.TextMatrix(l, lngPayGridColRef) == "PREPAY-A")
								{
									DeletePaymentRow(l);
								}
							}
						}
						else if (vsPayments.Row != 0)
						{
							DeletePaymentRow(vsPayments.Row);
						}
						CheckAllPending();
					}
				}
			}
		}

		private void GoToStatus()
		{
			fraPayment.Visible = false;
			mnuPayment.Visible = false;
            cmdProcessGoTo.Width = 142;
			cmdProcessGoTo.Text = strPaymentString;
            if (viewModel.VisibleService == UtilityType.Water)
			{
				if (WGRID.Enabled && sewerGridPanel.Visible)
				{
					WGRID.Focus();
				}
			}
			else
			{
                if (SGRID.Enabled && sewerGridPanel.Visible)
                {
					SGRID.Focus();
				}
			}
			cmdSavePayments.Enabled = false;
            cmdSavePayments.Visible = false;
            BottomPanel.Visible = false;
            viewModel.IsPaymentScreen = false;
			SetHeadingLabels();
		}

		private void GoToPayment()
        {
            BottomPanel.Visible = true;
			fraStatusLabels.Visible = false;
			fraPayment.Visible = true;
			mnuPayment.Visible = true;
			vsPayments.Visible = true;
			cmdProcessGoTo.Text = strStatusString;
			cmdProcessGoTo.Width = 96;

			Refresh();
			ResetUBPaymentFrame(true);
			Format_PaymentGrid();
			Format_UBvsPeriod();
			txtReference.Focus();
			cmdSavePayments.Enabled = true;
			cmdPaymentSaveExit.Enabled = true;
			viewModel.IsPaymentScreen = true;
			SetHeadingLabels();
		}

		private void DeletePaymentRow(int Row)
		{
			foreach (var payment in viewModel.GetPendingPayments(vsPayments.TextMatrix(Row, lngPayGridColBill)))
			{
				for (int row = vsPayments.Rows - 1; row >= 1; row--)
				{
					if (vsPayments.TextMatrix(row, lngPayGridColKeyNumber) == payment.PendingId.ToString() || vsPayments.TextMatrix(row, lngPayGridColKeyNumber) == (payment.ChargedInterestRecord != null ? payment.ChargedInterestRecord.PendingId.ToString() : ""))
					{
						if (row != 0)
						{
							vsPayments.RemoveItem(row);
						}
					}
				}

				viewModel.RemovePendingPayment(payment);
			}

			StartUp();
		}

		private void vsPeriod_DblClick(object sender, System.EventArgs e)
		{
			double dblAmt;
			if (viewModel.DisableAutoPaymentInsert)
			{
				return;
			}
			dblAmt = FCConvert.ToDouble(vsPeriod.TextMatrix(0, 1));
			SetBillNumberCombo("Auto");

			//if (Strings.Left(cmbService.Text, 1) == "B")
			//{
				//txtInterest.Text = Strings.Format(FCConvert.ToDouble(vsPeriod.TextMatrix(0, 1)), "#,##0.00");
			//}
			//else if (Strings.Left(cmbService.Text, 1) == "W")
			//{
			//	CreateUTOppositionLine(WGRID.Rows - 1, UtilityType.Water);
			//}
			//	else if (Strings.Left(cmbService.Text, 1) == "S")
			//{
			//	CreateUTOppositionLine(SGRID.Rows - 1, UtilityType.Sewer);
			//}
			vsPeriod.TextMatrix(0, 1, Strings.Format(dblAmt, "#,##0.00"));
			txtInterest.Text = Strings.Format(dblAmt, "#,##0.00");
		}

        public void OwnerNameCallBack(dynamic obj)
        {
            var text = (string) obj;
            lblOwnersName.Text = text;
        }

		private void FillInUTInfo()
		{
			try
            {
                var ownerText = "";
				if (!viewModel.Master.DeedName2.IsNullOrWhiteSpace())
				{
					ownerText =viewModel.Master.DeedName1.Trim() + " & " + viewModel.Master.DeedName2.Trim();
				}
				else
				{
					ownerText = viewModel.Master.DeedName1?.Trim();
				}

                lblOwnersName.Text = ownerText;
                lblOwnersName.Visible = true;
				lblOwnersName.BackColor = Color.White;
				lblMapLot.Text = viewModel.Customer.MapLot;
				lblMapLot.Visible = true;
				lblMapLot.BackColor = Color.White;
				lblLocation.Text = viewModel.Master.StreetNumber.Trim() + " " + viewModel.Master.Apt + " " + viewModel.Master.StreetName;
				if (viewModel.Master.StreetNumber.ToIntegerValue() == 0 && viewModel.Master.StreetName.IsNullOrWhiteSpace())
					lblLocation.Text = "";
				lblLocation.Visible = true;
				lblLocation.BackColor = Color.White;
				lblAccount.BackColor = Color.White;
				
				lblAccount.Text = viewModel.Customer.AccountNumber.ToString();
				this.HeaderText.Text = "Account " + viewModel.Customer.AccountNumber;
				
				lblPaymentInfo.Text = ownerText + " " + lblLocation.Text;

				lblAcctComment.ForeColor = Color.Red;
				ToolTip1.SetToolTip(lblAcctComment, "This account has a comment.");
				lblTaxAcquired.ForeColor = Color.Red;
				ToolTip1.SetToolTip(lblTaxAcquired, "This account has been tax acquired.");
				if (HasAccountComment())
                {
					lblAcctComment.Visible = true;
				}
				else
				{
					lblAcctComment.Visible = false;
				}

				lblTaxAcquired.Visible = viewModel.IsTaxAcquired();
                lblGrpInfo.ToolTipText = "";
				if (viewModel.GroupInfo != null)
				{
					lblGrpInfo.Visible = true;
                    var groupSummary = viewModel.GetGroupSummary(viewModel.EffectiveDate);
                    var comma = "";
                    if (groupSummary != null && groupSummary.AccountSummaries.Any())
                    {
                        if (groupSummary.AccountSummaries.Any())
                        {
                            var groupToolTipText = "Group Balance (";
                            foreach (var accountSummary in groupSummary.AccountSummaries.OrderBy(x => x.AccountType.ToAbbreviation()).ThenBy(y => y.Account))
                            {
                                groupToolTipText += comma + accountSummary.AccountType.ToAbbreviation() + " #" +
                                                    accountSummary.Account;
                                comma = ", ";
                            }
                            groupToolTipText += ") is " + groupSummary.Balance.FormatAsCurrencyNoSymbol();
                            lblGrpInfo.ToolTipText = groupToolTipText;
                        }
                    }
                }
				else
				{
					lblGrpInfo.Visible = false;
				}
				FillACHInfo();

				mnuFileDischarge.Visible = viewModel.LienDischargeExists();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling UT Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowRateInfo(UBBill bill, UtilityType service)
		{
			int lngErrCode = 0;
			try
			{
				var utility = viewModel.GetUtilityDetails(bill, service);

				vsRateInfo.Cols = 1;
				vsRateInfo.Cols = 5;
				vsRateInfo.Rows = 20;
				vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				fraRateInfo.ForeColor = ColorTranslator.FromOle(globalColorSettings.TRIOCOLORBLUE);
				
				vsRateInfo.ColWidth(0, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.25));
				vsRateInfo.ColWidth(1, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.19));
				vsRateInfo.ColWidth(2, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.04));
				vsRateInfo.ColWidth(3, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.23));
				vsRateInfo.ColWidth(4, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.29));
				vsRateInfo.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsRateInfo.Select(0, 0);
				vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;

				if (!bill.IsLien)
				{
					vsRateInfo.TextMatrix(0, 0, "Bill Id");
				}
				else
				{
					vsRateInfo.TextMatrix(0, 0, "Lien Record Number");
				}
				
				vsRateInfo.TextMatrix(1, 0, "RateKey");
				vsRateInfo.TextMatrix(2, 0, "Rate Type");
				vsRateInfo.TextMatrix(3, 0, "");

				vsRateInfo.TextMatrix(4, 0, "Billing Date");
				vsRateInfo.TextMatrix(5, 0, "Creation Date");
				vsRateInfo.TextMatrix(6, 0, "Period Start");
				vsRateInfo.TextMatrix(7, 0, "Period End");
				vsRateInfo.TextMatrix(8, 0, "Interest Date");
				vsRateInfo.TextMatrix(9, 0, "Interest Rate");
				vsRateInfo.TextMatrix(10, 0, "");
				vsRateInfo.TextMatrix(11, 0, "Principal");
				vsRateInfo.TextMatrix(12, 0, "Tax");
				vsRateInfo.TextMatrix(13, 0, "Interest");
				vsRateInfo.TextMatrix(14, 0, "Cost");
				if (bill.BillNumber != 0)
				{
					if (!bill.IsLien)
					{
						vsRateInfo.TextMatrix(15, 0, "Abatement");
					}
					else
					{
						vsRateInfo.TextMatrix(15, 0, "Maturity Fee");
					}
				}
				else
				{
					vsRateInfo.TextMatrix(15, 0, "Abatement");
				}
				vsRateInfo.TextMatrix(16, 0, "Paid");
				vsRateInfo.TextMatrix(17, 0, "");
				vsRateInfo.TextMatrix(18, 0, "Total Due");
				vsRateInfo.TextMatrix(19, 0, "Per Diem");
				if (bill.BillNumber != 0)
				{
					vsRateInfo.TextMatrix(0, 1, bill.Id);
					vsRateInfo.TextMatrix(1, 1, bill.BillNumber);
					if (bill.IsLien)
					{
						vsRateInfo.TextMatrix(2, 1, "Lien");
					}
					else
					{
						vsRateInfo.TextMatrix(2, 1, "Regular");
					}
					vsRateInfo.TextMatrix(3, 1, "");

					vsRateInfo.TextMatrix(4, 1, bill.BillDate != null ? ((DateTime)bill.BillDate).ToString("MM/dd/yyyy") : "");
					vsRateInfo.TextMatrix(5, 1, utility.RateCreatedDate != null ? ((DateTime)utility.RateCreatedDate).ToString("MM/dd/yyyy") : "");
					vsRateInfo.TextMatrix(6, 1, utility.RateStart != null ? ((DateTime)utility.RateStart).ToString("MM/dd/yyyy") : "");
					vsRateInfo.TextMatrix(7, 1, utility.RateEnd != null ? ((DateTime)utility.RateEnd).ToString("MM/dd/yyyy") : "");
					vsRateInfo.TextMatrix(8, 1, utility.InterestStartDate != null ? ((DateTime)utility.InterestStartDate).ToString("MM/dd/yyyy") : "");
					vsRateInfo.TextMatrix(9, 1, utility.InterestRate != 0 ? Strings.Format(utility.InterestRate * 100, "#,##0.00") + "%" : "");
					vsRateInfo.TextMatrix(10, 1, "");
					vsRateInfo.TextMatrix(11, 1, Strings.Format(utility.PrincipalOwed, "#,##0.00"));
					vsRateInfo.TextMatrix(12, 1, Strings.Format(utility.TaxOwed, "#,##0.00"));
					vsRateInfo.TextMatrix(13, 1, Strings.Format(utility.TotalInterest, "#,##0.00"));
					vsRateInfo.TextMatrix(14, 1, Strings.Format(utility.TotalCost, "#,##0.00"));
					vsRateInfo.TextMatrix(15, 1, Strings.Format(utility.AbatementTotal, "#,##0.00"));
					vsRateInfo.TextMatrix(16, 1, Strings.Format(utility.TotalPayment, "#,##0.00"));
					vsRateInfo.TextMatrix(18, 1, Strings.Format(utility.BalanceDue, "#,##0.00"));
					vsRateInfo.TextMatrix(19, 1, Strings.Format(utility.PerDiem, "#,##0.0000"));
				}
				else
				{
					vsRateInfo.TextMatrix(0, 1, "");
					vsRateInfo.TextMatrix(1, 1, "");
					vsRateInfo.TextMatrix(2, 1, "");
					vsRateInfo.TextMatrix(3, 1, "");
					vsRateInfo.TextMatrix(4, 1, "");
					vsRateInfo.TextMatrix(5, 1, "");
					vsRateInfo.TextMatrix(6, 1, "");
					vsRateInfo.TextMatrix(7, 1, "");
					vsRateInfo.TextMatrix(8, 1, "");
					vsRateInfo.TextMatrix(9, 1, "");
					vsRateInfo.TextMatrix(10, 1, "");
					vsRateInfo.TextMatrix(11, 1, "");
					vsRateInfo.TextMatrix(12, 1, "");
					vsRateInfo.TextMatrix(13, 1, "");
					vsRateInfo.TextMatrix(14, 1, "");
					vsRateInfo.TextMatrix(15, 0, "");
					vsRateInfo.TextMatrix(16, 0, "");
					vsRateInfo.TextMatrix(17, 0, "");
					vsRateInfo.TextMatrix(18, 0, "");
					vsRateInfo.TextMatrix(19, 0, "");
				}

				vsRateInfo.TextMatrix(0, 3, "Tenant");
				vsRateInfo.TextMatrix(1, 3, "Address");
				vsRateInfo.TextMatrix(2, 3, "");
				vsRateInfo.TextMatrix(3, 3, "");
				vsRateInfo.TextMatrix(4, 3, "");
				vsRateInfo.TextMatrix(5, 3, "");
				vsRateInfo.TextMatrix(6, 3, "Owner");
				vsRateInfo.TextMatrix(7, 3, "Address");
				vsRateInfo.TextMatrix(8, 3, "");
				vsRateInfo.TextMatrix(9, 3, "");
				vsRateInfo.TextMatrix(10, 3, "");
				vsRateInfo.TextMatrix(11, 3, "Readings");
				vsRateInfo.TextMatrix(12, 3, "Read Dates");

				vsRateInfo.TextMatrix(13, 3, "Interest Paid Date");
				vsRateInfo.TextMatrix(14, 3, "Map Lot");
				vsRateInfo.TextMatrix(15, 3, "Total Per Diem");
				if (bill.FinalBill)
				{
					vsRateInfo.TextMatrix(16, 3, "Final Bill Date");
					vsRateInfo.TextMatrix(17, 3, "Final Bill Start");
					vsRateInfo.TextMatrix(18, 3, "Final Bill End");
				}
				else
				{
					vsRateInfo.TextMatrix(16, 3, "");
					vsRateInfo.TextMatrix(17, 3, "");
					vsRateInfo.TextMatrix(18, 3, "");
				}
				if (bill.IsLien)
				{
					vsRateInfo.TextMatrix(19, 3, "Book / Page");
				}


				vsRateInfo.TextMatrix(0, 4, bill.BilledNameFormatted);
				vsRateInfo.TextMatrix(1, 4, bill.BilledAddress1);
				vsRateInfo.TextMatrix(2, 4, bill.BilledAddress2);
				vsRateInfo.TextMatrix(3, 4, bill.BilledAddress3);
				vsRateInfo.TextMatrix(4, 4, bill.BilledCity + ", " + bill.BilledState + " " + bill.BilledZipFormatted);
				vsRateInfo.TextMatrix(5, 4, "");

				vsRateInfo.TextMatrix(6, 4, bill.OwnerNameFormatted);
				vsRateInfo.TextMatrix(7, 4, bill.OwnerAddress1);
				vsRateInfo.TextMatrix(8, 4, bill.OwnerAddress2);
				vsRateInfo.TextMatrix(9, 4, bill.OwnerAddress3);
				vsRateInfo.TextMatrix(10, 4, bill.OwnerCity + ", " + bill.OwnerState + " " + bill.OwnerZipFormatted);

				vsRateInfo.TextMatrix(11, 4, "Cur:" + bill.CurrentReading + " Prev:" + bill.PreviousReading);

				vsRateInfo.TextMatrix(12, 4,
					"Cur:" + (bill.CurrentReadingDate != null
						? bill.CurrentReadingDate.Value.ToString("MM/dd/yyyy")
						: ""));
				vsRateInfo.TextMatrix(12, 4, vsRateInfo.TextMatrix(12, 4) + " Prev:" + (bill.PreviousReadingDate != null
					? bill.PreviousReadingDate.Value.ToString("MM/dd/yyyy")
					: ""));

				vsRateInfo.TextMatrix(13, 4, utility.LastInterestDate != null ? ((DateTime)utility.LastInterestDate).ToString("MM/dd/yyyy") : "");
				vsRateInfo.TextMatrix(14, 4, lblMapLot.Text);
				vsRateInfo.TextMatrix(15, 4, utility.PerDiem.ToString("#,##0.0000"));

				if (bill.FinalBill)
				{
					vsRateInfo.TextMatrix(16, 4, bill.FinalBillDate != null ? bill.FinalBillDate.ToString() : "");
					vsRateInfo.TextMatrix(17, 4, bill.FinalBillStartDate != null ? bill.FinalBillStartDate.ToString() : "");
					vsRateInfo.TextMatrix(18, 4, bill.FinalBillEndDate != null ? bill.FinalBillEndDate.ToString() : "");
				}
				else
				{
					vsRateInfo.TextMatrix(16, 4, "");
					vsRateInfo.TextMatrix(17, 4, "");
					vsRateInfo.TextMatrix(18, 4, "");
				}
				if (bill.IsLien)
				{
					vsRateInfo.TextMatrix(19, 4, "B" + bill.Book + " P" + bill.Page);
				}
				fraRateInfo.Text = "Rate Information";
				fraRateInfo.Left = FCConvert.ToInt32((this.Width - this.fraRateInfo.Width) / 3.0);
				fraRateInfo.CenterToContainer(this.ClientArea);
				fraRateInfo.Visible = true;
				vsRateInfo.SendToBack();
				mnuFilePrintRateInfo.Visible = true;
				cmdRIClose.Focus();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error" + " - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowPaymentInfo(UBPayment payment)
		{
			try
			{
				var bill = viewModel.GetBill(payment);
				var utility = viewModel.GetUtilityDetails(payment);
				if (bill != null && utility != null)
				{
					if (!bill.IsLien)
					{
						vsRateInfo.Cols = 5;
						vsRateInfo.Rows = 20;
						vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
						vsRateInfo.ColWidth(0, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.3));
						vsRateInfo.ColWidth(1, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.19));
						vsRateInfo.ColWidth(2, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.01));
						vsRateInfo.ColWidth(3, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.3));
						vsRateInfo.ColWidth(4, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.19));
						vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
						// this will hide the border of the other information on the shared grid
						vsRateInfo.Select(19, 0, 19, 1);
						vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
						vsRateInfo.Select(15, 0, 15, 1);
						vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
						vsRateInfo.Select(0, 0);
						vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 19, 1, 19, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
						vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
						// show the first set of information (rate information and period info)--------------
						vsRateInfo.TextMatrix(0, 0, "Account");
						vsRateInfo.TextMatrix(1, 0, "");
						vsRateInfo.TextMatrix(2, 0, "");
						vsRateInfo.TextMatrix(3, 0, "Bill ID");
						vsRateInfo.TextMatrix(4, 0, "Receipt Number");
						vsRateInfo.TextMatrix(5, 0, "");
						vsRateInfo.TextMatrix(6, 0, "Recorded Transaction");
						vsRateInfo.TextMatrix(7, 0, "Effective Interest Date");
						vsRateInfo.TextMatrix(8, 0, "Actual Transaction Date");
						vsRateInfo.TextMatrix(9, 0, "");
						vsRateInfo.TextMatrix(10, 0, "Teller ID");
						vsRateInfo.TextMatrix(11, 0, "");
						vsRateInfo.TextMatrix(12, 0, "Cash Drawer");
						vsRateInfo.TextMatrix(13, 0, "Affect Cash");
						vsRateInfo.TextMatrix(14, 0, "BD Account");
						vsRateInfo.TextMatrix(0, 3, "ID");
						vsRateInfo.TextMatrix(1, 3, "Reference");
						vsRateInfo.TextMatrix(2, 3, "Paid By");
						vsRateInfo.TextMatrix(3, 3, "Daily Close Out");
						vsRateInfo.TextMatrix(4, 3, "");
						vsRateInfo.TextMatrix(5, 3, "Principal");
						vsRateInfo.TextMatrix(6, 3, "Tax");
						vsRateInfo.TextMatrix(7, 3, "Interest");
						vsRateInfo.TextMatrix(8, 3, "Pre Lien Interest");
						vsRateInfo.TextMatrix(9, 3, "Lien Costs");
						vsRateInfo.TextMatrix(10, 3, "");
						vsRateInfo.TextMatrix(11, 3, "Total");
						vsRateInfo.TextMatrix(19, 0, "Comments");
						if (payment != null)
						{
							vsRateInfo.TextMatrix(0, 1, viewModel.Master.AccountNumber);
							vsRateInfo.TextMatrix(2, 1, "");
							vsRateInfo.TextMatrix(3, 1, payment.BillId);
							vsRateInfo.TextMatrix(4, 1, viewModel.GetPaymentReceiptNumber(payment.ReceiptId ?? 0, payment.ActualSystemDate));
						
							vsRateInfo.TextMatrix(5, 1, "");
							vsRateInfo.TextMatrix(6, 1, ((DateTime)payment.RecordedTransactionDate).ToString("MM/dd/yyyy"));
							vsRateInfo.TextMatrix(7, 1, ((DateTime)payment.EffectiveInterestDate).ToString("MM/dd/yyyy"));
							vsRateInfo.TextMatrix(8, 1, ((DateTime)payment.ActualSystemDate).ToString("MM/dd/yyyy"));
							vsRateInfo.TextMatrix(9, 1, "");
							vsRateInfo.TextMatrix(10, 1, payment.Teller);
							if (payment.CashDrawer == "Y")
							{
								vsRateInfo.TextMatrix(12, 1, "YES");
								vsRateInfo.TextMatrix(13, 1, "YES");
							}
							else
							{
								vsRateInfo.TextMatrix(12, 1, "NO");
								if (payment.GeneralLedger == "Y")
								{
									vsRateInfo.TextMatrix(13, 1, "YES");
								}
								else
								{
									vsRateInfo.TextMatrix(13, 1, "NO");
									vsRateInfo.TextMatrix(14, 1, payment.BudgetaryAccountNumber);
								}
							}
							vsRateInfo.TextMatrix(0, 4, payment.Id);
							vsRateInfo.TextMatrix(1, 4, payment.Reference);
							vsRateInfo.TextMatrix(2, 4, payment.PaidBy);
							if ((payment.DailyCloseOut ?? 0) != 0)
							{
								vsRateInfo.TextMatrix(3, 4, "True");
							}
							else
							{
								vsRateInfo.TextMatrix(3, 4, "False");
							}
							vsRateInfo.TextMatrix(4, 4, "");
							vsRateInfo.TextMatrix(5, 4, payment.Principal.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(6, 4, payment.Tax.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(7, 4, payment.CurrentInterest.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(8, 4, payment.PreLienInterest.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(9, 4, payment.Cost.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(10, 4, "");
							vsRateInfo.TextMatrix(11, 4, payment.PaymentTotal.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(19, 1, payment.Comments);
							vsRateInfo.TextMatrix(19, 2, vsRateInfo.TextMatrix(19, 1));
							vsRateInfo.TextMatrix(19, 3, vsRateInfo.TextMatrix(19, 1));
							vsRateInfo.TextMatrix(19, 4, vsRateInfo.TextMatrix(19, 1));
							vsRateInfo.MergeRow(19, true, 1, 4);
						}
						else
						{
							vsRateInfo.TextMatrix(0, 1, "");
							vsRateInfo.TextMatrix(1, 1, "");
							vsRateInfo.TextMatrix(2, 1, "");
							vsRateInfo.TextMatrix(3, 1, "");
							vsRateInfo.TextMatrix(4, 1, "");
							vsRateInfo.TextMatrix(5, 1, "");
							vsRateInfo.TextMatrix(6, 1, "");
							vsRateInfo.TextMatrix(7, 1, "");
							vsRateInfo.TextMatrix(8, 1, "");
							vsRateInfo.TextMatrix(9, 1, "");
							vsRateInfo.TextMatrix(0, 4, "");
							vsRateInfo.TextMatrix(1, 4, "");
							vsRateInfo.TextMatrix(2, 4, "");
							vsRateInfo.TextMatrix(3, 4, "");
							vsRateInfo.TextMatrix(4, 4, "");
							vsRateInfo.TextMatrix(5, 0, "");
							vsRateInfo.TextMatrix(6, 0, "");
							vsRateInfo.TextMatrix(7, 0, "");
							vsRateInfo.TextMatrix(8, 0, "");
							vsRateInfo.TextMatrix(19, 0, "");
						}
						// show the frame-----------------------------------------------------
						fraRateInfo.Text = "Payment Information";
						fraRateInfo.Left = FCConvert.ToInt32((this.Width - this.fraRateInfo.Width) / 3.0);
						fraRateInfo.CenterToContainer(this.ClientArea);
						fraRateInfo.Visible = true;
						vsRateInfo.SendToBack();
						cmdRIClose.Focus();
					}
					else
					{
						if (bill != null && utility != null)
						{
							vsRateInfo.Cols = 5;
							vsRateInfo.Rows = 21;
							vsRateInfo.ColWidth(0, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.4));
							vsRateInfo.ColWidth(1, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.585));
							vsRateInfo.ColWidth(2, this.vsRateInfo.WidthOriginal * 0);
							vsRateInfo.ColWidth(3, this.vsRateInfo.WidthOriginal * 0);
							vsRateInfo.ColWidth(4, this.vsRateInfo.WidthOriginal * 0);
							vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							vsRateInfo.CellBorder(Color.Black, 0, 1, 0, 0, 0, 1);
							vsRateInfo.Select(15, 0, 15, 1);
							vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
							vsRateInfo.Select(0, 0);
							vsRateInfo.ExtendLastCol = true;
							vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, this.vsRateInfo.Rows - 1, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
							// show the first set of information (rate information and period info)--------------
							vsRateInfo.TextMatrix(0, 0, "Lien Record Number");
							vsRateInfo.TextMatrix(1, 0, "Rate ID");
							vsRateInfo.TextMatrix(2, 0, "Status");
							vsRateInfo.TextMatrix(3, 0, "Date Created");
							vsRateInfo.TextMatrix(4, 0, "Interest Applied Through");
							vsRateInfo.TextMatrix(5, 0, "Book / Page");
							vsRateInfo.TextMatrix(6, 0, "Principal");
							vsRateInfo.TextMatrix(7, 0, "Tax");
							vsRateInfo.TextMatrix(8, 0, "Interest");
							vsRateInfo.TextMatrix(9, 0, "Costs");
							vsRateInfo.TextMatrix(10, 0, "Interest Charged");
							vsRateInfo.TextMatrix(11, 0, "Maturity Fee");
							vsRateInfo.TextMatrix(12, 0, "Lien Total");
							vsRateInfo.TextMatrix(13, 0, "");
							vsRateInfo.TextMatrix(14, 0, "Principal Paid");
							vsRateInfo.TextMatrix(15, 0, "Tax Paid");
							vsRateInfo.TextMatrix(16, 0, "Interest Paid");
							vsRateInfo.TextMatrix(17, 0, "Costs Paid");
							vsRateInfo.TextMatrix(18, 0, "Total Paid");
							vsRateInfo.TextMatrix(19, 0, "");
							vsRateInfo.TextMatrix(20, 0, "Total Due");

							vsRateInfo.TextMatrix(0, 1, bill.Id);
							vsRateInfo.TextMatrix(1, 1, bill.BillNumber);
							vsRateInfo.TextMatrix(2, 1, bill.BillStatus);
							vsRateInfo.TextMatrix(3, 1, ((DateTime)utility.RateCreatedDate).ToString("MM/dd/yyyy"));
							vsRateInfo.TextMatrix(4, 1, ((DateTime)utility.LastInterestDate).ToString("MM/dd/yyyy"));
							if (bill.Book != 0 || bill.Page != 0)
							{
								vsRateInfo.TextMatrix(5, 1, "B" + bill.Book + "P" + bill.Page);
							}
							else
							{
								vsRateInfo.TextMatrix(5, 1, "");
							}
							vsRateInfo.TextMatrix(6, 1, utility.PrincipalOwed.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(7, 1, utility.TaxOwed.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(8, 1, utility.IntOwed.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(9, 1, utility.CostOwed.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(10, 1, (utility.IntAdded * -1).ToString("#,##0.00"));
							vsRateInfo.TextMatrix(11, 1, (utility.MaturityFee * -1).ToString("#,##0.00"));
							vsRateInfo.TextMatrix(12, 1, (utility.PrincipalOwed + utility.TaxOwed + utility.CostOwed + utility.IntOwed - utility.IntAdded - utility.MaturityFee).ToString("#,##0.00"));
							vsRateInfo.TextMatrix(13, 1, "");
							vsRateInfo.TextMatrix(14, 1, utility.PrincipalPaid.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(15, 1, utility.TaxPaid.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(16, 1, (utility.PreLienInterestPaid + utility.IntPaid).ToString("#,##0.00"));
							vsRateInfo.TextMatrix(17, 1, utility.CostPaid.ToString("#,##0.00"));
							vsRateInfo.TextMatrix(18, 1, (utility.PrincipalPaid + utility.CostPaid + utility.IntPaid + utility.PreLienInterestPaid + utility.TaxPaid).ToString("#,##0.00"));
							vsRateInfo.TextMatrix(19, 1, "");
							vsRateInfo.TextMatrix(20, 1, utility.BalanceDue.ToString("#,##0.00"));
							// show the frame-----------------------------------------------------
							fraRateInfo.Text = "Lien Information";
							fraRateInfo.Left = FCConvert.ToInt32((Width - fraRateInfo.Width) / 3.0);
							fraRateInfo.CenterToContainer(ClientArea);
							fraRateInfo.Visible = true;
							vsRateInfo.SendToBack();
							cmdRIClose.Focus();
						}
					}
				}
				mnuFilePrintRateInfo.Visible = true;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ShowAccountInfo()
		{
			try
            {
                vsRateInfo.Rows = 0;
				vsRateInfo.Cols = 1;
				vsRateInfo.Cols = 2;
				vsRateInfo.Rows = 20;
				vsRateInfo.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				vsRateInfo.ExtendLastCol = true;
				vsRateInfo.ColWidth(0, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.32));
				vsRateInfo.ColWidth(1, FCConvert.ToInt32(this.vsRateInfo.WidthOriginal * 0.65));
				vsRateInfo.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);

				vsRateInfo.Select(19, 0, 19, 1);
				vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
				vsRateInfo.Select(15, 0, 15, 1);
				vsRateInfo.CellBorder(Color.Black, 0, 0, 0, 0, 0, 0);
				vsRateInfo.Select(0, 0);
				vsRateInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, this.vsRateInfo.Rows - 1, 0, this.vsRateInfo.Rows - 1, this.vsRateInfo.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsRateInfo.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;

				vsRateInfo.TextMatrix(0, 0, "Current Owner:");
				vsRateInfo.TextMatrix(1, 0, "Second Owner:");
				vsRateInfo.TextMatrix(2, 0, "Location:");
				vsRateInfo.TextMatrix(3, 0, "Billing Address:");
				vsRateInfo.TextMatrix(4, 0, "");
				vsRateInfo.TextMatrix(5, 0, "");
				vsRateInfo.TextMatrix(6, 0, "");

				vsRateInfo.TextMatrix(7, 0, "");
				vsRateInfo.TextMatrix(8, 0, "Owner Address:");
				vsRateInfo.TextMatrix(9, 0, "");
				vsRateInfo.TextMatrix(10, 0, "");
				vsRateInfo.TextMatrix(11, 0, "");
				vsRateInfo.TextMatrix(12, 0, "");
				vsRateInfo.TextMatrix(13, 0, "Map Lot:");
				vsRateInfo.TextMatrix(14, 0, "Book Page(s):");
				vsRateInfo.TextMatrix(15, 0, "");
				vsRateInfo.TextMatrix(16, 0, "Tax Acquired:");
				vsRateInfo.TextMatrix(17, 0, "Associated RE Account:");
				vsRateInfo.TextMatrix(18, 0, "Account Group:");
				vsRateInfo.TextMatrix(19, 0, "Comment:");
				vsRateInfo.TextMatrix(0, 1, viewModel.Master.DeedName1?.Trim() ?? String.Empty);
				vsRateInfo.TextMatrix(1, 1, viewModel.Master.DeedName2?.Trim() ?? String.Empty);
				vsRateInfo.TextMatrix(2, 1, viewModel.Master.StreetNumber + " " + viewModel.Master.Apt + " " + viewModel.Master.StreetName);
				// location
				vsRateInfo.TextMatrix(3, 1, viewModel.Customer.BilledAddress1);
				vsRateInfo.TextMatrix(4, 1, viewModel.Customer.BilledAddress2);
				vsRateInfo.TextMatrix(5, 1, viewModel.Customer.BilledAddress3);
				vsRateInfo.TextMatrix(6, 1, viewModel.Customer.BilledCity + ", " + viewModel.Customer.BilledState + " " + viewModel.Customer.BilledZip);
				vsRateInfo.TextMatrix(7, 1, "");
				// blank line
				vsRateInfo.TextMatrix(8, 1, viewModel.Customer.OwnerAddress1);
				vsRateInfo.TextMatrix(9, 1, viewModel.Customer.OwnerAddress2);
				vsRateInfo.TextMatrix(10, 1, viewModel.Customer.OwnerAddress3);
				vsRateInfo.TextMatrix(11, 1, viewModel.Customer.OwnerCity + ", " + viewModel.Customer.OwnerState + " " + viewModel.Customer.OwnerZip);
				vsRateInfo.TextMatrix(12, 1, "");
				// blank line
				vsRateInfo.TextMatrix(13, 1, viewModel.Master.MapLot);
				vsRateInfo.TextMatrix(14, 1, viewModel.Master.BookPage);
				vsRateInfo.TextMatrix(15, 1, "");
				// blank line
				vsRateInfo.TextMatrix(16, 1, viewModel.IsTaxAcquired() ? "TRUE" : "FALSE");
				if (viewModel.ModuleAssociationInfo != null)
				{
					this.vsRateInfo.TextMatrix(17, 1, viewModel.ModuleAssociationInfo.REMasterAcct);
				}
				else
				{
					this.vsRateInfo.TextMatrix(17, 1, "NONE");
				}
				vsRateInfo.TextMatrix(18, 1, viewModel.GroupInfo?.GroupNumber ?? 0);
				vsRateInfo.TextMatrix(19, 1, viewModel.AccountNote?.Text);
				// show the frame-----------------------------------------------------
				fraRateInfo.Text = "Utility Account Information";
				Focus();
				fraRateInfo.Visible = true;
				fraRateInfo.CenterToContainer(this.ClientArea);
				fraRateInfo.BringToFront();
				mnuFilePrintRateInfo.Visible = true;
				cmdRIClose.Focus();
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Rate Information Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CheckCD()
		{
			if (txtCD.Text == "N")
			{
				if (!viewModel.IsCashReceiptsActive() && !viewModel.BudgetaryExists)
				{
					txtCash.Enabled = false;
				}
				else
				{
					txtCash.Enabled = true;
					txtCash.TabStop = true;
				}
			}
			else
			{
				if (!viewModel.IsCashReceiptsActive() && !viewModel.BudgetaryExists)
				{
					txtCash.Enabled = false;
				}
				else
				{
					if (txtCash.Text == "N")
						txtCash.Text = "Y";
					txtCash.TabStop = false;
					txtCash.Enabled = false;
				}
				txtAcctNumber.Enabled = false;
				txtAcctNumber.TabStop = false;
			}
		}

		private void CheckCash()
		{
			if (txtCD.Text == "N")
			{
				if (txtCash.Text == "N")
				{
					txtAcctNumber.Enabled = true;
					txtAcctNumber.TabStop = true;
				}
				else
				{
					txtAcctNumber.Enabled = false;
					txtAcctNumber.TabStop = false;
				}
			}
			else
			{
				if (txtCash.Text == "N")
					txtCash.Text = "Y";
			}
		}

		private void AddPaymentRow(UBPayment payment)
		{
			vsPayments.AddItem("");

			var bill = viewModel.GetBill(payment);
			var utility = viewModel.GetUtilityDetails(payment);

			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColBill, bill.BillNumber);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColDate, ((DateTime)payment.RecordedTransactionDate).ToString("MM/dd/yyyy"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColRef, payment.Reference);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColService, utility.ServiceCode);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCode, payment.Code);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCDAC, payment.CashDrawer + "   " + payment.GeneralLedger);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColPrincipal, Strings.Format(payment.Principal, "#,##0.00"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColTax, Strings.Format(payment.Tax, "#,##0.00"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColInterest, Strings.Format(payment.InterestTotal, "#,##0.00"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCosts, Strings.Format(payment.Cost, "#,##0.00"));
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColKeyNumber, payment.PendingId);
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCHGINTNumber, "");
			vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColTotal, Strings.Format(payment.Principal + payment.Tax + payment.InterestTotal + payment.Cost, "#,##0.00"));

			if (payment.ChargedInterestRecord != null)
			{
				vsPayments.AddItem("");
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColBill, bill.BillNumber);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColDate, ((DateTime)payment.ChargedInterestRecord.RecordedTransactionDate).ToString("MM/dd/yyyy"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColRef, payment.ChargedInterestRecord.Reference);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColService, utility.ServiceCode);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCode, payment.ChargedInterestRecord.Code);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCDAC, payment.ChargedInterestRecord.CashDrawer + "   " + payment.GeneralLedger);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColPrincipal, Strings.Format(payment.ChargedInterestRecord.Principal, "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColTax, Strings.Format(payment.ChargedInterestRecord.Tax, "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColInterest, Strings.Format(payment.ChargedInterestRecord.InterestTotal, "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCosts, Strings.Format(payment.ChargedInterestRecord.Cost, "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColKeyNumber, payment.PendingId);
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColCHGINTNumber, "");
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngPayGridColTotal, Strings.Format(payment.ChargedInterestRecord.Principal + payment.ChargedInterestRecord.Tax + payment.ChargedInterestRecord.InterestTotal + payment.ChargedInterestRecord.Cost, "#,##0.00"));
			}
		}

		private void vsRateInfo_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }

            if (e.ColumnIndex >= vsRateInfo.Cols)
            {
                return;
            }

            if (e.RowIndex >= vsRateInfo.Rows)
            {
                return;
            }
            DataGridViewCell cell = vsRateInfo[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = vsRateInfo.GetFlexRowIndex(e.RowIndex);
			lngMC = vsRateInfo.GetFlexColIndex(e.ColumnIndex);
			if (lngMR >= 0 && lngMC >= 0)
			{
                cell.ToolTipText = vsRateInfo.TextMatrix(lngMR, lngMC);
			}
		}

		private void InputBoxAdjustment(bool boolCollapse)
		{
			if (txtPrincipal.Text.ToDecimalValue() == 0)
				txtPrincipal.Text = "0.00";
			if (txtInterest.Text.ToDecimalValue() == 0)
				txtInterest.Text = "0.00";
			if (txtCosts.Text.ToDecimalValue() == 0)
				txtCosts.Text = "0.00";
			if (txtTax.Text.ToDecimalValue() == 0)
				txtTax.Text = "0.00";

			txtPrincipal.Visible = !boolCollapse;
			txtCosts.Visible = !boolCollapse;
			txtTax.Visible = !boolCollapse;
			lblPrincipal.Visible = !boolCollapse;
			lblCosts.Visible = !boolCollapse;
			lblTax.Visible = !boolCollapse;
			if (boolCollapse)
			{
				txtPrincipal.Text = "0.00";
				txtCosts.Text = "0.00";
				txtTax.Text = "0.00";
				txtInterest.Text = (txtInterest.Text.ToDecimalValue() + txtPrincipal.Text.ToDecimalValue() + txtCosts.Text.ToDecimalValue() + txtTax.Text.ToDecimalValue()).ToString("#,##0.00");
				lblInterest.Text = "Amount";
			}
			else
			{
				lblInterest.Text = "Interest";
			}
		}

		private void SGRID_ClickEvent(object sender, System.EventArgs e)
		{
			GridClick(UtilityType.Sewer);
		}

		private void GridCollapse(UtilityType service)
		{
			FCGrid grid = SetGrid(service);

			int counter;
			int rows = 0;
			bool DeptFlag = false;
			bool DivisionFlag = false;

			if (viewModel.DontFireCollapse == false)
			{
				for (counter = 1; counter <= grid.Rows - 1; counter++)
				{
					if (grid.RowOutlineLevel(counter) == 0)
					{
						if (grid.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DeptFlag = true;
						}
						else
						{
							rows += 1;
							DeptFlag = false;
						}
					}
					else if (grid.RowOutlineLevel(counter) == 1)
					{
						if (DeptFlag == true)
						{
							// do nothing
						}
						else if (grid.IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							rows += 1;
							DivisionFlag = true;
							if (grid.TextMatrix(counter, lngGRIDColRef) == "Original" || Strings.Left(grid.TextMatrix(counter, lngGRIDColRef), 3) == Strings.Left(strDemandBillString, 3) || Strings.Left(grid.TextMatrix(counter, lngGRIDColRef), 4) == Strings.Left(strSupplemntalBillString, 4))
							{
								SwapRows(counter, NextSpacerLine(counter, grid), grid);
							}
						}
						else
						{
							rows += 1;
							DivisionFlag = false;
							if (grid.TextMatrix(counter, lngGRIDColRef) != "Original" && Strings.Left(grid.TextMatrix(counter, lngGRIDColRef), 4) != Strings.Left(strSupplemntalBillString, 4) && Strings.Left(grid.TextMatrix(counter, lngGRIDColRef), 3) != Strings.Left(strDemandBillString, 3))
							{
								SwapRows(counter, NextSpacerLine(counter, grid), grid);
							}
						}
					}
					else
					{
						if (DeptFlag == true || DivisionFlag == true)
						{
							// do nothing
						}
						else
						{
							rows += 1;
						}
					}
				}
			}
		}

		private void SGRID_Collapsed(int row, bool isCollapsed)
		{
			GridCollapse(UtilityType.Sewer);
		}

		private void SGRID_DblClick(object sender, System.EventArgs e)
		{
			GridDoubleClick(UtilityType.Sewer);
		}

		private void SGRID_Enter(object sender, System.EventArgs e)
		{
			if (fraRateInfo.Visible == true)
				fraRateInfo.Visible = false;
		}

		private void GridKeyPress(UtilityType service, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			if (keyAscii == 32)
			{
				// spacebar
				keyAscii = 0;
				GridDoubleClick(service);
			}
			else if (keyAscii == 13 && SGRID.RowOutlineLevel(SGRID.Row) == 1)
			{
				keyAscii = 0;
				GridClick(service);
			}
		}

		private void SGRID_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			GridKeyPress(UtilityType.Sewer, e);
		}

		private void SGRID_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			GridMouseDown(UtilityType.Sewer, e);
		}

		private void GridMouseMove(UtilityType service, DataGridViewCellFormattingEventArgs e)
		{
			FCGrid grid = SetGrid(service);

			string strTemp = "";
			int lngMR = 0;
			int lngMC = 0;
			lngMR = e.RowIndex;
			lngMC = e.ColumnIndex;

			if (lngMR > -1 && lngMC > -1)
			{
				DataGridViewCell cell = grid[lngMC, lngMR];
				if (lngMR == 0)
				{
					if (lngMC == lngGRIDCOLYear || lngMC == lngGRIDColBillNumber)
					{
						// Year
						if (lngMR == grid.Rows - 1)
						{
							strTemp = "Effective Date : " + grid.TextMatrix(lngMR, lngMC);
						}
						else
						{
							strTemp = "";
						}
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColDate)
					{
						// Date
						strTemp = "Recorded Date";
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColRef)
					{
						// Reference
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColService)
					{
						// Period
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColPaymentCode)
					{
						// Code
						strTemp += " A - Abatement" + ", ";
						strTemp += " C - Correction" + ", ";
						strTemp += " I - Interest" + ", ";
						strTemp += " P - Payment" + ", ";
						strTemp += " R - Refunded Abatement" + ", ";
						strTemp += " Y - PrePayment";
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColPTC)
					{
						strTemp = "Principal + Tax + Costs";
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColPrincipal)
					{
						// Prin
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColInterest || lngMC == lngGRIDColTax)
					{
						// Int
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColCosts)
					{
						// Costs
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else if (lngMC == lngGRIDColTotal)
					{
						// Total
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
					else
					{
						strTemp = "";
						cell.ToolTipText = strTemp;
					}
				}
				else
				{
					lngMR = grid.MouseRow;
					if (lngMR > 0 && lngMR < grid.Rows)
					{
						if (grid.TextMatrix(lngMR, lngGRIDColLineCode) != "" && grid.TextMatrix(lngMR, lngGRIDColLineCode) != "=")
						{
							if (grid.TextMatrix(lngMR, lngGRIDColRef) != "CURINT" && grid.TextMatrix(lngMR, lngGRIDColRef) != "EARNINT")
							{
								if (grid.RowOutlineLevel(lngMR) == 1 && grid.MouseCol == lngGRIDColBillNumber)
								{
									if (grid.MouseCol == lngGRIDColBillNumber && grid.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
									{
										strTemp = viewModel.GetUtilityDetails(grid.TextMatrix(lngMR, lngGRIDColBillNumber).ToIntegerValue(), service).RateDescription;
										cell.ToolTipText = strTemp;
									}
									else
									{
										cell.ToolTipText = "Right-Click for Rate information and totals.";
									}
								}
								else if (grid.RowOutlineLevel(lngMR) == 2)
								{
									cell.ToolTipText = "Right-Click for Payment information.";
								}
								else
								{
									if (grid.MouseCol == lngGRIDColBillNumber && grid.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
									{
										strTemp = viewModel.GetUtilityDetails(grid.TextMatrix(lngMR, lngGRIDColBillNumber).ToIntegerValue(), service).RateDescription;
										cell.ToolTipText = strTemp;
									}
								}
							}
							else
							{
								cell.ToolTipText = "";
							}
						}
						else
						{
							if (grid.MouseCol == lngGRIDColBillNumber && grid.TextMatrix(lngMR, lngGRIDColBillNumber) != "")
							{
								strTemp = viewModel.GetUtilityDetails(grid.TextMatrix(lngMR, lngGRIDColBillNumber).ToIntegerValue(), service).RateDescription;
								cell.ToolTipText = strTemp;
							}
						}
					}
					else
					{
						cell.ToolTipText = "";
					}
				}
			}
		}

		private void SGRID_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			GridMouseMove(UtilityType.Sewer, e);
		}
		
		public UtilityType FindPaymentType(string serviceCode = "")
		{
			try
			{
				if (serviceCode.Trim() == "")
				{
					serviceCode = cmbService.Text.Left(1);
				}

				if (serviceCode == "W")
				{
					return UtilityType.Water;
				}
				else if (serviceCode == "S")
				{
					return UtilityType.Sewer;
				}
				else 
				{
					if (viewModel.PayWaterFirst)
					{
						if (!viewModel.HasWaterBill() && viewModel.HasSewerBill())
						{
							return UtilityType.Sewer;
						}
						else
						{
							return UtilityType.Water;
						}
					}
					else
					{
						if (viewModel.HasWaterBill() && !viewModel.HasSewerBill())
						{
							return UtilityType.Water;
						}
						else
						{
							return UtilityType.Sewer;
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Payment Type", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return UtilityType.None;
			}
		}

		private string GetServiceCode(UtilityType service)
		{
			if (service == UtilityType.Water)
			{
				return "W";
			}
			else
			{
				return "S";
			}
		}

		private void SetService(UtilityType service)
		{
			try
			{
				var serviceCode = GetServiceCode(service);

				for (int intCT = 0; intCT <= cmbService.Items.Count - 1; intCT++)
				{
					if (Strings.UCase(serviceCode) == Strings.UCase(Strings.Left(cmbService.Items[intCT].ToString(), 1)))
					{
						boolDoNotSetService = true;
						cmbService.SelectedIndex = intCT;
						boolDoNotSetService = false;
						break;
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Service", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetHeadingLabels()
        {
            lblWaterHeading.Visible = waterGridPanel.Visible;
            lblWaterHeading.Left = waterGridPanel.Left;
            lblWaterHeading.Width = waterGridPanel.Width;
            lblWaterHeading.Top = waterGridPanel.Top - lblWaterHeading.Height;
            lblSewerHeading.Visible = sewerGridPanel.Visible;
            lblSewerHeading.Left = sewerGridPanel.Left;
            lblSewerHeading.Width = sewerGridPanel.Width;
            lblSewerHeading.Top = sewerGridPanel.Top - lblSewerHeading.Height;
        }

		private void FillACHInfo()
		{
			if (viewModel.AccountACHInfo != null)
			{
				if (viewModel.AccountACHInfo.ACHActive ?? false)
				{
					if (viewModel.AccountACHInfo.ACHPrenote ?? false)
					{
						lblACHInfo.ToolTipText = "AutoPay is in PreNote status";
					}
					else
					{
						lblACHInfo.ToolTipText = "AutoPay is Active";
					}

					lblACHInfo.Visible = true;
				}
				else
				{
					lblACHInfo.Visible = false;
				}
			}
			else
			{
				lblACHInfo.Visible = false;
			}
		}

		private void lblGrpInfo_Click(object sender, EventArgs e)
		{
			frmGroupBalance.InstancePtr.Init(viewModel.GroupInfo.ID, ViewModel.EffectiveDate);
		}

		private void frmUBPaymentStatus_FormClosing(object sender, Wisej.Web.FormClosingEventArgs e)
		{
			if (cancelling)
			{
				ViewModel.Cancel();
			}
		}

		private void frmUBPaymentStatus_Shown(object sender, EventArgs e)
		{
			ResetUBPaymentFrame(true);
		}

        private void txtAcctNumber_Click(object sender, EventArgs e)
        {

        }
        private void AddSewerTotalToPay(decimal sewerTotal)
        {
            SetService(UtilityType.Sewer);
            SetBillNumberCombo("Auto");
            SetCodeCombo("P");
            vsPeriod.TextMatrix(0, 1, Strings.Format(sewerTotal, "#,##0.00"));
            txtInterest.Text = Strings.Format(sewerTotal, "#,##0.00");
        }

        private void AddWaterTotalToPay(decimal waterTotal)
        {
            SetService(UtilityType.Water);
            SetBillNumberCombo("Auto");
            SetCodeCombo("P");
            vsPeriod.TextMatrix(0, 1, Strings.Format(waterTotal, "#,##0.00"));
            txtInterest.Text = Strings.Format(waterTotal, "#,##0.00");
        }
	}
}
