﻿using fecherFoundation;

namespace TWUT0000.Receipting
{
	partial class frmBatchPayment
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Wisej Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fraRecover = new fecherFoundation.FCFrame();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.lblRecover = new fecherFoundation.FCLabel();
			this.cmbBatchList = new fecherFoundation.FCComboBox();
			this.cmdBatchChoice = new fecherFoundation.FCButton();
			this.fraBatch = new fecherFoundation.FCFrame();
			this.Label11 = new fecherFoundation.FCLabel();
			this.txtTotal = new fecherFoundation.FCTextBox();
			this.cmdProcessBatch = new fecherFoundation.FCButton();
			this.vsBatch = new fecherFoundation.FCGrid();
			this.txtBillNumber = new fecherFoundation.FCTextBox();
			this.lblBillNumber = new fecherFoundation.FCLabel();
			this.cmbSewer = new fecherFoundation.FCComboBox();
			this.txtAmount = new fecherFoundation.FCTextBox();
			this.Label10 = new fecherFoundation.FCLabel();
			this.txtBatchAccount = new fecherFoundation.FCTextBox();
			this.Label9 = new fecherFoundation.FCLabel();
			this.lblLocation = new fecherFoundation.FCLabel();
			this.lblName = new fecherFoundation.FCLabel();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.fraBatchQuestions = new fecherFoundation.FCFrame();
			this.cmdBatch = new fecherFoundation.FCButton();
			this.txtPaidBy = new fecherFoundation.FCTextBox();
			this.fcLabel3 = new fecherFoundation.FCLabel();
			this.lblEffectiveDate = new fecherFoundation.FCLabel();
			this.lblPaymentDate = new fecherFoundation.FCLabel();
			this.txtEffectiveDate = new Global.T2KDateBox();
			this.txtPaymentDate = new Global.T2KDateBox();
			this.txtTellerID = new fecherFoundation.FCTextBox();
			this.label8 = new fecherFoundation.FCLabel();
			this.chkReceipt = new fecherFoundation.FCCheckBox();
			this.cmdSaveBatch = new fecherFoundation.FCButton();
			this.fraValidate = new fecherFoundation.FCFrame();
			this.lblValidate = new fecherFoundation.FCLabel();
			this.cmdValidateCancel = new fecherFoundation.FCButton();
			this.cmdValidateNo = new fecherFoundation.FCButton();
			this.cmdValidateYes = new fecherFoundation.FCButton();
			this.fcMenuStrip1 = new fecherFoundation.FCMenuStrip();
			this.mnuBatchRecover = new fecherFoundation.FCToolStripMenuItem();
			this.mnuBatchPurge = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileImportPayPortBatch = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRecover)).BeginInit();
			this.fraRecover.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBatchChoice)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBatch)).BeginInit();
			this.fraBatch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessBatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsBatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBatchQuestions)).BeginInit();
			this.fraBatchQuestions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveBatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraValidate)).BeginInit();
			this.fraValidate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdValidateCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdValidateNo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdValidateYes)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveBatch);
			this.BottomPanel.Location = new System.Drawing.Point(0, 630);
			this.BottomPanel.Size = new System.Drawing.Size(1212, 109);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraRecover);
			this.ClientArea.Controls.Add(this.fraBatchQuestions);
			this.ClientArea.Controls.Add(this.fraBatch);
			this.ClientArea.Controls.Add(this.fraValidate);
			this.ClientArea.Size = new System.Drawing.Size(1212, 570);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(1212, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(191, 30);
			this.HeaderText.Text = "Batch Payments";
			// 
			// fraRecover
			// 
			this.fraRecover.Controls.Add(this.cmdCancel);
			this.fraRecover.Controls.Add(this.lblRecover);
			this.fraRecover.Controls.Add(this.cmbBatchList);
			this.fraRecover.Controls.Add(this.cmdBatchChoice);
			this.fraRecover.Location = new System.Drawing.Point(21, 6);
			this.fraRecover.Name = "fraRecover";
			this.fraRecover.Size = new System.Drawing.Size(392, 176);
			this.fraRecover.TabIndex = 6;
			this.fraRecover.Text = "Batch Recover";
			this.fraRecover.Visible = false;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.Location = new System.Drawing.Point(224, 117);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(80, 40);
			this.cmdCancel.TabIndex = 4;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// lblRecover
			// 
			this.lblRecover.Location = new System.Drawing.Point(20, 35);
			this.lblRecover.Name = "lblRecover";
			this.lblRecover.Size = new System.Drawing.Size(203, 68);
			this.lblRecover.TabIndex = 2;
			// 
			// cmbBatchList
			// 
			this.cmbBatchList.Location = new System.Drawing.Point(241, 35);
			this.cmbBatchList.Name = "cmbBatchList";
			this.cmbBatchList.TabIndex = 1;
			// 
			// cmdBatchChoice
			// 
			this.cmdBatchChoice.AppearanceKey = "actionButton";
			this.cmdBatchChoice.Location = new System.Drawing.Point(84, 117);
			this.cmdBatchChoice.Name = "cmdBatchChoice";
			this.cmdBatchChoice.Size = new System.Drawing.Size(90, 40);
			this.cmdBatchChoice.TabIndex = 3;
			this.cmdBatchChoice.Text = "Purge";
			this.cmdBatchChoice.Click += new System.EventHandler(this.cmdBatchChoice_Click);
			// 
			// fraBatch
			// 
			this.fraBatch.AppearanceKey = "groupBoxNoBorders";
			this.fraBatch.Controls.Add(this.Label11);
			this.fraBatch.Controls.Add(this.txtTotal);
			this.fraBatch.Controls.Add(this.cmdProcessBatch);
			this.fraBatch.Controls.Add(this.vsBatch);
			this.fraBatch.Controls.Add(this.txtBillNumber);
			this.fraBatch.Controls.Add(this.lblBillNumber);
			this.fraBatch.Controls.Add(this.cmbSewer);
			this.fraBatch.Controls.Add(this.txtAmount);
			this.fraBatch.Controls.Add(this.Label10);
			this.fraBatch.Controls.Add(this.txtBatchAccount);
			this.fraBatch.Controls.Add(this.Label9);
			this.fraBatch.Controls.Add(this.lblLocation);
			this.fraBatch.Controls.Add(this.lblName);
			this.fraBatch.Controls.Add(this.lblInstructions);
			this.fraBatch.Location = new System.Drawing.Point(51, 136);
			this.fraBatch.Name = "fraBatch";
			this.fraBatch.Size = new System.Drawing.Size(1103, 380);
			this.fraBatch.TabIndex = 8;
			this.fraBatch.Visible = false;
			// 
			// Label11
			// 
			this.Label11.AutoSize = true;
			this.Label11.Location = new System.Drawing.Point(613, 334);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(51, 16);
			this.Label11.TabIndex = 13;
			this.Label11.Text = "TOTAL";
			// 
			// txtTotal
			// 
			this.txtTotal.Location = new System.Drawing.Point(683, 322);
			this.txtTotal.Name = "txtTotal";
			this.txtTotal.Size = new System.Drawing.Size(120, 40);
			this.txtTotal.TabIndex = 12;
			// 
			// cmdProcessBatch
			// 
			this.cmdProcessBatch.Name = "cmdProcessBatch";
			this.cmdProcessBatch.TabIndex = 14;
			// 
			// vsBatch
			// 
			this.vsBatch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsBatch.Cols = 10;
			this.vsBatch.FixedCols = 0;
			this.vsBatch.Location = new System.Drawing.Point(20, 174);
			this.vsBatch.Name = "vsBatch";
			this.vsBatch.RowHeadersVisible = false;
			this.vsBatch.Rows = 1;
			this.vsBatch.Size = new System.Drawing.Size(1064, 125);
			this.vsBatch.TabIndex = 10;
			// 
			// txtBillNumber
			// 
			this.txtBillNumber.Location = new System.Drawing.Point(704, 105);
			this.txtBillNumber.Name = "txtBillNumber";
			this.txtBillNumber.Size = new System.Drawing.Size(99, 40);
			this.txtBillNumber.TabIndex = 9;
			// 
			// lblBillNumber
			// 
			this.lblBillNumber.AutoSize = true;
			this.lblBillNumber.Location = new System.Drawing.Point(594, 119);
			this.lblBillNumber.Name = "lblBillNumber";
			this.lblBillNumber.Size = new System.Drawing.Size(97, 16);
			this.lblBillNumber.TabIndex = 8;
			this.lblBillNumber.Text = "BILL NUMBER";
			// 
			// cmbSewer
			// 
			this.cmbSewer.Items.AddRange(new object[] {
            "Water",
            "Sewer"});
			this.cmbSewer.Location = new System.Drawing.Point(454, 105);
			this.cmbSewer.Name = "cmbSewer";
			this.cmbSewer.TabIndex = 7;
			this.cmbSewer.Text = "Water";
			// 
			// txtAmount
			// 
			this.txtAmount.Location = new System.Drawing.Point(313, 105);
			this.txtAmount.Name = "txtAmount";
			this.txtAmount.Size = new System.Drawing.Size(120, 40);
			this.txtAmount.TabIndex = 6;
			this.txtAmount.Enter += new System.EventHandler(this.txtAmount_Enter);
			this.txtAmount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAmount_KeyDown);
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(227, 119);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(65, 15);
			this.Label10.TabIndex = 5;
			this.Label10.Text = "AMOUNT";
			// 
			// txtBatchAccount
			// 
			this.txtBatchAccount.Location = new System.Drawing.Point(114, 105);
			this.txtBatchAccount.Name = "txtBatchAccount";
			this.txtBatchAccount.Size = new System.Drawing.Size(90, 40);
			this.txtBatchAccount.TabIndex = 4;
			this.txtBatchAccount.Enter += new System.EventHandler(this.txtBatchAccount_Enter);
			this.txtBatchAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtBatchAccount_Validating);
			this.txtBatchAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtBatchAccount_KeyDown);
			this.txtBatchAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBatchAccount_KeyPress);
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(20, 119);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(65, 15);
			this.Label9.TabIndex = 3;
			this.Label9.Text = "ACCOUNT";
			// 
			// lblLocation
			// 
			this.lblLocation.Location = new System.Drawing.Point(340, 80);
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Size = new System.Drawing.Size(300, 15);
			this.lblLocation.TabIndex = 2;
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(20, 80);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(300, 15);
			this.lblName.TabIndex = 1;
			// 
			// lblInstructions
			// 
			this.lblInstructions.Location = new System.Drawing.Point(20, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(570, 30);
			this.lblInstructions.TabIndex = 14;
			// 
			// fraBatchQuestions
			// 
			this.fraBatchQuestions.Controls.Add(this.cmdBatch);
			this.fraBatchQuestions.Controls.Add(this.txtPaidBy);
			this.fraBatchQuestions.Controls.Add(this.fcLabel3);
			this.fraBatchQuestions.Controls.Add(this.lblEffectiveDate);
			this.fraBatchQuestions.Controls.Add(this.lblPaymentDate);
			this.fraBatchQuestions.Controls.Add(this.txtEffectiveDate);
			this.fraBatchQuestions.Controls.Add(this.txtPaymentDate);
			this.fraBatchQuestions.Controls.Add(this.txtTellerID);
			this.fraBatchQuestions.Controls.Add(this.label8);
			this.fraBatchQuestions.Controls.Add(this.chkReceipt);
			this.fraBatchQuestions.Location = new System.Drawing.Point(31, 36);
			this.fraBatchQuestions.Name = "fraBatchQuestions";
			this.fraBatchQuestions.Size = new System.Drawing.Size(449, 330);
			this.fraBatchQuestions.TabIndex = 7;
			this.fraBatchQuestions.Text = "Batch Update Information";
			this.fraBatchQuestions.Visible = false;
			// 
			// cmdBatch
			// 
			this.cmdBatch.AppearanceKey = "actionButton";
			this.cmdBatch.Location = new System.Drawing.Point(270, 267);
			this.cmdBatch.Name = "cmdBatch";
			this.cmdBatch.Size = new System.Drawing.Size(80, 40);
			this.cmdBatch.TabIndex = 10;
			this.cmdBatch.Text = "Next";
			this.cmdBatch.Click += new System.EventHandler(this.cmdBatch_Click);
			// 
			// txtPaidBy
			// 
			this.txtPaidBy.Location = new System.Drawing.Point(270, 217);
			this.txtPaidBy.MaxLength = 25;
			this.txtPaidBy.Name = "txtPaidBy";
			this.txtPaidBy.Size = new System.Drawing.Size(135, 40);
			this.txtPaidBy.TabIndex = 9;
			this.txtPaidBy.Enter += new System.EventHandler(this.txtPaidBy_Enter);
			this.txtPaidBy.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPaidBy_KeyDown);
			// 
			// fcLabel3
			// 
			this.fcLabel3.Location = new System.Drawing.Point(20, 231);
			this.fcLabel3.Name = "fcLabel3";
			this.fcLabel3.Size = new System.Drawing.Size(70, 15);
			this.fcLabel3.TabIndex = 8;
			this.fcLabel3.Text = "PAID BY";
			// 
			// lblEffectiveDate
			// 
			this.lblEffectiveDate.AutoSize = true;
			this.lblEffectiveDate.Location = new System.Drawing.Point(20, 181);
			this.lblEffectiveDate.Name = "lblEffectiveDate";
			this.lblEffectiveDate.Size = new System.Drawing.Size(218, 16);
			this.lblEffectiveDate.TabIndex = 7;
			this.lblEffectiveDate.Text = "EFFECTIVE DATE (MM/DD/YYYY)";
			// 
			// lblPaymentDate
			// 
			this.lblPaymentDate.AutoSize = true;
			this.lblPaymentDate.Location = new System.Drawing.Point(20, 131);
			this.lblPaymentDate.Name = "lblPaymentDate";
			this.lblPaymentDate.Size = new System.Drawing.Size(209, 16);
			this.lblPaymentDate.TabIndex = 6;
			this.lblPaymentDate.Text = "PAYMENT DATE (MM/DD/YYYY)";
			// 
			// txtEffectiveDate
			// 
			this.txtEffectiveDate.Location = new System.Drawing.Point(270, 167);
			this.txtEffectiveDate.Mask = "##/##/####";
			this.txtEffectiveDate.MaxLength = 10;
			this.txtEffectiveDate.Name = "txtEffectiveDate";
			this.txtEffectiveDate.Size = new System.Drawing.Size(135, 22);
			this.txtEffectiveDate.TabIndex = 5;
			this.txtEffectiveDate.GotFocus += new System.EventHandler(this.txtEffectiveDate_GotFocus);
			this.txtEffectiveDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtEffectiveDate_KeyDown);
			this.txtEffectiveDate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEffectiveDate_KeyPress);
			// 
			// txtPaymentDate
			// 
			this.txtPaymentDate.Location = new System.Drawing.Point(270, 117);
			this.txtPaymentDate.Mask = "##/##/####";
			this.txtPaymentDate.MaxLength = 10;
			this.txtPaymentDate.Name = "txtPaymentDate";
			this.txtPaymentDate.Size = new System.Drawing.Size(135, 22);
			this.txtPaymentDate.TabIndex = 4;
			this.txtPaymentDate.GotFocus += new System.EventHandler(this.txtPaymentDate_GotFocus);
			this.txtPaymentDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPaymentDate_KeyDown);
			this.txtPaymentDate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPaymentDate_KeyPress);
			// 
			// txtTellerID
			// 
			this.txtTellerID.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
			this.txtTellerID.Location = new System.Drawing.Point(270, 67);
			this.txtTellerID.MaxLength = 3;
			this.txtTellerID.Name = "txtTellerID";
			this.txtTellerID.Size = new System.Drawing.Size(135, 40);
			this.txtTellerID.TabIndex = 2;
			this.txtTellerID.Enter += new System.EventHandler(this.txtTellerID_Enter);
			this.txtTellerID.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTellerID_KeyDown);
			this.txtTellerID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTellerID_KeyPress);
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(20, 81);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(70, 15);
			this.label8.TabIndex = 1;
			this.label8.Text = "TELLER ID";
			// 
			// chkReceipt
			// 
			this.chkReceipt.Location = new System.Drawing.Point(20, 30);
			this.chkReceipt.Name = "chkReceipt";
			this.chkReceipt.Size = new System.Drawing.Size(207, 23);
			this.chkReceipt.TabIndex = 11;
			this.chkReceipt.Text = "Default Receipt Option to \'Yes\'";
			// 
			// cmdSaveBatch
			// 
			this.cmdSaveBatch.AppearanceKey = "acceptButton";
			this.cmdSaveBatch.ForeColor = System.Drawing.Color.White;
			this.cmdSaveBatch.Location = new System.Drawing.Point(521, 30);
			this.cmdSaveBatch.Name = "cmdSaveBatch";
			this.cmdSaveBatch.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveBatch.Size = new System.Drawing.Size(170, 48);
			this.cmdSaveBatch.TabIndex = 6;
			this.cmdSaveBatch.Text = "Save Batch";
			this.cmdSaveBatch.Click += new System.EventHandler(this.cmdSaveBatch_Click);
			// 
			// fraValidate
			// 
			this.fraValidate.AppearanceKey = "groupBoxNoBorders";
			this.fraValidate.BackColor = System.Drawing.Color.White;
			this.fraValidate.Controls.Add(this.lblValidate);
			this.fraValidate.Controls.Add(this.cmdValidateCancel);
			this.fraValidate.Controls.Add(this.cmdValidateNo);
			this.fraValidate.Controls.Add(this.cmdValidateYes);
			this.fraValidate.Location = new System.Drawing.Point(466, 195);
			this.fraValidate.Name = "fraValidate";
			this.fraValidate.Size = new System.Drawing.Size(280, 180);
			this.fraValidate.TabIndex = 9;
			this.fraValidate.Visible = false;
			// 
			// lblValidate
			// 
			this.lblValidate.Location = new System.Drawing.Point(20, 30);
			this.lblValidate.Name = "lblValidate";
			this.lblValidate.Size = new System.Drawing.Size(237, 72);
			this.lblValidate.TabIndex = 3;
			// 
			// cmdValidateCancel
			// 
			this.cmdValidateCancel.AppearanceKey = "actionButton";
			this.cmdValidateCancel.Location = new System.Drawing.Point(177, 121);
			this.cmdValidateCancel.Name = "cmdValidateCancel";
			this.cmdValidateCancel.Size = new System.Drawing.Size(80, 40);
			this.cmdValidateCancel.TabIndex = 2;
			this.cmdValidateCancel.Text = "Cancel";
			this.cmdValidateCancel.Click += new System.EventHandler(this.cmdValidateCancel_Click);
			// 
			// cmdValidateNo
			// 
			this.cmdValidateNo.AppearanceKey = "actionButton";
			this.cmdValidateNo.Location = new System.Drawing.Point(99, 121);
			this.cmdValidateNo.Name = "cmdValidateNo";
			this.cmdValidateNo.Size = new System.Drawing.Size(60, 40);
			this.cmdValidateNo.TabIndex = 1;
			this.cmdValidateNo.Text = "No";
			this.cmdValidateNo.Click += new System.EventHandler(this.cmdValidateNo_Click);
			// 
			// cmdValidateYes
			// 
			this.cmdValidateYes.AppearanceKey = "actionButton";
			this.cmdValidateYes.Location = new System.Drawing.Point(20, 121);
			this.cmdValidateYes.Name = "cmdValidateYes";
			this.cmdValidateYes.Size = new System.Drawing.Size(60, 40);
			this.cmdValidateYes.TabIndex = 4;
			this.cmdValidateYes.Text = "Yes";
			this.cmdValidateYes.Click += new System.EventHandler(this.cmdValidateYes_Click);
			// 
			// fcMenuStrip1
			// 
			this.fcMenuStrip1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBatchRecover,
            this.mnuBatchPurge,
            this.mnuFileImportPayPortBatch});
			this.fcMenuStrip1.Name = null;
			// 
			// mnuBatchRecover
			// 
			this.mnuBatchRecover.Index = 0;
			this.mnuBatchRecover.Name = "mnuBatchRecover";
			this.mnuBatchRecover.Text = "Recover Batch";
			this.mnuBatchRecover.Click += new System.EventHandler(this.mnuBatchRecover_Click);
			// 
			// mnuBatchPurge
			// 
			this.mnuBatchPurge.Index = 1;
			this.mnuBatchPurge.Name = "mnuBatchPurge";
			this.mnuBatchPurge.Text = "Purge Batch";
			this.mnuBatchPurge.Click += new System.EventHandler(this.mnuBatchPurge_Click);
			// 
			// mnuFileImportPayPortBatch
			// 
			this.mnuFileImportPayPortBatch.Index = 2;
			this.mnuFileImportPayPortBatch.Name = "mnuFileImportPayPortBatch";
			this.mnuFileImportPayPortBatch.Text = "Import PayPort Batch";
			this.mnuFileImportPayPortBatch.Click += new System.EventHandler(this.mnuFileImportPayPortBatch_Click);
			// 
			// frmBatchPayment
			// 
			this.ClientSize = new System.Drawing.Size(1212, 739);
			this.Menu = this.fcMenuStrip1;
			this.Name = "frmBatchPayment";
			this.Text = "frmBatchPayment";
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRecover)).EndInit();
			this.fraRecover.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdBatchChoice)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBatch)).EndInit();
			this.fraBatch.ResumeLayout(false);
			this.fraBatch.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessBatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsBatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBatchQuestions)).EndInit();
			this.fraBatchQuestions.ResumeLayout(false);
			this.fraBatchQuestions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdBatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtEffectiveDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPaymentDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveBatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraValidate)).EndInit();
			this.fraValidate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdValidateCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdValidateNo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdValidateYes)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private fecherFoundation.FCFrame fraRecover;
		private fecherFoundation.FCLabel lblRecover;
		private fecherFoundation.FCComboBox cmbBatchList;
		private fecherFoundation.FCButton cmdBatchChoice;
		private fecherFoundation.FCFrame fraBatch;
		private fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCTextBox txtTotal;
		private fecherFoundation.FCButton cmdProcessBatch;
		private fecherFoundation.FCGrid vsBatch;
		private fecherFoundation.FCTextBox txtBillNumber;
		private fecherFoundation.FCLabel lblBillNumber;
		private fecherFoundation.FCComboBox cmbSewer;
		private fecherFoundation.FCTextBox txtAmount;
		private fecherFoundation.FCLabel Label10;
		private fecherFoundation.FCTextBox txtBatchAccount;
		private fecherFoundation.FCLabel Label9;
		private fecherFoundation.FCLabel lblLocation;
		private fecherFoundation.FCLabel lblName;
		private fecherFoundation.FCLabel lblInstructions;
		private fecherFoundation.FCFrame fraBatchQuestions;
		private fecherFoundation.FCButton cmdBatch;
		public fecherFoundation.FCTextBox txtPaidBy;
		private fecherFoundation.FCLabel fcLabel3;
		private fecherFoundation.FCLabel lblEffectiveDate;
		private fecherFoundation.FCLabel lblPaymentDate;
		private Global.T2KDateBox txtEffectiveDate;
		private Global.T2KDateBox txtPaymentDate;
		public fecherFoundation.FCTextBox txtTellerID;
		private fecherFoundation.FCLabel label8;
		private fecherFoundation.FCCheckBox chkReceipt;
		public fecherFoundation.FCButton cmdSaveBatch;
		private fecherFoundation.FCFrame fraValidate;
		private fecherFoundation.FCLabel lblValidate;
		private fecherFoundation.FCButton cmdValidateCancel;
		private fecherFoundation.FCButton cmdValidateNo;
		private fecherFoundation.FCButton cmdValidateYes;
		private fecherFoundation.FCMenuStrip fcMenuStrip1;
		private fecherFoundation.FCToolStripMenuItem mnuBatchRecover;
		private fecherFoundation.FCToolStripMenuItem mnuBatchPurge;
		private fecherFoundation.FCToolStripMenuItem mnuFileImportPayPortBatch;
		private fecherFoundation.FCButton cmdCancel;
	}
}