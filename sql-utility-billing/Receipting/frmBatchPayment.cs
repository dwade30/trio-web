﻿using System;
using System.IO;
using fecherFoundation;
using fecherFoundation.VisualBasicLayer;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting;
using SharedApplication.UtilityBilling.Receipting.Interfaces;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000.Receipting
{
	public partial class frmBatchPayment : BaseForm, IView<IUtilityBillingBatchPaymentViewModel>
	{
		private IUtilityBillingBatchPaymentViewModel viewModel { get; set; }
		private GlobalColorSettings globalColorSettings;
		private bool cancelling = true;



		// CODE FREEZE  TROGES-88  START
		string strUTService = "";
		// kk02232018 troges-88  Add Batch Payment for UT
		clsDRWrapper rsBatch = new clsDRWrapper();
		clsDRWrapper rsBatchBackup = new clsDRWrapper();
		clsDRWrapper rsBatchLien = new clsDRWrapper();
		clsDRWrapper rsBLN = new clsDRWrapper();
		int lngBill; // Don't think we're going to use this. Unlike RE not every account will have the same bill number
		string strPaidBy = "";
		string strRef = "";
		string strTLRID = "";
		DateTime dtPaymentDate;
		DateTime dtEffectiveDate;
		double dblIntRate;
		double dblCurPrin; // will hold the value of the prin, int and cost to add to the grid
		double dblCurTax; // and will be cleared by the AccountValidate Account
		double dblCurInt;
		double dblPLInt;
		double dblCurCost; // these will be filled as a side effect of the CalculateAccount function
		DateTime dtIntPaidDate; // this will hold the InterestStartDate
		int lngBatchCHGINTNumber; // this is how the CHGINT Number will be passed back to the PaymentRec
		double dblXtraCurInt;
		bool boolResultsScreen; // this is true when the result grid is being shown
		double dblTotalDue; // this will hold the total to warn the user if they enter too much
							// xxxx DO WE ALLOW THIS????
							// xxxx Public intPrePayYear            As Integer                  'this will be where the prepayyear form will put the year the user select
		bool boolSearch;
		string strSearchString = "";
		string strPassSortOrder = "";
		bool boolPassActivate;
		double dblOriginalAmount;
		bool boolBatchImport;

		public int lngColBatchAcct;
		public int lngColBatchName;
		public int lngColBatchBill;
		public int lngColBatchService;
		public int lngColBatchRef;
		public int lngColBatchPrin;
		public int lngColBatchTax;
		public int lngColBatchInt;
		public int lngColBatchCost;
		public int lngColBatchTotal;
		public int lngColBatchKey;
		public int lngColBatchPrintReceipt;
		public int lngColBatchCorrectAmount;

		// YYYYYYYYYYYYYYYYYYYYYY
		public bool boolProcessingBatch;
		// YYYYYYYYYYYYYYYYYYYYYY

		int lngColBSKey;
		int lngColBSName;
		int lngColBSMapLot;
		int lngColBSAccount;


		private struct Import
		{
			public string[] Account;
			public string[] Name;
			public string[] Year;
			public string[] Amount;
			public string[] PaymentDate;
			public string[] CRLF;
		};
		Import CRImport = new Import();
		Import[] arrCRImportError = null;
		// CODE FREEZE  TROGES-88  END

		public frmBatchPayment()
		{
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{

		}

		public frmBatchPayment(IUtilityBillingBatchPaymentViewModel viewModel,
			GlobalColorSettings globalColorSettings) : this()
		{
			this.viewModel = viewModel;
			this.globalColorSettings = globalColorSettings;

			ShowBatchQuestions();
		}

		public IUtilityBillingBatchPaymentViewModel ViewModel
		{
			get => viewModel;
			set => viewModel = value;
		}

		private void cmdSaveBatch_Click(object sender, EventArgs e)
		{
			if (MessageBox.Show("Are you sure you want to process the batch with total of " + txtTotal.Text + "?", "Process Batch", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) == DialogResult.Yes)
			{
				PrintBatchReceipts();
				ProcessBatch();
				Close();
			}
		}

		private void cmdBatch_Click(object sender, System.EventArgs e)
		{
			if (CheckBatchQuestions())
			{
				LockBatch(true);
			}
		}

		private void LockBatch(bool boolLock)
		{
			fraValidate.Visible = boolLock;
			fraValidate.Top = fraBatchQuestions.Top + cmdBatch.Top - 100;
			//fraValidate.Left = FCConvert.ToInt32((frmGetMasterAccount.InstancePtr.Width - fraValidate.Width) / 2.0);
			lblValidate.Text = "IMPORTANT: Verify that the information entered is correct and press Yes.";

			cmdBatch.Enabled = !boolLock;
			txtTellerID.Enabled = !boolLock;
			txtPaymentDate.Enabled = !boolLock;
			txtEffectiveDate.Enabled = !boolLock;
			txtPaidBy.Enabled = !boolLock;
			chkReceipt.Enabled = !boolLock;
		}

		private void cmdBatchChoice_Click(object sender, System.EventArgs e)
		{
			if (cmbBatchList.SelectedIndex != -1)
			{
				string VBtoVar = FCConvert.ToString(fraRecover.Tag);

				if (VBtoVar == "P")
				{
					PurgeRecords(cmbBatchList.Items[cmbBatchList.SelectedIndex].ToString());
				}
				else if (VBtoVar == "R")
				{
					RecoverRecords(cmbBatchList.Items[cmbBatchList.SelectedIndex].ToString());
				}
			}
			else
			{
				MessageBox.Show("Please select a batch from the drop down list.", "Data Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void CreateBatchPaymentLine(int lngAcct, double dblPrin, double dblTax, double dblInt, double dblCost, DateTime dtDate, DateTime dtETDate, int lBill, string strSrvc)
		{
			try
			{
				var data = new BatchRecover
				{
					BillNumber = lngBill,
					TellerId = strTLRID,
					PaidBy = strPaidBy,
					Reference = strRef,
					Service = strSrvc,
					AccountNumber = lngAcct,
					Name = lblName,
					Principal = dblPrin,
					Interest = dblInt,
					Cost = dblCost,
					Tax = dblTax,
					BatchRecoverDate = dtDate,
					ETDate = dtETDate
				};

				var id = viewModel.AddBatchRecoverEntry(data);

				strRef = FCConvert.ToString(lngAcct) + "-" + FCConvert.ToString(lBill);

				if (id == 0)
				{
					MessageBox.Show("Error adding record batch.  Please try again.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}

				vsBatch.AddItem("");

				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchAcct, lngAcct);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchName, lblName.Text);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchBill, lBill);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchService, strSrvc);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchRef, strRef);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrin, dblPrin);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchTax, dblTax);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchInt, dblInt);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCost, dblCost);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchTotal, dblPrin + dblInt + dblCost + dblTax);
				vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchKey, id);
				if (chkReceipt.CheckState == CheckState.Checked)
				{
					vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrintReceipt, -1);
				}
				if (dblCurPrin + dblCurInt + dblPLInt + dblCurCost + dblTax == dblOriginalAmount)
				{
					vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCorrectAmount, "Yes");
				}
				else
				{
					vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCorrectAmount, "No");
				}
			}
			catch (Exception ex)
			{   
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Batch Records", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PrintBatchReceipts()
		{
			try
			{   
				int lngRW;

				for (lngRW = 1; lngRW <= vsBatch.Rows - 1; lngRW++)
				{
					if (Conversion.Val(vsBatch.TextMatrix(lngRW, lngColBatchPrintReceipt)) == -1)
					{
						if (viewModel.StartedFromCashReceipts)
						{
							//arBatchReceipt.InstancePtr.Init(lngRW);
							//arBatchReceipt.InstancePtr.Close();
						}
					}
				}
			}
			catch (Exception ex)
			{   
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Batch Receipts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileImportPayPortBatch_Click(object sender, System.EventArgs e)
		{
			ImportPayportBatch();
		}

		private void mnuBatchPurge_Click(object sender, System.EventArgs e)
		{
			fraRecover.Tag = "P";
			cmdBatchChoice.Text = "Purge";
			lblRecover.Text = "Choose the Batch that you would like to purge.";

			if (FillBatchCombo())
			{
				ViewModel.BatchStarted = false;
				ShowBatchQuestions();
				fraBatchQuestions.Visible = false; 

				fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 2.0);
				fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 3.0);
				fraRecover.Visible = true;
			}
		}

		private void mnuBatchRecover_Click(object sender, System.EventArgs e)
		{
			fraRecover.Tag = "R"; 
			cmdBatchChoice.Text = "Recover";
			lblRecover.Text = "Choose the Batch that you would like to recover.";

			FillBatchCombo();

			ViewModel.BatchStarted = false;
			ShowBatchQuestions();
			fraBatchQuestions.Visible = false; 

			fraRecover.Left = FCConvert.ToInt32((this.Width - fraRecover.Width) / 2.0);
			fraRecover.Top = FCConvert.ToInt32((this.Height - fraRecover.Height) / 3.0);
			fraRecover.Visible = true;
		}

		
		private void cmdValidateCancel_Click(object sender, System.EventArgs e)
		{
			LockBatch(false); 
		}

		private void cmdValidateNo_Click(object sender, System.EventArgs e)
		{
			LockBatch(false); 
		}

		private void cmdValidateYes_Click(object sender, System.EventArgs e)
		{
			LockBatch(false); 
			ViewModel.BatchStarted = true;
			ShowBatch();
			fraBatchQuestions.Visible = false;
			mnuFileImportPayPortBatch.Enabled = true;
		}
		
		private void txtAmount_Enter(object sender, System.EventArgs e)
		{
			txtAmount.SelectionStart = 0;
			txtAmount.SelectionLength = txtAmount.Text.Length;
		}

		private void txtAmount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
			{
			}
			else if (KeyCode == FCConvert.ToInt32(Keys.Left))
			{
				if (txtAmount.SelectionStart == 0)
				{
					txtBatchAccount.Focus();
				}
			}
			else if (KeyCode == FCConvert.ToInt32(Keys.Return))
			{
				if (txtAmount.Text.ToDecimalValue() != 0)
				{
					if (txtBatchAccount.Text.ToIntegerValue() != 0)
					{
						if (MessageBox.Show("Would you like to save this payment?", "Save Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							if (AdjustPaymentAmount(txtAmount.Text.ToDoubleValue()) != 0)
							{
								KeyCode = 0;
								return; 
							}

							AddPaymentToBatch(rsBatch, FCConvert.ToInt32(FCConvert.ToDouble(txtBatchAccount.Text)));
						}
						else
						{
							txtBatchAccount.Focus();
						}
					}
					else
					{
						MessageBox.Show("The account entered cannot have a value of zero.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtBatchAccount.Focus();
					}
				}
				else
				{
					MessageBox.Show("The payment entered cannot be zero.", "Invalid Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtAmount.Focus();
				}
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtBatchAccount_Enter(object sender, System.EventArgs e)
		{
			txtBatchAccount.SelectionStart = 0;
			txtBatchAccount.SelectionLength = txtBatchAccount.Text.Length;
		}

		private void txtBatchAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			bool boolMove = false;
			if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)))
			{
			}
			else if (KeyCode == FCConvert.ToInt32(Keys.Right))
			{
				if (txtBatchAccount.SelectionStart == txtBatchAccount.Text.Length || txtBatchAccount.SelectionStart == 0 && txtBatchAccount.SelectionLength == txtBatchAccount.Text.Length)
				{
					txtBatchAccount_Validate(ref boolMove);
					if (boolMove == false)
					{
						txtAmount.Focus();
					}
				}
			}
			else if (KeyCode == FCConvert.ToInt32(Keys.Return))
			{
				txtBatchAccount_Validate(ref boolMove);
				if (boolMove == false)
				{
					txtAmount.Focus();
				}
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtBatchAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back))
			{
			}
			else
			{
				KeyAscii = (Keys)0;
			}

			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtBatchAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// this sub will check to make sure that this is a valid account and if so, will calculate
			// the current Principal, Tax, Interest and Costs and display them as default in the Amount box
			int lngAcct;
			double dblTemp = 0;
			int lngBillKey;
			double dblCurrInt = 0;
			bool boolPrevBill = false;

			string strService = "";
			int lngBillNum;

			// during the validate, this will get account information and display it for the user
			if (cmbSewer.Text == "Sewer")
			{
				strService = "S";
			}
			else
			{
				strService = "W";
			}
			if (txtBatchAccount.Text == "")
			{
				return;
			}
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBatchAccount.Text)));
			lngBillNum = FCConvert.ToInt32(Math.Round(Conversion.Val(txtBillNumber.Text)));

			// make sure that only one payment per service can be in the batch from each account
			if (vsBatch.FindRow(lngAcct, -1, 1) == -1)
			{
				// reset the totals
				dblCurPrin = 0;
				dblCurTax = 0;
				dblCurInt = 0;
				dblPLInt = 0;
				dblCurCost = 0;
				dblTotalDue = 0;
				boolPrevBill = false;
				if (lngAcct != 0)
				{
					// find the account
					rsBatch.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND (Service = '" + strService + "' OR Service = 'B') ORDER BY BillNumber DESC", modExtraModules.strUTDatabase);
					if (!rsBatch.EndOfFile() && !rsBatch.BeginningOfFile())
					{
						rsBatch.FindFirstRecord("BillNumber", lngBillNum);
						if (rsBatch.NoMatch)
						{
							if (!boolBatchImport)
							{
								MessageBox.Show("Bill for account " + FCConvert.ToString(lngAcct) + " not found.  Please mark this account and add it at the end of the batch.", "Missing Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							txtBatchAccount.Text = "";
							txtAmount.Text = "";
							txtBillNumber.Text = "";
							e.Cancel = true;
						}
						else
						{
							lngBillKey = rsBatch.Get_Fields("ID");
							TRYAGAIN:;
							// kk02262018 troges-88  Rewrite the FindPreviousRecord as a query
							// rsBatch.FindPreviousRecord , , "BillNumber < " & lngBillNum & " AND (" & strService & "PrinOwed - " & strService & "PrinPaid) > 0"
							// If Not rsBatch.NoMatch Then
							rsBatch.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND " + "(Service = '" + strService + "' OR Service = 'B') AND (" + strService + "PrinOwed - " + strService + "PrinPaid) > 0", modExtraModules.strUTDatabase);
							if (!rsBatch.EndOfFile())
							{
								if (modUTCalculations.CalculateAccountUT(rsBatch, dtEffectiveDate, ref dblCurrInt, FCConvert.CBool(strService == "W")) == 0)
								{
									goto TRYAGAIN;
								}
								else
								{
									boolPrevBill = true;
								}
								// End If
								if (boolPrevBill && !boolBatchImport)
								{
									// there are previous bills with balances
									if (MessageBox.Show("There are previous bill(s) with account balances.  Would you like to continue adding payment?", "Previous Account Balance", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
									{
										modGlobalFunctions.AddCYAEntry_6("CR", "Batch payment with previous balances. - Acct: " + txtBatchAccount.Text);
									}
									else
									{
										e.Cancel = false;
										return;
									}
								}
							}
							// this will put the recordset back to the original bill
							// kk02262018 troges-88  Change the FindFirstRecord to a query to reload the Bill
							// rsBatch.FindFirstRecord , , "ID = " & lngBillKey
							rsBatch.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND (Service = '" + strService + "' OR Service = 'B') ORDER BY BillNumber DESC", modExtraModules.strUTDatabase);
							// this will move the labels to right right of the amount box
							// lblName.Left = txtAmount.Left + txtAmount.Width + 200
							// lblLocation.Left = lblName.Left
							// fill the labels with the account information
							lblName.Text = "Name: " + Strings.Trim(rsBatch.Get_Fields("BName") + " ");
							lblLocation.Text = "Location: " + Strings.Trim(rsBatch.Get_Fields("Location") + " ");
							// XXXX NOT DEALING WITH LIENS AT THIS TIME
							// If rsBatch.Fields("LienRecordNumber") = 0 Then
							// XXXXXXXX                        dblOriginalAmount = CalculateAccountCL(rsBatch, lngAcct, dtEffectiveDate, dblTemp, dblCurPrin, dblCurInt, dblCurCost, , , , , , , , , intPer)
							var tempDate = DateTime.Now;
							dblOriginalAmount = modUTCalculations.CalculateAccountUT(rsBatch, dtEffectiveDate, ref dblTemp, FCConvert.CBool(strService == "W"), ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref tempDate, ref dblCurTax);
							dblPLInt = 0;
							txtAmount.Text = Strings.Format(dblOriginalAmount, "#,##0.00");
							if (Conversion.Val(txtAmount.Text) == 0)
							{
								dblTotalDue = 0;
							}
							else
							{
								dblTotalDue = FCConvert.ToDouble(txtAmount.Text);
							}
						}
					}
					else
					{
						if (!boolBatchImport)
						{
							MessageBox.Show("Please enter a valid account number.", "Account Validation", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						txtBatchAccount.Text = "";
						txtAmount.Text = "";
						txtBillNumber.Text = "";
						txtBatchAccount.Focus();
					}
				}
				else
				{
					if (!boolBatchImport)
					{
						MessageBox.Show("Please enter a valid account number.", "Account Validation", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					txtBatchAccount.Text = "";
					txtAmount.Text = "";
					txtBillNumber.Text = "";
					txtBatchAccount.Focus();
				}
			}
			else
			{
				if (!boolBatchImport)
				{
					MessageBox.Show("Please enter an account number that has not been entered already or delete the current payment for this account.", "Multiple Payments", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				txtBatchAccount.Text = "";
				txtAmount.Text = "";
				txtBillNumber.Text = "";
			}
		}
		public void txtBatchAccount_Validate(ref bool Cancel)
		{
			txtBatchAccount_Validating(txtBatchAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtPaidBy_Enter(object sender, EventArgs e)
		{
			txtPaidBy.SelectionStart = 0;
			txtPaidBy.SelectionLength = txtPaidBy.Text.Length;
		}

		private void txtPaidBy_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);


			if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
			{
			}
			else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
			{
				KeyCode = 0;
				cmdBatch.Focus();
			}
			else if (KeyCode == FCConvert.ToInt32(Keys.Up))
			{
				KeyCode = 0;
				txtEffectiveDate.Focus();
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtPaymentDate_GotFocus(object sender, EventArgs e)
		{
			txtPaymentDate.SelectionStart = 0;
			txtPaymentDate.SelectionLength = txtPaymentDate.Text.Length;
		}

		private void txtPaymentDate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
			{
			}
			else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
			{
				KeyCode = 0;
				txtEffectiveDate.Focus();
			}
			else if (KeyCode == FCConvert.ToInt32(Keys.Up))
			{
				KeyCode = 0;
				txtTellerID.Focus();
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtPaymentDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii >= 47 && KeyAscii <= 57) || (KeyAscii == 8))
			{
			}
			else
			{
				KeyAscii = 0;
			}
		}

		private void txtEffectiveDate_GotFocus(object sender, EventArgs e)
		{
			txtEffectiveDate.SelStart = 0;
			txtEffectiveDate.SelLength = txtEffectiveDate.Text.Length;
		}

		private void txtEffectiveDate_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if ((KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9) || (KeyCode == FCConvert.ToInt32(Keys.Divide)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)))
			{
			}
			else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
			{
				KeyCode = 0;
				txtPaidBy.Focus();
			}
			else if (KeyCode == FCConvert.ToInt32(Keys.Up))
			{
				KeyCode = 0;
				txtPaymentDate.Focus();
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtEffectiveDate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii >= 47 && KeyAscii <= 57) || (KeyAscii == 8))
			{
			}
			else
			{
				KeyAscii = 0;
			}
		}

		public void ShowBatchQuestions()
		{
			fraBatch.Visible = false;
			fraRecover.Visible = false;

			fraBatchQuestions.Left = FCConvert.ToInt32((this.Width - fraBatchQuestions.Width) / 17.0);
			fraBatchQuestions.Top = FCConvert.ToInt32((this.Height - fraBatchQuestions.Height) / 18.0);
			fraBatchQuestions.Visible = true;
		}

		private void ShowBatch()
		{
			int lngRW = 0;
			// this will align the Search Screen and Show it
			FormatBatchGrid();
			this.Text = "Batch";
			rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE TellerID = '" + strTLRID + "' AND PaidBy = '" + strPaidBy + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "'", modExtraModules.strUTDatabase);
			if (!rsBatchBackup.EndOfFile() && !rsBatchBackup.BeginningOfFile())
			{ // if there are already records out there
				if (MessageBox.Show("There are already some files saved in the recovery with this Teller ID and Paid By.  These may be pending transactions, would you like to load them?  If you do not, they will be removed.", "Recover Previous Batch?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					do
					{
						vsBatch.AddItem("");
						lngRW = vsBatch.Rows - 1;
						vsBatch.TextMatrix(lngRW, lngColBatchAcct, rsBatchBackup.Get_Fields("AccountNumber"));
						vsBatch.TextMatrix(lngRW, lngColBatchName, rsBatchBackup.Get_Fields("Name"));
						vsBatch.TextMatrix(lngRW, lngColBatchBill, rsBatchBackup.Get_Fields("BillNumber"));
						vsBatch.TextMatrix(lngRW, lngColBatchService, rsBatchBackup.Get_Fields("Service"));
						vsBatch.TextMatrix(lngRW, lngColBatchRef, rsBatchBackup.Get_Fields("Ref"));
						vsBatch.TextMatrix(lngRW, lngColBatchPrin, Strings.Format(rsBatchBackup.Get_Fields("Prin"), "#,##0.00"));
						vsBatch.TextMatrix(lngRW, lngColBatchTax, Strings.Format(rsBatchBackup.Get_Fields("Tax"), "#,##0.00"));
						vsBatch.TextMatrix(lngRW, lngColBatchInt, Strings.Format(rsBatchBackup.Get_Fields("Int"), "#,##0.00"));
						vsBatch.TextMatrix(lngRW, lngColBatchCost, Strings.Format(rsBatchBackup.Get_Fields("Cost"), "#,##0.00"));
						vsBatch.TextMatrix(lngRW, lngColBatchTotal, FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchPrin)) + FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchTax)) + FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchInt)) + FCConvert.ToDouble(vsBatch.TextMatrix(lngRW, lngColBatchCost)));
						vsBatch.TextMatrix(lngRW, lngColBatchKey, rsBatchBackup.Get_Fields("ID"));
						if (FCConvert.CBool(rsBatchBackup.Get_Fields("PayCorrect")))
						{
							vsBatch.TextMatrix(lngRW, lngColBatchCorrectAmount, "Yes");
						}
						else
						{
							vsBatch.TextMatrix(lngRW, lngColBatchCorrectAmount, "No");
						}
						rsBatchBackup.MoveNext();
					} while (!rsBatchBackup.EndOfFile());
					// total the payments
					txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
					cmdSaveBatch.Enabled = true;
					mnuBatchRecover.Enabled = false;
					mnuFileImportPayPortBatch.Enabled = false;
					rsBatchBackup.MoveFirst();
				}
				else
				{
					do
					{
						rsBatchBackup.Delete();
						rsBatchBackup.MoveNext();
					} while (!rsBatchBackup.EndOfFile());
				}
			}
			fraBatchQuestions.Visible = false;
			fraRecover.Visible = false;

			fraBatch.Left = FCConvert.ToInt32((this.Width - fraBatch.Width) / 2.0);
			fraBatch.Top = FCConvert.ToInt32((this.Height - fraBatch.Height) / 3.0);
			fraBatch.Visible = true;
		}

		private bool FillBatchCombo()
		{
			var results = viewModel.GetBatchRecoverData();

			if (results != null)
			{
				foreach (var result in results)
				{
					cmbBatchList.Items.Add(modGlobalFunctions.PadStringWithSpaces(((DateTime)result.BatchRecoverDate).ToString("MM/dd/yyyy"), 10) + " " + result.TellerId + " " + result.PaidBy);
				}

				if (cmbBatchList.Items.Count > 0)
				{
					cmbBatchList.SelectedIndex = 0;
				}

				return true;
			}
			else
			{
				MessageBox.Show("No batch found.", "Empty Batch Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
			}
		}

		private void FormatBatchGrid()
		{
			int lngWid = 0;
			vsBatch.Cols = 13;
			lngWid = vsBatch.WidthOriginal;
			// width of each column
			vsBatch.ColWidth(lngColBatchAcct, FCConvert.ToInt32(lngWid * 0.05)); // Account
			vsBatch.ColWidth(lngColBatchName, FCConvert.ToInt32(lngWid * 0.14)); // Name
			vsBatch.ColWidth(lngColBatchBill, FCConvert.ToInt32(lngWid * 0.05)); // BillNumber
			vsBatch.ColWidth(lngColBatchService, FCConvert.ToInt32(lngWid * 0.05)); // Service
			vsBatch.ColWidth(lngColBatchRef, FCConvert.ToInt32(lngWid * 0.1)); // Ref
			vsBatch.ColWidth(lngColBatchPrin, FCConvert.ToInt32(lngWid * 0.1)); // Principal
			vsBatch.ColWidth(lngColBatchTax, FCConvert.ToInt32(lngWid * 0.1)); // Tax
			vsBatch.ColWidth(lngColBatchInt, FCConvert.ToInt32(lngWid * 0.1)); // Interest
			vsBatch.ColWidth(lngColBatchCost, FCConvert.ToInt32(lngWid * 0.1)); // Costs
			vsBatch.ColWidth(lngColBatchTotal, FCConvert.ToInt32(lngWid * 0.12)); // Total
			vsBatch.ColWidth(lngColBatchKey, 0);
			vsBatch.ColWidth(lngColBatchPrintReceipt, FCConvert.ToInt32(lngWid * 0.04)); // Print Receipt
			vsBatch.ColWidth(lngColBatchCorrectAmount, FCConvert.ToInt32(lngWid * 0.04)); // Correct Amount
																						  // Headers
			vsBatch.TextMatrix(0, lngColBatchAcct, "Acct");
			vsBatch.TextMatrix(0, lngColBatchName, "Name");
			vsBatch.TextMatrix(0, lngColBatchBill, "Bill");
			vsBatch.TextMatrix(0, lngColBatchService, "Service");
			vsBatch.TextMatrix(0, lngColBatchRef, "Ref");
			vsBatch.TextMatrix(0, lngColBatchPrin, "Principal");
			vsBatch.TextMatrix(0, lngColBatchTax, "Tax");
			vsBatch.TextMatrix(0, lngColBatchInt, "Interest");
			vsBatch.TextMatrix(0, lngColBatchCost, "Cost");
			vsBatch.TextMatrix(0, lngColBatchTotal, "Total");
			vsBatch.TextMatrix(0, lngColBatchKey, "");
			vsBatch.TextMatrix(0, lngColBatchPrintReceipt, "Rct");
			vsBatch.TextMatrix(0, lngColBatchCorrectAmount, "Auto");
			vsBatch.ColAlignment(lngColBatchAcct, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBatch.ColAlignment(lngColBatchBill, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsBatch.ColAlignment(lngColBatchPrin, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBatch.ColAlignment(lngColBatchTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBatch.ColAlignment(lngColBatchInt, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBatch.ColAlignment(lngColBatchCost, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBatch.ColAlignment(lngColBatchTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsBatch.ColFormat(lngColBatchPrin, "#,##0.00");
			vsBatch.ColFormat(lngColBatchTax, "#,##0.00");
			vsBatch.ColFormat(lngColBatchInt, "#,##0.00");
			vsBatch.ColFormat(lngColBatchCost, "#,##0.00");
			vsBatch.ColFormat(lngColBatchTotal, "#,##0.00");
			vsBatch.ColDataType(lngColBatchPrintReceipt, FCGrid.DataTypeSettings.flexDTBoolean);
		}

		private void ProcessBatch()
		{
			// this will go through each payment and add the correct payments to the
			// UT database and the correct receipt records to the CR database
			// this will cycle through the grid and find the total
			int lngCT;
			int lngRowCount;
			double dblSum;
			bool blnPrint;
			UpdatePrintReceiptChoice();
			lngRowCount = vsBatch.Rows - 1;
			dblSum = 0;
			blnPrint = false;
			for (lngCT = 1; lngCT <= lngRowCount; lngCT++)
			{
				dblSum += FCConvert.ToDouble(vsBatch.TextMatrix(lngCT, 7)); // Summing the Tax (8 is the costs in CL) column?  And then doing nothing with it. Take this out XXXXXXXXXXXX  TODO
				if (Strings.Trim(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) != "")
				{
					//FC:FINAL:MSH - wrong conversion (same with i.issue #1779)
					//if (FCConvert.ToBoolean(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) == true)
					if (FCConvert.CBool(vsBatch.TextMatrix(lngCT, lngColBatchPrintReceipt)) == true)
					{
						blnPrint = true;
					}
				}
			}
			if (lngRowCount > 0)
			{
				// Create Payment Records in UT
				if (CreateBatchUTRecords())
				{ // this will create all of the payment records for the Batch Update
					if (modExtraModules.IsThisCR())
					{ // If there is CR then send the info back there
					  //FC:TODO:DDU!!!!!!CreateUTBatchReceiptEntry(blnPrint);
					}

					MessageBox.Show("Batch Update Successful.", "Batch Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// clear the grid
					vsBatch.Rows = 1;
					// clear the BatchRecover table
					// PurgeRecords PadStringWithSpaces(CStr(dtPaymentDate), 10) & " " & strTLRID & " " & strPaidBy
					Close();
				}
			}
		}

		private void txtTellerID_Enter(object sender, System.EventArgs e)
		{
			txtTellerID.SelectionStart = 0;
			txtTellerID.SelectionLength = txtTellerID.Text.Length;
		}

		private void txtTellerID_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if ((KeyCode >= FCConvert.ToInt32(Keys.A)) && KeyCode <= FCConvert.ToInt32(Keys.Z) || (KeyCode == FCConvert.ToInt32(Keys.Delete)) || (KeyCode == FCConvert.ToInt32(Keys.Back)) || (KeyCode == FCConvert.ToInt32(Keys.Left)) || (KeyCode == FCConvert.ToInt32(Keys.Right)) || (KeyCode >= FCConvert.ToInt32(Keys.D0)) && KeyCode <= FCConvert.ToInt32(Keys.D9))
			{
			}
			else if ((KeyCode == FCConvert.ToInt32(Keys.Return)) || (KeyCode == FCConvert.ToInt32(Keys.Down)))
			{
				KeyCode = 0;
				txtPaymentDate.Focus();
			}
			else if (KeyCode == FCConvert.ToInt32(Keys.Up))
			{
				KeyCode = 0;
				chkReceipt.Focus();
			}
			else
			{
				KeyCode = 0;
			}
		}

		private void txtTellerID_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii >= FCConvert.ToInt32(Keys.A)) && KeyAscii <= FCConvert.ToInt32(Keys.Z) || (KeyAscii == FCConvert.ToInt32(Keys.Back)) || (KeyAscii >= FCConvert.ToInt32(Keys.D0)) && KeyAscii <= FCConvert.ToInt32(Keys.D9))
			{
			}
			else if (KeyAscii >= FCConvert.ToInt32(Keys.NumPad1) && KeyAscii <= FCConvert.ToInt32(Keys.F11))
			{
				KeyAscii = KeyAscii - 32;
			}
			else
			{
				KeyAscii = 0;
			}

			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private short AdjustPaymentAmount(double dblNewAmount, bool boolDoNotQueryOverPay = false)
		{
			short AdjustPaymentAmount = 0;
			try
			{   
				if (dblNewAmount >= dblCurInt + dblPLInt)
				{
					dblNewAmount -= (dblCurInt + dblPLInt);

					if (dblNewAmount >= dblCurCost)
					{
						dblNewAmount -= dblCurCost;

						if (dblNewAmount >= dblCurTax)
						{
							dblNewAmount = FCConvert.ToDouble(Strings.Format(dblNewAmount - dblCurTax, "0.00"));

							if (dblNewAmount >= dblCurPrin)
							{
								dblNewAmount = FCConvert.ToDouble(Strings.Format(dblNewAmount - dblCurPrin, "0.00"));

							}
							else
							{ // less than interest, costs, tax and principal
								dblCurPrin = dblNewAmount;
								dblNewAmount = 0;
							}
						}
						else
						{ // less than interest, costs and tax
							dblCurPrin = 0;
							dblCurTax = dblNewAmount;
							dblNewAmount = 0;
						}

					}
					else
					{ // less than interest and costs
						dblCurPrin = 0;
						dblCurTax = 0;
						dblCurCost = dblNewAmount;
						dblNewAmount = 0;
					}

				}
				else if (dblNewAmount >= dblCurInt)
				{ // less than interest
					dblNewAmount -= dblCurInt;
					dblPLInt = dblNewAmount;
					dblNewAmount = 0;
				}
				else
				{
					dblCurInt = dblNewAmount;
					dblPLInt = 0;
					dblCurPrin = 0;
					dblCurTax = 0;
					dblCurCost = 0;
					dblNewAmount = 0;
				}

				if (Math.Round(dblNewAmount, 2) > 0)
				{ // if there is leftover, then the payment is too much
					if (boolDoNotQueryOverPay)
					{
						dblCurPrin += dblNewAmount;
					}
					else
					{
						if (!boolBatchImport)
						{
							if (MessageBox.Show("This payment is more than owed, would you like to continue?", "Overpayment", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								dblCurPrin += dblNewAmount;
							}
							else
							{ // they do not want the overpayment
								AdjustPaymentAmount = -1;
							}
						}
						else
						{
							dblCurPrin += dblNewAmount;
						}
					}
				}
				return AdjustPaymentAmount;
			}
			catch
			{   
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Adjusting Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AdjustPaymentAmount;
		}

		private double CalculateGridTotal()
		{
			double CalculateGridTotal = 0;
			// this will cycle through the grid and find the total
			int lngCT;
			double dblSum;
			dblSum = 0;
			for (lngCT = 1; lngCT <= vsBatch.Rows - 1; lngCT++)
			{
				if (vsBatch.TextMatrix(lngCT, lngColBatchTotal) != "" && Conversion.Val(vsBatch.TextMatrix(lngCT, lngColBatchTotal)) != 0)
				{
					dblSum += FCConvert.ToDouble(vsBatch.TextMatrix(lngCT, lngColBatchTotal));
				}
			}
			if (dblSum > 0)
			{ // turns on the menu option when calculating the total
				cmdSaveBatch.Enabled = true;
				mnuBatchRecover.Enabled = false;
			}
			CalculateGridTotal = dblSum;
			return CalculateGridTotal;
		}

		private void AddPaymentToBatch(clsDRWrapper rsTemp, int lngAcct)
		{
			int lngRow;
			int lngKey = 0;
			int lngTBillNumber = 0;

			// add it to the temporary table

			// .OpenRecordset "SELECT * FROM BatchRecover", strUTDatabase
			rsBatchBackup.AddNew();

			// set the key value from the batch recover table
			lngKey = rsBatchBackup.Get_Fields_Int32("ID");
			lngTBillNumber = rsBatch.Get_Fields_Int32("BillNumber");

			strRef = FCConvert.ToString(lngAcct) + "-" + FCConvert.ToString(lngTBillNumber);

			// add the values to the table
			rsBatchBackup.Set_Fields("TellerID", strTLRID);
			rsBatchBackup.Set_Fields("PaidBy", strPaidBy + " ");
			rsBatchBackup.Set_Fields("Ref", strRef + " ");
			rsBatchBackup.Set_Fields("AccountNumber", lngAcct);
			rsBatchBackup.Set_Fields("Name", Strings.Right(lblName.Text, lblName.Text.Length - 6));
			rsBatchBackup.Set_Fields("Prin", dblCurPrin);
			rsBatchBackup.Set_Fields("Tax", dblCurTax);
			rsBatchBackup.Set_Fields("Int", dblCurInt + dblPLInt);
			rsBatchBackup.Set_Fields("Cost", dblCurCost);
			rsBatchBackup.Set_Fields("BatchRecoverDate", dtPaymentDate);
			rsBatchBackup.Set_Fields("ETDate", dtEffectiveDate);
			// XXXXXXXX        .Fields("Service") = strSrvc
			// xxx        .Fields("Type") = " "

			// save the record
			if (!rsBatchBackup.Update(true))
			{
				MessageBox.Show("Error adding record batch.  Please try again.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				return;
			}
			vsBatch.AddItem("");
			lngRow = vsBatch.Rows - 1;
			// add it to the grid
			vsBatch.TextMatrix(lngRow, lngColBatchAcct, lngAcct);
			vsBatch.TextMatrix(lngRow, lngColBatchName, Strings.Right(lblName.Text, lblName.Text.Length - 6));
			vsBatch.TextMatrix(lngRow, lngColBatchBill, lngBill);
			// XXXXXXXXXXXXX        .TextMatrix(lngRow, lngColBatchService) = strSrvc
			vsBatch.TextMatrix(lngRow, lngColBatchRef, strRef);
			vsBatch.TextMatrix(lngRow, lngColBatchPrin, dblCurPrin);
			vsBatch.TextMatrix(lngRow, lngColBatchTax, dblCurTax);
			vsBatch.TextMatrix(lngRow, lngColBatchInt, dblCurInt + dblPLInt);
			vsBatch.TextMatrix(lngRow, lngColBatchCost, dblCurCost);
			vsBatch.TextMatrix(lngRow, lngColBatchTotal, dblCurPrin + dblCurInt + dblPLInt + dblCurCost);
			vsBatch.TextMatrix(lngRow, lngColBatchKey, lngKey);
			if (chkReceipt.CheckState == CheckState.Checked)
			{
				vsBatch.TextMatrix(lngRow, lngColBatchPrintReceipt, -1);
			}
			else
			{
				vsBatch.TextMatrix(lngRow, lngColBatchPrintReceipt, 0);
			}
			if (dblCurPrin + dblCurInt + dblPLInt + dblCurCost == dblOriginalAmount)
			{
				vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "Yes");
			}
			else
			{
				vsBatch.TextMatrix(lngRow, lngColBatchCorrectAmount, "No");
			}
			// total grid
			txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
			// This will not allow an import after a row has already been added
			// XXXX mnuFileImport.Enabled = False
			mnuFileImportPayPortBatch.Enabled = false;
			// clear the payment boxes and setfocus to the account box
			txtAmount.Text = "";
			txtBatchAccount.Text = "";
			lblName.Text = "";
			lblLocation.Text = "";
			txtBatchAccount.Focus();
		}

		private void DeletePaymentFromBatch(int lngRow)
		{
			try
			{   // On Error GoTo ERROR_HANDLER
				int lngKey = 0;

				if (MessageBox.Show("Are you sure that you would like to delete this payment line.", "Delete Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
				{
					return;
				}

				// get the key number for the BatchRecover table
				if (vsBatch.TextMatrix(lngRow, lngColBatchKey) != "")
				{
					lngKey = FCConvert.ToInt32(vsBatch.TextMatrix(lngRow, lngColBatchKey));

					// delete payment from the database
					rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
					if (!rsBatchBackup.EndOfFile())
					{
						rsBatchBackup.Delete();
						// If rsBatchBackup.Execute("DELETE FROM BatchRecover WHERE ID = " & lngKey, strUTDatabase) Then
						// nothing
					}
					else
					{
						MessageBox.Show("Error Executing SQL Statement on removal of backup record.", "Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}

					// delete this payment from the grid
					vsBatch.RemoveItem(lngRow);

					if (vsBatch.Rows == 1)
					{
						cmdSaveBatch.Enabled = false;
						mnuBatchRecover.Enabled = true;
					}
				}
				else
				{
					// empty row
					// delete this line from the grid
					vsBatch.RemoveItem(lngRow);
				}

				txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");
				return;
			}
			catch
			{   
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err().Number) + " - " + Information.Err().Description + ".", "Error Deleting Row", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsBatch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == FCConvert.ToInt32(Keys.Delete))
			{
				// delete a row from the grid
				DeletePaymentFromBatch(vsBatch.Row);
				if (vsBatch.Rows == 1)
				{
					// XXXX mnuFileImport.Enabled = True
					mnuFileImportPayPortBatch.Enabled = true;
				}
			}
		}

		private void PurgeRecords(string strParam)
		{
			// this will find all the records that match these parameters and will delete them from the temp table
			string strPB;
			string strTID = "";
			DateTime dtDate = DateTime.Now;

			if (strParam.Length >= 10)
			{
				dtDate = DateAndTime.DateValue(Strings.Left(strParam, 10));
				strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 10));
			}

			if (strParam.Length >= 4)
			{
				strTID = Strings.Trim(Strings.Left(strParam, 4));
				strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 4));
			}

			strPB = Strings.Trim(strParam);

			if (!rsBatchBackup.Execute("DELETE FROM BatchRecover WHERE BatchRecoverDate = '" + FCConvert.ToString(dtDate) + "' AND TellerID = '" + strTID + "' AND PaidBy = '" + strPB + "'", modExtraModules.strUTDatabase, false))
			{
				MessageBox.Show("Error #" + rsBatchBackup.ErrorNumber + " - " + rsBatchBackup.ErrorDescription + ".", "Purge Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}

			fraRecover.Visible = false;
			fraBatchQuestions.Visible = false;
			fraBatch.Visible = false;
			// XXXX    fraSearch.Visible = True
		}

		private void RecoverRecords(string strParam)
		{
			// this will find all the records that match these parameters and will delete them from the temp table
			string strPB;
			string strTID = "";
			DateTime dtDate = DateTime.Now;

			if (strParam.Length >= 10)
			{
				dtDate = DateAndTime.DateValue(Strings.Left(strParam, 10));
				strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 10));
			}

			if (strParam.Length >= 4)
			{
				strTID = Strings.Trim(Strings.Left(strParam, 4));
				strParam = Strings.Trim(Strings.Right(strParam, strParam.Length - 4));
			}

			strPB = Strings.Trim(strParam);

			FormatBatchGrid();

			if (!rsBatchBackup.OpenRecordset("SELECT * FROM BatchRecover WHERE BatchRecoverDate = '" + FCConvert.ToString(dtDate) + "' AND TellerID = '" + strTID + "' AND PaidBy = '" + strPB + "'", modExtraModules.strUTDatabase))
			{
				MessageBox.Show("Error #" + rsBatchBackup.ErrorNumber + " - " + rsBatchBackup.ErrorDescription + ".", "Batch Recover Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else
			{
				if (!rsBatchBackup.EndOfFile() && !rsBatchBackup.BeginningOfFile())
				{
					vsBatch.Rows = 1;
					rsBatchBackup.MoveFirst();
					do
					{

						vsBatch.AddItem("");
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchAcct, rsBatchBackup.Get_Fields("AccountNumber")); // Account Number
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchName, rsBatchBackup.Get_Fields("Name")); // Name
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchBill, rsBatchBackup.Get_Fields("BillNumber")); // Bill Number
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchService, rsBatchBackup.Get_Fields("Service")); // Service
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchRef, rsBatchBackup.Get_Fields("Ref")); // Ref
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrin, rsBatchBackup.Get_Fields("Prin")); // Prin
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchTax, rsBatchBackup.Get_Fields("Tax")); // Tax
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchInt, rsBatchBackup.Get_Fields("Int")); // Int
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCost, rsBatchBackup.Get_Fields("Cost")); // Cost
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchTotal, FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrin)) + FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchInt)) + FCConvert.ToDouble(vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchCost))); // Total
						vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchKey, rsBatchBackup.Get_Fields("ID")); // Key in Table
						if (FCConvert.CBool(rsBatchBackup.Get_Fields("PrintReceipt")))
						{ // if Print Receipt is true
							vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrintReceipt, -1); // then check the box in the grid
						}
						else
						{
							vsBatch.TextMatrix(vsBatch.Rows - 1, lngColBatchPrintReceipt, 0);
						}

						rsBatchBackup.MoveNext();
					} while (!rsBatchBackup.EndOfFile());

					rsBatchBackup.MoveFirst();
					txtTellerID.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("TellerID"));
					txtPaymentDate.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("BatchRecoverDate"));
					txtEffectiveDate.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("ETDate"));
					txtPaidBy.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("PaidBy"));
					txtBillNumber.Text = FCConvert.ToString(rsBatchBackup.Get_Fields("BillNumber"));
					if (FCConvert.ToString(rsBatchBackup.Get_Fields("Service")) == "S")
					{
						cmbSewer.Text = "Sewer";
					}
					else
					{
						cmbSewer.Text = "Water";
					}
				}
				else
				{
					MessageBox.Show("Error processing batch with Payment Date: " + FCConvert.ToString(dtDate) + ", Teller ID: " + strTID + " and Paid By: " + strPB + ".", "Batch Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}

			// total the payments
			txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");

			// XXXX mnuFileImport.Enabled = False
			mnuFileImportPayPortBatch.Enabled = false;
			if (CheckBatchQuestions())
			{
				fraBatchQuestions.Visible = false;
				fraRecover.Visible = false;


				fraBatch.Left = FCConvert.ToInt32((this.Width - fraBatch.Width) / 2.0);
				fraBatch.Top = FCConvert.ToInt32((this.Height - fraBatch.Height) / 3.0);
				fraBatch.Visible = true;

			}
			else
			{
				fraBatch.Visible = true;
				fraRecover.Visible = false;

				//FC:FINAL:BSE #3548  frame should be aligned to the left-margin
				fraBatchQuestions.Left = FCConvert.ToInt32((this.Width - fraBatchQuestions.Width) / 17.0);
				fraBatchQuestions.Top = FCConvert.ToInt32((this.Height - fraBatchQuestions.Height) / 18.0);
				fraBatchQuestions.Visible = true;

			}
		}

		private void vsBatch_RowColChange(object sender, BeforeRowColChangeEventArgs e)
		{
			if (e.NewCol == lngColBatchPrintReceipt)
			{
				vsBatch.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsBatch.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private bool CreateBatchUTRecords()
		{
			bool CreateBatchUTRecords = false;
			clsDRWrapper rsBatchUT = new clsDRWrapper(); // Payment Rec
			clsDRWrapper rsBillRec = new clsDRWrapper(); // Billing Master
			clsDRWrapper rsLien = new clsDRWrapper(); // Lien
			string strSQL;
			double dblTotalDue = 0;
			int lBill;
			string strSrvc = "";

			lBill = 0;
			// strSQL = "SELECT * FROM BatchRecover WHERE TellerID = '" & strTLRID & "' AND BatchRecoverDate = '" & dtPaymentDate & "' AND ETDate = '" & dtEffectiveDate & "' AND PaidBy = '" & strPaidBy & "' AND Year   = " & lngYear
			strSQL = "SELECT * FROM BatchRecover WHERE TellerID = '" + strTLRID + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "' AND PaidBy = '" + strPaidBy + "' ";

			rsBatchBackup.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (!rsBatchBackup.EndOfFile() && !rsBatchBackup.BeginningOfFile())
			{
				do
				{ // cycle through all of the batch list
				  // this function will return the total due and seperate the total into interest, cost, tax and principal due
				  // it also returns the current interest in the variable dblXtraCurInt in order to create an Interest Line
					lBill = FCConvert.ToInt32(Math.Round(Conversion.Val(rsBatchBackup.Get_Fields("BillNumber"))));
					strSrvc = rsBatchBackup.Get_Fields_String("Service");
					if (lBill == 0) lBill = lngBill; // XXXXXX TODO: NO DEFAULT BILL NUMBER                                                                                               '
					strSQL = "SELECT * FROM Bill WHERE ActualAccountNumber = " + rsBatchBackup.Get_Fields("AccountNumber") + " AND BillNumber = " + FCConvert.ToString(lBill) + " AND (Service = '" + strSrvc + "' OR Service = 'B')";
					if (rsBillRec.OpenRecordset(strSQL, modExtraModules.strUTDatabase))
					{
						dblCurInt = 0;
						dblXtraCurInt = 0; // XXXXXXXXXXXXX TODO: FIX THE PARAMETER LIST
										   // dblTotalDue = CalculateAccountUT(rsBillRec, rsBatchBackup.Fields("AccountNumber"), rsBatchBackup.Fields("ETDate"), dblXtraCurInt, dblCurPrin, dblCurInt, dblCurCost)
						var tempDate = DateTime.Now;
						dblTotalDue = modUTCalculations.CalculateAccountUT(rsBillRec, rsBatchBackup.Get_Fields("ETDate"), ref dblXtraCurInt, FCConvert.CBool(strSrvc == "W"), ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, ref tempDate, ref dblCurTax);
						dblPLInt = Math.Round(dblTotalDue - (dblCurPrin + dblCurInt + dblCurCost + dblCurTax), 2);
						strSQL = "SELECT * FROM PaymentRec WHERE ID = 0";
						rsBatchUT.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
						rsBatchUT.AddNew();
						rsBatchUT.Update();
						rsBatchBackup.Set_Fields("PaymentKey", rsBatchUT.Get_Fields("ID"));
						rsBatchBackup.Update(true);
						AdjustPaymentAmount(rsBatchBackup.Get_Fields("Prin") + rsBatchBackup.Get_Fields("Int") + rsBatchBackup.Get_Fields("Cost") + rsBatchBackup.Get_Fields("Tax"), true);

						// Account
						rsBatchUT.Set_Fields("AccountKey", rsBillRec.Get_Fields("AccountKey"));
						// Bill Number
						rsBatchUT.Set_Fields("BillNumber", rsBatchBackup.Get_Fields("BillNumber"));
						// BillKey
						// XXXX                If rsBillRec.Fields("LienRecordNumber") = 0 Then
						rsBatchUT.Set_Fields("BillKey", rsBillRec.Get_Fields("ID"));
						// XXXX                Else
						// XXXX                    rsBatchUT.Fields("BillKey") = rsBillRec.Fields("LienRecordNumber")
						// XXXX                End If
						// Service
						rsBatchUT.Set_Fields("Service", strSrvc);
						// CHGINTNumber
						rsBatchUT.Set_Fields("CHGINTNumber", 0);
						// CHGINTDate
						rsBatchUT.Set_Fields("CHGINTDate", DateAndTime.DateValue(FCConvert.ToString(0)));
						// ActualSystemDate
						rsBatchUT.Set_Fields("ActualSystemDate", DateTime.Today);
						// EffectiveInterestDate
						rsBatchUT.Set_Fields("EffectiveInterestDate", rsBatchBackup.Get_Fields("ETDate"));
						// RecordedTransactionDate
						rsBatchUT.Set_Fields("RecordedTransactionDate", rsBatchBackup.Get_Fields("BatchRecoverDate"));
						// Teller
						rsBatchUT.Set_Fields("Teller", strTLRID);
						// Reference
						if (Strings.Trim(rsBatchBackup.Get_Fields("Ref")).Length > 10)
						{
							rsBatchUT.Set_Fields("Reference", Strings.Left(Strings.Trim(rsBatchBackup.Get_Fields("Ref")), 10));
						}
						else
						{
							rsBatchUT.Set_Fields("Reference", Strings.Trim(rsBatchBackup.Get_Fields("Ref")));
						}
						// Period
						rsBatchUT.Set_Fields("Period", "A");
						// Code
						rsBatchUT.Set_Fields("Code", "P");
						// ReceiptNumber
						// this has to be a check to see if it is coming from CR not just if they have it
						if (modExtraModules.IsThisCR())
						{
							rsBatchUT.Set_Fields("ReceiptNumber", -1);
						}
						else
						{
							rsBatchUT.Set_Fields("ReceiptNumber", 0);
						}
						// Principal
						rsBatchUT.Set_Fields("Principal", Math.Round(dblCurPrin, 2));
						// PreLienInterest
						rsBatchUT.Set_Fields("PreLienInterest", Math.Round(dblPLInt, 2));
						// CurrentInterest
						rsBatchUT.Set_Fields("CurrentInterest", Math.Round(dblCurInt, 2));
						// LienCost
						rsBatchUT.Set_Fields("LienCost", Math.Round(dblCurCost, 2));
						// Tax
						rsBatchUT.Set_Fields("Tax", Math.Round(dblCurTax, 2));
						// TransNumber
						rsBatchUT.Set_Fields("TransNumber", 0);
						// PaidBy
						rsBatchUT.Set_Fields("PaidBy", strPaidBy);
						// Comments
						rsBatchUT.Set_Fields("Comments", "This was a batch transaction.");
						// CashDrawer
						rsBatchUT.Set_Fields("CashDrawer", "Y");
						// GeneralLedger
						rsBatchUT.Set_Fields("GeneralLedger", "Y");
						// BudgetaryAccountNumber
						rsBatchUT.Set_Fields("BudgetaryAccountNumber", "");
						// BillCode
						// XXXX                If rsBillRec.Fields("LienRecordNumber") <> 0 Then
						// XXXX                    rsBatchUT.Fields("BillCode") = "L"
						// XXXX                Else
						// XXXX                    rsBatchUT.Fields("BillCode") = "R"
						// XXXX                End If
						rsBatchUT.Set_Fields("Lien", false);

						if (!rsBatchUT.Update(false))
						{ // if there is an error creating this record
							MessageBox.Show("Error #" + rsBatchUT.ErrorNumber + " - " + rsBatch.ErrorDescription + " for account number " + rsBatchBackup.Get_Fields("AccountNumber"), "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							CreateBatchUTRecords = false;
						}
						else
						{ // if the record was created correctly
							if (dblXtraCurInt != 0)
							{ // if there is interest that has to be charged, then this will do it
								rsBatchUT.FindFirstRecord("AccountKey", rsBillRec.Get_Fields("AccountKey"));
								if (CreateBatchCHGINTRecord(rsBatchUT, dblXtraCurInt))
								{
									rsBatchUT.Edit();
									rsBatchUT.Set_Fields("CHGINTNumber", lngBatchCHGINTNumber);
									rsBatchUT.Update(true);
								}
								else
								{
									MessageBox.Show("Error creating Charged Interest Record for account number " + rsBatchBackup.Get_Fields("AccountNumber") + ".", "Batch CHGINT Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								}
							}

							// MAL@20080603: Change to not update CR records yet
							// Tracker Reference: 14055
							if (!modExtraModules.IsThisCR())
							{

								// XXXXXXXXXXXXX DON'T PROCESS LIEN PAYMENT - ADD TAX
								// MAL@20071210: Update Bill Record with Amounts Listed
								// Tracker Reference: 11594
								if (rsBillRec.Get_Fields("LienRecordNumber") != 0)
								{
									// Update Lien Record
									// rsLien.OpenRecordset "SELECT * FROM Lien WHERE LienRecordNumber = " & SERVICE & rsBillRec.Fields("LienRecordNumber"), strUTDatabase
									// If rsLien.RecordCount > 0 Then
									// rsLien.Edit
									// rsLien.Fields("PrincipalPaid") = rsLien.Fields("PrincipalPaid") + Round(dblCurPrin, 2)
									// rsLien.Fields("InterestPaid") = rsLien.Fields("InterestPaid") + Round(dblCurInt, 2)
									// rsLien.Fields("CostsPaid") = rsLien.Fields("CostsPaid") + Round(dblCurCost, 2)
									// rsLien.Fields("TaxPaid") = rsLien.Fields("TaxPaid") + Round(dblCurTax, 2)
									// rsLien.Fields("PLIPaid") = rsLien.Fields("PLIPaid") + Round(dblPLInt, 2)
									// rsLien.Update
									// End If
								}
								else
								{
									// Update Bill Record
									rsBillRec.Edit();
									rsBillRec.Set_Fields(strSrvc + "PrinPaid", rsBillRec.Get_Fields(strSrvc + "PrinPaid") + Math.Round(dblCurPrin, 2));
									rsBillRec.Set_Fields(strSrvc + "IntPaid", rsBillRec.Get_Fields(strSrvc + "IntPaid") + Math.Round(dblCurInt, 2));
									rsBillRec.Set_Fields(strSrvc + "CostPaid", rsBillRec.Get_Fields(strSrvc + "CostPaid") + Math.Round(dblCurCost, 2));
									rsBillRec.Set_Fields(strSrvc + "TaxPaid", rsBillRec.Get_Fields(strSrvc + "TaxPaid") + Math.Round(dblCurTax, 2));
									rsBillRec.Update();
								}
							}
						}
					}
					else
					{
						MessageBox.Show("Error processing account number " + rsBatchBackup.Get_Fields("AccountNumber") + ", no bill found.", "Bill Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					rsBatchBackup.MoveNext();
				} while (!rsBatchBackup.EndOfFile());
				CreateBatchUTRecords = true;
			}
			else
			{
				MessageBox.Show("Error loading batch payments.", "Batch Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				CreateBatchUTRecords = false;
			}
			return CreateBatchUTRecords;
		}

		private bool CreateBatchCHGINTRecord(clsDRWrapper rsTemp, double dblInt)
		{
			bool CreateBatchCHGINTRecord = false;
			// this will create an CHGINT interest record with a status of Pending if there is CR
			DateTime dtStartingDate; // this will be the beginning date of interest
			DateTime dtEffectiveInterestDate;
			clsDRWrapper rsCHGINT = new clsDRWrapper();
			string strSQL;

			lngBatchCHGINTNumber = 0;

			strSQL = "SELECT * FROM PaymentRec WHERE ID = 0";
			if (rsCHGINT.OpenRecordset(strSQL, modExtraModules.strUTDatabase))
			{
				rsCHGINT.AddNew();
				rsCHGINT.Update();
				lngBatchCHGINTNumber = rsCHGINT.Get_Fields_Int32("ID");
				// Account
				rsCHGINT.Set_Fields("AccountKey", rsTemp.Get_Fields("AccountKey"));
				// Bill Number
				rsCHGINT.Set_Fields("BillNumber", rsTemp.Get_Fields("BillNumber"));
				// BillKey
				rsCHGINT.Set_Fields("BillKey", rsTemp.Get_Fields("BillKey"));
				// Service
				rsCHGINT.Set_Fields("Service", rsTemp.Get_Fields("Service"));
				// CHGINTNumber
				rsCHGINT.Set_Fields("CHGINTNumber", rsTemp.Get_Fields("ID")); // XXXX? IS THIS CORRECT?
																			  // CHGINTDate - this is the last interest date in order to backtrack (delete payments)
				rsCHGINT.Set_Fields("CHGINTDate", dtIntPaidDate);
				// ActualSystemDate
				rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Today);
				// EffectiveInterestDate
				rsCHGINT.Set_Fields("EffectiveInterestDate", rsTemp.Get_Fields("EffectiveInterestDate"));
				// RecordedTransactionDate
				rsCHGINT.Set_Fields("RecordedTransactionDate", rsTemp.Get_Fields("RecordedTransactionDAte"));
				// Teller
				rsCHGINT.Set_Fields("Teller", strTLRID);
				// Reference
				rsCHGINT.Set_Fields("Reference", "CHGINT");
				// Period
				rsCHGINT.Set_Fields("Period", "A");
				// Code
				rsCHGINT.Set_Fields("Code", "I");
				// ReceiptNumber
				// this has to be a check to see if it is coming from CR not just if they have it
				if (modExtraModules.IsThisCR())
				{
					rsCHGINT.Set_Fields("ReceiptNumber", -1);
				}
				else
				{
					rsCHGINT.Set_Fields("ReceiptNumber", 0);
				}
				// Principal
				rsCHGINT.Set_Fields("Principal", 0);
				// PreLienInterest
				rsCHGINT.Set_Fields("PreLienInterest", 0);
				// CurrentInterest
				rsCHGINT.Set_Fields("CurrentInterest", Math.Round(dblXtraCurInt * -1, 2));
				// LienCost
				rsCHGINT.Set_Fields("LienCost", 0);
				// Tax
				rsCHGINT.Set_Fields("Tax", 0);
				// TransNumber
				rsCHGINT.Set_Fields("TransNumber", 0);
				// PaidBy
				rsCHGINT.Set_Fields("PaidBy", strPaidBy);
				// Comments
				rsCHGINT.Set_Fields("Comments", "This was a batch transaction.");
				// CashDrawer
				rsCHGINT.Set_Fields("CashDrawer", "Y");
				// GeneralLedger
				rsCHGINT.Set_Fields("GeneralLedger", "Y");
				// BudgetaryAccountNumber
				rsCHGINT.Set_Fields("BudgetaryAccountNumber", "");
				// BillCode
				rsCHGINT.Set_Fields("Lien", rsTemp.Get_Fields("Lien"));

				if (rsCHGINT.Update(false))
				{ // make sure that it updated
					CreateBatchCHGINTRecord = true;
				}
				else
				{
					MessageBox.Show("Error #" + rsCHGINT.ErrorNumber + " - " + rsCHGINT.ErrorDescription + ".", "Batch CHGINT Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					CreateBatchCHGINTRecord = false;
				}
			}
			else
			{
				MessageBox.Show("Error #" + rsCHGINT.ErrorNumber + " - " + rsCHGINT.ErrorDescription + ".", "Batch CHGINT Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				CreateBatchCHGINTRecord = false;
			}
			return CreateBatchCHGINTRecord;
		}

		// VBto upgrade warning: 'Return' As DateTime	OnWriteFCConvert.ToInt16(
		private DateTime GetIntStartDate(ref int lngRateKey, ref int lngYear)
		{
			DateTime GetIntStartDate = System.DateTime.Now;
			// this will return the interest start date of the bill from the rate record in case no payments have been made
			// to set the InterestPaidThroughDate
			clsDRWrapper rsRateRec = new clsDRWrapper();

			rsRateRec.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRateKey));
			if (!rsRateRec.EndOfFile() && !rsRateRec.BeginningOfFile())
			{
				if (FCConvert.CBool(rsRateRec.Get_Fields("InterestStartDate1")) != 0)
				{
					GetIntStartDate = rsRateRec.Get_Fields_DateTime("InterestStartDate1");
				}
			}

			if (GetIntStartDate.ToOADate() == 0)
			{
				GetIntStartDate = DateTime.FromOADate(0);
			}
			return GetIntStartDate;
		}

		private bool CheckBatchQuestions()
		{
			bool CheckBatchQuestions = false;

			// check the Teller ID
			if (viewModel.ValidTellerId(txtTellerID.Text))
			{
				viewModel.TellerId = Strings.Trim(txtTellerID.Text);
			}
			else
			{
				MessageBox.Show("Please enter a valid Teller ID.", "Teller Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
			}


			// check the Payment date
			if (Strings.Trim(txtPaymentDate.Text) != "")
			{
				if (Information.IsDate(txtPaymentDate.Text))
				{
					viewModel.PaymentDate = DateAndTime.DateValue(txtPaymentDate.Text);
				}
				else
				{
					MessageBox.Show("Please enter a valid payment date.", "Invalid Payment Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return false;
				}
			}
			else
			{
				MessageBox.Show("Please enter a payment date.", "Invalid Payment Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
			}

			// check the Effective date
			if (Strings.Trim(txtEffectiveDate.Text) != "")
			{
				if (Information.IsDate(txtEffectiveDate.Text))
				{
					viewModel.EffectiveDate = DateAndTime.DateValue(txtEffectiveDate.Text);
				}
				else
				{
					MessageBox.Show("Please enter a valid effective date.", "Invalid Effective Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return false;
				}
			}
			else
			{
				MessageBox.Show("Please enter a effective date.", "Invalid Effective Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
			}
		
			if (Strings.Trim(txtPaidBy.Text) != "")
			{
				viewModel.PaidBy = Strings.Trim(txtPaidBy.Text);
			}
			else
			{
				MessageBox.Show("Please enter a Paid By name.", "Invalid Paid By Name", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return false;
			}

			return true;
		}

		private void UpdatePrintReceiptChoice()
		{
			// this sub will update the BatchRecover table with the print receipt checkbox values
			clsDRWrapper rsCHK = new clsDRWrapper();
			string strPrint = "";
			int intCT;
			string strSQL = "";
			int lngErrNum = 0;
			string strErrDesc = "";

			try
			{
				for (intCT = 1; intCT <= vsBatch.Rows - 1; intCT++)
				{
					if (Conversion.Val(vsBatch.TextMatrix(intCT, 10)) == -1)
					{
						strPrint = "'True'";
					}
					else
					{
						strPrint = "'False'";
					}

					strSQL = "UPDATE BatchRecover Set PrintReceipt = " + strPrint + " WHERE TellerID = '" + strTLRID + "' AND BatchRecoverDate = '" + FCConvert.ToString(dtPaymentDate) + "' AND ETDate = '" + FCConvert.ToString(dtEffectiveDate) + "' AND " + "BillNumber = " + vsBatch.TextMatrix(intCT, lngColBatchBill) + " AND Service = '" + vsBatch.TextMatrix(intCT, lngColBatchService) + "' AND AccountNumber = " + vsBatch.TextMatrix(intCT, lngColBatchAcct);
					if (!rsCHK.Execute(strSQL, modExtraModules.strUTDatabase, false))
					{
						lngErrNum = rsCHK.ErrorNumber;
						strErrDesc = rsCHK.ErrorDescription;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				if (lngErrNum != 0)
				{
					MessageBox.Show("Error #" + FCConvert.ToString(lngErrNum) + " - " + strErrDesc + " has occurred updating the Print Receipt Column.", "Update Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void SavePayment()
		{
			if (txtAmount.Text != "")
			{
				if (MessageBox.Show("Would you like to save this payment?", "Save Payment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (txtAmount.DataChanged)
					{
						if (Conversion.Val(txtAmount.Text) != 0)
						{
							if (FCConvert.ToDouble(txtAmount.Text) > dblTotalDue)
							{
								if (MessageBox.Show("Payment is more than due, would you like to continue.", "Overpayment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) != DialogResult.Yes)
								{
									return;
								}
								else
								{
									modGlobalFunctions.AddCYAEntry_6("CR", "Added CL batch overpayment. - Acct: " + txtBatchAccount.Text);
								}
							}
							if (AdjustPaymentAmount(FCConvert.ToDouble(txtAmount.Text)) != 0)
							{
								return; // error has occured, exit sub and do not save payment
							}
						}
						else
						{
							MessageBox.Show("Payment amount must be numeric and greater than 0.", "Invalid Payment Amount", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return;
						}
					}
					AddPaymentToBatch(rsBatch, FCConvert.ToInt32(FCConvert.ToDouble(txtBatchAccount.Text)));
				}
				else
				{
					txtBatchAccount.Focus();
				}
			}
		}

		private void ShowAccount(int lngAcct)
		{
			// this will show the account selected in acct box
			txtBatchAccount.Text = FCConvert.ToString(lngAcct);

			// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
			// fraBatchSearchResults.Visible = False
			// fraBatchSearchCriteria.Visible = False
			fraBatch.Enabled = true;
		}

		private void ImportPayportBatch()
		{
			bool boolOpen = false;
			StreamReader ts = null;
			try
			{
				string strFileName = "";
				Information.Err().Clear();
				MDIParent.InstancePtr.CommonDialog1.Flags = FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNNoChangeDir) + FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNFileMustExist) + FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNExplorer) + FCConvert.ToInt32(FCCommonDialog.FileOpenConstants.cdlOFNLongNames);
				MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				MDIParent.InstancePtr.CommonDialog1.FileName = "";
				MDIParent.InstancePtr.CommonDialog1.InitDir = Environment.CurrentDirectory;
				/*? On Error Resume Next  */
				MDIParent.InstancePtr.CommonDialog1.ShowOpen();
				if (Information.Err().Number != 0)
				{
					Information.Err().Clear();
					return;
				}
				strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;

				boolBatchImport = true;
				boolOpen = false;
				string strLine = "";
				double dblFileTot;
				int lngLoadedYear;
				dblFileTot = 0;
				if (FCFileSystem.FileExists(strFileName))
				{
					ts = FCFileSystem.OpenText(strFileName);
					boolOpen = true;
					while (!ts.EndOfStream)
					{
						//Application.DoEvents();
						strLine = ts.ReadLine();
						if (Strings.UCase(Strings.Left(strLine, 6)) != "BILLID")
						{
							dblFileTot += TestPayport(strLine);
						}
					}
					ts.Close();
					boolOpen = false;
					if (MessageBox.Show("The total file amount is " + Strings.Format(dblFileTot, "#,###,###,##0.00") + " is this correct?", "Confirm Amount", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
					{
						return;
					}
					ts = FCFileSystem.OpenText(strFileName);
					boolOpen = true;
					while (!ts.EndOfStream)
					{
						//Application.DoEvents();
						strLine = ts.ReadLine();
						if (Strings.UCase(Strings.Left(strLine, 6)) != "BILLID")
						{
							ParsePayport(strLine, txtPaidBy.Text);
						}
					}
					ts.Close();
					boolOpen = false;
				}
				else
				{
					MessageBox.Show("File " + strFileName + " not found", "File Not Found", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				boolBatchImport = false;
				txtTotal.Text = Strings.Format(CalculateGridTotal(), "#,##0.00");

				txtBatchAccount.Text = "";
				txtAmount.Text = "";
				return;
			}
			catch (Exception ex)
			{
				if (boolOpen)
				{
					ts.Close();
				}
				MessageBox.Show("Error Number " + Information.Err(ex).Description + "\r\n" + "In Import", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private double TestPayport(string strLine)
		{
			double TestPayport = 0;
			double dblReturn;
			string[] arrDet = null;

			// Payment detail format "|" Delimited record - Bill ID|Bill Type|Bill Number|Account|Pmt Date|Pmt Amount|Name

			dblReturn = 0;
			arrDet = Strings.Split(strLine, "|", -1, CompareConstants.vbBinaryCompare);
			if (Conversion.Val(arrDet[2]) > 0)
			{
				if (Conversion.Val(arrDet[3]) > 0)
				{
					// account is greater than 0
					dblReturn = Conversion.Val(arrDet[5]);
				}
			}
			TestPayport = dblReturn;
			return TestPayport;
		}

		private void ParsePayport(string strLine, string strPaidName)
		{
			string[] arrDet = null;

			int lngAccount;
			int lBill;
			Decimal crPayment;
			string strPaidBy = "";
			string strSrvc;
			bool boolCancel = false;

			// Payment detail format "|" Delimited record - Bill ID|Bill Type|Bill Number|Account|Pmt Date|Pmt Amount|Name
			arrDet = Strings.Split(strLine, "|", -1, CompareConstants.vbBinaryCompare);

			lBill = FCConvert.ToInt32(Math.Round(Conversion.Val(arrDet[2])));
			lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(arrDet[3])));
			strSrvc = Strings.Left(arrDet[1], 1); // The service is SW or WA(?)

			if (lngAccount > 0)
			{
				crPayment = FCConvert.ToDecimal(Conversion.Val(Strings.Trim(arrDet[5])));
				if (crPayment != 0)
				{
					strPaidBy = Strings.Trim(arrDet[6]);
					if (strPaidBy == "" || modExtraModules.IsThisCR()) strPaidBy = strPaidName;
					txtBatchAccount.Text = FCConvert.ToString(lngAccount);
					if (crPayment != 0)
					{
						boolCancel = false;
						txtBillNumber.Text = FCConvert.ToString(lBill);
						if (strSrvc == "S")
						{
							cmbSewer.Text = "Sewer";
						}
						else
						{
							cmbSewer.Text = "Water";
						}
						txtAmount.Text = FCConvert.ToString(crPayment);
						txtBatchAccount_Validate(ref boolCancel);
						if (!boolCancel)
						{
							txtAmount.Text = FCConvert.ToString(crPayment);
							if (AdjustPaymentAmount(FCConvert.ToDouble(crPayment)) == 0)
							{
								CreateBatchPaymentLine(lngAccount, dblCurPrin, dblCurTax, dblCurInt + dblPLInt, dblCurCost, dtPaymentDate, dtEffectiveDate, lBill, strSrvc);
							}
						}
					}
				}
			}
		}

		private void cmdCancel_Click(object sender, EventArgs e)
		{
			if (viewModel.BatchStarted)
			{
				ShowBatch();
				mnuFileImportPayPortBatch.Enabled = true;
			}
			else
			{
				ShowBatchQuestions();
				mnuFileImportPayPortBatch.Enabled = false;
			}
		}
	}
}
