﻿using System;
using fecherFoundation;
using Global;
using SharedApplication;
using SharedApplication.Extensions;
using SharedApplication.TaxCollections.Lien;
using SharedApplication.UtilityBilling.Models;
using SharedApplication.UtilityBilling.Receipting.Interfaces;
using TWSharedLibrary;
using Wisej.Web;


namespace TWUT0000.Receipting
{
	public partial class frmUBNewLienDischargeNotice : BaseForm, IModalView<IUtilityBillingLienDischargeNoticeViewModel>
	{
		public frmUBNewLienDischargeNotice()
		{
			InitializeComponent();
		}
		
		private GlobalColorSettings colorSettings;
		public frmUBNewLienDischargeNotice(IUtilityBillingLienDischargeNoticeViewModel viewModel, GlobalColorSettings colorSettings) : this()
		{
			ViewModel = viewModel;
			this.colorSettings = colorSettings;
		}

		bool boolLoaded;
		int lngLienRecordNumber;
		bool boolPrintAllSaved;
		bool boolPrintArchive;
		bool boolAbateDefault;
		string strAbate;
		string strPayment;
		bool boolDefaultAppearedDate;
		int lngColData;
		int lngColTitle;
		int lngColHidden;
		int lngRowTName;
		int lngRowMuni;
		int lngRowCounty;
		int lngRowOwner1;
		int lngRowOwner2;
		int lngRowPayDate;
		int lngRowFileDate;
		int lngRowBook;
		int lngRowPage;
		int lngRowHisHer;
		int lngRowSignerName;
		int lngRowTitle;
		int lngRowSignerDesignation;
		int lngRowCommissionExpiration;
		int lngRowMapLot;
		int lngRowSignedDate;
		int lngRowAbatementDefault;
		int lngRowAppearedDate;
		int lngTempAccountNumber;
		DateTime dtPaymentDate;
		bool boolWater;
		string strWS = "";

		private void frmUBNewLienDischargeNotice_Load(object sender, EventArgs e)
		{
			Init();
			boolLoaded = true;
			FormatGrid(true);

			strAbate = "Paid With Abatement";
			strPayment = "Paid With Payment";

			LoadSettings();
		}

		private void Init()
		{
			try
			{
				lngColData = 2;
				lngColHidden = 1;
				lngColTitle = 0;
				lngRowTName = 0;
				lngRowTitle = 1;
				lngRowMuni = 2;
				lngRowCounty = 3;
				lngRowOwner1 = 4;
				lngRowOwner2 = 5;
				lngRowPayDate = 6;
				lngRowFileDate = 7;
				lngRowSignedDate = 8;
				lngRowBook = 9;
				lngRowPage = 10;
				lngRowMapLot = 11;
				lngRowAbatementDefault = 12;
				lngRowHisHer = 13;
				lngRowSignerName = 14;
				lngRowSignerDesignation = 15;
				lngRowCommissionExpiration = 16;
				lngRowAppearedDate = 17;

				if (ViewModel.LienToDischarge != null)
				{
					lblYear.Text = "Lien : " + FCConvert.ToString(ViewModel.LienToDischarge.ID);
					lblYear.Visible = true;
				}
				else
				{
					lblYear.Visible = false;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FormatGrid(bool boolReset = false)
		{
			int lngWid;
			lngWid = vsData.WidthOriginal;
			if (boolPrintAllSaved)
			{
				if (boolReset)
				{
					vsData.Rows = 0;
					vsData.Rows = 10;
				}
				vsData.ColWidth(lngColTitle, (lngWid * 0.45).ToInteger());
				// title
				vsData.ColWidth(lngColHidden, 0);
				// hidden row
				vsData.ColWidth(lngColData, (lngWid * 0.53).ToInteger());
				// data
				vsData.TextMatrix(lngRowTName, lngColTitle, "Treasurer Name");
				vsData.TextMatrix(lngRowMuni, lngColTitle, "Organization");
				vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
				vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Treasurer Signing Date");
				vsData.TextMatrix(lngRowHisHer, lngColTitle, "His / Her  (Treasurer)");
				vsData.TextMatrix(lngRowSignerName, lngColTitle, "Signer's Name");
				vsData.TextMatrix(lngRowTitle, lngColTitle, "Treasurer Title");
				vsData.TextMatrix(lngRowSignerDesignation, lngColTitle, "Signer's Designation");
				vsData.TextMatrix(lngRowCommissionExpiration, lngColTitle, "Commission Expiration");
				vsData.TextMatrix(lngRowAppearedDate, lngColTitle, "Appeared Date");
			}
			else
			{
				if (boolReset)
				{
					vsData.Rows = 0;
					vsData.Rows = 18;
				}
				vsData.ColWidth(lngColTitle, (lngWid * 0.38).ToInteger());
				// title
				vsData.ColWidth(lngColHidden, 0);
				// hidden row
				vsData.ColWidth(lngColData, (lngWid * 0.62).ToInteger());
				// data
				vsData.TextMatrix(lngRowTName, lngColTitle, "Treasurer Name");
				vsData.TextMatrix(lngRowMuni, lngColTitle, "Organization");
				vsData.TextMatrix(lngRowCounty, lngColTitle, "County");
				vsData.TextMatrix(lngRowOwner1, lngColTitle, "Owner Name");
				vsData.TextMatrix(lngRowOwner2, lngColTitle, "Second Owner");
				vsData.TextMatrix(lngRowPayDate, lngColTitle, "Payment Date");
				vsData.TextMatrix(lngRowFileDate, lngColTitle, "Filing Date");
				vsData.TextMatrix(lngRowSignedDate, lngColTitle, "Treasurer Signing Date");
				vsData.TextMatrix(lngRowBook, lngColTitle, "Book");
				vsData.TextMatrix(lngRowPage, lngColTitle, "Page");
				vsData.TextMatrix(lngRowMapLot, lngColTitle, "Map Lot");
				vsData.TextMatrix(lngRowAbatementDefault, lngColTitle, "How was the lien paid?");
				vsData.TextMatrix(lngRowHisHer, lngColTitle, "His / Her  (Treasurer)");
				vsData.TextMatrix(lngRowSignerName, lngColTitle, "Signer's Name");
				vsData.TextMatrix(lngRowTitle, lngColTitle, "Treasurer Title");
				vsData.TextMatrix(lngRowSignerDesignation, lngColTitle, "Signer's Designation");
				vsData.TextMatrix(lngRowCommissionExpiration, lngColTitle, "Commission Expiration");
				vsData.TextMatrix(lngRowAppearedDate, lngColTitle, "Appeared Date");
			}
		}

		private bool SaveSettings()
		{

			DateTime dtComExp;
			DateTime dtAppear;

			if (ValidateAnswers())
			{
				var dischargeNotice = ViewModel.GetControlDischargeNotice();

				dischargeNotice.County = vsData.TextMatrix(lngRowCounty, lngColData);
				dischargeNotice.Treasurer = vsData.TextMatrix(lngRowTName, lngColData);

				// his/her
				if (vsData.TextMatrix(lngRowHisHer, lngColData).ToUpper().StartsWith("HIS"))
				{
					dischargeNotice.Male = true;
				}
				else
				{
					dischargeNotice.Male = false;
				}

				dischargeNotice.SignerName = vsData.TextMatrix(lngRowSignerName, lngColData).Trim();
				dischargeNotice.SignerDesignation = vsData.TextMatrix(lngRowSignerDesignation, lngColData).Trim();

				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
				{
					if (vsData.TextMatrix(lngRowCommissionExpiration, lngColData).IsDate())
					{
						dischargeNotice.CommissionExpiration =
							vsData.TextMatrix(lngRowCommissionExpiration, lngColData).ToDate();
					}
					else
					{
						dischargeNotice.CommissionExpiration = null;
					}
				}
				else
				{
					dischargeNotice.CommissionExpiration = null;
				}
				ViewModel.SaveNotice(dischargeNotice);
				vsData.Select(0, 0);
				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
				{
					dtComExp = DateAndTime.DateValue(vsData.TextMatrix(lngRowCommissionExpiration, lngColData));
				}
				else
				{
					dtComExp = DateTime.FromOADate(0);
				}
				if (Strings.InStr(1, vsData.TextMatrix(lngRowAppearedDate, lngColData), "_") == 0 && Strings.Trim(vsData.TextMatrix(lngRowAppearedDate, lngColData)) != "")
				{
					dtAppear = DateAndTime.DateValue(vsData.TextMatrix(lngRowAppearedDate, lngColData));
				}
				else
				{
					dtAppear = DateTime.FromOADate(0);
				}
				if (Strings.UCase(Strings.Trim(vsData.TextMatrix(lngRowAbatementDefault, lngColData))) == Strings.UCase(Strings.Trim(strAbate)))
				{
					boolAbateDefault = true;
				}
				else
				{
					boolAbateDefault = false;
				}

				arUTLienDischargeNotice.InstancePtr.Init(ViewModel.LienToDischarge.Water ?? false, vsData.TextMatrix(lngRowTName, lngColData), vsData.TextMatrix(lngRowMuni, lngColData), vsData.TextMatrix(lngRowCounty, lngColData), vsData.TextMatrix(lngRowOwner1, lngColData), Strings.Trim(vsData.TextMatrix(lngRowOwner2, lngColData)), DateAndTime.DateValue(vsData.TextMatrix(lngRowPayDate, lngColData)), DateAndTime.DateValue(vsData.TextMatrix(lngRowFileDate, lngColData)), vsData.TextMatrix(lngRowBook, lngColData), vsData.TextMatrix(lngRowPage, lngColData), vsData.TextMatrix(lngRowHisHer, lngColData), vsData.TextMatrix(lngRowSignerName, lngColData), vsData.TextMatrix(lngRowSignerDesignation, lngColData), dtComExp, this.Modal, vsData.TextMatrix(lngRowMapLot, lngColData), vsData.TextMatrix(lngRowTitle, lngColData), lngTempAccountNumber, DateAndTime.DateValue(vsData.TextMatrix(lngRowSignedDate, lngColData)), dtAppear, boolAbateDefault);

				ViewModel.SetDischargeNeededToPrinted();
				ViewModel.SetLienDischargePrinted();
				return true;
			}

			return false;
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
				var dischargeNotice = ViewModel.GetControlDischargeNotice();

				if (lngRowAbatementDefault > 0)
				{
					if (ViewModel.AbateDefault)
					{
						vsData.TextMatrix(lngRowAbatementDefault, lngColData, strAbate);
					}
					else
					{
						vsData.TextMatrix(lngRowAbatementDefault, lngColData, strPayment);
					}
				}

				if (dischargeNotice != null)
				{
					// load all of the rows
					// Row - 1  'Treasurer Name
					vsData.TextMatrix(lngRowTName, lngColData, dischargeNotice.Treasurer);
					// Row - 3  'County
					// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
					vsData.TextMatrix(lngRowCounty, lngColData, dischargeNotice.County);
					// Row - 10 'His/Her
					if (dischargeNotice.Male.GetValueOrDefault())
					{
						vsData.TextMatrix(lngRowHisHer, lngColData, "His");
					}
					else
					{
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
					}
					// Signer's name - 2
					vsData.TextMatrix(lngRowSignerName, lngColData, dischargeNotice.SignerName);
					// Signer's Designation
					vsData.TextMatrix(lngRowSignerDesignation, lngColData, dischargeNotice.SignerDesignation);
					// Commission Expiration Date
					vsData.TextMatrix(lngRowCommissionExpiration, lngColData, dischargeNotice.CommissionExpiration.GetValueOrDefault().FormatAndPadShortDate());
				}
				else
				{
					var lienControl = ViewModel.GetControlLienProcess();

					if (lienControl != null)
					{
						// Row - 1  'Treasurer Name
						vsData.TextMatrix(lngColTitle, lngColData, lienControl.CollectorName);
						// Row - 3  'County
						// TODO: Check the table for the column [County] and replace with corresponding Get_Field method
						vsData.TextMatrix(lngRowCounty, lngColData, lienControl.County);
						// Row - 10 'His/Her
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
						// Signer's name
						vsData.TextMatrix(lngRowSignerName, lngColData, lienControl.Signer);
						// Signer's Designation
						vsData.TextMatrix(lngRowSignerDesignation, lngColData, lienControl.Designation);
						// Commission Expiration Date
						vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					}
					else
					{
						// Row - 1  'Treasurer Name
						vsData.TextMatrix(lngRowTName, lngColData, "");
						// Row - 3  'County
						vsData.TextMatrix(lngRowCounty, lngColData, "");
						// Row - 10 'His/Her
						vsData.TextMatrix(lngRowHisHer, lngColData, "Her");
						// Signer's name
						vsData.TextMatrix(lngRowSignerName, lngColData, "");
						// Signer's Designation
						vsData.TextMatrix(lngRowSignerDesignation, lngColData, "");
						// Commission Expiration Date
						vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
					}
				}
				// Row - 0  Titles
				vsData.TextMatrix(lngRowTitle, lngColData, "Treasurer");
				// Row - 2  Muni Name
				vsData.TextMatrix(lngRowMuni, lngColData, ViewModel.TownName());
				var mainBill = ViewModel.GetLienBill();

				vsData.TextMatrix(lngRowOwner1, lngColData, mainBill.Oname);
				vsData.TextMatrix(lngRowOwner2, lngColData, mainBill.Oname2);

				vsData.TextMatrix(lngRowPayDate, lngColData, ViewModel.PayDate.FormatAndPadShortDate());
				vsData.TextMatrix(lngRowSignedDate, lngColData, ViewModel.PayDate.FormatAndPadShortDate());
				vsData.TextMatrix(lngRowAppearedDate, lngColData, "");
				vsData.TextMatrix(lngRowFileDate, lngColData, ViewModel.LienToDischarge.RateKey.BillDate?.FormatAndPadShortDate());
				vsData.TextMatrix(lngRowBook, lngColData, ViewModel.LienToDischarge.Book);
				vsData.TextMatrix(lngRowPage, lngColData, ViewModel.LienToDischarge.Page);
				// Row - Map Lot
				var account = ViewModel.GetAccountDetails(mainBill.AccountId);

				vsData.TextMatrix(lngRowMapLot, lngColData, (mainBill.MapLot ?? "") != "" ? mainBill.MapLot : account?.MapLot ?? "");
				lngTempAccountNumber = account.AccountNumber ?? 0;
			}
			catch (Exception ex)
			{
				// ERROR_HANDLER:
				FCMessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + ex.GetBaseException().Message + ".", MsgBoxStyle.Critical, "Error Loading Settings");
			}
		}

		public IUtilityBillingLienDischargeNoticeViewModel ViewModel { get; set; }
		public void ShowModal()
		{
			this.Show(FormShowEnum.Modal);
		}

		private void vsData_CurrentCellChanged(object sender, EventArgs e)
		{
			if (vsData.Col == lngColData)
			{
				if (vsData.Row == lngRowTName)
				{
					// Treasurer Name
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowMuni)
				{
					// Muni Name
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowCounty)
				{
					// County
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowOwner1)
				{
					// Name1
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowOwner2)
				{
					// Name2
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowPayDate)
				{
					// Payment Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignedDate)
				{
					// Treas Signing Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowFileDate)
				{
					// Filing Date
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowBook)
				{
					// Book
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowPage)
				{
					// Page
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowHisHer)
				{
					// His/Her
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignerName || vsData.Row == lngRowTitle)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowSignerDesignation)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowCommissionExpiration)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowAbatementDefault)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				else if (vsData.Row == lngRowAppearedDate)
				{
					vsData.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsData.EditCell();
					vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
			}
			else
			{
				vsData.Editable = FCGrid.EditableSettings.flexEDNone;
				vsData.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private bool ValidateAnswers()
		{
			vsData.Select(0, 1);
			// Row - 0  'Treasurer Name
			if (Strings.Trim(vsData.TextMatrix(lngRowTName, lngColData)) == "")
			{
				MessageBox.Show("Please enter the Treasurer's name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowTName, lngColData);
				return false;
			}
			// Row - 1  'Muni Name
			if (Strings.Trim(vsData.TextMatrix(lngRowMuni, lngColData)) == "")
			{
				MessageBox.Show("Please enter the Municipality name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowMuni, lngColData);
				return false;
			}
			// Row - 2  'County
			if (Strings.Trim(vsData.TextMatrix(lngRowCounty, lngColData)) == "")
			{
				MessageBox.Show("Please enter the County name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowCounty, lngColData);
				return false;
			}
			// Row - 5  'Payment Date
			if (Strings.Trim(vsData.TextMatrix(lngRowSignedDate, lngColData)) == "")
			{
				MessageBox.Show("Please enter a treasurer signing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowSignedDate, lngColData);
				return false;
			}
			else
			{
				if (!Information.IsDate(vsData.TextMatrix(lngRowSignedDate, lngColData)))
				{
					MessageBox.Show("Please enter a valid treasurer signing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowSignedDate, lngColData);
					return false;
				}
			}
			
			// Row - 3  'Name1
			if (Strings.Trim(vsData.TextMatrix(lngRowOwner1, lngColData)) == "")
			{
				MessageBox.Show("Please enter the Owner's name.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowOwner1, lngColData);
				return false;
			}
			// Row - 4  'Name2
			// no validations
			// Row - 5  'Payment Date
			if (Strings.Trim(vsData.TextMatrix(lngRowPayDate, lngColData)) == "")
			{
				MessageBox.Show("Please enter a payment date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowPayDate, lngColData);
				return false;
			}
			else
			{
				if (!Information.IsDate(vsData.TextMatrix(lngRowPayDate, lngColData)))
				{
					MessageBox.Show("Please enter a valid payment date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowPayDate, lngColData);
					return false;
				}
			}
			// Row - 6  'Filing Date
			if (Strings.Trim(vsData.TextMatrix(lngRowFileDate, lngColData)) == "")
			{
				MessageBox.Show("Please enter a lien filing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				vsData.Select(lngRowFileDate, lngColData);
				return false;
			}
			else
			{
				if (!Information.IsDate(vsData.TextMatrix(lngRowFileDate, lngColData)))
				{
					MessageBox.Show("Please enter a valid lien filing date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					vsData.Select(lngRowFileDate, lngColData);
					return false;
				}
			}
			
			if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) != "")
			{
				if (Strings.Trim(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == "/  /")
				{
					vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
				}
				else
				{
					if (!Information.IsDate(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)))
					{
						if (Conversion.Val(vsData.TextMatrix(lngRowCommissionExpiration, lngColData)) == 0)
						{
							vsData.TextMatrix(lngRowCommissionExpiration, lngColData, "");
						}
						else
						{
							MessageBox.Show("Please enter a valid Commission Expiration date.", "Validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vsData.Select(lngRowCommissionExpiration, lngColData);
							return false;
						}
					}
				}
			}
			return true;
		}

		private void vsData_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsData.GetFlexRowIndex(e.RowIndex);
			int col = vsData.GetFlexColIndex(e.ColumnIndex);
			if (row == lngRowCommissionExpiration)
			{
				if (vsData.EditText == "  /  /    " || vsData.EditText == "__/__/____")
				{
					vsData.EditText = "";
					vsData.TextMatrix(row, col, "");
					vsData.EditMask = "";
				}
			}
		}

		private void vsData_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsData.EditMask = string.Empty;
			vsData.ComboList = "";
			if (vsData.Row == lngRowPayDate || vsData.Row == lngRowFileDate || vsData.Row == lngRowCommissionExpiration || vsData.Row == lngRowSignedDate || vsData.Row == lngRowAppearedDate)
			{
				vsData.EditMask = "##/##/####";
			}
			else if (vsData.Row == lngRowAbatementDefault)
			{
				vsData.ComboList = "0;" + strPayment + "|1;" + strAbate;
			}
		}

		private void vsData_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			if (vsData.Row == lngRowPayDate || vsData.Row == lngRowFileDate || vsData.Row == lngRowBook || vsData.Row == lngRowPage || vsData.Row == lngRowCommissionExpiration || vsData.Row == lngRowAppearedDate)
			{
				// only allow numbers
				if ((e.KeyChar >=  (char)Keys.D0 && e.KeyChar <= (char)Keys.D9) || (e.KeyChar == (char)Keys.Back) || (e.KeyChar == (char)Keys.Decimal) || (FCConvert.ToInt32(e.KeyChar) == 46))
				{
					// do nothing
				}
				else
				{
					e.Handled = true;
				}
			}
		}

		private void btnProcess_Click(object sender, EventArgs e)
		{
			if (SaveSettings())
			{
				Close();
			}
		}
	}
}
