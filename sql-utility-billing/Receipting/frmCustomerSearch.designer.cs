//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000.Receipting
{
	/// <summary>
	/// Summary description for frmGetMasterAccount.
	/// </summary>
	partial class frmCustomerSearch : BaseForm
	{
		public fecherFoundation.FCComboBox cmbHidden;
		public fecherFoundation.FCLabel lblHidden;
		public FCGrid vsSearch;
		public fecherFoundation.FCPanel fraCriteria;
		public fecherFoundation.FCTextBox txtSearch2;
		public fecherFoundation.FCCheckBox chkShowPreviousOwnerInfo;
		public fecherFoundation.FCTextBox txtSearch;
		public fecherFoundation.FCLabel lblSearch2;
		public fecherFoundation.FCLabel lblSearch;
		public fecherFoundation.FCLabel lblSearchInfo;
		public fecherFoundation.FCTextBox txtGetAccountNumber;
		public fecherFoundation.FCButton cmdSelectAccount;
		public fecherFoundation.FCLabel Label2;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSelectAccount;
		public fecherFoundation.FCToolStripMenuItem mnuFileSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFileClearSearch;
		public fecherFoundation.FCToolStripMenuItem mnuUndeleteMaster;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuQuit;
		public FCToolStripMenuItem mnuBatchProcessing;
		public FCToolStripMenuItem mnuFileBatchSearch;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.cmbHidden = new fecherFoundation.FCComboBox();
			this.lblHidden = new fecherFoundation.FCLabel();
			this.vsSearch = new fecherFoundation.FCGrid();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuBatchProcessing = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileBatchSearch = new fecherFoundation.FCToolStripMenuItem();
			this.fraCriteria = new fecherFoundation.FCPanel();
			this.lblNewAccountLabel = new fecherFoundation.FCLabel();
			this.cmbSearchPlace = new fecherFoundation.FCComboBox();
			this.lblSearchPlace = new fecherFoundation.FCLabel();
			this.txtGetAccountNumber = new fecherFoundation.FCTextBox();
			this.txtSearch2 = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.chkShowPreviousOwnerInfo = new fecherFoundation.FCCheckBox();
			this.txtSearch = new fecherFoundation.FCTextBox();
			this.lblSearchInfo = new fecherFoundation.FCLabel();
			this.lblSearch2 = new fecherFoundation.FCLabel();
			this.lblSearch = new fecherFoundation.FCLabel();
			this.cmdSelectAccount = new fecherFoundation.FCButton();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSelectAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClearSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuUndeleteMaster = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdUndeleteMaster = new fecherFoundation.FCButton();
			this.cmdFileClearSearch = new fecherFoundation.FCButton();
			this.cmdFileSearch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).BeginInit();
			this.fraCriteria.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowPreviousOwnerInfo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUndeleteMaster)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClearSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSelectAccount);
			this.BottomPanel.Location = new System.Drawing.Point(0, 415);
			this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsSearch);
			this.ClientArea.Controls.Add(this.fraCriteria);
			this.ClientArea.Size = new System.Drawing.Size(1078, 635);
			this.ClientArea.Controls.SetChildIndex(this.fraCriteria, 0);
			this.ClientArea.Controls.SetChildIndex(this.vsSearch, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSearch);
			this.TopPanel.Controls.Add(this.cmdFileClearSearch);
			this.TopPanel.Controls.Add(this.cmdUndeleteMaster);
			this.TopPanel.Size = new System.Drawing.Size(1078, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdUndeleteMaster, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClearSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(243, 28);
			this.HeaderText.Text = "Select Master Account";
			// 
			// cmbHidden
			// 
			this.cmbHidden.Items.AddRange(new object[] {
            "Name",
            "Mailing Address",
            "Location",
            "Map / Lot",
            "RE Acct Number",
            "Serial #",
            "Remote Serial #",
            "Ref 1"});
			this.cmbHidden.Location = new System.Drawing.Point(385, 48);
			this.cmbHidden.Name = "cmbHidden";
			this.cmbHidden.Size = new System.Drawing.Size(183, 40);
			this.cmbHidden.TabIndex = 5;
			this.cmbHidden.Text = "Name";
			this.cmbHidden.TextChanged += new System.EventHandler(this.optSearchType_CheckedChanged);
			// 
			// lblHidden
			// 
			this.lblHidden.AutoSize = true;
			this.lblHidden.Location = new System.Drawing.Point(282, 62);
			this.lblHidden.Name = "lblHidden";
			this.lblHidden.Size = new System.Drawing.Size(79, 15);
			this.lblHidden.TabIndex = 12;
			this.lblHidden.Text = "SEARCH BY";
			// 
			// vsSearch
			// 
			this.vsSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsSearch.Cols = 5;
			this.vsSearch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.vsSearch.FixedCols = 0;
			this.vsSearch.Location = new System.Drawing.Point(24, 171);
			this.vsSearch.Name = "vsSearch";
			this.vsSearch.RowHeadersVisible = false;
			this.vsSearch.Rows = 1;
			this.vsSearch.ShowFocusCell = false;
			this.vsSearch.Size = new System.Drawing.Size(1071, 244);
			this.vsSearch.TabIndex = 1;
			this.vsSearch.Visible = false;
			this.vsSearch.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsSearch_MouseMoveEvent);
			this.vsSearch.Click += new System.EventHandler(this.vsSearch_Click);
			this.vsSearch.DoubleClick += new System.EventHandler(this.vsSearch_DblClick);
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuBatchProcessing,
            this.mnuFileBatchSearch});
			this.MainMenu1.Name = null;
			// 
			// mnuBatchProcessing
			// 
			this.mnuBatchProcessing.Index = 0;
			this.mnuBatchProcessing.Name = "mnuBatchProcessing";
			this.mnuBatchProcessing.Text = "Batch Processing";
			this.mnuBatchProcessing.Click += new System.EventHandler(this.mnuBatchProcessing_Click);
			// 
			// mnuFileBatchSearch
			// 
			this.mnuFileBatchSearch.Index = 1;
			this.mnuFileBatchSearch.Name = "mnuFileBatchSearch";
			this.mnuFileBatchSearch.Shortcut = Wisej.Web.Shortcut.F6;
			this.mnuFileBatchSearch.Text = "Account Search";
			// 
			// fraCriteria
			// 
			this.fraCriteria.AppearanceKey = "groupBoxNoBorders";
			this.fraCriteria.Controls.Add(this.lblNewAccountLabel);
			this.fraCriteria.Controls.Add(this.cmbSearchPlace);
			this.fraCriteria.Controls.Add(this.lblSearchPlace);
			this.fraCriteria.Controls.Add(this.txtGetAccountNumber);
			this.fraCriteria.Controls.Add(this.cmbHidden);
			this.fraCriteria.Controls.Add(this.lblHidden);
			this.fraCriteria.Controls.Add(this.txtSearch2);
			this.fraCriteria.Controls.Add(this.Label2);
			this.fraCriteria.Controls.Add(this.chkShowPreviousOwnerInfo);
			this.fraCriteria.Controls.Add(this.txtSearch);
			this.fraCriteria.Controls.Add(this.lblSearchInfo);
			this.fraCriteria.Controls.Add(this.lblSearch2);
			this.fraCriteria.Controls.Add(this.lblSearch);
			this.fraCriteria.Name = "fraCriteria";
			this.fraCriteria.Size = new System.Drawing.Size(1038, 149);
			this.fraCriteria.TabIndex = 6;
			// 
			// lblNewAccountLabel
			// 
			this.lblNewAccountLabel.AutoSize = true;
			this.lblNewAccountLabel.Location = new System.Drawing.Point(14, 14);
			this.lblNewAccountLabel.Name = "lblNewAccountLabel";
			this.lblNewAccountLabel.Size = new System.Drawing.Size(396, 15);
			this.lblNewAccountLabel.TabIndex = 14;
			this.lblNewAccountLabel.Text = "ENTER ACCOUNT NUMBER.  ENTER 0 TO ADD NEW ACCOUNT";
			this.lblNewAccountLabel.Visible = false;
			// 
			// cmbSearchPlace
			// 
			this.cmbSearchPlace.Items.AddRange(new object[] {
            "Starts With",
            "Contains"});
			this.cmbSearchPlace.Location = new System.Drawing.Point(810, 48);
			this.cmbSearchPlace.Name = "cmbSearchPlace";
			this.cmbSearchPlace.Size = new System.Drawing.Size(183, 40);
			this.cmbSearchPlace.TabIndex = 11;
			this.cmbSearchPlace.Text = "Contains";
			// 
			// lblSearchPlace
			// 
			this.lblSearchPlace.AutoSize = true;
			this.lblSearchPlace.Location = new System.Drawing.Point(462, 150);
			this.lblSearchPlace.Name = "lblSearchPlace";
			this.lblSearchPlace.Size = new System.Drawing.Size(104, 15);
			this.lblSearchPlace.TabIndex = 2;
			this.lblSearchPlace.Text = "SEARCH PLACE";
			this.lblSearchPlace.Visible = false;
			// 
			// txtGetAccountNumber
			// 
			this.txtGetAccountNumber.BackColor = System.Drawing.SystemColors.Window;
			this.txtGetAccountNumber.Cursor = Wisej.Web.Cursors.Default;
			this.txtGetAccountNumber.Location = new System.Drawing.Point(122, 48);
			this.txtGetAccountNumber.MaxLength = 9;
			this.txtGetAccountNumber.Name = "txtGetAccountNumber";
			this.txtGetAccountNumber.Size = new System.Drawing.Size(126, 40);
			this.txtGetAccountNumber.TabIndex = 3;
			this.txtGetAccountNumber.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtGetAccountNumber_KeyPress);
			// 
			// txtSearch2
			// 
			this.txtSearch2.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch2.Cursor = Wisej.Web.Cursors.Default;
			this.txtSearch2.Location = new System.Drawing.Point(629, 47);
			this.txtSearch2.MaxLength = 9;
			this.txtSearch2.Name = "txtSearch2";
			this.txtSearch2.Size = new System.Drawing.Size(90, 40);
			this.txtSearch2.TabIndex = 7;
			this.txtSearch2.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.txtSearch2.Visible = false;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 62);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(66, 19);
			this.Label2.TabIndex = 13;
			this.Label2.Text = "ACCOUNT";
			// 
			// chkShowPreviousOwnerInfo
			// 
			this.chkShowPreviousOwnerInfo.Checked = true;
			this.chkShowPreviousOwnerInfo.CheckState = Wisej.Web.CheckState.Checked;
			this.chkShowPreviousOwnerInfo.Location = new System.Drawing.Point(30, 111);
			this.chkShowPreviousOwnerInfo.Name = "chkShowPreviousOwnerInfo";
			this.chkShowPreviousOwnerInfo.Size = new System.Drawing.Size(167, 22);
			this.chkShowPreviousOwnerInfo.TabIndex = 11;
			this.chkShowPreviousOwnerInfo.Text = "Show Previous Owners";
			// 
			// txtSearch
			// 
			this.txtSearch.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearch.Cursor = Wisej.Web.Cursors.Default;
			this.txtSearch.Location = new System.Drawing.Point(598, 48);
			this.txtSearch.Name = "txtSearch";
			this.txtSearch.Size = new System.Drawing.Size(193, 40);
			this.txtSearch.TabIndex = 8;
			this.txtSearch.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearch_KeyDown);
			this.txtSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSearch_KeyPress);
			// 
			// lblSearchInfo
			// 
			this.lblSearchInfo.AutoSize = true;
			this.lblSearchInfo.Location = new System.Drawing.Point(685, 59);
			this.lblSearchInfo.Name = "lblSearchInfo";
			this.lblSearchInfo.Size = new System.Drawing.Size(242, 15);
			this.lblSearchInfo.TabIndex = 4;
			this.lblSearchInfo.Text = "ENTER THE CRITERIA TO SEARCH BY";
			this.lblSearchInfo.Visible = false;
			// 
			// lblSearch2
			// 
			this.lblSearch2.AutoSize = true;
			this.lblSearch2.Location = new System.Drawing.Point(602, 62);
			this.lblSearch2.Name = "lblSearch2";
			this.lblSearch2.Size = new System.Drawing.Size(11, 15);
			this.lblSearch2.TabIndex = 5;
			this.lblSearch2.Text = "#";
			this.lblSearch2.Visible = false;
			// 
			// lblSearch
			// 
			this.lblSearch.AutoSize = true;
			this.lblSearch.Location = new System.Drawing.Point(706, 60);
			this.lblSearch.Name = "lblSearch";
			this.lblSearch.Size = new System.Drawing.Size(96, 15);
			this.lblSearch.TabIndex = 6;
			this.lblSearch.Text = "STREET NAME";
			this.lblSearch.Visible = false;
			// 
			// cmdSelectAccount
			// 
			this.cmdSelectAccount.AppearanceKey = "acceptButton";
			this.cmdSelectAccount.Cursor = Wisej.Web.Cursors.Default;
			this.cmdSelectAccount.ForeColor = System.Drawing.Color.White;
			this.cmdSelectAccount.Location = new System.Drawing.Point(271, 30);
			this.cmdSelectAccount.Name = "cmdSelectAccount";
			this.cmdSelectAccount.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSelectAccount.Size = new System.Drawing.Size(170, 48);
			this.cmdSelectAccount.TabIndex = 5;
			this.cmdSelectAccount.Text = "Select Account";
			this.cmdSelectAccount.Click += new System.EventHandler(this.mnuSelectAccount_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSelectAccount,
            this.mnuFileSearch,
            this.mnuFileClearSearch,
            this.mnuUndeleteMaster,
            this.Seperator,
            this.mnuQuit});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuSelectAccount
			// 
			this.mnuSelectAccount.Index = 0;
			this.mnuSelectAccount.Name = "mnuSelectAccount";
			this.mnuSelectAccount.Text = "Select Account";
			this.mnuSelectAccount.Click += new System.EventHandler(this.mnuSelectAccount_Click);
			// 
			// mnuFileSearch
			// 
			this.mnuFileSearch.Index = 1;
			this.mnuFileSearch.Name = "mnuFileSearch";
			this.mnuFileSearch.Text = "Search";
			this.mnuFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// mnuFileClearSearch
			// 
			this.mnuFileClearSearch.Index = 2;
			this.mnuFileClearSearch.Name = "mnuFileClearSearch";
			this.mnuFileClearSearch.Text = "Clear Search";
			this.mnuFileClearSearch.Click += new System.EventHandler(this.mnuFileClearSearch_Click);
			// 
			// mnuUndeleteMaster
			// 
			this.mnuUndeleteMaster.Index = 3;
			this.mnuUndeleteMaster.Name = "mnuUndeleteMaster";
			this.mnuUndeleteMaster.Text = "Undelete Master Account";
			this.mnuUndeleteMaster.Click += new System.EventHandler(this.mnuUndeleteMaster_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuQuit
			// 
			this.mnuQuit.Index = 5;
			this.mnuQuit.Name = "mnuQuit";
			this.mnuQuit.Text = "Exit";
			this.mnuQuit.Click += new System.EventHandler(this.mnuQuit_Click);
			// 
			// cmdUndeleteMaster
			// 
			this.cmdUndeleteMaster.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdUndeleteMaster.Cursor = Wisej.Web.Cursors.Default;
			this.cmdUndeleteMaster.Location = new System.Drawing.Point(885, 29);
			this.cmdUndeleteMaster.Name = "cmdUndeleteMaster";
			this.cmdUndeleteMaster.Size = new System.Drawing.Size(177, 24);
			this.cmdUndeleteMaster.TabIndex = 1;
			this.cmdUndeleteMaster.Text = "Undelete Master Account";
			this.cmdUndeleteMaster.Click += new System.EventHandler(this.mnuUndeleteMaster_Click);
			// 
			// cmdFileClearSearch
			// 
			this.cmdFileClearSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClearSearch.Cursor = Wisej.Web.Cursors.Default;
			this.cmdFileClearSearch.Location = new System.Drawing.Point(778, 29);
			this.cmdFileClearSearch.Name = "cmdFileClearSearch";
			this.cmdFileClearSearch.Size = new System.Drawing.Size(101, 24);
			this.cmdFileClearSearch.TabIndex = 2;
			this.cmdFileClearSearch.Text = "Clear Search";
			this.cmdFileClearSearch.Click += new System.EventHandler(this.mnuFileClearSearch_Click);
			// 
			// cmdFileSearch
			// 
			this.cmdFileSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSearch.Cursor = Wisej.Web.Cursors.Default;
			this.cmdFileSearch.ImageSource = "button-search";
			this.cmdFileSearch.Location = new System.Drawing.Point(691, 29);
			this.cmdFileSearch.Name = "cmdFileSearch";
			this.cmdFileSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdFileSearch.TabIndex = 3;
			this.cmdFileSearch.Text = "Search";
			this.cmdFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// frmCustomerSearch
			// 
			this.ClientSize = new System.Drawing.Size(1078, 695);
			this.Cursor = Wisej.Web.Cursors.Default;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmCustomerSearch";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Select Master Account";
			this.Activated += new System.EventHandler(this.frmCustomerSearch_Activated);
			this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmCustomerSearch_FormClosing);
			this.Resize += new System.EventHandler(this.frmGetMasterAccount_Resize);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmGetMasterAccount_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraCriteria)).EndInit();
			this.fraCriteria.ResumeLayout(false);
			this.fraCriteria.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowPreviousOwnerInfo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSelectAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdUndeleteMaster)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClearSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSearch)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdUndeleteMaster;
		private FCButton cmdFileClearSearch;
		private FCButton cmdFileSearch;
		public FCComboBox cmbSearchPlace;
		public FCLabel lblSearchPlace;
        public FCLabel lblNewAccountLabel;
    }
}
