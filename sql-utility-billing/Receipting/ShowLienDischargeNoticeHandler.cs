﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;
using SharedApplication.UtilityBilling.Receipting.Interfaces;

namespace TWUT0000.Receipting
{
	public class ShowLienDischargeNoticeHandler : CommandHandler<ShowLienDischargeNotice>
	{
		private IModalView<IUtilityBillingLienDischargeNoticeViewModel> dischargeView;
		public ShowLienDischargeNoticeHandler(IModalView<IUtilityBillingLienDischargeNoticeViewModel> dischargeView)
		{
			this.dischargeView = dischargeView;
		}
		protected override void Handle(ShowLienDischargeNotice command)
		{
			dischargeView.ViewModel.PayDate = command.PayDate;
			dischargeView.ViewModel.LienToDischarge = command.LienPaid;
			dischargeView.ViewModel.AbateDefault = command.LienPaid.Payments.OrderByDescending(x => x.RecordedTransactionDate).FirstOrDefault().Code == "A";
			dischargeView.ShowModal();
		}
	}
}
