﻿namespace TWUT0000.Receipting
{
	partial class frmUBNewLienDischargeNotice
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Wisej Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fraVariables = new fecherFoundation.FCFrame();
			this.vsData = new fecherFoundation.FCGrid();
			this.lblName = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.btnProcess = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraVariables)).BeginInit();
			this.fraVariables.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsData)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.btnProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 456);
			this.BottomPanel.Size = new System.Drawing.Size(578, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.ClientArea.Controls.Add(this.fraVariables);
			this.ClientArea.Dock = Wisej.Web.DockStyle.None;
			this.ClientArea.Size = new System.Drawing.Size(598, 566);
			this.ClientArea.Controls.SetChildIndex(this.fraVariables, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(598, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(238, 28);
			this.HeaderText.Text = "Lien Discharge Notice";
			// 
			// fraVariables
			// 
			this.fraVariables.AppearanceKey = "groupBoxNoBorders";
			this.fraVariables.Controls.Add(this.vsData);
			this.fraVariables.Controls.Add(this.lblName);
			this.fraVariables.Controls.Add(this.lblYear);
			this.fraVariables.Location = new System.Drawing.Point(21, 13);
			this.fraVariables.Name = "fraVariables";
			this.fraVariables.Size = new System.Drawing.Size(537, 443);
			this.fraVariables.TabIndex = 1001;
			this.fraVariables.Text = "Confirm Data";
			// 
			// vsData
			// 
			this.vsData.Cols = 3;
			this.vsData.ColumnHeadersVisible = false;
			this.vsData.FixedRows = 0;
			this.vsData.Location = new System.Drawing.Point(0, 65);
			this.vsData.Name = "vsData";
			this.vsData.Rows = 1;
			this.vsData.Size = new System.Drawing.Size(532, 372);
			this.vsData.TabIndex = 2;
			this.vsData.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsData_KeyPressEdit);
			this.vsData.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsData_CellBeginEdit);
			this.vsData.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsData_CellValidating);
			this.vsData.CurrentCellChanged += new System.EventHandler(this.vsData_CurrentCellChanged);
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(156, 35);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(351, 20);
			this.lblName.TabIndex = 1;
			this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(20, 35);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(130, 20);
			this.lblYear.TabIndex = 0;
			this.lblYear.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// btnProcess
			// 
			this.btnProcess.AppearanceKey = "acceptButton";
			this.btnProcess.Location = new System.Drawing.Point(211, 30);
			this.btnProcess.Name = "btnProcess";
			this.btnProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnProcess.Size = new System.Drawing.Size(156, 48);
			this.btnProcess.TabIndex = 3;
			this.btnProcess.Text = "Save & Continue";
			this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
			// 
			// frmUBNewLienDischargeNotice
			// 
			this.ClientSize = new System.Drawing.Size(598, 625);
			this.Name = "frmUBNewLienDischargeNotice";
			this.Load += new System.EventHandler(this.frmUBNewLienDischargeNotice_Load);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraVariables)).EndInit();
			this.fraVariables.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsData)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnProcess)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		public fecherFoundation.FCFrame fraVariables;
		public fecherFoundation.FCGrid vsData;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel lblYear;
		private fecherFoundation.FCButton btnProcess;
	}
}
