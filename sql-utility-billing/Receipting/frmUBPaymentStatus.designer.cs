//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;

#if TWCR0000
using TWCR0000;


#elif TWBD0000
using TWBD0000;
#endif
namespace Global
{
	/// <summary>
	/// Summary description for frmUBPaymentStatus.
	/// </summary>
	partial class frmUBPaymentStatus : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCButton> cmdDisc;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label1;
		public fecherFoundation.FCFrame fraRateInfo;
		public FCGrid vsRateInfo;
		public fecherFoundation.FCButton cmdRIClose;
		public fecherFoundation.FCPanel fraPayment;
		public fecherFoundation.FCMenuStrip MainMenu1;
		public FCGrid vsPayments;
		public fecherFoundation.FCTextBox txtTax;
		public fecherFoundation.FCTextBox txtTotalPendingDue;
		public fecherFoundation.FCTextBox txtCash;
		public fecherFoundation.FCTextBox txtCD;
		public fecherFoundation.FCComboBox cmbBillNumber;
		public fecherFoundation.FCTextBox txtPrincipal;
		public fecherFoundation.FCTextBox txtCosts;
		public fecherFoundation.FCTextBox txtReference;
		public fecherFoundation.FCTextBox txtTransactionDate2;
		public fecherFoundation.FCTextBox txtInterest;
		public fecherFoundation.FCTextBox txtComments;
		public fecherFoundation.FCTextBox txtBLID;
		public fecherFoundation.FCComboBox cmbCode;
		public fecherFoundation.FCComboBox cmbService;
		public Global.T2KDateBox txtTransactionDate;
		public FCGrid vsPeriod;
		public FCGrid txtAcctNumber;
		public fecherFoundation.FCLabel Label1_8;
		public fecherFoundation.FCLabel Label1_4;
		public fecherFoundation.FCLabel lblTax;
		public fecherFoundation.FCLabel lblTaxClub;
		public fecherFoundation.FCLabel Label1_9;
		public fecherFoundation.FCLabel lblTotalPendingDue;
		public fecherFoundation.FCLabel Label1_11;
		public fecherFoundation.FCLabel Label1_1;
		public fecherFoundation.FCLabel Label1_2;
		public fecherFoundation.FCLabel lblPrincipal;
		public fecherFoundation.FCLabel lblInterest;
		public fecherFoundation.FCLabel lblCosts;
		public fecherFoundation.FCLabel Label1_10;
		public fecherFoundation.FCLabel Label1_3;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label1_12;
		public fecherFoundation.FCPanel fraStatusLabels;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblOwnersName;
		public fecherFoundation.FCLabel lblMapLot;
		public fecherFoundation.FCLabel lblLocation;
		public FCCommonDialog CommonDialog1;
		public FCGrid WGRID;
		public FCGrid SGRID;
		public fecherFoundation.FCLabel lblGrpInfo;
		public fecherFoundation.FCLabel lblAcctComment;
		public fecherFoundation.FCLabel lblTaxAcquired;
		public fecherFoundation.FCLabel lblSewerHeading;
		public fecherFoundation.FCLabel lblWaterHeading;
		public fecherFoundation.FCPictureBox imgNote;
		public fecherFoundation.FCLabel lblPaymentInfo;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileShowMeterInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileAccountInfo;
		public fecherFoundation.FCToolStripMenuItem mnuFileSwitch;
		public fecherFoundation.FCToolStripMenuItem mnuFileShowSplitView;
		public fecherFoundation.FCToolStripMenuItem mnuFileComment;
		public fecherFoundation.FCToolStripMenuItem mnuFileEditNote;
        public fecherFoundation.FCToolStripMenuItem mnuPayment;
		public fecherFoundation.FCToolStripMenuItem mnuFileOptionsLoadValidAccount;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentClearPayment;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentClearList;
		public fecherFoundation.FCToolStripMenuItem mnuFileDischarge;
		public fecherFoundation.FCToolStripMenuItem mnuFileConsumptionReport;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintRateInfo;
		public fecherFoundation.FCToolStripMenuItem mnuPaymentSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuSpacer;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUBPaymentStatus));
            this.fraRateInfo = new fecherFoundation.FCFrame();
            this.vsRateInfo = new fecherFoundation.FCGrid();
            this.cmdRIClose = new fecherFoundation.FCButton();
            this.fraPayment = new fecherFoundation.FCPanel();
            this.vsPayments = new fecherFoundation.FCGrid();
            this.txtTax = new fecherFoundation.FCTextBox();
            this.txtTotalPendingDue = new fecherFoundation.FCTextBox();
            this.txtCash = new fecherFoundation.FCTextBox();
            this.txtCD = new fecherFoundation.FCTextBox();
            this.cmbBillNumber = new fecherFoundation.FCComboBox();
            this.txtPrincipal = new fecherFoundation.FCTextBox();
            this.txtCosts = new fecherFoundation.FCTextBox();
            this.txtReference = new fecherFoundation.FCTextBox();
            this.txtTransactionDate2 = new fecherFoundation.FCTextBox();
            this.txtInterest = new fecherFoundation.FCTextBox();
            this.txtComments = new fecherFoundation.FCTextBox();
            this.txtBLID = new fecherFoundation.FCTextBox();
            this.cmbCode = new fecherFoundation.FCComboBox();
            this.cmbService = new fecherFoundation.FCComboBox();
            this.txtTransactionDate = new Global.T2KDateBox();
            this.txtAcctNumber = new fecherFoundation.FCGrid();
            this.Label1_8 = new fecherFoundation.FCLabel();
            this.Label1_4 = new fecherFoundation.FCLabel();
            this.lblTax = new fecherFoundation.FCLabel();
            this.lblTaxClub = new fecherFoundation.FCLabel();
            this.Label1_9 = new fecherFoundation.FCLabel();
            this.lblTotalPendingDue = new fecherFoundation.FCLabel();
            this.Label1_11 = new fecherFoundation.FCLabel();
            this.Label1_1 = new fecherFoundation.FCLabel();
            this.Label1_2 = new fecherFoundation.FCLabel();
            this.lblPrincipal = new fecherFoundation.FCLabel();
            this.lblInterest = new fecherFoundation.FCLabel();
            this.lblCosts = new fecherFoundation.FCLabel();
            this.Label1_10 = new fecherFoundation.FCLabel();
            this.Label1_3 = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label1_12 = new fecherFoundation.FCLabel();
            this.vsPeriod = new fecherFoundation.FCGrid();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuFileAccountInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileShowMeterInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSwitch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileShowSplitView = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileComment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileEditNote = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPayment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileOptionsLoadValidAccount = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPaymentClearList = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileDischarge = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileConsumptionReport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintRateInfo = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPaymentClearPayment = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPaymentSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSpacer = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
            this.fraStatusLabels = new fecherFoundation.FCPanel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1_0 = new fecherFoundation.FCLabel();
            this.lblOwnersName = new fecherFoundation.FCLabel();
            this.lblMapLot = new fecherFoundation.FCLabel();
            this.lblAccount = new fecherFoundation.FCLabel();
            this.lblLocation = new fecherFoundation.FCLabel();
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.WGRID = new fecherFoundation.FCGrid();
            this.SGRID = new fecherFoundation.FCGrid();
            this.lblGrpInfo = new fecherFoundation.FCLabel();
            this.lblAcctComment = new fecherFoundation.FCLabel();
            this.lblTaxAcquired = new fecherFoundation.FCLabel();
            this.lblSewerHeading = new fecherFoundation.FCLabel();
            this.lblWaterHeading = new fecherFoundation.FCLabel();
            this.imgNote = new fecherFoundation.FCPictureBox();
            this.lblPaymentInfo = new fecherFoundation.FCLabel();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdProcessEffective = new fecherFoundation.FCButton();
            this.cmdFilePrint = new fecherFoundation.FCButton();
            this.cmdProcessGoTo = new fecherFoundation.FCButton();
            this.cmdProcessChangeAccount = new fecherFoundation.FCButton();
            this.cmdFileMortgageHolderInfo = new fecherFoundation.FCButton();
            this.cmdSavePayments = new fecherFoundation.FCButton();
            this.cmdPaymentSaveExit = new fecherFoundation.FCButton();
            this.lblACHInfo = new fecherFoundation.FCLabel();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.panelSewerTotals = new Wisej.Web.Panel();
            this.txtSewerShortAccountTotalsAsOf = new fecherFoundation.FCLabel();
            this.txtSewerTotalPTC = new fecherFoundation.FCLabel();
            this.txtSewerTotalTax = new fecherFoundation.FCLabel();
            this.txtSewerAccountTotalsAsOf = new fecherFoundation.FCLabel();
            this.txtSewerTotalPrincipal = new fecherFoundation.FCLabel();
            this.txtSewerTotalTotal = new fecherFoundation.FCLabel();
            this.txtSewerTotalInterest = new fecherFoundation.FCLabel();
            this.txtSewerTotalCosts = new fecherFoundation.FCLabel();
            this.sewerGridPanel = new Wisej.Web.TableLayoutPanel();
            this.waterGridPanel = new Wisej.Web.TableLayoutPanel();
            this.panel2 = new Wisej.Web.Panel();
            this.txtWaterTotalPrincipal = new fecherFoundation.FCLabel();
            this.txtWaterShortAccountTotalsAsOf = new fecherFoundation.FCLabel();
            this.txtWaterTotalPTC = new fecherFoundation.FCLabel();
            this.txtWaterTotalTax = new fecherFoundation.FCLabel();
            this.txtWaterAccountTotalsAsOf = new fecherFoundation.FCLabel();
            this.txtWaterTotalTotal = new fecherFoundation.FCLabel();
            this.txtWaterTotalInterest = new fecherFoundation.FCLabel();
            this.txtWaterTotalCosts = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).BeginInit();
            this.fraRateInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayment)).BeginInit();
            this.fraPayment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraStatusLabels)).BeginInit();
            this.fraStatusLabels.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WGRID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SGRID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessEffective)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessGoTo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessChangeAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileMortgageHolderInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSavePayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPaymentSaveExit)).BeginInit();
            this.panelSewerTotals.SuspendLayout();
            this.sewerGridPanel.SuspendLayout();
            this.waterGridPanel.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPaymentSaveExit);
            this.BottomPanel.Location = new System.Drawing.Point(0, 1150);
            this.BottomPanel.Size = new System.Drawing.Size(1058, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.waterGridPanel);
            this.ClientArea.Controls.Add(this.sewerGridPanel);
            this.ClientArea.Controls.Add(this.fraPayment);
            this.ClientArea.Controls.Add(this.fraStatusLabels);
            this.ClientArea.Controls.Add(this.lblSewerHeading);
            this.ClientArea.Controls.Add(this.lblWaterHeading);
            this.ClientArea.Controls.Add(this.lblPaymentInfo);
            this.ClientArea.Controls.Add(this.fraRateInfo);
            this.ClientArea.Size = new System.Drawing.Size(1078, 751);
            this.ClientArea.Controls.SetChildIndex(this.fraRateInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPaymentInfo, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblWaterHeading, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSewerHeading, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraStatusLabels, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraPayment, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.sewerGridPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.waterGridPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSavePayments);
            this.TopPanel.Controls.Add(this.cmdProcessEffective);
            this.TopPanel.Controls.Add(this.lblACHInfo);
            this.TopPanel.Controls.Add(this.cmdProcessGoTo);
            this.TopPanel.Controls.Add(this.cmdFilePrint);
            this.TopPanel.Controls.Add(this.cmdProcessChangeAccount);
            this.TopPanel.Controls.Add(this.lblGrpInfo);
            this.TopPanel.Controls.Add(this.cmdFileMortgageHolderInfo);
            this.TopPanel.Controls.Add(this.lblTaxAcquired);
            this.TopPanel.Controls.Add(this.imgNote);
            this.TopPanel.Controls.Add(this.lblAcctComment);
            this.TopPanel.Size = new System.Drawing.Size(1078, 60);
            this.TopPanel.Controls.SetChildIndex(this.lblAcctComment, 0);
            this.TopPanel.Controls.SetChildIndex(this.imgNote, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblTaxAcquired, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileMortgageHolderInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblGrpInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessChangeAccount, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessGoTo, 0);
            this.TopPanel.Controls.SetChildIndex(this.lblACHInfo, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessEffective, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSavePayments, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = false;
            this.HeaderText.Size = new System.Drawing.Size(208, 28);
            this.HeaderText.Text = "Account";
            // 
            // fraRateInfo
            // 
            this.fraRateInfo.BackColor = System.Drawing.SystemColors.Window;
            this.fraRateInfo.Controls.Add(this.vsRateInfo);
            this.fraRateInfo.Controls.Add(this.cmdRIClose);
            this.fraRateInfo.Location = new System.Drawing.Point(748, 490);
            this.fraRateInfo.Name = "fraRateInfo";
            this.fraRateInfo.Size = new System.Drawing.Size(1048, 660);
            this.fraRateInfo.TabIndex = 48;
            this.fraRateInfo.Text = "Year Information";
            this.fraRateInfo.Visible = false;
            // 
            // vsRateInfo
            // 
            this.vsRateInfo.Cols = 5;
            this.vsRateInfo.ColumnHeadersVisible = false;
            this.vsRateInfo.FixedCols = 0;
            this.vsRateInfo.FixedRows = 0;
            this.vsRateInfo.Location = new System.Drawing.Point(20, 30);
            this.vsRateInfo.Name = "vsRateInfo";
            this.vsRateInfo.RowHeadersVisible = false;
            this.vsRateInfo.Rows = 20;
            this.vsRateInfo.ShowFocusCell = false;
            this.vsRateInfo.Size = new System.Drawing.Size(1008, 550);
            this.vsRateInfo.TabIndex = 62;
            this.vsRateInfo.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsRateInfo_MouseMoveEvent);
            // 
            // cmdRIClose
            // 
            this.cmdRIClose.AppearanceKey = "actionButton";
            this.cmdRIClose.Location = new System.Drawing.Point(459, 600);
            this.cmdRIClose.Name = "cmdRIClose";
            this.cmdRIClose.Size = new System.Drawing.Size(83, 40);
            this.cmdRIClose.TabIndex = 49;
            this.cmdRIClose.Text = "Close";
            this.cmdRIClose.Click += new System.EventHandler(this.cmdRIClose_Click);
            // 
            // fraPayment
            // 
            this.fraPayment.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraPayment.AppearanceKey = "groupBoxNoBorders";
            this.fraPayment.Controls.Add(this.vsPayments);
            this.fraPayment.Controls.Add(this.txtTax);
            this.fraPayment.Controls.Add(this.txtTotalPendingDue);
            this.fraPayment.Controls.Add(this.txtCash);
            this.fraPayment.Controls.Add(this.txtCD);
            this.fraPayment.Controls.Add(this.cmbBillNumber);
            this.fraPayment.Controls.Add(this.txtPrincipal);
            this.fraPayment.Controls.Add(this.txtCosts);
            this.fraPayment.Controls.Add(this.txtReference);
            this.fraPayment.Controls.Add(this.txtTransactionDate2);
            this.fraPayment.Controls.Add(this.txtInterest);
            this.fraPayment.Controls.Add(this.txtComments);
            this.fraPayment.Controls.Add(this.txtBLID);
            this.fraPayment.Controls.Add(this.cmbCode);
            this.fraPayment.Controls.Add(this.cmbService);
            this.fraPayment.Controls.Add(this.txtTransactionDate);
            this.fraPayment.Controls.Add(this.txtAcctNumber);
            this.fraPayment.Controls.Add(this.Label1_8);
            this.fraPayment.Controls.Add(this.Label1_4);
            this.fraPayment.Controls.Add(this.lblTax);
            this.fraPayment.Controls.Add(this.lblTaxClub);
            this.fraPayment.Controls.Add(this.Label1_9);
            this.fraPayment.Controls.Add(this.lblTotalPendingDue);
            this.fraPayment.Controls.Add(this.Label1_11);
            this.fraPayment.Controls.Add(this.Label1_1);
            this.fraPayment.Controls.Add(this.Label1_2);
            this.fraPayment.Controls.Add(this.lblPrincipal);
            this.fraPayment.Controls.Add(this.lblInterest);
            this.fraPayment.Controls.Add(this.lblCosts);
            this.fraPayment.Controls.Add(this.Label1_10);
            this.fraPayment.Controls.Add(this.Label1_3);
            this.fraPayment.Controls.Add(this.Label8);
            this.fraPayment.Controls.Add(this.Label1_12);
            this.fraPayment.Controls.Add(this.vsPeriod);
            this.fraPayment.Location = new System.Drawing.Point(10, 520);
            this.fraPayment.Name = "fraPayment";
            this.fraPayment.Size = new System.Drawing.Size(1050, 348);
            this.fraPayment.TabIndex = 22;
            this.fraPayment.Visible = false;
            // 
            // vsPayments
            // 
            this.vsPayments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsPayments.Cols = 11;
            this.vsPayments.FixedCols = 0;
            this.vsPayments.Location = new System.Drawing.Point(20, 214);
            this.vsPayments.Name = "vsPayments";
            this.vsPayments.RowHeadersVisible = false;
            this.vsPayments.Rows = 1;
            this.vsPayments.ShowFocusCell = false;
            this.vsPayments.Size = new System.Drawing.Size(1010, 114);
            this.vsPayments.TabIndex = 63;
            this.vsPayments.KeyDown += new Wisej.Web.KeyEventHandler(this.vsPayments_KeyDownEvent);
            // 
            // txtTax
            // 
            this.txtTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtTax.Location = new System.Drawing.Point(800, 115);
            this.txtTax.MaxLength = 12;
            this.txtTax.Name = "txtTax";
            this.txtTax.Size = new System.Drawing.Size(70, 40);
            this.txtTax.TabIndex = 55;
            this.txtTax.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtTax.Enter += new System.EventHandler(this.txtTax_Enter);
            this.txtTax.Validating += new System.ComponentModel.CancelEventHandler(this.txtTax_Validating);
            this.txtTax.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTax_KeyDown);
            this.txtTax.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTax_KeyPress);
            // 
            // txtTotalPendingDue
            // 
            this.txtTotalPendingDue.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalPendingDue.Location = new System.Drawing.Point(880, 30);
            this.txtTotalPendingDue.LockedOriginal = true;
            this.txtTotalPendingDue.Name = "txtTotalPendingDue";
            this.txtTotalPendingDue.ReadOnly = true;
            this.txtTotalPendingDue.Size = new System.Drawing.Size(150, 40);
            this.txtTotalPendingDue.TabIndex = 11;
            this.txtTotalPendingDue.Text = "0.00";
            this.txtTotalPendingDue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtTotalPendingDue.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTotalPendingDue_KeyPress);
            // 
            // txtCash
            // 
            this.txtCash.Enabled = false;
            this.txtCash.Location = new System.Drawing.Point(525, 115);
            this.txtCash.MaxLength = 1;
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(40, 40);
            this.txtCash.TabIndex = 4;
            this.txtCash.Text = "Y";
            this.txtCash.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtCash.Enter += new System.EventHandler(this.txtCash_Enter);
            this.txtCash.DoubleClick += new System.EventHandler(this.txtCash_DoubleClick);
            this.txtCash.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCash_KeyDown);
            this.txtCash.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCash_KeyPress);
            // 
            // txtCD
            // 
            this.txtCD.Location = new System.Drawing.Point(480, 115);
            this.txtCD.MaxLength = 1;
            this.txtCD.Name = "txtCD";
            this.txtCD.Size = new System.Drawing.Size(40, 40);
            this.txtCD.TabIndex = 3;
            this.txtCD.Text = "Y";
            this.txtCD.TextAlign = Wisej.Web.HorizontalAlignment.Center;
            this.txtCD.Enter += new System.EventHandler(this.txtCD_Enter);
            this.txtCD.DoubleClick += new System.EventHandler(this.txtCD_DoubleClick);
            this.txtCD.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCD_KeyDown);
            this.txtCD.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCD_KeyPress);
            // 
            // cmbBillNumber
            // 
            this.cmbBillNumber.BackColor = System.Drawing.SystemColors.Window;
            this.cmbBillNumber.Location = new System.Drawing.Point(125, 115);
            this.cmbBillNumber.Name = "cmbBillNumber";
            this.cmbBillNumber.Size = new System.Drawing.Size(80, 40);
            this.cmbBillNumber.TabIndex = 12;
            this.cmbBillNumber.SelectedIndexChanged += new System.EventHandler(this.cmbBillNumber_SelectedIndexChanged);
            this.cmbBillNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbBillNumber_KeyDown);
            // 
            // txtPrincipal
            // 
            this.txtPrincipal.BackColor = System.Drawing.SystemColors.Window;
            this.txtPrincipal.Location = new System.Drawing.Point(720, 115);
            this.txtPrincipal.MaxLength = 17;
            this.txtPrincipal.Name = "txtPrincipal";
            this.txtPrincipal.Size = new System.Drawing.Size(70, 40);
            this.txtPrincipal.TabIndex = 7;
            this.txtPrincipal.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtPrincipal.Enter += new System.EventHandler(this.txtPrincipal_Enter);
            this.txtPrincipal.Validating += new System.ComponentModel.CancelEventHandler(this.txtPrincipal_Validating);
            this.txtPrincipal.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPrincipal_KeyDown);
            this.txtPrincipal.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPrincipal_KeyPress);
            // 
            // txtCosts
            // 
            this.txtCosts.BackColor = System.Drawing.SystemColors.Window;
            this.txtCosts.Location = new System.Drawing.Point(960, 115);
            this.txtCosts.MaxLength = 11;
            this.txtCosts.Name = "txtCosts";
            this.txtCosts.Size = new System.Drawing.Size(70, 40);
            this.txtCosts.TabIndex = 9;
            this.txtCosts.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtCosts.Enter += new System.EventHandler(this.txtCosts_Enter);
            this.txtCosts.Validating += new System.ComponentModel.CancelEventHandler(this.txtCosts_Validating);
            this.txtCosts.KeyDown += new Wisej.Web.KeyEventHandler(this.txtCosts_KeyDown);
            this.txtCosts.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCosts_KeyPress);
            // 
            // txtReference
            // 
            this.txtReference.BackColor = System.Drawing.SystemColors.Window;
            this.txtReference.Location = new System.Drawing.Point(335, 115);
            this.txtReference.MaxLength = 6;
            this.txtReference.Name = "txtReference";
            this.txtReference.Size = new System.Drawing.Size(65, 40);
            this.txtReference.TabIndex = 64;
            this.txtReference.Enter += new System.EventHandler(this.txtReference_Enter);
            this.txtReference.Validating += new System.ComponentModel.CancelEventHandler(this.txtReference_Validating);
            this.txtReference.KeyDown += new Wisej.Web.KeyEventHandler(this.txtReference_KeyDown);
            // 
            // txtTransactionDate2
            // 
            this.txtTransactionDate2.BackColor = System.Drawing.SystemColors.Window;
            this.txtTransactionDate2.Location = new System.Drawing.Point(493, 30);
            this.txtTransactionDate2.MaxLength = 10;
            this.txtTransactionDate2.Name = "txtTransactionDate2";
            this.txtTransactionDate2.Size = new System.Drawing.Size(55, 40);
            this.txtTransactionDate2.TabIndex = 13;
            this.txtTransactionDate2.Visible = false;
            // 
            // txtInterest
            // 
            this.txtInterest.BackColor = System.Drawing.SystemColors.Window;
            this.txtInterest.Location = new System.Drawing.Point(876, 115);
            this.txtInterest.MaxLength = 12;
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Size = new System.Drawing.Size(78, 40);
            this.txtInterest.TabIndex = 8;
            this.txtInterest.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtInterest.Enter += new System.EventHandler(this.txtInterest_Enter);
            this.txtInterest.Validating += new System.ComponentModel.CancelEventHandler(this.txtInterest_Validating);
            this.txtInterest.KeyDown += new Wisej.Web.KeyEventHandler(this.txtInterest_KeyDown);
            this.txtInterest.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtInterest_KeyPress);
            // 
            // txtComments
            // 
            this.txtComments.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.txtComments.BackColor = System.Drawing.SystemColors.Window;
            this.txtComments.Location = new System.Drawing.Point(410, 165);
            this.txtComments.MaxLength = 255;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(620, 40);
            this.txtComments.TabIndex = 10;
            this.txtComments.KeyDown += new Wisej.Web.KeyEventHandler(this.txtComments_KeyDown);
            // 
            // txtBLID
            // 
            this.txtBLID.BackColor = System.Drawing.SystemColors.Window;
            this.txtBLID.Location = new System.Drawing.Point(20, 217);
            this.txtBLID.Name = "txtBLID";
            this.txtBLID.Size = new System.Drawing.Size(152, 40);
            this.txtBLID.TabIndex = 23;
            this.txtBLID.Visible = false;
            // 
            // cmbCode
            // 
            this.cmbCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCode.Location = new System.Drawing.Point(410, 115);
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.Size = new System.Drawing.Size(60, 40);
            this.cmbCode.Sorted = true;
            this.cmbCode.TabIndex = 2;
            this.cmbCode.SelectedIndexChanged += new System.EventHandler(this.cmbCode_SelectedIndexChanged);
            this.cmbCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDown);
            // 
            // cmbService
            // 
            this.cmbService.BackColor = System.Drawing.SystemColors.Window;
            this.cmbService.Location = new System.Drawing.Point(20, 115);
            this.cmbService.Name = "cmbService";
            this.cmbService.Size = new System.Drawing.Size(95, 40);
            this.cmbService.Sorted = true;
            this.cmbService.TabIndex = 1;
            this.cmbService.SelectedIndexChanged += new System.EventHandler(this.cmbService_SelectedIndexChanged);
            this.cmbService.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbService_KeyDown);
            // 
            // txtTransactionDate
            // 
            this.txtTransactionDate.Location = new System.Drawing.Point(215, 115);
            this.txtTransactionDate.Mask = "##/##/####";
            this.txtTransactionDate.MaxLength = 10;
            this.txtTransactionDate.Name = "txtTransactionDate";
            this.txtTransactionDate.Size = new System.Drawing.Size(113, 22);
            this.txtTransactionDate.TabIndex = 51;
            this.txtTransactionDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtTransactionDate_Validate);
            this.txtTransactionDate.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTransactionDate_KeyDownEvent);
            // 
            // txtAcctNumber
            // 
            this.txtAcctNumber.Cols = 1;
            this.txtAcctNumber.ColumnHeadersVisible = false;
            this.txtAcctNumber.FixedCols = 0;
            this.txtAcctNumber.FixedRows = 0;
            this.txtAcctNumber.Location = new System.Drawing.Point(572, 115);
            this.txtAcctNumber.Name = "txtAcctNumber";
            this.txtAcctNumber.RowHeadersVisible = false;
            this.txtAcctNumber.Rows = 1;
            this.txtAcctNumber.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtAcctNumber.ShowFocusCell = false;
            this.txtAcctNumber.Size = new System.Drawing.Size(140, 42);
            this.txtAcctNumber.TabIndex = 6;
            this.txtAcctNumber.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.txtAcctNumber_ValidateEdit);
            this.txtAcctNumber.Enter += new System.EventHandler(this.txtAcctNumber_Enter);
            this.txtAcctNumber.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAcctNumber_KeyDownEvent);
            // 
            // Label1_8
            // 
            this.Label1_8.Location = new System.Drawing.Point(531, 90);
            this.Label1_8.Name = "Label1_8";
            this.Label1_8.Size = new System.Drawing.Size(22, 15);
            this.Label1_8.TabIndex = 27;
            this.Label1_8.Text = "AC";
            this.Label1_8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label1_8, "Affect Cash");
            // 
            // Label1_4
            // 
            this.Label1_4.AutoSize = true;
            this.Label1_4.Location = new System.Drawing.Point(20, 90);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(31, 15);
            this.Label1_4.TabIndex = 66;
            this.Label1_4.Text = "SVC";
            this.Label1_4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTax
            // 
            this.lblTax.AutoSize = true;
            this.lblTax.Location = new System.Drawing.Point(815, 90);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(29, 15);
            this.lblTax.TabIndex = 56;
            this.lblTax.Text = "TAX";
            this.lblTax.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblTaxClub
            // 
            this.lblTaxClub.Location = new System.Drawing.Point(194, 179);
            this.lblTaxClub.Name = "lblTaxClub";
            this.lblTaxClub.TabIndex = 52;
            this.lblTaxClub.Text = "TC";
            this.ToolTip1.SetToolTip(this.lblTaxClub, "Tax Club Payment");
            this.lblTaxClub.Visible = false;
            // 
            // Label1_9
            // 
            this.Label1_9.AutoSize = true;
            this.Label1_9.Location = new System.Drawing.Point(487, 90);
            this.Label1_9.Name = "Label1_9";
            this.Label1_9.Size = new System.Drawing.Size(23, 15);
            this.Label1_9.TabIndex = 26;
            this.Label1_9.Text = "CD";
            this.Label1_9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label1_9, "Cash Drawer");
            // 
            // lblTotalPendingDue
            // 
            this.lblTotalPendingDue.Location = new System.Drawing.Point(738, 44);
            this.lblTotalPendingDue.Name = "lblTotalPendingDue";
            this.lblTotalPendingDue.Size = new System.Drawing.Size(121, 15);
            this.lblTotalPendingDue.TabIndex = 50;
            this.lblTotalPendingDue.Text = "TOTAL PENDING";
            // 
            // Label1_11
            // 
            this.Label1_11.Location = new System.Drawing.Point(585, 90);
            this.Label1_11.Name = "Label1_11";
            this.Label1_11.Size = new System.Drawing.Size(45, 15);
            this.Label1_11.TabIndex = 35;
            this.Label1_11.Text = "ACCT #";
            // 
            // Label1_1
            // 
            this.Label1_1.AutoSize = true;
            this.Label1_1.Location = new System.Drawing.Point(232, 90);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(39, 15);
            this.Label1_1.TabIndex = 34;
            this.Label1_1.Text = "DATE";
            // 
            // Label1_2
            // 
            this.Label1_2.AutoSize = true;
            this.Label1_2.Location = new System.Drawing.Point(430, 90);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(42, 15);
            this.Label1_2.TabIndex = 33;
            this.Label1_2.Text = "CODE";
            // 
            // lblPrincipal
            // 
            this.lblPrincipal.AutoSize = true;
            this.lblPrincipal.Location = new System.Drawing.Point(720, 90);
            this.lblPrincipal.Name = "lblPrincipal";
            this.lblPrincipal.Size = new System.Drawing.Size(73, 15);
            this.lblPrincipal.TabIndex = 32;
            this.lblPrincipal.Text = "PRINCIPAL";
            this.lblPrincipal.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblInterest
            // 
            this.lblInterest.AutoSize = true;
            this.lblInterest.Location = new System.Drawing.Point(876, 90);
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Size = new System.Drawing.Size(68, 15);
            this.lblInterest.TabIndex = 31;
            this.lblInterest.Text = "INTEREST";
            this.lblInterest.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblCosts
            // 
            this.lblCosts.AutoSize = true;
            this.lblCosts.Location = new System.Drawing.Point(966, 90);
            this.lblCosts.Name = "lblCosts";
            this.lblCosts.Size = new System.Drawing.Size(49, 15);
            this.lblCosts.TabIndex = 30;
            this.lblCosts.Text = "COSTS";
            this.lblCosts.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label1_10
            // 
            this.Label1_10.AutoSize = true;
            this.Label1_10.Location = new System.Drawing.Point(352, 90);
            this.Label1_10.Name = "Label1_10";
            this.Label1_10.Size = new System.Drawing.Size(30, 15);
            this.Label1_10.TabIndex = 29;
            this.Label1_10.Text = "REF";
            // 
            // Label1_3
            // 
            this.Label1_3.AutoSize = true;
            this.Label1_3.Location = new System.Drawing.Point(125, 90);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(31, 15);
            this.Label1_3.TabIndex = 28;
            this.Label1_3.Text = "BILL";
            this.Label1_3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label8
            // 
            this.Label8.AutoSize = true;
            this.Label8.Location = new System.Drawing.Point(20, 179);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(169, 15);
            this.Label8.TabIndex = 25;
            this.Label8.Text = "PENDING TRANSACTIONS";
            // 
            // Label1_12
            // 
            this.Label1_12.AutoSize = true;
            this.Label1_12.Location = new System.Drawing.Point(304, 179);
            this.Label1_12.Name = "Label1_12";
            this.Label1_12.Size = new System.Drawing.Size(71, 15);
            this.Label1_12.TabIndex = 24;
            this.Label1_12.Text = "COMMENT";
            // 
            // vsPeriod
            // 
            this.vsPeriod.Cols = 5;
            this.vsPeriod.ColumnHeadersVisible = false;
            this.vsPeriod.FixedCols = 0;
            this.vsPeriod.FixedRows = 0;
            this.vsPeriod.Location = new System.Drawing.Point(20, 30);
            this.vsPeriod.Name = "vsPeriod";
            this.vsPeriod.RowHeadersVisible = false;
            this.vsPeriod.Rows = 1;
            this.vsPeriod.ShowFocusCell = false;
            this.vsPeriod.Size = new System.Drawing.Size(453, 42);
            this.vsPeriod.TabIndex = 64;
            this.vsPeriod.DoubleClick += new System.EventHandler(this.vsPeriod_DblClick);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileAccountInfo,
            this.mnuFileShowMeterInfo,
            this.mnuFileSwitch,
            this.mnuFileShowSplitView,
            this.mnuFileComment,
            this.mnuFileEditNote,
            this.mnuPayment,
            this.mnuFileDischarge,
            this.mnuFileConsumptionReport,
            this.mnuFilePrintRateInfo});
            this.MainMenu1.Name = null;
            // 
            // mnuFileAccountInfo
            // 
            this.mnuFileAccountInfo.Index = 0;
            this.mnuFileAccountInfo.Name = "mnuFileAccountInfo";
            this.mnuFileAccountInfo.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuFileAccountInfo.Text = "Show Account Info";
            this.mnuFileAccountInfo.Click += new System.EventHandler(this.mnuFileAccountInfo_Click);
            // 
            // mnuFileShowMeterInfo
            // 
            this.mnuFileShowMeterInfo.Index = 1;
            this.mnuFileShowMeterInfo.Name = "mnuFileShowMeterInfo";
            this.mnuFileShowMeterInfo.Text = "Show Meter Detail";
            this.mnuFileShowMeterInfo.Click += new System.EventHandler(this.mnuFileShowMeterInfo_Click);
            // 
            // mnuFileSwitch
            // 
            this.mnuFileSwitch.Index = 2;
            this.mnuFileSwitch.Name = "mnuFileSwitch";
            this.mnuFileSwitch.Shortcut = Wisej.Web.Shortcut.F9;
            this.mnuFileSwitch.Text = "Show Water Bills";
            this.mnuFileSwitch.Click += new System.EventHandler(this.mnuFileSwitch_Click);
            // 
            // mnuFileShowSplitView
            // 
            this.mnuFileShowSplitView.Index = 3;
            this.mnuFileShowSplitView.Name = "mnuFileShowSplitView";
            this.mnuFileShowSplitView.Text = "Show Split View";
            this.mnuFileShowSplitView.Visible = false;
            this.mnuFileShowSplitView.Click += new System.EventHandler(this.mnuFileShowSplitView_Click);
            // 
            // mnuFileComment
            // 
            this.mnuFileComment.Index = 4;
            this.mnuFileComment.Name = "mnuFileComment";
            this.mnuFileComment.Text = "Account Comment";
            this.mnuFileComment.Click += new System.EventHandler(this.mnuFileComment_Click);
            // 
            // mnuFileEditNote
            // 
            this.mnuFileEditNote.Index = 5;
            this.mnuFileEditNote.Name = "mnuFileEditNote";
            this.mnuFileEditNote.Text = "Add/Edit Note";
            this.mnuFileEditNote.Click += new System.EventHandler(this.mnuFileEditNote_Click);
            // 
            // mnuPayment
            // 
            this.mnuPayment.Index = 6;
            this.mnuPayment.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileOptionsLoadValidAccount,
            this.mnuPaymentClearList});
            this.mnuPayment.Name = "mnuPayment";
            this.mnuPayment.Text = "Options";
            this.mnuPayment.Visible = false;
            // 
            // mnuFileOptionsLoadValidAccount
            // 
            this.mnuFileOptionsLoadValidAccount.Index = 0;
            this.mnuFileOptionsLoadValidAccount.Name = "mnuFileOptionsLoadValidAccount";
            this.mnuFileOptionsLoadValidAccount.Shortcut = Wisej.Web.Shortcut.ShiftF2;
            this.mnuFileOptionsLoadValidAccount.Text = "Load Valid Account";
            this.mnuFileOptionsLoadValidAccount.Click += new System.EventHandler(this.mnuFileOptionsLoadValidAccount_Click);
            // 
            // mnuPaymentClearList
            // 
            this.mnuPaymentClearList.Index = 1;
            this.mnuPaymentClearList.Name = "mnuPaymentClearList";
            this.mnuPaymentClearList.Text = "Clear Pending Transactions";
            this.mnuPaymentClearList.Click += new System.EventHandler(this.mnuPaymentClearList_Click);
            // 
            // mnuFileDischarge
            // 
            this.mnuFileDischarge.Index = 7;
            this.mnuFileDischarge.Name = "mnuFileDischarge";
            this.mnuFileDischarge.Text = "Print Lien Discharge Notice";
            this.mnuFileDischarge.Visible = false;
            this.mnuFileDischarge.Click += new System.EventHandler(this.mnuFileDischarge_Click);
            // 
            // mnuFileConsumptionReport
            // 
            this.mnuFileConsumptionReport.Index = 8;
            this.mnuFileConsumptionReport.Name = "mnuFileConsumptionReport";
            this.mnuFileConsumptionReport.Text = "Print Consumption Report";
            this.mnuFileConsumptionReport.Click += new System.EventHandler(this.mnuFileConsumptionReport_Click);
            // 
            // mnuFilePrintRateInfo
            // 
            this.mnuFilePrintRateInfo.Index = 9;
            this.mnuFilePrintRateInfo.Name = "mnuFilePrintRateInfo";
            this.mnuFilePrintRateInfo.Text = "Print Account/Payment Info";
            this.mnuFilePrintRateInfo.Visible = false;
            this.mnuFilePrintRateInfo.Click += new System.EventHandler(this.mnuFilePrintRateInfo_Click);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Enabled = false;
            this.mnuProcess.Index = -1;
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuPaymentClearPayment
            // 
            this.mnuPaymentClearPayment.Index = -1;
            this.mnuPaymentClearPayment.Name = "mnuPaymentClearPayment";
            this.mnuPaymentClearPayment.Text = "Clear Payment Boxes";
            this.mnuPaymentClearPayment.Click += new System.EventHandler(this.mnuPaymentClearPayment_Click);
            // 
            // mnuPaymentSeperator
            // 
            this.mnuPaymentSeperator.Index = -1;
            this.mnuPaymentSeperator.Name = "mnuPaymentSeperator";
            this.mnuPaymentSeperator.Text = "-";
            // 
            // mnuSpacer
            // 
            this.mnuSpacer.Index = -1;
            this.mnuSpacer.Name = "mnuSpacer";
            this.mnuSpacer.Text = "-";
            // 
            // mnuProcessExit
            // 
            this.mnuProcessExit.Index = -1;
            this.mnuProcessExit.Name = "mnuProcessExit";
            this.mnuProcessExit.Text = "Exit";
            this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
            // 
            // fraStatusLabels
            // 
            this.fraStatusLabels.AppearanceKey = "groupBoxNoBorders";
            this.fraStatusLabels.Controls.Add(this.Label4);
            this.fraStatusLabels.Controls.Add(this.Label3);
            this.fraStatusLabels.Controls.Add(this.Label1_0);
            this.fraStatusLabels.Controls.Add(this.lblOwnersName);
            this.fraStatusLabels.Controls.Add(this.lblMapLot);
            this.fraStatusLabels.Controls.Add(this.lblAccount);
            this.fraStatusLabels.Controls.Add(this.lblLocation);
            this.fraStatusLabels.Location = new System.Drawing.Point(30, 0);
            this.fraStatusLabels.Name = "fraStatusLabels";
            this.fraStatusLabels.Size = new System.Drawing.Size(555, 124);
            this.fraStatusLabels.TabIndex = 14;
            this.fraStatusLabels.Visible = false;
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 86);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(55, 18);
            this.Label4.TabIndex = 19;
            this.Label4.Text = "MAP LOT";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 58);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(62, 18);
            this.Label3.TabIndex = 18;
            this.Label3.Text = "LOCATION";
            // 
            // Label1_0
            // 
            this.Label1_0.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Label1_0.Location = new System.Drawing.Point(20, 30);
            this.Label1_0.Name = "Label1_0";
            this.Label1_0.Size = new System.Drawing.Size(60, 18);
            this.Label1_0.TabIndex = 17;
            this.Label1_0.Text = "NAME";
            // 
            // lblOwnersName
            // 
            this.lblOwnersName.BackColor = System.Drawing.Color.Transparent;
            this.lblOwnersName.Location = new System.Drawing.Point(92, 30);
            this.lblOwnersName.Name = "lblOwnersName";
            this.lblOwnersName.Size = new System.Drawing.Size(453, 15);
            this.lblOwnersName.TabIndex = 16;
            this.lblOwnersName.TruncateTextToWidth = true;
            this.lblOwnersName.UseMnemonic = true;
            this.lblOwnersName.Visible = false;
            // 
            // lblMapLot
            // 
            this.lblMapLot.BackColor = System.Drawing.Color.Transparent;
            this.lblMapLot.Location = new System.Drawing.Point(92, 86);
            this.lblMapLot.Name = "lblMapLot";
            this.lblMapLot.Size = new System.Drawing.Size(151, 18);
            this.lblMapLot.TabIndex = 21;
            this.lblMapLot.Visible = false;
            // 
            // lblAccount
            // 
            this.lblAccount.BackColor = System.Drawing.Color.Transparent;
            this.lblAccount.Location = new System.Drawing.Point(410, 40);
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Size = new System.Drawing.Size(53, 19);
            this.lblAccount.TabIndex = 15;
            this.lblAccount.Visible = false;
            // 
            // lblLocation
            // 
            this.lblLocation.BackColor = System.Drawing.Color.Transparent;
            this.lblLocation.Location = new System.Drawing.Point(92, 58);
            this.lblLocation.Name = "lblLocation";
            this.lblLocation.Size = new System.Drawing.Size(317, 18);
            this.lblLocation.TabIndex = 20;
            this.lblLocation.Visible = false;
            // 
            // CommonDialog1
            // 
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            // 
            // WGRID
            // 
            this.WGRID.Cols = 20;
            this.WGRID.Dock = Wisej.Web.DockStyle.Fill;
            this.WGRID.Location = new System.Drawing.Point(3, 3);
            this.WGRID.Name = "WGRID";
            this.WGRID.Rows = 1;
            this.WGRID.ShowFocusCell = false;
            this.WGRID.Size = new System.Drawing.Size(902, 224);
            this.WGRID.TabIndex = 60;
            this.WGRID.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.WGRID_MouseMoveEvent);
            this.WGRID.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.WGRID_MouseDownEvent);
            this.WGRID.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.WGRID_RowExpanded);
            this.WGRID.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.WGRID_RowCollapsed);
            this.WGRID.Enter += new System.EventHandler(this.WGRID_Enter);
            this.WGRID.Click += new System.EventHandler(this.WGRID_ClickEvent);
            this.WGRID.DoubleClick += new System.EventHandler(this.WGRID_DblClick);
            this.WGRID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.WGRID_KeyPressEvent);
            // 
            // SGRID
            // 
            this.SGRID.Cols = 20;
            this.SGRID.Dock = Wisej.Web.DockStyle.Fill;
            this.SGRID.Location = new System.Drawing.Point(3, 3);
            this.SGRID.Name = "SGRID";
            this.SGRID.Rows = 1;
            this.SGRID.ShowFocusCell = false;
            this.SGRID.Size = new System.Drawing.Size(902, 224);
            this.SGRID.TabIndex = 59;
            this.SGRID.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.SGRID_MouseMoveEvent);
            this.SGRID.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.SGRID_MouseDownEvent);
            this.SGRID.RowExpanded += new Wisej.Web.DataGridViewRowEventHandler(this.SGRID_RowExpanded);
            this.SGRID.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.SGRID_RowCollapsed);
            this.SGRID.Enter += new System.EventHandler(this.SGRID_Enter);
            this.SGRID.Click += new System.EventHandler(this.SGRID_ClickEvent);
            this.SGRID.DoubleClick += new System.EventHandler(this.SGRID_DblClick);
            this.SGRID.KeyPress += new Wisej.Web.KeyPressEventHandler(this.SGRID_KeyPressEvent);
            // 
            // lblGrpInfo
            // 
            this.lblGrpInfo.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblGrpInfo.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblGrpInfo.Location = new System.Drawing.Point(134, 0);
            this.lblGrpInfo.Name = "lblGrpInfo";
            this.lblGrpInfo.Size = new System.Drawing.Size(34, 20);
            this.lblGrpInfo.TabIndex = 74;
            this.lblGrpInfo.Text = "G";
            this.lblGrpInfo.Visible = false;
            this.lblGrpInfo.Click += new System.EventHandler(this.lblGrpInfo_Click);
            // 
            // lblAcctComment
            // 
            this.lblAcctComment.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblAcctComment.Location = new System.Drawing.Point(102, 0);
            this.lblAcctComment.Name = "lblAcctComment";
            this.lblAcctComment.Size = new System.Drawing.Size(26, 23);
            this.lblAcctComment.TabIndex = 65;
            this.lblAcctComment.Text = "C";
            this.lblAcctComment.Visible = false;
            this.lblAcctComment.Click += new System.EventHandler(this.lblAcctComment_Click);
            // 
            // lblTaxAcquired
            // 
            this.lblTaxAcquired.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblTaxAcquired.Location = new System.Drawing.Point(70, 0);
            this.lblTaxAcquired.Name = "lblTaxAcquired";
            this.lblTaxAcquired.Size = new System.Drawing.Size(26, 23);
            this.lblTaxAcquired.TabIndex = 67;
            this.lblTaxAcquired.Text = "TA";
            this.lblTaxAcquired.Visible = false;
            // 
            // lblSewerHeading
            // 
            this.lblSewerHeading.Location = new System.Drawing.Point(462, 124);
            this.lblSewerHeading.Name = "lblSewerHeading";
            this.lblSewerHeading.Size = new System.Drawing.Size(369, 15);
            this.lblSewerHeading.TabIndex = 58;
            this.lblSewerHeading.Text = "SEWER";
            this.lblSewerHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblSewerHeading.DoubleClick += new System.EventHandler(this.lblSewerHeading_DoubleClick);
            // 
            // lblWaterHeading
            // 
            this.lblWaterHeading.Location = new System.Drawing.Point(30, 124);
            this.lblWaterHeading.Name = "lblWaterHeading";
            this.lblWaterHeading.Size = new System.Drawing.Size(371, 16);
            this.lblWaterHeading.TabIndex = 57;
            this.lblWaterHeading.Text = "WATER";
            this.lblWaterHeading.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblWaterHeading.DoubleClick += new System.EventHandler(this.lblWaterHeading_DoubleClick);
            // 
            // imgNote
            // 
            this.imgNote.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgNote.Image = ((System.Drawing.Image)(resources.GetObject("imgNote.Image")));
            this.imgNote.Location = new System.Drawing.Point(30, 0);
            this.imgNote.Name = "imgNote";
            this.imgNote.Size = new System.Drawing.Size(26, 23);
            this.imgNote.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.imgNote.Visible = false;
            this.imgNote.DoubleClick += new System.EventHandler(this.imgNote_DoubleClick);
            // 
            // lblPaymentInfo
            // 
            this.lblPaymentInfo.Location = new System.Drawing.Point(30, 23);
            this.lblPaymentInfo.Name = "lblPaymentInfo";
            this.lblPaymentInfo.Size = new System.Drawing.Size(541, 18);
            this.lblPaymentInfo.TabIndex = 5;
            // 
            // cmdProcessEffective
            // 
            this.cmdProcessEffective.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessEffective.Location = new System.Drawing.Point(354, 29);
            this.cmdProcessEffective.Name = "cmdProcessEffective";
            this.cmdProcessEffective.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdProcessEffective.Size = new System.Drawing.Size(102, 24);
            this.cmdProcessEffective.TabIndex = 1;
            this.cmdProcessEffective.Text = "Effective Date";
            this.cmdProcessEffective.Click += new System.EventHandler(this.mnuProcessEffective_Click);
            // 
            // cmdFilePrint
            // 
            this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFilePrint.Location = new System.Drawing.Point(598, 29);
            this.cmdFilePrint.Name = "cmdFilePrint";
            this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdFilePrint.Size = new System.Drawing.Size(138, 24);
            this.cmdFilePrint.TabIndex = 2;
            this.cmdFilePrint.Text = "Print Account Detail";
            this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // cmdProcessGoTo
            // 
            this.cmdProcessGoTo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessGoTo.Location = new System.Drawing.Point(462, 29);
            this.cmdProcessGoTo.Name = "cmdProcessGoTo";
            this.cmdProcessGoTo.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdProcessGoTo.Size = new System.Drawing.Size(130, 24);
            this.cmdProcessGoTo.TabIndex = 3;
            this.cmdProcessGoTo.Text = "Go To Status View";
            this.cmdProcessGoTo.Click += new System.EventHandler(this.mnuProcessGoTo_Click);
            // 
            // cmdProcessChangeAccount
            // 
            this.cmdProcessChangeAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessChangeAccount.Location = new System.Drawing.Point(740, 29);
            this.cmdProcessChangeAccount.Name = "cmdProcessChangeAccount";
            this.cmdProcessChangeAccount.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdProcessChangeAccount.Size = new System.Drawing.Size(120, 24);
            this.cmdProcessChangeAccount.TabIndex = 5;
            this.cmdProcessChangeAccount.Text = "Change Account";
            this.cmdProcessChangeAccount.Click += new System.EventHandler(this.mnuProcessChangeAccount_Click);
            // 
            // cmdFileMortgageHolderInfo
            // 
            this.cmdFileMortgageHolderInfo.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileMortgageHolderInfo.Location = new System.Drawing.Point(866, 29);
            this.cmdFileMortgageHolderInfo.Name = "cmdFileMortgageHolderInfo";
            this.cmdFileMortgageHolderInfo.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdFileMortgageHolderInfo.Size = new System.Drawing.Size(194, 24);
            this.cmdFileMortgageHolderInfo.TabIndex = 6;
            this.cmdFileMortgageHolderInfo.Text = "Mortgage Holder Information";
            this.cmdFileMortgageHolderInfo.Click += new System.EventHandler(this.mnuFileMortgageHolderInfo_Click);
            // 
            // cmdSavePayments
            // 
            this.cmdSavePayments.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdSavePayments.Enabled = false;
            this.cmdSavePayments.Location = new System.Drawing.Point(246, 29);
            this.cmdSavePayments.Name = "cmdSavePayments";
            this.cmdSavePayments.Shortcut = Wisej.Web.Shortcut.F11;
            this.cmdSavePayments.Size = new System.Drawing.Size(102, 24);
            this.cmdSavePayments.TabIndex = 8;
            this.cmdSavePayments.Text = "Add Payment";
            this.cmdSavePayments.Click += new System.EventHandler(this.mnuPaymentSave_Click);
            // 
            // cmdPaymentSaveExit
            // 
            this.cmdPaymentSaveExit.AppearanceKey = "acceptButton";
            this.cmdPaymentSaveExit.Enabled = false;
            this.cmdPaymentSaveExit.Location = new System.Drawing.Point(349, 30);
            this.cmdPaymentSaveExit.Name = "cmdPaymentSaveExit";
            this.cmdPaymentSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPaymentSaveExit.Size = new System.Drawing.Size(216, 48);
            this.cmdPaymentSaveExit.TabIndex = 9;
            this.cmdPaymentSaveExit.Text = "Save Payments & Exit";
            this.cmdPaymentSaveExit.Click += new System.EventHandler(this.mnuPaymentSaveExit_Click);
            // 
            // lblACHInfo
            // 
            this.lblACHInfo.Font = new System.Drawing.Font("default", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.lblACHInfo.ForeColor = System.Drawing.Color.FromArgb(255, 0, 0);
            this.lblACHInfo.Location = new System.Drawing.Point(165, 0);
            this.lblACHInfo.Name = "lblACHInfo";
            this.lblACHInfo.Size = new System.Drawing.Size(21, 23);
            this.lblACHInfo.TabIndex = 75;
            this.lblACHInfo.Text = "A";
            this.lblACHInfo.Visible = false;
            // 
            // panelSewerTotals
            // 
            this.panelSewerTotals.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.panelSewerTotals.Controls.Add(this.txtSewerShortAccountTotalsAsOf);
            this.panelSewerTotals.Controls.Add(this.txtSewerTotalPTC);
            this.panelSewerTotals.Controls.Add(this.txtSewerTotalTax);
            this.panelSewerTotals.Controls.Add(this.txtSewerAccountTotalsAsOf);
            this.panelSewerTotals.Controls.Add(this.txtSewerTotalPrincipal);
            this.panelSewerTotals.Controls.Add(this.txtSewerTotalTotal);
            this.panelSewerTotals.Controls.Add(this.txtSewerTotalInterest);
            this.panelSewerTotals.Controls.Add(this.txtSewerTotalCosts);
            this.panelSewerTotals.Location = new System.Drawing.Point(3, 233);
            this.panelSewerTotals.Name = "panelSewerTotals";
            this.panelSewerTotals.Size = new System.Drawing.Size(902, 44);
            this.panelSewerTotals.TabIndex = 1001;
            this.panelSewerTotals.TabStop = true;
            // 
            // txtSewerShortAccountTotalsAsOf
            // 
            this.txtSewerShortAccountTotalsAsOf.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtSewerShortAccountTotalsAsOf.BackColor = System.Drawing.SystemColors.Window;
            this.txtSewerShortAccountTotalsAsOf.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtSewerShortAccountTotalsAsOf.Location = new System.Drawing.Point(1, 5);
            this.txtSewerShortAccountTotalsAsOf.Margin = new Wisej.Web.Padding(0);
            this.txtSewerShortAccountTotalsAsOf.Name = "txtSewerShortAccountTotalsAsOf";
            this.txtSewerShortAccountTotalsAsOf.Size = new System.Drawing.Size(275, 40);
            this.txtSewerShortAccountTotalsAsOf.TabIndex = 107;
            this.txtSewerShortAccountTotalsAsOf.Text = "TOTALS AS OF 00/00/0000";
            this.txtSewerShortAccountTotalsAsOf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSewerTotalPTC
            // 
            this.txtSewerTotalPTC.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtSewerTotalPTC.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtSewerTotalPTC.BackColor = System.Drawing.SystemColors.Window;
            this.txtSewerTotalPTC.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtSewerTotalPTC.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtSewerTotalPTC.Location = new System.Drawing.Point(337, 4);
            this.txtSewerTotalPTC.Margin = new Wisej.Web.Padding(0);
            this.txtSewerTotalPTC.Name = "txtSewerTotalPTC";
            this.txtSewerTotalPTC.Size = new System.Drawing.Size(103, 40);
            this.txtSewerTotalPTC.TabIndex = 106;
            this.txtSewerTotalPTC.Text = "0.00";
            this.txtSewerTotalPTC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtSewerTotalPTC.Visible = false;
            // 
            // txtSewerTotalTax
            // 
            this.txtSewerTotalTax.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtSewerTotalTax.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtSewerTotalTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtSewerTotalTax.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtSewerTotalTax.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtSewerTotalTax.Location = new System.Drawing.Point(386, 4);
            this.txtSewerTotalTax.Margin = new Wisej.Web.Padding(0);
            this.txtSewerTotalTax.Name = "txtSewerTotalTax";
            this.txtSewerTotalTax.Size = new System.Drawing.Size(103, 40);
            this.txtSewerTotalTax.TabIndex = 105;
            this.txtSewerTotalTax.Text = "0.00";
            this.txtSewerTotalTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSewerAccountTotalsAsOf
            // 
            this.txtSewerAccountTotalsAsOf.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtSewerAccountTotalsAsOf.BackColor = System.Drawing.SystemColors.Window;
            this.txtSewerAccountTotalsAsOf.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtSewerAccountTotalsAsOf.Location = new System.Drawing.Point(1, 5);
            this.txtSewerAccountTotalsAsOf.Margin = new Wisej.Web.Padding(0);
            this.txtSewerAccountTotalsAsOf.Name = "txtSewerAccountTotalsAsOf";
            this.txtSewerAccountTotalsAsOf.Size = new System.Drawing.Size(275, 40);
            this.txtSewerAccountTotalsAsOf.TabIndex = 104;
            this.txtSewerAccountTotalsAsOf.Text = "ACCOUNT TOTALS AS OF 00/00/0000";
            this.txtSewerAccountTotalsAsOf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtSewerTotalPrincipal
            // 
            this.txtSewerTotalPrincipal.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtSewerTotalPrincipal.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtSewerTotalPrincipal.BackColor = System.Drawing.SystemColors.Window;
            this.txtSewerTotalPrincipal.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtSewerTotalPrincipal.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtSewerTotalPrincipal.Location = new System.Drawing.Point(276, 4);
            this.txtSewerTotalPrincipal.Margin = new Wisej.Web.Padding(0);
            this.txtSewerTotalPrincipal.Name = "txtSewerTotalPrincipal";
            this.txtSewerTotalPrincipal.Size = new System.Drawing.Size(103, 40);
            this.txtSewerTotalPrincipal.TabIndex = 100;
            this.txtSewerTotalPrincipal.Text = "0.00";
            this.txtSewerTotalPrincipal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSewerTotalTotal
            // 
            this.txtSewerTotalTotal.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtSewerTotalTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtSewerTotalTotal.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtSewerTotalTotal.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtSewerTotalTotal.Location = new System.Drawing.Point(743, 5);
            this.txtSewerTotalTotal.Margin = new Wisej.Web.Padding(0);
            this.txtSewerTotalTotal.Name = "txtSewerTotalTotal";
            this.txtSewerTotalTotal.Size = new System.Drawing.Size(154, 40);
            this.txtSewerTotalTotal.TabIndex = 103;
            this.txtSewerTotalTotal.Text = "0.00";
            this.txtSewerTotalTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSewerTotalInterest
            // 
            this.txtSewerTotalInterest.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtSewerTotalInterest.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtSewerTotalInterest.BackColor = System.Drawing.SystemColors.Window;
            this.txtSewerTotalInterest.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtSewerTotalInterest.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtSewerTotalInterest.Location = new System.Drawing.Point(495, 5);
            this.txtSewerTotalInterest.Margin = new Wisej.Web.Padding(0);
            this.txtSewerTotalInterest.Name = "txtSewerTotalInterest";
            this.txtSewerTotalInterest.Size = new System.Drawing.Size(106, 40);
            this.txtSewerTotalInterest.TabIndex = 101;
            this.txtSewerTotalInterest.Text = "0.00";
            this.txtSewerTotalInterest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtSewerTotalCosts
            // 
            this.txtSewerTotalCosts.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtSewerTotalCosts.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtSewerTotalCosts.BackColor = System.Drawing.SystemColors.Window;
            this.txtSewerTotalCosts.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtSewerTotalCosts.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtSewerTotalCosts.Location = new System.Drawing.Point(607, 5);
            this.txtSewerTotalCosts.Margin = new Wisej.Web.Padding(0);
            this.txtSewerTotalCosts.Name = "txtSewerTotalCosts";
            this.txtSewerTotalCosts.Size = new System.Drawing.Size(130, 40);
            this.txtSewerTotalCosts.TabIndex = 102;
            this.txtSewerTotalCosts.Text = "0.00";
            this.txtSewerTotalCosts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // sewerGridPanel
            // 
            this.sewerGridPanel.ColumnCount = 1;
            this.sewerGridPanel.ColumnStyles.Clear();
            this.sewerGridPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.sewerGridPanel.Controls.Add(this.SGRID, 0, 0);
            this.sewerGridPanel.Controls.Add(this.panelSewerTotals, 0, 1);
            this.sewerGridPanel.Location = new System.Drawing.Point(132, 142);
            this.sewerGridPanel.MinimumSize = new System.Drawing.Size(0, 280);
            this.sewerGridPanel.Name = "sewerGridPanel";
            this.sewerGridPanel.RowCount = 2;
            this.sewerGridPanel.RowStyles.Clear();
            this.sewerGridPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 100F));
            this.sewerGridPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Absolute, 50F));
            this.sewerGridPanel.Size = new System.Drawing.Size(908, 280);
            this.sewerGridPanel.TabIndex = 1004;
            this.sewerGridPanel.TabStop = true;
            this.sewerGridPanel.Visible = false;
            // 
            // waterGridPanel
            // 
            this.waterGridPanel.ColumnCount = 1;
            this.waterGridPanel.ColumnStyles.Clear();
            this.waterGridPanel.ColumnStyles.Add(new Wisej.Web.ColumnStyle(Wisej.Web.SizeType.Percent, 50F));
            this.waterGridPanel.Controls.Add(this.panel2, 0, 1);
            this.waterGridPanel.Controls.Add(this.WGRID, 0, 0);
            this.waterGridPanel.Location = new System.Drawing.Point(132, 204);
            this.waterGridPanel.MinimumSize = new System.Drawing.Size(0, 280);
            this.waterGridPanel.Name = "waterGridPanel";
            this.waterGridPanel.RowCount = 2;
            this.waterGridPanel.RowStyles.Clear();
            this.waterGridPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Percent, 100F));
            this.waterGridPanel.RowStyles.Add(new Wisej.Web.RowStyle(Wisej.Web.SizeType.Absolute, 50F));
            this.waterGridPanel.Size = new System.Drawing.Size(908, 280);
            this.waterGridPanel.TabIndex = 1005;
            this.waterGridPanel.TabStop = true;
            this.waterGridPanel.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.txtWaterTotalPrincipal);
            this.panel2.Controls.Add(this.txtWaterShortAccountTotalsAsOf);
            this.panel2.Controls.Add(this.txtWaterTotalPTC);
            this.panel2.Controls.Add(this.txtWaterTotalTax);
            this.panel2.Controls.Add(this.txtWaterAccountTotalsAsOf);
            this.panel2.Controls.Add(this.txtWaterTotalTotal);
            this.panel2.Controls.Add(this.txtWaterTotalInterest);
            this.panel2.Controls.Add(this.txtWaterTotalCosts);
            this.panel2.Location = new System.Drawing.Point(3, 233);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(902, 44);
            this.panel2.TabIndex = 1002;
            this.panel2.TabStop = true;
            // 
            // txtWaterTotalPrincipal
            // 
            this.txtWaterTotalPrincipal.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtWaterTotalPrincipal.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtWaterTotalPrincipal.Location = new System.Drawing.Point(276, 5);
            this.txtWaterTotalPrincipal.Name = "txtWaterTotalPrincipal";
            this.txtWaterTotalPrincipal.Size = new System.Drawing.Size(103, 40);
            this.txtWaterTotalPrincipal.TabIndex = 109;
            this.txtWaterTotalPrincipal.Text = "0.00";
            this.txtWaterTotalPrincipal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWaterShortAccountTotalsAsOf
            // 
            this.txtWaterShortAccountTotalsAsOf.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtWaterShortAccountTotalsAsOf.BackColor = System.Drawing.SystemColors.Window;
            this.txtWaterShortAccountTotalsAsOf.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtWaterShortAccountTotalsAsOf.Location = new System.Drawing.Point(1, 5);
            this.txtWaterShortAccountTotalsAsOf.Margin = new Wisej.Web.Padding(0);
            this.txtWaterShortAccountTotalsAsOf.Name = "txtWaterShortAccountTotalsAsOf";
            this.txtWaterShortAccountTotalsAsOf.Size = new System.Drawing.Size(275, 40);
            this.txtWaterShortAccountTotalsAsOf.TabIndex = 108;
            this.txtWaterShortAccountTotalsAsOf.Text = "TOTALS AS OF 00/00/0000";
            this.txtWaterShortAccountTotalsAsOf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtWaterTotalPTC
            // 
            this.txtWaterTotalPTC.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtWaterTotalPTC.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtWaterTotalPTC.BackColor = System.Drawing.SystemColors.Window;
            this.txtWaterTotalPTC.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtWaterTotalPTC.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtWaterTotalPTC.Location = new System.Drawing.Point(325, 5);
            this.txtWaterTotalPTC.Margin = new Wisej.Web.Padding(0);
            this.txtWaterTotalPTC.Name = "txtWaterTotalPTC";
            this.txtWaterTotalPTC.Size = new System.Drawing.Size(103, 40);
            this.txtWaterTotalPTC.TabIndex = 107;
            this.txtWaterTotalPTC.Text = "0.00";
            this.txtWaterTotalPTC.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.txtWaterTotalPTC.Visible = false;
            // 
            // txtWaterTotalTax
            // 
            this.txtWaterTotalTax.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtWaterTotalTax.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtWaterTotalTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtWaterTotalTax.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtWaterTotalTax.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtWaterTotalTax.Location = new System.Drawing.Point(386, 5);
            this.txtWaterTotalTax.Margin = new Wisej.Web.Padding(0);
            this.txtWaterTotalTax.Name = "txtWaterTotalTax";
            this.txtWaterTotalTax.Size = new System.Drawing.Size(103, 40);
            this.txtWaterTotalTax.TabIndex = 106;
            this.txtWaterTotalTax.Text = "0.00";
            this.txtWaterTotalTax.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWaterAccountTotalsAsOf
            // 
            this.txtWaterAccountTotalsAsOf.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtWaterAccountTotalsAsOf.BackColor = System.Drawing.SystemColors.Window;
            this.txtWaterAccountTotalsAsOf.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtWaterAccountTotalsAsOf.Location = new System.Drawing.Point(1, 5);
            this.txtWaterAccountTotalsAsOf.Margin = new Wisej.Web.Padding(0);
            this.txtWaterAccountTotalsAsOf.Name = "txtWaterAccountTotalsAsOf";
            this.txtWaterAccountTotalsAsOf.Size = new System.Drawing.Size(275, 40);
            this.txtWaterAccountTotalsAsOf.TabIndex = 104;
            this.txtWaterAccountTotalsAsOf.Text = "ACCOUNT TOTALS AS OF 00/00/0000";
            this.txtWaterAccountTotalsAsOf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtWaterTotalTotal
            // 
            this.txtWaterTotalTotal.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtWaterTotalTotal.BackColor = System.Drawing.SystemColors.Window;
            this.txtWaterTotalTotal.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtWaterTotalTotal.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtWaterTotalTotal.Location = new System.Drawing.Point(743, 5);
            this.txtWaterTotalTotal.Margin = new Wisej.Web.Padding(0);
            this.txtWaterTotalTotal.Name = "txtWaterTotalTotal";
            this.txtWaterTotalTotal.Size = new System.Drawing.Size(154, 40);
            this.txtWaterTotalTotal.TabIndex = 103;
            this.txtWaterTotalTotal.Text = "0.00";
            this.txtWaterTotalTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWaterTotalInterest
            // 
            this.txtWaterTotalInterest.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtWaterTotalInterest.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtWaterTotalInterest.BackColor = System.Drawing.SystemColors.Window;
            this.txtWaterTotalInterest.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtWaterTotalInterest.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtWaterTotalInterest.Location = new System.Drawing.Point(495, 5);
            this.txtWaterTotalInterest.Margin = new Wisej.Web.Padding(0);
            this.txtWaterTotalInterest.Name = "txtWaterTotalInterest";
            this.txtWaterTotalInterest.Size = new System.Drawing.Size(106, 40);
            this.txtWaterTotalInterest.TabIndex = 101;
            this.txtWaterTotalInterest.Text = "0.00";
            this.txtWaterTotalInterest.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtWaterTotalCosts
            // 
            this.txtWaterTotalCosts.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.txtWaterTotalCosts.Appearance = fecherFoundation.FCLabel.AppearanceConstants.ccFlat;
            this.txtWaterTotalCosts.BackColor = System.Drawing.SystemColors.Window;
            this.txtWaterTotalCosts.Font = new System.Drawing.Font("@defaultBold", 13F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Pixel);
            this.txtWaterTotalCosts.ForeColor = System.Drawing.Color.FromArgb(5, 204, 71);
            this.txtWaterTotalCosts.Location = new System.Drawing.Point(607, 5);
            this.txtWaterTotalCosts.Margin = new Wisej.Web.Padding(0);
            this.txtWaterTotalCosts.Name = "txtWaterTotalCosts";
            this.txtWaterTotalCosts.Size = new System.Drawing.Size(130, 40);
            this.txtWaterTotalCosts.TabIndex = 102;
            this.txtWaterTotalCosts.Text = "0.00";
            this.txtWaterTotalCosts.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmUBPaymentStatus
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1078, 811);
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmUBPaymentStatus";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Status Screen";
            this.Shown += new System.EventHandler(this.frmUBPaymentStatus_Shown);
            this.FormClosing += new Wisej.Web.FormClosingEventHandler(this.frmUBPaymentStatus_FormClosing);
            this.Enter += new System.EventHandler(this.frmUBPaymentStatus_Enter);
            this.Resize += new System.EventHandler(this.frmUBPaymentStatus_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUBPaymentStatus_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).EndInit();
            this.fraRateInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraPayment)).EndInit();
            this.fraPayment.ResumeLayout(false);
            this.fraPayment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransactionDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcctNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraStatusLabels)).EndInit();
            this.fraStatusLabels.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WGRID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SGRID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessEffective)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessGoTo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessChangeAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileMortgageHolderInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSavePayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPaymentSaveExit)).EndInit();
            this.panelSewerTotals.ResumeLayout(false);
            this.sewerGridPanel.ResumeLayout(false);
            this.waterGridPanel.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdFilePrint;
		public FCButton cmdProcessEffective;
		public FCButton cmdProcessGoTo;
		public FCButton cmdProcessChangeAccount;
		public FCButton cmdFileMortgageHolderInfo;
		public FCButton cmdSavePayments;
		public FCButton cmdPaymentSaveExit;
		public FCLabel Label1_0;
		public FCLabel lblAccount;
		private FCLabel lblACHInfo;
        private JavaScript javaScript1;
        private Panel panelSewerTotals;
        public FCLabel txtSewerAccountTotalsAsOf;
        public FCLabel txtSewerTotalPrincipal;
        public FCLabel txtSewerTotalTotal;
        public FCLabel txtSewerTotalInterest;
        public FCLabel txtSewerTotalCosts;
        private TableLayoutPanel sewerGridPanel;
        private TableLayoutPanel waterGridPanel;
        private Panel panel2;
        public FCLabel txtWaterAccountTotalsAsOf;
        public FCLabel txtWaterTotalTotal;
        public FCLabel txtWaterTotalInterest;
        public FCLabel txtWaterTotalCosts;
        public FCLabel txtSewerTotalTax;
        public FCLabel txtWaterTotalTax;
        public FCLabel txtWaterTotalPTC;
        public FCLabel txtSewerTotalPTC;
        public FCLabel txtWaterShortAccountTotalsAsOf;
        public FCLabel txtSewerShortAccountTotalsAsOf;
        public FCLabel txtWaterTotalPrincipal;
    }
}