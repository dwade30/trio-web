﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using System.IO;
using System.Linq;
using Global;
using SharedApplication;
using SharedApplication.Enums;
using SharedApplication.Extensions;
using SharedApplication.UtilityBilling.Receipting;
using SharedApplication.UtilityBilling.Receipting.Interfaces;
using TWSharedLibrary;
using TWUT0000;
using modGlobal = TWUT0000.modMain;

namespace TWUT0000.Receipting
{
	/// <summary>
	/// Summary description for frmGetMasterAccount.
	/// </summary>
	public partial class frmCustomerSearch : BaseForm, IView<IUtilityBillingCustomerSearchViewModel>
	{
		private IUtilityBillingCustomerSearchViewModel viewModel { get; set; }
		private GlobalColorSettings globalColorSettings;
		private bool cancelling = true;
		public frmCustomerSearch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			cmbSearchPlace.SelectedIndex = 0;
		}

		int lngColHidden;
		int lngColAccountNumber;
		int lngColName;
		int lngColLocation;
		int lngColLocationNumber;
		int lngColTenant;
		int lngColSortName;
		int lngColSortName2;
		int lngColMapLot;
		int lngColREAcct;

		public frmCustomerSearch(IUtilityBillingCustomerSearchViewModel viewModel, GlobalColorSettings globalColorSettings) : this()
		{
			this.Text = "Select Master Account";
			
			lngColHidden = 0;
			lngColAccountNumber = 1;
			lngColName = 2;
			lngColLocationNumber = 4;
			lngColLocation = 5;
			lngColTenant = 3;
			lngColSortName = 6;
			lngColSortName2 = 7;
			lngColMapLot = 8;
			lngColREAcct = 9;

			this.ViewModel = viewModel;
			this.globalColorSettings = globalColorSettings;
		}

		public IUtilityBillingCustomerSearchViewModel ViewModel
		{
			get { return viewModel; }
			set
			{
				viewModel = value;
				SetupView();
			}
		}

		public void SetupView()
		{
			ShowLastAccount();
		}

		private void ShowLastAccount()
		{
			if (ViewModel.ShowLastAccount())
			{
				txtGetAccountNumber.Text = ViewModel.LastAccountAccessed.ToString();
			}
			if (txtGetAccountNumber.Enabled && txtGetAccountNumber.Visible)
			{
				txtGetAccountNumber.Focus();
			}
			txtGetAccountNumber.SelectionStart = 0;
			txtGetAccountNumber.SelectionLength = txtGetAccountNumber.Text.Length;
		}

		public void StartProgram(int lngCustNumber, int customerId)
		{
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Account");
			cancelling = false;
			Close();

			viewModel.LastAccountAccessed = lngCustNumber;
			viewModel.ProceedToNextScreen(lngCustNumber,customerId);
		}

		private void ClearSearch()
		{
			cmbSearchPlace.SelectedIndex = 0;
			cmbHidden.Text = "";
			txtSearch.Text = "";
			txtSearch2.Text = "";
		}

		private void cmdGetAccountNumber_Click(object sender, System.EventArgs e)
		{
			GetCustomerData();
		}

		private void ChooseCustomer(int account)
		{
            if (account == 0 && viewModel.ShowAccountScreen)
            {
                StartProgram(0, 0);
                return;
            }
			viewModel.SetSelectedCustomer(account);
			if (viewModel.SelectedCustomer != null)
			{
				if (viewModel.SelectedCustomer.Deleted ?? false)
				{
					if (MessageBox.Show("Account has been deleted.  Would you like to undelete this account?",
						    "Error", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (viewModel.UndeleteCustomer(viewModel.SelectedCustomer.AccountNumber ?? 0))
						{
							MessageBox.Show("Account Number #" + FCConvert.ToString(account) + " has been undeleted.", "Successful Undeletion", MessageBoxButtons.OK, MessageBoxIcon.Information);
							StartProgram(account,viewModel.SelectedCustomer.ID);
						}
					}
					else
					{
						SetAct(0);
						ShowLastAccount();
					}
				}
				else
				{
					StartProgram(account, viewModel.SelectedCustomer.ID);
				}
			}
			else
			{
				if (viewModel.ShowAccountScreen)
				{
					if (MessageBox.Show("There is no account with that number.  Would you like to create it?",
						    "Account Number Not Used", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
					    DialogResult.Yes)
					{
						StartProgram(account, -1);
					}
				}
				else
				{
					MessageBox.Show("There is no account with that number.", "Error Loading Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				
			}
		}
		private void GetCustomerData()
		{
			if (!viewModel.SearchResultsVisible)
			{
				int account = txtGetAccountNumber.Text.ToIntegerValue();
				if (account != 0 || viewModel.ShowAccountScreen)
				{
					ChooseCustomer(account);
				}
				else
				{
					MessageBox.Show("Please select a valid customer.", "Missing Customer Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					if (txtGetAccountNumber.Visible && txtGetAccountNumber.Enabled)
					{
						txtGetAccountNumber.Focus();
					}
				}
			}
			else if (viewModel.SearchResultsVisible)
			{
				SelectAccount(vsSearch.Row);
			}
		}

		public void cmdGetAccountNumber_Click()
		{
			cmdGetAccountNumber_Click(cmdSelectAccount, new System.EventArgs());
		}

		private void SearchCustomers()
		{
			frmWait.InstancePtr.Init("Please Wait...." + "\r\n" + "Searching");
			if (Strings.Trim(txtSearch.Text) != "")
			{
				viewModel.Search(cmbHidden.Text == "Name" ? UtilityBillingCustomerSearchViewModel.SearchType.Name : cmbHidden.Text == "Mailing Address" ? UtilityBillingCustomerSearchViewModel.SearchType.Address :
					cmbHidden.Text == "Location" ? UtilityBillingCustomerSearchViewModel.SearchType.Location : cmbHidden.Text == "Map / Lot" ? UtilityBillingCustomerSearchViewModel.SearchType.MapLot :
					cmbHidden.Text == "RE Acct Number" ? UtilityBillingCustomerSearchViewModel.SearchType.REAccountNumber : cmbHidden.Text == "Serial #" ? UtilityBillingCustomerSearchViewModel.SearchType.SerialNumber :
					cmbHidden.Text == "Remote Serial #" ? UtilityBillingCustomerSearchViewModel.SearchType.Remote : UtilityBillingCustomerSearchViewModel.SearchType.XRef1
					, txtSearch.Text.Trim(), txtSearch2.Text.Trim(), cmbSearchPlace.Text == "Starts With" ? SearchCriteriaMatchType.StartsWith : SearchCriteriaMatchType.Contains, chkShowPreviousOwnerInfo.Checked);
			}
			else
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Please enter a search string.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}

			if (viewModel.SearchResults.Any())
			{
				if (viewModel.SearchResults.Count() > 1)
				{
					FillSearchGrid();
					SetAct(1);
					frmWait.InstancePtr.Unload();
				}
				else
                {
                    var chosenAccount = viewModel.SearchResults.FirstOrDefault();
                    if (chosenAccount != null)
                    {
                        StartProgram(chosenAccount.AccountNumber, chosenAccount.Id);
                    }
                    else
                    {
                        StartProgram(0, 0);
                    }                    
				}
			}
			else
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("No accounts found.", "No Results", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		public void cmdSearch_Click()
		{
			SearchCustomers();
		}

		private void SetAct(int intAct)
		{
			if (intAct == 0)
			{
				vsSearch.Visible = false;
			}
			else if (intAct == 1)
			{
				vsSearch.Top = fraCriteria.Top + fraCriteria.Height + 20;
				vsSearch.Left = FCConvert.ToInt32((this.Width - vsSearch.Width) / 2.0);
				vsSearch.Visible = true;
			}
		}

		private void frmGetMasterAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (Strings.Trim(txtSearch.Text) != "" && (!viewModel.SearchResultsVisible || vsSearch.Row <= 0))
				{
					SearchCustomers();
				}
				else
				{
					GetCustomerData();
				}
			}
			else if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (vsSearch.Visible)
				{
					SetAct(0);
				}
				else
				{
					Close();
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}


		private void frmGetMasterAccount_Resize(object sender, System.EventArgs e)
		{
			if (viewModel.SearchResultsVisible)
			{
				FormatGrid();
				SetGridHeight();
			}
		}

		private void mnuFileClearSearch_Click(object sender, System.EventArgs e)
		{
			ClearSearch();
		}

		private void mnuFileSearch_Click(object sender, System.EventArgs e)
		{
			SearchCustomers();
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuSelectAccount_Click(object sender, System.EventArgs e)
		{
			cmdGetAccountNumber_Click();
		}

		private void mnuUndeleteMaster_Click(object sender, System.EventArgs e)
		{
			int AcctNum = 0;
			string strAcctNum;

			strAcctNum = Interaction.InputBox("Please enter the Account Number that you wish to Undelete", "Undelete", "000000");
			if (Information.IsNumeric(strAcctNum) == false || Conversion.Val(strAcctNum) == 0)
			{
				MessageBox.Show("Account Numbers must be numeric and nonzero.", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			else
			{
				AcctNum = FCConvert.ToInt32(Math.Round(Conversion.Val(strAcctNum)));
				ChooseCustomer(AcctNum);
			}
		}

		private void optSearchType_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			txtSearch2.Visible = false;
			txtSearch2.Text = "";
			lblSearch.Visible = false;
			lblSearch2.Visible = false;
			txtSearch.Left = cmbHidden.Left + cmbHidden.Width + 30;

			chkShowPreviousOwnerInfo.Visible = false;
			chkShowPreviousOwnerInfo.Checked = false;
			
			cmbSearchPlace.Visible = true;
			switch (cmbHidden.Text)
			{
				case "Name":
				{
					chkShowPreviousOwnerInfo.Visible = true;
					chkShowPreviousOwnerInfo.Checked = true;
					break;
				}
				case "Mailing Address":
				{
					chkShowPreviousOwnerInfo.Visible = true;
					chkShowPreviousOwnerInfo.Checked = true;
					break;
				}
				case "Map / Lot":
				{
					chkShowPreviousOwnerInfo.Visible = true;
					chkShowPreviousOwnerInfo.Checked = true;
					break;
				}
				case "RE Acct Number":
				{
					cmbSearchPlace.Visible = false;
					break;
				}
				case "Location":
				{
					txtSearch2.Visible = true;
					lblSearch.Visible = true;
					lblSearch2.Visible = true;
					lblSearch2.Left = cmbHidden.Left + cmbHidden.Width + 30;
					txtSearch2.Left = lblSearch2.Left + lblSearch2.Width + 15;
					lblSearch.Left = txtSearch2.Left + txtSearch2.Width + 30;
					txtSearch.Left = lblSearch.Left + lblSearch.Width + 30;
					break;
				}
            }
            cmbSearchPlace.Left = txtSearch.Left + txtSearch.Width + 15;
		}

		private void optSearchType_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = FCConvert.ToInt16(cmbHidden.SelectedIndex);
			optSearchType_CheckedChanged(index, sender, e);
		}

		private void txtGetAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSearch_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			
			if (KeyAscii == Keys.Right || KeyAscii == Keys.F13)
				KeyAscii = (Keys)0;
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void FillSearchGrid()
		{
			try
			{
				// this will fill the grid with all of the accounts that match the search criteria
				string strAcct = "";
				string strName = "";
				string strLocation = "";
				string strLocationNumber = "";
				string strTenant = "";
				string strSortList = "";
				string strSortList2 = "";
				string strML = "";
				string strREAcct = "";

				bool boolShowPrev = false;
				// reset all of the sizes and clear the grid
				FormatGrid(true);

				foreach (var result in viewModel.SearchResults)
				{
					strSortList2 = "";
					strSortList = "";
					strAcct = result.AccountNumber.ToString();
					strName = result.FullNameLF.Trim();
					strLocationNumber = result.StreetNumber == 0 ? "" : result.StreetNumber.ToString();
					strLocation = result.StreetName;
					strTenant = result.BilledFullNameLF;
					strML = result.MapLot;
					strREAcct = result.REAccount == 0 ? "" : result.REAccount.ToString();

					if (cmbHidden.SelectedIndex == 2)
					{
						strSortList = strLocation;
						if (Information.IsNumeric(strLocationNumber))
						{
							strSortList2 = Strings.Format(strLocationNumber, "000000#");
						}
						else
						{
							strSortList2 = strLocationNumber;
						}
					}
					else if (cmbHidden.SelectedIndex == 0)
					{
						if (cmbSearchPlace.SelectedIndex == 0)
						{
							if (strName.Left(txtSearch.Text.Trim().Length).ToUpper() == txtSearch.Text.Trim().ToUpper())
							{
								strSortList = strName;
							}
							else
							{
								strSortList = strTenant;
							}
						}
						else
						{
							if (strName.ToUpper().Contains(txtSearch.Text.Trim().ToUpper()))
							{
								strSortList = strName;
							}
							else
							{
								strSortList = strTenant;
							}
						}
					}


					vsSearch.AddItem("");
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColAccountNumber, strAcct);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColName, strName);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocationNumber, strLocationNumber);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColLocation, strLocation);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColHidden, result.Id);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColTenant, strTenant);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColSortName, strSortList);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColSortName2, strSortList2);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColMapLot, strML);
					vsSearch.TextMatrix(vsSearch.Rows - 1, lngColREAcct, strREAcct);

					if (result.PreviousOwner)
					{
						vsSearch.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsSearch.Rows - 1, 0, vsSearch.Rows - 1, vsSearch.Cols - 1, globalColorSettings.TRIOCOLORHIGHLIGHT);
					}
				}

				vsSearch.ColDataType(lngColLocationNumber, FCGrid.DataTypeSettings.flexDTLong);
				vsSearch.ColDataType(lngColAccountNumber, FCGrid.DataTypeSettings.flexDTLong);

				SetGridHeight();

				if (cmbHidden.SelectedIndex == 0 || cmbHidden.SelectedIndex == 2)
				{
					vsSearch.Select(1, lngColSortName, vsSearch.Rows - 1, lngColSortName2);
					vsSearch.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					vsSearch.Refresh();
					vsSearch.Select(0, 0);
				}
			}
			catch (Exception ex)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Search Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtSearch_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			if (KeyCode == FCConvert.ToInt32(Keys.Return))
			{
				cmdSearch_Click();
			}
		}

		private void FormatGrid(bool boolReset = false)
		{
			try
			{
				int lngWid = vsSearch.WidthOriginal;
				vsSearch.Cols = 10;
				if (boolReset)
				{
					vsSearch.Rows = 1;
					vsSearch.TextMatrix(0, lngColAccountNumber, "Acct");
					vsSearch.TextMatrix(0, lngColName, "Owner Name");
					vsSearch.TextMatrix(0, lngColLocation, "Location");
					vsSearch.TextMatrix(0, lngColLocationNumber, "#");
					vsSearch.TextMatrix(0, lngColTenant, "Tenant Name");
					vsSearch.TextMatrix(0, lngColSortName, "Sort Name");
					vsSearch.TextMatrix(0, lngColMapLot, "Map Lot");
					vsSearch.TextMatrix(0, lngColREAcct, "RE Acct");
					vsSearch.ExtendLastCol = true;
				}
				vsSearch.ColWidth(lngColHidden, 0);
				vsSearch.ColWidth(lngColAccountNumber, FCConvert.ToInt32(lngWid * 0.15));
				vsSearch.ColWidth(lngColLocationNumber, FCConvert.ToInt32(lngWid * 0.06));
				vsSearch.ColWidth(lngColName, FCConvert.ToInt32(lngWid * 0.25));
				vsSearch.ColWidth(lngColTenant, FCConvert.ToInt32(lngWid * 0.25));
				vsSearch.ColHidden(lngColSortName, true);
				vsSearch.ColHidden(lngColSortName2, true);
				vsSearch.ColHidden(lngColMapLot, true);
				vsSearch.ColHidden(lngColREAcct, true);
			}
			catch (Exception ex)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Formatting Search Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetGridHeight()
		{
			int lngRW = 0;
			lngRW = vsSearch.Rows;
			if ((lngRW * vsSearch.RowHeight(0)) + 70 > this.Height * 0.61)
			{
				vsSearch.Height = FCConvert.ToInt32(this.Height * 0.61);
				vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			}
			else
			{
				vsSearch.Height = (lngRW * vsSearch.RowHeight(0)) + 70;
				vsSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
		}

		private void SelectAccount(int lngRow)
		{
			int lngAcct = 0;
			if (lngRow > 0)
			{

				lngAcct = vsSearch.TextMatrix(lngRow, lngColAccountNumber).ToIntegerValue();
				if (lngAcct > 0)
				{
					ChooseCustomer(lngAcct);
				}
			}
			SetAct(0);
		}

		private void vsSearch_DblClick(object sender, System.EventArgs e)
		{
			if (vsSearch.Row > 0)
			{
				SelectAccount(vsSearch.MouseRow);
			}
		}

		private void vsSearch_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsSearch[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = vsSearch.GetFlexRowIndex(e.RowIndex);
			lngMC = vsSearch.GetFlexColIndex(e.ColumnIndex);
			if (lngMR > 0 && lngMC >= 0)
			{
				if (FCConvert.ToBoolean(lngMR) & lngMC > 0)
				{
					//ToolTip1.SetToolTip(vsSearch, vsSearch.TextMatrix(lngMR, lngMC));
					cell.ToolTipText = vsSearch.TextMatrix(lngMR, lngMC);
				}
			}
		}

		private void vsSearch_Click(object sender, EventArgs e)
		{
			if (Conversion.Val(Strings.Left(vsSearch.TextMatrix(vsSearch.Row, lngColAccountNumber), 6)) != 0)
			{
				txtGetAccountNumber.Text = Math.Round(Conversion.Val(vsSearch.TextMatrix(vsSearch.Row, lngColAccountNumber))).ToString();
			}
		}

		private void frmCustomerSearch_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (cancelling)
			{
				ViewModel.Cancel();
			}
		}

		private void mnuBatchProcessing_Click(object sender, EventArgs e)
		{
			cancelling = false;
			Close();

			viewModel.ProceedToBatchScreen();
		}

		private void frmCustomerSearch_Activated(object sender, EventArgs e)
		{
			lblNewAccountLabel.Visible = viewModel.ShowAccountScreen;
		}
	}
}
