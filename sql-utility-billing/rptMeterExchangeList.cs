﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptMeterExchangeList.
	/// </summary>
	public partial class rptMeterExchangeList : BaseSectionReport
	{
		public rptMeterExchangeList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			// FC:FINAL:VGE - i912 Name changed
			this.Name = "Meter Exchange List";
		}

		public static rptMeterExchangeList InstancePtr
		{
			get
			{
				return (rptMeterExchangeList)Sys.GetInstance(typeof(rptMeterExchangeList));
			}
		}

		protected rptMeterExchangeList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsAccountInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMeterExchangeList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               07/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/15/2004              *
		// ********************************************************
		bool blnFirstRecord;
		// is this the first record to be shown in report
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		// Recordset to hold our report information
		string strOrderBy;
		// What the report should be ordered by
		DateTime datSetDate;
		// Set date to check.  Only show meters with set date on or before this date
		bool blnShowBlankSetDate;
		// Show meters with no set date
		int intArrayMax;
		// How many books there are in the array
		int[] BookArray = null;
		// Array holding book numbers of selected books
		string strBooksSelection;
		// A - All Books   S - Selected Books
		string strSQL = "";
		// SQL statement to get information we need for report
		int intTotal;
		// Total of how many meters were reported on
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// if first record then dont move recordset just show information
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				// else move recordset and set eof based on if there is any mor einformation to show
				rsAccountInfo.MoveNext();
				eArgs.EOF = rsAccountInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			blnFirstRecord = true;
			intTotal = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// increment our total variable
				intTotal += 1;
				//FC:FINAL:MSH - can't implicitly convert from int to string. In VB6 we can set value to textBox without calling property "Text", but here we must use it.
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccount.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("AccountNumber"));
				// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
				fldName.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("OwnerName"));
				if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("StreetNumber")) + " " + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName")));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("StreetNumber")) + " " + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName")));
				}
				fldSerialNumber.Text = rsAccountInfo.Get_Fields_String("SerialNumber");
				fldRemoteNumber.Text = rsAccountInfo.Get_Fields_String("Remote");
				//FC:FINAL:MSH - can't implicitly convert from int to string (same with internal issue #914)
				fldSize.Text = FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("Size"));
				if (FCConvert.ToBoolean(rsAccountInfo.Get_Fields_Boolean("Backflow")))
				{
					fldBackflow.Text = "Y";
				}
				else
				{
					fldBackflow.Text = "N";
				}
				// if set date is null then show blank else show the date
				// If rsAccountInfo.Fields("ReplacementDate") <> #12:00:00 AM# Then
				if (rsAccountInfo.Get_Fields_DateTime("SetDate").ToOADate() != 0)
				{
					fldSetDate.Text = Strings.Format(rsAccountInfo.Get_Fields_DateTime("SetDate"), "MM/dd/yy");
				}
				else
				{
					fldSetDate.Text = "";
				}
				fldPhone.Text = rsAccountInfo.Get_Fields_String("Telephone");
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// show total meters reported on
			fldTotal.Text = intTotal.ToString();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}
		// vbPorter upgrade warning: intCT As short	OnWriteFCConvert.ToInt32(
		public void Init(string strBooks, ref string strOrder, DateTime datSet, bool blnShowBlank, short intCT, ref int[] PassBookArray)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int counter;
				string strOrderBySQL = "";
				// Initialize variables to use for report
				strBooksSelection = strBooks;
				strOrderBy = strOrder;
				datSetDate = datSet;
				blnShowBlankSetDate = blnShowBlank;
				intArrayMax = intCT;
				BookArray = PassBookArray;
				// Set report label to show date selected on previous screen
				lblDate.Text = "Set Date of " + Strings.Format(datSet, "MM/dd/yy") + " or earlier";
				// if all books are to be shown
				if (strBooksSelection == "A")
				{
					// set label saying so
					lblBooks.Text = "All Books";
					// set order by sql statement based on selection from previous screen
					if (strOrderBy == "N")
					{
						strOrderBySQL = " ORDER BY OwnerName";
					}
					else if (strOrderBy == "L")
					{
						strOrderBySQL = " ORDER BY StreetName, StreetNumber";
					}
					else if (strOrderBy == "A")
					{
						strOrderBySQL = " ORDER BY AccountNumber";
					}
					else if (strOrderBy == "S")
					{
						strOrderBySQL = " ORDER BY SerialNumber";
					}
					// If strOrderBy = "N" Then
					// strOrderBySQL = " ORDER BY OwnerName"
					// ElseIf strOrderBy = "L" Then
					// strOrderBySQL = " ORDER BY StreetName, StreetNumber"
					// Else
					// strOrderBySQL = " ORDER BY AccountNumber"
					// End If
					// if we don't want null dates then check to make sure they are not equal to 12:00:00 AM else just select everything on or before date selected in previous screen
					// If blnShowBlankSetDate = False Then
					// rsAccountInfo.OpenRecordset "SELECT AccountNumber, OwnerName, StreetName, StreetNumber, Apt, Telephone, SerialNumber, Remote, Size, Backflow, SetDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE (ReplacementDate <= #" & datSetDate & "# AND ReplacementDate <> #12:00:00 AM#) AND UseMeterChangeOut = TRUE " & strSQL & strOrderBySQL
					// Else
					// rsAccountInfo.OpenRecordset "SELECT AccountNumber, OwnerName, StreetName, StreetNumber, Apt, Telephone, SerialNumber, Remote, Size, Backflow, SetDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE (ReplacementDate <= #" & datSetDate & "# OR ReplacementDate = #12:00:00 AM#) AND UseMeterChangeOut = TRUE " & strSQL & strOrderBySQL
					// End If
					if (blnShowBlankSetDate == false)
					{
						rsAccountInfo.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, StreetName, StreetNumber, Apt, Telephone, SerialNumber, Remote, Size, Backflow, SetDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID) WHERE (SetDate <= '" + FCConvert.ToString(datSetDate) + "' AND SetDate <> '1899-12-30') " + strSQL + strOrderBySQL);
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, StreetName, StreetNumber, Apt, Telephone, SerialNumber, Remote, Size, Backflow, SetDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID) WHERE (SetDate <= '" + FCConvert.ToString(datSetDate) + "' OR SetDate = '1899-12-30') " + strSQL + strOrderBySQL);
					}
				}
				else
				{
					// if we are only reporting on selected books then build the sql string to find the records we want
					if (Information.UBound(BookArray, 1) == 1)
					{
						lblBooks.Text = "Book " + FCConvert.ToString(BookArray[1]);
						strSQL = " = " + FCConvert.ToString(BookArray[1]);
					}
					else
					{
						lblBooks.Text = "Books ";
						for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
						{
							strSQL += FCConvert.ToString(BookArray[counter]) + ",";
							lblBooks.Text = lblBooks.Text + FCConvert.ToString(BookArray[counter]) + ",";
						}
						strSQL = "IN (" + Strings.Left(strSQL, strSQL.Length - 1) + ")";
						lblBooks.Text = Strings.Left(lblBooks.Text, lblBooks.Text.Length - 1);
					}
					// set order by clause based on selections from previous screen
					if (strOrderBy == "N")
					{
						strOrderBySQL = " ORDER BY OwnerName";
					}
					else if (strOrderBy == "L")
					{
						strOrderBySQL = " ORDER BY StreetName, StreetNumber";
					}
					else if (strOrderBy == "A")
					{
						strOrderBySQL = " ORDER BY Sequence";
					}
					else if (strOrderBy == "S")
					{
						strOrderBySQL = " ORDER BY SerialNumber";
					}
					// If strOrderBy = "N" Then
					// strOrderBySQL = " ORDER BY OwnerName"
					// ElseIf strOrderBy = "L" Then
					// strOrderBySQL = " ORDER BY StreetName, StreetNumber"
					// Else
					// strOrderBySQL = " ORDER BY Sequence"
					// End If
					// if we don't want null dates then check to make sure they are nto equal to 12:00:00 AM else just select everything on or before date selected in previous screen
					// If blnShowBlankSetDate = False Then
					// rsAccountInfo.OpenRecordset "SELECT AccountNumber, OwnerName, StreetName, StreetNumber, Apt, Telephone, SerialNumber, Remote, Size, Backflow, SetDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE (ReplacementDate <= #" & datSetDate & "# AND ReplacementDate <> #12:00:00 AM#) AND UseMeterChangeOut = TRUE AND BookNumber " & strSQL & strOrderBySQL
					// Else
					// rsAccountInfo.OpenRecordset "SELECT AccountNumber, OwnerName, StreetName, StreetNumber, Apt, Telephone, SerialNumber, Remote, Size, Backflow, SetDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE (ReplacementDate <= #" & datSetDate & "# OR ReplacementDate = #12:00:00 AM#) AND UseMeterChangeOut = TRUE AND BookNumber " & strSQL & strOrderBySQL
					// End If
					if (blnShowBlankSetDate == false)
					{
						rsAccountInfo.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, StreetName, StreetNumber, Apt, Telephone, SerialNumber, Remote, Size, Backflow, SetDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID) WHERE (SetDate <= '" + FCConvert.ToString(datSetDate) + "' AND SetDate <> '1899-12-30') AND BookNumber " + strSQL + strOrderBySQL);
					}
					else
					{
						rsAccountInfo.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, StreetName, StreetNumber, Apt, Telephone, SerialNumber, Remote, Size, Backflow, SetDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID) WHERE (SetDate <= '" + FCConvert.ToString(datSetDate) + "' OR SetDate = '1899-12-30') AND BookNumber " + strSQL + strOrderBySQL);
					}
				}
				// if we found some records show report otherwise pop up message and end report
				if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
				{
					frmReportViewer.InstancePtr.Init(this);
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void rptMeterExchangeList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMeterExchangeList properties;
			//rptMeterExchangeList.Caption	= "Meter Exchange List";
			//rptMeterExchangeList.Icon	= "rptMeterExchangeList.dsx":0000";
			//rptMeterExchangeList.Left	= 0;
			//rptMeterExchangeList.Top	= 0;
			//rptMeterExchangeList.Width	= 11880;
			//rptMeterExchangeList.Height	= 8595;
			//rptMeterExchangeList.StartUpPosition	= 3;
			//rptMeterExchangeList.SectionData	= "rptMeterExchangeList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
