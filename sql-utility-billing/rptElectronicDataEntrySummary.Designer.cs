﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptElectronicDataEntrySummary.
	/// </summary>
	partial class rptElectronicDataEntrySummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptElectronicDataEntrySummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSeq = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblNoInformation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalmeters = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNoMatchRecords = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNotUpdatedMeters = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDuplicateRecords = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalRecords = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalMeters = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNoMatchRecords = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNotUpdatedMeters = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDuplicateRecords = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMetersUpdated = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMetersUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblDuplicateMeters = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDuplicateMeters = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRecordsUpdated = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRecordsUpdated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblNotUpdatedRecords = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldNotUpdatedRecords = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoInformation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalmeters)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoMatchRecords)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNotUpdatedMeters)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDuplicateRecords)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRecords)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMeters)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoMatchRecords)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotUpdatedMeters)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateRecords)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMetersUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMetersUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDuplicateMeters)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateMeters)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRecordsUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRecordsUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNotUpdatedRecords)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotUpdatedRecords)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBook,
				this.fldSequence,
				this.fldCurrent,
				this.fldName,
				this.lblNoInformation,
				this.fldAccount
			});
			this.Detail.Height = 0.375F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblBook,
				this.lblSeq,
				this.lblCurrent,
				this.lblName,
				this.Line1,
				this.lblAccount
			});
			this.PageHeader.Height = 0.6875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label20,
				this.Line2,
				this.Label21,
				this.lblTotalmeters,
				this.lblNoMatchRecords,
				this.lblNotUpdatedMeters,
				this.lblDuplicateRecords,
				this.fldTotalRecords,
				this.fldTotalMeters,
				this.fldNoMatchRecords,
				this.fldNotUpdatedMeters,
				this.fldDuplicateRecords,
				this.lblMetersUpdated,
				this.fldMetersUpdated,
				this.lblDuplicateMeters,
				this.fldDuplicateMeters,
				this.lblRecordsUpdated,
				this.fldRecordsUpdated,
				this.lblNotUpdatedRecords,
				this.fldNotUpdatedRecords
			});
			this.GroupFooter1.Height = 2.354167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblType,
				this.Binder
			});
			this.GroupHeader2.DataField = "Binder";
			this.GroupHeader2.Height = 0.2291667F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Height = 0F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Electronic Data Entry Summary";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblBook
			// 
			this.lblBook.Height = 0.1875F;
			this.lblBook.HyperLink = null;
			this.lblBook.Left = 0.9375F;
			this.lblBook.Name = "lblBook";
			this.lblBook.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblBook.Text = "Book";
			this.lblBook.Top = 0.5F;
			this.lblBook.Width = 0.625F;
			// 
			// lblSeq
			// 
			this.lblSeq.Height = 0.1875F;
			this.lblSeq.HyperLink = null;
			this.lblSeq.Left = 1.5625F;
			this.lblSeq.Name = "lblSeq";
			this.lblSeq.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblSeq.Text = "Seq#";
			this.lblSeq.Top = 0.5F;
			this.lblSeq.Width = 0.625F;
			// 
			// lblCurrent
			// 
			this.lblCurrent.Height = 0.1875F;
			this.lblCurrent.HyperLink = null;
			this.lblCurrent.Left = 2.25F;
			this.lblCurrent.Name = "lblCurrent";
			this.lblCurrent.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.lblCurrent.Text = "Current";
			this.lblCurrent.Top = 0.5F;
			this.lblCurrent.Width = 0.8125F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 3.1875F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.5F;
			this.lblName.Width = 2.125F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.6875F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.6875F;
			this.Line1.Y2 = 0.6875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.3125F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.5F;
			this.lblAccount.Width = 0.625F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 0F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblType.Text = "Label20";
			this.lblType.Top = 0.03125F;
			this.lblType.Width = 2.9375F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.125F;
			this.Binder.Left = 3.34375F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.03125F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.65625F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0.9375F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "text-align: right";
			this.fldBook.Text = null;
			this.fldBook.Top = 0F;
			this.fldBook.Width = 0.625F;
			// 
			// fldSequence
			// 
			this.fldSequence.Height = 0.1875F;
			this.fldSequence.Left = 1.5625F;
			this.fldSequence.Name = "fldSequence";
			this.fldSequence.Style = "text-align: right";
			this.fldSequence.Text = null;
			this.fldSequence.Top = 0F;
			this.fldSequence.Width = 0.625F;
			// 
			// fldCurrent
			// 
			this.fldCurrent.Height = 0.1875F;
			this.fldCurrent.Left = 2.25F;
			this.fldCurrent.Name = "fldCurrent";
			this.fldCurrent.Style = "text-align: right";
			this.fldCurrent.Text = null;
			this.fldCurrent.Top = 0F;
			this.fldCurrent.Width = 0.8125F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 3.1875F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "text-align: left";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 3.4375F;
			// 
			// lblNoInformation
			// 
			this.lblNoInformation.Height = 0.1875F;
			this.lblNoInformation.HyperLink = null;
			this.lblNoInformation.Left = 0.5F;
			this.lblNoInformation.Name = "lblNoInformation";
			this.lblNoInformation.Style = "";
			this.lblNoInformation.Text = "No Information";
			this.lblNoInformation.Top = 0.1875F;
			this.lblNoInformation.Visible = false;
			this.lblNoInformation.Width = 4.5625F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0.3125F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "text-align: right";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.625F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 2.90625F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 1";
			this.Label20.Text = "Summary";
			this.Label20.Top = 0.3125F;
			this.Label20.Width = 1.6875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.40625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.5F;
			this.Line2.Width = 2.6875F;
			this.Line2.X1 = 2.40625F;
			this.Line2.X2 = 5.09375F;
			this.Line2.Y1 = 0.5F;
			this.Line2.Y2 = 0.5F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.1875F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 2.125F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "";
			this.Label21.Text = "Total records in file:";
			this.Label21.Top = 0.5F;
			this.Label21.Width = 1.6875F;
			// 
			// lblTotalmeters
			// 
			this.lblTotalmeters.Height = 0.1875F;
			this.lblTotalmeters.HyperLink = null;
			this.lblTotalmeters.Left = 2.125F;
			this.lblTotalmeters.Name = "lblTotalmeters";
			this.lblTotalmeters.Style = "";
			this.lblTotalmeters.Text = "Total meters in selected books:";
			this.lblTotalmeters.Top = 1.5F;
			this.lblTotalmeters.Width = 1.6875F;
			// 
			// lblNoMatchRecords
			// 
			this.lblNoMatchRecords.Height = 0.1875F;
			this.lblNoMatchRecords.HyperLink = null;
			this.lblNoMatchRecords.Left = 2.375F;
			this.lblNoMatchRecords.Name = "lblNoMatchRecords";
			this.lblNoMatchRecords.Style = "";
			this.lblNoMatchRecords.Text = "Total records with no match found:";
			this.lblNoMatchRecords.Top = 0.875F;
			this.lblNoMatchRecords.Width = 2.375F;
			// 
			// lblNotUpdatedMeters
			// 
			this.lblNotUpdatedMeters.Height = 0.1875F;
			this.lblNotUpdatedMeters.HyperLink = null;
			this.lblNotUpdatedMeters.Left = 2.375F;
			this.lblNotUpdatedMeters.Name = "lblNotUpdatedMeters";
			this.lblNotUpdatedMeters.Style = "";
			this.lblNotUpdatedMeters.Text = "Total meters not updated:";
			this.lblNotUpdatedMeters.Top = 1.875F;
			this.lblNotUpdatedMeters.Width = 1.6875F;
			// 
			// lblDuplicateRecords
			// 
			this.lblDuplicateRecords.Height = 0.1875F;
			this.lblDuplicateRecords.HyperLink = null;
			this.lblDuplicateRecords.Left = 2.375F;
			this.lblDuplicateRecords.Name = "lblDuplicateRecords";
			this.lblDuplicateRecords.Style = "";
			this.lblDuplicateRecords.Text = "Total duplicate readings:";
			this.lblDuplicateRecords.Top = 1.25F;
			this.lblDuplicateRecords.Width = 1.6875F;
			// 
			// fldTotalRecords
			// 
			this.fldTotalRecords.Height = 0.1875F;
			this.fldTotalRecords.Left = 4.5F;
			this.fldTotalRecords.Name = "fldTotalRecords";
			this.fldTotalRecords.Style = "text-align: right";
			this.fldTotalRecords.Text = null;
			this.fldTotalRecords.Top = 0.5F;
			this.fldTotalRecords.Width = 0.5F;
			// 
			// fldTotalMeters
			// 
			this.fldTotalMeters.Height = 0.1875F;
			this.fldTotalMeters.Left = 4.5F;
			this.fldTotalMeters.Name = "fldTotalMeters";
			this.fldTotalMeters.Style = "text-align: right";
			this.fldTotalMeters.Text = null;
			this.fldTotalMeters.Top = 1.5F;
			this.fldTotalMeters.Width = 0.5F;
			// 
			// fldNoMatchRecords
			// 
			this.fldNoMatchRecords.Height = 0.1875F;
			this.fldNoMatchRecords.Left = 4.75F;
			this.fldNoMatchRecords.Name = "fldNoMatchRecords";
			this.fldNoMatchRecords.Style = "text-align: right";
			this.fldNoMatchRecords.Text = null;
			this.fldNoMatchRecords.Top = 0.875F;
			this.fldNoMatchRecords.Width = 0.5F;
			// 
			// fldNotUpdatedMeters
			// 
			this.fldNotUpdatedMeters.Height = 0.1875F;
			this.fldNotUpdatedMeters.Left = 4.75F;
			this.fldNotUpdatedMeters.Name = "fldNotUpdatedMeters";
			this.fldNotUpdatedMeters.Style = "text-align: right";
			this.fldNotUpdatedMeters.Text = null;
			this.fldNotUpdatedMeters.Top = 1.875F;
			this.fldNotUpdatedMeters.Width = 0.5F;
			// 
			// fldDuplicateRecords
			// 
			this.fldDuplicateRecords.Height = 0.1875F;
			this.fldDuplicateRecords.Left = 4.75F;
			this.fldDuplicateRecords.Name = "fldDuplicateRecords";
			this.fldDuplicateRecords.Style = "text-align: right";
			this.fldDuplicateRecords.Text = null;
			this.fldDuplicateRecords.Top = 1.25F;
			this.fldDuplicateRecords.Width = 0.5F;
			// 
			// lblMetersUpdated
			// 
			this.lblMetersUpdated.Height = 0.1875F;
			this.lblMetersUpdated.HyperLink = null;
			this.lblMetersUpdated.Left = 2.375F;
			this.lblMetersUpdated.Name = "lblMetersUpdated";
			this.lblMetersUpdated.Style = "";
			this.lblMetersUpdated.Text = "Total meters updated:";
			this.lblMetersUpdated.Top = 1.6875F;
			this.lblMetersUpdated.Width = 1.6875F;
			// 
			// fldMetersUpdated
			// 
			this.fldMetersUpdated.Height = 0.1875F;
			this.fldMetersUpdated.Left = 4.75F;
			this.fldMetersUpdated.Name = "fldMetersUpdated";
			this.fldMetersUpdated.Style = "text-align: right";
			this.fldMetersUpdated.Text = null;
			this.fldMetersUpdated.Top = 1.6875F;
			this.fldMetersUpdated.Width = 0.5F;
			// 
			// lblDuplicateMeters
			// 
			this.lblDuplicateMeters.Height = 0.1875F;
			this.lblDuplicateMeters.HyperLink = null;
			this.lblDuplicateMeters.Left = 2.375F;
			this.lblDuplicateMeters.Name = "lblDuplicateMeters";
			this.lblDuplicateMeters.Style = "";
			this.lblDuplicateMeters.Text = "Total duplicate meters:";
			this.lblDuplicateMeters.Top = 2.0625F;
			this.lblDuplicateMeters.Width = 1.6875F;
			// 
			// fldDuplicateMeters
			// 
			this.fldDuplicateMeters.Height = 0.1875F;
			this.fldDuplicateMeters.Left = 4.75F;
			this.fldDuplicateMeters.Name = "fldDuplicateMeters";
			this.fldDuplicateMeters.Style = "text-align: right";
			this.fldDuplicateMeters.Text = null;
			this.fldDuplicateMeters.Top = 2.0625F;
			this.fldDuplicateMeters.Width = 0.5F;
			// 
			// lblRecordsUpdated
			// 
			this.lblRecordsUpdated.Height = 0.1875F;
			this.lblRecordsUpdated.HyperLink = null;
			this.lblRecordsUpdated.Left = 2.375F;
			this.lblRecordsUpdated.Name = "lblRecordsUpdated";
			this.lblRecordsUpdated.Style = "";
			this.lblRecordsUpdated.Text = "Total records updated:";
			this.lblRecordsUpdated.Top = 0.6875F;
			this.lblRecordsUpdated.Width = 1.6875F;
			// 
			// fldRecordsUpdated
			// 
			this.fldRecordsUpdated.Height = 0.1875F;
			this.fldRecordsUpdated.Left = 4.75F;
			this.fldRecordsUpdated.Name = "fldRecordsUpdated";
			this.fldRecordsUpdated.Style = "text-align: right";
			this.fldRecordsUpdated.Text = null;
			this.fldRecordsUpdated.Top = 0.6875F;
			this.fldRecordsUpdated.Width = 0.5F;
			// 
			// lblNotUpdatedRecords
			// 
			this.lblNotUpdatedRecords.Height = 0.1875F;
			this.lblNotUpdatedRecords.HyperLink = null;
			this.lblNotUpdatedRecords.Left = 2.375F;
			this.lblNotUpdatedRecords.Name = "lblNotUpdatedRecords";
			this.lblNotUpdatedRecords.Style = "";
			this.lblNotUpdatedRecords.Text = "Total records with duplicate meters:";
			this.lblNotUpdatedRecords.Top = 1.0625F;
			this.lblNotUpdatedRecords.Width = 2.375F;
			// 
			// fldNotUpdatedRecords
			// 
			this.fldNotUpdatedRecords.Height = 0.1875F;
			this.fldNotUpdatedRecords.Left = 4.75F;
			this.fldNotUpdatedRecords.Name = "fldNotUpdatedRecords";
			this.fldNotUpdatedRecords.Style = "text-align: right";
			this.fldNotUpdatedRecords.Text = null;
			this.fldNotUpdatedRecords.Top = 1.0625F;
			this.fldNotUpdatedRecords.Width = 0.5F;
			// 
			// rptElectronicDataEntrySummary
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoInformation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalmeters)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoMatchRecords)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNotUpdatedMeters)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDuplicateRecords)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRecords)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMeters)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoMatchRecords)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotUpdatedMeters)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateRecords)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMetersUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMetersUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDuplicateMeters)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDuplicateMeters)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRecordsUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRecordsUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNotUpdatedRecords)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNotUpdatedRecords)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNoInformation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSeq;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalmeters;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNoMatchRecords;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNotUpdatedMeters;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDuplicateRecords;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRecords;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMeters;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoMatchRecords;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNotUpdatedMeters;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDuplicateRecords;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMetersUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMetersUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDuplicateMeters;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDuplicateMeters;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRecordsUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRecordsUpdated;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNotUpdatedRecords;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNotUpdatedRecords;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
	}
}
