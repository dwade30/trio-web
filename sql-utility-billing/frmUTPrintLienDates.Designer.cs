﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTPrintLienDates.
	/// </summary>
	partial class frmUTPrintLienDates : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCComboBox cmbRK;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintListing;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTPrintLienDates));
			this.cmbPrint = new fecherFoundation.FCComboBox();
			this.lblPrint = new fecherFoundation.FCLabel();
			this.cmbRK = new fecherFoundation.FCComboBox();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrintListing = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSaveExit = new fecherFoundation.FCButton();
			this.cmdFilePrintListing = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintListing)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSaveExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 267);
			this.BottomPanel.Size = new System.Drawing.Size(442, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbPrint);
			this.ClientArea.Controls.Add(this.lblPrint);
			this.ClientArea.Controls.Add(this.cmbRK);
			this.ClientArea.Controls.Add(this.cmbYear);
			this.ClientArea.Size = new System.Drawing.Size(442, 207);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrintListing);
			this.TopPanel.Size = new System.Drawing.Size(442, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrintListing, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(187, 30);
			this.HeaderText.Text = "Print Lien Dates";
			// 
			// cmbPrint
			// 
			this.cmbPrint.AutoSize = false;
			this.cmbPrint.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbPrint.FormattingEnabled = true;
			this.cmbPrint.Location = new System.Drawing.Point(130, 30);
			this.cmbPrint.Name = "cmbPrint";
			this.cmbPrint.Size = new System.Drawing.Size(245, 40);
			this.cmbPrint.TabIndex = 1;
			// 
			// lblPrint
			// 
			this.lblPrint.AutoSize = true;
			this.lblPrint.Location = new System.Drawing.Point(30, 44);
			this.lblPrint.Name = "lblPrint";
			this.lblPrint.Size = new System.Drawing.Size(45, 15);
			this.lblPrint.TabIndex = 0;
			this.lblPrint.Text = "PRINT";
			// 
			// cmbRK
			// 
			this.cmbRK.AutoSize = false;
			this.cmbRK.BackColor = System.Drawing.SystemColors.Window;
			this.cmbRK.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRK.FormattingEnabled = true;
			this.cmbRK.Location = new System.Drawing.Point(130, 90);
			this.cmbRK.Name = "cmbRK";
			this.cmbRK.Size = new System.Drawing.Size(245, 40);
			this.cmbRK.TabIndex = 2;
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(130, 150);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(245, 40);
			this.cmbYear.TabIndex = 3;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrintListing,
				this.mnuFileSaveExit,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFilePrintListing
			// 
			this.mnuFilePrintListing.Index = 0;
			this.mnuFilePrintListing.Name = "mnuFilePrintListing";
			this.mnuFilePrintListing.Text = "Print Rate Key Listing";
			this.mnuFilePrintListing.Click += new System.EventHandler(this.mnuFilePrintListing_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 1;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Continue";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 3;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileSaveExit
			// 
			this.cmdFileSaveExit.AppearanceKey = "acceptButton";
			this.cmdFileSaveExit.Location = new System.Drawing.Point(147, 30);
			this.cmdFileSaveExit.Name = "cmdFileSaveExit";
			this.cmdFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSaveExit.Size = new System.Drawing.Size(158, 48);
			this.cmdFileSaveExit.TabIndex = 0;
			this.cmdFileSaveExit.Text = "Save & Continue";
			this.cmdFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// cmdFilePrintListing
			// 
			this.cmdFilePrintListing.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrintListing.AppearanceKey = "toolbarButton";
			this.cmdFilePrintListing.Location = new System.Drawing.Point(270, 29);
			this.cmdFilePrintListing.Name = "cmdFilePrintListing";
			this.cmdFilePrintListing.Size = new System.Drawing.Size(144, 24);
			this.cmdFilePrintListing.TabIndex = 1;
			this.cmdFilePrintListing.Text = "Print Rate Key Listing";
			this.cmdFilePrintListing.Click += new System.EventHandler(this.mnuFilePrintListing_Click);
			// 
			// frmUTPrintLienDates
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(442, 375);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUTPrintLienDates";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Lien Dates";
			this.Load += new System.EventHandler(this.frmUTPrintLienDates_Load);
			this.Activated += new System.EventHandler(this.frmUTPrintLienDates_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTPrintLienDates_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTPrintLienDates_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSaveExit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintListing)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSaveExit;
		private FCButton cmdFilePrintListing;
	}
}
