﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDisconnectNoticeOriginalPlainPaper2.
	/// </summary>
	partial class rptDisconnectNoticeOriginalPlainPaper2
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDisconnectNoticeOriginalPlainPaper2));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldReturnName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNoticeDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldShutOffDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerTotalLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotalLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAsterisk = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeperateSewerLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAsterisk2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeperateSewer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.linTotalLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldPUCText1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPUCText2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPUCText7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPUCText3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPUCText4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPUCText5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPUCText6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPUCText8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDefaultText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBAddress5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinAidText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoticeDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldShutOffDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTotalLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewerLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefaultText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinAidText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldReturnName,
				this.fldReturnAddress1,
				this.fldReturnAddress2,
				this.fldReturnAddress3,
				this.fldReturnAddress4,
				this.fldBookSequence,
				this.Field1,
				this.Field2,
				this.fldAccountNumber,
				this.Field3,
				this.Line1,
				this.Line2,
				this.lblText,
				this.Field4,
				this.fldNoticeDate,
				this.Field5,
				this.fldShutOffDate,
				this.Field6,
				this.fldWaterTotal,
				this.fldSewerTotalLabel,
				this.fldSewerTotal,
				this.fldTotalTotalLabel,
				this.fldTotalTotal,
				this.lblAsterisk,
				this.fldSeperateSewerLabel,
				this.lblAsterisk2,
				this.fldSeperateSewer,
				this.fldName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.linTotalLine,
				this.fldPUCText1,
				this.fldPUCText2,
				this.fldPUCText7,
				this.fldPUCText3,
				this.fldPUCText4,
				this.fldPUCText5,
				this.fldPUCText6,
				this.fldPUCText8,
				this.fldBName,
				this.fldBAddress1,
				this.fldBAddress2,
				this.fldBAddress3,
				this.fldBAddress4,
				this.fldAccount2,
				this.fldDefaultText,
				this.fldAddress5,
				this.fldBAddress5,
				this.fldFinAidText
			});
			this.Detail.Height = 10.48958F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.CanShrink = true;
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldReturnName
			// 
			this.fldReturnName.Height = 0.1875F;
			this.fldReturnName.Left = 0F;
			this.fldReturnName.Name = "fldReturnName";
			this.fldReturnName.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnName.Text = " ";
			this.fldReturnName.Top = 0F;
			this.fldReturnName.Visible = false;
			this.fldReturnName.Width = 2.375F;
			// 
			// fldReturnAddress1
			// 
			this.fldReturnAddress1.Height = 0.1875F;
			this.fldReturnAddress1.Left = 0F;
			this.fldReturnAddress1.Name = "fldReturnAddress1";
			this.fldReturnAddress1.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnAddress1.Text = " ";
			this.fldReturnAddress1.Top = 0.1875F;
			this.fldReturnAddress1.Visible = false;
			this.fldReturnAddress1.Width = 2.375F;
			// 
			// fldReturnAddress2
			// 
			this.fldReturnAddress2.Height = 0.1875F;
			this.fldReturnAddress2.Left = 0F;
			this.fldReturnAddress2.Name = "fldReturnAddress2";
			this.fldReturnAddress2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnAddress2.Text = " ";
			this.fldReturnAddress2.Top = 0.375F;
			this.fldReturnAddress2.Visible = false;
			this.fldReturnAddress2.Width = 2.375F;
			// 
			// fldReturnAddress3
			// 
			this.fldReturnAddress3.Height = 0.1875F;
			this.fldReturnAddress3.Left = 0F;
			this.fldReturnAddress3.Name = "fldReturnAddress3";
			this.fldReturnAddress3.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnAddress3.Text = " ";
			this.fldReturnAddress3.Top = 0.5625F;
			this.fldReturnAddress3.Visible = false;
			this.fldReturnAddress3.Width = 2.375F;
			// 
			// fldReturnAddress4
			// 
			this.fldReturnAddress4.Height = 0.1875F;
			this.fldReturnAddress4.Left = 0F;
			this.fldReturnAddress4.Name = "fldReturnAddress4";
			this.fldReturnAddress4.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnAddress4.Text = " ";
			this.fldReturnAddress4.Top = 0.75F;
			this.fldReturnAddress4.Visible = false;
			this.fldReturnAddress4.Width = 2.375F;
			// 
			// fldBookSequence
			// 
			this.fldBookSequence.Height = 0.1875F;
			this.fldBookSequence.Left = 1.0625F;
			this.fldBookSequence.Name = "fldBookSequence";
			this.fldBookSequence.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldBookSequence.Text = null;
			this.fldBookSequence.Top = 1.625F;
			this.fldBookSequence.Width = 1F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.25F;
			this.Field1.Left = 2.25F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 14.5pt; text-align: center; ddo-char-set: 0";
			this.Field1.Text = "DISCONNECTION NOTICE";
			this.Field1.Top = 1.5625F;
			this.Field1.Width = 3.375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 5.875F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.Field2.Text = " ";
			this.Field2.Top = 1.625F;
			this.Field2.Width = 0.5F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 6.4375F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAccountNumber.Text = " ";
			this.fldAccountNumber.Top = 1.625F;
			this.fldAccountNumber.Width = 0.8125F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 1.9375F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 0";
			this.Field3.Text = "YOUR BILL IS OVERDUE, PLEASE PAY PROMPTLY ";
			this.Field3.Top = 1.8125F;
			this.Field3.Width = 4F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.9375F;
			this.Line1.Width = 0.75F;
			this.Line1.X1 = 1.875F;
			this.Line1.X2 = 1.125F;
			this.Line1.Y1 = 1.9375F;
			this.Line1.Y2 = 1.9375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 5.875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 1.9375F;
			this.Line2.Width = 0.75F;
			this.Line2.X1 = 6.625F;
			this.Line2.X2 = 5.875F;
			this.Line2.Y1 = 1.9375F;
			this.Line2.Y2 = 1.9375F;
			// 
			// lblText
			// 
			this.lblText.Height = 1.3125F;
			this.lblText.Left = 0.75F;
			this.lblText.Name = "lblText";
			this.lblText.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.lblText.Text = null;
			this.lblText.Top = 2.0625F;
			this.lblText.Width = 6F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 3.4375F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.Field4.Text = "Notice Date:";
			this.Field4.Top = 0.25F;
			this.Field4.Width = 1.1875F;
			// 
			// fldNoticeDate
			// 
			this.fldNoticeDate.Height = 0.1875F;
			this.fldNoticeDate.Left = 4.625F;
			this.fldNoticeDate.Name = "fldNoticeDate";
			this.fldNoticeDate.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldNoticeDate.Text = " ";
			this.fldNoticeDate.Top = 0.25F;
			this.fldNoticeDate.Width = 0.9375F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 3.4375F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.Field5.Text = "Shut Off Date:";
			this.Field5.Top = 0.5625F;
			this.Field5.Width = 1.1875F;
			// 
			// fldShutOffDate
			// 
			this.fldShutOffDate.Height = 0.1875F;
			this.fldShutOffDate.Left = 4.625F;
			this.fldShutOffDate.Name = "fldShutOffDate";
			this.fldShutOffDate.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldShutOffDate.Text = " ";
			this.fldShutOffDate.Top = 0.5625F;
			this.fldShutOffDate.Width = 0.9375F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 5.5625F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.Field6.Text = "Water Due:";
			this.Field6.Top = 0.25F;
			this.Field6.Width = 1F;
			// 
			// fldWaterTotal
			// 
			this.fldWaterTotal.Height = 0.1875F;
			this.fldWaterTotal.Left = 6.5625F;
			this.fldWaterTotal.Name = "fldWaterTotal";
			this.fldWaterTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; ddo-char-set: 0";
			this.fldWaterTotal.Text = " ";
			this.fldWaterTotal.Top = 0.25F;
			this.fldWaterTotal.Width = 0.875F;
			// 
			// fldSewerTotalLabel
			// 
			this.fldSewerTotalLabel.Height = 0.1875F;
			this.fldSewerTotalLabel.Left = 5.5625F;
			this.fldSewerTotalLabel.Name = "fldSewerTotalLabel";
			this.fldSewerTotalLabel.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldSewerTotalLabel.Text = "Sewer Due:";
			this.fldSewerTotalLabel.Top = 0.4375F;
			this.fldSewerTotalLabel.Width = 1F;
			// 
			// fldSewerTotal
			// 
			this.fldSewerTotal.Height = 0.1875F;
			this.fldSewerTotal.Left = 6.5625F;
			this.fldSewerTotal.Name = "fldSewerTotal";
			this.fldSewerTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; ddo-char-set: 0";
			this.fldSewerTotal.Text = " ";
			this.fldSewerTotal.Top = 0.4375F;
			this.fldSewerTotal.Width = 0.875F;
			// 
			// fldTotalTotalLabel
			// 
			this.fldTotalTotalLabel.Height = 0.1875F;
			this.fldTotalTotalLabel.Left = 5.5625F;
			this.fldTotalTotalLabel.Name = "fldTotalTotalLabel";
			this.fldTotalTotalLabel.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldTotalTotalLabel.Text = "Total Due:";
			this.fldTotalTotalLabel.Top = 0.6875F;
			this.fldTotalTotalLabel.Width = 1F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 6.5625F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; ddo-char-set: 0";
			this.fldTotalTotal.Text = " ";
			this.fldTotalTotal.Top = 0.6875F;
			this.fldTotalTotal.Width = 0.875F;
			// 
			// lblAsterisk
			// 
			this.lblAsterisk.Height = 0.1875F;
			this.lblAsterisk.HyperLink = null;
			this.lblAsterisk.Left = 7.375F;
			this.lblAsterisk.Name = "lblAsterisk";
			this.lblAsterisk.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.lblAsterisk.Text = "*";
			this.lblAsterisk.Top = 0.25F;
			this.lblAsterisk.Width = 0.125F;
			// 
			// fldSeperateSewerLabel
			// 
			this.fldSeperateSewerLabel.Height = 0.1875F;
			this.fldSeperateSewerLabel.Left = 3.75F;
			this.fldSeperateSewerLabel.Name = "fldSeperateSewerLabel";
			this.fldSeperateSewerLabel.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left; ddo-char-set: 0";
			this.fldSeperateSewerLabel.Text = "DOES NOT INCLUDE SEWER CHARGES OF";
			this.fldSeperateSewerLabel.Top = 1F;
			this.fldSeperateSewerLabel.Width = 2.75F;
			// 
			// lblAsterisk2
			// 
			this.lblAsterisk2.Height = 0.1875F;
			this.lblAsterisk2.HyperLink = null;
			this.lblAsterisk2.Left = 3.5625F;
			this.lblAsterisk2.Name = "lblAsterisk2";
			this.lblAsterisk2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.lblAsterisk2.Text = "*";
			this.lblAsterisk2.Top = 1F;
			this.lblAsterisk2.Width = 0.1875F;
			// 
			// fldSeperateSewer
			// 
			this.fldSeperateSewer.Height = 0.1875F;
			this.fldSeperateSewer.Left = 6.5F;
			this.fldSeperateSewer.Name = "fldSeperateSewer";
			this.fldSeperateSewer.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left; ddo-char-set: 0";
			this.fldSeperateSewer.Text = " ";
			this.fldSeperateSewer.Top = 1F;
			this.fldSeperateSewer.Width = 0.875F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.375F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldName.Text = " ";
			this.fldName.Top = 0.375F;
			this.fldName.Width = 3F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 0.375F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress1.Text = " ";
			this.fldAddress1.Top = 0.5625F;
			this.fldAddress1.Width = 3F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 0.375F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress2.Text = " ";
			this.fldAddress2.Top = 0.75F;
			this.fldAddress2.Width = 3F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 0.375F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress3.Text = " ";
			this.fldAddress3.Top = 0.9375F;
			this.fldAddress3.Width = 3F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 0.375F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress4.Text = " ";
			this.fldAddress4.Top = 1.125F;
			this.fldAddress4.Width = 3F;
			// 
			// linTotalLine
			// 
			this.linTotalLine.Height = 0F;
			this.linTotalLine.Left = 5.5625F;
			this.linTotalLine.LineWeight = 1F;
			this.linTotalLine.Name = "linTotalLine";
			this.linTotalLine.Top = 0.6875F;
			this.linTotalLine.Width = 1.875F;
			this.linTotalLine.X1 = 5.5625F;
			this.linTotalLine.X2 = 7.4375F;
			this.linTotalLine.Y1 = 0.6875F;
			this.linTotalLine.Y2 = 0.6875F;
			// 
			// fldPUCText1
			// 
			this.fldPUCText1.Height = 0.1875F;
			this.fldPUCText1.Left = 0.375F;
			this.fldPUCText1.Name = "fldPUCText1";
			this.fldPUCText1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
			this.fldPUCText1.Text = null;
			this.fldPUCText1.Top = 4.25F;
			this.fldPUCText1.Width = 6.8125F;
			// 
			// fldPUCText2
			// 
			this.fldPUCText2.Height = 0.875F;
			this.fldPUCText2.Left = 0.625F;
			this.fldPUCText2.Name = "fldPUCText2";
			this.fldPUCText2.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.fldPUCText2.Text = null;
			this.fldPUCText2.Top = 4.4375F;
			this.fldPUCText2.Width = 6.5625F;
			// 
			// fldPUCText7
			// 
			this.fldPUCText7.Height = 0.25F;
			this.fldPUCText7.Left = 0.375F;
			this.fldPUCText7.Name = "fldPUCText7";
			this.fldPUCText7.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.fldPUCText7.Text = null;
			this.fldPUCText7.Top = 7.5625F;
			this.fldPUCText7.Width = 6.8125F;
			// 
			// fldPUCText3
			// 
			this.fldPUCText3.Height = 0.1875F;
			this.fldPUCText3.Left = 0.375F;
			this.fldPUCText3.Name = "fldPUCText3";
			this.fldPUCText3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
			this.fldPUCText3.Text = null;
			this.fldPUCText3.Top = 5.375F;
			this.fldPUCText3.Width = 6.8125F;
			// 
			// fldPUCText4
			// 
			this.fldPUCText4.Height = 0.8125F;
			this.fldPUCText4.Left = 0.375F;
			this.fldPUCText4.Name = "fldPUCText4";
			this.fldPUCText4.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.fldPUCText4.Text = null;
			this.fldPUCText4.Top = 5.5625F;
			this.fldPUCText4.Width = 6.8125F;
			// 
			// fldPUCText5
			// 
			this.fldPUCText5.Height = 0.1875F;
			this.fldPUCText5.Left = 0.375F;
			this.fldPUCText5.Name = "fldPUCText5";
			this.fldPUCText5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
			this.fldPUCText5.Text = null;
			this.fldPUCText5.Top = 6.4375F;
			this.fldPUCText5.Width = 6.8125F;
			// 
			// fldPUCText6
			// 
			this.fldPUCText6.Height = 0.8125F;
			this.fldPUCText6.Left = 0.375F;
			this.fldPUCText6.Name = "fldPUCText6";
			this.fldPUCText6.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.fldPUCText6.Text = null;
			this.fldPUCText6.Top = 6.625F;
			this.fldPUCText6.Width = 6.8125F;
			// 
			// fldPUCText8
			// 
			this.fldPUCText8.Height = 1.125F;
			this.fldPUCText8.Left = 4.1875F;
			this.fldPUCText8.Name = "fldPUCText8";
			this.fldPUCText8.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 0";
			this.fldPUCText8.Text = null;
			this.fldPUCText8.Top = 9.0625F;
			this.fldPUCText8.Width = 2.8125F;
			// 
			// fldBName
			// 
			this.fldBName.Height = 0.1875F;
			this.fldBName.Left = 0.625F;
			this.fldBName.Name = "fldBName";
			this.fldBName.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldBName.Text = " ";
			this.fldBName.Top = 9.0625F;
			this.fldBName.Width = 3F;
			// 
			// fldBAddress1
			// 
			this.fldBAddress1.Height = 0.1875F;
			this.fldBAddress1.Left = 0.625F;
			this.fldBAddress1.Name = "fldBAddress1";
			this.fldBAddress1.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldBAddress1.Text = " ";
			this.fldBAddress1.Top = 9.25F;
			this.fldBAddress1.Width = 3F;
			// 
			// fldBAddress2
			// 
			this.fldBAddress2.Height = 0.1875F;
			this.fldBAddress2.Left = 0.625F;
			this.fldBAddress2.Name = "fldBAddress2";
			this.fldBAddress2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldBAddress2.Text = " ";
			this.fldBAddress2.Top = 9.4375F;
			this.fldBAddress2.Width = 3F;
			// 
			// fldBAddress3
			// 
			this.fldBAddress3.Height = 0.1875F;
			this.fldBAddress3.Left = 0.625F;
			this.fldBAddress3.Name = "fldBAddress3";
			this.fldBAddress3.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldBAddress3.Text = " ";
			this.fldBAddress3.Top = 9.625F;
			this.fldBAddress3.Width = 3F;
			// 
			// fldBAddress4
			// 
			this.fldBAddress4.Height = 0.1875F;
			this.fldBAddress4.Left = 0.625F;
			this.fldBAddress4.MultiLine = false;
			this.fldBAddress4.Name = "fldBAddress4";
			this.fldBAddress4.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldBAddress4.Text = " ";
			this.fldBAddress4.Top = 9.8125F;
			this.fldBAddress4.Width = 3F;
			// 
			// fldAccount2
			// 
			this.fldAccount2.Height = 0.1875F;
			this.fldAccount2.Left = 0.625F;
			this.fldAccount2.Name = "fldAccount2";
			this.fldAccount2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAccount2.Text = " ";
			this.fldAccount2.Top = 8.875F;
			this.fldAccount2.Width = 2.0625F;
			// 
			// fldDefaultText
			// 
			this.fldDefaultText.Height = 0.8125F;
			this.fldDefaultText.Left = 0.75F;
			this.fldDefaultText.Name = "fldDefaultText";
			this.fldDefaultText.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.fldDefaultText.Text = null;
			this.fldDefaultText.Top = 3.375F;
			this.fldDefaultText.Width = 6F;
			// 
			// fldAddress5
			// 
			this.fldAddress5.Height = 0.1875F;
			this.fldAddress5.Left = 0.375F;
			this.fldAddress5.Name = "fldAddress5";
			this.fldAddress5.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress5.Text = " ";
			this.fldAddress5.Top = 1.3125F;
			this.fldAddress5.Visible = false;
			this.fldAddress5.Width = 3F;
			// 
			// fldBAddress5
			// 
			this.fldBAddress5.Height = 0.1875F;
			this.fldBAddress5.Left = 0.625F;
			this.fldBAddress5.MultiLine = false;
			this.fldBAddress5.Name = "fldBAddress5";
			this.fldBAddress5.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldBAddress5.Text = " ";
			this.fldBAddress5.Top = 10F;
			this.fldBAddress5.Width = 3F;
			// 
			// fldFinAidText
			// 
			this.fldFinAidText.Height = 0.8125F;
			this.fldFinAidText.Left = 0.375F;
			this.fldFinAidText.Name = "fldFinAidText";
			this.fldFinAidText.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.fldFinAidText.Text = null;
			this.fldFinAidText.Top = 7.9375F;
			this.fldFinAidText.Width = 6.8125F;
			// 
			// rptDisconnectNoticeOriginalPlainPaper2
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.fldReturnName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoticeDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldShutOffDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTotalLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewerLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPUCText8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDefaultText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBAddress5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinAidText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoticeDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldShutOffDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerTotalLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotalLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAsterisk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeperateSewerLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAsterisk2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeperateSewer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Line linTotalLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPUCText1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPUCText2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPUCText7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPUCText3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPUCText4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPUCText5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPUCText6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPUCText8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDefaultText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBAddress5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinAidText;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
