﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTPmtActivity.
	/// </summary>
	partial class rptUTPmtActivity
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUTPmtActivity));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldService = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldRcpt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
            this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
            this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lnFooterTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldTotalPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSRegTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSRegPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblSRegTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldSRegInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSRegCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSRegTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSRegPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWRegTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWRegPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblWRegTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldWRegInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWRegCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWRegTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWRegPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSLienTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSLienPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblSLienTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldSLienInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSLienCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSLienTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSLienPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWLienTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWLienPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblWLienTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldWLienInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWLienCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWLienTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldWLienPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.fldHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldHeaderPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldHeaderInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldHeaderCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldHeaderTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblHeaderDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblHeaderAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblHeaderBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblHeaderCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lblHeaderSrvc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldHeaderPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldHeaderRef = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.lnHeadTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.fldHeaderRcpt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRcpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSRegTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWRegTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSLienTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienPrincipal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWLienTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderTotal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderPrin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderInt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderTax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderAcct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderSrvc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderPLI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderRef)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderRcpt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldDate,
            this.fldCode,
            this.fldRef,
            this.fldPrincipal,
            this.fldTotal,
            this.fldInterest,
            this.fldCost,
            this.fldTax,
            this.fldAcct,
            this.fldBill,
            this.fldService,
            this.fldPLI,
            this.fldRcpt});
            this.Detail.Height = 0.1875F;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldDate
            // 
            this.fldDate.CanGrow = false;
            this.fldDate.Height = 0.1875F;
            this.fldDate.Left = 0F;
            this.fldDate.Name = "fldDate";
            this.fldDate.Style = "font-family: \'Tahoma\'";
            this.fldDate.Text = null;
            this.fldDate.Top = 0F;
            this.fldDate.Width = 0.8125F;
            // 
            // fldCode
            // 
            this.fldCode.CanGrow = false;
            this.fldCode.Height = 0.1875F;
            this.fldCode.Left = 3.1875F;
            this.fldCode.Name = "fldCode";
            this.fldCode.Style = "font-family: \'Tahoma\'";
            this.fldCode.Text = null;
            this.fldCode.Top = 0F;
            this.fldCode.Width = 0.1875F;
            // 
            // fldRef
            // 
            this.fldRef.CanGrow = false;
            this.fldRef.Height = 0.1875F;
            this.fldRef.Left = 2.5F;
            this.fldRef.Name = "fldRef";
            this.fldRef.Style = "font-family: \'Tahoma\'";
            this.fldRef.Text = null;
            this.fldRef.Top = 0F;
            this.fldRef.Width = 0.625F;
            // 
            // fldPrincipal
            // 
            this.fldPrincipal.CanGrow = false;
            this.fldPrincipal.Height = 0.1875F;
            this.fldPrincipal.Left = 4.125F;
            this.fldPrincipal.Name = "fldPrincipal";
            this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPrincipal.Text = null;
            this.fldPrincipal.Top = 0F;
            this.fldPrincipal.Width = 0.8125F;
            // 
            // fldTotal
            // 
            this.fldTotal.CanGrow = false;
            this.fldTotal.Height = 0.1875F;
            this.fldTotal.Left = 8.5F;
            this.fldTotal.Name = "fldTotal";
            this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotal.Text = null;
            this.fldTotal.Top = 0F;
            this.fldTotal.Width = 1F;
            // 
            // fldInterest
            // 
            this.fldInterest.CanGrow = false;
            this.fldInterest.Height = 0.1875F;
            this.fldInterest.Left = 5.875F;
            this.fldInterest.Name = "fldInterest";
            this.fldInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldInterest.Text = null;
            this.fldInterest.Top = 0F;
            this.fldInterest.Width = 0.8125F;
            // 
            // fldCost
            // 
            this.fldCost.CanGrow = false;
            this.fldCost.Height = 0.1875F;
            this.fldCost.Left = 7.625F;
            this.fldCost.Name = "fldCost";
            this.fldCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldCost.Text = null;
            this.fldCost.Top = 0F;
            this.fldCost.Width = 0.8125F;
            // 
            // fldTax
            // 
            this.fldTax.CanGrow = false;
            this.fldTax.Height = 0.1875F;
            this.fldTax.Left = 5F;
            this.fldTax.Name = "fldTax";
            this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTax.Text = null;
            this.fldTax.Top = 0F;
            this.fldTax.Width = 0.8125F;
            // 
            // fldAcct
            // 
            this.fldAcct.CanGrow = false;
            this.fldAcct.Height = 0.1875F;
            this.fldAcct.Left = 0.875F;
            this.fldAcct.Name = "fldAcct";
            this.fldAcct.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldAcct.Text = null;
            this.fldAcct.Top = 0F;
            this.fldAcct.Width = 0.8125F;
            // 
            // fldBill
            // 
            this.fldBill.CanGrow = false;
            this.fldBill.Height = 0.1875F;
            this.fldBill.Left = 1.6875F;
            this.fldBill.Name = "fldBill";
            this.fldBill.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldBill.Text = null;
            this.fldBill.Top = 0F;
            this.fldBill.Width = 0.4375F;
            // 
            // fldService
            // 
            this.fldService.CanGrow = false;
            this.fldService.Height = 0.1875F;
            this.fldService.Left = 2.1875F;
            this.fldService.Name = "fldService";
            this.fldService.Style = "font-family: \'Tahoma\'";
            this.fldService.Text = null;
            this.fldService.Top = 0F;
            this.fldService.Width = 0.1875F;
            // 
            // fldPLI
            // 
            this.fldPLI.CanGrow = false;
            this.fldPLI.Height = 0.1875F;
            this.fldPLI.Left = 6.75F;
            this.fldPLI.Name = "fldPLI";
            this.fldPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldPLI.Text = null;
            this.fldPLI.Top = 0F;
            this.fldPLI.Width = 0.8125F;
            // 
            // fldRcpt
            // 
            this.fldRcpt.CanGrow = false;
            this.fldRcpt.Height = 0.1875F;
            this.fldRcpt.Left = 3.4375F;
            this.fldRcpt.Name = "fldRcpt";
            this.fldRcpt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldRcpt.Text = null;
            this.fldRcpt.Top = 0F;
            this.fldRcpt.Width = 0.625F;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Height = 0F;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.Visible = false;
            // 
            // ReportFooter
            // 
            this.ReportFooter.CanShrink = true;
            this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotalTotal,
            this.fldTotalPrincipal,
            this.lblFooter,
            this.lnFooterTotal,
            this.fldTotalInterest,
            this.fldTotalCost,
            this.fldTotalTax,
            this.fldTotalPLI,
            this.fldSRegTotal,
            this.fldSRegPrincipal,
            this.lblSRegTotal,
            this.fldSRegInterest,
            this.fldSRegCost,
            this.fldSRegTax,
            this.fldSRegPLI,
            this.fldWRegTotal,
            this.fldWRegPrincipal,
            this.lblWRegTotal,
            this.fldWRegInterest,
            this.fldWRegCost,
            this.fldWRegTax,
            this.fldWRegPLI,
            this.fldSLienTotal,
            this.fldSLienPrincipal,
            this.lblSLienTotal,
            this.fldSLienInterest,
            this.fldSLienCost,
            this.fldSLienTax,
            this.fldSLienPLI,
            this.fldWLienTotal,
            this.fldWLienPrincipal,
            this.lblWLienTotal,
            this.fldWLienInterest,
            this.fldWLienCost,
            this.fldWLienTax,
            this.fldWLienPLI});
            this.ReportFooter.Height = 1.552083F;
            this.ReportFooter.Name = "ReportFooter";
            this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
            // 
            // fldTotalTotal
            // 
            this.fldTotalTotal.Height = 0.1875F;
            this.fldTotalTotal.Left = 8.5F;
            this.fldTotalTotal.Name = "fldTotalTotal";
            this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTotal.Text = null;
            this.fldTotalTotal.Top = 0.0625F;
            this.fldTotalTotal.Width = 1F;
            // 
            // fldTotalPrincipal
            // 
            this.fldTotalPrincipal.Height = 0.1875F;
            this.fldTotalPrincipal.Left = 4.0625F;
            this.fldTotalPrincipal.Name = "fldTotalPrincipal";
            this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPrincipal.Text = null;
            this.fldTotalPrincipal.Top = 0.0625F;
            this.fldTotalPrincipal.Width = 0.875F;
            // 
            // lblFooter
            // 
            this.lblFooter.Height = 0.1875F;
            this.lblFooter.HyperLink = null;
            this.lblFooter.Left = 2.5F;
            this.lblFooter.Name = "lblFooter";
            this.lblFooter.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblFooter.Text = "Total";
            this.lblFooter.Top = 0.0625F;
            this.lblFooter.Width = 1.4375F;
            // 
            // lnFooterTotal
            // 
            this.lnFooterTotal.Height = 0F;
            this.lnFooterTotal.Left = 3.4375F;
            this.lnFooterTotal.LineWeight = 1F;
            this.lnFooterTotal.Name = "lnFooterTotal";
            this.lnFooterTotal.Top = 0F;
            this.lnFooterTotal.Width = 6F;
            this.lnFooterTotal.X1 = 3.4375F;
            this.lnFooterTotal.X2 = 9.4375F;
            this.lnFooterTotal.Y1 = 0F;
            this.lnFooterTotal.Y2 = 0F;
            // 
            // fldTotalInterest
            // 
            this.fldTotalInterest.Height = 0.1875F;
            this.fldTotalInterest.Left = 5.8125F;
            this.fldTotalInterest.Name = "fldTotalInterest";
            this.fldTotalInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalInterest.Text = null;
            this.fldTotalInterest.Top = 0.0625F;
            this.fldTotalInterest.Width = 0.875F;
            // 
            // fldTotalCost
            // 
            this.fldTotalCost.Height = 0.1875F;
            this.fldTotalCost.Left = 7.5625F;
            this.fldTotalCost.Name = "fldTotalCost";
            this.fldTotalCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalCost.Text = null;
            this.fldTotalCost.Top = 0.0625F;
            this.fldTotalCost.Width = 0.875F;
            // 
            // fldTotalTax
            // 
            this.fldTotalTax.Height = 0.1875F;
            this.fldTotalTax.Left = 4.9375F;
            this.fldTotalTax.Name = "fldTotalTax";
            this.fldTotalTax.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalTax.Text = null;
            this.fldTotalTax.Top = 0.0625F;
            this.fldTotalTax.Width = 0.875F;
            // 
            // fldTotalPLI
            // 
            this.fldTotalPLI.Height = 0.1875F;
            this.fldTotalPLI.Left = 6.6875F;
            this.fldTotalPLI.Name = "fldTotalPLI";
            this.fldTotalPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldTotalPLI.Text = null;
            this.fldTotalPLI.Top = 0.0625F;
            this.fldTotalPLI.Width = 0.875F;
            // 
            // fldSRegTotal
            // 
            this.fldSRegTotal.Height = 0.1875F;
            this.fldSRegTotal.Left = 8.5F;
            this.fldSRegTotal.Name = "fldSRegTotal";
            this.fldSRegTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSRegTotal.Text = null;
            this.fldSRegTotal.Top = 0.5625F;
            this.fldSRegTotal.Width = 1F;
            // 
            // fldSRegPrincipal
            // 
            this.fldSRegPrincipal.Height = 0.1875F;
            this.fldSRegPrincipal.Left = 4.0625F;
            this.fldSRegPrincipal.Name = "fldSRegPrincipal";
            this.fldSRegPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSRegPrincipal.Text = null;
            this.fldSRegPrincipal.Top = 0.5625F;
            this.fldSRegPrincipal.Width = 0.875F;
            // 
            // lblSRegTotal
            // 
            this.lblSRegTotal.Height = 0.1875F;
            this.lblSRegTotal.HyperLink = null;
            this.lblSRegTotal.Left = 2.5F;
            this.lblSRegTotal.Name = "lblSRegTotal";
            this.lblSRegTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblSRegTotal.Text = "Sewer Bill Total";
            this.lblSRegTotal.Top = 0.5625F;
            this.lblSRegTotal.Width = 1.4375F;
            // 
            // fldSRegInterest
            // 
            this.fldSRegInterest.Height = 0.1875F;
            this.fldSRegInterest.Left = 5.8125F;
            this.fldSRegInterest.Name = "fldSRegInterest";
            this.fldSRegInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSRegInterest.Text = null;
            this.fldSRegInterest.Top = 0.5625F;
            this.fldSRegInterest.Width = 0.875F;
            // 
            // fldSRegCost
            // 
            this.fldSRegCost.Height = 0.1875F;
            this.fldSRegCost.Left = 7.5625F;
            this.fldSRegCost.Name = "fldSRegCost";
            this.fldSRegCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSRegCost.Text = null;
            this.fldSRegCost.Top = 0.5625F;
            this.fldSRegCost.Width = 0.875F;
            // 
            // fldSRegTax
            // 
            this.fldSRegTax.Height = 0.1875F;
            this.fldSRegTax.Left = 4.9375F;
            this.fldSRegTax.Name = "fldSRegTax";
            this.fldSRegTax.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSRegTax.Text = null;
            this.fldSRegTax.Top = 0.5625F;
            this.fldSRegTax.Width = 0.875F;
            // 
            // fldSRegPLI
            // 
            this.fldSRegPLI.Height = 0.1875F;
            this.fldSRegPLI.Left = 6.6875F;
            this.fldSRegPLI.Name = "fldSRegPLI";
            this.fldSRegPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSRegPLI.Text = null;
            this.fldSRegPLI.Top = 0.5625F;
            this.fldSRegPLI.Width = 0.875F;
            // 
            // fldWRegTotal
            // 
            this.fldWRegTotal.Height = 0.1875F;
            this.fldWRegTotal.Left = 8.5F;
            this.fldWRegTotal.Name = "fldWRegTotal";
            this.fldWRegTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWRegTotal.Text = null;
            this.fldWRegTotal.Top = 0.8125F;
            this.fldWRegTotal.Width = 1F;
            // 
            // fldWRegPrincipal
            // 
            this.fldWRegPrincipal.Height = 0.1875F;
            this.fldWRegPrincipal.Left = 4.0625F;
            this.fldWRegPrincipal.Name = "fldWRegPrincipal";
            this.fldWRegPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWRegPrincipal.Text = null;
            this.fldWRegPrincipal.Top = 0.8125F;
            this.fldWRegPrincipal.Width = 0.875F;
            // 
            // lblWRegTotal
            // 
            this.lblWRegTotal.Height = 0.1875F;
            this.lblWRegTotal.HyperLink = null;
            this.lblWRegTotal.Left = 2.5F;
            this.lblWRegTotal.Name = "lblWRegTotal";
            this.lblWRegTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblWRegTotal.Text = "Water Bill Total";
            this.lblWRegTotal.Top = 0.8125F;
            this.lblWRegTotal.Width = 1.4375F;
            // 
            // fldWRegInterest
            // 
            this.fldWRegInterest.Height = 0.1875F;
            this.fldWRegInterest.Left = 5.8125F;
            this.fldWRegInterest.Name = "fldWRegInterest";
            this.fldWRegInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWRegInterest.Text = null;
            this.fldWRegInterest.Top = 0.8125F;
            this.fldWRegInterest.Width = 0.875F;
            // 
            // fldWRegCost
            // 
            this.fldWRegCost.Height = 0.1875F;
            this.fldWRegCost.Left = 7.5625F;
            this.fldWRegCost.Name = "fldWRegCost";
            this.fldWRegCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWRegCost.Text = null;
            this.fldWRegCost.Top = 0.8125F;
            this.fldWRegCost.Width = 0.875F;
            // 
            // fldWRegTax
            // 
            this.fldWRegTax.Height = 0.1875F;
            this.fldWRegTax.Left = 4.9375F;
            this.fldWRegTax.Name = "fldWRegTax";
            this.fldWRegTax.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWRegTax.Text = null;
            this.fldWRegTax.Top = 0.8125F;
            this.fldWRegTax.Width = 0.875F;
            // 
            // fldWRegPLI
            // 
            this.fldWRegPLI.Height = 0.1875F;
            this.fldWRegPLI.Left = 6.6875F;
            this.fldWRegPLI.Name = "fldWRegPLI";
            this.fldWRegPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWRegPLI.Text = null;
            this.fldWRegPLI.Top = 0.8125F;
            this.fldWRegPLI.Width = 0.875F;
            // 
            // fldSLienTotal
            // 
            this.fldSLienTotal.Height = 0.1875F;
            this.fldSLienTotal.Left = 8.5F;
            this.fldSLienTotal.Name = "fldSLienTotal";
            this.fldSLienTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSLienTotal.Text = null;
            this.fldSLienTotal.Top = 1.0625F;
            this.fldSLienTotal.Width = 1F;
            // 
            // fldSLienPrincipal
            // 
            this.fldSLienPrincipal.Height = 0.1875F;
            this.fldSLienPrincipal.Left = 4.0625F;
            this.fldSLienPrincipal.Name = "fldSLienPrincipal";
            this.fldSLienPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSLienPrincipal.Text = null;
            this.fldSLienPrincipal.Top = 1.0625F;
            this.fldSLienPrincipal.Width = 0.875F;
            // 
            // lblSLienTotal
            // 
            this.lblSLienTotal.Height = 0.1875F;
            this.lblSLienTotal.HyperLink = null;
            this.lblSLienTotal.Left = 2.5F;
            this.lblSLienTotal.Name = "lblSLienTotal";
            this.lblSLienTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblSLienTotal.Text = "Sewer Lien Total";
            this.lblSLienTotal.Top = 1.0625F;
            this.lblSLienTotal.Width = 1.4375F;
            // 
            // fldSLienInterest
            // 
            this.fldSLienInterest.Height = 0.1875F;
            this.fldSLienInterest.Left = 5.8125F;
            this.fldSLienInterest.Name = "fldSLienInterest";
            this.fldSLienInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSLienInterest.Text = null;
            this.fldSLienInterest.Top = 1.0625F;
            this.fldSLienInterest.Width = 0.875F;
            // 
            // fldSLienCost
            // 
            this.fldSLienCost.Height = 0.1875F;
            this.fldSLienCost.Left = 7.5625F;
            this.fldSLienCost.Name = "fldSLienCost";
            this.fldSLienCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSLienCost.Text = null;
            this.fldSLienCost.Top = 1.0625F;
            this.fldSLienCost.Width = 0.875F;
            // 
            // fldSLienTax
            // 
            this.fldSLienTax.Height = 0.1875F;
            this.fldSLienTax.Left = 4.9375F;
            this.fldSLienTax.Name = "fldSLienTax";
            this.fldSLienTax.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSLienTax.Text = null;
            this.fldSLienTax.Top = 1.0625F;
            this.fldSLienTax.Width = 0.875F;
            // 
            // fldSLienPLI
            // 
            this.fldSLienPLI.Height = 0.1875F;
            this.fldSLienPLI.Left = 6.6875F;
            this.fldSLienPLI.Name = "fldSLienPLI";
            this.fldSLienPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldSLienPLI.Text = null;
            this.fldSLienPLI.Top = 1.0625F;
            this.fldSLienPLI.Width = 0.875F;
            // 
            // fldWLienTotal
            // 
            this.fldWLienTotal.Height = 0.1875F;
            this.fldWLienTotal.Left = 8.5F;
            this.fldWLienTotal.Name = "fldWLienTotal";
            this.fldWLienTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWLienTotal.Text = null;
            this.fldWLienTotal.Top = 1.3125F;
            this.fldWLienTotal.Width = 1F;
            // 
            // fldWLienPrincipal
            // 
            this.fldWLienPrincipal.Height = 0.1875F;
            this.fldWLienPrincipal.Left = 4.0625F;
            this.fldWLienPrincipal.Name = "fldWLienPrincipal";
            this.fldWLienPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWLienPrincipal.Text = null;
            this.fldWLienPrincipal.Top = 1.3125F;
            this.fldWLienPrincipal.Width = 0.875F;
            // 
            // lblWLienTotal
            // 
            this.lblWLienTotal.Height = 0.1875F;
            this.lblWLienTotal.HyperLink = null;
            this.lblWLienTotal.Left = 2.5F;
            this.lblWLienTotal.Name = "lblWLienTotal";
            this.lblWLienTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblWLienTotal.Text = "Water Lien Total";
            this.lblWLienTotal.Top = 1.3125F;
            this.lblWLienTotal.Width = 1.4375F;
            // 
            // fldWLienInterest
            // 
            this.fldWLienInterest.Height = 0.1875F;
            this.fldWLienInterest.Left = 5.8125F;
            this.fldWLienInterest.Name = "fldWLienInterest";
            this.fldWLienInterest.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWLienInterest.Text = null;
            this.fldWLienInterest.Top = 1.3125F;
            this.fldWLienInterest.Width = 0.875F;
            // 
            // fldWLienCost
            // 
            this.fldWLienCost.Height = 0.1875F;
            this.fldWLienCost.Left = 7.5625F;
            this.fldWLienCost.Name = "fldWLienCost";
            this.fldWLienCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWLienCost.Text = null;
            this.fldWLienCost.Top = 1.3125F;
            this.fldWLienCost.Width = 0.875F;
            // 
            // fldWLienTax
            // 
            this.fldWLienTax.Height = 0.1875F;
            this.fldWLienTax.Left = 4.9375F;
            this.fldWLienTax.Name = "fldWLienTax";
            this.fldWLienTax.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWLienTax.Text = null;
            this.fldWLienTax.Top = 1.3125F;
            this.fldWLienTax.Width = 0.875F;
            // 
            // fldWLienPLI
            // 
            this.fldWLienPLI.Height = 0.1875F;
            this.fldWLienPLI.Left = 6.6875F;
            this.fldWLienPLI.Name = "fldWLienPLI";
            this.fldWLienPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldWLienPLI.Text = null;
            this.fldWLienPLI.Top = 1.3125F;
            this.fldWLienPLI.Width = 0.875F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldHeaderTotal,
            this.fldHeaderPrin,
            this.fldHeaderInt,
            this.fldHeaderCost,
            this.fldHeaderTax,
            this.lblHeaderDate,
            this.lblHeaderAcct,
            this.lblHeaderBill,
            this.lblHeaderCode,
            this.lblHeaderSrvc,
            this.fldHeaderPLI,
            this.fldHeaderRef,
            this.lnHeadTotals,
            this.lblReportType,
            this.lblHeader,
            this.lblTime,
            this.lblMuniName,
            this.lblDate,
            this.lblPage,
            this.fldHeaderRcpt});
            this.PageHeader.Height = 0.84375F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // fldHeaderTotal
            // 
            this.fldHeaderTotal.Height = 0.1875F;
            this.fldHeaderTotal.Left = 8.5F;
            this.fldHeaderTotal.Name = "fldHeaderTotal";
            this.fldHeaderTotal.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldHeaderTotal.Text = "Total";
            this.fldHeaderTotal.Top = 0.625F;
            this.fldHeaderTotal.Width = 1F;
            // 
            // fldHeaderPrin
            // 
            this.fldHeaderPrin.Height = 0.1875F;
            this.fldHeaderPrin.Left = 4.125F;
            this.fldHeaderPrin.Name = "fldHeaderPrin";
            this.fldHeaderPrin.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldHeaderPrin.Text = "Principal";
            this.fldHeaderPrin.Top = 0.625F;
            this.fldHeaderPrin.Width = 0.8125F;
            // 
            // fldHeaderInt
            // 
            this.fldHeaderInt.Height = 0.1875F;
            this.fldHeaderInt.Left = 5.875F;
            this.fldHeaderInt.Name = "fldHeaderInt";
            this.fldHeaderInt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldHeaderInt.Text = "Interest";
            this.fldHeaderInt.Top = 0.625F;
            this.fldHeaderInt.Width = 0.8125F;
            // 
            // fldHeaderCost
            // 
            this.fldHeaderCost.Height = 0.1875F;
            this.fldHeaderCost.Left = 7.625F;
            this.fldHeaderCost.Name = "fldHeaderCost";
            this.fldHeaderCost.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldHeaderCost.Text = "Cost";
            this.fldHeaderCost.Top = 0.625F;
            this.fldHeaderCost.Width = 0.8125F;
            // 
            // fldHeaderTax
            // 
            this.fldHeaderTax.Height = 0.1875F;
            this.fldHeaderTax.Left = 5F;
            this.fldHeaderTax.Name = "fldHeaderTax";
            this.fldHeaderTax.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldHeaderTax.Text = "Tax";
            this.fldHeaderTax.Top = 0.625F;
            this.fldHeaderTax.Width = 0.8125F;
            // 
            // lblHeaderDate
            // 
            this.lblHeaderDate.Height = 0.1875F;
            this.lblHeaderDate.Left = 0F;
            this.lblHeaderDate.Name = "lblHeaderDate";
            this.lblHeaderDate.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblHeaderDate.Text = "Date";
            this.lblHeaderDate.Top = 0.625F;
            this.lblHeaderDate.Width = 0.5625F;
            // 
            // lblHeaderAcct
            // 
            this.lblHeaderAcct.Height = 0.1875F;
            this.lblHeaderAcct.Left = 1F;
            this.lblHeaderAcct.Name = "lblHeaderAcct";
            this.lblHeaderAcct.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblHeaderAcct.Text = "Account";
            this.lblHeaderAcct.Top = 0.625F;
            this.lblHeaderAcct.Width = 0.6875F;
            // 
            // lblHeaderBill
            // 
            this.lblHeaderBill.Height = 0.1875F;
            this.lblHeaderBill.Left = 1.75F;
            this.lblHeaderBill.Name = "lblHeaderBill";
            this.lblHeaderBill.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblHeaderBill.Text = "Bill";
            this.lblHeaderBill.Top = 0.625F;
            this.lblHeaderBill.Width = 0.3125F;
            // 
            // lblHeaderCode
            // 
            this.lblHeaderCode.Height = 0.1875F;
            this.lblHeaderCode.Left = 3.0625F;
            this.lblHeaderCode.Name = "lblHeaderCode";
            this.lblHeaderCode.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblHeaderCode.Text = "Code";
            this.lblHeaderCode.Top = 0.625F;
            this.lblHeaderCode.Width = 0.375F;
            // 
            // lblHeaderSrvc
            // 
            this.lblHeaderSrvc.Height = 0.1875F;
            this.lblHeaderSrvc.Left = 2.125F;
            this.lblHeaderSrvc.Name = "lblHeaderSrvc";
            this.lblHeaderSrvc.Style = "font-family: \'Tahoma\'; text-align: right";
            this.lblHeaderSrvc.Text = "S/W";
            this.lblHeaderSrvc.Top = 0.625F;
            this.lblHeaderSrvc.Width = 0.3125F;
            // 
            // fldHeaderPLI
            // 
            this.fldHeaderPLI.Height = 0.1875F;
            this.fldHeaderPLI.Left = 6.75F;
            this.fldHeaderPLI.Name = "fldHeaderPLI";
            this.fldHeaderPLI.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldHeaderPLI.Text = "PLI";
            this.fldHeaderPLI.Top = 0.625F;
            this.fldHeaderPLI.Width = 0.8125F;
            // 
            // fldHeaderRef
            // 
            this.fldHeaderRef.Height = 0.1875F;
            this.fldHeaderRef.Left = 2.5F;
            this.fldHeaderRef.Name = "fldHeaderRef";
            this.fldHeaderRef.Style = "font-family: \'Tahoma\'; text-align: left";
            this.fldHeaderRef.Text = "Ref";
            this.fldHeaderRef.Top = 0.625F;
            this.fldHeaderRef.Width = 0.5F;
            // 
            // lnHeadTotals
            // 
            this.lnHeadTotals.Height = 0F;
            this.lnHeadTotals.Left = 0F;
            this.lnHeadTotals.LineWeight = 1F;
            this.lnHeadTotals.Name = "lnHeadTotals";
            this.lnHeadTotals.Top = 0.8125F;
            this.lnHeadTotals.Width = 9.5F;
            this.lnHeadTotals.X1 = 0F;
            this.lnHeadTotals.X2 = 9.5F;
            this.lnHeadTotals.Y1 = 0.8125F;
            this.lnHeadTotals.Y2 = 0.8125F;
            // 
            // lblReportType
            // 
            this.lblReportType.Height = 0.375F;
            this.lblReportType.HyperLink = null;
            this.lblReportType.Left = 0F;
            this.lblReportType.Name = "lblReportType";
            this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
            this.lblReportType.Text = "Report Type";
            this.lblReportType.Top = 0.25F;
            this.lblReportType.Width = 9.5F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.25F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
            this.lblHeader.Text = "Payment Activity Report";
            this.lblHeader.Top = 0F;
            this.lblHeader.Width = 9.5F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.1875F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.1875F;
            this.lblTime.Width = 1.25F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 3.5F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 8.5F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1F;
            // 
            // lblPage
            // 
            this.lblPage.Height = 0.1875F;
            this.lblPage.HyperLink = null;
            this.lblPage.Left = 8.5F;
            this.lblPage.Name = "lblPage";
            this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
            this.lblPage.Text = null;
            this.lblPage.Top = 0.1875F;
            this.lblPage.Width = 1F;
            // 
            // fldHeaderRcpt
            // 
            this.fldHeaderRcpt.Height = 0.1875F;
            this.fldHeaderRcpt.Left = 3.5625F;
            this.fldHeaderRcpt.Name = "fldHeaderRcpt";
            this.fldHeaderRcpt.Style = "font-family: \'Tahoma\'; text-align: right";
            this.fldHeaderRcpt.Text = "Receipt";
            this.fldHeaderRcpt.Top = 0.625F;
            this.fldHeaderRcpt.Width = 0.5F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            this.PageFooter.Visible = false;
            // 
            // rptUTPmtActivity
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.5F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 9.510417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.ReportHeader);
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.Sections.Add(this.ReportFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
            ((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldRcpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldTotalPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSRegTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSRegPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWRegTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWRegPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSLienTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSLienPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienPrincipal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblWLienTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldWLienPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderTotal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderPrin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderInt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderTax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderAcct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderSrvc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderPLI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderRef)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldHeaderRcpt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRef;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldService;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRcpt;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnFooterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPLI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSRegTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegPLI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWRegTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegPLI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSLienTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSLienPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSLienTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSLienInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSLienCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSLienTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSLienPLI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWLienTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWLienPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWLienTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWLienInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWLienCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWLienTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWLienPLI;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblHeaderDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblHeaderAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblHeaderBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblHeaderCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblHeaderSrvc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderPLI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderRef;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeadTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldHeaderRcpt;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
