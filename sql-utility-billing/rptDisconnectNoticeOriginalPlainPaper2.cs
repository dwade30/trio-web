﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDisconnectNoticeOriginalPlainPaper2.
	/// </summary>
	public partial class rptDisconnectNoticeOriginalPlainPaper2 : BaseSectionReport
	{
		public rptDisconnectNoticeOriginalPlainPaper2()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Disconnection Notices";
		}

		public static rptDisconnectNoticeOriginalPlainPaper2 InstancePtr
		{
			get
			{
				return (rptDisconnectNoticeOriginalPlainPaper2)Sys.GetInstance(typeof(rptDisconnectNoticeOriginalPlainPaper2));
			}
		}

		protected rptDisconnectNoticeOriginalPlainPaper2 _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDisconnectNoticeOriginalPlainPaper2	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/02/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/28/2006              *
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int intArrayMax;
		// How many books there are in the array
		int[] BookArray = null;
		// Array holding book numbers of selected books
		string strBooksSelection;
		// A - All Books   S - Selected Books
		string strSQL = "";
		// SQL statement to get information we need for report
		bool blnIncSew;
		bool blnSepSew;
		Decimal curMinAmt;
		DateTime datNotice;
		DateTime datShutoff;
		string strPhoneNumber;
		int lngNewestRateKey;
		string strHours;
		string strDefaultText;
		string strMuniName;
		bool boolPrintForOwner;
		double dblReconFeeReg;
		double dblReconFeeAH;
		string strLocGovtContact;
		string strLocGovtPhone;
		bool boolDepositReqd;
		bool boolExcludeNCAccounts;
		// trouts-147
		double dblSewerAmount;
		double dblWaterAmount;
		// vbPorter upgrade warning: lngPassNewestRateKey As int	OnWriteFCConvert.ToDouble(
		public void Init(string strBooks, DateTime datNoticeDate, DateTime datShutOffDate, Decimal curMinimumAmount, bool blnIncludeSewer, bool blnSeperateSewer, string strName, string strAddress1, string strAddress2, string strAddress3, string strAddress4, short intCT, ref int[] PassBookArray, string strPassPhoneNumber, int lngPassNewestRateKey, string strPassHours, string strPassDefaultText, string strPassMuniName, double dblPassReconFeeReg, double dblPassReconFeeAH, string strPassGovtContact, string strPassGovtPhone, bool boolPassDepReqd)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int counter;
				string strRKSQL = "";
				strBooksSelection = strBooks;
				BookArray = PassBookArray;
				intArrayMax = intCT;
				blnIncSew = blnIncludeSewer;
				blnSepSew = blnSeperateSewer;
				curMinAmt = curMinimumAmount;
				datNotice = datNoticeDate;
				datShutoff = datShutOffDate;
				fldReturnName.Text = strName;
				fldReturnAddress1.Text = strAddress1;
				fldReturnAddress2.Text = strAddress2;
				fldReturnAddress3.Text = strAddress3;
				fldReturnAddress4.Text = strAddress4;
				fldReturnName.Visible = true;
				fldReturnAddress1.Visible = true;
				fldReturnAddress2.Visible = true;
				fldReturnAddress3.Visible = true;
				fldReturnAddress4.Visible = true;
				fldName.Visible = false;
				fldAddress1.Visible = false;
				fldAddress2.Visible = false;
				fldAddress3.Visible = false;
				fldAddress4.Visible = false;
				lngNewestRateKey = lngPassNewestRateKey;
				strPhoneNumber = strPassPhoneNumber;
				strHours = strPassHours;
				strDefaultText = strPassDefaultText;
				strMuniName = strPassMuniName;
				// kgk 11-21-2011 trout-758
				dblReconFeeReg = dblPassReconFeeReg;
				dblReconFeeAH = dblPassReconFeeAH;
				strLocGovtContact = strPassGovtContact;
				strLocGovtPhone = strPassGovtPhone;
				boolDepositReqd = boolPassDepReqd;
				if (strBooksSelection == "A")
				{
					strSQL = "";
				}
				else if (strBooksSelection == "B")
				{
					// if we are only reporting on selected books then build the sql string to find the records we want
					if (Information.UBound(BookArray, 1) == 1)
					{
						strSQL = " = " + FCConvert.ToString(BookArray[1]);
					}
					else
					{
						for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
						{
							strSQL += FCConvert.ToString(BookArray[counter]) + ",";
						}
						strSQL = "IN (" + Strings.Left(strSQL, strSQL.Length - 1) + ")";
					}
				}
				else
				{
					strSQL = "= " + frmSetupDisconnectNotices.InstancePtr.txtAccountNumber.Text;
				}
				if (lngNewestRateKey > 0)
				{
					strRKSQL = " AND BillingRateKey <= " + FCConvert.ToString(lngNewestRateKey);
				}
				if (strBooksSelection == "A")
				{
					if (blnIncSew)
					{
						rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " + strRKSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE (WaterTotal + SewerTotal) >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
					}
					else
					{
						rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " + strRKSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE WaterTotal >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
					}
				}
				else if (strBooksSelection == "B")
				{
					if (blnIncSew)
					{
						rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " + strRKSQL + " AND Sequence > " + FCConvert.ToString(Conversion.Val(frmSetupDisconnectEditReport.InstancePtr.txtSequenceNumber.Text)) + " AND MeterTable.BookNumber " + strSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE (WaterTotal + SewerTotal) >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
					}
					else
					{
						rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " + strRKSQL + " AND Sequence > " + FCConvert.ToString(Conversion.Val(frmSetupDisconnectEditReport.InstancePtr.txtSequenceNumber.Text)) + " AND MeterTable.BookNumber " + strSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE WaterTotal >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
					}
				}
				else
				{
					if (blnIncSew)
					{
						rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM ((MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID) " + "WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 AND Master.AccountNumber " + strSQL + strRKSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE (WaterTotal + SewerTotal) >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
					}
					else
					{
						rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM ((MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID) " + "WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 AND Master.AccountNumber " + strSQL + strRKSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE WaterTotal >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
					}
				}
				boolExcludeNCAccounts = FCConvert.CBool(frmSetupDisconnectNotices.InstancePtr.chkExcludeNC.CheckState == Wisej.Web.CheckState.Checked);
				// kk trouts-147  Add option to not send to NC accounts
				// if we found some records show report otherwise pop up message and end report
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						dblWaterAmount = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), true, ref datNotice, lngNewestRateKey), 2);
						dblSewerAmount = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), false, ref datNotice, lngNewestRateKey), 2);
						if (FCConvert.ToDecimal(dblWaterAmount) >= curMinAmt && dblWaterAmount > 0)
						{
							if (boolExcludeNCAccounts)
							{
								// kk09032015 trouts-147
								if (!FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("NoBill")))
								{
									break;
								}
							}
							else
							{
								break;
							}
						}
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
					if (rsInfo.EndOfFile() != true)
					{
						frmReportViewer.InstancePtr.Init(this);
					}
					else
					{
						MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
						this.Close();
					}
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile())
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				else
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				rsInfo.MovePrevious();
			}
			else
			{
				if (!boolPrintForOwner)
				{
					rsInfo.MoveNext();
					if (rsInfo.EndOfFile() != true)
					{
						do
						{
							dblWaterAmount = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), true, ref datNotice, lngNewestRateKey), 2);
							dblSewerAmount = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), false, ref datNotice, lngNewestRateKey), 2);
							if (FCConvert.ToDecimal(dblWaterAmount) >= curMinAmt && dblWaterAmount > 0)
							{
								if (boolExcludeNCAccounts)
								{
									// kk09032015 trouts-147
									if (!FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("NoBill")))
									{
										break;
									}
								}
								else
								{
									break;
								}
							}
							rsInfo.MoveNext();
						}
						while (rsInfo.EndOfFile() != true);
					}
				}
				eArgs.EOF = rsInfo.EndOfFile();
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile())
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				else
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				rsInfo.MovePrevious();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (strBooksSelection == "S")
			{
				frmSetupDisconnectNotices.InstancePtr.Show(App.MainForm);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strText = "";
				clsDRWrapper rsAccountInfo = new clsDRWrapper();
				// MAL@20080715: Move check for minimum amount to be first
				// Tracker Reference: 14569
				rsAccountInfo.OpenRecordset(modUTStatusPayments.UTMasterQuery(rsInfo.Get_Fields_Int32("AccountKey")), modExtraModules.strUTDatabase);
				if (modUTStatusPayments.Statics.TownService == "W" || !blnIncSew)
				{
					lblAsterisk.Visible = false;
					lblAsterisk2.Visible = false;
					fldSeperateSewer.Visible = false;
					fldSeperateSewerLabel.Visible = false;
					fldSewerTotalLabel.Visible = false;
					fldSewerTotal.Visible = false;
					linTotalLine.Visible = false;
					fldTotalTotalLabel.Visible = false;
					fldTotalTotal.Visible = false;
				}
				else
				{
					// TODO Get_Fields: Field [SewerTotal] not found!! (maybe it is an alias?)
					if (blnSepSew && FCConvert.ToInt32(rsInfo.Get_Fields("SewerTotal")) != 0)
					{
						lblAsterisk.Visible = true;
						lblAsterisk2.Visible = true;
						fldSeperateSewer.Visible = true;
						fldSeperateSewerLabel.Visible = true;
						fldSewerTotalLabel.Visible = false;
						fldSewerTotal.Visible = false;
						linTotalLine.Visible = false;
						fldTotalTotalLabel.Visible = false;
						fldTotalTotal.Visible = false;
					}
					else if (blnIncSew)
					{
						lblAsterisk.Visible = false;
						lblAsterisk2.Visible = false;
						fldSeperateSewer.Visible = false;
						fldSeperateSewerLabel.Visible = false;
						fldSewerTotalLabel.Visible = true;
						fldSewerTotal.Visible = true;
						linTotalLine.Visible = true;
						fldTotalTotalLabel.Visible = true;
						fldTotalTotal.Visible = true;
					}
				}
				// rsAccountInfo.OpenRecordset "SELECT * FROM Master WHERE Key = " & rsInfo.Fields("AccountKey")
				// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
				fldBookSequence.Text = rsInfo.Get_Fields_Int32("BookNumber") + " / " + rsInfo.Get_Fields("Sequence");
				if (rsAccountInfo.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					fldAccountNumber.Text = Strings.Format(rsAccountInfo.Get_Fields("AccountNumber"), "00000");
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					fldAccount2.Text = "Account # " + Strings.Format(rsAccountInfo.Get_Fields("AccountNumber"), "00000");
					if (boolPrintForOwner)
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						fldName.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("OwnerName"));
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						fldBName.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("OwnerName"));
						if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("OState"))) == "")
						{
							strText = rsAccountInfo.Get_Fields_String("BCity") + ", " + rsAccountInfo.Get_Fields_String("BState") + " " + rsAccountInfo.Get_Fields_String("BZip");
						}
						else
						{
							strText = rsAccountInfo.Get_Fields_String("OCity") + ", " + rsAccountInfo.Get_Fields_String("OState") + " " + rsAccountInfo.Get_Fields_String("OZip");
						}
						// If Trim(rsAccountInfo.Fields("BZip4")) <> "" Then
						// strText = strText & "-" & Trim(rsAccountInfo.Fields("BZip4"))
						// End If
						// MAL@20071212: Add second owner name
						// Reference: 116734
						// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsAccountInfo.Get_Fields("SecondOwnerName")) != "")
						{
							// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
							fldAddress1.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("SecondOwnerName"));
							fldAddress2.Text = rsAccountInfo.Get_Fields_String("OAddress1");
							fldAddress3.Text = rsAccountInfo.Get_Fields_String("OAddress2");
							fldAddress4.Text = rsAccountInfo.Get_Fields_String("OAddress3");
							fldAddress5.Text = strText;
						}
						else
						{
							fldAddress1.Text = rsAccountInfo.Get_Fields_String("OAddress1");
							fldAddress2.Text = rsAccountInfo.Get_Fields_String("OAddress2");
							fldAddress3.Text = rsAccountInfo.Get_Fields_String("OAddress3");
							fldAddress4.Text = strText;
							fldAddress5.Text = "";
						}
					}
					else
					{
						fldName.Text = rsAccountInfo.Get_Fields_String("Name");
						fldBName.Text = rsAccountInfo.Get_Fields_String("Name");
						if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("BState"))) == "")
						{
							strText = rsAccountInfo.Get_Fields_String("BCity") + ", " + rsAccountInfo.Get_Fields_String("OState") + " " + rsAccountInfo.Get_Fields_String("OZip");
						}
						else
						{
							strText = rsAccountInfo.Get_Fields_String("BCity") + ", " + rsAccountInfo.Get_Fields_String("BState") + " " + rsAccountInfo.Get_Fields_String("BZip");
						}
						// If Trim(rsAccountInfo.Fields("BZip4")) <> "" Then
						// strText = strText & "-" & Trim(rsAccountInfo.Fields("BZip4"))
						// End If
						// MAL@20071212
						if (FCConvert.ToString(rsAccountInfo.Get_Fields_String("Name2")) != "")
						{
							fldAddress1.Text = rsAccountInfo.Get_Fields_String("Name2");
							fldAddress2.Text = rsAccountInfo.Get_Fields_String("BAddress1");
							fldAddress3.Text = rsAccountInfo.Get_Fields_String("BAddress2");
							fldAddress4.Text = rsAccountInfo.Get_Fields_String("BAddress3");
							fldAddress5.Text = strText;
						}
						else
						{
							fldAddress1.Text = rsAccountInfo.Get_Fields_String("BAddress1");
							fldAddress2.Text = rsAccountInfo.Get_Fields_String("BAddress2");
							fldAddress3.Text = rsAccountInfo.Get_Fields_String("BAddress3");
							fldAddress4.Text = strText;
							fldAddress5.Text = "";
						}
					}
					if (frmSetupDisconnectNotices.InstancePtr.chkSendTo[1].CheckState == Wisej.Web.CheckState.Checked && !boolPrintForOwner)
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						if (rsAccountInfo.Get_Fields("OwnerName") != rsAccountInfo.Get_Fields_String("Name") && !FCConvert.ToBoolean(rsAccountInfo.Get_Fields_Boolean("SameBillOwner")))
						{
							boolPrintForOwner = true;
						}
						else
						{
							boolPrintForOwner = false;
						}
					}
					else
					{
						boolPrintForOwner = false;
					}
				}
				else
				{
					boolPrintForOwner = false;
					fldAccountNumber.Text = "00000";
					fldName.Text = "UNKNOWN";
				}
				fldNoticeDate.Text = Strings.Format(datNotice, "MM/dd/yyyy");
				fldShutOffDate.Text = Strings.Format(datShutoff, "MM/dd/yyyy");
				// kgk 12-19-2011 trout-770  Add option to show total due on account
				if (frmSetupDisconnectNotices.InstancePtr.cmbTotalDue.Text == "Total Due for Selected Rate Key")
				{
					dblWaterAmount = modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), true, ref datNotice, lngNewestRateKey);
					dblSewerAmount = modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), false, ref datNotice, lngNewestRateKey);
				}
				else
				{
					dblWaterAmount = modUTCalculations.CalculateAccountUTTotal_2(rsInfo.Get_Fields_Int32("AccountKey"), true, true, ref datNotice);
					dblSewerAmount = modUTCalculations.CalculateAccountUTTotal_2(rsInfo.Get_Fields_Int32("AccountKey"), false, true, ref datNotice);
				}
				fldWaterTotal.Text = Strings.Format(dblWaterAmount, "$#,##0.00");
				// rsInfo.Fields("WaterTotal")
				fldSewerTotal.Text = Strings.Format(dblSewerAmount, "$#,##0.00");
				// Format(rsInfo.Fields("SewerTotal"), "$#,##0.00")
				fldTotalTotal.Text = Strings.Format(dblWaterAmount + dblSewerAmount, "$#,##0.00");
				fldSeperateSewer.Text = Strings.Format(dblSewerAmount, "$#,##0.00");
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				lblText.Text = "Water service for this account will be discontinued for non-payment for the premises at " + rsAccountInfo.Get_Fields("StreetNumber") + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName"))) + ", on " + Strings.Format(datShutoff, "MM/dd/yyyy");
				// kgk 11-18-2011 trout-758  PUC rule changes for Disconnection Notice
				// lblText.Text = lblText.Text & ".  You can avoid disconnection by paying the outstanding balance in full or making a suitable arrangement with the "
				lblText.Text = lblText.Text + " or within ten (10) business days thereafter.  You can avoid disconnection by paying the outstanding balance in full or making a suitable arrangement with the ";
				if (Strings.Left(modGlobalConstants.Statics.gstrCityTown, 1) == "C")
				{
					// MAL@20070831:Added comma and space before hours information
					lblText.Text = lblText.Text + "City of " + strMuniName + " office, " + strHours + ".";
				}
				else if (Strings.Left(modGlobalConstants.Statics.gstrCityTown, 1) == "T")
				{
					lblText.Text = lblText.Text + "Town of " + strMuniName + " office, " + strHours + ".";
				}
				else
				{
					lblText.Text = lblText.Text + strMuniName + " office, " + strHours + ".";
				}
				// kgk 11-18-2011 trout-758
				// lblText.Text = lblText.Text & "  If service is disconnected, you are required to pay the past due amount in full and a reconnection charge, before water service can be restored."
				// lblText.Text = lblText.Text & "  There is a $20.00 charge to reestablish water service during business hours.  The charge to reestablish water service after business hours is $66.00."
				lblText.Text = lblText.Text + "  If service is disconnected, you are required to pay the past due amount in full and a reconnection charge";
				if (dblReconFeeReg != 0)
				{
					lblText.Text = lblText.Text + " of $" + Strings.Format(dblReconFeeReg, "#.00");
					if (dblReconFeeAH != 0)
					{
						lblText.Text = lblText.Text + " ($" + Strings.Format(dblReconFeeAH, "#.00") + " for after hours reconnection)";
					}
				}
				lblText.Text = lblText.Text + ", before water service can be restored.";
				if (boolDepositReqd)
				{
					lblText.Text = lblText.Text + "  A deposit may also be required.";
				}
				fldDefaultText.Text = strDefaultText;
				
				if (Strings.Trim(fldAddress1.Text) == "")
				{
					if (Strings.Trim(fldAddress2.Text) == "")
					{
						if (Strings.Trim(fldAddress3.Text) == "")
						{
							if (Strings.Trim(fldAddress4.Text) == "")
							{
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									// Do Nothing - All Empty
								}
								else
								{
									fldAddress1.Text = fldAddress5.Text;
									fldAddress2.Text = "";
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
							else
							{
								fldAddress1.Text = fldAddress4.Text;
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress2.Text = "";
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress2.Text = fldAddress5.Text;
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
						}
						else
						{
							fldAddress1.Text = fldAddress3.Text;
							if (Strings.Trim(fldAddress4.Text) == "")
							{
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress2.Text = "";
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress2.Text = fldAddress5.Text;
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
							else
							{
								fldAddress2.Text = fldAddress4.Text;
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress2.Text = "";
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress3.Text = fldAddress5.Text;
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
						}
					}
					else
					{
						fldAddress1.Text = fldAddress2.Text;
						if (Strings.Trim(fldAddress3.Text) == "")
						{
							if (Strings.Trim(fldAddress4.Text) == "")
							{
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress2.Text = "";
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress2.Text = fldAddress5.Text;
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
							else
							{
								fldAddress2.Text = fldAddress4.Text;
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress3.Text = fldAddress5.Text;
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
						}
						else
						{
							fldAddress2.Text = fldAddress3.Text;
							if (Strings.Trim(fldAddress4.Text) == "")
							{
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress3.Text = fldAddress5.Text;
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
							else
							{
								fldAddress3.Text = fldAddress4.Text;
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress4.Text = fldAddress5.Text;
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
						}
					}
				}
				else
				{
					if (Strings.Trim(fldAddress2.Text) == "")
					{
						if (Strings.Trim(fldAddress3.Text) == "")
						{
							if (Strings.Trim(fldAddress4.Text) == "")
							{
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress2.Text = "";
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress2.Text = fldAddress5.Text;
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
							else
							{
								fldAddress2.Text = fldAddress4.Text;
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress3.Text = fldAddress5.Text;
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
						}
						else
						{
							fldAddress2.Text = fldAddress3.Text;
							if (Strings.Trim(fldAddress4.Text) == "")
							{
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress3.Text = fldAddress5.Text;
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
							else
							{
								fldAddress3.Text = fldAddress4.Text;
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress4.Text = fldAddress5.Text;
									fldAddress5.Text = "";
								}
							}
						}
					}
					else
					{
						fldAddress2.Text = fldAddress2.Text;
						if (Strings.Trim(fldAddress3.Text) == "")
						{
							if (Strings.Trim(fldAddress4.Text) == "")
							{
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress3.Text = "";
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress3.Text = fldAddress5.Text;
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
							}
							else
							{
								fldAddress3.Text = fldAddress4.Text;
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress4.Text = fldAddress5.Text;
									fldAddress5.Text = "";
								}
							}
						}
						else
						{
							fldAddress3.Text = fldAddress3.Text;
							if (Strings.Trim(fldAddress4.Text) == "")
							{
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress4.Text = "";
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress4.Text = fldAddress5.Text;
									fldAddress5.Text = "";
								}
							}
							else
							{
								fldAddress4.Text = fldAddress4.Text;
								if (Strings.Trim(fldAddress5.Text) == "")
								{
									fldAddress5.Text = "";
								}
								else
								{
									fldAddress5.Text = fldAddress5.Text;
								}
							}
						}
					}
				}
				fldBAddress1.Text = fldAddress1.Text;
				fldBAddress2.Text = fldAddress2.Text;
				fldBAddress3.Text = fldAddress3.Text;
				fldBAddress4.Text = fldAddress4.Text;
				fldBAddress5.Text = fldAddress5.Text;
				// kgk 12-19-2011 trout-771  Change notice text for Hill NH Water Works - 01-23-2012  Hill Water Works
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "HILL WATER WORKS")
				{
					AltNoticeFormat();
				}
				else
				{
					strText = "Your service will be discontinued unless you do one of the following things:";
					fldPUCText1.Text = strText;
					strText = "A.  Pay the account balance in full; OR" + "\r\n";
					strText += "B.  Make an agreement with us to pay a mutually agreed upon portion of the outstanding bill immediately and the balance in reasonable installments; OR" + "\r\n";
					strText += "C.  Notify us of a medical condition that will be seriously aggravated by a lack of water service.";
					fldPUCText2.Text = strText;
					strText = "Medical Emergency";
					fldPUCText3.Text = strText;
					strText = "Disconnection can be postponed up to (30) days due to a medical emergency.";
					strText += "  There is a medical emergency when a registered physician certifies that the customer or occupant is seriously ill or has a medical condition which will be seriously aggravated by the lack of water service.";
					strText += "  While initial certification may be made by telephone, a physician must certify the emergency in writing within seven (7) days.";
					strText += "  During this medical period we will work with you to make a payment arrangement.";
					fldPUCText4.Text = strText;
					strText = "Dispute";
					fldPUCText5.Text = strText;
					strText = "If you dispute the Disconnection Notice or bill, please call our Billing Department at " + strPhoneNumber + ".";
					if (DateTime.Today.ToOADate() >= FCConvert.ToDateTime("11/2/2009").ToOADate())
					{
						strText += "  If we cannot resolve this matter, you have the right to submit this dispute to the Consumer Assistance and Safety Division, Maine Public Utilities Commission, State House Station #18, Augusta, ME 04333-0018, telephone 1-800-452-4699.";
					}
					else
					{
						strText += "  If we cannot resolve this matter, you have the right to submit this dispute to the Consumer Assistance Division, Maine Public Utilities Commission, 242 State Street, State House Station #18, Augusta, ME 04333-0013, telephone 287-3831 or 1-800-452-4699.";
					}
					strText += "  Per Chapter 660 of the Maine Public Utilities Commission, you must first try to resolve the dispute with the utility.";
					fldPUCText6.Text = strText;
					strText = "PLEASE GIVE THIS MATTER YOUR IMMEDIATE ATTENTION";
					fldPUCText7.Text = strText;
					// kgk 11-18-2011 trout-758  PUC rule changes for Disconnection Notice
					strText = "For information regarding sources of financial assistance please call 2-1-1 or go to http://www.211maine.org; or call the Department of Health and Human Services at (207) 287-3707;";
					strText += " or visit the Maine Community Action Association at http://www.mainecommunityaction.org";
					if (Strings.Trim(strLocGovtContact) != "")
					{
						strText += "; or contact " + Strings.Trim(strLocGovtContact);
						if (Strings.Trim(strLocGovtPhone) != "")
						{
							strText += " at " + Strings.Trim(strLocGovtPhone);
						}
					}
					strText += ".";
					fldFinAidText.Text = strText;
				}
				strText = "If payment has been rendered, please disregard this notice.  Do not send cash or coins and do not return with staples or paper clips.";
				fldPUCText8.Text = strText;
				rsAccountInfo.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void AltNoticeFormat()
		{
			string strText;
			float lngTemp;
			// THIS DISCONNECT NOTICE IS SPECIFIC FOR HILL NEW HAMPSHIRE
			fldPUCText1.Text = "";
			strText = "The Hill Water Works records indicate that you have the attached outstanding " + "balance on your account.  This action is prompted by all accounts with delinquent " + "balances 60 or more days delinquent." + "\r\n" + "\r\n" + "In accordance with Hill Water Works Rules and Regulations under DELINQUENT " + "ACCOUNTS, \"if the bill is not paid within 30 days, the Hill Water Works " + "commissioners can and will shut off your water service until you have made " + "satisfactory payment arrangements with the commissioners and paid a disconnect " + "fee of $" + Strings.Format((dblReconFeeReg != 0 ? dblReconFeeReg : 50), "0.00") + ".\"  The Hill Water Works also has the right to direct the Tax " + "Collector to attach a lien to your property for such outstanding balances.";
			fldPUCText2.Text = strText;
			fldPUCText2.Left = fldPUCText3.Left;
			fldPUCText2.Top = fldPUCText1.Top;
			fldPUCText3.Text = "";
			strText = "This letter is to notify you that on " + Strings.Format(datShutoff, "MMMM dd, yyyy") + " " + "your water service will be shut off unless payment in full of ALL delinquent " + "balances is made on your account.  Payment in full of ALL balances due must be " + "received in the Town Clerk/Tax Collector office by 6:30 pm on the date mentioned " + "above." + "\r\n" + "\r\n" + "If this delinquency is due to financial hardship, please contact the commissioners " + "to make payment arrangements prior to the date mentioned above.  You may also " + "acquire information for public assistance from the Town of Hill Office of Selectmen.";
			fldPUCText4.Text = strText;
			fldPUCText4.Font = FCUtils.SetFontStyle(fldPUCText4.Font, System.Drawing.FontStyle.Bold, true);
			//.Bold = true;
			fldPUCText4.Top = fldPUCText3.Top;
			strText = "We must hear from you soon of be forced to take action.  Please send your payment in " + "full of ALL balances due or call us to discuss the matter.";
			fldPUCText5.Text = strText;
			fldPUCText5.Font = FCUtils.SetFontStyle(fldPUCText5.Font, System.Drawing.FontStyle.Bold, true);
			//.Bold = false;
			fldPUCText6.Text = "";
			strText = "PLEASE GIVE THIS MATTER YOUR IMMEDIATE ATTENTION";
			fldPUCText7.Text = strText;
			fldFinAidText.Text = "";
			lngTemp = fldAccount2.Top - fldFinAidText.Top;
			fldAccount2.Top -= lngTemp;
			fldBName.Top -= lngTemp;
			fldBAddress1.Top -= lngTemp;
			fldBAddress2.Top -= lngTemp;
			fldBAddress3.Top -= lngTemp;
			fldBAddress4.Top -= lngTemp;
			fldBAddress5.Top -= lngTemp;
			fldPUCText8.Top -= lngTemp;
		}

		
	}
}
