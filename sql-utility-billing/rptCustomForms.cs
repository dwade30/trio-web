﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptCustomForms.
	/// </summary>
	public partial class rptCustomForms : BaseSectionReport
	{
		public rptCustomForms()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Forms";
		}

		public static rptCustomForms InstancePtr
		{
			get
			{
				return (rptCustomForms)Sys.GetInstance(typeof(rptCustomForms));
			}
		}

		protected rptCustomForms _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsBC.Dispose();
				rsMort.Dispose();
				rsTest.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomForms	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/05/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/26/2005              *
		// ********************************************************
		// THIS REPORT IS TOTOALLY NOT GENERIC AND HAS BEEN ALTERED FOR TWUT
		clsDRWrapper rsData = new clsDRWrapper();
		int intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		int intType;
		string strBarCode = "";
		string strIMPBarcode = "";
		// kk09242015 trouts-157
		bool boolIMPB;
		//
		clsDRWrapper rsBC = new clsDRWrapper();
		bool boolUseCustomScreen;
		int lngLienFormType;
		clsDRWrapper rsTest = new clsDRWrapper();
		float lngLineHeight;
		clsDRWrapper rsMort = new clsDRWrapper();
		string strLocalReportType;
		string strPassSQL;
		bool boolFailed;
		int intPrinterOption;
		int lngLastAccountKey;
		bool boolWater;
		bool blnFirstRecord;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			//CreateDataFields();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lngLastAccountKey = FCConvert.ToInt32(rsData.Get_Fields("Account"));
				eArgs.EOF = false;
			}
			else
			{
				rsData.MoveNext();
				TryAgain:
				;
				if (rsData.EndOfFile() != true)
				{
					lngLastAccountKey = FCConvert.ToInt32(rsData.Get_Fields("Account"));
				}
			}
			eArgs.EOF = rsData.EndOfFile();
		}
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref string strSQL, string strPassReportType, int intLabelType, ref string strPrinterName, ref string strFonttoUse, int lngLienType, bool boolPassWater, bool boolCustomScreen = true, string strPassDefaultPrinter = "", string strPassSortOrder = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strDBName = "";
				int intReturn;
				int x;
				bool boolUseFont;
				int lngLeftAdjustment;
				int lngTopAdjustment;
				string strPrintOption = "";
				//clsDRWrapper rsAdj = new clsDRWrapper();
				double dblLines = 0;
				string strTemp = "";
				boolWater = boolPassWater;
				strLocalReportType = strPassReportType;
				boolUseCustomScreen = boolCustomScreen;
				lngLienFormType = lngLienType;
				strPassSQL = strSQL;
				// kk09222015 trouts-157  Handle new CMF form with IMPBarcode and old 20 digit barcode - Added form to get and validate both numbers at once
				if (!frmGetStartingCMF.InstancePtr.Init(ref strBarCode, ref strIMPBarcode))
				{
					boolFailed = true;
					//FC:FINAL:MSH - issue #1219: use Unload() instead of Close() for executing ActiveReport_Terminate event and correct work
					//this.Close();
					this.Unload();
					if (boolUseCustomScreen)
					{
						frmCustomLabels.InstancePtr.Show(App.MainForm);
					}
					else
					{
						frmReprintCMForm.InstancePtr.Show(App.MainForm);
					}
					return;
				}
				if (Strings.Trim(strIMPBarcode) != "")
				{
					boolIMPB = true;
				}
				else
				{
					boolIMPB = false;
				}
				strFont = strFonttoUse;
				
				intType = intLabelType;
				boolPP = false;
				boolMort = false;
				intPrinterOption = 7;
				// find out what types should be printed   ie.  Owners, Mortgage Holders and/or New Owners
				if (FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[0].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState != Wisej.Web.CheckState.Checked))
				{
					if (frmCustomLabels.InstancePtr.chkFormPrintOptions[0].CheckState == Wisej.Web.CheckState.Checked)
					{
						// Owner
						if (frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState == Wisej.Web.CheckState.Checked)
						{
							// MH
							if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
							{
								// New Owner
								// this should not happen
							}
							else
							{
								// Owner and MH
								strPrintOption = "AND NOT NewOwner = -1";
								intPrinterOption = 3;
							}
						}
						else
						{
							if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
							{
								// New Owner
								// Owner and New Owner
								strPrintOption = "AND MortgageHolder = 0";
								intPrinterOption = 5;
							}
							else
							{
								// Owner
								strPrintOption = "AND (NOT NewOwner = -1 AND MortgageHolder = 0)";
								intPrinterOption = 1;
							}
						}
					}
					else
					{
						if (frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState == Wisej.Web.CheckState.Checked)
						{
							// MH
							if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
							{
								// New Owner
								// MH and New Owner
								strPrintOption = "AND (NewOwner = -1 OR MortgageHolder <> 0)";
								intPrinterOption = 6;
							}
							else
							{
								strPrintOption = "AND MortgageHolder <> 0";
								intPrinterOption = 2;
							}
						}
						else
						{
							if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
							{
								// New Owner
								// New Owner
								strPrintOption = "AND NewOwner = -1";
								intPrinterOption = 4;
							}
							else
							{
								// None
								strPrintOption = "AND Account = 0";
								intPrinterOption = 0;
							}
						}
					}
				}
				else
				{
					// get all of the copies
				}
				// get the list of CMFnumber that must be printed because they match the SQL string passed into this form
				// if other CMF need to be printed then a reprint option will have to be used
				rsBC.OpenRecordset("SELECT * FROM CMFNumbers WHERE ISNULL(LabelOnly,0) = 0 AND Type = " + FCConvert.ToString(lngLienFormType), modExtraModules.strUTDatabase);
				if (boolWater)
				{
					if (Strings.Trim(strPassSortOrder) != "")
					{
						rsData.OpenRecordset("SELECT CMFNumbers.*, Master.AccountNumber, p.FullNameLF AS OwnerName FROM CMFNumbers INNER JOIN Master ON CMFNumbers.Account = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.OwnerPartyID WHERE ISNULL(LabelOnly,0) = 0 AND ISNULL(BoolWater,0) = 1 AND Type = " + FCConvert.ToString(lngLienFormType) + strPrintOption + " ORDER BY " + strPassSortOrder + ", NewOwner desc, MortgageHolder", modExtraModules.strUTDatabase);
					}
					else
					{
						rsData.OpenRecordset("SELECT CMFNumbers.*, Master.AccountNumber, p.FullNameLF AS OwnerName FROM CMFNumbers INNER JOIN Master ON CMFNumbers.Account = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.OwnerPartyID WHERE ISNULL(LabelOnly,0) = 0 AND ISNULL(BoolWater,0) = 1 AND Type = " + FCConvert.ToString(lngLienFormType) + strPrintOption + " ORDER BY NewOwner desc, MortgageHolder", modExtraModules.strUTDatabase);
					}
				}
				else
				{
					if (Strings.Trim(strPassSortOrder) != "")
					{
						rsData.OpenRecordset("SELECT CMFNumbers.*, Master.AccountNumber, p.FullNameLF AS OwnerName FROM CMFNumbers INNER JOIN Master ON CMFNumbers.Account = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.OwnerPartyID WHERE ISNULL(LabelOnly,0) = 0 AND ISNULL(BoolWater,0) = 0 AND Type = " + FCConvert.ToString(lngLienFormType) + strPrintOption + " ORDER BY " + strPassSortOrder + ", NewOwner desc, MortgageHolder", modExtraModules.strUTDatabase);
					}
					else
					{
						rsData.OpenRecordset("SELECT CMFNumbers.*, Master.AccountNumber, p.FullNameLF AS OwnerName FROM CMFNumbers INNER JOIN Master ON CMFNumbers.Account = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.OwnerPartyID WHERE ISNULL(LabelOnly,0) = 0 AND ISNULL(BoolWater,0) = 0 AND Type = " + FCConvert.ToString(lngLienFormType) + strPrintOption + " ORDER BY NewOwner desc, MortgageHolder", modExtraModules.strUTDatabase);
					}
				}
				rsMort.OpenRecordset("SELECT * FROM MortgageHolders", "CentralData");
				if (rsData.RecordCount() != 0)
				{
					rsData.MoveFirst();
					boolDifferentPageSize = false;
					switch (intType)
					{
						case 0:
							{
								// Merit Certified Mail Form (Laser Format)
								this.Document.Printer.DefaultPageSettings.Landscape = true;
								this.PageSettings.Margins.Left = 0;
								this.PageSettings.Margins.Right = 0;
								//PageSettings.PaperSize = 255;
								PageSettings.PaperHeight = 15840 / 1440f;
								PageSettings.PaperWidth = 12240 / 1440f;
								PrintWidth = 14400 / 1440f;
								modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentCMFLaser", ref strTemp);
								if (Conversion.Val(strTemp) != 0)
								{
									dblLines = FCConvert.ToDouble(strTemp);
								}
								else
								{
									dblLines = 0;
								}
								if (240 * dblLines < 0)
								{
									this.PageSettings.Margins.Top = 0;
								}
								else
								{
									this.PageSettings.Margins.Top = FCConvert.ToSingle(240 * dblLines) / 1440f;
								}
								break;
							}
					}
					frmReportViewer.InstancePtr.Init(this._InstancePtr);
				}
				else
				{
					//FC:FINAL:MSH - issue #1219: use Unload() instead of Close() for executing ActiveReport_Terminate event and correct work
					//this.Close();
					this.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (this.Document.Pages.Count > 0)
				{
					if (boolWater)
					{
						switch (lngLienFormType)
						{
							case 20:
								{
									modGlobalFunctions.IncrementSavedReports("LastWUT30DayForms");
									this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUT30DayForms1.RDF"));
									break;
								}
							case 21:
								{
									modGlobalFunctions.IncrementSavedReports("LastWUTTransferToLienForms");
									this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTTransferToLienForms1.RDF"));
									break;
								}
							case 22:
								{
									modGlobalFunctions.IncrementSavedReports("LastWUTLienMaturityForms");
									this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTLienMaturityForms1.RDF"));
									break;
								}
						}
						//end switch
					}
					else
					{
						switch (lngLienFormType)
						{
							case 20:
								{
									modGlobalFunctions.IncrementSavedReports("LastSUT30DayForms");
									this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUT30DayForms1.RDF"));
									break;
								}
							case 21:
								{
									modGlobalFunctions.IncrementSavedReports("LastSUTTransferToLienForms");
									this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTTransferToLienForms1.RDF"));
									break;
								}
							case 22:
								{
									modGlobalFunctions.IncrementSavedReports("LastSUTLienMaturityForms");
									this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTLienMaturityForms1.RDF"));
									break;
								}
						}
						//end switch
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intReturn = 0;
				int const_PrintToolID;
				int cnt;
				Control ctrl;
				lngLineHeight = 240 / 1440f;
				const_PrintToolID = 9950;
				
                this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
                intReturn = 1;
				if (intReturn > 0)
				{
					FCGlobal.Printer.PaperSize = FCConvert.ToInt16(intReturn);
				}
				else
				{
					MessageBox.Show("Error.  Could not access form on printer.");
					this.Cancel();
					return;
				}

				if (modMain.Statics.gdblCMFAdjustment != 0 && intType == 1)
				{
					// if there is a user adjustment and using a laser printer
					foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl ctrl1 in this.Detail.Controls)
					{
						ctrl1.Top += lngLineHeight * (float)modMain.Statics.gdblCMFAdjustment;
					}
				}
				blnFirstRecord = true;
				CreateDataFields();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CreateDataFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intControlNumber;
				GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
				int intRow;
				int intCol;
				int intNumber;
				float lngAccTop = 0;
				// kgk 11-29-2011 trout-840  Add a line in each section of the CMF for "RE: Acct Holder" on Mortgage Holder forms
				// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
				intNumber = 13;
				// 11
				for (intRow = 1; intRow <= intNumber; intRow++)
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.CanGrow = false;
					NewField.Name = "txtData" + FCConvert.ToString(intRow);
					switch (intType)
					{
						case 0:
							{
								if (intRow <= 6)
								{
									// 5
									NewField.Top = ((((intRow - 1) * 225) + 3100)) / 1440f + FCConvert.ToSingle(modMain.Statics.gdblCMFAdjustment * lngLineHeight);
									NewField.Left = 4200 / 1440f;
								}
								else
								{
									NewField.Top = ((((intRow - 1) * 225) + 9200)) / 1440f + FCConvert.ToSingle(modMain.Statics.gdblCMFAdjustment * lngLineHeight);
									NewField.Left = 5000 / 1440f;
								}
								break;
							}
						case 1:
							{
								if (intRow <= 6)
								{
									// 5
									NewField.Top = ((((intRow - 1) * 225) + 1700)) / 1440f + FCConvert.ToSingle(modMain.Statics.gdblCMFAdjustment * lngLineHeight);
									NewField.Left = 4200 / 1440f;
								}
								else
								{
									NewField.Top = ((((intRow - 1) * 225) + 6300)) / 1440f + FCConvert.ToSingle(modMain.Statics.gdblCMFAdjustment * lngLineHeight);
									NewField.Left = 5000 / 1440f;
								}
								break;
							}
					}
					//end switch
					//NewField.BackColor = Color.Blue;
					NewField.Visible = true;
					NewField.Width = 5500 / 1440f;
					NewField.Height = 225 / 1440f;
					NewField.Text = string.Empty;
					NewField.MultiLine = false;
					NewField.WordWrap = true;
					if (Strings.Trim(strFont) != string.Empty)
					{
						NewField.Font = new Font(strFont, NewField.Font.Size);
					}
					if (intRow > 6 && intRow < 13)
					{
						// intRow > 5 And intRow < 11 Then
						NewField.Font = FCUtils.FontChangeSize(NewField.Font, FCConvert.ToInt32(NewField.Font.Size + 2));
						//.Size += 2;
					}
					if (intRow == 12)
					{
						// intRow = 10 Then
						Detail.Height = NewField.Top + NewField.Height + 1000 / 1440f;
					}
					if (intRow == 1)
					{
						lngAccTop = NewField.Top;
					}
					if (intRow == 13)
					{
						// intRow = 11 Then
						NewField.Top = lngAccTop - NewField.Height;
						NewField.Left = 4200 / 1440f;
					}
					Detail.Controls.Add(NewField);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			try
			{
                if (boolFailed) return;

                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                rptCertMailReport.InstancePtr.Init(FCConvert.ToInt16(lngLienFormType), ref intPrinterOption);
                frmReportViewer.InstancePtr.Init(rptCertMailReport.InstancePtr);
            }
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			// Call VerifyPrintToFile(Me, Tool)
			string vbPorterVar = ""/*Tool.Caption*/;

            if (vbPorterVar != "Print...") return;

            int intNumberOfPages = this.Document.Pages.Count;
            int intPageStart = 1;
            if (frmNumPages.InstancePtr.Init(ref intNumberOfPages, ref intPageStart, this))
            {
                this.PrintReport(false);
            }
        }

		private void PrintForms()
		{
			int lngErrCode = 0;
			var rsBillInfo = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill in the fields for labels
                int intControl;
                int intRow;
                string str1;
                string str2 = "";
                string str3;
                string str4;
                string str5;
                string str6;

                lngErrCode = 1;
                boolMort = FCConvert.ToInt32(rsData.Get_Fields_Int32("MortgageHolder")) != 0;
                str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name")));
                str2 = boolMort ? "RE:" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BName"))) : "";
                str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
                str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
                str5 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
                str6 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address4")));
                // kgk trout-840
                // condense the labels if some are blank
                if (Strings.Trim(str5) == string.Empty)
                {
                    // kgk trout-840
                    lngErrCode = 7;
                    str5 = str6;
                    str6 = "";
                }

                if (Strings.Trim(str4) == string.Empty)
                {
                    lngErrCode = 7;
                    str4 = str5;
                    str5 = "";
                }

                if (Strings.Trim(str3) == string.Empty)
                {
                    lngErrCode = 8;
                    str3 = str4;
                    if (boolMort)
                    {
                        str4 = str5;
                        str5 = "";
                    }
                    else
                    {
                        str4 = "";
                    }
                }

                if (Strings.Trim(str2) == string.Empty)
                {
                    lngErrCode = 9;
                    str2 = str3;
                    str3 = str4;
                    if (boolMort)
                    {
                        str4 = str5;
                        str5 = "";
                    }
                    else
                    {
                        str4 = "";
                    }
                }

                if (Strings.Trim(str1) == string.Empty)
                {
                    lngErrCode = 10;
                    str1 = str2;
                    str2 = str3;
                    str3 = str4;
                    if (boolMort)
                    {
                        str4 = str5;
                        str5 = "";
                    }
                    else
                    {
                        str4 = "";
                    }
                }

                lngErrCode = 11;
                for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
                {
                    if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) != "txtData")
                        continue;

                    intRow = FCConvert.ToInt32(
                        Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
                    var ctl = Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox;

                    switch (intRow)
                    {
                        case 1:
                        {
                            ctl.Text = str1;
                            break;
                        }
                        case 2:
                        {
                            ctl.Text = str2;
                            break;
                        }
                        case 3:
                        {
                            ctl.Text = str3;
                            break;
                        }
                        case 4:
                        {
                            ctl.Text = str4;
                            break;
                        }
                        case 5:
                        {
                            ctl.Text = str5;
                            break;
                        }
                        case 6:
                        {
                            ctl.Text = str6;
                            // kgk trout-840
                            break;
                        }
                        case 7:
                        {
                            // 6
                            ctl.Text = str1;
                            break;
                        }
                        case 8:
                        {
                            // 7
                            ctl.Text = str2;
                            break;
                        }
                        case 9:
                        {
                            // 8
                            ctl.Text = str3;
                            break;
                        }
                        case 10:
                        {
                            // 9
                            ctl.Text = str4;
                            break;
                        }
                        case 11:
                        {
                            // 10
                            ctl.Text = str5;
                            break;
                        }
                        case 12:
                        {
                            ctl.Text = str6;
                            // kgk trout-840
                            break;
                        }
                        case 13:
                        {
                            // 11
                            // Detail.Controls[intControl].Text = rsData.Fields("ActualAccountNumber")
                            // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                            ctl.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
                            break;
                        }
                    }

                    //end switch
                }

                // intControl
                lngErrCode = 12;
                // Debug.Print rsData.Fields("New.Account") & " - " & rsData.Fields("Name1")
                rsBillInfo.OpenRecordset("SELECT * FROM Bill WHERE ID = " + rsData.Get_Fields_Int32("BillKey"),
                    "TWUT0000.vb1");
                if (rsBillInfo.EndOfFile() != true)
                {
                    rsBillInfo.Edit();
                    rsBillInfo.Set_Fields("CertifiedMailNumber", strBarCode);
                    rsBillInfo.Set_Fields("IMPBTrackingNumber", strIMPBarcode);
                    rsBillInfo.Update();
                }

                rsBillInfo.DisposeOf();
                lngErrCode = 13;
                SaveCMFNumber(ref strBarCode, ref strIMPBarcode);
                lngErrCode = 14;
                strBarCode = modGlobalFunctions.GetNextMailFormCode(ref strBarCode);
                // kk09222015 trouts-157  Get the next IMPB Number in sequence
                if (boolIMPB)
                {
                    strIMPBarcode = modGlobalFunctions.GetNextMailFormCode_6(strIMPBarcode, true);
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Printing Form - " + FCConvert.ToString(lngErrCode),
                    MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
				rsBillInfo.Dispose();
            }
		}

		private void SaveCMFNumber(ref string strCMFNumber, ref string strIMPBNumber)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				rsBC.FindFirstRecord2("ID,Type", rsData.Get_Fields_Int32("ID") + "," + FCConvert.ToString(lngLienFormType), ",");
				// kk08122014 trocls-49  ' rsData.Fields("ID") & ", " & lngLienFormType
				if (rsBC.NoMatch)
				{
					// no match?? this should never happen
				}
				else
				{
					// update the bar code number
					rsBC.Edit();
					rsBC.Set_Fields("BarCode", strCMFNumber);
					rsBC.Set_Fields("IMPBTrackingNumber", strIMPBNumber);
					rsBC.Set_Fields("Printed", true);
					rsBC.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving CMF Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Strings.UCase(strLocalReportType) == "FORMS")
			{
				PrintForms();
			}
		}

		private void rptCustomForms_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCustomForms properties;
			//rptCustomForms.Caption	= "Custom Forms";
			//rptCustomForms.Icon	= "rptCustomForms.dsx":0000";
			//rptCustomForms.Left	= 0;
			//rptCustomForms.Top	= 0;
			//rptCustomForms.Width	= 11880;
			//rptCustomForms.Height	= 8595;
			//rptCustomForms.StartUpPosition	= 3;
			//rptCustomForms.SectionData	= "rptCustomForms.dsx":058A;
			//End Unmaped Properties
		}
	}
}
