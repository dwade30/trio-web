//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDemandFeeList.
	/// </summary>
	public class rptDemandFeeList : fecherFoundation.FCForm
	{

// nObj = 1
//   0	rptDemandFeeList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/11/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/11/2004              *
		// ********************************************************

		int lngTotalAccounts;
		int lngCount; // this will keep track of which row I am on in the grid
		double dblDemandFee;
		bool boolDone;
		double dblTotalDemand;
		double dblTotalCMF;
		double dblTotalPrin;
		bool boolReverse;
		bool boolWater;

		public void Init(ref int lngPassTotalAccounts, ref double dblPassDemand, ref bool boolPassWater, ref bool boolPassReverse = false)
		{
			lngTotalAccounts = lngPassTotalAccounts;
			dblDemandFee = dblPassDemand;
			boolReverse = boolPassReverse;
			boolWater = boolPassWater;
			// Me.Show
		}

		private void ActiveReport_FetchData(ref bool EOF)
		{
			if (!modUTLien.arrDemand[lngCount].Used && boolDone) {
				EOF = true;
			} else {
				EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			switch (KeyCode) {
				
				case Keys.Escape:
				{
					this.Unload();
					break;
				}
			} //end switch
		}

		private void ActiveReport_PageStart()
		{
			lblPage.Caption = "Page "+this.pageNumber;
		}

		private void ActiveReport_ReportEnd()
		{
			// save this report for future access
			if (this.Pages.Count>0) {
				if (boolWater) {
					if (boolReverse) {
						modGlobalFunctions.IncrementSavedReports_2("LastWUTReverseDemand");
						this.Pages.Save("RPT\\LastWUTReverseDemand1.RDF");
					} else {
						modGlobalFunctions.IncrementSavedReports_2("LastWUTDemandFeesList");
						this.Pages.Save("RPT\\LastWUTDemandFeesList1.RDF");
					}
				} else {
					if (boolReverse) {
						modGlobalFunctions.IncrementSavedReports_2("LastSUTReverseDemand");
						this.Pages.Save("RPT\\LastSUTReverseDemand1.RDF");
					} else {
						modGlobalFunctions.IncrementSavedReports_2("LastSUTDemandFeesList");
						this.Pages.Save("RPT\\LastSUTDemandFeesList1.RDF");
					}
				}
			}
		}

		private void ActiveReport_ReportStart()
		{
			lblDate.Caption = Strings.Format(DateTime.Today, "MM/DD/YYYY");
			lblTime.Caption = Strings.Format(DateAndTime.TimeOfDay, "HH:MM AMPM");
			lblMuniName.Caption = modGlobalConstants.MuniName;

			modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);

			if (boolReverse) {
				lblHeader.Caption = "Reverse Demand Fees List";
			} else {
				lblHeader.Caption = "Demand Fees List";
			}
			lngCount = 0;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			if (!boolDone) {

				// this will find the next available account that was processed
				while (!(modUTLien.arrDemand[lngCount].Processed || lngCount>=Information.UBound(modUTLien.arrDemand, 1))) {
					lngCount += 1;
				}

				// check to see if we are at the end of the list
				if (lngCount>=Information.UBound(modUTLien.arrDemand, 1)) {
					boolDone = true;
					// clear the fields so there are no repeats
					fldAcct.Text = "";
					fldName.Text = "";
					fldDemand.Text = "";
					fldCMF.Text = "";
					fldTotal.Text = "";
					return;
				}

				fldAcct.Text = modUTStatusPayments.GetAccountNumber(ref modUTLien.arrDemand[lngCount].Account);
				fldName.Text = modUTLien.arrDemand[lngCount].Name;
				fldDemand.Text = Strings.Format(modUTLien.arrDemand[lngCount].Fee, "#,##0.00");
				if (modUTLien.arrDemand[lngCount].CertifiedMailFee!=0) {
					fldCMF.Text = Strings.Format(modUTLien.arrDemand[lngCount].CertifiedMailFee-modUTLien.arrDemand[lngCount].Fee, "#,##0.00");
					fldTotal.Text = Strings.Format(modUTLien.arrDemand[lngCount].CertifiedMailFee, "#,##0.00");
					dblTotalCMF += modUTLien.arrDemand[lngCount].CertifiedMailFee-modUTLien.arrDemand[lngCount].Fee;
				} else {
					fldCMF.Text = "0.00";
					fldTotal.Text = fldDemand.Text;
				}

				dblTotalDemand += modUTLien.arrDemand[lngCount].Fee;

				// move to the next record in the array
				lngCount += 1;
			} else {
				// clear the fields
				fldAcct.Text = "";
				fldName.Text = "";
				fldDemand.Text = "";
				fldCMF.Text = "";
				fldTotal.Text = "";
			}
		}

		private void Detail_Format()
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblNumberOfAccounts.Caption = "There were "+Convert.ToString(lngTotalAccounts)+" accounts processed.";

			// lblTotalPrin.Caption = Format(dblTotalPrin, "#,##0.00")
			lblTotalCMF.Caption = Strings.Format(dblTotalCMF, "#,##0.00");
			lblTotalDemand.Caption = Strings.Format(dblTotalDemand, "#,##0.00");
			lblTotalTotal.Caption = Strings.Format(dblTotalCMF+dblTotalDemand, "#,##0.00"); // + dblTotalPrin
		}

		private void ReportFooter_Format()
		{
			SetupTotals();
		}

		private void rptDemandFeeList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptDemandFeeList properties;
			//rptDemandFeeList.Caption	= "Demand Fees List";
			//rptDemandFeeList.Icon	= "rptDemandFeesList.dsx":0000";
			//rptDemandFeeList.Left	= 0;
			//rptDemandFeeList.Top	= 0;
			//rptDemandFeeList.Width	= 11880;
			//rptDemandFeeList.Height	= 8595;
			//rptDemandFeeList.StartUpPosition	= 3;
			//rptDemandFeeList.SectionData	= "rptDemandFeesList.dsx":058A;
			//End Unmaped Properties
		}
	}
}