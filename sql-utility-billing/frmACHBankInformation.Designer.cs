﻿using fecherFoundation;
using System;
using Global;

namespace TWUT0000
{
    partial class frmACHBankInformation : BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
			Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdSave = new fecherFoundation.FCButton();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtDestName = new fecherFoundation.FCTextBox();
            this.txtDestRT = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.txtODFI = new fecherFoundation.FCTextBox();
            this.txtImmediateOriginRT = new fecherFoundation.FCTextBox();
            this.txtImmediateOriginName = new fecherFoundation.FCTextBox();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.cmbAccountType = new fecherFoundation.FCComboBox();
            this.txtCompanyID = new fecherFoundation.FCTextBox();
            this.txtCompanyAccount = new fecherFoundation.FCTextBox();
            this.txtCompanyName = new fecherFoundation.FCTextBox();
            this.txtCompanyRT = new fecherFoundation.FCTextBox();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 586);
            this.BottomPanel.Size = new System.Drawing.Size(818, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Size = new System.Drawing.Size(818, 526);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(818, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(257, 30);
            this.HeaderText.Text = "ACH Bank Information";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(341, 35);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(140, 48);
            this.cmdSave.TabIndex = 1;
            this.cmdSave.Text = "Save";
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.White;
            this.Frame1.Controls.Add(this.txtDestName);
            this.Frame1.Controls.Add(this.txtDestRT);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(21, 21);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(460, 116);
            this.Frame1.TabIndex = 35;
            this.Frame1.Text = "Bank / Immediate Destination";
            // 
            // txtDestName
            // 
            this.txtDestName.BackColor = System.Drawing.SystemColors.Window;
            this.txtDestName.Location = new System.Drawing.Point(20, 56);
            this.txtDestName.MaxLength = 23;
            this.txtDestName.Name = "txtDestName";
            this.txtDestName.Size = new System.Drawing.Size(270, 40);
            // 
            // txtDestRT
            // 
            this.txtDestRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtDestRT.Location = new System.Drawing.Point(310, 56);
            this.txtDestRT.MaxLength = 9;
            this.txtDestRT.Name = "txtDestRT";
            this.txtDestRT.Size = new System.Drawing.Size(130, 40);
            this.txtDestRT.TabIndex = 1;
            this.txtDestRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(310, 29);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(80, 19);
            this.Label2.TabIndex = 12;
            this.Label2.Text = "RT NUMBER";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(170, 19);
            this.Label1.TabIndex = 13;
            this.Label1.Text = "DESTINATION NAME";
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.White;
            this.Frame4.Controls.Add(this.txtODFI);
            this.Frame4.Controls.Add(this.txtImmediateOriginRT);
            this.Frame4.Controls.Add(this.txtImmediateOriginName);
            this.Frame4.Controls.Add(this.Label11);
            this.Frame4.Controls.Add(this.Label10);
            this.Frame4.Controls.Add(this.Label9);
            this.Frame4.Location = new System.Drawing.Point(21, 157);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(620, 116);
            this.Frame4.TabIndex = 37;
            this.Frame4.Text = "Immediate Origin";
            // 
            // txtODFI
            // 
            this.txtODFI.BackColor = System.Drawing.SystemColors.Window;
            this.txtODFI.Location = new System.Drawing.Point(470, 56);
            this.txtODFI.MaxLength = 9;
            this.txtODFI.Name = "txtODFI";
            this.txtODFI.Size = new System.Drawing.Size(130, 40);
            this.txtODFI.TabIndex = 4;
            this.txtODFI.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtImmediateOriginRT
            // 
            this.txtImmediateOriginRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtImmediateOriginRT.Location = new System.Drawing.Point(320, 56);
            this.txtImmediateOriginRT.MaxLength = 10;
            this.txtImmediateOriginRT.Name = "txtImmediateOriginRT";
            this.txtImmediateOriginRT.Size = new System.Drawing.Size(130, 40);
            this.txtImmediateOriginRT.TabIndex = 3;
            this.txtImmediateOriginRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtImmediateOriginName
            // 
            this.txtImmediateOriginName.BackColor = System.Drawing.SystemColors.Window;
            this.txtImmediateOriginName.Location = new System.Drawing.Point(20, 56);
            this.txtImmediateOriginName.MaxLength = 23;
            this.txtImmediateOriginName.Name = "txtImmediateOriginName";
            this.txtImmediateOriginName.Size = new System.Drawing.Size(281, 40);
            this.txtImmediateOriginName.TabIndex = 2;
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(470, 30);
            this.Label11.Name = "Label11";
            this.Label11.TabIndex = 37;
            this.Label11.Text = "ODFI NUMBER";
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(320, 30);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(130, 16);
            this.Label10.TabIndex = 36;
            this.Label10.Text = "ID / RT NUMBER";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 30);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(108, 16);
            this.Label9.TabIndex = 35;
            this.Label9.Text = "ORIGIN NAME";
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.White;
            this.Frame2.Controls.Add(this.cmbAccountType);
            this.Frame2.Controls.Add(this.txtCompanyID);
            this.Frame2.Controls.Add(this.txtCompanyAccount);
            this.Frame2.Controls.Add(this.txtCompanyName);
            this.Frame2.Controls.Add(this.txtCompanyRT);
            this.Frame2.Controls.Add(this.Label6);
            this.Frame2.Controls.Add(this.Label5);
            this.Frame2.Controls.Add(this.Label4);
            this.Frame2.Controls.Add(this.Label3);
            this.Frame2.Location = new System.Drawing.Point(21, 293);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(760, 168);
            this.Frame2.TabIndex = 36;
            this.Frame2.Text = "Company Information";
            // 
            // cmbAccountType
            // 
            this.cmbAccountType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAccountType.Location = new System.Drawing.Point(20, 108);
            this.cmbAccountType.Name = "cmbAccountType";
            this.cmbAccountType.Size = new System.Drawing.Size(150, 40);
            this.cmbAccountType.TabIndex = 9;
            // 
            // txtCompanyID
            // 
            this.txtCompanyID.BackColor = System.Drawing.SystemColors.Window;
            this.txtCompanyID.Location = new System.Drawing.Point(610, 56);
            this.txtCompanyID.MaxLength = 9;
            this.txtCompanyID.Name = "txtCompanyID";
            this.txtCompanyID.Size = new System.Drawing.Size(130, 40);
            this.txtCompanyID.TabIndex = 8;
            this.txtCompanyID.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCompanyAccount
            // 
            this.txtCompanyAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtCompanyAccount.Location = new System.Drawing.Point(460, 56);
            this.txtCompanyAccount.MaxLength = 17;
            this.txtCompanyAccount.Name = "txtCompanyAccount";
            this.txtCompanyAccount.Size = new System.Drawing.Size(130, 40);
            this.txtCompanyAccount.TabIndex = 7;
            this.txtCompanyAccount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtCompanyName
            // 
            this.txtCompanyName.BackColor = System.Drawing.SystemColors.Window;
            this.txtCompanyName.Location = new System.Drawing.Point(20, 56);
            this.txtCompanyName.MaxLength = 23;
            this.txtCompanyName.Name = "txtCompanyName";
            this.txtCompanyName.Size = new System.Drawing.Size(270, 40);
            this.txtCompanyName.TabIndex = 5;
            // 
            // txtCompanyRT
            // 
            this.txtCompanyRT.BackColor = System.Drawing.SystemColors.Window;
            this.txtCompanyRT.Location = new System.Drawing.Point(310, 56);
            this.txtCompanyRT.MaxLength = 9;
            this.txtCompanyRT.Name = "txtCompanyRT";
            this.txtCompanyRT.Size = new System.Drawing.Size(130, 40);
            this.txtCompanyRT.TabIndex = 6;
            this.txtCompanyRT.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(610, 30);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(86, 16);
            this.Label6.TabIndex = 18;
            this.Label6.Text = "COMPANY ID";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(460, 30);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(120, 16);
            this.Label5.TabIndex = 17;
            this.Label5.Text = "ACCOUNT NUMBER";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(310, 30);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(80, 16);
            this.Label4.TabIndex = 15;
            this.Label4.Text = "R/T NUMBER";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 30);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(60, 16);
            this.Label3.TabIndex = 16;
            this.Label3.Text = "NAME";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "";
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Continue";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSepar1
            // 
            this.mnuSepar1.Index = -1;
            this.mnuSepar1.Name = "mnuSepar1";
            this.mnuSepar1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmACHBankInformation
            // 
            this.ClientSize = new System.Drawing.Size(818, 694);
            this.Name = "frmACHBankInformation";
            this.Text = "ACH Bank Information";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.ResumeLayout(false);

        }



        #endregion

        private FCButton cmdSave;
        public FCFrame Frame1;
        public FCTextBox txtDestName;
        public FCTextBox txtDestRT;
        public FCLabel Label2;
        public FCLabel Label1;
        public FCFrame Frame4;
        public FCTextBox txtODFI;
        public FCTextBox txtImmediateOriginRT;
        public FCTextBox txtImmediateOriginName;
        public FCLabel Label11;
        public FCLabel Label10;
        public FCLabel Label9;
        public FCFrame Frame2;
        public FCComboBox cmbAccountType;
        public FCTextBox txtCompanyID;
        public FCTextBox txtCompanyAccount;
        public FCTextBox txtCompanyName;
        public FCTextBox txtCompanyRT;
        public FCLabel Label6;
        public FCLabel Label5;
        public FCLabel Label4;
        public FCLabel Label3;
        public fecherFoundation.FCToolStripMenuItem mnuFile;
        public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
        public fecherFoundation.FCToolStripMenuItem mnuSepar1;
        public fecherFoundation.FCToolStripMenuItem mnuExit;
    }
}