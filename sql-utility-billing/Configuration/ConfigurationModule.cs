﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Global;
using SharedApplication;
using SharedApplication.UtilityBilling.Notes;
using SharedApplication.UtilityBilling.Receipting.Interfaces;
using TWUT0000.Notes;
using TWUT0000.Receipting;

namespace TWUT0000.Configuration
{
	public class ConfigurationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			builder.RegisterType<frmCustomerSearch>().As<IView<IUtilityBillingCustomerSearchViewModel>>();
			builder.RegisterType<frmUBPaymentStatus>().As<IView<IUtilityBillingPaymentViewModel>>();
            builder.RegisterType<frmUtilityNote>().As<IModalView<IUtilityNoteViewModel>>();
            builder.RegisterType<frmUBNewLienDischargeNotice>().As<IModalView<IUtilityBillingLienDischargeNoticeViewModel>>();
		}
	}

}
