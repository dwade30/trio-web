﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCustomLisbonUpdateWinterBilling.
	/// </summary>
	partial class frmCustomLisbonUpdateWinterBilling : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWinter;
		public fecherFoundation.FCLabel lblWinter;
		public fecherFoundation.FCComboBox cboRateKeys;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomLisbonUpdateWinterBilling));
			this.cmbWinter = new fecherFoundation.FCComboBox();
			this.lblWinter = new fecherFoundation.FCLabel();
			this.cboRateKeys = new fecherFoundation.FCComboBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 307);
			this.BottomPanel.Size = new System.Drawing.Size(631, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbWinter);
			this.ClientArea.Controls.Add(this.lblWinter);
			this.ClientArea.Controls.Add(this.cboRateKeys);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(631, 247);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(631, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(242, 30);
			this.HeaderText.Text = "Update Winter Billing";
			// 
			// cmbWinter
			// 
			this.cmbWinter.AutoSize = false;
			this.cmbWinter.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWinter.FormattingEnabled = true;
			this.cmbWinter.Items.AddRange(new object[] {
				"Winter",
				"Summer"
			});
			this.cmbWinter.Location = new System.Drawing.Point(148, 128);
			this.cmbWinter.Name = "cmbWinter";
			this.cmbWinter.Size = new System.Drawing.Size(120, 40);
			this.cmbWinter.TabIndex = 0;
			this.cmbWinter.Text = "Winter";
			// 
			// lblWinter
			// 
			this.lblWinter.AutoSize = true;
			this.lblWinter.Location = new System.Drawing.Point(30, 142);
			this.lblWinter.Name = "lblWinter";
			this.lblWinter.Size = new System.Drawing.Size(59, 15);
			this.lblWinter.TabIndex = 1;
			this.lblWinter.Text = "SEASON";
			// 
			// cboRateKeys
			// 
			this.cboRateKeys.AutoSize = false;
			this.cboRateKeys.BackColor = System.Drawing.SystemColors.Window;
			this.cboRateKeys.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboRateKeys.FormattingEnabled = true;
			this.cboRateKeys.Location = new System.Drawing.Point(30, 68);
			this.cboRateKeys.Name = "cboRateKeys";
			this.cboRateKeys.Size = new System.Drawing.Size(238, 40);
			this.cboRateKeys.TabIndex = 0;
			this.cboRateKeys.DropDown += new System.EventHandler(this.cboRateKeys_DropDown);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(30, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(473, 18);
			this.Label1.TabIndex = 1;
			this.Label1.Text = "PLEASE SELECT THE RATE KEY YOU WISH TO SET THE WINTER BILLING FLAG FOR";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(250, 20);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(110, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Process";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmCustomLisbonUpdateWinterBilling
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(631, 415);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCustomLisbonUpdateWinterBilling";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Update Winter Billing";
			this.Load += new System.EventHandler(this.frmCustomLisbonUpdateWinterBilling_Load);
			this.Activated += new System.EventHandler(this.frmCustomLisbonUpdateWinterBilling_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCustomLisbonUpdateWinterBilling_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}
