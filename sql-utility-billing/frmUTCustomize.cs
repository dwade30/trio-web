﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTCustomize.
	/// </summary>
	public partial class frmUTCustomize : BaseForm
	{
		public frmUTCustomize()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}
        

        private void InitializeComponentEx()
		{
			this.lblTaxRate = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.chkInterest = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.lblConsIncrease = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblTaxRate.AddControlArrayElement(lblTaxRate_0, 0);
			this.lblTaxRate.AddControlArrayElement(lblTaxRate_1, 1);
			this.chkInterest.AddControlArrayElement(chkInterest_3, 3);
			this.chkInterest.AddControlArrayElement(chkInterest_2, 2);
			this.chkInterest.AddControlArrayElement(chkInterest_1, 1);
			this.chkInterest.AddControlArrayElement(chkInterest_0, 0);
			this.lblConsIncrease.AddControlArrayElement(lblConsIncrease_1, 1);
			this.lblConsIncrease.AddControlArrayElement(lblConsIncrease_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.txtTaxRate.AllowOnlyNumericInput(true);
            txtLDNSideAdj.AllowOnlyNumericInput(true);
            txtLienSideAdj.AllowOnlyNumericInput(true);
            txtFlatInterestAmount.AllowOnlyNumericInput(true);
            txtLDNBottomAdj.AllowOnlyNumericInput(true);
            txtLDNTopAdj.AllowOnlyNumericInput(true);
            txtLienBottomAdj.AllowOnlyNumericInput(true);
            txtLienTopAdj.AllowOnlyNumericInput(true);
            txtPPayInterestRate.AllowOnlyNumericInput(true);
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTCustomize InstancePtr
		{
			get
			{
				return (frmUTCustomize)Sys.GetInstance(typeof(frmUTCustomize));
			}
		}

		protected frmUTCustomize _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/06/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/02/2007              *
		// ********************************************************
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		bool boolDirty;
		clsGridAccount clsAcctW = new clsGridAccount();
		clsGridAccount clsAcctS = new clsGridAccount();
		clsGridAccount clsPrinAcctW = new clsGridAccount();
		clsGridAccount clsPrinAcctS = new clsGridAccount();
		clsGridAccount clsTaxAcctW = new clsGridAccount();
		clsGridAccount clsTaxAcctS = new clsGridAccount();
		clsGridAccount clsMFeeAcct = new clsGridAccount();
        private cSettingsController setController = new cSettingsController();
		private void cboApplyTo_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkAutoPrepayments_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded == true)
			{
				boolDirty = true;
			}
			if (chkAutoPrepayments.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (modUTStatusPayments.Statics.TownService == "B")
				{
					cboApplyTo.SelectedIndex = -1;
					lblApplyTo.Enabled = true;
					cboApplyTo.Enabled = true;
				}
				else if ((modUTStatusPayments.Statics.TownService == "W") || (modUTStatusPayments.Statics.TownService == "S"))
				{
					cboApplyTo.SelectedIndex = 0;
					lblApplyTo.Enabled = false;
					cboApplyTo.Enabled = false;
				}
			}
			else
			{
				lblApplyTo.Enabled = false;
				cboApplyTo.Enabled = false;
			}
		}

		private void chkBillCountS_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkBillCountW_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkBillingSummary_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkBillStormwaterFee_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded == true)
			{
				boolDirty = true;
			}
		}

		private void chkDisableAutoPmtFill_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded == true)
			{
				boolDirty = true;
			}
		}

		private void chkConsumptionsS_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkConsumptionsW_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkDefaultAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkInclChangeOutCons_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkSkipLDNPrompt_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkDollarAmountsS_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkDollarAmountsW_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkExportNoBillRecords_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkInterest_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkInterest_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = chkInterest.GetIndex((FCCheckBox)sender);
			chkInterest_CheckedChanged(index, sender, e);
		}

		private void chkMapLot_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkMeterReportS_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkMeterReportW_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkMinVarWarning_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkOnlyCheckOldestLien_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkSalesTaxS_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkSalesTaxW_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkShowLienAmtOnBill_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkStopOnConsumption_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkTACheckRE_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkTAUpdateRE_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void txtLienSideAdj_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLienSideAdj_Enter(object sender, System.EventArgs e)
		{
			txtLienSideAdj.SelectionStart = 0;
			txtLienSideAdj.SelectionLength = txtLienSideAdj.Text.Length;
		}

		private void txtLienSideAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLienTopAdj_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLienTopAdj_Enter(object sender, System.EventArgs e)
		{
			txtLienTopAdj.SelectionStart = 0;
			txtLienTopAdj.SelectionLength = txtLienTopAdj.Text.Length;
		}

		private void txtLienTopAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLienBottomAdj_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLienBottomAdj_Enter(object sender, System.EventArgs e)
		{
			txtLienBottomAdj.SelectionStart = 0;
			txtLienBottomAdj.SelectionLength = txtLienBottomAdj.Text.Length;
		}

		private void txtLienBottomAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLDNSideAdj_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLDNSideAdj_Enter(object sender, System.EventArgs e)
		{
			txtLDNSideAdj.SelectionStart = 0;
			txtLDNSideAdj.SelectionLength = txtLDNSideAdj.Text.Length;
		}

		private void txtLDNSideAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLDNTopAdj_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLDNTopAdj_Enter(object sender, System.EventArgs e)
		{
			txtLDNTopAdj.SelectionStart = 0;
			txtLDNTopAdj.SelectionLength = txtLDNTopAdj.Text.Length;
		}

		private void txtLDNTopAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLDNBottomAdj_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLDNBottomAdj_Enter(object sender, System.EventArgs e)
		{
			txtLDNBottomAdj.SelectionStart = 0;
			txtLDNBottomAdj.SelectionLength = txtLDNBottomAdj.Text.Length;
		}

		private void txtLDNBottomAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void chkUseCRAccounts_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
			if (chkUseCRAccounts.CheckState == Wisej.Web.CheckState.Checked)
			{
				cmdSetupICAccounts.Enabled = false;
			}
			else
			{
				cmdSetupICAccounts.Enabled = true;
			}
		}

		private void chkChpt660_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void chkShowLocation_CheckedChanged(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbDefaultBill_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style Private Sub cmbDefaultBill_Change()
			if (boolLoaded)
			{
				boolDirty = true;
				// kgk 10-21-2011  trout-766
				if (FCConvert.ToDouble(Strings.Trim(FCConvert.ToString(cmbDefaultBill.ItemData(cmbDefaultBill.SelectedIndex)))) == modCoreysUTCode.CNSTCUSTOMBILLTYPEOUTPRINT)
				{
					chkChpt660.Visible = true;
					chkChpt660.Enabled = true;
					chkInclChangeOutCons.Visible = true;
					// kjr trout-1118 11/7/2016
					chkInclChangeOutCons.Enabled = true;
                    chkSendCopies.Visible = true;
                    chkSendCopies.Enabled = true;
                }
				else
				{
					chkChpt660.Visible = false;
					chkChpt660.Enabled = false;
					chkChpt660.CheckState = Wisej.Web.CheckState.Unchecked;
					chkInclChangeOutCons.Visible = false;
					// kjr trout-1118 11/7/2016
					chkInclChangeOutCons.Enabled = false;
					chkInclChangeOutCons.CheckState = Wisej.Web.CheckState.Unchecked;
                    chkSendCopies.Visible = false;
                    chkSendCopies.Enabled = false;
                    chkSendCopies.Value = FCCheckBox.ValueSettings.Unchecked;
                }
			}
		}

		private void cmbDefaultBill_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbDefaultBill.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cmbDefaultBill_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will force the combobox to open with a spacebar
						if (modAPIsConst.SendMessageByNum(cmbDefaultBill.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbDefaultBill.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbDefaultCategoryS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbDefaultCategoryS_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbDefaultCategoryW_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbDefaultCategoryW_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbDefaultDigits_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbDefaultDigits_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbDefaultFrequency_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbDefaultFrequency_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbDefaultSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbDefaultSize_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbDEOrder_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbDEOrder_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbExtractFileType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbExtractFileType_Change()
			if (boolLoaded)
			{
                if (cmbExtractFileType.ItemData(cmbExtractFileType.ListIndex) == 24)
                {
                    lblExportFormat.Text = "VFlex Site Code";
                    cmbExportFormat.Visible = false;
                    txtVFlexSiteCode.Visible = true;
                }
                else
                {
                    lblExportFormat.Text = "Export Format";
                    cmbExportFormat.Visible = true;
                    txtVFlexSiteCode.Visible = false;
                }
				boolDirty = true;
			}
		}

		private void cmbMultiplier_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbMultiplier_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbService_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbService_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmbState_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// kgk 10-21-2011  Change does not fire on dropdown style cmbState_Change()
			if (boolLoaded)
			{
				boolDirty = true;
			}
		}

		private void cmdSetupICAccounts_Click(object sender, System.EventArgs e)
		{
			frmICAccountSetup.InstancePtr.Show();
		}

		private void frmUTCustomize_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				LoadSettings();
				optOptions_Click(cmbOptions.Items[0].ToString());
				boolLoaded = true;
			}
			else
			{
			}
		}

		private void frmUTCustomize_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			clsAcctW.GRID7Light = txtDefaultAccountW;
			clsAcctW.DefaultAccountType = "R";
			clsAcctS.GRID7Light = txtDefaultAccountS;
			clsAcctS.DefaultAccountType = "R";
			clsPrinAcctW.GRID7Light = txtPrinAcctW;
			clsPrinAcctW.DefaultAccountType = "R";
			clsPrinAcctS.GRID7Light = txtPrinAcctS;
			clsPrinAcctS.DefaultAccountType = "R";
			clsTaxAcctW.GRID7Light = txtTaxAcctW;
			clsTaxAcctW.DefaultAccountType = "R";
			clsTaxAcctS.GRID7Light = txtTaxAcctS;
			clsTaxAcctS.DefaultAccountType = "R";
			clsMFeeAcct.GRID7Light = txtMFeeAccount;
			clsMFeeAcct.DefaultAccountType = "R";
			LoadToolTips();
			LoadCombos();
		}

		private void frmUTCustomize_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDirty)
			{
				switch (MessageBox.Show("Items have been changed on this screen.  Would you like to save before exiting?", "Save", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							if (!SaveSettings())
							{
								e.Cancel = true;
								return;
							}
							else
							{
								// Continue Close
							}
							break;
						}
					case DialogResult.No:
						{
							// keep going
							break;
						}
					case DialogResult.Cancel:
						{
							e.Cancel = true;
							return;
						}
				}
				//end switch
			}
			boolLoaded = false;
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
		}

		private bool SaveSettings()
		{
			int intError = 0;
			bool SaveSettings = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDefaults = new clsDRWrapper();
				// vbPorter upgrade warning: intAns As short, int --> As DialogResult
				DialogResult intAns;
				string strError = "";
				// this will save all of the settings for Collections
				intError = 1;
				rsSettings.Edit();
				intError = 2;
				if (cmbBasis.Text == "365 Days")
				{
					rsSettings.Set_Fields("Basis", 365);
					modMain.Statics.gintBasis = 365;
					intError = 3;
				}
				else
				{
					rsSettings.Set_Fields("Basis", 360);
					modMain.Statics.gintBasis = 360;
					intError = 4;
				}
				// save the Town Service and set the variable
				if (FCConvert.ToString(rsSettings.Get_Fields_String("Service")) != Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1))
				{
					modGlobalFunctions.AddCYAEntry_80("UT", "Change Town Service", "Old : " + rsSettings.Get_Fields_String("Service"), "New : " + FCConvert.ToString(cmbService.Items[cmbService.SelectedIndex]));
					rsSettings.Set_Fields("Service", Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1));
					modUTStatusPayments.Statics.TownService = FCConvert.ToString(rsSettings.Get_Fields_String("Service"));
					// MAL@20080829: IConnect Account Options
					// Tracker Reference: 10680
					// kgk 10-13-2011 trout-733  Add Invoice Cloud
					if (modGlobalConstants.Statics.gboolIC || modGlobalConstants.Statics.gboolInvCld)
					{
						UpdateIConnectAccounts();
					}
				}
				// save autopay option
				if (cboAutopay.SelectedIndex == 0)
				{
					modUTStatusPayments.Statics.gboolAutoPayOldestBillFirst = true;
				}
				else
				{
					modUTStatusPayments.Statics.gboolAutoPayOldestBillFirst = false;
				}
				rsSettings.Set_Fields("AutoPayOption", cboAutopay.SelectedIndex);
				if (cmbUnitsOnBill.SelectedIndex > 0)
				{
					if (FCConvert.ToString(rsSettings.Get_Fields_Int32("ReadingUnitsOnBill")) != cmbUnitsOnBill.Items[cmbUnitsOnBill.SelectedIndex].ToString())
					{
						modGlobalFunctions.AddCYAEntry_80("UT", "Changing the Units to show on the bill.", "Old : " + rsSettings.Get_Fields_Int32("ReadingUnitsOnBill"), "New : " + FCConvert.ToString(cmbUnitsOnBill.Items[cmbMultiplier.SelectedIndex]));
					}
					rsSettings.Set_Fields("ReadingUnitsOnBill", cmbUnitsOnBill.Items[cmbUnitsOnBill.SelectedIndex].ToString());
				}
				else
				{
					rsSettings.Set_Fields("ReadingUnitsOnBill", 1);
				}
				// Town Reading Units
				if (cmbMultiplier.SelectedIndex > 0)
				{
					if (FCConvert.ToString(rsSettings.Get_Fields_Int32("ReadingUnits")) != cmbMultiplier.Items[cmbMultiplier.SelectedIndex].ToString())
					{
						modGlobalFunctions.AddCYAEntry_80("UT", "Changing the Reading Units of the town.", "Old : " + rsSettings.Get_Fields_Int32("ReadingUnits"), "New : " + FCConvert.ToString(cmbMultiplier.Items[cmbMultiplier.SelectedIndex]));
					}
					rsSettings.Set_Fields("ReadingUnits", cmbMultiplier.Items[cmbMultiplier.SelectedIndex].ToString());
				}
				else
				{
					rsSettings.Set_Fields("ReadingUnits", 1);
				}
				modExtraModules.Statics.glngTownReadingUnits = FCConvert.ToInt32(rsSettings.Get_Fields_Int32("ReadingUnits"));
				// Only check the oldest lien to find lien eligibility
				rsSettings.Set_Fields("OnlyCheckOldestBillForLien", FCConvert.CBool(chkOnlyCheckOldestLien.CheckState == Wisej.Web.CheckState.Checked));
				rsSettings.Set_Fields("UseBookForLien", FCConvert.CBool(chkUseBookForLiens.CheckState == Wisej.Web.CheckState.Checked));
				modUTCalculations.Statics.gboolUseBookForLien = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("UseBookForLien"));
				// Return Address
				vsReturnAddress.Row = (vsReturnAddress.Row + 1) % 3;
				rsSettings.Set_Fields("UTReturnAddress1", vsReturnAddress.TextMatrix(0, 0));
				rsSettings.Set_Fields("UTReturnAddress2", vsReturnAddress.TextMatrix(1, 0));
				rsSettings.Set_Fields("UTReturnAddress3", vsReturnAddress.TextMatrix(2, 0));
				rsSettings.Set_Fields("UTReturnAddress4", vsReturnAddress.TextMatrix(3, 0));
				// gboolShowMapLotInUTAudit
				rsSettings.Set_Fields("ShowMapLotInUTAudit", FCConvert.CBool(chkMapLot.CheckState == Wisej.Web.CheckState.Checked));
				modUTCalculations.Statics.gboolShowMapLotInUTAudit = FCConvert.CBool(rsSettings.Get_Fields_Boolean("ShowMapLotInUTAudit"));

				if (Conversion.Val(txtDiscountPercent.Text) > 0)
				{
					// kk060421015 Change to store rate as demial value to be consistent - 7% => 0.07
					rsSettings.Set_Fields("DiscountPercent", FCConvert.ToDouble(txtDiscountPercent.Text) / 100.0);
					// rsSettings.Fields("DiscountPercent") = CLng(CDbl(txtDiscountPercent.Text) * 100)
				}
				else
				{
					rsSettings.Set_Fields("DiscountPercent", 0);
				}
				intError = 5;
				rsSettings.Set_Fields("PayWaterFirst", FCConvert.CBool(cmbPayFirst.Text == "Water"));
				modUTStatusPayments.Statics.gboolPayWaterFirst = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("PayWaterFirst"));
				if (Conversion.Val(txtPPayInterestRate.Text) > 0)
				{
					// kk060421015 Change to store rate as demial value to be consistent - 7% => 0.07
					rsSettings.Set_Fields("OverPayInterestRate", FCConvert.ToDouble(txtPPayInterestRate.Text) / 100.0);
					// rsSettings.Fields("OverPayInterestRate") = Val(txtPPayInterestRate.Text)
				}
				else
				{
					rsSettings.Set_Fields("OverPayInterestRate", 0);
				}
				modExtraModules.Statics.dblOverPayRate = rsSettings.Get_Fields_Double("OverPayInterestRate");
				intError = 6;
				rsSettings.Set_Fields("AuditSeqReceipt", cmbSeq.Text == "Receipt Number");
				intError = 7;

				if (ValidateAdjustments(ref strError))
				{
					// changing to registry entries 02/16/2007
					if (Conversion.Val(txtCMFAdjust.Text) == 0)
					{
						intError = 105;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentCMFLaser", "0");
						modMain.Statics.gdblCMFAdjustment = 0;
					}
					else
					{
						intError = 115;
						// rsSettings.Fields("AdjustmentCMFLaser") = CDbl(txtCMFAdjust.Text)
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentCMFLaser", txtCMFAdjust.Text);
						modMain.Statics.gdblCMFAdjustment = FCConvert.ToDouble(txtCMFAdjust.Text);
					}
					if (Conversion.Val(txtLabelAdjustment.Text) == 0)
					{
						intError = 126;
						// rsSettings.Fields("AdjustmentLabels") = 0#
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabels", "0");
						modMain.Statics.gdblLabelsAdjustment = 0;
					}
					else
					{
						intError = 136;
						// rsSettings.Fields("AdjustmentLabels") = CDbl(txtLabelAdjustment.Text)
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabels", txtLabelAdjustment.Text);
						modMain.Statics.gdblLabelsAdjustment = Conversion.Val(txtLabelAdjustment.Text);
					}
					if (Conversion.Val(txtDMHLabelAdjust.Text) == 0)
					{
						intError = 107;
						// rsSettings.Fields("AdjustmentCMFLaser") = 0#
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMH", "0");
						modMain.Statics.gdblLabelsAdjustmentDMH = 0;
					}
					else
					{
						intError = 117;
						// rsSettings.Fields("AdjustmentCMFLaser") = CDbl(txtCMFAdjust.Text)
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMH", txtDMHLabelAdjust.Text);
						modMain.Statics.gdblLabelsAdjustmentDMH = Conversion.Val(txtDMHLabelAdjust.Text);
					}
					// vertical
					if (Conversion.Val(txtDMVLabelAdjust.Text) == 0)
					{
						intError = 108;
						// rsSettings.Fields("AdjustmentCMFLaser") = 0#
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMV", "0");
						modMain.Statics.gdblLabelsAdjustmentDMV = 0;
					}
					else
					{
						intError = 118;
						// rsSettings.Fields("AdjustmentCMFLaser") = CDbl(txtCMFAdjust.Text)
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMV", txtDMVLabelAdjust.Text);
						modMain.Statics.gdblLabelsAdjustmentDMV = Conversion.Val(txtDMVLabelAdjust.Text);
					}
					// Meter Reading Slip Adjustments
					if (Conversion.Val(txtMeterSlipsVAdj.Text) == 0)
					{
						intError = 109;
						// rsSettings.Fields("AdjustmentCMFLaser") = 0#
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "MeterReadingAdjustmentV", "0");
					}
					else
					{
						intError = 119;
						// rsSettings.Fields("AdjustmentCMFLaser") = CDbl(txtCMFAdjust.Text)
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "MeterReadingAdjustmentV", txtMeterSlipsVAdj.Text);
					}
					if (Conversion.Val(txtMeterSlipsHAdj.Text) == 0)
					{
						intError = 100;
						// rsSettings.Fields("AdjustmentCMFLaser") = 0#
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "MeterReadingAdjustmentH", "0");
					}
					else
					{
						intError = 110;
						// rsSettings.Fields("AdjustmentCMFLaser") = CDbl(txtCMFAdjust.Text)
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "MeterReadingAdjustmentH", txtMeterSlipsHAdj.Text);
					}
					if (Conversion.Val(txtSigAdjust_Lien.Text) == 0)
					{
						intError = 100;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "SignatureAdjustLien", "0");
						modMain.Statics.gdblLienSigAdjust = 0;
					}
					else
					{
						intError = 110;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "SignatureAdjustLien", txtSigAdjust_Lien.Text);
						modMain.Statics.gdblLienSigAdjust = Conversion.Val(txtSigAdjust_Lien.Text);
					}
					// kk05082014 trout-1082  Add lien/registry adjustments to UT
					// Lien Top Adjustment
					if (Conversion.Val(txtLienTopAdj.Text) == 0)
					{
						intError = 100;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentTop", "0");
						modMain.Statics.gdblLienAdjustmentTop = 0;
					}
					else
					{
						intError = 110;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentTop", txtLienTopAdj.Text);
						modMain.Statics.gdblLienAdjustmentTop = Conversion.Val(txtLienTopAdj.Text);
					}
					// Lien Bottom Adjustment
					if (Conversion.Val(txtLienBottomAdj.Text) == 0)
					{
						intError = 100;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentBottom", "0");
						modMain.Statics.gdblLienAdjustmentBottom = 0;
					}
					else
					{
						intError = 110;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentBottom", txtLienBottomAdj.Text);
						modMain.Statics.gdblLienAdjustmentBottom = Conversion.Val(txtLienBottomAdj.Text);
					}
					// Lien Side Adjustment - kk02082016 trout-1197  Add option to adjust side margins
					if (Conversion.Val(txtLienSideAdj.Text) == 0)
					{
						intError = 314;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentSide", "0");
						modMain.Statics.gdblLienAdjustmentSide = 0;
					}
					else
					{
						intError = 315;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentSide", txtLienSideAdj.Text);
						modMain.Statics.gdblLienAdjustmentSide = Conversion.Val(txtLienSideAdj.Text);
					}
					// kk02012016 trout-1197  Specs for registries changed to stright margins - removed the top right box stuff
					// Lien Top Right Box Size - kk08072015 trout-1151  Add option to define the box at the top right of form
					// Lien Discharge Top Adjustment
					if (Conversion.Val(txtLDNTopAdj.Text) == 0)
					{
						intError = 100;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentTop", "0");
						modUTCalculations.Statics.gdblUTLDNAdjustmentTop = 0;
						// kk06122014 trout-1082  Conflicting globals in CR  'gdblLDNAdjustmentTop = 0
					}
					else
					{
						intError = 110;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentTop", txtLDNTopAdj.Text);
						modUTCalculations.Statics.gdblUTLDNAdjustmentTop = Conversion.Val(txtLDNTopAdj.Text);
						// kk06122014 trout-1082  Conflicting globals in CR  'gdblLDNAdjustmentTop = Val(txtLDNTopAdj.Text)
					}
					// Lien Discharge Bottom Adjustment
					if (Conversion.Val(txtLDNBottomAdj.Text) == 0)
					{
						intError = 100;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentBottom", "0");
						modUTCalculations.Statics.gdblUTLDNAdjustmentBottom = 0;
						// kk06122014 trout-1082  Conflicting globals in CR  'gdblLDNAdjustmentBottom = 0
					}
					else
					{
						intError = 110;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentBottom", txtLDNBottomAdj.Text);
						modUTCalculations.Statics.gdblUTLDNAdjustmentBottom = Conversion.Val(txtLDNBottomAdj.Text);
						// kk06122014 trout-1082  Conflicting globals in CR  'gdblLDNAdjustmentBottom = Val(txtLDNBottomAdj.Text)
					}
					// Lien Discharge Side Adjustment - kk02082016 trout-1197  Add option to adjust side margins
					if (Conversion.Val(txtLDNSideAdj.Text) == 0)
					{
						intError = 314;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentSide", "0");
						modUTCalculations.Statics.gdblUTLDNAdjustmentSide = 0;
					}
					else
					{
						intError = 315;
						modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentSide", txtLDNSideAdj.Text);
						modUTCalculations.Statics.gdblUTLDNAdjustmentSide = Conversion.Val(txtLDNSideAdj.Text);
					}
				}
				else
				{
					MessageBox.Show("There is an error in your Adjustments settings." + "\r\n" + strError + "\r\n" + "Please review your settings and try again.", "Error in Adjustments", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					rsSettings.CancelUpdate();
					return SaveSettings;
				}
				if (chkDefaultAccount.CheckState == Wisej.Web.CheckState.Checked)
				{
					intError = 18;
					rsSettings.Set_Fields("ShowLastUTAccountInCR", true);
				}
				else
				{
					intError = 19;
					rsSettings.Set_Fields("ShowLastUTAccountInCR", false);
				}
				intError = 20;
				modUTStatusPayments.Statics.gboolShowLastUTAccountInCR = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowLastUTAccountInCR"));
				if (chkSkipLDNPrompt.CheckState == Wisej.Web.CheckState.Checked)
				{
					// kk09012015 trouts-146   Add option to skip LDN prompt in CR - automatically save
					rsSettings.Set_Fields("SkipLDNPrompt", true);
				}
				else
				{
					rsSettings.Set_Fields("SkipLDNPrompt", false);
				}
				if (chkStopOnConsumption.CheckState == Wisej.Web.CheckState.Checked)
				{
					intError = 25;
					rsSettings.Set_Fields("ShowDEConsumption", true);
				}
				else
				{
					intError = 26;
					rsSettings.Set_Fields("ShowDEConsumption", false);
				}
				if (chkExportNoBillRecords.CheckState == Wisej.Web.CheckState.Checked)
				{
					intError = 25;
					rsSettings.Set_Fields("ExportNoBillRecords", true);
				}
				else
				{
					intError = 26;
					rsSettings.Set_Fields("ExportNoBillRecords", false);
				}
				if (FCConvert.CBool(cmbReceiptSize.Text != "Wide"))
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "UTNarrowReceipt", "TRUE");
				}
				else
				{
					modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "UTNarrowReceipt", "FALSE");
				}
				intError = 40;
				// save account information
				if (txtPrinAcctS.TextMatrix(0, 0) != "")
				{
					rsSettings.Set_Fields("SewerPrincipalAccount", txtPrinAcctS.TextMatrix(0, 0));
				}
				else
				{
					rsSettings.Set_Fields("SewerPrincipalAccount", "");
				}
				if (txtTaxAcctS.TextMatrix(0, 0) != "")
				{
					rsSettings.Set_Fields("SewerTaxAccount", txtTaxAcctS.TextMatrix(0, 0));
				}
				else
				{
					rsSettings.Set_Fields("SewerTaxAccount", "");
				}
				if (txtPrinAcctW.TextMatrix(0, 0) != "")
				{
					rsSettings.Set_Fields("WaterPrincipalAccount", txtPrinAcctW.TextMatrix(0, 0));
				}
				else
				{
					rsSettings.Set_Fields("WaterPrincipalAccount", "");
				}
				if (txtTaxAcctW.TextMatrix(0, 0) != "")
				{
					rsSettings.Set_Fields("WaterTaxAccount", txtTaxAcctW.TextMatrix(0, 0));
				}
				else
				{
					rsSettings.Set_Fields("WaterTaxAccount", "");
				}
				intError = 50;
				if (cmbDefaultBill.SelectedIndex >= 0)
				{
					rsSettings.Set_Fields("DefaultBillFormat", Strings.Trim(FCConvert.ToString(cmbDefaultBill.ItemData(cmbDefaultBill.SelectedIndex))));
				}
				else if (FCConvert.ToDouble(Strings.Trim(FCConvert.ToString(cmbDefaultBill.ItemData(cmbDefaultBill.SelectedIndex)))) == modCoreysUTCode.CNSTCUSTOMBILLTYPEOUTPRINT)
				{
					rsSettings.Set_Fields("DefaultBillFormat", modCoreysUTCode.CNSTCUSTOMBILLTYPEOUTPRINT);
				}
				else
				{
					rsSettings.Set_Fields("DefaultBillFormat", 0);
				}
				// kgk 10-21-2011 trout-766
				rsSettings.Set_Fields("OutprintChapt660", (chkChpt660.CheckState == Wisej.Web.CheckState.Checked));
				// kjr trout-1118 11/7/2016
				rsSettings.Set_Fields("OutprintIncludeChangeOutCons", (chkInclChangeOutCons.CheckState == Wisej.Web.CheckState.Checked));

                if (chkSendCopies.Value == FCCheckBox.ValueSettings.Checked)
                {
                    setController.SaveSetting("True","OutprintSendCopies","UtilityBilling","", "","");
                }
                else
                {
                    setController.SaveSetting("False", "OutprintSendCopies", "UtilityBilling", "", "", "");
                }
				// Date Entry Method
				if (cmbDEType.Text == "Consumption")
				{
					rsSettings.Set_Fields("DEMethod", "C");
				}
				else
				{
					rsSettings.Set_Fields("DEMethod", "R");
				}

                if (chkDefDisplayHistory.Value == FCCheckBox.ValueSettings.Checked)
                {
                    rsSettings.Set_Fields("DefDisplayHistory",true);
                }
                else
                {
                    rsSettings.Set_Fields("DefDisplayHistory",false);
                }

				intError = 60;
				// Tax Rate
				if (Conversion.Val(txtTaxRate.Text) > 0 && Conversion.Val(txtTaxRate.Text) <= 100)
				{
					rsSettings.Set_Fields("TaxRate", Conversion.Val(txtTaxRate.Text) / 100.0);
				}
				else
				{
					rsSettings.Set_Fields("TaxRate", 0);
				}
				modUTStatusPayments.Statics.gdblUTMuniTaxRate = rsSettings.Get_Fields_Double("TaxRate");
				intError = 65;

				intError = 90;
				// Extract File Type
				if (cmbExtractFileType.SelectedIndex >= 0)
				{
					switch (cmbExtractFileType.ItemData(cmbExtractFileType.SelectedIndex))
					{
						case 0:
							{
								// 0 - Ti Sales
								rsSettings.Set_Fields("ExtractFileType", "T");
								rsSettings.Set_Fields("ReaderExtractType", "T");
								break;
							}
						case 1:
							{
								// 1 - Prescott Book / Sequence
								rsSettings.Set_Fields("ExtractFileType", "P");
								rsSettings.Set_Fields("ReaderExtractType", "P");
								break;
							}
						case 2:
							{
								// 2 - Aqua America
								rsSettings.Set_Fields("ExtractFileType", "A");
								rsSettings.Set_Fields("ReaderExtractType", "A");
								break;
							}
						case 3:
							{
								// 3 - RVS
								rsSettings.Set_Fields("ExtractFileType", "R");
								rsSettings.Set_Fields("ReaderExtractType", "R");
								break;
							}
						case 4:
							{
								// 4 - Badger
								rsSettings.Set_Fields("ExtractFileType", "B");
								rsSettings.Set_Fields("ReaderExtractType", "B");
								break;
							}
						case 5:
							{
								// 5 - TRIO DOS
								rsSettings.Set_Fields("ExtractFileType", "D");
								rsSettings.Set_Fields("ReaderExtractType", "D");
								break;
							}
						case 6:
							{
								// 6 - TRIO Windows
								rsSettings.Set_Fields("ExtractFileType", "W");
								rsSettings.Set_Fields("ReaderExtractType", "W");
								break;
							}
						case 7:
							{
								// 7 - TRIO DOS using the XRef1 field
								rsSettings.Set_Fields("ExtractFileType", "X");
								rsSettings.Set_Fields("ReaderExtractType", "X");
								break;
							}
						case 8:
							{
								// 8 - Prescott Account / Meter
								rsSettings.Set_Fields("ExtractFileType", "Q");
								rsSettings.Set_Fields("ReaderExtractType", "Q");
								break;
							}
						case 9:
							{
								// 9 - Northern Data
								rsSettings.Set_Fields("ExtractFileType", "N");
								rsSettings.Set_Fields("ReaderExtractType", "N");
								break;
							}
						case 10:
							{
								// 10- TRIO Windows using the XRef1 field
								rsSettings.Set_Fields("ExtractFileType", "Y");
								rsSettings.Set_Fields("ReaderExtractType", "Y");
								break;
							}
						case 11:
							{
								// 11- Northern Data 2
								rsSettings.Set_Fields("ExtractFileType", "F");
								rsSettings.Set_Fields("ReaderExtractType", "F");
								break;
							}
						case 12:
							{
								// 12- Belfast
								rsSettings.Set_Fields("ExtractFileType", "Z");
								rsSettings.Set_Fields("ReaderExtractType", "Z");
								break;
							}
						case 13:
							{
								// 13- Oakland
								rsSettings.Set_Fields("ExtractFileType", "O");
								rsSettings.Set_Fields("ReaderExtractType", "O");
								break;
							}
						case 14:
							{
								// MAL@20070925
								// 14 - RVS Windows
								rsSettings.Set_Fields("ExtractFileType", "V");
								rsSettings.Set_Fields("ReaderExtractType", "V");
								break;
							}
						case 15:
							{
								// MAL@20071019
								// 15 - EZ Reader
								rsSettings.Set_Fields("ExtractFileType", "E");
								rsSettings.Set_Fields("ReaderExtractType", "E");
								break;
							}
						case 16:
							{
								// MAL@20080925 ; Tracker Reference: 15568
								// 16 - Millinocket
								rsSettings.Set_Fields("ExtractFileType", "M");
								rsSettings.Set_Fields("ReaderExtractType", "M");
								break;
							}
						case 17:
							{
								// DJW@20090929
								// 17 - Lisbon
								rsSettings.Set_Fields("ExtractFileType", "L");
								rsSettings.Set_Fields("ReaderExtractType", "L");
								break;
							}
						case 18:
							{
								// DJW@20091120
								// 18 - Farmington
								rsSettings.Set_Fields("ExtractFileType", "I");
								rsSettings.Set_Fields("ReaderExtractType", "I");
								break;
							}
						case 19:
							{
								// kgk trout-754 09-22-2011
								// 19 - Dover-Foxcroft
								rsSettings.Set_Fields("ExtractFileType", "9");
								rsSettings.Set_Fields("ReaderExtractType", "9");
								// they don't use readers, just importing from Water Distr
								break;
							}
						case 20:
							{
								// kk trouts-5 03132013
								// 20 - Bangor Sewer (Import from Bangor Water District bill outprint file, no extract for reader
								rsSettings.Set_Fields("ExtractFileType", "G");
								rsSettings.Set_Fields("ReaderExtractType", "G");
								// they don't use readers, just importing from Water Distr
								break;
							}
						case 21:
							{
								// kk09022015  Veazie Water for Orono
								rsSettings.Set_Fields("ExtractFileType", "8");
								rsSettings.Set_Fields("ReaderExtractType", "8");
								// they don't use readers, just importing from Water Distr
								break;
							}
						case 22:
							{
								// kjr trout-1245 09-09-2016
								// 23 - Hampden
								rsSettings.Set_Fields("ExtractFileType", "H");
								rsSettings.Set_Fields("ReaderExtractType", "H");
								// they don't use readers, just importing from Water Distr
								break;
							}
                        case 23:
                        {
                            rsSettings.Set_Fields("ExtractFileType","7");
                            rsSettings.Set_Fields("ReaderExtractType","7");
                            break;
                        }
                        case 24:
                        {
                            rsSettings.Set_Fields("ExtractFileType","S");
                            rsSettings.Set_Fields("ReaderExtractType","S");
                            setController.SaveSetting(txtVFlexSiteCode.Text.Trim(),"VFlexSiteCode","UtilityBilling","","","");
                            break;
                        }
					}
					//end switch
				}
				else
				{
					rsSettings.Set_Fields("ExtractFileType", "T");
				}
				if (cmbExportFormat.Text == "Original")
				{
					rsSettings.Set_Fields("RemoteReaderExportType", 0);
					rsSettings.Set_Fields("ExportLocation", false);
				}
				else
				{
					rsSettings.Set_Fields("RemoteReaderExportType", 1);
					rsSettings.Set_Fields("ExportLocation", FCConvert.CBool(chkShowLocation.CheckState == Wisej.Web.CheckState.Checked));
					// kgk 12-19-2011 trout-767
				}
				rsSettings.Set_Fields("ForceReadingDate", FCConvert.CBool(chkForceReadDate.CheckState == Wisej.Web.CheckState.Checked));
				// kk01282014 trout-1009  Add option to force electronic data entry to use def date
				intError = 100;
				if (Conversion.Val(txtConsIncrease.Text) >= 0 && Conversion.Val(txtConsIncrease.Text) <= 100)
				{
					rsSettings.Set_Fields("ConsumptionPercent", FCConvert.ToString(Conversion.Val(txtConsIncrease.Text)));
				}
				else
				{
					rsSettings.Set_Fields("ConsumptionPercent", 0);
				}
				rsSettings.Set_Fields("HideMinimumVarianceWarning", FCConvert.CBool(chkMinVarWarning.CheckState == Wisej.Web.CheckState.Checked));
				intError = 120;
				if (cmbInterestType.Text == "Per Diem")
				{
					// Per Diem
					rsSettings.Set_Fields("RegularIntMethod", "P");
				}
				else if (cmbInterestType.Text == "Per Billing")
				{
					// Billing
					rsSettings.Set_Fields("RegularIntMethod", "B");
				}
				else if (cmbInterestType.Text == "Flat Rate")
				{
					// Flat
					if (FCConvert.ToString(rsSettings.Get_Fields_String("RegularIntMethod")) == "F" && rsSettings.Get_Fields_Decimal("FlatInterestAmount") != FCConvert.ToDecimal(FCConvert.ToDouble(txtFlatInterestAmount.Text)))
					{
						intAns = MessageBox.Show("Before you change your flat interest amount would you like to charge interest on any eligible accounts using the current flat interest amount?", "Charge Interest?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
						if (intAns == DialogResult.Yes)
						{
							modUTBilling.CalculateInterestAtBilling_68("", true);
						}
					}
					rsSettings.Set_Fields("RegularIntMethod", "F");
					rsSettings.Set_Fields("FlatInterestAmount", FCConvert.ToDouble(txtFlatInterestAmount.Text));
					modUTUseCR.Statics.gdblFlatInterestAmount = FCConvert.ToDouble(rsSettings.Get_Fields_Decimal("FlatInterestAmount"));
				}
				else if (cmbInterestType.Text == "On Demand")
				{
					// Demand
					rsSettings.Set_Fields("RegularIntMethod", "D");
				}
				modExtraModules.Statics.gstrRegularIntMethod = FCConvert.ToString(rsSettings.Get_Fields_String("RegularIntMethod"));
				// DJW@01142013 TROUT-912 Interest options were nto saving becuase there was no code in savesettings to do so
				if (cmbInterestType.Text != "Flat Rate")
				{
					// Charge Interest on these
					if (chkInterest[0].CheckState == Wisej.Web.CheckState.Checked)
					{
						rsSettings.Set_Fields("ChargeIntPrin", true);
					}
					else
					{
						rsSettings.Set_Fields("ChargeIntPrin", false);
					}
					if (chkInterest[1].CheckState == Wisej.Web.CheckState.Checked)
					{
						rsSettings.Set_Fields("ChargeIntTax", true);
					}
					else
					{
						rsSettings.Set_Fields("ChargeIntTax", false);
					}
					if (chkInterest[2].CheckState == Wisej.Web.CheckState.Checked)
					{
						rsSettings.Set_Fields("ChargeIntInt", true);
					}
					else
					{
						rsSettings.Set_Fields("ChargeIntInt", false);
					}
					if (chkInterest[3].CheckState == Wisej.Web.CheckState.Checked)
					{
						rsSettings.Set_Fields("ChargeIntCost", true);
					}
					else
					{
						rsSettings.Set_Fields("ChargeIntCost", false);
					}
				}
				intError = 130;
				rsSettings.Set_Fields("ShowLienAmountOnBill", FCConvert.CBool(chkShowLienAmtOnBill.CheckState == Wisej.Web.CheckState.Checked));
				modExtraModules.Statics.gboolShowLienAmountOnBill = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowLienAmountOnBill"));
				intError = 131;
				rsSettings.Set_Fields("BillStormwaterFee", FCConvert.CBool(chkBillStormwaterFee.CheckState == Wisej.Web.CheckState.Checked));
				intError = 150;
				rsSettings.Set_Fields("SBillToOwner", cmbSBillTo.Text == "Owner");
				modMain.Statics.gboolSBillToOwner = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("SBillToOwner"));
				rsSettings.Set_Fields("WBillToOwner", cmbWBillTo.Text == "Owner");
				modMain.Statics.gboolWBillToOwner = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("WBillToOwner"));
				intError = 200;
				intError = 104;
				rsSettings.Set_Fields("DefaultUTRTDToAutoChange", FCConvert.CBool(chkDefaultUTRTDDate.CheckState == Wisej.Web.CheckState.Checked));
				modUTCalculations.Statics.gboolDefaultUTRTDToAutoChange = FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DefaultUTRTDToAutoChange"));
				// kk03162015 trocrs-36  Add option to disable autofill payment amount on Insert/dblclick
				rsSettings.Set_Fields("DisableAutoPmtFill", FCConvert.CBool(chkDisableAutoPmtFill.CheckState == Wisej.Web.CheckState.Checked));
				// DE Order
				switch (cmbDEOrder.SelectedIndex)
				{
					case 0:
						{
							// 0 - Sequence
							rsSettings.Set_Fields("DEOrder", 0);
							break;
						}
					case 1:
						{
							// 1 - Owner Name
							rsSettings.Set_Fields("DEOrder", 1);
							break;
						}
					case 2:
						{
							// 2 - Tenant Name
							rsSettings.Set_Fields("DEOrder", 2);
							break;
						}
					case 3:
						{
							// 3 - Account Number
							rsSettings.Set_Fields("DEOrder", 3);
							break;
						}
				}
				//end switch
				intError = 250;
				if (cmbDefaultFrequency.SelectedIndex >= 0)
				{
					rsSettings.Set_Fields("MeterDefaultFrequency", cmbDefaultFrequency.ItemData(cmbDefaultFrequency.SelectedIndex));
				}
				else
				{
					rsSettings.Set_Fields("MeterDefaultFrequency", 0);
				}
				if (cmbDefaultDigits.SelectedIndex >= 0)
				{
					rsSettings.Set_Fields("MeterDefaultDigits", cmbDefaultDigits.Items[cmbDefaultDigits.SelectedIndex].ToString());
				}
				else
				{
					rsSettings.Set_Fields("MeterDefaultDigits", 0);
				}
				if (cmbDefaultSize.SelectedIndex >= 0)
				{
					rsSettings.Set_Fields("MeterDefaultSize", cmbDefaultSize.ItemData(cmbDefaultSize.SelectedIndex));
				}
				else
				{
					rsSettings.Set_Fields("MeterDefaultSize", 0);
				}
				if (cmbDefaultCategoryW.SelectedIndex >= 0)
				{
					rsSettings.Set_Fields("AccountDefaultCategoryW", cmbDefaultCategoryW.ItemData(cmbDefaultCategoryW.SelectedIndex));
				}
				else
				{
					rsSettings.Set_Fields("AccountDefaultCategoryW", 0);
				}
				if (cmbDefaultCategoryS.SelectedIndex >= 0)
				{
					rsSettings.Set_Fields("AccountDefaultCategoryS", cmbDefaultCategoryS.ItemData(cmbDefaultCategoryS.SelectedIndex));
				}
				else
				{
					rsSettings.Set_Fields("AccountDefaultCategoryS", 0);
				}
				intError = 251;
				if (cmbState.SelectedIndex >= 0)
				{
					rsSettings.Set_Fields("AccountDefaultState", Strings.Left(cmbState.Items[cmbState.SelectedIndex].ToString(), 2));
				}
				else
				{
					rsSettings.Set_Fields("AccountDefaultState", "");
				}
				if (txtDefaultAccountW.TextMatrix(0, 0) != "")
				{
					rsSettings.Set_Fields("AccountDefaultAccountW", txtDefaultAccountW.TextMatrix(0, 0));
				}
				else
				{
					rsSettings.Set_Fields("AccountDefaultAccountW", "");
				}
				if (txtDefaultAccountS.TextMatrix(0, 0) != "")
				{
					rsSettings.Set_Fields("AccountDefaultAccountS", txtDefaultAccountS.TextMatrix(0, 0));
				}
				else
				{
					rsSettings.Set_Fields("AccountDefaultAccountS", "");
				}
				if (txtDefaultCity.Text != "")
				{
					rsSettings.Set_Fields("AccountDefaultCity", txtDefaultCity.Text);
				}
				else
				{
					rsSettings.Set_Fields("AccountDefaultCity", "");
				}
				if (txtDefaultZip.Text != "")
				{
					rsSettings.Set_Fields("AccountDefaultZip", txtDefaultZip.Text);
				}
				else
				{
					rsSettings.Set_Fields("AccountDefaultZip", "");
				}
				rsSettings.Set_Fields("AccountDefaultUseREInfo", FCConvert.CBool(chkDefaultRE.CheckState == Wisej.Web.CheckState.Checked));
				rsSettings.Set_Fields("AccountDefaultMHInfo", FCConvert.CBool(chkDefaultMHInfo.CheckState == Wisej.Web.CheckState.Checked));
				if (cmbBillingRepSeq.Text == "Book and Sequence")
				{
					rsSettings.Set_Fields("BillingRepSeq", 0);
					// book seq
				}
				else if (cmbBillingRepSeq.Text == "Account Number")
				{
					rsSettings.Set_Fields("BillingRepSeq", 1);
					// acct
				}
				else if (cmbBillingRepSeq.Text == "Name")
				{
					rsSettings.Set_Fields("BillingRepSeq", 2);
					// name
				}
				else if (cmbBillingRepSeq.Text == "Location")
				{
					rsSettings.Set_Fields("BillingRepSeq", 3);
					// location
				}
				else if (cmbBillingRepSeq.Text == "Map Lot")
				{
					rsSettings.Set_Fields("BillingRepSeq", 4);
					// map lot
				}
				else
				{
					rsSettings.Set_Fields("BillingRepSeq", 0);
					// book seq
				}

				rsSettings.Set_Fields("AutoPrepayments", FCConvert.CBool(chkAutoPrepayments.CheckState == Wisej.Web.CheckState.Checked));
				modGlobalConstants.Statics.gblnAutoPrepayments = FCConvert.CBool(chkAutoPrepayments.CheckState == Wisej.Web.CheckState.Checked);
				if (FCConvert.CBool(chkAutoPrepayments.CheckState == Wisej.Web.CheckState.Checked))
				{
					switch (cboApplyTo.SelectedIndex)
					{
						case 0:
							{
								rsSettings.Set_Fields("AutoPaymentApplyTo", 1);
								break;
							}
						case 1:
							{
								rsSettings.Set_Fields("AutoPaymentApplyTo", 2);
								break;
							}
						default:
							{
								rsSettings.Set_Fields("AutoPaymentApplyTo", 0);
								break;
							}
					}
					//end switch
				}
				// MAL@20080624: Show Tax Acquired Caption on FC Notices
				// Tracker Reference: 14371
				rsSettings.Set_Fields("ShowTaxAcquiredCaption", FCConvert.CBool(chkShowTaxAcquired.CheckState == Wisej.Web.CheckState.Checked));
				modGlobalConstants.Statics.gblnShowTaxAcquiredCaption = FCConvert.CBool(chkShowTaxAcquired.CheckState == Wisej.Web.CheckState.Checked);
				// MAL@20080812: Auto-Update RE Tax Acquired Status
				// Tracker Reference: 14035
				rsSettings.Set_Fields("UpdateRETaxAcquired", FCConvert.CBool(chkTAUpdateRE.CheckState == Wisej.Web.CheckState.Checked));
				modGlobalConstants.Statics.gblnAutoUpdateRETaxAcq = FCConvert.CBool(chkTAUpdateRE.CheckState == Wisej.Web.CheckState.Checked);
				// DJW@20090720: Check RE Tax Acquired Status
				// Tracker Reference: 14035
				rsSettings.Set_Fields("CheckRETaxAcquired", FCConvert.CBool(chkTACheckRE.CheckState == Wisej.Web.CheckState.Checked));
				modGlobalConstants.Statics.gblnCheckRETaxAcq = FCConvert.CBool(chkTACheckRE.CheckState == Wisej.Web.CheckState.Checked);
				// MAL@20080820: IConnect Options
				// Tracker Reference: 10680
				rsSettings.Set_Fields("ICUseCRAccounts", FCConvert.CBool(chkUseCRAccounts.CheckState == Wisej.Web.CheckState.Checked));
				if (txtMFeeAccount.TextMatrix(0, 0) != "")
				{
					rsSettings.Set_Fields("ICMFeeAccount", txtMFeeAccount.TextMatrix(0, 0));
				}
				else
				{
					rsSettings.Set_Fields("ICMFeeAccount", "");
				}
				// kgk 10-13-2011 trout-733 Invoice Cloud Options
				if (txtInvStartDate.Text != "")
				{
					rsSettings.Set_Fields("InvCldInvStartDate", DateAndTime.DateValue(txtInvStartDate.Text));
				}
				intError = 300;
				// save the default analysis reports to show
				rsDefaults.OpenRecordset("SELECT * FROM DefaultAnalysisReports", modExtraModules.strUTDatabase);
				if (rsDefaults.EndOfFile())
				{
					rsDefaults.AddNew();
				}
				else
				{
					rsDefaults.Edit();
				}
				rsDefaults.Set_Fields("BillingSummaryS", FCConvert.CBool(chkBillingSummary.CheckState == Wisej.Web.CheckState.Checked));
				if (modUTStatusPayments.Statics.TownService != "W")
				{
					// Sewer
					rsDefaults.Set_Fields("DollarAmountS", FCConvert.CBool(chkDollarAmountsS.CheckState == Wisej.Web.CheckState.Checked));
					rsDefaults.Set_Fields("ConsumptionS", FCConvert.CBool(chkConsumptionsS.CheckState == Wisej.Web.CheckState.Checked));
					rsDefaults.Set_Fields("BillCountS", FCConvert.CBool(chkBillCountS.CheckState == Wisej.Web.CheckState.Checked));
					rsDefaults.Set_Fields("MeterReportS", FCConvert.CBool(chkMeterReportS.CheckState == Wisej.Web.CheckState.Checked));
					rsDefaults.Set_Fields("SalesTaxS", FCConvert.CBool(chkSalesTaxS.CheckState == Wisej.Web.CheckState.Checked));
				}
				if (modUTStatusPayments.Statics.TownService != "S")
				{
					// Water
					rsDefaults.Set_Fields("DollarAmountW", FCConvert.CBool(chkDollarAmountsW.CheckState == Wisej.Web.CheckState.Checked));
					rsDefaults.Set_Fields("ConsumptionW", FCConvert.CBool(chkConsumptionsW.CheckState == Wisej.Web.CheckState.Checked));
					rsDefaults.Set_Fields("BillCountW", FCConvert.CBool(chkBillCountW.CheckState == Wisej.Web.CheckState.Checked));
					rsDefaults.Set_Fields("MeterReportW", FCConvert.CBool(chkMeterReportW.CheckState == Wisej.Web.CheckState.Checked));
					rsDefaults.Set_Fields("SalesTaxW", FCConvert.CBool(chkSalesTaxW.CheckState == Wisej.Web.CheckState.Checked));
				}
				rsDefaults.Update();
				intError = 1000;
				// kgk 03-04-11 trout-683  Add custom report detail description overrides
				rsSettings.Set_Fields("RptDesc_PastDue", txtPastDue.Text);
				rsSettings.Set_Fields("RptDesc_Credit", txtCredit.Text);
				rsSettings.Set_Fields("RptDesc_Regular", txtRegular.Text);
				rsSettings.Set_Fields("RptDesc_Override", txtOverride.Text);
				rsSettings.Set_Fields("RptDesc_Interest", txtInterest.Text);
				rsSettings.Set_Fields("RptDesc_Liens", txtLiens.Text);
				rsSettings.Set_Fields("RptDesc_Tax", txtTax.Text);
				rsSettings.Set_Fields("RptDesc_Misc", txtMisc.Text);
				rsSettings.Set_Fields("RptDesc_Adj", txtAdj.Text);
				// kgk 03-24-11 trout-684  Jay - limit 1st cons billing to 2x prev flat billing
				rsSettings.Set_Fields("RateTypeChange", FCConvert.CBool(chkLimitConsBill.CheckState == Wisej.Web.CheckState.Checked));
				if (rsSettings.Update(true))
				{
					SaveSettings = true;
					boolDirty = false;
					MessageBox.Show("Save Successful.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					SaveSettings = false;
					MessageBox.Show("Save not successful.", "Not Successful - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
                //FC:FINAL:SBE - #4617 - reload navigation menu, after settings was changed
                App.MainForm.ReloadNavigationMenu();
                return SaveSettings;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Settings - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveSettings;
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				int lngCT;
				int lngTemp = 0;
				int lngKey = 0;
				clsDRWrapper rsDefaults = new clsDRWrapper();
				FillApplyToList();
				// this will load the settings from the database
				rsSettings.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (rsSettings.EndOfFile() != true && rsSettings.BeginningOfFile() != true)
				{
				}
				else
				{
					rsSettings.AddNew();
					// add defaults....
					rsSettings.Update(true);
				}
				if (Conversion.Val(rsSettings.Get_Fields_Int32("Basis")) > 0)
				{
					if (FCConvert.ToInt32(rsSettings.Get_Fields_Int32("Basis")) == 365)
					{
						cmbBasis.Text = "365 Days";
					}
					else
					{
						cmbBasis.Text = "360 Days";
					}
				}
				else
				{
					cmbBasis.Text = "365 Days";
					// default value
				}
				// townservice
				for (lngCT = 0; lngCT <= cmbService.Items.Count - 1; lngCT++)
				{
					if (Strings.Left(cmbService.Items[lngCT].ToString(), 1) == FCConvert.ToString(rsSettings.Get_Fields_String("Service")))
					{
						cmbService.SelectedIndex = lngCT;
						break;
					}
				}
				// autopay option
				if (Conversion.Val(rsSettings.Get_Fields_Int32("AutoPayOption")) != 0)
				{
					cboAutopay.SelectedIndex = 1;
				}
				else
				{
					cboAutopay.SelectedIndex = 0;
				}
				// Reading Units to show On Bill
				if (Conversion.Val(rsSettings.Get_Fields_Int32("ReadingUnitsOnBill")) > 0)
				{
					for (lngCT = 0; lngCT <= cmbUnitsOnBill.Items.Count - 1; lngCT++)
					{
						if (Conversion.Val(cmbUnitsOnBill.Items[lngCT].ToString()) == Conversion.Val(rsSettings.Get_Fields_Int32("ReadingUnitsOnBill")))
						{
							cmbUnitsOnBill.SelectedIndex = lngCT;
							break;
						}
					}
				}
				else
				{
					cmbUnitsOnBill.SelectedIndex = 0;
				}
				// Town Reading Units
				if (Conversion.Val(rsSettings.Get_Fields_Int32("ReadingUnits")) > 0)
				{
					for (lngCT = 0; lngCT <= cmbMultiplier.Items.Count - 1; lngCT++)
					{
						if (Conversion.Val(cmbMultiplier.Items[lngCT].ToString()) == Conversion.Val(rsSettings.Get_Fields_Int32("ReadingUnits")))
						{
							cmbMultiplier.SelectedIndex = lngCT;
							break;
						}
					}
				}
				else
				{
					// this will be ones
					cmbMultiplier.SelectedIndex = 0;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("OnlyCheckOldestBillForLien")))
				{
					chkOnlyCheckOldestLien.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkOnlyCheckOldestLien.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("UseBookForLien")))
				{
					chkUseBookForLiens.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkUseBookForLiens.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				vsReturnAddress.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("UTReturnAddress1")));
				vsReturnAddress.TextMatrix(1, 0, FCConvert.ToString(rsSettings.Get_Fields_String("UTReturnAddress2")));
				vsReturnAddress.TextMatrix(2, 0, FCConvert.ToString(rsSettings.Get_Fields_String("UTReturnAddress3")));
				vsReturnAddress.TextMatrix(3, 0, FCConvert.ToString(rsSettings.Get_Fields_String("UTReturnAddress4")));
				//vsReturnAddress.Height = vsReturnAddress.RowHeight(0) * 4;
				vsReturnAddress.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				// gboolShowMapLotInUTAudit
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowMapLotInUTAudit")))
				{
					chkMapLot.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkMapLot.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (modUTStatusPayments.Statics.TownService == "B")
				{
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("PayWaterFirst")))
					{
						// True if they want to pay water first
						cmbPayFirst.Text = "Water";
					}
					else
					{
						cmbPayFirst.Text = "Sewer";
					}
				}
				else if (modUTStatusPayments.Statics.TownService == "W")
				{
					cmbPayFirst.Clear();
					cmbPayFirst.Items.Add("Water");
					cmbPayFirst.Text = "Water";
					//optPayFirst[1].Enabled = false;
				}
				else
				{
					cmbPayFirst.Clear();
					cmbPayFirst.Items.Add("Sewer");
					cmbPayFirst.Text = "Sewer";
					//optPayFirst[0].Enabled = false;
				}
				if (FCConvert.ToString(rsSettings.Get_Fields_String("DEMethod")) == "C")
				{
					cmbDEType.Text = "Consumption";
				}
				else
				{
					cmbDEType.Text = "Reading";
				}

                if (rsSettings.Get_Fields_Boolean("DefDisplayHistory"))
                {
                    chkDefDisplayHistory.Value = FCCheckBox.ValueSettings.Checked;
                }
                else
                {
                    chkDefDisplayHistory.Value = FCCheckBox.ValueSettings.Unchecked;
                }
				// TODO Get_Fields: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
				if (Conversion.Val(rsSettings.Get_Fields("DiscountPercent")) > 0)
				{
					// this is the percentage of discount a town offers when a person pays in full early
					// kk060421015 Change to store rate as demial value to be consistent - 7% => 0.07
					// TODO Get_Fields: Check the table for the column [DiscountPercent] and replace with corresponding Get_Field method
					txtDiscountPercent.Text = Strings.Format((rsSettings.Get_Fields("DiscountPercent") * 100.0), "0.00");
					// txtDiscountPercent.Text = Format((rsSettings.Fields("DiscountPercent") / 100), "0.00")
				}
				else
				{
					txtDiscountPercent.Text = "0.00";
				}
				if (Conversion.Val(rsSettings.Get_Fields_Double("OverPayInterestRate")) > 0)
				{
					// this is the amount of interest paid to a customer that has over paid and has not gotten a refund yet
					// kk060421015 Change to store rate as demial value to be consistent - 7% => 0.07
					txtPPayInterestRate.Text = Strings.Format((rsSettings.Get_Fields_Double("OverPayInterestRate") * 100.0), "0.00");
					// txtPPayInterestRate.Text = Format(rsSettings.Fields("OverPayInterestRate"), "0.00")
				}
				else
				{
					txtPPayInterestRate.Text = "0.00";
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("AuditSeqReceipt")))
				{
					cmbSeq.Text = "Receipt Number";
				}
				else
				{
					cmbSeq.Text = "Account Number";
				}
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentCMFLaser", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtCMFAdjust.Text = "0";
				}
				else
				{
					txtCMFAdjust.Text = Strings.Format(FCConvert.ToDouble(strTemp), "#0");
				}
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabels", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtLabelAdjustment.Text = "0";
				}
				else
				{
					txtLabelAdjustment.Text = Strings.Format(FCConvert.ToDouble(strTemp), "#0");
				}
				// dot matrix adjustments
				// horizontal
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMH", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtDMHLabelAdjust.Text = "0";
				}
				else
				{
					txtDMHLabelAdjust.Text = Strings.Format(FCConvert.ToDouble(strTemp), "#0");
				}
				// Vertical
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMV", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtDMVLabelAdjust.Text = "0";
				}
				else
				{
					txtDMVLabelAdjust.Text = Strings.Format(FCConvert.ToDouble(strTemp), "#0");
				}
				// meter reading slips adjustments
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "MeterReadingAdjustmentV", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtMeterSlipsVAdj.Text = "0";
				}
				else
				{
					txtMeterSlipsVAdj.Text = Strings.Format(FCConvert.ToDouble(strTemp), "#0");
				}
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "MeterReadingAdjustmentH", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtMeterSlipsHAdj.Text = "0";
				}
				else
				{
					txtMeterSlipsHAdj.Text = Strings.Format(FCConvert.ToDouble(strTemp), "#0");
				}
				// MAL@20080618: Add adjustment for lien notice signature
				// Tracker Reference: 14327
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "SignatureAdjustLien", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtSigAdjust_Lien.Text = "0";
				}
				else
				{
					txtSigAdjust_Lien.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// kk05082014 trout-1082
				// Lien Top Adjustment
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentTop", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtLienTopAdj.Text = "0";
				}
				else
				{
					txtLienTopAdj.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Lien Bottom Adjustment
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentBottom", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtLienBottomAdj.Text = "0";
				}
				else
				{
					txtLienBottomAdj.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Lien Side Adjustment - kk02082016 trout-1197  Add option to adjust side margins
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLienAdjustmentSide", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtLienSideAdj.Text = "0";
				}
				else
				{
					txtLienSideAdj.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// kk02082016 trout-1197  Specs for registries changed to stright margins - removed the top right box stuff
				// Lien Top Right Box Size - kk08072015 trout-1151  Add option to define the box at the top right of form
				// Lien Discharge Top Adjustment
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentTop", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtLDNTopAdj.Text = "0";
				}
				else
				{
					txtLDNTopAdj.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Lien Discharge Bottom Adjustment
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentBottom", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtLDNBottomAdj.Text = "0";
				}
				else
				{
					txtLDNBottomAdj.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// Lien Discharge Side Adjustment - kk02082016 trout-1197  Add option to adjust side margins
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "gdblLDNAdjustmentSide", ref strTemp);
				if (Conversion.Val(strTemp) == 0)
				{
					txtLDNSideAdj.Text = "0";
				}
				else
				{
					txtLDNSideAdj.Text = FCConvert.ToString(FCConvert.ToDouble(strTemp));
				}
				// cash receipting default
				if (modGlobalConstants.Statics.gboolCR)
				{
					cmbReceiptSize.Enabled = false;
					if (FCConvert.CBool(rsSettings.Get_Fields_Boolean("ShowLastUTAccountInCR")))
					{
						chkDefaultAccount.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkDefaultAccount.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.CBool(rsSettings.Get_Fields_Boolean("SkipLDNPrompt")))
					{
						// kk09012015 trouts-146   Add option to skip LDN prompt in CR - automatically save
						chkSkipLDNPrompt.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkSkipLDNPrompt.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					// hide the CR frame and show the CL frame
					cmbReceiptSize.Enabled = true;
					cmbReceiptSize.Visible = true;
					// get the default receipt length
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "UTNarrowReceipt", ref strTemp);
					if (Strings.Trim(Strings.UCase(strTemp)) != "TRUE")
					{
						cmbReceiptSize.Text = "Wide";
					}
					else
					{
						cmbReceiptSize.Text = "Narrow";
					}
				}
				// this is to show the consumption before moving to the next meter in DE
				if (FCConvert.CBool(rsSettings.Get_Fields_Boolean("ShowDEConsumption")))
				{
					chkStopOnConsumption.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkStopOnConsumption.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.CBool(rsSettings.Get_Fields_Boolean("ExportNoBillRecords")))
				{
					chkExportNoBillRecords.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkExportNoBillRecords.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// fill the account boxes
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("SewerPrincipalAccount"))) != "")
				{
					txtPrinAcctS.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("SewerPrincipalAccount")));
				}
				else
				{
					txtPrinAcctS.TextMatrix(0, 0, "");
				}
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("SewerTaxAccount"))) != "")
				{
					txtTaxAcctS.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("SewerTaxAccount")));
				}
				else
				{
					txtTaxAcctS.TextMatrix(0, 0, "");
				}
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("WaterPrincipalAccount"))) != "")
				{
					txtPrinAcctW.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("WaterPrincipalAccount")));
				}
				else
				{
					txtPrinAcctW.TextMatrix(0, 0, "");
				}
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("WaterTaxAccount"))) != "")
				{
					txtTaxAcctW.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("WaterTaxAccount")));
				}
				else
				{
					txtTaxAcctW.TextMatrix(0, 0, "");
				}
				// Default Bill Format
				if (Conversion.Val(rsSettings.Get_Fields_String("DefaultBillFormat")) != 0)
				{
					lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSettings.Get_Fields_String("DefaultBillFormat"))));
					for (lngCT = 0; lngCT <= cmbDefaultBill.Items.Count - 1; lngCT++)
					{
						if (lngKey == cmbDefaultBill.ItemData(lngCT))
						{
							cmbDefaultBill.SelectedIndex = lngCT;
							break;
						}
					}
				}
				// kgk 10-21-2011 trout-766  Outprinting Chapter 660 option
				if (lngKey == modCoreysUTCode.CNSTCUSTOMBILLTYPEOUTPRINT)
				{
					chkChpt660.Visible = true;
					chkChpt660.Enabled = true;
					chkInclChangeOutCons.Visible = true;
					// kjr trout-1118 11/7/2016
					chkInclChangeOutCons.Enabled = true;
                    chkSendCopies.Visible = true;
                    chkSendCopies.Enabled = true;
                }
				else
				{
					chkChpt660.Visible = false;
					chkChpt660.Enabled = false;
					chkInclChangeOutCons.Visible = false;
					// kjr trout-1118 11/7/2016
					chkInclChangeOutCons.Enabled = false;
                    chkSendCopies.Visible = false;
                    chkSendCopies.Enabled = false;
                }
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("OutprintChapt660")))
				{
					chkChpt660.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkChpt660.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Int32("OutprintIncludeChangeOutCons")))
				{
					// kjr trout-1118 11/7/2016
					chkInclChangeOutCons.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkInclChangeOutCons.CheckState = Wisej.Web.CheckState.Unchecked;
				}

                var strSendCopies = setController.GetSettingValue("OutprintSendCopies", "UtilityBilling", "", "", "");
                if (strSendCopies.Trim().ToLower() == "true" && lngKey == modCoreysUTCode.CNSTCUSTOMBILLTYPEOUTPRINT)
                {
                    chkSendCopies.Value = FCCheckBox.ValueSettings.Checked;
                }
                else
                {
                    chkSendCopies.Value = FCCheckBox.ValueSettings.Unchecked;
                }
				// Default Meter Digits
				if (Conversion.Val(rsSettings.Get_Fields_Int32("MeterDefaultDigits")) != 0)
				{
					lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSettings.Get_Fields_Int32("MeterDefaultDigits"))));
					for (lngCT = 0; lngCT <= cmbDefaultDigits.Items.Count - 1; lngCT++)
					{
						if (lngKey == FCConvert.ToDouble(cmbDefaultDigits.Items[lngCT].ToString()))
						{
							cmbDefaultDigits.SelectedIndex = lngCT;
							break;
						}
					}
				}
				// Default Meter Size
				if (Conversion.Val(rsSettings.Get_Fields_Int32("MeterDefaultSize")) != 0)
				{
					lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSettings.Get_Fields_Int32("MeterDefaultSize"))));
					for (lngCT = 0; lngCT <= cmbDefaultSize.Items.Count - 1; lngCT++)
					{
						if (lngKey == cmbDefaultSize.ItemData(lngCT))
						{
							cmbDefaultSize.SelectedIndex = lngCT;
							break;
						}
					}
				}
				// Default Meter Frequency
				if (Conversion.Val(rsSettings.Get_Fields_Int32("MeterDefaultFrequency")) != 0)
				{
					lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSettings.Get_Fields_Int32("MeterDefaultFrequency"))));
					for (lngCT = 0; lngCT <= cmbDefaultFrequency.Items.Count - 1; lngCT++)
					{
						if (lngKey == cmbDefaultFrequency.ItemData(lngCT))
						{
							cmbDefaultFrequency.SelectedIndex = lngCT;
							break;
						}
					}
				}
				// Default Account CategoryW
				if (Conversion.Val(rsSettings.Get_Fields_Int32("AccountDefaultCategoryW")) != 0)
				{
					lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSettings.Get_Fields_Int32("AccountDefaultCategoryW"))));
					for (lngCT = 0; lngCT <= cmbDefaultCategoryW.Items.Count - 1; lngCT++)
					{
						if (lngKey == cmbDefaultCategoryW.ItemData(lngCT))
						{
							cmbDefaultCategoryW.SelectedIndex = lngCT;
							break;
						}
					}
				}
				// Default Account CategoryS
				if (Conversion.Val(rsSettings.Get_Fields_Int32("AccountDefaultCategoryS")) != 0)
				{
					lngKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSettings.Get_Fields_Int32("AccountDefaultCategoryS"))));
					for (lngCT = 0; lngCT <= cmbDefaultCategoryS.Items.Count - 1; lngCT++)
					{
						if (lngKey == cmbDefaultCategoryS.ItemData(lngCT))
						{
							cmbDefaultCategoryS.SelectedIndex = lngCT;
							break;
						}
					}
				}
				// Default Account W
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultAccountW"))) != "")
				{
					txtDefaultAccountW.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultAccountW")));
				}
				else
				{
					txtDefaultAccountW.TextMatrix(0, 0, "");
				}
				// Default Account S
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultAccountS"))) != "")
				{
					txtDefaultAccountS.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultAccountS")));
				}
				else
				{
					txtDefaultAccountS.TextMatrix(0, 0, "");
				}
				// Default City, State, Zip
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultCity"))) != "")
				{
					txtDefaultCity.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultCity"));
				}
				else
				{
					txtDefaultCity.Text = "";
				}
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultState"))) != "")
				{
					for (lngCT = 0; lngCT <= cmbState.Items.Count - 1; lngCT++)
					{
						if (FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultState")) == Strings.Left(cmbState.Items[lngCT].ToString(), 2))
						{
							cmbState.SelectedIndex = lngCT;
							break;
						}
					}
				}
				else
				{
					cmbState.SelectedIndex = -1;
				}
				if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultZip"))) != "")
				{
					txtDefaultZip.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultZip"));
				}
				else
				{
					txtDefaultZip.Text = "";
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("AccountDefaultUseREInfo")))
				{
					chkDefaultRE.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDefaultRE.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("AccountDefaultMHInfo")))
				{
					chkDefaultMHInfo.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDefaultMHInfo.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				txtTaxRate.Text = Strings.Format(rsSettings.Get_Fields_Double("TaxRate") * 100.0, "#0.00");
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DefaultUTRTDToAutoChange")))
				{
					chkDefaultUTRTDDate.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDefaultUTRTDDate.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// kk03162015 trocrs-36  Add option to disable autofill payment amount on Insert/dblclick
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("DisableAutoPmtFill")))
				{
					chkDisableAutoPmtFill.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkDisableAutoPmtFill.CheckState = Wisej.Web.CheckState.Unchecked;
				}

				string vbPorterVar = rsSettings.Get_Fields_String("ExtractFileType");
				if (vbPorterVar == "T")
				{
					// 0 - Ti Sales
					lngTemp = 0;
				}
				else if (vbPorterVar == "P")
				{
					// 1 - Prescott
					lngTemp = 1;
				}
				else if (vbPorterVar == "A")
				{
					// 2 - Aqua America
					lngTemp = 2;
				}
				else if (vbPorterVar == "R")
				{
					// 3 - RVS
					lngTemp = 3;
				}
				else if (vbPorterVar == "B")
				{
					// 4 - Badger
					lngTemp = 4;
				}
				else if (vbPorterVar == "D")
				{
					// 5 - TRIO DOS
					lngTemp = 5;
				}
				else if (vbPorterVar == "W")
				{
					// 6 - TRIO Windows
					lngTemp = 6;
				}
				else if (vbPorterVar == "X")
				{
					// 7 - TRIO DOS using the XRef1 field
					lngTemp = 7;
				}
				else if (vbPorterVar == "Q")
				{
					lngTemp = 8;
				}
				else if (vbPorterVar == "N")
				{
					// Northern data
					lngTemp = 9;
				}
				else if (vbPorterVar == "F")
				{
					lngTemp = 11;
				}
				else if (vbPorterVar == "Z")
				{
					lngTemp = 12;
				}
				else if (vbPorterVar == "O")
				{
					lngTemp = 13;
				}
				else if (vbPorterVar == "V")
				{
					// MAL@20070925 ; RVS Windows
					lngTemp = 14;
				}
				else if (vbPorterVar == "E")
				{
					// MAL@20071019
					lngTemp = 15;
				}
				else if (vbPorterVar == "M")
				{
					// MAL@20080925 ; Millinocket
					lngTemp = 16;
				}
				else if (vbPorterVar == "L")
				{
					// DJW@20090929 ; Lisbon
					lngTemp = 17;
				}
				else if (vbPorterVar == "I")
				{
					// DJW@20091120 ; Farmington
					lngTemp = 18;
				}
				else if (vbPorterVar == "9")
				{
					// kgk trout-754 ; Dover-Foxcroft
					lngTemp = 19;
				}
				else if (vbPorterVar == "G")
				{
					// kgk trouts-5 ; Bangor  05302013
					lngTemp = 20;
				}
				else if (vbPorterVar == "8")
				{
					// kk09022015 ; Veazie
					lngTemp = 21;
				}
				else if (vbPorterVar == "H")
				{
					// kjr trout-1245 ; Hampden 09092016
					lngTemp = 22;
				}
                else if (vbPorterVar == "7")
                {
                    lngTemp = 23;
                }
                else if (vbPorterVar == "S")
                {
                    lngTemp = 24;
                    cmbExportFormat.Visible = false;
                    lblExportFormat.Text = "VFlex Site Code";
                    txtVFlexSiteCode.Visible = true;
                    txtVFlexSiteCode.Text =
                        setController.GetSettingValue("VFlexSiteCode", "UtilityBilling", "", "", "");
                }
				cmbExportFormat.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rsSettings.Get_Fields_Int32("RemoteReaderExportType")));
				// kgk 12-19-2011 trout-767  Add option to export location in comment field
				if (cmbExportFormat.Text == "Extended")
				{
					chkShowLocation.Enabled = true;
				}
				else
				{
					chkShowLocation.Enabled = false;
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ExportLocation")))
				{
					chkShowLocation.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkShowLocation.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// kk01282014 trout-1009  Add option to force electronic data entry to use the default date
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ForceReadingDate")))
				{
					chkForceReadDate.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkForceReadDate.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				for (lngCT = 0; lngCT <= cmbExtractFileType.Items.Count - 1; lngCT++)
				{
					if (cmbExtractFileType.ItemData(lngCT) == lngTemp)
					{
						cmbExtractFileType.SelectedIndex = lngCT;
					}
				}
				txtConsIncrease.Text = Strings.Format(Conversion.Val(rsSettings.Get_Fields_Double("ConsumptionPercent")), "#0.00");
				// HideMinimumVarianceWarning
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("HideMinimumVarianceWarning")))
				{
					chkMinVarWarning.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkMinVarWarning.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// Flat Interest Amount - Initialize it to 0.00
				txtFlatInterestAmount.Text = "0.00";
				// Interest Type
				string vbPorterVar1 = rsSettings.Get_Fields_String("RegularIntMethod");
				if (vbPorterVar1 == "P")
				{
					// Per Diem
					cmbInterestType.Text = "Per Diem";
				}
				else if (vbPorterVar1 == "B")
				{
					// Billing
					cmbInterestType.Text = "Per Billing";
				}
				else if (vbPorterVar1 == "F")
				{
					// Flat
					cmbInterestType.Text = "Flat Rate";
					// Flat Interest Amount
					if (Conversion.Val(rsSettings.Get_Fields_Decimal("FlatInterestAmount")) != 0)
					{
						txtFlatInterestAmount.Text = Strings.Format(rsSettings.Get_Fields_Decimal("FlatInterestAmount"), "#,##0.00");
					}
					else
					{
						txtFlatInterestAmount.Text = "0.00";
					}
				}
				else if (vbPorterVar1 == "D")
				{
					// Demand
					cmbInterestType.Text = "On Demand";
				}
				else
				{
					cmbInterestType.Text = "Per Diem";
				}
				if (cmbInterestType.Text != "Flat Rate")
				{
					// Charge Interest on these
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ChargeIntPrin")))
					{
						chkInterest[0].CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkInterest[0].CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ChargeIntTax")))
					{
						chkInterest[1].CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkInterest[1].CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ChargeIntInt")))
					{
						chkInterest[2].CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkInterest[2].CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ChargeIntCost")))
					{
						chkInterest[3].CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkInterest[3].CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowLienAmountOnBill")))
				{
					chkShowLienAmtOnBill.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkShowLienAmtOnBill.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// kk 06242013 trout-6  Add option to enable/disable stormwater fee billing
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					chkBillStormwaterFee.Enabled = true;
					chkBillStormwaterFee.Visible = true;
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("BillStormwaterFee")))
					{
						chkBillStormwaterFee.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkBillStormwaterFee.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("SBillToOwner")))
				{
					cmbSBillTo.Text = "Owner";
				}
				else
				{
					cmbSBillTo.Text = "Tenant";
				}
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("WBillToOwner")))
				{
					cmbWBillTo.Text = "Owner";
				}
				else
				{
					cmbWBillTo.Text = "Tenant";
				}
				// 0 - Sequence
				// 1 - Owner Name
				// 2 - Tenant Name
				cmbDEOrder.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rsSettings.Get_Fields_Int32("DEOrder") + " "));
				// Billing Report Sequence
				// 0 - Book/Seq
				// 1 - Account Number
				// 2 - Name
				// 3 - Location
				// 4 - Map Lot
				cmbBillingRepSeq.SelectedIndex = FCConvert.ToInt32(Conversion.Val(rsSettings.Get_Fields_Int32("BillingRepSeq")));
				// MAL@20080116: 113597
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("AutoPrepayments")))
				{
					chkAutoPrepayments.CheckState = Wisej.Web.CheckState.Checked;
					if (rsSettings.Get_Fields_Int32("AutoPaymentApplyTo") == 1)
					{
						cboApplyTo.SelectedIndex = 0;
					}
					else if (rsSettings.Get_Fields_Int32("AutoPaymentApplyTo") == 2)
					{
						cboApplyTo.SelectedIndex = 1;
					}
					else if (rsSettings.Get_Fields_Int32("AutoPaymentApplyTo") == 0)
					{
						cboApplyTo.SelectedIndex = 0;
					}
					if (modUTStatusPayments.Statics.TownService == "B")
					{
						lblApplyTo.Enabled = true;
						cboApplyTo.Enabled = true;
					}
					else if ((modUTStatusPayments.Statics.TownService == "W") || (modUTStatusPayments.Statics.TownService == "S"))
					{
						cboApplyTo.SelectedIndex = 0;
						lblApplyTo.Enabled = false;
						cboApplyTo.Enabled = false;
					}
				}
				else
				{
					chkAutoPrepayments.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// MAL@20080624: 14371
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ShowTaxAcquiredCaption")))
				{
					chkShowTaxAcquired.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkShowTaxAcquired.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// MAL@20080812: Add Update RE Tax Acquired Status Option
				// Tracker Reference: 14035
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("UpdateRETaxAcquired")))
				{
					chkTAUpdateRE.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkTAUpdateRE.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// DJW@20090720: Add Check RE Tax Acquired Status Option
				// Tracker Reference: 14035
				if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("CheckRETaxAcquired")))
				{
					chkTACheckRE.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkTACheckRE.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// MAL@20080820: Disable IConnect Options if no IConnect Utility ID exists
				// Tracker Reference: 10680
				// kgk 10-13-2011 trout-733  Add Invoice Cloud
				if (!modGlobalConstants.Statics.gboolIC && !modGlobalConstants.Statics.gboolInvCld)
				{
					//FC:FINAL:CHN - issue #1030: incorrect menu ordering
					cmbOptions.Clear();
					cmbOptions.Items.Add("Adjustments");
					cmbOptions.Items.Add("Billing Options");
					cmbOptions.Items.Add("Bill Options");
					cmbOptions.Items.Add("Payment Options");
					cmbOptions.Items.Add("Interest / Lien Options");
					cmbOptions.Items.Add("Other Options");
					cmbOptions.Items.Add("Default Reports");
					cmbOptions.Items.Add("Default Account Options");
					//cmbOptions.Items.Add("Set Signature File");
					cmbOptions.Items.Add("Bill Detail Descriptions");
					//optOptions[9].Enabled = false;
					cmbOptions.SelectedIndex = 0;
				}
				else if (modGlobalConstants.Statics.gboolIC)
				{
					fraIConnect.Text = "IConnect Options";
					//optOptions[9].Text = "IConnect Options";
					//optOptions[9].Enabled = true;
					//FC:FINAL:CHN - issue #1030: incorrect menu ordering
					cmbOptions.Clear();
					cmbOptions.Items.Add("Adjustments");
					cmbOptions.Items.Add("Billing Options");
					cmbOptions.Items.Add("Bill Options");
					cmbOptions.Items.Add("Payment Options");
					cmbOptions.Items.Add("Interest / Lien Options");
					cmbOptions.Items.Add("Other Options");
					cmbOptions.Items.Add("Default Reports");
					cmbOptions.Items.Add("Default Account Options");
					//  cmbOptions.Items.Add("Set Signature File");
					cmbOptions.Items.Add("IConnect Options");
					cmbOptions.Items.Add("Bill Detail Descriptions");
					cmbOptions.Text = "IConnect Options";
					if (modGlobalConstants.Statics.gboolCR)
					{
						chkUseCRAccounts.Enabled = true;
						if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ICUseCRAccounts")))
						{
							chkUseCRAccounts.CheckState = Wisej.Web.CheckState.Checked;
							cmdSetupICAccounts.Enabled = false;
						}
						else
						{
							chkUseCRAccounts.CheckState = Wisej.Web.CheckState.Unchecked;
							cmdSetupICAccounts.Enabled = true;
						}
					}
					else
					{
						chkUseCRAccounts.Enabled = false;
						cmdSetupICAccounts.Enabled = true;
					}
					if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("ICMFeeAccount"))) != "")
					{
						txtMFeeAccount.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("ICMFeeAccount")));
					}
					else
					{
						txtMFeeAccount.TextMatrix(0, 0, "");
					}
					lblInvStartDate.Visible = false;
					txtInvStartDate.Visible = false;
					txtInvStartDate.Enabled = false;
				}
				else if (modGlobalConstants.Statics.gboolInvCld)
				{
					fraIConnect.Text = "Invoice Cloud Options";
					cmbOptions.Clear();
					cmbOptions.Items.Add("Adjustments");
					cmbOptions.Items.Add("Billing Options");
                    cmbOptions.Items.Add("Bill Options");
                    cmbOptions.Items.Add("Payment Options");
					cmbOptions.Items.Add("Interest / Lien Options");
					cmbOptions.Items.Add("Other Options");
					cmbOptions.Items.Add("Default Reports");
					cmbOptions.Items.Add("Default Account Options");
					//cmbOptions.Items.Add("Set Signature File");
					cmbOptions.Items.Add("Invoice Cloud Options");
					cmbOptions.Items.Add("Bill Detail Descriptions");
                    //FC:FINAL:BSE #3960 wrong label
					cmbOptions.Text = "Adjustments";
					//optOptions[9].Text = "Invoice Cloud Options";
					//optOptions[9].Enabled = true;
					if (modGlobalConstants.Statics.gboolCR)
					{
						chkUseCRAccounts.Enabled = true;
						if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("ICUseCRAccounts")))
						{
							chkUseCRAccounts.CheckState = Wisej.Web.CheckState.Checked;
							cmdSetupICAccounts.Enabled = false;
						}
						else
						{
							chkUseCRAccounts.CheckState = Wisej.Web.CheckState.Unchecked;
							cmdSetupICAccounts.Enabled = true;
						}
					}
					else
					{
						chkUseCRAccounts.Enabled = false;
						cmdSetupICAccounts.Enabled = true;
					}
					if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("ICMFeeAccount"))) != "")
					{
						txtMFeeAccount.TextMatrix(0, 0, FCConvert.ToString(rsSettings.Get_Fields_String("ICMFeeAccount")));
					}
					else
					{
						txtMFeeAccount.TextMatrix(0, 0, "");
					}
					lblInvStartDate.Visible = true;
					txtInvStartDate.Visible = true;
					txtInvStartDate.Enabled = true;
					txtInvStartDate.Text = Strings.Format(rsSettings.Get_Fields_DateTime("InvCldInvStartDate"), "MM/dd/yyyy");
				}
				// save the default analysis reports to show
				rsDefaults.OpenRecordset("SELECT * FROM DefaultAnalysisReports", modExtraModules.strUTDatabase);
				if (rsDefaults.EndOfFile())
				{
					rsDefaults.AddNew();
					rsDefaults.Update();
				}
				else
				{
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("BillingSummaryS")))
					{
						chkBillingSummary.CheckState = Wisej.Web.CheckState.Checked;
					}
					if (modUTStatusPayments.Statics.TownService != "W")
					{
						// Sewer
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("DollarAmountS")))
						{
							chkDollarAmountsS.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ConsumptionS")))
						{
							chkConsumptionsS.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("BillCountS")))
						{
							chkBillCountS.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("MeterReportS")))
						{
							chkMeterReportS.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SalesTaxS")))
						{
							chkSalesTaxS.CheckState = Wisej.Web.CheckState.Checked;
						}
					}
					if (modUTStatusPayments.Statics.TownService != "S")
					{
						// Water
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("DollarAmountW")))
						{
							chkDollarAmountsW.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ConsumptionW")))
						{
							chkConsumptionsW.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("BillCountW")))
						{
							chkBillCountW.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("MeterReportW")))
						{
							chkMeterReportW.CheckState = Wisej.Web.CheckState.Checked;
						}
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SalesTaxW")))
						{
							chkSalesTaxW.CheckState = Wisej.Web.CheckState.Checked;
						}
					}
				}
				// kgk 03-04-11 trout-683  Add custom report detail description overrides
				txtPastDue.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_PastDue"));
				txtCredit.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_Credit"));
				txtRegular.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_Regular"));
				txtOverride.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_Override"));
				txtInterest.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_Interest"));
				txtLiens.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_Liens"));
				txtTax.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_Tax"));
				txtMisc.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_Misc"));
				txtAdj.Text = FCConvert.ToString(rsSettings.Get_Fields_String("RptDesc_Adj"));
				// kgk 03-24-11 trout-684  Jay - limit 1st cons billing to 2x prev flat billing
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
				{
					chkLimitConsBill.Enabled = true;
					chkLimitConsBill.Visible = true;
					if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("RateTypeChange")))
					{
						chkLimitConsBill.CheckState = Wisej.Web.CheckState.Checked;
					}
				}
				boolDirty = false;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Default Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveSettings())
			{
				this.Unload();
			}
		}

		private void optBasis_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optBasis_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbBasis.SelectedIndex;
			optBasis_CheckedChanged(index, sender, e);
		}

		private void optDEType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optDEType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbDEType.SelectedIndex;
			optDEType_CheckedChanged(index, sender, e);
		}

		private void optExportFormat_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			// kgk 12-19-2011 trout-767
			boolDirty = true;
			if (Index == 1)
			{
				chkShowLocation.Enabled = true;
			}
			else
			{
				chkShowLocation.CheckState = Wisej.Web.CheckState.Unchecked;
				chkShowLocation.Enabled = false;
			}
		}

		private void optExportFormat_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbExportFormat.SelectedIndex;
			optExportFormat_CheckedChanged(index, sender, e);
		}

		private void optInterestType_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
				case 1:
				case 3:
					{
						lblFlatInterestRate.Enabled = false;
						txtFlatInterestAmount.Enabled = false;
						chkInterest[0].Enabled = true;
						chkInterest[1].Enabled = true;
						chkInterest[2].Enabled = true;
						chkInterest[3].Enabled = true;
						break;
					}
				case 2:
					{
						lblFlatInterestRate.Enabled = true;
						txtFlatInterestAmount.Enabled = true;
						chkInterest[0].Enabled = false;
						chkInterest[1].Enabled = false;
						chkInterest[2].Enabled = false;
						chkInterest[3].Enabled = false;
						break;
					}
			}
			//end switch
		}

		private void optInterestType_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbInterestType.SelectedIndex;
			optInterestType_CheckedChanged(index, sender, e);
		}

		private void optOptions_CheckedChanged(string item, object sender, System.EventArgs e)
		{
			ShowFrame(item);
		}

		public void optOptions_Click(string item)
		{
			optOptions_CheckedChanged(item, cmbOptions, new System.EventArgs());
		}

		private void optOptions_CheckedChanged(object sender, System.EventArgs e)
		{
			//int index = cmbOptions.SelectedIndex;
			if (cmbOptions.SelectedItem != null)
			{
				string item = cmbOptions.SelectedItem.ToString();
				optOptions_CheckedChanged(item, sender, e);
			}
		}

		private void optPayFirst_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optPayFirst_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbPayFirst.SelectedIndex;
			optPayFirst_CheckedChanged(index, sender, e);
		}

		private void optReceiptSize_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optReceiptSize_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReceiptSize.SelectedIndex;
			optReceiptSize_CheckedChanged(index, sender, e);
		}

		private void optSBillTo_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optSBillTo_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSBillTo.SelectedIndex;
			optSBillTo_CheckedChanged(index, sender, e);
		}

		private void optSeq_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optSeq_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbSeq.SelectedIndex;
			optSeq_CheckedChanged(index, sender, e);
		}

		private void optWBillTo_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void optWBillTo_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbWBillTo.SelectedIndex;
			optWBillTo_CheckedChanged(index, sender, e);
		}

		private void txtCMFAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtCMFAdjust_Enter(object sender, System.EventArgs e)
		{
			txtCMFAdjust.SelectionStart = 0;
			txtCMFAdjust.SelectionLength = txtCMFAdjust.Text.Length;
		}

		private void txtCMFAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// MAL@20080321: Add check for '-' (45) to add negative values
			// Tracker Reference: 12787
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtCMFAdjust_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtCMFAdjust.Text) >= 10)
			{
				txtCMFAdjust.Text = "10";
			}
			if (Conversion.Val(txtCMFAdjust.Text) <= 0)
			{
				txtCMFAdjust.Text = "0";
			}
		}

		private void txtConsIncrease_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtConsIncrease_Enter(object sender, System.EventArgs e)
		{
			txtConsIncrease.SelectionStart = 0;
			txtConsIncrease.SelectionLength = txtConsIncrease.Text.Length;
		}

		private void txtConsIncrease_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (KeyAscii == Keys.Delete))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtConsIncrease_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtConsIncrease.Text) <= 100 && Conversion.Val(txtConsIncrease.Text) >= 0)
			{
				txtConsIncrease.Text = Strings.Format(Conversion.Val(txtConsIncrease.Text), "#0.00");
			}
			else
			{
				txtConsIncrease.Text = "0.00";
			}
		}

		private void txtDiscountPercent_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtDiscountPercent_Enter(object sender, System.EventArgs e)
		{
			txtDiscountPercent.SelectionStart = 0;
			txtDiscountPercent.SelectionLength = txtDiscountPercent.Text.Length;
		}

		private void txtDiscountPercent_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (KeyAscii == Keys.Delete))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDMHLabelAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtDMHLabelAdjust_Enter(object sender, System.EventArgs e)
		{
			txtDMHLabelAdjust.SelectionStart = 0;
			txtDMHLabelAdjust.SelectionLength = txtDMHLabelAdjust.Text.Length;
		}

		private void txtDMHLabelAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// MAL@20080321: Add check for '-' (45) to add negative values
			// Tracker Reference: 12787
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDMHLabelAdjust_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtDMHLabelAdjust.Text) >= 4)
			{
				txtDMHLabelAdjust.Text = "4";
			}
			if (Conversion.Val(txtDMHLabelAdjust.Text) <= 0)
			{
				txtDMHLabelAdjust.Text = "0";
			}
		}

		private void txtMFeeAccount_Change()
		{
			boolDirty = true;
		}

		private void txtMFeeAccount_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtMFeeAccount.TextMatrix(0, 0));
				if (strAcct != "")
				{
					txtMFeeAccount.TextMatrix(0, 0, strAcct);
				}
			}
		}

		private void txtSigAdjust_Lien_TextChanged(object sender, System.EventArgs e)
		{
			// MAL@20080618: Add adjustment for Lien Notice signature
			// Tracker Reference: 14327
			boolDirty = true;
		}

		private void txtSigAdjust_Lien_Enter(object sender, System.EventArgs e)
		{
			txtSigAdjust_Lien.SelectionStart = 0;
			txtSigAdjust_Lien.SelectionLength = txtSigAdjust_Lien.Text.Length;
		}

		private void txtSigAdjust_Lien_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// MAL@20080321: Add check for '-' (45) to add negative values
			// Tracker Reference: 12787
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSigAdjust_Lien_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// kk02082016 trout-1197  Changed signature adjustment from +/-6 Lines to 0-1.3 inches
			if (Conversion.Val(txtSigAdjust_Lien.Text) >= 0.5)
			{
				// kjr 12.7.16 trout-807  Change limit to match max line movement in form of .5 inch
				txtSigAdjust_Lien.Text = ".50";
			}
			if (Conversion.Val(txtSigAdjust_Lien.Text) <= 0)
			{
				txtSigAdjust_Lien.Text = "0.00";
			}
		}

		private void txtDMVLabelAdjust_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtDMVLabelAdjust_Enter(object sender, System.EventArgs e)
		{
			txtDMVLabelAdjust.SelectionStart = 0;
			txtDMVLabelAdjust.SelectionLength = txtDMVLabelAdjust.Text.Length;
		}

		private void txtDMVLabelAdjust_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// MAL@20080321: Add check for '-' (45) to add negative values
			// Tracker Reference: 12787
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtDMVLabelAdjust_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtDMVLabelAdjust.Text) >= 4)
			{
				txtDMVLabelAdjust.Text = "4";
			}
			if (Conversion.Val(txtDMVLabelAdjust.Text) <= 0)
			{
				txtDMVLabelAdjust.Text = "0";
			}
		}

		private void txtFlatInterestAmount_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtFlatInterestAmount_Enter(object sender, System.EventArgs e)
		{
			txtFlatInterestAmount.SelectionStart = 0;
			txtFlatInterestAmount.SelectionLength = txtFlatInterestAmount.Text.Length;
		}

		private void txtFlatInterestAmount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (KeyAscii == Keys.Delete))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLabelAdjustment_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLabelAdjustment_Enter(object sender, System.EventArgs e)
		{
			txtLabelAdjustment.SelectionStart = 0;
			txtLabelAdjustment.SelectionLength = txtLabelAdjustment.Text.Length;
		}

		private void txtLabelAdjustment_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// MAL@20080321: Add check for '-' (45) to add negative values
			// Tracker Reference: 12787
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtLabelAdjustment_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtLabelAdjustment.Text) >= 4)
			{
				txtLabelAdjustment.Text = "4";
			}
			if (Conversion.Val(txtLabelAdjustment.Text) <= 0)
			{
				txtLabelAdjustment.Text = "0";
			}
		}

		private void txtMeterSlipsHAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// MAL@20080321: Add check for '-' (45) to add negative values
			// Tracker Reference: 12787
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMeterSlipsHAdj_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMeterSlipsHAdj.Text) >= 4)
			{
				txtMeterSlipsHAdj.Text = "4";
			}
			if (Conversion.Val(txtMeterSlipsHAdj.Text) <= 0)
			{
				txtMeterSlipsHAdj.Text = "0";
			}
		}

		private void txtMeterSlipsVAdj_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// MAL@20080321: Add check for '-' (45) to add negative values
			// Tracker Reference: 12787
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMeterSlipsVAdj_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtMeterSlipsVAdj.Text) >= 4)
			{
				txtMeterSlipsVAdj.Text = "4";
			}
			if (Conversion.Val(txtMeterSlipsVAdj.Text) <= 0)
			{
				txtMeterSlipsVAdj.Text = "0";
			}
		}

		private void txtPPayInterestRate_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtPPayInterestRate_Enter(object sender, System.EventArgs e)
		{
			txtPPayInterestRate.SelectionStart = 0;
			txtPPayInterestRate.SelectionLength = txtPPayInterestRate.Text.Length;
		}

		private void txtPPayInterestRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Decimal) || (KeyAscii == Keys.Delete))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtInvStartDate_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtInvStartDate_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strTest;
			strTest = txtInvStartDate.Text;
			if (!Information.IsDate(strTest))
			{
				MessageBox.Show("Please enter a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void ShowFrame(string item)
		{
			fraBilling.Visible = false;
			fraAdjustments.Visible = false;
			fraPaymentOptions.Visible = false;
			fraOther.Visible = false;
			fraInterest.Visible = false;
			fraDefaultAnalysisReports.Visible = false;
			fraBillOptions.Visible = false;
			fraDefaultOptions.Visible = false;
			fraIConnect.Visible = false;
			fraBillDetDesc.Visible = false;
			// kgk
			switch (item)
			{
				case "Adjustments":
					{
						// this is the adjustments frame
						//fraAdjustments.Top = fraCustomize.Top;
						fraAdjustments.Visible = true;
						break;
					}
				case "Billing Options":
					{
						// this is the billing options frame
						//fraBilling.Top = fraCustomize.Top;
						//fraBilling.Left = fraAdjustments.Left;
						fraBilling.Visible = true;
						break;
					}
				case "Payment Options":
					{
						// this is the payment options frame
						//fraPaymentOptions.Top = fraCustomize.Top;
						//fraPaymentOptions.Left = fraAdjustments.Left;
						fraPaymentOptions.Visible = true;
						break;
					}
				case "Interest / Lien Options":
					{
						// Interest Options
						//fraInterest.Top = fraCustomize.Top;
						//fraInterest.Left = fraAdjustments.Left;
						fraInterest.Visible = true;
						break;
					}
				case "Other Options":
					{
						// this is the other options frame
						//fraOther.Top = fraCustomize.Top;
						//fraOther.Left = fraAdjustments.Left;
						fraOther.Visible = true;
						break;
					}
				case "Default Reports":
					{
						// this is the default analysis reports frame
						//fraDefaultAnalysisReports.Top = fraCustomize.Top;
						//fraDefaultAnalysisReports.Left = fraAdjustments.Left;
						fraDefaultAnalysisReports.Visible = true;
						break;
					}
				case "Bill Options":
					{
						// Bill Options
						//fraBillOptions.Top = fraCustomize.Top;
						//fraBillOptions.Left = fraAdjustments.Left;
						fraBillOptions.Visible = true;
						break;
					}
				case "Default Account Options":
					{
						// Default Options
						//fraDefaultOptions.Top = fraCustomize.Top;
						//fraDefaultOptions.Left = fraAdjustments.Left;
						fraDefaultOptions.Visible = true;
						break;
					}
                case "Invoice Cloud Options":
                case "IConnect Options":
					{
						// IConnect Options
						//fraIConnect.Top = fraCustomize.Top;
						//fraIConnect.Left = fraAdjustments.Left;
						fraIConnect.Visible = true;
						break;
					}
				case "Bill Detail Descriptions":
					{
						// kgk trout-683  Add option to custom descriptions to custom reports
						//fraBillDetDesc.Top = fraCustomize.Top;
						//fraBillDetDesc.Left = fraAdjustments.Left;
						fraBillDetDesc.Visible = true;
						break;
					}
				default:
					break;
			}
			//end switch
		}

		private void txtPrinAcctS_Change()
		{
			boolDirty = true;
		}

		private void txtPrinAcctS_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtPrinAcctS.TextMatrix(0, 0));
				if (strAcct != "")
				{
					txtPrinAcctS.TextMatrix(0, 0, strAcct);
				}
			}
		}

		private void txtPrinAcctW_Change()
		{
			boolDirty = true;
		}

		private void txtPrinAcctW_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtPrinAcctW.TextMatrix(0, 0));
				if (strAcct != "")
				{
					txtPrinAcctW.TextMatrix(0, 0, strAcct);
				}
			}
		}

		private void txtTaxAcctS_Change()
		{
			boolDirty = true;
		}

		private void txtTaxAcctS_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtTaxAcctS.TextMatrix(0, 0));
				if (strAcct != "")
				{
					txtTaxAcctS.TextMatrix(0, 0, strAcct);
				}
			}
		}

		private void txtTaxAcctW_Change()
		{
			boolDirty = true;
		}

		private void txtTaxAcctW_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtTaxAcctW.TextMatrix(0, 0));
				if (strAcct != "")
				{
					txtTaxAcctW.TextMatrix(0, 0, strAcct);
				}
			}
		}

		private void LoadCombos()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBill = new clsDRWrapper();
				// Custom Bill Types
				rsBill.OpenRecordset("SELECT * FROM CustomBills", modExtraModules.strUTDatabase);
				cmbDefaultBill.Clear();
				while (!rsBill.EndOfFile())
				{
					cmbDefaultBill.AddItem(rsBill.Get_Fields_String("FormatName"));
					cmbDefaultBill.ItemData(cmbDefaultBill.NewIndex, FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID")));
					rsBill.MoveNext();
				}
				cmbDefaultBill.AddItem("Outprinting File");
				cmbDefaultBill.ItemData(cmbDefaultBill.NewIndex, modCoreysUTCode.CNSTCUSTOMBILLTYPEOUTPRINT);
				// State
				rsBill.OpenRecordset("SELECT * FROM tblStates WHERE CountryCode = 'US' ORDER BY StateCode", "CentralData");
				cmbState.Clear();
				while (!rsBill.EndOfFile())
				{
					cmbState.AddItem(rsBill.Get_Fields_String("StateCode") + " - " + rsBill.Get_Fields_String("Description"));
					rsBill.MoveNext();
				}

				cmbExtractFileType.Clear();
				cmbExtractFileType.AddItem("Ti Sales");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 0);
				cmbExtractFileType.AddItem("Prescott Book / Sequence");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 1);
				cmbExtractFileType.AddItem("Prescott Account / Meter");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 8);
				cmbExtractFileType.AddItem("Aqua America");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 2);
				cmbExtractFileType.AddItem("RVS");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 3);
				cmbExtractFileType.AddItem("RVS Windows");
				// MAL@20070925
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 14);
				cmbExtractFileType.AddItem("Badger");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 4);
				cmbExtractFileType.AddItem("TRIO DOS");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 5);
				cmbExtractFileType.AddItem("TRIO DOS - XRef");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 7);
				cmbExtractFileType.AddItem("TRIO Windows");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 6);
				cmbExtractFileType.AddItem("TRIO Windows - XRef");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 10);
				cmbExtractFileType.AddItem("EZ Reader");
				// MAL@20071019
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 15);
                cmbExtractFileType.AddItem(@"VFlex / Sensus Analytics");
                cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 24);
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "ELIOT")
				{
					cmbExtractFileType.AddItem("NDS");
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 9);
				}
				cmbExtractFileType.AddItem("NDS 2");
				cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 11);
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BELFAST")
				{
					cmbExtractFileType.AddItem("NDS - Belfast");
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 12);
				}
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "OAKLAND")
				{
					cmbExtractFileType.AddItem("Oakland");
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 13);
				}
				// MAL@20080925: Millinocket Import
				// Tracker Reference: 15568
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "MILLINOCKET")
				{
					cmbExtractFileType.AddItem("Millinocket");
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 16);
				}
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
				{
					cmbExtractFileType.AddItem("Lisbon");
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 17);
				}
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "FARMINGTON")
				{
					cmbExtractFileType.AddItem("Farmington");
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 18);
				}
				// kgk trout-754 09-22-2011
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "DOVER FOXCROFT")
				{
					cmbExtractFileType.AddItem("Dover Foxcroft");
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 19);
				}
				// kk trouts-5 03122013  Add Bangor Sewer import from Water District bill outprint file
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					cmbExtractFileType.AddItem("Bangor");
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 20);
				}
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "VEAZIE" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "ORONO")
				{
					cmbExtractFileType.AddItem("Veazie");
					// kgk 06122013 trout-968  Veazie
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 21);
				}
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 7)) == "HAMPDEN")
				{
					cmbExtractFileType.AddItem("Hampden");
					// kjr 09092016 trout-1245 Hampden custom
					cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 22);
				}

                if (modGlobalConstants.Statics.MuniName.ToLower().StartsWith("topsham se") ||
                    modGlobalConstants.Statics.MuniName.ToLower().StartsWith("brunswick se"))
                {
                    cmbExtractFileType.AddItem(@"Brunswick/Topsham");
                    cmbExtractFileType.ItemData(cmbExtractFileType.NewIndex, 23);
                }
				// Data Entry Order
				cmbDEOrder.Clear();
				cmbDEOrder.AddItem("Sequence");
				cmbDEOrder.ItemData(cmbDEOrder.NewIndex, 0);
				cmbDEOrder.AddItem("Owner Name");
				cmbDEOrder.ItemData(cmbDEOrder.NewIndex, 1);
				cmbDEOrder.AddItem("Tenant Name");
				cmbDEOrder.ItemData(cmbDEOrder.NewIndex, 2);
				cmbDEOrder.AddItem("Account Number");
				cmbDEOrder.ItemData(cmbDEOrder.NewIndex, 3);
				// Town Service
				cmbService.Clear();
				cmbService.AddItem("Both");
				cmbService.AddItem("Water");
				cmbService.AddItem("Sewer");
				// Digits
				cmbDefaultDigits.Clear();
				cmbDefaultDigits.AddItem("3");
				cmbDefaultDigits.AddItem("4");
				cmbDefaultDigits.AddItem("5");
				cmbDefaultDigits.AddItem("6");
				cmbDefaultDigits.AddItem("7");
				cmbDefaultDigits.AddItem("8");
				cmbDefaultDigits.AddItem("9");
				// Size
				rsBill.OpenRecordset("SELECT * FROM MeterSizes", modExtraModules.strUTDatabase);
				cmbDefaultSize.Clear();
				while (!rsBill.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbDefaultSize.AddItem(rsBill.Get_Fields("Code") + " - " + rsBill.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbDefaultSize.ItemData(cmbDefaultSize.NewIndex, FCConvert.ToInt32(rsBill.Get_Fields("Code")));
					rsBill.MoveNext();
				}
				// Frequency
				rsBill.OpenRecordset("SELECT * FROM Frequency", modExtraModules.strUTDatabase);
				cmbDefaultFrequency.Clear();
				while (!rsBill.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbDefaultFrequency.AddItem(rsBill.Get_Fields("Code") + " - " + rsBill.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbDefaultFrequency.ItemData(cmbDefaultFrequency.NewIndex, FCConvert.ToInt32(rsBill.Get_Fields("Code")));
					rsBill.MoveNext();
				}
				// CategoryW
				// CategoryS
				rsBill.OpenRecordset("SELECT * FROM Category", modExtraModules.strUTDatabase);
				cmbDefaultCategoryS.Clear();
				cmbDefaultCategoryW.Clear();
				while (!rsBill.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbDefaultCategoryS.AddItem(rsBill.Get_Fields("Code") + " - " + rsBill.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbDefaultCategoryS.ItemData(cmbDefaultCategoryS.NewIndex, FCConvert.ToInt32(rsBill.Get_Fields("Code")));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbDefaultCategoryW.AddItem(rsBill.Get_Fields("Code") + " - " + rsBill.Get_Fields_String("ShortDescription"));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbDefaultCategoryW.ItemData(cmbDefaultCategoryW.NewIndex, FCConvert.ToInt32(rsBill.Get_Fields("Code")));
					rsBill.MoveNext();
				}
				// Reading Units on bill
				cmbUnitsOnBill.Clear();
				cmbUnitsOnBill.AddItem("1");
				cmbUnitsOnBill.ItemData(cmbUnitsOnBill.NewIndex, 1);
				cmbUnitsOnBill.AddItem("10");
				cmbUnitsOnBill.ItemData(cmbUnitsOnBill.NewIndex, 10);
				cmbUnitsOnBill.AddItem("100");
				cmbUnitsOnBill.ItemData(cmbUnitsOnBill.NewIndex, 100);
				cmbUnitsOnBill.AddItem("1000");
				cmbUnitsOnBill.ItemData(cmbUnitsOnBill.NewIndex, 1000);
				// Town Reading Units
				cmbMultiplier.Clear();
				cmbMultiplier.AddItem("1");
				cmbMultiplier.ItemData(cmbMultiplier.NewIndex, 1);
				cmbMultiplier.AddItem("10");
				cmbMultiplier.ItemData(cmbMultiplier.NewIndex, 10);
				cmbMultiplier.AddItem("100");
				cmbMultiplier.ItemData(cmbMultiplier.NewIndex, 100);
				cmbMultiplier.AddItem("1000");
				cmbMultiplier.ItemData(cmbMultiplier.NewIndex, 1000);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Combos", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtTaxRate_Enter(object sender, System.EventArgs e)
		{
			txtTaxRate.SelectionStart = 0;
			txtTaxRate.SelectionLength = txtTaxRate.Text.Length;
		}

		private void txtTaxRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii == Keys.Back) || (KeyAscii == Keys.Delete) || (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9))
			{

			}
			else if ((KeyAscii == Keys.NumPad3) || (KeyAscii == Keys.C))
			{
				// c, C - this will clear the box
				KeyAscii = (Keys)0;
				txtTaxRate.Text = "0.00";
				txtTaxRate.SelectionStart = 0;
				txtTaxRate.SelectionLength = 4;
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void LoadToolTips()
		{
			ToolTip1.SetToolTip(lblDEOrder, "This will determine the order in which the meters will be listed in the Data Entry screen.");
			ToolTip1.SetToolTip(cmbDEOrder, ToolTip1.GetToolTip(lblDEOrder));
			// "This will determine the order in which the meters will be listed in the Data Entry screen."
			// Dot Matrix Adjustments
			ToolTip1.SetToolTip(txtDMHLabelAdjust, "This is the horizontal adjustment in characters. Valid values are 0 to 4, including fractions.");
			ToolTip1.SetToolTip(txtDMVLabelAdjust, "This is the vertical adjustment in lines. Valid values are 0 to 4, including fractions.");
			// Laser Adjustments
			ToolTip1.SetToolTip(txtCMFAdjust, "This is the CMF adjustment in lines. Valid values are 0 to 10, including fractions.");
			ToolTip1.SetToolTip(txtLabelAdjustment, "This is the laser label adjustment in lines. Valid values are 0 to 4, including fractions.");
			ToolTip1.SetToolTip(txtSigAdjust_Lien, "This is the vertical adjustment in inches. Valid values are 0 to .5, including fractions.");
			// Meter Reading Slip Adjustmants
			ToolTip1.SetToolTip(txtMeterSlipsHAdj, "This is the horizontal adjustment. Valid values are 0 to 4, including fractions.");
			ToolTip1.SetToolTip(txtMeterSlipsVAdj, "This is the vertical adjustment. Valid values are 0 to 4, including fractions.");
			// Registry Adjustments for filings
			ToolTip1.SetToolTip(txtLienTopAdj, "This is the top margin adjustment in inches. Valid values are -1.5 to 4, including fractions.");
			ToolTip1.SetToolTip(txtLienBottomAdj, "This is the bottom margin adjustment in inches. Valid values are -1.5 to 4, including fractions.");
			ToolTip1.SetToolTip(txtLienSideAdj, "This is the side margin adjustment in inches. Valid values are -0.75 to 1, including fractions.");
			ToolTip1.SetToolTip(txtLDNTopAdj, "This is the top margin adjustment in inches. Valid values are -1.5 to 4, including fractions.");
			ToolTip1.SetToolTip(txtLDNBottomAdj, "This is the bottom margin adjustment in inches. Valid values are -1.5 to 4, including fractions.");
			ToolTip1.SetToolTip(txtLDNSideAdj, "This is the side margin adjustment in inches. Valid values are -0.75 to 1, including fractions.");
			ToolTip1.SetToolTip(cmbExportFormat, "Select the remote reader export file type.");
		}

		private void vsReturnAddress_Enter(object sender, System.EventArgs e)
		{
			vsReturnAddress.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void vsReturnAddress_RowColChange(object sender, System.EventArgs e)
		{
			if (vsReturnAddress.Row == vsReturnAddress.Rows - 1)
			{
				vsReturnAddress.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsReturnAddress.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void FillApplyToList()
		{
			cboApplyTo.AddItem("Primary Service", 0);
			cboApplyTo.AddItem("Paid Service", 1);
		}

		private bool ValidateAdjustments(ref string strError)
		{
			bool ValidateAdjustments = false;
			// kk08072015 trocl-1250  Rewrite this so it's easier to follow and reports more than 1 issue at a time.
			bool blnOK;
			blnOK = true;
			// Optimistic
			strError = "";
			// CMF Laser
			if (FCConvert.ToDouble(txtCMFAdjust.Text) > 10 || FCConvert.ToDouble(txtCMFAdjust.Text) < 0)
			{
				blnOK = false;
				strError += "Certified Mail Form Adjustment must be between 0 and 10" + "\r\n";
			}
			// Label Laser
			if (FCConvert.ToDouble(txtLabelAdjustment.Text) > 4 || FCConvert.ToDouble(txtLabelAdjustment.Text) < 0)
			{
				blnOK = false;
				strError += "Laser Label Vertical Adjustment must be between 0 and 4" + "\r\n";
			}
			// Label H Dot Matrix
			if (FCConvert.ToDouble(txtDMHLabelAdjust.Text) > 4 || FCConvert.ToDouble(txtDMHLabelAdjust.Text) < 0)
			{
				blnOK = false;
				strError += "Dot Matrix Label Horizontal Adjustment must be between 0 and 4" + "\r\n";
			}
			// Label V Dot Matrix
			if (FCConvert.ToDouble(txtDMVLabelAdjust.Text) > 4 || FCConvert.ToDouble(txtDMVLabelAdjust.Text) < 0)
			{
				blnOK = false;
				strError += "Dot Matrix Label Vertical Adjustment must be between 0 and 4" + "\r\n";
			}
			// Meter Slip H                                                          'kk09192014 0-4 twips??
			if (FCConvert.ToDouble(txtMeterSlipsHAdj.Text) > 4 || FCConvert.ToDouble(txtMeterSlipsHAdj.Text) < 0)
			{
				blnOK = false;
				strError += "Meter Slip Horizontal ... " + "\r\n";
			}
			// Meter Slip V
			if (FCConvert.ToDouble(txtMeterSlipsVAdj.Text) > 4 || FCConvert.ToDouble(txtMeterSlipsVAdj.Text) < 0)
			{
				// kk09192014 0-4 twips??
				blnOK = false;
				strError += "Meter Slip Vertical ... " + "\r\n";
			}
			// kk05082014 trout-1082  Add lien doc adjustments
			// Lien Top -  - Default margin is 1.5" so allow -1.5 to 4" adjustment      'kk02082016 trout-1197  Change def to 1.5"
			if (Conversion.Val(txtLienTopAdj.Text) > 4 || Conversion.Val(txtLienTopAdj.Text) < -1.5)
			{
				// kk08062015 trout-1151    >= 0
				blnOK = false;
				strError += "Lien Notice Top Adjustment must be between -1.5 and 4 inches" + "\r\n";
			}
			// Lien Bottom -  - Default margin is 1.5" so allow -1.5 to 4" adjustment   'kk02082016 trout-1197  Change def to 1.5"
			if (Conversion.Val(txtLienBottomAdj.Text) > 4 || Conversion.Val(txtLienBottomAdj.Text) < -1.5)
			{
				// kk08062015 trout-1151    >= 0
				blnOK = false;
				strError += "Lien Notice Bottom Adjustment must be between -1.5 and 4 inches" + "\r\n";
			}
			// kk02082016 trout-1197  New specs for registry filings. Removed the top right box.  Added side margin adjustments
			// Lien Side - Default margin is 0.75" so allow -0.75 to 1" adjustment; Applies to left and right sides
			if (Conversion.Val(txtLienSideAdj.Text) > 1 || Conversion.Val(txtLienSideAdj.Text) < -0.75)
			{
				blnOK = false;
				strError += "Lien Notice Side Adjustment must be between -0.75 and 1 inches" + "\r\n";
			}
			// Lien Discharge Top - Default margin is 1.5" so allow -1.5 to 4" adjustment            'kk02082016 trout-1197  Change to -1.5
			if (Conversion.Val(txtLDNTopAdj.Text) > 4 || Conversion.Val(txtLDNTopAdj.Text) < -1.5)
			{
				// kk08062015 trout-1151    >= 0
				blnOK = false;
				strError += "Lien Discharge Notice Top Adjustment must be between -1.5 and 4 inches" + "\r\n";
			}
			// Lien Discharge Bottom - Default margin is 1.5" so allow -1.5 to 4" adjustment         'kk02082016 trout-1197  Change def to 1.5"
			if (Conversion.Val(txtLDNBottomAdj.Text) > 4 || Conversion.Val(txtLDNBottomAdj.Text) < -1.5)
			{
				// kk08062015 trout-1151    >= 0
				blnOK = false;
				strError += "Lien Discharge Notice Bottom Adjustment must be between -1.5 and 4 inches" + "\r\n";
			}
			// Lien Discharge Side - Default margin is 0.75" so allow -0.75 to 1" adjustment; Applies to left and right sides   'kk02082016 trout-1197  Added side margin adjustments
			if (Conversion.Val(txtLDNSideAdj.Text) > 1 || Conversion.Val(txtLDNSideAdj.Text) < -0.75)
			{
				blnOK = false;
				strError += "Lien Discharge Notice Side Adjustment must be between -0.75 and 1 inches" + "\r\n";
			}
			// Lien Notice Signature        'kk02082016 trout-1197  Change to 0 to 1.3 inches 'kjr 12.7.16 trout-807  Change limit to match max line movement in form of .5 inch
			if (Conversion.Val(txtSigAdjust_Lien.Text) > 0.5 || Conversion.Val(txtSigAdjust_Lien.Text) < 0)
			{
				blnOK = false;
				strError += "Lien Notice Signature Adjustment must be between 0 and .5 inches" + "\r\n";
			}
			ValidateAdjustments = blnOK;
			return ValidateAdjustments;
		}

		private void UpdateIConnectAccounts()
		{
			// Tracker Reference: 10680
			clsDRWrapper rsAcct = new clsDRWrapper();
			rsAcct.OpenRecordset("SELECT * FROM tblIConnectAccountInfo", modExtraModules.strUTDatabase);
			if (rsAcct.RecordCount() > 0)
			{
				while (!rsAcct.EndOfFile())
				{
					if (modUTStatusPayments.Statics.TownService == "S")
					{
						if (rsAcct.Get_Fields_Int32("TypeCode") == 94 || rsAcct.Get_Fields_Int32("TypeCode") == 95 || rsAcct.Get_Fields_Int32("TypeCode") == 894 || rsAcct.Get_Fields_Int32("TypeCode") == 895)
						{
							rsAcct.Edit();
							rsAcct.Set_Fields("Active", true);
							rsAcct.Update();
						}
						else
						{
							rsAcct.Edit();
							rsAcct.Set_Fields("Active", false);
							rsAcct.Update();
							break;
						}
					}
					else if (modUTStatusPayments.Statics.TownService == "W")
					{
						if (rsAcct.Get_Fields_Int32("TypeCode") == 93 || rsAcct.Get_Fields_Int32("TypeCode") == 96 || rsAcct.Get_Fields_Int32("TypeCode") == 893 || rsAcct.Get_Fields_Int32("TypeCode") == 896)
						{
							rsAcct.Edit();
							rsAcct.Set_Fields("Active", true);
							rsAcct.Update();
						}
						else
						{
							rsAcct.Edit();
							rsAcct.Set_Fields("Active", false);
							rsAcct.Update();
						}
					}
					else if (modUTStatusPayments.Statics.TownService == "B")
					{
						rsAcct.Edit();
						rsAcct.Set_Fields("Active", true);
						rsAcct.Update();
					}
					rsAcct.MoveNext();
				}
			}
		}
	}
}
