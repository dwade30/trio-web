﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptCalculationDetail.
	partial class srptCalculationDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptCalculationDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblDescription = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWAmt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSAmt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnSubtotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblWarning = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWAmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSAmt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarning)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblDescription,
				this.fldWAmt,
				this.fldSAmt
			});
			this.Detail.Height = 0.1388889F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotals,
				this.fldWTotal,
				this.fldSTotal,
				this.lnSubtotals,
				this.lblWarning
			});
			this.GroupFooter1.Height = 0.3229167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblDescription
			// 
			this.lblDescription.Height = 0.1527778F;
			this.lblDescription.HyperLink = null;
			this.lblDescription.Left = 1.0625F;
			this.lblDescription.Name = "lblDescription";
			this.lblDescription.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.lblDescription.Text = null;
			this.lblDescription.Top = 0F;
			this.lblDescription.Width = 1.25F;
			// 
			// fldWAmt
			// 
			this.fldWAmt.Height = 0.1388889F;
			this.fldWAmt.Left = 2.3125F;
			this.fldWAmt.Name = "fldWAmt";
			this.fldWAmt.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldWAmt.Text = "0.00";
			this.fldWAmt.Top = 0F;
			this.fldWAmt.Width = 0.6875F;
			// 
			// fldSAmt
			// 
			this.fldSAmt.Height = 0.1388889F;
			this.fldSAmt.Left = 3F;
			this.fldSAmt.Name = "fldSAmt";
			this.fldSAmt.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldSAmt.Text = "0.00";
			this.fldSAmt.Top = 0F;
			this.fldSAmt.Width = 0.6875F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1388889F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 1.375F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.lblTotals.Text = "Total";
			this.lblTotals.Top = 0F;
			this.lblTotals.Width = 0.9375F;
			// 
			// fldWTotal
			// 
			this.fldWTotal.Height = 0.1388889F;
			this.fldWTotal.Left = 2.3125F;
			this.fldWTotal.Name = "fldWTotal";
			this.fldWTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldWTotal.Text = "0.00";
			this.fldWTotal.Top = 0F;
			this.fldWTotal.Width = 0.6875F;
			// 
			// fldSTotal
			// 
			this.fldSTotal.Height = 0.1388889F;
			this.fldSTotal.Left = 3F;
			this.fldSTotal.Name = "fldSTotal";
			this.fldSTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.fldSTotal.Text = "0.00";
			this.fldSTotal.Top = 0F;
			this.fldSTotal.Width = 0.6875F;
			// 
			// lnSubtotals
			// 
			this.lnSubtotals.Height = 0F;
			this.lnSubtotals.Left = 2.3125F;
			this.lnSubtotals.LineWeight = 1F;
			this.lnSubtotals.Name = "lnSubtotals";
			this.lnSubtotals.Top = 0F;
			this.lnSubtotals.Width = 1.375F;
			this.lnSubtotals.X1 = 2.3125F;
			this.lnSubtotals.X2 = 3.6875F;
			this.lnSubtotals.Y1 = 0F;
			this.lnSubtotals.Y2 = 0F;
			// 
			// lblWarning
			// 
			this.lblWarning.Height = 0.1388889F;
			this.lblWarning.HyperLink = null;
			this.lblWarning.Left = 0.4375F;
			this.lblWarning.Name = "lblWarning";
			this.lblWarning.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblWarning.Text = null;
			this.lblWarning.Top = 0.1388889F;
			this.lblWarning.Width = 6F;
			// 
			// srptCalculationDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.45F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWAmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSAmt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarning)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWAmt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSAmt;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSubtotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWarning;
	}
}
