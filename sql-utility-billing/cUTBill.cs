//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Collections.Generic;


namespace TWUT0000
{
	public class cUTBill
	{

		//=========================================================

		private cUTBillParty OwnParty = new/*AsNew*/ cUTBillParty();
		private cUTBillParty TenParty = new/*AsNew*/ cUTBillParty();
		private Dictionary<string, cUTBillUtility> utilityDetails = new/*AsNew*/ Dictionary<string, cUTBillUtility>();
		private Dictionary<int, cUTBillMeterDetail> meterDetails = new/*AsNew*/ Dictionary<int, cUTBillMeterDetail>();
		private UTMailToParty partyToMailTo;
		private string strMapLot;
		private string strLocation;
		private string strMessage;
		private int intBook;
		private int lngActualAccountNumber;
		private string strCategory;
		private double dblImperviousSurfaceArea;
		private cUTReading currReading = new/*AsNew*/ cUTReading();
		private cUTReading prevReading = new/*AsNew*/ cUTReading();
		private cUTBillRateInformation rateInfo = new/*AsNew*/ cUTBillRateInformation();
		private string strComment;
		private UTBillType intServiceType;
		private DateTime dtStatementDate;
		private int lngConsumption;
		private bool boolWaterBillOwner;
		private bool boolSewerBillOwner;

		public enum UTMailToParty
		{
			Owner = 0,
			Tenant = 1
		};

		public enum UTBillType
		{
			water = 0,
			Sewer = 1,
			both = 2
		};

		public bool SewerBillToOwner
		{
			set
			{
				boolSewerBillOwner = value;
			}

			get
			{
					bool SewerBillToOwner = false;
				SewerBillToOwner = boolSewerBillOwner;
				return SewerBillToOwner;
			}
		}



		public bool WaterBillToOwner
		{
			set
			{
				boolWaterBillOwner = value;
			}

			get
			{
					bool WaterBillToOwner = false;
				WaterBillToOwner = boolWaterBillOwner;
				return WaterBillToOwner;
			}
		}



		public object SetPartyToMailTo(UTMailToParty whichParty)
		{
			object SetPartyToMailTo = null;
			partyToMailTo = whichParty;
			return SetPartyToMailTo;
		}

		public int Consumption
		{
			set
			{
				lngConsumption = value;
			}

			get
			{
					int Consumption = 0;
				Consumption = lngConsumption;
				return Consumption;
			}
		}



		public DateTime StatementDate
		{
			set
			{
				dtStatementDate = value;
			}

			get
			{
					DateTime StatementDate = System.DateTime.Now;
				StatementDate = dtStatementDate;
				return StatementDate;
			}
		}



		public UTBillType ServiceType
		{
			set
			{
				intServiceType = value;
			}

			get
			{
					UTBillType ServiceType = (UTBillType)0;
				ServiceType = intServiceType;
				return ServiceType;
			}
		}



		public string Comment
		{
			set
			{
				strComment = value;
			}

			get
			{
					string Comment = "";
				Comment = strComment;
				return Comment;
			}
		}



		public cUTBillRateInformation RateInformation
		{
			get
			{
					cUTBillRateInformation RateInformation = null;
				RateInformation = rateInfo;
				return RateInformation;
			}
		}

		public cUTReading GetPreviousReading()
		{
			cUTReading GetPreviousReading = null;
			GetPreviousReading = prevReading.GetCopy();
			return GetPreviousReading;
		}

		public cUTReading GetCurrentReading()
		{
			return  currReading.GetCopy();
		}

		public void SetCurrentReading(ref cUTReading currRead)
		{
			currReading = currRead.GetCopy();
		}

		public void SetPreviousReading(ref cUTReading prevRead)
		{
			prevReading = prevRead.GetCopy();
		}

		public double ImperviousSurfaceArea
		{
			set
			{
				dblImperviousSurfaceArea = value;
			}

			get
			{
					double ImperviousSurfaceArea = 0;
				ImperviousSurfaceArea = dblImperviousSurfaceArea;
				return ImperviousSurfaceArea;
			}
		}



		public string Category
		{
			set
			{
				strCategory = value;
			}

			get
			{
					string Category = "";
				Category = strCategory;
				return Category;
			}
		}



		public int AccountNumber
		{
			set
			{
				lngActualAccountNumber = value;
			}

			get
			{
					int AccountNumber = 0;
				AccountNumber = lngActualAccountNumber;
				return AccountNumber;
			}
		}



		public int Book
        {
            set
            {
                intBook = value;
            }
            get
            {
                return intBook;
            }
        }



		public string Message
		{
			set
			{
				strMessage = value;
			}

			get
			{
					string Message = "";
				Message = strMessage;
				return Message;
			}
		}



		public string Location
		{
			set
			{
				strLocation = value;
			}

			get
			{
					string Location = "";
				Location = strLocation;
				return Location;
			}
		}



		public string MapLot
		{
			set
			{
				strMapLot = value;
			}

			get
			{
					string MapLot = "";
				MapLot = strMapLot;
				return MapLot;
			}
		}



		public bool TenantIsSameAsOwner()
		{
			bool TenantIsSameAsOwner = false;
			TenantIsSameAsOwner = OwnParty.IsSame(ref TenParty);
			return TenantIsSameAsOwner;
		}

		public cUTBillParty GetMailToParty()
		{
			cUTBillParty GetMailToParty = null;
			if (partyToMailTo==UTMailToParty.Tenant) {
				GetMailToParty = TenParty.GetCopy();
			} else {
				GetMailToParty = OwnParty.GetCopy();
			}
			return GetMailToParty;
		}

		public void SetTenantInformation(ref cUTBillParty partyInfo)
		{
			TenParty = partyInfo.GetCopy();
		}

		public void SetOwnerInformation(ref cUTBillParty partyInfo)
		{
			OwnParty = partyInfo.GetCopy();
		}

		public double GetTotalConsumption()
		{
			double GetTotalConsumption = 0;
			double dblTotal = 0;

            foreach (var mDetail in meterDetails.Values)
            {
                dblTotal += mDetail.Consumption;
            }
			GetTotalConsumption = dblTotal;
			return GetTotalConsumption;
		}

		public cUTBillMeterDetail GetMeterDetailsForMeter(short intMeter)
		{
			cUTBillMeterDetail GetMeterDetailsForMeter = null;
			// vbPorter upgrade warning: mDetail As cUTBillMeterDetail	OnWrite(object)
			cUTBillMeterDetail mDetail;
			if (meterDetails.ContainsKey(intMeter)) {
				mDetail = meterDetails[intMeter];
				GetMeterDetailsForMeter = mDetail.GetCopy();
			} else {
				GetMeterDetailsForMeter = new cUTBillMeterDetail();
			}
			return GetMeterDetailsForMeter;
		}

		public void SetMeterDetailsForMeter(short intMeter, ref cUTBillMeterDetail meterDetail)
		{
			cUTBillMeterDetail mDetail;
			mDetail = meterDetail.GetCopy();
			if (meterDetails.ContainsKey(intMeter)) {
				meterDetails[intMeter] = mDetail;
			} else {
				meterDetails.Add(intMeter, mDetail);
			}
		}

		public cUTBillParty OwnerParty
		{
			get
			{
					cUTBillParty OwnerParty = null;
				OwnerParty = OwnParty;
				return OwnerParty;
			}
		}

		public cUTBillParty TenantParty
		{
			get
			{
					cUTBillParty TenantParty = null;
				TenantParty = TenParty;
				return TenantParty;
			}
		}

		public cUTBillUtility GetUtilityDetailsForType(string strUtilityType)
		{
			cUTBillUtility GetUtilityDetailsForType = null;
			if (utilityDetails.ContainsKey(strUtilityType)) {
				GetUtilityDetailsForType = utilityDetails[strUtilityType];
			} else {
				GetUtilityDetailsForType = new cUTBillUtility();
			}
			return GetUtilityDetailsForType;
		}

		public void SetUtilityChargesForType(string strUtilityType, cUTBillUtility utilDetails)
		{
			if (!utilityDetails.ContainsKey(strUtilityType)) {
				utilityDetails.Add(strUtilityType, utilDetails);
			} else {
				utilityDetails[strUtilityType] = utilDetails;
			}
		}

		public double GetOverallTotalPastDue()
		{
			double dblReturn = 0;
            foreach (var curUtil in utilityDetails.Values)
            {
                dblReturn += FCUtils.Round(curUtil.GetTotalPastDue(), 2);
            }
			return dblReturn;
		}

		public double GetOverallTotalDue()
		{
			double dblReturn = 0;
			foreach(var currUtil in utilityDetails.Values)
            {

				dblReturn += FCUtils.Round(currUtil.TotalDue, 2);
			}
			return dblReturn;
		}

		public double GetOverallCurrentDue()
		{
			double dblReturn = 0;
            foreach (var currUtil in utilityDetails.Values)
            {
                dblReturn += FCUtils.Round(currUtil.CurrentDue, 2);
            }
            return dblReturn;
        }

	}
}
