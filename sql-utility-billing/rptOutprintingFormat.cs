﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptOutprintingFormat.
	/// </summary>
	public partial class rptOutprintingFormat : BaseSectionReport
	{
		public rptOutprintingFormat()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Outprinting Format";
		}

		public static rptOutprintingFormat InstancePtr
		{
			get
			{
				return (rptOutprintingFormat)Sys.GetInstance(typeof(rptOutprintingFormat));
			}
		}

		protected rptOutprintingFormat _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOutprintingFormat	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/28/2006              *
		// ********************************************************
		bool boolDone;
		int lngCount;
		string lngAmountFieldLength;
		// vbPorter upgrade warning: lngTotalSize As int	OnWriteFCConvert.ToDouble(
		int lngTotalSize;
		bool boolChptr660;
		bool boolInclChangeOutCons;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			clsDRWrapper rsCfg = new clsDRWrapper();
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngCount = 1;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngAmountFieldLength = "8";
			rsCfg.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
			if (!rsCfg.EndOfFile())
			{
				boolChptr660 = FCConvert.ToBoolean(rsCfg.Get_Fields_Boolean("OutprintChapt660"));
				boolInclChangeOutCons = FCConvert.ToBoolean(rsCfg.Get_Fields_Int32("OutPrintIncludeChangeOutCons"));
				// trout-1118 12.4.17 kjr
			}
			rsCfg.Dispose();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				boolDone = false;
				lblFieldName.Text = "";
				lblSize.Text = "";
				fldDescription.Text = "";
				switch (lngCount)
				{
					case 1:
						{
							lblFieldName.Text = " A - Account";
							lblSize.Text = "10";
							fldDescription.Text = "Account Number";
							break;
						}
					case 2:
						{
							lblFieldName.Text = " B - Billed Name";
							lblSize.Text = "34";
							fldDescription.Text = "Billed Name";
							break;
						}
					case 3:
						{
							lblFieldName.Text = " C - Billed Name 2";
							lblSize.Text = "34";
							fldDescription.Text = "Second Billed Name";
							break;
						}
					case 4:
						{
							lblFieldName.Text = " D - Address 1";
							lblSize.Text = "34";
							fldDescription.Text = "Current Address";
							break;
						}
					case 5:
						{
							lblFieldName.Text = " E - Address 2";
							lblSize.Text = "34";
							fldDescription.Text = "";
							break;
						}
					case 6:
						{
							lblFieldName.Text = " F - Address 3";
							lblSize.Text = "34";
							fldDescription.Text = "";
							break;
						}
					case 7:
						{
							lblFieldName.Text = " G - City";
							lblSize.Text = "24";
							fldDescription.Text = "";
							break;
						}
					case 8:
						{
							lblFieldName.Text = " H - State";
							lblSize.Text = "2";
							fldDescription.Text = "Two character code for the state.";
							break;
						}
					case 9:
						{
							lblFieldName.Text = " I - Zip";
							lblSize.Text = "5";
							fldDescription.Text = "Zip Code";
							break;
						}
					case 10:
						{
							lblFieldName.Text = " J - Zip 4";
							lblSize.Text = "5";
							fldDescription.Text = "Four character zipcode preceded by a hyphen if a value exists.";
							break;
						}
					case 11:
						{
							lblFieldName.Text = " K - Category";
							lblSize.Text = "25";
							fldDescription.Text = "Category Description";
							break;
						}
					case 12:
						{
							lblFieldName.Text = " L - Map Lot";
							lblSize.Text = "17";
							fldDescription.Text = "Current Map and Lot";
							break;
						}
					case 13:
						{
							lblFieldName.Text = " M - Location";
							lblSize.Text = "32";
							fldDescription.Text = "Location of the Property";
							break;
						}
					case 14:
						{
							lblFieldName.Text = " N - Bill Message";
							lblSize.Text = "255";
							fldDescription.Text = "Bill Message from the Master screen.";
							break;
						}
					case 15:
						{
							lblFieldName.Text = " O - Statement Date";
							lblSize.Text = "8";
							fldDescription.Text = "Mailing Date";
							break;
						}
					case 16:
						{
							lblFieldName.Text = " P - Billing Period";
							lblSize.Text = "7";
							fldDescription.Text = "Billing Period Covered";
							break;
						}
					case 17:
						{
							lblFieldName.Text = " Q - Reading Date";
							lblSize.Text = "8";
							fldDescription.Text = "Current Reading Date";
							break;
						}
					case 18:
						{
							lblFieldName.Text = " R - Start Date";
							lblSize.Text = "8";
							fldDescription.Text = "Billing Period Start Date";
							break;
						}
					case 19:
						{
							lblFieldName.Text = " S - End Date";
							lblSize.Text = "8";
							fldDescription.Text = "Billing Period End Date";
							break;
						}
					case 20:
						{
							lblFieldName.Text = " T - Interest Date";
							lblSize.Text = "8";
							fldDescription.Text = "Interest Start Date";
							break;
						}
					case 21:
						{
							lblFieldName.Text = " U - Total Overall Current";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Current due for this account.";
							break;
						}
					case 22:
						{
							lblFieldName.Text = " V - Total Overall Past Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Past due for this account.";
							break;
						}
					case 23:
						{
							lblFieldName.Text = " W - Overall Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount due for this account.";
							break;
						}
					case 24:
						{
							lblFieldName.Text = " X - Book";
							lblSize.Text = "4";
							fldDescription.Text = "Book Number";
							break;
						}
					case 25:
						{
							lblFieldName.Text = " Y - Sequence";
							lblSize.Text = "5";
							fldDescription.Text = "Sequence Number";
							break;
						}
					case 26:
						{
							lblFieldName.Text = " Z - Water Consumption";
							lblSize.Text = "6";
							fldDescription.Text = "Billed Consumption";
							break;
						}
					case 27:
						{
							lblFieldName.Text = "AA - Water Unit Amount";
							lblSize.Text = "4";
							fldDescription.Text = "Number of water units setup for this meter.";
							break;
						}
					case 28:
						{
							lblFieldName.Text = "AB - Water Consumption Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for water consumption.";
							break;
						}
					case 29:
						{
							lblFieldName.Text = "AC - Water Units Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for water units.";
							break;
						}
					case 30:
						{
							lblFieldName.Text = "AD - Water Flat Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for water flat rate.";
							break;
						}
					case 31:
						{
							lblFieldName.Text = "AE - Water Adjustment Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for water adjustments.";
							break;
						}
					case 32:
						{
							lblFieldName.Text = "AF - Water Total w/o Adjs";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for water, not including adjustments.";
							break;
						}
					case 33:
						{
							lblFieldName.Text = "AG - Water Regular";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for the period selected up to the date of the outprinting.";
							break;
						}
					case 34:
						{
							lblFieldName.Text = "AH - Water Tax";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Tax amount for the period selected up to the date of the outprinting.";
							break;
						}
					case 35:
						{
							lblFieldName.Text = "AI - Water Past Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Non lien amount due previous to the period selected up to the date of the outprinting if it is positive.";
							break;
						}
					case 36:
						{
							lblFieldName.Text = "AJ - Water Credits";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Non lien amount due previous to the period selected up to the date of the outprinting if it is negative.";
							break;
						}
					case 37:
						{
							lblFieldName.Text = "AK - Water Total Amount Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount due for the the whole account up to the day of the outprinting.";
							break;
						}
					case 38:
						{
							lblFieldName.Text = "AL - Water Interest";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Current interest calculated to the date of the outprinting.";
							break;
						}
					case 39:
						{
							lblFieldName.Text = "AM - Water Lien Amount";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Lien amount due previous to the period selected up to the date of the outprinting.";
							break;
						}
					case 40:
						{
							lblFieldName.Text = "AN - Water Current Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount due for the period selected up to the date of the outprinting.";
							break;
						}
					case 41:
						{
							lblFieldName.Text = "AO - Total Water Past Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount due previous to the period selected up to the date of the outprinting.";
							break;
						}
					case 42:
						{
							lblFieldName.Text = "AP - Sewer Consumption";
							lblSize.Text = "6";
							fldDescription.Text = "Billed Consumption";
							break;
						}
					case 43:
						{
							lblFieldName.Text = "AQ - Sewer Unit Amount";
							lblSize.Text = "4";
							fldDescription.Text = "Number of sewer units setup for this meter.";
							break;
						}
					case 44:
						{
							lblFieldName.Text = "AR - Sewer Consumption Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for sewer consumption.";
							break;
						}
					case 45:
						{
							lblFieldName.Text = "AS - Sewer Units Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for sewer units.";
							break;
						}
					case 46:
						{
							lblFieldName.Text = "AT - Sewer Flat Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for sewer flat rate.";
							break;
						}
					case 47:
						{
							lblFieldName.Text = "AU - Sewer Adjustment Total";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for sewer adjustments.";
							break;
						}
					case 48:
						{
							lblFieldName.Text = "AV - Sewer Total w/o Adjs";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for sewer, not including adjustments.";
							break;
						}
					case 49:
						{
							lblFieldName.Text = "AW - Sewer Regular";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount charged for the period selected up to the date of the outprinting.";
							break;
						}
					case 50:
						{
							lblFieldName.Text = "AX - Sewer Tax";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Tax amount for the period selected up to the date of the outprinting.";
							break;
						}
					case 51:
						{
							lblFieldName.Text = "AY - Sewer Past Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Non lien amount due previous to the period selected up to the date of the outprinting.";
							break;
						}
					case 52:
						{
							lblFieldName.Text = "AZ - Sewer Credits";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Non lien amount due previous to the period selected up to the date of the outprinting if it is negative.";
							break;
						}
					case 53:
						{
							lblFieldName.Text = "BA - Sewer Total Amount Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount due for the the whole account up to the day of the outprinting.";
							break;
						}
					case 54:
						{
							lblFieldName.Text = "BB - Sewer Interest";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Current interest calculated to the date of the outprinting.";
							break;
						}
					case 55:
						{
							lblFieldName.Text = "BC - Sewer Lien Amount";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Lien amount due previous to the period selected up to the date of the outprinting.";
							break;
						}
					case 56:
						{
							lblFieldName.Text = "BD - Sewer Current Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount due for the period selected up to the date of the outprinting.";
							break;
						}
					case 57:
						{
							lblFieldName.Text = "BE - Total Sewer Past Due";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount due previous to the period selected up to the date of the outprinting.";
							break;
						}
					case 58:
						{
							lblFieldName.Text = "BF - Meter 1 Consumption";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount of consumption for Meter 1.";
							break;
						}
					case 59:
						{
							lblFieldName.Text = "BG - Meter 1 Previous";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Previous Reading for Meter 1.";
							break;
						}
					case 60:
						{
							lblFieldName.Text = "BH - Meter 1 Current";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Current Reading for Meter 1.";
							break;
						}
					case 61:
						{
							lblFieldName.Text = "BI - Meter 2 Consumption";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount of consumption for Meter 2.";
							break;
						}
					case 62:
						{
							lblFieldName.Text = "BJ - Meter 2 Previous";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Previous Reading for Meter 2.";
							break;
						}
					case 63:
						{
							lblFieldName.Text = "BK - Meter 2 Current";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Current Reading for Meter 2.";
							break;
						}
					case 64:
						{
							lblFieldName.Text = "BL - Meter 3 Consumption";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Amount of consumption for all other meters.";
							break;
						}
					case 65:
						{
							lblFieldName.Text = "BM - Meter 3 Previous";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Previous Reading for all other meters.";
							break;
						}
					case 66:
						{
							lblFieldName.Text = "BN - Meter 3 Current";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Current Reading for all other meters.";
							break;
						}
					case 67:
						{
							lblFieldName.Text = "BO - Amount A";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Split 1 For Water.";
							break;
						}
					case 68:
						{
							lblFieldName.Text = "BP - Amount B";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Split 2 For Water.";
							break;
						}
					case 69:
						{
							lblFieldName.Text = "BQ - Amount C";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Split 1 For Sewer.";
							break;
						}
					case 70:
						{
							lblFieldName.Text = "BR - Amount D";
							lblSize.Text = lngAmountFieldLength;
							fldDescription.Text = "Split 2 For Sewer.";
							break;
						}
					case 71:
						{
							if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
							{
								lblFieldName.Text = "BS - Note";
								lblSize.Text = lngAmountFieldLength;
								fldDescription.Text = "Account Comment.";
							}
							else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
							{
								// trout-684 add actual consumption amount to outprint file
								lblFieldName.Text = "BS - Actual Consumption Amount";
								lblSize.Text = lngAmountFieldLength;
								fldDescription.Text = "Actual Consumption Amount.";
							}
							else
							{
								lblFieldName.Text = "BS - Due Date";
								lblSize.Text = lngAmountFieldLength;
								fldDescription.Text = "Payment Due Date.";
							}
							break;
						}
					case 72:
						{
							if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
							{
								lblFieldName.Text = "BT - Due Date";
								lblSize.Text = lngAmountFieldLength;
								fldDescription.Text = "Payment Due Date.";
							}
							else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
							{
								lblFieldName.Text = "BT - Due Date";
								lblSize.Text = lngAmountFieldLength;
								fldDescription.Text = "Payment Due Date.";
							}
							else if (boolChptr660)
							{
								// kgk 10-20-2011 trout-766  Chapt 660 rules
								lblFieldName.Text = "BT - Meter 1 Est/Act";
								lblSize.Text = lngAmountFieldLength;
								fldDescription.Text = "Meter 1 Estimated or Actual Reading.";
							}
							else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
							{
								lblFieldName.Text = "BT - Owner Name";
								lblSize.Text = "34";
								fldDescription.Text = "Owner Name.";
							}
							else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
							{
								lblFieldName.Text = "BT - Impervious Surface";
								lblSize.Text = "34";
								fldDescription.Text = "Impervious Surface.";
							}
							else if (boolInclChangeOutCons)
							{
								// trout-1118 12.4.17 kjr
								lblFieldName.Text = "BT - Meter 1 Change Out Consumption";
								lblSize.Text = lngAmountFieldLength;
								fldDescription.Text = "Meter 1 Change Out Consumption.";
							}
							else
							{
								boolDone = true;
							}
							break;
						}
					case 73:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "BU - Meter 1 Est/Act";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 1 Estimated or Actual Reading.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "BU - Meter 1 Est/Act";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 1 Estimated or Actual Reading.";
								}
								else
								{
									lblFieldName.Text = "BU - Meter 1 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 1 Changed Out.";
								}
							}
							else
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
								{
									lblFieldName.Text = "BU - Owner Name 2";
									lblSize.Text = "34";
									fldDescription.Text = "Second Owner Name.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
								{
									lblFieldName.Text = "BU - Previous Reading Date";
									lblSize.Text = "34";
									fldDescription.Text = "Previous Reading Date.";
								}
								else if (boolInclChangeOutCons == true)
								{
									// trout-1118 12.4.17 kjr
									lblFieldName.Text = "BU - Meter 2 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Change Out Consumption.";
								}
								else
								{
									boolDone = true;
								}
							}
							break;
						}
					case 74:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "BV - Meter 1 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 1 Changed Out.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "BV - Meter 1 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 1 Changed Out.";
								}
								else
								{
									lblFieldName.Text = "BV - Meter 2 Est/Act";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Estimated or Actual Reading.";
								}
							}
							else
							{
								if (boolInclChangeOutCons == true)
								{
									// trout-1118 12.4.17 kjr
									if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
									{
										lblFieldName.Text = "BV - Meter 2 Changed Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 2 Changed Out Consumption.";
									}
									else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
									{
										lblFieldName.Text = "BV - Meter 2 Changed Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 2 Changed Out Consumption.";
									}
									else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
									{
										lblFieldName.Text = "BV - Meter 1 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 1 Change Out Consumption.";
									}
									else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
									{
										lblFieldName.Text = "BV - Meter 1 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 1 Change Out Consumption.";
									}
									else
									{
										lblFieldName.Text = "BV - Meter 3 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 3 Change Out Consumption.";
									}
								}
								else
								{
									boolDone = true;
								}
							}
							break;
						}
					case 75:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "BW - Meter 2 Est/Act";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Estimated or Actual Reading.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "BW - Meter 2 Est/Act";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Estimated or Actual Reading.";
								}
								else
								{
									lblFieldName.Text = "BW - Meter 2 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Changed Out.";
								}
							}
							else
							{
								if (boolInclChangeOutCons == true)
								{
									// trout-1118 12.4.17 kjr
									if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
									{
										lblFieldName.Text = "BW - Meter 3 Changed Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 3 Changed Out Consumption.";
									}
									else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
									{
										lblFieldName.Text = "BW - Meter 3 Changed Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 3 Changed Out Consumption.";
									}
									else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
									{
										lblFieldName.Text = "BW - Meter 2 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 2 Change Out Consumption.";
									}
									else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
									{
										lblFieldName.Text = "BW - Meter 2 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 2 Change Out Consumption.";
									}
								}
								else
								{
									boolDone = true;
								}
							}
							break;
						}
					case 76:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "BX - Meter 2 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Changed Out.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "BX - Meter 2 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Changed Out.";
								}
								else
								{
									lblFieldName.Text = "BX - Meter 3 Est/Act";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Estimated or Actual Reading.";
								}
							}
							else
							{
								if (boolInclChangeOutCons == true)
								{
									// trout-1118 12.4.17 kjr
									if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
									{
										lblFieldName.Text = "BX - Meter 3 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 3 Change Out Consumption.";
									}
									else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
									{
										lblFieldName.Text = "BX - Meter 3 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 3 Change Out Consumption.";
									}
								}
							}
							break;
						}
					case 77:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "BY - Meter 3 Est/Act";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Estimated or Actual Reading.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "BY - Meter 3 Est/Act";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Estimated or Actual Reading.";
								}
								else
								{
									lblFieldName.Text = "BY - Meter 3 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Changed Out.";
								}
							}
							break;
						}
					case 78:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "BZ - Meter 3 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Changed Out.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "BZ - Meter 3 Changed Out";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Changed Out.";
								}
								else
								{
									lblFieldName.Text = "BZ - Water Adj 1 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 1 Description.";
								}
							}
							break;
						}
					case 79:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CA - Water Adj 1 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 1 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CA - Water Adj 1 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 1 Description.";
								}
								else
								{
									lblFieldName.Text = "CA - Water Adj 1 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 1 Amount.";
								}
							}
							break;
						}
					case 80:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CB - Water Adj 1 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 1 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CB - Water Adj 1 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 1 Amount.";
								}
								else
								{
									lblFieldName.Text = "CB - Water Adj 2 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 2 Description.";
								}
							}
							break;
						}
					case 81:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CC - Water Adj 2 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 2 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CC - Water Adj 2 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 2 Description.";
								}
								else
								{
									lblFieldName.Text = "CC - Water Adj 2 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 2 Amount.";
								}
							}
							break;
						}
					case 82:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CD - Water Adj 2 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 2 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CD - Water Adj 2 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 2 Amount.";
								}
								else
								{
									lblFieldName.Text = "CD - Water Adj 3 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 3 Description.";
								}
							}
							break;
						}
					case 83:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CE - Water Adj 3 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 3 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CE - Water Adj 3 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 3 Description.";
								}
								else
								{
									lblFieldName.Text = "CE - Water Adj 3 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 3 Amount.";
								}
							}
							break;
						}
					case 84:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CF - Water Adj 3 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 3 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CF - Water Adj 3 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 3 Amount.";
								}
								else
								{
									lblFieldName.Text = "CF - Water Adj 4 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 4 Description.";
								}
							}
							break;
						}
					case 85:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CG - Water Adj 4 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 4 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CG - Water Adj 4 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 4 Description.";
								}
								else
								{
									lblFieldName.Text = "CG - Water Adj 4 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 4 Amount.";
								}
							}
							break;
						}
					case 86:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CH - Water Adj 4 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 4 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CH - Water Adj 4 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 4 Amount.";
								}
								else
								{
									lblFieldName.Text = "CH - Water Adj 5 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 5 Description.";
								}
							}
							break;
						}
					case 87:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CI - Water Adj 5 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 5 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CI - Water Adj 5 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 5 Description.";
								}
								else
								{
									lblFieldName.Text = "CI - Water Adj 5 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 5 Amount.";
								}
							}
							break;
						}
					case 88:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CJ - Water Adj 5 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 5 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CJ - Water Adj 5 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Water Adjustment 5 Amount.";
								}
								else
								{
									lblFieldName.Text = "CJ - Sewer Adj 1 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 1 Description.";
								}
							}
							break;
						}
					case 89:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CK - Sewer Adj 1 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 1 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CK - Sewer Adj 1 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 1 Description.";
								}
								else
								{
									lblFieldName.Text = "CK - Sewer Adj 1 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 1 Amount.";
								}
							}
							break;
						}
					case 90:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CL - Sewer Adj 1 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 1 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CL - Sewer Adj 1 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 1 Amount.";
								}
								else
								{
									lblFieldName.Text = "CL - Sewer Adj 2 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 2 Description.";
								}
							}
							break;
						}
					case 91:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CM - Sewer Adj 2 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 2 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CM - Sewer Adj 2 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 2 Description.";
								}
								else
								{
									lblFieldName.Text = "CM - Sewer Adj 2 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 2 Amount.";
								}
							}
							break;
						}
					case 92:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CN - Sewer Adj 2 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 2 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CN - Sewer Adj 2 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 2 Amount.";
								}
								else
								{
									lblFieldName.Text = "CN - Sewer Adj 3 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 3 Description.";
								}
							}
							break;
						}
					case 93:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CO - Sewer Adj 3 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 3 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CO - Sewer Adj 3 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 3 Description.";
								}
								else
								{
									lblFieldName.Text = "CO - Sewer Adj 3 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 3 Amount.";
								}
							}
							break;
						}
					case 94:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CP - Sewer Adj 3 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 3 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CP - Sewer Adj 3 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 3 Amount.";
								}
								else
								{
									lblFieldName.Text = "CP - Sewer Adj 4 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 4 Description.";
								}
							}
							break;
						}
					case 95:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CQ - Sewer Adj 4 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 4 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CQ - Sewer Adj 4 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 4 Description.";
								}
								else
								{
									lblFieldName.Text = "CQ - Sewer Adj 4 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 4 Amount.";
								}
							}
							break;
						}
					case 96:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CR - Sewer Adj 4 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 4 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CR - Sewer Adj 4 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 4 Amount.";
								}
								else
								{
									lblFieldName.Text = "CR - Sewer Adj 5 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 5 Description.";
								}
							}
							break;
						}
					case 97:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CS - Sewer Adj 5 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 5 Description.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CS - Sewer Adj 5 Descr";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 5 Description.";
								}
								else
								{
									lblFieldName.Text = "CS - Sewer Adj 5 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 5 Amount.";
								}
							}
							break;
						}
					case 98:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
								{
									lblFieldName.Text = "CT - Owner Name";
									lblSize.Text = "34";
									fldDescription.Text = "Owner Name.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
								{
									lblFieldName.Text = "CT - Impervious Surface";
									lblSize.Text = "34";
									fldDescription.Text = "Impervious Surface.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CT - Sewer Adj 5 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 5 Amount.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CT - Sewer Adj 5 Amount";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Sewer Adjustment 5 Amount.";
								}
								else if (boolInclChangeOutCons == true)
								{
									// trout-1118 12.4.17 kjr
									lblFieldName.Text = "CT - Meter 1 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 1 Change Out Consumption.";
								}
							}
							break;
						}
					case 99:
						{
							if (boolChptr660)
							{
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
								{
									lblFieldName.Text = "CU - Owner Name 2";
									lblSize.Text = "34";
									fldDescription.Text = "Second Owner Name.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
								{
									lblFieldName.Text = "CU - Pervious Reading Date";
									lblSize.Text = "34";
									fldDescription.Text = "Previous Reading Date.";
								}
								else if (boolInclChangeOutCons == true)
								{
									// trout-1118 12.4.17 kjr
									if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
									{
										lblFieldName.Text = "CU - Meter 1 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 1 Change Out Consumption.";
									}
									else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
									{
										lblFieldName.Text = "CU - Meter 1 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 1 Change Out Consumption.";
									}
									else
									{
										lblFieldName.Text = "CU - Meter 2 Change Out Consumption";
										lblSize.Text = lngAmountFieldLength;
										fldDescription.Text = "Meter 2 Change Out Consumption.";
									}
								}
							}
							break;
						}
					case 100:
						{
							if (boolChptr660 && boolInclChangeOutCons == true)
							{
								// trout-1118 12.4.17 kjr
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CV - Meter 2 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Change Out Consumption.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CV - Meter 2 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Change Out Consumption.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
								{
									lblFieldName.Text = "CV - Meter 1 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 1 Change Out Consumption.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
								{
									lblFieldName.Text = "CV -Meter 1 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 1 Change Out Consumption.";
								}
								else
								{
									lblFieldName.Text = "CV - Meter 3 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Change Out Consumption.";
								}
							}
							break;
						}
					case 101:
						{
							if (boolChptr660 && boolInclChangeOutCons == true)
							{
								// trout-1118 12.4.17 kjr
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
								{
									lblFieldName.Text = "CW - Meter 3 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Change Out Consumption.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
								{
									lblFieldName.Text = "CW - Meter 3 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Change Out Consumption.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
								{
									lblFieldName.Text = "CW - Meter 2 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Change Out Consumption.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
								{
									lblFieldName.Text = "CW -Meter 2 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 2 Change Out Consumption.";
								}
								else
								{
									boolDone = true;
								}
							}
							break;
						}
					case 102:
						{
							if (boolChptr660 && boolInclChangeOutCons == true)
							{
								// trout-1118 12.4.17 kjr
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
								{
									lblFieldName.Text = "CX - Meter 3 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Change Out Consumption.";
								}
								else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
								{
									lblFieldName.Text = "CX -Meter 3 Change Out Consumption";
									lblSize.Text = lngAmountFieldLength;
									fldDescription.Text = "Meter 3 Change Out Consumption.";
								}
								else
								{
									boolDone = true;
								}
							}
							break;
						}
					default:
						{
							boolDone = true;
							break;
						}
				}
				//end switch
				lngCount += 1;
				lngTotalSize += FCConvert.ToInt32(Conversion.Val(lblSize.Text));
				// Debug.Print lngTotalSize
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error in Outprinting Format Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			lblTotal.Text = "Total Size : " + FCConvert.ToString(lngTotalSize);
		}

		private void rptOutprintingFormat_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptOutprintingFormat properties;
			//rptOutprintingFormat.Text	= "Outprinting Format";
			//rptOutprintingFormat.Icon	= "rptOutprintingFormat.dsx":0000";
			//rptOutprintingFormat.Left	= 0;
			//rptOutprintingFormat.Top	= 0;
			//rptOutprintingFormat.Width	= 11880;
			//rptOutprintingFormat.Height	= 8595;
			//rptOutprintingFormat.StartUpPosition	= 3;
			//rptOutprintingFormat.SectionData	= "rptOutprintingFormat.dsx":058A;
			//End Unmaped Properties
		}
	}
}
