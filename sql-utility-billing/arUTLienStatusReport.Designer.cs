﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arUTLienStatusReport.
	/// </summary>
	partial class arUTLienStatusReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arUTLienStatusReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPLInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipalDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPLInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPrincipalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPLInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCurrentInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblLienSummary = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSummaryYear1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSummaryTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnSummaryHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lnSummaryTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldSummaryPrin1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSummaryPrinPaid1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSummaryPLI1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSummaryCosts1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSummaryCurrentInt1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPLInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrentInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPLInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPLInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLienSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryYear1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryPrin1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryPrinPaid1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryPLI1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryCosts1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryCurrentInt1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldPrincipal,
				this.fldName,
				this.fldPLInt,
				this.fldCosts,
				this.fldCurrentInt,
				this.fldTotal,
				this.fldYear,
				this.fldPrincipalDue
			});
			this.Detail.Height = 0.3541667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalPrincipalDue,
				this.lblTotals,
				this.fldTotalPrincipal,
				this.fldTotalPLInt,
				this.fldTotalCosts,
				this.fldTotalCurrentInt,
				this.fldTotalTotal,
				this.Line2,
				this.lblLienSummary,
				this.lblSummaryYear1,
				this.fldSummaryTotal1,
				this.lnSummaryHeader,
				this.lnSummaryTotal,
				this.fldSummaryPrin1,
				this.fldSummaryPrinPaid1,
				this.fldSummaryPLI1,
				this.fldSummaryCosts1,
				this.fldSummaryCurrentInt1
			});
			this.ReportFooter.Height = 0.96875F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lblAccount,
				this.lblPrincipal,
				this.lblName,
				this.lblPLInt,
				this.lblCosts,
				this.lblCurrentInt,
				this.lblTotal,
				this.lblReportType,
				this.Line1,
				this.lblYear,
				this.lblPrincipalDue
			});
			this.PageHeader.Height = 0.9375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Lien Status List";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.2F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.35F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.15F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.2F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.35F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.2F;
			this.lblPage.Width = 1.15F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.2F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.7F;
			this.lblAccount.Width = 0.75F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 1.9375F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0.6875F;
			this.lblPrincipal.Width = 0.9375F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 1.375F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.6875F;
			this.lblName.Width = 1.4375F;
			// 
			// lblPLInt
			// 
			this.lblPLInt.Height = 0.1875F;
			this.lblPLInt.HyperLink = null;
			this.lblPLInt.Left = 3.8125F;
			this.lblPLInt.Name = "lblPLInt";
			this.lblPLInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPLInt.Text = "Pre Lien Int";
			this.lblPLInt.Top = 0.6875F;
			this.lblPLInt.Width = 0.9375F;
			// 
			// lblCosts
			// 
			this.lblCosts.Height = 0.1875F;
			this.lblCosts.HyperLink = null;
			this.lblCosts.Left = 4.8125F;
			this.lblCosts.Name = "lblCosts";
			this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCosts.Text = "Costs";
			this.lblCosts.Top = 0.6875F;
			this.lblCosts.Width = 0.8125F;
			// 
			// lblCurrentInt
			// 
			this.lblCurrentInt.Height = 0.2F;
			this.lblCurrentInt.HyperLink = null;
			this.lblCurrentInt.Left = 5.6F;
			this.lblCurrentInt.Name = "lblCurrentInt";
			this.lblCurrentInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCurrentInt.Text = "Interest";
			this.lblCurrentInt.Top = 0.7F;
			this.lblCurrentInt.Width = 0.95F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.2F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 6.5F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.7F;
			this.lblTotal.Width = 1F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.5625F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblReportType.Text = "Report Type";
			this.lblReportType.Top = 0.1875F;
			this.lblReportType.Width = 7.5F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.9F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.9F;
			this.Line1.Y2 = 0.9F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 0.75F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblYear.Text = "Year";
			this.lblYear.Top = 0.6875F;
			this.lblYear.Width = 0.5625F;
			// 
			// lblPrincipalDue
			// 
			this.lblPrincipalDue.Height = 0.1875F;
			this.lblPrincipalDue.HyperLink = null;
			this.lblPrincipalDue.Left = 2.875F;
			this.lblPrincipalDue.Name = "lblPrincipalDue";
			this.lblPrincipalDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPrincipalDue.Text = "Principal Due";
			this.lblPrincipalDue.Top = 0.6875F;
			this.lblPrincipalDue.Width = 1F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.75F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 1.9375F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0.1875F;
			this.fldPrincipal.Width = 0.9375F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1.3125F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 4.6875F;
			// 
			// fldPLInt
			// 
			this.fldPLInt.Height = 0.1875F;
			this.fldPLInt.Left = 3.8125F;
			this.fldPLInt.Name = "fldPLInt";
			this.fldPLInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPLInt.Text = null;
			this.fldPLInt.Top = 0.1875F;
			this.fldPLInt.Width = 0.9375F;
			// 
			// fldCosts
			// 
			this.fldCosts.Height = 0.1875F;
			this.fldCosts.Left = 4.6875F;
			this.fldCosts.Name = "fldCosts";
			this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCosts.Text = null;
			this.fldCosts.Top = 0.1875F;
			this.fldCosts.Width = 0.9375F;
			// 
			// fldCurrentInt
			// 
			this.fldCurrentInt.Height = 0.1875F;
			this.fldCurrentInt.Left = 5.625F;
			this.fldCurrentInt.Name = "fldCurrentInt";
			this.fldCurrentInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCurrentInt.Text = null;
			this.fldCurrentInt.Top = 0.1875F;
			this.fldCurrentInt.Width = 0.9375F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.5F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0.1875F;
			this.fldTotal.Width = 1F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 0.75F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldYear.Text = null;
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.5625F;
			// 
			// fldPrincipalDue
			// 
			this.fldPrincipalDue.Height = 0.1875F;
			this.fldPrincipalDue.Left = 2.875F;
			this.fldPrincipalDue.Name = "fldPrincipalDue";
			this.fldPrincipalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipalDue.Text = null;
			this.fldPrincipalDue.Top = 0.1875F;
			this.fldPrincipalDue.Width = 0.9375F;
			// 
			// fldTotalPrincipalDue
			// 
			this.fldTotalPrincipalDue.Height = 0.1875F;
			this.fldTotalPrincipalDue.Left = 2.875F;
			this.fldTotalPrincipalDue.Name = "fldTotalPrincipalDue";
			this.fldTotalPrincipalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrincipalDue.Text = "0.00";
			this.fldTotalPrincipalDue.Top = 0F;
			this.fldTotalPrincipalDue.Width = 0.9375F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.8125F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0F;
			this.lblTotals.Width = 1.125F;
			// 
			// fldTotalPrincipal
			// 
			this.fldTotalPrincipal.Height = 0.1875F;
			this.fldTotalPrincipal.Left = 1.9375F;
			this.fldTotalPrincipal.Name = "fldTotalPrincipal";
			this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrincipal.Text = "0.00";
			this.fldTotalPrincipal.Top = 0F;
			this.fldTotalPrincipal.Width = 0.9375F;
			// 
			// fldTotalPLInt
			// 
			this.fldTotalPLInt.Height = 0.2F;
			this.fldTotalPLInt.Left = 3.8F;
			this.fldTotalPLInt.Name = "fldTotalPLInt";
			this.fldTotalPLInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPLInt.Text = "0.00";
			this.fldTotalPLInt.Top = 0F;
			this.fldTotalPLInt.Width = 0.95F;
			// 
			// fldTotalCosts
			// 
			this.fldTotalCosts.Height = 0.1875F;
			this.fldTotalCosts.Left = 4.75F;
			this.fldTotalCosts.Name = "fldTotalCosts";
			this.fldTotalCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCosts.Text = "0.00";
			this.fldTotalCosts.Top = 0F;
			this.fldTotalCosts.Width = 0.875F;
			// 
			// fldTotalCurrentInt
			// 
			this.fldTotalCurrentInt.Height = 0.2F;
			this.fldTotalCurrentInt.Left = 5.6F;
			this.fldTotalCurrentInt.Name = "fldTotalCurrentInt";
			this.fldTotalCurrentInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCurrentInt.Text = "0.00";
			this.fldTotalCurrentInt.Top = 0F;
			this.fldTotalCurrentInt.Width = 0.95F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.2F;
			this.fldTotalTotal.Left = 6.5F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0F;
			this.fldTotalTotal.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 1.9375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 5.5625F;
			this.Line2.X1 = 1.9375F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// lblLienSummary
			// 
			this.lblLienSummary.Height = 0.1875F;
			this.lblLienSummary.HyperLink = null;
			this.lblLienSummary.Left = 0F;
			this.lblLienSummary.Name = "lblLienSummary";
			this.lblLienSummary.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblLienSummary.Text = "Lien Summary";
			this.lblLienSummary.Top = 0.375F;
			this.lblLienSummary.Visible = false;
			this.lblLienSummary.Width = 7.5F;
			// 
			// lblSummaryYear1
			// 
			this.lblSummaryYear1.Height = 0.1875F;
			this.lblSummaryYear1.HyperLink = null;
			this.lblSummaryYear1.Left = 0.4375F;
			this.lblSummaryYear1.Name = "lblSummaryYear1";
			this.lblSummaryYear1.Style = "font-family: \'Tahoma\'";
			this.lblSummaryYear1.Text = null;
			this.lblSummaryYear1.Top = 0.5625F;
			this.lblSummaryYear1.Visible = false;
			this.lblSummaryYear1.Width = 1.5F;
			// 
			// fldSummaryTotal1
			// 
			this.fldSummaryTotal1.Height = 0.1875F;
			this.fldSummaryTotal1.Left = 6.5F;
			this.fldSummaryTotal1.Name = "fldSummaryTotal1";
			this.fldSummaryTotal1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummaryTotal1.Text = null;
			this.fldSummaryTotal1.Top = 0.5625F;
			this.fldSummaryTotal1.Visible = false;
			this.fldSummaryTotal1.Width = 1F;
			// 
			// lnSummaryHeader
			// 
			this.lnSummaryHeader.Height = 0F;
			this.lnSummaryHeader.Left = 2.3125F;
			this.lnSummaryHeader.LineWeight = 1F;
			this.lnSummaryHeader.Name = "lnSummaryHeader";
			this.lnSummaryHeader.Top = 0.5625F;
			this.lnSummaryHeader.Visible = false;
			this.lnSummaryHeader.Width = 2.9375F;
			this.lnSummaryHeader.X1 = 2.3125F;
			this.lnSummaryHeader.X2 = 5.25F;
			this.lnSummaryHeader.Y1 = 0.5625F;
			this.lnSummaryHeader.Y2 = 0.5625F;
			// 
			// lnSummaryTotal
			// 
			this.lnSummaryTotal.Height = 0F;
			this.lnSummaryTotal.Left = 1.9375F;
			this.lnSummaryTotal.LineWeight = 1F;
			this.lnSummaryTotal.Name = "lnSummaryTotal";
			this.lnSummaryTotal.Top = 0.75F;
			this.lnSummaryTotal.Visible = false;
			this.lnSummaryTotal.Width = 5.5625F;
			this.lnSummaryTotal.X1 = 1.9375F;
			this.lnSummaryTotal.X2 = 7.5F;
			this.lnSummaryTotal.Y1 = 0.75F;
			this.lnSummaryTotal.Y2 = 0.75F;
			// 
			// fldSummaryPrin1
			// 
			this.fldSummaryPrin1.Height = 0.1875F;
			this.fldSummaryPrin1.Left = 1.9375F;
			this.fldSummaryPrin1.Name = "fldSummaryPrin1";
			this.fldSummaryPrin1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummaryPrin1.Text = null;
			this.fldSummaryPrin1.Top = 0.5625F;
			this.fldSummaryPrin1.Visible = false;
			this.fldSummaryPrin1.Width = 0.9375F;
			// 
			// fldSummaryPrinPaid1
			// 
			this.fldSummaryPrinPaid1.Height = 0.1875F;
			this.fldSummaryPrinPaid1.Left = 2.875F;
			this.fldSummaryPrinPaid1.Name = "fldSummaryPrinPaid1";
			this.fldSummaryPrinPaid1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummaryPrinPaid1.Text = null;
			this.fldSummaryPrinPaid1.Top = 0.5625F;
			this.fldSummaryPrinPaid1.Visible = false;
			this.fldSummaryPrinPaid1.Width = 0.9375F;
			// 
			// fldSummaryPLI1
			// 
			this.fldSummaryPLI1.Height = 0.1875F;
			this.fldSummaryPLI1.Left = 3.8125F;
			this.fldSummaryPLI1.Name = "fldSummaryPLI1";
			this.fldSummaryPLI1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummaryPLI1.Text = null;
			this.fldSummaryPLI1.Top = 0.5625F;
			this.fldSummaryPLI1.Visible = false;
			this.fldSummaryPLI1.Width = 0.9375F;
			// 
			// fldSummaryCosts1
			// 
			this.fldSummaryCosts1.Height = 0.1875F;
			this.fldSummaryCosts1.Left = 4.75F;
			this.fldSummaryCosts1.Name = "fldSummaryCosts1";
			this.fldSummaryCosts1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummaryCosts1.Text = null;
			this.fldSummaryCosts1.Top = 0.5625F;
			this.fldSummaryCosts1.Visible = false;
			this.fldSummaryCosts1.Width = 0.875F;
			// 
			// fldSummaryCurrentInt1
			// 
			this.fldSummaryCurrentInt1.Height = 0.1875F;
			this.fldSummaryCurrentInt1.Left = 5.625F;
			this.fldSummaryCurrentInt1.Name = "fldSummaryCurrentInt1";
			this.fldSummaryCurrentInt1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldSummaryCurrentInt1.Text = null;
			this.fldSummaryCurrentInt1.Top = 0.5625F;
			this.fldSummaryCurrentInt1.Visible = false;
			this.fldSummaryCurrentInt1.Width = 0.9375F;
			// 
			// arUTLienStatusReport
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPLInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrentInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPLInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrentInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPLInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLienSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSummaryYear1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryPrin1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryPrinPaid1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryPLI1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryCosts1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSummaryCurrentInt1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrentInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipalDue;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCurrentInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLienSummary;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSummaryYear1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryTotal1;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSummaryHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnSummaryTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryPrin1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryPrinPaid1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryPLI1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryCosts1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSummaryCurrentInt1;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrentInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipalDue;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
