﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptLienRemovalPayments.
	/// </summary>
	public partial class rptLienRemovalPayments : BaseSectionReport
	{
		public rptLienRemovalPayments()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptLienRemovalPayments InstancePtr
		{
			get
			{
				return (rptLienRemovalPayments)Sys.GetInstance(typeof(rptLienRemovalPayments));
			}
		}

		protected rptLienRemovalPayments _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsPay.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLienRemovalPayments	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/15/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsPay = new clsDRWrapper();
		double dblPrin;
		double dblInt;
		double dblCost;
		bool boolWater;
		string strWS = "";

		public void Init(ref int lngLRN, bool boolPassWater)
		{
			boolWater = boolPassWater;
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			rsData.OpenRecordset("SELECT * FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE Lien.ID = " + FCConvert.ToString(lngLRN), modExtraModules.strUTDatabase);
			if (!rsData.EndOfFile())
			{
				rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngLRN) + " AND ISNULL(Lien,0) = 1", modExtraModules.strUTDatabase);
			}
			else
			{
				// end report
				MessageBox.Show("Cannot find lien and bill record.", "Report Cancelled", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			frmReportViewer.InstancePtr.Init(this._InstancePtr);
			//this.Show(FCForm.FormShowEnum.Modal);
			//Application.DoEvents();
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsPay.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			SetHeaderString();
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will print the row from the grid
			if (!rsPay.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				fldPrincipal.Text = Strings.Format(rsPay.Get_Fields("Principal"), "#,##0.00");
				fldInterest.Text = Strings.Format(rsPay.Get_Fields_Decimal("PreLienInterest") + rsPay.Get_Fields_Decimal("CurrentInterest"), "#,##0.00");
				fldCosts.Text = Strings.Format(rsPay.Get_Fields_Decimal("LienCost"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				fldTotal.Text = Strings.Format(rsPay.Get_Fields("Principal") + rsPay.Get_Fields_Decimal("PreLienInterest") + rsPay.Get_Fields_Decimal("CurrentInterest") + rsPay.Get_Fields_Decimal("LienCost"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCode.Text = FCConvert.ToString(rsPay.Get_Fields("Code"));
				fldDate.Text = rsPay.Get_Fields_DateTime("RecordedTransactionDate").ToString("MM/dd/yyyy");
				;
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				fldPer.Text = FCConvert.ToString(rsPay.Get_Fields("Period"));
				fldRef.Text = rsPay.Get_Fields_String("Reference");
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblPrin += FCConvert.ToDouble(rsPay.Get_Fields("Principal"));
				dblInt += FCConvert.ToDouble(rsPay.Get_Fields_Decimal("PreLienInterest") + rsPay.Get_Fields_Decimal("CurrentInterest"));
				dblCost += FCConvert.ToDouble(rsPay.Get_Fields_Decimal("LienCost"));
				rsPay.MoveNext();
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			lblAccount.Text = "Account " + FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey")));
			lblName.Text = "Name: " + rsData.Get_Fields_String("OName");
			lblLocation.Text = "Location: " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location")));
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// show the totals
			fldTotalPrin.Text = Strings.Format(dblPrin, "#,##0.00");
			fldTotalInt.Text = Strings.Format(dblInt, "#,##0.00");
			fldTotalCost.Text = Strings.Format(dblCost, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblPrin + dblInt + dblCost, "#,##0.00");
		}

		
	}
}
