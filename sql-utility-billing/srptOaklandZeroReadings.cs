﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using System;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptOaklandZeroReadings.
	/// </summary>
	public partial class srptOaklandZeroReadings : FCSectionReport
	{
		public srptOaklandZeroReadings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Daily Audit Detail";
		}

		public static srptOaklandZeroReadings InstancePtr
		{
			get
			{
				return (srptOaklandZeroReadings)Sys.GetInstance(typeof(srptOaklandZeroReadings));
			}
		}

		protected srptOaklandZeroReadings _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptOaklandZeroReadings	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/10/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/15/2004              *
		// ********************************************************
		bool blnFirstRecord;
		int lngCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Consumption == 0)
				{
					eArgs.EOF = false;
				}
				else
				{
					//TODO
					//goto MoveNext;
				}
			}
			else
			{
				MoveNext:
				;
				lngCounter += 1;
				if (lngCounter <= Information.UBound(modTSCONSUM.Statics.OaklandReportInfo, 1))
				{
					if (modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Consumption == 0)
					{
						eArgs.EOF = false;
					}
					else
					{
						goto MoveNext;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngCounter = 0;
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCounter > Information.UBound(modTSCONSUM.Statics.OaklandReportInfo, 1))
			{
				fldAccountNumber.Text = "";
				fldName.Text = "";
				fldPreviousReading.Text = "";
				fldConsumption.Text = "";
			}
			else
			{
				fldAccountNumber.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].AccountNumber.ToString();
				fldName.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].OwnerName;
				fldPreviousReading.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].PreviousReading.ToString();
				fldConsumption.Text = "0";
			}
		}

		private void srptOaklandZeroReadings_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptOaklandZeroReadings properties;
			//srptOaklandZeroReadings.Caption	= "Daily Audit Detail";
			//srptOaklandZeroReadings.Icon	= "srptOaklandZeroReadings.dsx":0000";
			//srptOaklandZeroReadings.Left	= 0;
			//srptOaklandZeroReadings.Top	= 0;
			//srptOaklandZeroReadings.Width	= 11880;
			//srptOaklandZeroReadings.Height	= 8175;
			//srptOaklandZeroReadings.StartUpPosition	= 3;
			//srptOaklandZeroReadings.SectionData	= "srptOaklandZeroReadings.dsx":058A;
			//End Unmaped Properties
		}
	}
}
