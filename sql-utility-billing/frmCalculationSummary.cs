﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCalculationSummary.
	/// </summary>
	public partial class frmCalculationSummary : BaseForm
	{
		public frmCalculationSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.vsEdit = new System.Collections.Generic.List<FCGrid>();
			this.vsEdit.AddControlArrayElement(vsEdit_0, 0);
			this.vsEdit.AddControlArrayElement(vsEdit_1, 1);
			this.vsEdit.AddControlArrayElement(vsEdit_2, 2);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCalculationSummary InstancePtr
		{
			get
			{
				return (frmCalculationSummary)Sys.GetInstance(typeof(frmCalculationSummary));
			}
		}

		protected frmCalculationSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/13/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/25/2005              *
		// ********************************************************
		int CategoryCount;
		int[] BookArray = null;
		// holds the books to be summarized
		string strSQL;
		// string builder
		int UnloadFlag;
		// flag to tell what type of unload the form is undergoing
		public bool boolCalculate;
		// True if this is the calculation summary, false if this is the billing edit summary
		// the public variables will be set from the previous form
		public double WOverride;
		// Override Totals
		public double WFlat;
		// Flat Totals
		public double WUnits;
		// Units Totals
		public double WCons;
		// Consumption Totals
		public double WAdjust;
		// Adjustment Totals
		public double WMisc;
		// Misc
		public double WTax;
		// Tax Amount
		public double WSubTotal;
		// Sub Total
		public int WBillCount;
		// Number of Bills
		public double SOverride;
		// Override Totals
		public double SFlat;
		// Flat Totals
		public double SUnits;
		// Units Totals
		public double SCons;
		// Consumption Totals
		public double SAdjust;
		// Adjustment Totals
		public double SMisc;
		// Misc
		public double STax;
		// Tax Amount
		public double SSubTotal;
		// Sub Total
		public int SBillCount;
		// Number of Bills
		public void Init(ref bool boolPassCalculate, ref int[] PassBookArray, bool boolHide = false)
		{
			BookArray = PassBookArray;
			// this will pass the bookarray in so that this form can use it
			boolCalculate = boolPassCalculate;
			if (boolCalculate)
			{
				this.Text = "Calculation Summary";
				//FC:FINAL:MSH - issue #976: change header text
				//FC:FINAL:DDU:#1145 - remove header text
				//this.HeaderText.Text = "Calculation Summary";
				Fill_Grid();
				ShowFrame(ref fraCalc);
				fraBillingEdit.Visible = false;
			}
			else
			{
				this.Text = "Billing Edit Summary";
				//FC:FINAL:MSH - issue #976: change header text
				//FC:FINAL:DDU:#1145 - remove header text
				//this.HeaderText.Text = "Billing Edit Summary";
				Fill_Grid();
				Format_BEGrids();
				Fill_BEGrid();
				AdjustBEGridHeight();
				ShowFrame(ref fraBillingEdit);
				fraCalc.Visible = false;
				// force the click event of the option
				optWS_Click(0);
			}
			cmdSummary.Visible = true;
			cmdPrint.Visible = true;
			this.Show(App.MainForm);
			if (vsSumW.Visible && vsSumW.Enabled)
			{
				//Application.DoEvents();
				this.Focus();
				vsSumW.Focus();
			}
			if (boolHide)
			{
				this.Hide();
			}
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//Application.DoEvents();
				if (boolCalculate)
				{
					frmReportViewer.InstancePtr.Init(arCalculationSummary.InstancePtr);
				}
				else
				{
					arBillingEdit.InstancePtr.Init(ref BookArray, false, false, true, false);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Print", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void cmdSummary_Click(object sender, System.EventArgs e)
		{
			UnloadFlag = 0;
            this.Unload();
        }

		public void cmdSummary_Click()
		{
			cmdSummary_Click(cmdSummary, new System.EventArgs());
		}

		private void frmCalculationSummary_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
				return;
		}

		private void frmCalculationSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCalculationSummary properties;
			//frmCalculationSummary.FillStyle	= 0;
			//frmCalculationSummary.ScaleWidth	= 9300;
			//frmCalculationSummary.ScaleHeight	= 7725;
			//frmCalculationSummary.LinkTopic	= "Form2";
			//frmCalculationSummary.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblWater.Text = "Stormwater";
				lblWaterSum.Text = "Stormwater";
			}
		}

		private void frmCalculationSummary_Resize(object sender, System.EventArgs e)
		{
			if (boolCalculate)
			{
				Format_Grid();
				vsSumCalculateHeight();
				ShowFrame(ref fraCalc);
			}
			else
			{
				Format_BEGrids();
				AdjustBEGridHeight();
				ShowFrame(ref fraBillingEdit);
			}
		}

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            if (UnloadFlag == 1)
            {
                frmCalculation.InstancePtr.Unload();
            }
            else
            {
                frmCalculation.InstancePtr.Show();
                //UnloadFlag = 1; //'MAL@20070831:Commented out - it was causing this form to be unloaded when it was hit a second time
            }
        }

        private void frmCalculationSummary_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				UnloadFlag = 1;
				this.Unload();
            }
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Format_Grid()
		{
			// format the summary grid
			vsSumW.Cols = 5;
			vsSumW.ColWidth(0, FCConvert.ToInt32(vsSumW.WidthOriginal * 0.35));
			vsSumW.ColWidth(1, FCConvert.ToInt32(vsSumW.WidthOriginal * 0));
			vsSumW.ColWidth(2, FCConvert.ToInt32(vsSumW.WidthOriginal * 0.13));
			vsSumW.ColWidth(3, FCConvert.ToInt32(vsSumW.WidthOriginal * 0.25));
			vsSumW.ColWidth(4, FCConvert.ToInt32(vsSumW.WidthOriginal * 0.24));
			vsSumW.ExtendLastCol = true;
			vsSumW.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
			vsSumW.TextMatrix(0, 0, "Category");
			vsSumW.TextMatrix(0, 2, "Count");
			vsSumW.TextMatrix(0, 3, "Consumption");
			vsSumW.TextMatrix(0, 4, "Amount");
			vsSumW.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//FC:FINAL:DDU:#976 - fixed alignment of columns
			//vsSumW.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSumW.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSumW.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSumW.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSumW.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSumW.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsSumW.ColFormat(4, "#,##0.00");
			vsSumS.Cols = 5;
			vsSumS.ColWidth(0, FCConvert.ToInt32(vsSumS.WidthOriginal * 0.35));
			vsSumS.ColWidth(1, FCConvert.ToInt32(vsSumS.WidthOriginal * 0));
			vsSumS.ColWidth(2, FCConvert.ToInt32(vsSumS.WidthOriginal * 0.13));
			vsSumS.ColWidth(3, FCConvert.ToInt32(vsSumS.WidthOriginal * 0.25));
			vsSumS.ColWidth(4, FCConvert.ToInt32(vsSumS.WidthOriginal * 0.24));
			vsSumS.ExtendLastCol = true;
			vsSumS.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
			vsSumS.TextMatrix(0, 0, "Category");
			vsSumS.TextMatrix(0, 2, "Count");
			vsSumS.TextMatrix(0, 3, "Consumption");
			vsSumS.TextMatrix(0, 4, "Amount");
			vsSumS.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//FC:FINAL:DDU:#976 - fixed alignment of columns
			//vsSumS.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSumS.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSumS.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSumS.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSumS.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsSumS.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsSumS.ColFormat(4, "#,##0.00");
		}

		private void Fill_Grid()
		{
			// fills the labels with info from the calculation screen
			// water
			lblWOverride.Text = Strings.Format(WOverride, "Currency");
			lblWFlat.Text = Strings.Format(WFlat, "Currency");
			lblWUnits.Text = Strings.Format(WUnits, "Currency");
			lblWCons.Text = Strings.Format(WCons, "Currency");
			lblWMisc.Text = Strings.Format(WMisc, "Currency");
			lblWAdjustment.Text = Strings.Format(WAdjust, "Currency");
			lblWTax.Text = Strings.Format(WTax, "Currency");
			lblWSubTotal.Text = Strings.Format(WSubTotal, "Currency");
			// sewer
			lblSOverride.Text = Strings.Format(SOverride, "Currency");
			lblSFlat.Text = Strings.Format(SFlat, "Currency");
			lblSUnits.Text = Strings.Format(SUnits, "Currency");
			lblSCons.Text = Strings.Format(SCons, "Currency");
			lblSMisc.Text = Strings.Format(SMisc, "Currency");
			lblSAdjustment.Text = Strings.Format(SAdjust, "Currency");
			lblSTax.Text = Strings.Format(STax, "Currency");
			lblSSubTotal.Text = Strings.Format(SSubTotal, "Currency");
			// calculate the summary information and fill the grid
			FindGridValues();
			vsSumCalculateHeight();
		}

		private void mnuDetail_Click(object sender, System.EventArgs e)
		{
			cmdSummary_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void mnuQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void vsSumCalculateHeight()
		{
			int ExposedRows;
			// keeps track of rows that are not collapsed
			ExposedRows = vsSumW.Rows;
			// this will calculate the height of the Water flex grid depending on how many rows are open with a max height of the 25% of the form height
			if (((vsSumW.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows) + 75) < (frmCalculationSummary.InstancePtr.Height * 0.62))
			{
				vsSumW.Height = vsSumW.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows + 75;
			}
			else
			{
				vsSumW.Height = FCConvert.ToInt32((frmCalculationSummary.InstancePtr.Height * 0.62));
			}
			ExposedRows = vsSumS.Rows;
			// this will calculate the height of the Sewer flex grid depending on how many rows are open with a max height of the 25% of the form height
			if (((vsSumS.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows) + 75) < (frmCalculationSummary.InstancePtr.Height * 0.62))
			{
				vsSumS.Height = vsSumS.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows + 75;
			}
			else
			{
				vsSumS.Height = FCConvert.ToInt32((frmCalculationSummary.InstancePtr.Height * 0.62));
			}
		}

		private void FindGridValues()
		{
			// calculates values and fills the grid with those values
			int i;
			string strSQL1 = "";
			string strField;
			string strBook = "";
			string strStatus = "";
			bool Found;
			clsDRWrapper rsCat = new clsDRWrapper();
			clsDRWrapper rsTotal = new clsDRWrapper();
			// create a book string
			switch (Information.UBound(BookArray, 1))
			{
				case 1:
					{
						// on element in the array
						strBook = " BookNumber = " + FCConvert.ToString(BookArray[1]);
						break;
					}
				default:
					{
						// many elements in the array
						strBook = " BookNumber IN (";
						for (i = 1; i <= Information.UBound(BookArray, 1); i++)
						{
							strBook += FCConvert.ToString(BookArray[i]) + ",";
						}
						strBook = Strings.Left(strBook, strBook.Length - 1) + ")";
						break;
					}
			}
			//end switch
			// create a status string
			if (boolCalculate)
			{
				strStatus = "BillStatus = 'X'";
			}
			else
			{
				strStatus = "BillStatus = 'E'";
			}
			strField = "SELECT [Category].Code, [Category].LongDescription, COUNT(Bill.ID) as BillCount, SUM(Bill.Consumption) as TotC, SUM(WaterCharged) as TotW";
			// SUM(Bill.TotalWBillAmount)
			// INNER JOIN
			// DJW@02082010 added AND FinalBilled = False to where clause
			// DJW@01102013 TROUT-895 removed AND WaterCharged <> 0 to show consumption for no charge meters
			strSQL = " FROM (((Bill LEFT JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID) INNER JOIN Category ON Master.WaterCategory = Category.Code) WHERE " + strStatus + " AND (MeterTable.Service <> 'S') AND ISNULL(FinalBilled,0) = 0 AND Combine = 'N' AND " + strBook + " GROUP BY Category.Code, Category.LongDescription";
			// WHERE ((Bill.BillStatus = 'D' OR Bill.BillStatus = 'X' OR Bill.BillStatus = 'E') AND MeterTable.Book = " & CurrentBook & " AND Master.Category = " & Val(vsSum.TextMatrix(i, 0)) & ")"
			rsCat.OpenRecordset(strField + strSQL);
			if (rsCat.RecordCount() != 0)
			{
				i = 1;
				while (!rsCat.EndOfFile())
				{
					vsSumW.Rows = i + 1;
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields("Code")) == false)
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						vsSumW.TextMatrix(i, 0, FCConvert.ToString(rsCat.Get_Fields("Code")));
					}
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields_String("LongDescription")) == false)
					{
						vsSumW.TextMatrix(i, 1, FCConvert.ToString(rsCat.Get_Fields_String("LongDescription")));
					}
					// TODO Get_Fields: Field [BillCount] not found!! (maybe it is an alias?)
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields("BillCount")) == false)
					{
						// TODO Get_Fields: Field [BillCount] not found!! (maybe it is an alias?)
						vsSumW.TextMatrix(i, 2, FCConvert.ToString(rsCat.Get_Fields("BillCount")));
						// Bill Count
					}
					// TODO Get_Fields: Field [TotC] not found!! (maybe it is an alias?)
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields("TotC")) == false)
					{
						// TODO Get_Fields: Field [TotC] not found!! (maybe it is an alias?)
						vsSumW.TextMatrix(i, 3, FCConvert.ToString(rsCat.Get_Fields("TotC")));
						// Consumption Total
					}
					// TODO Get_Fields: Field [TotW] not found!! (maybe it is an alias?)
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields("TotW")) == false)
					{
						// TODO Get_Fields: Field [TotW] not found!! (maybe it is an alias?)
						vsSumW.TextMatrix(i, 4, FCConvert.ToString(rsCat.Get_Fields("TotW")));
						// Water Total
					}
					i += 1;
					rsCat.MoveNext();
				}
			}
			else
			{
				// MsgBox "No records in water category table.", vbExclamation, "Error"
			}
			strField = "SELECT [Category].Code, COUNT(Bill.ID) as BillCount, SUM(Bill.Consumption) as TotC, SUM(SewerCharged) as TotS, Category.LongDescription";
			// strField = "SELECT [Category].Code, COUNT(Bill.Bill) as BillCount, SUM(Bill.Consumption) as TotC, SUM(SPrinOwed + SIntOwed + SCostOwed + STaxOwed) as TotS, Category.LongDescription"
			// INNER JOIN
			// DJW@02082010 added AND FinalBilled = False to where clause
			// DJW@01102013 TROUT-895 removed AND SererCharged <> 0 to show consumption for no charge meters
			strSQL = " FROM (((Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID) INNER JOIN Category ON Master.SewerCategory = Category.Code) WHERE " + strStatus + " AND (MeterTable.Service <> 'W') AND Combine = 'N' AND ISNULL(FinalBilled,0) = 0 AND " + strBook + " GROUP BY Category.Code, Category.LongDescription";
			rsCat.OpenRecordset(strField + strSQL);
			if (rsCat.RecordCount() != 0)
			{
				i = 1;
				while (!rsCat.EndOfFile())
				{
					vsSumS.Rows = i + 1;
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields("Code")) == false)
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						vsSumS.TextMatrix(i, 0, FCConvert.ToString(rsCat.Get_Fields("Code")));
					}
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields_String("LongDescription")) == false)
					{
						vsSumS.TextMatrix(i, 1, FCConvert.ToString(rsCat.Get_Fields_String("LongDescription")));
					}
					// TODO Get_Fields: Field [BillCount] not found!! (maybe it is an alias?)
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields("BillCount")) == false)
					{
						// TODO Get_Fields: Field [BillCount] not found!! (maybe it is an alias?)
						vsSumS.TextMatrix(i, 2, FCConvert.ToString(rsCat.Get_Fields("BillCount")));
						// Bill Count
					}
					// TODO Get_Fields: Field [TotC] not found!! (maybe it is an alias?)
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields("TotC")) == false)
					{
						// TODO Get_Fields: Field [TotC] not found!! (maybe it is an alias?)
						vsSumS.TextMatrix(i, 3, FCConvert.ToString(rsCat.Get_Fields("TotC")));
						// Consumption Total
					}
					// TODO Get_Fields: Field [TotS] not found!! (maybe it is an alias?)
					if (fecherFoundation.FCUtils.IsNull(rsCat.Get_Fields("TotS")) == false)
					{
						// TODO Get_Fields: Field [TotS] not found!! (maybe it is an alias?)
						vsSumS.TextMatrix(i, 4, FCConvert.ToString(rsCat.Get_Fields("TotS")));
						// Water Total
					}
					i += 1;
					rsCat.MoveNext();
				}
			}
		}

		private void Format_BEGrids()
		{
			int i;
			vsEdit[0].MergeRow(0, true);
			vsEdit[0].ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsEdit[0].ColWidth(0, vsEdit[0].WidthOriginal * 0);
			vsEdit[0].ColWidth(1, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.045));
			vsEdit[0].ColWidth(2, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.035));
			vsEdit[0].ColWidth(3, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.105));
			vsEdit[0].ColWidth(4, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.08));
			vsEdit[0].ColWidth(5, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.09));
			vsEdit[0].ColWidth(6, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.11));
			vsEdit[0].ColWidth(7, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.035));
			vsEdit[0].ColWidth(8, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.105));
			vsEdit[0].ColWidth(9, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.08));
			vsEdit[0].ColWidth(10, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.09));
			vsEdit[0].ColWidth(11, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.11));
			vsEdit[0].ColWidth(12, FCConvert.ToInt32(vsEdit[0].WidthOriginal * 0.12));
			vsEdit[0].TextMatrix(1, 1, "Book");
			vsEdit[0].TextMatrix(1, 2, "Bills");
			vsEdit[0].TextMatrix(1, 3, "Regular");
			vsEdit[0].TextMatrix(1, 4, "Misc+Adj");
			vsEdit[0].TextMatrix(1, 5, "Tax");
			vsEdit[0].TextMatrix(1, 6, "Total");
			vsEdit[0].TextMatrix(1, 7, "Bills");
			vsEdit[0].TextMatrix(1, 8, "Regular");
			vsEdit[0].TextMatrix(1, 9, "Misc+Adj");
			vsEdit[0].TextMatrix(1, 10, "Tax");
			vsEdit[0].TextMatrix(1, 11, "Total");
			vsEdit[0].TextMatrix(1, 12, "Totals");
			// .Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 2, 0, 12) = True
			vsEdit[0].TextMatrix(0, 2, "Water");
			vsEdit[0].TextMatrix(0, 3, "Water");
			vsEdit[0].TextMatrix(0, 4, "Water");
			vsEdit[0].TextMatrix(0, 5, "Water");
			vsEdit[0].TextMatrix(0, 6, "Water");
			vsEdit[0].TextMatrix(0, 7, "Sewer");
			vsEdit[0].TextMatrix(0, 8, "Sewer");
			vsEdit[0].TextMatrix(0, 9, "Sewer");
			vsEdit[0].TextMatrix(0, 10, "Sewer");
			vsEdit[0].TextMatrix(0, 11, "Sewer");
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				vsEdit[0].TextMatrix(0, 2, "Stormwater");
				vsEdit[0].TextMatrix(0, 3, "Stormwater");
				vsEdit[0].TextMatrix(0, 4, "Stormwater");
				vsEdit[0].TextMatrix(0, 5, "Stormwater");
				vsEdit[0].TextMatrix(0, 6, "Stormwater");
			}
			vsEdit[0].MergeRow(0, true);
			vsEdit[0].MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			vsEdit[0].ColFormat(3, "#,##0.00");
			vsEdit[0].ColFormat(4, "#,##0.00");
			vsEdit[0].ColFormat(5, "#,##0.00");
			vsEdit[0].ColFormat(6, "#,##0.00");
			vsEdit[0].ColFormat(8, "#,##0.00");
			vsEdit[0].ColFormat(9, "#,##0.00");
			vsEdit[0].ColFormat(10, "#,##0.00");
			vsEdit[0].ColFormat(11, "#,##0.00");
			vsEdit[0].ColFormat(12, "#,##0.00");
			vsEdit[0].Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, 12, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsEdit[1].ColWidth(0, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.03));
			vsEdit[1].ColWidth(1, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.1));
			vsEdit[1].ColWidth(2, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.2));
			vsEdit[1].ColWidth(3, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.13));
			vsEdit[1].ColWidth(4, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.13));
			vsEdit[1].ColWidth(5, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.11));
			vsEdit[1].ColWidth(6, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.11));
			vsEdit[1].ColWidth(7, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.08));
			vsEdit[1].ColWidth(8, FCConvert.ToInt32(vsEdit[1].WidthOriginal * 0.08));
			vsEdit[1].ColFormat(3, "#,##0.00");
			vsEdit[1].ColFormat(4, "#,##0.00");
			vsEdit[1].TextMatrix(0, 1, " Category ");
			vsEdit[1].TextMatrix(1, 1, "Code");
			vsEdit[1].TextMatrix(1, 2, "Description");
			vsEdit[1].TextMatrix(1, 3, "Water");
			vsEdit[1].TextMatrix(1, 4, "Sewer");
			vsEdit[1].TextMatrix(1, 5, "Water");
			vsEdit[1].TextMatrix(1, 6, "Sewer");
			vsEdit[1].TextMatrix(1, 7, "Water");
			vsEdit[1].TextMatrix(1, 8, "Sewer");
			vsEdit[1].TextMatrix(0, 3, "  Dollar Amounts  ");
			vsEdit[1].TextMatrix(0, 5, "   Consumption   ");
			vsEdit[1].TextMatrix(0, 7, "    Bill Count    ");
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				vsEdit[1].TextMatrix(1, 3, "Stormwater");
				vsEdit[1].TextMatrix(1, 5, "Stormwater");
				vsEdit[1].TextMatrix(1, 7, "Stormwater");
			}
			vsEdit[1].MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
			vsEdit[1].ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsEdit[1].Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, 8, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsEdit[2].ColWidth(0, FCConvert.ToInt32(vsEdit[2].WidthOriginal * 0.05));
			vsEdit[2].ColWidth(1, FCConvert.ToInt32(vsEdit[2].WidthOriginal * 0.4));
			vsEdit[2].ColWidth(2, FCConvert.ToInt32(vsEdit[2].WidthOriginal * 0.24));
			vsEdit[2].ColWidth(3, FCConvert.ToInt32(vsEdit[2].WidthOriginal * 0.24));
			vsEdit[2].MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			vsEdit[2].TextMatrix(0, 1, "Meter Size");
			vsEdit[2].TextMatrix(0, 2, "Count");
			vsEdit[2].TextMatrix(0, 3, "Consumption");
			vsEdit[2].Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 3, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void optWS_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Index == 0)
			{
				vsEdit[0].Visible = true;
				vsEdit[1].Visible = false;
				vsEdit[2].Visible = false;
			}
			else if (Index == 1)
			{
				vsEdit[1].Visible = true;
				vsEdit[0].Visible = false;
				vsEdit[2].Visible = false;
			}
			else
			{
				vsEdit[2].Visible = true;
				vsEdit[0].Visible = false;
				vsEdit[1].Visible = false;
			}
		}

		public void optWS_Click(int Index)
		{
			optWS_CheckedChanged(Index, cmbWS, new System.EventArgs());
		}

		private void optWS_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbWS.SelectedIndex;
			optWS_CheckedChanged(index, sender, e);
		}

		private void AdjustBEGridHeight()
		{
			int ExposedRows;
			ExposedRows = vsEdit[0].Rows;
			//FC:FINAL:RPU:#I977 - The size of grids was set in designer
			// this will calculate the height of the flex grid depending on how many rows are open with a max height of the 69.4% of the form height
			//if (((vsEdit[0].Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows) + 75) < (this.Height * 0.694) + 20)
			//{
			//    vsEdit[0].Height = vsEdit[0].Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows + 75;
			//}
			//else
			//{
			//    vsEdit[0].Height = FCConvert.ToInt32((this.Height * 0.694) + 20);
			//}
			int counter;
			// vbPorter upgrade warning: LevelZeroCollapsed As bool	OnWrite(bool, short)
			bool LevelZeroCollapsed = false;
			// vbPorter upgrade warning: LevelOneCollapsed As bool	OnWrite(bool, short)
			bool LevelOneCollapsed = false;
			ExposedRows = 2;
			for (counter = 2; counter <= vsEdit[1].Rows - 1; counter++)
			{
				if (vsEdit[1].RowOutlineLevel(counter) == 0)
				{
					if (vsEdit[1].IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						ExposedRows += 1;
						LevelZeroCollapsed = true;
					}
					else
					{
						ExposedRows += 1;
						LevelZeroCollapsed = false;
					}
				}
				else if (vsEdit[1].RowOutlineLevel(counter) == 1)
				{
					if (LevelZeroCollapsed == true)
					{
						// do nothing
					}
					else if (vsEdit[1].IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						ExposedRows += 1;
						LevelOneCollapsed = true;
					}
					else
					{
						ExposedRows += 1;
						LevelOneCollapsed = false;
					}
				}
				else
				{
					if (LevelZeroCollapsed == true || LevelOneCollapsed == true)
					{
						// do nothing
					}
					else
					{
						ExposedRows += 1;
					}
				}
			}
			ExposedRows = ExposedRows;
			//FC:FINAL:RPU:#I977 - The size of grids was set in designer
			// this will calculate the height of the flex grid depending on how many rows are open with a max height of the 69.4% of the form height
			//if (((vsEdit[1].Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows) + 75) < (this.Height * 0.694) + 20)
			//{
			//    vsEdit[1].Height = vsEdit[1].Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows + 75;
			//}
			//else
			//{
			//    vsEdit[1].Height = FCConvert.ToInt32((this.Height * 0.694) + 20);
			//}
			counter = 0;
			LevelZeroCollapsed = FCConvert.ToBoolean(0);
			LevelOneCollapsed = FCConvert.ToBoolean(0);
			ExposedRows = 1;
			for (counter = 1; counter <= vsEdit[2].Rows - 1; counter++)
			{
				if (vsEdit[2].RowOutlineLevel(counter) == 0)
				{
					if (vsEdit[2].IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						ExposedRows += 1;
						LevelZeroCollapsed = true;
					}
					else
					{
						ExposedRows += 1;
						LevelZeroCollapsed = false;
					}
				}
				else if (vsEdit[2].RowOutlineLevel(counter) == 1)
				{
					if (LevelZeroCollapsed == true)
					{
						// do nothing
					}
					else if (vsEdit[2].IsCollapsed(counter) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						ExposedRows += 1;
						LevelOneCollapsed = true;
					}
					else
					{
						ExposedRows += 1;
						LevelOneCollapsed = false;
					}
				}
				else
				{
					if (LevelZeroCollapsed == true || LevelOneCollapsed == true)
					{
						// do nothing
					}
					else
					{
						ExposedRows += 1;
					}
				}
			}
			//FC:FINAL:RPU:#I977 - The size of grids was set in designer
			// this will calculate the height of the flex grid depending on how many rows are open with a max height of the 69.4% of the form height
			//if (((vsEdit[2].Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows) + 75) < (this.Height * 0.694) + 20)
			//{
			//    vsEdit[2].Height = vsEdit[2].Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows + 75;
			//}
			//else
			//{
			//    vsEdit[2].Height = FCConvert.ToInt32((this.Height * 0.694) + 20);
			//}
		}

		private void Fill_BEGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will start all of the processes to fill the grids with the correct information
				int i;
				// counts the number of books
				int j = 0;
				// counts rows inside of BillingEdit.vsEdit flexgrid
				string strLine = "";
				// will hold the string created to fill the flexgrid line
				// vbPorter upgrade warning: BookWBillCount As int	OnWrite(int, double)
				int BookWBillCount = 0;
				// water bill count for book
				double BookWRegular = 0;
				// water regular for book
				double BookWMiscAdj = 0;
				// water misc + adjustment for book
				double BookWTax = 0;
				// water tax for book
				double BookWTotal = 0;
				// water total for book
				// vbPorter upgrade warning: BookSBillCount As int	OnWrite(int, double)
				int BookSBillCount = 0;
				// sewer bill count for book
				double BookSRegular = 0;
				// sewer regular for book
				double BookSMiscAdj = 0;
				// sewer misc + adjustment for book
				double BookSTax = 0;
				// sewer tax for book
				double BookSTotal = 0;
				// sewer total for book
				double BookTotal = 0;
				// book totals
				vsEdit[0].Rows = 2;
				// clear the grids
				vsEdit[1].Rows = 2;
				vsEdit[2].Rows = 1;
				// **************** Fill the Billing Summary Grid *********************
				j = 3;
				// starting row
				for (i = 1; i <= Information.UBound(BookArray, 1); i++)
				{
					// how many books in the array
					BookWBillCount = 0;
					BookWRegular = 0;
					BookWMiscAdj = 0;
					BookWTax = 0;
					BookWTotal = 0;
					BookSBillCount = 0;
					BookSRegular = 0;
					BookSMiscAdj = 0;
					BookSTax = 0;
					BookSTotal = 0;
					BookTotal = 0;
					if (j < frmCalculation.InstancePtr.vsCalc.Rows)
					{
						do
						{
							// rows in the array
							// totals by book
							if (frmCalculation.InstancePtr.vsCalc.RowOutlineLevel(j) == 2)
							{
								if (Strings.UCase(Strings.Left(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption), 3)) == "OVE")
								{
									BookWRegular += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColWater));
									BookSRegular += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColSewer));
								}
								else if (Strings.UCase(Strings.Left(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption), 3)) == "FLA")
								{
									BookWRegular += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColWater));
									BookSRegular += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColSewer));
								}
								else if (Strings.UCase(Strings.Left(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption), 3)) == "UNI")
								{
									BookWRegular += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColWater));
									BookSRegular += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColSewer));
								}
								else if (Strings.UCase(Strings.Left(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption), 3)) == "CON")
								{
									BookWRegular += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColWater));
									BookSRegular += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColSewer));
								}
								else if (Strings.UCase(Strings.Left(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption), 3)) == "MIS")
								{
									BookWMiscAdj += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColWater));
									BookSMiscAdj += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColSewer));
								}
								else if (Strings.UCase(Strings.Left(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption), 3)) == "ADJ")
								{
									BookWMiscAdj += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColWater));
									BookSMiscAdj += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColSewer));
								}
								else if (Strings.UCase(Strings.Left(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption), 3)) == "TAX")
								{
									BookWTax += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColWater));
									BookSTax += Conversion.Val(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColSewer));
								}
								else
								{
									if (frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption) != "Subtotals")
									{
										// If .TextMatrix(j, frmCalculation.lngGridColConsumption) <> "NOTE" Then
										// MsgBox "stop"
										// End If
									}
								}
								if (frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColConsumption) == "Subtotals")
								{
									if (FCConvert.ToDouble(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColWater)) != 0)
									{
										BookWBillCount += 1;
									}
									if (FCConvert.ToDouble(frmCalculation.InstancePtr.vsCalc.TextMatrix(j, frmCalculation.InstancePtr.lngGridColSewer)) != 0)
									{
										BookSBillCount += 1;
									}
								}
							}
							j += 1;
						}
						while (!(frmCalculation.InstancePtr.vsCalc.RowOutlineLevel(j - 1) == 0 || j >= frmCalculation.InstancePtr.vsCalc.Rows - 1));
					}
					BookWTotal = BookWRegular + BookWMiscAdj + BookWTax;
					BookSTotal = BookSRegular + BookSMiscAdj + BookSTax;
					BookTotal = BookWTotal + BookSTotal;
					strLine = "\t" + FCConvert.ToString(BookArray[i]);
					// col 0 and 1
					strLine += "\t" + FCConvert.ToString(BookWBillCount);
					// col 2
					strLine += "\t" + FCConvert.ToString(BookWRegular);
					// col 3
					strLine += "\t" + FCConvert.ToString(BookWMiscAdj);
					// col 4
					strLine += "\t" + FCConvert.ToString(BookWTax);
					// col 5
					strLine += "\t" + FCConvert.ToString(BookWTotal);
					// col 6
					strLine += "\t" + FCConvert.ToString(BookSBillCount);
					// col 7
					strLine += "\t" + FCConvert.ToString(BookSRegular);
					// col 8
					strLine += "\t" + FCConvert.ToString(BookSMiscAdj);
					// col 9
					strLine += "\t" + FCConvert.ToString(BookSTax);
					// col 10
					strLine += "\t" + FCConvert.ToString(BookSTotal);
					// col 11
					strLine += "\t" + FCConvert.ToString(BookTotal);
					// col 12
					vsEdit[0].AddItem(strLine, vsEdit[0].Rows);
					// insert line into the grid
					vsEdit[0].Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsEdit[0].Rows - 1, 1, vsEdit[0].Rows - 1, 12, 0x80000018);
				}
				// initialize variables to recycle them
				BookWBillCount = 0;
				BookWRegular = 0;
				BookWMiscAdj = 0;
				BookWTax = 0;
				BookWTotal = 0;
				BookSBillCount = 0;
				BookSRegular = 0;
				BookSMiscAdj = 0;
				BookSTax = 0;
				BookSTotal = 0;
				BookTotal = 0;
				// find totals for all books
				for (i = 2; i <= vsEdit[0].Rows - 1; i++)
				{
					if (vsEdit[0].RowOutlineLevel(i) == 0)
					{
						BookWBillCount += FCConvert.ToInt32(Conversion.Val(vsEdit[0].TextMatrix(i, 2)));
						BookWRegular += Conversion.Val(vsEdit[0].TextMatrix(i, 3));
						BookWMiscAdj += Conversion.Val(vsEdit[0].TextMatrix(i, 4));
						BookWTax += Conversion.Val(vsEdit[0].TextMatrix(i, 5));
						BookWTotal += Conversion.Val(vsEdit[0].TextMatrix(i, 6));
						BookSBillCount += FCConvert.ToInt32(Conversion.Val(vsEdit[0].TextMatrix(i, 7)));
						BookSRegular += Conversion.Val(vsEdit[0].TextMatrix(i, 8));
						BookSMiscAdj += Conversion.Val(vsEdit[0].TextMatrix(i, 9));
						BookSTax += Conversion.Val(vsEdit[0].TextMatrix(i, 10));
						BookSTotal += Conversion.Val(vsEdit[0].TextMatrix(i, 11));
						BookTotal += Conversion.Val(vsEdit[0].TextMatrix(i, 12));
					}
				}
				// totals line
				strLine = "\t" + "Total";
				// col 0 and 1
				strLine += "\t" + FCConvert.ToString(BookWBillCount);
				// col 2
				strLine += "\t" + Strings.Format(BookWRegular, "#,###.00");
				// col 3
				strLine += "\t" + Strings.Format(BookWMiscAdj, "#,###.00");
				// col 4
				strLine += "\t" + Strings.Format(BookWTax, "#,###.00");
				// col 5
				if (Conversion.Val(BookWTotal) == Conversion.Val(BookWRegular + BookWMiscAdj + BookWTax))
				{
					strLine += "\t" + FCConvert.ToString(BookWTotal);
					// col 6
				}
				else
				{
					strLine += "\t" + "Error";
					// col 6
				}
				strLine += "\t" + FCConvert.ToString(BookSBillCount);
				// col 7
				strLine += "\t" + FCConvert.ToString(BookSRegular);
				// col 8
				strLine += "\t" + FCConvert.ToString(BookSMiscAdj);
				// col 9
				strLine += "\t" + FCConvert.ToString(BookSTax);
				// col 10
				if (Conversion.Val(BookSTotal) == Conversion.Val(BookSRegular + BookSMiscAdj + BookSTax))
				{
					strLine += "\t" + FCConvert.ToString(BookSTotal);
					// col 11
				}
				else
				{
					strLine += "\t" + "Error";
					// col 11
				}
				// error checking for matching totals
				if (Conversion.Val(BookTotal) == Conversion.Val(BookWTotal + BookSTotal))
				{
					strLine += "\t" + FCConvert.ToString(BookTotal);
					// col 12
				}
				else
				{
					strLine += "\t" + "ERROR";
				}
				vsEdit[0].AddItem(strLine, vsEdit[0].Rows);
				// insert line into the grid
				vsEdit[0].Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsEdit[0].Rows - 1, 1, vsEdit[0].Rows - 1, 12, 0xFF8080);
				vsEdit[0].Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsEdit[0].Rows - 1, 1, vsEdit[0].Rows - 1, 12, Color.White);
				// .Cell(FCGrid.CellPropertySettings.flexcpFontBold, .rows - 1, 1, .rows - 1, 12) = True
				// ******************Fill the Category Summary Grid*********************************
				BookWBillCount = 0;
				BookSBillCount = 0;
				BookWTotal = 0;
				BookSTotal = 0;
				// vbPorter upgrade warning: BookWCons As int	OnWrite(short, double)
				int BookWCons = 0;
				// vbPorter upgrade warning: BookSCons As int	OnWrite(short, double)
				int BookSCons = 0;
				int HeaderRow = 0;
				clsDRWrapper rsBookW = new clsDRWrapper();
				// hold the current book information (Water)
				clsDRWrapper rsBookS = new clsDRWrapper();
				// hold the current book information (Sewer)
				bool Found;
				Found = false;
				for (i = 1; i <= Information.UBound(BookArray, 1); i++)
				{
					// DJW 01102013 TROUT-895 removed AND WaterCharged <> 0 to showe consumption for no charge meters
					strSQL = "SELECT [Category].Code, [Category].LongDescription, COUNT(Bill.ID) as BillCount, SUM(Bill.Consumption) as TotC, SUM(WaterCharged) as TotW  FROM (((Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID) INNER JOIN Category ON Master.WaterCategory = Category.Code) WHERE (MeterTable.Service = 'W' OR MeterTable.Service = 'B') AND Combine = 'N' AND BillStatus = 'E' AND BookNumber = " + FCConvert.ToString(BookArray[i]) + " GROUP BY Category.Code, Category.LongDescription";
					// (BillStatus = 'X' OR BillStatus = 'E') AND
					rsBookW.OpenRecordset(strSQL);
					// DJW 01102013 TROUT-895 removed AND SewerCharged <> 0 to showe consumption for no charge meters
					strSQL = "SELECT [Category].Code, [Category].LongDescription, COUNT(Bill.ID) as BillCount, SUM(Bill.Consumption) as TotC, SUM(SewerCharged) as TotS  FROM (((Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID) INNER JOIN Category ON Master.SewerCategory = Category.Code) WHERE (MeterTable.Service = 'S' OR MeterTable.Service = 'B') AND Combine = 'N' AND BillStatus = 'E' AND BookNumber = " + FCConvert.ToString(BookArray[i]) + " GROUP BY Category.Code, Category.LongDescription";
					// (BillStatus = 'X' OR BillStatus = 'E') AND
					rsBookS.OpenRecordset(strSQL);
					HeaderRow = vsEdit[1].Rows;
					strLine = "\t" + "Book #" + FCConvert.ToString(BookArray[i]);
					// col 0 and 1
					vsEdit[1].AddItem(strLine, HeaderRow);
					// adds a row that will have values entered into it at the end of this loop
					Level_6(HeaderRow, 0, 1);
					if (rsBookW.RecordCount() > 0)
					{
						do
						{
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							strLine = "\t" + rsBookW.Get_Fields("Code");
							// col 0 and 1
							strLine += "\t" + rsBookW.Get_Fields_String("LongDescription");
							// col 2
							// TODO Get_Fields: Field [TotW] not found!! (maybe it is an alias?)
							strLine += "\t" + rsBookW.Get_Fields("TotW");
							// col 3
							// TODO Get_Fields: Field [TotC] not found!! (maybe it is an alias?)
							strLine += "\t" + "\t" + rsBookW.Get_Fields("TotC");
							// col 5
							// TODO Get_Fields: Field [BillCount] not found!! (maybe it is an alias?)
							strLine += "\t" + "\t" + rsBookW.Get_Fields("BillCount");
							// col 7
							vsEdit[1].AddItem(strLine, vsEdit[1].Rows);
							Level_8(vsEdit[1].Rows - 1, 1, 1);
							rsBookW.MoveNext();
						}
						while (!rsBookW.EndOfFile());
					}
					if (rsBookS.RecordCount() > 0)
					{
						do
						{
							for (j = HeaderRow; j <= vsEdit[1].Rows - 1; j++)
							{
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								if (Conversion.Val(rsBookS.Get_Fields("Code")) == Conversion.Val(vsEdit[1].TextMatrix(j, 1)))
								{
									Found = true;
									break;
								}
								else
								{
									// MAL@20080408: Add else to reset value
									// Tracker Reference: 13031
									Found = false;
								}
							}
							if (Found == false)
							{
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								strLine = "\t" + rsBookS.Get_Fields("Code");
								// col 0 and 1
								strLine += "\t" + rsBookS.Get_Fields_String("LongDescription");
								// col 2
								// TODO Get_Fields: Field [TotS] not found!! (maybe it is an alias?)
								strLine += "\t" + "\t" + rsBookS.Get_Fields("TotS");
								// col 3
								// TODO Get_Fields: Field [TotC] not found!! (maybe it is an alias?)
								strLine += "\t" + rsBookS.Get_Fields("TotC");
								// col 5
								// TODO Get_Fields: Field [BillCount] not found!! (maybe it is an alias?)
								strLine += "\t" + "\t" + "\t" + rsBookS.Get_Fields("BillCount");
								// col 7
								vsEdit[1].AddItem(strLine, vsEdit[1].Rows);
								Level_8(vsEdit[1].Rows - 1, 1, 1);
							}
							else
							{
								// TODO Get_Fields: Field [TotS] not found!! (maybe it is an alias?)
								vsEdit[1].TextMatrix(j, 4, FCConvert.ToString(rsBookS.Get_Fields("TotS")));
								// TODO Get_Fields: Field [TotC] not found!! (maybe it is an alias?)
								vsEdit[1].TextMatrix(j, 6, FCConvert.ToString(rsBookS.Get_Fields("TotC")));
								// TODO Get_Fields: Field [BillCount] not found!! (maybe it is an alias?)
								vsEdit[1].TextMatrix(j, 8, FCConvert.ToString(rsBookS.Get_Fields("BillCount")));
							}
							rsBookS.MoveNext();
						}
						while (!rsBookS.EndOfFile());
					}
					// reinitialize and reuse variables
					BookWTotal = 0;
					BookSTotal = 0;
					BookWCons = 0;
					BookSCons = 0;
					BookWBillCount = 0;
					BookSBillCount = 0;
					for (j = HeaderRow; j <= vsEdit[1].Rows - 1; j++)
					{
						BookWTotal += Conversion.Val(vsEdit[1].TextMatrix(j, 3));
						BookSTotal += Conversion.Val(vsEdit[1].TextMatrix(j, 4));
						BookWCons += FCConvert.ToInt32(Conversion.Val(vsEdit[1].TextMatrix(j, 5)));
						BookSCons += FCConvert.ToInt32(Conversion.Val(vsEdit[1].TextMatrix(j, 6)));
						BookWBillCount += FCConvert.ToInt32(Conversion.Val(vsEdit[1].TextMatrix(j, 7)));
						BookSBillCount += FCConvert.ToInt32(Conversion.Val(vsEdit[1].TextMatrix(j, 8)));
					}
					vsEdit[1].TextMatrix(HeaderRow, 3, FCConvert.ToString(BookWTotal));
					vsEdit[1].TextMatrix(HeaderRow, 4, FCConvert.ToString(BookSTotal));
					vsEdit[1].TextMatrix(HeaderRow, 5, FCConvert.ToString(BookWCons));
					vsEdit[1].TextMatrix(HeaderRow, 6, FCConvert.ToString(BookSCons));
					vsEdit[1].TextMatrix(HeaderRow, 7, FCConvert.ToString(BookWBillCount));
					vsEdit[1].TextMatrix(HeaderRow, 8, FCConvert.ToString(BookSBillCount));
				}
				// make final totals row
				// reinitialize and reuse variables
				BookWTotal = 0;
				BookSTotal = 0;
				BookWCons = 0;
				BookSCons = 0;
				BookWBillCount = 0;
				BookSBillCount = 0;
				for (j = 2; j <= vsEdit[1].Rows - 1; j++)
				{
					// totals the book row totals
					if (vsEdit[1].RowOutlineLevel(j) == 0)
					{
						BookWTotal += Conversion.Val(vsEdit[1].TextMatrix(j, 3));
						BookSTotal += Conversion.Val(vsEdit[1].TextMatrix(j, 4));
						BookWCons += FCConvert.ToInt32(Conversion.Val(vsEdit[1].TextMatrix(j, 5)));
						BookSCons += FCConvert.ToInt32(Conversion.Val(vsEdit[1].TextMatrix(j, 6)));
						BookWBillCount += FCConvert.ToInt32(Conversion.Val(vsEdit[1].TextMatrix(j, 7)));
						BookSBillCount += FCConvert.ToInt32(Conversion.Val(vsEdit[1].TextMatrix(j, 8)));
					}
				}
				strLine = "\t" + "Grand Total:";
				// col 0,1
				strLine += "\t" + "\t" + FCConvert.ToString(BookWTotal);
				// col 2,3
				strLine += "\t" + FCConvert.ToString(BookSTotal);
				// col 4
				strLine += "\t" + FCConvert.ToString(BookWCons);
				// col 5
				strLine += "\t" + FCConvert.ToString(BookSCons);
				// col 6
				strLine += "\t" + FCConvert.ToString(BookWBillCount);
				// col 7
				strLine += "\t" + FCConvert.ToString(BookSBillCount);
				// col 8
				vsEdit[1].AddItem(strLine, vsEdit[1].Rows);
				Level_8(vsEdit[1].Rows - 1, 0, 1);
				// .IsSubtotal(.Rows - 1) = False
				vsEdit[1].Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsEdit[1].Rows - 1, 1, vsEdit[1].Rows - 1, 8, 0xFF8080);
				vsEdit[1].Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsEdit[1].Rows - 1, 1, vsEdit[1].Rows - 1, 8, Color.White);
				// .Cell(FCGrid.CellPropertySettings.flexcpFontBold, .rows - 1, 1, .rows - 1, 8) = True
				for (i = 2; i <= vsEdit[1].Rows - 1; i++)
				{
					if (vsEdit[1].RowOutlineLevel(i) == 0)
					{
						vsEdit[1].IsCollapsed(i, FCGrid.CollapsedSettings.flexOutlineCollapsed);
					}
				}
				// ******************* Fill the Meter Reports Grid *********************
				// water
				if (Information.UBound(BookArray, 1) == 0)
				{
					MessageBox.Show("No books were selected.", "Select Book", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				// DJW@01102013 TROUT-895 Removed AND WaterCharged <> 0 to shwo consumption on no charge meters
				strSQL = "SELECT MeterSizes.LongDescription as Name, COUNT(MeterTable.MeterNumber) as MeterCount, SUM(Bill.Consumption) as Cons FROM (MeterTable INNER JOIN MeterSizes ON MeterTable.Size = MeterSizes.Code) INNER JOIN Bill ON MeterTable.ID = Bill.MeterKey WHERE Combine = 'N' AND BillStatus = 'E' AND (";
				for (i = 1; i <= Information.UBound(BookArray, 1); i++)
				{
					strSQL += "MeterTable.BookNumber = " + FCConvert.ToString(BookArray[i]) + " OR ";
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ")";
				strSQL += " AND MeterTable.Service <> 'S' GROUP BY MeterSizes.Code, MeterSizes.LongDescription";
				// Bill.BillStatus = 'E' AND
				clsDRWrapper rsX = new clsDRWrapper();
				i = 1;
				rsX.OpenRecordset(strSQL);
				if (rsX.RecordCount() != 0)
				{
					i += 1;
					vsEdit[2].Rows = i;
					Level_8(vsEdit[2].Rows - 1, 0, 2);
					vsEdit[2].TextMatrix(i - 1, 1, "Water Meters");
					do
					{
						i += 1;
						vsEdit[2].Rows = i;
						Level_8(vsEdit[2].Rows - 1, 1, 2);
						vsEdit[2].TextMatrix(i - 1, 1, FCConvert.ToString(rsX.Get_Fields_String("Name")));
						// TODO Get_Fields: Field [MeterCount] not found!! (maybe it is an alias?)
						vsEdit[2].TextMatrix(i - 1, 2, FCConvert.ToString(rsX.Get_Fields("MeterCount")));
						// TODO Get_Fields: Field [Cons] not found!! (maybe it is an alias?)
						vsEdit[2].TextMatrix(i - 1, 3, FCConvert.ToString(rsX.Get_Fields("Cons")));
						rsX.MoveNext();
					}
					while (!rsX.EndOfFile());
				}
				// sewer
				// DJW@01102013 TROUT-895 Removed AND SewerCharged <> 0 to shwo consumption on no charge meters
				strSQL = "SELECT MeterSizes.LongDescription as Name, COUNT(MeterTable.MeterNumber) as MeterCount, SUM(Bill.Consumption) as Cons FROM (MeterTable INNER JOIN MeterSizes ON MeterTable.Size = MeterSizes.Code) INNER JOIN Bill ON MeterTable.ID = Bill.MeterKey WHERE Combine = 'N' AND BillStatus = 'E' AND (";
				for (i = 1; i <= Information.UBound(BookArray, 1); i++)
				{
					strSQL += "MeterTable.BookNumber = " + FCConvert.ToString(BookArray[i]) + " OR ";
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ")";
				strSQL += " AND MeterTable.Service <> 'W' GROUP BY MeterSizes.Code, MeterSizes.LongDescription";
				// AND Bill.BillStatus = 'E'
				rsX.OpenRecordset(strSQL);
				if (rsX.RecordCount() != 0)
				{
					vsEdit[2].Rows += 1;
					i = vsEdit[2].Rows;
					Level_8(vsEdit[2].Rows - 1, 0, 2);
					vsEdit[2].TextMatrix(i - 1, 1, "Sewer Meters");
					do
					{
						i += 1;
						vsEdit[2].Rows = i;
						Level_8(vsEdit[2].Rows - 1, 1, 2);
						vsEdit[2].TextMatrix(i - 1, 1, FCConvert.ToString(rsX.Get_Fields_String("Name")));
						// TODO Get_Fields: Field [MeterCount] not found!! (maybe it is an alias?)
						vsEdit[2].TextMatrix(i - 1, 2, FCConvert.ToString(rsX.Get_Fields("MeterCount")));
						// TODO Get_Fields: Field [Cons] not found!! (maybe it is an alias?)
						vsEdit[2].TextMatrix(i - 1, 3, FCConvert.ToString(rsX.Get_Fields("Cons")));
						rsX.MoveNext();
					}
					while (!rsX.EndOfFile());
				}
				else
				{
				}
				int Cons = 0;
				int Bills = 0;
				for (i = vsEdit[2].Rows - 1; i >= 1; i--)
				{
					if (vsEdit[2].RowOutlineLevel(i) > 0)
					{
						Cons += FCConvert.ToInt32(vsEdit[2].TextMatrix(i, 3));
						Bills += FCConvert.ToInt32(vsEdit[2].TextMatrix(i, 2));
					}
					else if (vsEdit[2].RowOutlineLevel(i) == 0)
					{
						vsEdit[2].TextMatrix(i, 2, FCConvert.ToString(Bills));
						Bills = 0;
						vsEdit[2].TextMatrix(i, 3, FCConvert.ToString(Cons));
						Cons = 0;
					}
				}
				for (i = 1; i <= vsEdit[2].Rows - 1; i++)
				{
					if (vsEdit[2].RowOutlineLevel(i) == 0)
					{
						// If .IsSubtotal(i) Then
						vsEdit[2].IsCollapsed(i, FCGrid.CollapsedSettings.flexOutlineCollapsed);
						// End If
					}
				}
                modColorScheme.ColorGrid(vsEdit_0);
                modColorScheme.ColorGrid(vsEdit_1);
                modColorScheme.ColorGrid(vsEdit_2);
                return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
		}
		// vbPorter upgrade warning: Row As object	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lvl As object	OnWriteFCConvert.ToInt16(
		// vbPorter upgrade warning: i As Variant, short --> As int	OnWriteFCConvert.ToInt16(
		private void Level_6(int Row, int lvl, int i)
		{
			Level(ref Row, ref lvl, i);
		}

		private void Level_8(int Row, int lvl, int i)
		{
			Level(ref Row, ref lvl, i);
		}

		private void Level(ref int Row, ref int lvl, int i)
		{
			// set the row as a group
			// row = row to set level of
			// level is ths level of the row
			// i = the index of the grid, this determines how many columns are used
			int TotCol;
			if (i == 0)
			{
				TotCol = 11;
			}
			else if (i == 1)
			{
				TotCol = 8;
			}
			else
			{
				TotCol = 3;
			}
			if (lvl == 0)
			{
				vsEdit[i].Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, TotCol, 0x80000018);
			}
			else if (lvl == 1)
			{
				vsEdit[i].Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, 1, Row, TotCol, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsEdit[i].Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, TotCol, 0x80000016);
			}
			else
			{
				vsEdit[i].Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, TotCol, Color.White);
			}
			// set the indentation level of the group
			vsEdit[i].RowOutlineLevel(Row, lvl);
			if (lvl < 1)
			{
				vsEdit[i].IsSubtotal(Row, true);
			}
		}

		private void ShowFrame(ref FCPanel fraFrame)
		{
			int wid;
			int hei;
			wid = this.Width;
			hei = this.Height;
			//FC:FINAL:RPU:#i977 - These were setted directly in designer
			//fraFrame.Left = FCConvert.ToInt32((wid - fraFrame.Width) / 2.0);
			//fraFrame.Top = FCConvert.ToInt32((hei - fraFrame.Height) / 2.5);
			fraFrame.Visible = true;
		}

		private void VSEDIT_Collapsed(ref short Index)
		{
			AdjustBEGridHeight();
		}

		private void vsEdit_AfterCollapse(short Index, object sender, DataGridViewRowEventArgs e)
		{
			VSEDIT_Collapsed(ref Index);
		}

		private void vsEdit_AfterCollapse(object sender, DataGridViewRowEventArgs e)
		{
			short index = vsEdit.GetIndex((FCGrid)sender);
			vsEdit_AfterCollapse(index, sender, e);
		}

		private void cmbWS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			optWS_CheckedChanged(sender, e);
		}
	}
}
