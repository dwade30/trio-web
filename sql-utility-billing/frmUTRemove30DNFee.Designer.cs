﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmRemove30DNFee.
	/// </summary>
	partial class frmRemove30DNFee : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public fecherFoundation.FCFrame fraDate;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel lblyearTtitle;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.cmbWS = new fecherFoundation.FCComboBox();
			this.lblWS = new fecherFoundation.FCLabel();
			this.fraDate = new fecherFoundation.FCFrame();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.lblyearTtitle = new fecherFoundation.FCLabel();
			this.lblAccountTitle = new fecherFoundation.FCLabel();
			this.lblDate = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).BeginInit();
			this.fraDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 313);
			this.BottomPanel.Size = new System.Drawing.Size(428, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraDate);
			this.ClientArea.Size = new System.Drawing.Size(428, 253);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(428, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(314, 30);
			this.HeaderText.Text = "Remove 30 Day Notice Fee";
			// 
			// cmbWS
			// 
			this.cmbWS.AutoSize = false;
			this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWS.FormattingEnabled = true;
			this.cmbWS.Items.AddRange(new object[] {
				"Water",
				"Sewer"
			});
			this.cmbWS.Location = new System.Drawing.Point(186, 76);
			this.cmbWS.Name = "cmbWS";
			this.cmbWS.Size = new System.Drawing.Size(202, 40);
			this.cmbWS.TabIndex = 2;
			this.cmbWS.Text = "Water";
			// 
			// lblWS
			// 
			this.lblWS.AutoSize = true;
			this.lblWS.Location = new System.Drawing.Point(30, 90);
			this.lblWS.Name = "lblWS";
			this.lblWS.Size = new System.Drawing.Size(107, 15);
			this.lblWS.TabIndex = 1;
			this.lblWS.Text = "WATER / SEWER";
			// 
			// fraDate
			// 
			this.fraDate.AppearanceKey = "groupBoxNoBorders";
			this.fraDate.Controls.Add(this.cmbYear);
			this.fraDate.Controls.Add(this.cmbWS);
			this.fraDate.Controls.Add(this.lblWS);
			this.fraDate.Controls.Add(this.txtAccount);
			this.fraDate.Controls.Add(this.lblyearTtitle);
			this.fraDate.Controls.Add(this.lblAccountTitle);
			this.fraDate.Controls.Add(this.lblDate);
            this.fraDate.Controls.Add(this.cmdFilePrint);
			this.fraDate.Location = new System.Drawing.Point(0, 0);
			this.fraDate.Name = "fraDate";
			this.fraDate.Size = new System.Drawing.Size(389, 299);
			this.fraDate.TabIndex = 0;
			this.fraDate.Visible = false;
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(186, 196);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(202, 40);
			this.cmbYear.TabIndex = 6;
			this.cmbYear.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbYear_KeyDown);
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.LinkItem = null;
			this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAccount.LinkTopic = null;
			this.txtAccount.Location = new System.Drawing.Point(186, 136);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(202, 40);
			this.txtAccount.TabIndex = 4;
			this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
			this.txtAccount.TextChanged += new System.EventHandler(this.txtAccount_TextChanged);
			this.txtAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccount_Validating);
			// 
			// lblyearTtitle
			// 
			this.lblyearTtitle.Location = new System.Drawing.Point(30, 210);
			this.lblyearTtitle.Name = "lblyearTtitle";
			this.lblyearTtitle.Size = new System.Drawing.Size(74, 20);
			this.lblyearTtitle.TabIndex = 5;
			this.lblyearTtitle.Text = "RATE KEY";
			// 
			// lblAccountTitle
			// 
			this.lblAccountTitle.Location = new System.Drawing.Point(30, 150);
			this.lblAccountTitle.Name = "lblAccountTitle";
			this.lblAccountTitle.Size = new System.Drawing.Size(74, 16);
			this.lblAccountTitle.TabIndex = 3;
			this.lblAccountTitle.Text = "ACCOUNT";
			// 
			// lblDate
			// 
			this.lblDate.Location = new System.Drawing.Point(30, 30);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(359, 30);
			this.lblDate.TabIndex = 0;
			this.lblDate.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePrint.Text = "Save";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.AppearanceKey = "acceptButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(30, 256);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePrint.Size = new System.Drawing.Size(100, 48);
			this.cmdFilePrint.TabIndex = 0;
			this.cmdFilePrint.Text = "Save";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmRemove30DNFee
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = null;
			this.ClientSize = new System.Drawing.Size(428, 421);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmRemove30DNFee";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Remove 30 Day Notice Fee";
			this.Load += new System.EventHandler(this.frmRemove30DNFee_Load);
			this.Activated += new System.EventHandler(this.frmRemove30DNFee_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmRemove30DNFee_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).EndInit();
			this.fraDate.ResumeLayout(false);
			this.fraDate.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFilePrint;
	}
}
