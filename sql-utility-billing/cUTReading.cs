//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class cUTReading
	{

		//=========================================================

		private int lngReading;
		private DateTime dtReadingDate;
		private string strReadingCode;

		public int Reading
		{
			set
			{
				lngReading = value;
			}

			get
			{
					int Reading = 0;
				Reading = lngReading;
				return Reading;
			}
		}




		public DateTime ReadingDate
		{
			set
			{
				dtReadingDate = value;
			}

			get
			{
					DateTime ReadingDate = System.DateTime.Now;
				ReadingDate = dtReadingDate;
				return ReadingDate;
			}
		}



		public string ReadingCode
		{
			set
			{
				strReadingCode = value;
			}

			get
			{
					string ReadingCode = "";
				ReadingCode = strReadingCode;
				return ReadingCode;
			}
		}



		public cUTReading GetCopy()
		{
			cUTReading GetCopy = null;
			cUTReading returnCopy = new/*AsNew*/ cUTReading();
			returnCopy.Reading = Reading;
			returnCopy.ReadingCode = ReadingCode;
			returnCopy.ReadingDate = ReadingDate;
			GetCopy = returnCopy;
			return GetCopy;
		}

	}
}
