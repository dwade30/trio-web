﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptMeterImportErrorReport.
	/// </summary>
	partial class rptMeterImportErrorReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMeterImportErrorReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAddress = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReadingDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReading = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReading = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReadingDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldReadingType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReadingType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReadingDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReadingDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReadingType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReadingType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccountNumber,
				this.fldName,
				this.fldReadingDate,
				this.fldReading,
				this.fldAddress1,
				this.lblReading,
				this.lblReadingDate,
				this.fldReadingType,
				this.lblReadingType
			});
			this.Detail.Height = 0.4895833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCountTotal,
				this.fldTotalCount
			});
			this.ReportFooter.Height = 0.4375F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccountNumber,
				this.lblName,
				this.Label1,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.Line2,
				this.lblAddress
			});
			this.PageHeader.Height = 0.71875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.Height = 0.1875F;
			this.lblAccountNumber.HyperLink = null;
			this.lblAccountNumber.Left = 0F;
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblAccountNumber.Text = "Account";
			this.lblAccountNumber.Top = 0.5F;
			this.lblAccountNumber.Width = 1.1875F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 1.25F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.5F;
			this.lblName.Width = 2.4375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label1.Text = "Meter Import Error Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 6.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.2F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.55F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.2F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 5.55F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.2F;
			this.lblPageNumber.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.6875F;
			this.Line2.Width = 6.5F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 6.5F;
			this.Line2.Y1 = 0.6875F;
			this.Line2.Y2 = 0.6875F;
			// 
			// lblAddress
			// 
			this.lblAddress.Height = 0.1875F;
			this.lblAddress.HyperLink = null;
			this.lblAddress.Left = 3.75F;
			this.lblAddress.Name = "lblAddress";
			this.lblAddress.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblAddress.Text = "Address";
			this.lblAddress.Top = 0.5F;
			this.lblAddress.Width = 2.4375F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 0F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldAccountNumber.Text = null;
			this.fldAccountNumber.Top = 0F;
			this.fldAccountNumber.Width = 1.1875F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1.25F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2.4375F;
			// 
			// fldReadingDate
			// 
			this.fldReadingDate.Height = 0.1875F;
			this.fldReadingDate.Left = 3.625F;
			this.fldReadingDate.Name = "fldReadingDate";
			this.fldReadingDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldReadingDate.Text = null;
			this.fldReadingDate.Top = 0.1875F;
			this.fldReadingDate.Width = 1F;
			// 
			// fldReading
			// 
			this.fldReading.Height = 0.1875F;
			this.fldReading.Left = 1.4375F;
			this.fldReading.Name = "fldReading";
			this.fldReading.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldReading.Text = null;
			this.fldReading.Top = 0.1875F;
			this.fldReading.Width = 0.9375F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 3.75F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldAddress1.Text = null;
			this.fldAddress1.Top = 0F;
			this.fldAddress1.Width = 2.4375F;
			// 
			// lblReading
			// 
			this.lblReading.Height = 0.1875F;
			this.lblReading.HyperLink = null;
			this.lblReading.Left = 0.6875F;
			this.lblReading.Name = "lblReading";
			this.lblReading.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblReading.Text = "Reading:";
			this.lblReading.Top = 0.1875F;
			this.lblReading.Width = 0.75F;
			// 
			// lblReadingDate
			// 
			this.lblReadingDate.Height = 0.1875F;
			this.lblReadingDate.HyperLink = null;
			this.lblReadingDate.Left = 2.5F;
			this.lblReadingDate.Name = "lblReadingDate";
			this.lblReadingDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblReadingDate.Text = "Reading Date:";
			this.lblReadingDate.Top = 0.1875F;
			this.lblReadingDate.Width = 1.125F;
			// 
			// fldReadingType
			// 
			this.fldReadingType.Height = 0.1875F;
			this.fldReadingType.Left = 5.875F;
			this.fldReadingType.Name = "fldReadingType";
			this.fldReadingType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldReadingType.Text = null;
			this.fldReadingType.Top = 0.1875F;
			this.fldReadingType.Width = 0.5F;
			// 
			// lblReadingType
			// 
			this.lblReadingType.Height = 0.1875F;
			this.lblReadingType.HyperLink = null;
			this.lblReadingType.Left = 4.75F;
			this.lblReadingType.Name = "lblReadingType";
			this.lblReadingType.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblReadingType.Text = "Reading Type:";
			this.lblReadingType.Top = 0.1875F;
			this.lblReadingType.Width = 1.125F;
			// 
			// fldCountTotal
			// 
			this.fldCountTotal.Height = 0.1875F;
			this.fldCountTotal.Left = 3.9375F;
			this.fldCountTotal.Name = "fldCountTotal";
			this.fldCountTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldCountTotal.Text = null;
			this.fldCountTotal.Top = 0.125F;
			this.fldCountTotal.Width = 0.625F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 1.125F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldTotalCount.Text = "Total Count:";
			this.fldTotalCount.Top = 0.125F;
			this.fldTotalCount.Width = 2.5F;
			// 
			// rptMeterImportErrorReport
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReadingDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReadingDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReadingType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReadingType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReadingDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReading;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReading;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReadingDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReadingType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReadingType;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAddress;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
