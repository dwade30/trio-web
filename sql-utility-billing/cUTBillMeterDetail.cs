//Fecher vbPorter - Version 1.0.0.90

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class cUTBillMeterDetail
	{

		//=========================================================

		private int lngCurrentReading;
		private int lngPreviousReading;
		private double dblConsumption;
		private bool boolIsEstimate;
		private bool boolPrevChangeOut;
		private double dblChangeoutConsumption;

        public int Sequence { get; set; } = 0;



		public int currentReading
		{
			set
			{
				lngCurrentReading = value;
			}

			get
			{
					int currentReading = 0;
				currentReading = lngCurrentReading;
				return currentReading;
			}
		}



		public int previousReading
		{
			set
			{
				lngPreviousReading = value;
			}

			get
			{
					int previousReading = 0;
				previousReading = lngPreviousReading;
				return previousReading;
			}
		}



		public double Consumption
		{
			set
			{
				dblConsumption = value;
			}

			get
			{
					double Consumption = 0;
				Consumption = dblConsumption;
				return Consumption;
			}
		}



		public bool IsEstimate
		{
			set
			{
				boolIsEstimate = value;
			}

			get
			{
					bool IsEstimate = false;
				IsEstimate = boolIsEstimate;
				return IsEstimate;
			}
		}



		public bool WasChangedOut
		{
			set
			{
				boolPrevChangeOut = value;
			}

			get
			{
					bool WasChangedOut = false;
				WasChangedOut = boolPrevChangeOut;
				return WasChangedOut;
			}
		}



		public double ChangeoutConsumption
		{
			set
			{
				dblChangeoutConsumption = value;
			}

			get
			{
					double ChangeoutConsumption = 0;
				ChangeoutConsumption = dblChangeoutConsumption;
				return ChangeoutConsumption;
			}
		}



		public cUTBillMeterDetail GetCopy()
		{
			cUTBillMeterDetail GetCopy = null;
			cUTBillMeterDetail mDetail = new/*AsNew*/ cUTBillMeterDetail();
			mDetail.ChangeoutConsumption = ChangeoutConsumption;
			mDetail.Consumption = Consumption;
			mDetail.currentReading = currentReading;
			mDetail.IsEstimate = IsEstimate;
			mDetail.previousReading = previousReading;
			mDetail.WasChangedOut = WasChangedOut;
			mDetail.Sequence = Sequence;
			GetCopy = mDetail;
			return GetCopy;
		}

	}
}
