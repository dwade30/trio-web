//Fecher vbPorter - Version 1.0.0.90

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class clsACHOriginatorInfo
    {


        public string ImmediateOriginName { get; set; } = "";


        public string ImmediateOriginRT { get; set; } = "";

        public string OriginatingDFI { get; set; } = "";

	}
}
