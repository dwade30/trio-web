﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptLienFeeIncr.
	/// </summary>
	public partial class rptLienFeeIncr : BaseSectionReport
	{
		public rptLienFeeIncr()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActiveReport1";
		}

		public static rptLienFeeIncr InstancePtr
		{
			get
			{
				return (rptLienFeeIncr)Sys.GetInstance(typeof(rptLienFeeIncr));
			}
		}

		protected rptLienFeeIncr _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLienFeeIncr	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int lngRecCnt;
		int lngProcCnt;

		public void Init(ref string strResult, ref string strTitle)
		{
			frmReportViewer.InstancePtr.Init(this._InstancePtr);
			//this.Show(FCForm.FormShowEnum.Modal);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = FCConvert.CBool(lngRecCnt > Information.UBound(modUTLien.Statics.arrDemand, 1));
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngRecCnt = 0;
			lngProcCnt = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			while (modUTLien.Statics.arrDemand[lngRecCnt].Processed == false)
			{
				lngRecCnt += 1;
				if (lngRecCnt > Information.UBound(modUTLien.Statics.arrDemand, 1))
				{
					Detail.Visible = false;
					return;
				}
			}
			fldAccount.Text = modUTLien.Statics.arrDemand[lngRecCnt].Account.ToString();
			fldName.Text = modUTLien.Statics.arrDemand[lngRecCnt].Name;
			fldYear.Text = modUTLien.Statics.arrDemand[lngRecCnt].BillNumber.ToString();
			lngProcCnt += 1;
			lngRecCnt += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldProcCnt.Text = lngProcCnt.ToString();
			fldTotalIncrease.Text = Strings.Format(lngProcCnt * 6, "Currency");
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			fldDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			fldUserID.Text = modGlobalConstants.Statics.gstrUserID;
		}

		private void rptLienFeeIncr_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLienFeeIncr properties;
			//rptLienFeeIncr.Caption	= "ActiveReport1";
			//rptLienFeeIncr.Left	= 0;
			//rptLienFeeIncr.Top	= 0;
			//rptLienFeeIncr.Width	= 19080;
			//rptLienFeeIncr.Height	= 11490;
			//rptLienFeeIncr.StartUpPosition	= 3;
			//rptLienFeeIncr.SectionData	= "rptLienFeeIncr.dsx":0000;
			//End Unmaped Properties
		}
	}
}
