﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmTaxAcquiredList.
	/// </summary>
	public partial class frmTaxAcquiredList : BaseForm
	{
		public frmTaxAcquiredList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTaxAcquiredList InstancePtr
		{
			get
			{
				return (frmTaxAcquiredList)Sys.GetInstance(typeof(frmTaxAcquiredList));
			}
		}

		protected frmTaxAcquiredList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/11/2007              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/11/2007              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		bool boolChargeForNewOwner;
		DateTime dtMailDate;
		clsDRWrapper rsRE = new clsDRWrapper();
		int lngBillingYear;
		string strYearList = "";
		int lngValidateRowYear;
		int lngGridColTaxAcquired;
		int lngGridColAccount;
		int lngGridColName;
		int lngGridColLocation;
		int lngGridColOSTaxYear;
		int lngGridColOSTaxTotal;
		int lngGridColBillKey;

		private void chkUpdateRE_CheckedChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: intAnswer As short, int --> As DialogResult
			DialogResult intAnswer;
			if ((modGlobalConstants.Statics.gblnAutoUpdateRETaxAcq && chkUpdateRE.CheckState == Wisej.Web.CheckState.Unchecked) || (!modGlobalConstants.Statics.gblnAutoUpdateRETaxAcq && chkUpdateRE.CheckState == Wisej.Web.CheckState.Checked))
			{
				intAnswer = MessageBox.Show("Are you sure you wish to override the default setting?", "Override Default?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intAnswer == DialogResult.Yes)
				{
					// Do Nothing
				}
				else
				{
					if (chkUpdateRE.CheckState == Wisej.Web.CheckState.Checked)
					{
						chkUpdateRE.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					else
					{
						chkUpdateRE.CheckState = Wisej.Web.CheckState.Checked;
					}
				}
			}
		}

		private void frmTaxAcquiredList_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				this.Text = "Tax Acquired Properties";
				lblInstructionHeader.Visible = true;
				// False 'commented back in from a bug fix #4854
				lblInstruction.Visible = true;
				lblValidateInstruction.Visible = true;
				// MAL@20080812 ; Tracker Reference: 14035
				lblInstruction.Text = "To Set Account To Tax Aquired:" + "\r\n" + "     1. Enter Tax Acquired Date" + "\r\n" + "     2. Check the box beside the account" + "\r\n" + "     3. Select 'Save' or press F12";
				if (modGlobalConstants.Statics.gblnAutoUpdateRETaxAcq)
				{
					chkUpdateRE.CheckState = Wisej.Web.CheckState.Checked;
					chkUpdateRE.Enabled = true;
				}
				else
				{
					chkUpdateRE.CheckState = Wisej.Web.CheckState.Unchecked;
					chkUpdateRE.Enabled = false;
				}
				lblValidateInstruction.Text = "Select a billing rate key";
				lblValidateInstruction.Alignment = HorizontalAlignment.Center;
				//Application.DoEvents();
				FillValidateGrid();
				if (modGlobalConstants.Statics.gboolBD)
				{
					modValidateAccount.SetBDFormats();
				}
				ShowGridFrame();
				SetAct_2(0);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				vsValidate.ComboList = strYearList;
				vsValidate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsValidate.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
		}

		private void frmTaxAcquiredList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTaxAcquiredList properties;
			//frmTaxAcquiredList.FillStyle	= 0;
			//frmTaxAcquiredList.ScaleWidth	= 9195;
			//frmTaxAcquiredList.ScaleHeight	= 7785;
			//frmTaxAcquiredList.LinkTopic	= "Form2";
			//frmTaxAcquiredList.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			lngValidateRowYear = 0;
			lngGridColTaxAcquired = 0;
			lngGridColBillKey = 1;
			lngGridColAccount = 2;
			lngGridColName = 3;
			lngGridColLocation = 4;
			lngGridColOSTaxYear = 5;
			lngGridColOSTaxTotal = 6;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FCUtils.EraseSafe(modUTLien.Statics.arrDemand);
			FCUtils.EraseSafe(modMain.Statics.arrTALienPrincipalW);
			FCUtils.EraseSafe(modMain.Statics.arrTAPrincipalW);
			FCUtils.EraseSafe(modMain.Statics.arrTALienPrincipalS);
			FCUtils.EraseSafe(modMain.Statics.arrTAPrincipalS);
		}

		private void frmTaxAcquiredList_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmTaxAcquiredList_Resize(object sender, System.EventArgs e)
		{
			FormatGrid_2(true);
			if (fraGrid.Visible)
			{
				ShowGridFrame();
			}
			else
			{
				ShowValidateFrame();
			}
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
			//Application.DoEvents();
			//if (MDIParent.InstancePtr.Enabled && MDIParent.InstancePtr.Visible)
			//{
			//	MDIParent.InstancePtr.Focus();
			//}
		}

		private void ShowValidateFrame()
		{
			// this will show/center the frame with the grid on it
			//fraValidate.Top = FCConvert.ToInt32((this.Height - fraValidate.Height) / 3.0);
			//fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
			fraValidate.Visible = true;
			//// this will set the height of the grid
			//if (vsValidate.Rows > 0)
			//{
			//    vsValidate.Height = ((vsValidate.Rows) * vsValidate.RowHeight(0)) + 70;
			//}
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
			//if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.Height - lblInstruction.Height - 500)
			//{
			//    vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500;
			//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70;
			//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void FormatGrid_2(bool boolResize)
		{
			FormatGrid(boolResize);
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 7;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngGridColTaxAcquired, FCConvert.ToInt32(wid * 0.04));
			// checkbox
			vsDemand.ColWidth(lngGridColBillKey, 0);
			// BillKey
			vsDemand.ColWidth(lngGridColAccount, FCConvert.ToInt32(wid * 0.08));
			// Acct
			vsDemand.ColWidth(lngGridColName, FCConvert.ToInt32(wid * 0.45));
			// Account
			vsDemand.ColWidth(lngGridColLocation, FCConvert.ToInt32(wid * 0.2));
			// Location
			vsDemand.ColWidth(lngGridColOSTaxYear, FCConvert.ToInt32(wid * 0.1));
			vsDemand.ColWidth(lngGridColOSTaxTotal, FCConvert.ToInt32(wid * 0.1));
			// .ColFormat(lngGridColOSTaxYear) = "#,##0.00"
			vsDemand.ColFormat(lngGridColOSTaxTotal, "#,##0.00");
			vsDemand.ColDataType(lngGridColTaxAcquired, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColOSTaxYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngGridColOSTaxTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, lngGridColTaxAcquired, "TA");
			vsDemand.TextMatrix(0, lngGridColAccount, "Acct");
			vsDemand.TextMatrix(0, lngGridColName, "Name");
			vsDemand.TextMatrix(0, lngGridColLocation, "Location");
			vsDemand.TextMatrix(0, lngGridColOSTaxTotal, "Total Due");
			//FC:FINAL:DDU:#i983 - set the fixedcols first
			vsValidate.FixedCols = 1;
			vsValidate.Cols = 3;
			wid = vsValidate.WidthOriginal;
			vsValidate.ColWidth(0, FCConvert.ToInt32(wid * 0.7));
			// Question
			vsValidate.ColWidth(1, FCConvert.ToInt32(wid * 0.28));
			// Answer
			vsValidate.ColWidth(2, 0);
			// Hidden field
			vsValidate.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// .TextMatrix(0, 1) = "Parameters"
			// .TextMatrix(0, 2) = "Value"
		}

		private void FillDemandGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				clsDRWrapper rsRK = new clsDRWrapper();
				DateTime dtBillDate;
				string strSQL;
				int lngIndex;
				double dblXInt = 0;
				double dblBillAmount = 0;
				double dblTotalAmount = 0;
				clsDRWrapper rsLN = new clsDRWrapper();
				string strTest = "";
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				vsDemand.Rows = 1;
				lngBillingYear = FCConvert.ToInt32(Math.Round(Conversion.Val(vsValidate.TextMatrix(lngValidateRowYear, 1))));
				// FormatYear(vsValidate.TextMatrix(lngValidateRowYear, 1))
				vsDemand.TextMatrix(0, lngGridColOSTaxYear, Strings.Format(FCConvert.ToString(Conversion.Val(vsValidate.TextMatrix(lngValidateRowYear, 1))), "0000"));
				strSQL = "SELECT * FROM Bill WHERE BillNumber = " + FCConvert.ToString(lngBillingYear) + " ORDER BY BName, ActualAccountNumber";
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (modGlobalConstants.Statics.gboolRE && modGlobalConstants.Statics.gblnCheckRETaxAcq)
				{
					strTest = modUTStatusPayments.GetTaxAcquiredAccountList();
					if (strTest != "")
					{
						rsRE.OpenRecordset("SELECT * FROM Master WHERE AccountNumber NOT IN (" + modUTStatusPayments.GetTaxAcquiredAccountList() + ") AND ISNULL(InBankruptcy,0) = 0 ORDER BY AccountNumber", modExtraModules.strUTDatabase);
					}
					else
					{
						rsRE.OpenRecordset("SELECT * FROM Master WHERE ISNULL(InBankruptcy,0) = 0 ORDER BY AccountNumber", modExtraModules.strUTDatabase);
					}
				}
				else
				{
					rsRE.OpenRecordset("SELECT * FROM Master WHERE ISNULL(TaxAcquired,0) = 0 AND ISNULL(InBankruptcy,0) = 0 ORDER BY AccountNumber", modExtraModules.strUTDatabase);
				}
				if (rsRE.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("There are no accounts set to Tax Acquired", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Unload();
				}
				lngIndex = -1;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Accounts", true, rsRE.RecordCount(), true);
				while (!rsRE.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					// kgk            rsData.FindFirstRecord , , "BillNumber = " & lngBillingYear & " AND AccountKey = " & rsRE.Fields("Key")
					rsData.FindFirstRecord2("BillNumber,AccountKey", FCConvert.ToString(lngBillingYear) + "," + rsRE.Get_Fields_Int32("ID"), ",");
					if (rsData.NoMatch)
					{
						// skip this account because there are no eligible bills
						goto SKIP;
					}
					dblBillAmount = 0;
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("WLienRecordNumber")) != 0)
					{
						rsLN.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsData.Get_Fields_Int32("WLienRecordNumber"), modExtraModules.strUTDatabase);
						if (!rsLN.EndOfFile())
						{
							dblBillAmount = modUTCalculations.CalculateAccountUTLien(rsLN, dtMailDate, ref dblXInt, true);
						}
					}
					else
					{
						dblBillAmount = modUTCalculations.CalculateAccountUT(rsData, dtMailDate, ref dblXInt, true);
					}
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SLienRecordNumber")) != 0)
					{
						rsLN.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsData.Get_Fields_Int32("SLienRecordNumber"), modExtraModules.strUTDatabase);
						if (!rsLN.EndOfFile())
						{
							dblBillAmount += modUTCalculations.CalculateAccountUTLien(rsLN, dtMailDate, ref dblXInt, false);
						}
					}
					else
					{
						dblBillAmount += modUTCalculations.CalculateAccountUT(rsData, dtMailDate, ref dblXInt, false);
					}
					if (dblBillAmount <= 0)
					{
						// skip this account because this account has already been paid
						goto SKIP;
					}
					// add a row/element
					vsDemand.AddItem("");
					Array.Resize(ref modUTLien.Statics.arrDemand, vsDemand.Rows - 1 + 1);
					// this will make sure that there are enough elements to use
					lngIndex = vsDemand.Rows - 2;
					modUTLien.Statics.arrDemand[lngIndex].Used = true;
					modUTLien.Statics.arrDemand[lngIndex].Processed = false;
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColAccount, FCConvert.ToString(rsData.Get_Fields_Int32("ActualAccountNumber")));
					// account number
					modUTLien.Statics.arrDemand[lngIndex].Account = FCConvert.ToInt32(rsData.Get_Fields_Int32("ActualAccountNumber"));
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColName, FCConvert.ToString(rsData.Get_Fields_String("BName")));
					// name
					modUTLien.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("BName"));
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColBillKey, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					// billkey
					modUTLien.Statics.arrDemand[lngIndex].BillKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("ID"));
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location"))) != "")
					{
						vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location"))));
					}
					else
					{
						// if there is nothing in the bill then show the RE Master info
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						if (Strings.Trim(FCConvert.ToString(rsRE.Get_Fields("StreetNumber"))) != "")
						{
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(rsRE.Get_Fields("StreetNumber") + " " + rsRE.Get_Fields_String("StreetName")));
							// location
						}
						else
						{
							vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsRE.Get_Fields_String("StreetName"))));
							// location
						}
					}
					// amounts
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColOSTaxYear, Strings.Format(dblBillAmount, "#,##0.00"));
					dblTotalAmount = modUTCalculations.CalculateAccountUTTotal(rsData.Get_Fields_Int32("AccountKey"), true) + modUTCalculations.CalculateAccountUTTotal(rsData.Get_Fields_Int32("AccountKey"), false);
					vsDemand.TextMatrix(vsDemand.Rows - 1, lngGridColOSTaxTotal, FCConvert.ToString(dblTotalAmount));
					SKIP:
					;
					rsRE.MoveNext();
				}
				if (lngIndex < 0)
				{
					frmWait.InstancePtr.Unload();
					// MAL@20081104: Corrected the wording to make it simpler and easier to understand
					// Tracker Reference: 15536
					// MsgBox "There are no " & lngBillingYear & " bills eligible for Tax Acquired Status.", vbInformation, "No Eligible Bills"
					MessageBox.Show("There are no bills eligible for Tax Acquired Status", "No Eligible Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Unload();
				}
				// check all of the accounts
				mnuFileSelectAll_Click();
				//if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.Height - lblInstruction.Height - 500)
				//{
				//    vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500;
				//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//    vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70;
				//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillValidateGrid()
		{
			// this will fill the validate grid with the information from the Control_30DayNotice table
			// which is the information that was used to print the 30 Day Notices the last time
			clsDRWrapper rsValidate = new clsDRWrapper();
			clsDRWrapper rsRate = new clsDRWrapper();
			rsValidate.OpenRecordset("SELECT DISTINCT BillNumber FROM Bill WHERE WLienRecordNumber <> 0 OR SLienRecordNumber <> 0 ORDER BY BillNumber desc", modExtraModules.strUTDatabase);
			rsRate.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
			//FC:FINAL:CHN - issue #1153: fixed focus index in combobox. Refix.
			strYearList = "";
			rsValidate.MoveFirst();
			while (!rsValidate.EndOfFile())
			{
				rsRate.FindFirstRecord("ID", rsValidate.Get_Fields_Int32("BillNumber"));
				if (rsRate.NoMatch)
				{
					strYearList += rsValidate.Get_Fields_Int32("BillNumber") + "|";
				}
				else
				{
					strYearList += rsValidate.Get_Fields_Int32("BillNumber") + " - " + Strings.Format(rsRate.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "|";
				}
				rsValidate.MoveNext();
			}
			if (strYearList.Length > 0)
			{
				strYearList = strYearList.Substring(0, strYearList.Length - 1);
				// delete empty variant from choice
			}
			vsValidate.Rows = 1;
			vsValidate.TextMatrix(0, 0, "Bill Number");
			// set the height of the grid
			//if ((vsValidate.Rows) * vsValidate.RowHeight(0) > fraValidate.Height - 300)
			//{
			//    vsValidate.Height = fraValidate.Height - 300;
			//    vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsValidate.Height = (vsValidate.Rows) * vsValidate.RowHeight(0) + 70;
			//    vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void SetAct_2(short intAct)
		{
			SetAct(ref intAct);
		}

		private void SetAct(ref short intAct)
		{
			// this will change all of the menu options
			switch (intAct)
			{
				case 0:
					{
						ShowValidateFrame();
						fraGrid.Visible = false;
						//FC:FINAL:CHN - issue #1155: Save button does not work. Need to rename button.
						cmdSave.Text = "Accept Parameters";
						mnuFilePrint.Text = "Accept Parameters";
						mnuFileSelectAll.Visible = false;
						//FC:FINAL:MSH - issue #1150: change visibility of the button
						cmdFileSelectAll.Visible = false;
						intAction = 0;
						break;
					}
				case 1:
					{
						ShowGridFrame();
						//FC:FINAL:MSH - issue #1150: move changing visibility here to avoid errors if form will be disposed
						mnuFileSelectAll.Visible = true;
						//FC:FINAL:MSH - issue #1150: change visibility of the button
						cmdFileSelectAll.Visible = true;
						FillDemandGrid();
						fraValidate.Visible = false;
						//FC:FINAL:CHN - issue #1155: Save button does not work. Need to rename button.
						cmdSave.Text = "Save";
						mnuFilePrint.Text = "Save";
						intAction = 1;
						break;
					}
			}
			//end switch
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						vsValidate.Select(0, 1);
						// this will say that the user accepts the parameters shown
						if (vsValidate.TextMatrix(lngValidateRowYear, 1) != "")
						{
							// MsgBox "After demand fees are applied, it will not be possible to reprint the notices, labels or certified mailers.", vbExclamation, "Apply Demand Fees"
							SetAct_2(1);
						}
						else
						{
							MessageBox.Show("Please enter a bill number.", "Missing Paremeters", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						break;
					}
				case 1:
					{
						AffectTaxAcquiredProperties();
						this.Unload();
						//Application.DoEvents();
						//if (MDIParent.InstancePtr.Enabled && MDIParent.InstancePtr.Visible)
						//{
						//	MDIParent.InstancePtr.Focus();
						//}
						break;
					}
			}
			//end switch
		}

		private void AffectTaxAcquiredProperties()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngCount = 0;
				clsDRWrapper rsRate = new clsDRWrapper();
				// Start fresh
				FCUtils.EraseSafe(modMain.Statics.arrTALienPrincipalW);
				FCUtils.EraseSafe(modMain.Statics.arrTAPrincipalW);
				FCUtils.EraseSafe(modMain.Statics.arrTALienPrincipalS);
				FCUtils.EraseSafe(modMain.Statics.arrTAPrincipalS);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Tax Acquired");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					//Application.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						modUTLien.Statics.arrDemand[FindDemandIndex(FCConvert.ToInt32(vsDemand.TextMatrix(lngCT, lngGridColBillKey)))].Processed = true;
						CalculateTAJournalEntries_2(FCConvert.ToInt32(Conversion.Val(vsDemand.TextMatrix(lngCT, lngGridColAccount))));
						lngCount += 1;
					}
					else
					{
						// not selected...do nothing
					}
				}
				frmWait.InstancePtr.Unload();
				if (CreateTAJournalEntry())
				{
					frmWait.InstancePtr.Unload();
					if (lngCount != 1)
					{
						MessageBox.Show(FCConvert.ToString(lngCount) + " accounts were affected.", "Tax Acquired Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show(FCConvert.ToString(lngCount) + " account was affected.", "Tax Acquired Status", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					// update RE
					if (!UpdateTaxAcquiredAccounts())
					{
						MessageBox.Show("Errors setting tax acquired status.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					// print the list of accounts that have been affected
					rptTaxAcquiredList.InstancePtr.Init(ref lngCount);
					frmReportViewer.InstancePtr.Init(rptTaxAcquiredList.InstancePtr);
				}
				else
				{
					// Failed the journal entry
					// do not set the acquired field and do not show any report
					MessageBox.Show("The Budgetary journal entries were not created and the Tax Acquired field has not been updated.", "Error Updating", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Affecting Status", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool UpdateTaxAcquiredAccounts()
		{
			bool UpdateTaxAcquiredAccounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				clsDRWrapper rsReal = new clsDRWrapper();
				UpdateTaxAcquiredAccounts = true;
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					//Application.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						rsRE.FindFirstRecord("AccountNumber", vsDemand.TextMatrix(lngCT, lngGridColAccount));
						if (!rsRE.NoMatch)
						{
							rsRE.Edit();
							rsRE.Set_Fields("TaxAcquired", true);
							// MAL@20080812 ; Tracker Reference: 14035
							// rsRE.Fields("TADate") = Now
							if (txtTADate.IsEmpty)
							{
								txtTADate.Text = FCConvert.ToString(DateTime.Today);
							}
							rsRE.Set_Fields("TADate", txtTADate.Text);
							rsRE.Update();
							// MAL@20080812: Update RE Account automatically
							// Tracker Reference: 14035
							if (chkUpdateRE.CheckState == Wisej.Web.CheckState.Checked && rsRE.Get_Fields_Boolean("UseREAccount") == true)
							{
								rsReal.OpenRecordset("SELECT TaxAcquired, TADate FROM Master WHERE RSAccount = " + rsRE.Get_Fields_Int32("REAccount") + " AND RSCard = 1", modExtraModules.strREDatabase);
								if (rsReal.RecordCount() > 0)
								{
									if (rsReal.Get_Fields_Boolean("TaxAcquired") == false)
									{
										// Don't Update if already TA
										rsReal.Edit();
										rsReal.Set_Fields("TaxAcquired", true);
										rsReal.Set_Fields("TADate", txtTADate.Text);
										rsReal.Update();
									}
								}
							}
						}
					}
				}
				return UpdateTaxAcquiredAccounts;
			}
			catch (Exception ex)
			{
				
				UpdateTaxAcquiredAccounts = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating TA Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateTaxAcquiredAccounts;
		}
		// vbPorter upgrade warning: lngBillKey As int	OnWrite(string)
		private int FindDemandIndex(int lngBillKey)
		{
			int FindDemandIndex = 0;
			int lngCT;
			for (lngCT = 0; lngCT <= Information.UBound(modUTLien.Statics.arrDemand, 1); lngCT++)
			{
				if (lngBillKey == modUTLien.Statics.arrDemand[lngCT].BillKey)
				{
					FindDemandIndex = lngCT;
					break;
				}
			}
			return FindDemandIndex;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(mnuFileSelectAll, new System.EventArgs());
		}

		private void vsDemand_DblClick(object sender, System.EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click();
			}
		}

		private void vsDemand_RowColChange(object sender, System.EventArgs e)
		{
			if (vsDemand.Col == lngGridColTaxAcquired)
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, lngGridColTaxAcquired, FCConvert.ToString(0));
			}
		}
		//FC:FINAL:DDU:#i983 - no need of this code
		//private void vsValidate_RowColChange(object sender, System.EventArgs e)
		//{
		//    vsValidate.ComboList = strYearList;
		//    vsValidate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		//}
		// vbPorter upgrade warning: lngAcct As int	OnWriteFCConvert.ToDouble(
		private void CalculateTAJournalEntries_2(int lngAcct)
		{
			CalculateTAJournalEntries(ref lngAcct);
		}

		private void CalculateTAJournalEntries(ref int lngAcct)
		{
			try
			{
				// On Error GoTo ERROR_HANLDER
				// This function will add records to the journal for the whole account
				// arrJournalEntry(Year - DEFAULTSUBTRACTIONVALUE) = the journal amount   'this will hold the principal amount
				clsDRWrapper rsCL = new clsDRWrapper();
				clsDRWrapper rsLN = new clsDRWrapper();
				double dblAmount = 0;
				clsDRWrapper rsMaster = new clsDRWrapper();
				int lngRes;
				rsCL.OpenRecordset("SELECT Bill.ID, BillNumber, WLienRecordNumber, WCombinationLienKey, WPrinOwed, WPrinPaid, " + "SLienRecordNumber, SCombinationLienKey, SPrinOwed,SPrinPaid " + "FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE " + "((WLienRecordNumber = 0 OR (WLienRecordNumber <> 0 AND Bill.ID = WCombinationLienKey)) " + "OR (SLienRecordNumber = 0 OR (SLienRecordNumber <> 0 AND Bill.ID = SCombinationLienKey))) " + "AND AccountNumber = " + FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
				while (!rsCL.EndOfFile())
				{
					//Application.DoEvents();
					// If gboolMultipleTowns Then
					// lngRes = rsCL.Fields("RITranCode")
					// Else
					// lngRes = 0
					// End If
					if (FCConvert.ToInt32(rsCL.Get_Fields_Int32("WLienRecordNumber")) != 0)
					{
						// Lien
						if (rsCL.Get_Fields_Int32("ID") == rsCL.Get_Fields_Int32("WCombinationLienKey"))
						{
							rsLN.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsCL.Get_Fields_Int32("WLienRecordNumber"), modExtraModules.strUTDatabase);
							if (!rsLN.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								dblAmount = rsLN.Get_Fields("Principal") - rsLN.Get_Fields("PrinPaid");
								modMain.Statics.arrTALienPrincipalW[rsCL.Get_Fields_Int32("BillNumber")] += dblAmount;
							}
							else
							{
								// do nothing
							}
						}
					}
					else
					{
						// Non Lien
						dblAmount = rsCL.Get_Fields_Double("WPrinOwed") - rsCL.Get_Fields_Double("WPrinPaid");
						modMain.Statics.arrTAPrincipalW[rsCL.Get_Fields_Int32("BillNumber")] += dblAmount;
					}
					if (FCConvert.ToInt32(rsCL.Get_Fields_Int32("SLienRecordNumber")) != 0)
					{
						// Lien
						if (rsCL.Get_Fields_Int32("ID") == rsCL.Get_Fields_Int32("SCombinationLienKey"))
						{
							rsLN.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsCL.Get_Fields_Int32("SLienRecordNumber"), modExtraModules.strUTDatabase);
							if (!rsLN.EndOfFile())
							{
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								dblAmount = rsLN.Get_Fields("Principal") - rsLN.Get_Fields("PrinPaid");
								modMain.Statics.arrTALienPrincipalS[rsCL.Get_Fields_Int32("BillNumber")] += dblAmount;
							}
							else
							{
								// do nothing
							}
						}
					}
					else
					{
						// Non Lien
						dblAmount = rsCL.Get_Fields_Double("SPrinOwed") - rsCL.Get_Fields_Double("SPrinPaid");
						modMain.Statics.arrTAPrincipalS[rsCL.Get_Fields_Int32("BillNumber")] += dblAmount;
					}
					// If dblAmount <> 0 Then
					// Debug.Print rsCL.Fields("BillNumber") & " - " & dblAmount
					// End If
					rsCL.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ERROR_HANLDER:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Journal Entries", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CreateTAJournalEntry()
		{
			bool CreateTAJournalEntry = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsEntries = new clsDRWrapper();
				clsDRWrapper rsBillInfo = new clsDRWrapper();
				clsDRWrapper Master = new clsDRWrapper();
				clsDRWrapper rsTown = new clsDRWrapper();
				string strTableName;
				int lngJournal = 0;
				string strLAccountNumberW = "";
				string strRAccountNumberW = "";
				string strLTAAccountNumberW = "";
				string strRTAAccountNumberW = "";
				string strLAccountNumberS = "";
				string strRAccountNumberS = "";
				string strLTAAccountNumberS = "";
				string strRTAAccountNumberS = "";
				string strSQL = "";
				double[] dblRFundAmount = new double[99 + 1];
				double[] dblLFundAmount = new double[99 + 1];
				int lngYear = 0;
				int intFundS = 0;
				// kk04032015 trout-927  Have to separate Sewer and Water funds
				int intFundW = 0;
				int intCT;
				bool boolUseYearW = false;
				bool boolUseLYearW = false;
				bool boolTAUseYearW = false;
				bool boolTAUseLYearW = false;
				bool boolUseYearS = false;
				bool boolUseLYearS = false;
				bool boolTAUseYearS = false;
				bool boolTAUseLYearS = false;
				int intResCode;
				string strMulti = "";
				clsBudgetaryPosting tPostInfo;
				// kk01282015 trouts-129  Add automatic posting of journals
				clsPostingJournalInfo tJournalInfo;
				CreateTAJournalEntry = true;
				if (!modGlobalConstants.Statics.gboolBD)
				{
					// if the user does not have BD then exit and return back true
					return CreateTAJournalEntry;
				}
				// Find Receivable Account Number
				Master.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
				if (!Master.EndOfFile())
				{
					// Water
					Master.FindFirstRecord("Code", "WR");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strRAccountNumberW = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "WL");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strLAccountNumberW = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "WT");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strRTAAccountNumberW = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "WA");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strLTAAccountNumberW = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					// Sewer
					Master.FindFirstRecord("Code", "SR");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strRAccountNumberS = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "SL");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strLAccountNumberS = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "ST");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strRTAAccountNumberS = FCConvert.ToString(Master.Get_Fields("Account"));
					}
					Master.FindFirstRecord("Code", "SA");
					if (!Master.NoMatch)
					{
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						strLTAAccountNumberS = FCConvert.ToString(Master.Get_Fields("Account"));
					}
				}
				// Check to make sure the accounts are all there - and that they're not all zeros (trout-927)
				// Water
				if (modUTStatusPayments.Statics.TownService != "S")
				{
					if (Strings.Trim(strRAccountNumberW) == "" || Conversion.Val(strRAccountNumberW) == 0)
					{
						MessageBox.Show("Please enter the Water Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTAJournalEntry = false;
						return CreateTAJournalEntry;
					}
					if (Strings.Trim(strLAccountNumberW) == "" || Conversion.Val(strLAccountNumberW) == 0)
					{
						MessageBox.Show("Please enter the Water Lien Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTAJournalEntry = false;
						return CreateTAJournalEntry;
					}
					if (Strings.Trim(strRTAAccountNumberW) == "" || Conversion.Val(strRTAAccountNumberW) == 0)
					{
						MessageBox.Show("Please enter the Water Tax Acquired Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTAJournalEntry = false;
						return CreateTAJournalEntry;
					}
					if (Strings.Trim(strLTAAccountNumberW) == "" || Conversion.Val(strLTAAccountNumberW) == 0)
					{
						MessageBox.Show("Please enter the Water Tax Acquired Lien Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTAJournalEntry = false;
						return CreateTAJournalEntry;
					}
				}
				// Sewer
				if (modUTStatusPayments.Statics.TownService != "W")
				{
					if (Strings.Trim(strRAccountNumberS) == "" || Conversion.Val(strRAccountNumberS) == 0)
					{
						MessageBox.Show("Please enter the Sewer Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTAJournalEntry = false;
						return CreateTAJournalEntry;
					}
					if (Strings.Trim(strLAccountNumberS) == "" || Conversion.Val(strLAccountNumberS) == 0)
					{
						MessageBox.Show("Please enter the Sewer Lien Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTAJournalEntry = false;
						return CreateTAJournalEntry;
					}
					if (Strings.Trim(strRTAAccountNumberS) == "" || Conversion.Val(strRTAAccountNumberS) == 0)
					{
						MessageBox.Show("Please enter the Sewer Tax Acquired Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTAJournalEntry = false;
						return CreateTAJournalEntry;
					}
					if (Strings.Trim(strLTAAccountNumberS) == "" || Conversion.Val(strLTAAccountNumberS) == 0)
					{
						MessageBox.Show("Please enter the Sewer Tax Acquired Lien Receivable Account in Budgetary.", "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						CreateTAJournalEntry = false;
						return CreateTAJournalEntry;
					}
				}
				strTableName = "";
				// get journal number
				if (modBudgetaryAccounting.LockJournal() == false)
				{
					// add entries to incomplete journals table
					strTableName = "Temp";
				}
				else
				{
					Master.OpenRecordset(("SELECT TOP 1 * FROM JournalMaster ORDER BY JournalNumber DESC"), "TWBD0000.vb1");
					if (Master.EndOfFile() != true && Master.BeginningOfFile() != true)
					{
						Master.MoveLast();
						Master.MoveFirst();
						// TODO Get_Fields: Check the table for the column [JournalNumber] and replace with corresponding Get_Field method
						lngJournal = Master.Get_Fields("JournalNumber") + 1;
					}
					else
					{
						lngJournal = 1;
					}
					Master.AddNew();
					Master.Set_Fields("JournalNumber", lngJournal);
					Master.Set_Fields("Status", "E");
					Master.Set_Fields("StatusChangeTime", DateAndTime.TimeOfDay);
					Master.Set_Fields("StatusChangeDate", DateTime.Today);
					Master.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " UT TA Status");
					Master.Set_Fields("Type", "GJ");
					Master.Set_Fields("Period", DateTime.Today.Month);
					Master.Update();
					Master.Reset();
					modBudgetaryAccounting.UnlockJournal();
				}
				if (modGlobalConstants.Statics.gboolCR)
				{
					// If gboolMultipleTowns Then
					// strMulti = " AND TownKey = 1"
					// End If
					// Water
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 93" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolUseYearW = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolUseYearW = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 96" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolUseLYearW = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolUseLYearW = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 893" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolTAUseYearW = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolTAUseYearW = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 896" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolTAUseLYearW = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolTAUseLYearW = false;
					}
					// Sewer
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 94" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolUseYearS = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolUseYearS = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 95" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolUseLYearS = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolUseLYearS = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 894" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolTAUseYearS = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolTAUseYearS = false;
					}
					rsEntries.OpenRecordset("SELECT * FROM Type WHERE Typecode = 895" + strMulti, modExtraModules.strCRDatabase);
					if (!rsEntries.EndOfFile())
					{
						boolTAUseLYearS = FCConvert.ToBoolean(rsEntries.Get_Fields_Boolean("Year1"));
					}
					else
					{
						boolTAUseLYearS = false;
					}
				}
				else
				{
					boolUseYearW = false;
					boolUseLYearW = false;
					boolTAUseYearW = false;
					boolTAUseLYearW = false;
					boolUseYearS = false;
					boolUseLYearS = false;
					boolTAUseYearS = false;
					boolTAUseLYearS = false;
				}
				rsEntries.OpenRecordset("SELECT * FROM " + strTableName + "JournalEntries WHERE ID = 0", "TWBD0000.vb1");
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Adding Journal Entries", true, Information.UBound(modMain.Statics.arrTAPrincipalW, 1));
				// MAL@20081015: Change to not be a hard-coded number
				// Tracker Reference: 15536
				// intFund = 1
				if (modGlobalConstants.Statics.gboolBD)
				{
					intFundS = GetFundNumber_2("S");
					// kk04032015 trout-927  Have to separate Sewer and Water funds
					intFundW = GetFundNumber_2("W");
				}
				else
				{
					intFundS = 1;
					intFundW = 1;
				}
				// Add Regular Tax Entries and recievable entry
				for (intCT = 0; intCT <= Information.UBound(modMain.Statics.arrTAPrincipalW, 1); intCT++)
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					// Water
					if (modUTStatusPayments.Statics.TownService != "S")
					{
						if (modMain.Statics.arrTAPrincipalW[intCT] != 0)
						{
							// Regular Receivable
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
							if (boolUseYearW)
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundW, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strRAccountNumberW)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							else
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundW, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strRAccountNumberW)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							rsEntries.Set_Fields("Amount", modMain.Statics.arrTAPrincipalW[intCT] * -1);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
							// Tax Acquired Receivable
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
							// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
							if (boolTAUseYearW)
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundW, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strRTAAccountNumberW)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							else
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundW, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strRTAAccountNumberW)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							rsEntries.Set_Fields("Amount", modMain.Statics.arrTAPrincipalW[intCT]);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
						if (modMain.Statics.arrTALienPrincipalW[intCT] != 0)
						{
							// Lien Receivable
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
							// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
							if (boolUseLYearW)
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundW, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strLAccountNumberW)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							else
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundW, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strLAccountNumberW)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							rsEntries.Set_Fields("Amount", modMain.Statics.arrTALienPrincipalW[intCT] * -1);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
							// Tax Acquired Lien Receivable
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
							// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
							if (boolTAUseLYearW)
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundW, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strLTAAccountNumberW)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							else
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundW, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strLTAAccountNumberW)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							rsEntries.Set_Fields("Amount", modMain.Statics.arrTALienPrincipalW[intCT]);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
					}
					// Sewer
					if (modUTStatusPayments.Statics.TownService != "W")
					{
						if (modMain.Statics.arrTAPrincipalS[intCT] != 0)
						{
							// Regular Receivable
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
							if (boolUseYearS)
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundS, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strRAccountNumberS)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							else
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundS, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strRAccountNumberS)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							rsEntries.Set_Fields("Amount", modMain.Statics.arrTAPrincipalS[intCT] * -1);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
							// Tax Acquired Receivable
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
							// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
							if (boolTAUseYearS)
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundS, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strRTAAccountNumberS)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							else
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundS, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strRTAAccountNumberS)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							rsEntries.Set_Fields("Amount", modMain.Statics.arrTAPrincipalS[intCT]);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
						if (modMain.Statics.arrTALienPrincipalS[intCT] != 0)
						{
							// Lien Receivable
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
							// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
							if (boolUseLYearS)
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundS, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strLAccountNumberS)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							else
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundS, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strLAccountNumberS)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							rsEntries.Set_Fields("Amount", modMain.Statics.arrTALienPrincipalS[intCT] * -1);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
							// Tax Acquired Lien Receivable
							rsEntries.AddNew();
							rsEntries.Set_Fields("Type", "G");
							rsEntries.Set_Fields("JournalEntriesDate", DateTime.Today);
							rsEntries.Set_Fields("Description", Strings.Format(DateTime.Today, "MM/dd/yyyy") + " Tax Acquired");
							rsEntries.Set_Fields("JournalNumber", lngJournal);
							lngYear = intCT + modExtraModules.DEFAULTSUBTRACTIONVALUE;
							// intFund = 1  'How do I get the fund?     'GetFundFromAccount(strRAccountNumber)
							if (boolTAUseLYearS)
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundS, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strLTAAccountNumberS)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(((lngYear / 10) % 100), FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							else
							{
								rsEntries.Set_Fields("Account", "G " + modMain.PadToString_6(ref intFundS, FCConvert.ToInt16(Conversion.Val(Strings.Left(modAccountTitle.Statics.Ledger, 2)))) + "-" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(strLTAAccountNumberS)), FCConvert.ToInt16(Conversion.Val(Strings.Mid(modAccountTitle.Statics.Ledger, 3, 2)))) + "-" + modMain.PadToString_8(0, FCConvert.ToInt16(Conversion.Val(Strings.Right(modAccountTitle.Statics.Ledger, 2)))));
							}
							rsEntries.Set_Fields("Amount", modMain.Statics.arrTALienPrincipalS[intCT]);
							rsEntries.Set_Fields("WarrantNumber", 0);
							rsEntries.Set_Fields("Period", DateTime.Today.Month);
							rsEntries.Set_Fields("RCB", "R");
							rsEntries.Set_Fields("Status", "E");
							rsEntries.Update();
						}
					}
				}
				frmWait.InstancePtr.Unload();
				// kk01282015 trouts-129  Allow Automatic posting of journals
				if (lngJournal == 0)
				{
					MessageBox.Show("Unable to complete the journal.  You must go into the Budgetary System > Data Entry > Build Incomplete Journals to finish creating the journal.", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					if (modSecurityValidation.ValidPermissions(null, modMain.UTSECAUTOPOSTJOURNAL, false) && modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptUTLiens))
					{
						if (MessageBox.Show("The entries have been saved into journal " + Strings.Format(lngJournal, "0000") + ".  Would you like to post the journal?", "Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							tJournalInfo = new clsPostingJournalInfo();
							tJournalInfo.JournalNumber = lngJournal;
							tJournalInfo.JournalType = "GJ";
							tJournalInfo.Period = FCConvert.ToString(DateTime.Today.Month);
							tJournalInfo.CheckDate = "";
							tPostInfo = new clsBudgetaryPosting();
							tPostInfo.AddJournal(tJournalInfo);
							tPostInfo.AllowPreview = true;
							tPostInfo.PageBreakBetweenJournalsOnReport = true;
							tPostInfo.WaitForReportToEnd = true;
							tPostInfo.PostJournals();
							tJournalInfo = null;
							tPostInfo = null;
						}
					}
					else
					{
						MessageBox.Show("The Budgetary journal entries were created in journal #" + FCConvert.ToString(lngJournal) + ".", "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				return CreateTAJournalEntry;
			}
			catch (Exception ex)
			{
				
				CreateTAJournalEntry = false;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Journal", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateTAJournalEntry;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetFundNumber_2(string strWS)
		{
			return GetFundNumber(ref strWS);
		}

		private short GetFundNumber(ref string strWS)
		{
			short GetFundNumber = 0;
			// Tracker Reference: 15536
			clsDRWrapper rsUtil = new clsDRWrapper();
			clsDRWrapper rsDept = new clsDRWrapper();
			int lngDept;
			int intResult = 0;
			rsUtil.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
			if (rsUtil.RecordCount() > 0)
			{
				// kk04032015 trout-927  Change this to use GetFundFromAccount function, since we can't count on these accounts being R Accounts
				// x Select Case TownService
				if (strWS == "S")
				{
					if (FCConvert.ToString(rsUtil.Get_Fields_String("SewerPrincipalAccount")) != "" && modValidateAccount.AccountValidate(rsUtil.Get_Fields_String("SewerPrincipalAccount")))
					{
						if (Strings.Left(FCConvert.ToString(rsUtil.Get_Fields_String("SewerPrincipalAccount")), 1) != "M")
						{
							// x lngDept = Mid(rsUtil.Fields("SewerPrincipalAccount"), 3, InStr(1, rsUtil.Fields("SewerPrincipalAccount"), "-", vbTextCompare) - 3)
							intResult = modBudgetaryAccounting.GetFundFromAccount_2(rsUtil.Get_Fields_String("SewerPrincipalAccount"));
						}
					}
				}
				else if (strWS == "W")
				{
					if (FCConvert.ToString(rsUtil.Get_Fields_String("WaterPrincipalAccount")) != "" && modValidateAccount.AccountValidate(rsUtil.Get_Fields_String("WaterPrincipalAccount")))
					{
						if (Strings.Left(FCConvert.ToString(rsUtil.Get_Fields_String("WaterPrincipalAccount")), 1) != "M")
						{
							// x lngDept = Mid(rsUtil.Fields("WaterPrincipalAccount"), 3, InStr(1, rsUtil.Fields("WaterPrincipalAccount"), "-", vbTextCompare) - 3)
							intResult = modBudgetaryAccounting.GetFundFromAccount_2(rsUtil.Get_Fields_String("WaterPrincipalAccount"));
						}
					}
					// x Case "B"
					// x     If rsUtil.Fields("WaterPrincipalAccount") <> "" And AccountValidate(rsUtil.Fields("WaterPrincipalAccount")) Then
					// x         If Left(rsUtil.Fields("WaterPrincipalAccount"), 1) <> "M" Then
					// x             lngDept = Mid(rsUtil.Fields("WaterPrincipalAccount"), 3, InStr(1, rsUtil.Fields("WaterPrincipalAccount"), "-", vbTextCompare) - 3)
					// x         End If
					// x     End If
				}
				// x With rsDept
				// x     .OpenRecordset "SELECT * FROM DeptDivTitles WHERE Department = '" & lngDept & "'", strBDDatabase
				// x
				// x     If .RecordCount > 0 Then
				// x         intResult = .Fields("Fund")
				// x     Else
				// x         intResult = 1
				// x     End If
				// x End With
			}
			GetFundNumber = FCConvert.ToInt16(intResult);
			return GetFundNumber;
		}
	}
}
