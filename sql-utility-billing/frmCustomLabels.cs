﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Diagnostics;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCustomLabels.
	/// </summary>
	public partial class frmCustomLabels : BaseForm
	{
		public frmCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkFormPrintOptions = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkFormPrintOptions.AddControlArrayElement(chkFormPrintOptions_0, 0);
			this.chkFormPrintOptions.AddControlArrayElement(chkFormPrintOptions_1, 1);
			this.chkFormPrintOptions.AddControlArrayElement(chkFormPrintOptions_2, 2);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomLabels InstancePtr
		{
			get
			{
				return (frmCustomLabels)Sys.GetInstance(typeof(frmCustomLabels));
			}
		}

		protected frmCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/05/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/08/2006              *
		// ********************************************************
		//
		// THIS FORM IS FOR THE CREATION OF CUSTOM REPORTS. IT ALLOWS THE USER
		// TO SELECT THE FIELDS TO BE DISPLAYED, THE ORDER TO DISPLAY THEM IN
		// THE SORT ORDER TO SHOW THEM IN AND THE ABILITY TO FILTER THE RECORDS
		// BY ANY FIELD EVEN IF IT IS NOT BEING SHOWN.
		//
		// THIS FORM ***MUST*** WORK WITH modCustomReport.mod and rptCustomLabels.rpt
		//
		// THE DEVELOPER NEEDS ONLY ADD A CASE STATEMENT IN THE ROUNTING
		// SetFormFieldCaptions IN modCustomReport.mod
		// NO CHANGES TO THIS FORM OR THE REPORT NEEDS TO BE DONE.
		//
		// A CALL TO THIS FORM WOULD BE:
		// frmCustomLabels.Show , MDIParent
		// Call SetFormFieldCaptions(frmCustomLabels, "Births")
		//
		//
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolSaveReport;
		string strPassedInSQL;
		int intRateType;
		bool boolLoaded;
		string strPassSortOrder = "";
		bool boolWater;
		string strBookList;
		clsPrintLabel labLabelTypes = new clsPrintLabel();
		// vbPorter upgrade warning: intRT As short	OnWriteFCConvert.ToInt32(
		public void Init(string strSQL, int intRT, bool boolPassWater = false, string strPassBookList = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will pass the correct SQL string into the report and show itself
				strPassedInSQL = strSQL;
				strBookList = strPassBookList;
				intRateType = intRT;
				boolWater = boolPassWater;
				//Application.DoEvents();
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Custom Label Init Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmbFormType_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbFormType.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
		}

		private void cmbFormType_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						// this will force the combobox to open with a spacebar
						if (modAPIsConst.SendMessageByNum(cmbFormType.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbFormType.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intIndex;
			intIndex = labLabelTypes.Get_IndexFromID(cmbLabelType.ItemData(cmbLabelType.SelectedIndex));
			lblDescription.Text = labLabelTypes.Get_Description(intIndex);
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			string strSQL = "";
			string strPrinterName = "";
			int NumFonts = 0;
			bool boolUseFont = false;
			string strFont = "";
			int intCPI = 0;
			int X;
			string strDefPrinterName;
			bool boolCondensed;
			string strSort = "";
			boolCondensed = FCConvert.CBool(chkCondensed.CheckState == Wisej.Web.CheckState.Checked);
			strDefPrinterName = FCGlobal.Printer.DeviceName;
			// make them choose the printer so we can set the correct printer font
			// kk11212014 troge-241 Fix issue with printer and default printer flip-flopping
			// Err.Clear
			// On Error Resume Next
			// MDIParent.CommonDialog1.CancelError = True          'this will make the dialog box return an error if the user presses the cancel button
			// MDIParent.CommonDialog1.ShowPrinter
			// If Err.Number = 0 Then
			//if (modPrinterDialogBox.ShowPrinterDialog(this.Handle.ToInt32()))
			{
				// make sure that the grid does not have any fields that are being edited
				vsWhere.Select(0, 0);
				// BUILD THE SQL FOR THE NEW CUSTOM REPORT
				strSQL = BuildSQL(ref strSort);
				strPrinterName = FCGlobal.Printer.DeviceName;
				
				// SHOW THE REPORT
				if (!boolUseFont)
					strFont = "";
				if (intRateType == 0)
				{
					//FC:FINAL:DDU:#1134 - fixed intRateType value after frmRateRecChoice is unloadded
					rptCustomMHLabels.InstancePtr.intRateType = frmRateRecChoice.InstancePtr.intRateType;
					rptCustomMHLabels.InstancePtr.Init(ref strSQL, ref modUTStatusList.Statics.strReportType, cmbLabelType.ItemData(cmbLabelType.SelectedIndex), ref strPrinterName, ref strFont);
				}
				else if (intRateType >= 10 && intRateType <= 12)
				{
					//FC:FINAL:DDU:#1134 - fixed intRateType value after frmRateRecChoice is unloadded
					rptCustomLabels.InstancePtr.intRateType = frmRateRecChoice.InstancePtr.intRateType;
					rptCustomLabels.InstancePtr.Init(ref strSQL, ref modUTStatusList.Statics.strReportType, cmbLabelType.ItemData(cmbLabelType.SelectedIndex), ref strPrinterName, ref strFont, ref strPassSortOrder, FCConvert.ToInt16(intRateType + 10), ref boolWater, strDefPrinterName, boolCondensed);
				}
				else if (intRateType >= 20 && intRateType <= 22)
				{
					rptCustomForms.InstancePtr.Init(ref strSQL, modUTStatusList.Statics.strReportType, cmbFormType.SelectedIndex, ref strPrinterName, ref strFont, frmRateRecChoice.InstancePtr.intRateType, boolWater, true, strDefPrinterName, strSort);
				}
				this.Unload();
				// kk11242014  On Error GoTo 0         'this will turn the resume next code off
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private string BuildSQL(ref string strPassSort)
		{
			string BuildSQL = "";
			// BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
			string strSQL = "";
			string strWhere = "";
			string strOrderBy = "";
			string strWhereClause = "";
			BuildSQL = "";
			if (strPassedInSQL != "")
			{
				strSQL = strPassedInSQL;
				strOrderBy = BuildSortParameter();
				strPassSortOrder = strOrderBy;
				if (intRateType >= 10 && intRateType <= 12)
				{
					// this will clear the sort criteria to be used later instead of on the label query
					strOrderBy = "";
				}
				if (Strings.Trim(strPassSortOrder) == "")
				{
					strPassSortOrder = " OName, AccountNumber";
				}
				if (intRateType == 12 || intRateType == 22)
				{
					if (boolWater)
					{
						strWhereClause = " WHERE WCombinationLienKey = Bill";
					}
					else
					{
						strWhereClause = " WHERE SCombinationLienKey = Bill";
					}
				}
				else
				{
					strWhereClause = " WHERE Bill = Bill";
				}
				if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "FORMS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LABELS"))
				{
					// this will return the SQL statement for this batch of reports
					// check for a range of Names
					if (vsWhere.TextMatrix(0, 1) != "" || vsWhere.TextMatrix(0, 2) != "")
					{
						if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != "")
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								// both full
								strWhereClause += " AND OwnerName >= '" + vsWhere.TextMatrix(0, 1) + "     ' AND OwnerName <= '" + vsWhere.TextMatrix(0, 2) + "zzzzz'";
							}
							else
							{
								// first full second empty
								strWhereClause += " AND OwnerName >= '" + vsWhere.TextMatrix(0, 1) + "     '";
							}
						}
						else
						{
							if (Strings.Trim(vsWhere.Text) != "")
							{
								// first empty second full
								strWhereClause += " AND OwnerName <= '" + vsWhere.TextMatrix(0, 2) + "'";
							}
							else
							{
								// both empty
								// strWhereClause = ""
							}
						}
					}
					// check for a range of accounts
					if (vsWhere.TextMatrix(1, 1) != "" || vsWhere.TextMatrix(1, 2) != "")
					{
						if (Strings.Trim(vsWhere.TextMatrix(1, 1)) != "")
						{
							if (Strings.Trim(vsWhere.TextMatrix(1, 2)) != "")
							{
								// both full
								strWhereClause += " AND AccountNumber >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " AND AccountNumber <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2)));
							}
							else
							{
								// first full second empty
								strWhereClause += " AND AccountNumber >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1)));
							}
						}
						else
						{
							if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
							{
								// first empty second full
								strWhereClause += " AND AccountNumber <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2)));
							}
							else
							{
								// both empty
								// strWhereClause = Left(strWhereClause, Len(strWhereClause) - 6)  'take the last 6 chars off
							}
						}
					}
				}
				if (Strings.Trim(strBookList) != "")
				{
					strWhereClause += " AND " + strBookList;
				}
				if (Strings.Trim(strOrderBy) != string.Empty)
				{
					strSQL += strWhereClause + " ORDER BY " + strOrderBy;
				}
				else
				{
					strSQL += strWhereClause + " ORDER BY OwnerName, AccountNumber";
				}
			}
			else
			{
				if (Strings.UCase(modUTStatusList.Statics.strReportType) == "MORTGAGEHOLDER")
				{
					strSQL = "SELECT * FROM Mortgageholders ";
				}
				else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "FORMS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LABELS"))
				{
					// this will return the SQL statement for this batch of reports
					Debug.Assert(true);
					// Better never reach here because these are CL bill fields
					Debug.Assert(false);
					if (vsWhere.TextMatrix(0, 1) != "" || vsWhere.TextMatrix(0, 2) != "")
					{
						// range of Names
						if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != "")
						{
							if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
							{
								// both full
								strWhereClause = " AND Name1 BETWEEN '" + vsWhere.TextMatrix(0, 1) + "' AND '" + vsWhere.TextMatrix(0, 2) + "'";
							}
							else
							{
								// first full second empty
								strWhereClause = " AND Name1 >= '" + vsWhere.TextMatrix(0, 1) + "'";
							}
						}
						else
						{
							if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
							{
								// first empty second full
								strWhereClause = " AND Name1 <= '" + vsWhere.TextMatrix(0, 2) + "'";
							}
							else
							{
								// both empty
								strWhereClause = "";
							}
						}
					}
					else if (vsWhere.TextMatrix(1, 1) != "" || vsWhere.TextMatrix(1, 2) != "")
					{
						// range of accounts
						if (Strings.Trim(vsWhere.TextMatrix(1, 1)) != "")
						{
							if (Strings.Trim(vsWhere.TextMatrix(1, 2)) != "")
							{
								// both full
								strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " AND Account <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2)));
							}
							else
							{
								// first full second empty
								strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1)));
							}
						}
						else
						{
							if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
							{
								// first empty second full
								strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2)));
							}
							else
							{
								// both empty
								strWhereClause = "";
							}
						}
					}
				}
				strWhere = BuildWhereParameter();
				strSQL += strWhere;
				// BUILD A SORT CRITERIA TO APPEND TO THE SQL STATEMENT
				strOrderBy = BuildSortParameter();
				if (Strings.Trim(strOrderBy) != string.Empty)
				{
					strSQL += " ORDER BY " + strOrderBy;
				}
			}
			strPassSort = strPassSortOrder;
			BuildSQL = strSQL;
			return BuildSQL;
		}

		private string BuildSortParameter()
		{
			string BuildSortParameter = "";
			// BUILD THE SORT CRITERIA FOR THE SQL STATEMENT
			string strSort;
			int intCounter;
			// CLEAR OUT THE VARIABLES
			strSort = " ";
			// GET THE FIELDS TO SORT BY
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				if (lstSort.Selected(intCounter))
				{
					if (strSort != " ")
						strSort += ", ";
					if (vsWhere.TextMatrix(lstSort.ItemData(intCounter), 3) == "Account")
					{
						// MAL@20080304: Changed to point to the correct table name
						// Tracker Reference: 12533
						// strSort = strSort & "CMFNumbers.ActualAccountNumber"
						// strSort = strSort & "New.ActualAccountNumber"
						// MAL@20080313: Change again to not use table name for CMF forms
						// Tracker Reference: 12766
						if (intRateType == 0)
						{
							strSort += "New.ActualAccountNumber";
						}
						else if (intRateType >= 10 && intRateType <= 12)
						{
							strSort += "New.ActualAccountNumber";
						}
						else if (intRateType >= 20 && intRateType <= 22)
						{
							strSort += "ActualAccountNumber";
						}
					}
					else
					{
						if (intRateType == 0)
						{
							strSort += "New.BName";
						}
						else if (intRateType >= 10 && intRateType <= 12)
						{
							strSort += "New.OwnerName";
						}
						else if (intRateType >= 20 && intRateType <= 22)
						{
							strSort += "OwnerName";
						}
						// strSort = strSort & "New.BName"
						// strSort = strSort & "CMFNumbers.BName" '   vsWhere.TextMatrix(lstSort.ItemData(intCounter), 3)
					}
				}
			}
			BuildSortParameter = strSort;
			return BuildSortParameter;
		}

		private string BuildWhereParameter()
		{
			string BuildWhereParameter = "";
			// BUILD THE WHERE CLAUSE TO ADD TO THE SQL STATEMENT
			string strWhere;
			int intCounter;
			string strTemp = "";
			string strWord;
			// CLEAR THE VARIABLES
			strWhere = " ";
			strWord = " WHERE ";
			if (modUTStatusList.Statics.strReportType == "REALESTATE")
			{
				strWord = " AND ";
				strWhere = " WHERE RSCard = 1 ";
			}
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				switch (intCounter)
				{
					case 0:
						{
							// name
							// If strReportType = "REALESTATE" Then
							// strTemp = "RSName"
							// Else
							strTemp = "Name";
							// End If
							if (Strings.UCase(modUTStatusList.Statics.strReportType) == "MORTGAGEHOLDER")
							{
								if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != string.Empty)
									{
										strWhere += strWord + strTemp + " > '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "     ' AND " + strTemp + " < '" + Strings.Trim(vsWhere.TextMatrix(0, 2)) + "zzzzz' ";
									}
									else
									{
										strWhere += strWord + strTemp + " > '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "     ' AND " + strTemp + " < '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "zzzzz' ";
									}
									strWord = " AND ";
								}
							}
							else
							{
								if (Strings.Trim(vsWhere.TextMatrix(0, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != string.Empty)
									{
										strWhere += strWord + strTemp + " BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(0, 2)) + "' ";
									}
									else
									{
										strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "' OR " + strTemp + " LIKE '" + Strings.Trim(vsWhere.TextMatrix(0, 1)) + "%' ";
									}
									strWord = " AND ";
								}
							}
							break;
						}
					case 1:
						{
							// account
							// If strReportType = "MORTGAGEHOLDER" Then
							// mh
							strTemp = "MortgageHolderID";
							// ElseIf strReportType = "REALESTATE" Then
							// re
							// strTemp = "rsaccount"
							// Else
							// strTemp = "account"
							// pp
							// End If
							if (Conversion.Val(vsWhere.TextMatrix(1, 1)) > 0)
							{
								if (Conversion.Val(vsWhere.TextMatrix(1, 2)) > 0)
								{
									strWhere += strWord + strTemp + " BETWEEN " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " AND " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 2))) + " ";
								}
								else
								{
									strWhere += strWord + strTemp + " = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(1, 1))) + " ";
								}
								strWord = " AND ";
							}
							break;
						}
					case 2:
						{
							// location
							// If strReportType = "MORTGAGEHOLDER" Then
							// mh
							strTemp = "zip";
							if (Strings.Trim(vsWhere.TextMatrix(2, 1)) != string.Empty)
							{
								if (Strings.Trim(vsWhere.TextMatrix(2, 2)) != string.Empty)
								{
									strWhere += strWord + strTemp + " BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(2, 2)) + "' ";
								}
								else
								{
									strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' ";
								}
								strWord = " AND ";
							}
							// ElseIf strReportType = "REALESTATE" Then
							// re
							// strTemp = "rslocstreet"
							// Else
							// pp
							// strTemp = "street"
							// End If
							if (Strings.Trim(vsWhere.TextMatrix(2, 1)) != string.Empty)
							{
								if (Strings.Trim(vsWhere.TextMatrix(2, 2)) != string.Empty)
								{
									strWhere += strWord + strTemp + " BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(2, 2)) + "' ";
								}
								else
								{
									strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(2, 1)) + "' ";
								}
								strWord = " AND ";
							}
							break;
						}
					case 3:
						{
							// zip
							strTemp = "zip";
							if (modUTStatusList.Statics.strReportType == "PERSONALPROPERTY")
							{
								if (Strings.Trim(vsWhere.TextMatrix(3, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(3, 2)) != string.Empty)
									{
										strWhere += strWord + strTemp + " BETWEEN " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 1))) + " AND " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 2))) + " ";
									}
									else
									{
										strWhere += strWord + strTemp + " = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(3, 1))) + " ";
									}
									strWord = " AND ";
								}
							}
							else
							{
								if (modUTStatusList.Statics.strReportType == "REALESTATE")
									strTemp = "rszip";
								if (Strings.Trim(vsWhere.TextMatrix(3, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(3, 2)) != string.Empty)
									{
										strWhere += strWord + strTemp + " BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(3, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(3, 2)) + "' ";
									}
									else
									{
										strWhere += strWord + strTemp + " = '" + Strings.Trim(vsWhere.TextMatrix(3, 1)) + "' ";
									}
									strWord = " AND ";
								}
							}
							break;
						}
					case 4:
						{
							// maplot or businesscode
							if (modUTStatusList.Statics.strReportType == "PERSONALPROPERTY")
							{
								if (Conversion.Val(vsWhere.TextMatrix(4, 1)) > 0)
								{
									if (Conversion.Val(vsWhere.TextMatrix(4, 2)) > 0)
									{
										strWhere += strWord + " businesscode BETWEEN " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 1))) + " AND " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 2))) + " ";
									}
									else
									{
										strWhere += strWord + " businesscode = " + FCConvert.ToString(Conversion.Val(vsWhere.TextMatrix(4, 1))) + " ";
									}
									strWord = " AND ";
								}
							}
							else
							{
								if (Strings.Trim(vsWhere.TextMatrix(4, 1)) != string.Empty)
								{
									if (Strings.Trim(vsWhere.TextMatrix(4, 2)) != string.Empty)
									{
										strWhere += strWord + " rsmaplot BETWEEN '" + Strings.Trim(vsWhere.TextMatrix(4, 1)) + "' AND '" + Strings.Trim(vsWhere.TextMatrix(4, 2)) + "' ";
									}
									else
									{
										strWhere += strWord + " rsmaplot = '" + Strings.Trim(vsWhere.TextMatrix(4, 1)) + "' ";
									}
									strWord = " AND ";
								}
							}
							break;
						}
				}
				//end switch
			}
			// intCounter
			BuildWhereParameter = strWhere;
			return BuildWhereParameter;
		}

		private void frmCustomLabels_Activated(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (boolLoaded)
				{
				}
				else
				{
					// load the form
					boolLoaded = true;
					if (intRateType >= 20)
					{
						lblFormType.Visible = true;
						cmbFormType.Visible = true;
						fraType.Visible = false;
					}
					else
					{
						lblFormType.Visible = false;
						cmbFormType.Visible = false;
						fraType.Visible = true;
					}
					LoadNotesText();
					//Application.DoEvents();
					if (cmbLabelType.Visible && cmbLabelType.Enabled)
					{
						cmbLabelType.Focus();
					}
					else if (cmbFormType.Visible && cmbFormType.Enabled)
					{
						cmbFormType.Focus();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Activate Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmCustomLabels_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
			// Case vbKeyF2
			// KeyCode = 0
			// Call mnuAddRow_Click
			// Case vbKeyF3
			// KeyCode = 0
			// Call mnuAddColumn_Click
			// Case vbKeyF4
			// KeyCode = 0
			// Call mnuDeleteRow_Click
			// Case vbKeyF5
			// KeyCode = 0
			// Call mnuDeleteColumn_Click
			// Case vbKeyF10
			// KeyCode = 0
			// mnuPrint_Click
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmCustomLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomLabels properties;
			//frmCustomLabels.ScaleWidth	= 9045;
			//frmCustomLabels.ScaleHeight	= 7395;
			//frmCustomLabels.LinkTopic	= "Form1";
			//frmCustomLabels.LockControls	= true;
			//End Unmaped Properties
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				intError = 1;
				switch (intRateType)
				{
					case 0:
						{
							intError = 2;
							SetFormFieldCaptions(frmCustomLabels.InstancePtr, "MORTGAGEHOLDER");
							this.Text = "Mortgage Holder Labels";
							FillLabelTypeCombo();
							break;
						}
					case 10:
					case 11:
					case 12:
						{
							intError = 3;
							SetFormFieldCaptions(frmCustomLabels.InstancePtr, "LABELS");
							this.Text = "Lien Mail Labels";
							FillLabelTypeCombo();
							cmbFormType.Clear();
							break;
						}
					case 20:
					case 21:
					case 22:
						{
							intError = 4;
							// this is Certified Mail Forms
							SetFormFieldCaptions(frmCustomLabels.InstancePtr, "FORMS");
							this.Text = "Lien Certified Mail Forms";
							FillFormTypeCombo();
							cmbLabelType.Clear();
							SetCopiesChkBoxes();
							break;
						}
				}
				//end switch
				intError = 5;
				FillLayoutGrid();
				// GET THE SIZE AND ALIGNEMENT OF THE CUSTOM REPORT FORM INSIDE
				// OF THE MDI PARENT FORM
				intError = 6;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				intError = 7;
				modGlobalFunctions.SetTRIOColors(this);
				intError = 8;
				if (intRateType == 0)
				{
					intError = 9;
					if (cmbLabelType.Items.Count != 0)
					{
						cmbLabelType.SelectedIndex = 0;
					}
					intError = 10;
					fraSort.Enabled = true;
					fraFields.Enabled = true;
					//fraNotes.Enabled = true;
					fraWhere.Enabled = true;
					//fraMessage.Visible = false;
					vsLayout.Visible = true;
				}
				else
				{
					intError = 12;
					if (intRateType < 20)
					{
						intError = 13;
						if (cmbLabelType.Items.Count != 0)
						{
							cmbLabelType.SelectedIndex = 0;
						}
					}
					else
					{
						intError = 14;
						if (cmbLabelType.Items.Count != 0)
						{
							cmbLabelType.SelectedIndex = 0;
						}
					}
					intError = 15;
					//fraMessage.Top = vsLayout.Top + vsLayout.Height;
					intError = 16;
					//if ((Frame1.Height - fraMessage.Top) - 100 > 0)
					//{
					//    fraMessage.Height = (Frame1.Height - fraMessage.Top) - 100;
					//}
					//else
					//{
					//    // don't change it, cuz it is messed up or minimized
					//}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Load Error - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			frmRateRecChoice.InstancePtr.Unload();
			rptLienNoticeSummary.InstancePtr.Unload();
			boolLoaded = false;
		}

		private void frmCustomLabels_Resize(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// resize the grids
				// set the height of the Grid
				//vsLayout.Height = (vsLayout.Rows * vsLayout.RowHeight(0)) + 70;
				// turn off the scroll bars
				vsLayout.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				// set the width of the columns
				vsLayout.ColWidth(0, vsLayout.Width);
				vsLayout.ExtendLastCol = true;
				// set the height of the Grid
				//if (vsWhere.Rows > 0) vsWhere.Height = (vsWhere.Rows * vsWhere.RowHeight(0)) + 70;
				// turn off the scroll bars
				vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				// set the width of the columns
				vsWhere.ColWidth(0, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
				vsWhere.ColWidth(1, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
				vsWhere.ColWidth(2, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
				vsWhere.ExtendLastCol = true;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Resize Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void fraFields_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
			{
				lstFields.SetSelected(intCounter, true);
			}
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (lstFields.SelectedIndex < 0)
				return;
			switch (lstFields.SelectedIndex)
			{
				case 0:
					{
						// mortgage holders
						modUTStatusList.Statics.strReportType = "MORTGAGEHOLDER";
						LoadWhereGrid(this);
						LoadSortList(this);
						// Case 1
						// Real Estate
						// strReportType = "REALESTATE"
						// Call LoadWhereGrid(Me)
						// Call LoadSortList(Me)
						// 
						// Case 2
						// Personal Property
						// strReportType = "PERSONALPROPERTY"
						// Call LoadWhereGrid(Me)
						// Call LoadSortList(Me)
						break;
					}
			}
			//end switch
		}

		private void FillLayoutGrid()
		{
			int GridWidth;
			vsLayout.Cols = 1;
			vsLayout.Rows = 4;
			vsLayout.ColWidth(0, 4 * 1440);
			// four inches
			vsLayout.TextMatrix(0, 0, "Name");
			vsLayout.TextMatrix(1, 0, "Address 1");
			vsLayout.TextMatrix(2, 0, "Address 2");
			vsLayout.TextMatrix(3, 0, "City, State 00000");
		}

		//private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// intStart = lstFields.ListIndex
		//}

		//private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
		//	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	// strtemp = lstFields.List(lstFields.ListIndex)
		//	// intID = lstFields.ItemData(lstFields.ListIndex)
		//	// 
		//	// CHANGE THE NEW ITEM
		//	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
		//	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
		//	// 
		//	// SAVE THE OLD ITEM
		//	// lstFields.List(intStart) = strtemp
		//	// lstFields.ItemData(intStart) = intID
		//	// 
		//	// SET BOTH ITEMS TO BE SELECTED
		//	// lstFields.Selected(lstFields.ListIndex) = True
		//	// lstFields.Selected(intStart) = True
		//	// End If
		//}

		//private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
		//	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
		//	// WHERE TO SWAP THE TWO ITEMS.
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	intStart = lstSort.SelectedIndex;
		//}

		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		//{
		//	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
		//	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
		//	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
		//	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
		//	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
		//	// ITEMS THAT ARE TO BE SWAPED
		//	// 
		//	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
		//	// WILL BE DISPLAYED ON THE REPORT ITSELF
		//	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
		//	//FC:FINAL:DDU - #i853 - No need for drag and drop here
		//	//if (intStart != lstSort.SelectedIndex)
		//	//{
		//	//    // SAVE THE CAPTION AND ID FOR THE NEW ITEM
		//	//    strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
		//	//    intID = lstSort.ItemData(lstSort.SelectedIndex);
		//	//    // CHANGE THE NEW ITEM
		//	//    lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
		//	//    lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
		//	//    // SAVE THE OLD ITEM
		//	//    lstSort.Items[intStart].Text = strTemp;
		//	//    lstSort.ItemData(intStart, intID);
		//	//    // SET BOTH ITEMS TO BE SELECTED
		//	//    lstSort.SetSelected(lstSort.ListIndex, true);
		//	//    lstSort.SetSelected(intStart, true);
		//	//}
		//}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void vsLayout_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (FCConvert.ToString(vsLayout.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, vsLayout.Row, vsLayout.Col)) == string.Empty)
			{
				vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col, -1);
			}
		}

		private void vsLayout_MouseDownEvent(object sender, DataGridViewCellMouseEventArgs e)
		{
			// If vsLayout.MouseRow = 0 Then vsLayout.Row = 1
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEE TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			vsWhere.TextMatrix(vsWhere.Row, 3, vsWhere.ComboData());
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF TEXT...ALLOW ANYTHING
			// IF DATE...ADD A MASK TO FORCE THE USE TO ENTER CORRECT DATA
			// IF COMBO...ADD THE LIST OF OPTIONS
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			switch (FCConvert.ToInt32(modUTStatusList.Statics.strWhereType[vsWhere.Row]))
			{
				case modUTStatusList.GRIDTEXT:
					{
						break;
					}
				case modUTStatusList.GRIDDATE:
					{
						vsWhere.EditMask = "##/##/####";
						break;
					}
				case modUTStatusList.GRIDCOMBOIDTEXT:
				case modUTStatusList.GRIDCOMBOIDNUM:
				case modUTStatusList.GRIDCOMBOTEXT:
					{
						vsWhere.ComboList = modUTStatusList.Statics.strComboList[vsWhere.Row, 0];
						break;
					}
			}
			//end switch
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			if (vsWhere.Row >= 0)
			{
				if (modUTStatusList.Statics.strComboList[vsWhere.Row, 0] != string.Empty)
				{
					vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsWhere.ComboList = modUTStatusList.Statics.strComboList[vsWhere.Row, 0];
				}
				if (vsWhere.Col == 2)
				{
					if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
					{
						vsWhere.Col = 1;
					}
				}
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// THIS WILL VALIDATE THE DATA THAT THE USER PUTS INTO THE WHERE
			// GIRD THAT WILL FILTER OUT RECORDS
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsWhere.GetFlexRowIndex(e.RowIndex);
			int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
			switch (FCConvert.ToInt32(modUTStatusList.Statics.strWhereType[row]))
			{
				case modUTStatusList.GRIDTEXT:
					{
						// ANYTHING GOES IF IT IS A TEXT FIELD
						break;
					}
				case modUTStatusList.GRIDDATE:
					{
						// MAKE SURE THAT IT IS A VALID DATE
						if (Strings.Trim(vsWhere.EditText) == "/  /")
						{
							vsWhere.EditMask = string.Empty;
							vsWhere.EditText = string.Empty;
							vsWhere.TextMatrix(row, col, string.Empty);
							vsWhere.Refresh();
							return;
						}
						if (Strings.Trim(vsWhere.EditText).Length == 0)
						{
						}
						else if (Strings.Trim(vsWhere.EditText).Length != 10)
						{
							MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							e.Cancel = true;
							return;
						}
						if (!modUTStatusList.IsValidDate(vsWhere.EditText))
						{
							e.Cancel = true;
							return;
						}
						break;
					}
				case modUTStatusList.GRIDCOMBOIDTEXT:
					{
						// ASSIGN THE LIST TO THE COMBO IN THE GRID
						vsWhere.ComboList = modUTStatusList.Statics.strComboList[row, 0];
						break;
					}
			}
			//end switch
		}

		private void SetColumnCaptions(dynamic FormName)
		{
			// THIS ROUTINE GETS THE CAPTIONS THAT ARE TO BE DISPLAYED ON
			// THE REPORT ITSELF
			// THIS ARRAY (strFieldCaptions) HOLDS ONLY THE FIELD NAMES THAT ARE
			// TO BE DISPLAYED WHERE THE ARRAY (strCaptions) HOLDS ALL POSSIBLE
			// FIELD NAMES
			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.Items.Count - 1; intCounter++)
			{
				if (FormName.lstFields.GetSelected(intCounter))
				{
					modUTStatusList.Statics.strFieldCaptions[intCount] = modUTStatusList.Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}

		private void GetNumberOfFields(string strSQL)
		{
			// THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
			// ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
			// TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
			// ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			object strTemp2;
			int intCount;
			int intCounter;
			if (Strings.Trim(strSQL) == string.Empty)
			{
				modUTStatusList.Statics.intNumberOfSQLFields = -1;
				return;
			}
			strTemp = Strings.Split(strSQL, "from", -1, CompareConstants.vbBinaryCompare);
			strSQL = Strings.Mid(strTemp[0], 7, FCConvert.ToString(strTemp[0]).Length - 6);
			strTemp = Strings.Split(strSQL, ",", -1, CompareConstants.vbBinaryCompare);
			modUTStatusList.Statics.intNumberOfSQLFields = Information.UBound(strTemp, 1);
			for (intCounter = 0; intCounter <= modUTStatusList.Statics.intNumberOfSQLFields; intCounter++)
			{
				if (Strings.Trim(strTemp[intCounter]) != string.Empty)
				{
					// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
					modUTStatusList.Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
					// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
					// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
					for (intCount = 0; intCount <= modUTStatusList.Statics.intNumberOfSQLFields; intCount++)
					{
						if (modUTStatusList.Statics.strFields[intCount] == Strings.Trim(strTemp[intCounter]))
						{
							modUTStatusList.Statics.strFieldCaptions[intCounter] = modUTStatusList.Statics.strCaptions[intCount];
							break;
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		private object CheckForAS(string strFieldName, short intSegment = 2)
		{
			object CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (Strings.UCase(strTemp[1]) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					if (intSegment == 1)
					{
						CheckForAS = strTemp[0];
					}
					else
					{
						CheckForAS = strTemp[2];
					}
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					CheckForAS = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				CheckForAS = Strings.Trim(strTemp[0]);
			}
			return CheckForAS;
		}

		private void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 20; intCount++)
			{
				modUTStatusList.Statics.strComboList[intCount, 0] = string.Empty;
				modUTStatusList.Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		private void SetFormFieldCaptions(Form FormName, string ReportType)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
				// 
				// ****************************************************************
				// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
				// TO ADD A NEW REPORT TYPE.
				// ****************************************************************
				modUTStatusList.Statics.strReportType = Strings.UCase(ReportType);
				// CLEAR THE COMBO LIST ARRAY
				ClearComboListArray();
				if (Strings.UCase(ReportType) == "MORTGAGEHOLDER")
				{
					SetMortgageHolderParameters(FormName);
				}
				else if (Strings.UCase(ReportType) == "LABELS")
				{
					LoadWhereGrid(FormName);
					LoadSortList(FormName);
					fraFields.Enabled = true;
					fraFields.Visible = true;
				}
				else if (Strings.UCase(ReportType) == "FORMS")
				{
					LoadWhereGrid(FormName);
					LoadSortList(FormName);
					fraFields.Enabled = true;
					fraFields.Visible = true;
				}
				// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
				// Call LoadSortList(FormName)
				// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
				// Call LoadWhereGrid(FormName)
				// LOAD THE SAVED REPORT COMBO ON THE FORM
				// CallByName FormName, "LoadCombo", VbMethod
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Set Form Field Captions Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private void SetMortgageHolderParameters(dynamic FormName)
		{
			modUTStatusList.Statics.strCustomTitle = "Mortgage Holder Labels";
			FormName.lstFields.AddItem("Mortgage Holder");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 0);
			// FormName.lstFields.AddItem ("Real Estate")
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 1
			// FormName.lstFields.AddItem ("Personal Property")
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 2
			LoadWhereGrid(FormName);
			LoadSortList(FormName);
		}

		private void SetHeaderGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
			switch (Strings.UCase(modUTStatusList.Statics.strReportType))
			{
			// Case "DOGS"
			// 
			// 
			// Case "BIRTHS"
				default:
					{
						// GridName.BackColor = vbBlue
						// GridName.ForeColor = vbWhite
						break;
					}
			}
			//end switch
			// GridName.Select 0, 0, 0, GridName.Cols - 1
			// GridName.CellFontBold = True
		}

		private void SetDataGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
			if (Strings.UCase(modUTStatusList.Statics.strReportType) == "DOGS")
			{
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "BIRTHS")
			{
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		private void LoadSortList(dynamic FormName)
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
			FormName.lstSort.Clear();
			for (intCounter = 0; intCounter <= FormName.vsWhere.Rows - 1; intCounter++)
			{
				FormName.lstSort.AddItem(FormName.vsWhere.TextMatrix(intCounter, 0));
				FormName.lstSort.ItemData(FormName.lstSort.NewIndex, intCounter);
			}
			// set the default sort list
			if (intRateType == 0)
			{
				// do nothing
			}
			else if ((intRateType >= 10 && intRateType <= 12) || (intRateType >= 20 && intRateType <= 22))
			{
				if (lstSort.Items.Count > 1)
				{
					lstSort.SetSelected(0, true);
					lstSort.SetSelected(1, true);
				}
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomLabels)
		private void LoadWhereGrid(dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int intCounter;
			int GridWidth;
			(FormName as frmCustomLabels).vsWhere.Rows = 0;
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			GridWidth = FormName.vsWhere.WidthOriginal;
			(FormName as frmCustomLabels).vsWhere.Cols = 4;
			(FormName as frmCustomLabels).vsWhere.ColWidth(0, FCConvert.ToInt32((0.36 * GridWidth)));
			(FormName as frmCustomLabels).vsWhere.ColWidth(1, FCConvert.ToInt32((0.31 * GridWidth)));
			(FormName as frmCustomLabels).vsWhere.ColWidth(2, FCConvert.ToInt32((0.31 * GridWidth)));
			(FormName as frmCustomLabels).vsWhere.ColWidth(3, 0);
			// hidden for the field name
			if (modUTStatusList.Statics.strReportType == "MORTGAGEHOLDER")
			{
				FormName.vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "Name");
				FormName.vsWhere.AddItem("Holder Number" + "\t" + "\t" + "\t" + "MortgageHolderID");
				FormName.vsWhere.AddItem("Zip" + "\t" + "\t" + "\t" + "Zip");
			}
			else if ((modUTStatusList.Statics.strReportType == "LABELS") || (modUTStatusList.Statics.strReportType == "FORMS"))
			{
				FormName.vsWhere.AddItem("Name" + "\t" + "\t" + "\t" + "Name1");
				FormName.vsWhere.AddItem("Account" + "\t" + "\t" + "\t" + "Account");
				FormName.vsWhere.Visible = true;
			}
			// set the height of the Grid
			//vsWhere.Height = (vsWhere.Rows * vsWhere.RowHeight(0)) + 70;
			// turn off the scroll bars
			vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			// set the width of the columns
			vsWhere.ColWidth(0, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
			vsWhere.ColWidth(1, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
			vsWhere.ColWidth(2, FCConvert.ToInt32(vsWhere.WidthOriginal / 3.0));
			vsWhere.ExtendLastCol = true;
		}

		private void LoadGridCellAsCombo(ref Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				modUTStatusList.Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, "TWRE0000.vb1");
				while (!rsCombo.EndOfFile())
				{
					modUTStatusList.Statics.strComboList[intRowNumber, 0] += "|";
					modUTStatusList.Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
					modUTStatusList.Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
					rsCombo.MoveNext();
				}
                rsCombo.DisposeOf();
			}
		}

		private void FillLabelTypeCombo()
		{
			int counter;
			// hide labels that are not supported by BD because of the 5 lines I need to print for an address
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE5066)
				{
					labLabelTypes.Set_Visible(counter, false);
					break;
				}
			}
			// fill combo box with all available types of labels
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_Visible(counter))
				{
					cmbLabelType.AddItem(labLabelTypes.Get_Caption(counter));
					cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabelTypes.Get_ID(counter));
				}
				else
				{
					if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256)
					{
						cmbLabelType.AddItem(labLabelTypes.Get_Caption(counter));
						cmbLabelType.ItemData(cmbLabelType.NewIndex, labLabelTypes.Get_ID(counter));
					}
				}
			}
		}

		private void FillFormTypeCombo()
		{
			cmbFormType.AddItem("Hygrade Certified Mailer (Laser)");
			cmbFormType.ItemData(cmbFormType.NewIndex, 0);
			cmbFormType.SelectedIndex = 0;
		}

		private void LoadNotesText()
		{
			// this will put the text in the note textbox
			string strText = "";
			if (Strings.UCase(modUTStatusList.Statics.strReportType) == "MORTGAGEHOLDER")
			{
				strTemp = "1. Create a new report by from the drop down box on the right of the screen." + "\r\n" + "\r\n";
				strTemp += "2. Choose the fields that you would like on your report from the 'Fields to display on Report'." + "\r\n" + "\r\n";
				strTemp += "3. The 'Fields to Sort By' section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
				strTemp += "4. Set any criteria needed in the 'Select Search Criteria' list." + "\r\n" + "\r\n";
				strTemp += "Other Notes:" + "\r\n" + "    Some fields will be printed automatically." + "\r\n" + "\r\n";
				strTemp += "\r\n" + "    If you choose a default report, only the account number and the tax year criteria will effect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LABELS")
			{
				strTemp = "1. Select the type of ." + "\r\n" + "\r\n";
				strTemp += "2. Choose the fields that you would like on your report from the 'Fields to display on Report'." + "\r\n" + "\r\n";
				strTemp += "3. The 'Fields to Sort By' section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
				strTemp += "4. Set any criteria needed in the 'Select Search Criteria' list." + "\r\n" + "\r\n";
				strTemp += "Other Notes:" + "\r\n" + "    Some fields will be printed automatically." + "\r\n" + "\r\n";
				strTemp += "\r\n" + "    If you choose a default report, only the account number and the tax year criteria will effect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "FORMS")
			{
				strTemp = "1. Create a new report by from the drop down box on the right of the screen." + "\r\n" + "\r\n";
				strTemp += "2. Choose the fields that you would like on your report from the 'Fields to display on Report'." + "\r\n" + "\r\n";
				strTemp += "3. The 'Fields to Sort By' section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
				strTemp += "4. Set any criteria needed in the 'Select Search Criteria' list." + "\r\n" + "\r\n";
				strTemp += "Other Notes:" + "\r\n" + "    Some fields will be printed automatically." + "\r\n" + "\r\n";
				strTemp += "\r\n" + "    If you choose a default report, only the account number and the tax year criteria will effect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
			}
			//txtNotes.Text = strText;
		}

		private void SetCopiesChkBoxes()
		{
			// kgk trout-416  Set forms so print checkboxes to match the selections made when printing 30-Day Notices
			clsDRWrapper rsControl = new clsDRWrapper();
			string strCntlTbl = "";
			string strWS = "";
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			switch (intRateType)
			{
				case 20:
					{
						strCntlTbl = strWS + "Control_30DayNotice";
						break;
					}
				case 21:
					{
						strCntlTbl = strWS + "Control_LienProcess";
						break;
					}
				case 22:
					{
						strCntlTbl = strWS + "Control_LienMaturity";
						break;
					}
			}
			//end switch
			rsControl.OpenRecordset("SELECT * FROM " + strCntlTbl, modExtraModules.strUTDatabase);
			if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("PayCertMailFee")))
			{
				chkFormPrintOptions[0].CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkFormPrintOptions[0].CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToMortHolder")))
			{
				if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("ChargeForMortHolder")))
				{
					chkFormPrintOptions[1].CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkFormPrintOptions[1].CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkFormPrintOptions[1].CheckState = Wisej.Web.CheckState.Unchecked;
			}
			if (FCConvert.ToString(rsControl.Get_Fields_String("SendCopyToNewOwner")) != "")
			{
				if (Strings.InStr(FCConvert.ToString(rsControl.Get_Fields_String("SendCopyToNewOwner")), "Yes, charge", CompareConstants.vbBinaryCompare) != 0)
				{
					chkFormPrintOptions[2].CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkFormPrintOptions[2].CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
			else
			{
				chkFormPrintOptions[2].CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}
	}
}
