﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmNewUTAccountNumber.
	/// </summary>
	public partial class frmNewUTAccountNumber : BaseForm
	{
		public frmNewUTAccountNumber()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmNewUTAccountNumber InstancePtr
		{
			get
			{
				return (frmNewUTAccountNumber)Sys.GetInstance(typeof(frmNewUTAccountNumber));
			}
		}

		protected frmNewUTAccountNumber _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/11/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/11/2005              *
		// ********************************************************
		int lngNumber;

		public int Init(ref int lngDefaultAcct)
		{
			int Init = 0;
			this.Text = "New Account Number";
			lngNumber = lngDefaultAcct;
			txtAccount.Text = FCConvert.ToString(lngNumber);
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngNumber;
			return Init;
		}

		private void frmNewUTAccountNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyCode == Keys.F12)
				{
					// check to make sure that the account number entered does not already exist
					ValidateAccountNumber();
				}
				// MAL@20070904: Check for Esc key or form close to stop account from being added
				// If KeyCode = 27 Or KeyCode = 46 Then   'Esc or 'X'
				if (KeyCode == Keys.Escape)
				{
					modGlobalConstants.Statics.blnAddCancel = true;
					// User cancelled adding a new account
					frmAccountMaster.InstancePtr.boolDoNotReturnToSearch = false;
					// Return to Search Screen
					this.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Prepayment Keydown Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmNewUTAccountNumber_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyAscii == Keys.Return)
				{
					// return key
					ValidateAccountNumber();
				}
				// MAL@20070904
				if (KeyAscii == Keys.Escape)
				{
					modGlobalConstants.Statics.blnAddCancel = true;
					// User cancelled adding a new account
					frmAccountMaster.InstancePtr.boolDoNotReturnToSearch = false;
					this.Unload();
				}
				// this will make sure that the characters are all uppercase
				if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
				{
					KeyAscii = KeyAscii - 32;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Prepayment Keypress Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
		}

		private void frmNewUTAccountNumber_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmNewUTAccountNumber properties;
			//frmNewUTAccountNumber.ScaleWidth	= 2895;
			//frmNewUTAccountNumber.ScaleHeight	= 1545;
			//frmNewUTAccountNumber.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWMINI);
			modGlobalFunctions.SetTRIOColors(this);
			this.Left = FCConvert.ToInt32((FCGlobal.Screen.Width - this.Width) / 2.0);
			this.Top = FCConvert.ToInt32((FCGlobal.Screen.Height - this.Height) / 2.0);
		}

		private bool ValidateAccountNumber()
		{
			bool ValidateAccountNumber = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAcct = new clsDRWrapper();
				rsAcct.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(Conversion.Val(txtAccount.Text)), modExtraModules.strUTDatabase);
				if (rsAcct.EndOfFile())
				{
					// MAL@20070904: Added check for maximum digits
					if (txtAccount.Text.Length > 9)
					{
						MessageBox.Show("Invalid account number. Account numbers must be 9 digits or less.", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtAccount.Text = FCConvert.ToString(lngNumber);
					}
					else if (Conversion.Val(txtAccount.Text) == 0)
					{
						MessageBox.Show("Invalid account number. Zero (0) is a reserved number.", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtAccount.Text = FCConvert.ToString(lngNumber);
					}
					else
					{
						lngNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
						frmAccountMaster.InstancePtr.boolDoNotReturnToSearch = true;
						//FC:FINAL:DDU:#1198 - don't close form in order to set account without error due to disposed object
						//frmAccountMaster.InstancePtr.Unload();
						this.Unload();
					}
				}
				else
				{
					MessageBox.Show("Invalid account number.  Please select a different one.", "Duplicate Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtAccount.Text = FCConvert.ToString(lngNumber);
				}
				return ValidateAccountNumber;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Validate Account Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateAccountNumber;
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// MAL@20070905: Check for 'X' out of form
			if (e.CloseReason == FCCloseReason.FormControlMenu)
			{
				modGlobalConstants.Statics.blnAddCancel = true;
				// User cancelled adding a new account
				frmAccountMaster.InstancePtr.boolDoNotReturnToSearch = false;
				this.Unload();
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			// no answer
			ValidateAccountNumber();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			ValidateAccountNumber();
		}

		private void txtAccount_Enter(object sender, System.EventArgs e)
		{
			txtAccount.SelectionStart = 0;
			txtAccount.SelectionLength = txtAccount.Text.Length;
		}
	}
}
