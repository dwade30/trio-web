﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmLienExclusion.
	/// </summary>
	public partial class frmLienExclusion : BaseForm
	{
		public frmLienExclusion()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienExclusion InstancePtr
		{
			get
			{
				return (frmLienExclusion)Sys.GetInstance(typeof(frmLienExclusion));
			}
		}

		protected frmLienExclusion _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/18/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/18/2006              *
		// ********************************************************
		int lngAction;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		clsDRWrapper rsValidate = new clsDRWrapper();
		bool boolLoaded;
		string strRateKeyList;
		bool boolDONOTACTIVATE;
		string strWS = "";
		int lngColCheck;
		int lngColAccount;
		int lngColName;
		int lngColLocation;
		int lngColBillKey;
		int lngColOrigCheck;

		public void Init(int lngType, ref string strRKList, ref bool boolWater)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngAction = lngType;
				strRateKeyList = strRKList;
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmLienExclusion_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				FormatGrid_2(true);
				ShowGridFrame();
				SetAct();
				boolLoaded = true;
				if (!boolDONOTACTIVATE)
				{
					if (!FillDemandGrid())
					{
						boolDONOTACTIVATE = true;
						this.Unload();
					}
					else
					{
						boolDONOTACTIVATE = false;
					}
				}
				else
				{
					// MsgBox "Stop"
				}
			}
		}

		private void frmLienExclusion_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLienExclusion properties;
			//frmLienExclusion.FillStyle	= 0;
			//frmLienExclusion.ScaleWidth	= 9045;
			//frmLienExclusion.ScaleHeight	= 7410;
			//frmLienExclusion.LinkTopic	= "Form2";
			//frmLienExclusion.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lngColCheck = 0;
			lngColOrigCheck = 1;
			lngColAccount = 2;
			lngColName = 3;
			lngColLocation = 5;
			lngColBillKey = 4;
			boolLoaded = false;
		}

		private void frmLienExclusion_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
				return;
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			boolDONOTACTIVATE = false;
		}

		private void frmLienExclusion_Resize(object sender, System.EventArgs e)
		{
			ShowGridFrame();
			// this sets the height of the grid
			//if ((vsDemand.Rows * vsDemand.RowHeight(0)) + 70 > fraGrid.Height - vsDemand.Top - 300)
			//{
			//    vsDemand.Height = fraGrid.Height - vsDemand.Top - 300;
			//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
			//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			// this will clear all account checkboxes in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(0));
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
		}

		private void FormatGrid_2(bool boolReset)
		{
			FormatGrid(ref boolReset);
		}

		private void FormatGrid(ref bool boolReset)
		{
			int wid = 0;
			vsDemand.Cols = 6;
			if (boolReset)
			{
				vsDemand.Rows = 1;
			}
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(lngColCheck, FCConvert.ToInt32(wid * 0.1));
			// Checkbox
			vsDemand.ColWidth(lngColAccount, FCConvert.ToInt32(wid * 0.1));
			// Acct
			vsDemand.ColWidth(lngColName, FCConvert.ToInt32(wid * 0.45));
			// Name
			vsDemand.ColWidth(lngColOrigCheck, 0);
			// Hidden check field
			vsDemand.ColWidth(lngColBillKey, 0);
			// Hidden Bill Key field
			vsDemand.ColWidth(lngColLocation, FCConvert.ToInt32(wid * 0.3));
			// Location
			vsDemand.ColDataType(lngColCheck, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColDataType(lngColOrigCheck, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(lngColBillKey, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.TextMatrix(0, lngColAccount, "Account");
			vsDemand.TextMatrix(0, lngColName, "Name");
			vsDemand.TextMatrix(0, lngColLocation, "Location");
		}

		private bool FillDemandGrid()
		{
			bool FillDemandGrid = false;
			int lngError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL = "";
				int lngIndex;
				clsDRWrapper rsMaster = new clsDRWrapper();
				double dblXInt = 0;
				string strBillFlds = "";
				FillDemandGrid = true;
				// rsMaster.OpenRecordset "SELECT * FROM Master", strUTDatabase
				lngError = 1;
				vsDemand.Rows = 1;
				lngError = 2;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				lngError = 3;
				if (lngAction == 1 || lngAction == 2)
				{
					strBillFlds = "ID, LienProcessExclusion, ActualAccountNumber, OName, Location, AccountKey, BillingRateKey, " + strWS + "IntPaidDate, " + strWS + "IntAdded, " + strWS + "CostAdded, " + strWS + "PrinOwed, " + strWS + "TaxOwed, " + strWS + "IntOwed, " + strWS + "CostOwed, " + strWS + "PrinPaid, " + strWS + "TaxPaid, " + strWS + "IntPaid, " + strWS + "CostPaid";
				}
				else if (lngAction == 3)
				{
					strBillFlds = "b.ID, b.LienProcessExclusion, b.ActualAccountNumber, b.OName, b.Location, b.AccountKey, " + "b." + strWS + "LienRecordNumber, n.RateKey, n.IntPaidDate, " + "n.Principal, n.Tax, n.Interest, n.IntAdded, n.Costs, n.MaturityFee, " + "n.PrinPaid, n.TaxPaid, n.PLIPaid, n.IntPaid, n.CostPaid";
				}
				switch (lngAction)
				{
					case 1:
						{
							// 30dn
							if (frmRateRecChoice.InstancePtr.cmbRange.Text == "All Accounts")
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill WHERE BillingRateKey IN " + strRateKeyList + " AND (" + strWS + "LienProcessStatus = 0 OR " + strWS + "LienProcessStatus = 1) ORDER BY OName, ActualAccountNumber";
								// this will be all the records that have had Lien Maturity Notices
							}
							else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill WHERE BillingRateKey IN " + strRateKeyList + " AND (" + strWS + "LienProcessStatus = 0 OR " + strWS + "LienProcessStatus = 1) AND OName > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' ORDER BY OName, ActualAccountNumber";
							}
							else
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill WHERE BillingRateKey IN " + strRateKeyList + " AND (" + strWS + "LienProcessStatus = 0 OR " + strWS + "LienProcessStatus = 1) AND ActualAccountNumber >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND ActualAccountNumber <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " ORDER BY OName, ActualAccountNumber";
							}
							break;
						}
					case 2:
						{
							// lien
							if (frmRateRecChoice.InstancePtr.cmbRange.Text == "All Accounts")
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill WHERE BillingRateKey IN " + strRateKeyList + " AND (" + strWS + "LienProcessStatus = 2 OR " + strWS + "LienProcessStatus = 3) ORDER BY OName, ActualAccountNumber";
								// this will be all the records that have had Lien Maturity Notices
							}
							else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill WHERE BillingRateKey IN " + strRateKeyList + " AND (" + strWS + "LienProcessStatus = 2 OR " + strWS + "LienProcessStatus = 3) AND OName > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' ORDER BY OName, ActualAccountNumber";
							}
							else
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill WHERE BillingRateKey IN " + strRateKeyList + " AND (" + strWS + "LienProcessStatus = 2 OR " + strWS + "LienProcessStatus = 3) AND ActualAccountNumber >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND ActualAccountNumber <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " ORDER BY OName, ActualAccountNumber";
							}
							break;
						}
					case 3:
						{
							// lien mat
							if (frmRateRecChoice.InstancePtr.cmbRange.Text == "All Accounts")
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill b INNER JOIN Lien n ON b." + strWS + "LienRecordNumber = n.ID WHERE n.RateKey IN " + strRateKeyList + " AND (" + strWS + "LienStatusEligibility = 4 OR " + strWS + "LienStatusEligibility = 5) ORDER BY OName, ActualAccountNumber";
								// this will be all the records that have had Lien Maturity Notices
							}
							else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill b INNER JOIN Lien n ON b." + strWS + "LienRecordNumber = n.ID WHERE n.RateKey IN " + strRateKeyList + " AND (" + strWS + "LienStatusEligibility = 4 OR " + strWS + "LienStatusEligibility = 5) AND OName > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' ORDER BY OName, ActualAccountNumber";
							}
							else
							{
								strSQL = "SELECT " + strBillFlds + " FROM Bill b INNER JOIN Lien n ON b." + strWS + "LienRecordNumber = n.ID WHERE n.RateKey IN " + strRateKeyList + " AND (" + strWS + "LienStatusEligibility = 4 OR " + strWS + "LienStatusEligibility = 5) AND ActualAccountNumber >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND ActualAccountNumber <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " ORDER BY OName, ActualAccountNumber";
							}
							rsLData.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
							// this is a recordset of all the lien records
							break;
						}
				}
				//end switch
				lngError = 4;
				// strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 5 AND LienStatusEligibility = 5 ORDER BY Name1"      'this will be all the records that have had 30 Day Notices printed
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					FillDemandGrid = false;
					MessageBox.Show("There are no accounts eligible.", "No Eligible Accounts.", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return FillDemandGrid;
				}
				lngError = 5;
				while (!rsData.EndOfFile())
				{
					//Application.DoEvents();
					// find the first lien record
					TRYAGAIN:
					;
					lngError = 11;
					switch (lngAction)
					{
						case 1:
						case 2:
							{
								if (modUTCalculations.CalculateAccountUT(rsData, DateTime.Today, ref dblXInt, FCConvert.CBool(strWS == "W")) <= 0)
								{
									// skip this account
									goto SKIP30DN;
								}
								// add a row/element
								vsDemand.AddItem("");
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCheck, FCConvert.ToString(rsData.Get_Fields_Boolean("LienProcessExclusion")));
								// Live checkbox
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColOrigCheck, FCConvert.ToString(rsData.Get_Fields_Boolean("LienProcessExclusion")));
								// Original Value
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAccount, FCConvert.ToString(rsData.Get_Fields_Int32("ActualAccountNumber")));
								// account number
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColName, FCConvert.ToString(rsData.Get_Fields_String("OName")));
								// name
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColBillKey, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
								// billkey
								vsDemand.TextMatrix(vsDemand.Rows - 1, lngColLocation, FCConvert.ToString(rsData.Get_Fields_String("Location")));
								SKIP30DN:
								;
								rsData.MoveNext();
								break;
							}
						case 3:
							{
								rsLData.FindFirstRecord("ID", Conversion.Val(rsData.Get_Fields(strWS + "LienRecordNumber")));
								if (rsLData.NoMatch)
								{
									// if there is no match, then report the error
									lngError = 12;
									frmWait.InstancePtr.Unload();
									MessageBox.Show("Cannot find Lien Record #" + rsData.Get_Fields(strWS + "LienRecordNumber") + ".", "Missing Lien Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
									if (rsLData.Get_Fields("Principal") - rsLData.Get_Fields("PrinPaid") <= 0)
									{
										rsData.MoveNext();
										if (rsData.EndOfFile())
											break;
										goto TRYAGAIN;
									}
									rsMaster.OpenRecordset("SELECT InBankruptcy FROM Master WHERE AccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber"), modExtraModules.strUTDatabase);
									if (rsMaster.EndOfFile())
									{
										lngError = 12;
										frmWait.InstancePtr.Unload();
										MessageBox.Show("Cannot find Master Record for account #" + rsData.Get_Fields_Int32("ActualAccountNumber") + ".", "Missing Master Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
										rsData.MoveNext();
									}
									else
									{
										if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("InBankruptcy")))
										{
											// this account is not eligible for LMFs
											rsData.MoveNext();
										}
										else
										{
											if (modUTCalculations.CalculateAccountUTLien(rsData, DateTime.Today, ref dblXInt, FCConvert.CBool(strWS == "W"), 0, 0, 0, true) <= 0)
											{
												goto SKIPLDN;
											}
											lngError = 13;
											vsDemand.AddItem("");
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColCheck, FCConvert.ToString(rsData.Get_Fields_Boolean("LienProcessExclusion")));
											// Live Checkbox
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColOrigCheck, FCConvert.ToString(rsData.Get_Fields_Boolean("LienProcessExclusion")));
											// Original Value
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColAccount, FCConvert.ToString(rsData.Get_Fields_Int32("ActualAccountNumber")));
											// account number
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColName, FCConvert.ToString(rsData.Get_Fields_String("OName")));
											// name
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColBillKey, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
											// billkey
											vsDemand.TextMatrix(vsDemand.Rows - 1, lngColLocation, FCConvert.ToString(rsData.Get_Fields_String("Location")));
											lngError = 14;
											SKIPLDN:
											;
											rsData.MoveNext();
										}
									}
								}
								break;
							}
					}
					//end switch
				}
				lngError = 9;
				// this sets the height of the grid
				//if ((vsDemand.Rows * vsDemand.RowHeight(0)) + 70 > fraGrid.Height - vsDemand.Top - 300)
				//{
				//    vsDemand.Height = fraGrid.Height - vsDemand.Top - 300;
				//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//    vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
				//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				lngError = 11;
				frmWait.InstancePtr.Unload();
				return FillDemandGrid;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				FillDemandGrid = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillDemandGrid;
		}

		private void SetAct()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will change all of the menu options
				switch (lngAction)
				{
					case 1:
						{
							// 30 DN validation
							this.Text = "30 Day Notice Exclusion";
                            //FC:FINAL:BSE #3979 header text should be the same as form text 
                            this.HeaderText.Text = "30 Day Notice Exclusion";
							lblInstruction.Text = "To exclude accounts from 30 Day Notices:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Save and Continue' or press F12.";
							break;
						}
					case 2:
						{
							// Lien Notice
							this.Text = "Lien Notice Exclusion";
                            //FC:FINAL:BSE #3979 header text should be the same as form text 
                            this.HeaderText.Text = "Lien Notice Exclusion";
							lblInstruction.Text = "To exclude accounts from Lien Notices:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Save and Continue' or press F12.";
							break;
						}
					case 3:
						{
							// Lien Mat notice
							this.Text = "Lien Maturity Notice Exclusion";
							lblInstruction.Text = "To exclude accounts from Lien Maturity Notices:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Save and Continue' or press F12.";
							break;
						}
				}
				//end switch
				ShowGridFrame();
				mnuFileSelectAll.Visible = true;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Set Action", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (ExcludeBills())
			{
				this.Unload();
			}
		}

		private bool ExcludeBills()
		{
			bool ExcludeBills = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				clsDRWrapper rsUpd = new clsDRWrapper();
				ExcludeBills = true;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Bills");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					//Application.DoEvents();
					if (FCConvert.ToBoolean(vsDemand.TextMatrix(lngCT, lngColCheck)) != FCConvert.ToBoolean(vsDemand.TextMatrix(lngCT, lngColOrigCheck)))
					{
						rsData.FindFirstRecord("ID", Conversion.Val(vsDemand.TextMatrix(lngCT, lngColBillKey)));
						if (!rsData.NoMatch)
						{
							lngDemandCount += 1;
							// rsData.Edit
							// rsData.Fields("LienProcessExclusion") = vsDemand.TextMatrix(lngCT, lngColCheck)
							// rsData.Update
							// kk12162016 trouts-201  Need to set to 0 when check is false!
							if (FCConvert.CBool(vsDemand.TextMatrix(lngCT, lngColCheck)))
							{
								rsUpd.Execute("UPDATE Bill Set LienProcessExclusion = 1 WHERE ID = " + FCConvert.ToString(Conversion.Val(vsDemand.TextMatrix(lngCT, lngColBillKey))), modExtraModules.strUTDatabase);
							}
							else
							{
								rsUpd.Execute("UPDATE Bill Set LienProcessExclusion = 0 WHERE ID = " + FCConvert.ToString(Conversion.Val(vsDemand.TextMatrix(lngCT, lngColBillKey))), modExtraModules.strUTDatabase);
							}
						}
					}
				}
				frmWait.InstancePtr.Unload();
				if (lngDemandCount == 1)
				{
					MessageBox.Show("1 bill was affected.", "Exclusion List Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(lngDemandCount) + " bills were affected.", "Exclusion List Updated", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return ExcludeBills;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Excluding Bills", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ExcludeBills;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(mnuFileSelectAll, new System.EventArgs());
		}

		private void vsDemand_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsDemand.Col == lngColCheck)
			{
				if (FCConvert.CBool(vsDemand.TextMatrix(vsDemand.Row, lngColCheck)) == true)
				{
					vsDemand.TextMatrix(vsDemand.Row, lngColCheck, FCConvert.ToString(false));
				}
				else
				{
					vsDemand.TextMatrix(vsDemand.Row, lngColCheck, FCConvert.ToString(true));
				}
			}
		}

		private void vsDemand_DblClick(object sender, System.EventArgs e)
		{
			if (vsDemand.MouseCol == 0 && vsDemand.MouseRow == 0)
			{
				mnuFileSelectAll_Click();
			}
		}

		private void vsDemand_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			// Dim lngMR           As Long
			// Dim lngMC           As Long
			// 
			// lngMR = vsDemand.MouseRow
			// lngMC = vsDemand.MouseCol
			// If lngMR > 0 And lngMC > 0 Then
			// If Val(vsDemand.TextMatrix(lngMR, 6)) = -1 Then
			// vsDemand.ToolTipText = "A Lien Record has already been created for this account.  This account is not eligible."
			// Else
			// vsDemand.ToolTipText = ""
			// End If
			// End If
		}

		private void vsDemand_RowColChange(object sender, System.EventArgs e)
		{
			vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
		}
	}
}
