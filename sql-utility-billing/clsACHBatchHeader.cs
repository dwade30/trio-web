//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class clsACHBatchHeader
	{

		//=========================================================

		private bool boolBalanced;
		private string strCompanyName = "";
		private string strDiscretionaryData = "";
		private string strCompanyID = "";
		private string strEntryClassCode = "";
		private string strEntryDescription = "";
		private string strDescriptiveDate = "";
		private string strEffectiveEntryDate = "";
		private string strOriginatorStatusCode = "";
		private string strOriginatingDFI = "";
		private string strLastError = "";
		private string strACHCompanyIDPrefix;

		public string RecordCode
		{
			get
			{
					string RecordCode = "";
				RecordCode = "5";
				return RecordCode;
			}
		}

		public bool Balanced
		{
			get
			{
					bool Balanced = false;
				Balanced = boolBalanced;
				return Balanced;
			}

			set
			{
				boolBalanced = value;
			}
		}



		public string CompanyName
		{
			get
			{
					string CompanyName = "";
				CompanyName = strCompanyName;
				return CompanyName;
			}

			set
			{
				strCompanyName = value;
			}
		}



		public string DiscretionaryData
		{
			get
			{
					string DiscretionaryData = "";
				DiscretionaryData = strDiscretionaryData;
				return DiscretionaryData;
			}

			set
			{
				strDiscretionaryData = value;
			}
		}



		public string CompanyID
		{
			get
			{
					string CompanyID = "";
				CompanyID = strCompanyID;
				return CompanyID;
			}

			set
			{
				strCompanyID = value;
			}
		}



		public string EntryClassCode
		{
			get
			{
					string EntryClassCode = "";
				EntryClassCode = strEntryClassCode;
				return EntryClassCode;
			}

			set
			{
				strEntryClassCode = value;
			}
		}



		public string EntryDescription
		{
			get
			{
					string EntryDescription = "";
				EntryDescription = strEntryDescription;
				return EntryDescription;
			}

			set
			{
				strEntryDescription = value;
			}
		}



		public string DescriptiveDate
		{
			get
			{
					string DescriptiveDate = "";
				DescriptiveDate = strDescriptiveDate;
				return DescriptiveDate;
			}

			set
			{
				strDescriptiveDate = value;
			}
		}



		public string EffectiveEntryDate
		{
			get
			{
					string EffectiveEntryDate = "";
				EffectiveEntryDate = strEffectiveEntryDate;
				return EffectiveEntryDate;
			}

			set
			{
				strEffectiveEntryDate = value;
			}
		}



		public string OriginatorStatusCode
		{
			get
			{
					string OriginatorStatusCode = "";
				OriginatorStatusCode = strOriginatorStatusCode;
				return OriginatorStatusCode;
			}

			set
			{
				strOriginatorStatusCode = value;
			}
		}



		public string OriginatingDFI
		{
			get
			{
					string OriginatingDFI = "";
				OriginatingDFI = strOriginatingDFI;
				return OriginatingDFI;
			}

			set
			{
				strOriginatingDFI = value;
			}
		}



        public int BatchNumber { get; set; } = 0;




		public string LastError
		{
			get
			{
					string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public string ServiceClassCode
		{
			get
			{
					string ServiceClassCode = "";
				if (boolBalanced) {
					ServiceClassCode = "200";
				} else {
					ServiceClassCode = "220";
				}
				return ServiceClassCode;
			}
		}

		public clsACHBatchHeader() : base()
		{
			strEntryClassCode = "PPD";
			strEntryDescription = "PAYROLL";
			strOriginatorStatusCode = "1";
			BatchNumber = 0;
			strACHCompanyIDPrefix = "1";
			strLastError = "";
		}

		public string OutputLine()
		{
			string OutputLine = "";
			try
			{	// On Error GoTo ErrorHandler
				string strLine;
				strLine = RecordCode;
				strLine += ServiceClassCode;
				strLine += fecherFoundation.Strings.Left(strCompanyName+fecherFoundation.Strings.StrDup(16, " "), 16);
				strLine += fecherFoundation.Strings.Left(strDiscretionaryData+fecherFoundation.Strings.StrDup(20, " "), 20);
				strLine += strACHCompanyIDPrefix+strCompanyID;
				strLine += strEntryClassCode;
				strLine += fecherFoundation.Strings.Left(strEntryDescription+fecherFoundation.Strings.StrDup(10, " "), 10);
				if (fecherFoundation.Information.IsDate(strDescriptiveDate)) {
					strLine += fecherFoundation.Strings.Right(FCConvert.ToString(DateTime.Parse(strDescriptiveDate).Year), 2)+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Parse(strDescriptiveDate).Month), 2)+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Parse(strDescriptiveDate).Day), 2);
				} else {
					strLine += fecherFoundation.Strings.StrDup(6, "0");
				}
				// effective entry date
				if (fecherFoundation.Information.IsDate(strEffectiveEntryDate)) {
					strLine += fecherFoundation.Strings.Right(FCConvert.ToString(DateTime.Parse(strEffectiveEntryDate).Year), 2)+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Parse(strEffectiveEntryDate).Month), 2)+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Parse(strEffectiveEntryDate).Day), 2);
				} else {
					strLine += fecherFoundation.Strings.StrDup(6, "0");
				}

				strLine += fecherFoundation.Strings.StrDup(3, " ");
				strLine += strOriginatorStatusCode;
				strLine += fecherFoundation.Strings.Left(strOriginatingDFI+fecherFoundation.Strings.StrDup(Convert.ToInt32("0"), fecherFoundation.Strings.Chr(8).ToString()), 8);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(7, "0")+FCConvert.ToString(BatchNumber), 7);
				OutputLine = strLine;
				return OutputLine;
			}
			catch (Exception ex)
			{	// ErrorHandler:
				strLastError = "Could not build batch header record."+"\n"+ex.Message;
			}
			return OutputLine;
		}

	}
}
