﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for sarLienEditMortHolders.
	/// </summary>
	public partial class sarLienEditMortHolders : FCSectionReport
	{
		public sarLienEditMortHolders()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "sarLienEditMortHolders";
		}

		public static sarLienEditMortHolders InstancePtr
		{
			get
			{
				return (sarLienEditMortHolders)Sys.GetInstance(typeof(sarLienEditMortHolders));
			}
		}

		protected sarLienEditMortHolders _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMort.Dispose();
				clsRealEstate.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarLienEditMortHolders	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               09/26/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               07/14/2004              *
		// ********************************************************
		clsDRWrapper rsMort = new clsDRWrapper();
		int lngAcct;
		bool boolShowAll;
		int lngExtraLines;
		bool boolUTMort;
		clsDRWrapper clsRealEstate = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsMort.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			//Application.DoEvents();
			if (frmRateRecChoice.InstancePtr.cmbExtraLines.SelectedIndex != -1)
			{
				lngExtraLines = FCConvert.ToInt32(Math.Round(Conversion.Val(frmRateRecChoice.InstancePtr.cmbExtraLines.Items[frmRateRecChoice.InstancePtr.cmbExtraLines.SelectedIndex].ToString())));
			}
			else
			{
				lngExtraLines = 0;
			}
			if (lngAcct < 0)
			{
				clsRealEstate.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAcct * -1), modExtraModules.strUTDatabase);
			}
			else
			{
				clsRealEstate.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAcct), modExtraModules.strUTDatabase);
			}
			if (clsRealEstate.EndOfFile() != true && clsRealEstate.BeginningOfFile() != true)
			{
				if (clsRealEstate.Get_Fields_Boolean("UseMortgageHolder") == true)
				{
					boolUTMort = false;
				}
				else
				{
					boolUTMort = true;
				}
			}
			else
			{
				boolUTMort = true;
			}
			if (lngAcct < 0)
			{
				boolShowAll = true;
				lngAcct *= -1;
				if (boolUTMort)
				{
					// MAL@20081112: Add check for Send flag
					// Tracker Reference: 16047
					// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & lngAcct & " AND Module = 'UT'", strGNDatabase
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(lngAcct) + " AND Module = 'UT' AND ReceiveCopies = 1", "CentralData");
				}
				else
				{
					// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & clsRealEstate.Fields("REAccount") & " AND Module = 'RE'", strGNDatabase
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + clsRealEstate.Get_Fields_Int32("REAccount") + " AND Module = 'RE' AND ReceiveCopies = 1", "CentralData");
				}
				fldMHAddr1.Visible = true;
				fldMHAddr2.Visible = true;
				fldMHAddr3.Visible = true;
				fldMHAddr4.Visible = true;
				fldBookPage.Visible = true;
				fldMHAddr1.Top = 180 / 1440f;
				fldMHAddr2.Top = 450 / 1440f;
				fldMHAddr3.Top = 810 / 1440f;
				fldMHAddr4.Top = 1080 / 1440f;
				fldBookPage.Top = 1350 / 1440f;
				this.Detail.Height = 1620 / 1440f;
			}
			else
			{
				if (lngAcct > 0)
				{
					if (boolUTMort)
					{
						rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(lngAcct) + " AND Module = 'UT' AND ReceiveCopies = 1", "CentralData");
					}
					else
					{
						rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + clsRealEstate.Get_Fields_Int32("REAccount") + " AND Module = 'RE' AND ReceiveCopies = 1", "CentralData");
					}
					switch (arLienProcessEditReport.InstancePtr.intReportDetail)
					{
						case 1:
							{
								fldMHAddr1.Visible = false;
								fldMHAddr2.Visible = false;
								fldMHAddr3.Visible = false;
								fldMHAddr4.Visible = false;
								fldBookPage.Visible = false;
								fldMHAddr1.Top = 0;
								fldMHAddr2.Top = 0;
								fldMHAddr3.Top = 0;
								fldMHAddr4.Top = 0;
								fldBookPage.Top = 0;
								this.Detail.Height = 180 / 1440f;
								break;
							}
						case 2:
							{
								fldMHAddr1.Visible = true;
								fldMHAddr2.Visible = true;
								fldMHAddr3.Visible = true;
								fldMHAddr4.Visible = true;
								fldBookPage.Visible = true;
								fldMHAddr1.Top = 180 / 1440f;
								fldMHAddr2.Top = 450 / 1440f;
								fldMHAddr3.Top = 810 / 1440f;
								fldMHAddr4.Top = 1080 / 1440f;
								fldBookPage.Top = 1350 / 1440f;
								this.Detail.Height = 1620 / 1440f;
								break;
							}
					}
					//end switch
				}
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			float lngNextLine = 0;
			if (!rsMort.EndOfFile())
			{
				fldMHName.Text = rsMort.Get_Fields_String("Name");
				lngNextLine = 180 / 1440F;
				if (arLienProcessEditReport.InstancePtr.intReportDetail == 2 || boolShowAll)
				{
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1"))) != "")
					{
						fldMHAddr1.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1")));
						fldMHAddr1.Top = lngNextLine;
						lngNextLine += 270 / 1440F;
					}
					else
					{
						fldMHAddr1.Text = "";
						fldMHAddr1.Top = 0;
					}
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
					{
						fldMHAddr2.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2")));
						fldMHAddr2.Top = lngNextLine;
						lngNextLine += 270 / 1440F;
					}
					else
					{
						fldMHAddr2.Text = "";
						fldMHAddr2.Top = 0;
					}
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
					{
						fldMHAddr3.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3")));
						fldMHAddr3.Top = lngNextLine;
						lngNextLine += 270 / 1440F;
					}
					else
					{
						fldMHAddr3.Text = "";
						fldMHAddr3.Top = 0;
					}
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("City"))) != "")
					{
						fldMHAddr4.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("City"))) + " " + Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("State"))) + ", " + Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip")));
						if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
						{
							fldMHAddr4.Text = fldMHAddr4.Text + Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4")));
						}
						fldMHAddr4.Top = lngNextLine;
						lngNextLine += 270 / 1440F;
					}
					else
					{
						fldMHAddr4.Text = "";
						fldMHAddr4.Top = 0;
					}
					if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("BookPage"))) != "")
					{
						fldBookPage.Text = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("BookPage")));
						fldBookPage.Top = lngNextLine;
						lngNextLine += 270 / 1440F;
					}
					else
					{
						fldBookPage.Text = "";
						fldBookPage.Top = 0;
					}
				}
				lblExtraLines.Top = (lngNextLine + (((lngExtraLines / 2.0F) - 1) * 270 / 1440F));
				Detail.Height = (lngNextLine + ((lngExtraLines / 2.0F) * 270 / 1440F));
				rsMort.MoveNext();
			}
			else
			{
				fldMHName.Text = "";
				fldMHAddr1.Text = "";
				fldMHAddr2.Text = "";
				fldMHAddr3.Text = "";
				fldMHAddr4.Text = "";
				fldBookPage.Text = "";
				lblExtraLines.Top = 0;
			}
		}

		private void sarLienEditMortHolders_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarLienEditMortHolders properties;
			//sarLienEditMortHolders.Icon	= "sarLienEditMortHolders.dsx":0000";
			//sarLienEditMortHolders.Left	= 0;
			//sarLienEditMortHolders.Top	= 0;
			//sarLienEditMortHolders.Width	= 11880;
			//sarLienEditMortHolders.Height	= 8010;
			//sarLienEditMortHolders.StartUpPosition	= 3;
			//sarLienEditMortHolders.SectionData	= "sarLienEditMortHolders.dsx":058A;
			//End Unmaped Properties
		}
	}
}
