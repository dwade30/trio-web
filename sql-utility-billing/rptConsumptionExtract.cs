﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System.Runtime.InteropServices;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptConsumptionExtract.
	/// </summary>
	public partial class rptConsumptionExtract : BaseSectionReport
	{
		public rptConsumptionExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Consumption Extract";
		}

		public static rptConsumptionExtract InstancePtr
		{
			get
			{
				return (rptConsumptionExtract)Sys.GetInstance(typeof(rptConsumptionExtract));
			}
		}

		protected rptConsumptionExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptConsumptionExtract	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// vbPorter upgrade warning: lngRecordCounter As int	OnWrite(int, double)
		int lngRecordCounter;
		int lngBookCount;
		int lngFinalCount;
		// vbPorter upgrade warning: lngBookConsumption As int	OnWrite(double, short)
		int lngBookConsumption;
		// vbPorter upgrade warning: lngFinalConsumption As int	OnWriteFCConvert.ToDouble(
		int lngFinalConsumption;
		bool blnFirstRecord;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (lngRecordCounter <= FCFileSystem.LOF(1) / Marshal.SizeOf(modTSCONSUM.Statics.CR))
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			else
			{
				lngRecordCounter += 1;
				if (lngRecordCounter <= FCFileSystem.LOF(1) / Marshal.SizeOf(modTSCONSUM.Statics.CR))
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			if (!eArgs.EOF)
			{
				FCFileSystem.FileGet(1, ref modTSCONSUM.Statics.CR, lngRecordCounter);
				this.Fields["Binder"].Value = Conversion.Val(modTSCONSUM.Statics.CR.Book);
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCFileSystem.FileClose(1);
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strBooks;
			int lngCurrentBook = 0;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			blnFirstRecord = true;
			fldBookCount.Text = "0";
			fldBookConsumption.Text = "0";
			fldFinalCount.Text = "0";
			fldFinalConsumption.Text = "0";
			strBooks = "Book(s): ";
			FCFileSystem.FileClose(1);
			FCFileSystem.FileOpen(1, "TSCONSUM.DAT", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(modTSCONSUM.Statics.CR));
			for (lngRecordCounter = 1; lngRecordCounter <= FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(modTSCONSUM.Statics.CR)); lngRecordCounter++)
			{
				FCFileSystem.FileGet(1, ref modTSCONSUM.Statics.CR, lngRecordCounter);
				if (lngCurrentBook != Conversion.Val(modTSCONSUM.Statics.CR.Book))
				{
					strBooks += FCConvert.ToString(Conversion.Val(modTSCONSUM.Statics.CR.Book)) + ", ";
					lngCurrentBook = FCConvert.ToInt32(Math.Round(Conversion.Val(modTSCONSUM.Statics.CR.Book)));
				}
			}
			if (strBooks != "Book(s): ")
			{
				strBooks = Strings.Left(strBooks, strBooks.Length - 2);
			}
			lblBooks.Text = strBooks;
			FCFileSystem.FileClose(1);
			FCFileSystem.FileOpen(1, "TSCONSUM.DAT", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(modTSCONSUM.Statics.CR));
			lngRecordCounter = 1;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldSequence.Text = modTSCONSUM.Statics.CR.Sequence;
			fldAccount.Text = Strings.Format(Conversion.Val(modTSCONSUM.Statics.CR.AcctNumber), "000000");
			fldName.Text = modTSCONSUM.Statics.CR.Name;
			fldLocation.Text = modTSCONSUM.Statics.CR.Location;
			fldConsumption.Text = FCConvert.ToString(Conversion.Val(modTSCONSUM.Statics.CR.Consumption));
			fldPrevious.Text = FCConvert.ToString(Conversion.Val(modTSCONSUM.Statics.CR.Previous));
			fldCurrent.Text = FCConvert.ToString(Conversion.Val(modTSCONSUM.Statics.CR.Current));
			lngBookCount += 1;
			//FC:FINAL:RPU:#i862 - Compare the length of string as in Original
			//if (Marshal.SizeOf(modTSCONSUM.CR.Consumption) > 9)
			if (modTSCONSUM.Statics.CR.Consumption.Length > 9)
			{
				lngBookConsumption += FCConvert.ToInt32(Conversion.Val(Strings.Left(modTSCONSUM.Statics.CR.Consumption, 9)));
				lngFinalConsumption += FCConvert.ToInt32(Conversion.Val(Strings.Left(modTSCONSUM.Statics.CR.Consumption, 9)));
			}
			else
			{
				lngBookConsumption += FCConvert.ToInt32(Conversion.Val(modTSCONSUM.Statics.CR.Consumption));
				lngFinalConsumption += FCConvert.ToInt32(Conversion.Val(modTSCONSUM.Statics.CR.Consumption));
			}
			lngFinalCount += 1;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldBookCount.Text = Strings.Format(lngBookCount, "#,##0");
			fldBookConsumption.Text = Strings.Format(lngBookConsumption, "#,##0");
			lngBookCount = 0;
			lngBookConsumption = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			fldFinalCount.Text = Strings.Format(lngFinalCount, "#,##0");
			fldFinalConsumption.Text = Strings.Format(lngFinalConsumption, "#,##0");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldBook.Text = "Book " + modTSCONSUM.Statics.CR.Book;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptConsumptionExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptConsumptionExtract properties;
			//rptConsumptionExtract.Caption	= "Consumption Extract";
			//rptConsumptionExtract.Icon	= "rptConsumptionExtract.dsx":0000";
			//rptConsumptionExtract.Left	= 0;
			//rptConsumptionExtract.Top	= 0;
			//rptConsumptionExtract.Width	= 11880;
			//rptConsumptionExtract.Height	= 8595;
			//rptConsumptionExtract.StartUpPosition	= 3;
			//rptConsumptionExtract.SectionData	= "rptConsumptionExtract.dsx":058A;
			//End Unmaped Properties
		}
	}
}
