﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReminderMailer.
	/// </summary>
	partial class frmReminderMailer : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCFrame> fraText;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtNotPastDue;
		public fecherFoundation.FCCheckBox chkShowPending;
		public fecherFoundation.FCFrame fraText_0;
		public fecherFoundation.FCTextBox txtNotPastDue_1;
		public fecherFoundation.FCTextBox txtNotPastDue_0;
		public fecherFoundation.FCLabel lblNotAmountDue;
		public fecherFoundation.FCLabel lblNotMessage;
		public fecherFoundation.FCLabel lblNotHeader;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReminderMailer));
			this.chkShowPending = new fecherFoundation.FCCheckBox();
			this.fraText_0 = new fecherFoundation.FCFrame();
			this.txtNotPastDue_1 = new fecherFoundation.FCTextBox();
			this.txtNotPastDue_0 = new fecherFoundation.FCTextBox();
			this.lblNotAmountDue = new fecherFoundation.FCLabel();
			this.lblNotMessage = new fecherFoundation.FCLabel();
			this.lblNotHeader = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.cmdFileClear = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowPending)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraText_0)).BeginInit();
			this.fraText_0.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 381);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkShowPending);
			this.ClientArea.Controls.Add(this.fraText_0);
			this.ClientArea.Size = new System.Drawing.Size(610, 321);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileClear);
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(271, 30);
			this.HeaderText.Text = "Reminder Notice-Mailer";
			// 
			// chkShowPending
			// 
			this.chkShowPending.Location = new System.Drawing.Point(30, 30);
			this.chkShowPending.Name = "chkShowPending";
			this.chkShowPending.Size = new System.Drawing.Size(235, 27);
			this.chkShowPending.TabIndex = 0;
			this.chkShowPending.Text = "Show Tenative Pending Due";
			this.chkShowPending.Visible = false;
			// 
			// fraText_0
			// 
			this.fraText_0.Controls.Add(this.txtNotPastDue_1);
			this.fraText_0.Controls.Add(this.txtNotPastDue_0);
			this.fraText_0.Controls.Add(this.lblNotAmountDue);
			this.fraText_0.Controls.Add(this.lblNotMessage);
			this.fraText_0.Controls.Add(this.lblNotHeader);
			this.fraText_0.Location = new System.Drawing.Point(30, 75);
			this.fraText_0.Name = "fraText_0";
			this.fraText_0.Size = new System.Drawing.Size(545, 243);
			this.fraText_0.TabIndex = 1;
			this.fraText_0.Text = "Message For Reminder Notices";
			// 
			// txtNotPastDue_1
			// 
			this.txtNotPastDue_1.MaxLength = 1028;
			this.txtNotPastDue_1.AutoSize = false;
			this.txtNotPastDue_1.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotPastDue_1.LinkItem = null;
			this.txtNotPastDue_1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotPastDue_1.LinkTopic = null;
			this.txtNotPastDue_1.Location = new System.Drawing.Point(20, 180);
			this.txtNotPastDue_1.Multiline = true;
			this.txtNotPastDue_1.Name = "txtNotPastDue_1";
			this.txtNotPastDue_1.Size = new System.Drawing.Size(478, 40);
			this.txtNotPastDue_1.TabIndex = 4;
			this.txtNotPastDue_1.Visible = false;
			this.txtNotPastDue_1.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotPastDue_KeyDown);
			// 
			// txtNotPastDue_0
			// 
			this.txtNotPastDue_0.MaxLength = 70;
			this.txtNotPastDue_0.AutoSize = false;
			this.txtNotPastDue_0.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotPastDue_0.LinkItem = null;
			this.txtNotPastDue_0.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotPastDue_0.LinkTopic = null;
			this.txtNotPastDue_0.Location = new System.Drawing.Point(20, 90);
			this.txtNotPastDue_0.Name = "txtNotPastDue_0";
			this.txtNotPastDue_0.Size = new System.Drawing.Size(478, 40);
			this.txtNotPastDue_0.TabIndex = 2;
			this.txtNotPastDue_0.KeyDown += new Wisej.Web.KeyEventHandler(this.txtNotPastDue_KeyDown);
			// 
			// lblNotAmountDue
			// 
			this.lblNotAmountDue.Location = new System.Drawing.Point(20, 150);
			this.lblNotAmountDue.Name = "lblNotAmountDue";
			this.lblNotAmountDue.Size = new System.Drawing.Size(478, 17);
			this.lblNotAmountDue.TabIndex = 3;
			this.lblNotAmountDue.Text = "AMOUNT DUE XX/XX/XXXX  ###,###.##";
			// 
			// lblNotMessage
			// 
			this.lblNotMessage.Location = new System.Drawing.Point(20, 60);
			this.lblNotMessage.Name = "lblNotMessage";
			this.lblNotMessage.Size = new System.Drawing.Size(470, 24);
			this.lblNotMessage.TabIndex = 1;
			this.lblNotMessage.Text = "THIS IS TO REMIND YOU THAT YOUR UTILITY BILL IS DUE ON XX/XX/XXXX";
			// 
			// lblNotHeader
			// 
			this.lblNotHeader.Location = new System.Drawing.Point(20, 30);
			this.lblNotHeader.Name = "lblNotHeader";
			this.lblNotHeader.Size = new System.Drawing.Size(470, 13);
			this.lblNotHeader.TabIndex = 0;
			this.lblNotHeader.Text = "******  UTILITY - REMINDER NOTICE  ******";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileClear,
				this.mnuFilePrint,
				this.mnuFileSeperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileClear
			// 
			this.mnuFileClear.Index = 0;
			this.mnuFileClear.Name = "mnuFileClear";
			this.mnuFileClear.Text = "Clear";
			this.mnuFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 1;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 2;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 3;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.AppearanceKey = "acceptButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(267, 30);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePrint.Size = new System.Drawing.Size(100, 48);
			this.cmdFilePrint.TabIndex = 0;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdFileClear
			// 
			this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClear.AppearanceKey = "toolbarButton";
			this.cmdFileClear.Location = new System.Drawing.Point(532, 29);
			this.cmdFileClear.Name = "cmdFileClear";
			this.cmdFileClear.Size = new System.Drawing.Size(50, 24);
			this.cmdFileClear.TabIndex = 1;
			this.cmdFileClear.Text = "Clear";
			this.cmdFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// frmReminderMailer
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 489);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmReminderMailer";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reminder Notice - Mailer";
			this.Load += new System.EventHandler(this.frmReminderMailer_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmReminderMailer_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReminderMailer_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkShowPending)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraText_0)).EndInit();
			this.fraText_0.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFilePrint;
		private FCButton cmdFileClear;
	}
}
