//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class clsACHFileHeaderRecord
	{

		//=========================================================

		private string strPriorityCode;
		private string strImmediateDestination; // achbankrt
		private string strImmediateOriginRT;
		private string strFileIDModifier;
		private string strRecordSize;
		private string strBlockSize;
		private string strImmediateDestinationName;
		private string strImmediateOriginName;
		private string strReferenceCode;
		private string strLastError;

		public string PriorityCode
		{
			set
			{
				strPriorityCode = value;
			}

			get
			{
					string PriorityCode = "";
				PriorityCode = strPriorityCode;
				return PriorityCode;
			}
		}



		public string ImmediateDestination
		{
			set
			{
				strImmediateDestination = value;
			}

			get
			{
					string ImmediateDestination = "";
				ImmediateDestination = strImmediateDestination;
				return ImmediateDestination;
			}
		}



		public string ImmediateOriginRT
		{
			set
			{
				strImmediateOriginRT = value;
			}

			get
			{
					string ImmediateOriginRT = "";
				ImmediateOriginRT = strImmediateOriginRT;
				return ImmediateOriginRT;
			}
		}



		public string FileIDModifier
		{
			set
			{
				strFileIDModifier = value;
			}

			get
			{
					string FileIDModifier = "";
				FileIDModifier = strFileIDModifier;
				return FileIDModifier;
			}
		}



		public string RecordSize
		{
			set
			{
				strRecordSize = value;
			}

			get
			{
					string RecordSize = "";
				RecordSize = strRecordSize;
				return RecordSize;
			}
		}



		public string BlockSize
		{
			set
			{
				strBlockSize = value;
			}

			get
			{
					string BlockSize = "";
				BlockSize = strBlockSize;
				return BlockSize;
			}
		}



		public string ImmediateDestinationName
		{
			set
			{
				strImmediateDestinationName = value;
			}

			get
			{
					string ImmediateDestinationName = "";
				ImmediateDestinationName = strImmediateDestinationName;
				return ImmediateDestinationName;
			}
		}



		public string ImmediateOriginName
		{
			set
			{
				strImmediateOriginName = value;
			}

			get
			{
					string ImmediateOriginName = "";
				ImmediateOriginName = strImmediateOriginName;
				return ImmediateOriginName;
			}
		}



		public string ReferenceCode
		{
			set
			{
				strReferenceCode = value;
			}

			get
			{
					string ReferenceCode = "";
				ReferenceCode = strReferenceCode;
				return ReferenceCode;
			}
		}



		public string LastError
		{
			set
			{
				strLastError = value;
			}

			get
			{
					string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}



		public clsACHFileHeaderRecord() : base()
		{
			strPriorityCode = "01";
			strFileIDModifier = "A";
			strRecordSize = "094";
			strBlockSize = "10";
			strReferenceCode = "        ";
		}

		public bool CheckData()
		{
			bool CheckData = false;
			if (strPriorityCode=="") {
				strLastError = "Priority code is blank.";
				return CheckData;
			}
			if (strImmediateDestination=="") {
				strLastError = "The immediate destination is blank.";
				return CheckData;
			}
			if (strImmediateOriginRT=="") {
				strLastError = "The immediate origin is blank.";
				return CheckData;
			}
			if (strImmediateDestinationName=="") {
				strLastError = "The immediate destination name is blank.";
				return CheckData;
			}
			if (strImmediateOriginName=="") {
				strLastError = "The immediate origin is blank.";
				return CheckData;
			}
			CheckData = true;
			return CheckData;
		}

		public string RecordCode
		{
			get
			{
					string RecordCode = "";
				RecordCode = "1";
				return RecordCode;
			}
		}

		public string OutputLine()
		{
			string OutputLine = "";
			strLastError = "";
			string strLine = "";
			string strTemp = "";

			try
			{	// On Error GoTo ErrorHandler
				if (CheckData()) {
					strLine = RecordCode;
					strLine += strPriorityCode;
					strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(10, " ")+strImmediateDestination, 10);
					strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(10, " ")+strImmediateOriginRT, 10);
					strLine += fecherFoundation.Strings.Right(FCConvert.ToString(DateTime.Now.Year), 2)+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Now.Month), 2)+fecherFoundation.Strings.Right("00"+FCConvert.ToString(DateTime.Now.Day), 2);
					strLine += fecherFoundation.Strings.Format(DateTime.Now, "hhmm");
					strLine += strFileIDModifier;
					strLine += strRecordSize;
					strLine += strBlockSize;
					strLine += "1";
					strLine += fecherFoundation.Strings.Left(strImmediateDestinationName+fecherFoundation.Strings.StrDup(23, " "), 23);
					strLine += fecherFoundation.Strings.Left(strImmediateOriginName+fecherFoundation.Strings.StrDup(23, " "), 23);
					strLine += fecherFoundation.Strings.Left(strReferenceCode+fecherFoundation.Strings.StrDup(8, " "), 8);
					OutputLine = strLine;
				} else {
					strLastError = "Could not build file header record."+"\n"+strLastError;
					return OutputLine;
				}

				OutputLine = strLine;
				return OutputLine;
			}
			catch
			{	// ErrorHandler:
				strLastError = "Could not build file header record.";
			}
			return OutputLine;
		}


	}
}
