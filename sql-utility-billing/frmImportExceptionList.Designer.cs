﻿namespace TWUT0000
{
    partial class frmImportExceptionList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
			fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.txtAddAcct = new fecherFoundation.FCTextBox();
            this.vsGrid = new fecherFoundation.FCGrid();
            this.cmdRemoveAccount = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveAccount)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 482);
            this.BottomPanel.Size = new System.Drawing.Size(1024, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.vsGrid);
            this.ClientArea.Controls.Add(this.txtAddAcct);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Size = new System.Drawing.Size(1024, 422);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdRemoveAccount);
            this.TopPanel.Size = new System.Drawing.Size(1024, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRemoveAccount, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = false;
            this.HeaderText.Size = new System.Drawing.Size(282, 30);
            this.HeaderText.Text = "Import Exception List";
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(30, 44);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(69, 15);
            this.fcLabel1.TabIndex = 3;
            this.fcLabel1.Text = "ACCOUNT";
            // 
            // txtAddAcct
            // 
            this.txtAddAcct.InputType.Type = Wisej.Web.TextBoxType.Number;
            this.txtAddAcct.Location = new System.Drawing.Point(114, 30);
            this.txtAddAcct.Name = "txtAddAcct";
            this.txtAddAcct.Size = new System.Drawing.Size(167, 41);
            this.txtAddAcct.TabIndex = 1;
            // 
            // vsGrid
            // 
            this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsGrid.Location = new System.Drawing.Point(30, 100);
            this.vsGrid.Name = "vsGrid";
            this.vsGrid.Size = new System.Drawing.Size(964, 302);
            this.vsGrid.TabIndex = 2;
            // 
            // cmdRemoveAccount
            // 
            this.cmdRemoveAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRemoveAccount.Location = new System.Drawing.Point(825, 31);
            this.cmdRemoveAccount.Name = "cmdRemoveAccount";
            this.cmdRemoveAccount.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdRemoveAccount.Size = new System.Drawing.Size(186, 24);
            this.cmdRemoveAccount.TabIndex = 2;
            this.cmdRemoveAccount.Text = "Remove Account From List";
            // 
            // frmImportExceptionList
            // 
            this.ClientSize = new System.Drawing.Size(1024, 590);
            this.Name = "frmImportExceptionList";
            this.Text = "Import Exception List";
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveAccount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private fecherFoundation.FCLabel fcLabel1;
        private fecherFoundation.FCTextBox txtAddAcct;
        private fecherFoundation.FCGrid vsGrid;
        private fecherFoundation.FCButton cmdRemoveAccount;
    }
}