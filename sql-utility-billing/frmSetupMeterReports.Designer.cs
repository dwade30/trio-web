﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmSetupMeterReports.
	/// </summary>
	partial class frmSetupMeterReports : BaseForm
	{
		public fecherFoundation.FCComboBox cmbName;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblAll;
		public fecherFoundation.FCComboBox cmbLabelLocation;
		public fecherFoundation.FCLabel lblLabelLocation;
		public fecherFoundation.FCComboBox cmbLong;
		public fecherFoundation.FCLabel lblLong;
		public fecherFoundation.FCFrame fraLabelInformation;
		public fecherFoundation.FCFrame fraLabelType;
		public fecherFoundation.FCComboBox cboLabelType;
		public fecherFoundation.FCLabel lblLabelDescription;
		public fecherFoundation.FCCheckBox chkDuplicateLabels;
		public fecherFoundation.FCCommonDialog CommonDialog1;
		public fecherFoundation.FCFrame fraSetDate;
		public fecherFoundation.FCCheckBox chkShowNoMeterSetDate;
		public Global.T2KDateBox txtSetDate;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupMeterReports));
            this.cmbName = new fecherFoundation.FCComboBox();
            this.lblName = new fecherFoundation.FCLabel();
            this.cmbWS = new fecherFoundation.FCComboBox();
            this.lblWS = new fecherFoundation.FCLabel();
            this.cmbAll = new fecherFoundation.FCComboBox();
            this.lblAll = new fecherFoundation.FCLabel();
            this.cmbLabelLocation = new fecherFoundation.FCComboBox();
            this.lblLabelLocation = new fecherFoundation.FCLabel();
            this.cmbLong = new fecherFoundation.FCComboBox();
            this.lblLong = new fecherFoundation.FCLabel();
            this.fraLabelInformation = new fecherFoundation.FCFrame();
            this.fraLabelType = new fecherFoundation.FCFrame();
            this.cboLabelType = new fecherFoundation.FCComboBox();
            this.lblLabelDescription = new fecherFoundation.FCLabel();
            this.chkDuplicateLabels = new fecherFoundation.FCCheckBox();
            this.CommonDialog1 = new fecherFoundation.FCCommonDialog();
            this.fraSetDate = new fecherFoundation.FCFrame();
            this.chkShowNoMeterSetDate = new fecherFoundation.FCCheckBox();
            this.txtSetDate = new Global.T2KDateBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLabelInformation)).BeginInit();
            this.fraLabelInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLabelType)).BeginInit();
            this.fraLabelType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDuplicateLabels)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSetDate)).BeginInit();
            this.fraSetDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowNoMeterSetDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSetDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 592);
            this.BottomPanel.Size = new System.Drawing.Size(1039, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraSetDate);
            this.ClientArea.Controls.Add(this.cmbName);
            this.ClientArea.Controls.Add(this.lblName);
            this.ClientArea.Controls.Add(this.cmbWS);
            this.ClientArea.Controls.Add(this.lblWS);
            this.ClientArea.Controls.Add(this.cmbAll);
            this.ClientArea.Controls.Add(this.lblAll);
            this.ClientArea.Controls.Add(this.fraLabelInformation);
            this.ClientArea.Controls.Add(this.cmbLong);
            this.ClientArea.Controls.Add(this.lblLong);
            this.ClientArea.Size = new System.Drawing.Size(1039, 532);
            this.ClientArea.TabIndex = 0;
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(1039, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(234, 30);
            this.HeaderText.Text = "Meter Reading Slips";
            // 
            // cmbName
            // 
            this.cmbName.Items.AddRange(new object[] {
            "Name",
            "Location",
            "Account",
            "Serial Number"});
            this.cmbName.Location = new System.Drawing.Point(631, 30);
            this.cmbName.Name = "cmbName";
            this.cmbName.Size = new System.Drawing.Size(331, 40);
            this.cmbName.TabIndex = 5;
            this.cmbName.Text = "Name";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(503, 44);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(72, 15);
            this.lblName.TabIndex = 4;
            this.lblName.Text = "ORDER BY";
            // 
            // cmbWS
            // 
            this.cmbWS.Items.AddRange(new object[] {
            "All",
            "Water",
            "Sewer"});
            this.cmbWS.Location = new System.Drawing.Point(631, 90);
            this.cmbWS.Name = "cmbWS";
            this.cmbWS.Size = new System.Drawing.Size(331, 40);
            this.cmbWS.TabIndex = 7;
            // 
            // lblWS
            // 
            this.lblWS.AutoSize = true;
            this.lblWS.Location = new System.Drawing.Point(580, 104);
            this.lblWS.Name = "lblWS";
            this.lblWS.Size = new System.Drawing.Size(62, 15);
            this.lblWS.Visible = false;
            this.lblWS.TabIndex = 6;
            this.lblWS.Text = "SERVICE";
            // 
            // cmbAll
            // 
            this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Selected Books"});
            this.cmbAll.Location = new System.Drawing.Point(198, 30);
            this.cmbAll.Name = "cmbAll";
            this.cmbAll.Size = new System.Drawing.Size(207, 40);
            this.cmbAll.TabIndex = 1;
            this.cmbAll.Text = "All";
            this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
            // 
            // lblAll
            // 
            this.lblAll.AutoSize = true;
            this.lblAll.Location = new System.Drawing.Point(30, 44);
            this.lblAll.Name = "lblAll";
            this.lblAll.Size = new System.Drawing.Size(116, 15);
            this.lblAll.TabIndex = 8;
            this.lblAll.Text = "ACCOUNT RANGE";
            // 
            // cmbLabelLocation
            // 
            this.cmbLabelLocation.Items.AddRange(new object[] {
            "Address",
            "Location"});
            this.cmbLabelLocation.Location = new System.Drawing.Point(122, 30);
            this.cmbLabelLocation.Name = "cmbLabelLocation";
            this.cmbLabelLocation.Size = new System.Drawing.Size(207, 40);
            this.cmbLabelLocation.TabIndex = 1;
            this.cmbLabelLocation.Text = "Address";
            // 
            // lblLabelLocation
            // 
            this.lblLabelLocation.AutoSize = true;
            this.lblLabelLocation.Location = new System.Drawing.Point(20, 44);
            this.lblLabelLocation.Name = "lblLabelLocation";
            this.lblLabelLocation.Size = new System.Drawing.Size(79, 15);
            this.lblLabelLocation.TabIndex = 4;
            this.lblLabelLocation.Text = "LABEL INFO";
            // 
            // cmbLong
            // 
            this.cmbLong.Items.AddRange(new object[] {
            "8 1/2\" Slip",
            "9\" Slip"});
            this.cmbLong.Location = new System.Drawing.Point(196, 90);
            this.cmbLong.Name = "cmbLong";
            this.cmbLong.Size = new System.Drawing.Size(209, 40);
            this.cmbLong.TabIndex = 3;
            this.cmbLong.Text = "8 1/2\" Slip";
            // 
            // lblLong
            // 
            this.lblLong.AutoSize = true;
            this.lblLong.Location = new System.Drawing.Point(30, 104);
            this.lblLong.Name = "lblLong";
            this.lblLong.Size = new System.Drawing.Size(70, 15);
            this.lblLong.TabIndex = 2;
            this.lblLong.Text = "SLIP TYPE";
            // 
            // fraLabelInformation
            // 
            this.fraLabelInformation.Controls.Add(this.fraLabelType);
            this.fraLabelInformation.Controls.Add(this.cmbLabelLocation);
            this.fraLabelInformation.Controls.Add(this.lblLabelLocation);
            this.fraLabelInformation.Controls.Add(this.chkDuplicateLabels);
            this.fraLabelInformation.Location = new System.Drawing.Point(30, 160);
            this.fraLabelInformation.Name = "fraLabelInformation";
            this.fraLabelInformation.Size = new System.Drawing.Size(376, 316);
            this.fraLabelInformation.TabIndex = 8;
            this.fraLabelInformation.Text = "Label Information";
            this.fraLabelInformation.Visible = false;
            // 
            // fraLabelType
            // 
            this.fraLabelType.Controls.Add(this.cboLabelType);
            this.fraLabelType.Controls.Add(this.lblLabelDescription);
            this.fraLabelType.Location = new System.Drawing.Point(20, 130);
            this.fraLabelType.Name = "fraLabelType";
            this.fraLabelType.Size = new System.Drawing.Size(309, 168);
            this.fraLabelType.TabIndex = 3;
            this.fraLabelType.Text = "Select Label Type";
            // 
            // cboLabelType
            // 
            this.cboLabelType.BackColor = System.Drawing.SystemColors.Window;
            this.cboLabelType.Location = new System.Drawing.Point(20, 30);
            this.cboLabelType.Name = "cboLabelType";
            this.cboLabelType.Size = new System.Drawing.Size(259, 40);
            this.cboLabelType.SelectedIndexChanged += new System.EventHandler(this.cboLabelType_SelectedIndexChanged);
            // 
            // lblLabelDescription
            // 
            this.lblLabelDescription.Location = new System.Drawing.Point(20, 90);
            this.lblLabelDescription.Name = "lblLabelDescription";
            this.lblLabelDescription.Size = new System.Drawing.Size(259, 67);
            this.lblLabelDescription.TabIndex = 1;
            // 
            // chkDuplicateLabels
            // 
            this.chkDuplicateLabels.Location = new System.Drawing.Point(20, 90);
            this.chkDuplicateLabels.Name = "chkDuplicateLabels";
            this.chkDuplicateLabels.Size = new System.Drawing.Size(185, 26);
            this.chkDuplicateLabels.TabIndex = 2;
            this.chkDuplicateLabels.Text = "Print one label per account";
            // 
            // CommonDialog1
            // 
            this.CommonDialog1.Name = "CommonDialog1";
            this.CommonDialog1.Size = new System.Drawing.Size(0, 0);
            // 
            // fraSetDate
            // 
            this.fraSetDate.Controls.Add(this.chkShowNoMeterSetDate);
            this.fraSetDate.Controls.Add(this.txtSetDate);
            this.fraSetDate.Controls.Add(this.Label1);
            this.fraSetDate.Location = new System.Drawing.Point(21, 160);
            this.fraSetDate.Name = "fraSetDate";
            this.fraSetDate.Size = new System.Drawing.Size(568, 179);
            this.fraSetDate.TabIndex = 9;
            this.fraSetDate.Text = "Set Date";
            this.fraSetDate.Visible = false;
            // 
            // chkShowNoMeterSetDate
            // 
            this.chkShowNoMeterSetDate.Location = new System.Drawing.Point(20, 130);
            this.chkShowNoMeterSetDate.Name = "chkShowNoMeterSetDate";
            this.chkShowNoMeterSetDate.Size = new System.Drawing.Size(227, 27);
            this.chkShowNoMeterSetDate.TabIndex = 2;
            this.chkShowNoMeterSetDate.Text = "List meters with no set date";
            // 
            // txtSetDate
            // 
            this.txtSetDate.Location = new System.Drawing.Point(20, 70);
            this.txtSetDate.Mask = "##/##/####";
            this.txtSetDate.MaxLength = 10;
            this.txtSetDate.Name = "txtSetDate";
            this.txtSetDate.Size = new System.Drawing.Size(115, 40);
            this.txtSetDate.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(542, 29);
            this.Label1.TabIndex = 3;
            this.Label1.Text = "ALL METERS WITH A SET DATE ON OR BEFORE THE DATE YOU ENTERED WILL BE LISTED";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Process";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 1;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(465, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(108, 48);
            this.cmdProcessSave.Text = "Process";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmSetupMeterReports
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1039, 700);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSetupMeterReports";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Meter Reading Slips";
            this.Load += new System.EventHandler(this.frmSetupMeterReports_Load);
            this.Activated += new System.EventHandler(this.frmSetupMeterReports_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupMeterReports_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLabelInformation)).EndInit();
            this.fraLabelInformation.ResumeLayout(false);
            this.fraLabelInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraLabelType)).EndInit();
            this.fraLabelType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkDuplicateLabels)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSetDate)).EndInit();
            this.fraSetDate.ResumeLayout(false);
            this.fraSetDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowNoMeterSetDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSetDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}
