﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptLienNoticeSummary.
	/// </summary>
	public partial class rptLienNoticeSummary : BaseSectionReport
	{
		public rptLienNoticeSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptLienNoticeSummary InstancePtr
		{
			get
			{
				return (rptLienNoticeSummary)Sys.GetInstance(typeof(rptLienNoticeSummary));
			}
		}

		protected rptLienNoticeSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsUT.Dispose();
				rsLien.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLienNoticeSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/31/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/05/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int intYear;
		clsDRWrapper rsUT = new clsDRWrapper();
		clsDRWrapper rsLien = new clsDRWrapper();
		double dblTotalPrin;
		double dblTotalTax;
		double dblTotalPLI;
		double dblTotalCost;
		double dblTotalCurrentInt;
		double dblTotalTotal;
		double dblTotalDemand;
		DateTime dtMailDate;
		double dblDemand;
		bool boolWater;
		string strWS = "";
		int lngReportType;
		string str30DNQuery;
		int lngLastAccountKey;
		//clsDRWrapper rsSummary = new clsDRWrapper();
		string strSummary = "";
		string strHeaderTitle = "";
		int intCurrentIndex;
		string strPassedAcctList = "";
        bool boolAddressCO;
		// vbPorter upgrade warning: intPassYear As short	OnWriteFCConvert.ToInt32(
		public void Init(ref string strAccountList, string strPassHeaderTitle, string strPassReportType, ref short intPassYear, ref DateTime dtPassDate, ref bool boolPassWater, bool blnAcctSort, int lngRepType, ref string strPass30DNQuery, ref bool boolAddressCareOf)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strWhere = "";
				string strOrder = "";
				string strTemp = "";
                boolAddressCO = boolAddressCareOf;
				boolWater = boolPassWater;
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				intCurrentIndex = 0;
				lngReportType = lngRepType;
				// MAL@20080813: Add sort order option
				// Tracker Reference: 14183
				if (blnAcctSort)
				{
					strOrder = "ORDER BY ActualAccountNumber, OName";
				}
				else
				{
					strOrder = "ORDER BY OName, ActualAccountNumber";
				}
				str30DNQuery = strPass30DNQuery;
				dtMailDate = dtPassDate;
				// this will set the title of the report
				if (Strings.Trim(strPassHeaderTitle) != "")
				{
					lblHeader.Text = strPassHeaderTitle;
					this.Name = strPassHeaderTitle;
				}
				else
				{
					lblHeader.Text = "Summary Report";
					this.Name = "Summary Report";
				}
				// this will set the description of the report
				if (Strings.Trim(strPassReportType) != "")
				{
					lblReportType.Text = strPassReportType;
				}
				else
				{
					lblReportType.Text = strPassReportType;
				}
				intYear = intPassYear;
				// this will setup the data connection
				if (Strings.Trim(strAccountList) != "")
				{
					if (Strings.Right(strAccountList, 1) == ",")
					{
						strAccountList = Strings.Left(strAccountList, strAccountList.Length - 1);
					}
					strPassedAcctList = strAccountList;
					strTemp = "SELECT * FROM Bill WHERE ID IN (" + strAccountList + ") AND BillStatus = 'B'";
					rsData.OpenRecordset("SELECT * FROM Bill WHERE ID IN (" + strAccountList + ") AND BillStatus = 'B' " + strOrder, modExtraModules.strUTDatabase);
					if (rsData.EndOfFile())
					{
						this.Close();
					}
					else
					{
						// MAL@20070927: Added Select Case to handle new multiple owner code
						strHeaderTitle = strPassHeaderTitle;
						if (strPassHeaderTitle == "30 Day Notice Summary")
						{
							strSummary = strPass30DNQuery;
						}
						else if (strPassHeaderTitle == "Lien Notice Summary")
						{
							strSummary = strTemp;
							// kgk   rsData.Name
						}
						else if (strPassHeaderTitle == "Lien Maturity Notice Summary")
						{
							strSummary = strTemp;
							// kgk   rsData.Name
						}
						// kgk rsUT.OpenRecordset "SELECT Key, AccountNumber, OAddress1, OAddress2, OAddress3, OCity, OState, OZip, OZip4, InBankruptcy FROM Master", strUTDatabase
						rsUT.OpenRecordset("SELECT Master.ID, AccountNumber, DeedName1,DeedName2, p.Address1 AS OAddress1, p.Address2 AS OAddress2, p.Address3 AS OAddress3, p.City AS OCity, p.State AS OState, p.Zip AS OZip, InBankruptcy " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView p ON p.ID = Master.OwnerPartyID", modExtraModules.strUTDatabase);
						rsLien.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
						if (lngReportType == 0)
						{
							lblDemand.Visible = false;
							fldDemand.Visible = false;
							fldTotalDemand.Visible = false;
						}
						frmReportViewer.InstancePtr.Init(this._InstancePtr);
					}
				}
				else
				{
					Cancel();
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Summary", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1);
			// EOF = rsData.EndOfFile
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCombineBills = new clsDRWrapper();
				double dblPrin;
				double dblTax;
				double dblPLI;
				double dblCosts;
				double dblCurrInt;
				double dblTotal = 0;
				double dblAddedCosts = 0;
				int lngIndex = 0;
				string strWS = "";
				double dblTempInt;
				bool boolCombinedBills;
				double dblTempCurInt = 0;
				clsDRWrapper rsTempInfo = new clsDRWrapper();
				bool blnContinue = false;
				double dblMinimum = 0;
				double dblBalanceDue = 0;
				double dblDemand = 0;
				int lngCurrBillKey;
				clsDRWrapper rsPayment = new clsDRWrapper();
				double dblCurrBillInt = 0;
				// Latest Tenant/Owner Information
				string strLastOwner = "";
				string strLastOwner2 = "";
				string strLastAddress = "";
				string strLastAddress2 = "";
				string strLastAddress3 = "";
				string strLastCity = "";
				string strLastState = "";
				string strLastZip = "";
                string OwnerName = "";
				clsDRWrapper rsTempData = new clsDRWrapper();
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				TRYAGAIN:
				;
				dblPrin = 0;
				dblTax = 0;
				dblPLI = 0;
				dblCosts = 0;
				dblCurrInt = 0;
				fldAccount.Text = "";
				fldName.Text = "";
				fldCosts.Text = "";
				fldPrincipal.Text = "";
				fldTax.Text = "";
				fldDemand.Text = "";
				fldPLInt.Text = "";
				fldTotal.Text = "";
				if (intCurrentIndex <= Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
				{
					rsTempData.OpenRecordset("SELECT * FROM Bill WHERE ID IN (" + strPassedAcctList + ") AND AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey) + " AND BillStatus = 'B' ", modExtraModules.strUTDatabase);
					if (!rsTempData.EndOfFile())
					{
						// If intCurrentIndex <= UBound(typAccountInfo) Then
						// this is the extra amount due from costs for this account
						rsTempInfo.OpenRecordset("SELECT * FROM (" + rsTempData.Name() + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey));
						if (rsTempInfo.EndOfFile() != true && rsTempInfo.BeginningOfFile() != true)
						{
							do
							{
								// MAL@20071108: Get demand fee from Payment table
								// kk08242016 trouts-200  Add service to query so we don't combine demand fees from Water and Sewer
								rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey) + " AND BillKey = " + rsTempInfo.Get_Fields_Int32("ID") + " AND Reference = 'DEMAND' AND Service = '" + strWS + "'", modExtraModules.strUTDatabase);
								if (rsPayment.RecordCount() > 0)
								{
									while (!rsPayment.EndOfFile())
									{
										dblDemand += FCConvert.ToDouble((rsPayment.Get_Fields_Decimal("LienCost") * -1));
										rsPayment.MoveNext();
									}
								}
								else
								{
									dblDemand = dblDemand;
								}
								lngIndex = ReturnAccountIndex_2(rsTempInfo.Get_Fields_Int32("ID"));
								if (lngIndex > -1)
								{
									dblAddedCosts = modUTLien.Statics.arrDemand[lngIndex].Fee + modUTLien.Statics.arrDemand[lngIndex].CertifiedMailFee;
									break;
								}
								else
								{
									dblAddedCosts = 0;
									// dblDemand = 0
								}
								rsTempInfo.MoveNext();
							}
							while (rsTempInfo.EndOfFile() != true);
						}
						else
						{
							lngIndex = -1;
						}
						// MAL@20070911: Get Total Balance Due - If greater then a requested minimum then include it
						if (frmFreeReport.InstancePtr.dblMinimumAmount > 0)
						{
							dblMinimum = frmFreeReport.InstancePtr.dblMinimumAmount;
						}
						else
						{
							dblMinimum = 0;
						}
						if (lngReportType > 0 && Strings.Trim(str30DNQuery) != "")
						{
							// If lngLastAccountKey <> .Fields("AccountKey") Then
							rsCombineBills.OpenRecordset("SELECT * FROM (" + str30DNQuery + ") AS qTmpY WHERE AccountKey = " + rsTempData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
							if (rsCombineBills.RecordCount() > 0)
							{
								boolCombinedBills = true;
								while (!rsCombineBills.EndOfFile())
								{
									if (FCConvert.ToInt32(rsCombineBills.Get_Fields(strWS + "LienRecordNumber")) == 0)
									{
										// regular billing
										// MAL@20080204: Split out tax amount
										// Tracker Reference: 12107
										// dblPrin = Round(dblPrin + (rsCombineBills.Fields(strWS & "PrinOwed") + rsCombineBills.Fields(strWS & "TaxOwed")) - rsCombineBills.Fields(strWS & "PrinPaid"), 2)
										dblPrin = FCUtils.Round(dblPrin + rsCombineBills.Get_Fields(strWS + "PrinOwed") - rsCombineBills.Get_Fields(strWS + "PrinPaid"), 2);
										dblTax = FCUtils.Round(dblTax + rsCombineBills.Get_Fields(strWS + "TaxOwed") - rsCombineBills.Get_Fields(strWS + "TaxPaid"), 2);
										dblPLI += rsCombineBills.Get_Fields(strWS + "IntOwed") + (rsCombineBills.Get_Fields(strWS + "IntAdded") * -1) - rsCombineBills.Get_Fields(strWS + "IntPaid");
										// MAL@20071108
										// dblCosts = dblCosts + rsCombineBills.Fields(strWS & "CostOwed") - rsCombineBills.Fields(strWS & "CostAdded") - rsCombineBills.Fields(strWS & "CostPaid")
										// MAL@20080310: Add check for demand fees not already found
										// Tracker Reference: 12723
										if (dblDemand == 0)
										{
											dblDemand = (rsCombineBills.Get_Fields(strWS + "CostAdded") * -1) - rsCombineBills.Get_Fields(strWS + "CostPaid");
										}
										dblTempCurInt = 0;
										// this will get filled from the calculate account function call
										dblTotal += modUTCalculations.CalculateAccountUT(rsCombineBills, dtMailDate, ref dblTempCurInt, boolWater);
										dblCurrInt += dblTempCurInt;
									}
									else
									{
										// there should be no combined lien records
									}
									rsCombineBills.MoveNext();
								}
								// MAL@20071108
								// dblCosts = dblCosts + dblAddedCosts
								// MAL@20080310: Correct to have Added Costs be in the Demand Fee column
								// Tracker Reference: 12585
								if (lngReportType == 1 && strHeaderTitle == "30 Day Notice Summary")
								{
									dblCosts = 0;
									dblDemand = dblAddedCosts;
								}
								else
								{
									dblCosts = dblAddedCosts;
									dblDemand = dblDemand;
								}
								dblTotal = dblPrin + dblTax + dblCosts + dblPLI + dblCurrInt + dblDemand;
								// MAL@20070913: Added demand fees
							}
							else
							{
								boolCombinedBills = false;
							}
							lngLastAccountKey = FCConvert.ToInt32(rsTempData.Get_Fields_Int32("AccountKey"));
							// Else
							// .MoveNext
							// GoTo TRYAGAIN
							// End If
						}
						else
						{
							if (FCConvert.ToInt32(rsTempData.Get_Fields(strWS + "LienRecordNumber")) == 0)
							{
								// regular billing
								// MAL@20080204: Split out tax amount
								// Tracker Reference: 12107
								// dblPrin = (.Fields(strWS & "PrinOwed") + .Fields(strWS & "TaxOwed")) - .Fields(strWS & "PrinPaid")
								dblPrin = FCConvert.ToDouble(rsTempData.Get_Fields(strWS + "PrinOwed") - rsTempData.Get_Fields(strWS + "PrinPaid"));
								dblTax = FCConvert.ToDouble(rsTempData.Get_Fields(strWS + "TaxOwed") - rsTempData.Get_Fields(strWS + "TaxPaid"));
								dblPLI = FCConvert.ToDouble(rsTempData.Get_Fields(strWS + "IntOwed") + (rsTempData.Get_Fields(strWS + "IntAdded") * -1) - rsTempData.Get_Fields(strWS + "IntPaid"));
								dblCosts = dblAddedCosts;
								// .Fields(strWS & "CostOwed") + dblAddedCosts - .Fields(strWS & "CostAdded") - .Fields(strWS & "CostPaid")
								dblCurrInt = 0;
								// this will get filled from the calculate account function call
								// xx                        dblTotal = CalculateAccountUT(rsData, dtMailDate, dblCurrInt, boolWater) + dblAddedCosts + dblDemand    'MAL@20070918
								dblTotal = modUTCalculations.CalculateAccountUT(rsTempData, dtMailDate, ref dblCurrInt, boolWater) + dblAddedCosts + dblDemand;
								// MAL@20070918
							}
							else
							{
								// MAL@20081112: Check for multiple bill records / rate keys
								// Tracker Reference: 16214
								// If lngLastAccountKey <> .Fields("AccountKey") Then
								rsCombineBills.OpenRecordset("SELECT * FROM (" + str30DNQuery + ") AS qTmpY WHERE AccountKey = " + rsTempData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
								if (rsCombineBills.RecordCount() > 0)
								{
									boolCombinedBills = true;
									while (!rsCombineBills.EndOfFile())
									{
										if (FCConvert.ToInt32(rsCombineBills.Get_Fields(strWS + "LienRecordNumber")) != 0)
										{
											rsLien.MoveFirst();
											rsLien.FindFirstRecord("ID", rsCombineBills.Get_Fields(strWS + "LienRecordNumber"));
											if (!rsLien.NoMatch)
											{
												// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
												dblPrin += FCConvert.ToDouble(rsLien.Get_Fields("Principal") - rsLien.Get_Fields("PrinPaid"));
												// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
												dblTax += FCConvert.ToDouble(rsLien.Get_Fields("Tax") - rsLien.Get_Fields("TaxPaid"));
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												dblPLI += FCConvert.ToDouble(/*Round*/(rsLien.Get_Fields("Interest") + (rsLien.Get_Fields("IntAdded") * -1) - rsLien.Get_Fields("IntPaid") - rsLien.Get_Fields("PLIPaid")));
												dblCosts = dblAddedCosts;
												// + (rsLien.Fields("Costs") - rsLien.Fields("CostPaid"))
												dblCurrBillInt = 0;
												// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
												dblTotal += modUTCalculations.CalculateAccountUTLien(rsLien, dtMailDate, ref dblCurrBillInt, boolWater) - (rsLien.Get_Fields("Costs") - rsLien.Get_Fields_Double("CostPaid"));
												// + dblAddedCosts + dblDemand - (rsLien.Fields("Costs") - rsLien.Fields("CostPaid")) 'MAL@20070918
												dblCurrInt += dblCurrBillInt;
											}
											else
											{
												dblPrin = dblPrin;
												dblTax = dblTax;
												dblPLI = dblPLI;
												dblCosts = dblCosts;
												dblCurrInt = dblCurrInt;
											}
										}
										else
										{
										}
										rsCombineBills.MoveNext();
									}
									// MAL@20071108
									// dblCosts = dblCosts + dblAddedCosts
									// MAL@20080310: Correct to have Added Costs be in the Demand Fee column
									// Tracker Reference: 12585
									if (lngReportType == 1 && strHeaderTitle == "30 Day Notice Summary")
									{
										dblCosts = 0;
										dblDemand = dblAddedCosts;
									}
									else
									{
										// DJW@20091112 subtracted demand costs from total costs
										dblCosts = dblAddedCosts;
										dblDemand = dblDemand;
									}
									if (lngReportType == 0)
									{
										dblTotal = dblPrin + dblTax + dblCosts + dblPLI + dblCurrInt;
									}
									else
									{
										dblTotal = dblPrin + dblTax + dblCosts + dblPLI + dblCurrInt + dblDemand;
										// MAL@20070913: Added demand fees
									}
								}
								else
								{
									boolCombinedBills = false;
								}
								lngLastAccountKey = FCConvert.ToInt32(rsTempData.Get_Fields_Int32("AccountKey"));
								
							}
						}
						dblBalanceDue = dblTotal;
						blnContinue = (dblBalanceDue >= dblMinimum);
						if (!blnContinue)
						{
							lngLastAccountKey = modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey;
							intCurrentIndex += 1;
							goto TRYAGAIN;
						}
						else
						{
							fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
							fldTax.Text = Strings.Format(dblTax, "#,##0.00");
							fldPLInt.Text = Strings.Format(dblPLI + dblCurrInt, "#,##0.00");
							fldCosts.Text = Strings.Format(dblCosts, "#,##0.00");
							fldDemand.Text = Strings.Format(dblDemand, "#,##0.00");
							fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
							dblTotalPrin += dblPrin;
							dblTotalTax += dblTax;
							dblTotalPLI += dblPLI + dblCurrInt;
							dblTotalCost += dblCosts;
							dblTotalDemand += dblDemand;
							dblTotalTotal += dblTotal;
							fldAccount.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(FCConvert.ToInt32(rsTempData.Get_Fields_Int32("AccountKey"))));
							fldAddress.Text = "";
							if (modMain.HasMoreThanOneOwner(rsTempData.Get_Fields_Int32("AccountKey"), "SUMRPT", strSummary))
							{
								modMain.GetLatestOwnerInformation(rsTempData.Get_Fields_Int32("AccountKey"), "SUMRPT", ref strLastOwner, ref strLastOwner2, ref strLastAddress, ref strLastAddress2, ref strLastAddress3, ref strLastCity, ref strLastState, ref strLastZip, strSummary);
							}
							else
							{
								strLastOwner = Strings.Trim(FCConvert.ToString(rsTempData.Get_Fields_String("OName")));
								strLastOwner2 = Strings.Trim(FCConvert.ToString(rsTempData.Get_Fields_String("OName2")));
								strLastAddress = FCConvert.ToString(rsTempData.Get_Fields_String("OAddress1"));
								strLastAddress2 = FCConvert.ToString(rsTempData.Get_Fields_String("OAddress2"));
								strLastAddress3 = FCConvert.ToString(rsTempData.Get_Fields_String("OAddress3"));
								strLastCity = FCConvert.ToString(rsTempData.Get_Fields_String("OCity"));
								strLastState = FCConvert.ToString(rsTempData.Get_Fields_String("OState"));
								if (Strings.Trim(FCConvert.ToString(rsTempData.Get_Fields_String("OZip4"))) != "")
								{
									strLastZip = rsTempData.Get_Fields_String("OZip") + "-" + rsTempData.Get_Fields_String("OZip4");
								}
								else
								{
									strLastZip = FCConvert.ToString(rsTempData.Get_Fields_String("OZip"));
								}
							}

                            OwnerName = Strings.Trim(strLastOwner);
                            if (Strings.Trim(strLastOwner2) != null)
                            {
                                OwnerName = OwnerName + " and " + Strings.Trim(strLastOwner2);
                            }
                            fldName.Text = Strings.Trim(OwnerName);
                            fldAddress.Text = "";

							if (Strings.Trim(strLastAddress) != "")
							{
								fldAddress.Text = fldAddress.Text + Strings.Trim(strLastAddress) + "\r\n";
							}
							if (Strings.Trim(strLastAddress2) != "")
							{
								fldAddress.Text = fldAddress.Text + Strings.Trim(strLastAddress2) + "\r\n";
							}
							if (Strings.Trim(strLastAddress3) != "")
							{
								fldAddress.Text = fldAddress.Text + Strings.Trim(strLastAddress3);
							}
							fldAddress.Text = fldAddress.Text + strLastCity + ", " + strLastState + " " + strLastZip;
							if (Strings.Trim(fldAddress.Text) == "" || boolAddressCO)
							{
								rsUT.FindFirstRecord("ID", rsTempData.Get_Fields_Int32("AccountKey"));
								if (!rsUT.EndOfFile())
								{
									if (rsUT.Get_Fields_Boolean("InBankruptcy") && lngReportType == 2)
									{
										// skip this account because it is in bankruptcy
										rsTempData.MoveNext();
										fldAddress.Text = "";
										fldAccount.Text = "";
										fldPrincipal.Text = "";
										fldTax.Text = "";
										fldPLInt.Text = "";
										fldCosts.Text = "";
										fldDemand.Text = "";
										fldTotal.Text = "";
										fldName.Text = "";
										if (rsTempData.EndOfFile())
											return;
										goto TRYAGAIN;
									}

                                    if (Strings.Trim(fldAddress.Text) == "" || boolAddressCO)
                                    {
                                        if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
                                        {
                                            fldAddress.Text = rsUT.Get_Fields_String("OAddress1") + "\r\n";
                                        }

                                        if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
                                        {
                                            fldAddress.Text =
                                                fldAddress.Text + rsUT.Get_Fields_String("OAddress2") + "\r\n";
                                        }

                                        if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
                                        {
                                            fldAddress.Text =
                                                fldAddress.Text + rsUT.Get_Fields_String("OAddress3") + "\r\n";
                                        }

                                        // MAL@20080325: Tracker Reference: 12766
                                        if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
                                        {
                                            fldAddress.Text =
                                                fldAddress.Text + rsUT.Get_Fields_String("OCity") + ", " +
                                                rsUT.Get_Fields_String("OState") + " " + rsUT.Get_Fields_String("OZip");
                                        }
                                        else
                                        {
                                            fldAddress.Text =
                                                fldAddress.Text + rsUT.Get_Fields_String("OCity") +
                                                rsUT.Get_Fields_String("OState") + rsUT.Get_Fields_String("OZip");
                                        }
                                    }

                                    if (boolAddressCO && Strings.LCase(strLastOwner) !=
                                        Strings.LCase(rsUT.Get_Fields_String("DeedName1")))
                                    {
                                        fldAddress.Text = "C/O " + rsUT.Get_Fields_String("DeedName1") + "\r\n" + fldAddress.Text;
                                    }
                                }
							}
							intCurrentIndex += 1;
						}
					}
					else
					{
						intCurrentIndex += 1;
						goto TRYAGAIN;
					}
				}
				else
				{
					fldAccount.Text = "";
					fldAddress.Text = "";
					fldName.Text = "";
				}
				rsTempInfo.Dispose();
				rsCombineBills.Dispose();
				rsPayment.Dispose();
				rsTempData.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        private int ReturnAccountIndex_2(int lngBK)
		{
			return ReturnAccountIndex(ref lngBK);
		}

		private int ReturnAccountIndex(ref int lngBK)
		{
			int ReturnAccountIndex = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the array index for the account passed in...-1 if not found
				int lngCT;
				bool boolFound = false;
				for (lngCT = 0; lngCT <= Information.UBound(modUTLien.Statics.arrDemand, 1); lngCT++)
				{
					if (modUTLien.Statics.arrDemand[lngCT].BillKey == lngBK)
					{
						boolFound = true;
						break;
					}
				}
				if (boolFound)
				{
					ReturnAccountIndex = lngCT;
				}
				else
				{
					ReturnAccountIndex = -1;
				}
				return ReturnAccountIndex;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Array Index", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ReturnAccountIndex;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// fill in the totals
			fldTotalPrincipal.Text = Strings.Format(dblTotalPrin, "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTotalTax, "#,##0.00");
			fldTotalPLInt.Text = Strings.Format(dblTotalPLI, "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblTotalCost, "#,##0.00");
			// fldTotalCurrentInt.Text = Format(dblTotalCurrentInt, "#,##0.00")
			fldTotalTotal.Text = Strings.Format(dblTotalTotal, "#,##0.00");
			fldTotalDemand.Text = Strings.Format(dblTotalDemand, "#,##0.00");
		}

		private void RptLienNoticeSummary_Disposed(object sender, System.EventArgs e)
		{
			if (frmFreeReport.InstancePtr != null)
			{
				frmFreeReport.InstancePtr.Unload();
			}
		}

		private void rptLienNoticeSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLienNoticeSummary properties;
			//rptLienNoticeSummary.Caption	= "Account Detail";
			//rptLienNoticeSummary.Icon	= "rptLienNoticeSummary.dsx":0000";
			//rptLienNoticeSummary.Left	= 0;
			//rptLienNoticeSummary.Top	= 0;
			//rptLienNoticeSummary.Width	= 11880;
			//rptLienNoticeSummary.Height	= 8595;
			//rptLienNoticeSummary.StartUpPosition	= 3;
			//rptLienNoticeSummary.SectionData	= "rptLienNoticeSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
