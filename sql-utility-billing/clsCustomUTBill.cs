﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	public class clsCustomUTBill
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// DATE           :               01/20/2005              *
		// *
		// MODIFIED BY    :               Corey Gray              *
		// LAST UPDATED   :               06/03/2005              *
		// ********************************************************
		// This is set up so that you can have as many classes
		// as you want per module.  You do not have to handle
		// all custom bill/reports the same way
		// You can have multiple classes, or just one class
		// that alters its behaviour based on a variable
		// that determines which type you are doing.
		// The properties and functions are set up to be used
		// like recordsets from the report.  They do not have
		// to actually work this way.  EndOfFile could return
		// clsreport.endoffile, or it could return whether an
		// index is pointing past the end of an array or grid
		// Any functions or variables can be changed or added
		// as long as the interface that the report expects
		// is unchanged. I.E. add as much as you want or change an existing function that
		// the report calls, but don't rename or delete it.
		private string strSQLReport = string.Empty;
		private clsDRWrapper clsReport = new clsDRWrapper();
		private int lngCurrentFormatID;
		private string strDisplayTitle = string.Empty;
		private string strPrinterName = string.Empty;
		private string strThisModule = string.Empty;
		private string strThisDB = string.Empty;
		private string strDataDB = string.Empty;
		private clsDRWrapper clsCustomCodes = new clsDRWrapper();
		private int lngVerticalAlignment;
		private bool boolUsePrinterFonts;
		private bool boolDotMatrix;
		private bool boolWaterFormat;
		private bool boolSewerFormat;
		private int intBill;
		// keeps track of which bill it is on
		// when combined bill print 2 bills this tells you whether you have
		// printed both yet or not
		private bool boolSplitBill;
		// if a combined format, tells whether to print this bill as one or two
		private bool boolSupressZero;
		// if true then don't show bills with 0 or negative amounts
		public bool SkipZeroNegativeBills
		{
			set
			{
				boolSupressZero = value;
			}
			get
			{
				bool SkipZeroNegativeBills = false;
				SkipZeroNegativeBills = boolSupressZero;
				return SkipZeroNegativeBills;
			}
		}

		public bool UseWaterBills
		{
			set
			{
				boolWaterFormat = value;
			}
			get
			{
				bool UseWaterBills = false;
				UseWaterBills = boolWaterFormat;
				return UseWaterBills;
			}
		}

		public bool UseSewerBills
		{
			set
			{
				boolSewerFormat = value;
			}
			get
			{
				bool UseSewerBills = false;
				UseSewerBills = boolSewerFormat;
				return UseSewerBills;
			}
		}

		public bool DotMatrixFormat
		{
			set
			{
				boolDotMatrix = value;
			}
			get
			{
				bool DotMatrixFormat = false;
				DotMatrixFormat = boolDotMatrix;
				return DotMatrixFormat;
			}
		}

		public bool UsePrinterFonts
		{
			set
			{
				boolUsePrinterFonts = value;
			}
			get
			{
				bool UsePrinterFonts = false;
				UsePrinterFonts = boolUsePrinterFonts;
				return UsePrinterFonts;
			}
		}

		public int VerticalAlignment
		{
			set
			{
				// adjust up or down by lngtwips
				lngVerticalAlignment = value;
			}
			get
			{
				int VerticalAlignment = 0;
				VerticalAlignment = lngVerticalAlignment;
				return VerticalAlignment;
			}
		}

		public string Module
		{
			set
			{
				// I.E. BL or UT etc.
				strThisModule = value;
			}
			get
			{
				string Module = "";
				Module = strThisModule;
				return Module;
			}
		}

		public string DBFile
		{
			set
			{
				// The filename.  Usually TW[Mod abbreviation]0000.vb1
				strThisDB = value;
			}
			get
			{
				string DBFile = "";
				DBFile = strThisDB;
				return DBFile;
			}
		}

		public string DataDBFile
		{
			set
			{
				// The filename of the db that has the data
				strDataDB = value;
			}
			get
			{
				string DataDBFile = "";
				if (strDataDB != string.Empty)
				{
					DataDBFile = strDataDB;
				}
				else
				{
					DataDBFile = strThisDB;
				}
				return DataDBFile;
			}
		}

		public string SQL
		{
			set
			{
				// The sql statement.  This is not accessed directly
				// by the report since the programmer might use an array of types or a grid
				// instead of a recordset
				strSQLReport = value;
			}
			get
			{
				string SQL = "";
				SQL = strSQLReport;
				return SQL;
			}
		}

		public int FormatID
		{
			set
			{
				// The autoid of the report
				// Should be set before the report is run
				lngCurrentFormatID = value;
			}
			get
			{
				int FormatID = 0;
				FormatID = lngCurrentFormatID;
				return FormatID;
			}
		}

		public string ReportTitle
		{
			set
			{
				// What you want displayed if the report is previewed
				strDisplayTitle = value;
			}
			get
			{
				string ReportTitle = "";
				ReportTitle = strDisplayTitle;
				return ReportTitle;
			}
		}

		public string PrinterName
		{
			set
			{
				// If you need a specific printer and don't want the user to
				// be able to choose one, fill this in before calling the report
				strPrinterName = value;
			}
			get
			{
				string PrinterName = "";
				PrinterName = strPrinterName;
				return PrinterName;
			}
		}

		public bool EndOfFile
		{
			get
			{
				bool EndOfFile = false;
				// end of file, or more generally, end of the data
				EndOfFile = clsReport.EndOfFile();
				return EndOfFile;
			}
		}

		public bool BeginningOfFile
		{
			get
			{
				bool BeginningOfFile = false;
				BeginningOfFile = clsReport.BeginningOfFile();
				return BeginningOfFile;
			}
		}

		public void MoveNext()
		{
			MoveAgain:
			;
			clsReport.MoveNext();
			if (!clsReport.EndOfFile())
			{
				if (this.UseWaterBills && this.UseSewerBills)
				{
					// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
					if (Conversion.Val(clsReport.Get_Fields("printbilltype")) == modCoreysUTCode.CNSTUTPRINTBILLTYPECOMBINED)
					{
						modCoreysUTCode.SetBillTypeForPrinting_8(true, true);
					}
					// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
					else if (Conversion.Val(clsReport.Get_Fields("printbilltype")) == modCoreysUTCode.CNSTUTPRINTBILLTYPESEWER)
					{
						modCoreysUTCode.SetBillTypeForPrinting_8(false, true);
					}
						// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
						else if (Conversion.Val(clsReport.Get_Fields("printbilltype")) == modCoreysUTCode.CNSTUTPRINTBILLTYPEWATER)
					{
						modCoreysUTCode.SetBillTypeForPrinting_8(true, false);
					}
				}
				if (SkipZeroNegativeBills)
				{
					// Dim dblPrin As Double
					// Dim dblTax As Double
					// Dim dblCost As Double
					double dblWTotal = 0;
					double dblSTotal = 0;
					// dblPrin = 0
					// dblTax = 0
					// dblCost = 0
					// If Me.UseWaterBills Then
					// dblPrin = Val(clsReport.Fields("wprinowed")) - Val(clsReport.Fields("wprinpaid"))
					// dblTax = Val(clsReport.Fields("wtaxowed")) - Val(clsReport.Fields("wtaxpaid"))
					// dblCost = Val(clsReport.Fields("wcostowed")) - Val(clsReport.Fields("wcostadded")) - Val(clsReport.Fields("wcostpaid"))
					// End If
					// If Me.UseSewerBills Then
					// dblPrin = dblPrin + (Val(clsReport.Fields("sprinowed")) - Val(clsReport.Fields("sprinpaid")))
					// dblTax = dblTax + (Val(clsReport.Fields("staxowed")) - Val(clsReport.Fields("staxpaid")))
					// dblCost = dblCost + (Val(clsReport.Fields("scostowed")) - Val(clsReport.Fields("scostadded")) - Val(clsReport.Fields("scostpaid")))
					// End If
					// 
					// If dblPrin + dblTax + dblCost <= 0 Then
					// GoTo MoveAgain
					// End If
					if (this.UseWaterBills)
					{
						dblWTotal = modUTCalculations.CalculateAccountUTTotal(clsReport.Get_Fields_Int32("AccountKey"), true);
					}
					else
					{
						dblWTotal = 0;
					}
					if (this.UseSewerBills)
					{
						dblSTotal = modUTCalculations.CalculateAccountUTTotal(clsReport.Get_Fields_Int32("AccountKey"), false);
					}
					else
					{
						dblSTotal = 0;
					}
					if (dblSTotal + dblWTotal <= 0)
					{
						goto MoveAgain;
					}
				}
			}
			// If Not clsReport.EndOfFile Then
			// Dim dblTotal As Double
			// Dim dblCurInt As Double
			// Dim dblTotCharInt As Double
			// 
			// dblTotal = 0
			// Do While dblTotal = 0 And Not clsReport.EndOfFile
			// dblTotal = 0
			// 
			// If Me.UseWaterBills Then
			// dblTotal = dblTotal + CalculateAccountUTTotal(clsReport.Fields("accountkey"), True, gboolShowLienAmountOnBill, dblCurInt, dblTotCharInt)
			// End If
			// If Me.UseSewerBills Then
			// dblTotal = dblTotal + CalculateAccountUTTotal(clsReport.Fields("accountkey"), False, gboolShowLienAmountOnBill, dblCurInt, dblTotCharInt)
			// End If
			// 
			// If dblTotal = 0 Then
			// Me.MoveNext
			// End If
			// Loop
			// End If
		}

		public void MoveLast()
		{
			clsReport.MoveLast();
		}

		public void MovePrevious()
		{
			// most likely not used
			clsReport.MovePrevious();
		}

		public void MoveFirst()
		{
			clsReport.MoveFirst();
			bool boolSkip;
			boolSkip = true;
			while (boolSkip)
			{
				boolSkip = false;
				if (!clsReport.EndOfFile())
				{
					if (this.UseWaterBills && this.UseSewerBills)
					{
						// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
						if (Conversion.Val((clsReport.Get_Fields("printbilltype"))) == modCoreysUTCode.CNSTUTPRINTBILLTYPECOMBINED)
						{
							modCoreysUTCode.SetBillTypeForPrinting_8(true, true);
						}
						// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
						else if (Conversion.Val((clsReport.Get_Fields("printbilltype"))) == modCoreysUTCode.CNSTUTPRINTBILLTYPESEWER)
						{
							modCoreysUTCode.SetBillTypeForPrinting_8(false, true);
						}
							// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
							else if (Conversion.Val((clsReport.Get_Fields("printbilltype"))) == modCoreysUTCode.CNSTUTPRINTBILLTYPEWATER)
						{
							modCoreysUTCode.SetBillTypeForPrinting_8(true, false);
						}
					}
					if (SkipZeroNegativeBills)
					{
						// Dim dblPrin As Double
						// Dim dblTax As Double
						// Dim dblCost As Double
						double dblWTotal = 0;
						double dblSTotal = 0;
						// dblPrin = 0
						// dblTax = 0
						// dblCost = 0
						// If Me.UseWaterBills Then
						// dblPrin = Val(clsReport.Fields("wprinowed")) - Val(clsReport.Fields("wprinpaid"))
						// dblTax = Val(clsReport.Fields("wtaxowed")) - Val(clsReport.Fields("wtaxpaid"))
						// dblCost = Val(clsReport.Fields("wcostowed")) - Val(clsReport.Fields("wcostadded")) - Val(clsReport.Fields("wcostpaid"))
						// End If
						// If Me.UseSewerBills Then
						// dblPrin = dblPrin + (Val(clsReport.Fields("sprinowed")) - Val(clsReport.Fields("sprinpaid")))
						// dblTax = dblTax + (Val(clsReport.Fields("staxowed")) - Val(clsReport.Fields("staxpaid")))
						// dblCost = dblCost + (Val(clsReport.Fields("scostowed")) - Val(clsReport.Fields("scostadded")) - Val(clsReport.Fields("scostpaid")))
						// End If
						// 
						// If dblPrin + dblTax + dblCost <= 0 Then
						// clsReport.MoveNext
						// boolSkip = True
						// End If
						if (this.UseWaterBills)
						{
							dblWTotal = modUTCalculations.CalculateAccountUTTotal(clsReport.Get_Fields_Int32("AccountKey"), true);
						}
						else
						{
							dblWTotal = 0;
						}
						if (this.UseSewerBills)
						{
							dblSTotal = modUTCalculations.CalculateAccountUTTotal(clsReport.Get_Fields_Int32("AccountKey"), false);
						}
						else
						{
							dblSTotal = 0;
						}
						if (dblSTotal + dblWTotal <= 0)
						{
							clsReport.MoveNext();
							boolSkip = true;
						}
					}
				}
			}
		}

		public bool LoadData()
		{
			bool LoadData = false;
			// this function returns false if it fails
			// It loads the data.  In the example class, this loads a recordset
			// the function calls all assume a recordset, but the programmer can have movenext
			// add 1 to an index to an array.  The class controls how the data is loaded and read
			// The report will use it like a recordset, but the implementation is irrelevant
			double dblTotal;
			double dblCurInt;
			double dblTotCharInt;
			try
			{
				// On Error GoTo ErrorHandler
				LoadData = false;
				// init a variable
				modCoreysUTCode.GetReadingUnitsOnBill();
				// load the data
				clsReport.OpenRecordset(strSQLReport, DataDBFile);
				// load the codes
				clsCustomCodes.OpenRecordset("select * from custombillcodes order by id", strThisDB);
				// set the flags in the module since it can't access this class
				modCoreysUTCode.SetBillTypeForPrinting_8(UseWaterBills, UseSewerBills);
				// if combined, set the first one up
				if (!clsReport.EndOfFile())
				{
					if (UseWaterBills && UseSewerBills)
					{
						// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
						if (Conversion.Val(clsReport.Get_Fields("printbilltype")) == modCoreysUTCode.CNSTUTPRINTBILLTYPECOMBINED)
						{
							modCoreysUTCode.SetBillTypeForPrinting_8(true, true);
						}
						// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
						else if (Conversion.Val(clsReport.Get_Fields("printbilltype")) == modCoreysUTCode.CNSTUTPRINTBILLTYPESEWER)
						{
							modCoreysUTCode.SetBillTypeForPrinting_8(false, true);
						}
							// TODO Get_Fields: Field [printbilltype] not found!! (maybe it is an alias?)
							else if (Conversion.Val(clsReport.Get_Fields("printbilltype")) == modCoreysUTCode.CNSTUTPRINTBILLTYPEWATER)
						{
							modCoreysUTCode.SetBillTypeForPrinting_8(true, false);
						}
					}
				}
				// If Not clsReport.EndOfFile Then
				// dblTotal = 0
				// 
				// If Me.UseWaterBills Then
				// dblTotal = dblTotal + CalculateAccountUTTotal(clsReport.Fields("accountkey"), True, gboolShowLienAmountOnBill, dblCurInt, dblTotCharInt)
				// End If
				// If Me.UseSewerBills Then
				// dblTotal = dblTotal + CalculateAccountUTTotal(clsReport.Fields("accountkey"), False, gboolShowLienAmountOnBill, dblCurInt, dblTotCharInt)
				// End If
				// 
				// If dblTotal <= 0 Then
				// Me.MoveNext
				// End If
				// End If
				LoadData = true;
				return LoadData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In clsCustomBill.LoadData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadData;
		}

		public string EmailTag()
		{
			string EmailTag = "";
			int nTemp = clsReport.Get_Fields_Int32("AccountKey");
			EmailTag = modUTStatusPayments.GetAccountEmail(ref nTemp);
			return EmailTag;
		}

		public string GetDataByCode(int lngCode, string strExtraParameters)
		{
			string GetDataByCode = "";
			// the code corresponds to the data in the custombillcodes table
			bool boolSpecialCase = false;
			GetDataByCode = "";
			modCustomBill.CustomBillCodeType CurCode = new modCustomBill.CustomBillCodeType();
			if (clsCustomCodes.FindFirstRecord("ID", lngCode))
			{
				boolSpecialCase = FCConvert.ToBoolean(clsCustomCodes.Get_Fields_Boolean("SpecialCase"));
				// TODO Get_Fields: Check the table for the column [Category] and replace with corresponding Get_Field method
				CurCode.Category = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCustomCodes.Get_Fields("Category"))));
				CurCode.Datatype = FCConvert.ToInt32(Math.Round(Conversion.Val(clsCustomCodes.Get_Fields_Int16("datatype"))));
				CurCode.DBName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("dbname"));
				CurCode.FieldID = lngCode;
				CurCode.FieldName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("FieldName"));
				CurCode.FormatString = FCConvert.ToString(clsCustomCodes.Get_Fields_String("FormatString"));
				CurCode.SpecialCase = boolSpecialCase;
				CurCode.TableName = FCConvert.ToString(clsCustomCodes.Get_Fields_String("tablename"));
				if (boolSpecialCase == true)
				{
					// it is a special case and can't be simply loaded straight from the database
					GetDataByCode = modCoreysUTCode.HandleSpecialCase(ref clsReport, ref CurCode, ref strExtraParameters);
				}
				else
				{
					// is a simple code that just takes the data
					GetDataByCode = modCoreysUTCode.HandleRegularCase(ref clsReport, ref CurCode);
				}
			}
			else if (lngCode < 0)
			{
				CurCode.Category = 0;
				CurCode.Datatype = 0;
				CurCode.DBName = "";
				CurCode.FieldID = lngCode;
				CurCode.FieldName = "";
				CurCode.FormatString = "";
				CurCode.SpecialCase = true;
				CurCode.TableName = "";
				GetDataByCode = modCoreysUTCode.HandleSpecialCase(ref clsReport, ref CurCode, ref strExtraParameters);
			}
			return GetDataByCode;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		public int CentimetersToTwips(ref double dblCentimeters)
		{
			int CentimetersToTwips = 0;
			// returns twips
			CentimetersToTwips = FCConvert.ToInt32(dblCentimeters * 567);
			return CentimetersToTwips;
		}
		// vbPorter upgrade warning: 'Return' As int	OnWriteFCConvert.ToDouble(
		public int InchesToTwips(ref double dblInches)
		{
			int InchesToTwips = 0;
			// returns twips
			InchesToTwips = FCConvert.ToInt32(dblInches * 1440);
			return InchesToTwips;
		}

		public double TwipsToInches(ref int lngTwips)
		{
			double TwipsToInches = 0;
			TwipsToInches = lngTwips / 1440.0;
			return TwipsToInches;
		}

		public double TwipsToCentimeters(ref int lngTwips)
		{
			double TwipsToCentimeters = 0;
			TwipsToCentimeters = lngTwips / 567.0;
			return TwipsToCentimeters;
		}

		public clsCustomUTBill() : base()
		{
			boolSupressZero = false;
		}
	}
}
