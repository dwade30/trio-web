//Fecher vbPorter - Version 1.0.0.90
using System;
using Wisej.Web;
using Microsoft.VisualBasic;

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmAcctAutoPay.
	/// </summary>
	public partial class frmAcctAutoPay
	{
		public fecherFoundation.FCCheckBox chkPreNote;
		public fecherFoundation.FCCheckBox chkActive;
		public fecherFoundation.FCTextBox txtLimit;
		public fecherFoundation.FCTextBox txtAccountNumber;
		public fecherFoundation.FCTextBox txtTransitRouting;
		public fecherFoundation.FCLabel lblLimit;
		public fecherFoundation.FCLabel lblAccountNumber;
		public fecherFoundation.FCLabel lblTransitRouting;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuSep;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public frmAcctAutoPay()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmAcctAutoPay InstancePtr
		{
			get
			{
				return (frmAcctAutoPay)Sys.GetInstance(typeof(frmAcctAutoPay));
			}
		}
		protected static frmAcctAutoPay _InstancePtr = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.chkPreNote = new fecherFoundation.FCCheckBox();
            this.chkActive = new fecherFoundation.FCCheckBox();
            this.txtLimit = new fecherFoundation.FCTextBox();
            this.txtAccountNumber = new fecherFoundation.FCTextBox();
            this.txtTransitRouting = new fecherFoundation.FCTextBox();
            this.lblLimit = new fecherFoundation.FCLabel();
            this.lblAccountNumber = new fecherFoundation.FCLabel();
            this.lblTransitRouting = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSep = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmbAccountType = new fecherFoundation.FCComboBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPreNote)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmbAccountType);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.chkActive);
            this.ClientArea.Controls.Add(this.chkPreNote);
            this.ClientArea.Controls.Add(this.txtLimit);
            this.ClientArea.Controls.Add(this.txtAccountNumber);
            this.ClientArea.Controls.Add(this.txtTransitRouting);
            this.ClientArea.Controls.Add(this.lblAccountNumber);
            this.ClientArea.Controls.Add(this.lblLimit);
            this.ClientArea.Controls.Add(this.lblTransitRouting);
            this.ClientArea.Controls.Add(this.cmdSave);
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = false;
            this.HeaderText.Size = new System.Drawing.Size(450, 30);
            this.HeaderText.Text = "Configure Auto-Pay for Account";
            // 
            // chkPreNote
            // 
            this.chkPreNote.Location = new System.Drawing.Point(99, 27);
            this.chkPreNote.Name = "chkPreNote";
            this.chkPreNote.Size = new System.Drawing.Size(82, 24);
            this.chkPreNote.TabIndex = 2;
            this.chkPreNote.Text = "Pre-Note";
            this.chkPreNote.CheckedChanged += new System.EventHandler(this.chkPreNote_CheckedChanged);
            // 
            // chkActive
            // 
            this.chkActive.Location = new System.Drawing.Point(16, 27);
            this.chkActive.Name = "chkActive";
            this.chkActive.Size = new System.Drawing.Size(66, 24);
            this.chkActive.TabIndex = 1;
            this.chkActive.Text = "Active";
            this.chkActive.CheckedChanged += new System.EventHandler(this.chkActive_CheckedChanged);
            // 
            // txtLimit
            // 
            this.txtLimit.Location = new System.Drawing.Point(161, 228);
            this.txtLimit.Name = "txtLimit";
            this.txtLimit.Size = new System.Drawing.Size(90, 24);
            this.txtLimit.TabIndex = 6;
            this.txtLimit.TextChanged += new System.EventHandler(this.txtLimit_TextChanged);
            this.txtLimit.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLimit_KeyPress);
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.Location = new System.Drawing.Point(161, 131);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Size = new System.Drawing.Size(163, 24);
            this.txtAccountNumber.TabIndex = 4;
            this.txtAccountNumber.TextChanged += new System.EventHandler(this.txtAccountNumber_TextChanged);
            // 
            // txtTransitRouting
            // 
            this.txtTransitRouting.Location = new System.Drawing.Point(161, 80);
            this.txtTransitRouting.Name = "txtTransitRouting";
            this.txtTransitRouting.Size = new System.Drawing.Size(163, 24);
            this.txtTransitRouting.TabIndex = 3;
            this.txtTransitRouting.TextChanged += new System.EventHandler(this.txtTransitRouting_TextChanged);
            this.txtTransitRouting.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTransitRouting_KeyPress);
            // 
            // lblLimit
            // 
            this.lblLimit.Location = new System.Drawing.Point(16, 233);
            this.lblLimit.Name = "lblLimit";
            this.lblLimit.Size = new System.Drawing.Size(114, 19);
            this.lblLimit.TabIndex = 4;
            this.lblLimit.Text = "PAYMENT LIMIT";
            // 
            // lblAccountNumber
            // 
            this.lblAccountNumber.Location = new System.Drawing.Point(16, 131);
            this.lblAccountNumber.Name = "lblAccountNumber";
            this.lblAccountNumber.Size = new System.Drawing.Size(129, 19);
            this.lblAccountNumber.TabIndex = 2;
            this.lblAccountNumber.Text = "ACCOUNT NUMBER";
            // 
            // lblTransitRouting
            // 
            this.lblTransitRouting.Location = new System.Drawing.Point(16, 83);
            this.lblTransitRouting.Name = "lblTransitRouting";
            this.lblTransitRouting.Size = new System.Drawing.Size(129, 19);
            this.lblTransitRouting.TabIndex = 12;
            this.lblTransitRouting.Text = "ROUTING NUMBER";
            // 
            // MainMenu1
            // 
            this.MainMenu1.Name = null;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessSave,
            this.mnuSep,
            this.mnuProcessExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 0;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // mnuSep
            // 
            this.mnuSep.Index = 1;
            this.mnuSep.Name = "mnuSep";
            this.mnuSep.Text = "-";
            // 
            // mnuProcessExit
            // 
            this.mnuProcessExit.Index = 2;
            this.mnuProcessExit.Name = "mnuProcessExit";
            this.mnuProcessExit.Text = "Exit    (Esc)";
            this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(20, 300);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(140, 48);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmbAccountType
            // 
            this.cmbAccountType.Items.AddRange(new object[] {
            "Checking",
            "Savings"});
            this.cmbAccountType.Location = new System.Drawing.Point(161, 181);
            this.cmbAccountType.Name = "cmbAccountType";
            this.cmbAccountType.Size = new System.Drawing.Size(163, 22);
            this.cmbAccountType.TabIndex = 5;
            // 
            // fcLabel1
            // 
            this.fcLabel1.Location = new System.Drawing.Point(16, 185);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(106, 19);
            this.fcLabel1.TabIndex = 14;
            this.fcLabel1.Text = "ACCOUNT TYPE";
            // 
            // frmAcctAutoPay
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Menu = this.MainMenu1;
            this.Name = "frmAcctAutoPay";
            this.Text = "Configure Auto-Pay for Account";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.frmAcctAutoPay_Unload);
            this.Load += new System.EventHandler(this.frmAcctAutoPay_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAcctAutoPay_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkPreNote)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkActive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
        #endregion

        private FCButton cmdSave;
        public FCComboBox cmbAccountType;
        public FCLabel fcLabel1;
    }
}
