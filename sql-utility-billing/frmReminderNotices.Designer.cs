﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using fecherFoundation.VisualBasicLayer;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReminderNotices.
	/// </summary>
	partial class frmReminderNotices : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCComboBox cmbRN;
		public fecherFoundation.FCLabel lblRN;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkSendTo;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtPastDue;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraNOPastDue;
		public fecherFoundation.FCTextBox txtNoPastDue;
		public fecherFoundation.FCLabel lblNoPastDue;
		public fecherFoundation.FCPanel fraGrid;
		public fecherFoundation.FCTextBox txtReportTitle;
		public fecherFoundation.FCFrame fraSendTo;
		public fecherFoundation.FCCheckBox chkSendTo_1;
		public fecherFoundation.FCCheckBox chkSendTo_0;
		public fecherFoundation.FCFrame fraMinAmount;
		public fecherFoundation.FCTextBox txtMinimumAmount;
		public fecherFoundation.FCLabel lblMinAmount;
		public fecherFoundation.FCFrame fraRN;
		public FCGrid vsReturnAddress;
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCFrame fraType;
		public fecherFoundation.FCCheckBox chkPastDueOnly;
		public fecherFoundation.FCCheckBox chkCombineService;
		public fecherFoundation.FCCheckBox chkBulkMailing;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCLabel lblInstruction;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.cmbOrder = new fecherFoundation.FCComboBox();
			this.lblOrder = new fecherFoundation.FCLabel();
			this.cmbRN = new fecherFoundation.FCComboBox();
			this.lblRN = new fecherFoundation.FCLabel();
			this.fraMessage = new fecherFoundation.FCFrame();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.txtPastDue = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.fraNOPastDue = new fecherFoundation.FCFrame();
			this.txtNoPastDue = new fecherFoundation.FCTextBox();
			this.lblNoPastDue = new fecherFoundation.FCLabel();
			this.fraGrid = new fecherFoundation.FCPanel();
			this.txtReportTitle = new fecherFoundation.FCTextBox();
			this.fraSendTo = new fecherFoundation.FCFrame();
			this.chkSendTo_1 = new fecherFoundation.FCCheckBox();
			this.chkSendTo_0 = new fecherFoundation.FCCheckBox();
			this.fraMinAmount = new fecherFoundation.FCFrame();
			this.txtMinimumAmount = new fecherFoundation.FCTextBox();
			this.lblMinAmount = new fecherFoundation.FCLabel();
			this.fraRN = new fecherFoundation.FCFrame();
			this.vsReturnAddress = new fecherFoundation.FCGrid();
			this.vsGrid = new fecherFoundation.FCGrid();
			this.fraType = new fecherFoundation.FCFrame();
			this.chkPastDueOnly = new fecherFoundation.FCCheckBox();
			this.chkCombineService = new fecherFoundation.FCCheckBox();
			this.chkBulkMailing = new fecherFoundation.FCCheckBox();
			this.lblTitle = new fecherFoundation.FCLabel();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
			this.fraMessage.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraNOPastDue)).BeginInit();
			this.fraNOPastDue.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).BeginInit();
			this.fraGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSendTo)).BeginInit();
			this.fraSendTo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSendTo_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSendTo_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMinAmount)).BeginInit();
			this.fraMinAmount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraRN)).BeginInit();
			this.fraRN.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsReturnAddress)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
			this.fraType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPastDueOnly)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCombineService)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBulkMailing)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 692);
			this.BottomPanel.Size = new System.Drawing.Size(800, 108);
			this.ToolTip1.SetToolTip(this.BottomPanel, null);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraMessage);
			this.ClientArea.Controls.Add(this.fraGrid);
			this.ClientArea.Size = new System.Drawing.Size(800, 632);
			this.ToolTip1.SetToolTip(this.ClientArea, null);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(800, 60);
			this.ToolTip1.SetToolTip(this.TopPanel, null);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(208, 30);
			this.HeaderText.Text = "Reminder Notices";
			this.ToolTip1.SetToolTip(this.HeaderText, null);
			// 
			// cmbOrder
			// 
			this.cmbOrder.AutoSize = false;
			this.cmbOrder.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOrder.FormattingEnabled = true;
			this.cmbOrder.Items.AddRange(new object[] {
				"Account",
				"Book, Sequence"
			});
			this.cmbOrder.Location = new System.Drawing.Point(117, 30);
			this.cmbOrder.Name = "cmbOrder";
			this.cmbOrder.Size = new System.Drawing.Size(214, 40);
			this.cmbOrder.TabIndex = 3;
			this.cmbOrder.Text = "Book, Sequence";
			this.ToolTip1.SetToolTip(this.cmbOrder, null);
			// 
			// lblOrder
			// 
			this.lblOrder.AutoSize = true;
			this.lblOrder.Location = new System.Drawing.Point(30, 44);
			this.lblOrder.Name = "lblOrder";
			this.lblOrder.Size = new System.Drawing.Size(52, 15);
			this.lblOrder.TabIndex = 2;
			this.lblOrder.Text = "ORDER";
			this.ToolTip1.SetToolTip(this.lblOrder, null);
			// 
			// cmbRN
			// 
			this.cmbRN.AutoSize = false;
			this.cmbRN.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRN.FormattingEnabled = true;
			this.cmbRN.Items.AddRange(new object[] {
				"Labels",
				"Post Cards",
				"Forms",
				"Mailer",
				//"Outprinting File"
			});
			this.cmbRN.Location = new System.Drawing.Point(93, 30);
			this.cmbRN.Name = "cmbRN";
			this.cmbRN.Size = new System.Drawing.Size(237, 40);
			this.cmbRN.TabIndex = 1;
			this.cmbRN.Text = "Labels";
			this.ToolTip1.SetToolTip(this.cmbRN, null);
			this.cmbRN.SelectedIndexChanged += new System.EventHandler(this.optRN_CheckedChanged);
			// 
			// lblRN
			// 
			this.lblRN.AutoSize = true;
			this.lblRN.Location = new System.Drawing.Point(20, 44);
			this.lblRN.Name = "lblRN";
			this.lblRN.Size = new System.Drawing.Size(56, 15);
			this.lblRN.TabIndex = 0;
			this.lblRN.Text = "SELECT";
			this.ToolTip1.SetToolTip(this.lblRN, null);
			// 
			// fraMessage
			// 
			this.fraMessage.AppearanceKey = "groupBoxLeftBorder";
			this.fraMessage.Controls.Add(this.Frame1);
			this.fraMessage.Controls.Add(this.fraNOPastDue);
			this.fraMessage.Location = new System.Drawing.Point(736, 30);
			this.fraMessage.Name = "fraMessage";
			this.fraMessage.Size = new System.Drawing.Size(638, 447);
			this.fraMessage.TabIndex = 0;
			this.fraMessage.Text = "Form Messages";
			this.ToolTip1.SetToolTip(this.fraMessage, null);
			this.fraMessage.Visible = false;
			// 
			// Frame1
			// 
			this.Frame1.Controls.Add(this.txtPastDue);
			this.Frame1.Controls.Add(this.Label1);
			this.Frame1.Location = new System.Drawing.Point(20, 237);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(591, 179);
			this.Frame1.TabIndex = 1;
			this.Frame1.Text = "Past Due Message";
			this.ToolTip1.SetToolTip(this.Frame1, null);
			// 
			// txtPastDue
			// 
			this.txtPastDue.AutoSize = false;
			this.txtPastDue.BackColor = System.Drawing.SystemColors.Window;
			this.txtPastDue.LinkItem = null;
			this.txtPastDue.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPastDue.LinkTopic = null;
			this.txtPastDue.Location = new System.Drawing.Point(20, 90);
			this.txtPastDue.Name = "txtPastDue";
			this.txtPastDue.Size = new System.Drawing.Size(555, 40);
			this.txtPastDue.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtPastDue, null);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 30);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(555, 35);
			this.Label1.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.Label1, null);
			// 
			// fraNOPastDue
			// 
			this.fraNOPastDue.Controls.Add(this.txtNoPastDue);
			this.fraNOPastDue.Controls.Add(this.lblNoPastDue);
			this.fraNOPastDue.Location = new System.Drawing.Point(20, 30);
			this.fraNOPastDue.Name = "fraNOPastDue";
			this.fraNOPastDue.Size = new System.Drawing.Size(591, 179);
			this.fraNOPastDue.TabIndex = 0;
			this.fraNOPastDue.Text = "No Past Due Message";
			this.ToolTip1.SetToolTip(this.fraNOPastDue, null);
			// 
			// txtNoPastDue
			// 
			this.txtNoPastDue.AutoSize = false;
			this.txtNoPastDue.BackColor = System.Drawing.SystemColors.Window;
			this.txtNoPastDue.LinkItem = null;
			this.txtNoPastDue.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNoPastDue.LinkTopic = null;
			this.txtNoPastDue.Location = new System.Drawing.Point(20, 90);
			this.txtNoPastDue.Name = "txtNoPastDue";
			this.txtNoPastDue.Size = new System.Drawing.Size(555, 40);
			this.txtNoPastDue.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.txtNoPastDue, null);
			// 
			// lblNoPastDue
			// 
			this.lblNoPastDue.Location = new System.Drawing.Point(20, 30);
			this.lblNoPastDue.Name = "lblNoPastDue";
			this.lblNoPastDue.Size = new System.Drawing.Size(555, 35);
			this.lblNoPastDue.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lblNoPastDue, null);
			// 
			// fraGrid
			// 
			this.fraGrid.AppearanceKey = "groupBoxNoBorders";
			this.fraGrid.Controls.Add(this.txtReportTitle);
			this.fraGrid.Controls.Add(this.cmbOrder);
			this.fraGrid.Controls.Add(this.fraSendTo);
			this.fraGrid.Controls.Add(this.lblOrder);
			this.fraGrid.Controls.Add(this.fraMinAmount);
			this.fraGrid.Controls.Add(this.fraRN);
			this.fraGrid.Controls.Add(this.fraType);
			this.fraGrid.Controls.Add(this.lblTitle);
			this.fraGrid.Controls.Add(this.lblInstruction);
			this.fraGrid.Location = new System.Drawing.Point(0, 0);
			this.fraGrid.Name = "fraGrid";
			this.fraGrid.Size = new System.Drawing.Size(708, 974);
			this.fraGrid.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.fraGrid, null);
			this.fraGrid.Visible = false;
			// 
			// txtReportTitle
			// 
			this.txtReportTitle.AutoSize = false;
			this.txtReportTitle.BackColor = System.Drawing.SystemColors.Window;
			this.txtReportTitle.LinkItem = null;
			this.txtReportTitle.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtReportTitle.LinkTopic = null;
			this.txtReportTitle.Location = new System.Drawing.Point(198, 372);
			this.txtReportTitle.Name = "txtReportTitle";
			this.txtReportTitle.Size = new System.Drawing.Size(490, 40);
			this.txtReportTitle.TabIndex = 7;
			this.ToolTip1.SetToolTip(this.txtReportTitle, null);
			// 
			// fraSendTo
			// 
			this.fraSendTo.Controls.Add(this.chkSendTo_1);
			this.fraSendTo.Controls.Add(this.chkSendTo_0);
			this.fraSendTo.Location = new System.Drawing.Point(405, 228);
			this.fraSendTo.Name = "fraSendTo";
			this.fraSendTo.Size = new System.Drawing.Size(303, 124);
			this.fraSendTo.TabIndex = 5;
			this.fraSendTo.Text = "Send To";
			this.ToolTip1.SetToolTip(this.fraSendTo, null);
			// 
			// chkSendTo_1
			// 
			this.chkSendTo_1.Location = new System.Drawing.Point(20, 77);
			this.chkSendTo_1.Name = "chkSendTo_1";
			this.chkSendTo_1.Size = new System.Drawing.Size(159, 27);
			this.chkSendTo_1.TabIndex = 1;
			this.chkSendTo_1.Text = "Owner(if different)";
			this.ToolTip1.SetToolTip(this.chkSendTo_1, null);
			this.chkSendTo_1.CheckedChanged += new System.EventHandler(this.chkSendTo_CheckedChanged);
			// 
			// chkSendTo_0
			// 
			this.chkSendTo_0.Checked = true;
			this.chkSendTo_0.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkSendTo_0.Location = new System.Drawing.Point(20, 30);
			this.chkSendTo_0.Name = "chkSendTo_0";
			this.chkSendTo_0.Size = new System.Drawing.Size(78, 27);
			this.chkSendTo_0.TabIndex = 0;
			this.chkSendTo_0.Text = "Tenant";
			this.ToolTip1.SetToolTip(this.chkSendTo_0, null);
			this.chkSendTo_0.CheckedChanged += new System.EventHandler(this.chkSendTo_CheckedChanged);
			// 
			// fraMinAmount
			// 
			this.fraMinAmount.Controls.Add(this.txtMinimumAmount);
			this.fraMinAmount.Controls.Add(this.lblMinAmount);
			this.fraMinAmount.Location = new System.Drawing.Point(405, 90);
			this.fraMinAmount.Name = "fraMinAmount";
			this.fraMinAmount.Size = new System.Drawing.Size(303, 118);
			this.fraMinAmount.TabIndex = 4;
			this.fraMinAmount.Text = "Min Amount";
			this.ToolTip1.SetToolTip(this.fraMinAmount, null);
			// 
			// txtMinimumAmount
			// 
			this.txtMinimumAmount.AutoSize = false;
			this.txtMinimumAmount.BackColor = System.Drawing.SystemColors.Window;
			this.txtMinimumAmount.LinkItem = null;
			this.txtMinimumAmount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMinimumAmount.LinkTopic = null;
			this.txtMinimumAmount.Location = new System.Drawing.Point(20, 58);
			this.txtMinimumAmount.Name = "txtMinimumAmount";
			this.txtMinimumAmount.Size = new System.Drawing.Size(127, 40);
			this.txtMinimumAmount.TabIndex = 1;
			this.txtMinimumAmount.Text = "0.00";
			this.txtMinimumAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			this.ToolTip1.SetToolTip(this.txtMinimumAmount, "Minimum principal due in order to process.");
			this.txtMinimumAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMinimumAmount_KeyPress);
			this.txtMinimumAmount.Enter += new System.EventHandler(this.txtMinimumAmount_Enter);
			this.txtMinimumAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtMinimumAmount_Validating);
			// 
			// lblMinAmount
			// 
			this.lblMinAmount.Location = new System.Drawing.Point(20, 30);
			this.lblMinAmount.Name = "lblMinAmount";
			this.lblMinAmount.Size = new System.Drawing.Size(263, 18);
			this.lblMinAmount.TabIndex = 0;
			this.lblMinAmount.Text = "MINIMUM PRINCIPAL AMOUNT TO PROCESS";
			this.ToolTip1.SetToolTip(this.lblMinAmount, null);
			// 
			// fraRN
			// 
			this.fraRN.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left)));
			this.fraRN.Controls.Add(this.vsReturnAddress);
			this.fraRN.Controls.Add(this.vsGrid);
			this.fraRN.Location = new System.Drawing.Point(30, 432);
			this.fraRN.Name = "fraRN";
			this.fraRN.Size = new System.Drawing.Size(678, 515);
			this.fraRN.TabIndex = 8;
			this.fraRN.Text = "Selection Criteria";
			this.ToolTip1.SetToolTip(this.fraRN, null);
			// 
			// vsReturnAddress
			// 
			this.vsReturnAddress.AllowSelection = false;
			this.vsReturnAddress.AllowUserToResizeColumns = false;
			this.vsReturnAddress.AllowUserToResizeRows = false;
			this.vsReturnAddress.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsReturnAddress.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsReturnAddress.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsReturnAddress.BackColorBkg = System.Drawing.Color.Empty;
			this.vsReturnAddress.BackColorFixed = System.Drawing.Color.Empty;
			this.vsReturnAddress.BackColorSel = System.Drawing.Color.Empty;
			this.vsReturnAddress.Cols = 1;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsReturnAddress.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsReturnAddress.ColumnHeadersHeight = 30;
			this.vsReturnAddress.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsReturnAddress.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsReturnAddress.DragIcon = null;
			this.vsReturnAddress.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsReturnAddress.ExtendLastCol = true;
			this.vsReturnAddress.FixedCols = 0;
			this.vsReturnAddress.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsReturnAddress.FrozenCols = 0;
			this.vsReturnAddress.GridColor = System.Drawing.Color.Empty;
			this.vsReturnAddress.GridColorFixed = System.Drawing.Color.Empty;
			this.vsReturnAddress.Location = new System.Drawing.Point(20, 224);
			this.vsReturnAddress.Name = "vsReturnAddress";
			this.vsReturnAddress.ReadOnly = true;
			this.vsReturnAddress.RowHeadersVisible = false;
			this.vsReturnAddress.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsReturnAddress.RowHeightMin = 0;
			this.vsReturnAddress.Rows = 5;
			this.vsReturnAddress.ScrollTipText = null;
			this.vsReturnAddress.ShowColumnVisibilityMenu = false;
			this.vsReturnAddress.Size = new System.Drawing.Size(638, 271);
			this.vsReturnAddress.StandardTab = true;
			this.vsReturnAddress.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsReturnAddress.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsReturnAddress.TabIndex = 1;
			this.ToolTip1.SetToolTip(this.vsReturnAddress, null);
			this.vsReturnAddress.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsReturnAddress_BeforeEdit);
			this.vsReturnAddress.CurrentCellChanged += new System.EventHandler(this.vsReturnAddress_RowColChange);
			this.vsReturnAddress.Click += new System.EventHandler(this.vsReturnAddress_ClickEvent);
			// 
			// vsGrid
			// 
			this.vsGrid.AllowSelection = false;
			this.vsGrid.AllowUserToResizeColumns = false;
			this.vsGrid.AllowUserToResizeRows = false;
			this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrid.Cols = 5;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsGrid.ColumnHeadersHeight = 30;
			this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsGrid.DragIcon = null;
			this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrid.ExtendLastCol = true;
			this.vsGrid.FixedCols = 0;
			this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.FrozenCols = 0;
			this.vsGrid.GridColor = System.Drawing.Color.Empty;
			this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.Location = new System.Drawing.Point(20, 30);
			this.vsGrid.Name = "vsGrid";
			this.vsGrid.ReadOnly = true;
			this.vsGrid.RowHeadersVisible = false;
			this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrid.RowHeightMin = 0;
			this.vsGrid.Rows = 1;
			this.vsGrid.ScrollTipText = null;
			this.vsGrid.ShowColumnVisibilityMenu = false;
			this.vsGrid.Size = new System.Drawing.Size(638, 183);
			this.vsGrid.StandardTab = true;
			this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsGrid.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.vsGrid, null);
			this.vsGrid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsGrid_KeyDownEdit);
			this.vsGrid.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsGrid_ChangeEdit);
			this.vsGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsGrid_BeforeEdit);
			this.vsGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsGrid_AfterEdit);
			this.vsGrid.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsGrid_ValidateEdit);
			this.vsGrid.CurrentCellChanged += new System.EventHandler(this.vsGrid_RowColChange);
			this.vsGrid.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsGrid_MouseMoveEvent);
			this.vsGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.vsGrid_KeyDownEvent);
			// 
			// fraType
			// 
			this.fraType.Controls.Add(this.chkPastDueOnly);
			this.fraType.Controls.Add(this.cmbRN);
			this.fraType.Controls.Add(this.lblRN);
			this.fraType.Controls.Add(this.chkCombineService);
			this.fraType.Controls.Add(this.chkBulkMailing);
			this.fraType.Location = new System.Drawing.Point(30, 90);
			this.fraType.Name = "fraType";
			this.fraType.Size = new System.Drawing.Size(350, 262);
			this.fraType.TabIndex = 1;
			this.fraType.Text = "Type";
			this.ToolTip1.SetToolTip(this.fraType, null);
			// 
			// chkPastDueOnly
			// 
			this.chkPastDueOnly.Location = new System.Drawing.Point(20, 184);
			this.chkPastDueOnly.Name = "chkPastDueOnly";
			this.chkPastDueOnly.Size = new System.Drawing.Size(206, 27);
			this.chkPastDueOnly.TabIndex = 4;
			this.chkPastDueOnly.Text = "Calculate Past Due Only";
			this.ToolTip1.SetToolTip(this.chkPastDueOnly, "Do not calculate bills newer than the one selected.");
			// 
			// chkCombineService
			// 
			this.chkCombineService.Location = new System.Drawing.Point(20, 137);
			this.chkCombineService.Name = "chkCombineService";
			this.chkCombineService.Size = new System.Drawing.Size(128, 27);
			this.chkCombineService.TabIndex = 3;
			this.chkCombineService.Text = "Combine W/S";
			this.ToolTip1.SetToolTip(this.chkCombineService, "Combine Water and Sewer totals.");
			this.chkCombineService.CheckedChanged += new System.EventHandler(this.chkCombineService_CheckedChanged);
			// 
			// chkBulkMailing
			// 
			this.chkBulkMailing.Location = new System.Drawing.Point(20, 90);
			this.chkBulkMailing.Name = "chkBulkMailing";
			this.chkBulkMailing.Size = new System.Drawing.Size(186, 27);
			this.chkBulkMailing.TabIndex = 2;
			this.chkBulkMailing.Text = "Print Bulk Mailing Info";
			this.ToolTip1.SetToolTip(this.chkBulkMailing, null);
			// 
			// lblTitle
			// 
			this.lblTitle.Location = new System.Drawing.Point(30, 386);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(130, 18);
			this.lblTitle.TabIndex = 6;
			this.lblTitle.Text = "ENTER REPORT TITLE";
			this.ToolTip1.SetToolTip(this.lblTitle, null);
			// 
			// lblInstruction
			// 
			this.lblInstruction.Location = new System.Drawing.Point(30, 30);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(350, 69);
			this.lblInstruction.TabIndex = 0;
			this.ToolTip1.SetToolTip(this.lblInstruction, null);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePrint.Text = "Save & Preview";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.AppearanceKey = "acceptButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(297, 37);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePrint.Size = new System.Drawing.Size(167, 48);
			this.cmdFilePrint.TabIndex = 0;
			this.cmdFilePrint.Text = "Save & Preview";
			this.ToolTip1.SetToolTip(this.cmdFilePrint, null);
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmReminderNotices
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = null;
			this.ClientSize = new System.Drawing.Size(800, 800);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmReminderNotices";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reminder Notices";
			this.ToolTip1.SetToolTip(this, null);
			this.Load += new System.EventHandler(this.frmReminderNotices_Load);
			this.Activated += new System.EventHandler(this.frmReminderNotices_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReminderNotices_KeyPress);
			this.Resize += new System.EventHandler(this.frmReminderNotices_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
			this.fraMessage.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraNOPastDue)).EndInit();
			this.fraNOPastDue.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).EndInit();
			this.fraGrid.ResumeLayout(false);
			this.fraGrid.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSendTo)).EndInit();
			this.fraSendTo.ResumeLayout(false);
			this.fraSendTo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSendTo_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSendTo_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMinAmount)).EndInit();
			this.fraMinAmount.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraRN)).EndInit();
			this.fraRN.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsReturnAddress)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
			this.fraType.ResumeLayout(false);
			this.fraType.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkPastDueOnly)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCombineService)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBulkMailing)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFilePrint;
	}
}
