﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arLienMaturity.
	/// </summary>
	public partial class arLienMaturity : BaseSectionReport
	{
		public arLienMaturity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Maturity";
		}

		public static arLienMaturity InstancePtr
		{
			get
			{
				return (arLienMaturity)Sys.GetInstance(typeof(arLienMaturity));
			}
		}

		protected arLienMaturity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                if (rsData != null)
					rsData.Dispose();
                if (rsCMFNumbers != null)
					rsCMFNumbers.Dispose();
                if (rsUT != null)
					rsUT.Dispose();
                if (rsRate != null)
					rsRate.Dispose();
                if (rsBook != null)
					rsBook.Dispose();
                if (rsBills != null)
					rsBills.Dispose();
                if (rsCert != null)
					rsCert.Dispose();
                if (rsMort != null)
					rsMort.Dispose();
                if (rsRE != null)
					rsRE.Dispose();
                if (rsPayments != null)
					rsPayments.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arLienMaturity	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/28/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsCMFNumbers = new clsDRWrapper();
		string strSQL;
		string strText = "";
		// vbPorter upgrade warning: intYear As short --> As int	OnRead(string)
		short intYear;
		bool boolPayCert;
		bool boolMort;
		bool boolPayMortCert;
		bool boolPayNewOwner;
		bool boolAddressCO;
		clsDRWrapper rsCert;
		int lngCopies;
		// this is to keep track of the number of mortgage holder copies left
		bool boolCopy;
		// is this a copy or the original
		int intNewOwnerChoice;
		// this is if the user wants to send to any new owners 0 = Do not send any, 1 =
		clsDRWrapper rsMort = new clsDRWrapper();
		clsDRWrapper rsRE = new clsDRWrapper();
		clsDRWrapper rsUT = new clsDRWrapper();
		clsDRWrapper rsRate = new clsDRWrapper();
		clsDRWrapper rsPayments = new clsDRWrapper();
		bool boolNewOwner;
		// this is true if there is a new owner
		bool boolAtLeastOneRecord;
		// this is true if there is at least one record to see
		string strAcctList = "";
		string strReportDesc = "";
		// this will be the string that will describe the parameters the user set for the report ie. Account 1 to 100
		clsDRWrapper rsBook = new clsDRWrapper();
		int lngArrayIndex;
		string strDateCheck = "";
		DateTime dtInterestDate;
		bool boolWater;
		string strWS = "";
		string strRKList = "";
		bool blnMultiBills;
		clsDRWrapper rsBills = new clsDRWrapper();
		bool blnCombine;
		// these variables are for counting the accounts
		public int lngBalanceZero;
		public int lngBalanceNeg;
		public int lngBalancePos;
		public int lngDemandFeesApplied;
		string strBalanceZero = "";
		string strBalanceNeg = "";
		string strBalancePos = "";
		string strDemandFeesApplied = "";
		int lngCMFBillKey;
		string strAddressPrefix = "";
		string str30DNQuery = "";
		int intCurrentIndex;
		string strQryFlds;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// EOF = rsData.EndOfFile
				eArgs.EOF = intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1);
				if (eArgs.EOF)
				{
					EndReport();
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Fetch Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void EndReport()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will set all of the bill records to status of 1
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 1, FCConvert.ToString(lngBalanceZero));
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 1, FCConvert.ToString(lngBalanceNeg));
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 1, FCConvert.ToString(lngBalancePos));
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 1, FCConvert.ToString(lngBalanceZero + lngBalanceNeg + lngBalancePos));
				// this will take the trailing comma off
				if (strBalanceZero.Length > 0)
				{
					strBalanceZero = Strings.Left(strBalanceZero, strBalanceZero.Length - 1);
				}
				if (strBalanceNeg.Length > 0)
				{
					strBalanceNeg = Strings.Left(strBalanceNeg, strBalanceNeg.Length - 1);
				}
				if (strBalancePos.Length > 0)
				{
					strBalancePos = Strings.Left(strBalancePos, strBalancePos.Length - 1);
				}
				if (strDemandFeesApplied.Length > 0)
				{
					strDemandFeesApplied = Strings.Left(strDemandFeesApplied, strDemandFeesApplied.Length - 1);
				}
				// set the strings of accounts for the return grid to show
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 2, strBalanceZero);
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 2, strBalanceNeg);
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 2, strBalancePos);
				frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 2, strDemandFeesApplied);
				if (lngDemandFeesApplied > 0)
				{
					// show a special label that is highlighted to show this information
					// frmFreeReport.vsSummary.AddItem ""
					// frmFreeReport.vsSummary.AddItem ""
					if ((((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70) < (frmFreeReport.InstancePtr.fraSummary.HeightOriginal - 200))
					{
						frmFreeReport.InstancePtr.vsSummary.HeightOriginal = ((frmFreeReport.InstancePtr.vsSummary.Rows - 1 ) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70 ;
					}
					else
					{
						frmFreeReport.InstancePtr.vsSummary.HeightOriginal = frmFreeReport.InstancePtr.fraSummary.HeightOriginal - 200;
					}
				}
				frmWait.InstancePtr.Unload();
				frmFreeReport.InstancePtr.Focus();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Report End", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modUTBilling.AcctInfo[] typAccountInfo = null;  // - "AutoDim"
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCounter = 0;
				int intDefSel = 0;
				string strTemp = "";
				boolWater = frmFreeReport.InstancePtr.boolWater;
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				intCurrentIndex = 0;
				FCUtils.EraseSafe(modUTLien.Statics.arrDemand);
				frmWait.InstancePtr.Hide();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT", "LienMaturityPaperSize", ref strTemp);
				if (strTemp == "Legal")
				{
					intDefSel = 1;
				}
				else
				{
					intDefSel = 0;
				}
				frmQuestion.InstancePtr.Init(10, "Letter (8.5 x 11)", "Legal (8.5 x 14)", "Select Paper Size", intDefSel);
				if (modUTBilling.Statics.gintPassQuestion == 0)
				{
					strTemp = "Letter";
				}
				else
				{
					strTemp = "Legal";
				}
				modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT", "LienMaturityPaperSize", strTemp);
				if (modUTBilling.Statics.gintPassQuestion == 0)
				{
					FCGlobal.Printer.PaperSize = 1;
					// Letter size
				}
				else
				{
					FCGlobal.Printer.PaperSize = 5;
					// Legal size
				}
				frmQuestion.InstancePtr.Unload();
				//Application.DoEvents();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				frmWait.InstancePtr.Show();
				// kk02042016 trout-1197  1.5" top 1st page, 1.5" bottom last page, 0.75" left and right margins
				this.PageSettings.Margins.Top = FCConvert.ToSingle((1.5F) + (modMain.Statics.gdblLienAdjustmentTop));
				this.PageSettings.Margins.Bottom = FCConvert.ToSingle((1.5F) + (modMain.Statics.gdblLienAdjustmentBottom));
				// kk04212014 trocl-1156
				this.PageSettings.Margins.Left = FCConvert.ToSingle((0.75F) + (modMain.Statics.gdblLienAdjustmentSide));
				this.PageSettings.Margins.Right = this.PageSettings.Margins.Left;
				this.PrintWidth = (8.5F) - (this.PageSettings.Margins.Left + this.PageSettings.Margins.Right);
				rtbText.Width = this.PrintWidth;
				lblDate.Left = 0;
				lblDate.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
				this.Hide();
				//.Visible = false; // this will have to be set back to true to be shown
				SetHeaderFooter(true);
				lngBalanceNeg = 0;
				lngBalancePos = 0;
				lngBalanceZero = 0;
				lngDemandFeesApplied = 0;
				boolPayNewOwner = false;
				boolPayCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1), 2) == "Ye");
				boolMort = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1), 2) == "Ye");
				boolAddressCO = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1), 8) == "Owner at");
				// MAL@20080423: Check which address will need to print if user adds the Address fields to the notice
				// Tracker Reference:
				if (boolAddressCO)
				{
					strAddressPrefix = "O";
				}
				else
				{
					strAddressPrefix = "B";
				}
				if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 2) == "Ye")
				{
					if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7) == "Yes, ch")
					{
						intNewOwnerChoice = 2;
						boolPayNewOwner = true;
					}
					else
					{
						intNewOwnerChoice = 1;
					}
				}
				else
				{
					intNewOwnerChoice = 0;
				}
				boolNewOwner = false;
				if (boolMort)
				{
					boolPayMortCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1), 9) == "Yes, char");
				}
				else
				{
					boolPayMortCert = false;
				}
				// this will add a picture of the signature of the treasurer to the 30 Day Notice
				if (modMain.Statics.gboolUseSigFile)
				{
					imgSig.Visible = true;
					imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrUtilitySigPath);
				}
				else
				{
					imgSig.Visible = false;
				}
				if (modGlobalConstants.Statics.gboolRE)
				{
					rsRE.OpenRecordset("SELECT * FROM MASTER", modExtraModules.strREDatabase);
				}
				rsUT.OpenRecordset("SELECT Master.ID, Apt, StreetNumber, StreetName, MapLot, UseREAccount, REAccount, OwnerPartyID, InBankruptcy, AccountNumber, "
				                   + "DeedName1 AS OwnerName, DeedName2 AS SecondOwnerName, " + "pOwn.Address1 AS OAddress1, pOwn.Address2 AS OAddress2, pOwn.Address3 AS OAddress3, " + "pOwn.City AS OCity, pOwn.State AS OState, pOwn.Zip AS OZip " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID " + "LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID");
				rsRate.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
				strSQL = SetupSQL();
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (rsData.EndOfFile() != true)
				{
					// lngMaxNumber = rsData.RecordCount
					rsCMFNumbers.OpenRecordset("SELECT * FROM CMFNumbers", modExtraModules.strUTDatabase);
					// lblDate.Caption = PadToString(rsData.Fields("Account"), 6)    'this will show the account number in the top right of the page
					modUTBilling.Statics.typAccountInfo = new modUTBilling.AcctInfo[0 + 1];
					modUTBilling.Statics.typAccountInfo[0].AccountKey = rsData.Get_Fields_Int32("AccountKey");
					if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "LIENMAT"))
					{
						modMain.GetLatestOwnerInformation_59022(rsData.Get_Fields_Int32("AccountKey"), "LIENMAT", ref modUTBilling.Statics.typAccountInfo[0].AccountName, "", "", "", "", "", "", "");
					}
					else
					{
						modUTBilling.Statics.typAccountInfo[0].AccountName = rsData.Get_Fields_String("OName");
					}
					intCounter = 1;
					rsData.MoveNext();
					while (rsData.EndOfFile() != true)
					{
						if (modUTBilling.Statics.typAccountInfo[intCounter - 1].AccountKey != rsData.Get_Fields_Int32("AccountKey"))
						{
							Array.Resize(ref modUTBilling.Statics.typAccountInfo, intCounter + 1);
							modUTBilling.Statics.typAccountInfo[intCounter].AccountKey = rsData.Get_Fields_Int32("AccountKey");
							if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "LIENMAT"))
							{
								modMain.GetLatestOwnerInformation_59022(rsData.Get_Fields_Int32("AccountKey"), "LIENMAT", ref modUTBilling.Statics.typAccountInfo[intCounter].AccountName, "", "", "", "", "", "", "");
							}
							else
							{
								modUTBilling.Statics.typAccountInfo[intCounter].AccountName = rsData.Get_Fields_String("OName");
							}
							intCounter += 1;
						}
						rsData.MoveNext();
					}
					rsData.MoveFirst();
					if (frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account")
					{
						// do nothing the data is already in account number order
					}
					else
					{
						modUTBilling.strSort_24(modUTBilling.Statics.typAccountInfo, true, true);
					}
					modGlobalConstants.Statics.blnHasRecords = true;
				}
				else
				{
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					MessageBox.Show("No eligible accounts found.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Cancel();
					modGlobalConstants.Statics.blnHasRecords = false;
					return;
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, rsData.RecordCount());
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			try
			{
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                Int32 tempRepType = 1;
                rptLienNoticeSummary.InstancePtr.Init(ref strBalancePos, "Lien Maturity Notice Summary", strReportDesc, ref intYear, ref dtInterestDate, ref boolWater, FCConvert.CBool(frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account"), 0, ref str30DNQuery, ref boolAddressCO);
                return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Report Terminate", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngAccountKey = 0;
				if (intCurrentIndex <= Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
				{
					lngAccountKey = modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey;
					rsBills.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrLienMaturityQuery + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
					if (rsBills.RecordCount() > 1)
					{
						blnCombine = true;
					}
					else
					{
						blnCombine = false;
					}
					// calculate fields
					CalculateVariableTotals();
					if (intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
						return;
					frmWait.InstancePtr.IncrementProgress();
					SetHeaderFooter();
                    // this will set the main string
                    rtbText.Font = new Font("Courier New", rtbText.Font.Size);
                    rtbText.SetHtmlText(SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text));
					rtbText.SelectionStart = 0;
					rtbText.SelectionLength = rtbText.Text.Length;
					if (modMain.Statics.gboolUseSigFile)
					{
						// kk02052016 trout-1197  Rework lien notice. Change the way signature line is located.
						// The adjustments are applied before the richtext section is expanded, so we have to limit the adjustment amount.
						// If the signature moves into the rtbText area or above it, the signature will not be pushed down when the rtbText is expanded.
						if (modMain.Statics.gdblLienSigAdjust < 0)
						{
							// The digital signature starts out directly below the rich text box
							imgSig.Top = rtbText.Top + rtbText.Height;
							// Don't allow it to go any higher
						}
						else
						{
							if (imgSig.Top + (modMain.Statics.gdblLienSigAdjust * 1440) < Line2.Y1)
							{
								// The farthest it can move is so the top is even with the signature line
								imgSig.Top += FCConvert.ToSingle((modMain.Statics.gdblLienSigAdjust));
							}
							else
							{
								imgSig.Top = Line2.Y1;
							}
						}
					}
					if (lngCopies > 1)
					{
						// this is to produce multiple copies for certain parties (mortgage holders)
						lngCopies -= 1;
						boolCopy = true;
					}
					else
					{
						// rsData.MoveNext
						intCurrentIndex += 1;
						boolCopy = false;
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetHeaderFooter(bool boolFirstRun = false)
		{
			string strTemp = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will set the header values
				if (boolCopy)
				{
					strTemp = " * * * COPY * * *  NOTICE OF IMPENDING AUTOMATIC FORECLOSURE  * * * COPY * * * ";
				}
				else
				{
					strTemp = "NOTICE OF IMPENDING AUTOMATIC FORECLOSURE";
				}
				if (boolWater)
				{
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						// kk06232015 trouts-154 / kjr 11.16.16 trouts-208
						strTemp += "\r\n" + "STORMWATER LIEN";
					}
					else
					{
						strTemp += "\r\n" + "WATER LIEN";
					}
				}
				else
				{
					strTemp += "\r\n" + "SEWER LIEN";
				}
				lblReportHeader.Text = "STATE OF MAINE" + "\r\n" + GetVariableValue_2("UTILITYTITLE") + "\r\n" + strTemp + "\r\n" + GetVariableValue_2("LEGALDESCRIPTION");
				lblReportHeader.WordWrap = false;
				// kjr 11.16.16 trouts-208
				if (boolFirstRun)
				{
					if (boolWater)
					{
						arLienMaturity.InstancePtr.Name = "Water Lien Maturity Notice";
					}
					else
					{
						arLienMaturity.InstancePtr.Name = "Sewer Lien Maturity Notice";
					}
				}
				else
				{
					// fldCollector.Text = GetVariableValue("COLLECTOR")
					fldCollector.Text = GetVariableValue_2("SIGNER");
					fldTitle.Text = GetVariableValue_2("JOP");
					fldMuni.Text = GetVariableValue_2("MUNI");
					fldCounty.Text = "County of " + GetVariableValue_2("COUNTY");
					fldPrincipal.Text = Strings.Format(GetVariableValue_2("PRINCIPAL"), "#,##0.00");
					fldTax.Text = Strings.Format(GetVariableValue_2("TAX"), "#,##0.00");
					fldInterest.Text = Strings.Format(GetVariableValue_2("INTEREST"), "#,##0.00");
					fldDemand.Text = Strings.Format(GetVariableValue_2("DEMAND"), "#,##0.00");
					fldFilingFee.Text = Strings.Format(GetVariableValue_2("FORECLOSUREFEE"), "#,##0.00");
					fldCertMailFee.Text = Strings.Format(GetVariableValue_2("CERTTOTAL"), "#,##0.00");
					if (Strings.Trim(fldPrincipal.Text) == "")
					{
						fldPrincipal.Text = "0.00";
					}
					if (Strings.Trim(fldTax.Text) == "")
					{
						fldTax.Text = "0.00";
					}
					if (Strings.Trim(fldInterest.Text) == "")
					{
						fldInterest.Text = "0.00";
					}
					if (Strings.Trim(fldDemand.Text) == "")
					{
						fldDemand.Text = "0.00";
					}
					if (Strings.Trim(fldCertMailFee.Text) == "")
					{
						fldCertMailFee.Text = "0.00";
					}
					if (Strings.Trim(FCConvert.ToString(FCConvert.ToDouble(fldFilingFee.Text))) == "")
					{
						fldFilingFee.Text = "0.00";
					}
					fldTotal.Text = Strings.Format(FCConvert.ToDouble(fldPrincipal.Text) + FCConvert.ToDouble(fldTax.Text) + FCConvert.ToDouble(fldInterest.Text) + FCConvert.ToDouble(fldDemand.Text) + FCConvert.ToDouble(fldCertMailFee.Text) + FCConvert.ToDouble(fldFilingFee.Text), "#,##0.00");
					if (modUTLien.Statics.gboolUseMailDateForMaturity)
					{
						fldBottomMessage.Text = "Amount due as of " + GetVariableValue_2("MAILDATE") + ".  Please call us at " + GetVariableValue_2("COLLECTORPHONE") + " for the amount due on any desired payment date.";
					}
					else
					{
						fldBottomMessage.Text = "Amount due as of " + GetVariableValue_2("FORECLOSUREDATE") + ".  Please contact us at " + GetVariableValue_2("COLLECTORPHONE") + " for the amount due on any desired payment date.";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Setting Up Header/Footer", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string SetupSQL()
		{
			string SetupSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the SQL statement for this batch of reports
				string strWhereClause;
				int intCT;
				string strOrderBy = "";
				string strRK = "";
				strWhereClause = "";
				strReportDesc = "";
				if (frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account")
				{
					strOrderBy = " ORDER BY ActualAccountNumber, OName";
				}
				else
				{
					strOrderBy = " ORDER BY OName, ActualAccountNumber";
				}
				if (Strings.Trim(frmFreeReport.InstancePtr.strRateKeyList) != "")
				{
					strRKList = " AND BillingRateKey IN " + frmFreeReport.InstancePtr.strRateKeyList;
				}
				else
				{
					strRKList = "";
				}
				if (Strings.Trim(frmFreeReport.InstancePtr.strBookList) != "")
				{
					strRKList = " AND (" + frmFreeReport.InstancePtr.strBookList + ")";
				}
				else
				{
					strRKList = "";
				}
				// this will select the correct ratekeys
				for (intCT = 1; intCT <= frmRateRecChoice.InstancePtr.vsRate.Rows - 1; intCT++)
				{
					// strWhereClause = " AND (LienRec.RateKey = " & frmRateRecChoice.lngRateRecNumber & " OR BillingMaster.RateKey = " & frmRateRecChoice.lngRateRecNumber & ")"
					if (Conversion.Val(frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, 0)) == -1)
					{
						if (Strings.Trim(strWhereClause) == "")
						{
							strWhereClause = " AND (";
							strWhereClause += "Lien.RateKey = " + frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, frmRateRecChoice.InstancePtr.lngHiddenCol) + " OR Bill.BillingRateKey = " + frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, frmRateRecChoice.InstancePtr.lngHiddenCol);
						}
						else
						{
							strWhereClause += " OR Lien.RateKey = " + frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, frmRateRecChoice.InstancePtr.lngHiddenCol) + " OR Bill.BillingRateKey = " + frmRateRecChoice.InstancePtr.vsRate.TextMatrix(intCT, frmRateRecChoice.InstancePtr.lngHiddenCol);
						}
					}
				}
				if (Strings.Trim(strWhereClause) != "")
				{
					strWhereClause += ")";
				}
				if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Account")
				{
					// range of accounts
					// strOrderBy = " ORDER BY ActualAccountNumber, OName"
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							strWhereClause += " AND ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
							strReportDesc = "ActualAccountNumber " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause = " AND ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
							strReportDesc = "ActualAccountNumber Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause += " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
							strReportDesc = "ActualAccountNumber Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
					}
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
				{
					// range of names
					// strOrderBy = " ActualAccountNumber, ORDER BY OName"
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause += " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "' AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
							strReportDesc = "Name " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause += " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "'";
							strReportDesc = "Name Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause += " AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "'";
							strReportDesc = "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
				}
				else
				{
					// strOrderBy = " ORDER BY ActualAccountNumber, OName"
				}
				// create the string for the minimum amount
				// If frmFreeReport.dblMinimumAmount > 0 Then
				strWhereClause += " AND (Principal - Lien.PrinPaid) > " + FCConvert.ToString(frmFreeReport.InstancePtr.dblMinimumAmount);
				// End If
				// kk 01242013 trout-920  Don't print notice if maturity fee is already applied
				strWhereClause += " AND MaturityFee = 0";
				strQryFlds = "Bill.ID AS Bill, BillingRateKey, BillDate, AccountKey, ActualAccountNumber, Service, BName, OName, OAddress1, OAddress2, OAddress3, OCity, OState, OZip, OZip4, MapLot, SLienRecordNumber, WLienRecordNumber, SCombinationLienKey, WCombinationLienKey, Lien.ID AS LienKey, Lien.*";
				modMain.Statics.gstrLienMaturityQuery = "SELECT " + strQryFlds + " FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE NOT (LienProcessExclusion = 1) AND (" + strWS + "LienStatusEligibility = 4 OR " + strWS + "LienStatusEligibility= 5) " + strWhereClause + strRKList;
				// kgk   & strOrderBy
				str30DNQuery = "SELECT " + strQryFlds + " FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE " + strWS + "CombinationLienKey = Bill.ID AND BillStatus = 'B' AND NOT (LienProcessExclusion = 1) AND (" + strWS + "LienStatusEligibility = 5 OR " + strWS + "LienStatusEligibility = 6 OR " + strWS + "LienStatusEligibility = 4)" + strWhereClause + strRKList;
				// kgk       & " " & strOrderBy
				SetupSQL = "SELECT * FROM (" + modMain.Statics.gstrLienMaturityQuery + ") AS qTmp ORDER BY AccountKey";
				// MAL@20070918
				// SetupSQL = "SELECT DISTINCT AccountKey FROM LienMaturityQuery"
				return SetupSQL;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Setup SQL", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SetupSQL;
		}

		private string SetupVariablesInString(string strOriginal)
		{
			string SetupVariablesInString = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will replace all of the variables with the information needed by recursively
				// calling this function to replace one variable each time
				string strBuildString;
				// this is the string that will be built and returned at the end
				string strTemp = "";
				// this is a temporary sting that will be used to store and transfer string segments
				int lngNextVariable;
				// this is the position of the beginning of the next variable (0 = no more variables)
				int lngEndOfLastVariable;
				// this is the position of the end of the last variable
				lngEndOfLastVariable = 0;
				lngNextVariable = 1;
				strBuildString = "";
				// priming read
				if (Strings.InStr(1, strOriginal, "<", CompareConstants.vbBinaryCompare) > 0)
				{
					lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<", CompareConstants.vbBinaryCompare);
					// do until there are no more variables left
					do
					{
						// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
						strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
						// set the end pointer
						lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">", CompareConstants.vbBinaryCompare);
						// replace the variable
						strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
						// check for another variable
						lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<", CompareConstants.vbBinaryCompare);
					}
					while (!(lngNextVariable == 0));
				}
				// take the last of the string and add it to the end
				strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
				// check for variables
				if (Strings.InStr(1, "<", strOriginal, CompareConstants.vbBinaryCompare) > 0)
				{
					// strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
					MessageBox.Show("ERROR: There are still variables in the report string.", "SetupVariablesInString Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					SetupVariablesInString = strBuildString;
				}
				return SetupVariablesInString;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Setup Variables In String", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SetupVariablesInString;
		}

		private string GetVariableValue_2(string strVarName)
		{
			return GetVariableValue(ref strVarName);
		}

		private string GetVariableValue(ref string strVarName)
		{
			string GetVariableValue = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will take a variable name and find it in the array, then get the value and return it as a string
				int intCT;
				string strValue = "";
				// this is a dummy code to show the user where the hard codes are
				if (strVarName == "CRLF")
				{
					GetVariableValue = "";
					return GetVariableValue;
				}
				for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
				{
					if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
					{
						switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
						{
							case 0:
								{
									// value from the static variables in the grid
									switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
									{
									// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
										case 0:
											{
												strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
												break;
											}
										case 1:
											{
												strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
												break;
											}
										case 2:
											{
												//FC:FINAL:MSH - I.Issue #1005: incorrect date format
												strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM d, yyyy");
												break;
											}
										case 3:
											{
												strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
												break;
											}
										case 4:
											{
												strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
												break;
											}
									}
									//end switch
									break;
								}
							case 1:
								{
									// value from the dynamic variables in the database
									// MAL@20080423: Test for address variables and add appropriate prefix
									// Tracker Reference:
									if (Strings.UCase(Strings.Left(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName, 4)) == "ADDR")
									{
										strValue = FCConvert.ToString(rsData.Get_Fields(strAddressPrefix + modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
									}
									else
									{
										strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
									}
									if (Strings.Trim(strValue) == "")
									{
										if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
										{
											// maybe put a blank line there
											strValue = "__________";
										}
									}
									break;
								}
							case 2:
								{
									// questions at the bottom of the grid
									strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
									break;
								}
							case 3:
								{
									// calculated values that are stored in the DatabaseFieldName field
									strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
									if (Strings.Trim(strValue) == "")
									{
										if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
										{
											// maybe put a blank line there
											strValue = "__________";
										}
									}
									break;
								}
						}
						//end switch
						break;
					}
				}
				GetVariableValue = strValue;
				return GetVariableValue;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get Variable Values", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetVariableValue;
		}

		private void CalculateVariableTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will calculate all of the fields that need it and store
				// the value in the DatabaseFieldName string in the frfFreeReport
				// struct that these fields include principal, interest, costs,
				// total due and certified mail fee
				//clsDRWrapper rsSetCert;
				clsDRWrapper rsLN = new clsDRWrapper();
				clsDRWrapper rsCombine = new clsDRWrapper();
				double dblTotalDue = 0;
				double dblInt = 0;
				double dblPrin = 0;
				double dblTax = 0;
				double dblPayments = 0;
				double dblDemand = 0;
				double dblCertMailFee = 0;
				int lngMortHolder = 0;
				string strAddressBar = "";
				string strBillDate = "";
				string strLessPaymentsLine = "";
				string strLocation = "";
				string strTemp = "";
				string strTemp2 = "";
				string strTemp3 = "";
				string strTemp4 = "";
				string strTemp5 = "";
				string strOwnerName = "";
				string strMapLot = "";
				string strBookPage = "";
				double dblFCFee = 0;
				string strTrueFalse = "";
				double dblBillDue = 0;
				double dblBillPrin;
				double dblBillCurInt = 0;
				double dblBillInt = 0;
				bool blnBillsCombined;
				int lngBillCount = 0;
				string strBillList = "";
				// Latest Tenant/Owner Information
				string strLastOwner;
				string strLastOwner2;
				string strLastAddress;
				string strLastAddress2;
				string strLastAddress3;
				string strLastCity;
				string strLastState;
				string strLastZip;
				// for CMFNumbers
				string strCMFName = "";
				int lngCMFMHNumber = 0;
				bool boolCMFNewOwner;
				bool boolPrintCMF;
				// true if a CMF should be printed for that person
				string strCMFAddr1;
				string strCMFAddr2;
				string strCMFAddr3;
				string strCMFAddr4;
				double dblTemp = 0;
				TRYAGAIN:
				;
				strCMFAddr1 = "";
				strCMFAddr2 = "";
				strCMFAddr3 = "";
				strCMFAddr4 = "";
				strLastOwner = "";
				strLastOwner2 = "";
				strLastAddress = "";
				strLastAddress2 = "";
				strLastAddress3 = "";
				strLastCity = "";
				strLastState = "";
				strLastZip = "";
				// MAL@20070927: Add check for multiple owners on bills being liened
				// Changed code from this point on to refer to new string variables rather then rsData.Fields
				rsData.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrLienMaturityQuery + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey));
				if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "LIENMAT"))
				{
					modMain.GetLatestOwnerInformation(rsData.Get_Fields_Int32("AccountKey"), "LIENMAT", ref strLastOwner, ref strLastOwner2, ref strLastAddress, ref strLastAddress2, ref strLastAddress3, ref strLastCity, ref strLastState, ref strLastZip);
				}
				else
				{
					strLastOwner = Strings.Trim(FCConvert.ToString(rsBills.Get_Fields_String("OName")));
					strLastOwner2 = Strings.Trim(FCConvert.ToString(rsBills.Get_Fields_String("OName2")));
					strLastAddress = FCConvert.ToString(rsBills.Get_Fields_String("OAddress1"));
					strLastAddress2 = FCConvert.ToString(rsBills.Get_Fields_String("OAddress2"));
					strLastAddress3 = FCConvert.ToString(rsBills.Get_Fields_String("OAddress3"));
					strLastCity = FCConvert.ToString(rsBills.Get_Fields_String("OCity"));
					strLastState = FCConvert.ToString(rsBills.Get_Fields_String("OState"));
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OZip4"))) != "")
					{
						strLastZip = rsData.Get_Fields_String("OZip") + "-" + rsData.Get_Fields_String("OZip4");
					}
					else
					{
						strLastZip = FCConvert.ToString(rsData.Get_Fields_String("OZip"));
					}
				}
				boolPrintCMF = true;
				if (!boolCopy)
				{
					dblTotalDue = 0;
					lngBillCount = 0;
					strBillList = "";
					if (modUTLien.Statics.gboolUseMailDateForMaturity)
					{
						dtInterestDate = DateAndTime.DateValue(GetVariableValue_2("MAILDATE"));
					}
					else
					{
						dtInterestDate = DateAndTime.DateValue(GetVariableValue_2("FORECLOSUREDATE"));
					}
					// MAL@20081112: Complete the multiple bills calculations
					// Tracker Reference: 16214
					if (blnCombine)
					{
						rsCombine.OpenRecordset(rsBills.Name(), modExtraModules.strUTDatabase);
						while (!rsCombine.EndOfFile())
						{
							// this will calculate the principal, interest and total due
							dblBillInt = 0;
							dblBillCurInt = 0;
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							if (FCConvert.ToInt32(rsCombine.Get_Fields_Int32("LienKey")) != 0 && rsCombine.Get_Fields(strWS + "CombinationLienKey") == rsCombine.Get_Fields("Bill"))
							{
								// BILL.ID
								dblBillDue = modUTCalculations.CalculateAccountUTLien(rsCombine, dtInterestDate, ref dblBillCurInt, boolWater, dblBillInt, dblTemp);
							}
							else
							{
								dblBillDue = 0;
							}
							if (dblBillDue > 0)
							{
								// count this bill
								// strAcctList = strAcctList & rsCombine.Fields("Bill") & ","
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								strBillList += rsCombine.Get_Fields("Bill") + ",";
								lngBillCount += 1;
								dblInt += dblBillInt + dblTemp;
								dblTotalDue += dblBillDue;
							}
							rsCombine.MoveNext();
						}
						// kk 10032013
						strBillList = Strings.Left(strBillList, strBillList.Length - 1);
						// If lngBillCount > 1 Then
						blnBillsCombined = true;
						// Else
						// blnBillsCombined = False
						// End If
					}
					else
					{
						lngBillCount = 1;
						blnBillsCombined = false;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strBillList = FCConvert.ToString(rsBills.Get_Fields("Bill"));
					}
					if (!blnCombine)
					{
						// this will calculate the principal, interest and total due
						// If rsData.Fields("LienKey") <> 0 Then
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						if (FCConvert.ToInt32(rsBills.Get_Fields_Int32("LienKey")) != 0 && rsBills.Get_Fields(strWS + "CombinationLienKey") == rsBills.Get_Fields("Bill"))
						{
							rsLN.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsBills.Get_Fields_Int32("LienKey"));
							dblInt = 0;
							dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLN, dtInterestDate, ref dblInt, boolWater);
							// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
							dblInt += (rsLN.Get_Fields("Interest") - rsLN.Get_Fields("IntAdded") - rsLN.Get_Fields("IntPaid") - rsLN.Get_Fields("PLIPaid"));
						}
						else
						{
							rsBills.MoveNext();
							if (rsBills.EndOfFile())
								return;
						}
					}
					rsUT.FindFirstRecord("ID", rsData.Get_Fields_Int32("AccountKey"));
					if (rsUT.NoMatch)
					{
						// MAL@20080221: Change to show notices for Tax Acquired accounts
						// ElseIf rsUT.Fields("InBankruptcy") Or rsUT.Fields("TaxAcquired") Then
					}
					else if (rsUT.Get_Fields_Boolean("InBankruptcy"))
					{
						intCurrentIndex += 1;
						if (intCurrentIndex <= Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
						{
							rsBills.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrLienMaturityQuery + ") AS qTmp WHERE AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey), modExtraModules.strUTDatabase);
							goto TRYAGAIN;
						}
						else
						{
							return;
						}
						// If rsData.EndOfFile Then Exit Sub
						// GoTo TRYAGAIN
					}
					if (FCUtils.Round(dblTotalDue, 2) == 0)
					{
						lngBalanceZero += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strBalanceZero += rsBills.Get_Fields("Bill") + ", ";
						rsBills.MoveNext();
						if (rsBills.EndOfFile())
							return;
					}
					// DJW@11122009 - Changed from Bill to CombinationLienKey in strings sent to summary report
					if (dblTotalDue > 0)
					{
						// these are the account that will be demanded
						lngBalancePos += 1;
						strAcctList += rsBills.Get_Fields_Int32("AccountKey") + ",";
						if (blnCombine)
						{
							rsCombine.MoveFirst();
							do
							{
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								strBalancePos += rsCombine.Get_Fields("Bill") + ", ";
								rsCombine.MoveNext();
							}
							while (rsCombine.EndOfFile() != true);
						}
						else
						{
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							strBalancePos += rsBills.Get_Fields("Bill") + ", ";
						}
					}
					else
					{
						if (dblTotalDue < 0)
						{
							lngBalanceNeg += 1;
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							strBalanceNeg += rsBills.Get_Fields("Bill") + ", ";
							// strBalanceNeg = strBalanceNeg & rsData.Fields("Bill") & ", "
						}
					}
					// MAL@20080527: Set starting copies
					lngCopies = 1;
					// this will find out how many mortgage holders this account has and how many copies to print
					if (boolMort)
					{
						lngMortHolder = CalculateMortgageHolders();
						lngCopies += lngMortHolder;
					}
					else
					{
						lngMortHolder = 0;
						lngCopies = lngCopies;
					}
					// this will find out the certified mail fee
					if (boolPayCert)
					{
						if (boolPayMortCert)
						{
							dblCertMailFee = (lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"))) + FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
						else
						{
							dblCertMailFee = FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
					}
					else
					{
						if (boolPayMortCert)
						{
							dblCertMailFee = lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
						else
						{
							dblCertMailFee = 0;
						}
					}

					if (Strings.Trim(strLastOwner2) != "")
					{
						strOwnerName = strLastOwner + " and " + strLastOwner2;
					}
					else
					{
						strOwnerName = strLastOwner;
					}
					// check to see if one is going to be sent to the next owner
					// If intNewOwnerChoice <> 0 And NewOwnerUT(rsData, rsUT) Then
					bool boolUTMatch = false;
                    //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
					//if (intNewOwnerChoice != 0 && modMain.NewOwnerUT(ref rsBills, ref rsUT, ref boolUTMatch))
					if (intNewOwnerChoice != 0 & modMain.NewOwnerUT(ref rsBills, ref rsUT, ref boolUTMatch))
					{
						// if there has been a new owner, then make a copy for the new owner as well
						if (!boolNewOwner)
						{
							if (boolPayNewOwner)
							{
								// charge for the new owner's copy
								dblCertMailFee += FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
							}
							lngCopies += 1;
							boolNewOwner = true;
						}
					}
					// lblDate.Caption = PadToString(GetAccountNumber(rsData.Fields("AccountKey")), 6)
					lblDate.Text = modMain.PadToString_8(modUTStatusPayments.GetAccountNumber_2(rsBills.Get_Fields_Int32("AccountKey")), 6);
					if (blnCombine)
					{
						rsCombine.MoveFirst();
						while (!rsCombine.EndOfFile())
						{
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							if (FCConvert.ToInt32(rsCombine.Get_Fields_Int32("LienKey")) != 0 && rsCombine.Get_Fields(strWS + "CombinationLienKey") == rsCombine.Get_Fields("Bill"))
							{
								rsLN.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsCombine.Get_Fields_Int32("LienKey"));
								// MAL@20081112: Cycle through all the bills
								// Tracker Reference: 16214
								// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
								dblPrin += rsLN.Get_Fields("Principal");
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								dblPayments += rsLN.Get_Fields("PrinPaid");
								// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
								dblTax += (rsLN.Get_Fields("Tax") - rsLN.Get_Fields("TaxPaid"));
								// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
								if (Conversion.Val(rsLN.Get_Fields("Costs")) > 0)
								{
									// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
									dblDemand += FCUtils.Round(rsLN.Get_Fields("Costs") - rsLN.Get_Fields_Double("CostPaid"), 2);
								}
								else
								{
									dblDemand = dblDemand;
								}
							}
							rsCombine.MoveNext();
						}
					}
					else
					{
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						if (FCConvert.ToInt32(rsBills.Get_Fields_Int32("LienKey")) != 0 && rsBills.Get_Fields(strWS + "CombinationLienKey") == rsBills.Get_Fields("Bill"))
						{
							rsLN.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsBills.Get_Fields_Int32("LienKey"));
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblPrin = rsLN.Get_Fields("Principal");
							// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
							dblPayments = rsLN.Get_Fields("PrinPaid");
							// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
							dblTax = rsLN.Get_Fields("Tax") - rsLN.Get_Fields("TaxPaid");
							// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
							if (Conversion.Val(rsLN.Get_Fields("Costs")) > 0)
							{
								// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
								dblDemand += FCUtils.Round(rsLN.Get_Fields("Costs") - rsLN.Get_Fields_Double("CostPaid"), 2);
							}
							else
							{
								dblDemand = dblDemand;
							}
						}
					}

					if (FCUtils.Round(dblPayments, 2) != 0)
					{
						// only show this when there are payments
						if (dblPayments < 0)
						{
							// if the principal paid is a negative number then some adjustment has happened (supplemental) this should not happen later in the code life
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsBills.Get_Fields("Bill") + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
							if (rsPayments.EndOfFile())
							{
								strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
							else
							{
								strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
						}
						else
						{
							// principal paid is greater then zero
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsBills.Get_Fields("Bill") + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
							if (rsPayments.EndOfFile())
							{
								strLessPaymentsLine = "less payment and adjustment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
							else
							{
								strLessPaymentsLine = "less payment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
						}
					}
					else
					{
						strLessPaymentsLine = "";
					}

					// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
					if (Strings.Trim(rsLN.Get_Fields("Book") + " ") != "" || Strings.Trim(rsLN.Get_Fields("Page") + " ") != "")
					{
						// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
						strBookPage = "Book " + rsLN.Get_Fields("Book") + ", Page " + rsLN.Get_Fields("Page");
					}
					else
					{
						strBookPage = "";
					}
					// Location
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					strLocation = Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("StreetNumber"))) + " " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("Apt"))) + " " + rsUT.Get_Fields_String("StreetName");
					// MapLot
					strMapLot = FCConvert.ToString(rsBills.Get_Fields_String("MapLot"));
					if (Strings.Trim(strMapLot) == "")
					{
						strMapLot = "________________";
					}
					if (Strings.Trim(strLocation) == "")
					{
						strLocation = "________________";
					}
					if (Strings.Trim(strBookPage) == "")
					{
						strBookPage = "________________";
					}
					// commitment date
					rsRate.FindFirstRecord("ID", rsBills.Get_Fields_Int32("RateKey"));
					if (!rsRate.NoMatch)
					{
						//FC:FINAL:MSH - i.issue #1005: incorrect converting from date to int
						//if (FCConvert.ToInt32(rsRate.Get_Fields("BillDate")) != 0)
						if (FCConvert.ToDateTime(rsRate.Get_Fields_DateTime("BillDate") as object).ToOADate() != 0)
						{
							strBillDate = Strings.Format(rsRate.Get_Fields_DateTime("BillDate"), "MMMM d, yyyy");
						}
						else
						{
							strBillDate = "__________";
						}
					}
					else
					{
						strBillDate = "__________";
					}

					strAddressBar = "    " + strLastOwner;
					if (Strings.Trim(strLastOwner2) != "")
					{
						strAddressBar += " and " + strLastOwner2;
					}
					// Or intNewOwnerChoice = 1
					// If (boolAddressCO Or (rsBills.Fields("OAddress1") = "" And rsBills.Fields("OAddress2") = "" And rsBills.Fields("OAddress3") = "")) Then   'this is the owner at time of billing C/O the new owner and the new owner address
					if (boolAddressCO || (strLastAddress == "" && strLastAddress2 == "" && strLastAddress3 == ""))
					{
						// this is the owner at time of billing C/O the new owner and the new owner address
						// or if there is no information in the CL fields (Should only happen the first year after conversion)
						if (boolAddressCO && boolNewOwner)
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							strAddressBar += "\r\n" + "    C/O " + rsUT.Get_Fields("OwnerName");
							// name on the current RE account
							// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName"))) != "")
							{
								// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
								strAddressBar += "\r\n" + "        " + rsUT.Get_Fields("SecondOwnerName");
								// second owner name on the current RE account
							}
						}
						// if the row needs to be added, then do it
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
						}
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strAddressBar += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strAddressBar += " " + rsUT.Get_Fields_String("OZip");
							}
						}
						else
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strAddressBar += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strAddressBar += rsUT.Get_Fields_String("OZip");
							}
						}
						if (boolAddressCO && boolNewOwner)
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							if (rsUT.Get_Fields("OwnerName") != rsBills.Get_Fields_String("OName"))
							{
								// this is information for the Certified Mailing List
								// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
								strCMFAddr1 = "C/O " + rsUT.Get_Fields("OwnerName");
								strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
								strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
								// MAL@20080325: Tracker Reference: 12766
								if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
									if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
									{
										strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
									}
									else
									{
										strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
									}
								}
								else
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
									if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
									{
										strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
									}
									else
									{
										strCMFAddr4 += rsUT.Get_Fields_String("OZip");
									}
								}
							}
							else
							{
								// this is information for the Certified Mailing List
								strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
								strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
								strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
								// MAL@20080325: Tracker Reference: 12766
								if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
									if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
									{
										strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
									}
									else
									{
										strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
									}
								}
								else
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
									if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
									{
										strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
									}
									else
									{
										strCMFAddr4 += rsUT.Get_Fields_String("OZip");
									}
								}
							}
						}
						else
						{
							// this is information for the Certified Mailing List
							strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
							strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
							strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
								}
							}
							else
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strCMFAddr4 += rsUT.Get_Fields_String("OZip");
								}
							}
						}
					}
					else
					{
						// this is the owner name at the time of billing and the address at the time of billing
						// If (rsBills.Fields("OAddress1") = "" And rsBills.Fields("OAddress2") = "" And rsBills.Fields("OAddress3") = "") Then
						if (strLastAddress == "" && strLastAddress2 == "" && strLastAddress3 == "")
						{
							// if the row needs to be added, then do it
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
							}
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
							}
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
							}
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strAddressBar += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strAddressBar += " " + rsUT.Get_Fields_String("OZip");
								}
							}
							else
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strAddressBar += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strAddressBar += rsUT.Get_Fields_String("OZip");
								}
							}
							// this is information for the Certified Mailing List
							strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
							strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
							strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
								}
							}
							else
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strCMFAddr4 += rsUT.Get_Fields_String("OZip");
								}
							}
						}
						else
						{
							if (Strings.Trim(strLastAddress) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress;
							}
							if (Strings.Trim(strLastAddress2) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress2;
							}
							if (Strings.Trim(strLastAddress3) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress3;
							}
							// MAL@20080320: Tracker Reference: 12766
							if (strLastCity != "")
							{
								strLastCity += ", ";
							}
							else
							{
								strLastCity = strLastCity;
							}
							if (strLastState != "")
							{
								strLastState += " ";
							}
							else
							{
								strLastState = strLastState;
							}
							strAddressBar += "\r\n" + "    " + strLastCity + strLastState + strLastZip;
							// this is information for the Certified Mailing List
							strCMFAddr1 = strLastAddress;
							strCMFAddr2 = strLastAddress2;
							strCMFAddr3 = strLastAddress3;
							// strCMFAddr4 = strLastCity & ", " & strLastState & " " & strLastZip
							strCMFAddr4 = strLastCity + strLastState + strLastZip;
						}
					}

					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					if (modUTStatusPayments.IsAccountTaxAcquired_7(rsUT.Get_Fields("AccountNumber")) && modGlobalConstants.Statics.gblnShowTaxAcquiredCaption)
					{
						// If IsAccountTaxAcquired(, GetAccountNumber(rsData.Fields("AccountKey"))) And gblnShowTaxAcquiredCaption Then    'kjr
						lblTaxAcquired.Visible = true;
					}
					else
					{
						lblTaxAcquired.Visible = false;
					}
					// info for CMFNumbers table
					strCMFName = strOwnerName;
					lngCMFMHNumber = 0;
					// boolNewOwner = False
					lngCMFBillKey = FCConvert.ToInt32(rsBills.Get_Fields(strWS + "CombinationLienKey"));
					// Billing year (0)
					modRhymalReporting.Statics.frfFreeReport[0].DatabaseFieldName = FCConvert.ToString(intYear);
					// BookPage (3)
					modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = strBookPage;
					// Address Bar (14)
					modRhymalReporting.Statics.frfFreeReport[14].DatabaseFieldName = strAddressBar;
					// principal (20)
					modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = Strings.Format(dblPrin - dblPayments, "#,##0.00");
					// Tax (37)
					modRhymalReporting.Statics.frfFreeReport[37].DatabaseFieldName = Strings.Format(dblTax, "#,##0.00");
					// interest (21)
					modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = Strings.Format(dblInt, "#,##0.00");
					// less payments (22)
					modRhymalReporting.Statics.frfFreeReport[22].DatabaseFieldName = strLessPaymentsLine;
					// city/town (23)
					modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = modGlobalConstants.Statics.gstrCityTown;
					// owner name (24)
					modRhymalReporting.Statics.frfFreeReport[24].DatabaseFieldName = strOwnerName;
					// location (29)
					modRhymalReporting.Statics.frfFreeReport[29].DatabaseFieldName = strLocation;
					// total certified mail fee (30)
					modRhymalReporting.Statics.frfFreeReport[30].DatabaseFieldName = Strings.Format(dblCertMailFee, "#,##0.00");
					// Demand Fees (32)
					modRhymalReporting.Statics.frfFreeReport[32].DatabaseFieldName = Strings.Format(dblDemand, "#,##0.00");
					// CommitmentDate (33)
					modRhymalReporting.Statics.frfFreeReport[33].DatabaseFieldName = strBillDate;
					// total due (28)
					modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = Strings.Format(dblTotalDue + dblCertMailFee + dblDemand, "#,##0.00");
					// MapLot (34)
					modRhymalReporting.Statics.frfFreeReport[34].DatabaseFieldName = strMapLot;
					dblFCFee = FCConvert.ToDouble(GetVariableValue_2("FORECLOSUREFEE"));
					Array.Resize(ref modUTLien.Statics.arrDemand, lngArrayIndex + 1);
					// this will make sure that there are enough elements to use
					modUTLien.Statics.arrDemand[lngArrayIndex].Used = true;
					modUTLien.Statics.arrDemand[lngArrayIndex].Processed = false;
					modUTLien.Statics.arrDemand[lngArrayIndex].Account = FCConvert.ToInt32(rsBills.Get_Fields_Int32("AccountKey"));
					modUTLien.Statics.arrDemand[lngArrayIndex].BillKey = FCConvert.ToInt32(rsBills.Get_Fields(strWS + "CombinationLienKey"));
					// kk10242014 trout-1106  Use combo lien key     ' = rsBills.Fields("Bill")
					modUTLien.Statics.arrDemand[lngArrayIndex].Name = FCConvert.ToString(rsBills.Get_Fields_String("OName"));
					modUTLien.Statics.arrDemand[lngArrayIndex].Fee = dblDemand + dblFCFee;
					modUTLien.Statics.arrDemand[lngArrayIndex].CertifiedMailFee = dblCertMailFee;
					lngArrayIndex += 1;
					// MAL@20080602: Change to update all bills with this lien number to the correct number of copies
					// Call UpdateCopies(rsBills.Fields("AccountKey"), rsBills.Fields("LienKey"))
					UpdateCopies(strBillList);
				}
				else
				{
					// this is the difference when it is a copy
					// address bar
					if (rsMort.EndOfFile() != true)
					{
						// info for CMFNumbers table
						strCMFName = FCConvert.ToString(rsMort.Get_Fields_String("Name"));
						lngCMFMHNumber = FCConvert.ToInt32(rsMort.Get_Fields_Int32("MortgageHolderID"));
						// kk 12112013  trouts-44   rsMort.Fields("MortgageAssociation.MortgageHolderID")
						// boolNewOwner = False
						lngCMFBillKey = 0;
						lngCMFBillKey = FCConvert.ToInt32(rsBills.Get_Fields(strWS + "CombinationLienKey"));
						// kk10242014 trout-1106  Use combo lien key     ' = rsBills.Fields("Bill")
						strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address1"));
						if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1"))) != "")
						{
							strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address1"));
							if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
							{
								strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address2"));
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr4 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr4 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
						}
						else
						{
							if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
							{
								strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address2"));
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr1 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr1 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
						}
						strAddressBar = "    " + modGlobalFunctions.PadStringWithSpaces(rsMort.Get_Fields_String("Name"), 36, false) + modGlobalFunctions.PadStringWithSpaces(" ", 11, false) + rsBills.Get_Fields_String("OName");

						if (strLastCity != "")
						{
							strLastCity += ", ";
						}
						else
						{
							strLastCity = strLastCity;
						}
						if (strLastState != "")
						{
							strLastState += " ";
						}
						else
						{
							strLastState = strLastState;
						}
						if (Strings.Trim(strLastAddress) != "")
						{
							strTemp = strLastAddress;
							strTemp2 = strLastAddress2;
							strTemp3 = strLastAddress3;
							// strTemp4 = strLastCity & ", " & strLastState & " " & strLastZip
							strTemp4 = strLastCity + strLastState + " " + strLastZip;
						}
						else if (Strings.Trim(strLastAddress2) != "")
						{
							strTemp = strLastAddress2;
							strTemp2 = strLastAddress3;
							// strTemp3 = strLastCity & ", " & strLastState & " " & strLastZip
							strTemp3 = strLastCity + strLastState + strLastZip;
							strTemp4 = "";
						}
						else
						{
							// there is no address
							// if the row needs to be added, then do it
							if (Strings.Trim(strLastAddress) != "")
							{
								strTemp = strLastAddress;
							}
							if (Strings.Trim(strLastAddress2) != "")
							{
								strTemp2 = strLastAddress2;
							}
							if (Strings.Trim(strLastAddress3) != "")
							{
								strTemp3 = strLastAddress3;
							}
							// strTemp4 = strLastCity & ", " & strLastState & " " & strLastZip
							strTemp4 = strLastCity + strLastState + strLastZip;
						}

						if (Strings.Trim(strLastOwner2) != "")
						{
							// move all of the fields down a level
							strTemp5 = strTemp4;
							strTemp4 = strTemp3;
							strTemp3 = strTemp2;
							strTemp2 = strTemp;
							strTemp = Strings.Trim(strLastOwner2);
						}
						if (Strings.Trim(strTemp4) == "")
						{
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp3) == "")
						{
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp2) == "")
						{
							strTemp2 = strTemp3;
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp) == "")
						{
							strTemp = strTemp2;
							strTemp2 = strTemp3;
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr1, 47, false) + strTemp;
						strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr2, 47, false) + strTemp2;
						if (Strings.Trim(strCMFAddr3) != "" || Strings.Trim(strTemp3) != "")
						{
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr3, 47, false) + strTemp3;
						}
						if (Strings.Trim(strCMFAddr4) != "" || Strings.Trim(strTemp4) != "")
						{
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr4, 47, false) + strTemp4;
						}
						// set name and address bar for a mortgage holder
						modRhymalReporting.Statics.frfFreeReport[14].DatabaseFieldName = strAddressBar;
						rsMort.MoveNext();
					}
					else if (boolNewOwner)
					{
						// this is the last copy to be made, it is for the new owner
						// boolNewOwner = False                            'this will set it off for the next account
						// info for CMFNumbers table
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						strCMFName = Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("OwnerName")));
						lngCMFMHNumber = 0;
						// boolNewOwner = True
						lngCMFBillKey = 0;
						lngCMFBillKey = FCConvert.ToInt32(rsBills.Get_Fields(strWS + "CombinationLienKey"));
						// kk10242014 trout-1106  Use combo lien key     ' = rsBills.Fields("Bill")
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						strAddressBar = "    " + rsUT.Get_Fields("OwnerName");
						// name at time of billing
						// strAddressBar = strAddressBar & vbCrLf & "    C/O " & rsUT.Fields("RSName")   'name on the current RE account
						// if the row needs to be added, then do it
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
						}
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strAddressBar += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strAddressBar += " " + rsUT.Get_Fields_String("OZip");
							}
						}
						else
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strAddressBar += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strAddressBar += rsUT.Get_Fields_String("OZip");
							}
						}
						// this is information for the Certified Mailing List
						strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
						strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
						strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
							}
						}
						else
						{
							strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strCMFAddr4 += rsUT.Get_Fields_String("OZip");
							}
						}
						// set name and address bar for a New Owner
						modRhymalReporting.Statics.frfFreeReport[14].DatabaseFieldName = strAddressBar;
						rsUT.MoveNext();
					}
					else
					{
						// else leave the address bar the same
					}

				}
				if (boolWater)
				{
					strTrueFalse = "1";
					// "TRUE"
				}
				else
				{
					strTrueFalse = "0";
					// "FALSE"
				}
				// this will add the information to the CMFNumber table so that it will be
				// easier to figure out which accounts need certified mail forms to be printed for them
				// MAL@20071016: Added support for new Process Type
				// Call Reference: 114824
				if (lngCMFMHNumber != 0)
				{
					// rsCMFNumbers.FindFirstRecord2 "BillKey,Type,MortgageHolder,boolWater,ProcessType", rsBills.Fields("Bill") & ",22," & lngCMFMHNumber & "," & strTrueFalse & ",LMAT", ","     'kk08122014 trocls-49   ' ",'LMAT'"
					rsCMFNumbers.FindFirstRecord2("BillKey,Type,MortgageHolder,boolWater,ProcessType", FCConvert.ToString(lngCMFBillKey) + ",22," + FCConvert.ToString(lngCMFMHNumber) + "," + strTrueFalse + ",LMAT", ",");
					// kk08122014 trocls-49   ' ",'LMAT'"
				}
				else
				{
					if (boolCopy && boolNewOwner)
					{
						// rsCMFNumbers.FindFirstRecord2 "BillKey,Type,NewOwner,boolWater,ProcessType", rsBills.Fields("Bill") & ",22,1," & strTrueFalse & ",LMAT", ","     'kk08122014 trocls-49   ' ",'LMAT'"
						rsCMFNumbers.FindFirstRecord2("BillKey,Type,NewOwner,boolWater,ProcessType", FCConvert.ToString(lngCMFBillKey) + ",22,1," + strTrueFalse + ",LMAT", ",");
						// kk08122014 trocls-49   ' ",'LMAT'"
					}
					else
					{
						// rsCMFNumbers.FindFirstRecord2 "BillKey,Type,NewOwner,boolWater,ProcessType", rsBills.Fields("Bill") & ",22,0," & strTrueFalse & ",LMAT", ","     'kk08122014 trocls-49   ' ",'LMAT'"
						rsCMFNumbers.FindFirstRecord2("BillKey,Type,NewOwner,boolWater,ProcessType", FCConvert.ToString(lngCMFBillKey) + ",22,0," + strTrueFalse + ",LMAT", ",");
						// kk08122014 trocls-49   ' ",'LMAT'"
					}
				}
				if (rsCMFNumbers.NoMatch)
				{
					// Or (boolCopy And boolPrintCMF) Then
					rsCMFNumbers.AddNew();
				}
				else
				{
					rsCMFNumbers.Edit();
				}
				rsCMFNumbers.Set_Fields("boolWater", boolWater);
				rsCMFNumbers.Set_Fields("Name", strCMFName);
				rsCMFNumbers.Set_Fields("Address1", strCMFAddr1);
				rsCMFNumbers.Set_Fields("Address2", strCMFAddr2);
				rsCMFNumbers.Set_Fields("Address3", strCMFAddr3);
				rsCMFNumbers.Set_Fields("Address4", strCMFAddr4);
				rsCMFNumbers.Set_Fields("Billkey", lngCMFBillKey);
				rsCMFNumbers.Set_Fields("MortgageHolder", lngCMFMHNumber);
				rsCMFNumbers.Set_Fields("Account", rsBills.Get_Fields_Int32("AccountKey"));
				rsCMFNumbers.Set_Fields("Service", rsBills.Get_Fields_String("Service"));
				// MAL@20071114: Changed to use LastOwner Name if BName is blank (Pre-conversion)
				// rsCMFNumbers.Fields("BName") = rsBills.Fields("BName")
				if (fecherFoundation.FCUtils.IsNull(rsBills.Get_Fields_String("BName")) || FCConvert.ToString(rsBills.Get_Fields_String("BName")) == "")
				{
					rsCMFNumbers.Set_Fields("BName", strCMFName);
				}
				else
				{
					rsCMFNumbers.Set_Fields("BName", rsBills.Get_Fields_String("BName"));
				}
				rsCMFNumbers.Set_Fields("ActualAccountNumber", rsBills.Get_Fields_Int32("ActualAccountNumber"));
				if (lngCMFMHNumber != 0)
				{
					// mort holder
					// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not boolPayMortCert
				}
				else
				{
					rsCMFNumbers.Set_Fields("NewOwner", false);
					if (boolCopy && boolNewOwner)
					{
						rsCMFNumbers.Set_Fields("NewOwner", true);
						// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not (intNewOwnerChoice = 2 Or intNewOwnerChoice = 3)   'Not boolPayNewOwner
						boolNewOwner = false;
					}
					else
					{
						// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not boolPayCert
					}
				}
				rsCMFNumbers.Set_Fields("Type", 22);
				rsCMFNumbers.Set_Fields("ProcessType", "LMAT");
				// MAL@20071016
				rsCMFNumbers.Update(true);
				rsLN.Dispose();
				rsCombine.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Calculate Variable Error - " + rsBills.Get_Fields("Bill"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				// & rsData.Fields("BillKey")
			}
		}

		private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				bool boolUseRE = false;
				clsDRWrapper rsM = new clsDRWrapper();
				rsM.OpenRecordset("SELECT UseREAccount, UseMortgageHolder, REAccount, ID FROM Master WHERE ID = " + rsData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
				if (!rsM.EndOfFile())
				{
					// MAL@20081105: Changed to not require the UseREAccount option to be selected to use the RE MHs option
					// Tracker Reference: 15925
					// boolUseRE = rsM.Fields("UseREAccount") And rsM.Fields("UseMortgageHolder") And rsM.Fields("REAccount") <> 0
					boolUseRE = rsM.Get_Fields_Boolean("UseMortgageHolder") && FCConvert.ToInt32(rsM.Get_Fields_Int32("REAccount")) != 0;
				}
				// MAL@20081106: Change to take the Receive Copies option into account
				// Tracker Reference: 16047
				if (boolUseRE)
				{
					// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & rsM.Fields("REAccount") & " AND Module = 'RE'", strGNDatabase
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + rsM.Get_Fields_Int32("REAccount") + " AND Module = 'RE' AND ISNULL(ReceiveCopies,0) = 1", "CentralData");
				}
				else
				{
					// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & GetUTAccountNumber(rsData.Fields("AccountKey")) & " AND Module = 'UT'", strGNDatabase
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(modUTStatusPayments.GetUTAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"))) + " AND Module = 'UT' AND ISNULL(ReceiveCopies,0) = 1", "CentralData");
				}
				if (rsMort.EndOfFile() != true)
				{
					CalculateMortgageHolders = rsMort.RecordCount();
				}
				else
				{
					CalculateMortgageHolders = 0;
				}
				return CalculateMortgageHolders;
			}
			catch
			{
				
				frmWait.InstancePtr.Unload();
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}

		public void SaveLienStatus()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (Strings.Right(strAcctList, 1) == ",")
				{
					strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
				}
				// this statement will change the status of the accounts that have been process
				if (Strings.Trim(strAcctList) != "")
				{
					rsData.Execute("UPDATE Bill SET " + strWS + "LienProcessStatus = 5, " + strWS + "LienStatusEligibility = 6 WHERE AccountKey IN (" + strAcctList + ") AND BillStatus = 'B' AND " + strWS + "LienStatusEligibility >= 4", modExtraModules.strUTDatabase);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Save Lien Status", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void UpdateCopies(string strBillList)
		{
			clsDRWrapper rsCopy = new clsDRWrapper();
			// kk 10222013 trouts-44  .OpenRecordset "SELECT * FROM Bill WHERE ID IN(" & Left(strBillList, Len(strBillList) - 1) & ")", strUTDatabase
			rsCopy.OpenRecordset("SELECT * FROM Bill WHERE ID IN(" + strBillList + ")", modExtraModules.strUTDatabase);
			if (rsCopy.RecordCount() > 0)
			{
				rsCopy.MoveFirst();
				while (!rsCopy.EndOfFile())
				{
					rsCopy.Edit();
					rsCopy.Set_Fields("Copies", lngCopies);
					rsCopy.Update();
					rsCopy.MoveNext();
				}
			}
		}

		private void arLienMaturity_Load(object sender, System.EventArgs e)
		{
		}

		private void arLienMaturity_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
