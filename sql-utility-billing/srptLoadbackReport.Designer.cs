namespace UtilityBillingReports.Reports
{
	/// <summary>
	/// Summary description for srptLoadbackReport.
	/// </summary>
	partial class srptLoadbackReport
	{

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLoadbackReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrin = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPLI = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCHGINT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRK = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillNumberLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooterTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooterPrin = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooterTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooterCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooterCHGINT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooterTaxTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooterPLI = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnFooter = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPLI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCHGINT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPLI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRK)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillNumberLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterPrin)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterCHGINT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterTaxTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterPLI)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAcct,
            this.fldPrin,
            this.fldName,
            this.fldTax,
            this.fldCost,
            this.fldCHGINT,
            this.fldTotal,
            this.fldPLI,
            this.fldRK,
            this.fldBillNumberLabel});
			this.Detail.Height = 0.4375F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPage});
			this.PageHeader.Height = 0.4791667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblAcct,
            this.lblPrin,
            this.Line1,
            this.lblName,
            this.lblReportType,
            this.lblTax,
            this.lblCost,
            this.lblTotal,
            this.lblPLI,
            this.Label1,
            this.Binder});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.5729167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldFooterTotal,
            this.fldFooterPrin,
            this.fldFooterTax,
            this.fldFooterCost,
            this.fldFooterCHGINT,
            this.fldFooterTaxTotal,
            this.fldFooterPLI,
            this.lnFooter});
			this.GroupFooter1.Height = 0.3125F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3125F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
			this.lblHeader.Text = "Loadback Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.8125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.25F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.8125F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.6875F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.8125F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.6875F;
			// 
			// lblAcct
			// 
			this.lblAcct.Height = 0.1875F;
			this.lblAcct.HyperLink = null;
			this.lblAcct.Left = 0F;
			this.lblAcct.Name = "lblAcct";
			this.lblAcct.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblAcct.Text = "Account";
			this.lblAcct.Top = 0.375F;
			this.lblAcct.Width = 0.75F;
			// 
			// lblPrin
			// 
			this.lblPrin.Height = 0.1875F;
			this.lblPrin.HyperLink = null;
			this.lblPrin.Left = 1.875F;
			this.lblPrin.Name = "lblPrin";
			this.lblPrin.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblPrin.Text = "Principal";
			this.lblPrin.Top = 0.375F;
			this.lblPrin.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5625F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 7.5F;
			this.Line1.X2 = 0F;
			this.Line1.Y1 = 0.5625F;
			this.Line1.Y2 = 0.5625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.75F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left; ddo-char-set: 0";
			this.lblName.Text = "Current Owner";
			this.lblName.Top = 0.375F;
			this.lblName.Width = 1.125F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.1875F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblReportType.Text = "Non Lien";
			this.lblReportType.Top = 0.0625F;
			this.lblReportType.Width = 7.5F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 2.8125F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.375F;
			this.lblTax.Width = 0.9375F;
			// 
			// lblCost
			// 
			this.lblCost.Height = 0.1875F;
			this.lblCost.HyperLink = null;
			this.lblCost.Left = 3.75F;
			this.lblCost.Name = "lblCost";
			this.lblCost.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblCost.Text = "Cost";
			this.lblCost.Top = 0.375F;
			this.lblCost.Width = 0.9375F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 6.5F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.375F;
			this.lblTotal.Width = 1F;
			// 
			// lblPLI
			// 
			this.lblPLI.Height = 0.1875F;
			this.lblPLI.HyperLink = null;
			this.lblPLI.Left = 4.6875F;
			this.lblPLI.Name = "lblPLI";
			this.lblPLI.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblPLI.Text = "PLI";
			this.lblPLI.Top = 0.375F;
			this.lblPLI.Width = 0.9375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 5.625F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.Label1.Text = "Charged Int";
			this.Label1.Top = 0.375F;
			this.Label1.Width = 0.875F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.0625F;
			this.Binder.Left = 4.28125F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.28125F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.8125F;
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.75F;
			// 
			// fldPrin
			// 
			this.fldPrin.Height = 0.1875F;
			this.fldPrin.Left = 1.875F;
			this.fldPrin.Name = "fldPrin";
			this.fldPrin.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldPrin.Text = null;
			this.fldPrin.Top = 0.1875F;
			this.fldPrin.Width = 0.9375F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.75F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 5.25F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 2.8125F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTax.Text = null;
			this.fldTax.Top = 0.1875F;
			this.fldTax.Width = 0.9375F;
			// 
			// fldCost
			// 
			this.fldCost.Height = 0.1875F;
			this.fldCost.Left = 3.75F;
			this.fldCost.Name = "fldCost";
			this.fldCost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldCost.Text = null;
			this.fldCost.Top = 0.1875F;
			this.fldCost.Width = 0.9375F;
			// 
			// fldCHGINT
			// 
			this.fldCHGINT.Height = 0.1875F;
			this.fldCHGINT.Left = 5.625F;
			this.fldCHGINT.Name = "fldCHGINT";
			this.fldCHGINT.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldCHGINT.Text = null;
			this.fldCHGINT.Top = 0.1875F;
			this.fldCHGINT.Width = 0.875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.5F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0.1875F;
			this.fldTotal.Width = 1F;
			// 
			// fldPLI
			// 
			this.fldPLI.Height = 0.1875F;
			this.fldPLI.Left = 4.6875F;
			this.fldPLI.Name = "fldPLI";
			this.fldPLI.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldPLI.Text = null;
			this.fldPLI.Top = 0.1875F;
			this.fldPLI.Width = 0.9375F;
			// 
			// fldRK
			// 
			this.fldRK.Height = 0.1875F;
			this.fldRK.Left = 1.125F;
			this.fldRK.Name = "fldRK";
			this.fldRK.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 0";
			this.fldRK.Text = null;
			this.fldRK.Top = 0.1875F;
			this.fldRK.Width = 0.75F;
			// 
			// fldBillNumberLabel
			// 
			this.fldBillNumberLabel.Height = 0.1875F;
			this.fldBillNumberLabel.Left = 0.375F;
			this.fldBillNumberLabel.Name = "fldBillNumberLabel";
			this.fldBillNumberLabel.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldBillNumberLabel.Text = "Bill:";
			this.fldBillNumberLabel.Top = 0.1875F;
			this.fldBillNumberLabel.Width = 0.75F;
			// 
			// fldFooterTotal
			// 
			this.fldFooterTotal.Height = 0.1875F;
			this.fldFooterTotal.Left = 0F;
			this.fldFooterTotal.Name = "fldFooterTotal";
			this.fldFooterTotal.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.fldFooterTotal.Text = null;
			this.fldFooterTotal.Top = 0.125F;
			this.fldFooterTotal.Width = 1.875F;
			// 
			// fldFooterPrin
			// 
			this.fldFooterPrin.Height = 0.1875F;
			this.fldFooterPrin.Left = 1.875F;
			this.fldFooterPrin.Name = "fldFooterPrin";
			this.fldFooterPrin.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldFooterPrin.Text = null;
			this.fldFooterPrin.Top = 0.125F;
			this.fldFooterPrin.Width = 0.9375F;
			// 
			// fldFooterTax
			// 
			this.fldFooterTax.Height = 0.1875F;
			this.fldFooterTax.Left = 2.8125F;
			this.fldFooterTax.Name = "fldFooterTax";
			this.fldFooterTax.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldFooterTax.Text = null;
			this.fldFooterTax.Top = 0.125F;
			this.fldFooterTax.Width = 0.9375F;
			// 
			// fldFooterCost
			// 
			this.fldFooterCost.Height = 0.1875F;
			this.fldFooterCost.Left = 3.75F;
			this.fldFooterCost.Name = "fldFooterCost";
			this.fldFooterCost.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldFooterCost.Text = null;
			this.fldFooterCost.Top = 0.125F;
			this.fldFooterCost.Width = 0.9375F;
			// 
			// fldFooterCHGINT
			// 
			this.fldFooterCHGINT.Height = 0.1875F;
			this.fldFooterCHGINT.Left = 5.625F;
			this.fldFooterCHGINT.Name = "fldFooterCHGINT";
			this.fldFooterCHGINT.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldFooterCHGINT.Text = null;
			this.fldFooterCHGINT.Top = 0.125F;
			this.fldFooterCHGINT.Width = 0.875F;
			// 
			// fldFooterTaxTotal
			// 
			this.fldFooterTaxTotal.Height = 0.1875F;
			this.fldFooterTaxTotal.Left = 6.5F;
			this.fldFooterTaxTotal.Name = "fldFooterTaxTotal";
			this.fldFooterTaxTotal.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldFooterTaxTotal.Text = null;
			this.fldFooterTaxTotal.Top = 0.125F;
			this.fldFooterTaxTotal.Width = 1F;
			// 
			// fldFooterPLI
			// 
			this.fldFooterPLI.Height = 0.1875F;
			this.fldFooterPLI.Left = 4.6875F;
			this.fldFooterPLI.Name = "fldFooterPLI";
			this.fldFooterPLI.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.fldFooterPLI.Text = null;
			this.fldFooterPLI.Top = 0.125F;
			this.fldFooterPLI.Width = 0.9375F;
			// 
			// lnFooter
			// 
			this.lnFooter.Height = 0F;
			this.lnFooter.Left = 1.875F;
			this.lnFooter.LineWeight = 1F;
			this.lnFooter.Name = "lnFooter";
			this.lnFooter.Top = 0.0625F;
			this.lnFooter.Width = 5.625F;
			this.lnFooter.X1 = 1.875F;
			this.lnFooter.X2 = 7.5F;
			this.lnFooter.Y1 = 0.0625F;
			this.lnFooter.Y2 = 0.0625F;
			// 
			// srptLoadbackReport
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPLI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCHGINT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPLI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRK)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillNumberLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterPrin)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterCHGINT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterTaxTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooterPLI)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCHGINT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLI;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRK;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillNumberLabel;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrin;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPLI;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterPrin;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterCHGINT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterTaxTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooterPLI;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnFooter;
	}
}
