﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTLienDischargeGetAcct.
	/// </summary>
	public partial class frmUTLienDischargeGetAcct : BaseForm
	{
		public frmUTLienDischargeGetAcct()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTLienDischargeGetAcct InstancePtr
		{
			get
			{
				return (frmUTLienDischargeGetAcct)Sys.GetInstance(typeof(frmUTLienDischargeGetAcct));
			}
		}

		protected frmUTLienDischargeGetAcct _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/16/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/20/2005              *
		// ********************************************************
		clsDRWrapper rsYear = new clsDRWrapper();
		string strWS = "";

		private void chkAllNotices_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkAllNotices.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkReprint.CheckState = Wisej.Web.CheckState.Unchecked;
				fraSingle.Enabled = false;
			}
			else
			{
				if (chkReprint.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					fraSingle.Enabled = true;
				}
			}
		}

		private void chkReprint_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkReprint.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkAllNotices.CheckState = Wisej.Web.CheckState.Unchecked;
				fraSingle.Enabled = false;
			}
			else
			{
				if (chkAllNotices.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					fraSingle.Enabled = true;
				}
			}
		}

		private void cmbYear_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// If KeyAscii = 13 And cmbYear.ListIndex >= 0 Then
			// PrintDischargeNotice
			// End If
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void cmbYear_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If cmbYear.ListIndex >= 0 Then
			// PrintDischargeNotice
			// End If
		}

		private void frmUTLienDischargeGetAcct_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (KeyCode == Keys.Escape)
				{
					this.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Prepayment Keydown Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmUTLienDischargeGetAcct_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTLienDischargeGetAcct properties;
			//frmUTLienDischargeGetAcct.ScaleWidth	= 3885;
			//frmUTLienDischargeGetAcct.ScaleHeight	= 2460;
			//frmUTLienDischargeGetAcct.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Left = FCConvert.ToInt32((FCGlobal.Screen.Width - this.Width) / 2.0);
			this.Top = FCConvert.ToInt32((FCGlobal.Screen.Height - this.Height) / 2.0);
			if (modUTStatusPayments.Statics.TownService == "B")
			{
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = true;
			}
			else if (modUTStatusPayments.Statics.TownService == "W")
			{
				cmbWS.Clear();
				cmbWS.Items.Add("Water");
				cmbWS.Text = "Water";
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				cmbWS.Clear();
				cmbWS.Items.Add("Sewer");
				cmbWS.Text = "Sewer";
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (chkAllNotices.CheckState == Wisej.Web.CheckState.Checked)
			{
				PrintDischargeNotice_2(true);
			}
			else if (chkReprint.CheckState == Wisej.Web.CheckState.Checked)
			{
				PrintDischargeNotice_8(true, true);
			}
			else
			{
				if (Conversion.Val(txtAcct.Text) > 0)
				{
					if (cmbYear.SelectedIndex >= 0)
					{
						PrintDischargeNotice_2(false);
					}
				}
			}
		}

		private void optWS_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtAcct.Text) != 0)
			{
				txtAcct_Validate(false);
			}
		}

		private void optWS_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbWS.SelectedIndex;
			optWS_CheckedChanged(index, sender, e);
		}

		private void txtAcct_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
                //FC:FINAL:AM:#4059 - don't call validate because it will be triggered twice
                //txtAcct_Validate(false);
                Support.SendKeys("{TAB}");
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAcct_Validating_2(bool Cancel)
		{
			txtAcct_Validating(new object(), new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtAcct_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngAcct;
				cmbYear.Enabled = false;
				cmbYear.Clear();
				if (cmbWS.Text == "Water")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAcct.Text)));
				if (lngAcct > 0)
				{
					// fill the cmbyear
					rsYear.OpenRecordset("SELECT Lien.RateKey, Lien.ID AS LienKey FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE Bill.ID = " + strWS + "CombinationLienKey AND AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAcct)), modExtraModules.strUTDatabase);
					if (!rsYear.EndOfFile())
					{
						while (!rsYear.EndOfFile())
						{
							//cmbYear.AddItem(rsYear.Get_Fields("RateKey"));
							// FC:FINAL:VGE - #i932 Type conversion
							cmbYear.AddItem(FCConvert.ToString(rsYear.Get_Fields_Int32("RateKey")));
							cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsYear.Get_Fields_Int32("RateKey")));
							rsYear.MoveNext();
						}
					}
					else
					{
						MessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " does not have any valid liened years.", "No Liens", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						// Cancel = True
						if (strWS == "W")
						{
							cmbWS.Text = "Water";
						}
						else
						{
							cmbWS.Text = "Sewer";
						}
						return;
					}
				}
				else
				{
					MessageBox.Show("Please enter a valid account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// Cancel = True
					return;
				}
				if (cmbYear.Items.Count > 0)
				{
					// if there are valid years then
					e.Cancel = false;
					// enable the cmbyear
					cmbYear.Enabled = true;
					// set the focus to the combo box
					cmbYear.SelectedIndex = 0;
					if (cmbYear.Visible)
					{
						cmbYear.Focus();
					}
				}
				else
				{
					// if there are no valid years then
					MessageBox.Show("No lien eligible for a Lien Discharge Notice.", "No Liens", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void txtAcct_Validate(bool Cancel)
		{
			txtAcct_Validating(txtAcct, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void PrintDischargeNotice_2(bool boolAll, bool boolArchive = false)
		{
			PrintDischargeNotice(ref boolAll, boolArchive);
		}

		private void PrintDischargeNotice_8(bool boolAll, bool boolArchive = false)
		{
			PrintDischargeNotice(ref boolAll, boolArchive);
		}

		private void PrintDischargeNotice(ref bool boolAll, bool boolArchive = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (cmbWS.Text == "Water")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				if (boolAll)
				{
					if (!boolArchive)
					{
						frmUTLienDischargeNotice.InstancePtr.Init(DateTime.Today, FCConvert.CBool(strWS == "W"), 0, true);
						//FC:FINAL:BSE #4060 leave form open
                        //this.Unload();
					}
					else
					{
						frmUTLienDischargeNotice.InstancePtr.Init(DateTime.Today, FCConvert.CBool(strWS == "W"), 0, true, false, true);
						this.Unload();
					}
				}
				else
				{
					rsYear.FindFirstRecord("RateKey", cmbYear.ItemData(cmbYear.SelectedIndex));
					if (!rsYear.NoMatch)
					{
						// found it
						frmUTLienDischargeNotice.InstancePtr.Init(DateTime.Today, FCConvert.CBool(strWS == "W"), rsYear.Get_Fields_Int32("LienKey"));
						this.Unload();
					}
				}
				return;
			}
			catch
			{
				
			}
		}

		private void fraSingle_Click(object sender, EventArgs e)
		{
			// FC:FINAL:VGE - #932 Forcing validating on click (to bypass combobox being disabled)
			txtAcct_Validate(false);
		}
	}
}
