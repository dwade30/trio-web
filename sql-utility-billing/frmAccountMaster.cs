﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;
using TWSharedLibrary;
using SharedApplication;
using SharedApplication.CentralDocuments;
using SharedApplication.UtilityBilling.Commands;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmAccountMaster.
	/// </summary>
	public partial class frmAccountMaster : BaseForm
	{
		public frmAccountMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblBook = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblBook.AddControlArrayElement(lblBook_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            mnuAutoPay.Click += MnuAutoPay_Click;
            txtTenant2CustNum.AllowOnlyNumericInput();
            txtTenant1CustNum.AllowOnlyNumericInput();
            txtOwner2CustNum.AllowOnlyNumericInput();
            txtOwner1CustNum.AllowOnlyNumericInput();
        }


        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmAccountMaster InstancePtr
		{
			get
			{
				return (frmAccountMaster)Sys.GetInstance(typeof(frmAccountMaster));
			}
		}

		protected frmAccountMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/10/2007              *
		// ********************************************************
		clsDRWrapper rsCurrentAccount = new clsDRWrapper();
		// main recordset holding the account information
		int lngKey;
		// this is the key to the account that is being looked at
		bool boolDirty;
		// if this is true then the user has made changes to this account/meter and needs to be saved
		bool boolSameBillToOwner;
		string strRateComboList = "";
		// this is the string that the grid combo boxes will use to show all of the rate tables
		string strRateTypeComboList = "";
		// this is the string that the grid combo boxes will use to show all of the rate types to choose from
		string strRateTypeComboList2 = "";
		// this is the string that the grid combo boxes will use to show all of the rate types to choose from
		short intAction;
		// this is which screen it is on 0 - Master Screen, 1 - Meter Screen
		bool boolMeterWaterService;
		bool boolMeterSewerService;
		bool boolNoSaveMeter;
		// if this is true then do not save the new info because another meter is being loaded
		int lngCurrentMeterNumber;
		// this will aid in saving info to the meterkeyarray
		bool boolLoaded;
		bool boolChangingRowCol;
		string strOwnerName = "";
		// Hold the owner's name in case of a name change
		string strTenantName = "";
		// kk01202016 trout-936
		public bool boolDoNotReturnToSearch;
		clsGridAccount clsWAcct = new clsGridAccount();
		clsGridAccount clsSAcct = new clsGridAccount();
		clsGridAccount clsWCat = new clsGridAccount();
		clsGridAccount clsSCat = new clsGridAccount();
		public bool blnReturntoSearch;
		// Grid Variables
		int lngColMasterHeading;
		int lngColMasterData;
		int lngColMasterHidden;
		
		int lngRowREMapLot;
		int lngRowREAccount;
		int lngRowREBookPage;
		int lngRowMeterTopHeader;
		int lngRowMeterHeading;
		int lngRowMeterPayment1;
		int lngRowMeterPayment2;
		int lngRowMeterPayment3;
		int lngRowMeterPayment4;
		int lngRowMeterPayment5;
		int lngRowMeterAdjust;
	
		int lngColMeterCheck;
		int lngColMeterWTitle;
		int lngColMeterWType;
		int lngColMeterWHidden;
		int lngColMeterWAmount;
		int lngColMeterWRateTable;
		int lngColMeterWAccount;
		int lngColMeterSTitle;
		int lngColMeterSType;
		int lngColMeterSHidden;
		int lngColMeterSAmount;
		int lngColMeterSRateTable;
		int lngColMeterSAccount;
		int lngColChangePrevID;
		int lngColChangeAcctKey;
		int lngColChangeMeterID;
		int lngColChangeSetDate;
		int lngColChangeSerial;
		int lngColChangeRemote;
		int lngColChangeSize;
		int lngColChangeDigits;
		int lngColChangeMult;
		int lngColChangeFinal;
		int lngColChangeReason;
		// Central Party Stuff
		cParty oOwner1 = new cParty();
		cParty oOwner2 = new cParty();
		cParty oTenant1 = new cParty();
		cParty oTenant2 = new cParty();
        private cPartyController partCont = new cPartyController();
		// this is an array that will hold all of the meter information...
		modMain.MeterKey[] MeterKeyArray = new modMain.MeterKey[99 + 1];

		public void Init(int lngKeyNumber, int lngPassNewAccount = 0, bool boolPassDoNotReturnToSearch = false)
		{
			int lngErrCode = 0;
			try
			{
				// lngAccountNumber is the account that the user selected on the GetMasterAccount screen
				int lngNewAccount = 0;
				bool boolNewAccount = false;
				boolDoNotReturnToSearch = boolPassDoNotReturnToSearch;
				modGlobalConstants.Statics.blnAddCancel = false;
				// Reset
				lngErrCode = 3;
				if (lngKeyNumber == -1)
				{
					// this is a new account
					boolNewAccount = true;
					rsCurrentAccount.OpenRecordset("SELECT Max(AccountNumber) AS AccountNumber FROM Master", modExtraModules.strUTDatabase);
					if (!rsCurrentAccount.EndOfFile())
					{
						lngErrCode = 4;
						// rsCurrentAccount.MoveLast
						lngErrCode = 5;
						if (lngPassNewAccount != 0)
						{
							lngNewAccount = lngPassNewAccount;
						}
						else
						{
							lngNewAccount = rsCurrentAccount.Get_Fields_Int32("AccountNumber") + 1;
						}
						lngErrCode = 6;
					}
					else
					{
						lngErrCode = 7;
						lngNewAccount = 1;
					}
					frmWait.InstancePtr.Unload();
					lngNewAccount = frmNewUTAccountNumber.InstancePtr.Init(ref lngNewAccount);
					// MAL@20070904: Check for cancelled add and exit form if cancelled
					if (!modGlobalConstants.Statics.blnAddCancel)
					{
						lngErrCode = 8;
						// kk11202014  FindFirst is too slow with large data sets
						rsCurrentAccount.OpenRecordset("SELECT * FROM Master WHERE ID = -1", modExtraModules.strUTDatabase);
						rsCurrentAccount.AddNew();
						// this will add a new account to the DB
						lngErrCode = 9;
						modUTStatusPayments.Statics.lngCurrentAccountUT = lngNewAccount;
						lngErrCode = 10;
						rsCurrentAccount.Set_Fields("DateOfChange", DateTime.Today);
						// date changed/created
						rsCurrentAccount.Set_Fields("AccountNumber", lngNewAccount);
						// set the account number (this can be changed by the user)
						lngErrCode = 11;
						rsCurrentAccount.Update();
						// save the new account
						lngErrCode = 12;
						lngKey = rsCurrentAccount.Get_Fields_Int32("ID");
					}
					else
					{
						blnReturntoSearch = true;
						this.Unload();
						return;
					}
				}
				else
				{
					lngKey = lngKeyNumber;
				}
				lngErrCode = 13;
				rsCurrentAccount.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
				lngErrCode = 14;
				if (rsCurrentAccount.EndOfFile())
				{
					lngErrCode = 15;
					MessageBox.Show("Error loading account with key " + FCConvert.ToString(lngKey) + ".", "Error Loading Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					this.Unload();
					return;
				}
				if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
				{
					if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("UseREAccount")))
					{
						lngErrCode = 1000;
						if (!modMain.UpdateUTAccountwithREInfo_6(lngKeyNumber, rsCurrentAccount.Get_Fields_Int32("REAccount")))
						{
							frmWait.InstancePtr.Unload();
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							MessageBox.Show("Cound not update RE location information for account " + rsCurrentAccount.Get_Fields_Int32("AccountNumber") + ".  Please verify that the information is correct.", "Error Updaing RE Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
                        else
                        {
                            rsCurrentAccount.OpenRecordset("Select * from master where id = " + lngKey,
                                "UtilityBilling");
                        }
						lngErrCode = 1001;
						// rsCurrentAccount.Refresh
					}
					// End If
				}
				this.LoadForm();
				this.Text = "Account " + rsCurrentAccount.Get_Fields_Int32("AccountNumber") + " Master";
				lngErrCode = 16;
				//FC:FINAL:DSE:#i1125 Code should be executed before other loads
				LoadCombos();
				// this will load all of the account information
				// FormatMasterGrid True
				lngErrCode = 17;
				// FormatREGrid True
				lngErrCode = 18;
				FillMasterScreen(boolNewAccount);
				lngErrCode = 19;
				FillMeterArray();
				lngErrCode = 20;
				LoadMeterCombo();
				lngErrCode = 21;
				SetMasterConstants();
				FormatMeterGrid_2(true);
				lngErrCode = 22;
				FormatReadingGrid_2(true);
				lngErrCode = 23;
				FormatPercentageGrid_2(true);
				lngErrCode = 24;
				boolDirty = false;
				frmWait.InstancePtr.Unload();
				lngErrCode = 25;
				// show the form
				this.Show(App.MainForm);
				lngErrCode = 26;
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Account Master - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Unload();
			}
		}

		private void chkBillSameAsOwner_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked)
			{
			}
			else
			{
				if (!(oOwner1 == null))
				{
					txtTenant1CustNum.Text = txtOwner1CustNum.Text;
					oTenant1 = modMain.Statics.gCPCtlr.GetParty(oOwner1.ID);
				}
				if (!(oOwner2 == null))
				{
					txtTenant2CustNum.Text = txtOwner2CustNum.Text;
					oTenant2 = modMain.Statics.gCPCtlr.GetParty(oOwner2.ID);
				}
				ShowPartyInfo_2("T");
			}
			// k DISABLE THE Party Lookup Fields
			txtTenant1CustNum.Enabled = FCConvert.CBool(chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked);
			cmdTenant1Search.Enabled = FCConvert.CBool(chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked);
			cmdTenant1Edit.Enabled = FCConvert.CBool(chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked);
			imgTenant1Memo.Enabled = FCConvert.CBool(chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked);
			txtTenant2CustNum.Enabled = FCConvert.CBool(chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked);
			cmdTenant2Search.Enabled = FCConvert.CBool(chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked);
			cmdTenant2Edit.Enabled = FCConvert.CBool(chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked);
			imgTenant2Memo.Enabled = FCConvert.CBool(chkBillSameAsOwner.CheckState != Wisej.Web.CheckState.Checked);
			boolDirty = true;
		}

		private void chkFinal_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkIncludeInExtract_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			MeterKeyArray[lngCurrentMeterNumber].IncludeInExtract = FCConvert.CBool(chkIncludeInExtract.CheckState == Wisej.Web.CheckState.Checked);
		}

		private void chkMeterFinalBill_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			MeterKeyArray[lngCurrentMeterNumber].FinalBilled = FCConvert.CBool(chkMeterFinalBill.CheckState == Wisej.Web.CheckState.Checked);
		}

		private void chkMeterNoBill_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			MeterKeyArray[lngCurrentMeterNumber].NoBill = FCConvert.CBool(chkMeterNoBill.CheckState == Wisej.Web.CheckState.Checked);
		}

		private void chkNegativeConsumption_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			MeterKeyArray[lngCurrentMeterNumber].NegativeConsumption = FCConvert.CBool(chkNegativeConsumption.CheckState == Wisej.Web.CheckState.Checked);
		}

		private void chkNoBill_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkUseRE_CheckedChanged(object sender, System.EventArgs e)
		{
			// get information from the re account
			int lngAcct = 0;
			if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
			{
				// if the user has real estate
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtREAcct.Text)));
				// Val(vsREInfo.TextMatrix(lngRowREAccount, lngColMasterData))   'get the account number
				if (lngAcct != 0)
				{
					// kk09132017 trouts-239  Skip filling in values if RE Acct is not set
					if (chkUseRE.CheckState != Wisej.Web.CheckState.Checked)
					{
						// this means that it is being checked
						LoadREInfo_6(lngAcct, false);
                        EnableREInfo();
                    }
					else
					{
						LoadREInfo_6(lngAcct, true);
						chkUseREMortgage.CheckState = Wisej.Web.CheckState.Checked;
                        DisableREInfo();
                    }
				}
				boolDirty = true;
			}
			else
			{
				// fraREInfo.Enabled = False
			}
		}

		private void chkUseREMortgage_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkUseREMortgage.CheckState == Wisej.Web.CheckState.Unchecked && chkUseRE.CheckState == Wisej.Web.CheckState.Checked)
			{
				chkUseREMortgage.CheckState = Wisej.Web.CheckState.Checked;
				MessageBox.Show("You must uncheck the option to get infor from RE before you may uncheck this option.", "Invalid Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			boolDirty = true;
		}

		private void cmbBook_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbBook_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBK = new clsDRWrapper();
				string strStatus = "";
				int intCT;
				if (!boolNoSaveMeter)
				{
					if (cmbBook.SelectedIndex >= 0)
					{
						if (MeterKeyArray[lngCurrentMeterNumber].BookNumber != 0)
						{
							// check to see if the meter is a different status that the book that it is being put into
							rsBK.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(cmbBook.ItemData(cmbBook.SelectedIndex)), modExtraModules.strUTDatabase);
							if (!rsBK.EndOfFile())
							{
								strStatus = Strings.Left(FCConvert.ToString(rsBK.Get_Fields_String("CurrentStatus")), 1);
								if (strStatus == MeterKeyArray[lngCurrentMeterNumber].BillingStatus || MeterKeyArray[lngCurrentMeterNumber].BillingStatus == "")
								{
									// allow the change if the book status is the same
									MeterKeyArray[lngCurrentMeterNumber].BookNumber = cmbBook.ItemData(cmbBook.SelectedIndex);
								}
								else
								{
									// do not allow the change
									MessageBox.Show("This meter (" + MeterKeyArray[lngCurrentMeterNumber].BillingStatus + ") cannot be moved to book #" + FCConvert.ToString(cmbBook.ItemData(cmbBook.SelectedIndex)) + "(" + strStatus + ") because the status does not match", "Cannot Move Book", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									// reset the combo box
									for (intCT = 0; intCT <= cmbBook.Items.Count - 1; intCT++)
									{
										if (MeterKeyArray[lngCurrentMeterNumber].BookNumber == Conversion.Val(cmbBook.Items[intCT].ToString()))
										{
											cmbBook.SelectedIndex = intCT;
										}
									}
									return;
								}
							}
							else
							{
								// book could not be found no change in the meter record
								MessageBox.Show("Book #" + FCConvert.ToString(cmbBook.ItemData(cmbBook.SelectedIndex)) + " could not be found.", "Missing Book", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
						}
						else
						{
							// allow the change if no book was selected before
							MeterKeyArray[lngCurrentMeterNumber].BookNumber = cmbBook.ItemData(cmbBook.SelectedIndex);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Changing Book", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmbBook_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbBook.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbBook_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbBook.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbBook.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbCombineMeter_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbCombineMeter_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (cmbCombineMeter.SelectedIndex >= 0)
				{
					MeterKeyArray[lngCurrentMeterNumber].Combine = Strings.Left(cmbCombineMeter.Items[cmbCombineMeter.SelectedIndex].ToString(), 1);
				}
			}
		}

		private void cmbCombineMeter_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbCombineMeter.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbCombineMeter.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbFrequency_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbFrequency_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (cmbFrequency.SelectedIndex >= 0)
				{
					MeterKeyArray[lngCurrentMeterNumber].Frequency = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbFrequency.Items[cmbFrequency.SelectedIndex].ToString(), 3))));
				}
			}
		}

		private void cmbFrequency_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbFrequency.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbFrequency_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbFrequency.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbFrequency.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbMeterDigits_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbMeterDigits_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (cmbMeterDigits.SelectedIndex >= 0)
				{
					MeterKeyArray[lngCurrentMeterNumber].Digits = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbMeterDigits.Items[cmbMeterDigits.SelectedIndex].ToString())));
				}
			}
		}

		private void cmbMeterDigits_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbMeterDigits.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbMeterDigits.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbMeterNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbMeterNumber_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// this will load the current meter information selected
			LoadMeterInformation_2(FCConvert.ToInt16(cmbMeterNumber.ItemData(cmbMeterNumber.SelectedIndex)));
			if (cmbMeterNumber.ItemData(cmbMeterNumber.SelectedIndex) > 1)
			{
				chkNegativeConsumption.Enabled = true;
				cmbCombineMeter.Enabled = true;
				if (MeterKeyArray[cmbMeterNumber.ItemData(cmbMeterNumber.SelectedIndex)].NegativeConsumption)
				{
					cmbCombineMeter.SelectedIndex = 1;
				}
			}
			else
			{
				chkNegativeConsumption.Enabled = false;
				cmbCombineMeter.Enabled = false;
			}
		}

		private void cmbMeterNumber_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbMeterNumber.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbMeterNumber.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbMeterSize_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbMeterSize_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (cmbMeterSize.SelectedIndex >= 0)
				{
					MeterKeyArray[lngCurrentMeterNumber].Size = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbMeterSize.Items[cmbMeterSize.SelectedIndex].ToString(), 2))));
				}
			}
		}

		private void cmbMeterSize_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbMeterSize.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 200, 0);
		}

		private void cmbMeterSize_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbMeterSize.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbMeterSize.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbMultiplier_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbMultiplier_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (cmbMultiplier.SelectedIndex >= 0)
				{
					MeterKeyArray[lngCurrentMeterNumber].Multiplier = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbMultiplier.Items[cmbMultiplier.SelectedIndex].ToString())));
				}
				else
				{
					MeterKeyArray[lngCurrentMeterNumber].Multiplier = 1;
				}
			}
		}

		private void cmbMultiplier_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbMultiplier.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbMultiplier.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbRadio_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			MeterKeyArray[lngCurrentMeterNumber].RadioAccessType = cmbRadio.SelectedIndex;
		}

		private void cmbRadio_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbRadio.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbSCat_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbSCat_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbSCat.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbSCat_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			string strAcct = "";
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbSCat.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbSCat.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.F2:
					{
						strAcct = frmLoadValidAccounts.InstancePtr.Init(Strings.Left(cmbSCat.Text, 2));
						if (strAcct != "")
						{
							cmbSCat.Text = strAcct;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbService_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbService_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (cmbService.SelectedIndex >= 0)
				{
					MeterKeyArray[lngCurrentMeterNumber].Service = Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1);
				}
			}
		}

		private void cmbService_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbService.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbService.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbService_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			int lngCT;
			for (lngCT = 2; lngCT <= vsMeter.Rows - 1; lngCT++)
			{
				SetMeterRow_6(lngCT, true);
			}
		}

		private void cmbWCat_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void cmbWCat_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cmbWCat.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void cmbWCat_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			string strAcct = "";
			switch (KeyCode)
			{
				case Keys.Space:
					{
						if (modAPIsConst.SendMessageByNum(cmbWCat.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbWCat.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
							KeyCode = (Keys)0;
						}
						break;
					}
				case Keys.F2:
					{
						strAcct = frmLoadValidAccounts.InstancePtr.Init(Strings.Left(cmbWCat.Text, 2));
						if (strAcct != "")
						{
							cmbWCat.Text = strAcct;
						}
						break;
					}
			}
			//end switch
		}

		private void cmbRadio_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
			MeterKeyArray[lngCurrentMeterNumber].RadioAccessType = cmbRadio.SelectedIndex;
		}

		private void cmdChangeOutClose_Click(object sender, System.EventArgs e)
		{
			fraChangeOutHistory.Visible = false;
			fraMeter.Visible = true;
		}

		private void cmdOwner1Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID = 0;
			cmdOwner1Edit.Enabled = false;
			// kk01232015 trouts-134  Prevent the code from running twice
			if (Information.IsNumeric(txtOwner1CustNum.Text) && Conversion.Val(txtOwner1CustNum.Text) > 0)
			{
				// kk01232015 trouts-134  And prevent Type mismatch
				lngID = FCConvert.ToInt32(txtOwner1CustNum.Text);
				lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
				if (lngID > 0)
				{
					txtOwner1CustNum.Text = FCConvert.ToString(lngID);
					oOwner1 = modMain.Statics.gCPCtlr.GetParty(lngID);
				}
				else
				{
					oOwner1 = null;
				}
			}
			ShowPartyInfo_2("O");
		}

		private void cmdOwner2Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID = 0;
			cmdOwner2Edit.Enabled = false;
			// kk01232015 trouts-134  Prevent the code from running twice
			if (Information.IsNumeric(txtOwner2CustNum.Text) && Conversion.Val(txtOwner2CustNum.Text) > 0)
			{
				// kk01232015 trouts-134  And prevent Type mismatch
				lngID = FCConvert.ToInt32(txtOwner2CustNum.Text);
				lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
				if (lngID > 0)
				{
					oOwner2 = modMain.Statics.gCPCtlr.GetParty(lngID);
				}
				else
				{
					oOwner2 = null;
				}
			}
			ShowPartyInfo_2("O");
		}

		private void cmdTenant1Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID = 0;
			cmdTenant1Edit.Enabled = false;
			// kk01232015 trouts-134  Prevent the code from running twice
			if (Information.IsNumeric(txtTenant1CustNum.Text) && Conversion.Val(txtTenant1CustNum.Text) > 0)
			{
				// kk01232015 trouts-134  And prevent Type mismatch
				lngID = FCConvert.ToInt32(txtTenant1CustNum.Text);
				lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
				if (lngID > 0)
				{
					oTenant1 = modMain.Statics.gCPCtlr.GetParty(lngID);
				}
				else
				{
					oTenant1 = null;
				}
			}
			ShowPartyInfo_2("T");
		}

		private void cmdTenant2Edit_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: lngID As int	OnWrite(string, int)
			int lngID = 0;
			cmdTenant2Edit.Enabled = false;
			// kk01232015 trouts-134  Prevent the code from running twice
			if (Information.IsNumeric(txtTenant2CustNum.Text) && Conversion.Val(txtTenant2CustNum.Text) > 0)
			{
				// kk01232015 trouts-134  And prevent Type mismatch
				lngID = FCConvert.ToInt32(txtTenant2CustNum.Text);
				lngID = frmEditCentralParties.InstancePtr.Init(ref lngID);
				if (lngID > 0)
				{
					oTenant2 = modMain.Statics.gCPCtlr.GetParty(lngID);
				}
				else
				{
					oTenant2 = null;
				}
			}
			ShowPartyInfo_2("T");
		}

		private void cmdOwner1Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
            int lngOrigId;
			lngID = frmCentralPartySearch.InstancePtr.Init();
            lngOrigId = Convert.ToInt32(txtOwner1CustNum.Text);
			if (lngID > 0)
			{
                if (lngID != lngOrigId)
                {
                    UpdateOwner(lngID);
                }
			}
		}

		private void cmdOwner2Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
            int lngOrigId;
			lngID = frmCentralPartySearch.InstancePtr.Init();
            lngOrigId = Convert.ToInt32(txtOwner2CustNum.Text);
			if (lngID > 0)
			{
                if (lngID != lngOrigId)
                {
                    UpdateSecondOwner(lngID);
                }
			}
		}

		private void cmdTenant1Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtTenant1CustNum.Text = FCConvert.ToString(lngID);
				oTenant1 = modMain.Statics.gCPCtlr.GetParty(lngID);
				ShowPartyInfo_2("T");
				boolDirty = true;
			}
		}

		private void cmdTenant2Search_Click(object sender, System.EventArgs e)
		{
			int lngID;
			lngID = frmCentralPartySearch.InstancePtr.Init();
			if (lngID > 0)
			{
				txtTenant2CustNum.Text = FCConvert.ToString(lngID);
				oTenant2 = modMain.Statics.gCPCtlr.GetParty(lngID);
				ShowPartyInfo_2("T");
				boolDirty = true;
			}
		}

		private void imgOwner1Memo_Click(object sender, System.EventArgs e)
		{
			frmComment.InstancePtr.Init("UT", FCConvert.ToInt32(FCConvert.ToDouble(txtOwner1CustNum.Text)), "Comment", "", 0, true);
			// kk01062014 trouts-119  Added
		}

		private void imgOwner2Memo_Click(object sender, System.EventArgs e)
		{
			frmComment.InstancePtr.Init("UT", FCConvert.ToInt32(FCConvert.ToDouble(txtOwner2CustNum.Text)), "Comment", "", 0, true);
			// kk01062014 trouts-119  Added
		}

		private void imgTenant1Memo_Click(object sender, System.EventArgs e)
		{
			frmComment.InstancePtr.Init("UT", FCConvert.ToInt32(FCConvert.ToDouble(txtTenant1CustNum.Text)), "Comment", "", 0, true);
			// kk01062014 trouts-119  Added
		}

		private void imgTenant2Memo_Click(object sender, System.EventArgs e)
		{
			frmComment.InstancePtr.Init("UT", FCConvert.ToInt32(FCConvert.ToDouble(txtTenant2CustNum.Text)), "Comment", "", 0, true);
			// kk01062014 trouts-119  Added
		}

		private void txtOwner1CustNum_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtOwner1CustNum_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtOwner1CustNum.Text) && Conversion.Val(txtOwner1CustNum.Text) > 0)
			{
				UpdateOwner(FCConvert.ToInt32(Conversion.Val(txtOwner1CustNum.Text)));
			}
            else
            {
                txtOwner1CustNum.Text = "";
                oOwner1 = null;

                ShowPartyInfo("O");
            }
        }

		private void txtOwner2CustNum_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtOwner2CustNum_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtOwner2CustNum.Text) && Conversion.Val(txtOwner2CustNum.Text) > 0)
			{				
                UpdateSecondOwner(Convert.ToInt32(txtOwner2CustNum.Text));
			}
            else
            {
                txtOwner2CustNum.Text = "";
                oOwner2 = null;

                ShowPartyInfo("O");
            }
        }

		private void txtTenant1CustNum_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTenant1CustNum_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtTenant1CustNum.Text) && Conversion.Val(txtTenant1CustNum.Text) > 0)
			{
				oTenant1 = modMain.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(Conversion.Val(txtTenant1CustNum.Text)));
			}
			else
			{
				txtTenant1CustNum.Text = "";
				oTenant1 = null;
			}
			ShowPartyInfo_2("T");
		}

		private void txtTenant2CustNum_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtTenant2CustNum_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Information.IsNumeric(txtTenant2CustNum.Text) && Conversion.Val(txtTenant2CustNum.Text) > 0)
			{
				oTenant2 = modMain.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(Conversion.Val(txtTenant2CustNum.Text)));
			}
			else
			{
				txtTenant2CustNum.Text = "";
				oTenant2 = null;
			}
			ShowPartyInfo_2("T");
		}

		private void frmAccountMaster_Activated(object sender, System.EventArgs e)
		{
			SetAct(ref intAction);
			if (Strings.Trim(modUTStatusPayments.GetUTAccountComment(ref lngKey)) != "")
			{
				lblAcctComment.Visible = true;
			}
			else
			{
				lblAcctComment.Visible = false;
			}
			//FC:FINAL:MSH - issue #909: set focus to the textbox (as in original)
			txtLocationNumber.Focus();
		}
		// vbPorter upgrade warning: intAct As short	OnWriteFCConvert.ToInt32(
		private void SetAct_2(short intAct)
		{
			SetAct(ref intAct);
		}

		private void SetAct(ref short intAct)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				switch (intAct)
				{
					case 0:
						{
							// Master Screen
							intAction = 0;

							fraMaster.Visible = true;
							// hide the meter frame
							fraMeter.Visible = false;
							// kk 03052013 trouts-6  Add field for Impervious Surface Area
							if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
							{
								lblImpervSurf.Visible = true;
								txtImpervSurf.Visible = true;
								txtImpervSurf.Enabled = true;
							}
							// hide/show the correct menu items
							mnuFileShowMeter.Text = "Show Meter Screen";
							cmdFileShowMeter.Text = "Show Meter Screen";
							cmdFileShowMeter.Width = 138;
							mnuFileMeterAdd.Visible = false;
							cmdFileMeterAdd.Visible = false;
							mnuFileMeterDelete.Visible = false;
							cmdFileMeterDelete.Visible = false;
							mnuFileMeterNext.Visible = false;
							cmdFileMeterNext.Visible = false;
							mnuFileMeterPrevious.Visible = false;
							cmdFileMeterPrevious.Visible = false;
							mnuFileResetMeterStatus.Visible = false;
							// seperator3.Visible = False
							mnuFileChangeOutHistory.Visible = false;
							// MAL@20080111
							break;
						}
					case 1:
						{
							// Meter Screen
							intAction = 1;
							fraMeter.Visible = true;
							if (cmbMeterNumber.SelectedIndex != -1 && cmbMeterNumber.Items.Count > 0)
							{
								LoadMeterInformation_2(FCConvert.ToInt16(cmbMeterNumber.ItemData(0)));
							}
							// hide the meter frame
							fraMaster.Visible = false;
							// hide/show the correct menu items
							mnuFileShowMeter.Text = "Show Master Screen";
							cmdFileShowMeter.Text = "Show Master Screen";
							cmdFileShowMeter.Width = 149;
							mnuFileMeterAdd.Visible = true;
							cmdFileMeterAdd.Visible = true;
							mnuFileMeterDelete.Visible = true;
							cmdFileMeterDelete.Visible = true;
							mnuFileMeterNext.Visible = true;
							cmdFileMeterNext.Visible = true;
							mnuFileMeterPrevious.Visible = true;
							cmdFileMeterPrevious.Visible = true;
							mnuFileResetMeterStatus.Visible = true;
							// seperator3.Visible = True
							mnuFileChangeOutHistory.Visible = true;
							if (HasMeterHistory())
							{
								mnuFileChangeOutHistory.Enabled = true;
							}
							else
							{
								mnuFileChangeOutHistory.Enabled = false;
							}
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Action", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
		}

		private void EmptyHandler_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
		}

		private void frmAccountMaster_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						if (fraFinalBillInfo.Visible)
						{
							fraFinalBillInfo.Visible = false;
							fraMeter.Visible = true;
						}
						else
						{
							//FC:FINAL:MSH - issue #921: call exit screen to give opportunity for saving data before closing the form
							//this.Unload();
							ExitScreen();
						}
						break;
					}
			}
			//end switch
		}

		private void frmAccountMaster_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.Escape:
					{
						//FC:FINAL:MSH - issue #921: move ExitScreen to frmAccountMaster_KeyDown handler
						// if escape is pressed unload the current form
						KeyAscii = (Keys)0;
						//ExitScreen();
						break;
					}
				case Keys.Return:
					{
						// return
						Support.SendKeys("{Tab}", false);
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmAccountMaster_Load(object sender, System.EventArgs e)
		{
			SetMasterConstants();
			// this will set all of the constants for the grids
			//FC:FINAL:MSH - issue #919: SetFixedSize not implemented, but in original on SetFixedSize call will be called Form_Resize, which initialize grids
			frmAccountMaster_Resize(this, new EventArgs());
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// set the size of the form
			modGlobalFunctions.SetTRIOColors(this);
			// set the TRIO Colors on the form
			if (modGlobalConstants.Statics.gboolBD)
			{
				modValidateAccount.SetBDFormats();
			}

            if (modGlobalConstants.Statics.MuniName.ToLower().StartsWith("brunswick se"))
            {
                txtLocationNumber.MaxLength = 10;
            }
			vsMeter.Editable = FCGrid.EditableSettings.flexEDKbdMouse;

			if (modUTStatusPayments.Statics.TownService == "B")
			{
				chkBillSameAsOwner.Text = "Tenant information same as owner (Combined Bill)";
			}
			else
			{
				chkBillSameAsOwner.Text = "Tenant information same as owner";
			}
			clsWAcct.GRID7Light = vsMeter;
			clsWAcct.AccountCol = FCConvert.ToInt16(lngColMeterWAccount);
			clsWAcct.DefaultAccountType = "R";
			clsSAcct.GRID7Light = vsMeter;
			clsSAcct.AccountCol = FCConvert.ToInt16(lngColMeterSAccount);
			clsSAcct.DefaultAccountType = "R";
			clsWCat.GRID7Light = txtWaterAcct;
			clsWCat.DefaultAccountType = "R";
			clsSCat.GRID7Light = txtSewerAcct;
			clsSCat.DefaultAccountType = "R";
			lblAcctComment.ForeColor = Color.Red;
			ToolTip1.SetToolTip(lblAcctComment, "This account has a comment.");
			lblBankruptcy.ForeColor = Color.Red;
			ToolTip1.SetToolTip(lblBankruptcy, "This account is in bankruptcy.");
			lblTaxAcquired.ForeColor = Color.Red;
			ToolTip1.SetToolTip(lblTaxAcquired, "This account has been tax acquired.");
			// kk trouts-6 02282013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				fraWater.Text = "Stormwater Account";
			}
			if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
			{
				// if the user has real estate or BL then turn the option to use this on
				chkUseRE.Enabled = true;
			}
			else
			{
				chkUseRE.Enabled = false;
				// fraREInfo.Enabled = False
				txtREAcct.Enabled = false;
				chkUseREMortgage.Enabled = false;
			}

            if (modUTCalculations.Statics.gUTSettings.AllowAutoPay)
            {
                mnuAutoPay.Visible = true;
            }
            else
            {
                mnuAutoPay.Visible = false;
            }
			boolLoaded = true;
		}
		
		private void FormatPercentageGrid_2(bool boolReset)
		{
			FormatPercentageGrid(boolReset);
		}

		private void FormatPercentageGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsPercentage.WidthOriginal;
			if (boolReset || boolLoaded == false)
			{
				vsPercentage.Rows = 0;
				vsPercentage.Cols = 3;
				vsPercentage.ExtendLastCol = true;
				// set up each rows label in the left column
				vsPercentage.Rows = 3;
				vsPercentage.FixedRows = 1;
				vsPercentage.FixedCols = 1;
				vsPercentage.TextMatrix(0, 0, "");
				vsPercentage.TextMatrix(1, 0, "Bill at % of Cons");
				vsPercentage.TextMatrix(2, 0, "Taxable %:");
				// kk trouts-6 03012013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					vsPercentage.TextMatrix(0, 1, "Stormwater");
				}
				else
				{
					vsPercentage.TextMatrix(0, 1, "Water");
				}
				vsPercentage.TextMatrix(0, 2, "Sewer");
				vsPercentage.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
			vsPercentage.ColWidth(0, FCConvert.ToInt32(lngWid * 0.49));
			vsPercentage.ColWidth(1, FCConvert.ToInt32(lngWid * 0.26));
			vsPercentage.ColWidth(2, FCConvert.ToInt32(lngWid * 0.24));
			//vsPercentage.Height = (vsPercentage.Rows * vsPercentage.RowHeight(0)) + 70;
		}

		private void FormatReadingGrid_2(bool boolReset)
		{
			FormatReadingGrid(boolReset);
		}

		private void FormatReadingGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsReading.WidthOriginal;
			if (boolReset || boolLoaded == false)
			{
				vsReading.Rows = 0;
				vsReading.Cols = 3;
				vsReading.ExtendLastCol = true;
				// set up each rows label in the left column
				vsReading.Rows = 6;
				vsReading.FixedRows = 1;
				vsReading.FixedCols = 1;
				vsReading.TextMatrix(0, 0, "");
				vsReading.TextMatrix(1, 0, "Backflow:");
				vsReading.TextMatrix(2, 0, "Previous:");
				vsReading.TextMatrix(3, 0, "Current:");
				vsReading.TextMatrix(4, 0, "Set Date:");
				vsReading.TextMatrix(5, 0, "Replace:");
				// .TextMatrix(6, 0) = "Location:"
				vsReading.TextMatrix(0, 1, "Read");
				vsReading.TextMatrix(0, 2, "Date");
				vsReading.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
			vsReading.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 4, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			// .MergeCells = flexMergeFree
			// .MergeRow(6) = True
			// .TextMatrix(6, 2) = .TextMatrix(6, 1)
			vsReading.ColWidth(0, FCConvert.ToInt32(lngWid * 0.35));
			vsReading.ColWidth(1, FCConvert.ToInt32(lngWid * 0.25));
			vsReading.ColWidth(2, FCConvert.ToInt32(lngWid * 0.39));
			// .Cell(FCGrid.CellPropertySettings.flexcpCustomFormat, 1, 1) = flexDTBoolean
			//vsReading.Height = FCConvert.ToInt32((vsReading.Rows * vsReading.RowHeight(0)) + (vsReading.RowHeight(0) * 0.1));
			vsReading.EditingControlShowing -= VsReading_EditingControlShowing;
			vsReading.EditingControlShowing += VsReading_EditingControlShowing;
            //FC:FINAL:CHN - issue #1570: Fix alignment of Grid Cell.
            vsReading.Cell( FCGrid.CellPropertySettings.flexcpAlignment, 3, 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
        }

		private void VsReading_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - issue #913: assign JavaScript handler for entering only numbers in appropriate cells as in original
				if (vsReading.Col == 1 && (vsReading.Row == 2 || vsReading.Row == 3 || vsReading.Row == 5))
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
                        box.AllowOnlyNumericInput();
                        box.RemoveAlphaCharactersOnValueChange();
					}
				}
				//FC:FINAL:MSH - issue #913: assign handler to the control
				e.Control.KeyDown -= vsReading_KeyDownEdit;
				e.Control.KeyDown += vsReading_KeyDownEdit;
			}
		}

		private void FormatMeterGrid_2(bool boolReset)
		{
			FormatMeterGrid(boolReset);
		}

		private void FormatMeterGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsMeter.WidthOriginal;
			if (boolReset || boolLoaded == false)
			{
				vsMeter.Rows = 0;
				vsMeter.Cols = 13;
				vsMeter.ExtendLastCol = true;
				vsMeter.EditMaxLength = 20;
				// set up each rows label in the left column
				vsMeter.Rows = 8;
				vsMeter.FixedRows = 2;
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWAmount, "Water");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWRateTable, "Water");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWAccount, "Water");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWTitle, "Water");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWType, "Water");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWHidden, "Water");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterSAmount, "Sewer");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterSRateTable, "Sewer");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterSAccount, "Sewer");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterSTitle, "Sewer");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterSType, "Sewer");
				vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterSHidden, "Sewer");
				// kk trouts-6 02282013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWAmount, "Stormwater");
					vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWRateTable, "Stormwater");
					vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWAccount, "Stormwater");
					vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWTitle, "Stormwater");
					vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWType, "Stormwater");
					vsMeter.TextMatrix(lngRowMeterTopHeader, lngColMeterWHidden, "Stormwater");
				}
				vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterPayment1, lngColMeterWTitle, lngRowMeterPayment1, vsMeter.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterPayment2, lngColMeterWTitle, lngRowMeterPayment2, vsMeter.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterPayment3, lngColMeterWTitle, lngRowMeterPayment3, vsMeter.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterPayment4, lngColMeterWTitle, lngRowMeterPayment4, vsMeter.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterPayment5, lngColMeterWTitle, lngRowMeterPayment5, vsMeter.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterAdjust, lngColMeterWTitle, 0, vsMeter.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterAdjustDescription, lngColMeterWTitle, , .Cols - 1) = TRIOCOLORGRAYBACKGROUND
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterAdjustDescription, lngColMeterCheck, , .Cols - 1) = TRIOCOLORGRAYBACKGROUND
				// .Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowMeterNoBill, lngColMeterTitle, lngRowMeterFinalBill, .Cols - 1) = TRIOCOLORGRAYBACKGROUND
				txtAdjustDescriptionS.Enabled = false;
				txtAdjustDescriptionW.Enabled = false;
				// merge the top row
				vsMeter.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
				vsMeter.MergeRow(lngRowMeterTopHeader, true);
				// merge the adjustment description line
				// .TextMatrix(lngRowMeterAdjustDescription, lngColMeterSHidden) = 0
				// .MergeRow(lngRowMeterAdjustDescription) = True
				// align the headers to the center
				//vsMeter.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRowMeterTopHeader, 0, lngRowMeterTopHeader, vsMeter.Rows - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
				// MAL@20070831: Added Heading for Type Columns
				vsMeter.TextMatrix(lngRowMeterHeading, lngColMeterWTitle, "Type");
				vsMeter.TextMatrix(lngRowMeterHeading, lngColMeterWAmount, "Amount");
				vsMeter.TextMatrix(lngRowMeterHeading, lngColMeterWRateTable, "Rate");
				vsMeter.TextMatrix(lngRowMeterHeading, lngColMeterWAccount, "Account");
				vsMeter.TextMatrix(lngRowMeterHeading, lngColMeterSTitle, "Type");
				vsMeter.TextMatrix(lngRowMeterHeading, lngColMeterSAmount, "Amount");
				vsMeter.TextMatrix(lngRowMeterHeading, lngColMeterSRateTable, "Rate");
				vsMeter.TextMatrix(lngRowMeterHeading, lngColMeterSAccount, "Account");
				// .TextMatrix(lngRowMeterNoBill, lngColMeterTitle) = "No Bill"
				// .TextMatrix(lngRowMeterFinalBill, lngColMeterTitle) = "Final Bill"
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterWTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterWTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterWTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterWTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterWTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterSTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterSTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterSTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterSTitle, "");
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterSTitle, "");
				vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterWTitle, "Adjustment");
				vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterSTitle, "Adjustment");
				// .TextMatrix(lngRowMeterAdjustDescription, lngColMeterWTitle) = "Adj Description"
				// .TextMatrix(lngRowMeterAdjustDescription, lngColMeterSTitle) = "Adj Description"
				vsMeter.ColDataType(lngColMeterCheck, FCGrid.DataTypeSettings.flexDTBoolean);
				vsMeter.ColFormat(lngColMeterWAmount, "#,##0.00");
				vsMeter.ColFormat(lngColMeterSAmount, "#,##0.00");
				vsMeter.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//FC:FINAL:CHN - issue #918: Fix changing cell alignment after edit.
				vsMeter.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsMeter.ColAlignment(5, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsMeter.ColAlignment(10, FCGrid.AlignmentSettings.flexAlignRightCenter);
				vsMeter.ColAlignment(11, FCGrid.AlignmentSettings.flexAlignRightCenter);
			}
			// make a black line down the middle of the grid
			//FC:FINAL:DDU:  don't select twice
			//vsMeter.Select(0, lngColMeterSTitle, vsMeter.Rows - 1, lngColMeterSTitle);
			vsMeter.CellBorder(ColorTranslator.FromOle(1), 2, 0, 0, 0, 2, 0);
			vsMeter.Select(0, 0);
			vsMeter.ColWidth(lngColMeterCheck, FCConvert.ToInt32(lngWid * 0.05));
			vsMeter.ColWidth(lngColMeterWTitle, FCConvert.ToInt32(lngWid * 0.16));
			vsMeter.ColWidth(lngColMeterWHidden, 0);
			vsMeter.ColWidth(lngColMeterWType, 0);
			vsMeter.ColWidth(lngColMeterWAmount, FCConvert.ToInt32(lngWid * 0.09));
			vsMeter.ColWidth(lngColMeterWRateTable, FCConvert.ToInt32(lngWid * 0.07));
			vsMeter.ColWidth(lngColMeterWAccount, FCConvert.ToInt32(lngWid * 0.15));
			vsMeter.ColWidth(lngColMeterSTitle, FCConvert.ToInt32(lngWid * 0.16));
			vsMeter.ColWidth(lngColMeterSHidden, 0);
			vsMeter.ColWidth(lngColMeterSType, 0);
			vsMeter.ColWidth(lngColMeterSAmount, FCConvert.ToInt32(lngWid * 0.09));
			vsMeter.ColWidth(lngColMeterSRateTable, FCConvert.ToInt32(lngWid * 0.07));
			vsMeter.ColWidth(lngColMeterSAccount, FCConvert.ToInt32(lngWid * 0.15));
			//vsMeter.Height = (vsMeter.Rows * vsMeter.RowHeight(0)) + 70;
			vsMeter.EditingControlShowing -= VsMeter_EditingControlShowing;
			vsMeter.EditingControlShowing += VsMeter_EditingControlShowing;
		}

		private void VsMeter_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - issue #916: assign JavaScript handler for entering only numbers and dot in appropriate cells as in original
				if (vsMeter.Col == 10 || vsMeter.Col == 4)
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
					}
				}
			}
		}

		private void FormatAccountInfoGrid_2(bool boolReset)
		{
			FormatAccountInfoGrid(boolReset);
		}

		private void FormatAccountInfoGrid(bool boolReset = false)
		{
			int lngWid = 0;
			lngWid = vsAccountInfo.WidthOriginal;
			if (boolReset || boolLoaded == false)
			{
				vsAccountInfo.Rows = 0;
				vsAccountInfo.Cols = 4;
				vsAccountInfo.ExtendLastCol = true;
				// set up each rows label in the left column
				vsAccountInfo.Rows = 3;
				vsAccountInfo.TextMatrix(0, 0, "Account:");
				vsAccountInfo.TextMatrix(1, 0, "Tenant:");
				vsAccountInfo.TextMatrix(2, 0, "Owner:");
				vsAccountInfo.TextMatrix(0, 2, "Map/Lot:");
				vsAccountInfo.TextMatrix(1, 2, "Location:");
				vsAccountInfo.TextMatrix(2, 2, "Category:");
			}
			vsAccountInfo.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsAccountInfo.ColWidth(0, FCConvert.ToInt32(lngWid * 0.2));
			vsAccountInfo.ColWidth(1, FCConvert.ToInt32(lngWid * 0.29));
			vsAccountInfo.ColWidth(2, FCConvert.ToInt32(lngWid * 0.2));
			vsAccountInfo.ColWidth(3, FCConvert.ToInt32(lngWid * 0.29));
			//vsAccountInfo.Height = (vsAccountInfo.Rows * vsAccountInfo.RowHeight(0)) + 70;
			vsAccountInfo.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, vsAccountInfo.Rows - 1, vsAccountInfo.Cols - 1, modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
		}

		private void FillMasterScreen(bool boolNewAccount = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will also fill the master information on the meter screen
				string strTemp = "";
				int intCT;
				clsDRWrapper rsSettings = new clsDRWrapper();
				int lngCT;
				int lngCat = 0;
                EnableREInfo();
				// Account Number
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				txtAccountNumber.Text = rsCurrentAccount.Get_Fields_String("AccountNumber");
				// Add the Account Number to the textbox
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				lblAcctNumberText.Text = rsCurrentAccount.Get_Fields_String("AccountNumber");
				if (rsCurrentAccount.Get_Fields_Boolean("InBankruptcy"))
				{
					lblBankruptcy.Visible = true;
				}
				else
				{
					lblBankruptcy.Visible = false;
				}
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				if (modUTStatusPayments.IsAccountTaxAcquired_7(rsCurrentAccount.Get_Fields_Int32("AccountNumber")))
				{
					lblTaxAcquired.Visible = true;
				}
				else
				{
					lblTaxAcquired.Visible = false;
				}

                FillACHInfo();
				// fill the grid
				strTemp = "";
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(rsCurrentAccount.Get_Fields_String("StreetNumber")) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					strTemp = rsCurrentAccount.Get_Fields_String("StreetNumber");
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					txtLocationNumber.Text = rsCurrentAccount.Get_Fields_String("StreetNumber");
				}
				if (Strings.Trim(rsCurrentAccount.Get_Fields_String("StreetName")) != "")
				{
					strTemp += Strings.Trim(rsCurrentAccount.Get_Fields_String("StreetName"));
					txtLocationStreet.Text = rsCurrentAccount.Get_Fields_String("StreetName");
				}

				txtOwner1CustNum.Text = FCConvert.ToString(rsCurrentAccount.Get_Fields_Int32("OwnerPartyID"));
				txtOwner2CustNum.Text = FCConvert.ToString(rsCurrentAccount.Get_Fields_Int32("SecondOwnerPartyID"));
                txtDeedName1.Text = rsCurrentAccount.Get_Fields_String("DeedName1");
                txtDeedName2.Text = rsCurrentAccount.Get_Fields_String("DeedName2");
				if (Conversion.Val(rsCurrentAccount.Get_Fields_Int32("OwnerPartyID")) != 0)
				{
					oOwner1 = modMain.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(Conversion.Val(rsCurrentAccount.Get_Fields_Int32("OwnerPartyID"))));
				}
				else
				{
					oOwner1 = null;
					// kk 09302013 trouts-37
				}
				if (Conversion.Val(rsCurrentAccount.Get_Fields_Int32("SecondOwnerPartyID")) != 0)
				{
					oOwner2 = modMain.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(Conversion.Val(rsCurrentAccount.Get_Fields_Int32("SecondOwnerPartyID"))));
				}
				else
				{
					oOwner2 = null;
					// kk 09302013 trouts-37
				}
				ShowPartyInfo_2("O");
				txtTenant1CustNum.Text = FCConvert.ToString(rsCurrentAccount.Get_Fields_Int32("BillingPartyID"));
				txtTenant2CustNum.Text = FCConvert.ToString(rsCurrentAccount.Get_Fields_Int32("SecondBillingPartyID"));
				if (Conversion.Val(rsCurrentAccount.Get_Fields_Int32("BillingPartyID")) != 0)
				{
					oTenant1 = modMain.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(Conversion.Val(rsCurrentAccount.Get_Fields_Int32("BillingPartyID"))));
				}
				else
				{
					oTenant1 = null;
					// kk 09302013 trouts-37
				}
				if (Conversion.Val(rsCurrentAccount.Get_Fields_Int32("SecondBillingPartyID")) != 0)
				{
					oTenant2 = modMain.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(Conversion.Val(rsCurrentAccount.Get_Fields_Int32("SecondBillingPartyID"))));
				}
				else
				{
					oTenant2 = null;
					// kk 09302013 trouts-37
				}
				ShowPartyInfo_2("T");
				
				boolSameBillToOwner = FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("SameBillOwner"));
				
				strTemp = "";
				if (Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("Directions"))) != "")
				{
					strTemp = Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("Directions")));
				}
				// lngRowMasterDirections = 9
				// vsMaster.TextMatrix(lngRowMasterDirections, lngColMasterData) = strTemp
				txtDirections.Text = strTemp;
				strTemp = "";
				if (Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("DataEntry"))) != "")
				{
					strTemp = Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("DataEntry")));
				}
				// lngRowMasterDataEntryMessage = 10
				// vsMaster.TextMatrix(lngRowMasterDataEntryMessage, lngColMasterData) = strTemp
				txtDEMessage.Text = strTemp;
				strTemp = "";
				if (Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("BillMessage"))) != "")
				{
					strTemp = Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("BillMessage")));
				}
				// lngRowMasterBillMessage = 11
				txtBillMessage.Text = strTemp;
				strTemp = "";
				if (Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("Telephone"))) != "")
				{
					strTemp = Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("Telephone")));
				}
				// lngRowMasterTelephone = 12
				txtPhone.Text = strTemp;
				strTemp = "";
				if (Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("Email"))) != "")
				{
					strTemp = Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("Email")));
				}
				// lngRowMasterEmail = 13
				// vsMaster.TextMatrix(lngRowMasterEmail, lngColMasterData) = strTemp
				txtEmail.Text = strTemp;
				// Comment
				strTemp = "";
				if (Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("Comment"))) != "")
				{
					strTemp = Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("Comment")));
				}
				txtComment.Text = strTemp;
				// Fill the Categories Comboboxes
				LoadCategoryCombo();
				if (rsCurrentAccount.Get_Fields_Int32("SewerCategory") > 0)
				{
					for (intCT = 0; intCT <= cmbSCat.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(Strings.Left(cmbSCat.Items[intCT].ToString(), 2)) == Conversion.Val(rsCurrentAccount.Get_Fields_Int32("SewerCategory")))
						{
							cmbSCat.SelectedIndex = intCT;
							break;
						}
					}
					if (intCT >= cmbSCat.Items.Count)
					{
						// this will set the combo box to blank
						cmbSCat.SelectedIndex = -1;
					}
				}
				if (rsCurrentAccount.Get_Fields_Int32("WaterCategory") > 0)
				{
					for (intCT = 0; intCT <= cmbWCat.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(Strings.Left(cmbWCat.Items[intCT].ToString(), 2)) == Conversion.Val(rsCurrentAccount.Get_Fields_Int32("WaterCategory")))
						{
							cmbWCat.SelectedIndex = intCT;
							break;
						}
					}
					if (intCT >= cmbWCat.Items.Count)
					{
						// this will set the combo box to blank
						cmbWCat.SelectedIndex = -1;
					}
				}
				// fill the category default account numbers
				if (Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("SewerAccount"))) != "")
				{
					txtSewerAcct.Text = Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("SewerAccount")));
				}
				if (Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("WaterAccount"))) != "")
				{
					txtWaterAcct.Text = Strings.Trim(FCConvert.ToString(rsCurrentAccount.Get_Fields_String("WaterAccount")));
				}
				// trout-718 08-02-2011 kgk  fill WBillToOwner and SBillToOwner fields
				if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("SBillToOwner")))
				{
					cmbSBillTo.SelectedIndex = 0;
				}
				else
				{
					cmbSBillTo.SelectedIndex = 1;
				}
				if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("WBillToOwner")))
				{
					cmbWBillTo.SelectedIndex = 0;
				}
				else
				{
					cmbWBillTo.SelectedIndex = 1;
				}
				// final billed
				if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("FinalBill")))
				{
					chkFinal.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkFinal.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// nobilled
				if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("NoBill")))
				{
					chkNoBill.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkNoBill.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("EmailBill")))
				{
					chkEmailBills.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkEmailBills.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("SameBillOwner")))
				{
					chkBillSameAsOwner.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkBillSameAsOwner.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// kk 03052013 trouts-6  Add field for Impervious Surface Area
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					if (rsCurrentAccount.Get_Fields_Decimal("ImpervSurfArea") >= 0)
					{
						// kk07082015 trouts-149  Indicate account is not billed for SW by setting the Value to -1
						txtImpervSurf.Text = FCConvert.ToString(rsCurrentAccount.Get_Fields_Decimal("ImpervSurfArea"));
					}
					else
					{
						txtImpervSurf.Text = "";
					}
				}
				if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
				{
					chkUseRE.Enabled = true;
					chkUseREMortgage.Enabled = true;
					// use RE information
					if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("UseREAccount")))
					{
						chkUseRE.CheckState = Wisej.Web.CheckState.Checked;
						// load the RE information from the RE DB
						LoadREInfo_8(FCConvert.ToInt32(Conversion.Val(rsCurrentAccount.Get_Fields_Int32("REAccount"))), true);
                        DisableREInfo();
					}
					else
					{
						chkUseRE.CheckState = Wisej.Web.CheckState.Unchecked;
						// load the RE information from the UT DB
						LoadREInfo_8(FCConvert.ToInt32(Conversion.Val(rsCurrentAccount.Get_Fields_Int32("REAccount"))), false);
					}
					// use RE mortgage holders
					if (FCConvert.ToBoolean(rsCurrentAccount.Get_Fields_Boolean("UseMortgageHolder")))
					{
						chkUseREMortgage.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkUseREMortgage.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				}
				else
				{
					// if the user does not have real estate than these features should be shut off
					chkUseRE.CheckState = Wisej.Web.CheckState.Unchecked;
					chkUseREMortgage.CheckState = Wisej.Web.CheckState.Unchecked;
					// load the RE information from the UT DB
					LoadREInfo_8(FCConvert.ToInt32(Conversion.Val(rsCurrentAccount.Get_Fields_Int32("REAccount"))), false);
				}
				if (boolNewAccount)
				{
					rsSettings.OpenRecordset("SELECT * FROM UtilityBilling");
					if (!rsSettings.EndOfFile())
					{
						// Default Account CategoryW
						if (Conversion.Val(rsSettings.Get_Fields_Int32("AccountDefaultCategoryW")) != 0)
						{
							lngCat = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSettings.Get_Fields_Int32("AccountDefaultCategoryW"))));
							for (lngCT = 0; lngCT <= cmbWCat.Items.Count - 1; lngCT++)
							{
								if (lngCat == cmbWCat.ItemData(lngCT))
								{
									cmbWCat.SelectedIndex = lngCT;
									break;
								}
							}
						}
						// Default Account CategoryS
						if (Conversion.Val(rsSettings.Get_Fields_Int32("AccountDefaultCategoryS")) != 0)
						{
							lngCat = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSettings.Get_Fields_Int32("AccountDefaultCategoryS"))));
							for (lngCT = 0; lngCT <= cmbWCat.Items.Count - 1; lngCT++)
							{
								if (lngCat == cmbWCat.ItemData(lngCT))
								{
									cmbWCat.SelectedIndex = lngCT;
									break;
								}
							}
						}
						// Default Account W
						if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultAccountW"))) != "")
						{
							txtWaterAcct.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultAccountW"));
						}
						else
						{
							txtWaterAcct.Text = "";
						}
						// Default Account S
						if (Strings.Trim(FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultAccountS"))) != "")
						{
							txtSewerAcct.Text = FCConvert.ToString(rsSettings.Get_Fields_String("AccountDefaultAccountS"));
						}
						else
						{
							txtSewerAcct.Text = "";
						}
						// trout-718 08-02-2011 kgk  Set default WBillToOwner and SBillOwner fields
						if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("WBillToOwner")))
						{
							cmbWBillTo.SelectedIndex = 0;
						}
						else
						{
							cmbWBillTo.SelectedIndex = 1;
						}
						if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("SBillToOwner")))
						{
							cmbSBillTo.SelectedIndex = 0;
						}
						else
						{
							cmbSBillTo.SelectedIndex = 1;
						}
						
						if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("AccountDefaultUseREInfo")))
						{
							chkUseRE.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkUseRE.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (FCConvert.ToBoolean(rsSettings.Get_Fields_Boolean("AccountDefaultMHInfo")))
						{
							chkUseREMortgage.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkUseREMortgage.CheckState = Wisej.Web.CheckState.Unchecked;
						}
					}
				}
				FormatAccountInfoGrid_2(true);
                vsAccountInfo.TextMatrix(2, 1, rsCurrentAccount.Get_Fields_String("DeedName1"));
                FillAccountInfo();
				// set the first cells
				vsMaster.Select(0, 1);
				// vsREInfo.Select 0, 1
				// vsMeter.Select 2, 0
				vsPercentage.Select(1, 1);
				vsReading.Select(1, 1);
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Master Screen", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillAccountInfo()
		{
			// THIS IS TO FILL THE MASTER INFORMATION ON THE METER SCREEN
			// account number
			vsAccountInfo.TextMatrix(0, 1, txtAccountNumber.Text);
			// bill to
			// k vsAccountInfo.TextMatrix(1, 1) = txtBName.Text  ' vsMaster.TextMatrix(lngRowMasterBillTo, lngColMasterData)
			if (!(oTenant1 == null))
			{
				vsAccountInfo.TextMatrix(1, 1, oTenant1.FullNameLastFirst);
			}
			// map lot
			vsAccountInfo.TextMatrix(0, 3, Strings.Trim(txtREMapLot.Text));
			// vsREInfo.TextMatrix(lngRowREMapLot, lngColMasterData)
			// location
			vsAccountInfo.TextMatrix(1, 3, Strings.Trim(txtLocationNumber.Text + " " + txtLocationStreet.Text));
			// vsMaster.TextMatrix(lngRowMasterLocation, lngColMasterData)
			// categories
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				vsAccountInfo.TextMatrix(2, 3, "Stormwater: " + Strings.Left(cmbWCat.Text, 2) + "     Sewer: " + Strings.Left(cmbSCat.Text, 2));
			}
			else
			{
				vsAccountInfo.TextMatrix(2, 3, "Water: " + Strings.Left(cmbWCat.Text, 2) + "     Sewer: " + Strings.Left(cmbSCat.Text, 2));
			}
		}

		private void SetMasterConstants()
		{
			// this will set all of the row numbers in order to easily move them around if needed
			lngColMasterHeading = 0;
			lngColMasterData = 2;
			lngColMasterHidden = 1;
			
			lngRowREMapLot = 1;
			lngRowREAccount = 0;
			lngRowREBookPage = 2;
			// for the meter grid
			lngRowMeterTopHeader = 0;
			lngRowMeterHeading = 1;
			lngRowMeterPayment1 = 2;
			lngRowMeterPayment2 = 3;
			lngRowMeterPayment3 = 4;
			lngRowMeterPayment4 = 5;
			lngRowMeterPayment5 = 6;
			lngRowMeterAdjust = 7;

			lngColMeterCheck = 0;
			lngColMeterWTitle = 1;
			lngColMeterWHidden = 2;
			lngColMeterWType = 3;
			lngColMeterWAmount = 4;
			lngColMeterWRateTable = 5;
			lngColMeterWAccount = 6;
			lngColMeterSTitle = 7;
			lngColMeterSHidden = 8;
			lngColMeterSType = 9;
			lngColMeterSAmount = 10;
			lngColMeterSRateTable = 11;
			lngColMeterSAccount = 12;
		}

		private void LoadCategoryCombo()
		{
			clsDRWrapper rsCat = new clsDRWrapper();
			rsCat.OpenRecordset("SELECT * FROM Category ORDER BY Code");
			while (!rsCat.EndOfFile())
			{
				if (Strings.Trim(FCConvert.ToString(rsCat.Get_Fields_String("LongDescription"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbSCat.Items.Add(modGlobalFunctions.PadStringWithSpaces(rsCat.Get_Fields_String("Code"), 3, false) + " - " + rsCat.Get_Fields_String("LongDescription"));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbWCat.Items.Add(modGlobalFunctions.PadStringWithSpaces(rsCat.Get_Fields_String("Code"), 3, false) + " - " + rsCat.Get_Fields_String("LongDescription"));
				}
				rsCat.MoveNext();
			}
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolDirty)
			{
				switch (MessageBox.Show("If you leave this screen without saving you will lose any changes that you have made.  Do you wish to exit this screen?", "Leave Screen?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							// allow this to unload
							intAction = 0;
							boolLoaded = false;
							boolDirty = false;
							break;
						}
					case DialogResult.No:
					case DialogResult.Cancel:
						{
							e.Cancel = true;
							break;
						}
				}
				//end switch
			}
			else
			{
				intAction = 0;
				boolLoaded = false;
			}
			//FC:FINAL:MSH - issue #912: missing operator and incorrect converting with ~ operator
			//if (FCConvert.ToBoolean(~Cancel))
			if (!e.Cancel)
			{
				// show the search screen
				//Application.DoEvents();
				if (!boolDoNotReturnToSearch)
				{
					//FC:FINAL:MSH - issue #912: hide current form before showing new form for correct focusing
					this.Hide();
					//frmGetMasterAccount.InstancePtr.Init(0, true);
                    StaticSettings.GlobalCommandDispatcher.Send(new ViewUTAccount());
                }
			}
		}

		private void frmAccountMaster_Resize(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// FormatMasterGrid
				// FormatREGrid
				FormatAccountInfoGrid();
				FormatPercentageGrid();
				FormatReadingGrid();
				FormatMeterGrid();
				SetAct(ref intAction);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Resizing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngREAccount As int	OnWrite(int, double)
		private void LoadREInfo_6(int lngREAccount, bool boolFromRE)
		{
			LoadREInfo(ref lngREAccount, ref boolFromRE);
		}

		private void LoadREInfo_8(int lngREAccount, bool boolFromRE)
		{
			LoadREInfo(ref lngREAccount, ref boolFromRE);
		}

		private void LoadREInfo(ref int lngREAccount, ref bool boolFromRE)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRE = new clsDRWrapper();
				string strMapLot = "";
				string strBook = "";
				string strPage = "";
				string strOwner = "";
				string strOwner2 = "";
				string strOwnerMailing1 = "";
				string strOwnerMailing2 = "";
				string strOwnerMailing3 = "";
				string strOCity = "";
				string strOState = "";
				string strOZip = "";
				string strOZip4 = "";
				string strLocNum = "";
				string strLocStreet = "";
				if (boolFromRE)
				{
					// this will load the information from the RE database
					if ((modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL) && lngREAccount != 0)
					{
						rsRE.OpenRecordset("SELECT * FROM Master LEFT JOIN BookPage ON ((Master.RSAccount = BookPage.Account AND [Current] = 1) AND (Master.RSCard = BookPage.Card)) WHERE RSAccount = " + FCConvert.ToString(lngREAccount) + " AND RSCard = 1 ", modExtraModules.strREDatabase);
						if (!rsRE.EndOfFile())
						{
							// from RE
							strMapLot = FCConvert.ToString(rsRE.Get_Fields_String("RSMapLot"));
							// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
							if (Strings.Trim(rsRE.Get_Fields_String("Book")) != "")
							{
								// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
								strBook = Strings.Trim(rsRE.Get_Fields_String("Book"));
							}
							else
							{
								strBook = "";
							}
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							if (Strings.Trim(rsRE.Get_Fields_String("Page")) != "")
							{
								// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
								strPage = rsRE.Get_Fields_String("Page");
							}
							else
							{
								strPage = "";
							}

							/*

							strOwner = rsRE.Fields("RSName")
                If Trim(rsRE.Fields("RSSECOWNER")) <> "" Then
                    strOwner2 = Trim(rsRE.Fields("RSSECOWNER"))
                Else
                    strOwner2 = ""
                End If
                
                strOwnerMailing1 = rsRE.Fields("RSAddr1")
                strOwnerMailing2 = rsRE.Fields("RSAddr2")
                'addr3 is city, then append state and zip to it
                strOCity = rsRE.Fields("RSAddr3")
                strOState = rsRE.Fields("RSState")
                strOZip = rsRE.Fields("RSZip")
                If Trim(rsRE.Fields("RSZip4")) <> "" Then
                    strOZip4 = rsRE.Fields("RSZip4")
                End If
                
                'this will shift the address up if there are blanks
                If Trim(strOwnerMailing2) = "" Then
                    strOwnerMailing2 = strOwnerMailing3
                    strOwnerMailing3 = ""
                End If
                If Trim(strOwnerMailing1) = "" Then
                    strOwnerMailing1 = strOwnerMailing2
                    strOwnerMailing2 = ""
                End If

							 */
							strLocNum = rsRE.Get_Fields_String("RSLOCNUMALPH");
							strLocStreet = rsRE.Get_Fields_String("RSLOCSTREET");
						}
						else
						{
							// from UT
							strMapLot = rsCurrentAccount.Get_Fields_String("MapLot");
							// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
							strBook = rsCurrentAccount.Get_Fields_String("Book");
							// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
							strPage = rsCurrentAccount.Get_Fields_String("Page");
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							strLocNum = rsCurrentAccount.Get_Fields_String("StreetNumber");
							strLocStreet = rsCurrentAccount.Get_Fields_String("StreetName");
						}
					}
					else
					{
						// from UT
						strMapLot = rsCurrentAccount.Get_Fields_String("MapLot");
						// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
						strBook = rsCurrentAccount.Get_Fields_String("Book");
						// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
						strPage = rsCurrentAccount.Get_Fields_String("Page");
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						strLocNum = rsCurrentAccount.Get_Fields_String("StreetNumber");
						strLocStreet = rsCurrentAccount.Get_Fields_String("StreetName");
					}
				}
				else
				{
					// from UT
					// let this stay whatever it was
					strMapLot = rsCurrentAccount.Get_Fields_String("MapLot");
					// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
					strBook = rsCurrentAccount.Get_Fields_String("Book");
					// TODO Get_Fields: Check the table for the column [Page] and replace with corresponding Get_Field method
					strPage = rsCurrentAccount.Get_Fields_String("Page");
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					strLocNum = rsCurrentAccount.Get_Fields_String("StreetNumber");
					strLocStreet = rsCurrentAccount.Get_Fields_String("StreetName");
				}
				if (boolFromRE || !modGlobalConstants.Statics.gboolRE)
				{
					txtREAcct.Text = FCConvert.ToString(lngREAccount);
					txtREMapLot.Text = strMapLot;
					txtREBook.Text = strBook;
					txtREPage.Text = strPage;
					// MAL@20081105 ; Tracker Reference: 15933
					txtLocationNumber.Text = strLocNum;
					txtLocationStreet.Text = strLocStreet;

				}
				else
				{
					txtREAcct.Text = FCConvert.ToString(lngREAccount);
					txtREMapLot.Text = strMapLot;
					txtREBook.Text = strBook;
					txtREPage.Text = strPage;
					// MAL@20081105 ; Tracker Reference: 15933
					txtLocationNumber.Text = strLocNum;
					txtLocationStreet.Text = strLocStreet;
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading RE Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillMeterArray()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the array with all of the meter information
				clsDRWrapper rsMeter = new clsDRWrapper();
				int intIndex = 0;
				int intCT;
				// this will clear the array
				for (intCT = 1; intCT <= 99; intCT++)
				{
					if (MeterKeyArray[intCT].Used)
					{
						// clear the array
						MeterKeyArray[intCT].MeterNumber = 0;
						MeterKeyArray[intCT].Used = false;
					}
				}
				rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngKey) + " ORDER BY MeterNumber", modExtraModules.strUTDatabase);
				while (!rsMeter.EndOfFile())
				{
					intIndex += 1;
					// this will start at 1
					MeterKeyArray[intIndex].Used = true;
					// fill the meter information into the array
					MeterKeyArray[intIndex].Key = rsMeter.Get_Fields_Int32("ID");
					MeterKeyArray[intIndex].MeterNumber = rsMeter.Get_Fields_Int32("MeterNumber");
					// this is just a sequence
					MeterKeyArray[intIndex].AccountKey = rsMeter.Get_Fields_Int32("AccountKey");
					MeterKeyArray[intIndex].BookNumber = rsMeter.Get_Fields_Int32("BookNumber");
					if (MeterKeyArray[intIndex].MeterNumber == 1)
					{
						lblMasterBook.Text = "Book #: " + FCConvert.ToString(MeterKeyArray[intIndex].BookNumber);
					}
					// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
					if (rsMeter.Get_Fields_Int32("Sequence") >= 1000000)
					{
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						MeterKeyArray[intIndex].Sequence = FCConvert.ToInt32(Strings.Left(rsMeter.Get_Fields_String("Sequence"), 6));
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						MeterKeyArray[intIndex].Sequence = rsMeter.Get_Fields_Int32("Sequence");
					}
					MeterKeyArray[intIndex].SerialNumber = rsMeter.Get_Fields_String("SerialNumber");
					MeterKeyArray[intIndex].Size = rsMeter.Get_Fields_Int32("Size");
					MeterKeyArray[intIndex].Service = rsMeter.Get_Fields_String("Service");
					// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
					MeterKeyArray[intIndex].Frequency = rsMeter.Get_Fields_Int32("Frequency");
					MeterKeyArray[intIndex].Digits = rsMeter.Get_Fields_Int32("Digits");
					MeterKeyArray[intIndex].Multiplier = rsMeter.Get_Fields_Int32("Multiplier");
					MeterKeyArray[intIndex].Combine = rsMeter.Get_Fields_String("Combine");
					MeterKeyArray[intIndex].ReplacementConsumption = rsMeter.Get_Fields_Int32("ReplacementConsumption");
					MeterKeyArray[intIndex].NegativeConsumption = rsMeter.Get_Fields_Boolean("NegativeConsumption");
					if (!rsMeter.IsFieldNull("ReplacementDate"))
					{
						MeterKeyArray[intIndex].ReplacementDate = rsMeter.Get_Fields_DateTime("ReplacementDate");
					}
					else
					{
						MeterKeyArray[intIndex].ReplacementDate = DateTime.FromOADate(0);
					}
					MeterKeyArray[intIndex].IncludeInExtract = rsMeter.Get_Fields_Boolean("IncludeInExtract");
					MeterKeyArray[intIndex].RadioAccess = rsMeter.Get_Fields_Boolean("Radio");
					MeterKeyArray[intIndex].RadioAccessType = FCConvert.ToInt32(Math.Round(Conversion.Val(rsMeter.Get_Fields_Int32("RadioAccessType"))));
					MeterKeyArray[intIndex].MXUNumber = FCConvert.ToString(rsMeter.Get_Fields_String("MXU"));
					MeterKeyArray[intIndex].ReaderCode = FCConvert.ToString(rsMeter.Get_Fields_String("ReaderCode"));
					// kk 01182013 trout-896
					MeterKeyArray[intIndex].XRef1 = FCConvert.ToString(rsMeter.Get_Fields_String("XRef1"));
					MeterKeyArray[intIndex].XRef2 = FCConvert.ToString(rsMeter.Get_Fields_String("XRef2"));
					MeterKeyArray[intIndex].Longitude = FCConvert.ToString(rsMeter.Get_Fields_String("Long"));
					MeterKeyArray[intIndex].Latitude = FCConvert.ToString(rsMeter.Get_Fields_String("Lat"));
					MeterKeyArray[intIndex].UseRate1 = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseRate1"));
					MeterKeyArray[intIndex].UseRate2 = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseRate2"));
					MeterKeyArray[intIndex].UseRate3 = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseRate3"));
					MeterKeyArray[intIndex].UseRate4 = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseRate4"));
					MeterKeyArray[intIndex].UseRate5 = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseRate5"));
					MeterKeyArray[intIndex].UseAdjustRate = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseAdjustRate"));
					// water related fields
					MeterKeyArray[intIndex].WaterPercent = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterPercent"));
					MeterKeyArray[intIndex].WaterTaxPercent = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterTaxPercent"));
					MeterKeyArray[intIndex].WaterType1 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterType1"));
					MeterKeyArray[intIndex].WaterType2 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterType2"));
					MeterKeyArray[intIndex].WaterType3 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterType3"));
					MeterKeyArray[intIndex].WaterType4 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterType4"));
					MeterKeyArray[intIndex].WaterType5 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterType5"));
					MeterKeyArray[intIndex].WaterAmount1 = rsMeter.Get_Fields_Double("WaterAmount1");
					MeterKeyArray[intIndex].WaterAmount2 = rsMeter.Get_Fields_Double("WaterAmount2");
					MeterKeyArray[intIndex].WaterAmount3 = rsMeter.Get_Fields_Double("WaterAmount3");
					MeterKeyArray[intIndex].WaterAmount4 = rsMeter.Get_Fields_Double("WaterAmount4");
					MeterKeyArray[intIndex].WaterAmount5 = rsMeter.Get_Fields_Double("WaterAmount5");
					// these are the rate key values
					MeterKeyArray[intIndex].WaterKey1 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterKey1"));
					MeterKeyArray[intIndex].WaterKey2 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterKey2"));
					MeterKeyArray[intIndex].WaterKey3 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterKey3"));
					MeterKeyArray[intIndex].WaterKey4 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterKey4"));
					MeterKeyArray[intIndex].WaterKey5 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterKey5"));
					MeterKeyArray[intIndex].WaterAccount1 = FCConvert.ToString(rsMeter.Get_Fields_String("WaterAccount1"));
					MeterKeyArray[intIndex].WaterAccount2 = FCConvert.ToString(rsMeter.Get_Fields_String("WaterAccount2"));
					MeterKeyArray[intIndex].WaterAccount3 = FCConvert.ToString(rsMeter.Get_Fields_String("WaterAccount3"));
					MeterKeyArray[intIndex].WaterAccount4 = FCConvert.ToString(rsMeter.Get_Fields_String("WaterAccount4"));
					MeterKeyArray[intIndex].WaterAccount5 = FCConvert.ToString(rsMeter.Get_Fields_String("WaterAccount5"));
					MeterKeyArray[intIndex].WaterAdjustAmount = rsMeter.Get_Fields_Double("WaterAdjustAmount");
					MeterKeyArray[intIndex].WaterAdjustKey = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterAdjustKey"));
					MeterKeyArray[intIndex].WaterAdjustAccount = FCConvert.ToString(rsMeter.Get_Fields_String("WaterAdjustAccount"));
					MeterKeyArray[intIndex].WaterAdjustDescription = FCConvert.ToString(rsMeter.Get_Fields_String("WaterAdjustDescription"));
					MeterKeyArray[intIndex].WaterConsumption = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterConsumption"));
					MeterKeyArray[intIndex].WaterConsumptionOverride = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterConsumptionOverride"));
					// sewer related fields
					MeterKeyArray[intIndex].SewerPercent = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerPercent"));
					MeterKeyArray[intIndex].SewerTaxPercent = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerTaxPercent"));
					MeterKeyArray[intIndex].SewerType1 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerType1"));
					MeterKeyArray[intIndex].SewerType2 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerType2"));
					MeterKeyArray[intIndex].SewerType3 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerType3"));
					MeterKeyArray[intIndex].SewerType4 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerType4"));
					MeterKeyArray[intIndex].SewerType5 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerType5"));
					MeterKeyArray[intIndex].SewerAmount1 = rsMeter.Get_Fields_Double("SewerAmount1");
					MeterKeyArray[intIndex].SewerAmount2 = rsMeter.Get_Fields_Double("SewerAmount2");
					MeterKeyArray[intIndex].SewerAmount3 = rsMeter.Get_Fields_Double("SewerAmount3");
					MeterKeyArray[intIndex].SewerAmount4 = rsMeter.Get_Fields_Double("SewerAmount4");
					MeterKeyArray[intIndex].SewerAmount5 = rsMeter.Get_Fields_Double("SewerAmount5");
					MeterKeyArray[intIndex].SewerKey1 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerKey1"));
					MeterKeyArray[intIndex].SewerKey2 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerKey2"));
					MeterKeyArray[intIndex].SewerKey3 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerKey3"));
					MeterKeyArray[intIndex].SewerKey4 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerKey4"));
					MeterKeyArray[intIndex].SewerKey5 = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerKey5"));
					MeterKeyArray[intIndex].SewerAccount1 = FCConvert.ToString(rsMeter.Get_Fields_String("SewerAccount1"));
					MeterKeyArray[intIndex].SewerAccount2 = FCConvert.ToString(rsMeter.Get_Fields_String("SewerAccount2"));
					MeterKeyArray[intIndex].SewerAccount3 = FCConvert.ToString(rsMeter.Get_Fields_String("SewerAccount3"));
					MeterKeyArray[intIndex].SewerAccount4 = FCConvert.ToString(rsMeter.Get_Fields_String("SewerAccount4"));
					MeterKeyArray[intIndex].SewerAccount5 = FCConvert.ToString(rsMeter.Get_Fields_String("SewerAccount5"));
					MeterKeyArray[intIndex].SewerAdjustAmount = rsMeter.Get_Fields_Double("SewerAdjustAmount");
					MeterKeyArray[intIndex].SewerAdjustKey = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerAdjustKey"));
					MeterKeyArray[intIndex].SewerAdjustAccount = FCConvert.ToString(rsMeter.Get_Fields_String("SewerAdjustAccount"));
					MeterKeyArray[intIndex].SewerAdjustDescription = FCConvert.ToString(rsMeter.Get_Fields_String("SewerAdjustDescription"));
					MeterKeyArray[intIndex].SewerConsumption = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerConsumption"));
					MeterKeyArray[intIndex].SewerConsumptionOverride = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerConsumptionOverride"));
					MeterKeyArray[intIndex].PreviousReading = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading"));
					if (!rsMeter.IsFieldNull("PreviousReadingDate"))
					{
						MeterKeyArray[intIndex].PreviousReadingDate = (DateTime)rsMeter.Get_Fields_DateTime("PreviousReadingDate");
					}
					else
					{
						MeterKeyArray[intIndex].PreviousReadingDate = DateTime.FromOADate(0);
					}
					MeterKeyArray[intIndex].CurrentReading = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading"));
					if (!rsMeter.IsFieldNull("CurrentReadingDate"))
					{
						MeterKeyArray[intIndex].CurrentReadingDate = (DateTime)rsMeter.Get_Fields_DateTime("CurrentReadingDate");
					}
					else
					{
						MeterKeyArray[intIndex].CurrentReadingDate = DateTime.FromOADate(0);
					}
					MeterKeyArray[intIndex].CurrentCode = FCConvert.ToString(rsMeter.Get_Fields_String("CurrentCode"));
					if (!rsMeter.IsFieldNull("DateOfChange"))
					{
						MeterKeyArray[intIndex].DateOfChange = (DateTime)rsMeter.Get_Fields_DateTime("DateOfChange");
					}
					else
					{
						MeterKeyArray[intIndex].DateOfChange = DateTime.Today;
					}
					MeterKeyArray[intIndex].Remote = FCConvert.ToString(rsMeter.Get_Fields_String("Remote"));
					MeterKeyArray[intIndex].WCat = FCConvert.ToString(rsMeter.Get_Fields_Int32("WCat"));
					MeterKeyArray[intIndex].SCat = FCConvert.ToString(rsMeter.Get_Fields_Int32("SCat"));
					MeterKeyArray[intIndex].BackFlow = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("BackFlow"));
					if (!rsMeter.IsFieldNull("BackFlowDate"))
					{
						MeterKeyArray[intIndex].BackFlowDate = (DateTime)rsMeter.Get_Fields_DateTime("BackFlowDate");
					}
					else
					{
						MeterKeyArray[intIndex].BackFlowDate = DateTime.FromOADate(0);
					}
					if (!rsMeter.IsFieldNull("SetDate"))
					{
						MeterKeyArray[intIndex].SetDate = (DateTime)rsMeter.Get_Fields_DateTime("SetDate");
					}
					else
					{
						MeterKeyArray[intIndex].SetDate = DateTime.FromOADate(0);
					}
					MeterKeyArray[intIndex].BillingCode = FCConvert.ToString(rsMeter.Get_Fields_String("BillingCode"));
					MeterKeyArray[intIndex].Comment = FCConvert.ToString(rsMeter.Get_Fields_String("Comment"));
					MeterKeyArray[intIndex].FinalBilled = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("FinalBilled"));
					MeterKeyArray[intIndex].NoBill = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("NoBill"));
					MeterKeyArray[intIndex].BillingStatus = FCConvert.ToString(rsMeter.Get_Fields_String("BillingStatus"));
					MeterKeyArray[intIndex].Location = FCConvert.ToString(rsMeter.Get_Fields_String("Location"));
					rsMeter.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Meter Screen", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadCombos()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load all of the comboboxes on the master/meter screen
				clsDRWrapper rsCombo = new clsDRWrapper();
				cmbCombineMeter.Clear();
				// .AddItem "No"
				cmbCombineMeter.AddItem("Bill");
				cmbCombineMeter.AddItem("Consumption");
				cmbRadio.Clear();
				// kk 01182013 trout-896  Setup Badger options for Radio Access
				rsCombo.OpenRecordset("SELECT ReaderExtractType FROM UtilityBilling");
				if (FCConvert.ToString(rsCombo.Get_Fields_String("ReaderExtractType")) == "B")
				{
					cmbRadio.AddItem("Manual");
					// 0
					cmbRadio.AddItem("PI Trace");
					// 1
					cmbRadio.AddItem("MMI Trace");
					// 2
					cmbRadio.AddItem("Dialog/Touch");
					// 3
					cmbRadio.AddItem("ORION");
					// 4
					cmbRadio.AddItem("ORION ME/SE");
					// 5
					cmbRadio.AddItem("GALAXY");
					// 6
				}
				else if (FCConvert.ToString(rsCombo.Get_Fields_String("ReaderExtractType")) == "P" || FCConvert.ToString(rsCombo.Get_Fields_String("ReaderExtractType")) == "Q")
				{
					cmbRadio.AddItem("Manual");
					cmbRadio.AddItem("Auto, Non Radio");
					cmbRadio.AddItem("Auto, Radio");
					cmbRadio.AddItem("Auto, Flexnet");
				}
				else
				{
					cmbRadio.AddItem("Manual");
					cmbRadio.AddItem("Auto, Non Radio");
					cmbRadio.AddItem("Auto, Radio");
				}
				cmbFrequency.Clear();
				rsCombo.OpenRecordset("SELECT * FROM Frequency");
				while (!rsCombo.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbFrequency.AddItem(modMain.PadToString_8(rsCombo.Get_Fields_Int32("Code"), 2) + " - " + rsCombo.Get_Fields_String("LongDescription"));
					rsCombo.MoveNext();
				}
				cmbMeterDigits.Clear();
				cmbMeterDigits.AddItem("3");
				cmbMeterDigits.AddItem("4");
				cmbMeterDigits.AddItem("5");
				cmbMeterDigits.AddItem("6");
				cmbMeterDigits.AddItem("7");
				cmbMeterDigits.AddItem("8");
				cmbMeterDigits.AddItem("9");
				cmbMeterSize.Clear();
				rsCombo.OpenRecordset("SELECT * FROM MeterSizes");
				while (!rsCombo.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cmbMeterSize.AddItem(modMain.PadToString_8(rsCombo.Get_Fields_Int32("Code"), 2) + " - " + rsCombo.Get_Fields_String("LongDescription"));
					rsCombo.MoveNext();
				}
				cmbMultiplier.Clear();
				cmbMultiplier.AddItem("1");
				cmbMultiplier.AddItem("10");
				cmbMultiplier.AddItem("100");
				cmbMultiplier.AddItem("1000");
				cmbService.Clear();
				if (modUTStatusPayments.Statics.TownService == "B")
				{
					cmbService.AddItem("Both");
					cmbService.AddItem("Water");
					cmbService.AddItem("Sewer");
				}
				else if (modUTStatusPayments.Statics.TownService == "W")
				{
					cmbService.AddItem("Water");
				}
				else if (modUTStatusPayments.Statics.TownService == "S")
				{
					cmbService.AddItem("Sewer");
				}
				// create the string that the rate table combos will use in the grid
				strRateComboList = "#0;|";
				rsCombo.OpenRecordset("SELECT * FROM RateTable", modExtraModules.strUTDatabase);
				while (!rsCombo.EndOfFile())
				{
					strRateComboList += "#" + rsCombo.Get_Fields_Int32("RateTableNumber") + ";" + rsCombo.Get_Fields_Int32("RateTableNumber") + "\t" + rsCombo.Get_Fields_String("RateTableDescription") + "|";
					rsCombo.MoveNext();
				}
				if (Strings.Trim(strRateComboList) != "")
				{
					// take the last | from the string
					strRateComboList = Strings.Left(strRateComboList, strRateComboList.Length - 1);
				}
				// book combobox
				cmbBook.Clear();
				rsCombo.OpenRecordset("SELECT * FROM Book", modExtraModules.strUTDatabase);
				while (!rsCombo.EndOfFile())
				{
					cmbBook.AddItem(modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(rsCombo.Get_Fields_Int32("BookNumber"))), 4) + " - " + rsCombo.Get_Fields_String("Description"));
					cmbBook.ItemData(cmbBook.NewIndex, FCConvert.ToInt32(rsCombo.Get_Fields_Int32("BookNumber")));
					rsCombo.MoveNext();
				}
				strRateTypeComboList = "#0;|#1;Consumption|#2;Flat|#3;Units";
				strRateTypeComboList2 = "#0;|#1;Consumption|#2;Flat|#3;Units";
				// |#4;Negative Consumption"
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Combo Boxes", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: intMeterNumber As short	OnWriteFCConvert.ToInt32(
		private void LoadMeterCombo(short intMeterNumber = 1)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the combo from the array
				int intCT;
				bool boolFound = false;
				cmbMeterNumber.Clear();
				for (intCT = 1; intCT <= 99; intCT++)
				{
					if (MeterKeyArray[intCT].Used)
					{
						cmbMeterNumber.AddItem(FCConvert.ToString(MeterKeyArray[intCT].MeterNumber));
						// this will shwo the meter number to the user
						cmbMeterNumber.ItemData(cmbMeterNumber.NewIndex, intCT);
						// this is the index of the meter in the array
					}
				}
				if (cmbMeterNumber.Items.Count > 0)
				{
					if (intMeterNumber == 1)
					{
						cmbMeterNumber.SelectedIndex = 0;
					}
					else
					{
						for (intCT = 0; intCT <= cmbMeterNumber.Items.Count - 1; intCT++)
						{
							if (Conversion.Val(cmbMeterNumber.Items[intCT].ToString()) == intMeterNumber)
							{
								cmbMeterNumber.SelectedIndex = intCT;
								boolFound = true;
								break;
							}
						}
						if (!boolFound)
						{
							cmbMeterNumber.SelectedIndex = 0;
						}
					}
					LockMeterFields_2(false);
				}
				else
				{
					LockMeterFields_2(true);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Meter Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void lblAcctComment_DoubleClick(object sender, System.EventArgs e)
		{
			frmUTComment.InstancePtr.Init(ref lngKey);
		}

		private void lblFinalBill_Click(object sender, System.EventArgs e)
		{
			ShowFinalBillInfo();
		}

		private void mnuFileBankrupcty_Click(object sender, System.EventArgs e)
		{
			lblBankruptcy.Visible = !(lblBankruptcy.Visible);
			boolDirty = true;
		}

		private void mnuFileChangeOutHistory_Click(object sender, System.EventArgs e)
		{
			// MAL@20080111: Look up meter change out history
			// Call Reference: 116727
			int intCount = 0;
			int intRow;
			int lngMeter;
			int lngWid = 0;
			clsDRWrapper rsHistory = new clsDRWrapper();
			lngColChangePrevID = 0;
			lngColChangeAcctKey = 1;
			lngColChangeMeterID = 2;
			lngColChangeSetDate = 3;
			lngColChangeSerial = 4;
			lngColChangeRemote = 5;
			lngColChangeSize = 6;
			lngColChangeDigits = 7;
			lngColChangeMult = 8;
			lngColChangeFinal = 9;
			lngColChangeReason = 10;
			lngMeter = MeterKeyArray[cmbMeterNumber.ItemData(cmbMeterNumber.SelectedIndex)].Key;
			rsHistory.OpenRecordset("SELECT * FROM tblPreviousMeter WHERE AccountKey = " + FCConvert.ToString(lngKey) + " AND PreviousMeterKey = " + FCConvert.ToString(lngMeter), modExtraModules.strUTDatabase);
			//FC:FINAL:MSH - issue #954: use WidthOriginal for correct resizing
			lngWid = vsChangeOut.WidthOriginal;
			vsChangeOut.Rows = 1;
			vsChangeOut.Cols = 11;
			// set up each rows label in the left column
			vsChangeOut.Rows = rsHistory.RecordCount() + vsChangeOut.Rows;
			vsChangeOut.TextMatrix(0, lngColChangePrevID, "Previous Meter ID:");
			vsChangeOut.TextMatrix(0, lngColChangeAcctKey, "Account Key:");
			vsChangeOut.TextMatrix(0, lngColChangeMeterID, "Meter Key:");
			vsChangeOut.TextMatrix(0, lngColChangeSetDate, "Set Date:");
			vsChangeOut.TextMatrix(0, lngColChangeSerial, "Serial Num:");
			vsChangeOut.TextMatrix(0, lngColChangeRemote, "Remote Num:");
			vsChangeOut.TextMatrix(0, lngColChangeSize, "Size:");
			vsChangeOut.TextMatrix(0, lngColChangeDigits, "Digits:");
			vsChangeOut.TextMatrix(0, lngColChangeMult, "Multiplier:");
			vsChangeOut.TextMatrix(0, lngColChangeFinal, "Final Reading:");
			vsChangeOut.TextMatrix(0, lngColChangeReason, "Reason:");
			vsChangeOut.ColHidden(lngColChangePrevID, true);
			vsChangeOut.ColHidden(lngColChangeAcctKey, true);
			vsChangeOut.ColHidden(lngColChangeMeterID, true);
			vsChangeOut.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
			vsChangeOut.Editable = FCGrid.EditableSettings.flexEDNone;
			if (rsHistory.RecordCount() > 0)
			{
				rsHistory.MoveFirst();
				intCount = 1;
				while (!rsHistory.EndOfFile())
				{
					// TODO Get_Fields: Field [PreviousMeterID] not found!! (maybe it is an alias?)
					vsChangeOut.TextMatrix(intCount, lngColChangeFinal, FCConvert.ToString(rsHistory.Get_Fields("PreviousMeterID")));
					vsChangeOut.TextMatrix(intCount, lngColChangeAcctKey, FCConvert.ToString(rsHistory.Get_Fields_Int32("AccountKey")));
					vsChangeOut.TextMatrix(intCount, lngColChangeMeterID, FCConvert.ToString(rsHistory.Get_Fields_Int32("PreviousMeterKey")));
					//FC:FINAL:DSE Use correct value for empty date
					//if (DateAndTime.DateValue(FCConvert.ToString(rsHistory.Get_Fields("SetDate"))).ToOADate() == "12:00:00 AM")
					if (rsHistory.Get_Fields_DateTime("SetDate").ToOADate() == 0)
					{
						vsChangeOut.TextMatrix(intCount, lngColChangeSetDate, "");
					}
					else
					{
						//FC:FINAL:MSH - issue #954: add only short datetime format
						//vsChangeOut.TextMatrix(intCount, lngColChangeSetDate, FCConvert.ToString(rsHistory.Get_Fields_DateTime("SetDate")));
						vsChangeOut.TextMatrix(intCount, lngColChangeSetDate, Strings.Format(rsHistory.Get_Fields_DateTime("SetDate"), "MM/dd/yyyy"));
					}
					vsChangeOut.TextMatrix(intCount, lngColChangeSerial, FCConvert.ToString(rsHistory.Get_Fields_String("SerialNumber")));
					vsChangeOut.TextMatrix(intCount, lngColChangeRemote, FCConvert.ToString(rsHistory.Get_Fields_String("Remote")));
					vsChangeOut.TextMatrix(intCount, lngColChangeSize, FCConvert.ToString(rsHistory.Get_Fields_Int32("Size")));
					vsChangeOut.TextMatrix(intCount, lngColChangeDigits, FCConvert.ToString(rsHistory.Get_Fields_Int32("Digits")));
					vsChangeOut.TextMatrix(intCount, lngColChangeMult, FCConvert.ToString(rsHistory.Get_Fields_Int32("Multiplier")));
					vsChangeOut.TextMatrix(intCount, lngColChangeFinal, FCConvert.ToString(rsHistory.Get_Fields_Int32("LastReading")));
					vsChangeOut.TextMatrix(intCount, lngColChangeReason, FCConvert.ToString(rsHistory.Get_Fields_String("ChangeOutReason")));
					intCount += 1;
					rsHistory.MoveNext();
				}
			}
			vsChangeOut.ColWidth(lngColChangeFinal, 0);
			vsChangeOut.ColWidth(lngColChangeAcctKey, 0);
			vsChangeOut.ColWidth(lngColChangeMeterID, 0);
			vsChangeOut.ColWidth(lngColChangeSetDate, FCConvert.ToInt32(lngWid * 0.12));
			vsChangeOut.ColWidth(lngColChangeSerial, FCConvert.ToInt32(lngWid * 0.14));
			vsChangeOut.ColWidth(lngColChangeRemote, FCConvert.ToInt32(lngWid * 0.14));
			vsChangeOut.ColWidth(lngColChangeSize, FCConvert.ToInt32(lngWid * 0.1));
			vsChangeOut.ColWidth(lngColChangeDigits, FCConvert.ToInt32(lngWid * 0.1));
			vsChangeOut.ColWidth(lngColChangeMult, FCConvert.ToInt32(lngWid * 0.11));
			vsChangeOut.ColWidth(lngColChangeFinal, FCConvert.ToInt32(lngWid * 0.15));
			vsChangeOut.ColWidth(lngColChangeReason, FCConvert.ToInt32(lngWid * 0.25));
			//FC:FINAL:MSH - issue #954: use HeightOriginal for correct resizing
			//vsChangeOut.HeightOriginal = (vsChangeOut.Rows * vsChangeOut.RowHeight(0)) + (vsChangeOut.RowHeight(0) * 2);
			// show the actual frame
			fraChangeOutHistory.Top = FCConvert.ToInt32((this.Height - fraChangeOutHistory.Height) / 2.0);
			fraChangeOutHistory.Left = FCConvert.ToInt32((this.Width - fraChangeOutHistory.Width) / 2.0);
			fraMeter.Visible = false;
			fraChangeOutHistory.Visible = true;
		}

		private void mnuFileComment_Click(object sender, System.EventArgs e)
		{
			frmUTComment.InstancePtr.Init(ref lngKey);
		}

		private void mnuFileDeleteAccount_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMast = new clsDRWrapper();
				// vbPorter upgrade warning: intAns As short, int --> As DialogResult
				DialogResult intAns;
				double dblDue = 0;
				intAns = MessageBox.Show("Are you sure that you want to delete this account?", "Delete Master Account", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if (intAns == DialogResult.Yes)
				{
					if (modUTStatusPayments.Statics.TownService != "W")
					{
						dblDue = modUTCalculations.CalculateAccountUTTotal(modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT), false);
					}
					if (modUTStatusPayments.Statics.TownService != "S")
					{
						dblDue += modUTCalculations.CalculateAccountUTTotal(modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT), true);
					}
					if (dblDue != 0)
					{
						MessageBox.Show("This account still owes money and will not be deleted.", "Cannot Delete", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						modGlobalFunctions.AddCYAEntry_80("UT", "Account NOT Deleted, balance due.", "Account = " + FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT), "Key = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT)));
						return;
					}
					rsMast.OpenRecordset("SELECT * FROM Master WHERE AccountNumber = " + FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT), modExtraModules.strUTDatabase);
					if (!rsMast.EndOfFile())
					{
						modGlobalFunctions.AddCYAEntry_80("UT", "Delete UT Account", "Account = " + FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT), "Key = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT)));
						// set the account to deleted
						rsMast.Edit();
						rsMast.Set_Fields("Deleted", true);
						rsMast.Update();
						// remove the current billing if it has not been finalized
						rsMast.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMast.Get_Fields_Int32("ID") + " AND BillStatus <> 'B'");
						while (!rsMast.EndOfFile())
						{
							rsMast.Delete();
							rsMast.MoveNext();
						}
						MessageBox.Show("Account " + FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT) + " was deleted.", "Deleted", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						MessageBox.Show("Cannot find account key - " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT)) + ", this account was not deleted.", "No Deleted", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + " - " + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Deleting Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			ExitScreen_2(true);
		}

		private void mnuFileFindSeq_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsSeq = new clsDRWrapper();
				int lngBook;
				int lngCT;
				//FC:FINAL:DSE:#i1125 Exception if SelectedIndex is -1
				if (cmbBook.SelectedIndex == -1)
				{
					lngBook = 0;
				}
				else
				{
					lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbBook.Items[cmbBook.SelectedIndex].ToString())));
				}
				if (lngBook > 0)
				{
					for (lngCT = 1; lngCT <= 999999; lngCT++)
					{
						rsSeq.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(lngBook) + " AND Sequence = " + FCConvert.ToString(lngCT), modExtraModules.strUTDatabase);
						if (rsSeq.EndOfFile())
						{
							MessageBox.Show("The next available sequence is " + FCConvert.ToString(lngCT) + ".", "Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							txtSequence.Text = FCConvert.ToString(lngCT);
							return;
						}
					}
					MessageBox.Show("No available sequences in this book. (1-9999)", "No Seq", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				else
				{
					MessageBox.Show("Please enter a book.", "Invalid Book", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Next Seq", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileMeterAdd_Click(object sender, System.EventArgs e)
		{
			AddMeterToThisAccount();
		}

		private void mnuFileMeterDelete_Click(object sender, System.EventArgs e)
		{
			DeleteMeter();
			// delete the current meter
		}

		private void mnuFileMeterNext_Click(object sender, System.EventArgs e)
		{
			// move to the next meter
			if (cmbMeterNumber.Items.Count > 1)
			{
				cmbMeterNumber.SelectedIndex = (cmbMeterNumber.SelectedIndex + 1) % (cmbMeterNumber.Items.Count);
			}
		}

		private void mnuFileMeterPrevious_Click(object sender, System.EventArgs e)
		{
			// move to the previous meter
			if (cmbMeterNumber.Items.Count > 1)
			{
				if (cmbMeterNumber.SelectedIndex == 0)
				{
					cmbMeterNumber.SelectedIndex = cmbMeterNumber.Items.Count - 1;
				}
				else
				{
					cmbMeterNumber.SelectedIndex = (cmbMeterNumber.SelectedIndex - 1) % (cmbMeterNumber.Items.Count);
				}
			}
		}

		private void mnuFilePreviousOwner_Click(object sender, System.EventArgs e)
		{
			frmPreviousOwner.InstancePtr.Init(ref modUTStatusPayments.Statics.lngCurrentAccountUT);
		}

		private void mnuFileResetMeterStatus_Click(object sender, System.EventArgs e)
		{
			if (MessageBox.Show("Are you sure that you would like to reset the meter status?", "Reset Status", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				modGlobalFunctions.AddCYAEntry_242("UT", "Reset Meter Status", "Account Number : " + FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT), "Meter Key : " + FCConvert.ToString(modMain.Statics.lngCurrentMeterKey), "Meter Number : " + FCConvert.ToString(lngCurrentMeterNumber));
				boolDirty = true;
				MeterKeyArray[cmbMeterNumber.ItemData(cmbMeterNumber.ListIndex)].BillingStatus = "C";
				MessageBox.Show("Meter Status will be set to Cleared.", "Status Reset", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (fraFinalBillInfo.Visible)
			{
				if (SaveFinalBillInfo())
				{
					fraFinalBillInfo.Visible = false;
					fraMeter.Visible = true;
				}
			}
			else
			{
				ValidateControls();
				// kk04022015 trouts-49  Force change to cust number to update screen before saving
				SaveAccount();
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (fraFinalBillInfo.Visible)
			{
				if (SaveFinalBillInfo())
				{
					fraFinalBillInfo.Visible = false;
					fraMeter.Visible = true;
				}
			}
			else
			{
				ValidateControls();
				// kk04022015 trouts-49  Force change to cust number to update screen before saving
				if (SaveAccount())
				{
					ExitScreen_2(true);
				}
			}
		}

		private void mnuFileShowMeter_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						SetAct_2(1);
						break;
					}
				case 1:
					{
						SetAct_2(0);
						break;
					}
			}
			//end switch
		}

		private bool ExitScreen_2(bool boolUseUnload)
		{
			return ExitScreen(boolUseUnload);
		}

		private bool ExitScreen(bool boolUseUnload = true)
		{
			bool ExitScreen = false;
			// this will check for changes in the information and then exit if the user wishes
			try
			{
				// On Error GoTo ERROR_HANDLER
				// vbPorter upgrade warning: intAnswer As short, int --> As DialogResult
				DialogResult intAnswer;
				if (boolDirty)
				{
					intAnswer = MessageBox.Show("Would you like to save before exiting?", "Save Account", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					switch (intAnswer)
					{
						case DialogResult.Yes:
							{
								// save the codes
								SaveAccount();
								break;
							}
						case DialogResult.No:
							{
								// just exit the screen
								break;
							}
						case DialogResult.Cancel:
							{
								// do not leave
								return ExitScreen;
							}
					}
					//end switch
				}
				if (boolUseUnload)
				{
					this.Unload();
				}
				ExitScreen = true;
				return ExitScreen;
			}
			catch (Exception ex)
			{
				
				ExitScreen = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Exiting Screen", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ExitScreen;
		}

		private bool SaveAccount()
		{
			int lngErrCode = 0;
			bool SaveAccount = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsSave = new clsDRWrapper();
				clsDRWrapper rsBookUpdate = new clsDRWrapper();
				int intCT;
				string strTemp = "";
				string strBookString = "";
				bool boolCheckWTaxable = false;
				bool boolCheckSTaxable = false;
				clsDRWrapper rsCat = new clsDRWrapper();
				clsDRWrapper rsMeter = new clsDRWrapper();
				cParty oLastOwner;
				cParty oLastTenant;
				int lngOwnerID = 0;
				int lngTenantID = 0;
				bool boolOwnerChanged = false;
				bool boolOldBillToOwner = false;
				bool boolNewBillToOwner = false;
				bool boolTenantChanged = false;
				bool boolOldBillToTenant = false;
				bool boolNewBillToTenant = false;
				if (!ValidateData())
				{
					return SaveAccount;
				}
				lngErrCode = 1;
				SaveAccount = true;
				if (fraMeter.Visible)
				{
					// save all meter cahnges
					if (!vsMeter.IsCurrentCellInEditMode)
					{
						vsMeter.Select(0, 0);
						// kk11052014 trouts-116  Force changes in the Rate grid to validate/save
					}
					SaveMeterChanges();
				}
				// make sure that all fields have been validated
				if (vsAccountInfo.Enabled && vsAccountInfo.Visible)
				{
					if (this.ActiveControl.GetName() == "txtSequence")
					{
						txtSequence_Validate(false);
					}
					vsAccountInfo.Focus();
				}
				else if (vsMaster.Enabled && vsMaster.Visible)
				{
					vsMaster.Focus();
				}
				vsMaster.Select(0, lngColMasterData);
				rsSave.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
				lngErrCode = 2;
				if (rsSave.EndOfFile())
				{
					SaveAccount = false;
					MessageBox.Show("Account not found.", "Save Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					lngErrCode = 3;
					// save account information
					rsSave.Edit();
					lngErrCode = 4;
					rsSave.Set_Fields("AccountNumber", FCConvert.ToString(Conversion.Val(txtAccountNumber.Text)));
					lngErrCode = 5;
					rsSave.Set_Fields("InBankruptcy", lblBankruptcy.Visible);
					// kk10132016 trouts-201  This didn't get added from Access - Update online billing accounts for conveyance
					boolOwnerChanged = false;
					boolOldBillToOwner = FCConvert.CBool(rsSave.Get_Fields_Boolean("SameBillOwner") || rsSave.Get_Fields_Boolean("WBillToOwner") || rsSave.Get_Fields_Boolean("SBillToOwner"));
					boolNewBillToOwner = FCConvert.CBool(chkBillSameAsOwner.CheckState == Wisej.Web.CheckState.Checked || cmbWBillTo.SelectedIndex == 0 || cmbSBillTo.SelectedIndex == 0);
					lngOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSave.Get_Fields_Int32("OwnerPartyID"))));
					if (lngOwnerID != 0)
					{
						oLastOwner = modMain.Statics.gCPCtlr.GetParty(rsSave.Get_Fields_Int32("OwnerPartyID"));
                        strOwnerName = rsSave.Get_Fields_String("DeedName1");
                        if (oLastOwner.FullNameLastFirst.Trim().ToUpper() != oOwner1.FullNameLastFirst.Trim().ToUpper())
						{
							// This a change in the owner name
							switch (MessageBox.Show("Is this name change because of a change of ownership?", "Name Change", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
							{
								case DialogResult.Yes:
									{
										SavePreviousOwnerInformation();
										lngOwnerID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtOwner1CustNum.Text)));
										// Tracker Reference: 10680
										// kk01202016 trout-936     UpdateIConnectInformation (strOwnerName)
										boolOwnerChanged = true;
										break;
									}
								case DialogResult.No:
									{
										modGlobalFunctions.AddCYAEntry_80("UT", "Owner Name Change", "New : " + Strings.UCase(Strings.Trim(oOwner1.FullNameLastFirst)), "Old : " + Strings.UCase(Strings.Trim(strOwnerName)));
										break;
									}
							}
							//end switch
                            strOwnerName = txtDeedName1.Text;
                        }
					}
					else
					{
						// Tracker Reference: 10680
						if (strOwnerName == "")
                        {
                            strOwnerName = txtDeedName1.Text.Trim();
                        }
						// kk01202016 trout-936    UpdateIConnectInformation (strOwnerName)
						boolOwnerChanged = true;
					}
					// kk01202016 trout-936  Update the online billing account for Owner; Not sure we need to deactivate anything
					if ((boolOwnerChanged && boolOldBillToOwner && boolNewBillToOwner) || (!boolOldBillToOwner && boolNewBillToOwner))
					{
						UpdateIConnectInformation(strOwnerName, "O");
					}

					boolTenantChanged = false;
					boolOldBillToTenant = FCConvert.CBool(!FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("SameBillOwner")) && (!FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("WBillToOwner")) || !FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("SBillToOwner"))));
					boolNewBillToTenant = FCConvert.CBool(chkBillSameAsOwner.CheckState == Wisej.Web.CheckState.Unchecked && (cmbWBillTo.SelectedIndex == 0 || cmbSBillTo.SelectedIndex == 1));
					lngTenantID = FCConvert.ToInt32(Math.Round(Conversion.Val(rsSave.Get_Fields_Int32("BillingPartyID"))));
					if (lngTenantID != 0)
					{
						oLastTenant = modMain.Statics.gCPCtlr.GetParty(rsSave.Get_Fields_Int32("BillingPartyID"));
						strTenantName = oLastTenant.FullNameLastFirst;
						if (Strings.UCase(Strings.Trim(oTenant1.FullNameLastFirst)) != Strings.UCase(Strings.Trim(strTenantName)))
						{
							// The tenant name has changed
							switch (MessageBox.Show("Is the name change due to a new tenant account?", "Name Change", MessageBoxButtons.YesNo, MessageBoxIcon.Question))
							{
								case DialogResult.Yes:
									{
										strTenantName = oTenant1.FullNameLastFirst;
										lngTenantID = FCConvert.ToInt32(Math.Round(Conversion.Val(txtTenant1CustNum.Text)));
										boolTenantChanged = true;
										break;
									}
								case DialogResult.No:
									{
										strTenantName = oTenant1.FullNameLastFirst;
										break;
									}
							}
							//end switch
						}
					}
					else
					{
						if (strTenantName == "")
						{
							strTenantName = Strings.Trim(oTenant1.FullNameLastFirst);
						}
						boolTenantChanged = true;
					}
					// kk01202016 trout-936  Update the online billing account for Owner; Not sure we need to deactivate anything
					if ((boolTenantChanged && boolOldBillToTenant && boolNewBillToTenant) || (!boolOldBillToTenant && boolNewBillToTenant))
					{
						UpdateIConnectInformation(strTenantName, "T");
					}

					if (chkBillSameAsOwner.CheckState == Wisej.Web.CheckState.Checked)
					{
						txtTenant1CustNum.Text = txtOwner1CustNum.Text;
						txtTenant2CustNum.Text = txtOwner2CustNum.Text;
						if (Conversion.Val(txtTenant1CustNum.Text) != 0)
						{
							oTenant1 = modMain.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(Conversion.Val(txtTenant1CustNum.Text)));
						}
						else
						{
							oTenant1 = null;
							// kk 09302013 trouts-37
						}
						if (Conversion.Val(txtTenant2CustNum.Text) != 0)
						{
							oTenant2 = modMain.Statics.gCPCtlr.GetParty(FCConvert.ToInt32(Conversion.Val(txtTenant2CustNum.Text)));
						}
						else
						{
							oTenant2 = null;
							// kk 09302013 trouts-37
						}
						ShowPartyInfo_2("T");
					}
					
					rsSave.Set_Fields("BillingPartyID", txtTenant1CustNum.Text);
					rsSave.Set_Fields("SecondBillingPartyID", txtTenant2CustNum.Text);
					lngErrCode = 6;
					rsSave.Set_Fields("StreetNumber", Strings.Trim(txtLocationNumber.Text));
					rsSave.Set_Fields("StreetName", txtLocationStreet.Text);
					lngErrCode = 7;
					if (modUTStatusPayments.Statics.TownService == "S" || modUTStatusPayments.Statics.TownService == "B")
					{
						if (rsSave.Get_Fields_Int32("SewerCategory") != Conversion.Val(Strings.Left(cmbSCat.Text, 2)))
						{
							rsCat.OpenRecordset("SELECT * FROM Category WHERE Code = " + FCConvert.ToString(Conversion.Val(Strings.Left(cmbSCat.Text, 2))), modExtraModules.strUTDatabase);
							if (!rsCat.EndOfFile())
							{
								if (FCConvert.ToBoolean(rsCat.Get_Fields_Boolean("STaxable")))
								{
									boolCheckSTaxable = true;
								}
								else
								{
									boolCheckSTaxable = false;
								}
							}
							else
							{
								boolCheckSTaxable = false;
							}
						}
						else
						{
							boolCheckSTaxable = false;
						}
						rsSave.Set_Fields("SewerCategory", FCConvert.ToString(Conversion.Val(Strings.Left(cmbSCat.Text, 2))));
						// MAL@20080318:Change to update the meter table
						// Tracker Reference: 12701
						rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
						if (rsMeter.RecordCount() > 0)
						{
							rsMeter.MoveFirst();
							while (!rsMeter.EndOfFile())
							{
								rsMeter.Edit();
								rsMeter.Set_Fields("SCat", FCConvert.ToString(Conversion.Val(Strings.Left(cmbSCat.Text, 2))));
								rsMeter.Update();
								rsMeter.MoveNext();
							}
						}
						if (Strings.Trim(txtSewerAcct.Text) != "" && txtSewerAcct.Text.Replace("_", "").Replace("-", "").Length >= 3 && modGlobalConstants.Statics.gboolBD)
						{
							if (modValidateAccount.AccountValidate(txtSewerAcct.Text))
							{
								if (modMain.CheckDefaultFundForAccount_2(false, txtSewerAcct.Text))
								{
									rsSave.Set_Fields("SewerAccount", txtSewerAcct.Text);
								}
								else
								{
									rsSave.Set_Fields("SewerAccount", "");
									MessageBox.Show("Sewer account not saved correctly because the fund does not match the default.", "Invalid Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
							else
							{
								rsSave.Set_Fields("SewerAccount", "");
								MessageBox.Show("Sewer account not saved correctly.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						else if (!modGlobalConstants.Statics.gboolBD)
						{
							if (modValidateAccount.AccountValidate(txtSewerAcct.Text))
							{
								rsSave.Set_Fields("SewerAccount", txtSewerAcct.Text);
							}
							else
							{
								rsSave.Set_Fields("SewerAccount", "");
							}
						}
						else
						{
							rsSave.Set_Fields("SewerAccount", "");
						}
						//FC:FINAL:MSH - issue #931: incorrect operation. Should be saved result of selection (true/false)
						//rsSave.Set_Fields("SBillToOwner", cmbSBillTo.SelectedIndex = 0); // trout-718 08-02-2011 kgk
						rsSave.Set_Fields("SBillToOwner", cmbSBillTo.SelectedIndex == 0);
						// trout-718 08-02-2011 kgk
					}
					else
					{
						rsSave.Set_Fields("SewerCategory", 0);
						rsSave.Set_Fields("SewerAccount", "");
					}
					if (modUTStatusPayments.Statics.TownService == "W" || modUTStatusPayments.Statics.TownService == "B")
					{
						if (rsSave.Get_Fields_Int32("WaterCategory") != Conversion.Val(Strings.Left(cmbWCat.Text, 2)))
						{
							rsCat.OpenRecordset("SELECT * FROM Category WHERE Code = " + FCConvert.ToString(Conversion.Val(Strings.Left(cmbWCat.Text, 2))), modExtraModules.strUTDatabase);
							if (!rsCat.EndOfFile())
							{
								if (FCConvert.ToBoolean(rsCat.Get_Fields_Boolean("WTaxable")))
								{
									boolCheckWTaxable = true;
								}
								else
								{
									boolCheckWTaxable = false;
								}
							}
							else
							{
								boolCheckWTaxable = false;
							}
						}
						else
						{
							boolCheckWTaxable = false;
						}
						rsSave.Set_Fields("WaterCategory", FCConvert.ToString(Conversion.Val(Strings.Left(cmbWCat.Text, 2))));
						// MAL@20080318:Change to update the meter table
						// Tracker Reference: 12701
						rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
						if (rsMeter.RecordCount() > 0)
						{
							rsMeter.MoveFirst();
							while (!rsMeter.EndOfFile())
							{
								rsMeter.Edit();
								rsMeter.Set_Fields("WCat", FCConvert.ToString(Conversion.Val(Strings.Left(cmbWCat.Text, 2))));
								rsMeter.Update();
								rsMeter.MoveNext();
							}
						}
						if (Strings.Trim(txtWaterAcct.Text) != "" && txtWaterAcct.Text.Replace("-", "").Replace("_", "").Length >= 3 && modGlobalConstants.Statics.gboolBD)
						{
							if (modValidateAccount.AccountValidate(txtWaterAcct.Text))
							{
								if (modMain.CheckDefaultFundForAccount_2(true, txtWaterAcct.Text))
								{
									rsSave.Set_Fields("WaterAccount", txtWaterAcct.Text);
								}
								else
								{
									rsSave.Set_Fields("WaterAccount", "");
									MessageBox.Show("Water account not saved correctly because the fund does not match the default.", "Invalid Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								}
							}
							else
							{
								rsSave.Set_Fields("WaterAccount", "");
								MessageBox.Show("Water account not saved correctly.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
						}
						else if (!modGlobalConstants.Statics.gboolBD)
						{
							if (modValidateAccount.AccountValidate(txtWaterAcct.Text))
							{
								rsSave.Set_Fields("WaterAccount", txtWaterAcct.Text);
							}
							else
							{
								rsSave.Set_Fields("WaterAccount", "");
							}
						}
						else
						{
							rsSave.Set_Fields("WaterAccount", "");
						}
						rsSave.Set_Fields("WBillToOwner", cmbWBillTo.SelectedIndex == 0);
						// trout-718 08-02-2011 kgk
					}
					else
					{
						rsSave.Set_Fields("WaterCategory", 0);
						rsSave.Set_Fields("WaterAccount", "");
					}
					rsSave.Set_Fields("REAccount", FCConvert.ToString(Conversion.Val(txtREAcct.Text)));
					lngErrCode = 8;

					rsSave.Set_Fields("OwnerPartyID", txtOwner1CustNum.Text);
					rsSave.Set_Fields("SecondOwnerPartyID", txtOwner2CustNum.Text);
                    rsSave.Set_Fields("DeedName1",txtDeedName1.Text);
                    rsSave.Set_Fields("DeedName2",txtDeedName2.Text);
					lngErrCode = 9;
					rsSave.Set_Fields("DateOfChange", DateTime.Today);
					rsSave.Set_Fields("Comment", Strings.Trim(txtComment.Text));
					rsSave.Set_Fields("MapLot", Strings.Trim(txtREMapLot.Text));
					rsSave.Set_Fields("Book", FCConvert.ToString(Conversion.Val(Strings.Trim(txtREBook.Text))));
					rsSave.Set_Fields("Page", FCConvert.ToString(Conversion.Val(Strings.Trim(txtREPage.Text))));
					rsSave.Set_Fields("Code", 0);
					lngErrCode = 10;
					if (!FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("Deleted")))
					{
						// kk09262017 trouts-229  Leave the deleted flag alone if it is set
						rsSave.Set_Fields("Deleted", false);
					}
					rsSave.Set_Fields("Directions", Strings.Trim(txtDirections.Text));
					rsSave.Set_Fields("BillMessage", Strings.Trim(txtBillMessage.Text));
					rsSave.Set_Fields("DataEntry", Strings.Trim(txtDEMessage.Text));
					rsSave.Set_Fields("Telephone", Strings.Trim(txtPhone.Text));
					rsSave.Set_Fields("Email", Strings.Trim(txtEmail.Text));
					lngErrCode = 11;
					rsSave.Set_Fields("UseREAccount", FCConvert.CBool(chkUseRE.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("UseMortgageHolder", FCConvert.CBool(chkUseREMortgage.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("SameBillOwner", FCConvert.CBool(chkBillSameAsOwner.CheckState == Wisej.Web.CheckState.Checked));
					// not sure about this
					lngErrCode = 12;
					rsSave.Set_Fields("BookPage", "B" + FCConvert.ToString(Conversion.Val(txtREBook.Text)) + "P" + FCConvert.ToString(Conversion.Val(txtREPage.Text)));
					rsSave.Set_Fields("FinalBill", FCConvert.CBool(chkFinal.CheckState == Wisej.Web.CheckState.Checked));
					if (FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("FinalBill")))
					{
						// Set all meters to final billed
						SetAllMetersToFinalBilled();
					}
					rsSave.Set_Fields("NoBill", FCConvert.CBool(chkNoBill.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("EmailBill", FCConvert.CBool(chkEmailBills.CheckState == Wisej.Web.CheckState.Checked));
					rsSave.Set_Fields("Deposit", 0);
					// kk 03052013 trouts-6  Add field for Impervious Surface Area
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						if (txtImpervSurf.Text != "")
						{
							// kk07082015 trouts-149  Indicate account is not billed for SW by setting the Value to -1
							rsSave.Set_Fields("ImpervSurfArea", txtImpervSurf.Text);
						}
						else
						{
							rsSave.Set_Fields("ImpervSurfArea", -1);
						}
					}
					lngErrCode = 13;
					rsSave.Update();
					lngErrCode = 14;
					lngErrCode = 15;
					// save meter information
					// kk11202014 FindFirst is too slow with large data sets
					// rsSave.OpenRecordset "SELECT * FROM MeterTable", strUTDatabase
					lngErrCode = 16;
					for (intCT = 1; intCT <= 99; intCT++)
					{
						if (MeterKeyArray[intCT].Used)
						{
							// if this meter is used, then save the information
							lngErrCode = 17;
							if (MeterKeyArray[intCT].Key == 0)
							{
								// add a new meter and save the key in the the array
								lngErrCode = 18;
								rsSave.AddNew();
								lngErrCode = 19;
								rsSave.Update();
								lngErrCode = 20;
								MeterKeyArray[intCT].Key = FCConvert.ToInt32(rsSave.Get_Fields_Int32("ID"));
								lngErrCode = 21;
							}
							lngErrCode = 22;
							// kk11202014 FindFirst is too slow with large data sets
							// find the meter in the recordset
							// rsSave.FindFirstRecord "ID", .Key
							rsSave.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(MeterKeyArray[intCT].Key), modExtraModules.strUTDatabase);
							if (rsSave.EndOfFile() || rsSave.BeginningOfFile())
							{
								// If rsSave.NoMatch Then
								SaveAccount = false;
								MessageBox.Show("Meter " + FCConvert.ToString(intCT) + " for account " + FCConvert.ToString(Conversion.Val(txtAccountNumber.Text)) + " not found.", "Meter Save Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							else
							{
								lngErrCode = 23;
								rsSave.Edit();
								lngErrCode = 24;
								// save this meter's information
								rsSave.Set_Fields("MeterNumber", MeterKeyArray[intCT].MeterNumber);
								if (MeterKeyArray[intCT].MeterNumber == 1)
								{
									// set the master's UTBook field to match
									rsBookUpdate.OpenRecordset("SELECT * FROM Master WHERE ID = " + FCConvert.ToString(MeterKeyArray[intCT].AccountKey), modExtraModules.strUTDatabase);
									if (!rsBookUpdate.EndOfFile())
									{
										rsBookUpdate.Edit();
										rsBookUpdate.Set_Fields("UTBook", MeterKeyArray[intCT].BookNumber);
										rsBookUpdate.Update();
									}
								}
								rsSave.Set_Fields("AccountKey", lngKey);
								lngErrCode = 25;
								rsSave.Set_Fields("BooKNumber", MeterKeyArray[intCT].BookNumber);
								strBookString += FCConvert.ToString(MeterKeyArray[intCT].BookNumber) + ",";
								rsSave.Set_Fields("Sequence", MeterKeyArray[intCT].Sequence);
								rsSave.Set_Fields("SerialNumber", MeterKeyArray[intCT].SerialNumber);
								rsSave.Set_Fields("Size", MeterKeyArray[intCT].Size);
								lngErrCode = 26;
								rsSave.Set_Fields("Service", MeterKeyArray[intCT].Service);
								rsSave.Set_Fields("Frequency", MeterKeyArray[intCT].Frequency);
								rsSave.Set_Fields("Digits", MeterKeyArray[intCT].Digits);
								rsSave.Set_Fields("Multiplier", MeterKeyArray[intCT].Multiplier);
								if (MeterKeyArray[intCT].MeterNumber != 1)
								{
									rsSave.Set_Fields("NegativeConsumption", MeterKeyArray[intCT].NegativeConsumption);
								}
								else
								{
									rsSave.Set_Fields("NegativeConsumption", false);
								}
								lngErrCode = 27;
								if (MeterKeyArray[intCT].MeterNumber > 1)
								{
									if (Strings.Trim(MeterKeyArray[intCT].Combine) == "B" || Strings.Trim(MeterKeyArray[intCT].Combine) == "C")
									{
										rsSave.Set_Fields("Combine", MeterKeyArray[intCT].Combine);
									}
									else
									{
										rsSave.Set_Fields("Combine", "N");
									}
								}
								else
								{
									rsSave.Set_Fields("Combine", "N");
								}
								rsSave.Set_Fields("ReplacementConsumption", MeterKeyArray[intCT].ReplacementConsumption);
								rsSave.Set_Fields("ReplacementDate", MeterKeyArray[intCT].ReplacementDate);
								rsSave.Set_Fields("Location", MeterKeyArray[intCT].Location);
								lngErrCode = 28;
								rsSave.Set_Fields("XRef1", MeterKeyArray[intCT].XRef1);
								rsSave.Set_Fields("XRef2", MeterKeyArray[intCT].XRef2);
								rsSave.Set_Fields("Long", MeterKeyArray[intCT].Longitude);
								rsSave.Set_Fields("Lat", MeterKeyArray[intCT].Latitude);
								// water fields
								rsSave.Set_Fields("WaterPercent", MeterKeyArray[intCT].WaterPercent);
								rsSave.Set_Fields("WaterTaxPercent", MeterKeyArray[intCT].WaterTaxPercent);
								if (MeterKeyArray[intCT].WaterTaxPercent == 0 && boolCheckWTaxable && MeterKeyArray[intCT].Service != "S")
								{
									MessageBox.Show("The water taxable percentage is 0 on a taxable category for meter #" + FCConvert.ToString(MeterKeyArray[intCT].MeterNumber) + ".", "Zero Percent Taxable", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								lngErrCode = 29;
								rsSave.Set_Fields("WaterType1", MeterKeyArray[intCT].WaterType1);
								// 1-Consumption 2-Flat 3-Unit
								rsSave.Set_Fields("WaterType2", MeterKeyArray[intCT].WaterType2);
								rsSave.Set_Fields("WaterType3", MeterKeyArray[intCT].WaterType3);
								rsSave.Set_Fields("WaterType4", MeterKeyArray[intCT].WaterType4);
								rsSave.Set_Fields("WaterType5", MeterKeyArray[intCT].WaterType5);
								lngErrCode = 30;
								rsSave.Set_Fields("WaterAmount1", MeterKeyArray[intCT].WaterAmount1);
								rsSave.Set_Fields("WaterAmount2", MeterKeyArray[intCT].WaterAmount2);
								rsSave.Set_Fields("WaterAmount3", MeterKeyArray[intCT].WaterAmount3);
								rsSave.Set_Fields("WaterAmount4", MeterKeyArray[intCT].WaterAmount4);
								rsSave.Set_Fields("WaterAmount5", MeterKeyArray[intCT].WaterAmount5);
								lngErrCode = 31;
								rsSave.Set_Fields("WaterKey1", MeterKeyArray[intCT].WaterKey1);
								rsSave.Set_Fields("WaterKey2", MeterKeyArray[intCT].WaterKey2);
								rsSave.Set_Fields("WaterKey3", MeterKeyArray[intCT].WaterKey3);
								rsSave.Set_Fields("WaterKey4", MeterKeyArray[intCT].WaterKey4);
								rsSave.Set_Fields("WaterKey5", MeterKeyArray[intCT].WaterKey5);
								lngErrCode = 32;
								rsSave.Set_Fields("WaterAccount1", MeterKeyArray[intCT].WaterAccount1);
								rsSave.Set_Fields("WaterAccount2", MeterKeyArray[intCT].WaterAccount2);
								rsSave.Set_Fields("WaterAccount3", MeterKeyArray[intCT].WaterAccount3);
								rsSave.Set_Fields("WaterAccount4", MeterKeyArray[intCT].WaterAccount4);
								rsSave.Set_Fields("WaterAccount5", MeterKeyArray[intCT].WaterAccount5);
								lngErrCode = 33;
								rsSave.Set_Fields("WaterConsumption", MeterKeyArray[intCT].WaterConsumption);
								rsSave.Set_Fields("WaterConsumptionOverride", MeterKeyArray[intCT].WaterConsumptionOverride);
								lngErrCode = 34;
								rsSave.Set_Fields("WaterAdjustAmount", MeterKeyArray[intCT].WaterAdjustAmount);
								rsSave.Set_Fields("WaterAdjustKey", MeterKeyArray[intCT].WaterAdjustKey);
								rsSave.Set_Fields("WaterAdjustAccount", MeterKeyArray[intCT].WaterAdjustAccount);
								rsSave.Set_Fields("WaterAdjustDescription", MeterKeyArray[intCT].WaterAdjustDescription);
								// Sewer fields
								lngErrCode = 35;
								rsSave.Set_Fields("SewerPercent", MeterKeyArray[intCT].SewerPercent);
								rsSave.Set_Fields("SewerTaxPercent", MeterKeyArray[intCT].SewerTaxPercent);
								if (MeterKeyArray[intCT].SewerTaxPercent == 0 && boolCheckSTaxable && MeterKeyArray[intCT].Service != "W")
								{
									MessageBox.Show("The sewer taxable percentage is 0 on a taxable category for meter #" + FCConvert.ToString(MeterKeyArray[intCT].MeterNumber) + ".", "Zero Percent Taxable", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								lngErrCode = 35;
								rsSave.Set_Fields("SewerType1", MeterKeyArray[intCT].SewerType1);
								rsSave.Set_Fields("SewerType2", MeterKeyArray[intCT].SewerType2);
								rsSave.Set_Fields("SewerType3", MeterKeyArray[intCT].SewerType3);
								rsSave.Set_Fields("SewerType4", MeterKeyArray[intCT].SewerType4);
								rsSave.Set_Fields("SewerType5", MeterKeyArray[intCT].SewerType5);
								lngErrCode = 36;
								rsSave.Set_Fields("SewerAmount1", MeterKeyArray[intCT].SewerAmount1);
								rsSave.Set_Fields("SewerAmount2", MeterKeyArray[intCT].SewerAmount2);
								rsSave.Set_Fields("SewerAmount3", MeterKeyArray[intCT].SewerAmount3);
								rsSave.Set_Fields("SewerAmount4", MeterKeyArray[intCT].SewerAmount4);
								rsSave.Set_Fields("SewerAmount5", MeterKeyArray[intCT].SewerAmount5);
								lngErrCode = 37;
								rsSave.Set_Fields("SewerKey1", MeterKeyArray[intCT].SewerKey1);
								rsSave.Set_Fields("SewerKey2", MeterKeyArray[intCT].SewerKey2);
								rsSave.Set_Fields("SewerKey3", MeterKeyArray[intCT].SewerKey3);
								rsSave.Set_Fields("SewerKey4", MeterKeyArray[intCT].SewerKey4);
								rsSave.Set_Fields("SewerKey5", MeterKeyArray[intCT].SewerKey5);
								lngErrCode = 38;
								rsSave.Set_Fields("SewerAccount1", MeterKeyArray[intCT].SewerAccount1);
								rsSave.Set_Fields("SewerAccount2", MeterKeyArray[intCT].SewerAccount2);
								rsSave.Set_Fields("SewerAccount3", MeterKeyArray[intCT].SewerAccount3);
								rsSave.Set_Fields("SewerAccount4", MeterKeyArray[intCT].SewerAccount4);
								rsSave.Set_Fields("SewerAccount5", MeterKeyArray[intCT].SewerAccount5);
								lngErrCode = 39;
								rsSave.Set_Fields("SewerAdjustAmount", MeterKeyArray[intCT].SewerAdjustAmount);
								rsSave.Set_Fields("SewerAdjustKey", MeterKeyArray[intCT].SewerAdjustKey);
								rsSave.Set_Fields("SewerAdjustAccount", MeterKeyArray[intCT].SewerAdjustAccount);
								rsSave.Set_Fields("SewerAdjustDescription", MeterKeyArray[intCT].SewerAdjustDescription);
								rsSave.Set_Fields("SewerConsumption", MeterKeyArray[intCT].SewerConsumption);
								rsSave.Set_Fields("SewerConsumptionOverride", MeterKeyArray[intCT].SewerConsumptionOverride);
								lngErrCode = 40;
								rsSave.Set_Fields("PreviousReading", MeterKeyArray[intCT].PreviousReading);
								rsSave.Set_Fields("PreviousCode", MeterKeyArray[intCT].PreviousCode);
								rsSave.Set_Fields("PreviousReadingDate", MeterKeyArray[intCT].PreviousReadingDate);
								// MAL@20081104: If Book is in DE status, update the bill record readings
								// Tracker Reference: 15612
								UpdateCurrentBillRecord(lngKey, MeterKeyArray[intCT].Key, MeterKeyArray[intCT].BookNumber, MeterKeyArray[intCT].PreviousReading);
								lngErrCode = 41;
								rsSave.Set_Fields("CurrentReading", MeterKeyArray[intCT].CurrentReading);
								rsSave.Set_Fields("CurrentCode", MeterKeyArray[intCT].CurrentCode);
								rsSave.Set_Fields("CurrentReadingDate", MeterKeyArray[intCT].CurrentReadingDate);
								lngErrCode = 42;
								rsSave.Set_Fields("DateOfChange", MeterKeyArray[intCT].DateOfChange);
								rsSave.Set_Fields("Remote", MeterKeyArray[intCT].Remote);
								// MAL@20080422: Remove to prevent reset of Category values
								// Tracker Reference: 12701
								// lngErrCode = 43
								// rsSave.Fields("WCat") = Val(.WCat)
								// lngErrCode = 44
								// rsSave.Fields("SCat") = Val(.SCat)
								lngErrCode = 45;
								rsSave.Set_Fields("Backflow", MeterKeyArray[intCT].BackFlow);
								rsSave.Set_Fields("BackflowDate", MeterKeyArray[intCT].BackFlowDate);
								lngErrCode = 46;
								rsSave.Set_Fields("SetDate", MeterKeyArray[intCT].SetDate);
								rsSave.Set_Fields("BillingCode", MeterKeyArray[intCT].BillingCode);
								lngErrCode = 47;
								rsSave.Set_Fields("Comment", MeterKeyArray[intCT].Comment);
								lngErrCode = 48;
								rsSave.Set_Fields("FinalBilled", MeterKeyArray[intCT].FinalBilled);
								lngErrCode = 49;
								rsSave.Set_Fields("NoBill", MeterKeyArray[intCT].NoBill);
								lngErrCode = 50;
								if (FCConvert.ToString(rsSave.Get_Fields_String("BillingStatus")) != MeterKeyArray[intCT].BillingStatus)
								{
									modGlobalFunctions.AddCYAEntry_728("UT", "Reset Meter Status", "Account Number : " + FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT), "Meter Key : " + FCConvert.ToString(modMain.Statics.lngCurrentMeterKey), "Meter Number : " + FCConvert.ToString(lngCurrentMeterNumber), rsSave.Get_Fields_String("BillingStatus") + " to " + MeterKeyArray[intCT].BillingStatus);
									rsSave.Set_Fields("BillingStatus", MeterKeyArray[intCT].BillingStatus);
								}
								lngErrCode = 51;
								rsSave.Set_Fields("Radio", MeterKeyArray[intCT].RadioAccess);
								lngErrCode = 52;
								rsSave.Set_Fields("IncludeInExtract", MeterKeyArray[intCT].IncludeInExtract);
								lngErrCode = 53;
								rsSave.Set_Fields("MXU", Strings.Trim(MeterKeyArray[intCT].MXUNumber) + " ");
								rsSave.Set_Fields("ReaderCode", Strings.Trim(MeterKeyArray[intCT].ReaderCode) + " ");
								// kk 01182013 trout-896
								// check for bills that have not been completed
								AdjustUnBilledBillRecords(ref MeterKeyArray[intCT].AccountKey, ref MeterKeyArray[intCT].Key);
								lngErrCode = 54;
								rsSave.Set_Fields("UseRate1", MeterKeyArray[intCT].UseRate1);
								rsSave.Set_Fields("UseRate2", MeterKeyArray[intCT].UseRate2);
								rsSave.Set_Fields("UseRate3", MeterKeyArray[intCT].UseRate3);
								rsSave.Set_Fields("UseRate4", MeterKeyArray[intCT].UseRate4);
								rsSave.Set_Fields("UseRate5", MeterKeyArray[intCT].UseRate5);
								rsSave.Set_Fields("UseAdjustRate", MeterKeyArray[intCT].UseAdjustRate);
								if (MeterKeyArray[intCT].UseAdjustRate)
								{
									rsSave.Set_Fields("WaterAdjustDescription", MeterKeyArray[intCT].WaterAdjustDescription);
									rsSave.Set_Fields("SewerAdjustDescription", MeterKeyArray[intCT].SewerAdjustDescription);
								}
								lngErrCode = 55;
								rsSave.Set_Fields("RadioAccessType", MeterKeyArray[intCT].RadioAccessType);
								rsSave.Update();
								lngErrCode = 60;
							}
						}
						// this will reset the book status of all of the books associated with this account
						if (Strings.Trim(strBookString).Length > 1)
						{
							// remove the last comma
							strBookString = Strings.Left(strBookString, strBookString.Length - 1);
							if (Strings.Trim(strBookString) != "")
							{
								rsBookUpdate.OpenRecordset("SELECT * FROM Book WHERE BookNumber IN (" + strBookString + ")", modExtraModules.strUTDatabase);
								while (!rsBookUpdate.EndOfFile())
								{
									modMain.ResetBookStatus_2(rsBookUpdate.Get_Fields_Int32("BookNumber"));
									rsBookUpdate.MoveNext();
								}
							}
						}
					}
					lngErrCode = 70;
					boolDirty = false;
					MessageBox.Show("Save Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return SaveAccount;
			}
			catch (Exception ex)
			{
				
				SaveAccount = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Account - " + FCConvert.ToString(lngErrCode), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveAccount;
		}

		private void mnuFileViewLinkedDocument_Click(object sender, System.EventArgs e)
        {
            var account = FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT);
            //frmViewDocuments.InstancePtr.Init(0, $"Account {account}  {oOwner1.FullNameLastFirst}",  modUTStatusPayments.Statics.lngCurrentAccountUT.ToString(), modUTStatusPayments.Statics.lngCurrentAccountUT,"");
            //frmDocumentViewer.InstancePtr.Unload();
            //frmDocumentViewer.InstancePtr.Init($"Account {account}  {oOwner1.FullNameLastFirst}", "UtilityBilling", "Account", modUTStatusPayments.Statics.lngCurrentAccountUT, "", (short)FCForm.FormShowEnum.Modal, false);

            if (modUTStatusPayments.Statics.lngCurrentAccountUT <= 0) return;
            StaticSettings.GlobalCommandDispatcher.Send(new ShowDocumentViewer($"Account {account}  {oOwner1.FullNameLastFirst}", "UtilityBilling", "Account",
                modUTStatusPayments.Statics.lngCurrentAccountUT, "", true, false));
            var documentHeaders = StaticSettings.GlobalCommandDispatcher.Send(new GetHeadersByReferenceCommand()).Result;
            
        }

        private void txtAccountNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtAdjustDescriptionS_TextChanged(object sender, System.EventArgs e)
		{
			MeterKeyArray[lngCurrentMeterNumber].SewerAdjustDescription = txtAdjustDescriptionS.Text;
		}

		private void txtAdjustDescriptionW_TextChanged(object sender, System.EventArgs e)
		{
			MeterKeyArray[lngCurrentMeterNumber].WaterAdjustDescription = txtAdjustDescriptionW.Text;
		}

		private void txtBillMessage_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}
		
		private void txtComment_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtDEMessage_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtDirections_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtEmail_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtImpervSurfArea_Change()
		{
			boolDirty = true;
		}

		private void txtLat_TextChanged(object sender, System.EventArgs e)
		{
			MeterKeyArray[lngCurrentMeterNumber].Latitude = txtLat.Text;
		}

		private void txtLocation_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				boolDirty = true;
				if (Strings.Trim(txtLocation.Text) != "")
				{
					MeterKeyArray[lngCurrentMeterNumber].Location = Strings.Trim(txtLocation.Text);
				}
			}
		}

		private void txtLocationNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLocationStreet_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLong_TextChanged(object sender, System.EventArgs e)
		{
			MeterKeyArray[lngCurrentMeterNumber].Longitude = txtLong.Text;
		}

		private void txtMXU_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				MeterKeyArray[lngCurrentMeterNumber].MXUNumber = Strings.Trim(txtMXU.Text);
			}
			boolDirty = true;
		}

		private void txtMXU_Enter(object sender, System.EventArgs e)
		{
			if (txtMXU.Visible && txtMXU.Enabled)
			{
				txtMXU.SelectionStart = 0;
				txtMXU.SelectionLength = txtMXU.Text.Length;
			}
		}

		private void txtMXU_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				MeterKeyArray[lngCurrentMeterNumber].MXUNumber = Strings.Trim(txtMXU.Text);
			}
			boolDirty = true;
		}

		private void txtReaderCode_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				MeterKeyArray[lngCurrentMeterNumber].ReaderCode = Strings.Trim(txtReaderCode.Text);
			}
			boolDirty = true;
		}

		private void txtReaderCode_Enter(object sender, System.EventArgs e)
		{
			if (txtReaderCode.Visible && txtReaderCode.Enabled)
			{
				txtReaderCode.SelectionStart = 0;
				txtReaderCode.SelectionLength = txtReaderCode.Text.Length;
			}
		}

		private void txtReaderCode_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				MeterKeyArray[lngCurrentMeterNumber].ReaderCode = Strings.Trim(txtReaderCode.Text);
			}
			boolDirty = true;
		}

		private void SavePreviousOwnerInformation()
		{
			// this routine will fill in the previous owner table
			clsDRWrapper rsPrev = new clsDRWrapper();
			clsDRWrapper rsM = new clsDRWrapper();
			object strReturn;
			DateTime datSalesDate;
			strReturn = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			if (frmInput.InstancePtr.Init(ref strReturn, "Enter Sale Date", "Please enter the sale date for this property", 1700, false, modGlobalConstants.InputDTypes.idtDate))
			{
				datSalesDate = DateAndTime.DateValue(FCConvert.ToString(strReturn));
			}
			else
			{
				datSalesDate = DateTime.Today;
			}
			// k    rsM.OpenRecordset "SELECT * FROM Master WHERE AccountNumber = " & lngCurrentAccountUT, strUTDatabase
			int lngAcctKey = modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT);
			rsM.OpenRecordset(modUTStatusPayments.UTMasterQuery(lngAcctKey), modExtraModules.strUTDatabase);
			if (!rsM.EndOfFile())
			{
				rsPrev.OpenRecordset("SELECT * FROM PreviousOwner WHERE ID = 0", modExtraModules.strUTDatabase);
				rsPrev.AddNew();
				// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
				rsPrev.Set_Fields("OName", rsM.Get_Fields("OwnerName"));
				// strOwnerName
				// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
				rsPrev.Set_Fields("OName2", rsM.Get_Fields("SecondOwnerName"));
				rsPrev.Set_Fields("SaleDate", datSalesDate);
				rsPrev.Set_Fields("LastUpdated", DateTime.Now);
				rsPrev.Set_Fields("AccountKey", rsM.Get_Fields_Int32("ID"));
				rsPrev.Set_Fields("Address1", rsM.Get_Fields_String("OAddress1"));
				rsPrev.Set_Fields("Address2", rsM.Get_Fields_String("OAddress2"));
				rsPrev.Set_Fields("Address3", rsM.Get_Fields_String("OAddress3"));
				rsPrev.Set_Fields("City", rsM.Get_Fields_String("OCity"));
				rsPrev.Set_Fields("State", rsM.Get_Fields_String("OState"));
				rsPrev.Set_Fields("Zip", rsM.Get_Fields_String("OZip"));
				// rsPrev.Fields("Zip4") = rsM.Fields("OZip4")
				rsPrev.Update();
			}
		}

		private void txtPhone_ClickEvent(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtREAcct_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// get information from the re account
			int lngAcct = 0;
			if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
			{
				// if the user has real estate
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtREAcct.Text)));
				// Val(vsREInfo.TextMatrix(lngRowREAccount, lngColMasterData))   'get the account number
				if (lngAcct != 0)
				{
					// kk09132017 trouts-239  This is the code copied from chkUseRE_Clicked? Skip over it if user cleared the account number
					if (chkUseRE.CheckState != Wisej.Web.CheckState.Checked)
					{
						// this means that it is being checked
						LoadREInfo_6(lngAcct, false);
						txtREBook.ForeColor = Color.Black;
						txtREPage.ForeColor = Color.Black;
						txtREMapLot.ForeColor = Color.Black;
						lblREBook.ForeColor = Color.Black;
						lblREPage.ForeColor = Color.Black;
						lblREMapLot.ForeColor = Color.Black;
					}
					else
					{
						LoadREInfo_6(lngAcct, true);
						txtREBook.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						txtREPage.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						txtREMapLot.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						lblREBook.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						lblREPage.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						lblREMapLot.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
					}
				}
				boolDirty = true;
			}
			else
			{
				// fraREInfo.Enabled = False
			}
		}

		private void txtRemoteNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtRemoteNumber_Enter(object sender, System.EventArgs e)
		{
			if (txtRemoteNumber.Visible && txtRemoteNumber.Enabled)
			{
				txtRemoteNumber.SelectionStart = 0;
				txtRemoteNumber.SelectionLength = txtRemoteNumber.Text.Length;
			}
		}
		//private void txtRemoteNumber_Validating_2(bool Cancel) { txtRemoteNumber_Validating(ref Cancel); }
		private void txtRemoteNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (Strings.Trim(txtRemoteNumber.Text) != "")
				{
					MeterKeyArray[lngCurrentMeterNumber].Remote = Strings.Trim(txtRemoteNumber.Text);
				}
				else
				{
					MeterKeyArray[lngCurrentMeterNumber].Remote = "";
				}
			}
			boolDirty = true;
		}

		public void txtRemoteNumber_Validate(bool Cancel)
		{
			txtRemoteNumber_Validating(txtRemoteNumber, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtSequence_Enter(object sender, System.EventArgs e)
		{
			if (txtSequence.Visible && txtSequence.Enabled)
			{
				txtSequence.SelectionStart = 0;
				txtSequence.SelectionLength = txtSequence.Text.Length;
			}
		}

		private void txtSequence_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		//private void txtSequence_Validating_2(bool Cancel) { txtSequence_Validating(ref Cancel); }
		private void txtSequence_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBook = new clsDRWrapper();
				int lngBook = 0;
				int lngSeq = 0;
				if (!boolNoSaveMeter)
				{
					if (Strings.Trim(txtSequence.Text) != "")
					{
						// check to see if any other meters have the same book/sequence
						if (cmbBook.SelectedIndex >= 0)
						{
							lngBook = cmbBook.ItemData(cmbBook.SelectedIndex);
						}
						else
						{
							lngBook = 0;
						}
						lngSeq = FCConvert.ToInt32(Math.Round(Conversion.Val(txtSequence.Text)));
						if (lngBook <= 0)
						{
							MessageBox.Show("Please select a book for this meter.", "No Book Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							e.Cancel = false;
							// If cmbBook.Enabled And cmbBook.Visible Then
							// cmbBook.SetFocus
							// End If
							return;
							// MAL@20080806 ; Tracker Reference: 14901
							// ElseIf Not ValidSequenceLength(lngSeq) Then
							// Cancel = True
							// Exit Sub
						}
						else
						{
							rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(lngBook));
							if (!rsBook.EndOfFile())
							{
								rsBook.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(lngBook) + " AND Sequence = " + FCConvert.ToString(lngSeq));
								if (rsBook.EndOfFile())
								{
								}
								else
								{
									if (Conversion.Val(rsBook.Get_Fields_Int32("MeterKey")) == MeterKeyArray[lngCurrentMeterNumber].Key)
									{
										// this is the same meter
										// MeterKeyArray(lngCurrentMeterNumber).Sequence = Trim(txtSequence.Text)
									}
									else
									{
										MessageBox.Show("The sequence selected is already in use by account " + FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsBook.Get_Fields_Int32("AccountKey"))) + ".  Please select a unique sequence.", "Duplicate Sequence", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										// Case vbYes
										// allow it but add a cya entry
										// AddCYAEntry "UT", "Duplicate Book/Sequence", "Account = " & GetAccountNumber(MeterKeyArray(lngCurrentMeterNumber).AccountKey), "Meter Key = " & MeterKeyArray(lngCurrentMeterNumber).Key, "Book = " & lngBook, "Seq = " & Trim(txtSequence.Text)
										// MeterKeyArray(lngCurrentMeterNumber).Sequence = Trim(txtSequence.Text)
										// Case vbNo, vbCancel
										e.Cancel = true;
										return;
										// End Select
									}
								}
								// If ChangeSeqNumber(lngBook, lngSeq, MeterKeyArray(lngCurrentMeterNumber).BookNumber, MeterKeyArray(lngCurrentMeterNumber).Sequence) Then
								MeterKeyArray[lngCurrentMeterNumber].Sequence = FCConvert.ToInt32(Strings.Trim(txtSequence.Text));
								// Else
								// MsgBox "Sequence not changed", vbCritical, "Error Changing Seq"
								// End If
							}
							else
							{
								MessageBox.Show("The book selected could not be found in the book table.", "Invalid Book Selected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = false;
								if (cmbBook.Enabled && cmbBook.Visible)
								{
									cmbBook.Focus();
								}
								return;
							}
						}
					}
				}
				if (!e.Cancel)
				{
					boolDirty = true;
				}
				return;
			}
			catch (Exception ex)
			{
				
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Master Screen", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void txtSequence_Validate(bool Cancel)
		{
			txtSequence_Validating(txtSequence, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtSerialNumber_TextChanged(object sender, System.EventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (Strings.Trim(txtSerialNumber.Text) != "")
				{
					MeterKeyArray[lngCurrentMeterNumber].SerialNumber = Strings.Trim(txtSerialNumber.Text);
				}
			}
			boolDirty = true;
		}

		private void txtSerialNumber_Enter(object sender, System.EventArgs e)
		{
			if (txtSerialNumber.Visible && txtSerialNumber.Enabled)
			{
				txtSerialNumber.SelectionStart = 0;
				txtSerialNumber.SelectionLength = txtSerialNumber.Text.Length;
			}
		}
		//private void txtSerialNumber_Validating_2(bool Cancel) { txtSerialNumber_Validating(ref Cancel); }
		private void txtSerialNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (!boolNoSaveMeter)
			{
				if (Strings.Trim(txtSerialNumber.Text) != "")
				{
					MeterKeyArray[lngCurrentMeterNumber].SerialNumber = Strings.Trim(txtSerialNumber.Text);
				}
				else
				{
					MeterKeyArray[lngCurrentMeterNumber].SerialNumber = "";
					// trout-747 08-01-2011  Allow user to clear S/N
				}
			}
			boolDirty = true;
		}

		public void txtSerialNumber_Validate(bool Cancel)
		{
			txtSerialNumber_Validating(txtSerialNumber, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtSewerAcct_Change()
		{
			boolDirty = true;
			if (modGlobalConstants.Statics.gboolBD)
			{
				if (txtSewerAcct.Text.Length > 2)
				{
					ToolTip1.SetToolTip(txtSewerAcct, modAccountTitle.ReturnAccountDescription(txtSewerAcct.Text));
					// lblSewerAcctDesc.Caption = ReturnAccountDescription(txtSewerAcct.Text)
				}
			}
		}

		private void txtSewerAcct_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDirty = true;
			if (modGlobalConstants.Statics.gboolBD)
			{
				if (txtSewerAcct.EditText.Length > 2)
				{
					ToolTip1.SetToolTip(txtSewerAcct, modAccountTitle.ReturnAccountDescription(txtSewerAcct.EditText));
				}
			}
		}

		private void txtSewerAcct_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtSewerAcct.Text);
				if (strAcct != "")
				{
					txtSewerAcct.Text = strAcct;
				}
			}
		}

		private void txtSewerAcct_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtSewerAcct.Text != "" && txtSewerAcct.Text.Replace("-", "").Replace("_", "").Length >= 3)
			{
				if (!modMain.CheckDefaultFundForAccount_2(false, txtSewerAcct.Text))
				{
					if (txtSewerAcct.Visible)
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					txtSewerAcct.Text = "";
				}
			}
		}

		private void txtWaterAcct_Change()
		{
			boolDirty = true;
			if (modGlobalConstants.Statics.gboolBD)
			{
				if (txtWaterAcct.Text.Length > 2)
				{
					ToolTip1.SetToolTip(txtWaterAcct, modAccountTitle.ReturnAccountDescription(txtWaterAcct.Text));
					// lblWaterAcctDesc.Caption = ReturnAccountDescription(txtWaterAcct.Text)
					// lblWaterAcctDesc.Refresh
				}
			}
		}

		private void txtWaterAcct_ChangeEdit(object sender, System.EventArgs e)
		{
			boolDirty = true;
			if (modGlobalConstants.Statics.gboolBD)
			{
				if (txtWaterAcct.EditText.Length > 2)
				{
					ToolTip1.SetToolTip(txtWaterAcct, modAccountTitle.ReturnAccountDescription(txtWaterAcct.EditText));
				}
			}
		}

		private void txtWaterAcct_KeyDownEvent(object sender, KeyEventArgs e)
		{
			string strAcct = "";
			if (e.KeyCode == Keys.F2)
			{
				strAcct = frmLoadValidAccounts.InstancePtr.Init(txtWaterAcct.Text);
				if (strAcct != "")
				{
					txtWaterAcct.Text = strAcct;
				}
			}
		}

		private void txtWaterAcct_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (txtWaterAcct.Text != "" && txtWaterAcct.Text.Replace("-", "").Replace("_", "").Length >= 3)
			{
				if (!modMain.CheckDefaultFundForAccount_2(true, txtWaterAcct.Text))
				{
					if (txtWaterAcct.Visible)
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					txtWaterAcct.Text = "";
				}
			}
		}

		private void txtXRef1_TextChanged(object sender, System.EventArgs e)
		{
			MeterKeyArray[lngCurrentMeterNumber].XRef1 = txtXRef1.Text;
		}

		private void txtXRef2_TextChanged(object sender, System.EventArgs e)
		{
			MeterKeyArray[lngCurrentMeterNumber].XRef2 = txtXRef2.Text;
		}

		private void vsAccountInfo_RowColChange(object sender, System.EventArgs e)
		{
			vsAccountInfo.Editable = FCGrid.EditableSettings.flexEDNone;
			if (vsAccountInfo.Row == vsAccountInfo.Rows - 1 && vsAccountInfo.Col == vsAccountInfo.Cols - 1)
			{
				// if this is the last cell in the grid then change the tab behavior
				vsAccountInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsAccountInfo.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsAccountInfo_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (!e.Cancel)
			{
				boolDirty = true;
			}
		}

		private void vsChangeOut_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsChangeOut[e.ColumnIndex, e.RowIndex];
            if (vsChangeOut.GetFlexColIndex(e.ColumnIndex) == lngColChangeReason && vsChangeOut.GetFlexRowIndex(e.RowIndex) > 0)
			{
				//ToolTip1.SetToolTip(vsChangeOut, vsChangeOut.TextMatrix(vsChangeOut.MouseRow, lngColChangeReason));
				cell.ToolTipText = vsChangeOut.TextMatrix(vsChangeOut.GetFlexRowIndex(e.RowIndex), lngColChangeReason);
			}
		}
		
		private void vsMeter_ChangeEdit(object sender, System.EventArgs e)
		{
			ToolTip1.SetToolTip(vsMeter, "");
			// only check this on the correct row/col
			if ((vsMeter.Col == lngColMeterWAccount || vsMeter.Col == lngColMeterSAccount) && (vsMeter.Row == lngRowMeterPayment1 || vsMeter.Row == lngRowMeterPayment2 || vsMeter.Row == lngRowMeterPayment3 || vsMeter.Row == lngRowMeterPayment4 || vsMeter.Row == lngRowMeterPayment5))
			{
				// if this is the account col then
				if (modGlobalConstants.Statics.gboolBD)
				{
					// lblAccountDescription.Caption = ReturnAccountDescription(vsMeter.EditText)
					ToolTip1.SetToolTip(vsMeter, modAccountTitle.ReturnAccountDescription(vsMeter.EditText));
				}
			}
		}
		
		private void vsMeter_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// do not let characters into the amount fields unless it is a number
			if (vsMeter.Col == lngColMeterWAmount || vsMeter.Col == lngColMeterSAmount)
			{
				if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == (Keys)46) || (KeyAscii == Keys.Return))
				{
					// allow these keys
				}
				else if (KeyAscii == (Keys)45)
				{
					// if this is selected then negate the value in the field
					vsMeter.EditText = FCConvert.ToString(Conversion.Val(vsMeter.EditText) * -1);
					KeyAscii = 0;
				}
				else if ((KeyAscii == Keys.C) || (KeyAscii == Keys.C + 32))
				{
					vsMeter.EditText = FCConvert.ToString(0);
					KeyAscii = 0;
				}
				else
				{
					KeyAscii = 0;
				}

			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		
		private void vsMeter_Leave(object sender, System.EventArgs e)
		{
			if (vsMeter.TabBehavior == FCGrid.TabBehaviorSettings.flexTabControls)
			{
				vsMeter.Select(lngRowMeterPayment1, lngColMeterCheck);
			}
		}

		private void vsMeter_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsMeter[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = vsMeter.GetFlexRowIndex(e.RowIndex);
			lngMC = vsMeter.GetFlexColIndex(e.ColumnIndex);
            //ToolTip1.SetToolTip(vsMeter, "");
            cell.ToolTipText = "";
			if (lngMR > lngRowMeterHeading)
			{
				if (lngMC == lngColMeterWAmount)
				{
					switch (vsMeter.TextMatrix(lngMR, lngColMeterWType).Trim())
					{
						case "1":
							{
								// cons
								//ToolTip1.SetToolTip(vsMeter, "Overrides all other consumption values.");
								cell.ToolTipText = "Overrides all other consumption values.";
								break;
							}
						case "2":
							{
								// flat
								//ToolTip1.SetToolTip(vsMeter, "Overrides rate table values.");
								cell.ToolTipText = "Overrides rate table values.";
								break;
							}
						case "3":
							{
								// unit
								//ToolTip1.SetToolTip(vsMeter, "Number of units associated with this meter.");
								cell.ToolTipText =  "Number of units associated with this meter.";
								break;
							}
						default:
							{
                                //ToolTip1.SetToolTip(vsMeter, "");
                                cell.ToolTipText = "";
								break;
							}
					}
					//end switch
				}
				else if (lngMC == lngColMeterSAmount)
				{
					switch (vsMeter.TextMatrix(lngMR, lngColMeterSType).Trim())
					{
						case "1":
							{
								// cons
								//ToolTip1.SetToolTip(vsMeter, "Overrides all other consumption values.");
								cell.ToolTipText = "Overrides all other consumption values.";
								break;
							}
						case "2":
							{
								// flat
								//ToolTip1.SetToolTip(vsMeter, "Overrides rate table values.");
								cell.ToolTipText = "Overrides rate table values.";
								break;
							}
						case "3":
							{
								// unit
								//ToolTip1.SetToolTip(vsMeter, "Number of units associated with this meter.");
								cell.ToolTipText = "Number of units associated with this meter.";
								break;
							}
						default:
							{
                                //ToolTip1.SetToolTip(vsMeter, "");
                                cell.ToolTipText = "";
								break;
							}
					}
					//end switch
				}
			}
		}

		private void vsMeter_RowColChange(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngRow = 0;
				int lngCol = 0;
				bool boolDoNotUse = false;
				lngRow = vsMeter.Row;
				lngCol = vsMeter.Col;
				if (!boolChangingRowCol)
				{
					boolChangingRowCol = true;
					if (vsMeter.Col >= lngColMeterSTitle)
					{
						// it is a sewer column
						boolDoNotUse = FCConvert.CBool(FCConvert.ToBoolean(Conversion.Val(vsMeter.TextMatrix(lngRow, lngColMeterSHidden)) == modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT));
					}
					else if (vsMeter.Col >= lngColMeterWTitle)
					{
						// it is a water column
						boolDoNotUse = FCConvert.CBool(FCConvert.ToBoolean(Conversion.Val(vsMeter.TextMatrix(lngRow, lngColMeterWHidden)) == modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT));
					}
					if (boolDoNotUse)
					{
						// if this cell is grayed out then do not allow editing
						vsMeter.Editable = FCGrid.EditableSettings.flexEDNone;
						// MAL@20070906: Move column to not allow account number process
						if (vsMeter.Col == vsMeter.Cols - 1)
						{
							// Last column in grid (move to next row)
							vsMeter.Row += 1;
							vsMeter.Col = lngColMeterCheck;
						}
						else
						{
							vsMeter.Col += 1;
						}
					}
					else
					{
						vsMeter.ComboList = "";
						if (Conversion.Val(vsMeter.TextMatrix(lngRow, lngColMeterCheck)) == -1 || lngCol == lngColMeterCheck)
						{
							if (lngCol == lngColMeterCheck)
							{
								// If lngRow = lngRowMeterAdjustDescription Then
								// do not let the user click the adjustment description line
								// .Editable = flexEDNone
								// .TextMatrix(lngRowMeterAdjustDescription, lngColMeterCheck) = .TextMatrix(lngRowMeterAdjust, lngColMeterCheck)
								// Else
								vsMeter.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								// .EditCell
								// End If
							}
							else if (lngCol == lngColMeterWTitle || lngCol == lngColMeterSTitle)
							{
								if (lngRow == lngRowMeterPayment1 || lngRow == lngRowMeterPayment2 || lngRow == lngRowMeterPayment3 || lngRow == lngRowMeterPayment4 || lngRow == lngRowMeterPayment5)
								{
									if (Conversion.Val(cmbMeterNumber.Items[cmbMeterNumber.SelectedIndex].ToString()) > 1 && cmbCombineMeter.SelectedIndex == 2)
									{
										vsMeter.ComboList = strRateTypeComboList2;
									}
									else
									{
										vsMeter.ComboList = strRateTypeComboList;
									}
									vsMeter.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
									vsMeter.EditCell();
								}
								else
								{
									vsMeter.Editable = FCGrid.EditableSettings.flexEDNone;
								}
							}
							else if (lngCol == lngColMeterWHidden)
							{
							}
							else if (lngCol == lngColMeterWAmount)
							{
								vsMeter.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsMeter.EditCell();
							}
							else if (lngCol == lngColMeterWRateTable || lngCol == lngColMeterSRateTable)
							{
								if (lngRow != lngRowMeterAdjust)
								{
									vsMeter.ComboList = strRateComboList;
									vsMeter.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
									vsMeter.EditCell();
								}
								else
								{
									vsMeter.Editable = FCGrid.EditableSettings.flexEDNone;
								}
							}
							else if (lngCol == lngColMeterWAccount)
							{
								// Debug.Print "RowColChange Start TM - "; vsMeter.TextMatrix(vsMeter.Row, vsMeter.Col) & "   ET - " & vsMeter.EditText & "   SS - " & vsMeter.EditSelStart
								// If lngRow <> lngRowMeterAdjustDescription Then
								vsMeter.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								// show the account box
								modNewAccountBox.SetGridFormat(vsMeter, vsMeter.Row, vsMeter.Col, true);
								// Else
								// .Editable = flexEDNone
								// End If
								// Debug.Print "RowColChange End   TM - "; vsMeter.TextMatrix(vsMeter.Row, vsMeter.Col) & "   ET - " & vsMeter.EditText & "   SS - " & vsMeter.EditSelStart
							}
							else if (lngCol == lngColMeterSHidden)
							{
							}
							else if (lngCol == lngColMeterSAmount)
							{
								vsMeter.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								vsMeter.EditCell();
							}
							else if (lngCol == lngColMeterSAccount)
							{
								// If lngRow <> lngRowMeterAdjustDescription Then
								vsMeter.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
								// show the account box
								modNewAccountBox.SetGridFormat(vsMeter, vsMeter.Row, vsMeter.Col, true);
								// Else
								// .Editable = flexEDNone
								// End If
							}
						}
						else
						{
							// this row is not selected
							vsMeter.Editable = FCGrid.EditableSettings.flexEDNone;
						}
					}
					if (vsMeter.Row == vsMeter.Rows - 1 && vsMeter.Col == vsMeter.Cols - 1)
					{
						// if this is the last cell in the grid then change the tab behavior
						vsMeter.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
					}
					else
					{
						vsMeter.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
					}
					boolChangingRowCol = false;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In RowCol Change", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsMeter_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - issue #916: added saving and using correct indexes of the cell
			int row = vsMeter.GetFlexRowIndex(e.RowIndex);
			int col = vsMeter.GetFlexColIndex(e.ColumnIndex);
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strText = "";
				if (!boolNoSaveMeter)
				{
					strText = vsMeter.EditText;
					if (lngCurrentMeterNumber > 0)
					{
						if (row == lngRowMeterTopHeader)
						{
							// header row
							if (col == lngColMeterCheck)
							{
								SetMeterRow(row);
							}
							else if (col == lngColMeterWTitle)
							{
								// changing the combo box
							}
							else if (col == lngColMeterWAmount)
							{
								// nothing
							}
							else if (col == lngColMeterWRateTable)
							{
								// nothing
							}
							else if (col == lngColMeterWAccount)
							{
								// nothing
							}
							else if (col == lngColMeterWHidden)
							{
								// nothing
							}
							else if (col == lngColMeterSTitle)
							{
								// changing the combo box
							}
							else if (col == lngColMeterSAmount)
							{
								// nothing
							}
							else if (col == lngColMeterSRateTable)
							{
								// nothing
							}
							else if (col == lngColMeterSAccount)
							{
								// nothing
							}
							else if (col == lngColMeterSHidden)
							{
								// nothing
							}
						}
						else if (row == lngRowMeterHeading)
						{
							// title row
							if (col == lngColMeterCheck)
							{
								SetMeterRow(row);
							}
							else if (col == lngColMeterWTitle)
							{
								// do nothing combo box
							}
							else if (col == lngColMeterWAmount)
							{
								// nothing
							}
							else if (col == lngColMeterWRateTable)
							{
								// nothing
							}
							else if (col == lngColMeterWAccount)
							{
								// nothing
							}
							else if (col == lngColMeterWHidden)
							{
								// nothing
							}
							else if (col == lngColMeterSTitle)
							{
								// do nothing combo box
							}
							else if (col == lngColMeterSAmount)
							{
								// nothing
							}
							else if (col == lngColMeterSRateTable)
							{
								// nothing
							}
							else if (col == lngColMeterSAccount)
							{
								// nothing
							}
							else if (col == lngColMeterSHidden)
							{
								// nothing
							}
							// here are the rows that are dynamic
						}
						else if (row == lngRowMeterPayment1)
						{
							if (col == lngColMeterCheck)
							{
								SetMeterRow(row);
							}
							else if (col == lngColMeterWTitle)
							{
								// 1-Consumption 2-Flat 3-Unit
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].WaterKey1, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterWType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].WaterType1 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterType1 = -1;
								}
							}
							else if (col == lngColMeterWAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount1 = FCUtils.Round(FCConvert.ToDouble(strText), 2);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount1 = 0;
								}
							}
							else if (col == lngColMeterWRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_2(true, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].WaterType1))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterKey1 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterKey1 = 0;
								}
							}
							else if (col == lngColMeterWAccount)
							{

								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount1 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount1 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAccount1 = "";
								}
							}
							else if (col == lngColMeterWHidden)
							{
								// nothing
							}
							else if (col == lngColMeterSTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].SewerKey1, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterSType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].SewerType1 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerType1 = -1;
								}
							}
							else if (col == lngColMeterSAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount1 = FCConvert.ToDouble(strText);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount1 = 0;
								}
							}
							else if (col == lngColMeterSRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(false, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].SewerType1))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerKey1 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerKey1 = 0;
								}
							}
							else if (col == lngColMeterSAccount)
							{
								if (Strings.Trim(strText) != "" && strText.Replace("-", "").Replace("_", "").Length >= 3)
								{
									if (!modMain.CheckDefaultFundForAccount_2(false, strText))
									{
										MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										vsMeter.EditText = "";
										e.Cancel = true;
									}
								}
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount1 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount1 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAccount1 = "";
								}
							}
							else if (col == lngColMeterSHidden)
							{
								// this is the type of sewer transaction
								// nothing
							}
						}
						else if (row == lngRowMeterPayment2)
						{
							if (col == lngColMeterCheck)
							{
								SetMeterRow(row);
							}
							else if (col == lngColMeterWTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].WaterKey2, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterWType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].WaterType2 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterType2 = -1;
								}
							}
							else if (col == lngColMeterWAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount2 = FCUtils.Round(FCConvert.ToDouble(strText), 2);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount2 = 0;
								}
							}
							else if (col == lngColMeterWRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(true, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].WaterType2))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterKey2 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterKey2 = 0;
								}
							}
							else if (col == lngColMeterWAccount)
							{
								// Cancel = CheckAccountValidate(vsMeter, Row, Col, Cancel)
								// If Not Cancel Then
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount2 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount2 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAccount2 = "";
								}
							}
							else if (col == lngColMeterWHidden)
							{
								// nothing
							}
							else if (col == lngColMeterSTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].SewerKey2, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterSType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].SewerType2 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerType2 = -1;
								}
							}
							else if (col == lngColMeterSAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount2 = FCConvert.ToDouble(strText);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount2 = 0;
								}
							}
							else if (col == lngColMeterSRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(false, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].SewerType2))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerKey2 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerKey2 = 0;
								}
							}
							else if (col == lngColMeterSAccount)
							{
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount2 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount2 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAccount2 = "";
								}
							}
							else if (col == lngColMeterSHidden)
							{
								// this is the type of sewer transaction
								// nothing
							}
						}
						else if (row == lngRowMeterPayment3)
						{
							if (col == lngColMeterCheck)
							{
								SetMeterRow(row);
							}
							else if (col == lngColMeterWTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].WaterKey3, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterWType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].WaterType3 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterType3 = -1;
								}
							}
							else if (col == lngColMeterWAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount3 = FCUtils.Round(FCConvert.ToDouble(strText), 2);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount3 = 0;
								}
							}
							else if (col == lngColMeterWRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(true, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].WaterType3))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterKey3 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterKey3 = 0;
								}
							}
							else if (col == lngColMeterWAccount)
							{
								// Cancel = CheckAccountValidate(vsMeter, Row, Col, Cancel)
								// If Not Cancel Then
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount3 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount3 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAccount3 = "";
								}
							}
							else if (col == lngColMeterWHidden)
							{
								// nothing
							}
							else if (col == lngColMeterSTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].SewerKey3, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterSType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].SewerType3 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerType3 = -1;
								}
							}
							else if (col == lngColMeterSAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount3 = FCConvert.ToDouble(strText);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount3 = 0;
								}
							}
							else if (col == lngColMeterSRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(false, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].SewerType3))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerKey3 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerKey3 = 0;
								}
							}
							else if (col == lngColMeterSAccount)
							{
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount3 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount3 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAccount3 = "";
								}
							}
							else if (col == lngColMeterSHidden)
							{
								// this is the type of sewer transaction
								// nothing
							}
						}
						else if (row == lngRowMeterPayment4)
						{
							if (col == lngColMeterCheck)
							{
								SetMeterRow(row);
							}
							else if (col == lngColMeterWTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].WaterKey4, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterWType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].WaterType4 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterType4 = -1;
								}
							}
							else if (col == lngColMeterWAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount4 = FCUtils.Round(FCConvert.ToDouble(strText), 2);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount4 = 0;
								}
							}
							else if (col == lngColMeterWRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(true, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].WaterType4))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterKey4 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterKey4 = 0;
								}
							}
							else if (col == lngColMeterWAccount)
							{
								// Cancel = CheckAccountValidate(vsMeter, Row, Col, Cancel)
								// If Not Cancel Then
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount4 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount4 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAccount4 = "";
								}
							}
							else if (col == lngColMeterWHidden)
							{
								// nothing
							}
							else if (col == lngColMeterSTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].SewerKey4, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterSType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].SewerType4 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerType4 = -1;
								}
							}
							else if (col == lngColMeterSAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount4 = FCConvert.ToDouble(strText);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount4 = 0;
								}
							}
							else if (col == lngColMeterSRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(false, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].SewerType4))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerKey4 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerKey4 = 0;
								}
							}
							else if (col == lngColMeterSAccount)
							{
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount4 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount4 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAccount4 = "";
								}
							}
							else if (col == lngColMeterSHidden)
							{
								// this is the type of sewer transaction
								// nothing
							}
						}
						else if (row == lngRowMeterPayment5)
						{
							if (col == lngColMeterCheck)
							{
								SetMeterRow(row);
							}
							else if (col == lngColMeterWTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].WaterKey5, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterWType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].WaterType5 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterType5 = -1;
								}
							}
							else if (col == lngColMeterWAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount5 = FCUtils.Round(FCConvert.ToDouble(strText), 2);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAmount5 = 0;
								}
							}
							else if (col == lngColMeterWRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(true, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].WaterType5))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterKey5 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterKey5 = 0;
								}
							}
							else if (col == lngColMeterWAccount)
							{
								// Cancel = CheckAccountValidate(vsMeter, Row, Col, Cancel)
								// If Not Cancel Then
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount5 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAccount5 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAccount5 = "";
								}
							}
							else if (col == lngColMeterWHidden)
							{
								// nothing
							}
							else if (col == lngColMeterSTitle)
							{
								if (FCConvert.ToString(vsMeter.ComboData(vsMeter.ComboIndex)) != "")
								{
									if (CheckForMultipleRT_2(false, row, MeterKeyArray[lngCurrentMeterNumber].SewerKey5, FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex))))
									{
										vsMeter.TextMatrix(row, lngColMeterSType, vsMeter.ComboData(vsMeter.ComboIndex));
										MeterKeyArray[lngCurrentMeterNumber].SewerType5 = FCConvert.ToInt32(vsMeter.ComboData(vsMeter.ComboIndex));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerType5 = -1;
								}
							}
							else if (col == lngColMeterSAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount5 = FCConvert.ToDouble(strText);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAmount5 = 0;
								}
							}
							else if (col == lngColMeterSRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									if (CheckForMultipleRT_20(false, row, FCConvert.ToInt32(Conversion.Val(Strings.Left(strText, 3))), MeterKeyArray[lngCurrentMeterNumber].SewerType5))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerKey5 = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
									}
									else
									{
										MessageBox.Show("Cannot have the same rate table and type for the same meter.", "Duplicate Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										e.Cancel = true;
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerKey5 = 0;
								}
							}
							else if (col == lngColMeterSAccount)
							{
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount5 = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAccount5 = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAccount5 = "";
								}
							}
							else if (col == lngColMeterSHidden)
							{
								// this is the type of sewer transaction
								// nothing
							}
						}
						else if (row == lngRowMeterAdjust)
						{
							if (col == lngColMeterCheck)
							{
								SetMeterRow(row);
								if (Conversion.Val(vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterCheck)) != -1)
								{
									// this seems backwards but it works
									// vsMeter.TextMatrix(lngRowMeterAdjustDescription, lngColMeterCheck) = -1
									if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W")
									{
										txtAdjustDescriptionW.Enabled = true;
										txtAdjustDescriptionS.Enabled = false;
									}
									else if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "S")
									{
										txtAdjustDescriptionW.Enabled = false;
										txtAdjustDescriptionS.Enabled = true;
									}
									else
									{
										txtAdjustDescriptionW.Enabled = true;
										txtAdjustDescriptionS.Enabled = true;
									}
								}
								else
								{
									// vsMeter.TextMatrix(lngRowMeterAdjustDescription, lngColMeterCheck) = ""
									txtAdjustDescriptionW.Enabled = false;
									txtAdjustDescriptionS.Enabled = false;
								}
								// SetMeterRow lngRowMeterAdjustDescription, True
							}
							else if (col == lngColMeterWTitle)
							{
								vsMeter.TextMatrix(row, lngColMeterWType, vsMeter.ComboData(lngColMeterWTitle));
							}
							else if (col == lngColMeterWAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAdjustAmount = FCUtils.Round(FCConvert.ToDouble(strText), 2);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAdjustAmount = 0;
								}
							}
							else if (col == lngColMeterWRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAdjustKey = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAdjustKey = 0;
								}
							}
							else if (col == lngColMeterWAccount)
							{
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAdjustAccount = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].WaterAdjustAccount = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].WaterAdjustAccount = "";
								}
							}
							else if (col == lngColMeterWHidden)
							{
							}
							else if (col == lngColMeterWTitle)
							{
								// do nothing combo
							}
							else if (col == lngColMeterSAmount)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAdjustAmount = FCUtils.Round(FCConvert.ToDouble(strText), 2);
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAdjustAmount = 0;
								}
							}
							else if (col == lngColMeterSRateTable)
							{
								if (Strings.Trim(strText) != "")
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAdjustKey = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strText, 3))));
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAdjustKey = 0;
								}
							}
							else if (col == lngColMeterSAccount)
							{
								if (strText.Replace("-", "").Replace("_", "").Length >= 3 && e.Cancel == false)
								{
									if (modValidateAccount.AccountValidate(strText))
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAdjustAccount = strText;
									}
									else
									{
										MeterKeyArray[lngCurrentMeterNumber].SewerAdjustAccount = "";
									}
								}
								else
								{
									MeterKeyArray[lngCurrentMeterNumber].SewerAdjustAccount = "";
								}
							}
							else if (col == lngColMeterSHidden)
							{
							}
							
						}
					}
				}
				if (!e.Cancel)
				{
					boolDirty = true;
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				e.Cancel = true;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Meter Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsPercentage_Enter(object sender, System.EventArgs e)
		{
			vsPercentage.Select(1, 1);
		}

		private void vsPercentage_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii >= 48 && KeyAscii <= 57))
			{
				// do nothing
			}
			else if (KeyAscii == 46)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, vsPercentage.EditText, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (vsPercentage.EditSelStart < lngDecPlace && vsPercentage.EditSelLength + vsPercentage.EditSelStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = 0;
					}
				}
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsPercentage_Leave(object sender, System.EventArgs e)
		{
			// vsPercentage.Select 1, 1
		}

		private void vsPercentage_RowColChange(object sender, System.EventArgs e)
		{
			if (vsPercentage.Row > 0)
			{
				vsPercentage.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsPercentage.EditCell();
			}
			else
			{
				vsPercentage.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (vsPercentage.Row == vsPercentage.Rows - 1 && vsPercentage.Col == vsPercentage.Cols - 1)
			{
				// if this is the last cell in the grid then change the tab behavior
				vsPercentage.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsPercentage.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsPercentage_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			string strText = "";
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsPercentage.GetFlexRowIndex(e.RowIndex);
			int col = vsPercentage.GetFlexColIndex(e.ColumnIndex);
			if (!boolNoSaveMeter)
			{
				strText = vsPercentage.EditText;
				// get the information
				if (Conversion.Val(strText) >= 0 && Conversion.Val(strText) <= 200)
				{
					switch (col)
					{
						case 1:
							{
								// Water
								switch (row)
								{
									case 1:
										{
											// % Consumption
											MeterKeyArray[lngCurrentMeterNumber].WaterPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(strText)));
											vsPercentage.EditText = FCConvert.ToString(Conversion.Val(strText));
											break;
										}
									case 2:
										{
											// Taxable Percent
											MeterKeyArray[lngCurrentMeterNumber].WaterTaxPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(strText)));
											vsPercentage.EditText = FCConvert.ToString(Conversion.Val(strText));
											break;
										}
								}
								//end switch
								break;
							}
						case 2:
							{
								// Sewer
								switch (row)
								{
									case 1:
										{
											// % Consumption
											MeterKeyArray[lngCurrentMeterNumber].SewerPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(strText)));
											vsPercentage.EditText = FCConvert.ToString(Conversion.Val(strText));
											break;
										}
									case 2:
										{
											// Taxable Percent
											MeterKeyArray[lngCurrentMeterNumber].SewerTaxPercent = FCConvert.ToInt32(Math.Round(Conversion.Val(strText)));
											vsPercentage.EditText = FCConvert.ToString(Conversion.Val(strText));
											break;
										}
								}
								//end switch
								break;
							}
					}
					//end switch
				}
				else
				{
					e.Cancel = true;
					MessageBox.Show("Please enter a value less than 200% and greater than 0.", "Invalid data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			if (!e.Cancel)
			{
				boolDirty = true;
			}
		}

		private void vsReading_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsReading.ComboList = "";
			vsReading.EditMask = "";
			//FC:FINAL:MSH - issue #911: saving and using correct indexes of the cell
			int row = vsReading.GetFlexRowIndex(e.RowIndex);
			int col = vsReading.GetFlexColIndex(e.ColumnIndex);
			switch (col)
			{
				case 1:
					{
						if (row == 1)
						{
							vsReading.ComboList = "#1;Yes|#2;No";
						}
						break;
					}
				case 2:
					{
						// date field
						if (row < 6)
						{
                            // this will format the cell to use a data format
                            //FC:FINAL:CHN: Set correct date mask.
                            // vsReading.EditMask = "##/##/####";
                            vsReading.EditMask = "00/00/0000";
                            vsReading.EditMaxLength = 10;
						}
						else
						{
							vsReading.EditMask = "";
							vsReading.EditMaxLength = 30;
						}
						break;
					}
				default:
					{
						if (row < 6)
						{
							vsReading.EditMaxLength = 10;
						}
						else
						{
							vsReading.EditMaxLength = 30;
						}
						vsReading.EditMask = "";
						break;
					}
			}
			//end switch
		}

		private void vsReading_KeyDownEdit(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsReading.Col == 2)
			{
				switch (FCConvert.ToInt32(e.KeyCode))
				{
					case 46:
						{
							// delete key
							vsReading.TextMatrix(vsReading.Row, vsReading.Col, "");
							vsReading.EditText = "";
							break;
						}
				}
				//end switch
			}
			else if (vsReading.Col == 1 && vsReading.Row == 6)
			{
				KeyCode = 0;
			}
		}

		private void vsReading_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			switch (vsReading.Col)
			{
				case 1:
					{
						if (vsReading.Row < 6)
						{
							if ((KeyAscii == 8) || (KeyAscii >= 48 && KeyAscii <= 57))
							{
								// do nothing
							}
							else
							{
								KeyAscii = 0;
							}
						}
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsReading_Leave(object sender, System.EventArgs e)
		{
			vsReading.Select(1, 1);
		}

		private void vsReading_RowColChange(object sender, System.EventArgs e)
		{
			if (vsReading.Col > 0)
			{
				if (vsReading.Row > 0)
				{
					vsReading.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
				else
				{
					vsReading.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			if (vsReading.Col == 1 && vsReading.Row == 4)
			{
				// First col in set date
				vsReading.Editable = FCGrid.EditableSettings.flexEDNone;
				return;
			}
			if (vsReading.Row == 6)
			{
				vsReading.EditMaxLength = 30;
			}
			else
			{
				vsReading.EditMaxLength = 10;
			}
			if (vsReading.Row == vsReading.Rows - 1 && vsReading.Col == vsReading.Cols - 1)
			{
				// if this is the last cell in the grid then change the tab behavior
				vsReading.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsReading.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsReading_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int row = vsReading.GetFlexRowIndex(e.RowIndex);
			int col = vsReading.GetFlexColIndex(e.ColumnIndex);
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strText = "";
				DateTime dtDate;
				clsDRWrapper rsRD = new clsDRWrapper();
				if (!boolNoSaveMeter)
				{
					strText = vsReading.EditText;
					switch (col)
					{
						case 1:
							{
								switch (row)
								{
									case 1:
										{
											// Backflow Reading
											MeterKeyArray[lngCurrentMeterNumber].BackFlow = FCConvert.CBool(Strings.Left(strText, 1) == "Y");
											break;
										}
									case 2:
										{
											// Previous Reading
											MeterKeyArray[lngCurrentMeterNumber].PreviousReading = FCConvert.ToInt32(Math.Round(Conversion.Val(strText)));
											break;
										}
									case 3:
										{
											// Current Reading
											// kk 110812 trout-884  Change so -1 = No Read
											if (strText == "")
											{
												MeterKeyArray[lngCurrentMeterNumber].CurrentReading = -1;
											}
											else
											{
												MeterKeyArray[lngCurrentMeterNumber].CurrentReading = FCConvert.ToInt32(Math.Round(Conversion.Val(strText)));
											}
											break;
										}
									case 4:
										{
											// nothing
											break;
										}
									case 5:
										{
											// Replacement Reading
											if (MeterKeyArray[lngCurrentMeterNumber].ReplacementConsumption != Conversion.Val(strText))
											{
												rsRD.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(MeterKeyArray[lngCurrentMeterNumber].Key), modExtraModules.strUTDatabase);
												if (!rsRD.EndOfFile())
												{
													// DJW@20090130 Allow them to affect billign by setting replacement units in meter screen
													if (FCConvert.ToBoolean(rsRD.Get_Fields_Boolean("UseMeterChangeOut")))
													{
														if (MessageBox.Show("Would you like to affect the Meter Change Out consumption for the next billing?", "Affect Change Out Consumption", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
														{
															modGlobalFunctions.AddCYAEntry_80("UT", "Affecting Change Out Consumption", "Old : " + rsRD.Get_Fields_Int32("ConsumptionOfChangedOutMeter"), "New : " + FCConvert.ToString(Conversion.Val(strText)));
															rsRD.Edit();
															rsRD.Set_Fields("ConsumptionOfChangedOutMeter", FCConvert.ToString(Conversion.Val(strText)));
															rsRD.Set_Fields("UseMeterChangeOut", true);
															rsRD.Update();
														}
													}
													else
													{
														MessageBox.Show("This amount will not affect your next billing.  In order to do so you must run through the Meter Change Out Process.", "Bill will NOT be affected.", MessageBoxButtons.OK, MessageBoxIcon.Information);
													}
												}
											}
											MeterKeyArray[lngCurrentMeterNumber].ReplacementConsumption = FCConvert.ToInt32(Math.Round(Conversion.Val(strText)));
											break;
										}
									case 6:
										{
											vsReading.TextMatrix(6, 2, strText);
											vsReading.TextMatrix(6, 1, strText);
											MeterKeyArray[lngCurrentMeterNumber].Location = strText;
											break;
										}
								}
								//end switch
								break;
							}
						case 2:
							{
								if (Strings.Trim(strText) != "")
								{
									if (Information.IsDate(strText))
									{
										switch (row)
										{
											case 1:
												{
													// Backflow Date
													MeterKeyArray[lngCurrentMeterNumber].BackFlowDate = DateAndTime.DateValue(strText);
													break;
												}
											case 2:
												{
													// Previous Reading Date
													MeterKeyArray[lngCurrentMeterNumber].PreviousReadingDate = DateAndTime.DateValue(strText);
													break;
												}
											case 3:
												{
													// Current Reading Date
													MeterKeyArray[lngCurrentMeterNumber].CurrentReadingDate = DateAndTime.DateValue(strText);
													break;
												}
											case 4:
												{
													// Set Date
													MeterKeyArray[lngCurrentMeterNumber].SetDate = DateAndTime.DateValue(strText);
													break;
												}
											case 5:
												{
													// Replacement Date
													MeterKeyArray[lngCurrentMeterNumber].ReplacementDate = DateAndTime.DateValue(strText);
													break;
												}
										}
										//end switch
									}
									else
									{
										switch (row)
										{
											case 6:
												{
													vsReading.TextMatrix(6, 2, strText);
													vsReading.TextMatrix(6, 1, strText);
													MeterKeyArray[lngCurrentMeterNumber].Location = strText;
													break;
												}
											default:
												{
													vsReading.EditText = "";
													vsReading.TextMatrix(row, col, "");
													break;
												}
										}
										//end switch
									}
								}
								else
								{
								}
								break;
							}
					}
					//end switch
				}
				if (!e.Cancel)
				{
					boolDirty = true;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Reading Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool AddMeterToThisAccount()
		{
			bool AddMeterToThisAccount = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMeter = new clsDRWrapper();
				short intCT;
				// counter variable
				int intIndCT;
				// counter variable for the combobox
				bool boolFound;
				short intMeterNumber = 0;
				// this is the actual meter number
				int intIndex;
				// this is the index in the array
				clsDRWrapper rsDef = new clsDRWrapper();
				clsDRWrapper rsCat = new clsDRWrapper();
				int lngWTaxPercent = 0;
				int lngSTaxPercent = 0;
				if (!vsMeter.IsCurrentCellInEditMode)
				{
					vsMeter.Select(0, 0);
					// kk11052014 trouts-116  Force changes in the Rate grid to validate/save
				}
				// find the first free slot in the meter array
				boolFound = false;
				for (intCT = 1; intCT <= 99; intCT++)
				{
					if (MeterKeyArray[intCT].Used == false)
					{
						boolFound = true;
						ClearArrayValue(ref intCT);
						break;
					}
				}
				if (boolFound)
				{
					intIndex = intCT;
					// save the index for the array
					// find the fisrt non-used meter number
					for (intCT = 1; intCT <= 99; intCT++)
					{
						// start at one and try all the numbers to 99
						boolFound = false;
						for (intIndCT = 1; intIndCT <= cmbMeterNumber.Items.Count; intIndCT++)
						{
							if (MeterKeyArray[intIndCT].MeterNumber == intCT)
							{
								boolFound = true;
								break;
							}
						}
						if (boolFound)
						{
							// keep going
						}
						else
						{
							// this is the one
							intMeterNumber = intCT;
							break;
						}
					}
					if (modUTStatusPayments.Statics.TownService == "S" || modUTStatusPayments.Statics.TownService == "B")
					{
						if (Conversion.Val(cmbSCat.Text) != 0)
						{
							rsCat.OpenRecordset("SELECT * FROM Category WHERE Code = " + FCConvert.ToString(Conversion.Val(Strings.Left(cmbSCat.Text, 2))), modExtraModules.strUTDatabase);
							if (!rsCat.EndOfFile())
							{
								if (FCConvert.ToBoolean(rsCat.Get_Fields_Boolean("STaxable")))
								{
									lngSTaxPercent = 100;
								}
								else
								{
									lngSTaxPercent = 0;
								}
							}
							else
							{
								lngSTaxPercent = 0;
							}
						}
						else
						{
							lngSTaxPercent = 0;
						}
					}
					else
					{
						lngSTaxPercent = 0;
					}
					if (modUTStatusPayments.Statics.TownService == "W" || modUTStatusPayments.Statics.TownService == "B")
					{
						if (Conversion.Val(cmbWCat.Text) != 0)
						{
							rsCat.OpenRecordset("SELECT * FROM Category WHERE Code = " + FCConvert.ToString(Conversion.Val(Strings.Left(cmbWCat.Text, 2))), modExtraModules.strUTDatabase);
							if (!rsCat.EndOfFile())
							{
								if (FCConvert.ToBoolean(rsCat.Get_Fields_Boolean("WTaxable")))
								{
									lngWTaxPercent = 100;
								}
								else
								{
									lngWTaxPercent = 0;
								}
							}
							else
							{
								lngWTaxPercent = 0;
							}
						}
						else
						{
							lngWTaxPercent = 0;
						}
					}
					else
					{
						lngWTaxPercent = 0;
					}
					rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE ID = 0");
					rsMeter.AddNew();
					rsMeter.Set_Fields("AccountKey", lngKey);
					rsMeter.Set_Fields("MeterNumber", intMeterNumber);
					rsMeter.Set_Fields("BillingStatus", "C");
					rsMeter.Set_Fields("WaterTaxPercent", lngWTaxPercent);
					rsMeter.Set_Fields("SewerTaxPercent", lngSTaxPercent);
					rsMeter.Set_Fields("WaterPercent", 100);
					rsMeter.Set_Fields("SewerPercent", 100);
					rsMeter.Set_Fields("Multiplier", 1);
					// kk01232015 trouts-132  Set default multiplier in case user exits without saving
					rsDef.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
					if (!rsDef.EndOfFile())
					{
						rsMeter.Set_Fields("Size", FCConvert.ToString(Conversion.Val(rsDef.Get_Fields_Int32("MeterDefaultSize"))));
						rsMeter.Set_Fields("Digits", FCConvert.ToString(Conversion.Val(rsDef.Get_Fields_Int32("MeterDefaultDigits"))));
						rsMeter.Set_Fields("Frequency", FCConvert.ToString(Conversion.Val(rsDef.Get_Fields_Int32("MeterDefaultFrequency"))));
						MeterKeyArray[intIndex].Size = rsMeter.Get_Fields_Int32("Size");
						// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
						MeterKeyArray[intIndex].Frequency = rsMeter.Get_Fields_Int32("Frequency");
						MeterKeyArray[intIndex].Digits = rsMeter.Get_Fields_Int32("Digits");
						MeterKeyArray[intIndex].WaterTaxPercent = lngWTaxPercent;
						MeterKeyArray[intIndex].SewerTaxPercent = lngSTaxPercent;
					}
					if (modUTStatusPayments.Statics.TownService == "B")
					{
						MeterKeyArray[intIndex].Service = "";
					}
					else if (modUTStatusPayments.Statics.TownService == "S")
					{
						MeterKeyArray[intIndex].Service = "S";
					}
					else if (modUTStatusPayments.Statics.TownService == "W")
					{
						MeterKeyArray[intIndex].Service = "W";
					}
					MeterKeyArray[intIndex].BillingStatus = "C";
					// MeterKeyArray(intIndex).Key = rsMeter.Fields("MeterKey")
					rsMeter.Update();
					MeterKeyArray[intIndex].Key = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("ID"));
					// kk 06132013   rsMeter.Fields("MeterKey")
					MeterKeyArray[intIndex].Used = true;
					MeterKeyArray[intIndex].MeterNumber = intMeterNumber;
					AddMeterToThisAccount = true;
					// reload the combobox on the meter screen
					LoadMeterCombo(intMeterNumber);
				}
				else
				{
					AddMeterToThisAccount = false;
					MessageBox.Show("This account already has the maximum amount of meters allowed.", "Maximum Meters", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return AddMeterToThisAccount;
			}
			catch (Exception ex)
			{
				
				AddMeterToThisAccount = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Meter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddMeterToThisAccount;
		}
		// vbPorter upgrade warning: intMeterNumber As short	OnWriteFCConvert.ToInt32(
		private void LoadMeterInformation_2(short intMeterNumber)
		{
			LoadMeterInformation(ref intMeterNumber);
		}

		private void LoadMeterInformation(ref short intMeterNumber)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				// Dim rsF                         As New clsDRWrapper
				boolNoSaveMeter = true;
				// this will fill the meter information into the meter screen
				lngCurrentMeterNumber = intMeterNumber;
				modMain.Statics.lngCurrentMeterKey = MeterKeyArray[intMeterNumber].Key;
				// rsF.OpenRecordset "SELECT FinalEndDate, FinalStartDate, FinalBillDate, Final FROM MeterTable WHERE MeterKey = " & .Key, strUTDatabase
				// If Not rsF.EndOfFile Then
				// lblFinalBill.Visible = rsF.Fields("Final")
				// End If
				LockMeterFields_2(false);
				// load this meter's information
				// booknumber
				cmbBook.SelectedIndex = -1;
				if (cmbBook.Items.Count > 0)
				{
					for (intCT = 0; intCT <= cmbBook.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(Strings.Left(cmbBook.Items[intCT].ToString(), 5)) == MeterKeyArray[intMeterNumber].BookNumber)
						{
							cmbBook.SelectedIndex = intCT;
							break;
						}
					}
				}
				// txtBook.Text = .BookNumber
				// sequence
				txtSequence.Text = FCConvert.ToString(MeterKeyArray[intMeterNumber].Sequence);
				// serial number
				txtSerialNumber.Text = MeterKeyArray[intMeterNumber].SerialNumber;
				// Cross Reference fields
				txtXRef1.Text = MeterKeyArray[intMeterNumber].XRef1;
				txtXRef2.Text = MeterKeyArray[intMeterNumber].XRef2;
				txtLong.Text = MeterKeyArray[intMeterNumber].Longitude;
				txtLat.Text = MeterKeyArray[intMeterNumber].Latitude;
				// include in extract
				if (MeterKeyArray[intMeterNumber].IncludeInExtract)
				{
					chkIncludeInExtract.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkIncludeInExtract.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// radio access
				// If .RadioAccesst  Then
				// chkRadio.Value = vbChecked
				// Else
				// chkRadio.Value = vbUnchecked
				// End If
				cmbRadio.SelectedIndex = MeterKeyArray[intMeterNumber].RadioAccessType;
				// meter size
				cmbMeterSize.SelectedIndex = -1;
				if (cmbMeterSize.Items.Count > 0)
				{
					for (intCT = 0; intCT <= cmbMeterSize.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(Strings.Left(cmbMeterSize.Items[intCT].ToString(), 2)) == MeterKeyArray[intMeterNumber].Size)
						{
							cmbMeterSize.SelectedIndex = intCT;
							break;
						}
					}
				}
				if (MeterKeyArray[intMeterNumber].Service == "W" || MeterKeyArray[intMeterNumber].Service == "B")
				{
					boolMeterWaterService = true;
				}
				else
				{
					boolMeterWaterService = true;
				}
				if (MeterKeyArray[intMeterNumber].Service == "S" || MeterKeyArray[intMeterNumber].Service == "B")
				{
					boolMeterSewerService = true;
				}
				else
				{
					boolMeterSewerService = true;
				}
				cmbService.SelectedIndex = -1;
				if (cmbService.Items.Count > 0)
				{
					for (intCT = 0; intCT <= cmbService.Items.Count - 1; intCT++)
					{
						if (Strings.Left(cmbService.Items[intCT].ToString(), 1) == Strings.Left(MeterKeyArray[intMeterNumber].Service, 1))
						{
							cmbService.SelectedIndex = intCT;
							break;
						}
					}
				}
				cmbFrequency.SelectedIndex = -1;
				if (cmbFrequency.Items.Count > 0)
				{
					for (intCT = 0; intCT <= cmbFrequency.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbFrequency.Items[intCT].ToString()) == MeterKeyArray[intMeterNumber].Frequency)
						{
							cmbFrequency.SelectedIndex = intCT;
							break;
						}
					}
				}
				cmbMeterDigits.SelectedIndex = -1;
				if (cmbMeterDigits.Items.Count > 0)
				{
					for (intCT = 0; intCT <= cmbMeterDigits.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbMeterDigits.Items[intCT].ToString()) == MeterKeyArray[intMeterNumber].Digits)
						{
							cmbMeterDigits.SelectedIndex = intCT;
							break;
						}
					}
				}
				cmbMultiplier.SelectedIndex = -1;
				if (cmbMultiplier.Items.Count > 0)
				{
					for (intCT = 0; intCT <= cmbMultiplier.Items.Count - 1; intCT++)
					{
						if (Conversion.Val(cmbMultiplier.Items[intCT].ToString()) == MeterKeyArray[intMeterNumber].Multiplier)
						{
							cmbMultiplier.SelectedIndex = intCT;
							break;
						}
					}
				}
				if (MeterKeyArray[intMeterNumber].MeterNumber == 1)
				{
					// if this is the first meter in this account then do not allow any combination to occur
					cmbCombineMeter.Enabled = false;
					cmbCombineMeter.SelectedIndex = -1;
				}
				else
				{
					cmbCombineMeter.Enabled = true;
					cmbCombineMeter.SelectedIndex = -1;
					if (cmbCombineMeter.Items.Count > 0)
					{
						for (intCT = 0; intCT <= cmbCombineMeter.Items.Count - 1; intCT++)
						{
							if (Strings.Left(cmbCombineMeter.Items[intCT].ToString(), 1) == Strings.Left(MeterKeyArray[intMeterNumber].Combine, 1))
							{
								cmbCombineMeter.SelectedIndex = intCT;
								break;
							}
						}
					}
				}
				vsReading.TextMatrix(5, 1, FCConvert.ToString(MeterKeyArray[intMeterNumber].ReplacementConsumption));
				if (MeterKeyArray[intMeterNumber].ReplacementDate.ToOADate() == 0)
				{
					vsReading.TextMatrix(5, 2, "");
				}
				else
				{
					//FC:FINAL:MSH - issue #911: set to cell only date in short format
					//vsReading.TextMatrix(5, 2, FCConvert.ToString(MeterKeyArray[intMeterNumber].ReplacementDate));
					vsReading.TextMatrix(5, 2, Strings.Format(MeterKeyArray[intMeterNumber].ReplacementDate, "MM/dd/yyyy"));
				}
				// vsReading.TextMatrix(6, 1) = .Location
				// vsReading.TextMatrix(6, 2) = vsReading.TextMatrix(6, 1)
				txtLocation.Text = MeterKeyArray[intMeterNumber].Location;
				// check the correct rows
				if (MeterKeyArray[intMeterNumber].UseRate1)
				{
					vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterCheck, FCConvert.ToString(-1));
					SetMeterRow_6(lngRowMeterPayment1, true);
				}
				else
				{
					vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterCheck, FCConvert.ToString(0));
					SetMeterRow_6(lngRowMeterPayment1, true);
				}
				if (MeterKeyArray[intMeterNumber].UseRate2)
				{
					vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterCheck, FCConvert.ToString(-1));
					SetMeterRow_6(lngRowMeterPayment2, true);
				}
				else
				{
					vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterCheck, FCConvert.ToString(0));
					SetMeterRow_6(lngRowMeterPayment2, true);
				}
				if (MeterKeyArray[intMeterNumber].UseRate3)
				{
					vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterCheck, FCConvert.ToString(-1));
					SetMeterRow_6(lngRowMeterPayment3, true);
				}
				else
				{
					vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterCheck, FCConvert.ToString(0));
					SetMeterRow_6(lngRowMeterPayment3, true);
				}
				if (MeterKeyArray[intMeterNumber].UseRate4)
				{
					vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterCheck, FCConvert.ToString(-1));
					SetMeterRow_6(lngRowMeterPayment4, true);
				}
				else
				{
					vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterCheck, FCConvert.ToString(0));
					SetMeterRow_6(lngRowMeterPayment4, true);
				}
				if (MeterKeyArray[intMeterNumber].UseRate5)
				{
					vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterCheck, FCConvert.ToString(-1));
					SetMeterRow_6(lngRowMeterPayment5, true);
				}
				else
				{
					vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterCheck, FCConvert.ToString(0));
					SetMeterRow_6(lngRowMeterPayment5, true);
				}
				if (MeterKeyArray[intMeterNumber].UseAdjustRate)
				{
					vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterCheck, FCConvert.ToString(-1));
					SetMeterRow_6(lngRowMeterAdjust, true);
					if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "W")
					{
						txtAdjustDescriptionW.Enabled = true;
						txtAdjustDescriptionS.Enabled = false;
					}
					else if (Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString(), 1) == "S")
					{
						txtAdjustDescriptionW.Enabled = false;
						txtAdjustDescriptionS.Enabled = true;
					}
					else
					{
						txtAdjustDescriptionW.Enabled = true;
						txtAdjustDescriptionS.Enabled = true;
					}
					// SetMeterRow lngRowMeterAdjustDescription, True
				}
				else
				{
					vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterCheck, FCConvert.ToString(0));
					SetMeterRow_6(lngRowMeterAdjust, true);
					txtAdjustDescriptionS.Enabled = false;
					txtAdjustDescriptionW.Enabled = false;
					// SetMeterRow lngRowMeterAdjustDescription, True
				}
				// from meter table
				// vsMeter.TextMatrix(lngRowMeterAdjustDescription, lngColMeterWAmount) = .AdjustDescription
				txtAdjustDescriptionW.Text = MeterKeyArray[intMeterNumber].WaterAdjustDescription;
				// water fields
				vsPercentage.TextMatrix(1, 1, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterPercent));
				vsPercentage.TextMatrix(2, 1, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterTaxPercent));
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterWRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterKey1));
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterWRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterKey2));
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterWRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterKey3));
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterWRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterKey4));
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterWRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterKey5));
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterWAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterAmount1));
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterWAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterAmount2));
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterWAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterAmount3));
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterWAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterAmount4));
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterWAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterAmount5));
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterWAccount, MeterKeyArray[intMeterNumber].WaterAccount1);
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterWAccount, MeterKeyArray[intMeterNumber].WaterAccount2);
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterWAccount, MeterKeyArray[intMeterNumber].WaterAccount3);
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterWAccount, MeterKeyArray[intMeterNumber].WaterAccount4);
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterWAccount, MeterKeyArray[intMeterNumber].WaterAccount5);
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterWType, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterType1));
				// 1-Consumption 2-Flat 3-Unit 4-Negative Consumption
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterWType, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterType2));
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterWType, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterType3));
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterWType, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterType4));
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterWType, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterType5));
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterWTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].WaterType1));
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterWTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].WaterType2));
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterWTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].WaterType3));
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterWTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].WaterType4));
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterWTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].WaterType5));
				vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterWAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterAdjustAmount));
				vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterWRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].WaterAdjustKey));
				vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterWAccount, MeterKeyArray[intMeterNumber].WaterAdjustAccount);
				// vsMeter.TextMatrix(lngRowMeterAdjustDescription, lngColMeterWAmount) = .WaterAdjustDescription
				txtAdjustDescriptionW.Text = MeterKeyArray[intMeterNumber].WaterAdjustDescription;
				// = .WaterConsumption
				// = .WaterConsumptionOverride
				// Sewer fields
				vsPercentage.TextMatrix(1, 2, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerPercent));
				vsPercentage.TextMatrix(2, 2, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerTaxPercent));
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterSRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerKey1));
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterSRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerKey2));
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterSRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerKey3));
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterSRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerKey4));
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterSRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerKey5));
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterSAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerAmount1));
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterSAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerAmount2));
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterSAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerAmount3));
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterSAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerAmount4));
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterSAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerAmount5));
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterSAccount, MeterKeyArray[intMeterNumber].SewerAccount1);
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterSAccount, MeterKeyArray[intMeterNumber].SewerAccount2);
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterSAccount, MeterKeyArray[intMeterNumber].SewerAccount3);
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterSAccount, MeterKeyArray[intMeterNumber].SewerAccount4);
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterSAccount, MeterKeyArray[intMeterNumber].SewerAccount5);
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterSType, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerType1));
				// 1-Consumption 2-Flat 3-Unit 4-Negative Consumption
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterSType, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerType2));
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterSType, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerType3));
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterSType, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerType4));
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterSType, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerType5));
				// show the titles text
				vsMeter.TextMatrix(lngRowMeterPayment1, lngColMeterSTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].SewerType1));
				vsMeter.TextMatrix(lngRowMeterPayment2, lngColMeterSTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].SewerType2));
				vsMeter.TextMatrix(lngRowMeterPayment3, lngColMeterSTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].SewerType3));
				vsMeter.TextMatrix(lngRowMeterPayment4, lngColMeterSTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].SewerType4));
				vsMeter.TextMatrix(lngRowMeterPayment5, lngColMeterSTitle, modMain.TypeTitleText(ref MeterKeyArray[intMeterNumber].SewerType5));
				vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterSAmount, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerAdjustAmount));
				vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterSRateTable, FCConvert.ToString(MeterKeyArray[intMeterNumber].SewerAdjustKey));
				vsMeter.TextMatrix(lngRowMeterAdjust, lngColMeterSAccount, MeterKeyArray[intMeterNumber].SewerAdjustAccount);
				// vsMeter.TextMatrix(lngRowMeterAdjustDescription, lngColMeterSAmount) = .SewerAdjustDescription
				txtAdjustDescriptionS.Text = MeterKeyArray[intMeterNumber].SewerAdjustDescription;
				if (MeterKeyArray[intMeterNumber].NegativeConsumption)
				{
					chkNegativeConsumption.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkNegativeConsumption.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// = .SewerConsumption
				// = .SewerConsumptionOverride
				vsReading.TextMatrix(2, 1, FCConvert.ToString(MeterKeyArray[intMeterNumber].PreviousReading));
				// = .PreviousCode
				if (MeterKeyArray[intMeterNumber].PreviousReadingDate.ToOADate() == 0)
				{
					vsReading.TextMatrix(2, 2, "");
				}
				else
				{
					//FC:FINAL:MSH - issue #911: set to cell only date in short format
					//vsReading.TextMatrix(2, 2, FCConvert.ToString(MeterKeyArray[intMeterNumber].PreviousReadingDate));
					vsReading.TextMatrix(2, 2, Strings.Format(MeterKeyArray[intMeterNumber].PreviousReadingDate, "MM/dd/yyyy"));
				}
				// kk 110812 trout-884  Change so -1 = No Read
				if (MeterKeyArray[intMeterNumber].CurrentReading == -1)
				{
					vsReading.TextMatrix(3, 1, "");
				}
				else
				{
					vsReading.TextMatrix(3, 1, FCConvert.ToString(MeterKeyArray[intMeterNumber].CurrentReading));
				}
				// = .CurrentCode
				if (MeterKeyArray[intMeterNumber].CurrentReadingDate.ToOADate() == 0)
				{
					vsReading.TextMatrix(3, 2, "");
				}
				else
				{
					//FC:FINAL:MSH - issue #911: set to cell only date in short format
					//vsReading.TextMatrix(3, 2, FCConvert.ToString(MeterKeyArray[intMeterNumber].CurrentReadingDate));
					vsReading.TextMatrix(3, 2, Strings.Format(MeterKeyArray[intMeterNumber].CurrentReadingDate, "MM/dd/yyyy"));
				}
				// = .DateOfChange
				txtRemoteNumber.Text = MeterKeyArray[intMeterNumber].Remote;
				txtMXU.Text = MeterKeyArray[intMeterNumber].MXUNumber;
				txtReaderCode.Text = MeterKeyArray[intMeterNumber].ReaderCode;
				// kk 01182013 trout-896
				// kk trouts-6 03012013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					vsAccountInfo.TextMatrix(2, 3, "Stormwater: " + Strings.Left(cmbWCat.Text, 2) + "  Sewer: " + Strings.Left(cmbSCat.Text, 2));
				}
				else
				{
					vsAccountInfo.TextMatrix(2, 3, "Water: " + Strings.Left(cmbWCat.Text, 2) + "  Sewer: " + Strings.Left(cmbSCat.Text, 2));
				}
				if (MeterKeyArray[intMeterNumber].BackFlow)
				{
					vsReading.TextMatrix(1, 1, "Yes");
					// true
				}
				else
				{
					vsReading.TextMatrix(1, 1, "No");
				}
				if (MeterKeyArray[intMeterNumber].BackFlowDate.ToOADate() == 0)
				{
					vsReading.TextMatrix(1, 2, "");
				}
				else
				{
					//FC:FINAL:MSH - issue #911: set to cell only date in short format
					//vsReading.TextMatrix(1, 2, FCConvert.ToString(MeterKeyArray[intMeterNumber].BackFlowDate));
					vsReading.TextMatrix(1, 2, Strings.Format(MeterKeyArray[intMeterNumber].BackFlowDate, "MM/dd/yyyy"));
				}
				if (MeterKeyArray[intMeterNumber].SetDate.ToOADate() == 0)
				{
					vsReading.TextMatrix(4, 2, "");
				}
				else
				{
					//FC:FINAL:MSH - issue #911: set to cell only date in short format
					//vsReading.TextMatrix(4, 2, FCConvert.ToString(MeterKeyArray[intMeterNumber].SetDate));
					vsReading.TextMatrix(4, 2, Strings.Format(MeterKeyArray[intMeterNumber].SetDate, "MM/dd/yyyy"));
				}
				// = .BillingCode
				// = .Comment
				if (MeterKeyArray[intMeterNumber].FinalBilled)
				{
					chkMeterFinalBill.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkMeterFinalBill.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				if (MeterKeyArray[intMeterNumber].NoBill)
				{
					chkMeterNoBill.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkMeterNoBill.CheckState = Wisej.Web.CheckState.Unchecked;
				}
				// = .BillingStatus
				// If .RadioAccess Then
				// chkRadio.Value = vbChecked
				// Else
				// chkRadio.Value = vbUnchecked
				// End If
				boolNoSaveMeter = false;
				// MAL@20070831: Change Dirty Flag Back to False on Load Event
				boolDirty = false;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Meter Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool DeleteMeter()
		{
			bool DeleteMeter = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				int intIndex;
				clsDRWrapper rsDelete = new clsDRWrapper();
				// make sure that there is a meter loaded
				//FC:FINAL:MSH - issue #917: incorrect order in comparison throws an exception, because first we try to check Item by -1 index
				//if (Conversion.Val(cmbMeterNumber.Items[cmbMeterNumber.SelectedIndex].ToString()) == 0 || cmbMeterNumber.SelectedIndex == -1)
				if (cmbMeterNumber.SelectedIndex == -1 || Conversion.Val(cmbMeterNumber.Items[cmbMeterNumber.SelectedIndex].ToString()) == 0)
				{
					return DeleteMeter;
				}
				intCT = FCConvert.ToInt32(Math.Round(Conversion.Val(cmbMeterNumber.Items[cmbMeterNumber.SelectedIndex].ToString())));
				// this is the meter number that is shown to the user
				intIndex = cmbMeterNumber.ItemData(cmbMeterNumber.SelectedIndex);
				// this is the index in the array
				switch (MessageBox.Show("Are you sure that you would like to delete meter " + FCConvert.ToString(intCT) + "?", "Delete Meter", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							// do nothing...continue with this function
							break;
						}
					case DialogResult.No:
					case DialogResult.Cancel:
						{
							DeleteMeter = false;
							return DeleteMeter;
						}
				}
				//end switch
				if (MeterKeyArray[intIndex].Key != 0)
				{
					// actually delete the meter from the database
					rsDelete.Execute("DELETE FROM MeterTable WHERE ID= " + FCConvert.ToString(MeterKeyArray[intIndex].Key), modExtraModules.strUTDatabase);
					// kk01232015 trouts-132  Fixed the SQL statement
				}
				// remove the used property from the meter array
				MeterKeyArray[intIndex].Used = false;
				MeterKeyArray[intIndex].Key = 0;
				LoadMeterCombo();
				DeleteMeter = true;
				MessageBox.Show("Deletion successful.", "Delete Meter", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return DeleteMeter;
			}
			catch (Exception ex)
			{
				
				DeleteMeter = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Deleting Meter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return DeleteMeter;
		}

		private void SetMeterRow_6(int lngRW, bool boolService = false)
		{
			SetMeterRow(lngRW, boolService);
		}

		private void SetMeterRow(int lngRW, bool boolService = false)
		{
			// this will turn the meter grid rows on or off
			string strTemp = "";
			bool boolRowUsed = false;
			if (lngRW == lngRowMeterTopHeader)
			{
				// no bill    'this does not affect any of the other row
			}
			else if (lngRW == lngRowMeterHeading)
			{
				// final bill 'this does not affect any of the other row
			}
			else if (lngRW == lngRowMeterPayment1 || lngRW == lngRowMeterPayment2 || lngRW == lngRowMeterPayment3 || lngRW == lngRowMeterPayment4 || lngRW == lngRowMeterPayment5 || lngRW == lngRowMeterAdjust)
			{
				// , lngRowMeterAdjustDescription
				// Flat rate, Units, Consumption, Adjustment
				boolRowUsed = (boolService && Conversion.Val(vsMeter.TextMatrix(lngRW, lngColMeterCheck)) == -1) || (!boolService && Conversion.Val(vsMeter.TextMatrix(lngRW, lngColMeterCheck)) != -1);
				if (boolRowUsed)
				{
					if (lngRW == lngRowMeterPayment1)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate1 = true;
					}
					else if (lngRW == lngRowMeterPayment2)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate2 = true;
					}
					else if (lngRW == lngRowMeterPayment3)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate3 = true;
					}
					else if (lngRW == lngRowMeterPayment4)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate4 = true;
					}
					else if (lngRW == lngRowMeterPayment5)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate5 = true;
					}
					else if (lngRW == lngRowMeterAdjust)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseAdjustRate = true;
						strTemp = Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString() + " ", 1);
						if (strTemp != "W")
						{
							txtAdjustDescriptionS.Visible = true;
						}
						else
						{
							txtAdjustDescriptionS.Visible = false;
						}
						if (strTemp != "S")
						{
							txtAdjustDescriptionW.Visible = true;
						}
						else
						{
							txtAdjustDescriptionW.Visible = false;
						}
					}
					strTemp = Strings.Left(cmbService.Items[cmbService.SelectedIndex].ToString() + " ", 1);
					if (strTemp != "S")
					{
						// water
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWType, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWTitle, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWAccount, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWAmount, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWRateTable, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWHidden, Color.White);
						vsMeter.TextMatrix(lngRW, lngColMeterWHidden, ColorTranslator.ToOle(Color.White).ToString());
					}
					else
					{
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWType, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWTitle, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWAccount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWAmount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWRateTable, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWHidden, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.TextMatrix(lngRW, lngColMeterWHidden, FCConvert.ToString(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT));
					}
					if (strTemp != "W")
					{
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSType, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSTitle, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSAccount, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSAmount, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSRateTable, Color.White);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSHidden, Color.White);
						vsMeter.TextMatrix(lngRW, lngColMeterSHidden, ColorTranslator.ToOle(Color.White).ToString());
					}
					else
					{
						// sewer
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSType, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSTitle, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSAccount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSAmount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSRateTable, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSHidden, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
						vsMeter.TextMatrix(lngRW, lngColMeterSHidden, FCConvert.ToString(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT));
					}
				}
				else
				{
					if (lngRW == lngRowMeterPayment1)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate1 = false;
					}
					else if (lngRW == lngRowMeterPayment2)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate2 = false;
					}
					else if (lngRW == lngRowMeterPayment3)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate3 = false;
					}
					else if (lngRW == lngRowMeterPayment4)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate4 = false;
					}
					else if (lngRW == lngRowMeterPayment5)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseRate5 = false;
					}
					else if (lngRW == lngRowMeterAdjust)
					{
						MeterKeyArray[lngCurrentMeterNumber].UseAdjustRate = false;
						txtAdjustDescriptionW.Visible = false;
						txtAdjustDescriptionS.Visible = false;
					}
					// row not used
					// water
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWTitle, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWAccount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWAmount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWRateTable, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterWHidden, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.TextMatrix(lngRW, lngColMeterWHidden, FCConvert.ToString(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT));
					// sewer
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSTitle, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSAccount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSAmount, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSRateTable, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRW, lngColMeterSHidden, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
					vsMeter.TextMatrix(lngRW, lngColMeterSHidden, FCConvert.ToString(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT));
				}
				// Case lngRowMeterAdjustDescription
				// not used   'this does not affect any of the other row
			}
		}

		private void LockMeterFields_2(bool boolLock)
		{
			LockMeterFields(ref boolLock);
		}

		private void LockMeterFields(ref bool boolLock)
		{
			// this will lock/unlock all the fields
			fraCombos.Enabled = !boolLock;
			fraRates.Enabled = !boolLock;
			fraReading.Enabled = !boolLock;
			fraMiddle.Enabled = !boolLock;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void ClearArrayValue(ref short intIndex)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will make sure that the new meter in this index will have all the correct defaults
				MeterKeyArray[intIndex].AccountKey = 0;
				MeterKeyArray[intIndex].BackFlow = false;
				MeterKeyArray[intIndex].BackFlowDate = DateTime.FromOADate(0);
				MeterKeyArray[intIndex].BillingCode = "";
				MeterKeyArray[intIndex].BillingStatus = "";
				MeterKeyArray[intIndex].BookNumber = 0;
				MeterKeyArray[intIndex].Combine = "";
				MeterKeyArray[intIndex].Comment = "";
				MeterKeyArray[intIndex].CurrentCode = "";
				MeterKeyArray[intIndex].CurrentReading = 0;
				MeterKeyArray[intIndex].CurrentReadingDate = DateTime.FromOADate(0);
				MeterKeyArray[intIndex].DateOfChange = DateTime.FromOADate(0);
				MeterKeyArray[intIndex].Digits = 0;
				MeterKeyArray[intIndex].FinalBilled = false;
				MeterKeyArray[intIndex].Frequency = 0;
				MeterKeyArray[intIndex].Key = 0;
				MeterKeyArray[intIndex].MeterNumber = 0;
				MeterKeyArray[intIndex].Multiplier = 1;
				MeterKeyArray[intIndex].NoBill = false;
				MeterKeyArray[intIndex].PreviousCode = "";
				MeterKeyArray[intIndex].PreviousReading = 0;
				MeterKeyArray[intIndex].PreviousReadingDate = DateTime.FromOADate(0);
				MeterKeyArray[intIndex].Remote = "";
				MeterKeyArray[intIndex].ReplacementConsumption = 0;
				MeterKeyArray[intIndex].ReplacementDate = DateTime.FromOADate(0);
				MeterKeyArray[intIndex].Location = "";
				MeterKeyArray[intIndex].XRef1 = "";
				MeterKeyArray[intIndex].XRef2 = "";
				// MAL@20070904: Corrected so that all 5 account rows are cleared - it was set to clear row 1 5 times
				MeterKeyArray[intIndex].WaterAccount1 = "";
				MeterKeyArray[intIndex].WaterAccount2 = "";
				MeterKeyArray[intIndex].WaterAccount3 = "";
				MeterKeyArray[intIndex].WaterAccount4 = "";
				MeterKeyArray[intIndex].WaterAccount5 = "";
				MeterKeyArray[intIndex].WaterAmount1 = 0;
				MeterKeyArray[intIndex].WaterAmount2 = 0;
				MeterKeyArray[intIndex].WaterAmount3 = 0;
				MeterKeyArray[intIndex].WaterAmount4 = 0;
				MeterKeyArray[intIndex].WaterAmount5 = 0;
				MeterKeyArray[intIndex].WaterKey1 = 0;
				MeterKeyArray[intIndex].WaterKey2 = 0;
				MeterKeyArray[intIndex].WaterKey3 = 0;
				MeterKeyArray[intIndex].WaterKey4 = 0;
				MeterKeyArray[intIndex].WaterKey5 = 0;
				MeterKeyArray[intIndex].WaterType1 = 0;
				// 1-Consumption 2-Flat 3-Unit
				MeterKeyArray[intIndex].WaterType2 = 0;
				MeterKeyArray[intIndex].WaterType3 = 0;
				MeterKeyArray[intIndex].WaterType4 = 0;
				MeterKeyArray[intIndex].WaterType5 = 0;
				MeterKeyArray[intIndex].WaterAdjustAccount = "";
				MeterKeyArray[intIndex].WaterAdjustAmount = 0;
				MeterKeyArray[intIndex].WaterAdjustDescription = "";
				MeterKeyArray[intIndex].WaterAdjustKey = 0;
				MeterKeyArray[intIndex].WaterPercent = 100;
				MeterKeyArray[intIndex].WaterTaxPercent = 100;
				MeterKeyArray[intIndex].WCat = "";
				MeterKeyArray[intIndex].SCat = "";
				MeterKeyArray[intIndex].Sequence = 0;
				MeterKeyArray[intIndex].SerialNumber = "";
				MeterKeyArray[intIndex].Service = "";
				MeterKeyArray[intIndex].SetDate = DateTime.FromOADate(0);
				MeterKeyArray[intIndex].SewerAdjustAccount = "";
				MeterKeyArray[intIndex].SewerAdjustAmount = 0;
				MeterKeyArray[intIndex].SewerAdjustDescription = "";
				MeterKeyArray[intIndex].SewerAdjustKey = 0;
				MeterKeyArray[intIndex].SewerAccount1 = "";
				MeterKeyArray[intIndex].SewerAccount2 = "";
				MeterKeyArray[intIndex].SewerAccount3 = "";
				MeterKeyArray[intIndex].SewerAccount4 = "";
				MeterKeyArray[intIndex].SewerAccount5 = "";
				MeterKeyArray[intIndex].SewerAmount1 = 0;
				MeterKeyArray[intIndex].SewerAmount2 = 0;
				MeterKeyArray[intIndex].SewerAmount3 = 0;
				MeterKeyArray[intIndex].SewerAmount4 = 0;
				MeterKeyArray[intIndex].SewerAmount5 = 0;
				MeterKeyArray[intIndex].SewerKey1 = 0;
				MeterKeyArray[intIndex].SewerKey2 = 0;
				MeterKeyArray[intIndex].SewerKey3 = 0;
				MeterKeyArray[intIndex].SewerKey4 = 0;
				MeterKeyArray[intIndex].SewerKey5 = 0;
				MeterKeyArray[intIndex].SewerType1 = 0;
				MeterKeyArray[intIndex].SewerType2 = 0;
				MeterKeyArray[intIndex].SewerType3 = 0;
				MeterKeyArray[intIndex].SewerType4 = 0;
				MeterKeyArray[intIndex].SewerType5 = 0;
				MeterKeyArray[intIndex].SewerConsumption = 0;
				MeterKeyArray[intIndex].SewerConsumptionOverride = 0;
				MeterKeyArray[intIndex].SewerPercent = 100;
				MeterKeyArray[intIndex].SewerTaxPercent = 100;
				MeterKeyArray[intIndex].Size = 0;
				MeterKeyArray[intIndex].UseRate1 = false;
				MeterKeyArray[intIndex].UseRate2 = false;
				MeterKeyArray[intIndex].UseRate3 = false;
				MeterKeyArray[intIndex].UseRate4 = false;
				MeterKeyArray[intIndex].UseRate5 = false;
				MeterKeyArray[intIndex].UseAdjustRate = false;
				MeterKeyArray[intIndex].NegativeConsumption = false;
				MeterKeyArray[intIndex].Used = false;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Clearing Array Values", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngRT As int	OnWrite(int, double)
		// vbPorter upgrade warning: lngType As int	OnWrite(string, int)
		private bool CheckForMultipleRT_2(bool boolWater, int lngRow, int lngRT, int lngType)
		{
			return CheckForMultipleRT(ref boolWater, ref lngRow, ref lngRT, ref lngType);
		}

		private bool CheckForMultipleRT_20(bool boolWater, int lngRow, int lngRT, int lngType)
		{
			return CheckForMultipleRT(ref boolWater, ref lngRow, ref lngRT, ref lngType);
		}

		private bool CheckForMultipleRT(ref bool boolWater, ref int lngRow, ref int lngRT, ref int lngType)
		{
			bool CheckForMultipleRT = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This function will return true if any other Row for the same service for this meter has the same RT saved
				CheckForMultipleRT = true;
				// This is to adjust for which row gets passed in
				lngRow -= 1;
				if (lngRT > 0)
				{
					if (boolWater)
					{
						if (MeterKeyArray[lngCurrentMeterNumber].WaterKey1 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].WaterType1 == lngType && lngType != -1) && lngRow != 1)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
						if (MeterKeyArray[lngCurrentMeterNumber].WaterKey2 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].WaterType2 == lngType && lngType != -1) && lngRow != 2)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
						if (MeterKeyArray[lngCurrentMeterNumber].WaterKey3 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].WaterType3 == lngType && lngType != -1) && lngRow != 3)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
						if (MeterKeyArray[lngCurrentMeterNumber].WaterKey4 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].WaterType4 == lngType && lngType != -1) && lngRow != 4)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
						if (MeterKeyArray[lngCurrentMeterNumber].WaterKey5 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].WaterType5 == lngType && lngType != -1) && lngRow != 5)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
					}
					else
					{
						if (MeterKeyArray[lngCurrentMeterNumber].SewerKey1 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].SewerType1 == lngType && lngType != -1) && lngRow != 1)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
						if (MeterKeyArray[lngCurrentMeterNumber].SewerKey2 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].SewerType2 == lngType && lngType != -1) && lngRow != 2)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
						if (MeterKeyArray[lngCurrentMeterNumber].SewerKey3 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].SewerType3 == lngType && lngType != -1) && lngRow != 3)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
						if (MeterKeyArray[lngCurrentMeterNumber].SewerKey4 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].SewerType4 == lngType && lngType != -1) && lngRow != 4)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
						if (MeterKeyArray[lngCurrentMeterNumber].SewerKey5 == lngRT && (MeterKeyArray[lngCurrentMeterNumber].SewerType5 == lngType && lngType != -1) && lngRow != 5)
						{
							CheckForMultipleRT = false;
							return CheckForMultipleRT;
						}
					}
				}
				return CheckForMultipleRT;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Rate Tables", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckForMultipleRT;
		}

		private void SetAllMetersToFinalBilled()
		{
			// This function will set all of the meters for this account to Final Billed
			int lngCT;
			for (lngCT = 1; lngCT <= 99; lngCT++)
			{
				if (MeterKeyArray[lngCT].Used)
				{
					MeterKeyArray[lngCT].FinalBilled = true;
					boolDirty = true;
				}
			}
		}
		
		private void AdjustUnBilledBillRecords(ref int lngAcct, ref int lngMK)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsD = new clsDRWrapper();
				// kk07152015 trout-1173  Need to make sure there are no payments on the unbilled bill before deleting
				// rsBill.Execute "DELETE FROM Bill WHERE AccountKey = " & lngAcct & " AND MeterKey = " & lngMK & " AND BillingRateKey = 0 AND WPrinOwed = 0 AND SPrinOwed = 0 AND WPrinPaid = 0 AND SPrinPaid = 0", strUTDatabase
				rsBill.OpenRecordset("SELECT ID FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAcct) + " AND MeterKey = " + FCConvert.ToString(lngMK) + " AND BillingRateKey = 0 AND WPrinOwed = 0 AND SPrinOwed = 0 AND WLienRecordNumber = 0 AND SLienRecordNumber = 0 AND WPrinPaid = 0 AND SPrinPaid = 0", modExtraModules.strUTDatabase);
				while (!rsBill.EndOfFile())
				{
					rsD.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsBill.Get_Fields_Int32("ID") + " AND ISNULL(Lien,0) = 0", modExtraModules.strUTDatabase);
					if (rsD.EndOfFile())
					{
						rsD.Execute("DELETE FROM TempBreakDown WHERE Billkey = " + rsBill.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
						rsD.Execute("DELETE FROM Bill WHERE ID = " + rsBill.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
					}
					rsBill.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Removing Bills", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveMeterChanges()
		{
			// this will make sure the current control saves the last change
			string strActiveControl;
			strActiveControl = this.ActiveControl.GetName();
			if (Strings.UCase(strActiveControl) == Strings.UCase("TXTREMOTENUMBER"))
			{
				txtRemoteNumber_Validate(false);
			}
			else if (Strings.UCase(strActiveControl) == Strings.UCase("txtSerialNumber"))
			{
				txtSerialNumber_Validate(false);
			}
		}

		private bool ValidateData()
		{
			bool ValidateData = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int counter;
				cPartyAddress oAdd;
				string strPrimaryService = "";
				// check the master account information and make sure the most important data is saved
				// k    If Trim(txtOName.Text) = "" Then
				if ((oOwner1 == null) || Conversion.Val(txtOwner1CustNum.Text) == 0)
				{
					MessageBox.Show("Please enter an Owner name.", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);

					if (txtOwner1CustNum.Enabled && txtOwner1CustNum.Visible)
					{
						txtOwner1CustNum.Focus();
					}
					return ValidateData;
				}
				// Check for a primary address in Central Parties
				oAdd = oOwner1.GetAddress("UT");
				if (oAdd == null)
				{
					MessageBox.Show("Please enter an Owner address.", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					if (txtOwner1CustNum.Enabled && txtOwner1CustNum.Visible)
					{
						txtOwner1CustNum.Focus();
					}
					return ValidateData;
				}
				
				if (Strings.Trim(txtLocationStreet.Text) == "")
				{
					MessageBox.Show("Please enter a location", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					if (txtLocationStreet.Enabled && txtLocationStreet.Visible)
					{
						txtLocationStreet.Focus();
					}
					return ValidateData;
				}
				if (Strings.Trim(txtEmail.Text) == "" && chkEmailBills.CheckState == Wisej.Web.CheckState.Checked)
				{
					MessageBox.Show("You must enter an E-Mail address if you select to send bills by E-Mail", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					if (txtEmail.Enabled && txtEmail.Visible)
					{
						txtEmail.Focus();
					}
					return ValidateData;
				}
				for (counter = 1; counter <= 99; counter++)
				{
					// DJW@01092013 TROUT-769 Added check to not allow different service from main service
					if (counter == 1)
					{
						strPrimaryService = MeterKeyArray[counter].Service;
					}
					else
					{
						if (strPrimaryService == "B")
						{
							// do nothing
						}
						else
						{
							if (MeterKeyArray[counter].Service != strPrimaryService && MeterKeyArray[counter].Used == true)
							{
								MessageBox.Show("The service on all meters must match the service of the primary meter unless the primary meter has a service type of both", "Invalid Meter Service", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return ValidateData;
							}
						}
					}
					if (MeterKeyArray[counter].UseRate1)
					{
						if (MeterKeyArray[counter].WaterType1 <= 0 && MeterKeyArray[counter].SewerType1 <= 0)
						{
							MessageBox.Show("You must select a usage type for meter " + FCConvert.ToString(counter) + " before you may continue", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return ValidateData;
						}
					}
					if (MeterKeyArray[counter].UseRate2)
					{
						if (MeterKeyArray[counter].WaterType2 <= 0 && MeterKeyArray[counter].SewerType2 <= 0)
						{
							MessageBox.Show("You must select a usage type for meter " + FCConvert.ToString(counter) + " before you may continue.", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return ValidateData;
						}
					}
					if (MeterKeyArray[counter].UseRate3)
					{
						if (MeterKeyArray[counter].WaterType3 <= 0 && MeterKeyArray[counter].SewerType3 <= 0)
						{
							MessageBox.Show("You must select a usage type for meter " + FCConvert.ToString(counter) + " before you may continue.", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return ValidateData;
						}
					}
					if (MeterKeyArray[counter].UseRate4)
					{
						if (MeterKeyArray[counter].WaterType4 <= 0 && MeterKeyArray[counter].SewerType4 <= 0)
						{
							MessageBox.Show("You must select a usage type for meter " + FCConvert.ToString(counter) + " before you may continue.", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return ValidateData;
						}
					}
					if (MeterKeyArray[counter].UseRate5)
					{
						if (MeterKeyArray[counter].WaterType5 <= 0 && MeterKeyArray[counter].SewerType5 <= 0)
						{
							MessageBox.Show("You must select a usage type for meter " + FCConvert.ToString(counter) + " before you may continue.", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return ValidateData;
						}
					}
				}
				if (!(chkBillSameAsOwner.CheckState == Wisej.Web.CheckState.Checked))
				{
					// If Trim(txtBName.Text) = "" Then
					if ((oTenant1 == null) || Conversion.Val(txtTenant1CustNum.Text) == 0)
					{
						MessageBox.Show("Please enter a Tenant name.", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);

						if (txtTenant1CustNum.Enabled && txtTenant1CustNum.Visible)
						{
							txtTenant1CustNum.Focus();
						}
						return ValidateData;
					}
					// Check for a primary address in Central Parties
					oAdd = oOwner1.GetAddress("UT");
					if (oAdd == null)
					{
						// k       If Trim(txtBAddress1.Text) = "" Then
						MessageBox.Show("Please enter a Tenant address.", "Validate Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);

						if (txtTenant1CustNum.Enabled && txtTenant1CustNum.Visible)
						{
							txtTenant1CustNum.Focus();
						}
						return ValidateData;
					}
					
				}
				ValidateData = true;
				return ValidateData;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateData;
		}

		private bool SaveFinalBillInfo()
		{
			bool SaveFinalBillInfo = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				DateTime dtEnd;
				DateTime dtStart;
				DateTime dtBill;
				clsDRWrapper rsFinal = new clsDRWrapper();
				// save the final bill information
				rsFinal.OpenRecordset("SELECT * FROM Metertable WHERE ID = " + FCConvert.ToString(modMain.Statics.lngCurrentMeterKey));
				if (!rsFinal.EndOfFile())
				{
                    //if ((Strings.Trim(txtPeriodEnd.Text) != "" && Strings.Trim(txtPeriodStart.Text) != "" && Strings.Trim(txtFinalBillDate.Text) != "") && (Strings.Trim(txtPeriodEnd.Text) != "00/00/0000" && Strings.Trim(txtPeriodStart.Text) != "00/00/0000" && Strings.Trim(txtFinalBillDate.Text) != "00/00/0000"))
                    if(!txtPeriodEnd.IsEmpty && !txtPeriodStart.IsEmpty && !txtFinalBillDate.IsEmpty)
                    {
						//if (Strings.Trim(txtPeriodEnd.Text) != "00/00/0000" && Strings.Trim(txtPeriodStart.Text) != "00/00/0000" && Strings.Trim(txtFinalBillDate.Text) != "00/00/0000")
						{
							dtStart = DateAndTime.DateValue(txtPeriodStart.Text);
							dtEnd = DateAndTime.DateValue(txtPeriodEnd.Text);
							dtBill = DateAndTime.DateValue(txtFinalBillDate.Text);
							if (dtStart.ToOADate() < dtEnd.ToOADate() && dtBill.ToOADate() < dtEnd.ToOADate() && dtStart.ToOADate() < dtBill.ToOADate())
							{
								rsFinal.Edit();
								rsFinal.Set_Fields("FinalStartDate", dtStart);
								rsFinal.Set_Fields("FinalEndDate", dtEnd);
								rsFinal.Set_Fields("FinalBillDate", dtBill);
								rsFinal.Set_Fields("Final", true);
								rsFinal.Update();
								MessageBox.Show("Final Bill information saved.", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								SaveFinalBillInfo = true;
							}
							else
							{
								MessageBox.Show("Bill date must be between the start and end date.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								SaveFinalBillInfo = false;
							}
						}
						//else
						//{
						//	MessageBox.Show("Bill date must be between the start and end date.", "Invalid Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						//	SaveFinalBillInfo = false;
						//}
					}
					//else if ((Strings.Trim(txtPeriodEnd.Text) == "" && Strings.Trim(txtPeriodStart.Text) == "" && Strings.Trim(txtFinalBillDate.Text) == "") || (Strings.Trim(txtPeriodEnd.Text) == "00/00/0000" && Strings.Trim(txtPeriodStart.Text) == "00/00/0000" && Strings.Trim(txtFinalBillDate.Text) == "00/00/0000"))
                    else if (txtPeriodEnd.IsEmpty && txtPeriodStart.IsEmpty && txtFinalBillDate.IsEmpty)
                    {
						rsFinal.Edit();
						rsFinal.Set_Fields("FinalStartDate", 0);
						rsFinal.Set_Fields("FinalEndDate", 0);
						rsFinal.Set_Fields("FinalBillDate", 0);
						rsFinal.Set_Fields("Final", false);
						rsFinal.Update();
						MessageBox.Show("Final Bill information cleared.", "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveFinalBillInfo = true;
					}
					else
					{
						MessageBox.Show("Not all information filled out.", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveFinalBillInfo = false;
						// rsFinal.Edit
						// rsFinal.Fields("FinalStartDate") = ""
						// rsFinal.Fields("FinalEndDate") = ""
						// rsFinal.Fields("FinalBillDate") = ""
						// rsFinal.Fields("Final") = False
						// rsFinal.Update
					}
					if (SaveFinalBillInfo)
					{
						modGlobalFunctions.AddCYAEntry_242("UT", "Final Billing Information Change", "Start:" + FCConvert.ToString(rsFinal.Get_Fields_DateTime("FinalStartDate")), "End:" + FCConvert.ToString(rsFinal.Get_Fields_DateTime("FinalEndDate")), "Bill Date:" + FCConvert.ToString(rsFinal.Get_Fields_DateTime("FinalBillDate")));
						// MAL@20081112: Add check for existing non-billed bill records
						// Tracker Reference: 16036
						RemoveBillRecords();
					}
				}
				return SaveFinalBillInfo;
			}
			catch (Exception ex)
			{
				
				SaveFinalBillInfo = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveFinalBillInfo;
		}

		private void ShowFinalBillInfo()
		{
			// save the final bill information
			clsDRWrapper rsFinal = new clsDRWrapper();
			rsFinal.OpenRecordset("SELECT * FROM Metertable WHERE ID = " + FCConvert.ToString(modMain.Statics.lngCurrentMeterKey));
			if (!rsFinal.EndOfFile())
			{
				if (FCConvert.ToBoolean(rsFinal.Get_Fields_Boolean("Final")))
				{
					txtPeriodStart.Text = FCConvert.ToString(rsFinal.Get_Fields_DateTime("FinalStartDate"));
					txtPeriodEnd.Text = FCConvert.ToString(rsFinal.Get_Fields_DateTime("FinalEndDate"));
					txtFinalBillDate.Text = FCConvert.ToString(rsFinal.Get_Fields_DateTime("FinalBillDate"));
				}
				else
				{
					txtPeriodStart.Text = "";
					txtPeriodEnd.Text = "";
					txtFinalBillDate.Text = "";
				}
				// show the actual frame
				fraFinalBillInfo.Top = FCConvert.ToInt32((this.Height - fraFinalBillInfo.Height) / 2.0);
				fraFinalBillInfo.Left = FCConvert.ToInt32((this.Width - fraFinalBillInfo.Width) / 2.0);
				fraMeter.Visible = false;
				fraFinalBillInfo.Visible = true;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		private bool HasMeterHistory()
		{
			bool HasMeterHistory = false;
			bool blnResult = false;
			clsDRWrapper rsHistory = new clsDRWrapper();
			int lngAcctNum;
			int lngMeterNum = 0;
			lngAcctNum = lngKey;
			// MAL@20080331: Add check for existing meters
			// Tracker Reference: 12920
			if (cmbMeterNumber.Items.Count > 0)
			{
				lngMeterNum = MeterKeyArray[cmbMeterNumber.ItemData(cmbMeterNumber.SelectedIndex)].Key;
				rsHistory.OpenRecordset("SELECT * FROM tblPreviousMeter WHERE AccountKey = " + FCConvert.ToString(lngAcctNum) + " AND PreviousMeterKey = " + FCConvert.ToString(lngMeterNum));
				if (rsHistory.RecordCount() > 0)
				{
					blnResult = true;
				}
				else
				{
					blnResult = false;
				}
			}
			else
			{
				blnResult = false;
			}
			HasMeterHistory = blnResult;
			return HasMeterHistory;
		}

		private void UpdateIConnectInformation(string strOwner, string strOwnerTenant)
		{
			clsDRWrapper rsM = new clsDRWrapper();
			string strName = "";
			rsM.OpenRecordset("SELECT * FROM tblIConnectInfo WHERE OwnerTenant = '" + strOwnerTenant + "' AND AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT)), modExtraModules.strUTDatabase);
			strName = modMain.RemoveSpecialCharacters(strOwner, true);
			if (rsM.RecordCount() > 0)
			{
				rsM.Edit();
			}
			else
			{
				rsM.AddNew();
				rsM.Set_Fields("AccountKey", modUTStatusPayments.GetAccountKeyUT(ref modUTStatusPayments.Statics.lngCurrentAccountUT));
				rsM.Set_Fields("OwnerTenant", strOwnerTenant);
				// kk01202016 trout-936  Separate Owner and Tenant account information
			}
			rsM.Set_Fields("IConnectAccountKey", 0);
			rsM.Set_Fields("WebPIN", Strings.Left(strName, 3) + FCConvert.ToString(modUTStatusPayments.Statics.lngCurrentAccountUT));
			rsM.Set_Fields("AcceptEmail", false);
			// kk01202016 trout-936            .Fields("OwnerTenant") = "O"
			rsM.Set_Fields("MoveInDate", null);
			rsM.Set_Fields("SpecialMessage", "");
			rsM.Set_Fields("ActivationDate", null);
			rsM.Set_Fields("AcceptEbill", "P");
			rsM.Set_Fields("GraphFlag", "D");
			// kk01202016 trout-936  Track conveyance date and add uploaded flag
			rsM.Set_Fields("ConveyedDate", DateTime.Today);
			rsM.Set_Fields("Uploaded", false);
			rsM.Update();
		}
	
		private void UpdateCurrentBillRecord(int lngAcctKey, int lngMeterKey, int intBookNum, int lngPrevReading)
		{
			// Tracker Reference: 15612
			clsDRWrapper rsBill = new clsDRWrapper();
			clsDRWrapper rsBook = new clsDRWrapper();
			bool blnCont = false;
			// vbPorter upgrade warning: dtClear As DateTime	OnWrite(string)
			DateTime dtClear = default(DateTime);
			rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(intBookNum), modExtraModules.strUTDatabase);
			if (rsBook.RecordCount() > 0)
			{
				if (FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")) == "DE")
				{
					blnCont = true;
					dtClear = FCConvert.ToDateTime(Strings.Format(rsBook.Get_Fields_DateTime("CDate"), "MM/dd/yyyy"));
				}
				else
				{
					blnCont = false;
				}
			}
			if (blnCont)
			{
				rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND MeterKey = " + FCConvert.ToString(lngMeterKey) + " AND CurDate >= '" + FCConvert.ToString(dtClear) + "'", modExtraModules.strUTDatabase);
				if (rsBill.RecordCount() > 0)
				{
					rsBill.Edit();
					rsBill.Set_Fields("PrevReading", lngPrevReading);
					rsBill.Update();
				}
			}
		}

		private void RemoveBillRecords()
		{
			// Tracker Reference: 16036
			clsDRWrapper rsBill = new clsDRWrapper();
			rsBill.OpenRecordset("SELECT * FROM Bill WHERE MeterKey = " + FCConvert.ToString(modMain.Statics.lngCurrentMeterKey) + " AND BillStatus <> 'B'", modExtraModules.strUTDatabase);
			if (rsBill.RecordCount() > 0)
			{
				rsBill.MoveFirst();
				while (!rsBill.EndOfFile())
				{
					rsBill.Delete();
					rsBill.MoveNext();
				}
			}
		}

		private void ShowPartyInfo_2(string strOwnerTenant)
		{
			ShowPartyInfo(strOwnerTenant);
		}

		private void ShowPartyInfo( string strOwnerTenant)
		{
			cPartyAddress pAdd = null;
			FCCollection pComments = new FCCollection();
			string strTemp = "";
			string strTemp2 = "";
			// kk01232015 trouts-134  Tweaked this a bit to handle both parties all of the time
			if (strOwnerTenant == "O")
			{
				// Get the second owner party information
				if (!(oOwner2 == null))
				{
					strTemp2 = oOwner2.FullNameLastFirst;
					pComments = oOwner2.GetModuleSpecificComments("UT");
					if (pComments.Count > 0)
					{
						imgOwner2Memo.Visible = true;
					}
					else
					{
						imgOwner2Memo.Visible = false;
					}
					cmdOwner2Edit.Enabled = true;
				}
				else
				{
					imgOwner2Memo.Visible = false;
					cmdOwner2Edit.Enabled = false;
				}
				if (!(oOwner1 == null))
				{
					strTemp = oOwner1.FullNameLastFirst;
					pComments = oOwner1.GetModuleSpecificComments("UT");
					if (pComments.Count > 0)
					{
						imgOwner1Memo.Visible = true;
					}
					else
					{
						imgOwner1Memo.Visible = false;
					}
					cmdOwner1Edit.Enabled = true;
					pAdd = oOwner1.GetAddress("UT");
				}
				else
				{
					imgOwner1Memo.Visible = false;
					cmdOwner1Edit.Enabled = false;
				}
				if (strTemp2 != "")
				{
					//FC:FINAL:MSH - issue #905: replace "\r\n" for correct formatting string in label
					//strTemp += "\r\n" + strTemp2;
					strTemp += "\r\n" + strTemp2;
				}
				if (!(pAdd == null))
				{
					//FC:FINAL:MSH - issue #905: replace "\r\n" for correct formatting string in label
					//strTemp += "\r\n" + pAdd.GetFormattedAddress();
					strTemp += "\r\n" + pAdd.GetFormattedAddress();
				}
				lblOwnerCustInfo.Text = strTemp;
			}
			else if (strOwnerTenant == "T")
			{
				// Get the second party information
				if (!(oTenant2 == null))
				{
					strTemp2 = oTenant2.FullNameLastFirst;
					pComments = oTenant2.GetModuleSpecificComments("UT");
					if (pComments.Count > 0)
					{
						imgTenant2Memo.Visible = true;
					}
					else
					{
						imgTenant2Memo.Visible = false;
					}
					cmdTenant2Edit.Enabled = true;
				}
				else
				{
					imgTenant2Memo.Visible = false;
					cmdTenant2Edit.Enabled = false;
				}
				if (!(oTenant1 == null))
				{
					strTemp = oTenant1.FullNameLastFirst;
					pComments = oTenant1.GetModuleSpecificComments("UT");
					if (pComments.Count > 0)
					{
						imgTenant1Memo.Visible = true;
					}
					else
					{
						imgTenant1Memo.Visible = false;
					}
					cmdTenant1Edit.Enabled = true;
					pAdd = oTenant1.GetAddress("UT");
				}
				else
				{
					imgTenant1Memo.Visible = false;
					cmdTenant1Edit.Enabled = false;
				}
				if (strTemp2 != "")
				{
					//FC:FINAL:MSH - issue #905: replace "\r\n" for correct formatting string in label
					//strTemp += "\r\n" + strTemp2;
					strTemp += "\r\n" + strTemp2;
				}
				if (!(pAdd == null))
				{
					//FC:FINAL:MSH - issue #905: replace "\r\n" for correct formatting string in label
					//strTemp += "\r\n" + pAdd.GetFormattedAddress();
					strTemp += "\r\n" + pAdd.GetFormattedAddress();
				}
				lblTenantCustInfo.Text = strTemp;
			}
			pAdd = null;
			pComments = null;
		}

        private void AutoPay()
        {
            var strSrvc = "";
            if (boolMeterSewerService && boolMeterWaterService)
            {
                strSrvc = "B";
            }
            else if (boolMeterSewerService)
            {
                strSrvc = "S";
            }
            else if (boolMeterWaterService)
            {
                strSrvc = "W";
            }
            else
            {
                strSrvc = modUTStatusPayments.Statics.TownService;
            }

            frmAcctAutoPay.InstancePtr.Init(lngKey, txtAccountNumber.Text, strSrvc);
        }

        private void DisableREInfo()
        {
            txtREBook.Enabled = false;
            txtREMapLot.Enabled = false;
            txtREPage.Enabled = false;
            lblREBook.Enabled = false;
            lblREPage.Enabled = false;
            lblREMapLot.Enabled = false;
            txtDeedName1.Enabled = false;
            txtDeedName2.Enabled = false;
            txtOwner1CustNum.Enabled = false;
            cmdOwner1Edit.Enabled = false;
            cmdOwner1Search.Enabled = false;
            cmdOwner2Edit.Enabled = false;
            cmdOwner2Search.Enabled = false;
            txtOwner2CustNum.Enabled = false;
            chkUseREMortgage.Enabled = false;
        }

        private void EnableREInfo()
        {
            txtREBook.Enabled = true;
            txtREMapLot.Enabled = true;
            txtREPage.Enabled = true;
            lblREBook.Enabled = true;
            lblREPage.Enabled = true;
            lblREMapLot.Enabled = true;
            txtDeedName1.Enabled = true;
            txtDeedName2.Enabled = true;
            txtOwner1CustNum.Enabled = true;
            int intTemp;
            if (Int32.TryParse(txtOwner1CustNum.Text,out intTemp))
            {
                if (intTemp > 0)
                {
                    cmdOwner1Edit.Enabled = true;
                }
            }

            cmdOwner1Search.Enabled = true;
            if (Int32.TryParse(txtOwner2CustNum.Text, out intTemp))
            {
                if (intTemp > 0)
                {
                    cmdOwner2Edit.Enabled = true;
                }
            }

            cmdOwner2Search.Enabled = true;
            txtOwner2CustNum.Enabled = true;
            chkUseREMortgage.Enabled = true;
        }

        private void UpdateOwner(int intID)
        {
            int intOrigID = 0;
            if (oOwner1 != null)
            {
                intOrigID = oOwner1.ID;
            }

            if (intID != intOrigID)
            {
                txtOwner1CustNum.Text = intID.ToString();
                oOwner1 = partCont.GetParty(intID);
                if (oOwner1 != null)
                {
                    txtDeedName1.Text = oOwner1.FullNameLastFirst;
                    txtDeedName2.Text = "";
                    oOwner2 = null;
                    txtOwner2CustNum.Text = "";
                }
                ShowPartyInfo("O");
                boolDirty = true;
            }
        }

        private void UpdateSecondOwner(int intID)
        {
            int intOrigID = 0;
            if (oOwner2 != null)
            {
                intOrigID = oOwner2.ID;
            }

            if (intID != intOrigID)
            {
                txtOwner2CustNum.Text = intID.ToString();
                oOwner2 = partCont.GetParty(intID);
                if (oOwner2 != null)
                {
                    txtDeedName2.Text = oOwner2.FullNameLastFirst;
                }
                ShowPartyInfo("O");
                boolDirty = true;
            }
        }

        private void FillACHInfo()
        {
            var rsTemp = new clsDRWrapper();
            var strTemp = "";

            lblACHInfo.Visible = false;
            ToolTip1.SetToolTip(lblACHInfo, "");

            rsTemp.OpenRecordset(
                "select * from tblAcctACH where accountkey = " + lngKey.ToString() + " and achactive = 1",
                "UtilityBilling");
            while (!rsTemp.EndOfFile())
            {
                if (rsTemp.Get_Fields_Boolean("ACHActive"))
                {
                    if (rsTemp.Get_Fields_Boolean("ACHPrenote"))
                    {
                        strTemp = "AutoPay is in PreNote status";
                    }
                    else
                    {
                        strTemp = "AutoPay is Active";
                    }

                    ToolTip1.SetToolTip(lblACHInfo,strTemp);
                    lblACHInfo.Visible = true;
                }

                rsTemp.MoveNext();
            }
        }

        private void MnuAutoPay_Click(object sender, EventArgs e)
        {
            AutoPay();
        }
    }
}
