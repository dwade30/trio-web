﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmPurgePaidBills.
	/// </summary>
	partial class frmPurgePaidBills : BaseForm
	{
		public fecherFoundation.FCButton cmdPurge;
		public Global.T2KDateBox txtDate;
		public fecherFoundation.FCLabel lblInstructions;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPurgePaidBills));
			this.cmdPurge = new fecherFoundation.FCButton();
			this.txtDate = new Global.T2KDateBox();
			this.lblInstructions = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 286);
			this.BottomPanel.Size = new System.Drawing.Size(441, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmdPurge);
			this.ClientArea.Controls.Add(this.txtDate);
			this.ClientArea.Controls.Add(this.lblInstructions);
			this.ClientArea.Size = new System.Drawing.Size(441, 226);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(441, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(186, 30);
			this.HeaderText.Text = "Purge Paid Bills";
			// 
			// cmdPurge
			// 
			this.cmdPurge.AppearanceKey = "acceptButton";
			this.cmdPurge.ForeColor = System.Drawing.Color.White;
			this.cmdPurge.Location = new System.Drawing.Point(30, 124);
			this.cmdPurge.Name = "cmdPurge";
			this.cmdPurge.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPurge.Size = new System.Drawing.Size(87, 48);
			this.cmdPurge.TabIndex = 2;
			this.cmdPurge.Text = "Purge";
			this.cmdPurge.Click += new System.EventHandler(this.cmdPurge_Click);
			// 
			// txtDate
			// 
			this.txtDate.Location = new System.Drawing.Point(30, 64);
			//FC:FINAL:CHN - issue #1188: Allow only numeric for date.
			this.txtDate.Mask = "00/00/0000";
			//"##/##/####";
			this.txtDate.MaxLength = 10;
			this.txtDate.Name = "txtDate";
			this.txtDate.Size = new System.Drawing.Size(115, 40);
			this.txtDate.TabIndex = 1;
			this.txtDate.Text = "  /  /";
			// 
			// lblInstructions
			// 
			this.lblInstructions.AutoSize = true;
			this.lblInstructions.Location = new System.Drawing.Point(30, 30);
			this.lblInstructions.Name = "lblInstructions";
			this.lblInstructions.Size = new System.Drawing.Size(4, 14);
			this.lblInstructions.TabIndex = 0;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Process";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmPurgePaidBills
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(441, 394);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmPurgePaidBills";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Purge Paid Bills";
			this.Load += new System.EventHandler(this.frmPurgePaidBills_Load);
			this.Activated += new System.EventHandler(this.frmPurgePaidBills_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmPurgePaidBills_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPurge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
