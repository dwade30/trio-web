﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptRateTables.
	/// </summary>
	partial class rptRateTables
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRateTables));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldRateTableDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRateTableNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRateTable = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblServiceW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMinimumW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblConsumptionW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblChargeW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldConsumptionW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldChargeW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFlatChargeW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFlatChargeW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMiscChargesW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMiscDesc1W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMiscDesc2W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMiscAmt1W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMiscAmt2W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMiscAccount1W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMiscAccount2W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl1W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru1W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver1W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblChargeAmountW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldStep1W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl2W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru2W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver2W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep2W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl3W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru3W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver3W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep3W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl4W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru4W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver4W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep4W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl5W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru5W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver5W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep5W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl6W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru6W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver6W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep6W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl7W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru7W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver7W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep7W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl8W = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru8W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver8W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep8W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblServiceS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMinimumS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblConsumptionS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblChargeS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldConsumptionS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldChargeS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFlatChargeS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFlatChargeS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMiscChargesS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMiscDesc1S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMiscDesc2S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMiscAmt1S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMiscAmt2S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMiscAccount1S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMiscAccount2S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl1S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru1S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver1S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblChargeAmountS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldStep1S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl2S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru2S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver2S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep2S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl3S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru3S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver3S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep3S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl4S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru4S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver4S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep4S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl5S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru5S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver5S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep5S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl6S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru6S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver6S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep6S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl7S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru7S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver7S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep7S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lbl8S = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldThru8S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOver8S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStep8S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblOverW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblThruW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmtW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblStepW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOverS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblThruS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmtS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblStepS = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAmount1W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount2W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount3W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount4W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount5W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount6W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount7W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount8W = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount1S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount2S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount3S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount4S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount5S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount6S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount7S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount8S = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateTableDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateTableNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateTable)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblServiceW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMinimumW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsumptionW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChargeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumptionW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldChargeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFlatChargeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFlatChargeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscChargesW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscDesc1W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscDesc2W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAmt1W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAmt2W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAccount1W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAccount2W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl1W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru1W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver1W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChargeAmountW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep1W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl2W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru2W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver2W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep2W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl3W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru3W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver3W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep3W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl4W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru4W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver4W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep4W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl5W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru5W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver5W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep5W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl6W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru6W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver6W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep6W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl7W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru7W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver7W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep7W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl8W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru8W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver8W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep8W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblServiceS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMinimumS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsumptionS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChargeS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumptionS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldChargeS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFlatChargeS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFlatChargeS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscChargesS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscDesc1S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscDesc2S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAmt1S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAmt2S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAccount1S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAccount2S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl1S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru1S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver1S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChargeAmountS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep1S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl2S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru2S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver2S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep2S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl3S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru3S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver3S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep3S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl4S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru4S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver4S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep4S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl5S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru5S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver5S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep5S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl6S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru6S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver6S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep6S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl7S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru7S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver7S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep7S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl8S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru8S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver8S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep8S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblThruW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmtW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStepW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblThruS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmtS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStepS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount1W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount3W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount4W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount5W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount6W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount7W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount8W)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount1S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount3S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount4S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount5S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount6S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount7S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount8S)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldRateTableDescription,
				this.fldRateTableNumber,
				this.lblRateTable,
				this.lblServiceW,
				this.lblMinimumW,
				this.lblConsumptionW,
				this.lblChargeW,
				this.Line1,
				this.fldConsumptionW,
				this.fldChargeW,
				this.lblFlatChargeW,
				this.fldFlatChargeW,
				this.lblMiscChargesW,
				this.lblMiscDesc1W,
				this.lblMiscDesc2W,
				this.fldMiscAmt1W,
				this.fldMiscAmt2W,
				this.fldMiscAccount1W,
				this.fldMiscAccount2W,
				this.lbl1W,
				this.fldThru1W,
				this.fldOver1W,
				this.lblChargeAmountW,
				this.fldStep1W,
				this.lbl2W,
				this.fldThru2W,
				this.fldOver2W,
				this.fldStep2W,
				this.lbl3W,
				this.fldThru3W,
				this.fldOver3W,
				this.fldStep3W,
				this.lbl4W,
				this.fldThru4W,
				this.fldOver4W,
				this.fldStep4W,
				this.lbl5W,
				this.fldThru5W,
				this.fldOver5W,
				this.fldStep5W,
				this.lbl6W,
				this.fldThru6W,
				this.fldOver6W,
				this.fldStep6W,
				this.lbl7W,
				this.fldThru7W,
				this.fldOver7W,
				this.fldStep7W,
				this.lbl8W,
				this.fldThru8W,
				this.fldOver8W,
				this.fldStep8W,
				this.lblServiceS,
				this.lblMinimumS,
				this.lblConsumptionS,
				this.lblChargeS,
				this.fldConsumptionS,
				this.fldChargeS,
				this.lblFlatChargeS,
				this.fldFlatChargeS,
				this.lblMiscChargesS,
				this.lblMiscDesc1S,
				this.lblMiscDesc2S,
				this.fldMiscAmt1S,
				this.fldMiscAmt2S,
				this.fldMiscAccount1S,
				this.fldMiscAccount2S,
				this.lbl1S,
				this.fldThru1S,
				this.fldOver1S,
				this.lblChargeAmountS,
				this.fldStep1S,
				this.lbl2S,
				this.fldThru2S,
				this.fldOver2S,
				this.fldStep2S,
				this.lbl3S,
				this.fldThru3S,
				this.fldOver3S,
				this.fldStep3S,
				this.lbl4S,
				this.fldThru4S,
				this.fldOver4S,
				this.fldStep4S,
				this.lbl5S,
				this.fldThru5S,
				this.fldOver5S,
				this.fldStep5S,
				this.lbl6S,
				this.fldThru6S,
				this.fldOver6S,
				this.fldStep6S,
				this.lbl7S,
				this.fldThru7S,
				this.fldOver7S,
				this.fldStep7S,
				this.lbl8S,
				this.fldThru8S,
				this.fldOver8S,
				this.fldStep8S,
				this.Line2,
				this.lblOverW,
				this.lblThruW,
				this.lblAmtW,
				this.lblStepW,
				this.lblOverS,
				this.lblThruS,
				this.lblAmtS,
				this.lblStepS,
				this.fldAmount1W,
				this.fldAmount2W,
				this.fldAmount3W,
				this.fldAmount4W,
				this.fldAmount5W,
				this.fldAmount6W,
				this.fldAmount7W,
				this.fldAmount8W,
				this.fldAmount1S,
				this.fldAmount2S,
				this.fldAmount3S,
				this.fldAmount4S,
				this.fldAmount5S,
				this.fldAmount6S,
				this.fldAmount7S,
				this.fldAmount8S
			});
			this.Detail.Height = 3.947917F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblMuni,
				this.lblPage,
				this.lblTime
			});
			this.PageHeader.Height = 0.4270833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Rate Table Listing";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 0F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblDate.Text = null;
			this.lblDate.Top = 0.1875F;
			this.lblDate.Width = 1.5F;
			// 
			// lblMuni
			// 
			this.lblMuni.Height = 0.1875F;
			this.lblMuni.HyperLink = null;
			this.lblMuni.Left = 0F;
			this.lblMuni.Name = "lblMuni";
			this.lblMuni.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMuni.Text = null;
			this.lblMuni.Top = 0F;
			this.lblMuni.Width = 1.5F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.1875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.3125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 6.1875F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblTime.Text = null;
			this.lblTime.Top = 0F;
			this.lblTime.Width = 1.3125F;
			// 
			// fldRateTableDescription
			// 
			this.fldRateTableDescription.Height = 0.1875F;
			this.fldRateTableDescription.Left = 2.875F;
			this.fldRateTableDescription.Name = "fldRateTableDescription";
			this.fldRateTableDescription.Style = "font-family: \'Tahoma\'";
			this.fldRateTableDescription.Text = null;
			this.fldRateTableDescription.Top = 0.125F;
			this.fldRateTableDescription.Width = 3.25F;
			// 
			// fldRateTableNumber
			// 
			this.fldRateTableNumber.Height = 0.1875F;
			this.fldRateTableNumber.Left = 2.25F;
			this.fldRateTableNumber.Name = "fldRateTableNumber";
			this.fldRateTableNumber.Style = "font-family: \'Tahoma\'";
			this.fldRateTableNumber.Text = null;
			this.fldRateTableNumber.Top = 0.125F;
			this.fldRateTableNumber.Width = 0.625F;
			// 
			// lblRateTable
			// 
			this.lblRateTable.Height = 0.1875F;
			this.lblRateTable.HyperLink = null;
			this.lblRateTable.Left = 0.6875F;
			this.lblRateTable.Name = "lblRateTable";
			this.lblRateTable.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblRateTable.Text = "Rate Table:";
			this.lblRateTable.Top = 0.125F;
			this.lblRateTable.Width = 1.5625F;
			// 
			// lblServiceW
			// 
			this.lblServiceW.Height = 0.1875F;
			this.lblServiceW.HyperLink = null;
			this.lblServiceW.Left = 0F;
			this.lblServiceW.Name = "lblServiceW";
			this.lblServiceW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblServiceW.Text = "Water";
			this.lblServiceW.Top = 0.375F;
			this.lblServiceW.Width = 3.75F;
			// 
			// lblMinimumW
			// 
			this.lblMinimumW.Height = 0.1875F;
			this.lblMinimumW.HyperLink = null;
			this.lblMinimumW.Left = 0.125F;
			this.lblMinimumW.Name = "lblMinimumW";
			this.lblMinimumW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMinimumW.Text = "Minimum:";
			this.lblMinimumW.Top = 0.5625F;
			this.lblMinimumW.Width = 0.75F;
			// 
			// lblConsumptionW
			// 
			this.lblConsumptionW.Height = 0.1875F;
			this.lblConsumptionW.HyperLink = null;
			this.lblConsumptionW.Left = 0.3125F;
			this.lblConsumptionW.Name = "lblConsumptionW";
			this.lblConsumptionW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblConsumptionW.Text = "Consumption:";
			this.lblConsumptionW.Top = 0.75F;
			this.lblConsumptionW.Width = 1.1875F;
			// 
			// lblChargeW
			// 
			this.lblChargeW.Height = 0.1875F;
			this.lblChargeW.HyperLink = null;
			this.lblChargeW.Left = 0.3125F;
			this.lblChargeW.Name = "lblChargeW";
			this.lblChargeW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblChargeW.Text = "Charge:";
			this.lblChargeW.Top = 0.9375F;
			this.lblChargeW.Width = 1.1875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 5.5F;
			this.Line1.X1 = 1F;
			this.Line1.X2 = 6.5F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// fldConsumptionW
			// 
			this.fldConsumptionW.Height = 0.1875F;
			this.fldConsumptionW.Left = 2.8125F;
			this.fldConsumptionW.Name = "fldConsumptionW";
			this.fldConsumptionW.Style = "font-family: \'Tahoma\'";
			this.fldConsumptionW.Text = null;
			this.fldConsumptionW.Top = 0.75F;
			this.fldConsumptionW.Width = 0.75F;
			// 
			// fldChargeW
			// 
			this.fldChargeW.Height = 0.1875F;
			this.fldChargeW.Left = 2.8125F;
			this.fldChargeW.Name = "fldChargeW";
			this.fldChargeW.Style = "font-family: \'Tahoma\'";
			this.fldChargeW.Text = null;
			this.fldChargeW.Top = 0.9375F;
			this.fldChargeW.Width = 0.75F;
			// 
			// lblFlatChargeW
			// 
			this.lblFlatChargeW.Height = 0.1875F;
			this.lblFlatChargeW.HyperLink = null;
			this.lblFlatChargeW.Left = 0.125F;
			this.lblFlatChargeW.Name = "lblFlatChargeW";
			this.lblFlatChargeW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblFlatChargeW.Text = "Flat Rate:";
			this.lblFlatChargeW.Top = 1.125F;
			this.lblFlatChargeW.Width = 1.1875F;
			// 
			// fldFlatChargeW
			// 
			this.fldFlatChargeW.Height = 0.1875F;
			this.fldFlatChargeW.Left = 2.8125F;
			this.fldFlatChargeW.Name = "fldFlatChargeW";
			this.fldFlatChargeW.Style = "font-family: \'Tahoma\'";
			this.fldFlatChargeW.Text = null;
			this.fldFlatChargeW.Top = 1.125F;
			this.fldFlatChargeW.Width = 0.75F;
			// 
			// lblMiscChargesW
			// 
			this.lblMiscChargesW.Height = 0.1875F;
			this.lblMiscChargesW.HyperLink = null;
			this.lblMiscChargesW.Left = 0.125F;
			this.lblMiscChargesW.Name = "lblMiscChargesW";
			this.lblMiscChargesW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMiscChargesW.Text = "Misc Charges:";
			this.lblMiscChargesW.Top = 1.3125F;
			this.lblMiscChargesW.Width = 1.1875F;
			// 
			// lblMiscDesc1W
			// 
			this.lblMiscDesc1W.Height = 0.1875F;
			this.lblMiscDesc1W.HyperLink = null;
			this.lblMiscDesc1W.Left = 0.3125F;
			this.lblMiscDesc1W.Name = "lblMiscDesc1W";
			this.lblMiscDesc1W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMiscDesc1W.Text = null;
			this.lblMiscDesc1W.Top = 1.5F;
			this.lblMiscDesc1W.Width = 1.4375F;
			// 
			// lblMiscDesc2W
			// 
			this.lblMiscDesc2W.Height = 0.1875F;
			this.lblMiscDesc2W.HyperLink = null;
			this.lblMiscDesc2W.Left = 0.3125F;
			this.lblMiscDesc2W.Name = "lblMiscDesc2W";
			this.lblMiscDesc2W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMiscDesc2W.Text = null;
			this.lblMiscDesc2W.Top = 1.6875F;
			this.lblMiscDesc2W.Width = 1.4375F;
			// 
			// fldMiscAmt1W
			// 
			this.fldMiscAmt1W.Height = 0.1875F;
			this.fldMiscAmt1W.Left = 2.8125F;
			this.fldMiscAmt1W.Name = "fldMiscAmt1W";
			this.fldMiscAmt1W.Style = "font-family: \'Tahoma\'";
			this.fldMiscAmt1W.Text = null;
			this.fldMiscAmt1W.Top = 1.5F;
			this.fldMiscAmt1W.Width = 0.75F;
			// 
			// fldMiscAmt2W
			// 
			this.fldMiscAmt2W.Height = 0.1875F;
			this.fldMiscAmt2W.Left = 2.8125F;
			this.fldMiscAmt2W.Name = "fldMiscAmt2W";
			this.fldMiscAmt2W.Style = "font-family: \'Tahoma\'";
			this.fldMiscAmt2W.Text = null;
			this.fldMiscAmt2W.Top = 1.6875F;
			this.fldMiscAmt2W.Width = 0.75F;
			// 
			// fldMiscAccount1W
			// 
			this.fldMiscAccount1W.Height = 0.1875F;
			this.fldMiscAccount1W.Left = 1.75F;
			this.fldMiscAccount1W.Name = "fldMiscAccount1W";
			this.fldMiscAccount1W.Style = "font-family: \'Tahoma\'";
			this.fldMiscAccount1W.Text = null;
			this.fldMiscAccount1W.Top = 1.5F;
			this.fldMiscAccount1W.Width = 1.0625F;
			// 
			// fldMiscAccount2W
			// 
			this.fldMiscAccount2W.Height = 0.1875F;
			this.fldMiscAccount2W.Left = 1.75F;
			this.fldMiscAccount2W.Name = "fldMiscAccount2W";
			this.fldMiscAccount2W.Style = "font-family: \'Tahoma\'";
			this.fldMiscAccount2W.Text = null;
			this.fldMiscAccount2W.Top = 1.6875F;
			this.fldMiscAccount2W.Width = 1.0625F;
			// 
			// lbl1W
			// 
			this.lbl1W.Height = 0.1875F;
			this.lbl1W.HyperLink = null;
			this.lbl1W.Left = 0.3125F;
			this.lbl1W.Name = "lbl1W";
			this.lbl1W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl1W.Text = "1";
			this.lbl1W.Top = 2.3125F;
			this.lbl1W.Width = 0.375F;
			// 
			// fldThru1W
			// 
			this.fldThru1W.Height = 0.1875F;
			this.fldThru1W.Left = 1.5625F;
			this.fldThru1W.Name = "fldThru1W";
			this.fldThru1W.Style = "font-family: \'Tahoma\'";
			this.fldThru1W.Text = null;
			this.fldThru1W.Top = 2.3125F;
			this.fldThru1W.Width = 0.875F;
			// 
			// fldOver1W
			// 
			this.fldOver1W.Height = 0.1875F;
			this.fldOver1W.Left = 0.6875F;
			this.fldOver1W.Name = "fldOver1W";
			this.fldOver1W.Style = "font-family: \'Tahoma\'";
			this.fldOver1W.Text = null;
			this.fldOver1W.Top = 2.3125F;
			this.fldOver1W.Width = 0.875F;
			// 
			// lblChargeAmountW
			// 
			this.lblChargeAmountW.Height = 0.1875F;
			this.lblChargeAmountW.HyperLink = null;
			this.lblChargeAmountW.Left = 0F;
			this.lblChargeAmountW.Name = "lblChargeAmountW";
			this.lblChargeAmountW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblChargeAmountW.Text = "Charge Amount:";
			this.lblChargeAmountW.Top = 1.9375F;
			this.lblChargeAmountW.Width = 1.4375F;
			// 
			// fldStep1W
			// 
			this.fldStep1W.Height = 0.1875F;
			this.fldStep1W.Left = 3.1875F;
			this.fldStep1W.Name = "fldStep1W";
			this.fldStep1W.Style = "font-family: \'Tahoma\'";
			this.fldStep1W.Text = null;
			this.fldStep1W.Top = 2.3125F;
			this.fldStep1W.Width = 0.375F;
			// 
			// lbl2W
			// 
			this.lbl2W.Height = 0.1875F;
			this.lbl2W.HyperLink = null;
			this.lbl2W.Left = 0.3125F;
			this.lbl2W.Name = "lbl2W";
			this.lbl2W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl2W.Text = "2";
			this.lbl2W.Top = 2.5F;
			this.lbl2W.Width = 0.375F;
			// 
			// fldThru2W
			// 
			this.fldThru2W.Height = 0.1875F;
			this.fldThru2W.Left = 1.5625F;
			this.fldThru2W.Name = "fldThru2W";
			this.fldThru2W.Style = "font-family: \'Tahoma\'";
			this.fldThru2W.Text = null;
			this.fldThru2W.Top = 2.5F;
			this.fldThru2W.Width = 0.875F;
			// 
			// fldOver2W
			// 
			this.fldOver2W.Height = 0.1875F;
			this.fldOver2W.Left = 0.6875F;
			this.fldOver2W.Name = "fldOver2W";
			this.fldOver2W.Style = "font-family: \'Tahoma\'";
			this.fldOver2W.Text = null;
			this.fldOver2W.Top = 2.5F;
			this.fldOver2W.Width = 0.875F;
			// 
			// fldStep2W
			// 
			this.fldStep2W.Height = 0.1875F;
			this.fldStep2W.Left = 3.1875F;
			this.fldStep2W.Name = "fldStep2W";
			this.fldStep2W.Style = "font-family: \'Tahoma\'";
			this.fldStep2W.Text = null;
			this.fldStep2W.Top = 2.5F;
			this.fldStep2W.Width = 0.375F;
			// 
			// lbl3W
			// 
			this.lbl3W.Height = 0.1875F;
			this.lbl3W.HyperLink = null;
			this.lbl3W.Left = 0.3125F;
			this.lbl3W.Name = "lbl3W";
			this.lbl3W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl3W.Text = "3";
			this.lbl3W.Top = 2.6875F;
			this.lbl3W.Width = 0.375F;
			// 
			// fldThru3W
			// 
			this.fldThru3W.Height = 0.1875F;
			this.fldThru3W.Left = 1.5625F;
			this.fldThru3W.Name = "fldThru3W";
			this.fldThru3W.Style = "font-family: \'Tahoma\'";
			this.fldThru3W.Text = null;
			this.fldThru3W.Top = 2.6875F;
			this.fldThru3W.Width = 0.875F;
			// 
			// fldOver3W
			// 
			this.fldOver3W.Height = 0.1875F;
			this.fldOver3W.Left = 0.6875F;
			this.fldOver3W.Name = "fldOver3W";
			this.fldOver3W.Style = "font-family: \'Tahoma\'";
			this.fldOver3W.Text = null;
			this.fldOver3W.Top = 2.6875F;
			this.fldOver3W.Width = 0.875F;
			// 
			// fldStep3W
			// 
			this.fldStep3W.Height = 0.1875F;
			this.fldStep3W.Left = 3.1875F;
			this.fldStep3W.Name = "fldStep3W";
			this.fldStep3W.Style = "font-family: \'Tahoma\'";
			this.fldStep3W.Text = null;
			this.fldStep3W.Top = 2.6875F;
			this.fldStep3W.Width = 0.375F;
			// 
			// lbl4W
			// 
			this.lbl4W.Height = 0.1875F;
			this.lbl4W.HyperLink = null;
			this.lbl4W.Left = 0.3125F;
			this.lbl4W.Name = "lbl4W";
			this.lbl4W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl4W.Text = "4";
			this.lbl4W.Top = 2.875F;
			this.lbl4W.Width = 0.375F;
			// 
			// fldThru4W
			// 
			this.fldThru4W.Height = 0.1875F;
			this.fldThru4W.Left = 1.5625F;
			this.fldThru4W.Name = "fldThru4W";
			this.fldThru4W.Style = "font-family: \'Tahoma\'";
			this.fldThru4W.Text = null;
			this.fldThru4W.Top = 2.875F;
			this.fldThru4W.Width = 0.875F;
			// 
			// fldOver4W
			// 
			this.fldOver4W.Height = 0.1875F;
			this.fldOver4W.Left = 0.6875F;
			this.fldOver4W.Name = "fldOver4W";
			this.fldOver4W.Style = "font-family: \'Tahoma\'";
			this.fldOver4W.Text = null;
			this.fldOver4W.Top = 2.875F;
			this.fldOver4W.Width = 0.875F;
			// 
			// fldStep4W
			// 
			this.fldStep4W.Height = 0.1875F;
			this.fldStep4W.Left = 3.1875F;
			this.fldStep4W.Name = "fldStep4W";
			this.fldStep4W.Style = "font-family: \'Tahoma\'";
			this.fldStep4W.Text = null;
			this.fldStep4W.Top = 2.875F;
			this.fldStep4W.Width = 0.375F;
			// 
			// lbl5W
			// 
			this.lbl5W.Height = 0.1875F;
			this.lbl5W.HyperLink = null;
			this.lbl5W.Left = 0.3125F;
			this.lbl5W.Name = "lbl5W";
			this.lbl5W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl5W.Text = "5";
			this.lbl5W.Top = 3.0625F;
			this.lbl5W.Width = 0.375F;
			// 
			// fldThru5W
			// 
			this.fldThru5W.Height = 0.1875F;
			this.fldThru5W.Left = 1.5625F;
			this.fldThru5W.Name = "fldThru5W";
			this.fldThru5W.Style = "font-family: \'Tahoma\'";
			this.fldThru5W.Text = null;
			this.fldThru5W.Top = 3.0625F;
			this.fldThru5W.Width = 0.875F;
			// 
			// fldOver5W
			// 
			this.fldOver5W.Height = 0.1875F;
			this.fldOver5W.Left = 0.6875F;
			this.fldOver5W.Name = "fldOver5W";
			this.fldOver5W.Style = "font-family: \'Tahoma\'";
			this.fldOver5W.Text = null;
			this.fldOver5W.Top = 3.0625F;
			this.fldOver5W.Width = 0.875F;
			// 
			// fldStep5W
			// 
			this.fldStep5W.Height = 0.1875F;
			this.fldStep5W.Left = 3.1875F;
			this.fldStep5W.Name = "fldStep5W";
			this.fldStep5W.Style = "font-family: \'Tahoma\'";
			this.fldStep5W.Text = null;
			this.fldStep5W.Top = 3.0625F;
			this.fldStep5W.Width = 0.375F;
			// 
			// lbl6W
			// 
			this.lbl6W.Height = 0.1875F;
			this.lbl6W.HyperLink = null;
			this.lbl6W.Left = 0.3125F;
			this.lbl6W.Name = "lbl6W";
			this.lbl6W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl6W.Text = "6";
			this.lbl6W.Top = 3.25F;
			this.lbl6W.Width = 0.375F;
			// 
			// fldThru6W
			// 
			this.fldThru6W.Height = 0.1875F;
			this.fldThru6W.Left = 1.5625F;
			this.fldThru6W.Name = "fldThru6W";
			this.fldThru6W.Style = "font-family: \'Tahoma\'";
			this.fldThru6W.Text = null;
			this.fldThru6W.Top = 3.25F;
			this.fldThru6W.Width = 0.875F;
			// 
			// fldOver6W
			// 
			this.fldOver6W.Height = 0.1875F;
			this.fldOver6W.Left = 0.6875F;
			this.fldOver6W.Name = "fldOver6W";
			this.fldOver6W.Style = "font-family: \'Tahoma\'";
			this.fldOver6W.Text = null;
			this.fldOver6W.Top = 3.25F;
			this.fldOver6W.Width = 0.875F;
			// 
			// fldStep6W
			// 
			this.fldStep6W.Height = 0.1875F;
			this.fldStep6W.Left = 3.1875F;
			this.fldStep6W.Name = "fldStep6W";
			this.fldStep6W.Style = "font-family: \'Tahoma\'";
			this.fldStep6W.Text = null;
			this.fldStep6W.Top = 3.25F;
			this.fldStep6W.Width = 0.375F;
			// 
			// lbl7W
			// 
			this.lbl7W.Height = 0.1875F;
			this.lbl7W.HyperLink = null;
			this.lbl7W.Left = 0.3125F;
			this.lbl7W.Name = "lbl7W";
			this.lbl7W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl7W.Text = "7";
			this.lbl7W.Top = 3.4375F;
			this.lbl7W.Width = 0.375F;
			// 
			// fldThru7W
			// 
			this.fldThru7W.Height = 0.1875F;
			this.fldThru7W.Left = 1.5625F;
			this.fldThru7W.Name = "fldThru7W";
			this.fldThru7W.Style = "font-family: \'Tahoma\'";
			this.fldThru7W.Text = null;
			this.fldThru7W.Top = 3.4375F;
			this.fldThru7W.Width = 0.875F;
			// 
			// fldOver7W
			// 
			this.fldOver7W.Height = 0.1875F;
			this.fldOver7W.Left = 0.6875F;
			this.fldOver7W.Name = "fldOver7W";
			this.fldOver7W.Style = "font-family: \'Tahoma\'";
			this.fldOver7W.Text = null;
			this.fldOver7W.Top = 3.4375F;
			this.fldOver7W.Width = 0.875F;
			// 
			// fldStep7W
			// 
			this.fldStep7W.Height = 0.1875F;
			this.fldStep7W.Left = 3.1875F;
			this.fldStep7W.Name = "fldStep7W";
			this.fldStep7W.Style = "font-family: \'Tahoma\'";
			this.fldStep7W.Text = null;
			this.fldStep7W.Top = 3.4375F;
			this.fldStep7W.Width = 0.375F;
			// 
			// lbl8W
			// 
			this.lbl8W.Height = 0.1875F;
			this.lbl8W.HyperLink = null;
			this.lbl8W.Left = 0.3125F;
			this.lbl8W.Name = "lbl8W";
			this.lbl8W.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl8W.Text = "8";
			this.lbl8W.Top = 3.625F;
			this.lbl8W.Width = 0.375F;
			// 
			// fldThru8W
			// 
			this.fldThru8W.Height = 0.1875F;
			this.fldThru8W.Left = 1.5625F;
			this.fldThru8W.Name = "fldThru8W";
			this.fldThru8W.Style = "font-family: \'Tahoma\'";
			this.fldThru8W.Text = null;
			this.fldThru8W.Top = 3.625F;
			this.fldThru8W.Width = 0.875F;
			// 
			// fldOver8W
			// 
			this.fldOver8W.Height = 0.1875F;
			this.fldOver8W.Left = 0.6875F;
			this.fldOver8W.Name = "fldOver8W";
			this.fldOver8W.Style = "font-family: \'Tahoma\'";
			this.fldOver8W.Text = null;
			this.fldOver8W.Top = 3.625F;
			this.fldOver8W.Width = 0.875F;
			// 
			// fldStep8W
			// 
			this.fldStep8W.Height = 0.1875F;
			this.fldStep8W.Left = 3.1875F;
			this.fldStep8W.Name = "fldStep8W";
			this.fldStep8W.Style = "font-family: \'Tahoma\'";
			this.fldStep8W.Text = null;
			this.fldStep8W.Top = 3.625F;
			this.fldStep8W.Width = 0.375F;
			// 
			// lblServiceS
			// 
			this.lblServiceS.Height = 0.1875F;
			this.lblServiceS.HyperLink = null;
			this.lblServiceS.Left = 3.75F;
			this.lblServiceS.Name = "lblServiceS";
			this.lblServiceS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblServiceS.Text = "Sewer";
			this.lblServiceS.Top = 0.375F;
			this.lblServiceS.Width = 3.75F;
			// 
			// lblMinimumS
			// 
			this.lblMinimumS.Height = 0.1875F;
			this.lblMinimumS.HyperLink = null;
			this.lblMinimumS.Left = 3.875F;
			this.lblMinimumS.Name = "lblMinimumS";
			this.lblMinimumS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMinimumS.Text = "Minimum:";
			this.lblMinimumS.Top = 0.5625F;
			this.lblMinimumS.Width = 0.75F;
			// 
			// lblConsumptionS
			// 
			this.lblConsumptionS.Height = 0.1875F;
			this.lblConsumptionS.HyperLink = null;
			this.lblConsumptionS.Left = 4.0625F;
			this.lblConsumptionS.Name = "lblConsumptionS";
			this.lblConsumptionS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblConsumptionS.Text = "Consumption:";
			this.lblConsumptionS.Top = 0.75F;
			this.lblConsumptionS.Width = 1.1875F;
			// 
			// lblChargeS
			// 
			this.lblChargeS.Height = 0.1875F;
			this.lblChargeS.HyperLink = null;
			this.lblChargeS.Left = 4.0625F;
			this.lblChargeS.Name = "lblChargeS";
			this.lblChargeS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblChargeS.Text = "Charge:";
			this.lblChargeS.Top = 0.9375F;
			this.lblChargeS.Width = 1.1875F;
			// 
			// fldConsumptionS
			// 
			this.fldConsumptionS.Height = 0.1875F;
			this.fldConsumptionS.Left = 6.5625F;
			this.fldConsumptionS.Name = "fldConsumptionS";
			this.fldConsumptionS.Style = "font-family: \'Tahoma\'";
			this.fldConsumptionS.Text = null;
			this.fldConsumptionS.Top = 0.75F;
			this.fldConsumptionS.Width = 0.75F;
			// 
			// fldChargeS
			// 
			this.fldChargeS.Height = 0.1875F;
			this.fldChargeS.Left = 6.5625F;
			this.fldChargeS.Name = "fldChargeS";
			this.fldChargeS.Style = "font-family: \'Tahoma\'";
			this.fldChargeS.Text = null;
			this.fldChargeS.Top = 0.9375F;
			this.fldChargeS.Width = 0.75F;
			// 
			// lblFlatChargeS
			// 
			this.lblFlatChargeS.Height = 0.1875F;
			this.lblFlatChargeS.HyperLink = null;
			this.lblFlatChargeS.Left = 3.875F;
			this.lblFlatChargeS.Name = "lblFlatChargeS";
			this.lblFlatChargeS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblFlatChargeS.Text = "Flat Rate:";
			this.lblFlatChargeS.Top = 1.125F;
			this.lblFlatChargeS.Width = 1.1875F;
			// 
			// fldFlatChargeS
			// 
			this.fldFlatChargeS.Height = 0.1875F;
			this.fldFlatChargeS.Left = 6.5625F;
			this.fldFlatChargeS.Name = "fldFlatChargeS";
			this.fldFlatChargeS.Style = "font-family: \'Tahoma\'";
			this.fldFlatChargeS.Text = null;
			this.fldFlatChargeS.Top = 1.125F;
			this.fldFlatChargeS.Width = 0.75F;
			// 
			// lblMiscChargesS
			// 
			this.lblMiscChargesS.Height = 0.1875F;
			this.lblMiscChargesS.HyperLink = null;
			this.lblMiscChargesS.Left = 3.875F;
			this.lblMiscChargesS.Name = "lblMiscChargesS";
			this.lblMiscChargesS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMiscChargesS.Text = "Misc Charges:";
			this.lblMiscChargesS.Top = 1.3125F;
			this.lblMiscChargesS.Width = 1.1875F;
			// 
			// lblMiscDesc1S
			// 
			this.lblMiscDesc1S.Height = 0.1875F;
			this.lblMiscDesc1S.HyperLink = null;
			this.lblMiscDesc1S.Left = 4.0625F;
			this.lblMiscDesc1S.Name = "lblMiscDesc1S";
			this.lblMiscDesc1S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMiscDesc1S.Text = null;
			this.lblMiscDesc1S.Top = 1.5F;
			this.lblMiscDesc1S.Width = 1.4375F;
			// 
			// lblMiscDesc2S
			// 
			this.lblMiscDesc2S.Height = 0.1875F;
			this.lblMiscDesc2S.HyperLink = null;
			this.lblMiscDesc2S.Left = 4.0625F;
			this.lblMiscDesc2S.Name = "lblMiscDesc2S";
			this.lblMiscDesc2S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblMiscDesc2S.Text = null;
			this.lblMiscDesc2S.Top = 1.6875F;
			this.lblMiscDesc2S.Width = 1.4375F;
			// 
			// fldMiscAmt1S
			// 
			this.fldMiscAmt1S.Height = 0.1875F;
			this.fldMiscAmt1S.Left = 6.5625F;
			this.fldMiscAmt1S.Name = "fldMiscAmt1S";
			this.fldMiscAmt1S.Style = "font-family: \'Tahoma\'";
			this.fldMiscAmt1S.Text = null;
			this.fldMiscAmt1S.Top = 1.5F;
			this.fldMiscAmt1S.Width = 0.75F;
			// 
			// fldMiscAmt2S
			// 
			this.fldMiscAmt2S.Height = 0.1875F;
			this.fldMiscAmt2S.Left = 6.5625F;
			this.fldMiscAmt2S.Name = "fldMiscAmt2S";
			this.fldMiscAmt2S.Style = "font-family: \'Tahoma\'";
			this.fldMiscAmt2S.Text = null;
			this.fldMiscAmt2S.Top = 1.6875F;
			this.fldMiscAmt2S.Width = 0.75F;
			// 
			// fldMiscAccount1S
			// 
			this.fldMiscAccount1S.Height = 0.1875F;
			this.fldMiscAccount1S.Left = 5.5F;
			this.fldMiscAccount1S.Name = "fldMiscAccount1S";
			this.fldMiscAccount1S.Style = "font-family: \'Tahoma\'";
			this.fldMiscAccount1S.Text = null;
			this.fldMiscAccount1S.Top = 1.5F;
			this.fldMiscAccount1S.Width = 1.0625F;
			// 
			// fldMiscAccount2S
			// 
			this.fldMiscAccount2S.Height = 0.1875F;
			this.fldMiscAccount2S.Left = 5.5F;
			this.fldMiscAccount2S.Name = "fldMiscAccount2S";
			this.fldMiscAccount2S.Style = "font-family: \'Tahoma\'";
			this.fldMiscAccount2S.Text = null;
			this.fldMiscAccount2S.Top = 1.6875F;
			this.fldMiscAccount2S.Width = 1.0625F;
			// 
			// lbl1S
			// 
			this.lbl1S.Height = 0.1875F;
			this.lbl1S.HyperLink = null;
			this.lbl1S.Left = 4.0625F;
			this.lbl1S.Name = "lbl1S";
			this.lbl1S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl1S.Text = "1";
			this.lbl1S.Top = 2.3125F;
			this.lbl1S.Width = 0.375F;
			// 
			// fldThru1S
			// 
			this.fldThru1S.Height = 0.1875F;
			this.fldThru1S.Left = 5.3125F;
			this.fldThru1S.Name = "fldThru1S";
			this.fldThru1S.Style = "font-family: \'Tahoma\'";
			this.fldThru1S.Text = null;
			this.fldThru1S.Top = 2.3125F;
			this.fldThru1S.Width = 0.875F;
			// 
			// fldOver1S
			// 
			this.fldOver1S.Height = 0.1875F;
			this.fldOver1S.Left = 4.4375F;
			this.fldOver1S.Name = "fldOver1S";
			this.fldOver1S.Style = "font-family: \'Tahoma\'";
			this.fldOver1S.Text = null;
			this.fldOver1S.Top = 2.3125F;
			this.fldOver1S.Width = 0.875F;
			// 
			// lblChargeAmountS
			// 
			this.lblChargeAmountS.Height = 0.1875F;
			this.lblChargeAmountS.HyperLink = null;
			this.lblChargeAmountS.Left = 3.75F;
			this.lblChargeAmountS.Name = "lblChargeAmountS";
			this.lblChargeAmountS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblChargeAmountS.Text = "Charge Amount:";
			this.lblChargeAmountS.Top = 1.9375F;
			this.lblChargeAmountS.Width = 1.4375F;
			// 
			// fldStep1S
			// 
			this.fldStep1S.Height = 0.1875F;
			this.fldStep1S.Left = 6.9375F;
			this.fldStep1S.Name = "fldStep1S";
			this.fldStep1S.Style = "font-family: \'Tahoma\'";
			this.fldStep1S.Text = null;
			this.fldStep1S.Top = 2.3125F;
			this.fldStep1S.Width = 0.375F;
			// 
			// lbl2S
			// 
			this.lbl2S.Height = 0.1875F;
			this.lbl2S.HyperLink = null;
			this.lbl2S.Left = 4.0625F;
			this.lbl2S.Name = "lbl2S";
			this.lbl2S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl2S.Text = "2";
			this.lbl2S.Top = 2.5F;
			this.lbl2S.Width = 0.375F;
			// 
			// fldThru2S
			// 
			this.fldThru2S.Height = 0.1875F;
			this.fldThru2S.Left = 5.3125F;
			this.fldThru2S.Name = "fldThru2S";
			this.fldThru2S.Style = "font-family: \'Tahoma\'";
			this.fldThru2S.Text = null;
			this.fldThru2S.Top = 2.5F;
			this.fldThru2S.Width = 0.875F;
			// 
			// fldOver2S
			// 
			this.fldOver2S.Height = 0.1875F;
			this.fldOver2S.Left = 4.4375F;
			this.fldOver2S.Name = "fldOver2S";
			this.fldOver2S.Style = "font-family: \'Tahoma\'";
			this.fldOver2S.Text = null;
			this.fldOver2S.Top = 2.5F;
			this.fldOver2S.Width = 0.875F;
			// 
			// fldStep2S
			// 
			this.fldStep2S.Height = 0.1875F;
			this.fldStep2S.Left = 6.9375F;
			this.fldStep2S.Name = "fldStep2S";
			this.fldStep2S.Style = "font-family: \'Tahoma\'";
			this.fldStep2S.Text = null;
			this.fldStep2S.Top = 2.5F;
			this.fldStep2S.Width = 0.375F;
			// 
			// lbl3S
			// 
			this.lbl3S.Height = 0.1875F;
			this.lbl3S.HyperLink = null;
			this.lbl3S.Left = 4.0625F;
			this.lbl3S.Name = "lbl3S";
			this.lbl3S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl3S.Text = "3";
			this.lbl3S.Top = 2.6875F;
			this.lbl3S.Width = 0.375F;
			// 
			// fldThru3S
			// 
			this.fldThru3S.Height = 0.1875F;
			this.fldThru3S.Left = 5.3125F;
			this.fldThru3S.Name = "fldThru3S";
			this.fldThru3S.Style = "font-family: \'Tahoma\'";
			this.fldThru3S.Text = null;
			this.fldThru3S.Top = 2.6875F;
			this.fldThru3S.Width = 0.875F;
			// 
			// fldOver3S
			// 
			this.fldOver3S.Height = 0.1875F;
			this.fldOver3S.Left = 4.4375F;
			this.fldOver3S.Name = "fldOver3S";
			this.fldOver3S.Style = "font-family: \'Tahoma\'";
			this.fldOver3S.Text = null;
			this.fldOver3S.Top = 2.6875F;
			this.fldOver3S.Width = 0.875F;
			// 
			// fldStep3S
			// 
			this.fldStep3S.Height = 0.1875F;
			this.fldStep3S.Left = 6.9375F;
			this.fldStep3S.Name = "fldStep3S";
			this.fldStep3S.Style = "font-family: \'Tahoma\'";
			this.fldStep3S.Text = null;
			this.fldStep3S.Top = 2.6875F;
			this.fldStep3S.Width = 0.375F;
			// 
			// lbl4S
			// 
			this.lbl4S.Height = 0.1875F;
			this.lbl4S.HyperLink = null;
			this.lbl4S.Left = 4.0625F;
			this.lbl4S.Name = "lbl4S";
			this.lbl4S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl4S.Text = "4";
			this.lbl4S.Top = 2.875F;
			this.lbl4S.Width = 0.375F;
			// 
			// fldThru4S
			// 
			this.fldThru4S.Height = 0.1875F;
			this.fldThru4S.Left = 5.3125F;
			this.fldThru4S.Name = "fldThru4S";
			this.fldThru4S.Style = "font-family: \'Tahoma\'";
			this.fldThru4S.Text = null;
			this.fldThru4S.Top = 2.875F;
			this.fldThru4S.Width = 0.875F;
			// 
			// fldOver4S
			// 
			this.fldOver4S.Height = 0.1875F;
			this.fldOver4S.Left = 4.4375F;
			this.fldOver4S.Name = "fldOver4S";
			this.fldOver4S.Style = "font-family: \'Tahoma\'";
			this.fldOver4S.Text = null;
			this.fldOver4S.Top = 2.875F;
			this.fldOver4S.Width = 0.875F;
			// 
			// fldStep4S
			// 
			this.fldStep4S.Height = 0.1875F;
			this.fldStep4S.Left = 6.9375F;
			this.fldStep4S.Name = "fldStep4S";
			this.fldStep4S.Style = "font-family: \'Tahoma\'";
			this.fldStep4S.Text = null;
			this.fldStep4S.Top = 2.875F;
			this.fldStep4S.Width = 0.375F;
			// 
			// lbl5S
			// 
			this.lbl5S.Height = 0.1875F;
			this.lbl5S.HyperLink = null;
			this.lbl5S.Left = 4.0625F;
			this.lbl5S.Name = "lbl5S";
			this.lbl5S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl5S.Text = "5";
			this.lbl5S.Top = 3.0625F;
			this.lbl5S.Width = 0.375F;
			// 
			// fldThru5S
			// 
			this.fldThru5S.Height = 0.1875F;
			this.fldThru5S.Left = 5.3125F;
			this.fldThru5S.Name = "fldThru5S";
			this.fldThru5S.Style = "font-family: \'Tahoma\'";
			this.fldThru5S.Text = null;
			this.fldThru5S.Top = 3.0625F;
			this.fldThru5S.Width = 0.875F;
			// 
			// fldOver5S
			// 
			this.fldOver5S.Height = 0.1875F;
			this.fldOver5S.Left = 4.4375F;
			this.fldOver5S.Name = "fldOver5S";
			this.fldOver5S.Style = "font-family: \'Tahoma\'";
			this.fldOver5S.Text = null;
			this.fldOver5S.Top = 3.0625F;
			this.fldOver5S.Width = 0.875F;
			// 
			// fldStep5S
			// 
			this.fldStep5S.Height = 0.1875F;
			this.fldStep5S.Left = 6.9375F;
			this.fldStep5S.Name = "fldStep5S";
			this.fldStep5S.Style = "font-family: \'Tahoma\'";
			this.fldStep5S.Text = null;
			this.fldStep5S.Top = 3.0625F;
			this.fldStep5S.Width = 0.375F;
			// 
			// lbl6S
			// 
			this.lbl6S.Height = 0.1875F;
			this.lbl6S.HyperLink = null;
			this.lbl6S.Left = 4.0625F;
			this.lbl6S.Name = "lbl6S";
			this.lbl6S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl6S.Text = "6";
			this.lbl6S.Top = 3.25F;
			this.lbl6S.Width = 0.375F;
			// 
			// fldThru6S
			// 
			this.fldThru6S.Height = 0.1875F;
			this.fldThru6S.Left = 5.3125F;
			this.fldThru6S.Name = "fldThru6S";
			this.fldThru6S.Style = "font-family: \'Tahoma\'";
			this.fldThru6S.Text = null;
			this.fldThru6S.Top = 3.25F;
			this.fldThru6S.Width = 0.875F;
			// 
			// fldOver6S
			// 
			this.fldOver6S.Height = 0.1875F;
			this.fldOver6S.Left = 4.4375F;
			this.fldOver6S.Name = "fldOver6S";
			this.fldOver6S.Style = "font-family: \'Tahoma\'";
			this.fldOver6S.Text = null;
			this.fldOver6S.Top = 3.25F;
			this.fldOver6S.Width = 0.875F;
			// 
			// fldStep6S
			// 
			this.fldStep6S.Height = 0.1875F;
			this.fldStep6S.Left = 6.9375F;
			this.fldStep6S.Name = "fldStep6S";
			this.fldStep6S.Style = "font-family: \'Tahoma\'";
			this.fldStep6S.Text = null;
			this.fldStep6S.Top = 3.25F;
			this.fldStep6S.Width = 0.375F;
			// 
			// lbl7S
			// 
			this.lbl7S.Height = 0.1875F;
			this.lbl7S.HyperLink = null;
			this.lbl7S.Left = 4.0625F;
			this.lbl7S.Name = "lbl7S";
			this.lbl7S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl7S.Text = "7";
			this.lbl7S.Top = 3.4375F;
			this.lbl7S.Width = 0.375F;
			// 
			// fldThru7S
			// 
			this.fldThru7S.Height = 0.1875F;
			this.fldThru7S.Left = 5.3125F;
			this.fldThru7S.Name = "fldThru7S";
			this.fldThru7S.Style = "font-family: \'Tahoma\'";
			this.fldThru7S.Text = null;
			this.fldThru7S.Top = 3.4375F;
			this.fldThru7S.Width = 0.875F;
			// 
			// fldOver7S
			// 
			this.fldOver7S.Height = 0.1875F;
			this.fldOver7S.Left = 4.4375F;
			this.fldOver7S.Name = "fldOver7S";
			this.fldOver7S.Style = "font-family: \'Tahoma\'";
			this.fldOver7S.Text = null;
			this.fldOver7S.Top = 3.4375F;
			this.fldOver7S.Width = 0.875F;
			// 
			// fldStep7S
			// 
			this.fldStep7S.Height = 0.1875F;
			this.fldStep7S.Left = 6.9375F;
			this.fldStep7S.Name = "fldStep7S";
			this.fldStep7S.Style = "font-family: \'Tahoma\'";
			this.fldStep7S.Text = null;
			this.fldStep7S.Top = 3.4375F;
			this.fldStep7S.Width = 0.375F;
			// 
			// lbl8S
			// 
			this.lbl8S.Height = 0.1875F;
			this.lbl8S.HyperLink = null;
			this.lbl8S.Left = 4.0625F;
			this.lbl8S.Name = "lbl8S";
			this.lbl8S.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lbl8S.Text = "8";
			this.lbl8S.Top = 3.625F;
			this.lbl8S.Width = 0.375F;
			// 
			// fldThru8S
			// 
			this.fldThru8S.Height = 0.1875F;
			this.fldThru8S.Left = 5.3125F;
			this.fldThru8S.Name = "fldThru8S";
			this.fldThru8S.Style = "font-family: \'Tahoma\'";
			this.fldThru8S.Text = null;
			this.fldThru8S.Top = 3.625F;
			this.fldThru8S.Width = 0.875F;
			// 
			// fldOver8S
			// 
			this.fldOver8S.Height = 0.1875F;
			this.fldOver8S.Left = 4.4375F;
			this.fldOver8S.Name = "fldOver8S";
			this.fldOver8S.Style = "font-family: \'Tahoma\'";
			this.fldOver8S.Text = null;
			this.fldOver8S.Top = 3.625F;
			this.fldOver8S.Width = 0.875F;
			// 
			// fldStep8S
			// 
			this.fldStep8S.Height = 0.1875F;
			this.fldStep8S.Left = 6.9375F;
			this.fldStep8S.Name = "fldStep8S";
			this.fldStep8S.Style = "font-family: \'Tahoma\'";
			this.fldStep8S.Text = null;
			this.fldStep8S.Top = 3.625F;
			this.fldStep8S.Width = 0.375F;
			// 
			// Line2
			// 
			this.Line2.Height = 3.4375F;
			this.Line2.Left = 3.75F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.375F;
			this.Line2.Width = 0F;
			this.Line2.X1 = 3.75F;
			this.Line2.X2 = 3.75F;
			this.Line2.Y1 = 0.375F;
			this.Line2.Y2 = 3.8125F;
			// 
			// lblOverW
			// 
			this.lblOverW.Height = 0.1875F;
			this.lblOverW.HyperLink = null;
			this.lblOverW.Left = 0.6875F;
			this.lblOverW.Name = "lblOverW";
			this.lblOverW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblOverW.Text = "Over";
			this.lblOverW.Top = 2.125F;
			this.lblOverW.Width = 0.875F;
			// 
			// lblThruW
			// 
			this.lblThruW.Height = 0.1875F;
			this.lblThruW.HyperLink = null;
			this.lblThruW.Left = 1.5625F;
			this.lblThruW.Name = "lblThruW";
			this.lblThruW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblThruW.Text = "Thru";
			this.lblThruW.Top = 2.125F;
			this.lblThruW.Width = 0.875F;
			// 
			// lblAmtW
			// 
			this.lblAmtW.Height = 0.1875F;
			this.lblAmtW.HyperLink = null;
			this.lblAmtW.Left = 2.4375F;
			this.lblAmtW.Name = "lblAmtW";
			this.lblAmtW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblAmtW.Text = "Amount";
			this.lblAmtW.Top = 2.125F;
			this.lblAmtW.Width = 0.75F;
			// 
			// lblStepW
			// 
			this.lblStepW.Height = 0.1875F;
			this.lblStepW.HyperLink = null;
			this.lblStepW.Left = 3.1875F;
			this.lblStepW.Name = "lblStepW";
			this.lblStepW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblStepW.Text = "Step";
			this.lblStepW.Top = 2.125F;
			this.lblStepW.Width = 0.375F;
			// 
			// lblOverS
			// 
			this.lblOverS.Height = 0.1875F;
			this.lblOverS.HyperLink = null;
			this.lblOverS.Left = 4.4375F;
			this.lblOverS.Name = "lblOverS";
			this.lblOverS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblOverS.Text = "Over";
			this.lblOverS.Top = 2.125F;
			this.lblOverS.Width = 0.875F;
			// 
			// lblThruS
			// 
			this.lblThruS.Height = 0.1875F;
			this.lblThruS.HyperLink = null;
			this.lblThruS.Left = 5.3125F;
			this.lblThruS.Name = "lblThruS";
			this.lblThruS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblThruS.Text = "Thru";
			this.lblThruS.Top = 2.125F;
			this.lblThruS.Width = 0.875F;
			// 
			// lblAmtS
			// 
			this.lblAmtS.Height = 0.1875F;
			this.lblAmtS.HyperLink = null;
			this.lblAmtS.Left = 6.1875F;
			this.lblAmtS.Name = "lblAmtS";
			this.lblAmtS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblAmtS.Text = "Amount";
			this.lblAmtS.Top = 2.125F;
			this.lblAmtS.Width = 0.75F;
			// 
			// lblStepS
			// 
			this.lblStepS.Height = 0.1875F;
			this.lblStepS.HyperLink = null;
			this.lblStepS.Left = 6.9375F;
			this.lblStepS.Name = "lblStepS";
			this.lblStepS.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblStepS.Text = "Step";
			this.lblStepS.Top = 2.125F;
			this.lblStepS.Width = 0.375F;
			// 
			// fldAmount1W
			// 
			this.fldAmount1W.Height = 0.1875F;
			this.fldAmount1W.Left = 2.4375F;
			this.fldAmount1W.Name = "fldAmount1W";
			this.fldAmount1W.Style = "font-family: \'Tahoma\'";
			this.fldAmount1W.Text = null;
			this.fldAmount1W.Top = 2.3125F;
			this.fldAmount1W.Width = 0.75F;
			// 
			// fldAmount2W
			// 
			this.fldAmount2W.Height = 0.1875F;
			this.fldAmount2W.Left = 2.4375F;
			this.fldAmount2W.Name = "fldAmount2W";
			this.fldAmount2W.Style = "font-family: \'Tahoma\'";
			this.fldAmount2W.Text = null;
			this.fldAmount2W.Top = 2.5F;
			this.fldAmount2W.Width = 0.75F;
			// 
			// fldAmount3W
			// 
			this.fldAmount3W.Height = 0.1875F;
			this.fldAmount3W.Left = 2.4375F;
			this.fldAmount3W.Name = "fldAmount3W";
			this.fldAmount3W.Style = "font-family: \'Tahoma\'";
			this.fldAmount3W.Text = null;
			this.fldAmount3W.Top = 2.6875F;
			this.fldAmount3W.Width = 0.75F;
			// 
			// fldAmount4W
			// 
			this.fldAmount4W.Height = 0.1875F;
			this.fldAmount4W.Left = 2.4375F;
			this.fldAmount4W.Name = "fldAmount4W";
			this.fldAmount4W.Style = "font-family: \'Tahoma\'";
			this.fldAmount4W.Text = null;
			this.fldAmount4W.Top = 2.875F;
			this.fldAmount4W.Width = 0.75F;
			// 
			// fldAmount5W
			// 
			this.fldAmount5W.Height = 0.1875F;
			this.fldAmount5W.Left = 2.4375F;
			this.fldAmount5W.Name = "fldAmount5W";
			this.fldAmount5W.Style = "font-family: \'Tahoma\'";
			this.fldAmount5W.Text = null;
			this.fldAmount5W.Top = 3.0625F;
			this.fldAmount5W.Width = 0.75F;
			// 
			// fldAmount6W
			// 
			this.fldAmount6W.Height = 0.1875F;
			this.fldAmount6W.Left = 2.4375F;
			this.fldAmount6W.Name = "fldAmount6W";
			this.fldAmount6W.Style = "font-family: \'Tahoma\'";
			this.fldAmount6W.Text = null;
			this.fldAmount6W.Top = 3.25F;
			this.fldAmount6W.Width = 0.75F;
			// 
			// fldAmount7W
			// 
			this.fldAmount7W.Height = 0.1875F;
			this.fldAmount7W.Left = 2.4375F;
			this.fldAmount7W.Name = "fldAmount7W";
			this.fldAmount7W.Style = "font-family: \'Tahoma\'";
			this.fldAmount7W.Text = null;
			this.fldAmount7W.Top = 3.4375F;
			this.fldAmount7W.Width = 0.75F;
			// 
			// fldAmount8W
			// 
			this.fldAmount8W.Height = 0.1875F;
			this.fldAmount8W.Left = 2.4375F;
			this.fldAmount8W.Name = "fldAmount8W";
			this.fldAmount8W.Style = "font-family: \'Tahoma\'";
			this.fldAmount8W.Text = null;
			this.fldAmount8W.Top = 3.625F;
			this.fldAmount8W.Width = 0.75F;
			// 
			// fldAmount1S
			// 
			this.fldAmount1S.Height = 0.1875F;
			this.fldAmount1S.Left = 6.1875F;
			this.fldAmount1S.Name = "fldAmount1S";
			this.fldAmount1S.Style = "font-family: \'Tahoma\'";
			this.fldAmount1S.Text = null;
			this.fldAmount1S.Top = 2.3125F;
			this.fldAmount1S.Width = 0.75F;
			// 
			// fldAmount2S
			// 
			this.fldAmount2S.Height = 0.1875F;
			this.fldAmount2S.Left = 6.1875F;
			this.fldAmount2S.Name = "fldAmount2S";
			this.fldAmount2S.Style = "font-family: \'Tahoma\'";
			this.fldAmount2S.Text = null;
			this.fldAmount2S.Top = 2.5F;
			this.fldAmount2S.Width = 0.75F;
			// 
			// fldAmount3S
			// 
			this.fldAmount3S.Height = 0.1875F;
			this.fldAmount3S.Left = 6.1875F;
			this.fldAmount3S.Name = "fldAmount3S";
			this.fldAmount3S.Style = "font-family: \'Tahoma\'";
			this.fldAmount3S.Text = null;
			this.fldAmount3S.Top = 2.6875F;
			this.fldAmount3S.Width = 0.75F;
			// 
			// fldAmount4S
			// 
			this.fldAmount4S.Height = 0.1875F;
			this.fldAmount4S.Left = 6.1875F;
			this.fldAmount4S.Name = "fldAmount4S";
			this.fldAmount4S.Style = "font-family: \'Tahoma\'";
			this.fldAmount4S.Text = null;
			this.fldAmount4S.Top = 2.875F;
			this.fldAmount4S.Width = 0.75F;
			// 
			// fldAmount5S
			// 
			this.fldAmount5S.Height = 0.1875F;
			this.fldAmount5S.Left = 6.1875F;
			this.fldAmount5S.Name = "fldAmount5S";
			this.fldAmount5S.Style = "font-family: \'Tahoma\'";
			this.fldAmount5S.Text = null;
			this.fldAmount5S.Top = 3.0625F;
			this.fldAmount5S.Width = 0.75F;
			// 
			// fldAmount6S
			// 
			this.fldAmount6S.Height = 0.1875F;
			this.fldAmount6S.Left = 6.1875F;
			this.fldAmount6S.Name = "fldAmount6S";
			this.fldAmount6S.Style = "font-family: \'Tahoma\'";
			this.fldAmount6S.Text = null;
			this.fldAmount6S.Top = 3.25F;
			this.fldAmount6S.Width = 0.75F;
			// 
			// fldAmount7S
			// 
			this.fldAmount7S.Height = 0.1875F;
			this.fldAmount7S.Left = 6.1875F;
			this.fldAmount7S.Name = "fldAmount7S";
			this.fldAmount7S.Style = "font-family: \'Tahoma\'";
			this.fldAmount7S.Text = null;
			this.fldAmount7S.Top = 3.4375F;
			this.fldAmount7S.Width = 0.75F;
			// 
			// fldAmount8S
			// 
			this.fldAmount8S.Height = 0.1875F;
			this.fldAmount8S.Left = 6.1875F;
			this.fldAmount8S.Name = "fldAmount8S";
			this.fldAmount8S.Style = "font-family: \'Tahoma\'";
			this.fldAmount8S.Text = null;
			this.fldAmount8S.Top = 3.625F;
			this.fldAmount8S.Width = 0.75F;
			// 
			// rptRateTables
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateTableDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRateTableNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateTable)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblServiceW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMinimumW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsumptionW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChargeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumptionW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldChargeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFlatChargeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFlatChargeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscChargesW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscDesc1W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscDesc2W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAmt1W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAmt2W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAccount1W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAccount2W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl1W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru1W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver1W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChargeAmountW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep1W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl2W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru2W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver2W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep2W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl3W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru3W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver3W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep3W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl4W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru4W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver4W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep4W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl5W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru5W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver5W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep5W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl6W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru6W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver6W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep6W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl7W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru7W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver7W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep7W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl8W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru8W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver8W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep8W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblServiceS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMinimumS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsumptionS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChargeS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumptionS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldChargeS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFlatChargeS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFlatChargeS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscChargesS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscDesc1S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscDesc2S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAmt1S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAmt2S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAccount1S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscAccount2S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl1S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru1S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver1S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblChargeAmountS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep1S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl2S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru2S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver2S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep2S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl3S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru3S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver3S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep3S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl4S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru4S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver4S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep4S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl5S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru5S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver5S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep5S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl6S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru6S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver6S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep6S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl7S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru7S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver7S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep7S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl8S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldThru8S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOver8S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStep8S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblThruW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmtW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStepW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblThruS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmtS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStepS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount1W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount3W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount4W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount5W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount6W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount7W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount8W)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount1S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount2S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount3S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount4S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount5S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount6S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount7S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount8S)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRateTableDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRateTableNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRateTable;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblServiceW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMinimumW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblConsumptionW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblChargeW;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConsumptionW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldChargeW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFlatChargeW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFlatChargeW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMiscChargesW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMiscDesc1W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMiscDesc2W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscAmt1W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscAmt2W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscAccount1W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscAccount2W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl1W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru1W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver1W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblChargeAmountW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep1W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl2W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru2W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver2W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep2W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl3W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru3W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver3W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep3W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl4W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru4W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver4W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep4W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl5W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru5W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver5W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep5W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl6W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru6W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver6W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep6W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl7W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru7W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver7W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep7W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl8W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru8W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver8W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep8W;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblServiceS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMinimumS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblConsumptionS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblChargeS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConsumptionS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldChargeS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFlatChargeS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFlatChargeS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMiscChargesS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMiscDesc1S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMiscDesc2S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscAmt1S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscAmt2S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscAccount1S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscAccount2S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl1S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru1S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver1S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblChargeAmountS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep1S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl2S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru2S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver2S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep2S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl3S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru3S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver3S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep3S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl4S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru4S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver4S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep4S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl5S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru5S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver5S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep5S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl6S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru6S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver6S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep6S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl7S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru7S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver7S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep7S;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl8S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldThru8S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOver8S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStep8S;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOverW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblThruW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmtW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStepW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOverS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblThruS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmtS;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStepS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount1W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount2W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount3W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount4W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount5W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount6W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount7W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount8W;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount1S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount2S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount3S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount4S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount5S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount6S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount7S;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount8S;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
