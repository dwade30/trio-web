﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptAccountListingMDetail.
	partial class srptAccountListingMDetail
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptAccountListingMDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblCombine = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSequence = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblService = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFinalBill = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNoBill = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSize = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblPrevious = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBackflow = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrevious = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCombine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurrent = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldService = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNoBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSize = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSRT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWRT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSRT = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWRT = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSBT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWBT = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSBT = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWBT = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBackflow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblCombine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblService)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBackflow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevious)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCombine)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldService)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSRT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWRT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSRT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWRT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSBT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWBT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSBT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWBT)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBackflow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldSequence,
				this.fldPrevious,
				this.fldCombine,
				this.fldCurrent,
				this.fldService,
				this.fldFinalBill,
				this.fldNoBill,
				this.fldSize,
				this.fldSRT,
				this.fldWRT,
				this.lblSRT,
				this.lblWRT,
				this.fldSBT,
				this.fldWBT,
				this.lblSBT,
				this.lblWBT,
				this.fldBackflow
			});
			this.Detail.Height = 0.9375F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblCombine,
				this.lblSequence,
				this.lblService,
				this.lblFinalBill,
				this.lblNoBill,
				this.lblSize,
				this.Line1,
				this.lblPrevious,
				this.lblCurrent,
				this.lblBackflow
			});
			this.GroupHeader1.Height = 0.2916667F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblCombine
			// 
			this.lblCombine.Height = 0.1875F;
			this.lblCombine.HyperLink = null;
			this.lblCombine.Left = 0.5625F;
			this.lblCombine.Name = "lblCombine";
			this.lblCombine.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblCombine.Text = "Cmb";
			this.lblCombine.Top = 0.0625F;
			this.lblCombine.Width = 0.375F;
			// 
			// lblSequence
			// 
			this.lblSequence.Height = 0.1875F;
			this.lblSequence.HyperLink = null;
			this.lblSequence.Left = 0F;
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblSequence.Text = "Seq";
			this.lblSequence.Top = 0.0625F;
			this.lblSequence.Width = 0.5625F;
			// 
			// lblService
			// 
			this.lblService.Height = 0.1875F;
			this.lblService.HyperLink = null;
			this.lblService.Left = 2.5625F;
			this.lblService.Name = "lblService";
			this.lblService.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblService.Text = "Svc";
			this.lblService.Top = 0.0625F;
			this.lblService.Width = 0.375F;
			// 
			// lblFinalBill
			// 
			this.lblFinalBill.Height = 0.1875F;
			this.lblFinalBill.HyperLink = null;
			this.lblFinalBill.Left = 2.9375F;
			this.lblFinalBill.Name = "lblFinalBill";
			this.lblFinalBill.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblFinalBill.Text = "F";
			this.lblFinalBill.Top = 0.0625F;
			this.lblFinalBill.Width = 0.25F;
			// 
			// lblNoBill
			// 
			this.lblNoBill.Height = 0.1875F;
			this.lblNoBill.HyperLink = null;
			this.lblNoBill.Left = 3.1875F;
			this.lblNoBill.Name = "lblNoBill";
			this.lblNoBill.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblNoBill.Text = "N";
			this.lblNoBill.Top = 0.0625F;
			this.lblNoBill.Width = 0.25F;
			// 
			// lblSize
			// 
			this.lblSize.Height = 0.1875F;
			this.lblSize.HyperLink = null;
			this.lblSize.Left = 3.4375F;
			this.lblSize.Name = "lblSize";
			this.lblSize.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblSize.Text = "Size";
			this.lblSize.Top = 0.0625F;
			this.lblSize.Width = 0.375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.25F;
			this.Line1.Width = 4F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 4F;
			this.Line1.Y1 = 0.25F;
			this.Line1.Y2 = 0.25F;
			// 
			// lblPrevious
			// 
			this.lblPrevious.Height = 0.1875F;
			this.lblPrevious.HyperLink = null;
			this.lblPrevious.Left = 0.9375F;
			this.lblPrevious.Name = "lblPrevious";
			this.lblPrevious.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblPrevious.Text = "Prev:";
			this.lblPrevious.Top = 0.0625F;
			this.lblPrevious.Width = 0.4375F;
			// 
			// lblCurrent
			// 
			this.lblCurrent.Height = 0.1875F;
			this.lblCurrent.HyperLink = null;
			this.lblCurrent.Left = 1.75F;
			this.lblCurrent.Name = "lblCurrent";
			this.lblCurrent.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblCurrent.Text = "Cur:";
			this.lblCurrent.Top = 0.0625F;
			this.lblCurrent.Width = 0.375F;
			// 
			// lblBackflow
			// 
			this.lblBackflow.Height = 0.1875F;
			this.lblBackflow.HyperLink = null;
			this.lblBackflow.Left = 3.8125F;
			this.lblBackflow.Name = "lblBackflow";
			this.lblBackflow.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; ddo-char-set: 0";
			this.lblBackflow.Text = "B";
			this.lblBackflow.Top = 0.0625F;
			this.lblBackflow.Width = 0.1875F;
			// 
			// fldSequence
			// 
			this.fldSequence.Height = 0.1875F;
			this.fldSequence.Left = 0F;
			this.fldSequence.Name = "fldSequence";
			this.fldSequence.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldSequence.Text = null;
			this.fldSequence.Top = 0F;
			this.fldSequence.Width = 0.5625F;
			// 
			// fldPrevious
			// 
			this.fldPrevious.Height = 0.1875F;
			this.fldPrevious.Left = 0.9375F;
			this.fldPrevious.Name = "fldPrevious";
			this.fldPrevious.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldPrevious.Text = null;
			this.fldPrevious.Top = 0F;
			this.fldPrevious.Width = 0.8125F;
			// 
			// fldCombine
			// 
			this.fldCombine.Height = 0.1875F;
			this.fldCombine.Left = 0.5625F;
			this.fldCombine.Name = "fldCombine";
			this.fldCombine.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldCombine.Text = null;
			this.fldCombine.Top = 0F;
			this.fldCombine.Width = 0.375F;
			// 
			// fldCurrent
			// 
			this.fldCurrent.Height = 0.1875F;
			this.fldCurrent.Left = 1.75F;
			this.fldCurrent.Name = "fldCurrent";
			this.fldCurrent.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldCurrent.Text = null;
			this.fldCurrent.Top = 0F;
			this.fldCurrent.Width = 0.8125F;
			// 
			// fldService
			// 
			this.fldService.Height = 0.1875F;
			this.fldService.Left = 2.5625F;
			this.fldService.Name = "fldService";
			this.fldService.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.fldService.Text = null;
			this.fldService.Top = 0F;
			this.fldService.Width = 0.375F;
			// 
			// fldFinalBill
			// 
			this.fldFinalBill.Height = 0.1875F;
			this.fldFinalBill.Left = 2.9375F;
			this.fldFinalBill.Name = "fldFinalBill";
			this.fldFinalBill.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.fldFinalBill.Text = null;
			this.fldFinalBill.Top = 0F;
			this.fldFinalBill.Width = 0.25F;
			// 
			// fldNoBill
			// 
			this.fldNoBill.Height = 0.1875F;
			this.fldNoBill.Left = 3.1875F;
			this.fldNoBill.Name = "fldNoBill";
			this.fldNoBill.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.fldNoBill.Text = null;
			this.fldNoBill.Top = 0F;
			this.fldNoBill.Width = 0.25F;
			// 
			// fldSize
			// 
			this.fldSize.Height = 0.1875F;
			this.fldSize.Left = 3.4375F;
			this.fldSize.Name = "fldSize";
			this.fldSize.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldSize.Text = null;
			this.fldSize.Top = 0F;
			this.fldSize.Width = 0.375F;
			// 
			// fldSRT
			// 
			this.fldSRT.Height = 0.1875F;
			this.fldSRT.Left = 2F;
			this.fldSRT.Name = "fldSRT";
			this.fldSRT.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldSRT.Text = null;
			this.fldSRT.Top = 0.1875F;
			this.fldSRT.Visible = false;
			this.fldSRT.Width = 1.875F;
			// 
			// fldWRT
			// 
			this.fldWRT.Height = 0.1875F;
			this.fldWRT.Left = 2F;
			this.fldWRT.Name = "fldWRT";
			this.fldWRT.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldWRT.Text = null;
			this.fldWRT.Top = 0.375F;
			this.fldWRT.Visible = false;
			this.fldWRT.Width = 1.875F;
			// 
			// lblSRT
			// 
			this.lblSRT.Height = 0.1875F;
			this.lblSRT.HyperLink = null;
			this.lblSRT.Left = 1.1875F;
			this.lblSRT.Name = "lblSRT";
			this.lblSRT.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.lblSRT.Text = "S Rates";
			this.lblSRT.Top = 0.1875F;
			this.lblSRT.Visible = false;
			this.lblSRT.Width = 0.8125F;
			// 
			// lblWRT
			// 
			this.lblWRT.Height = 0.1875F;
			this.lblWRT.HyperLink = null;
			this.lblWRT.Left = 1.1875F;
			this.lblWRT.Name = "lblWRT";
			this.lblWRT.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.lblWRT.Text = "W Rates";
			this.lblWRT.Top = 0.375F;
			this.lblWRT.Visible = false;
			this.lblWRT.Width = 0.8125F;
			// 
			// fldSBT
			// 
			this.fldSBT.Height = 0.1875F;
			this.fldSBT.Left = 2F;
			this.fldSBT.Name = "fldSBT";
			this.fldSBT.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldSBT.Text = null;
			this.fldSBT.Top = 0.5625F;
			this.fldSBT.Visible = false;
			this.fldSBT.Width = 1.875F;
			// 
			// fldWBT
			// 
			this.fldWBT.Height = 0.1875F;
			this.fldWBT.Left = 2F;
			this.fldWBT.Name = "fldWBT";
			this.fldWBT.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldWBT.Text = null;
			this.fldWBT.Top = 0.75F;
			this.fldWBT.Visible = false;
			this.fldWBT.Width = 1.875F;
			// 
			// lblSBT
			// 
			this.lblSBT.Height = 0.1875F;
			this.lblSBT.HyperLink = null;
			this.lblSBT.Left = 1.1875F;
			this.lblSBT.Name = "lblSBT";
			this.lblSBT.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.lblSBT.Text = "S Types";
			this.lblSBT.Top = 0.5625F;
			this.lblSBT.Visible = false;
			this.lblSBT.Width = 0.8125F;
			// 
			// lblWBT
			// 
			this.lblWBT.Height = 0.1875F;
			this.lblWBT.HyperLink = null;
			this.lblWBT.Left = 1.1875F;
			this.lblWBT.Name = "lblWBT";
			this.lblWBT.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; ddo-char-set: 0";
			this.lblWBT.Text = "W Types";
			this.lblWBT.Top = 0.75F;
			this.lblWBT.Visible = false;
			this.lblWBT.Width = 0.8125F;
			// 
			// fldBackflow
			// 
			this.fldBackflow.Height = 0.1875F;
			this.fldBackflow.Left = 3.8125F;
			this.fldBackflow.Name = "fldBackflow";
			this.fldBackflow.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.fldBackflow.Text = null;
			this.fldBackflow.Top = 0F;
			this.fldBackflow.Width = 0.1875F;
			// 
			// srptAccountListingMDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 4F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblCombine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblService)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBackflow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrevious)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCombine)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldService)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSRT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWRT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSRT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWRT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSBT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWBT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSBT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWBT)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBackflow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrevious;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCombine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldService;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSize;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRT;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSRT;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWRT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSBT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWBT;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSBT;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWBT;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBackflow;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCombine;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSequence;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblService;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFinalBill;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNoBill;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSize;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrevious;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBackflow;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
