﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmSetupDisconnectEditReport.
	/// </summary>
	public partial class frmSetupDisconnectEditReport : BaseForm
	{
		public frmSetupDisconnectEditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupDisconnectEditReport InstancePtr
		{
			get
			{
				return (frmSetupDisconnectEditReport)Sys.GetInstance(typeof(frmSetupDisconnectEditReport));
			}
		}

		protected frmSetupDisconnectEditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               07/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               05/19/2006              *
		// ********************************************************
		public int lngColQuestion;
		public int lngColHidden;
		public int lngColData;
		public int lngRowRateKey;
		public string strRateKeyList = string.Empty;

		private void FormatGrid_2(bool boolResize)
		{
			FormatGrid(boolResize);
		}

		private void FormatGrid(bool boolResize = false)
		{
			int lngWid = 0;
			// This will size and format the grid
			vsDefaults.FixedRows = 0;
			vsDefaults.Rows = 1;
			vsDefaults.Cols = 3;
			lngWid = vsDefaults.WidthOriginal;
			vsDefaults.ColWidth(lngColQuestion, FCConvert.ToInt32(lngWid * 0.49));
			vsDefaults.ColWidth(lngColHidden, 0);
			vsDefaults.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.49));
			if (boolResize)
			{
				return;
			}
			vsDefaults.TextMatrix(lngRowRateKey, lngColQuestion, "Outstanding Rate Key");
			// ", between the hours of 8 a.m. and 3 p.m.
			SetDefaults();
			strRateKeyList = BuildRateKeyList();
		}

		private void frmSetupDisconnectEditReport_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupDisconnectEditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupDisconnectEditReport properties;
			//frmSetupDisconnectEditReport.FillStyle	= 0;
			//frmSetupDisconnectEditReport.ScaleWidth	= 5880;
			//frmSetupDisconnectEditReport.ScaleHeight	= 4980;
			//frmSetupDisconnectEditReport.LinkTopic	= "Form2";
			//frmSetupDisconnectEditReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			lngColQuestion = 0;
			lngColHidden = 1;
			lngColData = 2;
			lngRowRateKey = 0;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			FormatGrid();
		}

		private void frmSetupDisconnectEditReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSetupDisconnectEditReport_Resize(object sender, System.EventArgs e)
		{
			FormatGrid_2(true);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int[] lngFake = null;
			vsDefaults.Select(0, 0);
			if (!Information.IsDate(txtNoticeDate.Text))
			{
				MessageBox.Show("You must enter a valid notice date before you may continue.", "Invalid Notice Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtNoticeDate.Focus();
				return;
			}
			else if (DateAndTime.DateValue(txtNoticeDate.Text).ToOADate() < DateTime.Today.ToOADate())
			{
				MessageBox.Show("You must enter a notice date that is not earlier than todays date before you may continue.", "Invalid Notice Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtNoticeDate.Focus();
				return;
			}
			if (!Information.IsDate(txtShutOffDate.Text))
			{
				MessageBox.Show("You must enter a valid shutt off date before you may continue.", "Invalid Shut Off Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtShutOffDate.Focus();
				return;
			}
			else if (DateAndTime.DateValue(txtShutOffDate.Text).ToOADate() < DateTime.Today.ToOADate())
			{
				MessageBox.Show("You must enter a shut off date that is not earlier than todays date before you may continue.", "Invalid Shut Off Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
				txtShutOffDate.Focus();
				return;
			}
			// MAL@20080715: Add minimum amount
			// Tracker Reference: 12569
			if (txtMinimumAmt.Text == "")
			{
				txtMinimumAmt.Text = "0";
			}
			SaveDefaults();
			if (cmbSelected.SelectedIndex == 0)
			{
				rptDisconnectEditReport.InstancePtr.Init("A", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), 0, ref lngFake, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))), Conversion.Val(txtMinimumAmt.Text));
			}
			else
			{
				frmBookChoice.InstancePtr.Init(17, 0, false, FCConvert.ToString(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));
			}
			this.Hide();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedBooks.Enabled = false;
			txtSequenceNumber.Text = "";
		}

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedBooks.Enabled = true;
			txtSequenceNumber.Focus();
		}

		private string BuildRateKeyList()
		{
			string BuildRateKeyList = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRK = new clsDRWrapper();
				rsRK.OpenRecordset("SELECT ID, BillDate, Description FROM RateKeys WHERE RateType = 'R' ORDER BY ID desc", modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Loading Rate Keys", true, rsRK.RecordCount(), true);
				while (!rsRK.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					BuildRateKeyList = BuildRateKeyList + rsRK.Get_Fields_Int32("ID") + "\t" + Strings.Format(rsRK.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "\t" + rsRK.Get_Fields_String("Description") + "|";
					rsRK.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				return BuildRateKeyList;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building Rate Key", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildRateKeyList;
		}

		private void SetDefaults()
		{
			string strTemp = "";
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectRateKey", ref strTemp);
			vsDefaults.TextMatrix(lngRowRateKey, lngColData, strTemp);
		}

		private void SaveDefaults()
		{
			string strTemp = "";
			strTemp = vsDefaults.TextMatrix(lngRowRateKey, lngColData);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectRateKey", strTemp);
		}

		private void vsDefaults_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsDefaults.ComboList = "";
			if (vsDefaults.Col == lngColData)
			{
				if (vsDefaults.Row == lngRowRateKey)
				{
					vsDefaults.ComboList = strRateKeyList;
				}
			}
		}

		private void vsDefaults_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsDefaults[e.ColumnIndex, e.RowIndex];
            int lngMR;
			lngMR = vsDefaults.GetFlexRowIndex(e.RowIndex);
			if (lngMR == lngRowRateKey)
			{
				//ToolTip1.SetToolTip(vsDefaults, "The is the newest rate key to check.");
				cell.ToolTipText = "The is the newest rate key to check.";
			}
			else
			{
                //ToolTip1.SetToolTip(vsDefaults, "");
                cell.ToolTipText = "";
			}
		}

		private void vsDefaults_RowColChange(object sender, System.EventArgs e)
		{
			if (vsDefaults.Col == lngColData)
			{
				vsDefaults.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsDefaults.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void cmbSelected_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbSelected.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbSelected.Text == "Selected Books")
			{
				optSelected_CheckedChanged(sender, e);
			}
		}
	}
}
