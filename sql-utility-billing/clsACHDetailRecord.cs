//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class clsACHDetailRecord
	{

		//=========================================================

		public enum ACHDetailEntryType
		{
			ACHDetailEntryTypeCredit = 1,
			ACHDetailEntryTypeDebit = 2
		};

		public enum ACHDetailAcctType
		{
			ACHDetailAccountTypeChecking = 2,
			ACHDetailAccountTypeSavings = 3,
			ACHDetailAccountTypeLoan = 5
		};

		private string RecordCode()
		{
			string RecordCode = "";
			RecordCode = "6";
			return RecordCode;
		}

		public string TransactionByAccountType()
		{
			string TransactionByAccountType = "";
			LastError = "";
			string strReturn = "";

			switch (Convert.ToInt32(fecherFoundation.Strings.LCase(fecherFoundation.Strings.Left(AccountType+" ", 1)))) {
				
				case (int)ACHDetailAcctType.ACHDetailAccountTypeChecking:
				{
					strReturn = "2";
					break;
				}
				case (int)ACHDetailAcctType.ACHDetailAccountTypeSavings:
				{
					strReturn = "3";
					break;
				}
				case (int)ACHDetailAcctType.ACHDetailAccountTypeLoan:
				{
					strReturn = "5";
					break;
				}
				default: {
					LastError = "Invalid account type.";
					break;
				}
			} //end switch

			if (EntryType==ACHDetailEntryType.ACHDetailEntryTypeCredit) {
				if (!PreNote) {
					TransactionByAccountType = strReturn+"2";
				} else {
					TransactionByAccountType = strReturn+"3";
				}
			} else if (EntryType==ACHDetailEntryType.ACHDetailEntryTypeDebit) {
				if (!PreNote) {
					TransactionByAccountType = strReturn+"7";
				} else {
					TransactionByAccountType = strReturn+"8";
				}
			} else {
				LastError = "Invalid entry type.";
			}
			return TransactionByAccountType;
		}

		private bool CheckData()
		{
			bool CheckData = false;
			CheckData = false;
			if (Name=="") {
				LastError = "Name is blank.";
				return CheckData;
			}
			if (ImmediateOriginRT=="") {
				LastError = "The immediate origin RT is blank.";
				return CheckData;
			}
			if (RecordNumber<=0) {
				LastError = "The trace number is invalid.";
				return CheckData;
			}
			if (AccountNumber=="") {
				LastError = "The account number is blank.";
				return CheckData;
			}
			if (AccountType=="") {
				LastError = "The account type is blank.";
				return CheckData;
			}
			if (BankID=="") {
				LastError = "The DFI identifier is blank.";
				return CheckData;
			}
			CheckData = true;
			return CheckData;
		}

		public ACHDetailEntryType EntryType { get; set; }
		public int HashNumber
		{
			get
			{
					int HashNumber = 0;
				HashNumber = (int)Math.Round(fecherFoundation.Conversion.Val(fecherFoundation.Strings.Left(fecherFoundation.Strings.Right("000000000"+BankID, 9), 8)));
				return HashNumber;
			}
		}

		public double EffectiveTotal
		{
			get
			{
					double EffectiveTotal = 0;
				if (!PreNote) {
					EffectiveTotal = TotalAmount;
				} else {
					EffectiveTotal = 0;
				}
				return EffectiveTotal;
			}
		}

		public string TraceNumber
		{
			get
			{
					string TraceNumber = "";
				string strTemp;
				strTemp = fecherFoundation.Strings.Left(ImmediateOriginRT, 8);
				strTemp += fecherFoundation.Strings.Right("0000000"+FCConvert.ToString(RecordNumber), 7);
				TraceNumber = strTemp;
				return TraceNumber;
			}
		}

		public string OutputLineByRec(short intRecNum)
		{
			string OutputLineByRec = "";
			RecordNumber = intRecNum;
			OutputLineByRec = OutputLine();
			return OutputLineByRec;
		}

		public string OutputLine()
		{
			string OutputLine = "";
			LastError = "";
			string strLine = "";
			string strTemp = "";
			try
			{	// On Error GoTo ErrorHandler
				if (CheckData()) {
					strLine = RecordType;
					strTemp = TransactionByAccountType();
					if (strTemp!="") {
						strLine += strTemp;
					} else {
						return OutputLine;
					}
					strTemp = fecherFoundation.Strings.Right("000000000"+BankID, 9);
					strLine += strTemp;
					strTemp = AccountNumber;
					strTemp = FCConvert.ToString(modGlobalFunctions.PadStringWithSpaces(strTemp, 17, false));
					strLine += strTemp;
					strTemp = fecherFoundation.Strings.Format((EffectiveTotal*100), "0000000000");
					strLine += strTemp;
					strTemp = fecherFoundation.Strings.Left(Identifier+"               ", 15);
					strLine += strTemp;
					strTemp = fecherFoundation.Strings.Left(Name+fecherFoundation.Strings.StrDup(22, " "), 22);
					strLine += strTemp;
					strLine += fecherFoundation.Strings.StrDup(2, " ");
					strLine += FCConvert.ToString(AddendaIndicator);
					strLine += TraceNumber;
					OutputLine = strLine;
				} else {
					LastError = "Could not build detail record."+"\n"+LastError;
				}
				return OutputLine;
			}
			catch (Exception ex)
			{	// ErrorHandler:
				LastError = "Could not build detail record."+"\n"+ex.Message;
			}
			return OutputLine;
		}

        public string LastError { get; set; } = "";
        public bool PreNote { get; set; } = false;
        public double TotalAmount { get; set; } = 0;
        public string Name { get; set; } = "";
        public string ImmediateOriginRT { get; set; } = "";
        public string Identifier { get; set; } = "";
        public int RecordNumber { get; set; } = 0;
        public string AccountNumber { get; set; } = "";
        public string AccountType { get; set; } = "";

        public string BankID { get; set; } = "";

        public int AddendaIndicator { get; set; } = 0;



        public string RecordType { get; set; } = "";

		public clsACHDetailRecord() : base()
		{
			EntryType = ACHDetailEntryType.ACHDetailEntryTypeCredit;
		}

	}
}
