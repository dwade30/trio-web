﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptOutprintingFile.
	/// </summary>
	partial class rptOutprintingFile
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptOutprintingFile));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeaderTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWater = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblUnitAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWUnitAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSUnitAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWConsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSConsAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWFlatAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSFlatAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWAdjAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSAdjAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddr1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblCategory = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMapLot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldStatementDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblStatementDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldReadingDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReadingDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBillingPeriod = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBillingPeriod = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldInterestDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblInterestDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldStartDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblStartDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldEndDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblEndDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblConsAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFlatAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdjAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRegular = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWNonLienPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSNonLienPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSCredits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNonLienPastDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCredit = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalAmountDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblLienAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWTotalPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTotalPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalPastDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBookSeq = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFooterTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOverallTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnitAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnitAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnitAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWConsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSConsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWFlatAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSFlatAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWAdjAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSAdjAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddr1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatementDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStatementDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReadingDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReadingDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillingPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillingPeriod)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterestDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterestDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStartDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEndDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFlatAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWNonLienPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSNonLienPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSCredits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNonLienPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalAmountDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLienAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotalPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotalPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOverallTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcct,
				this.fldName,
				this.fldLocation,
				this.fldWater,
				this.fldSewer,
				this.fldTotal,
				this.lblUnitAmount,
				this.fldWUnitAmount,
				this.fldSUnitAmount,
				this.fldWConsAmount,
				this.fldSConsAmount,
				this.fldWFlatAmount,
				this.fldSFlatAmount,
				this.fldWAdjAmount,
				this.fldSAdjAmount,
				this.fldAddr1,
				this.fldCategory,
				this.lblCategory,
				this.fldMapLot,
				this.lblMapLot,
				this.fldStatementDate,
				this.lblStatementDate,
				this.fldReadingDate,
				this.lblReadingDate,
				this.fldBillingPeriod,
				this.lblBillingPeriod,
				this.fldInterestDate,
				this.lblInterestDate,
				this.fldStartDate,
				this.lblStartDate,
				this.fldEndDate,
				this.lblEndDate,
				this.lblConsAmount,
				this.lblFlatAmount,
				this.lblAdjAmount,
				this.lblRegular,
				this.fldWRegular,
				this.fldSRegular,
				this.fldWTax,
				this.fldSTax,
				this.fldWNonLienPastDue,
				this.fldSNonLienPastDue,
				this.fldWCredits,
				this.fldSCredits,
				this.lblTax,
				this.lblNonLienPastDue,
				this.lblCredit,
				this.fldWTotalDue,
				this.fldSTotalDue,
				this.lblTotalAmountDue,
				this.fldWLien,
				this.fldSLien,
				this.lblLienAmount,
				this.fldWTotalPastDue,
				this.fldSTotalPastDue,
				this.lblTotalPastDue,
				this.fldBookSeq,
				this.fldWInterest,
				this.fldSInterest,
				this.lblInterest
			});
			this.Detail.Height = 4.364583F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooterTotals,
				this.fldWTotal,
				this.fldSTotal,
				this.fldOverallTotal,
				this.Line2
			});
			this.ReportFooter.Height = 0.2604167F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.lblAccountNumber,
				this.Line1,
				this.lblName,
				this.Label9,
				this.Label10,
				this.lblHeaderTotal
			});
			this.PageHeader.Height = 0.65625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.lblHeader.Text = "Outprinting File Verification";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.175F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.125F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6.375F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.125F;
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.Height = 0.1875F;
			this.lblAccountNumber.HyperLink = null;
			this.lblAccountNumber.Left = 0F;
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblAccountNumber.Text = "Account Number";
			this.lblAccountNumber.Top = 0.4375F;
			this.lblAccountNumber.Width = 0.75F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.625F;
			this.Line1.Width = 7.4375F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.5F;
			this.Line1.Y1 = 0.625F;
			this.Line1.Y2 = 0.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.75F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.4375F;
			this.lblName.Width = 1F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.1875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label9.Text = "Water";
			this.Label9.Top = 0.4375F;
			this.Label9.Width = 1F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.3125F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label10.Text = "Sewer";
			this.Label10.Top = 0.4375F;
			this.Label10.Width = 1F;
			// 
			// lblHeaderTotal
			// 
			this.lblHeaderTotal.Height = 0.1875F;
			this.lblHeaderTotal.HyperLink = null;
			this.lblHeaderTotal.Left = 6.5F;
			this.lblHeaderTotal.Name = "lblHeaderTotal";
			this.lblHeaderTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblHeaderTotal.Text = "Total";
			this.lblHeaderTotal.Top = 0.4375F;
			this.lblHeaderTotal.Width = 1F;
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.75F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.75F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 3.25F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 2F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0.1875F;
			this.fldLocation.Width = 4.3125F;
			// 
			// fldWater
			// 
			this.fldWater.Height = 0.1875F;
			this.fldWater.Left = 4F;
			this.fldWater.Name = "fldWater";
			this.fldWater.OutputFormat = resources.GetString("fldWater.OutputFormat");
			this.fldWater.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWater.Text = " ";
			this.fldWater.Top = 0F;
			this.fldWater.Width = 1.1875F;
			// 
			// fldSewer
			// 
			this.fldSewer.Height = 0.1875F;
			this.fldSewer.Left = 5.1875F;
			this.fldSewer.Name = "fldSewer";
			this.fldSewer.OutputFormat = resources.GetString("fldSewer.OutputFormat");
			this.fldSewer.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSewer.Text = " ";
			this.fldSewer.Top = 0F;
			this.fldSewer.Width = 1.125F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.3125F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.OutputFormat = resources.GetString("fldTotal.OutputFormat");
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotal.Text = " ";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.1875F;
			// 
			// lblUnitAmount
			// 
			this.lblUnitAmount.Height = 0.1875F;
			this.lblUnitAmount.HyperLink = null;
			this.lblUnitAmount.Left = 0.75F;
			this.lblUnitAmount.Name = "lblUnitAmount";
			this.lblUnitAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblUnitAmount.Text = "Unit Amount:";
			this.lblUnitAmount.Top = 1.8125F;
			this.lblUnitAmount.Width = 2.25F;
			// 
			// fldWUnitAmount
			// 
			this.fldWUnitAmount.Height = 0.1875F;
			this.fldWUnitAmount.Left = 4F;
			this.fldWUnitAmount.Name = "fldWUnitAmount";
			this.fldWUnitAmount.OutputFormat = resources.GetString("fldWUnitAmount.OutputFormat");
			this.fldWUnitAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWUnitAmount.Text = " ";
			this.fldWUnitAmount.Top = 1.8125F;
			this.fldWUnitAmount.Width = 1.1875F;
			// 
			// fldSUnitAmount
			// 
			this.fldSUnitAmount.Height = 0.1875F;
			this.fldSUnitAmount.Left = 5.1875F;
			this.fldSUnitAmount.Name = "fldSUnitAmount";
			this.fldSUnitAmount.OutputFormat = resources.GetString("fldSUnitAmount.OutputFormat");
			this.fldSUnitAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSUnitAmount.Text = " ";
			this.fldSUnitAmount.Top = 1.8125F;
			this.fldSUnitAmount.Width = 1.125F;
			// 
			// fldWConsAmount
			// 
			this.fldWConsAmount.Height = 0.1875F;
			this.fldWConsAmount.Left = 4F;
			this.fldWConsAmount.Name = "fldWConsAmount";
			this.fldWConsAmount.OutputFormat = resources.GetString("fldWConsAmount.OutputFormat");
			this.fldWConsAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWConsAmount.Text = " ";
			this.fldWConsAmount.Top = 2F;
			this.fldWConsAmount.Width = 1.1875F;
			// 
			// fldSConsAmount
			// 
			this.fldSConsAmount.Height = 0.1875F;
			this.fldSConsAmount.Left = 5.1875F;
			this.fldSConsAmount.Name = "fldSConsAmount";
			this.fldSConsAmount.OutputFormat = resources.GetString("fldSConsAmount.OutputFormat");
			this.fldSConsAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSConsAmount.Text = " ";
			this.fldSConsAmount.Top = 2F;
			this.fldSConsAmount.Width = 1.125F;
			// 
			// fldWFlatAmount
			// 
			this.fldWFlatAmount.Height = 0.1875F;
			this.fldWFlatAmount.Left = 4F;
			this.fldWFlatAmount.Name = "fldWFlatAmount";
			this.fldWFlatAmount.OutputFormat = resources.GetString("fldWFlatAmount.OutputFormat");
			this.fldWFlatAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWFlatAmount.Text = " ";
			this.fldWFlatAmount.Top = 2.1875F;
			this.fldWFlatAmount.Width = 1.1875F;
			// 
			// fldSFlatAmount
			// 
			this.fldSFlatAmount.Height = 0.1875F;
			this.fldSFlatAmount.Left = 5.1875F;
			this.fldSFlatAmount.Name = "fldSFlatAmount";
			this.fldSFlatAmount.OutputFormat = resources.GetString("fldSFlatAmount.OutputFormat");
			this.fldSFlatAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSFlatAmount.Text = " ";
			this.fldSFlatAmount.Top = 2.1875F;
			this.fldSFlatAmount.Width = 1.125F;
			// 
			// fldWAdjAmount
			// 
			this.fldWAdjAmount.Height = 0.1875F;
			this.fldWAdjAmount.Left = 4F;
			this.fldWAdjAmount.Name = "fldWAdjAmount";
			this.fldWAdjAmount.OutputFormat = resources.GetString("fldWAdjAmount.OutputFormat");
			this.fldWAdjAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWAdjAmount.Text = " ";
			this.fldWAdjAmount.Top = 2.375F;
			this.fldWAdjAmount.Width = 1.1875F;
			// 
			// fldSAdjAmount
			// 
			this.fldSAdjAmount.Height = 0.1875F;
			this.fldSAdjAmount.Left = 5.1875F;
			this.fldSAdjAmount.Name = "fldSAdjAmount";
			this.fldSAdjAmount.OutputFormat = resources.GetString("fldSAdjAmount.OutputFormat");
			this.fldSAdjAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSAdjAmount.Text = " ";
			this.fldSAdjAmount.Top = 2.375F;
			this.fldSAdjAmount.Width = 1.125F;
			// 
			// fldAddr1
			// 
			this.fldAddr1.Height = 0.4375F;
			this.fldAddr1.Left = 2F;
			this.fldAddr1.Name = "fldAddr1";
			this.fldAddr1.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldAddr1.Text = null;
			this.fldAddr1.Top = 0.375F;
			this.fldAddr1.Width = 4.3125F;
			// 
			// fldCategory
			// 
			this.fldCategory.Height = 0.1875F;
			this.fldCategory.Left = 2F;
			this.fldCategory.Name = "fldCategory";
			this.fldCategory.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldCategory.Text = null;
			this.fldCategory.Top = 0.8125F;
			this.fldCategory.Width = 3.25F;
			// 
			// lblCategory
			// 
			this.lblCategory.Height = 0.1875F;
			this.lblCategory.HyperLink = null;
			this.lblCategory.Left = 0.75F;
			this.lblCategory.Name = "lblCategory";
			this.lblCategory.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblCategory.Text = "Category:";
			this.lblCategory.Top = 0.8125F;
			this.lblCategory.Width = 1.25F;
			// 
			// fldMapLot
			// 
			this.fldMapLot.Height = 0.1875F;
			this.fldMapLot.Left = 2F;
			this.fldMapLot.Name = "fldMapLot";
			this.fldMapLot.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldMapLot.Text = null;
			this.fldMapLot.Top = 1F;
			this.fldMapLot.Width = 1F;
			// 
			// lblMapLot
			// 
			this.lblMapLot.Height = 0.1875F;
			this.lblMapLot.HyperLink = null;
			this.lblMapLot.Left = 0.75F;
			this.lblMapLot.Name = "lblMapLot";
			this.lblMapLot.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblMapLot.Text = "MapLot:";
			this.lblMapLot.Top = 1F;
			this.lblMapLot.Width = 1.25F;
			// 
			// fldStatementDate
			// 
			this.fldStatementDate.Height = 0.1875F;
			this.fldStatementDate.Left = 2F;
			this.fldStatementDate.Name = "fldStatementDate";
			this.fldStatementDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldStatementDate.Text = null;
			this.fldStatementDate.Top = 1.1875F;
			this.fldStatementDate.Width = 1F;
			// 
			// lblStatementDate
			// 
			this.lblStatementDate.Height = 0.1875F;
			this.lblStatementDate.HyperLink = null;
			this.lblStatementDate.Left = 0.75F;
			this.lblStatementDate.Name = "lblStatementDate";
			this.lblStatementDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblStatementDate.Text = "Statement Date:";
			this.lblStatementDate.Top = 1.1875F;
			this.lblStatementDate.Width = 1.25F;
			// 
			// fldReadingDate
			// 
			this.fldReadingDate.Height = 0.1875F;
			this.fldReadingDate.Left = 2F;
			this.fldReadingDate.Name = "fldReadingDate";
			this.fldReadingDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldReadingDate.Text = null;
			this.fldReadingDate.Top = 1.375F;
			this.fldReadingDate.Width = 1F;
			// 
			// lblReadingDate
			// 
			this.lblReadingDate.Height = 0.1875F;
			this.lblReadingDate.HyperLink = null;
			this.lblReadingDate.Left = 0.75F;
			this.lblReadingDate.Name = "lblReadingDate";
			this.lblReadingDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblReadingDate.Text = "Reading Date:";
			this.lblReadingDate.Top = 1.375F;
			this.lblReadingDate.Width = 1.25F;
			// 
			// fldBillingPeriod
			// 
			this.fldBillingPeriod.Height = 0.1875F;
			this.fldBillingPeriod.Left = 4.25F;
			this.fldBillingPeriod.Name = "fldBillingPeriod";
			this.fldBillingPeriod.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldBillingPeriod.Text = null;
			this.fldBillingPeriod.Top = 1.1875F;
			this.fldBillingPeriod.Width = 1F;
			// 
			// lblBillingPeriod
			// 
			this.lblBillingPeriod.Height = 0.1875F;
			this.lblBillingPeriod.HyperLink = null;
			this.lblBillingPeriod.Left = 3F;
			this.lblBillingPeriod.Name = "lblBillingPeriod";
			this.lblBillingPeriod.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblBillingPeriod.Text = "Billing Period:";
			this.lblBillingPeriod.Top = 1.1875F;
			this.lblBillingPeriod.Width = 1.25F;
			// 
			// fldInterestDate
			// 
			this.fldInterestDate.Height = 0.1875F;
			this.fldInterestDate.Left = 2F;
			this.fldInterestDate.Name = "fldInterestDate";
			this.fldInterestDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldInterestDate.Text = null;
			this.fldInterestDate.Top = 1.5625F;
			this.fldInterestDate.Width = 1F;
			// 
			// lblInterestDate
			// 
			this.lblInterestDate.Height = 0.1875F;
			this.lblInterestDate.HyperLink = null;
			this.lblInterestDate.Left = 0.75F;
			this.lblInterestDate.Name = "lblInterestDate";
			this.lblInterestDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblInterestDate.Text = "Interest Date:";
			this.lblInterestDate.Top = 1.5625F;
			this.lblInterestDate.Width = 1.25F;
			// 
			// fldStartDate
			// 
			this.fldStartDate.Height = 0.1875F;
			this.fldStartDate.Left = 4.25F;
			this.fldStartDate.Name = "fldStartDate";
			this.fldStartDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldStartDate.Text = null;
			this.fldStartDate.Top = 1.375F;
			this.fldStartDate.Width = 1F;
			// 
			// lblStartDate
			// 
			this.lblStartDate.Height = 0.1875F;
			this.lblStartDate.HyperLink = null;
			this.lblStartDate.Left = 3F;
			this.lblStartDate.Name = "lblStartDate";
			this.lblStartDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblStartDate.Text = "Start Date:";
			this.lblStartDate.Top = 1.375F;
			this.lblStartDate.Width = 1.25F;
			// 
			// fldEndDate
			// 
			this.fldEndDate.Height = 0.1875F;
			this.fldEndDate.Left = 4.25F;
			this.fldEndDate.Name = "fldEndDate";
			this.fldEndDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldEndDate.Text = null;
			this.fldEndDate.Top = 1.5625F;
			this.fldEndDate.Width = 1F;
			// 
			// lblEndDate
			// 
			this.lblEndDate.Height = 0.1875F;
			this.lblEndDate.HyperLink = null;
			this.lblEndDate.Left = 3F;
			this.lblEndDate.Name = "lblEndDate";
			this.lblEndDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblEndDate.Text = "End Date:";
			this.lblEndDate.Top = 1.5625F;
			this.lblEndDate.Width = 1.25F;
			// 
			// lblConsAmount
			// 
			this.lblConsAmount.Height = 0.1875F;
			this.lblConsAmount.HyperLink = null;
			this.lblConsAmount.Left = 0.75F;
			this.lblConsAmount.Name = "lblConsAmount";
			this.lblConsAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblConsAmount.Text = "Consumption Amount:";
			this.lblConsAmount.Top = 2F;
			this.lblConsAmount.Width = 2.25F;
			// 
			// lblFlatAmount
			// 
			this.lblFlatAmount.Height = 0.1875F;
			this.lblFlatAmount.HyperLink = null;
			this.lblFlatAmount.Left = 0.75F;
			this.lblFlatAmount.Name = "lblFlatAmount";
			this.lblFlatAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblFlatAmount.Text = "Flat Amount:";
			this.lblFlatAmount.Top = 2.1875F;
			this.lblFlatAmount.Width = 2.25F;
			// 
			// lblAdjAmount
			// 
			this.lblAdjAmount.Height = 0.1875F;
			this.lblAdjAmount.HyperLink = null;
			this.lblAdjAmount.Left = 0.75F;
			this.lblAdjAmount.Name = "lblAdjAmount";
			this.lblAdjAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblAdjAmount.Text = "Adjustment Amount:";
			this.lblAdjAmount.Top = 2.375F;
			this.lblAdjAmount.Width = 2.25F;
			// 
			// lblRegular
			// 
			this.lblRegular.Height = 0.1875F;
			this.lblRegular.HyperLink = null;
			this.lblRegular.Left = 0.75F;
			this.lblRegular.Name = "lblRegular";
			this.lblRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblRegular.Text = "Regular";
			this.lblRegular.Top = 2.625F;
			this.lblRegular.Width = 2.25F;
			// 
			// fldWRegular
			// 
			this.fldWRegular.Height = 0.1875F;
			this.fldWRegular.Left = 4F;
			this.fldWRegular.Name = "fldWRegular";
			this.fldWRegular.OutputFormat = resources.GetString("fldWRegular.OutputFormat");
			this.fldWRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWRegular.Text = " ";
			this.fldWRegular.Top = 2.625F;
			this.fldWRegular.Width = 1.1875F;
			// 
			// fldSRegular
			// 
			this.fldSRegular.Height = 0.1875F;
			this.fldSRegular.Left = 5.1875F;
			this.fldSRegular.Name = "fldSRegular";
			this.fldSRegular.OutputFormat = resources.GetString("fldSRegular.OutputFormat");
			this.fldSRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSRegular.Text = " ";
			this.fldSRegular.Top = 2.625F;
			this.fldSRegular.Width = 1.125F;
			// 
			// fldWTax
			// 
			this.fldWTax.Height = 0.1875F;
			this.fldWTax.Left = 4F;
			this.fldWTax.Name = "fldWTax";
			this.fldWTax.OutputFormat = resources.GetString("fldWTax.OutputFormat");
			this.fldWTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWTax.Text = " ";
			this.fldWTax.Top = 2.8125F;
			this.fldWTax.Width = 1.1875F;
			// 
			// fldSTax
			// 
			this.fldSTax.Height = 0.1875F;
			this.fldSTax.Left = 5.1875F;
			this.fldSTax.Name = "fldSTax";
			this.fldSTax.OutputFormat = resources.GetString("fldSTax.OutputFormat");
			this.fldSTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSTax.Text = " ";
			this.fldSTax.Top = 2.8125F;
			this.fldSTax.Width = 1.125F;
			// 
			// fldWNonLienPastDue
			// 
			this.fldWNonLienPastDue.Height = 0.1875F;
			this.fldWNonLienPastDue.Left = 4F;
			this.fldWNonLienPastDue.Name = "fldWNonLienPastDue";
			this.fldWNonLienPastDue.OutputFormat = resources.GetString("fldWNonLienPastDue.OutputFormat");
			this.fldWNonLienPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWNonLienPastDue.Text = " ";
			this.fldWNonLienPastDue.Top = 3F;
			this.fldWNonLienPastDue.Width = 1.1875F;
			// 
			// fldSNonLienPastDue
			// 
			this.fldSNonLienPastDue.Height = 0.1875F;
			this.fldSNonLienPastDue.Left = 5.1875F;
			this.fldSNonLienPastDue.Name = "fldSNonLienPastDue";
			this.fldSNonLienPastDue.OutputFormat = resources.GetString("fldSNonLienPastDue.OutputFormat");
			this.fldSNonLienPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSNonLienPastDue.Text = " ";
			this.fldSNonLienPastDue.Top = 3F;
			this.fldSNonLienPastDue.Width = 1.125F;
			// 
			// fldWCredits
			// 
			this.fldWCredits.Height = 0.1875F;
			this.fldWCredits.Left = 4F;
			this.fldWCredits.Name = "fldWCredits";
			this.fldWCredits.OutputFormat = resources.GetString("fldWCredits.OutputFormat");
			this.fldWCredits.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWCredits.Text = " ";
			this.fldWCredits.Top = 3.1875F;
			this.fldWCredits.Width = 1.1875F;
			// 
			// fldSCredits
			// 
			this.fldSCredits.Height = 0.1875F;
			this.fldSCredits.Left = 5.1875F;
			this.fldSCredits.Name = "fldSCredits";
			this.fldSCredits.OutputFormat = resources.GetString("fldSCredits.OutputFormat");
			this.fldSCredits.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSCredits.Text = " ";
			this.fldSCredits.Top = 3.1875F;
			this.fldSCredits.Width = 1.125F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 0.75F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 2.8125F;
			this.lblTax.Width = 2.25F;
			// 
			// lblNonLienPastDue
			// 
			this.lblNonLienPastDue.Height = 0.1875F;
			this.lblNonLienPastDue.HyperLink = null;
			this.lblNonLienPastDue.Left = 0.75F;
			this.lblNonLienPastDue.Name = "lblNonLienPastDue";
			this.lblNonLienPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblNonLienPastDue.Text = "Non Lien Past Due";
			this.lblNonLienPastDue.Top = 3F;
			this.lblNonLienPastDue.Width = 2.25F;
			// 
			// lblCredit
			// 
			this.lblCredit.Height = 0.1875F;
			this.lblCredit.HyperLink = null;
			this.lblCredit.Left = 0.75F;
			this.lblCredit.Name = "lblCredit";
			this.lblCredit.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblCredit.Text = "Credits";
			this.lblCredit.Top = 3.1875F;
			this.lblCredit.Width = 2.25F;
			// 
			// fldWTotalDue
			// 
			this.fldWTotalDue.Height = 0.1875F;
			this.fldWTotalDue.Left = 4F;
			this.fldWTotalDue.Name = "fldWTotalDue";
			this.fldWTotalDue.OutputFormat = resources.GetString("fldWTotalDue.OutputFormat");
			this.fldWTotalDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWTotalDue.Text = " ";
			this.fldWTotalDue.Top = 3.9375F;
			this.fldWTotalDue.Width = 1.1875F;
			// 
			// fldSTotalDue
			// 
			this.fldSTotalDue.Height = 0.1875F;
			this.fldSTotalDue.Left = 5.1875F;
			this.fldSTotalDue.Name = "fldSTotalDue";
			this.fldSTotalDue.OutputFormat = resources.GetString("fldSTotalDue.OutputFormat");
			this.fldSTotalDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSTotalDue.Text = " ";
			this.fldSTotalDue.Top = 3.9375F;
			this.fldSTotalDue.Width = 1.125F;
			// 
			// lblTotalAmountDue
			// 
			this.lblTotalAmountDue.Height = 0.1875F;
			this.lblTotalAmountDue.HyperLink = null;
			this.lblTotalAmountDue.Left = 0.75F;
			this.lblTotalAmountDue.Name = "lblTotalAmountDue";
			this.lblTotalAmountDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblTotalAmountDue.Text = "Total Amount Due";
			this.lblTotalAmountDue.Top = 3.9375F;
			this.lblTotalAmountDue.Width = 2.25F;
			// 
			// fldWLien
			// 
			this.fldWLien.Height = 0.1875F;
			this.fldWLien.Left = 4F;
			this.fldWLien.Name = "fldWLien";
			this.fldWLien.OutputFormat = resources.GetString("fldWLien.OutputFormat");
			this.fldWLien.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWLien.Text = " ";
			this.fldWLien.Top = 3.375F;
			this.fldWLien.Width = 1.1875F;
			// 
			// fldSLien
			// 
			this.fldSLien.Height = 0.1875F;
			this.fldSLien.Left = 5.1875F;
			this.fldSLien.Name = "fldSLien";
			this.fldSLien.OutputFormat = resources.GetString("fldSLien.OutputFormat");
			this.fldSLien.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSLien.Text = " ";
			this.fldSLien.Top = 3.375F;
			this.fldSLien.Width = 1.125F;
			// 
			// lblLienAmount
			// 
			this.lblLienAmount.Height = 0.1875F;
			this.lblLienAmount.HyperLink = null;
			this.lblLienAmount.Left = 0.75F;
			this.lblLienAmount.Name = "lblLienAmount";
			this.lblLienAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblLienAmount.Text = "Lien Amount";
			this.lblLienAmount.Top = 3.375F;
			this.lblLienAmount.Width = 2.25F;
			// 
			// fldWTotalPastDue
			// 
			this.fldWTotalPastDue.Height = 0.1875F;
			this.fldWTotalPastDue.Left = 4F;
			this.fldWTotalPastDue.Name = "fldWTotalPastDue";
			this.fldWTotalPastDue.OutputFormat = resources.GetString("fldWTotalPastDue.OutputFormat");
			this.fldWTotalPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWTotalPastDue.Text = " ";
			this.fldWTotalPastDue.Top = 3.75F;
			this.fldWTotalPastDue.Width = 1.1875F;
			// 
			// fldSTotalPastDue
			// 
			this.fldSTotalPastDue.Height = 0.1875F;
			this.fldSTotalPastDue.Left = 5.1875F;
			this.fldSTotalPastDue.Name = "fldSTotalPastDue";
			this.fldSTotalPastDue.OutputFormat = resources.GetString("fldSTotalPastDue.OutputFormat");
			this.fldSTotalPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSTotalPastDue.Text = " ";
			this.fldSTotalPastDue.Top = 3.75F;
			this.fldSTotalPastDue.Width = 1.125F;
			// 
			// lblTotalPastDue
			// 
			this.lblTotalPastDue.Height = 0.1875F;
			this.lblTotalPastDue.HyperLink = null;
			this.lblTotalPastDue.Left = 0.75F;
			this.lblTotalPastDue.Name = "lblTotalPastDue";
			this.lblTotalPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblTotalPastDue.Text = "Total Past Due";
			this.lblTotalPastDue.Top = 3.75F;
			this.lblTotalPastDue.Width = 2.25F;
			// 
			// fldBookSeq
			// 
			this.fldBookSeq.Height = 0.1875F;
			this.fldBookSeq.Left = 0.75F;
			this.fldBookSeq.Name = "fldBookSeq";
			this.fldBookSeq.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldBookSeq.Text = null;
			this.fldBookSeq.Top = 0.1875F;
			this.fldBookSeq.Width = 1.25F;
			// 
			// fldWInterest
			// 
			this.fldWInterest.Height = 0.1875F;
			this.fldWInterest.Left = 4F;
			this.fldWInterest.Name = "fldWInterest";
			this.fldWInterest.OutputFormat = resources.GetString("fldWInterest.OutputFormat");
			this.fldWInterest.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWInterest.Text = " ";
			this.fldWInterest.Top = 3.5625F;
			this.fldWInterest.Width = 1.1875F;
			// 
			// fldSInterest
			// 
			this.fldSInterest.Height = 0.1875F;
			this.fldSInterest.Left = 5.1875F;
			this.fldSInterest.Name = "fldSInterest";
			this.fldSInterest.OutputFormat = resources.GetString("fldSInterest.OutputFormat");
			this.fldSInterest.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSInterest.Text = " ";
			this.fldSInterest.Top = 3.5625F;
			this.fldSInterest.Width = 1.125F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 0.75F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 3.5625F;
			this.lblInterest.Width = 2.25F;
			// 
			// lblFooterTotals
			// 
			this.lblFooterTotals.Height = 0.1875F;
			this.lblFooterTotals.HyperLink = null;
			this.lblFooterTotals.Left = 2.875F;
			this.lblFooterTotals.MultiLine = false;
			this.lblFooterTotals.Name = "lblFooterTotals";
			this.lblFooterTotals.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblFooterTotals.Text = "Totals:";
			this.lblFooterTotals.Top = 0.0625F;
			this.lblFooterTotals.Width = 1.125F;
			// 
			// fldWTotal
			// 
			this.fldWTotal.Height = 0.1875F;
			this.fldWTotal.Left = 4F;
			this.fldWTotal.Name = "fldWTotal";
			this.fldWTotal.OutputFormat = resources.GetString("fldWTotal.OutputFormat");
			this.fldWTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldWTotal.Text = " ";
			this.fldWTotal.Top = 0.0625F;
			this.fldWTotal.Width = 1.1875F;
			// 
			// fldSTotal
			// 
			this.fldSTotal.Height = 0.1875F;
			this.fldSTotal.Left = 5.1875F;
			this.fldSTotal.Name = "fldSTotal";
			this.fldSTotal.OutputFormat = resources.GetString("fldSTotal.OutputFormat");
			this.fldSTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldSTotal.Text = " ";
			this.fldSTotal.Top = 0.0625F;
			this.fldSTotal.Width = 1.125F;
			// 
			// fldOverallTotal
			// 
			this.fldOverallTotal.Height = 0.1875F;
			this.fldOverallTotal.Left = 6.3125F;
			this.fldOverallTotal.Name = "fldOverallTotal";
			this.fldOverallTotal.OutputFormat = resources.GetString("fldOverallTotal.OutputFormat");
			this.fldOverallTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldOverallTotal.Text = " ";
			this.fldOverallTotal.Top = 0.0625F;
			this.fldOverallTotal.Width = 1.1875F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 7.4375F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// rptOutprintingFile
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeaderTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblUnitAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnitAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnitAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWConsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSConsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWFlatAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSFlatAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWAdjAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSAdjAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddr1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatementDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStatementDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReadingDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReadingDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillingPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillingPeriod)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterestDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterestDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStartDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblEndDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFlatAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdjAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWNonLienPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSNonLienPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSCredits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNonLienPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCredit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalAmountDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLienAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotalPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotalPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOverallTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWater;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUnitAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWUnitAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSUnitAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWConsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSConsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWFlatAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSFlatAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWAdjAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSAdjAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddr1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCategory;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatementDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStatementDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReadingDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReadingDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillingPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillingPeriod;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterestDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterestDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStartDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStartDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEndDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblEndDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblConsAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFlatAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdjAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWNonLienPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSNonLienPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWCredits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSCredits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNonLienPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCredit;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalAmountDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWLien;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSLien;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLienAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTotalPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTotalPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSeq;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOverallTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderTotal;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
