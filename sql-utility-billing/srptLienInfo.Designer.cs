﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptLienInfo.
	/// </summary>
	partial class srptLienInfo
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptLienInfo));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldCode = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPaymentDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterPrincipalPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterTaxPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterInterestPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterCostsPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerPrincipalPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerTaxPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerInterestPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerCostsPayment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line8 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldReceipt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReference = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterPrincipalPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTaxPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterInterestPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterCostsPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerPrincipalPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTaxPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerInterestPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerCostsPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCode,
				this.fldPaymentDate,
				this.fldWaterPrincipalPayment,
				this.fldWaterTaxPayment,
				this.fldWaterInterestPayment,
				this.fldWaterCostsPayment,
				this.fldSewerPrincipalPayment,
				this.fldSewerTaxPayment,
				this.fldSewerInterestPayment,
				this.fldSewerCostsPayment,
				this.Line8,
				this.fldReceipt
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldCode
			// 
			this.fldCode.Height = 0.1875F;
			this.fldCode.Left = 1.96875F;
			this.fldCode.Name = "fldCode";
			this.fldCode.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldCode.Text = "Field1";
			this.fldCode.Top = 0F;
			this.fldCode.Width = 0.375F;
			// 
			// fldPaymentDate
			// 
			this.fldPaymentDate.Height = 0.1875F;
			this.fldPaymentDate.Left = 0.0625F;
			this.fldPaymentDate.Name = "fldPaymentDate";
			this.fldPaymentDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPaymentDate.Text = "Field1";
			this.fldPaymentDate.Top = 0F;
			this.fldPaymentDate.Width = 0.75F;
			// 
			// fldWaterPrincipalPayment
			// 
			this.fldWaterPrincipalPayment.Height = 0.1875F;
			this.fldWaterPrincipalPayment.Left = 3.125F;
			this.fldWaterPrincipalPayment.Name = "fldWaterPrincipalPayment";
			this.fldWaterPrincipalPayment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldWaterPrincipalPayment.Text = "Field1";
			this.fldWaterPrincipalPayment.Top = 0F;
			this.fldWaterPrincipalPayment.Width = 0.84375F;
			// 
			// fldWaterTaxPayment
			// 
			this.fldWaterTaxPayment.Height = 0.1875F;
			this.fldWaterTaxPayment.Left = 4.03125F;
			this.fldWaterTaxPayment.Name = "fldWaterTaxPayment";
			this.fldWaterTaxPayment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldWaterTaxPayment.Text = "Field1";
			this.fldWaterTaxPayment.Top = 0F;
			this.fldWaterTaxPayment.Width = 0.84375F;
			// 
			// fldWaterInterestPayment
			// 
			this.fldWaterInterestPayment.Height = 0.1875F;
			this.fldWaterInterestPayment.Left = 4.9375F;
			this.fldWaterInterestPayment.Name = "fldWaterInterestPayment";
			this.fldWaterInterestPayment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldWaterInterestPayment.Text = "Field1";
			this.fldWaterInterestPayment.Top = 0F;
			this.fldWaterInterestPayment.Width = 0.84375F;
			// 
			// fldWaterCostsPayment
			// 
			this.fldWaterCostsPayment.Height = 0.1875F;
			this.fldWaterCostsPayment.Left = 5.84375F;
			this.fldWaterCostsPayment.Name = "fldWaterCostsPayment";
			this.fldWaterCostsPayment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldWaterCostsPayment.Text = "Field1";
			this.fldWaterCostsPayment.Top = 0F;
			this.fldWaterCostsPayment.Width = 0.84375F;
			// 
			// fldSewerPrincipalPayment
			// 
			this.fldSewerPrincipalPayment.Height = 0.1875F;
			this.fldSewerPrincipalPayment.Left = 6.9375F;
			this.fldSewerPrincipalPayment.Name = "fldSewerPrincipalPayment";
			this.fldSewerPrincipalPayment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldSewerPrincipalPayment.Text = "Field1";
			this.fldSewerPrincipalPayment.Top = 0F;
			this.fldSewerPrincipalPayment.Width = 0.84375F;
			// 
			// fldSewerTaxPayment
			// 
			this.fldSewerTaxPayment.Height = 0.1875F;
			this.fldSewerTaxPayment.Left = 7.84375F;
			this.fldSewerTaxPayment.Name = "fldSewerTaxPayment";
			this.fldSewerTaxPayment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldSewerTaxPayment.Text = "Field1";
			this.fldSewerTaxPayment.Top = 0F;
			this.fldSewerTaxPayment.Width = 0.84375F;
			// 
			// fldSewerInterestPayment
			// 
			this.fldSewerInterestPayment.Height = 0.1875F;
			this.fldSewerInterestPayment.Left = 8.75F;
			this.fldSewerInterestPayment.Name = "fldSewerInterestPayment";
			this.fldSewerInterestPayment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldSewerInterestPayment.Text = "Field1";
			this.fldSewerInterestPayment.Top = 0F;
			this.fldSewerInterestPayment.Width = 0.84375F;
			// 
			// fldSewerCostsPayment
			// 
			this.fldSewerCostsPayment.Height = 0.1875F;
			this.fldSewerCostsPayment.Left = 9.65625F;
			this.fldSewerCostsPayment.Name = "fldSewerCostsPayment";
			this.fldSewerCostsPayment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldSewerCostsPayment.Text = "Field1";
			this.fldSewerCostsPayment.Top = 0F;
			this.fldSewerCostsPayment.Width = 0.84375F;
			// 
			// Line8
			// 
			this.Line8.Height = 0.1875F;
			this.Line8.Left = 6.8125F;
			this.Line8.LineWeight = 1F;
			this.Line8.Name = "Line8";
			this.Line8.Top = 0F;
			this.Line8.Width = 0F;
			this.Line8.X1 = 6.8125F;
			this.Line8.X2 = 6.8125F;
			this.Line8.Y1 = 0F;
			this.Line8.Y2 = 0.1875F;
			// 
			// fldReceipt
			// 
			this.fldReceipt.Height = 0.1875F;
			this.fldReceipt.Left = 2.40625F;
			this.fldReceipt.Name = "fldReceipt";
			this.fldReceipt.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldReceipt.Text = "Field1";
			this.fldReceipt.Top = 0F;
			this.fldReceipt.Width = 0.65625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDate,
				this.fldReference,
				this.fldWaterPrincipal,
				this.fldWaterTax,
				this.fldWaterInterest,
				this.fldWaterCosts,
				this.fldSewerPrincipal,
				this.fldSewerTax,
				this.fldSewerInterest,
				this.fldSewerCosts,
				this.Line7
			});
			this.GroupHeader1.Height = 0.1770833F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0.0625F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldDate.Text = "Field1";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.75F;
			// 
			// fldReference
			// 
			this.fldReference.Height = 0.1875F;
			this.fldReference.Left = 0.875F;
			this.fldReference.Name = "fldReference";
			this.fldReference.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldReference.Text = "Field1";
			this.fldReference.Top = 0F;
			this.fldReference.Width = 2.1875F;
			// 
			// fldWaterPrincipal
			// 
			this.fldWaterPrincipal.Height = 0.1875F;
			this.fldWaterPrincipal.Left = 3.125F;
			this.fldWaterPrincipal.Name = "fldWaterPrincipal";
			this.fldWaterPrincipal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldWaterPrincipal.Text = "Field1";
			this.fldWaterPrincipal.Top = 0F;
			this.fldWaterPrincipal.Width = 0.84375F;
			// 
			// fldWaterTax
			// 
			this.fldWaterTax.Height = 0.1875F;
			this.fldWaterTax.Left = 4.03125F;
			this.fldWaterTax.Name = "fldWaterTax";
			this.fldWaterTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldWaterTax.Text = "Field1";
			this.fldWaterTax.Top = 0F;
			this.fldWaterTax.Width = 0.84375F;
			// 
			// fldWaterInterest
			// 
			this.fldWaterInterest.Height = 0.1875F;
			this.fldWaterInterest.Left = 4.9375F;
			this.fldWaterInterest.Name = "fldWaterInterest";
			this.fldWaterInterest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldWaterInterest.Text = "Field1";
			this.fldWaterInterest.Top = 0F;
			this.fldWaterInterest.Width = 0.84375F;
			// 
			// fldWaterCosts
			// 
			this.fldWaterCosts.Height = 0.1875F;
			this.fldWaterCosts.Left = 5.84375F;
			this.fldWaterCosts.Name = "fldWaterCosts";
			this.fldWaterCosts.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldWaterCosts.Text = "Field1";
			this.fldWaterCosts.Top = 0F;
			this.fldWaterCosts.Width = 0.84375F;
			// 
			// fldSewerPrincipal
			// 
			this.fldSewerPrincipal.Height = 0.1875F;
			this.fldSewerPrincipal.Left = 6.9375F;
			this.fldSewerPrincipal.Name = "fldSewerPrincipal";
			this.fldSewerPrincipal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldSewerPrincipal.Text = "Field1";
			this.fldSewerPrincipal.Top = 0F;
			this.fldSewerPrincipal.Width = 0.84375F;
			// 
			// fldSewerTax
			// 
			this.fldSewerTax.Height = 0.1875F;
			this.fldSewerTax.Left = 7.84375F;
			this.fldSewerTax.Name = "fldSewerTax";
			this.fldSewerTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldSewerTax.Text = "Field1";
			this.fldSewerTax.Top = 0F;
			this.fldSewerTax.Width = 0.84375F;
			// 
			// fldSewerInterest
			// 
			this.fldSewerInterest.Height = 0.1875F;
			this.fldSewerInterest.Left = 8.75F;
			this.fldSewerInterest.Name = "fldSewerInterest";
			this.fldSewerInterest.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldSewerInterest.Text = "Field1";
			this.fldSewerInterest.Top = 0F;
			this.fldSewerInterest.Width = 0.84375F;
			// 
			// fldSewerCosts
			// 
			this.fldSewerCosts.Height = 0.1875F;
			this.fldSewerCosts.Left = 9.65625F;
			this.fldSewerCosts.Name = "fldSewerCosts";
			this.fldSewerCosts.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldSewerCosts.Text = "Field1";
			this.fldSewerCosts.Top = 0F;
			this.fldSewerCosts.Width = 0.84375F;
			// 
			// Line7
			// 
			this.Line7.Height = 0.1875F;
			this.Line7.Left = 6.8125F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 0F;
			this.Line7.Width = 0F;
			this.Line7.X1 = 6.8125F;
			this.Line7.X2 = 6.8125F;
			this.Line7.Y1 = 0F;
			this.Line7.Y2 = 0.1875F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// srptLienInfo
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.srptLienInfo_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldCode)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPaymentDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterPrincipalPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTaxPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterInterestPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterCostsPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerPrincipalPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTaxPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerInterestPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerCostsPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReceipt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReference)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCode;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPaymentDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterPrincipalPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterTaxPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterInterestPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterCostsPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerPrincipalPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerTaxPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerInterestPayment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerCostsPayment;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReceipt;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReference;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerInterest;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
