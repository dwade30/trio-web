﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmTableFileSetup.
	/// </summary>
	partial class frmTableFileSetup : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCPanel> fraRateTable;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtMinCons;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtRateS;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtRateW;
		public fecherFoundation.FCTabControl tabSetup;
		public fecherFoundation.FCTabPage tabSetup_Page1;
		public fecherFoundation.FCPanel Frame1;
		public fecherFoundation.FCGrid vsMeterSizeDescList;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCTabPage tabSetup_Page2;
		public FCGrid vsFrequency;
		public fecherFoundation.FCPanel Frame2;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCTabPage tabSetup_Page3;
		public FCGrid vsCategory;
		public fecherFoundation.FCPanel Frame3;
		public fecherFoundation.FCLabel Label21;
		public fecherFoundation.FCTabPage tabSetup_Page4;
		public FCGrid vsAdjust;
		public fecherFoundation.FCPanel Frame5;
		public fecherFoundation.FCLabel Label22;
		public fecherFoundation.FCTabPage tabSetup_Page5;
		public fecherFoundation.FCPanel fraRateTable_0;
		public fecherFoundation.FCCheckBox chkCombineToMisc;
		public fecherFoundation.FCTextBox txtMinCons_1;
		public fecherFoundation.FCTextBox txtMinCons_0;
		public fecherFoundation.FCTextBox txtRateS_0;
		public fecherFoundation.FCTextBox txtRateS_1;
		public FCGrid vsWMisc;
		public fecherFoundation.FCTextBox txtRateW_1;
		public fecherFoundation.FCTextBox txtRateW_0;
		public FCGrid vsRateTableS;
		public fecherFoundation.FCComboBox cmbRT;
		public FCGrid vsRateTableW;
		public FCGrid vsSMisc;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblRateWater;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCTabPage tabSetup_Page6;
		public FCGrid vsBookDescriptions;
		public fecherFoundation.FCPanel Frame4;
		public fecherFoundation.FCLabel Label10;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileMeterSize;
		public fecherFoundation.FCToolStripMenuItem mnuProcessAddMeterSize;
		public fecherFoundation.FCToolStripMenuItem mnuProcessDeleteMeterSize;
		public fecherFoundation.FCToolStripMenuItem mnuRateTables;
		public fecherFoundation.FCToolStripMenuItem mnuRateTablesNext;
		public fecherFoundation.FCToolStripMenuItem mnuRateTablesPrevious;
		public fecherFoundation.FCToolStripMenuItem mnuRateTablesAdd;
		public fecherFoundation.FCToolStripMenuItem mnuRateTableDelete;
		public fecherFoundation.FCToolStripMenuItem mnuRateTableEditDesc;
		public fecherFoundation.FCToolStripMenuItem mnuFileRateTableListing;
		public fecherFoundation.FCToolStripMenuItem mnuFileRateTableListingCond;
		public fecherFoundation.FCToolStripMenuItem mnuCategory;
		public fecherFoundation.FCToolStripMenuItem mnuAddCategory;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteCategory;
		public fecherFoundation.FCToolStripMenuItem mnuAdjust;
		public fecherFoundation.FCToolStripMenuItem mnuAdjustAdd;
		public fecherFoundation.FCToolStripMenuItem mnuAdjustDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFileBook;
		public fecherFoundation.FCToolStripMenuItem mnuFileBookAdd;
		public fecherFoundation.FCToolStripMenuItem mnuFileBookDelete;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTableFileSetup));
            this.tabSetup = new fecherFoundation.FCTabControl();
            this.tabSetup_Page1 = new fecherFoundation.FCTabPage();
            this.Frame1 = new fecherFoundation.FCPanel();
            this.vsMeterSizeDescList = new fecherFoundation.FCGrid();
            this.Label1 = new fecherFoundation.FCLabel();
            this.tabSetup_Page2 = new fecherFoundation.FCTabPage();
            this.vsFrequency = new fecherFoundation.FCGrid();
            this.Frame2 = new fecherFoundation.FCPanel();
            this.Label20 = new fecherFoundation.FCLabel();
            this.tabSetup_Page3 = new fecherFoundation.FCTabPage();
            this.vsCategory = new fecherFoundation.FCGrid();
            this.Frame3 = new fecherFoundation.FCPanel();
            this.Label21 = new fecherFoundation.FCLabel();
            this.tabSetup_Page4 = new fecherFoundation.FCTabPage();
            this.vsAdjust = new fecherFoundation.FCGrid();
            this.Frame5 = new fecherFoundation.FCPanel();
            this.Label22 = new fecherFoundation.FCLabel();
            this.tabSetup_Page5 = new fecherFoundation.FCTabPage();
            this.fraRateTable_0 = new fecherFoundation.FCPanel();
            this.chkCombineToMisc = new fecherFoundation.FCCheckBox();
            this.txtMinCons_1 = new fecherFoundation.FCTextBox();
            this.txtMinCons_0 = new fecherFoundation.FCTextBox();
            this.txtRateS_0 = new fecherFoundation.FCTextBox();
            this.txtRateS_1 = new fecherFoundation.FCTextBox();
            this.vsWMisc = new fecherFoundation.FCGrid();
            this.txtRateW_1 = new fecherFoundation.FCTextBox();
            this.txtRateW_0 = new fecherFoundation.FCTextBox();
            this.vsRateTableS = new fecherFoundation.FCGrid();
            this.cmbRT = new fecherFoundation.FCComboBox();
            this.vsRateTableW = new fecherFoundation.FCGrid();
            this.vsSMisc = new fecherFoundation.FCGrid();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblRateWater = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.tabSetup_Page6 = new fecherFoundation.FCTabPage();
            this.vsBookDescriptions = new fecherFoundation.FCGrid();
            this.Frame4 = new fecherFoundation.FCPanel();
            this.Label10 = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuRateTableEditDesc = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileRateTableListing = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileRateTableListingCond = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRateTables = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileMeterSize = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessAddMeterSize = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessDeleteMeterSize = new fecherFoundation.FCToolStripMenuItem();
            this.mnuCategory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddCategory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteCategory = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdjust = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdjustAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAdjustDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBook = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBookAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBookDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRateTablesNext = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRateTablesPrevious = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRateTablesAdd = new fecherFoundation.FCToolStripMenuItem();
            this.mnuRateTableDelete = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdProcessAddMeterSize = new fecherFoundation.FCButton();
            this.cmdProcessDeleteMeterSize = new fecherFoundation.FCButton();
            this.cmdRateTablesNext = new fecherFoundation.FCButton();
            this.cmdRateTablesPrevious = new fecherFoundation.FCButton();
            this.cmdRateTablesAdd = new fecherFoundation.FCButton();
            this.cmdRateTableDelete = new fecherFoundation.FCButton();
            this.cmdAddCategory = new fecherFoundation.FCButton();
            this.cmdDeleteCategory = new fecherFoundation.FCButton();
            this.cmdAdjustAdd = new fecherFoundation.FCButton();
            this.cmdAdjustDelete = new fecherFoundation.FCButton();
            this.cmdFileBookAdd = new fecherFoundation.FCButton();
            this.cmdFileBookDelete = new fecherFoundation.FCButton();
            this.javaScript1 = new Wisej.Web.JavaScript(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            this.tabSetup.SuspendLayout();
            this.tabSetup_Page1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMeterSizeDescList)).BeginInit();
            this.tabSetup_Page2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsFrequency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.tabSetup_Page3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.tabSetup_Page4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAdjust)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.tabSetup_Page5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateTable_0)).BeginInit();
            this.fraRateTable_0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCombineToMisc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsWMisc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateTableS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateTableW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSMisc)).BeginInit();
            this.tabSetup_Page6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsBookDescriptions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessAddMeterSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessDeleteMeterSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRateTablesNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRateTablesPrevious)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRateTablesAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRateTableDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdjustAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdjustDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileBookAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileBookDelete)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 598);
            this.BottomPanel.Size = new System.Drawing.Size(898, 90);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.tabSetup);
            this.ClientArea.Size = new System.Drawing.Size(898, 538);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileBookAdd);
            this.TopPanel.Controls.Add(this.cmdFileBookDelete);
            this.TopPanel.Controls.Add(this.cmdRateTablesNext);
            this.TopPanel.Controls.Add(this.cmdRateTablesPrevious);
            this.TopPanel.Controls.Add(this.cmdAdjustAdd);
            this.TopPanel.Controls.Add(this.cmdAdjustDelete);
            this.TopPanel.Controls.Add(this.cmdAddCategory);
            this.TopPanel.Controls.Add(this.cmdDeleteCategory);
            this.TopPanel.Controls.Add(this.cmdRateTablesAdd);
            this.TopPanel.Controls.Add(this.cmdRateTableDelete);
            this.TopPanel.Controls.Add(this.cmdProcessAddMeterSize);
            this.TopPanel.Controls.Add(this.cmdProcessDeleteMeterSize);
            this.TopPanel.Size = new System.Drawing.Size(898, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessDeleteMeterSize, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdProcessAddMeterSize, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRateTableDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRateTablesAdd, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteCategory, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddCategory, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAdjustDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAdjustAdd, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRateTablesPrevious, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRateTablesNext, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileBookDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileBookAdd, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(191, 30);
            this.HeaderText.Text = "Table File Setup";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // tabSetup
            // 
            this.tabSetup.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.tabSetup.Controls.Add(this.tabSetup_Page1);
            this.tabSetup.Controls.Add(this.tabSetup_Page2);
            this.tabSetup.Controls.Add(this.tabSetup_Page3);
            this.tabSetup.Controls.Add(this.tabSetup_Page4);
            this.tabSetup.Controls.Add(this.tabSetup_Page5);
            this.tabSetup.Controls.Add(this.tabSetup_Page6);
            this.javaScript1.SetJavaScript(this.tabSetup, resources.GetString("tabSetup.JavaScript"));
            this.tabSetup.Location = new System.Drawing.Point(30, 30);
            this.tabSetup.Name = "tabSetup";
            this.tabSetup.PageInsets = new Wisej.Web.Padding(1, 47, 1, 1);
            this.tabSetup.Size = new System.Drawing.Size(839, 494);
            this.tabSetup.TabIndex = 11;
            this.tabSetup.Text = "Frequency Codes";
            this.ToolTip1.SetToolTip(this.tabSetup, null);
            this.tabSetup.SelectedIndexChanged += new System.EventHandler(this.tabSetup_SelectedIndexChanged);
            // 
            // tabSetup_Page1
            // 
            this.tabSetup_Page1.Controls.Add(this.Frame1);
            this.tabSetup_Page1.Controls.Add(this.vsMeterSizeDescList);
            this.tabSetup_Page1.Controls.Add(this.Label1);
            this.tabSetup_Page1.Location = new System.Drawing.Point(1, 47);
            this.tabSetup_Page1.Name = "tabSetup_Page1";
            this.tabSetup_Page1.Size = new System.Drawing.Size(837, 446);
            this.tabSetup_Page1.Text = "Meter Size";
            this.ToolTip1.SetToolTip(this.tabSetup_Page1, null);
            // 
            // Frame1
            // 
            this.Frame1.Location = new System.Drawing.Point(19, 60);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(269, 2);
            this.Frame1.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.Frame1, null);
            // 
            // vsMeterSizeDescList
            // 
            //this.vsMeterSizeDescList.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            //| Wisej.Web.AnchorStyles.Left) 
            //| Wisej.Web.AnchorStyles.Right)));
            //this.vsMeterSizeDescList.Cols = 6;
            this.vsMeterSizeDescList.FixedCols = 0;
            this.vsMeterSizeDescList.Location = new System.Drawing.Point(20, 80);
            this.vsMeterSizeDescList.Name = "vsMeterSizeDescList";
            this.vsMeterSizeDescList.RowHeadersVisible = false;
            this.vsMeterSizeDescList.Rows = 1;
            this.vsMeterSizeDescList.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.vsMeterSizeDescList.Size = new System.Drawing.Size(1436, 690);
            this.vsMeterSizeDescList.TabIndex = 35;
            this.ToolTip1.SetToolTip(this.vsMeterSizeDescList, null);
            this.vsMeterSizeDescList.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsMeterSizeDescList_ChangeEdit);
            this.vsMeterSizeDescList.CurrentCellChanged += new System.EventHandler(this.vsMeterSizeDescList_RowColChange);
            this.vsMeterSizeDescList.Click += new System.EventHandler(this.vsMeterSizeDescList_ClickEvent);
            // 
            // Label1
            // 
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.Location = new System.Drawing.Point(20, 30);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(273, 20);
            this.Label1.TabIndex = 32;
            this.Label1.Text = "METER SIZE TABLE";
            this.ToolTip1.SetToolTip(this.Label1, null);
            // 
            // tabSetup_Page2
            // 
            this.tabSetup_Page2.Controls.Add(this.vsFrequency);
            this.tabSetup_Page2.Controls.Add(this.Frame2);
            this.tabSetup_Page2.Controls.Add(this.Label20);
            this.tabSetup_Page2.Location = new System.Drawing.Point(1, 47);
            this.tabSetup_Page2.Name = "tabSetup_Page2";
            this.tabSetup_Page2.Size = new System.Drawing.Size(837, 446);
            this.tabSetup_Page2.Text = "Frequency Codes";
            this.ToolTip1.SetToolTip(this.tabSetup_Page2, null);
            // 
            // vsFrequency
            // 
            //this.vsFrequency.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            //| Wisej.Web.AnchorStyles.Left) 
            //| Wisej.Web.AnchorStyles.Right)));
            this.vsFrequency.Cols = 3;
            this.vsFrequency.FixedCols = 0;
            this.vsFrequency.Location = new System.Drawing.Point(20, 80);
            this.vsFrequency.Name = "vsFrequency";
            this.vsFrequency.RowHeadersVisible = false;
            this.vsFrequency.Rows = 1;
            this.vsFrequency.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.vsFrequency.Size = new System.Drawing.Size(1436, 690);
            this.vsFrequency.TabIndex = 38;
            this.ToolTip1.SetToolTip(this.vsFrequency, null);
            this.vsFrequency.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsFrequency_KeyDownEdit);
            this.vsFrequency.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsFrequency_ChangeEdit);
            this.vsFrequency.CurrentCellChanged += new System.EventHandler(this.vsFrequency_RowColChange);
            this.vsFrequency.Click += new System.EventHandler(this.vsFrequency_ClickEvent);
            // 
            // Frame2
            // 
            this.Frame2.Location = new System.Drawing.Point(20, 60);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(245, 6);
            this.Frame2.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.Frame2, null);
            // 
            // Label20
            // 
            this.Label20.BackColor = System.Drawing.Color.Transparent;
            this.Label20.Location = new System.Drawing.Point(20, 30);
            this.Label20.Name = "Label20";
            this.Label20.Size = new System.Drawing.Size(237, 22);
            this.Label20.TabIndex = 30;
            this.Label20.Text = "FREQUENCY TABLE";
            this.ToolTip1.SetToolTip(this.Label20, null);
            // 
            // tabSetup_Page3
            // 
            this.tabSetup_Page3.Controls.Add(this.vsCategory);
            this.tabSetup_Page3.Controls.Add(this.Frame3);
            this.tabSetup_Page3.Controls.Add(this.Label21);
            this.tabSetup_Page3.Location = new System.Drawing.Point(1, 47);
            this.tabSetup_Page3.Name = "tabSetup_Page3";
            this.tabSetup_Page3.Size = new System.Drawing.Size(837, 446);
            this.tabSetup_Page3.Text = "Category Codes";
            this.ToolTip1.SetToolTip(this.tabSetup_Page3, null);
            // 
            // vsCategory
            // 
            //this.vsCategory.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            //| Wisej.Web.AnchorStyles.Left) 
            //| Wisej.Web.AnchorStyles.Right)));
            this.vsCategory.Cols = 10;
            this.vsCategory.Location = new System.Drawing.Point(20, 90);
            this.vsCategory.Name = "vsCategory";
            this.vsCategory.Rows = 1;
            this.vsCategory.Size = new System.Drawing.Size(1436, 680);
            this.vsCategory.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.vsCategory.StandardTab = false;
            this.vsCategory.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsCategory.TabIndex = 34;
            this.ToolTip1.SetToolTip(this.vsCategory, null);
            this.vsCategory.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsCategory_KeyDownEdit);
            this.vsCategory.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsCategory_KeyPressEdit);
            this.vsCategory.KeyUpEdit += new Wisej.Web.KeyEventHandler(this.vsCategory_KeyUpEdit);
            this.vsCategory.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsCategory_AfterEdit);
            this.vsCategory.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsCategory_ChangeEdit);
            this.vsCategory.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsCategory_BeforeEdit);
            this.vsCategory.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsCategory_ValidateEdit);
            this.vsCategory.CurrentCellChanged += new System.EventHandler(this.vsCategory_RowColChange);
            this.vsCategory.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsCategory_MouseMoveEvent);
            this.vsCategory.Enter += new System.EventHandler(this.vsCategory_Enter);
            this.vsCategory.Leave += new System.EventHandler(this.vsCategory_Leave);
            this.vsCategory.Click += new System.EventHandler(this.vsCategory_ClickEvent);
            this.vsCategory.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsCategory_KeyPressEvent);
            // 
            // Frame3
            // 
            this.Frame3.Location = new System.Drawing.Point(20, 60);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(269, 2);
            this.Frame3.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.Frame3, null);
            // 
            // Label21
            // 
            this.Label21.BackColor = System.Drawing.Color.Transparent;
            this.Label21.Location = new System.Drawing.Point(20, 30);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(261, 20);
            this.Label21.TabIndex = 29;
            this.Label21.Text = "USER CATEGORY TABLE";
            this.ToolTip1.SetToolTip(this.Label21, null);
            // 
            // tabSetup_Page4
            // 
            this.tabSetup_Page4.Controls.Add(this.vsAdjust);
            this.tabSetup_Page4.Controls.Add(this.Frame5);
            this.tabSetup_Page4.Controls.Add(this.Label22);
            this.tabSetup_Page4.Location = new System.Drawing.Point(1, 47);
            this.tabSetup_Page4.Name = "tabSetup_Page4";
            this.tabSetup_Page4.Size = new System.Drawing.Size(837, 446);
            this.tabSetup_Page4.Text = "Account Adjustment";
            this.ToolTip1.SetToolTip(this.tabSetup_Page4, null);
            // 
            // vsAdjust
            // 
            //this.vsAdjust.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            //| Wisej.Web.AnchorStyles.Left) 
            //| Wisej.Web.AnchorStyles.Right)));
            this.vsAdjust.Cols = 6;
            this.vsAdjust.FixedCols = 0;
            this.vsAdjust.Location = new System.Drawing.Point(20, 80);
            this.vsAdjust.Name = "vsAdjust";
            this.vsAdjust.RowHeadersVisible = false;
            this.vsAdjust.Rows = 1;
            this.vsAdjust.Size = new System.Drawing.Size(1436, 680);
            this.vsAdjust.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.vsAdjust.TabIndex = 36;
            this.ToolTip1.SetToolTip(this.vsAdjust, null);
            this.vsAdjust.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsAdjust_ChangeEdit);
            this.vsAdjust.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsAdjust_BeforeEdit);
            this.vsAdjust.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsAdjust_ValidateEdit);
            this.vsAdjust.CurrentCellChanged += new System.EventHandler(this.vsAdjust_RowColChange);
            this.vsAdjust.Enter += new System.EventHandler(this.vsAdjust_Enter);
            this.vsAdjust.Click += new System.EventHandler(this.vsAdjust_ClickEvent);
            this.vsAdjust.EditingControlShowing += new DataGridViewEditingControlShowingEventHandler(this.vsAdjust_EditingControlShowing);
            // 
            // Frame5
            // 
            this.Frame5.Location = new System.Drawing.Point(20, 60);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(269, 2);
            this.Frame5.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.Frame5, null);
            // 
            // Label22
            // 
            this.Label22.BackColor = System.Drawing.Color.Transparent;
            this.Label22.Location = new System.Drawing.Point(20, 30);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(261, 18);
            this.Label22.TabIndex = 28;
            this.Label22.Text = "ACCOUNT ADJUSTMENT TABLE";
            this.ToolTip1.SetToolTip(this.Label22, null);
            // 
            // tabSetup_Page5
            // 
            this.tabSetup_Page5.AutoScroll = true;
            this.tabSetup_Page5.Controls.Add(this.fraRateTable_0);
            this.tabSetup_Page5.Location = new System.Drawing.Point(1, 47);
            this.tabSetup_Page5.Name = "tabSetup_Page5";
            this.tabSetup_Page5.Size = new System.Drawing.Size(837, 446);
            this.tabSetup_Page5.Text = "Rate Tables";
            this.ToolTip1.SetToolTip(this.tabSetup_Page5, null);
            // 
            // fraRateTable_0
            // 
            this.fraRateTable_0.AutoScroll = true;
            this.fraRateTable_0.Controls.Add(this.chkCombineToMisc);
            this.fraRateTable_0.Controls.Add(this.txtMinCons_1);
            this.fraRateTable_0.Controls.Add(this.txtMinCons_0);
            this.fraRateTable_0.Controls.Add(this.txtRateS_0);
            this.fraRateTable_0.Controls.Add(this.txtRateS_1);
            this.fraRateTable_0.Controls.Add(this.vsWMisc);
            this.fraRateTable_0.Controls.Add(this.txtRateW_1);
            this.fraRateTable_0.Controls.Add(this.txtRateW_0);
            this.fraRateTable_0.Controls.Add(this.vsRateTableS);
            this.fraRateTable_0.Controls.Add(this.cmbRT);
            this.fraRateTable_0.Controls.Add(this.vsRateTableW);
            this.fraRateTable_0.Controls.Add(this.vsSMisc);
            this.fraRateTable_0.Controls.Add(this.Label15);
            this.fraRateTable_0.Controls.Add(this.Label7);
            this.fraRateTable_0.Controls.Add(this.Label5);
            this.fraRateTable_0.Controls.Add(this.Label2);
            this.fraRateTable_0.Controls.Add(this.Label3);
            this.fraRateTable_0.Controls.Add(this.Label4);
            this.fraRateTable_0.Controls.Add(this.Label6);
            this.fraRateTable_0.Controls.Add(this.lblRateWater);
            this.fraRateTable_0.Controls.Add(this.Label9);
            this.fraRateTable_0.Controls.Add(this.Label11);
            this.fraRateTable_0.Controls.Add(this.Label12);
            this.fraRateTable_0.Controls.Add(this.Label13);
            this.fraRateTable_0.Controls.Add(this.Label14);
            this.fraRateTable_0.Name = "fraRateTable_0";
            this.fraRateTable_0.Size = new System.Drawing.Size(821, 860);
            this.fraRateTable_0.TabIndex = 17;
            this.fraRateTable_0.Text = "Frame1";
            this.ToolTip1.SetToolTip(this.fraRateTable_0, null);
            // 
            // chkCombineToMisc
            // 
            this.chkCombineToMisc.Location = new System.Drawing.Point(632, 30);
            this.chkCombineToMisc.Name = "chkCombineToMisc";
            this.chkCombineToMisc.Size = new System.Drawing.Size(163, 27);
            this.chkCombineToMisc.TabIndex = 41;
            this.chkCombineToMisc.Text = "Combine to Misc 1";
            this.ToolTip1.SetToolTip(this.chkCombineToMisc, "This will force the fees from this rate key into the misc 1 fees.");
            this.chkCombineToMisc.CheckedChanged += new System.EventHandler(this.chkCombineToMisc_CheckedChanged);
            // 
            // txtMinCons_1
            // 
            this.txtMinCons_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtMinCons_1.Location = new System.Drawing.Point(722, 125);
            this.txtMinCons_1.Name = "txtMinCons_1";
            this.txtMinCons_1.Size = new System.Drawing.Size(80, 40);
            this.txtMinCons_1.TabIndex = 6;
            this.txtMinCons_1.Text = "0.00";
            this.txtMinCons_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtMinCons_1, null);
            this.txtMinCons_1.Enter += new System.EventHandler(this.txtMinCons_Enter);
            this.txtMinCons_1.TextChanged += new System.EventHandler(this.txtMinCons_TextChanged);
            this.txtMinCons_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMinCons_KeyPress);
            // 
            // txtMinCons_0
            // 
            this.txtMinCons_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtMinCons_0.Location = new System.Drawing.Point(316, 125);
            this.txtMinCons_0.Name = "txtMinCons_0";
            this.txtMinCons_0.Size = new System.Drawing.Size(80, 40);
            this.txtMinCons_0.TabIndex = 1;
            this.txtMinCons_0.Text = "0.00";
            this.txtMinCons_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtMinCons_0, null);
            this.txtMinCons_0.Enter += new System.EventHandler(this.txtMinCons_Enter);
            this.txtMinCons_0.TextChanged += new System.EventHandler(this.txtMinCons_TextChanged);
            this.txtMinCons_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMinCons_KeyPress);
            // 
            // txtRateS_0
            // 
            this.txtRateS_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtRateS_0.Location = new System.Drawing.Point(722, 185);
            this.txtRateS_0.Name = "txtRateS_0";
            this.txtRateS_0.Size = new System.Drawing.Size(80, 40);
            this.txtRateS_0.TabIndex = 7;
            this.txtRateS_0.Text = "0.00";
            this.txtRateS_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtRateS_0, null);
            this.txtRateS_0.Enter += new System.EventHandler(this.txtRateS_Enter);
            this.txtRateS_0.TextChanged += new System.EventHandler(this.txtRateS_TextChanged);
            this.txtRateS_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtRateS_Validating);
            this.txtRateS_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRateS_KeyPress);
            // 
            // txtRateS_1
            // 
            this.txtRateS_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtRateS_1.Location = new System.Drawing.Point(722, 245);
            this.txtRateS_1.Name = "txtRateS_1";
            this.txtRateS_1.Size = new System.Drawing.Size(80, 40);
            this.txtRateS_1.TabIndex = 8;
            this.txtRateS_1.Text = "0.00";
            this.txtRateS_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtRateS_1, null);
            this.txtRateS_1.Enter += new System.EventHandler(this.txtRateS_Enter);
            this.txtRateS_1.TextChanged += new System.EventHandler(this.txtRateS_TextChanged);
            this.txtRateS_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtRateS_Validating);
            this.txtRateS_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRateS_KeyPress);
            // 
            // vsWMisc
            // 
            this.vsWMisc.Cols = 4;
            this.vsWMisc.FixedCols = 0;
            this.vsWMisc.Location = new System.Drawing.Point(20, 340);
            this.vsWMisc.Name = "vsWMisc";
            this.vsWMisc.RowHeadersVisible = false;
            this.vsWMisc.Rows = 3;
            this.vsWMisc.Size = new System.Drawing.Size(376, 114);
            this.vsWMisc.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.vsWMisc, null);
            this.vsWMisc.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsWMisc_KeyPressEdit);
            this.vsWMisc.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWMisc_ChangeEdit);
            this.vsWMisc.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWMisc_ValidateEdit);
            this.vsWMisc.CurrentCellChanged += new System.EventHandler(this.vsWMisc_RowColChange);
            // 
            // txtRateW_1
            // 
            this.txtRateW_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtRateW_1.Location = new System.Drawing.Point(316, 245);
            this.txtRateW_1.Name = "txtRateW_1";
            this.txtRateW_1.Size = new System.Drawing.Size(80, 40);
            this.txtRateW_1.TabIndex = 3;
            this.txtRateW_1.Text = "0.00";
            this.txtRateW_1.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtRateW_1, null);
            this.txtRateW_1.Enter += new System.EventHandler(this.txtRateW_Enter);
            this.txtRateW_1.TextChanged += new System.EventHandler(this.txtRateW_TextChanged);
            this.txtRateW_1.Validating += new System.ComponentModel.CancelEventHandler(this.txtRateW_Validating);
            this.txtRateW_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRateW_KeyPress);
            // 
            // txtRateW_0
            // 
            this.txtRateW_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtRateW_0.Location = new System.Drawing.Point(316, 185);
            this.txtRateW_0.Name = "txtRateW_0";
            this.txtRateW_0.Size = new System.Drawing.Size(80, 40);
            this.txtRateW_0.TabIndex = 2;
            this.txtRateW_0.Text = "0.00";
            this.txtRateW_0.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtRateW_0, null);
            this.txtRateW_0.Enter += new System.EventHandler(this.txtRateW_Enter);
            this.txtRateW_0.TextChanged += new System.EventHandler(this.txtRateW_TextChanged);
            this.txtRateW_0.Validating += new System.ComponentModel.CancelEventHandler(this.txtRateW_Validating);
            this.txtRateW_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRateW_KeyPress);
            // 
            // vsRateTableS
            // 
            this.vsRateTableS.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.vsRateTableS.Cols = 3;
            this.vsRateTableS.ExtendLastCol = true;
            this.vsRateTableS.Location = new System.Drawing.Point(426, 474);
            this.vsRateTableS.Name = "vsRateTableS";
            this.vsRateTableS.Rows = 9;
            this.vsRateTableS.Size = new System.Drawing.Size(376, 367);
            this.vsRateTableS.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.vsRateTableS, null);
            this.vsRateTableS.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsRateTableS_KeyDownEdit);
            this.vsRateTableS.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsRateTableS_KeyPressEdit);
            this.vsRateTableS.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsRateTableS_AfterEdit);
            this.vsRateTableS.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsRateTableS_ChangeEdit);
            this.vsRateTableS.CurrentCellChanged += new System.EventHandler(this.vsRateTableS_RowColChange);
            this.vsRateTableS.Enter += new System.EventHandler(this.vsRateTableS_Enter);
            this.vsRateTableS.Click += new System.EventHandler(this.vsRateTableS_ClickEvent);
            // 
            // cmbRT
            // 
            this.cmbRT.BackColor = System.Drawing.SystemColors.Window;
            this.cmbRT.Location = new System.Drawing.Point(153, 30);
            this.cmbRT.Name = "cmbRT";
            this.cmbRT.Size = new System.Drawing.Size(450, 40);
            this.cmbRT.Sorted = true;
            this.ToolTip1.SetToolTip(this.cmbRT, "Choose a Rate Table to be displayed.");
            this.cmbRT.SelectedIndexChanged += new System.EventHandler(this.cmbRT_SelectedIndexChanged);
            // 
            // vsRateTableW
            // 
            this.vsRateTableW.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.vsRateTableW.Cols = 10;
            this.vsRateTableW.ExtendLastCol = true;
            this.vsRateTableW.Location = new System.Drawing.Point(20, 474);
            this.vsRateTableW.Name = "vsRateTableW";
            this.vsRateTableW.Rows = 9;
            this.vsRateTableW.Size = new System.Drawing.Size(376, 367);
            this.vsRateTableW.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.vsRateTableW, null);
            this.vsRateTableW.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsRateTableW_KeyDownEdit);
            this.vsRateTableW.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsRateTableW_KeyPressEdit);
            this.vsRateTableW.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsRateTableW_AfterEdit);
            this.vsRateTableW.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsRateTableW_ChangeEdit);
            this.vsRateTableW.CurrentCellChanged += new System.EventHandler(this.vsRateTableW_RowColChange);
            this.vsRateTableW.Enter += new System.EventHandler(this.vsRateTableW_Enter);
            this.vsRateTableW.Click += new System.EventHandler(this.vsRateTableW_ClickEvent);
            // 
            // vsSMisc
            // 
            this.vsSMisc.Cols = 4;
            this.vsSMisc.FixedCols = 0;
            this.vsSMisc.Location = new System.Drawing.Point(426, 340);
            this.vsSMisc.Name = "vsSMisc";
            this.vsSMisc.RowHeadersVisible = false;
            this.vsSMisc.Rows = 3;
            this.vsSMisc.Size = new System.Drawing.Size(376, 114);
            this.vsSMisc.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.vsSMisc, null);
            this.vsSMisc.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsSMisc_KeyPressEdit);
            this.vsSMisc.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsSMisc_ChangeEdit);
            this.vsSMisc.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsSMisc_ValidateEdit);
            this.vsSMisc.CurrentCellChanged += new System.EventHandler(this.vsSMisc_RowColChange);
            // 
            // Label15
            // 
            this.Label15.BackColor = System.Drawing.Color.Transparent;
            this.Label15.Location = new System.Drawing.Point(422, 305);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(374, 15);
            this.Label15.TabIndex = 40;
            this.Label15.Text = "MISC CHARGES";
            this.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTip1.SetToolTip(this.Label15, null);
            // 
            // Label7
            // 
            this.Label7.BackColor = System.Drawing.Color.Transparent;
            this.Label7.Location = new System.Drawing.Point(20, 305);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(370, 15);
            this.Label7.TabIndex = 39;
            this.Label7.Text = "MISC CHARGES";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // Label5
            // 
            this.Label5.BackColor = System.Drawing.Color.Transparent;
            this.Label5.Location = new System.Drawing.Point(16, 44);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(90, 18);
            this.Label5.TabIndex = 33;
            this.Label5.Text = "RATE TABLE";
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label2
            // 
            this.Label2.BackColor = System.Drawing.Color.Transparent;
            this.Label2.Location = new System.Drawing.Point(20, 139);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(59, 15);
            this.Label2.TabIndex = 27;
            this.Label2.Text = "MINIMUM";
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // Label3
            // 
            this.Label3.BackColor = System.Drawing.Color.Transparent;
            this.Label3.Location = new System.Drawing.Point(103, 139);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(168, 15);
            this.Label3.TabIndex = 26;
            this.Label3.Text = "CONSUMPTION OR UNITS";
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // Label4
            // 
            this.Label4.BackColor = System.Drawing.Color.Transparent;
            this.Label4.Location = new System.Drawing.Point(103, 199);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(126, 15);
            this.Label4.TabIndex = 25;
            this.Label4.Text = "CHARGE";
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label6
            // 
            this.Label6.BackColor = System.Drawing.Color.Transparent;
            this.Label6.Location = new System.Drawing.Point(20, 259);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(106, 15);
            this.Label6.TabIndex = 24;
            this.Label6.Text = "FLAT RATE";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // lblRateWater
            // 
            this.lblRateWater.BackColor = System.Drawing.Color.Transparent;
            this.lblRateWater.Location = new System.Drawing.Point(20, 90);
            this.lblRateWater.Name = "lblRateWater";
            this.lblRateWater.Size = new System.Drawing.Size(366, 15);
            this.lblRateWater.TabIndex = 23;
            this.lblRateWater.Text = "WATER";
            this.lblRateWater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTip1.SetToolTip(this.lblRateWater, null);
            // 
            // Label9
            // 
            this.Label9.BackColor = System.Drawing.Color.Transparent;
            this.Label9.Location = new System.Drawing.Point(422, 90);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(370, 15);
            this.Label9.TabIndex = 22;
            this.Label9.Text = "SEWER";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ToolTip1.SetToolTip(this.Label9, null);
            // 
            // Label11
            // 
            this.Label11.BackColor = System.Drawing.Color.Transparent;
            this.Label11.Location = new System.Drawing.Point(426, 139);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(59, 15);
            this.Label11.TabIndex = 21;
            this.Label11.Text = "MINIMUM";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // Label12
            // 
            this.Label12.BackColor = System.Drawing.Color.Transparent;
            this.Label12.Location = new System.Drawing.Point(507, 139);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(152, 15);
            this.Label12.TabIndex = 20;
            this.Label12.Text = "CONSUMPTION OR UNITS";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // Label13
            // 
            this.Label13.BackColor = System.Drawing.Color.Transparent;
            this.Label13.Location = new System.Drawing.Point(507, 199);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(126, 15);
            this.Label13.TabIndex = 19;
            this.Label13.Text = "CHARGE";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // Label14
            // 
            this.Label14.BackColor = System.Drawing.Color.Transparent;
            this.Label14.Location = new System.Drawing.Point(426, 259);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(106, 15);
            this.Label14.TabIndex = 18;
            this.Label14.Text = "FLAT RATE";
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // tabSetup_Page6
            // 
            this.tabSetup_Page6.Controls.Add(this.vsBookDescriptions);
            this.tabSetup_Page6.Controls.Add(this.Frame4);
            this.tabSetup_Page6.Controls.Add(this.Label10);
            this.tabSetup_Page6.Location = new System.Drawing.Point(1, 47);
            this.tabSetup_Page6.Name = "tabSetup_Page6";
            this.tabSetup_Page6.Size = new System.Drawing.Size(837, 446);
            this.tabSetup_Page6.Text = "Book Descriptions";
            this.ToolTip1.SetToolTip(this.tabSetup_Page6, null);
            // 
            // vsBookDescriptions
            // 
            //this.vsBookDescriptions.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            //| Wisej.Web.AnchorStyles.Left) 
            //| Wisej.Web.AnchorStyles.Right)));
            this.vsBookDescriptions.Cols = 8;
            this.vsBookDescriptions.FixedCols = 0;
            this.vsBookDescriptions.Location = new System.Drawing.Point(20, 80);
            this.vsBookDescriptions.Name = "vsBookDescriptions";
            this.vsBookDescriptions.RowHeadersVisible = false;
            this.vsBookDescriptions.Rows = 1;
            this.vsBookDescriptions.Size = new System.Drawing.Size(1436, 689);
            this.vsBookDescriptions.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            this.vsBookDescriptions.TabIndex = 37;
            this.ToolTip1.SetToolTip(this.vsBookDescriptions, null);
            this.vsBookDescriptions.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsBookDescriptions_KeyDownEdit);
            this.vsBookDescriptions.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsBookDescriptions_ChangeEdit);
            this.vsBookDescriptions.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsBookDescriptions_ValidateEdit);
            this.vsBookDescriptions.CurrentCellChanged += new System.EventHandler(this.vsBookDescriptions_RowColChange);
            // 
            // Frame4
            // 
            this.Frame4.Location = new System.Drawing.Point(20, 60);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(267, 2);
            this.Frame4.TabIndex = 13;
            this.ToolTip1.SetToolTip(this.Frame4, null);
            // 
            // Label10
            // 
            this.Label10.BackColor = System.Drawing.Color.Transparent;
            this.Label10.Location = new System.Drawing.Point(20, 30);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(261, 22);
            this.Label10.TabIndex = 31;
            this.Label10.Text = "BOOK DESCRIPTION TABLE";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuRateTableEditDesc,
            this.mnuFileRateTableListing,
            this.mnuFileRateTableListingCond});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuRateTableEditDesc
            // 
            this.mnuRateTableEditDesc.Index = 0;
            this.mnuRateTableEditDesc.Name = "mnuRateTableEditDesc";
            this.mnuRateTableEditDesc.Text = "Edit Description";
            this.mnuRateTableEditDesc.Click += new System.EventHandler(this.mnuRateTableEditDesc_Click);
            // 
            // mnuFileRateTableListing
            // 
            this.mnuFileRateTableListing.Index = 1;
            this.mnuFileRateTableListing.Name = "mnuFileRateTableListing";
            this.mnuFileRateTableListing.Text = "Rate Table Listing";
            this.mnuFileRateTableListing.Click += new System.EventHandler(this.mnuFileRateTableListing_Click);
            // 
            // mnuFileRateTableListingCond
            // 
            this.mnuFileRateTableListingCond.Index = 2;
            this.mnuFileRateTableListingCond.Name = "mnuFileRateTableListingCond";
            this.mnuFileRateTableListingCond.Text = "Rate Table Listing Condensed";
            this.mnuFileRateTableListingCond.Click += new System.EventHandler(this.mnuFileRateTableListingCond_Click);
            // 
            // mnuRateTables
            // 
            this.mnuRateTables.Index = -1;
            this.mnuRateTables.Name = "mnuRateTables";
            this.mnuRateTables.Text = "Rate Tables";
            this.mnuRateTables.Visible = false;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileMeterSize,
            this.mnuCategory,
            this.mnuAdjust,
            this.mnuFileBook,
            this.mnuProcessSave,
            this.mnuFileSaveExit,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileMeterSize
            // 
            this.mnuFileMeterSize.Index = 0;
            this.mnuFileMeterSize.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuProcessAddMeterSize,
            this.mnuProcessDeleteMeterSize});
            this.mnuFileMeterSize.Name = "mnuFileMeterSize";
            this.mnuFileMeterSize.Text = "Meter Size";
            // 
            // mnuProcessAddMeterSize
            // 
            this.mnuProcessAddMeterSize.Index = 0;
            this.mnuProcessAddMeterSize.Name = "mnuProcessAddMeterSize";
            this.mnuProcessAddMeterSize.Text = "Add Meter Size";
            this.mnuProcessAddMeterSize.Click += new System.EventHandler(this.mnuProcessAddMeterSize_Click);
            // 
            // mnuProcessDeleteMeterSize
            // 
            this.mnuProcessDeleteMeterSize.Index = 1;
            this.mnuProcessDeleteMeterSize.Name = "mnuProcessDeleteMeterSize";
            this.mnuProcessDeleteMeterSize.Text = "Delete Meter Size";
            this.mnuProcessDeleteMeterSize.Click += new System.EventHandler(this.mnuProcessDeleteMeterSize_Click);
            // 
            // mnuCategory
            // 
            this.mnuCategory.Index = 1;
            this.mnuCategory.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddCategory,
            this.mnuDeleteCategory});
            this.mnuCategory.Name = "mnuCategory";
            this.mnuCategory.Text = "Category";
            this.mnuCategory.Visible = false;
            // 
            // mnuAddCategory
            // 
            this.mnuAddCategory.Index = 0;
            this.mnuAddCategory.Name = "mnuAddCategory";
            this.mnuAddCategory.Text = "Add Category";
            this.mnuAddCategory.Click += new System.EventHandler(this.mnuAddCategory_Click);
            // 
            // mnuDeleteCategory
            // 
            this.mnuDeleteCategory.Index = 1;
            this.mnuDeleteCategory.Name = "mnuDeleteCategory";
            this.mnuDeleteCategory.Text = "Delete Cetegory";
            this.mnuDeleteCategory.Click += new System.EventHandler(this.mnuDeleteCategory_Click);
            // 
            // mnuAdjust
            // 
            this.mnuAdjust.Index = 2;
            this.mnuAdjust.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAdjustAdd,
            this.mnuAdjustDelete});
            this.mnuAdjust.Name = "mnuAdjust";
            this.mnuAdjust.Text = "Adjustments";
            this.mnuAdjust.Visible = false;
            // 
            // mnuAdjustAdd
            // 
            this.mnuAdjustAdd.Index = 0;
            this.mnuAdjustAdd.Name = "mnuAdjustAdd";
            this.mnuAdjustAdd.Text = "Add Adjustment";
            this.mnuAdjustAdd.Click += new System.EventHandler(this.mnuAdjustAdd_Click);
            // 
            // mnuAdjustDelete
            // 
            this.mnuAdjustDelete.Index = 1;
            this.mnuAdjustDelete.Name = "mnuAdjustDelete";
            this.mnuAdjustDelete.Text = "Delete Adjustment";
            this.mnuAdjustDelete.Click += new System.EventHandler(this.mnuAdjustDelete_Click);
            // 
            // mnuFileBook
            // 
            this.mnuFileBook.Index = 3;
            this.mnuFileBook.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileBookAdd,
            this.mnuFileBookDelete});
            this.mnuFileBook.Name = "mnuFileBook";
            this.mnuFileBook.Text = "Book Descriptions";
            this.mnuFileBook.Visible = false;
            // 
            // mnuFileBookAdd
            // 
            this.mnuFileBookAdd.Index = 0;
            this.mnuFileBookAdd.Name = "mnuFileBookAdd";
            this.mnuFileBookAdd.Text = "Add Book";
            this.mnuFileBookAdd.Click += new System.EventHandler(this.mnuFileBookAdd_Click);
            // 
            // mnuFileBookDelete
            // 
            this.mnuFileBookDelete.Index = 1;
            this.mnuFileBookDelete.Name = "mnuFileBookDelete";
            this.mnuFileBookDelete.Text = "Delete Book";
            this.mnuFileBookDelete.Click += new System.EventHandler(this.mnuFileBookDelete_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 4;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuProcessSave.Text = "Save";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // mnuFileSaveExit
            // 
            this.mnuFileSaveExit.Index = 5;
            this.mnuFileSaveExit.Name = "mnuFileSaveExit";
            this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveExit.Text = "Save & Exit";
            this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 6;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 7;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // mnuRateTablesNext
            // 
            this.mnuRateTablesNext.Index = -1;
            this.mnuRateTablesNext.Name = "mnuRateTablesNext";
            this.mnuRateTablesNext.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuRateTablesNext.Text = "Move Next";
            this.mnuRateTablesNext.Click += new System.EventHandler(this.mnuRateTablesNext_Click);
            // 
            // mnuRateTablesPrevious
            // 
            this.mnuRateTablesPrevious.Index = -1;
            this.mnuRateTablesPrevious.Name = "mnuRateTablesPrevious";
            this.mnuRateTablesPrevious.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuRateTablesPrevious.Text = "Move Previous";
            this.mnuRateTablesPrevious.Click += new System.EventHandler(this.mnuRateTablesPrevious_Click);
            // 
            // mnuRateTablesAdd
            // 
            this.mnuRateTablesAdd.Index = -1;
            this.mnuRateTablesAdd.Name = "mnuRateTablesAdd";
            this.mnuRateTablesAdd.Text = "Add Rate Table";
            this.mnuRateTablesAdd.Click += new System.EventHandler(this.mnuRateTablesAdd_Click);
            // 
            // mnuRateTableDelete
            // 
            this.mnuRateTableDelete.Index = -1;
            this.mnuRateTableDelete.Name = "mnuRateTableDelete";
            this.mnuRateTableDelete.Text = "Delete Rate Table";
            this.mnuRateTableDelete.Click += new System.EventHandler(this.mnuRateTableDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(331, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // cmdProcessAddMeterSize
            // 
            this.cmdProcessAddMeterSize.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessAddMeterSize.Location = new System.Drawing.Point(778, 29);
            this.cmdProcessAddMeterSize.Name = "cmdProcessAddMeterSize";
            this.cmdProcessAddMeterSize.Size = new System.Drawing.Size(112, 24);
            this.cmdProcessAddMeterSize.TabIndex = 1;
            this.cmdProcessAddMeterSize.Text = "Add Meter Size";
            this.cmdProcessAddMeterSize.Click += new System.EventHandler(this.mnuProcessAddMeterSize_Click);
            // 
            // cmdProcessDeleteMeterSize
            // 
            this.cmdProcessDeleteMeterSize.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdProcessDeleteMeterSize.Location = new System.Drawing.Point(748, 29);
            this.cmdProcessDeleteMeterSize.Name = "cmdProcessDeleteMeterSize";
            this.cmdProcessDeleteMeterSize.Size = new System.Drawing.Size(128, 24);
            this.cmdProcessDeleteMeterSize.TabIndex = 2;
            this.cmdProcessDeleteMeterSize.Text = "Delete Meter Size";
            this.cmdProcessDeleteMeterSize.Click += new System.EventHandler(this.mnuProcessDeleteMeterSize_Click);
            // 
            // cmdRateTablesNext
            // 
            this.cmdRateTablesNext.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRateTablesNext.Location = new System.Drawing.Point(655, 29);
            this.cmdRateTablesNext.Name = "cmdRateTablesNext";
            this.cmdRateTablesNext.Shortcut = Wisej.Web.Shortcut.F8;
            this.cmdRateTablesNext.Size = new System.Drawing.Size(84, 24);
            this.cmdRateTablesNext.TabIndex = 3;
            this.cmdRateTablesNext.Text = "Move Next";
            this.cmdRateTablesNext.Visible = false;
            this.cmdRateTablesNext.Click += new System.EventHandler(this.mnuRateTablesNext_Click);
            // 
            // cmdRateTablesPrevious
            // 
            this.cmdRateTablesPrevious.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRateTablesPrevious.Location = new System.Drawing.Point(623, 29);
            this.cmdRateTablesPrevious.Name = "cmdRateTablesPrevious";
            this.cmdRateTablesPrevious.Shortcut = Wisej.Web.Shortcut.F7;
            this.cmdRateTablesPrevious.Size = new System.Drawing.Size(106, 24);
            this.cmdRateTablesPrevious.TabIndex = 4;
            this.cmdRateTablesPrevious.Text = "Move Previous";
            this.cmdRateTablesPrevious.Visible = false;
            this.cmdRateTablesPrevious.Click += new System.EventHandler(this.mnuRateTablesPrevious_Click);
            // 
            // cmdRateTablesAdd
            // 
            this.cmdRateTablesAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRateTablesAdd.Location = new System.Drawing.Point(608, 29);
            this.cmdRateTablesAdd.Name = "cmdRateTablesAdd";
            this.cmdRateTablesAdd.Size = new System.Drawing.Size(110, 24);
            this.cmdRateTablesAdd.TabIndex = 5;
            this.cmdRateTablesAdd.Text = "Add Rate Table";
            this.cmdRateTablesAdd.Visible = false;
            this.cmdRateTablesAdd.Click += new System.EventHandler(this.mnuRateTablesAdd_Click);
            // 
            // cmdRateTableDelete
            // 
            this.cmdRateTableDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRateTableDelete.Location = new System.Drawing.Point(579, 29);
            this.cmdRateTableDelete.Name = "cmdRateTableDelete";
            this.cmdRateTableDelete.Size = new System.Drawing.Size(128, 24);
            this.cmdRateTableDelete.TabIndex = 6;
            this.cmdRateTableDelete.Text = "Delete Rate Table";
            this.cmdRateTableDelete.Visible = false;
            this.cmdRateTableDelete.Click += new System.EventHandler(this.mnuRateTableDelete_Click);
            // 
            // cmdAddCategory
            // 
            this.cmdAddCategory.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddCategory.Location = new System.Drawing.Point(466, 29);
            this.cmdAddCategory.Name = "cmdAddCategory";
            this.cmdAddCategory.Size = new System.Drawing.Size(104, 24);
            this.cmdAddCategory.TabIndex = 7;
            this.cmdAddCategory.Text = "Add Category";
            this.cmdAddCategory.Visible = false;
            this.cmdAddCategory.Click += new System.EventHandler(this.mnuAddCategory_Click);
            // 
            // cmdDeleteCategory
            // 
            this.cmdDeleteCategory.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteCategory.Location = new System.Drawing.Point(438, 29);
            this.cmdDeleteCategory.Name = "cmdDeleteCategory";
            this.cmdDeleteCategory.Size = new System.Drawing.Size(120, 24);
            this.cmdDeleteCategory.TabIndex = 8;
            this.cmdDeleteCategory.Text = "Delete Cetegory";
            this.cmdDeleteCategory.Visible = false;
            this.cmdDeleteCategory.Click += new System.EventHandler(this.mnuDeleteCategory_Click);
            // 
            // cmdAdjustAdd
            // 
            this.cmdAdjustAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAdjustAdd.Location = new System.Drawing.Point(607, 4);
            this.cmdAdjustAdd.Name = "cmdAdjustAdd";
            this.cmdAdjustAdd.Size = new System.Drawing.Size(116, 24);
            this.cmdAdjustAdd.TabIndex = 9;
            this.cmdAdjustAdd.Text = "Add Adjustment";
            this.cmdAdjustAdd.Visible = false;
            this.cmdAdjustAdd.Click += new System.EventHandler(this.mnuAdjustAdd_Click);
            // 
            // cmdAdjustDelete
            // 
            this.cmdAdjustDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAdjustDelete.Location = new System.Drawing.Point(584, 4);
            this.cmdAdjustDelete.Name = "cmdAdjustDelete";
            this.cmdAdjustDelete.Size = new System.Drawing.Size(130, 24);
            this.cmdAdjustDelete.TabIndex = 10;
            this.cmdAdjustDelete.Text = "Delete Adjustment";
            this.cmdAdjustDelete.Visible = false;
            this.cmdAdjustDelete.Click += new System.EventHandler(this.mnuAdjustDelete_Click);
            // 
            // cmdFileBookAdd
            // 
            this.cmdFileBookAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileBookAdd.Location = new System.Drawing.Point(492, 4);
            this.cmdFileBookAdd.Name = "cmdFileBookAdd";
            this.cmdFileBookAdd.Size = new System.Drawing.Size(76, 24);
            this.cmdFileBookAdd.TabIndex = 11;
            this.cmdFileBookAdd.Text = "Add Book";
            this.cmdFileBookAdd.Visible = false;
            this.cmdFileBookAdd.Click += new System.EventHandler(this.mnuFileBookAdd_Click);
            // 
            // cmdFileBookDelete
            // 
            this.cmdFileBookDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileBookDelete.Location = new System.Drawing.Point(464, 4);
            this.cmdFileBookDelete.Name = "cmdFileBookDelete";
            this.cmdFileBookDelete.Size = new System.Drawing.Size(94, 24);
            this.cmdFileBookDelete.TabIndex = 12;
            this.cmdFileBookDelete.Text = "Delete Book";
            this.cmdFileBookDelete.Visible = false;
            this.cmdFileBookDelete.Click += new System.EventHandler(this.mnuFileBookDelete_Click);
            // 
            // frmTableFileSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(898, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmTableFileSetup";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "TRIO Software Corporation";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmTableFileSetup_Load);
            this.Activated += new System.EventHandler(this.frmTableFileSetup_Activated);
            this.Resize += new System.EventHandler(this.frmTableFileSetup_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmTableFileSetup_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTableFileSetup_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            this.tabSetup.ResumeLayout(false);
            this.tabSetup_Page1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsMeterSizeDescList)).EndInit();
            this.tabSetup_Page2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsFrequency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.tabSetup_Page3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.tabSetup_Page4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsAdjust)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.tabSetup_Page5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraRateTable_0)).EndInit();
            this.fraRateTable_0.ResumeLayout(false);
            this.fraRateTable_0.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkCombineToMisc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsWMisc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateTableS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateTableW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSMisc)).EndInit();
            this.tabSetup_Page6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsBookDescriptions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessAddMeterSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessDeleteMeterSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRateTablesNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRateTablesPrevious)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRateTablesAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRateTableDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdjustAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdjustDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileBookAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileBookDelete)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdProcessDeleteMeterSize;
		private FCButton cmdProcessAddMeterSize;
		private FCButton cmdRateTableDelete;
		private FCButton cmdRateTablesAdd;
		private FCButton cmdRateTablesPrevious;
		private FCButton cmdRateTablesNext;
		private FCButton cmdDeleteCategory;
		private FCButton cmdAddCategory;
		private FCButton cmdFileBookDelete;
		private FCButton cmdFileBookAdd;
		private FCButton cmdAdjustDelete;
		private FCButton cmdAdjustAdd;
		private JavaScript javaScript1;
	}
}
