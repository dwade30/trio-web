﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmConsumptionHistoryReport.
	/// </summary>
	public partial class frmConsumptionHistoryReport : BaseForm
	{
		public frmConsumptionHistoryReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmConsumptionHistoryReport InstancePtr
		{
			get
			{
				return (frmConsumptionHistoryReport)Sys.GetInstance(typeof(frmConsumptionHistoryReport));
			}
		}

		protected frmConsumptionHistoryReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int[] lngBooks = null;
		string strKeys;
		string strDateRanges;
		int SelectCol;
		int DescriptionCol;
		bool blnWater;

		public void Init(ref int[] PassBookArray, ref string strRateKeys, ref string strService, string strPassDateRanges = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngBooks = PassBookArray;
				strKeys = strRateKeys;
				strDateRanges = strPassDateRanges;
				if (strService == "W")
				{
					blnWater = true;
				}
				else
				{
					blnWater = false;
				}
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmConsumptionHistoryReport_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmConsumptionHistoryReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmConsumptionHistoryReport properties;
			//frmConsumptionHistoryReport.FillStyle	= 0;
			//frmConsumptionHistoryReport.ScaleWidth	= 9045;
			//frmConsumptionHistoryReport.ScaleHeight	= 7230;
			//frmConsumptionHistoryReport.LinkTopic	= "Form2";
			//frmConsumptionHistoryReport.LockControls	= true;
			//frmConsumptionHistoryReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SelectCol = 0;
			DescriptionCol = 1;
			FormatGrids();
			FillGrids();
		}

		private void FillGrids()
		{
			clsDRWrapper rs = new clsDRWrapper();
			vsGrid.Rows = 1;
			vsGrid.Rows = 6;
			// kk11032014 trout-1050  Add Date Range
			// show the array titles
			vsGrid.TextMatrix(0, 0, "Criteria");
			vsGrid.TextMatrix(0, 1, "Beginning");
			vsGrid.TextMatrix(0, 2, "Ending");
			vsGrid.TextMatrix(1, 0, "Name");
			vsGrid.TextMatrix(2, 0, "Account");
			vsGrid.TextMatrix(3, 0, "Consumption");
			vsGrid.TextMatrix(4, 0, "Bill Amount");
			vsGrid.TextMatrix(5, 0, "Date");
			rs.OpenRecordset("SELECT * FROM Frequency WHERE RTrim(LongDescription) <> '' ORDER BY LongDescription", modExtraModules.strUTDatabase);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsFrequency.Rows += 1;
					vsFrequency.TextMatrix(vsFrequency.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsFrequency.TextMatrix(vsFrequency.Rows - 1, DescriptionCol, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					vsFrequency.RowData(vsFrequency.Rows - 1, rs.Get_Fields("Code"));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			rs.OpenRecordset("SELECT * FROM Category WHERE RTrim(LongDescription) <> '' ORDER BY LongDescription", modExtraModules.strUTDatabase);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsCategory.Rows += 1;
					vsCategory.TextMatrix(vsCategory.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsCategory.TextMatrix(vsCategory.Rows - 1, DescriptionCol, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					vsCategory.RowData(vsCategory.Rows - 1, rs.Get_Fields("Code"));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
			rs.OpenRecordset("SELECT * FROM MeterSizes WHERE RTrim(LongDescription) <> '' ORDER BY LongDescription", modExtraModules.strUTDatabase);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					vsMeterSize.Rows += 1;
					vsMeterSize.TextMatrix(vsMeterSize.Rows - 1, SelectCol, FCConvert.ToString(false));
					vsMeterSize.TextMatrix(vsMeterSize.Rows - 1, DescriptionCol, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					vsMeterSize.RowData(vsMeterSize.Rows - 1, rs.Get_Fields("Code"));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void FormatGrids()
		{
			vsGrid.Cols = 3;
			vsGrid.ColWidth(0, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.36));
			// Question
			vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.3));
			// Answer
			vsGrid.ColWidth(1, FCConvert.ToInt32(vsGrid.WidthOriginal * 0.3));
			// Answer Range
			vsGrid.ExtendLastCol = true;
			// align the columns
			vsGrid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrid.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsGrid.FixedRows = 1;
			// header row
			//vsGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsCategory.ColWidth(SelectCol, FCConvert.ToInt32(vsCategory.WidthOriginal * 0.2));
			vsCategory.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsCategory.TextMatrix(0, SelectCol, "");
			vsCategory.TextMatrix(0, DescriptionCol, "Description");
			vsFrequency.ColWidth(SelectCol, FCConvert.ToInt32(vsFrequency.WidthOriginal * 0.2));
			vsFrequency.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsFrequency.TextMatrix(0, SelectCol, "");
			vsFrequency.TextMatrix(0, DescriptionCol, "Description");
			vsMeterSize.ColWidth(SelectCol, FCConvert.ToInt32(vsMeterSize.WidthOriginal * 0.2));
			vsMeterSize.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			vsMeterSize.TextMatrix(0, SelectCol, "");
			vsMeterSize.TextMatrix(0, DescriptionCol, "Description");
		}

		private void frmConsumptionHistoryReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmConsumptionHistoryReport_Resize(object sender, System.EventArgs e)
		{
			FormatGrids();
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (ValidateEntries())
			{
				rptConsumptionHistoryComplete.InstancePtr.Init(ref lngBooks, ref strKeys, ref blnWater, false, strDateRanges);
			}
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (ValidateEntries())
			{
				rptConsumptionHistoryComplete.InstancePtr.Init(ref lngBooks, ref strKeys, ref blnWater, true, strDateRanges);
			}
		}

		private bool ValidateEntries()
		{
			bool ValidateEntries = false;
			int counter;
			bool blnSelected = false;
			ValidateEntries = false;
			if (cmbCategorySelected.SelectedIndex == 1)
			{
				blnSelected = false;
				for (counter = 1; counter <= vsCategory.Rows - 1; counter++)
				{
					if (FCConvert.CBool(vsCategory.TextMatrix(counter, SelectCol)) == true)
					{
						blnSelected = true;
						break;
					}
				}
				if (!blnSelected)
				{
					MessageBox.Show("You must select at least 1 Category before yo umay continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return ValidateEntries;
				}
			}
			if (cmbMeterSizeAll.SelectedIndex == 1)
			{
				blnSelected = false;
				for (counter = 1; counter <= vsMeterSize.Rows - 1; counter++)
				{
					if (FCConvert.CBool(vsMeterSize.TextMatrix(counter, SelectCol)) == true)
					{
						blnSelected = true;
						break;
					}
				}
				if (!blnSelected)
				{
					MessageBox.Show("You must select at least 1 Meter Size before yo umay continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return ValidateEntries;
				}
			}
			if (cmbFrequencyAll.SelectedIndex == 1)
			{
				blnSelected = false;
				for (counter = 1; counter <= vsFrequency.Rows - 1; counter++)
				{
					if (FCConvert.CBool(vsFrequency.TextMatrix(counter, SelectCol)) == true)
					{
						blnSelected = true;
						break;
					}
				}
				if (!blnSelected)
				{
					MessageBox.Show("You must select at least 1 Frequency before yo umay continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return ValidateEntries;
				}
			}
			for (counter = 1; counter <= 5; counter++)
			{
				// kk11032014 trout-1050
				if (vsGrid.TextMatrix(counter, 1) != "" && vsGrid.TextMatrix(counter, 2) != "")
				{
					if (counter == 1)
					{
						if (Strings.CompareString(vsGrid.TextMatrix(counter, 1), ">", vsGrid.TextMatrix(counter, 2)))
						{
							MessageBox.Show("The beginning value must be less than or equal to the ending value befoore you may continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidateEntries;
						}
					}
					else if (counter == 5)
					{
						if (Strings.Trim(vsGrid.TextMatrix(counter, 1)) != "" && !Information.IsDate(vsGrid.TextMatrix(counter, 1)))
						{
							MessageBox.Show("Beginning Date is not a valid date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidateEntries;
						}
						if (Strings.Trim(vsGrid.TextMatrix(counter, 2)) != "" && !Information.IsDate(vsGrid.TextMatrix(counter, 2)))
						{
							MessageBox.Show("Ending Date is not a valid date.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidateEntries;
						}
						if (DateAndTime.DateValue(vsGrid.TextMatrix(counter, 1)).ToOADate() > DateAndTime.DateValue(vsGrid.TextMatrix(counter, 2)).ToOADate())
						{
							MessageBox.Show("The beginning value must be less than or equal to the ending value befoore you may continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidateEntries;
						}
					}
					else
					{
						if (Conversion.Val(vsGrid.TextMatrix(counter, 1)) > Conversion.Val(vsGrid.TextMatrix(counter, 2)))
						{
							MessageBox.Show("The beginning value must be less than or equal to the ending value befoore you may continue.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return ValidateEntries;
						}
					}
				}
			}
			ValidateEntries = true;
			return ValidateEntries;
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void optCategoryAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsCategory.Rows - 1; counter++)
			{
				vsCategory.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
			vsCategory.Enabled = false;
			fraSelectedCategory.Enabled = false;
		}

		private void optCategorySelected_CheckedChanged(object sender, System.EventArgs e)
		{
			vsCategory.Enabled = true;
			fraSelectedCategory.Enabled = true;
		}

		private void optFrequencyAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsFrequency.Rows - 1; counter++)
			{
				vsFrequency.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
			vsFrequency.Enabled = false;
			fraSelectedFrequency.Enabled = false;
		}

		private void optFrequencySelected_CheckedChanged(object sender, System.EventArgs e)
		{
			vsFrequency.Enabled = true;
			fraSelectedFrequency.Enabled = true;
		}

		private void optGroupByAccountNumber_CheckedChanged(object sender, System.EventArgs e)
		{
			chkHideDetail.Enabled = true;
		}

		private void optGroupByBook_CheckedChanged(object sender, System.EventArgs e)
		{
			chkHideDetail.Enabled = true;
		}

		private void optGroupByCategory_CheckedChanged(object sender, System.EventArgs e)
		{
			chkHideDetail.Enabled = true;
		}

		private void optGroupByFrequency_CheckedChanged(object sender, System.EventArgs e)
		{
			chkHideDetail.Enabled = true;
		}

		private void optGroupByMeterSize_CheckedChanged(object sender, System.EventArgs e)
		{
			chkHideDetail.Enabled = true;
		}

		private void optGroupByNone_CheckedChanged(object sender, System.EventArgs e)
		{
			chkHideDetail.CheckState = Wisej.Web.CheckState.Unchecked;
			chkHideDetail.Enabled = false;
		}

		private void optMeterSizeAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= vsMeterSize.Rows - 1; counter++)
			{
				vsMeterSize.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
			vsMeterSize.Enabled = false;
			fraSelectedMeterSize.Enabled = false;
		}

		private void optMeterSizeSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			vsMeterSize.Enabled = true;
			fraSelectedMeterSize.Enabled = true;
		}

		private void vsGrid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsGrid.Row == 5)
			{
				if (vsGrid.Col == 1 || vsGrid.Col == 2)
				{
					vsGrid.EditMask = "##/##/####";
				}
			}
			else
			{
				vsGrid.EditMask = string.Empty;
			}
		}

		private void vsGrid_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (vsGrid.Col > 0)
			{
				switch (e.KeyCode)
				{
					case Keys.Delete:
						{
							vsGrid.TextMatrix(vsGrid.Row, vsGrid.Col, "");
							break;
						}
				}
				//end switch
			}
		}

		private void vsGrid_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						vsGrid.TextMatrix(vsGrid.Row, vsGrid.Col, "");
						break;
					}
			}
			//end switch
		}

		private void vsGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if (vsGrid.Row == 4)
			{
				if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii == 45))
				{
				}
				else if (KeyAscii == 46)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, vsGrid.EditText, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (vsGrid.EditSelStart < lngDecPlace && vsGrid.EditSelStart + vsGrid.EditSelLength >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = 0;
						}
					}
				}
				else
				{
					KeyAscii = 0;
				}
			}
			else if (vsGrid.Row == 2 || vsGrid.Row == 3)
			{
				if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii == 45))
				{
					// do nothing
				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsGrid.Row == 5)
			{
				if (Strings.Trim(vsGrid.TextMatrix(vsGrid.Row, 1)) == "/  /")
				{
					vsGrid.TextMatrix(vsGrid.Row, 1, string.Empty);
				}
				if (Strings.Trim(vsGrid.TextMatrix(vsGrid.Row, 2)) == "/  /")
				{
					vsGrid.TextMatrix(vsGrid.Row, 2, string.Empty);
				}
			}
		}

		private void vsGrid_Leave(object sender, System.EventArgs e)
		{
			vsGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			vsGrid.Select(0, 1);
		}

		private void vsGrid_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = e.RowIndex;
			lngMC = e.ColumnIndex;
			//FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
			if (lngMR > -1 && lngMC > -1)
			{
				DataGridViewCell cell = vsGrid[lngMC, lngMR];
				cell.ToolTipText = "";
				if (lngMC == 0)
				{
					switch (lngMR)
					{
						case 1:
							{
								cell.ToolTipText = "Show a range of names.";
								break;
							}
						case 2:
							{
								cell.ToolTipText = "Show a range of accounts.";
								break;
							}
						case 3:
							{
								cell.ToolTipText = "Filter results by consumption values.";
								break;
							}
						case 4:
							{
								cell.ToolTipText = "Filter results by bill amounts.";
								break;
							}
						case 5:
							{
								cell.ToolTipText = "Filter results by Period Start Date and Period End Date.";
								break;
							}
					}
					//end switch
				}
			}
		}

		private void vsGrid_RowColChange(object sender, System.EventArgs e)
		{
			int lngRow;
			int lngCol;
			lngRow = vsGrid.Row;
			lngCol = vsGrid.Col;
			if (vsGrid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngCol) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
			{
				if (lngRow == vsGrid.Rows - 1 && lngCol == vsGrid.Cols - 1)
				{
					vsGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					vsGrid.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				vsGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
				Support.SendKeys("{Tab}", false);
			}
		}

		private void vsMeterSize_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsMeterSize.Row > 0)
			{
				if (FCConvert.CBool(vsMeterSize.TextMatrix(vsMeterSize.Row, SelectCol)) == false)
				{
					vsMeterSize.TextMatrix(vsMeterSize.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsMeterSize.TextMatrix(vsMeterSize.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsMeterSize_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				if (vsMeterSize.Row > 0)
				{
					KeyCode = 0;
					if (FCConvert.CBool(vsMeterSize.TextMatrix(vsMeterSize.Row, SelectCol)) == false)
					{
						vsMeterSize.TextMatrix(vsMeterSize.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsMeterSize.TextMatrix(vsMeterSize.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsCategory_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsCategory.Row > 0)
			{
				if (FCConvert.CBool(vsCategory.TextMatrix(vsCategory.Row, SelectCol)) == false)
				{
					vsCategory.TextMatrix(vsCategory.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsCategory.TextMatrix(vsCategory.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsCategory_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				if (vsCategory.Row > 0)
				{
					KeyCode = 0;
					if (FCConvert.CBool(vsCategory.TextMatrix(vsCategory.Row, SelectCol)) == false)
					{
						vsCategory.TextMatrix(vsCategory.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsCategory.TextMatrix(vsCategory.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void vsFrequency_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsFrequency.Row > 0)
			{
				if (FCConvert.CBool(vsFrequency.TextMatrix(vsFrequency.Row, SelectCol)) == false)
				{
					vsFrequency.TextMatrix(vsFrequency.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsFrequency.TextMatrix(vsFrequency.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsFrequency_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				if (vsFrequency.Row > 0)
				{
					KeyCode = 0;
					if (FCConvert.CBool(vsFrequency.TextMatrix(vsFrequency.Row, SelectCol)) == false)
					{
						vsFrequency.TextMatrix(vsFrequency.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsFrequency.TextMatrix(vsFrequency.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void cmbMeterSizeAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbMeterSizeAll.Text == "All")
			{
				optMeterSizeAll_CheckedChanged(sender, e);
			}
			else
			{
				optMeterSizeSelected_CheckedChanged(sender, e);
			}
		}

		private void cmbFrequencyAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbFrequencyAll.Text == "All")
			{
				optFrequencyAll_CheckedChanged(sender, e);
			}
			else
			{
				optFrequencySelected_CheckedChanged(sender, e);
			}
		}

		private void cmbCategorySelected_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbCategorySelected.Text == "All")
			{
				optCategoryAll_CheckedChanged(sender, e);
			}
			else
			{
				optCategorySelected_CheckedChanged(sender, e);
			}
		}

		private void cmbGroupByCategory_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbGroupByCategory.Text == "Category")
			{
				optGroupByCategory_CheckedChanged(sender, e);
			}
			else if (cmbGroupByCategory.Text == "Account #")
			{
				optGroupByAccountNumber_CheckedChanged(sender, e);
			}
			else if (cmbGroupByCategory.Text == "Book")
			{
				optGroupByBook_CheckedChanged(sender, e);
			}
			else if (cmbGroupByCategory.Text == "Frequency")
			{
				optGroupByFrequency_CheckedChanged(sender, e);
			}
			else if (cmbGroupByCategory.Text == "Meter Size")
			{
				optGroupByMeterSize_CheckedChanged(sender, e);
			}
			else if (cmbGroupByCategory.Text == "None")
			{
				optGroupByNone_CheckedChanged(sender, e);
			}
		}
	}
}
