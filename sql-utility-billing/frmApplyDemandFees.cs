﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmApplyDemandFees.
	/// </summary>
	public partial class frmApplyDemandFees : BaseForm
	{
		public frmApplyDemandFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmApplyDemandFees InstancePtr
		{
			get
			{
				return (frmApplyDemandFees)Sys.GetInstance(typeof(frmApplyDemandFees));
			}
		}

		protected frmApplyDemandFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/30/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/08/2006              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolLoaded;
		double dblDemand;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		bool boolChargeForNewOwner;
		DateTime dtMailDate;
		clsDRWrapper rsUT = new clsDRWrapper();
		bool boolWater;
		string strWS = "";
		string strBookList;
		string strRateKeyList = "";

		public void Init(string strPassBookList)
		{
			strBookList = strPassBookList;
			this.Show();
		}

		private void frmApplyDemandFees_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				if (boolWater)
				{
					this.Text = "Apply Water Demand Fees";
				}
				else
				{
					this.Text = "Apply Sewer Demand Fees";
				}
				lblInstructionHeader.Visible = false;
				lblInstruction.Text = "To Apply Demand Fees:" + "\r\n" + "     1. Check the box beside the account." + "\r\n" + "     2. Select 'Apply Demand Fees' or press F12.";
				lblValidateInstruction.Text = "If these values are correct, Press F12 to advance.  If not, then please rerun your 30 Day Notices.";
				//Application.DoEvents();
				// show the water and sewer choice
				if (modUTStatusPayments.Statics.TownService == "B")
				{
					SetAct_2(2);
					//optWS[0].Enabled = true;
					//optWS[1].Enabled = true;
					//optWS[1].Checked = true;
					cmbWS.Text = "Sewer";
				}
				else
				{
					if (modUTStatusPayments.Statics.TownService == "W")
					{
						boolWater = true;
						strWS = "W";
						//optWS[0].Enabled = true;
						//optWS[1].Enabled = false;
						cmbWS.Clear();
						cmbWS.Items.Add("Water");
					}
					else
					{
						boolWater = false;
						strWS = "S";
						//optWS[0].Enabled = false;
						//optWS[1].Enabled = true;
						cmbWS.Clear();
						cmbWS.Items.Add("Sewer");
					}
					SetAct_2(0);
					FillValidateGrid();
					FillDemandGrid();
				}
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			}
		}

		private void frmApplyDemandFees_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmApplyDemandFees properties;
			//frmApplyDemandFees.FillStyle	= 0;
			//frmApplyDemandFees.ScaleWidth	= 9195;
			//frmApplyDemandFees.ScaleHeight	= 7875;
			//frmApplyDemandFees.LinkTopic	= "Form2";
			//frmApplyDemandFees.LockControls	= true;
			//frmApplyDemandFees.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FCUtils.EraseSafe(modUTLien.Statics.arrDemand);
		}

		private void frmApplyDemandFees_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmApplyDemandFees_Resize(object sender, System.EventArgs e)
		{
			FormatGrid_2(true);
			if (fraGrid.Visible)
			{
				ShowGridFrame();
			}
			else if (cmbWS.Visible)
			{
				ShowWSFrame();
			}
			else
			{
				ShowValidateFrame();
			}
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			ClearCheckBoxes();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowValidateFrame()
		{
			//FC:FINAL:RPU - no need to adjust size in code, fixed in designer
			// this will show/center the frame with the grid on it
			//fraValidate.Top = FCConvert.ToInt32((this.Height - fraValidate.Height) / 3.0);
			//fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
			fraValidate.Visible = true;
			//FC:FINAL:BSE #3973 hide label
            lblWS.Visible = false;
			// this will set the height of the grid
			//vsValidate.Height = (vsValidate.Rows * vsValidate.RowHeight(0)) + 70;
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
			//FC:FINAL:BSE #3973 hide label
            lblWS.Visible = false;
			//if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.Height - lblInstruction.Height - 500)
			//{
			//    vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500;
			//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70;
			//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void ShowWSFrame()
		{
			// this will show/center the frame with the grid on it
			//fraWS.Top = FCConvert.ToInt32((this.Height-fraWS.Height) / 3.0);
			//fraWS.Left = FCConvert.ToInt32((this.Width-fraWS.Width) / 2.0);
			cmbWS.Visible = true;
			//FC:FINAL:BSE #3973 show label
            lblWS.Visible = true;
		}

		private void FormatGrid_2(bool boolResize)
		{
			FormatGrid(boolResize);
		}

		private void FormatGrid(bool boolResize = false)
		{
			int wid = 0;
			if (!boolResize)
			{
				vsDemand.Rows = 1;
			}
			vsDemand.Cols = 7;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(0, FCConvert.ToInt32(wid * 0.1));
			// checkbox
			vsDemand.ColWidth(1, FCConvert.ToInt32(wid * 0.15));
			// Acct
			vsDemand.ColWidth(2, FCConvert.ToInt32(wid * 0.45));
			// Name
			vsDemand.ColWidth(3, FCConvert.ToInt32(wid * 0.15));
			// Total Notices Sent
			vsDemand.ColWidth(4, FCConvert.ToInt32(wid * 0.1));
			// Total Demand Fees
			vsDemand.ColWidth(5, 0);
			// Hidden Key Field
			vsDemand.ColWidth(6, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColFormat(4, "#,##0.00");
			vsDemand.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, 1, "Account");
			vsDemand.TextMatrix(0, 2, "Name");
			vsDemand.TextMatrix(0, 3, "Notices");
			vsDemand.TextMatrix(0, 4, "Amount");
			vsValidate.Cols = 3;
			wid = vsValidate.WidthOriginal;
			vsValidate.ColWidth(0, FCConvert.ToInt32(wid * 0.7));
			// Question
			vsValidate.ColWidth(1, FCConvert.ToInt32(wid * 0.28));
			// Answer
			vsValidate.ColWidth(2, 0);
			// Hidden field
			vsValidate.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.TextMatrix(0, 1, "Parameters");
			vsValidate.TextMatrix(0, 2, "Value");
		}

		private void FillDemandGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL;
				string strSQLSubQry;
				int lngIndex;
				int lngLastKey = 0;
				clsDRWrapper rsTemp = new clsDRWrapper();
				double dblTotalDue = 0;
				double dblXInt = 0;
				string strNewOwner = "";
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				vsDemand.Rows = 1;
				if (Strings.Trim(strBookList) != "")
				{
					strBookList = " AND (" + Strings.Trim(strBookList) + ")";
					// kgk 06-06-11 trout-724  Add parenthesis for multiple books
				}
				strSQL = "SELECT * FROM Bill WHERE " + strWS + "LienProcessStatus = 1 AND " + strWS + "LienStatusEligibility >= 2 AND ISNULL(" + strWS + "DemandGroupID,0) = 0 AND BillStatus = 'B' " + strBookList + " ORDER BY ActualAccountNumber, BillingRateKey";
				// desc"     'this will be all the records that have had 30 Day Notices printed
				strSQLSubQry = "SELECT TOP(100) PERCENT * FROM Bill WHERE " + strWS + "LienProcessStatus = 1 AND " + strWS + "LienStatusEligibility >= 2 AND ISNULL(" + strWS + "DemandGroupID,0) = 0 AND BillStatus = 'B' " + strBookList + " ORDER BY ActualAccountNumber, BillingRateKey";
				// kgk
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				rsUT.OpenRecordset("SELECT * FROM Master", modExtraModules.strUTDatabase);
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("There are no accounts eligible to have demand fees applied.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Unload();
				}
				while (!rsData.EndOfFile())
				{
					//Application.DoEvents();
					TRYAGAIN:
					;
					if (rsData.EndOfFile())
						break;
					// MAL@20080602: Move check for total due to be first
					// Tracker Reference: 13958
					rsTemp.OpenRecordset("SELECT * FROM Bill WHERE ID = " + rsData.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
					dblTotalDue = modUTCalculations.CalculateAccountUT(rsTemp, DateTime.Today, ref dblXInt, FCConvert.CBool(strWS == "W"));
					// kk03142014 dtLastInterestDate is an out var   ' , , , , , , rsData.Fields(strWS & "IntPaidDate"))
					if (dblTotalDue <= 0)
					{
						// get the next record
						// MAL@20080422: Add check for first row
						// Tracker Reference: 12837 (Found while working on this error)
						if (Information.IsNumeric(vsDemand.TextMatrix(vsDemand.Rows - 1, 3)))
						{
							// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
							vsDemand.TextMatrix(vsDemand.Rows - 1, 3, FCConvert.ToDouble(vsDemand.TextMatrix(vsDemand.Rows - 1, 3)) - FCConvert.ToDouble(rsData.Get_Fields("Copies")));
						}
						rsData.MoveNext();
						if (rsData.EndOfFile())
						{
							break;
						}
						goto TRYAGAIN;
					}
					else
					{
						// this will weed out the secondary bills for the mail bill
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey")) == lngLastKey)
						{
							// get the next record
							// MAL@20080422: Add check for 1st row
							// Tracker Reference: 12837 (Found while working on this error)
							// If IsNumeric(.TextMatrix(.rows - 1, 3)) Then
							// .TextMatrix(.rows - 1, 3) = .TextMatrix(.rows - 1, 3) + rsData.Fields("Copies")
							// End If
							rsData.MoveNext();
							if (rsData.EndOfFile())
							{
								break;
							}
							goto TRYAGAIN;
						}
						else
						{
							lngLastKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey"));
						}
						// MAL@20080325: Check that account has not been paid off
						// Tracker Reference: 12837
						// rsTemp.OpenRecordset "SELECT * FROM Bill WHERE Bill = " & rsData.Fields("Bill"), strUTDatabase
						// dblTotalDue = CalculateAccountUT(rsTemp, Date, dblXInt, CBool(strWS = "W"), , , , , , rsData.Fields(strWS & "IntPaidDate"))
						// If dblTotalDue <= 0 Then
						// get the next record
						// MAL@20080422: Add check for first row
						// Tracker Reference: 12837 (Found while working on this error)
						// If IsNumeric(.TextMatrix(.rows - 1, 3)) Then
						// .TextMatrix(.rows - 1, 3) = .TextMatrix(.rows - 1, 3) + rsData.Fields("Copies")
						// End If
						// rsData.MoveNext
						// If rsData.EndOfFile Then
						// Exit Do
						// End If
						// GoTo TRYAGAIN
						// Else
						// add a row/element
						vsDemand.AddItem("");
						Array.Resize(ref modUTLien.Statics.arrDemand, vsDemand.Rows - 1 + 1);
						// this will make sure that there are enough elements to use
						// lngIndex = .rows - 2
						// arrDemand(lngIndex).Used = True
						// arrDemand(lngIndex).Processed = False
						vsDemand.TextMatrix(vsDemand.Rows - 1, 1, FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"))));
						// show the actual account number
						// arrDemand(lngIndex).Account = rsData.Fields("AccountKey")                   'the account key
						// arrDemand(lngIndex).BillKey = rsData.Fields("Bill")                         'the bill key
						vsDemand.RowData(vsDemand.Rows - 1, rsData.Get_Fields_Int32("AccountKey"));
						if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "SUMRPT", strSQLSubQry))
						{
							// DJW@10/09/13 TROUTS-39 Had to remove order by clause from sql passed into function becuase it is used within an outter select
							string strTempSQL = "";
							int intStart = 0;
							intStart = Strings.InStr(1, rsData.Name(), "ORDER BY", CompareConstants.vbBinaryCompare);
							if (intStart != 0)
							{
								strTempSQL = Strings.Left(rsData.Name(), Strings.InStr(1, rsData.Name(), "ORDER BY", CompareConstants.vbBinaryCompare) - 1);
							}
							else
							{
								strTempSQL = rsData.Name();
							}
							modMain.GetLatestOwnerInformation_59022(rsData.Get_Fields_Int32("AccountKey"), "SUMRPT", ref strNewOwner, "", "", "", "", "", "", "", strTempSQL);
							vsDemand.TextMatrix(vsDemand.Rows - 1, 2, strNewOwner);
							// name
						}
						else
						{
							vsDemand.TextMatrix(vsDemand.Rows - 1, 2, FCConvert.ToString(rsData.Get_Fields_String("OName")));
							// name
						}
						// arrDemand(lngIndex).Name = rsData.Fields("OName")
						// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
						vsDemand.TextMatrix(vsDemand.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields("Copies")));
						// number of copies
						// amount
						if (rsData.Get_Fields(strWS + "CostOwed") > 0 || rsData.Get_Fields(strWS + "CostAdded") < 0)
						{
							// highlight the row and set the value = zero
							vsDemand.Cell(FCGrid.CellPropertySettings.flexcpForeColor, vsDemand.Rows - 1, 0, vsDemand.Rows - 1, vsDemand.Cols - 1, Color.Red);
							vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(0));
							vsDemand.TextMatrix(vsDemand.Rows - 1, 6, FCConvert.ToString(-1));
						}
						else
						{
							// gonna have to figure out the amount
							// arrDemand(lngIndex).Fee = dblDemand
							if (boolChargeCert)
							{
								if (boolChargeMort)
								{
									// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
									vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString((rsData.Get_Fields("Copies") * dblCertMailFee) + dblDemand));
									// arrDemand(lngIndex).CertifiedMailFee = CDbl(.TextMatrix(.rows - 1, 4))
								}
								else
								{
									// may have to add the new owner charge here...
									bool boolUTMatch = false;
                                    //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
									//if (boolChargeForNewOwner && modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
									if (boolChargeForNewOwner & modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
									{
										vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString((dblCertMailFee * 2) + dblDemand));
										// arrDemand(lngIndex).CertifiedMailFee = CDbl(.TextMatrix(.rows - 1, 4))
									}
									else
									{
										vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblCertMailFee + dblDemand));
										// arrDemand(lngIndex).CertifiedMailFee = CDbl(.TextMatrix(.rows - 1, 4))
									}
								}
							}
							else
							{
								vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblDemand));
								// arrDemand(lngIndex).CertifiedMailFee = 0
							}
						}
						vsDemand.TextMatrix(vsDemand.Rows - 1, 5, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
						// billkey
						rsData.MoveNext();
					}
				}
				// check all of the accounts
				mnuFileSelectAll_Click();
				//if (vsDemand.Rows * vsDemand.RowHeight(0) > fraGrid.Height - lblInstruction.Height - 500)
				//{
				//    vsDemand.Height = fraGrid.Height - lblInstruction.Height - 500;
				//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//    vsDemand.Height = vsDemand.Rows * vsDemand.RowHeight(0) + 70;
				//    vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				if (vsDemand.Rows > 1)
				{
					vsDemand.Select(1, 2);
					vsDemand.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					for (lngIndex = 1; lngIndex <= vsDemand.Rows - 1; lngIndex++)
					{
						//Application.DoEvents();
						modUTLien.Statics.arrDemand[lngIndex - 1].Used = true;
						modUTLien.Statics.arrDemand[lngIndex - 1].Processed = false;
						modUTLien.Statics.arrDemand[lngIndex - 1].Account = FCConvert.ToInt32(vsDemand.RowData(lngIndex));
						// the account key
						modUTLien.Statics.arrDemand[lngIndex - 1].BillKey = FCConvert.ToInt32(vsDemand.TextMatrix(lngIndex, 5));
						// the bill key
						modUTLien.Statics.arrDemand[lngIndex - 1].Name = vsDemand.TextMatrix(lngIndex, 2);
						modUTLien.Statics.arrDemand[lngIndex - 1].Fee = dblDemand;
						if (boolChargeCert)
						{
							modUTLien.Statics.arrDemand[lngIndex - 1].CertifiedMailFee = FCConvert.ToDouble(vsDemand.TextMatrix(lngIndex, 4));
						}
						else
						{
							modUTLien.Statics.arrDemand[lngIndex - 1].CertifiedMailFee = 0;
						}
					}
				}
				frmWait.InstancePtr.Unload();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillValidateGrid()
		{
			// this will fill the validate grid with the information from the Control_30DayNotice table
			// which is the information that was used to print the 30 Day Notices the last time
			clsDRWrapper rsValidate = new clsDRWrapper();
			vsValidate.Rows = 1;
			rsValidate.OpenRecordset("SELECT * FROM " + strWS + "Control_30DayNotice");
			if (rsValidate.EndOfFile())
			{
				MessageBox.Show("Please return to and run 'Print 30 Day Notices'.", "No Control Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Unload();
			}
			else
			{
				// vsValidate.AddItem "Billing Year" & vbTab & .Fields("BillingYear")
				// vsValidate.AddItem "Recorded Date" & vbTab & .Fields("DateCreated")
				dtMailDate = (DateTime)rsValidate.Get_Fields_DateTime("MailDate");
                //FC:FINAL:BSE #3973 remove timestamp
				vsValidate.AddItem("Interest/Mailing Date" + "\t" +Strings.Format(rsValidate.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
				vsValidate.AddItem("Demand Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("Demand"), "#,##0.00"));
				dblDemand = rsValidate.Get_Fields_Double("Demand");
				boolChargeMort = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder"));
				boolChargeCert = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("PayCertMailFee"));
				if (boolChargeCert)
				{
					vsValidate.AddItem("Cert Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					dblCertMailFee = rsValidate.Get_Fields_Double("CertMailFee");
					vsValidate.AddItem("Charge for Cert Mail Fee?" + "\t" + "Yes");
				}
				else
				{
					if (boolChargeMort)
					{
						vsValidate.AddItem("Cert Mail Fee" + "\t" + rsValidate.Get_Fields_Double("CertMailFee"));
					}
					else
					{
						vsValidate.AddItem("Cert Mail Fee" + "\t" + "0.00");
					}
					vsValidate.AddItem("Charge for Cert Mail Fee?" + "\t" + "No");
					dblCertMailFee = 0;
				}
				if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToMortHolder")))
				{
					if (boolChargeMort)
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "Yes");
					}
					else
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
					}
				}
				else
				{
					vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
				}
				if (FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")) != "")
				{
					if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 2) == "Ye")
					{
						if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 8) == "Yes, cha")
						{
							vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, charge CMF.");
							boolChargeForNewOwner = true;
						}
						else
						{
							vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, with no charge.");
							boolChargeForNewOwner = false;
						}
					}
					else
					{
						vsValidate.AddItem("Send to New Owner" + "\t" + "No");
					}
				}
				else
				{
					vsValidate.AddItem("Send to New Owner" + "\t" + "No");
				}
				// MAL@20071029: Rate Key List from Table
				strRateKeyList = FCConvert.ToString(rsValidate.Get_Fields_String("RateKeyList"));
				if (Strings.Right(strRateKeyList, 1) == ",")
				{
					// kgk  Right(strRateKeyList, Len(strRateKeyList)) = ","
					strRateKeyList = Strings.Left(strRateKeyList, strRateKeyList.Length - 1);
				}
			}
			//FC:FINAL:RPU - no need to adjust size in code, fixed in designer
			// set the height of the grid
			//if (vsValidate.Rows * vsValidate.RowHeight(0) > fraValidate.Height - 300)
			//{
			//    vsValidate.Height = fraValidate.Height - 300;
			//    vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//}
			//else
			//{
			//    vsValidate.Height = vsValidate.Rows * vsValidate.RowHeight(0) + 70;
			//    vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//}
		}

		private void SetAct_2(short intAct)
		{
			SetAct(ref intAct);
		}

		private void SetAct(ref short intAct)
		{
			// this will change all of the menu options
			fraGrid.Visible = false;
			fraValidate.Visible = false;
			cmbWS.Visible = false;
			cmdFileSelectAll.Visible = false;
			switch (intAct)
			{
				case 0:
					{
						// Validation Frame
						ShowValidateFrame();
						cmdFilePrint.Text = "Accept Parameters";
						intAction = 0;
                        //FC:FINAL:BSE #3972 move save button 
                        cmdFilePrint.Location = new System.Drawing.Point(671, 813);
                        break;
					}
				case 1:
					{
						// Demand Frame
						ShowGridFrame();
						cmdFileSelectAll.Visible = true;
						cmdFilePrint.Text = "Apply Demand Fees";
						intAction = 1;
                        //FC:FINAL:BSE #3972 move save button 
                        cmdFilePrint.Location = new System.Drawing.Point(671, 813);
                        break;
					}
				case 2:
					{
						// Water or Sewer Frame
						ShowWSFrame();
						intAction = 2;
						cmdFilePrint.Text = "Save";
                        //FC:FINAL:BSE #3972 move save button 
                        cmdFilePrint.Location = new System.Drawing.Point(30, 123);
                        break;
					}
			}
			//end switch
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						// this will say that the user accepts the parameters shown
						SetAct_2(1);
						break;
					}
				case 1:
					{
						// Apply Demand Fees
						ApplyDemandFees();
						this.Unload();
						break;
					}
				case 2:
					{
						if (cmbWS.Text == "Water")
						{
							strWS = "W";
							boolWater = true;
						}
						else
						{
							strWS = "S";
							boolWater = false;
						}
						// fill these grids and show the validation frame
						//Application.DoEvents();
						FillValidateGrid();
						FillDemandGrid();
						SetAct_2(0);
						break;
					}
			}
			//end switch
		}

		private void ApplyDemandFees()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				clsDRWrapper rsTemp = new clsDRWrapper();
				frmWait.InstancePtr.Init("Processing..." + "\r\n" + "Adding Fees", true, vsDemand.Rows - 1);
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, 6)) >= 0)
						{
							// then what?
							rsData.FindFirstRecord("ID", vsDemand.TextMatrix(lngCT, 5));
							if (rsData.NoMatch)
							{
								MessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, 1) + ".  No demand fees were applied.", "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, 5), MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
							else
							{
								modUTLien.Statics.arrDemand[FindDemandIndex_2(rsData.Get_Fields_Int32("AccountKey"))].Processed = true;
								AddDemandPayment_2(FCConvert.ToDouble(vsDemand.TextMatrix(lngCT, 4)));
								lngDemandCount += 1;
							}
						}
						else
						{
							rsData.FindFirstRecord("ID", vsDemand.TextMatrix(lngCT, 5));
							rsTemp.Execute("UPDATE Bill Set " + strWS + "LienProcessStatus = 2, " + strWS + "LienStatusEligibility = 3 WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND BillStatus = 'B' AND " + strWS + "LienStatusEligibility = 2", modExtraModules.strUTDatabase);
							// not eligible...do nothing
						}
					}
					else
					{
						// not selected...do nothing
					}
				}
				frmWait.InstancePtr.Unload();
				modMain.SetRateKeyDates_6(strWS, "3");
				MessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", "Demand Fees Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// print the list of accounts that have been affected
				rptDemandFeeList.InstancePtr.Init(ref lngDemandCount, ref dblDemand, ref boolWater);
				frmReportViewer.InstancePtr.Init(rptDemandFeeList.InstancePtr);
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Applying Fees", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int FindDemandIndex_2(int lngAcct)
		{
			return FindDemandIndex(ref lngAcct);
		}

		private int FindDemandIndex(ref int lngAcct)
		{
			int FindDemandIndex = 0;
			int lngCT;
			for (lngCT = 0; lngCT <= Information.UBound(modUTLien.Statics.arrDemand, 1); lngCT++)
			{
				if (lngAcct == modUTLien.Statics.arrDemand[lngCT].Account)
				{
					FindDemandIndex = lngCT;
					break;
				}
			}
			return FindDemandIndex;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				//Application.DoEvents();
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(cmdFileSelectAll, new System.EventArgs());
		}

		private void AddDemandPayment_2(double dblFeeAmount)
		{
			AddDemandPayment(ref dblFeeAmount);
		}

		private void AddDemandPayment(ref double dblFeeAmount)
		{
			bool blnAddGroup = false;
			// MAL@20071029
			double dblTotalCertMailFee;
			double dblTotalChange = 0;
			double dblCurInterest;
			int lngGroupKey = 0;
			clsDRWrapper rsPay = new clsDRWrapper();
			clsDRWrapper rsCount = new clsDRWrapper();
			// MAL@20071029
			string strSQL;
			// MAL@20071029
			// MAL@20071029: Create and Add Demand Group to All Necessary Bills
			// Tracker Reference: 11069
			strSQL = "SELECT AccountKey FROM Bill WHERE " + strWS + "LienProcessStatus = 1 AND " + strWS + "LienStatusEligibility >= 2 AND BillStatus = 'B' " + strBookList;
			// kgk    & " ORDER BY ActualAccountNumber, BillingRateKey" ' desc"     'this will be all the records that have had 30 Day Notices printed
			rsCount.OpenRecordset("SELECT Count(AccountKey) as TotalAccounts, AccountKey FROM Bill WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND AccountKey IN (" + strSQL + ") GROUP BY AccountKey", modExtraModules.strUTDatabase);
			if (rsCount.RecordCount() > 0)
			{
				// TODO Get_Fields: Field [TotalAccounts] not found!! (maybe it is an alias?)
				if (rsCount.Get_Fields("TotalAccounts") > 1)
				{
					lngGroupKey = Create30DNGroupKey();
					if (lngGroupKey > 0)
					{
						blnAddGroup = true;
					}
				}
				else
				{
					blnAddGroup = false;
				}
			}
			// this will actually create the payment line
			rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strUTDatabase);
			rsPay.AddNew();
			rsPay.Set_Fields("AccountKey", rsData.Get_Fields_Int32("AccountKey"));
			// .Fields("Year") = rsData.Fields("BillingYear")
			rsPay.Set_Fields("BillKey", rsData.Get_Fields_Int32("ID"));
			// .Fields("CHGINTNumber") = AddCHGINTForDemandFee    'do not charge interest (ron said so, to make it easier to reverse interest)
			rsPay.Set_Fields("CHGINTDate", DateTime.Today);
			rsPay.Set_Fields("ActualSystemDate", DateTime.Today);
			rsPay.Set_Fields("EffectiveInterestDate", DateTime.Today);
			rsPay.Set_Fields("RecordedTransactionDate", DateTime.Today);
			rsPay.Set_Fields("Teller", "");
			rsPay.Set_Fields("Reference", "DEMAND");
			rsPay.Set_Fields("Period", "A");
			rsPay.Set_Fields("Code", "3");
			rsPay.Set_Fields("ReceiptNumber", 0);
			rsPay.Set_Fields("Principal", 0);
			rsPay.Set_Fields("PreLienInterest", 0);
			rsPay.Set_Fields("CurrentInterest", 0);
			// calculate the lien costs
			// dblTotalCertMailFee = dblCertMailFee * rsData.Fields("Copies")
			// dblTotalChange = dblTotalCertMailFee + dblDemand
			dblTotalChange = dblFeeAmount;
			rsPay.Set_Fields("LienCost", (dblTotalChange * -1));
			rsPay.Set_Fields("TransNumber", 0);
			rsPay.Set_Fields("PaidBy", "Automatic/Computer");
			rsPay.Set_Fields("Comments", "Demand Fees");
			rsPay.Set_Fields("CashDrawer", "N");
			// MAL@20081015: Corrected to be "N" as expected
			// Tracker Reference: 15513
			// .Fields("GeneralLedger") = "Y"
			rsPay.Set_Fields("GeneralLedger", "N");
			rsPay.Set_Fields("BudgetaryAccountNumber", "");
			rsPay.Set_Fields("Service", strWS);
			// .Fields("BillCode") = "R"
			// rsPay.Fields ("DailyCloseOut")
			rsPay.Update();
			// this will edit the bill record
			// .OpenRecordset "SELECT * FROM CurrentAccountBills WHERE Billkey = " & .BillKey, strCLDATABASE
			// If .RecordCount <> 0 Then
			rsData.Edit();
			// .Fields(strWS & "IntAdded") = (.Fields(strWS & "IntAdded") - dblCurInterest)
			rsData.Set_Fields(strWS + "CostAdded", Conversion.Val(rsData.Get_Fields(strWS + "CostAdded")) - dblTotalChange);
			// .Fields("InterestAppliedThroughDate") = Date   'do not change the interest date (ron said so)
			// MAL@20071029: Update Group Key
			// MAL@20080303: Tracker Reference: 12580: Update the correct service field
			if (blnAddGroup)
			{
				rsData.Set_Fields(strWS + "DemandGroupID", lngGroupKey);
			}
			rsData.Update();
			// update all of the bills eligibility that are eligible
			rsPay.Execute("UPDATE Bill Set " + strWS + "LienProcessStatus = 2, " + strWS + "LienStatusEligibility = 3 WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND BillStatus = 'B' AND " + strWS + "LienStatusEligibility = 2", modExtraModules.strUTDatabase);
			// .Fields(strWS & "LienProcessStatus") = 2            'set the status to 'Demand Fees Applied'
			// .Fields(strWS & "LienStatusEligibility") = 3
			// .Update
			// End If
			strSQL = "SELECT AccountKey FROM Bill WHERE " + strWS + "LienProcessStatus = 2 AND " + strWS + "LienStatusEligibility = 3 AND BillStatus = 'B' " + strBookList;
			// kgk     & " ORDER BY ActualAccountNumber, BillingRateKey" ' desc"
			if (blnAddGroup)
			{
				rsCount.OpenRecordset("SELECT * FROM Bill WHERE ISNULL(" + strWS + "DemandGroupID,0) = 0 AND AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND AccountKey IN (" + strSQL + ") AND BillingRateKey IN (" + strRateKeyList + ")", modExtraModules.strUTDatabase);
				if (rsCount.RecordCount() > 0)
				{
					while (!rsCount.EndOfFile())
					{
						//Application.DoEvents();
						rsCount.Edit();
						rsCount.Set_Fields(strWS + "DemandGroupID", lngGroupKey);
						rsCount.Update();
						rsCount.MoveNext();
					}
				}
			}
		}

		//private int AddCHGINTForDemandFee()
		//{
		//	int AddCHGINTForDemandFee = 0;
		//	// this will calculate the CHGINT line for the account and then return
		//	// the CHGINT number to store in the payment record
		//	// this will use rsData which is set to the correct record
		//	double dblCurInt = 0;
		//	double dblTotalDue;
		//	clsDRWrapper rsCHGINT = new clsDRWrapper();
		//	// calculate the current interest to this date
		//	dblTotalDue = modUTCalculations.CalculateAccountUT(rsData, DateTime.Today, ref dblCurInt, boolWater);
		//	if (dblCurInt > 0)
		//	{
		//		// if there is interest due, then
		//		// this will actually create the payment line
		//		rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strUTDatabase);
		//		rsCHGINT.AddNew();				
		//		// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
		//		rsCHGINT.Set_Fields("Account", rsData.Get_Fields("Account"));
		//		// .Fields("Year") = rsData.Fields("BillingYear")
		//		rsCHGINT.Set_Fields("BillKey", rsData.Get_Fields_Int32("BillKey"));
		//		// XXXX Bill/ID???
		//		rsCHGINT.Set_Fields("CHGINTNumber", 0);
		//		rsCHGINT.Set_Fields("CHGINTDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("EffectiveInterestDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("RecordedTransactionDate", DateTime.Today);
		//		rsCHGINT.Set_Fields("Teller", "");
		//		rsCHGINT.Set_Fields("Reference", "CHGINT");
		//		rsCHGINT.Set_Fields("Period", "A");
		//		rsCHGINT.Set_Fields("Code", "I");
		//		rsCHGINT.Set_Fields("ReceiptNumber", 0);
		//		rsCHGINT.Set_Fields("Principal", 0);
		//		rsCHGINT.Set_Fields("PreLienInterest", 0);
		//		rsCHGINT.Set_Fields("CurrentInterest", dblCurInt);
		//		rsCHGINT.Set_Fields("LienCost", 0);
		//		rsCHGINT.Set_Fields("TransNumber", 0);
		//		rsCHGINT.Set_Fields("PaidBy", 0);
		//		rsCHGINT.Set_Fields("Comments", "Demand Fees");
		//		rsCHGINT.Set_Fields("CashDrawer", "N");
		//		rsCHGINT.Set_Fields("GeneralLedger", "Y");
		//		rsCHGINT.Set_Fields("BudgetaryAccountNumber", "");
		//		rsCHGINT.Set_Fields("BillCode", "R");
		//		// rsPay.Fields ("DailyCloseOut")
		//		rsCHGINT.Update();
		//	}
  //          AddCHGINTForDemandFee = FCConvert.ToInt32(rsCHGINT.Get_Fields_Int32("ID"));
  //          return AddCHGINTForDemandFee;
		//}

		private void vsDemand_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// this should not allow an account that already has demand fees to be checked
			if (vsDemand.Col == 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(vsDemand.Row, 6)) != 0)
				{
					vsDemand.TextMatrix(vsDemand.Row, 0, FCConvert.ToString(0));
				}
			}
		}

		private void vsDemand_DblClick(object sender, System.EventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = vsDemand.MouseRow;
			lngMC = vsDemand.MouseCol;
			if (lngMR == 0 && lngMC == 0)
			{
				mnuFileSelectAll_Click();
			}
		}

		private void vsDemand_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsDemand[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = vsDemand.GetFlexRowIndex(e.RowIndex);
			lngMC = vsDemand.GetFlexColIndex(e.ColumnIndex);
			if (lngMR > 0 && lngMC > 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(lngMR, 6)) == -1)
				{
					//ToolTip1.SetToolTip(vsDemand, "Demand Fees have already been added to this account.  This account is not eligible.");
					cell.ToolTipText = "Demand Fees have already been added to this account.  This account is not eligible.";
				}
				else
				{
                    //ToolTip1.SetToolTip(vsDemand, "");
                    cell.ToolTipText = "";
				}
			}
		}

		private void vsDemand_RowColChange(object sender, System.EventArgs e)
		{
			switch (vsDemand.Col)
			{
				case 0:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private void ClearCheckBoxes()
		{
			// this will set all of the textboxes to unchecked
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(0));
			}
		}

		private int Create30DNGroupKey()
		{
			int Create30DNGroupKey = 0;
			// MAL@20071029: New function to create group for 30DN demand fee groups
			clsDRWrapper rsGroup = new clsDRWrapper();
			int lngResult = 0;
			rsGroup.OpenRecordset("SELECT * FROM tblDemandFeeGroups");
			rsGroup.AddNew();
			// kgk        lngResult = .Fields("DemandGroupID")
			rsGroup.Set_Fields("DemandDateCreated", DateTime.Today);
			rsGroup.Update();
			lngResult = FCConvert.ToInt32(rsGroup.Get_Fields_Int32("ID"));
			Create30DNGroupKey = lngResult;
			return Create30DNGroupKey;
		}
	}
}
