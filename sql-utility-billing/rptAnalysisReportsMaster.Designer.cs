﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptAnalysisReportsMaster.
	/// </summary>
	partial class rptAnalysisReportsMaster
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAnalysisReportsMaster));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.sarBillingSummary = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarDollarAmountsS = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarConsumptionsS = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarBillCountS = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarMeterReportS = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarDollarAmountsW = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarConsumptionsW = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarBillCountW = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarMeterReportW = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarSalesTaxS = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarSalesTaxW = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.sarBillingSummary,
				this.sarDollarAmountsS,
				this.sarConsumptionsS,
				this.sarBillCountS,
				this.sarMeterReportS,
				this.sarDollarAmountsW,
				this.sarConsumptionsW,
				this.sarBillCountW,
				this.sarMeterReportW,
				this.sarSalesTaxS,
				this.sarSalesTaxW
			});
			this.Detail.Height = 0.71875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.lblReportType
			});
			this.PageHeader.Height = 0.625F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Analysis Reports";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 10F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 8.5F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.5F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 8.5F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.5F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.375F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblReportType.Text = "Analysis Reports";
			this.lblReportType.Top = 0.25F;
			this.lblReportType.Width = 10F;
			// 
			// sarBillingSummary
			// 
			this.sarBillingSummary.CloseBorder = false;
			this.sarBillingSummary.Height = 0.0625F;
			this.sarBillingSummary.Left = 0F;
			this.sarBillingSummary.Name = "sarBillingSummary";
			this.sarBillingSummary.Report = null;
			this.sarBillingSummary.Top = 0F;
			this.sarBillingSummary.Width = 10F;
			// 
			// sarDollarAmountsS
			// 
			this.sarDollarAmountsS.CloseBorder = false;
			this.sarDollarAmountsS.Height = 0.0625F;
			this.sarDollarAmountsS.Left = 0F;
			this.sarDollarAmountsS.Name = "sarDollarAmountsS";
			this.sarDollarAmountsS.Report = null;
			this.sarDollarAmountsS.Top = 0.0625F;
			this.sarDollarAmountsS.Width = 10F;
			// 
			// sarConsumptionsS
			// 
			this.sarConsumptionsS.CloseBorder = false;
			this.sarConsumptionsS.Height = 0.0625F;
			this.sarConsumptionsS.Left = 0F;
			this.sarConsumptionsS.Name = "sarConsumptionsS";
			this.sarConsumptionsS.Report = null;
			this.sarConsumptionsS.Top = 0.1875F;
			this.sarConsumptionsS.Width = 10F;
			// 
			// sarBillCountS
			// 
			this.sarBillCountS.CloseBorder = false;
			this.sarBillCountS.Height = 0.0625F;
			this.sarBillCountS.Left = 0F;
			this.sarBillCountS.Name = "sarBillCountS";
			this.sarBillCountS.Report = null;
			this.sarBillCountS.Top = 0.3125F;
			this.sarBillCountS.Width = 10F;
			// 
			// sarMeterReportS
			// 
			this.sarMeterReportS.CloseBorder = false;
			this.sarMeterReportS.Height = 0.0625F;
			this.sarMeterReportS.Left = 0F;
			this.sarMeterReportS.Name = "sarMeterReportS";
			this.sarMeterReportS.Report = null;
			this.sarMeterReportS.Top = 0.4375F;
			this.sarMeterReportS.Width = 10F;
			// 
			// sarDollarAmountsW
			// 
			this.sarDollarAmountsW.CloseBorder = false;
			this.sarDollarAmountsW.Height = 0.0625F;
			this.sarDollarAmountsW.Left = 0F;
			this.sarDollarAmountsW.Name = "sarDollarAmountsW";
			this.sarDollarAmountsW.Report = null;
			this.sarDollarAmountsW.Top = 0.125F;
			this.sarDollarAmountsW.Width = 10F;
			// 
			// sarConsumptionsW
			// 
			this.sarConsumptionsW.CloseBorder = false;
			this.sarConsumptionsW.Height = 0.0625F;
			this.sarConsumptionsW.Left = 0F;
			this.sarConsumptionsW.Name = "sarConsumptionsW";
			this.sarConsumptionsW.Report = null;
			this.sarConsumptionsW.Top = 0.25F;
			this.sarConsumptionsW.Width = 10F;
			// 
			// sarBillCountW
			// 
			this.sarBillCountW.CloseBorder = false;
			this.sarBillCountW.Height = 0.0625F;
			this.sarBillCountW.Left = 0F;
			this.sarBillCountW.Name = "sarBillCountW";
			this.sarBillCountW.Report = null;
			this.sarBillCountW.Top = 0.375F;
			this.sarBillCountW.Width = 10F;
			// 
			// sarMeterReportW
			// 
			this.sarMeterReportW.CloseBorder = false;
			this.sarMeterReportW.Height = 0.0625F;
			this.sarMeterReportW.Left = 0F;
			this.sarMeterReportW.Name = "sarMeterReportW";
			this.sarMeterReportW.Report = null;
			this.sarMeterReportW.Top = 0.5F;
			this.sarMeterReportW.Width = 10F;
			// 
			// sarSalesTaxS
			// 
			this.sarSalesTaxS.CloseBorder = false;
			this.sarSalesTaxS.Height = 0.0625F;
			this.sarSalesTaxS.Left = 0F;
			this.sarSalesTaxS.Name = "sarSalesTaxS";
			this.sarSalesTaxS.Report = null;
			this.sarSalesTaxS.Top = 0.5625F;
			this.sarSalesTaxS.Width = 10F;
			// 
			// sarSalesTaxW
			// 
			this.sarSalesTaxW.CloseBorder = false;
			this.sarSalesTaxW.Height = 0.0625F;
			this.sarSalesTaxW.Left = 0F;
			this.sarSalesTaxW.Name = "sarSalesTaxW";
			this.sarSalesTaxW.Report = null;
			this.sarSalesTaxW.Top = 0.625F;
			this.sarSalesTaxW.Width = 10F;
			// 
			// rptAnalysisReportsMaster
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarBillingSummary;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarDollarAmountsS;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarConsumptionsS;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarBillCountS;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarMeterReportS;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarDollarAmountsW;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarConsumptionsW;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarBillCountW;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarMeterReportW;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarSalesTaxS;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarSalesTaxW;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
