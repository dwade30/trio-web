﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arUTLienStatusReport.
	/// </summary>
	public partial class arUTLienStatusReport : BaseSectionReport
	{
		public arUTLienStatusReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Status Report";
		}

		public static arUTLienStatusReport InstancePtr
		{
			get
			{
				return (arUTLienStatusReport)Sys.GetInstance(typeof(arUTLienStatusReport));
			}
		}

		protected arUTLienStatusReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arUTLienStatusReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/02/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/02/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		string strSQL;
		double[] dblTotals = new double[4 + 1];
		double[] dblSummaryTotal = new double[5 + 1];
		int lngSummaryLineCT;
		double[,] dblArrTotals = new double[22000 + 1, 5 + 1];
		// 0 - fldSummaryPrin1
		// 1 - fldSummaryPrinPaid1
		// 2 - fldSummaryPLI1
		// 3 - fldSummaryCosts1
		// 4 - fldSummaryCurrentInt1
		// 5 - fldSummaryTotal1
		bool boolSummaryOnly;
		double[,] dblPayments = new double[10 + 1, 5 + 1];
		// 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total
		// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs
		bool boolWater;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// format the report
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolSummaryOnly = FCConvert.CBool(frmUTStatusList.InstancePtr.chkSummaryOnly.CheckState == Wisej.Web.CheckState.Checked);
			SetReportHeader();
			SetupFields();
			// build the SQL statement
			strSQL = BuildSQL();
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			SetFooterHeight(ref strSQL);
			// CreateSummaryTable strSQL
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this will return the correct SQL string depending on what the user choose on the frmUTStatusList
			string strFields;
			string strTemp;
			string strTempWhere = "";
			if (Strings.Trim(frmUTStatusList.InstancePtr.strRSWhere) != "")
			{
				strTempWhere = " WHERE " + frmUTStatusList.InstancePtr.strRSWhere;
			}
			else
			{
				strTempWhere = "";
			}
			strFields = "Account, BillingYear, Name1, Name2, LienRec.InterestCharged AS InterestCharged, LienRec.RateKey AS RateKey, LienRec.InterestAppliedThroughDate AS InterestAppliedThroughDate, Principal, Interest, Costs, LienRec.PrincipalPaid AS PrincipalPaid, LienRec.InterestPaid AS InterestPaid, PLIPaid, MaturityFee, CostsPaid, TransferFromBillingDateLast, LienRec.LienRecordNumber AS LRN ";
			strTemp = frmUTStatusList.InstancePtr.strRSOrder.Replace("RSName", "Name1");
			if (Strings.Trim(frmUTStatusList.InstancePtr.strRSWhere) == "")
			{
				if (Strings.Trim(strTemp) != "")
				{
					strTemp = frmUTStatusList.InstancePtr.strRSOrder.Replace("RSName", "Name1");
					strSQL = "SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.LienRecordNumber = BillingMaster.LienRecordNumber ORDER BY " + strTemp;
					// Principal > LienRec.PrincipalPaid
				}
				else
				{
					strSQL = "SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.LienRecordNumber = BillingMaster.LienRecordNumber";
					// WHERE Principal > LienRec.PrincipalPaid"
				}
			}
			else
			{
				if (Strings.Trim(strTemp) != "")
				{
					strSQL = "SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.LienRecordNumber = BillingMaster.LienRecordNumber " + strTempWhere + " ORDER BY " + strTemp;
				}
				else
				{
					strSQL = "SELECT " + strFields + " FROM LienRec INNER JOIN BillingMaster ON LienRec.LienRecordNumber = BillingMaster.LienRecordNumber " + strTempWhere;
				}
			}
			BuildSQL = strSQL;
			return BuildSQL;
		}

		private void BindFields()
		{
			// this will fill the fields in
			clsDRWrapper rsPayment = new clsDRWrapper();
			double dblCurInt = 0;
			double dblTotal = 0;
			double dblAmt = 0;
			double dblSum = 0;
			double dblPaymentRecieved = 0;
			double dblIntPaid = 0;
			double dblCostPaid = 0;
			double dblTempTotal;
			TRYAGAIN:
			;
			if (rsData.EndOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				fldAccount.Text = rsData.Get_Fields_String("Account");
				fldName.Text = modUTStatusList.GetStatusName_6(rsData, FCConvert.ToInt16(frmUTStatusList.InstancePtr.cmbNameOption.SelectedIndex));
				// Trim(rsData.Fields("Name1") & " " & rsData.Fields("Name2"))
				fldYear.Text = modExtraModules.FormatYear(FCConvert.ToString(rsData.Get_Fields_Int32("BillNumber")));
				dblPaymentRecieved = 0;
				dblIntPaid = 0;
				dblCostPaid = 0;
				dblAmt = 0;
				if (modMain.Statics.gboolUTUseAsOfDate)
				{
					// get the values from the payment records
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
					rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("Account") + " AND Year = " + rsData.Get_Fields_Int32("BillingYear") + " AND BillCode = 'L' AND BillKey = " + rsData.Get_Fields("LRN") + " AND RecordedTransactionDate <= #" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "#");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
					rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Account = " + rsData.Get_Fields("Account") + " AND Year = " + rsData.Get_Fields_Int32("BillingYear") + " AND BillCode = 'L' AND BillKey = " + rsData.Get_Fields("LRN"));
				}
				while (!rsPayment.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					dblPaymentRecieved += rsPayment.Get_Fields("Principal");
					dblIntPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest") + rsPayment.Get_Fields_Decimal("PreLienInterest"));
					dblCostPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "P")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(6, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					else if ((Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "X") || (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "S"))
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(9, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "U")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(8, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
							// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
							else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "Y")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(10, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
								// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
								else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "C")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(2, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "A")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(1, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
										// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
										else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "D")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(3, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
											// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
											else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "I")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(4, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
												// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
												else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "L")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(5, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
													// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
													else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "3")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(0, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
														// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
														else if (Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code"))) == "R")
					{
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						AddToPaymentArray_242(7, rsPayment.Get_Fields("Principal"), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest")), FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost")));
					}
					rsPayment.MoveNext();
				}
				if (!modMain.Statics.gboolUTUseAsOfDate)
				{
					// Original Principal
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					dblAmt = rsData.Get_Fields("Principal");
					fldPrincipal.Text = Strings.Format(dblAmt, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] += dblAmt;
					dblTotals[4] += dblAmt;
					// original principal total
					// this does not account into the line sum
					// Principal Due
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					dblAmt = rsData.Get_Fields("Principal") - rsData.Get_Fields_Decimal("PrincipalPaid");
					fldPrincipalDue.Text = Strings.Format(dblAmt, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] += dblAmt;
					dblTotals[0] += dblAmt;
					dblSum = dblAmt;
					// Interest Due
					// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
					dblAmt = rsData.Get_Fields("Interest") - FCConvert.ToDouble(rsData.Get_Fields("PLIPaid"));
					fldPLInt.Text = Strings.Format(dblAmt, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] += dblAmt;
					dblTotals[1] += dblAmt;
					dblSum += dblAmt;
					// Costs Due
					// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
					dblAmt = rsData.Get_Fields("Costs") - rsData.Get_Fields("MaturityFee") - rsData.Get_Fields_Decimal("CostsPaid");
					fldCosts.Text = Strings.Format(dblAmt, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] += dblAmt;
					dblTotals[2] += dblAmt;
					dblSum += dblAmt;
					dblTotal = modUTCalculations.CalculateAccountUTLien(rsData, DateTime.Now, ref dblCurInt, boolWater);
				}
				else
				{
					// Original Principal
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					dblAmt = rsData.Get_Fields("Principal");
					fldPrincipal.Text = Strings.Format(dblAmt, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] += dblAmt;
					dblTotals[4] += dblAmt;
					// original principal total
					// this does not account into the line sum
					// Principal Due
					// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
					dblAmt = rsData.Get_Fields("Principal") - dblPaymentRecieved;
					fldPrincipalDue.Text = Strings.Format(dblAmt, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] += dblAmt;
					dblTotals[0] += dblAmt;
					dblSum = dblAmt;
					// Interest Due
					// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
					dblAmt = rsData.Get_Fields("Interest") - dblIntPaid;
					fldPLInt.Text = Strings.Format(dblAmt, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] += dblAmt;
					dblTotals[1] += dblAmt;
					dblSum += dblAmt;
					// Costs Due
					// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
					dblAmt = rsData.Get_Fields("Costs") - dblCostPaid;
					fldCosts.Text = Strings.Format(dblAmt, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] += dblAmt;
					dblTotals[2] += dblAmt;
					dblSum += dblAmt;
					dblTotal = modUTCalculations.CalculateAccountUTLien(rsData, modMain.Statics.gdtUTStatusListAsOfDate, ref dblCurInt, boolWater);
					dblTotal = dblSum;
				}
				if (dblSum == 0 && dblTotal == 0)
				{
					rsData.MoveNext();
					// reverse all the the effects on the sums
					if (rsData.EndOfFile())
					{
						ReverseLastRecord(dblPaymentRecieved, dblIntPaid, dblCostPaid);
						dblSum = 0;
						dblTotal = 0;
					}
					else
					{
						dblAmt = FCConvert.ToDouble(fldPrincipal.Text);
						dblTotals[4] -= dblAmt;
						dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] -= dblAmt;
						dblAmt = FCConvert.ToDouble(fldPrincipalDue.Text);
						dblTotals[0] -= dblAmt;
						dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] -= dblAmt;
						dblAmt = FCConvert.ToDouble(fldPLInt.Text);
						dblTotals[1] -= dblAmt;
						dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] -= dblAmt;
						dblAmt = FCConvert.ToDouble(fldCosts.Text);
						dblTotals[2] -= dblAmt;
						dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] -= dblAmt;
						fldPrincipal.Text = "0.00";
						fldPrincipalDue.Text = "0.00";
						fldPLInt.Text = "0.00";
						fldCosts.Text = "0.00";
						dblCurInt = 0;
						dblSum = 0;
						dblTotal = 0;
					}
					goto TRYAGAIN;
				}
				dblCurInt += FCConvert.ToDouble(-rsData.Get_Fields_Decimal("InterestCharged") - rsData.Get_Fields_Decimal("InterestPaid"));
				fldCurrentInt.Text = Strings.Format(dblCurInt, "#,##0.00");
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 4] += dblCurInt;
				dblTotals[3] += dblCurInt;
				dblSum += dblCurInt;
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
				if (FCUtils.Round(dblTotal, 2) == FCUtils.Round(rsData.Get_Fields("Principal") + rsData.Get_Fields("Interest") + rsData.Get_Fields("Costs") + dblCurInt, 2))
				{
					fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 5] += dblTotal;
				}
				else
				{
					// I used the star to show that it did not match exactly, for debugging/testing purposes
					// fldTotal.Text = "*" & Format(rsData.Fields("Principal") + rsData.Fields("Interest") + rsData.Fields("Costs") + dblCurInt, "#,##0.00")
					fldTotal.Text = Strings.Format(dblSum, "#,##0.00");
					dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 5] += dblSum;
				}
				rsData.MoveNext();
			}
			rsPayment.Dispose();
		}

		private void SetupFields()
		{
			// this will set up the fields in the correct format depending on which fields are choosen on the frmUTStatusList
			if (boolSummaryOnly)
			{
				lblAccount.Visible = false;
				lblName.Visible = false;
				lblYear.Visible = false;
				fldAccount.Visible = false;
				fldYear.Visible = false;
				fldName.Visible = false;
				fldPrincipal.Visible = false;
				fldPrincipalDue.Visible = false;
				fldPLInt.Visible = false;
				fldCosts.Visible = false;
				fldCurrentInt.Visible = false;
				fldTotal.Visible = false;
				Line1.Visible = false;
				Line2.Visible = false;
				Detail.Height = 0;
				return;
			}
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
			for (intCT = 0; intCT <= frmUTStatusList.InstancePtr.vsWhere.Rows - 2; intCT++)
			{
				if (frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) != "" || frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2) != "")
				{
					switch (intCT)
					{
						case 0:
							{
								// Account Number
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
								{
									if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
									{
										if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
										{
											strTemp += "Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
										}
										else
										{
											strTemp += "Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
										}
									}
									else
									{
										strTemp += "Below Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								}
								else
								{
									strTemp += "Above Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
								break;
							}
						case 1:
							{
								// Name
								if (Strings.Trim(strTemp) != "")
									strTemp += "\r\n";
								strTemp += " Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								break;
							}
						case 2:
							{
								// Tax Year
								if (Strings.Trim(strTemp) != "")
									strTemp += "\r\n";
								strTemp += " Tax Year: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								break;
							}
						case 3:
							{
								// Balance Due
								if (Strings.Trim(strTemp) != "")
									strTemp += "\r\n";
								strTemp += " Balance Due " + Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) + " " + Strings.Format(Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)), "#,##0.00");
								break;
							}
						default:
							{
								break;
								break;
							}
					}
					//end switch
				}
			}
			if (modMain.Statics.gboolUTUseAsOfDate)
			{
				strTemp += "\r\n" + " As Of: " + Strings.Format(modMain.Statics.gdtUTStatusListAsOfDate, "MM/dd/yyyy");
			}
			else
			{
				strTemp += "\r\n" + " As Of: " + Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
			lblReportType.Text = strTemp;
		}

		private void CreateSummaryTable()
		{
			// strSQLString As String)
			int lngIndex;
			for (lngIndex = 19800; lngIndex <= 22000; lngIndex++)
			{
				if (dblArrTotals[lngIndex, 5] != 0)
				{
					AddSummaryLine(ref lngIndex, ref dblArrTotals[lngIndex, 0], ref dblArrTotals[lngIndex, 1], ref dblArrTotals[lngIndex, 2], ref dblArrTotals[lngIndex, 3], ref dblArrTotals[lngIndex, 4], ref dblArrTotals[lngIndex, 5], ref lngSummaryLineCT);
				}
			}
			AddSummaryLine_2(-1, dblSummaryTotal[0], dblSummaryTotal[1], dblSummaryTotal[2], dblSummaryTotal[3], dblSummaryTotal[4], dblSummaryTotal[5], ref lngSummaryLineCT);
		}

		private void AddSummaryLine_2(int lngYear, double dblAmount0, double dblAmount1, double dblAmount2, double dblAmount3, double dblAmount4, double dblAmount5, ref int lngCT)
		{
			AddSummaryLine(ref lngYear, ref dblAmount0, ref dblAmount1, ref dblAmount2, ref dblAmount3, ref dblAmount4, ref dblAmount5, ref lngCT);
		}

		private void AddSummaryLine(ref int lngYear, ref double dblAmount0, ref double dblAmount1, ref double dblAmount2, ref double dblAmount3, ref double dblAmount4, ref double dblAmount5, ref int lngCT)
		{
			string strTemp = "";
			// this will add another per diem line in the report footer
			if (lngCT == 0)
			{
				lngCT = 1;
				fldSummaryTotal1.Visible = true;
				lblSummaryYear1.Visible = true;
				lblLienSummary.Visible = true;
				lnSummaryHeader.Visible = true;
				lnSummaryTotal.Visible = true;
				fldSummaryPrin1.Visible = true;
				fldSummaryPrinPaid1.Visible = true;
				fldSummaryPLI1.Visible = true;
				fldSummaryCosts1.Visible = true;
				fldSummaryCurrentInt1.Visible = true;
				fldSummaryTotal1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				fldSummaryPrin1.Text = Strings.Format(dblAmount0, "#,##0.00");
				fldSummaryPrinPaid1.Text = Strings.Format(dblAmount1, "#,##0.00");
				fldSummaryPLI1.Text = Strings.Format(dblAmount2, "#,##0.00");
				fldSummaryCosts1.Text = Strings.Format(dblAmount3, "#,##0.00");
				fldSummaryCurrentInt1.Text = Strings.Format(dblAmount4, "#,##0.00");
				fldSummaryTotal1.Text = Strings.Format(dblAmount5, "#,##0.00");
				lblSummaryYear1.Text = modExtraModules.FormatYear(lngYear.ToString());
				dblSummaryTotal[0] += dblAmount0;
				dblSummaryTotal[1] += dblAmount1;
				dblSummaryTotal[2] += dblAmount2;
				dblSummaryTotal[3] += dblAmount3;
				dblSummaryTotal[4] += dblAmount4;
				dblSummaryTotal[5] += dblAmount5;
			}
			else
			{
				// increment the number of rows of fields
				lngCT += 1;
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
				// 0 - fldSummaryPrin1
				obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				obNew.Name = "fldSummaryPrin" + FCConvert.ToString(lngCT);
				obNew.Top = fldSummaryTotal1.Top + (((lngCT - 1) / 1440f) * fldSummaryTotal1.Height);
				obNew.Left = fldSummaryPrin1.Left;
				obNew.Width = fldSummaryPrin1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strTemp = obNew.Font.ToString();
				obNew.Font = fldSummaryTotal1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = Strings.Format(dblAmount0, "#,##0.00");
				dblSummaryTotal[0] += dblAmount0;
				this.ReportFooter.Controls.Add(obNew);
				// 1 - fldSummaryPrinPaid1
				obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				obNew.Name = "fldSummaryPrinPaid" + FCConvert.ToString(lngCT);
				obNew.Top = fldSummaryTotal1.Top + (((lngCT - 1) / 1440f) * fldSummaryTotal1.Height);
				obNew.Left = fldSummaryPrinPaid1.Left;
				obNew.Width = fldSummaryPrinPaid1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strTemp = obNew.Font.ToString();
				obNew.Font = fldSummaryTotal1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = Strings.Format(dblAmount1, "#,##0.00");
				dblSummaryTotal[1] += dblAmount1;
				this.ReportFooter.Controls.Add(obNew);
				// 2 - fldSummaryPLI1
				obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				obNew.Name = "fldSummaryPLI" + FCConvert.ToString(lngCT);
				obNew.Top = fldSummaryTotal1.Top + (((lngCT - 1) / 1440f) * fldSummaryTotal1.Height);
				obNew.Left = fldSummaryPLI1.Left;
				obNew.Width = fldSummaryPLI1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strTemp = obNew.Font.ToString();
				obNew.Font = fldSummaryTotal1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = Strings.Format(dblAmount2, "#,##0.00");
				dblSummaryTotal[2] += dblAmount2;
				this.ReportFooter.Controls.Add(obNew);
				// 3 - fldSummaryCosts1
				obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				obNew.Name = "fldSummaryCosts" + FCConvert.ToString(lngCT);
				obNew.Top = fldSummaryTotal1.Top + (((lngCT - 1) / 1440f) * fldSummaryTotal1.Height);
				obNew.Left = fldSummaryCosts1.Left;
				obNew.Width = fldSummaryCosts1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strTemp = obNew.Font.ToString();
				obNew.Font = fldSummaryTotal1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = Strings.Format(dblAmount3, "#,##0.00");
				dblSummaryTotal[3] += dblAmount3;
				this.ReportFooter.Controls.Add(obNew);
				// 4 - fldSummaryCurrentInt1
				obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				obNew.Name = "fldSummaryCurrentInt" + FCConvert.ToString(lngCT);
				obNew.Top = fldSummaryTotal1.Top + (((lngCT - 1) / 1440f) * fldSummaryTotal1.Height);
				obNew.Left = fldSummaryCurrentInt1.Left;
				obNew.Width = fldSummaryCurrentInt1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strTemp = obNew.Font.ToString();
				obNew.Font = fldSummaryTotal1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = Strings.Format(dblAmount4, "#,##0.00");
				dblSummaryTotal[4] += dblAmount4;
				this.ReportFooter.Controls.Add(obNew);
				// 5 - fldSummaryTotal1
				// add a field
				obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				obNew.Name = "fldSummaryTotal" + FCConvert.ToString(lngCT);
				obNew.Top = fldSummaryTotal1.Top + (((lngCT - 1) / 1440f) * fldSummaryTotal1.Height);
				obNew.Left = fldSummaryTotal1.Left;
				obNew.Width = fldSummaryTotal1.Width;
				obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				strTemp = obNew.Font.ToString();
				obNew.Font = fldSummaryTotal1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = Strings.Format(dblAmount5, "#,##0.00");
				dblSummaryTotal[5] += dblAmount5;
				this.ReportFooter.Controls.Add(obNew);
				// year
				// add a label
				obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				obNew.Name = "lblSummaryYear" + FCConvert.ToString(lngCT);
				obNew.Top = lblSummaryYear1.Top + (((lngCT - 1) / 1440f) * lblSummaryYear1.Height);
				obNew.Left = lblSummaryYear1.Left;
				strTemp = obNew.Font.ToString();
				obNew.Font = lblSummaryYear1.Font;
				// this sets the font to the same as the field that is already created
				if (lngYear == -1)
				{
					obNew.Text = "Total:";
					lnSummaryTotal.Y1 = obNew.Top;
					lnSummaryTotal.Y2 = obNew.Top;
				}
				else
				{
					obNew.Text = modExtraModules.FormatYear(lngYear.ToString());
				}
				this.ReportFooter.Controls.Add(obNew);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// fill the totals line in
			fldTotalPrincipal.Text = Strings.Format(dblTotals[4], "#,##0.00");
			fldTotalPrincipalDue.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotalPLInt.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldTotalCurrentInt.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[3], "#,##0.00");
			CreateSummaryTable();
			// strSQL
		}

		private void SetFooterHeight(ref string strSQLString)
		{
			clsDRWrapper rsSummary = new clsDRWrapper();
			rsSummary.OpenRecordset("SELECT BillingYear, SUM(Principal + Interest + Costs) AS Total FROM (" + strSQLString + ") GROUP BY BillingYear");
			this.ReportFooter.Height = lblLienSummary.Top + (lblLienSummary.Height * (rsSummary.RecordCount() + 2 / 1440f));
			rsSummary.Dispose();
		}

		private void AddToPaymentArray_242(int lngIndex, double dblPrin, double dblPLI, double dblCurInt, double dblCost)
		{
			AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblPLI, ref dblCurInt, ref dblCost);
		}

		private void AddToPaymentArray(ref int lngIndex, ref double dblPrin, ref double dblPLI, ref double dblCurInt, ref double dblCost)
		{
			dblPayments[lngIndex, 0] += dblPrin;
			dblPayments[lngIndex, 1] += dblPLI;
			dblPayments[lngIndex, 2] += dblCurInt;
			dblPayments[lngIndex, 3] += dblCost;
		}

		private void ReverseLastRecord(double dblPaymentRecieved = 0, double dblIntPaid = 0, double dblCostPaid = 0)
		{
			// this will remove the totals of the last record from year and final totals
			double dblAmt = 0;
			rsData.MoveLast();
			// get the last record back
			fldAccount.Text = "";
			fldName.Text = "";
			fldYear.Text = "";
			fldCurrentInt.Text = "";
			fldTotal.Text = "";
			if (!modMain.Statics.gboolUTUseAsOfDate)
			{
				// Original Principal
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblAmt = rsData.Get_Fields("Principal");
				fldPrincipal.Text = "";
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] -= dblAmt;
				dblTotals[4] -= dblAmt;
				// original principal total
				// this does not account into the line sum
				// Principal Due
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblAmt = rsData.Get_Fields("Principal") - rsData.Get_Fields_Decimal("PrincipalPaid");
				fldPrincipalDue.Text = "";
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] -= dblAmt;
				dblTotals[0] -= dblAmt;
				// Interest Due
				// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
				dblAmt = rsData.Get_Fields("Interest") - FCConvert.ToDouble(rsData.Get_Fields("PLIPaid"));
				fldPLInt.Text = "";
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] -= dblAmt;
				dblTotals[1] -= dblAmt;
				// Costs Due
				// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
				dblAmt = rsData.Get_Fields("Costs") - rsData.Get_Fields("MaturityFee") - rsData.Get_Fields_Decimal("CostsPaid");
				fldCosts.Text = "";
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] -= dblAmt;
				dblTotals[2] -= dblAmt;
			}
			else
			{
				// Original Principal
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblAmt = rsData.Get_Fields("Principal");
				fldPrincipal.Text = "";
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 0] -= dblAmt;
				dblTotals[4] -= dblAmt;
				// original principal total
				// this does not account into the line sum
				// Principal Due
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblAmt = rsData.Get_Fields("Principal") - dblPaymentRecieved;
				fldPrincipalDue.Text = "";
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 1] -= dblAmt;
				dblTotals[0] -= dblAmt;
				// Interest Due
				// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
				dblAmt = rsData.Get_Fields("Interest") - dblIntPaid;
				fldPLInt.Text = "";
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 2] -= dblAmt;
				dblTotals[1] -= dblAmt;
				// Costs Due
				// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
				dblAmt = rsData.Get_Fields("Costs") - dblCostPaid;
				fldCosts.Text = "";
				dblArrTotals[rsData.Get_Fields_Int32("BillingYear"), 3] -= dblAmt;
				dblTotals[2] -= dblAmt;
			}
			this.Detail.Height = 0;
			// go to the next record
			rsData.MoveNext();
		}

		private void arUTLienStatusReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arUTLienStatusReport properties;
			//arUTLienStatusReport.Caption	= "Lien Status Report";
			//arUTLienStatusReport.Icon	= "arUTLienStatusReport.dsx":0000";
			//arUTLienStatusReport.Left	= 0;
			//arUTLienStatusReport.Top	= 0;
			//arUTLienStatusReport.Width	= 11880;
			//arUTLienStatusReport.Height	= 8595;
			//arUTLienStatusReport.StartUpPosition	= 3;
			//arUTLienStatusReport.SectionData	= "arUTLienStatusReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
