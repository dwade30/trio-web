﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmMeterChangeOut.
	/// </summary>
	partial class frmMeterChangeOut : BaseForm
	{
		public fecherFoundation.FCFrame fraInformation;
		public fecherFoundation.FCTextBox txtReason;
		public Global.T2KDateBox txtSetDate;
		public fecherFoundation.FCComboBox cboDgits;
		public fecherFoundation.FCComboBox cboSize;
		public fecherFoundation.FCComboBox cboBackflow;
		public fecherFoundation.FCTextBox txtSerialNumber;
		public fecherFoundation.FCTextBox txtRemoteNumber;
		public fecherFoundation.FCTextBox txtNewMeterReading;
		public fecherFoundation.FCTextBox txtOldMeterReading;
		public fecherFoundation.FCLabel lblReason;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel lblPreviousReading;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCGrid vsMeters;
		public fecherFoundation.FCComboBox cboAccounts;
		public fecherFoundation.FCFrame fraAccountSearch;
		public fecherFoundation.FCTextBox txtSearchAccount;
		public fecherFoundation.FCTextBox txtSearchName;
		public fecherFoundation.FCGrid vsAccountSearch;
		public fecherFoundation.FCLabel lblSearchAccount;
		public fecherFoundation.FCLabel lblSearchName;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileNameSearch;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMeterChangeOut));
            this.txtNewMeterReading = new fecherFoundation.FCTextBox();
            this.txtOldMeterReading = new fecherFoundation.FCTextBox();
            this.fraInformation = new fecherFoundation.FCFrame();
            this.txtReason = new fecherFoundation.FCTextBox();
            this.txtSetDate = new Global.T2KDateBox();
            this.cboDgits = new fecherFoundation.FCComboBox();
            this.cboSize = new fecherFoundation.FCComboBox();
            this.cboBackflow = new fecherFoundation.FCComboBox();
            this.txtSerialNumber = new fecherFoundation.FCTextBox();
            this.txtRemoteNumber = new fecherFoundation.FCTextBox();
            this.lblReason = new fecherFoundation.FCLabel();
            this.Label9 = new fecherFoundation.FCLabel();
            this.lblPreviousReading = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.vsMeters = new fecherFoundation.FCGrid();
            this.cboAccounts = new fecherFoundation.FCComboBox();
            this.fraAccountSearch = new fecherFoundation.FCFrame();
            this.txtSearchAccount = new fecherFoundation.FCTextBox();
            this.txtSearchName = new fecherFoundation.FCTextBox();
            this.vsAccountSearch = new fecherFoundation.FCGrid();
            this.lblSearchAccount = new fecherFoundation.FCLabel();
            this.lblSearchName = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileNameSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.cmdFileNameSearch = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).BeginInit();
            this.fraInformation.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSetDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsMeters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).BeginInit();
            this.fraAccountSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileNameSearch)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 482);
            this.BottomPanel.Size = new System.Drawing.Size(1024, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraInformation);
            this.ClientArea.Controls.Add(this.fraAccountSearch);
            this.ClientArea.Controls.Add(this.cboAccounts);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Size = new System.Drawing.Size(1024, 422);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileNameSearch);
            this.TopPanel.Size = new System.Drawing.Size(1024, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileNameSearch, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(214, 30);
            this.HeaderText.Text = "Meter Change Out";
            // 
            // txtNewMeterReading
            // 
            this.txtNewMeterReading.BackColor = System.Drawing.SystemColors.Window;
            this.txtNewMeterReading.Location = new System.Drawing.Point(212, 130);
            this.txtNewMeterReading.Name = "txtNewMeterReading";
            this.txtNewMeterReading.Size = new System.Drawing.Size(131, 40);
            this.txtNewMeterReading.TabIndex = 6;
            this.txtNewMeterReading.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtNewMeterReading_KeyPress);
            // 
            // txtOldMeterReading
            // 
            this.txtOldMeterReading.BackColor = System.Drawing.SystemColors.Window;
            this.txtOldMeterReading.Location = new System.Drawing.Point(212, 80);
            this.txtOldMeterReading.Name = "txtOldMeterReading";
            this.txtOldMeterReading.Size = new System.Drawing.Size(131, 40);
            this.txtOldMeterReading.TabIndex = 4;
            this.txtOldMeterReading.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtOldMeterReading_KeyPress);
            // 
            // fraInformation
            // 
            this.fraInformation.Controls.Add(this.txtReason);
            this.fraInformation.Controls.Add(this.txtSetDate);
            this.fraInformation.Controls.Add(this.cboDgits);
            this.fraInformation.Controls.Add(this.cboSize);
            this.fraInformation.Controls.Add(this.cboBackflow);
            this.fraInformation.Controls.Add(this.txtSerialNumber);
            this.fraInformation.Controls.Add(this.txtRemoteNumber);
            this.fraInformation.Controls.Add(this.txtNewMeterReading);
            this.fraInformation.Controls.Add(this.txtOldMeterReading);
            this.fraInformation.Controls.Add(this.lblReason);
            this.fraInformation.Controls.Add(this.Label9);
            this.fraInformation.Controls.Add(this.lblPreviousReading);
            this.fraInformation.Controls.Add(this.Label8);
            this.fraInformation.Controls.Add(this.Label7);
            this.fraInformation.Controls.Add(this.Label6);
            this.fraInformation.Controls.Add(this.Label5);
            this.fraInformation.Controls.Add(this.Label4);
            this.fraInformation.Controls.Add(this.Label3);
            this.fraInformation.Controls.Add(this.Label2);
            this.fraInformation.Location = new System.Drawing.Point(30, 90);
            this.fraInformation.Name = "fraInformation";
            this.fraInformation.Size = new System.Drawing.Size(525, 575);
            this.fraInformation.TabIndex = 4;
            this.fraInformation.Text = "Enter Information";
            this.fraInformation.Visible = false;
            // 
            // txtReason
            // 
            this.txtReason.BackColor = System.Drawing.SystemColors.Window;
            this.txtReason.Location = new System.Drawing.Point(212, 430);
            this.txtReason.MaxLength = 255;
            this.txtReason.Multiline = true;
            this.txtReason.Name = "txtReason";
            this.txtReason.ScrollBars = Wisej.Web.ScrollBars.Vertical;
            this.txtReason.Size = new System.Drawing.Size(293, 126);
            this.txtReason.TabIndex = 18;
            // 
            // txtSetDate
            // 
            this.txtSetDate.Location = new System.Drawing.Point(212, 380);
            this.txtSetDate.Mask = "##/##/####";
            this.txtSetDate.Name = "txtSetDate";
            this.txtSetDate.Size = new System.Drawing.Size(131, 40);
            this.txtSetDate.TabIndex = 16;
            // 
            // cboDgits
            // 
            this.cboDgits.BackColor = System.Drawing.SystemColors.Window;
            this.cboDgits.Location = new System.Drawing.Point(212, 30);
            this.cboDgits.Name = "cboDgits";
            this.cboDgits.Size = new System.Drawing.Size(131, 40);
            this.cboDgits.TabIndex = 1;
            this.cboDgits.SelectedIndexChanged += new System.EventHandler(this.cboDgits_SelectedIndexChanged);
            // 
            // cboSize
            // 
            this.cboSize.BackColor = System.Drawing.SystemColors.Window;
            this.cboSize.Location = new System.Drawing.Point(212, 330);
            this.cboSize.Name = "cboSize";
            this.cboSize.Size = new System.Drawing.Size(133, 40);
            this.cboSize.TabIndex = 14;
            this.cboSize.DropDown += new System.EventHandler(this.cboSize_DropDown);
            // 
            // cboBackflow
            // 
            this.cboBackflow.BackColor = System.Drawing.SystemColors.Window;
            this.cboBackflow.Location = new System.Drawing.Point(212, 280);
            this.cboBackflow.Name = "cboBackflow";
            this.cboBackflow.Size = new System.Drawing.Size(131, 40);
            this.cboBackflow.TabIndex = 12;
            this.cboBackflow.DropDown += new System.EventHandler(this.cboBackflow_DropDown);
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtSerialNumber.Location = new System.Drawing.Point(212, 230);
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(131, 40);
            this.txtSerialNumber.TabIndex = 10;
            // 
            // txtRemoteNumber
            // 
            this.txtRemoteNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtRemoteNumber.Location = new System.Drawing.Point(212, 180);
            this.txtRemoteNumber.Name = "txtRemoteNumber";
            this.txtRemoteNumber.Size = new System.Drawing.Size(131, 40);
            this.txtRemoteNumber.TabIndex = 8;
            // 
            // lblReason
            // 
            this.lblReason.Location = new System.Drawing.Point(20, 444);
            this.lblReason.Name = "lblReason";
            this.lblReason.Size = new System.Drawing.Size(113, 18);
            this.lblReason.TabIndex = 17;
            this.lblReason.Text = "CHANGE REASON";
            // 
            // Label9
            // 
            this.Label9.Location = new System.Drawing.Point(20, 394);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(73, 18);
            this.Label9.TabIndex = 15;
            this.Label9.Text = "SET DATE";
            // 
            // lblPreviousReading
            // 
            this.lblPreviousReading.Location = new System.Drawing.Point(367, 44);
            this.lblPreviousReading.Name = "lblPreviousReading";
            this.lblPreviousReading.Size = new System.Drawing.Size(125, 50);
            this.lblPreviousReading.TabIndex = 2;
            this.lblPreviousReading.Text = "PREVIOUS";
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(20, 44);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(73, 18);
            this.Label8.Text = "DIGITS";
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(20, 344);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(73, 18);
            this.Label7.TabIndex = 13;
            this.Label7.Text = "SIZE";
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(20, 144);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(123, 18);
            this.Label6.TabIndex = 5;
            this.Label6.Text = "NEW METER READING";
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 94);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(130, 18);
            this.Label5.TabIndex = 3;
            this.Label5.Text = "OLD METER READING";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 294);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(73, 18);
            this.Label4.TabIndex = 11;
            this.Label4.Text = "BACKFLOW";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 244);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(73, 18);
            this.Label3.TabIndex = 9;
            this.Label3.Text = "SERIAL #";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 194);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(73, 18);
            this.Label2.TabIndex = 7;
            this.Label2.Text = "REMOTE #";
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AppearanceKey = "groupBoxNoBorder";
            this.Frame1.Controls.Add(this.vsMeters);
            this.Frame1.Location = new System.Drawing.Point(30, 90);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(958, 288);
            this.Frame1.TabIndex = 2;
            this.Frame1.Text = "Select Meter";
            this.Frame1.Visible = false;
            // 
            // vsMeters
            // 
            this.vsMeters.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsMeters.Cols = 7;
            this.vsMeters.ExtendLastCol = true;
            this.vsMeters.FixedCols = 0;
            this.vsMeters.Location = new System.Drawing.Point(0, 30);
            this.vsMeters.Name = "vsMeters";
            this.vsMeters.RowHeadersVisible = false;
            this.vsMeters.Rows = 1;
            this.vsMeters.ShowFocusCell = false;
            this.vsMeters.Size = new System.Drawing.Size(919, 240);
            this.vsMeters.TabIndex = 3;
            this.vsMeters.DoubleClick += new System.EventHandler(this.vsMeters_DblClick);
            this.vsMeters.KeyDown += new Wisej.Web.KeyEventHandler(this.vsMeters_KeyDownEvent);
            // 
            // cboAccounts
            // 
            this.cboAccounts.BackColor = System.Drawing.SystemColors.Window;
            this.cboAccounts.Location = new System.Drawing.Point(199, 30);
            this.cboAccounts.Name = "cboAccounts";
            this.cboAccounts.Size = new System.Drawing.Size(545, 40);
            this.cboAccounts.Sorted = true;
            this.cboAccounts.SelectedIndexChanged += new System.EventHandler(this.cboAccounts_SelectedIndexChanged);
            // 
            // fraAccountSearch
            // 
            this.fraAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraAccountSearch.Controls.Add(this.txtSearchAccount);
            this.fraAccountSearch.Controls.Add(this.txtSearchName);
            this.fraAccountSearch.Controls.Add(this.vsAccountSearch);
            this.fraAccountSearch.Controls.Add(this.lblSearchAccount);
            this.fraAccountSearch.Controls.Add(this.lblSearchName);
            this.fraAccountSearch.Location = new System.Drawing.Point(30, 90);
            this.fraAccountSearch.Name = "fraAccountSearch";
            this.fraAccountSearch.Size = new System.Drawing.Size(977, 270);
            this.fraAccountSearch.TabIndex = 23;
            this.fraAccountSearch.Text = "Account Search";
            this.fraAccountSearch.Visible = false;
            // 
            // txtSearchAccount
            // 
            this.txtSearchAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearchAccount.Location = new System.Drawing.Point(461, 30);
            this.txtSearchAccount.Name = "txtSearchAccount";
            this.txtSearchAccount.Size = new System.Drawing.Size(127, 40);
            this.txtSearchAccount.TabIndex = 24;
            this.txtSearchAccount.TabStop = false;
            this.txtSearchAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearchAccount_KeyDown);
            // 
            // txtSearchName
            // 
            this.txtSearchName.BackColor = System.Drawing.SystemColors.Window;
            this.txtSearchName.Location = new System.Drawing.Point(132, 30);
            this.txtSearchName.Name = "txtSearchName";
            this.txtSearchName.Size = new System.Drawing.Size(127, 40);
            this.txtSearchName.TabIndex = 25;
            this.txtSearchName.TabStop = false;
            this.txtSearchName.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearchName_KeyDown);
            // 
            // vsAccountSearch
            // 
            this.vsAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsAccountSearch.Location = new System.Drawing.Point(20, 90);
            this.vsAccountSearch.Name = "vsAccountSearch";
            this.vsAccountSearch.Rows = 1;
            this.vsAccountSearch.ShowFocusCell = false;
            this.vsAccountSearch.Size = new System.Drawing.Size(938, 160);
            this.vsAccountSearch.TabIndex = 26;
            this.vsAccountSearch.DoubleClick += new System.EventHandler(this.vsAccountSearch_DblClick);
            // 
            // lblSearchAccount
            // 
            this.lblSearchAccount.Location = new System.Drawing.Point(333, 44);
            this.lblSearchAccount.Name = "lblSearchAccount";
            this.lblSearchAccount.Size = new System.Drawing.Size(80, 23);
            this.lblSearchAccount.TabIndex = 28;
            this.lblSearchAccount.Text = "ACCOUNT";
            // 
            // lblSearchName
            // 
            this.lblSearchName.Location = new System.Drawing.Point(30, 44);
            this.lblSearchName.Name = "lblSearchName";
            this.lblSearchName.Size = new System.Drawing.Size(56, 23);
            this.lblSearchName.TabIndex = 27;
            this.lblSearchName.Text = "NAME";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(123, 18);
            this.Label1.TabIndex = 1;
            this.Label1.Text = "SELECT ACCOUNT";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileNameSearch,
            this.mnuProcessSave,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileNameSearch
            // 
            this.mnuFileNameSearch.Index = 0;
            this.mnuFileNameSearch.Name = "mnuFileNameSearch";
            this.mnuFileNameSearch.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuFileNameSearch.Text = "Search";
            this.mnuFileNameSearch.Click += new System.EventHandler(this.mnuFileNameSearch_Click);
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 1;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSave.Text = "Save & Exit";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(480, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(95, 48);
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // cmdFileNameSearch
            // 
            this.cmdFileNameSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileNameSearch.ImageSource = "button-search";
            this.cmdFileNameSearch.Location = new System.Drawing.Point(950, 29);
            this.cmdFileNameSearch.Name = "cmdFileNameSearch";
            this.cmdFileNameSearch.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdFileNameSearch.Size = new System.Drawing.Size(81, 24);
            this.cmdFileNameSearch.TabIndex = 1;
            this.cmdFileNameSearch.Text = "Search";
            this.cmdFileNameSearch.Click += new System.EventHandler(this.mnuFileNameSearch_Click);
            // 
            // frmMeterChangeOut
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1024, 590);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmMeterChangeOut";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Meter Change Out";
            this.Load += new System.EventHandler(this.frmMeterChangeOut_Load);
            this.Activated += new System.EventHandler(this.frmMeterChangeOut_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMeterChangeOut_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInformation)).EndInit();
            this.fraInformation.ResumeLayout(false);
            this.fraInformation.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSetDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsMeters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).EndInit();
            this.fraAccountSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileNameSearch)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
		private FCButton cmdFileNameSearch;
	}
}
