﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptAnalMeterReportS.
	/// </summary>
	public partial class rptAnalMeterReportS : BaseSectionReport
	{
		public rptAnalMeterReportS()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Meter Report Report";
		}

		public static rptAnalMeterReportS InstancePtr
		{
			get
			{
				return (rptAnalMeterReportS)Sys.GetInstance(typeof(rptAnalMeterReportS));
			}
		}

		protected rptAnalMeterReportS _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAnalMeterReportS	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/26/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/16/2006              *
		// ********************************************************
		int lngTotalCons;
		// holds total Consumption
		int lngTotalCount;
		int lngSize;
		bool boolPreBilling;
		bool boolLandscape;
		float lngWidth;
		bool boolGo;
		int lngArrayIndex;
		bool boolDone;

		private struct MeterSizeTotals
		{
			public string ShortDescription;
			public int Count;
			public int Consumption;
			public int MeterSizeCode;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public MeterSizeTotals(int unusedParam)
			{
				this.ShortDescription = string.Empty;
				this.Count = 0;
				this.Consumption = 0;
				this.MeterSizeCode = 0;
			}
		};

		MeterSizeTotals[] MeterSizeArray = null;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// sets the book number on the Page header
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngWidth = rptAnalysisReportsMaster.InstancePtr.lngWidth;
			boolLandscape = rptAnalysisReportsMaster.InstancePtr.boolLandscape;
			boolPreBilling = rptAnalysisReportsMaster.InstancePtr.boolPreBilling;
			BuildSQL();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				TRYAGAIN:
				;
				if (lngArrayIndex <= Information.UBound(MeterSizeArray, 1))
				{
					if (MeterSizeArray[lngArrayIndex].Count > 0 || MeterSizeArray[lngArrayIndex].Consumption > 0)
					{
						fldMeterSize.Text = MeterSizeArray[lngArrayIndex].ShortDescription;
						fldConsumption.Text = MeterSizeArray[lngArrayIndex].Consumption.ToString();
						fldCount.Text = MeterSizeArray[lngArrayIndex].Count.ToString();
						fldCode.Text = MeterSizeArray[lngArrayIndex].MeterSizeCode.ToString();
						lngTotalCons += MeterSizeArray[lngArrayIndex].Consumption;
						lngTotalCount += MeterSizeArray[lngArrayIndex].Count;
						lngArrayIndex += 1;
					}
					else
					{
						// this will skip all empty ones
						lngArrayIndex += 1;
						goto TRYAGAIN;
					}
				}
				else
				{
					boolDone = true;
					fldMeterSize.Text = "";
					fldConsumption.Text = "";
					fldCount.Text = "";
					fldCode.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields - MR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
            clsDRWrapper rsMeter = new clsDRWrapper();
            clsDRWrapper rsExtraMeters = new clsDRWrapper();
            clsDRWrapper rsMeterSize = new clsDRWrapper();
            clsDRWrapper rsCount = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                string strTemp = "";
                string strFields = "";
                int lngCT = 0;

                int lngCons = 0;
                bool boolMS = false;
                bool boolCT = false;
                int lngMax;
                string strRK = "";
                string strFinal = "";
                if (rptAnalysisReportsMaster.InstancePtr.boolFinal)
                {
                    strFinal = " AND ISNULL(Bill.Final,0) = 1 ";
                }
                else
                {
                    strFinal = "";
                }

                if (boolPreBilling)
                {
                    strTemp = "<>";
                    strRK = "";
                }
                else
                {
                    strTemp = "=";
                    strRK = " AND BillingRateKey IN " + rptAnalysisReportsMaster.InstancePtr.strRateKeyList;
                }

                // Get the MeterSize list
                rsMeterSize.OpenRecordset("SELECT * FROM MeterSizes ORDER BY Code", modExtraModules.strUTDatabase);
                if (!rsMeterSize.EndOfFile())
                {
                    MeterSizeArray = new MeterSizeTotals[rsMeterSize.RecordCount() + 1];
                    // Get the eligible bill list to find consumptions
                    rsMeter.OpenRecordset(
                        "SELECT MeterTable.ID,MeterNumber,Bill.AccountKey,Size,Consumption FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE (" +
                        rptAnalysisReportsMaster.InstancePtr.strBookList + ") AND BillStatus " + strTemp +
                        " 'B' AND MeterTable.Service <> 'W'" + strRK + strFinal, modExtraModules.strUTDatabase);
                    // This will find the count
                    rsCount.OpenRecordset(
                        "SELECT * FROM MeterTable INNER JOIN (SELECT Distinct ID AS MK FROM (" + rsMeter.Name() +
                        ") AS qTmp) AS List ON MeterTable.ID = List.MK", modExtraModules.strUTDatabase);
                    // loop through the metersizes and fill the array
                    while (!rsMeterSize.EndOfFile())
                    {
                        boolMS = true;
                        // Fill in the code and the description
                        // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                        MeterSizeArray[lngCT].MeterSizeCode = FCConvert.ToInt32(rsMeterSize.Get_Fields("Code"));
                        if (Strings.Trim(rsMeterSize.Get_Fields_String("ShortDescription") + " ") == "")
                        {
                            MeterSizeArray[lngCT].ShortDescription = "Uncoded";
                        }
                        else
                        {
                            MeterSizeArray[lngCT].ShortDescription =
                                FCConvert.ToString(rsMeterSize.Get_Fields_String("ShortDescription"));
                        }

                        // increment the counters
                        lngCT += 1;
                        rsMeterSize.MoveNext();
                    }

                    // loop through the meters and determine the meter count from the meter key list (No duplicates)
                    while (!rsCount.EndOfFile())
                    {
                        boolCT = true;
                        lngCT = GetMeterCodeIndex_2(rsCount.Get_Fields_Int32("Size"));
                        // This will return the Index of the code
                        if (lngCT >= 0)
                        {
                            MeterSizeArray[lngCT].Count += 1;
                            if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("MeterNumber")) == 1)
                            {
                                // A main meter can only have a meternumber of 1
                                // check for combined meters
                                rsExtraMeters.OpenRecordset(
                                    "SELECT Size FROM MeterTable WHERE AccountKey = " +
                                    rsMeter.Get_Fields_Int32("AccountKey") + " AND Combine <> 'N' AND Service <> 'W'",
                                    modExtraModules.strUTDatabase);
                                while (!rsExtraMeters.EndOfFile())
                                {
                                    lngCT = GetMeterCodeIndex_2(
                                        FCConvert.ToInt32(rsExtraMeters.Get_Fields_Int32("Size")));
                                    // This will return the Index of the code
                                    if (lngCT >= 0)
                                    {
                                        MeterSizeArray[lngCT].Count += 1;
                                    }

                                    rsExtraMeters.MoveNext();
                                }
                            }
                        }

                        rsCount.MoveNext();
                    }

                    // Loop through all the meters and calculate the consumption from the bills
                    while (!rsMeter.EndOfFile())
                    {
                        // This will let me know that both sizes, a count and meters were
                        // present and there will be some sort of totals
                        boolGo = boolMS && boolCT;
                        // Reset Variables
                        lngCons = 0;
                        // Find the total the consumption and only show it in the size of the main meter
                        lngCT = GetMeterCodeIndex_2(FCConvert.ToInt32(rsMeter.Get_Fields_Int32("Size")));
                        // This will return the Index of the code
                        if (lngCT >= 0)
                        {
                            lngCons = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("Consumption"));
                            MeterSizeArray[lngCT].Consumption += lngCons;
                        }

                        rsMeter.MoveNext();
                    }
                }
                else
                {
                    MessageBox.Show("No meter sizes have been set up.  Please return to Table File Setup to do so.",
                        "No Meter Sizes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                }

                return BuildSQL;
            }
            catch (Exception ex)
            {

                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Building SQL - MR", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
                rsMeter.Dispose();
                rsExtraMeters.Dispose();
                rsMeterSize.Dispose();
                rsCount.Dispose();
			}
			return BuildSQL;
		}

		private void ShowEndTotals()
		{
			// this will fill the totals in
			fldTotalConsumption.Text = lngTotalCons.ToString();
			fldTotalCount.Text = lngTotalCount.ToString();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (boolPreBilling)
			{
				lblDisclaimer.Text = "";
			}
			else
			{
				lblDisclaimer.Text = "*** Consumption totals may be skewed because of combined meters and changes in meter size.";
			}
			// fill in the report footer
			ShowEndTotals();
		}

		private int GetMeterCodeIndex_2(int lngCode)
		{
			return GetMeterCodeIndex(ref lngCode);
		}

		private int GetMeterCodeIndex(ref int lngCode)
		{
			int GetMeterCodeIndex = 0;
			// this function will find the index of the meter size
			int lngIndex;
			for (lngIndex = 0; lngIndex <= Information.UBound(MeterSizeArray, 1); lngIndex++)
			{
				if (lngCode == MeterSizeArray[lngIndex].MeterSizeCode)
				{
					GetMeterCodeIndex = lngIndex;
					return GetMeterCodeIndex;
				}
			}
			// else return a -1 so I can skip this meter
			GetMeterCodeIndex = -1;
			return GetMeterCodeIndex;
		}

		
	}
}
