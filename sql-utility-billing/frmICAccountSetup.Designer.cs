﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmICAccountSetup.
	/// </summary>
	partial class frmICAccountSetup : BaseForm
	{
		public fecherFoundation.FCPanel fraReceiptList;
		public fecherFoundation.FCFrame fraOptions;
		public fecherFoundation.FCComboBox cmbType;
		public fecherFoundation.FCTextBox txtTitle;
		public FCGrid cmbCode;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public FCGrid vsReceipt;
		public fecherFoundation.FCFrame fraDefaultAccount;
		public FCGrid txtDefaultAccount;
		public fecherFoundation.FCLabel lblDefaultAccount;
		public fecherFoundation.FCLabel lblAccountTitle;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCPanel fraNewType;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdNewCode;
		public fecherFoundation.FCTextBox txtCode;
		public fecherFoundation.FCLabel Label15;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileBack;
		public fecherFoundation.FCToolStripMenuItem mnuFileForward;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmICAccountSetup));
            this.fraReceiptList = new fecherFoundation.FCPanel();
            this.fraDefaultAccount = new fecherFoundation.FCFrame();
            this.txtDefaultAccount = new fecherFoundation.FCGrid();
            this.lblDefaultAccount = new fecherFoundation.FCLabel();
            this.fraOptions = new fecherFoundation.FCFrame();
            this.cmbType = new fecherFoundation.FCComboBox();
            this.txtTitle = new fecherFoundation.FCTextBox();
            this.cmbCode = new fecherFoundation.FCGrid();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.vsReceipt = new fecherFoundation.FCGrid();
            this.lblAccountTitle = new fecherFoundation.FCLabel();
            this.Label8 = new fecherFoundation.FCLabel();
            this.fraNewType = new fecherFoundation.FCPanel();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdNewCode = new fecherFoundation.FCButton();
            this.txtCode = new fecherFoundation.FCTextBox();
            this.Label15 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileBack = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileForward = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdFileBack = new fecherFoundation.FCButton();
            this.cmdFileForward = new fecherFoundation.FCButton();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceiptList)).BeginInit();
            this.fraReceiptList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultAccount)).BeginInit();
            this.fraDefaultAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOptions)).BeginInit();
            this.fraOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsReceipt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraNewType)).BeginInit();
            this.fraNewType.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileBack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileForward)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(997, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraReceiptList);
            this.ClientArea.Controls.Add(this.fraNewType);
            this.ClientArea.Size = new System.Drawing.Size(997, 520);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileForward);
            this.TopPanel.Controls.Add(this.cmdFileBack);
            this.TopPanel.Size = new System.Drawing.Size(997, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileBack, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileForward, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(172, 30);
            this.HeaderText.Text = "Account Setup";
            // 
            // fraReceiptList
            // 
            this.fraReceiptList.AppearanceKey = "groupBoxNoBorders";
            this.fraReceiptList.Controls.Add(this.fraDefaultAccount);
            this.fraReceiptList.Controls.Add(this.fraOptions);
            this.fraReceiptList.Controls.Add(this.vsReceipt);
            this.fraReceiptList.Controls.Add(this.lblAccountTitle);
            this.fraReceiptList.Controls.Add(this.Label8);
            this.fraReceiptList.Name = "fraReceiptList";
            this.fraReceiptList.Size = new System.Drawing.Size(977, 501);
            // 
            // fraDefaultAccount
            // 
            this.fraDefaultAccount.AppearanceKey = "groupBoxNoBorders";
            this.fraDefaultAccount.Controls.Add(this.txtDefaultAccount);
            this.fraDefaultAccount.Controls.Add(this.lblDefaultAccount);
            this.fraDefaultAccount.Location = new System.Drawing.Point(11, 431);
            this.fraDefaultAccount.Name = "fraDefaultAccount";
            this.fraDefaultAccount.Size = new System.Drawing.Size(486, 61);
            this.fraDefaultAccount.TabIndex = 4;
            // 
            // txtDefaultAccount
            // 
            this.txtDefaultAccount.Cols = 1;
            this.txtDefaultAccount.ColumnHeadersVisible = false;
            this.txtDefaultAccount.ExtendLastCol = true;
            this.txtDefaultAccount.FixedCols = 0;
            this.txtDefaultAccount.FixedRows = 0;
            this.txtDefaultAccount.Location = new System.Drawing.Point(246, 10);
            this.txtDefaultAccount.Name = "txtDefaultAccount";
            this.txtDefaultAccount.RowHeadersVisible = false;
            this.txtDefaultAccount.Rows = 1;
            this.txtDefaultAccount.Size = new System.Drawing.Size(203, 42);
            this.txtDefaultAccount.TabIndex = 1;
            this.txtDefaultAccount.Enter += new System.EventHandler(this.txtDefaultAccount_Enter);
            this.txtDefaultAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtDefaultAccount_Validating);
            this.txtDefaultAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtDefaultAccount_KeyDownEvent);
            // 
            // lblDefaultAccount
            // 
            this.lblDefaultAccount.Location = new System.Drawing.Point(30, 17);
            this.lblDefaultAccount.Name = "lblDefaultAccount";
            this.lblDefaultAccount.Size = new System.Drawing.Size(181, 25);
            this.lblDefaultAccount.TabIndex = 2;
            // 
            // fraOptions
            // 
            this.fraOptions.AppearanceKey = "groupBoxNoBorders";
            this.fraOptions.Controls.Add(this.cmbType);
            this.fraOptions.Controls.Add(this.txtTitle);
            this.fraOptions.Controls.Add(this.cmbCode);
            this.fraOptions.Controls.Add(this.Label2);
            this.fraOptions.Controls.Add(this.Label1);
            this.fraOptions.Name = "fraOptions";
            this.fraOptions.Size = new System.Drawing.Size(760, 89);
            this.fraOptions.TabIndex = 5;
            // 
            // cmbType
            // 
            this.cmbType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbType.Location = new System.Drawing.Point(89, 28);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(79, 40);
            this.cmbType.Sorted = true;
            this.cmbType.TabIndex = 1;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            this.cmbType.DropDown += new System.EventHandler(this.cmbType_DropDown);
            this.cmbType.DoubleClick += new System.EventHandler(this.cmbType_DoubleClick);
            this.cmbType.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbType_KeyDown);
            // 
            // txtTitle
            // 
            this.txtTitle.BackColor = System.Drawing.SystemColors.Window;
            this.txtTitle.Enabled = false;
            this.txtTitle.Location = new System.Drawing.Point(338, 30);
            this.txtTitle.MaxLength = 30;
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.Size = new System.Drawing.Size(352, 40);
            this.txtTitle.TabIndex = 4;
            this.txtTitle.Enter += new System.EventHandler(this.txtTitle_Enter);
            this.txtTitle.TextChanged += new System.EventHandler(this.txtTitle_TextChanged);
            // 
            // cmbCode
            // 
            this.cmbCode.Cols = 1;
            this.cmbCode.ColumnHeadersVisible = false;
            this.cmbCode.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.cmbCode.FixedCols = 0;
            this.cmbCode.FixedRows = 0;
            this.cmbCode.Location = new System.Drawing.Point(702, 37);
            this.cmbCode.Name = "cmbCode";
            this.cmbCode.ReadOnly = false;
            this.cmbCode.RowHeadersVisible = false;
            this.cmbCode.Rows = 1;
            this.cmbCode.Size = new System.Drawing.Size(52, 40);
            this.cmbCode.TabIndex = 2;
            this.cmbCode.Visible = false;
            this.cmbCode.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDownEdit);
            this.cmbCode.ComboDropDown += new System.EventHandler(this.cmbCode_ComboDropDown);
            this.cmbCode.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.cmbCode_ChangeEdit);
            this.cmbCode.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.cmbCode_ValidateEdit);
            this.cmbCode.Enter += new System.EventHandler(this.cmbCode_Enter);
            this.cmbCode.Click += new System.EventHandler(this.cmbCode_ClickEvent);
            this.cmbCode.DoubleClick += new System.EventHandler(this.cmbCode_DblClick);
            this.cmbCode.Validating += new System.ComponentModel.CancelEventHandler(this.cmbCode_Validating);
            this.cmbCode.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbCode_KeyDownEvent);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(231, 44);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(89, 16);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "DESCRIPTION";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(37, 18);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "CODE";
            // 
            // vsReceipt
            // 
            this.vsReceipt.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsReceipt.Cols = 6;
            this.vsReceipt.Location = new System.Drawing.Point(30, 104);
            this.vsReceipt.Name = "vsReceipt";
            this.vsReceipt.Rows = 7;
            this.vsReceipt.Size = new System.Drawing.Size(931, 277);
            this.vsReceipt.TabIndex = 1;
            this.vsReceipt.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsReceipt_KeyPressEdit);
            this.vsReceipt.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsReceipt_ChangeEdit);
            this.vsReceipt.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsReceipt_StartEdit);
            this.vsReceipt.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsReceipt_ValidateEdit);
            this.vsReceipt.CurrentCellChanged += new System.EventHandler(this.vsReceipt_RowColChange);
            this.vsReceipt.Enter += new System.EventHandler(this.vsReceipt_Enter);
            this.vsReceipt.Click += new System.EventHandler(this.vsReceipt_ClickEvent);
            this.vsReceipt.KeyDown += new Wisej.Web.KeyEventHandler(this.vsReceipt_KeyDownEvent);
            // 
            // lblAccountTitle
            // 
            this.lblAccountTitle.Location = new System.Drawing.Point(153, 397);
            this.lblAccountTitle.Name = "lblAccountTitle";
            this.lblAccountTitle.Size = new System.Drawing.Size(406, 25);
            this.lblAccountTitle.TabIndex = 3;
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(41, 397);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(100, 25);
            this.Label8.TabIndex = 2;
            this.Label8.Text = "ACCOUNT TITLE";
            // 
            // fraNewType
            // 
            this.fraNewType.AppearanceKey = "groupBoxNoBorders";
            this.fraNewType.Controls.Add(this.cmdCancel);
            this.fraNewType.Controls.Add(this.cmdNewCode);
            this.fraNewType.Controls.Add(this.txtCode);
            this.fraNewType.Controls.Add(this.Label15);
            this.fraNewType.Location = new System.Drawing.Point(0, 417);
            this.fraNewType.Name = "fraNewType";
            this.fraNewType.Size = new System.Drawing.Size(399, 81);
            this.fraNewType.TabIndex = 1;
            this.fraNewType.Text = "New Type Code";
            this.fraNewType.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.ForeColor = System.Drawing.Color.White;
            this.cmdCancel.Location = new System.Drawing.Point(233, 10);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(67, 40);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Visible = false;
            this.cmdCancel.Enter += new System.EventHandler(this.cmdCancel_Enter);
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdNewCode
            // 
            this.cmdNewCode.AppearanceKey = "actionButton";
            this.cmdNewCode.ForeColor = System.Drawing.Color.White;
            this.cmdNewCode.Location = new System.Drawing.Point(154, 10);
            this.cmdNewCode.Name = "cmdNewCode";
            this.cmdNewCode.Size = new System.Drawing.Size(69, 40);
            this.cmdNewCode.TabIndex = 2;
            this.cmdNewCode.Text = "Save";
            this.cmdNewCode.Visible = false;
            // 
            // txtCode
            // 
            this.txtCode.BackColor = System.Drawing.SystemColors.Window;
            this.txtCode.Location = new System.Drawing.Point(89, 10);
            this.txtCode.MaxLength = 3;
            this.txtCode.Name = "txtCode";
            this.txtCode.Size = new System.Drawing.Size(52, 40);
            this.txtCode.TabIndex = 1;
            this.txtCode.Enter += new System.EventHandler(this.txtCode_Enter);
            this.txtCode.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCode_KeyPress);
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(30, 24);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(33, 18);
            this.Label15.TabIndex = 4;
            this.Label15.Text = "CODE";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileBack,
            this.mnuFileForward,
            this.mnuProcessSeperator2,
            this.mnuProcessSave,
            this.mnuProcessSaveExit,
            this.mnuProcessSeperator,
            this.mnuProcessExit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileBack
            // 
            this.mnuFileBack.Index = 0;
            this.mnuFileBack.Name = "mnuFileBack";
            this.mnuFileBack.Shortcut = Wisej.Web.Shortcut.F7;
            this.mnuFileBack.Text = "Previous";
            this.mnuFileBack.Click += new System.EventHandler(this.mnuFileBack_Click);
            // 
            // mnuFileForward
            // 
            this.mnuFileForward.Index = 1;
            this.mnuFileForward.Name = "mnuFileForward";
            this.mnuFileForward.Shortcut = Wisej.Web.Shortcut.F8;
            this.mnuFileForward.Text = "Next";
            this.mnuFileForward.Click += new System.EventHandler(this.mnuFileForward_Click);
            // 
            // mnuProcessSeperator2
            // 
            this.mnuProcessSeperator2.Index = 2;
            this.mnuProcessSeperator2.Name = "mnuProcessSeperator2";
            this.mnuProcessSeperator2.Text = "-";
            // 
            // mnuProcessSave
            // 
            this.mnuProcessSave.Index = 3;
            this.mnuProcessSave.Name = "mnuProcessSave";
            this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuProcessSave.Text = "Save";
            this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // mnuProcessSaveExit
            // 
            this.mnuProcessSaveExit.Index = 4;
            this.mnuProcessSaveExit.Name = "mnuProcessSaveExit";
            this.mnuProcessSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuProcessSaveExit.Text = "Save & Exit";
            this.mnuProcessSaveExit.Click += new System.EventHandler(this.mnuProcessSaveExit_Click);
            // 
            // mnuProcessSeperator
            // 
            this.mnuProcessSeperator.Index = 5;
            this.mnuProcessSeperator.Name = "mnuProcessSeperator";
            this.mnuProcessSeperator.Text = "-";
            // 
            // mnuProcessExit
            // 
            this.mnuProcessExit.Index = 6;
            this.mnuProcessExit.Name = "mnuProcessExit";
            this.mnuProcessExit.Text = "Exit";
            this.mnuProcessExit.Click += new System.EventHandler(this.mnuProcessExit_Click);
            // 
            // cmdFileBack
            // 
            this.cmdFileBack.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileBack.Location = new System.Drawing.Point(908, 29);
            this.cmdFileBack.Name = "cmdFileBack";
            this.cmdFileBack.Size = new System.Drawing.Size(69, 24);
            this.cmdFileBack.TabIndex = 2;
            this.cmdFileBack.Text = "Previous";
            this.cmdFileBack.Click += new System.EventHandler(this.mnuFileBack_Click);
            // 
            // cmdFileForward
            // 
            this.cmdFileForward.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileForward.Location = new System.Drawing.Point(852, 29);
            this.cmdFileForward.Name = "cmdFileForward";
            this.cmdFileForward.Size = new System.Drawing.Size(49, 24);
            this.cmdFileForward.TabIndex = 1;
            this.cmdFileForward.Text = "Next";
            this.cmdFileForward.Click += new System.EventHandler(this.mnuFileForward_Click);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(223, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(90, 48);
            this.cmdProcessSave.Text = "Save";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmICAccountSetup
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(997, 688);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmICAccountSetup";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Account Setup";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmICAccountSetup_Load);
            this.Activated += new System.EventHandler(this.frmICAccountSetup_Activated);
            this.Resize += new System.EventHandler(this.frmICAccountSetup_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmICAccountSetup_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReceiptList)).EndInit();
            this.fraReceiptList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultAccount)).EndInit();
            this.fraDefaultAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOptions)).EndInit();
            this.fraOptions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsReceipt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraNewType)).EndInit();
            this.fraNewType.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNewCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileBack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileForward)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileForward;
		private FCButton cmdFileBack;
		private FCButton cmdProcessSave;
	}
}
