﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptOaklandDisketteNoMatch.
	/// </summary>
	public partial class srptOaklandDisketteNoMatch : FCSectionReport
	{
		public static srptOaklandDisketteNoMatch InstancePtr
		{
			get
			{
				return (srptOaklandDisketteNoMatch)Sys.GetInstance(typeof(srptOaklandDisketteNoMatch));
			}
		}

		protected srptOaklandDisketteNoMatch _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public srptOaklandDisketteNoMatch()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	srptOaklandDisketteNoMatch	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/10/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/15/2004              *
		// ********************************************************
		bool blnFirstRecord;
		int lngCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// MAL@20070924: Rework to test for accounts that already exist in Bad Numbers table
			// Call Reference: 117160
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (lngCounter <= modTSCONSUM.Statics.lngNoMatchCounter - 1)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
			else
			{
				lngCounter += 1;
				if (lngCounter <= modTSCONSUM.Statics.lngNoMatchCounter - 1)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngCounter = 0;
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// MAL@20070924: Added check for existence of serial number in new table
			// Call Reference: 117160
			if (lngCounter > modTSCONSUM.Statics.lngNoMatchCounter - 1)
			{
				LayoutAction = GrapeCity.ActiveReports.LayoutAction.NextRecord;
				// fldSerialNumber = ""
				// fldName = ""
				// fldAddress = ""
				// fldConsumption = ""
				// fldDate = ""
			}
			else
			{
				if (IsInBadTable(modTSCONSUM.Statics.NoAccountFound[lngCounter].SerialNumber))
				{
					LayoutAction = GrapeCity.ActiveReports.LayoutAction.NextRecord;
					// fldSerialNumber = ""
					// fldName = ""
					// fldAddress = ""
					// fldConsumption = ""
					// fldDate = ""
				}
				else
				{
					fldSerialNumber.Text = modTSCONSUM.Statics.NoAccountFound[lngCounter].SerialNumber;
					fldName.Text = modTSCONSUM.Statics.NoAccountFound[lngCounter].OwnerName;
					fldAddress.Text = modTSCONSUM.Statics.NoAccountFound[lngCounter].OwnerAddress;
					fldConsumption.Text = Strings.Format(Conversion.Val(modTSCONSUM.Statics.NoAccountFound[lngCounter].Consumption) / 100, "#,##0");
					fldDate.Text = Strings.Left(modTSCONSUM.Statics.NoAccountFound[lngCounter].ReadingDate, 2) + "/" + Strings.Mid(modTSCONSUM.Statics.NoAccountFound[lngCounter].ReadingDate, 3, 2) + "/" + Strings.Right(modTSCONSUM.Statics.NoAccountFound[lngCounter].ReadingDate, 2);
				}
			}
		}
		// vbPorter upgrade warning: lngSerialNumber As Variant --> As FixedString
		private bool IsInBadTable(string lngSerialNumber)
		{
			bool IsInBadTable = false;
			// MAL@20070924: Added function to check for serial number in table
			bool blnResult;
			clsDRWrapper rsBadNum = new clsDRWrapper();
			rsBadNum.OpenRecordset("SELECT * FROM tblBadNumbers", modExtraModules.strUTDatabase);
			rsBadNum.FindFirstRecord("BadAccountNumber", lngSerialNumber);
			blnResult = !rsBadNum.NoMatch;
			IsInBadTable = blnResult;
			rsBadNum.Dispose();
			return IsInBadTable;
		}

		private void srptOaklandDisketteNoMatch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptOaklandDisketteNoMatch properties;
			//srptOaklandDisketteNoMatch.Caption	= "Daily Audit Detail";
			//srptOaklandDisketteNoMatch.Icon	= "srptDisketteNoMatch.dsx":0000";
			//srptOaklandDisketteNoMatch.Left	= 0;
			//srptOaklandDisketteNoMatch.Top	= 0;
			//srptOaklandDisketteNoMatch.Width	= 11880;
			//srptOaklandDisketteNoMatch.Height	= 8175;
			//srptOaklandDisketteNoMatch.StartUpPosition	= 3;
			//srptOaklandDisketteNoMatch.SectionData	= "srptDisketteNoMatch.dsx":058A;
			//End Unmaped Properties
		}
	}
}
