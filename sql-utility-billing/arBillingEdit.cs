﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arBillingEdit.
	/// </summary>
	public partial class arBillingEdit : BaseSectionReport
	{
		public arBillingEdit()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Billing Edit Report";
		}

		public static arBillingEdit InstancePtr
		{
			get
			{
				return (arBillingEdit)Sys.GetInstance(typeof(arBillingEdit));
			}
		}

		protected arBillingEdit _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsBook.Dispose();
				rsM.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arBillingEdit	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/31/2006              *
		// ********************************************************
		double TotalW;
		// holds the water totals for all books
		double TotalS;
		// holds sewer totals for all books
		double BookWTotal;
		// holds water total for each book
		double BookSTotal;
		// holds sewer total for each book
		double AcctTotal;
		// holds the total for each account
		string strBookLoc = "";
		int ct;
		int Book;
		clsDRWrapper rsBook = new clsDRWrapper();
		clsDRWrapper rsM = new clsDRWrapper();
		int[] BookArray = null;
		bool boolCalculate;
		bool boolShowSummary;
		bool boolShowDetail;
		bool boolShowMapLot;
		bool boolErrors;
		public bool boolShowSubtotals;
		public bool boolFinal;
		int lngOrder;

		public void Init(ref int[] BA, bool boolCalc, bool boolPassShowSubtotals = true, bool boolPassShowSummary = false, bool boolPassShowDetail = true, bool boolPassShowMapLot = false, bool boolFinalBilling = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsOrder = new clsDRWrapper();
				rsOrder.OpenRecordset("SELECT BillingRepSeq FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!rsOrder.EndOfFile())
				{
					lngOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOrder.Get_Fields_Int32("BillingRepSeq"))));
				}
				BookArray = BA;
				boolCalculate = boolCalc;
				boolShowDetail = boolPassShowDetail;
				boolShowSummary = boolPassShowSummary;
				boolShowSubtotals = boolPassShowSubtotals;
				boolShowMapLot = boolPassShowMapLot;
				if (boolCalculate)
				{
					this.Name = "Calculation Report";
				}
				else
				{
					this.Name = "Billing Edit Report";
				}
				lblHeader.Text = this.Name;
				boolFinal = boolFinalBilling;
				if (LoadBook())
				{
					// show the report if there are any books to view
					frmReportViewer.InstancePtr.Init(this);
				}
				else
				{
					this.Close();
				}
				rsOrder.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			if (lngOrder == 0)
			{
				this.Fields.Add("Binder");
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsBook.EndOfFile() || !boolShowDetail;
			if (!eArgs.EOF && lngOrder == 0)
			{
				// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
				this.Fields["Binder"].Value = rsBook.Get_Fields("Book");
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (frmReportViewer.InstancePtr.Enabled && frmReportViewer.InstancePtr.Visible)
			{
				frmReportViewer.InstancePtr.Focus();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// sets the book number on the Page header
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			this.Document.Printer.DefaultPageSettings.Landscape = false;
			FillHeaderLabels();
			// kk trouts-6 02282013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblWTotal.Text = "Stormwater";
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblTemp;
				int lngBook = 0;
				double dblWTotal = 0;
				double dblSTotal = 0;
				//Application.DoEvents();
				if (!rsBook.EndOfFile() && boolShowDetail)
				{
					// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
					lngBook = FCConvert.ToInt32(rsBook.Get_Fields("Book"));
					lblBook.Text = "Book #";
					// & lngBook
					if (boolShowDetail)
					{
						if (frmCalculation.InstancePtr.cmbDetail.Text == "Detail")
						{
							// Show All Detail
							lblWarning.Visible = false;
							lblWarning.Top = 0;
							srptCalculationDetailOb.Report = new srptCalculationDetail();
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							srptCalculationDetailOb.Report.UserData = rsBook.Get_Fields("Bill");
							srptCalculationDetailOb.Visible = true;
							fldSTotal.Visible = false;
							fldWTotal.Visible = false;
							lnWarning.Visible = true;
						}
						else
						{
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							lblWarning.Text = GetWarningMessage_2(rsBook.Get_Fields("Bill"));
							if (Strings.Trim(lblWarning.Text) == "")
							{
								lblWarning.Visible = false;
								lblWarning.Top = 0;
							}
							else
							{
								lblWarning.Visible = true;
								this.Detail.Height = lblWarning.Top + lblWarning.Height;
							}
							srptCalculationDetailOb.Top = 0;
							srptCalculationDetailOb.Visible = false;
							lnWarning.Visible = false;
						}
					}
					else
					{
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						lblWarning.Text = GetWarningMessage_2(rsBook.Get_Fields("Bill"));
						if (Strings.Trim(lblWarning.Text) == "")
						{
							lblWarning.Visible = false;
							lblWarning.Top = 0;
							this.Detail.Height = 0;
						}
						else
						{
							boolErrors = true;
							lblWarning.Top = 0;
							lblWarning.Visible = true;
							this.Detail.Height = lblWarning.Top + lblWarning.Height;
						}
						srptCalculationDetailOb.Top = 0;
						srptCalculationDetailOb.Visible = false;
						lnWarning.Visible = false;
						lnWarning.Top = 0;
					}
					// keep track of totals for the book to put at the last line of this report
					// TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
					dblWTotal = FCConvert.ToDouble(rsBook.Get_Fields_Decimal("WMiscAmount") + rsBook.Get_Fields_Decimal("WAdjustAmount") + rsBook.Get_Fields_Decimal("WDEAdjustAmount") + rsBook.Get_Fields_Decimal("WFlatAmount") + rsBook.Get_Fields_Decimal("WUnitsAmount") + rsBook.Get_Fields_Decimal("WConsumptionAmount") + rsBook.Get_Fields("WTax"));
					// TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
					dblSTotal = FCConvert.ToDouble(rsBook.Get_Fields_Decimal("SMiscAmount") + rsBook.Get_Fields_Decimal("SAdjustAmount") + rsBook.Get_Fields_Decimal("SDEAdjustAmount") + rsBook.Get_Fields_Decimal("SFlatAmount") + rsBook.Get_Fields_Decimal("SUnitsAmount") + rsBook.Get_Fields_Decimal("SConsumptionAmount") + rsBook.Get_Fields("STax"));
					BookWTotal += dblWTotal;
					BookSTotal += dblSTotal;
					TotalW += dblWTotal;
					TotalS += dblSTotal;
					AcctTotal = dblWTotal + dblSTotal;
					fldAcctTotal.Text = Strings.Format(AcctTotal, "#,##0.00");
					//FC:FINAL:MSH - can't implicitly convert from int to string
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					fldAcct.Text = FCConvert.ToString(rsBook.Get_Fields("AccountNumber"));

                    var didBillOwner = rsBook.Get_Fields_Boolean(rsBook.Get_Fields_String("Service").ToLower() == "w" ? "WBillOwner" : "SBillOwner");
                    var billedName = rsBook.Get_Fields_String(didBillOwner ? "OName" : "BName");
                    
                    if (!boolShowMapLot)
                    {
                        fldName.Text = billedName;
                    }
                    else
                    {
                        // TODO Get_Fields: Field [Map] not found!! (maybe it is an alias?)
                        fldName.Text = billedName + "\r\n" + Strings.Trim(FCConvert.ToString(rsBook.Get_Fields("Map")));
                    }
                    //}
                    // TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
                    fldSeq.Text = FCConvert.ToString(rsBook.Get_Fields("Sequence"));
					if (rsBook.Get_Fields_Boolean("WHasOverride") == true || rsBook.Get_Fields_Boolean("SHasOverride") == true)
					{
						fldSeq.Text = "*" + fldSeq.Text;
					}
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLoc.Text = Strings.Trim(rsBook.Get_Fields("StreetNumber") + " " + rsBook.Get_Fields_String("StreetName"));
					fldPrev.Text = FCConvert.ToString(rsBook.Get_Fields_Int32("PrevReading"));
					fldCur.Text = FCConvert.ToString(rsBook.Get_Fields_Int32("CurReading"));
					fldWTotal.Text = Strings.Format(dblWTotal, "#,##0.00");
					fldSTotal.Text = Strings.Format(dblSTotal, "#,##0.00");
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					rsM.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT_2(rsBook.Get_Fields("AccountNumber"))) + " AND MeterNumber = 1", modExtraModules.strUTDatabase);
					if (!rsM.EndOfFile())
					{
						// kk 110812 trout-884  Add check for No Read (-1)
						if (rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading") < 0 || FCConvert.ToInt32(rsM.Get_Fields_Int32("CurrentReading")) == -1)
						{
							fldCons.Text = FCConvert.ToString(rsBook.Get_Fields_Int32("Consumption"));
						}
						else
						{
							fldCons.Text = FCConvert.ToString(rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading"));
							// kk 08292013 trout-984  Indicate change out consumption
							if (FCConvert.ToBoolean(rsM.Get_Fields_Boolean("UseMeterChangeOut")))
							{
								if (FCConvert.ToInt32(rsM.Get_Fields_Int32("ConsumptionOfChangedOutMeter")) != 0)
								{
									fldCons.Text = FCConvert.ToString(rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading") + rsM.Get_Fields_Int32("ConsumptionOfChangedOutMeter"));
								}
							}
						}
					}
					else
					{
						fldCons.Text = FCConvert.ToString(rsBook.Get_Fields_Int32("Consumption"));
					}
					rsBook.MoveNext();
				}
				// this will show the book totals by increasing the height of the detail section of the report
				if (!rsBook.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
					if (FCConvert.ToInt32(rsBook.Get_Fields("Book")) != lngBook && lngOrder == 0)
					{
						ShowBookTotals(ref lngBook);
						// this will fill the group totals
					}
				}
				else
				{
					if (lngOrder == 0)
					{
						ShowBookTotals(ref lngBook);
					}
					ShowEndTotals();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmCalculation.InstancePtr.Visible = true;
			//FC:FINAL:DDU:#993 - don't close frmCalculationSummary
			//frmCalculationSummary.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (!boolShowDetail || lngOrder != 0)
			{
				// hide the total fields
				lnBookTotal.Visible = false;
				lblBookTotal.Visible = false;
				fldBookSTotal.Visible = false;
				fldBookWTotal.Visible = false;
				fldBookTotal.Visible = false;
				this.GroupFooter1.Height = 0;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (!boolShowDetail)
			{
				// hide the total fields
				lnBookHeader.Visible = false;
				lblSeq.Visible = false;
				lblPrev.Visible = false;
				lblCurrent.Visible = false;
				lblCons.Visible = false;
				lblWTotal.Visible = false;
				lblSTotal.Visible = false;
				lblTotal.Visible = false;
				lblAccountNumber.Visible = false;
				lblName.Visible = false;
				lblLocation.Visible = false;
				lblBook.Visible = false;
				fldBook.Visible = false;
				this.GroupHeader1.Height = 0;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private bool LoadBook()
		{
			bool LoadBook = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				string strFields = "";
				string strSeq = "";
				int lngCT;
				if (Information.UBound(BookArray, 1) > 0)
				{
					if (Information.UBound(BookArray, 1) == 1)
					{
						strTemp = " = " + FCConvert.ToString(BookArray[1]);
					}
					else
					{
						for (lngCT = 1; lngCT <= Information.UBound(BookArray, 1); lngCT++)
						{
							strTemp += FCConvert.ToString(BookArray[lngCT]) + ",";
						}
						strTemp = "IN (" + Strings.Left(strTemp, strTemp.Length - 1) + ")";
					}
					switch (lngOrder)
					{
						case 0:
							{
								// Book/Seq
								strSeq = " ORDER BY Bill.Book, MeterTable.Sequence, AccountNumber";
								break;
							}
						case 1:
							{
								// Account Number
								strSeq = " ORDER BY AccountNumber";
								break;
							}
						case 2:
							{
								// Name
								strSeq = " ORDER BY BName";
								// kgk 07-19-2012 Took Name out of strFields - Name
								break;
							}
						case 3:
							{
								// Location
								strSeq = " ORDER BY StreetName, StreetNumber";
								// kgk 07-19-2012  val(StreetNumber)
								break;
							}
						case 4:
							{
								// Map Lot
								strSeq = " ORDER BY Master.MapLot";
								break;
							}
					}
					//end switch
					if (!(Strings.Trim(strTemp) == ""))
					{
						strFields =
                            "Bill.ID AS Bill, Master.MapLot AS Map, BillNumber, Bill.MeterKey, MeterTable.Service AS Service, BillStatus, AccountNumber, Note, BName, OName, Sequence, StreetNumber, StreetName, PrevReading, CurReading, Consumption, Bill.Book, WMiscAmount, WAdjustAmount, WDEAdjustAmount, WFlatAmount, WUnitsAmount, WConsumptionAmount, WTax, WaterOverrideAmount, SMiscAmount, SAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount, STax, SewerOverrideAmount, Bill.Final AS Final, WHasOverride, SHasOverride, Master.Deleted,sbillowner,wbillowner ";
						if (boolFinal)
						{
							rsBook.OpenRecordset("SELECT " + strFields + " FROM (Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON MeterTable.AccountKey = Master.ID WHERE Bill.Book " + strTemp + " AND ISNULL(Bill.Final,0) = 1 AND Combine = 'N' AND BillStatus <> 'B' AND ISNULL(Master.Deleted,0) = 0" + strSeq, modExtraModules.strUTDatabase);
						}
						else
						{
							rsBook.OpenRecordset("SELECT " + strFields + " FROM (Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON MeterTable.AccountKey = Master.ID WHERE Bill.Book " + strTemp + " AND Combine = 'N' AND BillStatus <> 'B' AND ISNULL(Master.Deleted,0) = 0" + strSeq, modExtraModules.strUTDatabase);
						}
					}
					else
					{
						ShowEndTotals();
					}
					LoadBook = true;
				}
				else
				{
					LoadBook = false;
				}
				return LoadBook;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Book", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadBook;
		}

		private void ShowBookTotals(ref int lngBook)
		{
			if (lngOrder == 0)
			{
				fldBookWTotal.Text = Strings.Format(BookWTotal, "#,##0.00");
				fldBookSTotal.Text = Strings.Format(BookSTotal, "#,##0.00");
				fldBookTotal.Text = Strings.Format(BookSTotal + BookWTotal, "#,##0.00");
				lblBookTotal.Text = "Book " + FCConvert.ToString(lngBook) + " Total:";
			}
			else
			{
				fldBookWTotal.Text = "";
				fldBookSTotal.Text = "";
				fldBookTotal.Text = "";
				lblBookTotal.Text = "";
			}
			// reset the book variables
			BookWTotal = 0;
			BookSTotal = 0;
		}

		private void ShowEndTotals()
		{
			fldGrandWTotal.Text = Strings.Format(TotalW, "#,##0.00");
			fldGrandSTotal.Text = Strings.Format(TotalS, "#,##0.00");
			fldGrandTotal.Text = Strings.Format(TotalW + TotalS, "#,##0.00");
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		public string GetWarningMessage_2(int lngBK)
		{
			return GetWarningMessage(ref lngBK);
		}

		public string GetWarningMessage(ref int lngBK)
		{
			string GetWarningMessage = "";
			int lngCT;
			for (lngCT = 0; lngCT <= Information.UBound(modUTBilling.Statics.WarningArray, 1); lngCT++)
			{
				if (modUTBilling.Statics.WarningArray[lngCT].BillKey == lngBK)
				{
					string vbPorterVar = modUTBilling.Statics.WarningArray[lngCT].Type;
					if (vbPorterVar == "W")
					{
						GetWarningMessage = " * * * WARNING * * * ";
					}
					else if (vbPorterVar == "N")
					{
						GetWarningMessage = " * * * NOTE * * * ";
					}
					else if (vbPorterVar == "E")
					{
						GetWarningMessage = " * * * ERROR * * * ";
					}
					GetWarningMessage = GetWarningMessage + " - " + modUTBilling.Statics.WarningArray[lngCT].Message;
					return GetWarningMessage;
				}
			}
			return GetWarningMessage;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				string strBook = "";
				clsDRWrapper rsA = new clsDRWrapper();
				if (boolShowSummary)
				{
					// create a book string
					strBook = "";
					for (intCT = 1; intCT <= Information.UBound(BookArray, 1); intCT++)
					{
						if (BookArray[intCT] != 0)
						{
							if (Information.UBound(BookArray, 1) > 1)
							{
								if (strBook == "")
								{
									strBook = "Book = " + FCConvert.ToString(BookArray[intCT]);
								}
								else
								{
									strBook += " OR Book = " + FCConvert.ToString(BookArray[intCT]);
								}
							}
							else
							{
								strBook += FCConvert.ToString(BookArray[intCT]) + ",";
							}
						}
					}
					if (strBook.Length > 0)
					{
						// remove the last comma
						if (Information.UBound(BookArray, 1) == 1)
						{
							strBook = "Book = " + Strings.Left(strBook, strBook.Length - 1);
						}
						else
						{
							strBook = "(" + strBook + ")";
						}
					}
					frmCalculationSummary.InstancePtr.Init(ref boolCalculate, ref BookArray, true);
					frmCalculationSummary.InstancePtr.Hide();
					//Application.DoEvents();
					if (boolCalculate)
					{
						srptSummary.Report = new arCalculationSummary();
						srptSummary.Visible = true;
					}
					else
					{
						frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
						rsA.OpenRecordset("SELECT * FROM DefaultAnalysisReports", modExtraModules.strUTDatabase);
						srptSummary.Report = new rptAnalysisReportsMaster();
						if (!rsA.EndOfFile())
						{
							//rptAnalysisReportsMaster.InstancePtr.Unload();
							//Application.DoEvents();
							rptAnalysisReportsMaster.InstancePtr.Init("SELECT * FROM Bill WHERE BillingRateKey = 0 AND " + strBook, false, rsA.Get_Fields_Boolean("DollarAmountW") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), rsA.Get_Fields_Boolean("ConsumptionW") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), rsA.Get_Fields_Boolean("BillCountW") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), rsA.Get_Fields_Boolean("MeterReportW") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), rsA.Get_Fields_Boolean("SalesTaxW") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), rsA.Get_Fields_Boolean("DollarAmountS") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), rsA.Get_Fields_Boolean("ConsumptionS") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), rsA.Get_Fields_Boolean("BillCountS") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), rsA.Get_Fields_Boolean("MeterReportS") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), rsA.Get_Fields_Boolean("SalesTaxS") && FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), false, ref strBook, 2, true, true, "", boolFinal);
						}
						else
						{
							rptAnalysisReportsMaster.InstancePtr.Init("SELECT * FROM Bill WHERE BillingRateKey = 0 AND " + strBook, false, FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "S"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), FCConvert.CBool(modUTStatusPayments.Statics.TownService != "W"), false, ref strBook, 2, true, true, "", boolFinal);
						}
						srptSummary.Visible = true;
						srptSummary2.Report = new arCalculationSummary();
						srptSummary2.Visible = true;
					}
				}
				else
				{
					srptSummary.Visible = false;
				}
				if (!boolShowDetail)
				{
					// hide the total fields
					lnTotals.Visible = false;
					// lblReportTotal.Visible = False
					lblReportTotal.Top = 0;
					lblReportTotal.Width = 4200 / 1440f;
					lblReportTotal.Text = "There were no errors in this report.";
					fldGrandSTotal.Visible = false;
					fldGrandWTotal.Visible = false;
					fldGrandTotal.Visible = false;
					srptSummary.Top = lblReportTotal.Height;
				}
				rsA.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Footer", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void arBillingEdit_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arBillingEdit properties;
			//arBillingEdit.Caption	= "Billing Edit Report";
			//arBillingEdit.Icon	= "arBillingEdit.dsx":0000";
			//arBillingEdit.Left	= 0;
			//arBillingEdit.Top	= 0;
			//arBillingEdit.Width	= 15240;
			//arBillingEdit.Height	= 11115;
			//arBillingEdit.WindowState	= 2;
			//arBillingEdit.SectionData	= "arBillingEdit.dsx":058A;
			//End Unmaped Properties
		}
	}
}
