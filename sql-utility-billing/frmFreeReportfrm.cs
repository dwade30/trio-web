﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Extensions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmFreeReport.
	/// </summary>
	public partial class frmFreeReport : BaseForm
	{
		public frmFreeReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.lblPsuedoFooter = new System.Collections.Generic.List<fecherFoundation.FCLabel>();
			this.lblPsuedoFooter.AddControlArrayElement(lblPsuedoFooter_1, 1);
			this.lblPsuedoFooter.AddControlArrayElement(lblPsuedoFooter_2, 2);
			this.lblPsuedoFooter.AddControlArrayElement(lblPsuedoFooter_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;

            //FC:FINAL:BSE:#3987 allow only numeric input in grid columns
            vsVariableSetup.EditingControlShowing -= vsVariableSetup_EditingControlShowing;
            vsVariableSetup.EditingControlShowing += vsVariableSetup_EditingControlShowing;
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmFreeReport InstancePtr
		{
			get
			{
				return (frmFreeReport)Sys.GetInstance(typeof(frmFreeReport));
			}
		}

		protected frmFreeReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/08/2006              *
		// ********************************************************
		public bool boolUseQuestions;
		bool boolLoaded;
		int intAct;
		bool boolDontCheck;
		string strCurrentReportName = "";
		int lngCurrentSummaryRow;
		bool boolResize;
		bool boolUnloadingReport;
		// vbPorter upgrade warning: dblMinimumAmount As double	OnWrite(string)
		public double dblMinimumAmount;
		// this will hold the lowest amount of principal that should be liened
		public string strSigPassword = "";
		public bool boolWater;
		string strWS = "";
		public string strRateKeyList = "";
		public string strBookList = "";
		public bool boolCheckOnlyOldestBill;
		public string strSavedBookList;
		int intMaxRows;

		private void cboSavedReport_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// THIS CODE WILL ALLOW THE USER TO DELETE A CUSTOM REPORT THAT
			// WAS PREVIOUSELY SAVED
			string strTemp = "";
			clsDRWrapper rs = new clsDRWrapper();
			int intCT;
			int intStart;
			int lngRW;
			if (cmbReport.SelectedIndex == 2)
			{
				if (cboSavedReport.SelectedIndex < 0)
					return;
				if (MessageBox.Show("This will delete the custom report " + cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString() + ". Continue?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					clsDRWrapper rsDelete = new clsDRWrapper();
					rsDelete.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modMain.DEFAULTDATABASE);
					if (rsDelete.EndOfFile() != true)
					{
						rsDelete.Edit();
						rsDelete.Set_Fields("ReportName", "ARCHIVE" + rsDelete.Get_Fields_String("ReportName"));
						rsDelete.Update();
						LoadCombo();
						MessageBox.Show("Custom report deleted successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					else
					{
						MessageBox.Show("Custom report could not be found.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
			else if (cmbReport.SelectedIndex == 1)
			{
				// check to make sure that a report was selected
				if (cboSavedReport.SelectedIndex < 0)
					return;
				// show the report
				rs.OpenRecordset("SELECT * FROM SavedFreeReports WHERE ID = " + FCConvert.ToString(cboSavedReport.ItemData(cboSavedReport.SelectedIndex)), modMain.DEFAULTDATABASE);
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					rtbData.Text = FCConvert.ToString(rs.Get_Fields_String("ReportText"));
					strCurrentReportName = cboSavedReport.Items[cboSavedReport.SelectedIndex].ToString();
					// this will save the name
					// load the contraints
					// lngRw = 0
					// For intCT = 0 To MAXFREEVARIABLES
					// If intCT < 30 And lngRw < vsVariableSetup.rows Then
					// If frfFreeReport(intCT).VariableType <> 1 And frfFreeReport(intCT).VariableType <> 3 Then
					// vsVariableSetup.TextMatrix(lngRw, 1) = .Fields("FieldDefault" & intCT)
					// frfFreeReport(intCT).RowNumber = lngRw
					// lngRw = lngRw + 1
					// Else
					// vsVariableSetup.TextMatrix(intCT, 1) = ""
					// End If
					// Else
					// Exit For
					// End If
					// Next
				}
				else
				{
					MessageBox.Show("There was an error while opening this file.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				cmbReport.SelectedIndex = 0;
				// set it back to create
			}
		}

		private void chkRecommitment_CheckedChanged(object sender, System.EventArgs e)
		{
			fraRecommitmentInfo.Enabled = 0 != (chkRecommitment.CheckState);
			if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				vsRecommitment.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 1, 1, modGlobalConstants.Statics.TRIOCOLORBLACK);
			}
			else
			{
				vsRecommitment.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
			}
		}

		private void chkUseSig_CheckedChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				FileInfo fsoSig;
				bool boolPasswordAccept;
				int lngAnswer = 0;
				if (chkUseSig.CheckState == Wisej.Web.CheckState.Checked)
				{
					lngAnswer = frmSignaturePassword.InstancePtr.Init(modSignatureFile.UTILITYSIG);
					TRYAGAIN:
					;
					if (lngAnswer > 0)
					{
						fsoSig = new FileInfo(Path.Combine(FCFileSystem.Statics.UserDataFolder, modSignatureFile.GetSigFileFromID(ref lngAnswer)));
						// is the password correct
						modSignatureFile.Statics.gstrUtilitySigPath = fsoSig.FullName;
						// yes the password is correct
						if (File.Exists(modSignatureFile.Statics.gstrUtilitySigPath))
						{
							// rock on
							modMain.Statics.gboolUseSigFile = true;
							imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrUtilitySigPath);
							imgSig.Visible = true;
							modGlobalFunctions.AddCYAEntry_8("UT", "Use signature file for lien process.", modRhymalReporting.Statics.strFreeReportType);
						}
						else
						{
							modMain.Statics.gboolUseSigFile = false;
							chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
							if (Strings.Trim(modSignatureFile.Statics.gstrUtilitySigPath) == "")
							{
								MessageBox.Show("No path setup.  Please set it up in File Maintenance - Customize.", "Invalid Path", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							else
							{
								MessageBox.Show(modSignatureFile.Statics.gstrUtilitySigPath + " is not a valid path.  Please set it up in File Maintenance - Customize.", "Invalid Path", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							imgSig.Visible = false;
						}
					}
					else
					{
						modMain.Statics.gboolUseSigFile = false;
						imgSig.Visible = false;
						chkUseSig.CheckState = Wisej.Web.CheckState.Unchecked;
						ToolTip1.SetToolTip(chkUseSig, "No path setup.");
					}
				}
				else
				{
					modMain.Statics.gboolUseSigFile = false;
					imgSig.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Password Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckSigPassword(ref string strPW)
		{
			bool CheckSigPassword = false;
			// this will validate the password signature
			if (Strings.UCase(Strings.Trim(modSignatureFile.Statics.gstrTreasurerSigPassword)) == Strings.UCase(Strings.Trim(strPW)))
			{
				CheckSigPassword = true;
			}
			else
			{
				CheckSigPassword = false;
			}
			return CheckSigPassword;
		}

		private void cmdAdd_Click(object sender, System.EventArgs e)
		{
			// THIS ALLOWS THE USER TO SAVE THE REPORT THAT WAS JUST GENERATED
			// THIS ONLY SAVES THE SQL STATEMENT THAT IS GENERATED. THE USER
			// WILL NOT SEE THE LIST BOXES AND WHERE GRID FILLED IN FOR THEM IF
			// THEY DISPLAY A SAVED REPORT SO NAMING EACH REPORT IS ***VERY***
			// IMPORTANT
			string strReturn;
			clsDRWrapper rsSave = new clsDRWrapper();
			int intRow;
			int intCol;
			string strTemp = "";
			int intCT;
			// vbPorter upgrade warning: intAns As short, int --> As DialogResult
			DialogResult intAns;
			TRYAGAIN:
			;
			strReturn = Interaction.InputBox("Enter name for new report", "New Custom Report"/*, null, -1, -1*/);
			strReturn = modGlobalFunctions.RemoveApostrophe(strReturn);
			if (strReturn == string.Empty)
			{
				// DO NOT SAVE REPORT
			}
			else
			{
				// THIS ALLOWS FOR THE BUILDING OF THE SQL STATEMENT BUT DOES
				// NOT SHOW IT
				// SAVE THE REPORT
				SAVETHIS:
				;
				rsSave.OpenRecordset("SELECT * FROM SavedFreeReports WHERE (ReportName = '" + strReturn + "' OR ReportName = 'DEFAULT" + strReturn + "') AND Type = '" + Strings.UCase(modUTStatusList.Statics.strReportType) + "'", modMain.DEFAULTDATABASE);
				if (!rsSave.EndOfFile())
				{
					intAns = MessageBox.Show("A report by that name already exists. Would you like to save it anyway?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					if (intAns == DialogResult.Yes)
					{
						// Yes
						// save the old report as an archive
						rsSave.Edit();
						rsSave.Set_Fields("ReportName", "ARCHIVE" + rsSave.Get_Fields_String("ReportName"));
						rsSave.Set_Fields("LastUpdated", DateTime.Today);
						rsSave.Update();
						goto SAVETHIS;
					}
					else if (intAns == DialogResult.Cancel)
					{
						// Cancel
						return;
					}
					else
					{
						// No
						goto TRYAGAIN;
					}
				}
				else
				{
					rsSave.AddNew();
					rsSave.Set_Fields("ReportName", strReturn);
					rsSave.Set_Fields("ReportText", rtbData.Text);
					rsSave.Set_Fields("LastUpdated", DateTime.Today);
					rsSave.Set_Fields("Type", modUTStatusList.Statics.strReportType);
					intRow = 0;
					// For intCT = 0 To MAXFREEVARIABLES
					// If intCT < 30 And intRow < vsVariableSetup.rows Then
					// If frfFreeReport(intCT).VariableType <> 1 And frfFreeReport(intCT).VariableType <> 3 Then
					// .Fields("FieldDefault" & intCT) = vsVariableSetup.TextMatrix(intRow, 1)
					// intRow = intRow + 1
					// Else
					// 
					// End If
					// Else
					// Exit For
					// End If
					// Next
					if (rsSave.Update())
					{
						MessageBox.Show("Custom Report saved as " + strReturn, "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					LoadCombo();
				}
			}
		}

		private void cmdAddVariable_Click(object sender, System.EventArgs e)
		{
			if (lstVariable.SelectedIndex != -1)
			{
				InsertTagAtCursor_2(FCConvert.ToInt16(lstVariable.ItemData(lstVariable.SelectedIndex)));
				rtbData.Focus();
			}
			else
			{
				MessageBox.Show("Please choose a variable to insert before using the button.", "Select Variable", MessageBoxButtons.OK, MessageBoxIcon.Information);
				lstVariable.Focus();
			}
		}

		private void frmFreeReport_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
				// do nothing
			}
			else
			{
				StartUp();
			}
		}

		private void StartUp()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strDefaultReport = "";
				string strTemp = "";
				int intCT;
				if (modUTStatusPayments.Statics.TownService == "B")
				{
					boolWater = FCConvert.CBool(frmRateRecChoice.InstancePtr.cmbWS.Text == "Water");
					if (boolWater)
					{
						strWS = "W";
					}
					else
					{
						strWS = "S";
					}
				}
				else
				{
					boolWater = FCConvert.CBool(modUTStatusPayments.Statics.TownService == "W");
					if (boolWater)
					{
						strWS = "W";
					}
					else
					{
						strWS = "S";
					}
				}
				SetPositions();
				vsRecommitment.Cell(FCGrid.CellPropertySettings.flexcpForeColor, 0, 0, 1, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUT);
				ResetVariables();
				LoadReport();
				LoadVariableCombo();
				LoadFreeVariablesGRID();
				LoadDefaultInformation();
				ShowFrame(ref fraVariableSetup);
				// load the saved report combo box
				// LoadCombo
				cmbReport.SelectedIndex = 1;
				// MAL@20080702: Set default sort order
				// Tracker Reference: 11834
				cmbSortOrder.SelectedIndex = 1;
				// set the default report
				if (cboSavedReport.Items.Count > 0)
				{
					if ((Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICEW") || (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICES") || (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE"))
					{
						// MAL@20070917: Changed to find both report types
						// set the default year from the last time this was entered
						// GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORT30DN", strTemp
						strTemp = GetDefaultNotice_2(1);
					}
					else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
					{
						// set the default year from the last time this was entered
						// GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORTLIEN", strTemp
						strTemp = GetDefaultNotice_2(2);
					}
					else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY")
					{
						// set the default year from the last time this was entered
						// GetKeyValues HKEY_CURRENT_USER, REGISTRYKEY & "CL\", "LASTFREEREPORTMATURITY", strTemp
						strTemp = GetDefaultNotice_2(3);
						// kk07232014  Removing this Case.  It should be RNFORM, but there's no RNFORM field in the CollectionControl table
						// Case "RMFORM"
					}
					else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "RNFORM")
					{
						// kk04012016 trout-613  Add back in and hide the lien exclusion form
						// strTemp = GetDefaultNotice(10)
						cmdFileExclusion.Visible = false;
					}
					if (Strings.Trim(strTemp) != "")
					{
						for (intCT = 0; intCT <= cboSavedReport.Items.Count - 1; intCT++)
						{
							if (Strings.Trim(cboSavedReport.Items[intCT].ToString()) == Strings.Trim(strTemp))
							{
								cboSavedReport.SelectedIndex = intCT;
								break;
							}
						}
						// if it cannot find the report then set the default to the first one
						if (intCT == cboSavedReport.Items.Count)
						{
							cboSavedReport.SelectedIndex = 0;
						}
					}
					else
					{
						cboSavedReport.SelectedIndex = 0;
					}
				}
				fraEdit.Visible = false;
				SetAct_2(0);
				boolLoaded = true;
				if (rtbData.Visible == true)
				{
					rtbData.Focus();
					// MAL@20070831: Set focus to a control so F7 acts as expected
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error On StartUp", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetAct_2(short intActNum)
		{
			SetAct(ref intActNum);
		}

		private void SetAct(ref short intActNum)
		{
			// this will change all of the settings that are needed to change between actions
			switch (intActNum)
			{
				case 0:
					{
						// beginning screen (shows the variables that the user can change)
						// change the menu option caption
						cmdShowCodes.Visible = false;
						cmdFilePrintPreview.Visible = true;
						// mnuFileChangeAct.Caption = "Save and Continue"
						cmdFileChangeAct.Visible = false;
						// show the correct frames
						ShowFrame(ref fraVariableSetup);
						if ((modUTStatusList.Statics.strReportType == "LIENPROCESS") || (modUTStatusList.Statics.strReportType == "30DAYNOTICE"))
						{
							fraRecommitment.Visible = true;
							cmdFileExclusion.Visible = true;
						}
						else if (modUTStatusList.Statics.strReportType == "RNFORM")
						{
							// move to the next step, variables will already be filled
							SetAct_2(1);
							cmdFileExclusion.Visible = false;
						}
						else
						{
							fraRecommitment.Visible = false;
							// hide the others
							fraEdit.Visible = false;
							fraSummary.Visible = false;
							fraSigFile.Visible = true;
							intAct = 0;
							cmdFileExclusion.Visible = true;
						}
						break;
					}
				case 1:
					{
						// show the report screen (large white box with the save report and variables at the bottom)
						// change the menu option caption
						cmdShowCodes.Visible = true;
						cmdFilePrintPreview.Visible = true;
						cmdFileChangeAct.Text = "Previous";
						cmdFileChangeAct.Visible = true;
						if (modUTStatusList.Statics.strReportType == "RNFORM")
						{
							cmdFileExclusion.Visible = false;
						}
						else
						{
							cmdFileExclusion.Visible = true;
						}
						// show the correct frames
						ShowFrame(ref fraEdit);
						// set the labels on the next frame to look like the report
						SetPsuedoReportLabel();
						// hide the others
						fraVariableSetup.Visible = false;
						fraSummary.Visible = false;
						fraSigFile.Visible = false;
						intAct = 1;
						break;
					}
				case 2:
					{
						// this will set the actions to the summary screen for the user to choose if
						// they want to see the report or go back and fix any accounts
						cmdShowCodes.Visible = false;
						cmdFileChangeAct.Visible = false;
						fraVariableSetup.Visible = false;
						cmdFileExclusion.Visible = false;
						fraEdit.Visible = false;
						fraSigFile.Visible = false;
						ShowFrame(ref fraSummary);
						intAct = 2;
						break;
					}
			}
			//end switch
		}

		private bool ValidateVariables()
		{
			bool ValidateVariables = false;
			// this will check to make sure that all of the variables that are needed to be filled in by
			// the user are, only then are they allowed to print the report
			int intCT;
			ValidateVariables = true;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (modRhymalReporting.Statics.frfFreeReport[intCT].Required && modRhymalReporting.Statics.frfFreeReport[intCT].VariableType == 2)
				{
					// is this a required question?
					if (Strings.Trim(vsVariableSetup.TextMatrix(intCT, 1)) == "")
					{
						ValidateVariables = false;
						MessageBox.Show(vsVariableSetup.TextMatrix(intCT, 1) + " is a required field.", "Question Validation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						vsVariableSetup.Select(intCT, 1);
						vsVariableSetup.Focus();
						break;
					}
				}
			}
			return ValidateVariables;
		}

		private void frmFreeReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F7:
					{
						// previous
						// MAL@20070831: Changed to act like the ESC key
						KeyCode = (Keys)0;
						Form_KeyPress(Keys.Escape);
						// If intAct > 0 Then
						// SetAct intAct - 1
						// End If
						break;
					}
				case Keys.F8:
					{
						// next
						KeyCode = (Keys)0;
						if (intAct == 0)
						{
							SetAct_2(1);
						}
						break;
					}
				case Keys.F10:
					{
						// print preview
						KeyCode = (Keys)0;
						break;
					}
				case Keys.F5:
					{
						KeyCode = (Keys)0;
						if (intAct == 1)
						{
							RefreshHardCodes();
						}
						break;
					}
			}
			//end switch
		}

		private void frmFreeReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmFreeReport properties;
			//frmFreeReport.FillStyle	= 0;
			//frmFreeReport.ScaleWidth	= 9045;
			//frmFreeReport.ScaleHeight	= 7470;
			//frmFreeReport.LinkTopic	= "Form2";
			//frmFreeReport.LockControls	= true;
			//frmFreeReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// CheckReportTable
			intAct = 0;
			modMain.Statics.gboolUseSigFile = false;
			intMaxRows = vsVariableSetup.Rows;
			// this will change the caption on the
			if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES"))
			{
				// MAL@20070917: Changed to take both report types into account '"30DAYNOTICES"
				this.Text = "30 Day Notice";
			}
			else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW"))
			{
				this.Text = "Lien Certificate";
			}
			else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW"))
			{
				this.Text = "Lien Maturity Notice";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "RNFORM")
			{
				this.Text = "Reminder Notice Form";
			}
			this.HeaderText.Text = this.Text;
		}

		private void frmFreeReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		public void Form_KeyPress(Keys KeyAscii)
		{
			frmFreeReport_KeyPress(this, new Wisej.Web.KeyPressEventArgs((char)KeyAscii));
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			if (boolUnloadingReport)
			{
				e.Cancel = true;
			}
			else
			{
				frmRateRecChoice.InstancePtr.Unload();
				if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
				{
					frmReportViewer.InstancePtr.Unload();
					// Unload ar30DayNotice
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
				{
					arLienProcess.InstancePtr.Cancel();
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY")
				{
					arLienMaturity.InstancePtr.Cancel();
				}
				else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "RNFORM")
				{
					// Unload rptReminderForm
				}
				ResetVariables();
				boolLoaded = false;
			}
		}

		private void frmFreeReport_Resize(object sender, System.EventArgs e)
		{
			int intCT;
			int intTotal;
			boolResize = true;
			FormatVariableGrid();
			FormatRecommitmentGrid();
			FormatSummaryGrid();
			if (fraSummary.Visible && fraSummary.Enabled)
			{
				SetupAccountList(vsSummary.Row);
			}
			intTotal = 0;
			for (intCT = 0; intCT <= vsVariableSetup.Rows - 1; intCT++)
			{
				if (Conversion.Val(vsVariableSetup.TextMatrix(intCT, 2)) != 0)
				{
					intTotal += 1;
				}
			}
			SetPositions();
			boolResize = false;
		}

		private void lstVariable_DoubleClick(object sender, System.EventArgs e)
		{
			if (lstVariable.SelectedIndex != -1)
			{
				InsertTagAtCursor_2(FCConvert.ToInt16(lstVariable.ItemData(lstVariable.SelectedIndex)));
				rtbData.Focus();
			}
		}

		private void mnuFileChangeAct_Click(object sender, System.EventArgs e)
		{
			switch (intAct)
			{
				case 0:
					{
						// forward to edit
						break;
					}
				case 1:
					{
						// back to questions
						SetAct_2(0);
						break;
					}
			}
			//end switch
		}

		private void mnuFileExclusion_Click(object sender, System.EventArgs e)
		{
			if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
			{
				frmLienExclusion.InstancePtr.Init(1, ref strRateKeyList, ref boolWater);
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
			{
				frmLienExclusion.InstancePtr.Init(2, ref strRateKeyList, ref boolWater);
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY")
			{
				frmLienExclusion.InstancePtr.Init(3, ref strRateKeyList, ref boolWater);
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void LoadVariableCombo()
		{
			// this will load the combobox of variables
			int intCT;
			lstVariable.Clear();
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				// make sure that no empty variables get in or any questions
				if (modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser != "" && modRhymalReporting.Statics.frfFreeReport[intCT].VariableType != 2)
				{
					switch (modRhymalReporting.Statics.frfFreeReport[intCT].Tag)
					{
					// Case "ADDRESS1", "ADDRESS2", "ADDRESS3"
					// this will weed out the mailing address lines
						default:
							{
								// this will show the title of the tag and the actual tag that will show on the report
								//FC:FINAL:CHN - issue #1119: missing tag values at listbox.
								var nameToShowUser = modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser;
								// modGlobalFunctions.PadStringWithSpaces(modRhymalReporting.frfFreeReport[intCT].NameToShowUser, 30, false);
								var showingTag = modRhymalReporting.Statics.frfFreeReport[intCT].Tag;
								lstVariable.AddItem(nameToShowUser + "\t" + showingTag);
								lstVariable.ItemData(lstVariable.NewIndex, intCT);
								break;
							}
					}
					//end switch
				}
				else
				{
				}
			}
			//FC:FINAL:CHN - issue #1119: missing tag values at listbox.
			if (lstVariable.Columns.Count < 2)
			{
				lstVariable.Columns.Add(new ColumnHeader());
			}
			//FC:FINAL:CHN - issue #1117: Improper sorting of Tag Grid List
			lstVariable.Sort();
		}

		private void LoadReport()
		{
			// This sub will load the variables
			// Each index is considered a variable that is allowed to be selected by the
			// user unless it has VariableType = 2 which is a question.  This is for your use only.
			// 
			// kk 01242013 trout-817   Seriously considered rewriting the frfFreeReport array as a collection of objects
			// to eliminate the need to know which array index goes with which variable
			int intCT;
			if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "30DAYNOTICE")
			{
				intCT = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MailDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Notice Mail Date (MM/DD/YYYY)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAILDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date that this notice will be mailed.";
				intCT = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "BillingYear";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Tax Year";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TAXYEAR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 4;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Billing year that this notice is being created for.";
				intCT = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MapLot";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Map Lot";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAPLOT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				// frfFreeReport(2).ToolTip = "Map and Lot number."
				intCT = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "BookPage";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Book and Page";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "BOOKPAGE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				// frfFreeReport(3).ToolTip = "Book and Page number."
				intCT = 4;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CollectorName";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Current Collector";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the name of the Tax Collector.";
				intCT = 5;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CollectorTitle";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Collector Title";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COLLECTORTITLE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the title of the Tax Collector.";
				intCT = 6;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MuniTitle";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Utility Title";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "UTILITYTITLE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the name of the collecting agency to be shown.";
				intCT = 7;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MuniName";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Municipality";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MUNI";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Municipality's name the way that it should be shown.";
				intCT = 8;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "County";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "County";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COUNTY";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "County that the municipality is located in.";
				intCT = 9;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "State";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "State (ie. Maine)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "STATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "State that the municipality is located in.";
				intCT = 10;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Zip";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Zip Code";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ZIP";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Zip code of the municipality.";
				intCT = 11;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Commitment Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COMMITMENT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date in which the bills were commited.";
				intCT = 12;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Demand";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Demand Fee";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "DEMAND";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Amount of the demand fee applied to the account.";
				intCT = 13;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CertMailFee";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Certified Mail Fee";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAILFEE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Amount of the Certified Mail Fee charged.";
				intCT = 14;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "DescriptionCode";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "DESCRIPTION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is not used.";
				intCT = 15;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MapPreparer";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Map Preparer Name";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "PREPARER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the name of the Map Preparer.";
				intCT = 16;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MapPrepareDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Map Prepared Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "PREPAREDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				// XXXX
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date on which the map was prepared.";
				boolUseQuestions = true;
				// this will fill/show the questions to be asked in the same grid as the variables
				intCT = 17;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "LegalDescription";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Legal Description";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LEGALDESCRIPTION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is a reference to the pertinent statute.";
				intCT = 18;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;Owner at the time of billing, C/O current Owner and Address.|1;Owner and Address at time of billing.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mail To:";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "How would you like it addressed.";
				intCT = 19;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Charge Certified Mail Fee for notice?";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 20;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblPrincipal";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Original Principal";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "PRINCIPAL";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 21;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblInterest";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Interest Due";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "INTEREST";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 22;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblLessPayments";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Less Payments Line";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LESSPAYMENTS";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 23;
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = modGlobalConstants.Statics.gstrCityTown + " of";
				}
				else
				{
					modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "";
				}
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "City or Town Of";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "CITYTOWNOF";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 24;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Name1";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Owner Names (on bill)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "OWNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 25;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address1";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 1";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS1";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 26;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address2";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 2";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS2";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 27;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address3";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 3";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS3";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 28;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "TotalDue";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Total Principal Due";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TOTALDUE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 29;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Location";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Location";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LOCATION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 30;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "NameandAddressBar";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Names and Address Bar (2-3 lines)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "NAMEADDRESS";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 31;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "0.00";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Certified Mail Fee Total";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "CERTTOTAL";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 32;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Would you like to send a copy to each Mortgage Holder?";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 33;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "OldCollector";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Recommitment Collector";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "OLDCOLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 34;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "RecommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Recommitment Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "RECOMMITMENTDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				intCT = 35;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Send Copy To New Owner?, (if different)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COPYTONEWOWNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Send a copy of the notice to the new owner also.";
				// as well as the person that the bill was committed to."
				intCT = 36;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblTax";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Original Tax";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TAX";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENPROCESS")
			{
				intCT = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Filing Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Filing Date (MM/DD/YYYY)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "FILINGDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date that the Lien will be filed.";
				intCT = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "BillingYear";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Tax Year";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TAXYEAR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 4;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Billing year that this lien is being created for.";
				intCT = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MapLot";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Map Lot";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAPLOT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "BookPage";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Book and Page";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "BOOKPAGE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 4;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CollectorName";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Current Collector";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the name of the Collector.";
				intCT = 5;
				// kk08022016 trout-1238  Add option to Lien Process to configure the Collector/Treasurer title
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CollectorTitle";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Collector Title";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TITLE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the title of the Collector.";
				intCT = 6;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MuniTitle";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Utility Title";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "UTILITYTITLE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the name of the collecting agency to be shown.";
				intCT = 7;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MuniName";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Municipality";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MUNI";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Municipality's name the way that it should be shown.";
				intCT = 8;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "County";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "County";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COUNTY";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "County that the municipality is located in.";
				intCT = 9;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "State";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "State (ie. Maine)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "STATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "State that the municipality is located in.";
				intCT = 10;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "JOP";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Signer's Designation";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "JOP";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Signer's title.  (ie. Justice of the Peace)";
				intCT = 11;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Signer";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Signer";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "SIGNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Signer's name.";
				intCT = 12;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CommissionExpiration";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Commission Expiration Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COMMISSIONEXPIRATION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date in which the signer's commission expires.";
				intCT = 13;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Commitment Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COMMITMENT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date in which the bills were commited.";
				intCT = 14;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "FilingFee";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Filing Fee";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "FILINGFEE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Amount of the filing fee applied to the account.";
				intCT = 15;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MailFee";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Certified Mail Fee";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAILFEE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Amount of the Certified Mail Fee charged.";
				intCT = 16;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address Bar";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Address Bar (2-3 Lines)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESSBAR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 17;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MapPreparer";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Map Preparer Name";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "PREPARER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "The name of the map preparer.";
				intCT = 18;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MapPrepareDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Map Prepared Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "PREPAREDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date on which the map was prepared.";
				boolUseQuestions = true;
				// this will fill/show the questions to be asked in the same grid as the variables
				intCT = 19;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "LegalDescription";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Legal Description";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LEGALDESCRIPTION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is a reference to the pertinent statute.";
				intCT = 20;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;Owner at the time of billing, C/O current Owner and Address.|1;Owner and Address at time of billing.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mail To:";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "How would you like it addressed.";
				intCT = 21;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Charge Certified Mail Fee for notice?";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 22;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblPrincipal";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Original Principal";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "PRINCIPAL";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 23;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblInterest";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Interest Due";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "INTEREST";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 24;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblLessPayments";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Less Payments Line";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LESSPAYMENTS";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 25;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CityTownOf";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "City or Town Of";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "CITYTOWNOF";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 26;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Name1";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Owner Names (on bill)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "OWNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 27;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address1";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 1";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS1";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 28;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address2";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 2";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS2";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 29;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address3";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 3";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS3";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 30;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "TotalDue";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Total Due";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TOTALDUE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 31;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Location";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Location";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LOCATION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 32;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CostAndSignature";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Cost and Signature (6 lines)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "SIGNATURELINE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 33;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "0.00";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Certified Mail Fee Total";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "CERTTOTAL";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 34;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Would you like to send a copy to each Mortgage Holder?";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 35;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "OldCollector";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Recommitment Collector";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "OLDCOLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 36;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "RecommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Recommitment Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "RECOMMITMENTDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				intCT = 37;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "DemandFees";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Demand Fees";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "DEMAND";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 38;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Send Copy To New Owner?, (if different)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COPYTONEWOWNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Send a copy of the notice to the new owner also.";
				// as well as the person that the bill was committed to."
				intCT = 39;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblTax";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Original Tax";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TAX";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "LIENMATURITY")
			{
				intCT = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "BillingYear";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Tax Year";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TAXYEAR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 4;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Billing year that this notice is being created for.";
				intCT = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Notice Mail Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Notice Mail Date (MM/DD/YYYY)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAILDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date that this notice will be mailed.";
				intCT = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Lien Filing Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Lien Filing Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LIENFILINGDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date that this lien will be filed.";
				intCT = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "BookPage";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Book and Page";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "BOOKPAGE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 4;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Treasurer";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Current Treasurer";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COLLECTOR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 5;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MuniTitle";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Utility Title";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "UTILITYTITLE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the name of the collecting agency to be shown.";
				intCT = 6;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MuniName";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Municipality";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MUNI";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Municipality's name the way that it should be shown.";
				intCT = 7;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "County";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "County";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COUNTY";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "County that the municipality is located in.";
				intCT = 8;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "State (ie. Maine)";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "State";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "STATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "State that the municipality is located in.";
				intCT = 9;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "JOP";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Signer's Designation";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "JOP";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Signer's title.  (ie. Justice of the Peace)";
				intCT = 10;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Signer";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Signer";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "SIGNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Signer's name.";
				intCT = 11;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "SignerPhone";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Signer's Telephone";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "SIGNERPHONE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Phone number where the Signer can be reached.";
				intCT = 12;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CollectorPhone";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Treasurer's Telephone";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COLLECTORPHONE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Phone number where the Tax Collector can be reached.";
				intCT = 13;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MailFee";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Certified Mail Fee";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAILFEE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Amount of the Certified Mail Fee charged.";
				intCT = 14;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address Bar";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Address Bar (2-3 Lines)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESSBAR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 15;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "ForeclosureFee";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Foreclosure Fee";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "FORECLOSUREFEE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Amount of the lien maturity fee applied to the account.";
				intCT = 16;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Foreclosure Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Foreclosure Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "FORECLOSUREDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date that the property will be forclosed apon.";
				intCT = 17;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Legal Description";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Legal Description";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LEGALDESCRIPTION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "This is the statue that allows the municipality to forclose apon property.";
				boolUseQuestions = true;
				// this will fill/show the questions to be asked in the same grid as the variables
				intCT = 18;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;Owner at the time of billing, C/O current Owner and Address.|1;Owner and Address at time of billing.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mail To:";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "How would you like it addressed.";
				intCT = 19;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Charge Certified Mail Fee for notice?";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 20;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblPrincipal";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Original Principal";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "PRINCIPAL";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 21;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblInterest";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Interest Due";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "INTEREST";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 22;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblLessPayments";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Less Payments Line";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LESSPAYMENTS";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 23;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CityTownOf";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "City or Town Of";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "CITYTOWNOF";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 24;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Name1";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Owner Names (on bill)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "OWNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 25;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address1";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 1";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS1";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 26;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address2";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 2";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS2";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 27;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Address3";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mailing Address Line 3";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ADDRESS3";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 28;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "TotalDue";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Total Due";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TOTALDUE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 29;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Location";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Location";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LOCATION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 30;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "0.00";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Certified Mail Fee Total";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "CERTTOTAL";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 31;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Would you like to send a copy to each Mortgage Holder?";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 32;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "DemandFees";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Demand Fees";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "DEMAND";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 33;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "CommitmentDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Commitment Date";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COMMITMENT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 34;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MapLot";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Map Lot";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAPLOT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				intCT = 35;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;No|1;Yes, without Certified Mail Fee.|2;Yes, charge Certified Mail Fee.";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Send Copy To New Owner?, (if different)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "COPYTONEWOWNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Send a copy of the notice to the new owner also.";
				// as well as the person that the bill was committed to."
				intCT = 36;
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "0;MUNICIPALITY|1;DISTRICT";
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Utility Organization";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Organization Title";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ORGANIZATION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 2;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 37;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblTax";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Tax";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TAX";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				intCT = 38;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "dblTax";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Original Tax";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "TAX";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = true;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				// intCT = 39
				// frfFreeReport(intCT).DatabaseFieldName = "Interest Grace Date"
				// frfFreeReport(intCT).NameToShowUser = "Interest Grace Date (MM/dd/yyyy)"
				// frfFreeReport(intCT).Tag = "GRACEDATE"
				// frfFreeReport(intCT).Required = True
				// frfFreeReport(intCT).VariableType = 0            '0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				// frfFreeReport(intCT).Type = 2
				// frfFreeReport(intCT).ToolTip = "Date that the interest will start accruing after the Maturity Fees are applied.  Do not put anything if interest should just accrue normally."
			}
			else if (Strings.UCase(modRhymalReporting.Statics.strFreeReportType) == "RNFORM")
			{
				intCT = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Account";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Account Number";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "ACCOUNT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 1;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Name";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Owner Name";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "OWNER";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MailDate";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Mail Date (MM/dd/yyyy)";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAILDATE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 2;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date that this notice will be mailed.";
				intCT = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Interest Department";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Interest Department";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "INTDEPT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Date that this notice will be mailed.";
				intCT = 4;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Interest Department Phone";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Interest Department Phone";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "INTDEPTPHONE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 5;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Matured Year Department";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Matured Year Department";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MATDEPT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 1;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 6;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Matured Year Department Phone";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Matured Year Department Phone";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MATDEPTPHONE";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 7;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "High Year";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "High Year";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "HIGHYEAR";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 4;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 8;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Last Year Matured";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Last Year Matured";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LASTYEARMAT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 4;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "";
				intCT = 9;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "Location";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Location";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "LOCATION";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				// 0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				// 0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				modRhymalReporting.Statics.frfFreeReport[intCT].ToolTip = "Location of the property.";
				intCT = 10;
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "MapLot";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "Map Lot";
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "MAPLOT";
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 3;
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
				// intCT = 11
				// frfFreeReport(intCT).DatabaseFieldName = "Account"
				// frfFreeReport(intCT).NameToShowUser = "Account Number"
				// frfFreeReport(intCT).Tag = "ACCOUNT"
				// frfFreeReport(intCT).Required = False
				// frfFreeReport(intCT).VariableType = 3            '0 - Variable (Question Grid), 1 - Dynamic Variable (Recordset), 2 - Static, 3 - Calculated
				// frfFreeReport(intCT).Type = 1                    '0 - Text, 1 - Number, 2 - Date, 3 - Do not ask for default (comes from DB), 4 = Formatted Year
				// frfFreeReport(intCT).ToolTip = ""
			}
			else
			{
				ResetVariables();
			}
		}

		private void LoadFreeVariablesGRID()
		{
			// this will actually put the variables and questions into the grid
			int intCT;
			int lngRows;
			lngRows = 0;
			vsVariableSetup.Rows = 0;
			// format the grid widths
			FormatVariableGrid();
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (Strings.Trim(modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser) != "" && modRhymalReporting.Statics.frfFreeReport[intCT].VariableType != 1 && modRhymalReporting.Statics.frfFreeReport[intCT].VariableType != 3)
				{
					vsVariableSetup.AddItem(Strings.Trim(modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser) + "\t" + "\t" + FCConvert.ToString(intCT));
					// If frfFreeReport(intCT).VariableType <> 1 Then
					modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber = lngRows;
					lngRows += 1;
					// this is for easy counting of the rows
					// End If
				}
			}
			// set the height of the grid
			// MAL@20080807: Set sort order group position
			// Tracker Reference: 14183
			//FC:FINAL:AM:#1056 - set the row height
			vsVariableSetup.RowHeight(-1, 350);
		}

		private bool CheckControlDate_2(short intType)
		{
			return CheckControlDate(ref intType);
		}

		private bool CheckControlDate(ref short intType)
		{
			bool CheckControlDate = false;
			// this will find out if the old information should be used or the new information should be
			// by looking at the dates that the control records were used
			clsDRWrapper rsOld = new clsDRWrapper();
			clsDRWrapper rsNew = new clsDRWrapper();
			switch (intType)
			{
				case 1:
					{
						rsOld.OpenRecordset("SELECT * FROM " + strWS + "Control_30DayNotice");
						rsNew.OpenRecordset("SELECT * FROM " + strWS + "Control_LienProcess");
						if (!rsNew.EndOfFile())
						{
							if (!rsOld.EndOfFile())
							{
								if (rsOld.Get_Fields_DateTime("MailDate") > rsNew.Get_Fields_DateTime("FilingDate"))
								{
									CheckControlDate = true;
								}
								else
								{
									CheckControlDate = false;
								}
							}
							else
							{
								CheckControlDate = false;
							}
						}
						else
						{
							CheckControlDate = true;
						}
						break;
					}
				case 2:
					{
						rsOld.OpenRecordset("SELECT * FROM " + strWS + "Control_LienProcess");
						rsNew.OpenRecordset("SELECT * FROM " + strWS + "Control_LienMaturity");
						if (!rsNew.EndOfFile())
						{
							if (!rsOld.EndOfFile())
							{
								if (rsOld.Get_Fields_DateTime("FilingDate") > rsNew.Get_Fields_DateTime("MailDate"))
								{
									CheckControlDate = true;
								}
								else
								{
									CheckControlDate = false;
								}
							}
							else
							{
								CheckControlDate = false;
							}
						}
						else
						{
							CheckControlDate = true;
						}
						break;
					}
			}
			//end switch
			return CheckControlDate;
		}

		private void LoadDefaultInformation()
		{
			// this will put the default information into the grid if needed
			clsDRWrapper rsDefaults = new clsDRWrapper();
			clsDRWrapper rsOldDefaults = new clsDRWrapper();
			string strSQL = "";
			bool boolUseOldInfo = false;
			rsDefaults.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
			if (!rsDefaults.EndOfFile())
			{
				boolCheckOnlyOldestBill = FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("OnlyCheckOldestBillForLien"));
			}
			if (modRhymalReporting.Statics.strFreeReportType == "30DAYNOTICE")
			{
				strSQL = "SELECT * FROM " + strWS + "Control_30DayNotice";
			}
			else if (modRhymalReporting.Statics.strFreeReportType == "LIENPROCESS")
			{
				boolUseOldInfo = CheckControlDate_2(1);
				if (boolUseOldInfo)
				{
					strSQL = "SELECT * FROM " + strWS + "Control_30DayNotice";
				}
				else
				{
					strSQL = "SELECT * FROM " + strWS + "Control_LienProcess";
				}
			}
			else if (modRhymalReporting.Statics.strFreeReportType == "LIENMATURITY")
			{
				boolUseOldInfo = CheckControlDate_2(2);
				if (boolUseOldInfo)
				{
					strSQL = "SELECT * FROM " + strWS + "Control_LienProcess";
				}
				else
				{
					strSQL = "SELECT * FROM " + strWS + "Control_LienMaturity";
					rsOldDefaults.OpenRecordset("SELECT * FROM " + strWS + "Control_LienMaturity", modExtraModules.strUTDatabase);
				}
			}
			else
			{
				return;
			}
			rsDefaults.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (!rsDefaults.EndOfFile())
			{
				// there is a control record for the previous part of the process
				if ((modUTStatusList.Statics.strReportType == "30DAYNOTICEW") || (modUTStatusList.Statics.strReportType == "30DAYNOTICES"))
				{
					// use the Control_30DayNotice table
					// 0  - Mail Date
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
					// 4  - Collector Name
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorName")));
					// 5 - Collector Title
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorTitle")));
					// 6 - Muni Title
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MuniTitle")));
					// 7  - Municipality
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, modGlobalConstants.Statics.MuniName);
					// 8  - County
					// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
					// 9  - State
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
					// 10  - Zip
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Zip")));
					// 12 - Demand Fee
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("Demand"), "#,##0.00"));
					// 13 - Mail Fee
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					// 14 - Description Code
					// .TextMatrix(frfFreeReport(13).RowNumber, 1) = rsDefaults.Fields("DescriptionCode")
					// 15 - Map Preparer
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPreparer")));
					// 16 - Map Prepared Date
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPrepareDate")));
					// 17  - Legal Description
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")));
					// 18 - Mail To:
					if (rsDefaults.Get_Fields_Double("MailTo") == 0)
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Owner at the time of billing, C/O current Owner and Address.");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Owner and Address at time of billing.");
					}
					// 19 - Pay Certified Mail Fee?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1, "Yes");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1, "No");
					}
					// 32 - Send a copy to each mortgage holder?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1, "No");
					}
					// 35 - Send copy to new owner?
					// "0;No|1;Yes, send it to the owner at the time of billing, C/O the current owner with current address.|2,Yes, owner and address as it was at the time of billing."
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
						// Select Case rsDefaults.Fields("SendCopyToNewOwner")
						// Case 1
						// .TextMatrix(frfFreeReport(35).RowNumber, 1) = "Yes, send it to the owner at the time of billing, C/O the current owner with current address."
						// Case 2
						// .TextMatrix(frfFreeReport(35).RowNumber, 1) = "Yes, owner and address as it was at the time of billing."
						// Case Else
						// .TextMatrix(frfFreeReport(35).RowNumber, 1) = "No"
						// End Select
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
					}
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) != "")
					{
						chkRecommitment.CheckState = Wisej.Web.CheckState.Checked;
						vsRecommitment.TextMatrix(0, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")));
						vsRecommitment.TextMatrix(1, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("RecommitmentDate"), "MM/dd/yyyy"));
					}
					else
					{
						chkRecommitment.CheckState = Wisej.Web.CheckState.Unchecked;
						vsRecommitment.TextMatrix(0, 1, "");
						vsRecommitment.TextMatrix(1, 1, "");
					}
					// MAL@20080813: Add new sort order option
					// Tracker Reference: 14183
					string vbPorterVar = rsDefaults.Get_Fields_String("DefaultSort");
					if (vbPorterVar == "N")
					{
						cmbSortOrder.SelectedIndex = 1;
					}
					else if (vbPorterVar == "A")
					{
						cmbSortOrder.SelectedIndex = 0;
					}
					else
					{
						cmbSortOrder.SelectedIndex = 1;
					}
				}
				else if ((modUTStatusList.Statics.strReportType == "LIENPROCESSW") || (modUTStatusList.Statics.strReportType == "LIENPROCESSS"))
				{
                    // use the Control_30DayNotice table
                    if (boolUseOldInfo)
					{
						// 0  - Mail Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1, "");
						// Format(rsDefaults.Fields("MailDate"), "MM/dd/yyyy")
						// 10  - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, "");
						// rsDefaults.Fields("Designation")
						// 11 - Signer
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, "");
						// rsDefaults.Fields("Signer")
						// 12 - Commission Expiration
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, "");
						// Format(rsDefaults.Fields("CommissionExpirationDate"), "MM/dd/yyyy")
						// 14 - Filing Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, "");
						// rsDefaults.Fields("FilingFee")
					}
					else
					{
						// 0  - Filing Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("FilingDate"), "MM/dd/yyyy"));
						// 10 - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
						// 11 - Signer
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
						// 12 - CommissionExpirationDate
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("CommissionExpirationDate"), "MM/dd/yyyy"));
						// 14 - Filing Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_Double("FilingFee")));
					}
                    // 4  - Collector Name
                    vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorName")));
					// 5 - Collecor Title
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorTitle")));
					// kk08022016 trout-1238  Same in 30DN and Lien control
					// 6 - Muni Title
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MuniTitle")));
					// 7  - Municipality
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, modGlobalConstants.Statics.MuniName);
					// 8  - County
					// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
					// 9  - State
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
					// 15 - Mail Fee
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					// 17 - Map Preparer
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPreparer")));
					// 18 - Map Prepared Date
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPrepareDate")));
					// 19  - Legal Description
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")));
					// 20 - Mail To:
					if (rsDefaults.Get_Fields_Double("MailTo") == 0)
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[20].RowNumber, 1, "Owner at the time of billing, C/O current Owner and Address.");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[20].RowNumber, 1, "Owner and Address at time of billing.");
					}
					// 21 - Pay Certified Mail Fee?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[21].RowNumber, 1, "Yes");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[21].RowNumber, 1, "No");
					}
					// 34 - Send a copy to each mortgage holder?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "No");
					}
					// 38 - Send copy to new owner?
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[38].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[38].RowNumber, 1, "No");
					}
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) != "")
					{
						chkRecommitment.CheckState = Wisej.Web.CheckState.Checked;
						vsRecommitment.TextMatrix(0, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")));
						vsRecommitment.TextMatrix(1, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("RecommitmentDate")));
					}
					else
					{
						chkRecommitment.CheckState = Wisej.Web.CheckState.Unchecked;
						vsRecommitment.TextMatrix(0, 1, "");
						vsRecommitment.TextMatrix(1, 1, "");
					}
					// MAL@20080813: Add new sort order option
					// Tracker Reference: 14183
					string vbPorterVar1 = rsDefaults.Get_Fields_String("DefaultSort");
					if (vbPorterVar1 == "N")
					{
						cmbSortOrder.SelectedIndex = 1;
					}
					else if (vbPorterVar1 == "A")
					{
						cmbSortOrder.SelectedIndex = 0;
					}
					else
					{
						cmbSortOrder.SelectedIndex = 1;
					}
					if (!boolUseOldInfo)
					{
						// this will fill in the rest of the information if there is any from the current year control
						strSQL = "SELECT * FROM " + strWS + "Control_LienProcess";
						rsDefaults.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
						// open the lien control record
						if (!rsDefaults.EndOfFile())
						{
							// 0  - Mail Date
							//FC:FINAL:MSH - if rsDefaults.Get_Fields("FilingDate") will be equal to an empty string will be throwed an exception (same with i.issue #1002)
							if (FCConvert.ToDateTime(rsDefaults.Get_Fields_DateTime("FilingDate") as object).ToOADate() != DateTime.MinValue.ToOADate())
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("FilingDate"), "MM/dd/yyyy"));
							}
							// 10 - Signer's Designation
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
							}
							// 11 - Signer's Name
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
							}
							// 12 - Commission Expiration
							// MAL@20071210: Check for data being a time only to avoid 12/30/1899
							// Tracker Reference: 11493
							// If rsDefaults.Fields("CommissionExpirationDate") <> "" Then
							if (FCConvert.ToString(rsDefaults.Get_Fields_DateTime("CommissionExpirationDate")) != "" && ((DateTime)rsDefaults.Get_Fields_DateTime("CommissionExpirationDate")).ToOADate() > DateAndTime.DateValue("12/30/1899").ToOADate())
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("CommissionExpirationDate"), "MM/dd/yyyy"));
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, "");
							}
							// 14 - Filing Fee
							if (!rsDefaults.IsFieldNull("FilingFee"))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("FilingFee"), "#,##0.00"));
							}
						}
						else
						{
							// leave everything blank
						}
						// MAL@20080813: Add new sort order option
						// Tracker Reference: 14183
						cmbSortOrder.SelectedIndex = 1;
					}
					else
					{
						// leave all of these fields blank
					}
				}
				else if ((modUTStatusList.Statics.strReportType == "LIENMATURITYW") || (modUTStatusList.Statics.strReportType == "LIENMATURITYS"))
				{
					// use Control_LienProcess table
					if (boolUseOldInfo)
					{
						// 1  - Mail Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1, "");
						// Format(rsDefaults.Fields("FilingDate"), "MM/dd/yyyy")
						// 2  - Lien Filing Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("FilingDate"), "MM/dd/yyyy"));
					}
					else
					{
						// 1  - Mail Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
						// 2  - Lien Filing Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("LienFilingDate"), "MM/dd/yyyy"));
					}
					// 4  - Collector
					rsOldDefaults.OpenRecordset("SELECT * FROM " + strWS + "Control_LienProcess");
					if (rsOldDefaults.EndOfFile())
					{
						modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = "";
					}
					else
					{
						// make sure that the collector field gets filled
						modRhymalReporting.Statics.frfFreeReport[4].DatabaseFieldName = FCConvert.ToString(rsOldDefaults.Get_Fields_String("CollectorName"));
					}
					// 5  - Utility Title
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MuniTitle")));
					// 6  - Municipality
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, modGlobalConstants.Statics.MuniName);
					// 7  - County
					// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
					// 8  - State
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
					// 9  - Designation
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
					// 10 - Signer
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
					// 11 - Signer Phone
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, "");
					// rsDefaults.Fields("SignerPhone")
					// 12  - Collector Phone
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, "");
					// rsDefaults.Fields("CollectorPhone")
					// 13 - Mail Fee
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
					if (boolUseOldInfo)
					{
						// 15 - Foreclosure Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, "");
					}
					else
					{
						// 15 - Foreclosure Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("ForeclosureFee"), "#,##0.00"));
					}
					// 16 - Foreclosure Date
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, "");
					// rsDefaults.Fields("ForeclosureDate")
					// 17 - Legal Description
					vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")));
					// 18 - Mail To:
					if (rsDefaults.Get_Fields_Double("MailTo") == 0)
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Owner at the time of billing, C/O current Owner and Address.");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Owner and Address at time of billing.");
					}
					// 19 - Pay Certified Mail Fee?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1, "Yes");
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1, "No");
					}
					// 31 - Send a copy to each mortgage holder?
					if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1, "Yes, charge Certified Mail Fee.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1, "Yes, without Certified Mail Fee.");
						}
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1, "No");
					}
					// 35 - Send copy to new owner?
					if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
					}
					else
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
					}
					if (!boolUseOldInfo)
					{
						// this will fill in the rest of the information if there is any from the current year control
						strSQL = "SELECT * FROM " + strWS + "Control_LienMaturity";
						rsDefaults.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
						// open the lien maturity control record
						if (!rsDefaults.EndOfFile())
						{
							// 1  - Mail Date
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
							// 2  - Filing Date
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("LienFilingDate"), "MM/dd/yyyy"));
							// 11 - Signer's Phone
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("SignerPhone")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SignerPhone")));
							}
							// 12 - Tax Collectors Phone
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorPhone")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorPhone")));
							}
							// 15- Foreclosure Fee
							if (!rsDefaults.IsFieldNull("ForeclosureFee"))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("ForeclosureFee"), "#,##0.00"));
							}
							// 16- Foreclosure Date
							if (!rsDefaults.IsFieldNull("ForeclosureDate"))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("ForeclosureDate")));
							}
							// 17- Legal Description
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")));
							}
							// 36 - Utility Organization
							if (FCConvert.ToString(rsDefaults.Get_Fields_String("Organization")) != "")
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, Strings.Trim(rsDefaults.Get_Fields_String("Organization") + " "));
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1, "MUNICIPALITY");
							}
							// MAL@20080813: Add new sort order option
							// Tracker Reference: 14183
							cmbSortOrder.SelectedIndex = 1;
						}
						else
						{
							// leave everything blank
						}
					}
					else
					{
						// leave everything blank
					}
					// MAL@20080813: Add new sort order option
					// Tracker Reference: 14183
					string vbPorterVar2 = rsDefaults.Get_Fields_String("DefaultSort");
					if (vbPorterVar2 == "N")
					{
						cmbSortOrder.SelectedIndex = 1;
					}
					else if (vbPorterVar2 == "A")
					{
						cmbSortOrder.SelectedIndex = 0;
					}
					else
					{
						cmbSortOrder.SelectedIndex = 1;
					}
				}
			}
			else
			{
				// if there is not a current control record for the last part of the process, then it will check
				// the control record of the previous year and the same part of the process
				if (modRhymalReporting.Statics.strFreeReportType == "30DAYNOTICE")
				{
					// use the 30 day notice table
					// MAL@20080813: Add new sort order option
					// Tracker Reference: 14183
					cmbSortOrder.SelectedIndex = 1;
				}
				else if (modRhymalReporting.Statics.strFreeReportType == "LIENPROCESS")
				{
					strSQL = "SELECT * FROM " + strWS + "Control_LienProcess";
					rsDefaults.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				}
				else if (modRhymalReporting.Statics.strFreeReportType == "LIENMATURITY")
				{
					strSQL = "SELECT * FROM " + strWS + "Control_LienMaturity";
					rsDefaults.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				}
				if (rsDefaults.EndOfFile() != true)
				{
					if (modRhymalReporting.Statics.strFreeReportType == "30DAYNOTICE")
					{
						// do nothing
						// MAL@20080813: Add new sort order option
						// Tracker Reference: 14183
						cmbSortOrder.SelectedIndex = 1;
					}
					else if (modRhymalReporting.Statics.strFreeReportType == "LIENPROCESS")
					{
						// this will use the Control_LienProcess table
						// 4  - Collector Name
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorName")));
						// 5  - Collecor Title
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorTitle")));
						// kk08022016 trout-1238
						// 6 - Muni Title
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MuniTitle")));
						// 7 - Municipality
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, modGlobalConstants.Statics.MuniName);
						// 8 - County
						// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
						// 9 - State
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
						// 10 - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
						// 11 - Signer
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
						// 12 - Commission Expiration Date
						// MAL@20071210
						// If rsDefaults.Fields("CommissionExpirationDate") <> 0 Then
						if (FCConvert.ToDateTime(rsDefaults.Get_Fields_DateTime("CommissionExpirationDate") as object).ToOADate() != 0 && Strings.CompareString(Strings.Format(rsDefaults.Get_Fields_DateTime("CommissionExpirationDate"), "MM/dd/yyyy"), ">", "12/30/1899"))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_DateTime("CommissionExpirationDate"), "MM/dd/yyyy"));
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, "");
						}
						// 14 - FilingFee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("FilingFee"), "#,##0.00"));
						// 15 - Mail Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
                        // 17 - Map Preparer
                        vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPreparer")));
						// 18 - Map Prepared Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MapPrepareDate")));
						// 19 - Legal Description
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")));
						// 20 - Mail To:
						if (rsDefaults.Get_Fields_Double("MailTo") == 0)
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[20].RowNumber, 1, "Owner at the time of billing, C/O current Owner and Address.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[20].RowNumber, 1, "Owner and Address at time of billing.");
						}
						// 21 - Pay Certified Mail Fee?
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[21].RowNumber, 1, "Yes");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[21].RowNumber, 1, "No");
						}
						// 34 - Send a copy to each mortgage holder?
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
						{
							if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "Yes, charge Certified Mail Fee.");
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "Yes, without Certified Mail Fee.");
							}
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1, "No");
						}
						// 38 - Send copy to new owner?
						if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[38].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[38].RowNumber, 1, "No");
						}
						if (FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")) != "")
						{
							chkRecommitment.CheckState = Wisej.Web.CheckState.Checked;
							vsRecommitment.TextMatrix(0, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("OldCollector")));
							vsRecommitment.TextMatrix(1, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("RecommitmentDate")));
						}
						else
						{
							chkRecommitment.CheckState = Wisej.Web.CheckState.Unchecked;
							vsRecommitment.TextMatrix(0, 1, "");
							vsRecommitment.TextMatrix(1, 1, "");
						}
                        // MAL@20080813: Add new sort order option
                        // Tracker Reference: 14183
                        string vbPorterVar3 = rsDefaults.Get_Fields_String("DefaultSort");
						if (vbPorterVar3 == "N")
						{
							cmbSortOrder.SelectedIndex = 1;
						}
						else if (vbPorterVar3 == "A")
						{
							cmbSortOrder.SelectedIndex = 0;
						}
						else
						{
							cmbSortOrder.SelectedIndex = 1;
						}
					}
					else if (modRhymalReporting.Statics.strFreeReportType == "LIENMATURITY")
					{
						// use Control_LienMaturity table
						// 1  - Mail Date
						// 2  - Lien Filing Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("LienFilingDate")));
						// 4  - Collector
						// frfFreeReport(4).DatabaseFieldName = rsDefaults.Fields("CollectorName")
						// 5  - Utility Title
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("MuniTitle")));
						// 6  - Municipality
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1, modGlobalConstants.Statics.MuniName);
						// 7  - County
						// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields("County")));
						// 8  - State
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("State")));
						// 9  - Designation
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Designation")));
						// 10 - Signer
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("Signer")));
						// 11 - Signer Phone
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SignerPhone")));
						// 12 - Collector Phone
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("CollectorPhone")));
						// 13 - Mail Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("CertMailFee"), "#,##0.00"));
						// 15 - Foreclosure Fee
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, Strings.Format(rsDefaults.Get_Fields_Double("ForeclosureFee"), "#,##0.00"));
						// 16 - Foreclosure Date
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_DateTime("ForeclosureDate")));
						// 17 - Legal Description
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("LegalDescription")));
						// 18 - Mail To:
						if (rsDefaults.Get_Fields_Double("MailTo") == 0)
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Owner at the time of billing, C/O current Owner and Address.");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1, "Owner and Address at time of billing.");
						}
						// 19 - Pay Certified Mail Fee?
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("PayCertMailFee")))
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1, "Yes");
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1, "No");
						}
						// 31 - Send a copy to each mortgage holder?
						if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("SendCopyToMortHolder")))
						{
							if (FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("ChargeForMortHolder")))
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1, "Yes, charge Certified Mail Fee.");
							}
							else
							{
								vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1, "Yes, without Certified Mail Fee.");
							}
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1, "No");
						}
						// 35 - Send copy to new owner?
						if (FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")) != "")
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, FCConvert.ToString(rsDefaults.Get_Fields_String("SendCopyToNewOwner")));
						}
						else
						{
							vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1, "No");
						}
						// MAL@20080813: Add new sort order option
						// Tracker Reference: 14183
						string vbPorterVar4 = rsDefaults.Get_Fields_String("DefaultSort");
						if (vbPorterVar4 == "N")
						{
							cmbSortOrder.SelectedIndex = 1;
						}
						else if (vbPorterVar4 == "A")
						{
							cmbSortOrder.SelectedIndex = 0;
						}
						else
						{
							cmbSortOrder.SelectedIndex = 1;
						}
					}
				}
			}
		}

		private void mnuFilePrintCode_Click(object sender, System.EventArgs e)
		{
			// this will print the code from the RTB
			//! Load arTextDump;
			arTextDump.InstancePtr.strText = rtbData.Text;
			frmReportViewer.InstancePtr.Init(arTextDump.InstancePtr);
			// .Show vbModal, MDIParent
		}

		private void mnuFilePrintPreview_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strReturnVariables = "";
				bool boolContinue = false;
				if (intAct > 0)
				{
					if (Strings.Trim(rtbData.Text) != "")
					{
						SetCellValue();
						if (intAct == 1)
						{
							// kk07232014 trout-1109  Force update of UT accounts from RE database so new owners are done correctly
							if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
							{
								modMain.UpdateAllUTAccountsWithREInfo();
							}
							if (Strings.UCase(modUTStatusList.Statics.strReportType) != "RNFORM")
							{
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
								frmWait.InstancePtr.Init("Recalulating Eligibility." + "\r\n" + "This may take a few moments.");
								// If boolInitial Then         'this will check to see if this is an initial run...if so, the LienStatusEligibility field will
								// get set back to the previous level to be rechecked to see if all the accounts are still eligible
								InitialSettings();
								// End If
								FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
								// this will create the report and show the summary
								if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES"))
								{
									if (CheckFormMandatoryVariables_2(1, rtbData.Text, ref strReturnVariables))
									{
										SaveDefaultNotice_8(1, cboSavedReport.Text);
										boolUnloadingReport = true;
										frmReportViewer.InstancePtr.Unload();
										//FC:FINAL:AM:#1167 - don't dispose the report here
										//ar30DayNotice.InstancePtr.Unload();
										boolUnloadingReport = false;
										modUTCalculations.ClearCMFTable_Utility("30DN");
										// MAL@20071016: Call function to clear all existing records of this type
										ar30DayNotice.InstancePtr.Run(false);
										//Application.DoEvents(); // MAL@20070921: Added to allow the form to refresh correctly ; Call Reference: 116833
										// MAL@20070926: Added variable to stop if there are no records
										boolContinue = modGlobalConstants.Statics.blnHasRecords;
										// boolContinue = True
									}
									else
									{
										frmWait.InstancePtr.Unload();
										MessageBox.Show("Mandatory variables were omitted from the form created.  Please make sure that they are included." + "\r\n" + strReturnVariables, "Missing Variables", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
								}
								else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS"))
								{
									if (CheckFormMandatoryVariables_2(2, rtbData.Text, ref strReturnVariables))
									{
										SaveDefaultNotice_8(2, cboSavedReport.Text);
										boolUnloadingReport = true;
										// Unload arLienProcess
										frmReportViewer.InstancePtr.Unload();
										arLienProcess.InstancePtr.Unload();
										boolUnloadingReport = false;
										modUTCalculations.ClearCMFTable_Utility("LIEN");
										// MAL@20071016: Call function to clear all existing records of this type
										arLienProcess.InstancePtr.Run(false);
										//FC:FINAL:DDU:#i1003 - Added variable to stop if there are no records
										boolContinue = modGlobalConstants.Statics.blnHasRecords;
										//boolContinue = true;
									}
									else
									{
										MessageBox.Show("Mandatory variables were omitted from the form created.  Please make sure that they are included." + "\r\n" + strReturnVariables, "Missing Variables", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
								}
								else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS"))
								{
									if (CheckFormMandatoryVariables_2(3, rtbData.Text, ref strReturnVariables))
									{
										SaveDefaultNotice_8(3, cboSavedReport.Text);
										boolUnloadingReport = true;
										frmReportViewer.InstancePtr.Unload();
										arLienMaturity.InstancePtr.Unload();
										boolUnloadingReport = false;
										modUTCalculations.ClearCMFTable_Utility("LMAT");
										// MAL@20071016: Call function to clear all existing records of this type
										arLienMaturity.InstancePtr.Run(false);
										//FC:FINAL:DDU:#i1003 - Added variable to stop if there are no records
										boolContinue = modGlobalConstants.Statics.blnHasRecords;
										//boolContinue = true;
									}
									else
									{
										MessageBox.Show("Mandatory variables were omitted from the form created.  Please make sure that they are included." + "\r\n" + strReturnVariables, "Missing Variables", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									}
								}
								if (boolContinue)
								{
									FormatSummaryGrid_2(false);
									SetAct_2(2);
									SetPositions();
								}
								//FC:FINAL:DDU:#i1003 - close form here, not in reports
								else
								{
									this.Unload();
								}
							}
							else
							{
								rptReminderForm.InstancePtr.Init(frmReminderNotices.InstancePtr.strPassRNFORMSQL, DateAndTime.DateValue(frmReminderNotices.InstancePtr.vsGrid.TextMatrix(3, 1)), rtbData.Text, ref boolWater, frmReminderNotices.InstancePtr.txtReportTitle.Text);
								return;
							}
						}
						else if (intAct == 2)
						{
							FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
							this.Refresh();
							// this will actually show the report
							// and this will save all of the eligibility and status information
							if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW")
							{
								// ar30DayNotice.Visible = True
								frmReportViewer.InstancePtr.Init(ar30DayNotice.InstancePtr);
								ar30DayNotice.InstancePtr.SaveLienStatus();
								// this will set all of the Bill status' to 1
								SaveControlInformation();
								// this saves the control information into the 30DayNotice control table with the defaults used for the variables
								SetAct_2(1);
								SaveRateRecInformation();
							}
							else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW")
							{
								// arLienProcess.Visible = True
								frmReportViewer.InstancePtr.Init(arLienProcess.InstancePtr);
								arLienProcess.InstancePtr.SaveLienStatus();
								// this will set all of the Bill status' to 3
								SaveControlInformation();
								// this saves the control information into the LienProcess control table with the defaults used for the variables
								SetAct_2(1);
							}
							else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW")
							{
								// arLienMaturity.Visible = True
								frmReportViewer.InstancePtr.Init(arLienMaturity.InstancePtr);
								arLienMaturity.InstancePtr.SaveLienStatus();
								// this will set all of the Bill status' to 5
								SaveControlInformation();
								// this saves the control information into the LienMaturity control table with the defaults used for the variables
								SetAct_2(1);
							}
							else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES")
							{
								// ar30DayNotice.Visible = True
								frmReportViewer.InstancePtr.Init(ar30DayNotice.InstancePtr);
								ar30DayNotice.InstancePtr.SaveLienStatus();
								// this will set all of the Bill status' to 1
								SaveControlInformation();
								// this saves the control information into the 30DayNotice control table with the defaults used for the variables
								SetAct_2(1);
								SaveRateRecInformation();
							}
							else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS")
							{
								// arLienProcess.Visible = True
								frmReportViewer.InstancePtr.Init(arLienProcess.InstancePtr);
								arLienProcess.InstancePtr.SaveLienStatus();
								// this will set all of the Bill status' to 3
								SaveControlInformation();
								// this saves the control information into the LienProcess control table with the defaults used for the variables
								SetAct_2(1);
							}
							else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS")
							{
								// arLienMaturity.Visible = True
								frmReportViewer.InstancePtr.Init(arLienMaturity.InstancePtr);
								arLienMaturity.InstancePtr.SaveLienStatus();
								// this will set all of the Bill status' to 5
								SaveControlInformation();
								// this saves the control information into the LienMaturity control table with the defaults used for the variables
								SetAct_2(1);
							}
							else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "RNFORM")
							{
								return;
							}
						}
						else
						{
							// intAct = 0
							SetAct_2(1);
							// move on to the next screen
						}
						// reset the frmwait and the turn the mousepointer back into the default rather than the hourglass
						frmWait.InstancePtr.Unload();
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
					}
					else
					{
						MessageBox.Show("There is no text to be printed.  Either load a saved report or create your own before printing.", "Missing Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				else
				{
					if (ValidateUserVariables())
					{
						if ((modUTStatusList.Statics.strReportType == "LIENMATURITYS") || (modUTStatusList.Statics.strReportType == "30DAYNOTICES") || (modUTStatusList.Statics.strReportType == "LIENMATURITYW") || (modUTStatusList.Statics.strReportType == "30DAYNOTICEW"))
						{
							// if this is a lien maturity or 30 DN
							// get the choice from the user of whether to use the mailing date or the foreclosure date as the interest date
							ASKQUESTIONAGAIN:
							;
							if (modUTStatusList.Statics.strReportType == "LIENMATURITYS" || modUTStatusList.Statics.strReportType == "LIENMATURITYW")
							{
								frmQuestion.InstancePtr.Init(10, "Use the Mailing Date as the Interest Date", "Use the Maturity Date as the Interest Date", "Select One", 0);
							}
							else
							{
								frmQuestion.InstancePtr.Init(10, "Use the Mailing Date as the Interest Date", "Use the 30 Days after as the Interest Date", "Select One", 0);
							}
							// frmQuestion.Init 10, "Use the Mailing Date as the Interest Date.", "Use the Maturity Date as the Interest Date."
							switch (modUTBilling.Statics.gintPassQuestion)
							{
								case -1:
									{
										MessageBox.Show("Please select an answer.", "Invalid Answer", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										goto ASKQUESTIONAGAIN;
										break;
									}
								case 0:
									{
										modUTLien.Statics.gboolUseMailDateForMaturity = true;
										break;
									}
								case 1:
									{
										modUTLien.Statics.gboolUseMailDateForMaturity = false;
										break;
									}
							}
							//end switch
						}
						if (SaveControlInformation())
						{
							SetAct_2(1);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "PrintPreview", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileRefresh_Click(object sender, System.EventArgs e)
		{
			RefreshHardCodes();
		}

		private void optReport_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// edit
						if (cboSavedReport.Items.Count == 0)
						{
							LoadCombo();
						}
						if (strCurrentReportName != "")
						{
							cboSavedReport.Text = strCurrentReportName;
						}
						cboSavedReport.Enabled = false;
						break;
					}
				case 1:
					{
						// show saved
						cboSavedReport.Enabled = true;
						LoadCombo();
						break;
					}
				case 2:
					{
						// delete
						cboSavedReport.Enabled = true;
						LoadCombo_2(1);
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
		}

		private void optReport_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbReport.SelectedIndex;
			optReport_CheckedChanged(index, sender, e);
		}

		private void rtbData_KeyDown(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			string strText = "";
			int lngStart = 0;
			int lngLen = 0;
			boolDontCheck = true;
			switch (KeyCode)
			{
				case Keys.Back:
					{
						KeyCode = 0;
						// backspace - need to make sure that there is not a tag where the user is trying to delete
						// if it is a tag, then delete the whole thing
						NewRTB_BackSpace();
						break;
					}
				case Keys.Delete:
					{
						boolDontCheck = false;
						KeyCode = 0;
						NewRTB_Delete();
						break;
					}
				case Keys.Right:
					{
						// right arrow key
						strText = rtbData.Text;
						lngStart = rtbData.SelStart;
						lngLen = rtbData.SelLength;
						lngStart += 1;
						if (lngLen > 0)
						{
							if (e.Shift)
							{
								if (Strings.Mid(strText, lngStart + 1, 1) == "<")
								{
									KeyCode = 0;
									boolDontCheck = false;
									rtbData.SelLength = 0;
									rtbData.SelStart = lngStart + lngLen - 1;
								}
								else
								{
									rtbData.SelStart = lngStart + lngLen - 1;
								}
							}
							else
							{
								rtbData.SelStart = lngStart + lngLen - 2;
							}
						}
						else
						{
							if (Strings.Mid(strText, lngStart, 1) == "<")
							{
								KeyCode = 0;
								lngLen = Strings.InStr(lngStart, strText, ">", CompareConstants.vbBinaryCompare) - lngStart;
								rtbData.SelLength = lngLen + 1;
							}
						}
						break;
					}
				case Keys.Left:
					{
						// left arrow key
						strText = rtbData.Text;
						lngStart = rtbData.SelStart;
						lngLen = rtbData.SelLength;
						if (lngStart != 0)
						{
							if (lngLen > 0)
							{
								if (e.Shift)
								{
									KeyCode = 0;
									boolDontCheck = false;
									rtbData.SelLength = 0;
									rtbData.SelStart = lngStart;
								}
								else
								{
								}
							}
							else
							{
								if (Strings.Mid(strText, lngStart, 1) == ">")
								{
									KeyCode = 0;
									lngLen += lngStart - Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
									lngStart = Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
									rtbData.SelStart = lngStart - 1;
									rtbData.SelLength = lngLen + 1;
									boolDontCheck = false;
								}
							}
						}
						break;
					}
				case Keys.Up:
					{
						rtbData.SelLength = 0;
						boolDontCheck = false;
						break;
					}
				case Keys.Down:
					{
						rtbData.SelLength = 0;
						boolDontCheck = false;
						break;
					}
				case Keys.Return:
					{
						rtbData.SelText = "<CRLF>";
						break;
					}
			}
			//end switch
			boolDontCheck = false;
		}

		private void rtbData_KeyPress(ref short KeyAscii)
		{
			switch (KeyAscii)
			{
				case 60:
				case 62:
					{
						KeyAscii = 0;
						break;
					}
			}
			//end switch
		}

		private void rtbData_SelChange()
		{
			int lngStart = rtbData.SelStart;
			int lngLen = rtbData.SelLength;
			CheckInsideTag(ref lngStart, ref lngLen);
		}

		private void vsRecommitment_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			string strTemp;
			strTemp = Strings.Trim(vsRecommitment.TextMatrix(vsRecommitment.Row, 1));
			if (strTemp == "__/__/____" || strTemp == "____-_")
			{
				vsRecommitment.TextMatrix(vsRecommitment.Row, 1, string.Empty);
			}
		}

		private void vsRecommitment_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsRecommitment.Row == 1 && vsRecommitment.Col == 1)
			{
				vsRecommitment.EditMask = "##/##/####";
			}
			else
			{
				vsRecommitment.EditMask = "";
			}
		}

        //FC:FINAL:CHN - issue #1166: Allow only numeric on input.
        private void vsVariableSetup_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            if (e.Control != null && e.Control is TextBox)
            {
                var box = e.Control as TextBox;
                if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES"))
                {
                    if (vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[0].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[10].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[12].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[13].RowNumber)
                    {
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
                    }
                }
                else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW"))
                {
                    if (vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[0].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[12].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[14].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[15].RowNumber)
                    {
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
                    }
                }
                else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW"))
                {
                    if (vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[0].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[1].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[11].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[12].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[13].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[15].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[16].RowNumber)
                    {
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
                    }
                }
            }
        }

		private void vsRecommitment_ClickEvent(object sender, System.EventArgs e)
		{
			if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (vsRecommitment.Col == 1)
				{
					vsRecommitment.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsRecommitment.EditCell();
				}
				else
				{
					vsRecommitment.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else
			{
				vsRecommitment.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsRecommitment_RowColChange(object sender, System.EventArgs e)
		{
			if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				if (vsRecommitment.Col == 1)
				{
					vsRecommitment.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsRecommitment.EditCell();
				}
				else
				{
					vsRecommitment.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
			else
			{
				vsRecommitment.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsSummary_ClickEvent(object sender, System.EventArgs e)
		{
			// this will setup the grid below the vsSummary grid with
			// the accounts that make up the number that is in this grid
			// SetupAccountList vsSummary.Row
		}

		private void SetupAccountList(int lngRow)
		{
			clsDRWrapper rsBill = new clsDRWrapper();
			int lngRows;
			string strTemp = "";
			int lngAcct = 0;
			int lngYear = 0;
			// format the grid
			vsReturnInfo.Cols = 3;
			vsReturnInfo.ColWidth(0, FCConvert.ToInt32(vsReturnInfo.WidthOriginal * 0.15));
			vsReturnInfo.ColWidth(1, 0);
			vsReturnInfo.ColWidth(2, FCConvert.ToInt32(vsReturnInfo.WidthOriginal * 0.6));
			vsReturnInfo.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsReturnInfo.ExtendLastCol = true;
			if (lngCurrentSummaryRow != lngRow)
			{
				// only refill the grid if it is a different row
				lngCurrentSummaryRow = lngRow;
				// reset the number of rows
				vsReturnInfo.Rows = 1;
				// fill the top row with titles
				vsReturnInfo.FixedRows = 1;
				vsReturnInfo.TextMatrix(0, 0, "Account");
				vsReturnInfo.TextMatrix(0, 1, "BillKey");
				vsReturnInfo.TextMatrix(0, 2, "Name");
				if (Strings.Trim(vsSummary.TextMatrix(lngRow, 2)) != "")
				{
					// set the default year from the last time this was entered
					modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "LastRateRecChoiceYear", ref strTemp);
					if (Conversion.Val(strTemp) > 0)
					{
						lngYear = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
					}
					else
					{
						// get the year
						if ((modUTStatusList.Statics.strReportType == "30DAYNOTICE") || (modUTStatusList.Statics.strReportType == "LIENPROCESS"))
						{
							lngYear = FCConvert.ToDateTime(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)).Year;
							// frmRateRecChoice.cmbYear.List (frmRateRecChoice.cmbYear.ListIndex)
						}
						else if (modUTStatusList.Statics.strReportType == "LIENMATURITY")
						{
							lngYear = FCConvert.ToDateTime(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)).Year;
						}
					}
					// set the string off accounts
					strTemp = Strings.Trim(vsSummary.TextMatrix(lngRow, 2));
					// open a recordset with all possible bills so that you can get the name info
					rsBill.OpenRecordset("SELECT Account, Name1, BillKey FROM Bill WHERE Bill = " + FCConvert.ToString(lngYear));
					// fill the grid with the accounts
					while (!(Strings.Trim(strTemp).Length == 0))
					{
						if (Strings.InStr(1, strTemp, ",", CompareConstants.vbBinaryCompare) != 0)
						{
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strTemp, Strings.InStr(1, strTemp, ",", CompareConstants.vbBinaryCompare)))));
							// get the next account
							strTemp = Strings.Right(strTemp, strTemp.Length - Strings.InStr(1, strTemp, ",", CompareConstants.vbBinaryCompare));
							// remove the account from the list
						}
						else
						{
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
							// take the last number
							strTemp = "";
							// this will end the loop
						}
						// add the account
						if (lngAcct > 0)
						{
							rsBill.FindFirstRecord("Billkey", lngAcct);
							if (!rsBill.NoMatch)
							{
								// add the row
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								vsReturnInfo.AddItem(rsBill.Get_Fields("Account") + "\t" + rsBill.Get_Fields_Int32("BillKey") + "\t" + rsBill.Get_Fields_String("Name1"));
							}
						}
					}
				}
				vsReturnInfo.Select(0, 1);
			}
			if (vsReturnInfo.Rows == 1)
			{
				vsReturnInfo.Visible = false;
			}
			else
			{
				vsReturnInfo.Visible = true;
			}
		}

		private void vsVariableSetup_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			string strTemp;
			strTemp = Strings.Trim(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 1));
			if (strTemp == "__/__/____" || strTemp == "____-_")
			{
				vsVariableSetup.TextMatrix(vsVariableSetup.Row, 1, string.Empty);
			}
		}

		private void vsVariableSetup_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			bool boolSkip = false;
			// find the telephone rows and format them to be local telephone numbers
			if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW"))
			{
				if (Conversion.Val(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 2)) == 12 || Conversion.Val(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 2)) == 11)
				{
					// DJW@01182013 TROUT-915 Needed to change rows form 10, 11 to 11, 12 because of added row in lein maturity grid
					{
						boolSkip = true;
						// this will make the code skip the next part
						// MAL@20080812: Correct to use standard phone mask
						// Tracker Reference: 14973
						// vsVariableSetup.EditMask = "###-####"
						vsVariableSetup.EditMask = "(###) ###-####";
					}
				}
			}
			if (!boolSkip)
			{
				if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 2))].Type == 2)
				{
					// date
					vsVariableSetup.EditMask = "##/##/####";
					// ElseIf frfFreeReport(Val(vsVariableSetup.TextMatrix(Row, 2))).Type = 4 Then         'year
					// vsVariableSetup.EditMask = "####"
				}
				else
				{
					vsVariableSetup.EditMask = "";
				}
			}
		}

		private void vsVariableSetup_Enter(object sender, System.EventArgs e)
		{
			if (Strings.UCase(modUTStatusList.Statics.strReportType) == "RNFORM")
			{
			}
			else
			{
				vsVariableSetup.Select(0, 1);
			}
		}

		private void vsVariableSetup_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this code will stop keystrokes that I do not want to happen
				Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
				if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES"))
				{
					if (vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[0].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[10].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[12].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[13].RowNumber)
					{
						// kk10062015  trout-1114    ', frfFreeReport(16).RowNumber
						if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == (Keys)8) || (KeyAscii == (Keys)46))
						{
							// allow
						}
						else
						{
							KeyAscii = 0;
						}
					}
				}
				else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW"))
				{
					if (vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[0].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[12].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[14].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[15].RowNumber)
					{
						// kk10062015  trout-1114    ', frfFreeReport(17).RowNumber
						if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == (Keys)8) || (KeyAscii == (Keys)46))
						{
							// allow
						}
						else
						{
							KeyAscii = 0;
						}
					}
				}
				else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW"))
				{
					if (vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[0].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[1].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[11].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[12].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[13].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[15].RowNumber || vsVariableSetup.Row == modRhymalReporting.Statics.frfFreeReport[16].RowNumber)
					{
						if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == (Keys)8) || (KeyAscii == (Keys)45) || (KeyAscii == (Keys)46))
						{
							// allow
						}
						else
						{
							KeyAscii = 0;
						}
					}
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Key Press Edit", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsVariableSetup_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsVariableSetup[e.ColumnIndex, e.RowIndex];
            // this will set the tool tip when the user moves over the row
            if (vsVariableSetup.GetFlexRowIndex(e.RowIndex) >= 0)
			{
				//ReturnToolTip(vsVariableSetup.MouseRow);
				cell.ToolTipText = FCConvert.ToString(vsVariableSetup.GetFlexRowIndex(e.RowIndex));
			}
		}

		private string ReturnToolTip(int lngRow)
		{
			string ReturnToolTip = "";
			// this will return the correct tooltip for that row
			int lngIndex;
			ReturnToolTip = "";
			// this will check each of the array elements and return the tooltip that is to be shown
			for (lngIndex = 0; lngIndex <= modRhymalReporting.MAXFREEVARIABLES; lngIndex++)
			{
				if (lngRow == modRhymalReporting.Statics.frfFreeReport[lngIndex].RowNumber)
				{
					ReturnToolTip = modRhymalReporting.Statics.frfFreeReport[lngIndex].ToolTip;
					break;
				}
			}
			return ReturnToolTip;
		}

		private void vsVariableSetup_RowColChange(object sender, System.EventArgs e)
		{
			int intCT;
			// only allow input into col 1
			if (vsVariableSetup.Col == 1)
			{
				vsVariableSetup.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsVariableSetup.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			// set the combo boxes in the grid
			intCT = FCConvert.ToInt32(Math.Round(Conversion.Val(vsVariableSetup.TextMatrix(vsVariableSetup.Row, 2))));
			if (Strings.Trim(modRhymalReporting.Statics.frfFreeReport[intCT].ComboList) != "")
			{
				// does it have a combo list
				vsVariableSetup.ComboList = modRhymalReporting.Statics.frfFreeReport[intCT].ComboList;
			}
			else
			{
				vsVariableSetup.ComboList = "";
			}
			if (vsVariableSetup.Row == vsVariableSetup.Rows - 1)
			{
				vsVariableSetup.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsVariableSetup.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void ShowFrame(ref FCPanel fraFrame)
		{
			// show the frame in the middle of the form
			fraFrame.Visible = true;
		}

		private void FormatVariableGrid()
		{
			int wid = 0;
			wid = vsVariableSetup.WidthOriginal;
			vsVariableSetup.ColWidth(0, FCConvert.ToInt32(wid * 0.50));
			// Question
			vsVariableSetup.ColWidth(1, FCConvert.ToInt32(wid * 0.48));
			// Answer
			vsVariableSetup.ColWidth(2, FCConvert.ToInt32(wid * 0.0));
			// Required
			vsVariableSetup.ColWidth(3, FCConvert.ToInt32(wid * 0.0));
			// Hidden Field
		}

		private void FormatRecommitmentGrid()
		{
			int wid = 0;
			wid = vsRecommitment.WidthOriginal;
			vsRecommitment.ColWidth(0, FCConvert.ToInt32(wid * 0.55));
			// Question
			vsRecommitment.ColWidth(1, FCConvert.ToInt32(wid * 0.38));
			// Answer
			vsRecommitment.ColWidth(2, FCConvert.ToInt32(wid * 0.0));
			// Required
			vsRecommitment.ColWidth(3, FCConvert.ToInt32(wid * 0.0));
			// Hidden Field
			vsRecommitment.ExtendLastCol = true;
			vsRecommitment.TextMatrix(0, 0, "Previous Tax Collector:");
			vsRecommitment.TextMatrix(1, 0, "Recommitment Date:");
		}

		private bool ValidateVariablesinGRID()
		{
			bool ValidateVariablesinGRID = false;
			// this function will return true if all of the required variables/Questions are filled in
			int intCT;
			bool boolGood;
			vsVariableSetup.Select(0, 0);
			boolGood = true;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(Conversion.Val(vsVariableSetup.TextMatrix(intCT, 2)))].Required)
				{
					if (Strings.Trim(vsVariableSetup.TextMatrix(intCT, 1)) == "")
					{
						boolGood = false;
						break;
					}
				}
			}
			ValidateVariablesinGRID = boolGood;
			return ValidateVariablesinGRID;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short ValidateVariablesInRTB()
		{
			short ValidateVariablesInRTB = 0;
			// this function will check through the string in the Rich Text Box
			// and make sure that all of the required tags are in the string at some point
			// if all the required fields are entered, then a 0 is returned
			// if one is missing, the first one that is found will have it's
			// index in the frffreereport array returned
			int intCT;
			ValidateVariablesInRTB = 0;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(Conversion.Val(vsVariableSetup.TextMatrix(intCT, 2)))].Required)
				{
					if (Strings.Trim(vsVariableSetup.TextMatrix(intCT, 1)) == "")
					{
						ValidateVariablesInRTB = FCConvert.ToInt16(intCT);
						break;
					}
				}
			}
			return ValidateVariablesInRTB;
		}

		private void ResetVariables()
		{
			// this will clear any variables on a restart
			int intCT;
			boolLoaded = false;
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				modRhymalReporting.Statics.frfFreeReport[intCT].ComboList = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].NameToShowUser = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].VariableType = 0;
				modRhymalReporting.Statics.frfFreeReport[intCT].Required = false;
				modRhymalReporting.Statics.frfFreeReport[intCT].Tag = "";
				modRhymalReporting.Statics.frfFreeReport[intCT].Type = 0;
			}
		}

		private void vsVariableSetup_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsVariableSetup.GetFlexRowIndex(e.RowIndex);
			int col = vsVariableSetup.GetFlexColIndex(e.ColumnIndex);
			if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(vsVariableSetup.TextMatrix(row, 2))].Type == 2)
			{
				// date
				if (Strings.Trim(vsVariableSetup.EditText) == "/  /")
				{
					vsVariableSetup.EditMask = string.Empty;
					vsVariableSetup.EditText = string.Empty;
					vsVariableSetup.TextMatrix(row, col, string.Empty);
					vsVariableSetup.Refresh();
					return;
				}
				if (Strings.Trim(vsVariableSetup.EditText).Length == 0)
				{
				}
				else if (Strings.Trim(vsVariableSetup.EditText).Length != 10)
				{
					MessageBox.Show("Invalid date.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
					return;
				}
				if (!modUTStatusList.IsValidDate(vsVariableSetup.EditText))
				{
					e.Cancel = true;
					return;
				}
			}
			else if (modRhymalReporting.Statics.frfFreeReport[FCConvert.ToInt32(vsVariableSetup.TextMatrix(row, 2))].Type == 1)
			{
				// number
				vsVariableSetup.EditText = Strings.Format(vsVariableSetup.EditText, "0.00");
			}
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		private void InsertTagAtCursor_2(short intIndex)
		{
			InsertTagAtCursor(ref intIndex);
		}

		private void InsertTagAtCursor(ref short intIndex)
		{
			// this will insert the tag selected by the combobox into the rtb at the point where the cursor is
			rtbData.SelText = "<" + modRhymalReporting.Statics.frfFreeReport[intIndex].Tag + ">";
		}

		private void CheckInsideTag(ref int lngStart, ref int lngLen)
		{
			// this will check to make sure that neither the beginning or the end of the selection is in the middle of a tag
			int lngEnd = 0;
			int lngGT = 0;
			int lngLT = 0;
			string strText = "";
			string strTemp = "";
			if (!boolDontCheck)
			{
				if (lngStart == 0 && lngLen == 0)
					return;
				lngStart += 1;
				// adjust for the 1 based array the text functions use
				strText = rtbData.Text;
				// check the beginning of the selection
				if (lngStart == 1)
				{
					lngGT = Strings.InStrRev(strText, ">", lngStart, CompareConstants.vbTextCompare/*?*/);
				}
				else
				{
					lngGT = Strings.InStrRev(strText, ">", lngStart - 1, CompareConstants.vbTextCompare/*?*/);
				}
				lngLT = Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/);
				if (lngLT > lngGT || (lngLT != 0 && lngGT == 0))
				{
					// this means that we are in a tag on the front side of the selection
					lngLen += (lngStart - lngLT);
					lngStart = lngLT;
				}
				else
				{
					// we are not in a tag from the front of the selection
				}
				// check the end of the selection
				lngEnd = lngStart + lngLen;
				lngGT = Strings.InStr(lngEnd, strText, ">", CompareConstants.vbBinaryCompare);
				lngLT = Strings.InStr(lngEnd, strText, "<", CompareConstants.vbBinaryCompare);
				if (lngGT < lngLT || (lngGT != 0 && lngLT == 0))
				{
					// this means that the end of the selection is inside of a tag
					lngLen += (lngGT - lngEnd + 1);
				}
				boolDontCheck = true;
				rtbData.SelStart = lngStart - 1;
				// adjust for the 0 based array the text boxes use
				if (lngStart > 0)
				{
					rtbData.SelLength = lngLen;
				}
				boolDontCheck = false;
			}
		}

		private void NewRTB_BackSpace()
		{
			// this will actually take over for the real backspace key
			string strText;
			string strTemp = "";
			int lngStart = 0;
			int lngDelNum = 0;
			strText = rtbData.Text;
			if (rtbData.SelLength > 0)
			{
				// if there is a selection, then just delete it
				rtbData.SelText = "";
			}
			else
			{
				lngStart = rtbData.SelStart;
				if (lngStart > 1)
				{
					boolDontCheck = true;
					// turn off the selction checking
					if (Strings.Mid(strText, lngStart, 1) == ">")
					{
						// I need to select and delete the whole tag
						lngDelNum = lngStart - Strings.InStrRev(strText, "<", lngStart, CompareConstants.vbTextCompare/*?*/) + 1;
						rtbData.SelStart = lngStart - lngDelNum;
						// start at the beginning
						rtbData.SelLength = lngDelNum;
						// the length of the tag to delete
						rtbData.SelText = "";
						// delete the text selected
					}
					else
					{
						//FC:FINAL:CHN - issue #1000: double deleting on clicking "Backspace" button
						// rtbData.SelStart = lngStart - 1;
						// rtbData.SelLength = 1;
						// rtbData.SelText = "";
					}
					boolDontCheck = false;
				}
				else
				{
					// if at the beginning, then don't do anything
					// do nothing
				}
			}
		}

		private void NewRTB_Delete()
		{
			// this will do the delete key's job when inside a tag
			int lngStart = 0;
			int lngLen = 0;
			string strText;
			strText = rtbData.Text;
			boolDontCheck = true;
			if (rtbData.SelLength > 0)
			{
				// if there is a selection, then just delete it
				rtbData.SelText = "";
			}
			else
			{
				// if there is no selection made, then check to see if there is a tag
				lngStart = rtbData.SelStart;
				if (Strings.Mid(strText, lngStart + 1, 1) == "<")
				{
					lngLen = Strings.InStr(lngStart + 1, strText, ">", CompareConstants.vbBinaryCompare) - lngStart;
					if (lngLen > 0)
					{
						rtbData.SelLength = lngLen;
						rtbData.SelText = "";
					}
				}
				else
				{
					// not a tag, just delete one character
					rtbData.SelLength = 1;
					rtbData.SelText = "";
				}
			}
			boolDontCheck = false;
		}

		public void LoadCombo_2(short intType)
		{
			LoadCombo(intType);
		}

		public void LoadCombo(short intType = 0)
		{
			// LOAD THE COMBO WITH ALL PREVIOUSLY SAVED REPORTS
			clsDRWrapper rsReports = new clsDRWrapper();
			string strSQL = "";
			// CLEAR OUT THE CONTROL
			cboSavedReport.Clear();
			// OPEN THE RECORDSET
			switch (intType)
			{
				case 0:
					{
						// generic show all
						strSQL = "SELECT * FROM SavedFreeReports WHERE Type = '" + Strings.UCase(modUTStatusList.Statics.strReportType) + "' AND Left(ReportName,7) <> 'ARCHIVE'";
						break;
					}
				case 1:
					{
						// show all to delete
						strSQL = "SELECT * FROM SavedFreeReports WHERE Type = '" + Strings.UCase(modUTStatusList.Statics.strReportType) + "' AND Left(ReportName,7) <> 'ARCHIVE' AND Left(ReportName,7) <> 'DEFAULT'";
						break;
					}
				case 2:
					{
						break;
					}
			}
			//end switch
			rsReports.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			while (!rsReports.EndOfFile())
			{
				// ADD THE ITEM TO THE COMBO
				if (Strings.Left(FCConvert.ToString(rsReports.Get_Fields_String("ReportName")), 7) != "ARCHIVE" && Strings.Left(FCConvert.ToString(rsReports.Get_Fields_String("ReportName")), 7) != "DEFAULT")
				{
					cboSavedReport.AddItem(rsReports.Get_Fields_String("ReportName"));
				}
				else
				{
					cboSavedReport.AddItem(Strings.Right(FCConvert.ToString(rsReports.Get_Fields_String("ReportName")), FCConvert.ToString(rsReports.Get_Fields_String("ReportName")).Length - 7));
				}
				// ADD THE AUTONUMBER AS THE ID TO EACH ITEM
				cboSavedReport.ItemData(cboSavedReport.NewIndex, FCConvert.ToInt32(rsReports.Get_Fields_Int32("ID")));
				// GET THE NEXT RECORD
				rsReports.MoveNext();
			}
		}

		private void FormatSummaryGrid_2(bool boolClear)
		{
			FormatSummaryGrid(boolClear);
		}

		private void FormatSummaryGrid(bool boolClear = false)
		{
			int wid = 0;
			if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES"))
			{
				vsSummary.Cols = 3;
				wid = vsSummary.WidthOriginal;
				vsSummary.ColWidth(0, FCConvert.ToInt32(wid * 0.75));
				vsSummary.ColWidth(1, FCConvert.ToInt32(wid * 0.2));
				vsSummary.ColWidth(2, 0);
				vsSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				if (!boolResize)
				{
					if (boolClear)
					{
						vsSummary.Rows = 1;
					}
					vsSummary.Rows = 5;
					vsSummary.RowHeight(0, 0);
					vsSummary.TextMatrix(1, 0, "Zero balance accounts:");
					// .TextMatrix(1, 1) = ar30DayNotice.lngBalanceZero
					vsSummary.TextMatrix(2, 0, "Negative balance accounts:");
					// .TextMatrix(2, 1) = ar30DayNotice.lngBalanceNeg
					vsSummary.TextMatrix(3, 0, "Positive balance accounts:");
					// .TextMatrix(3, 1) = ar30DayNotice.lngBalancePos
					// this will look like a line between the values and the total
					vsSummary.Select(4, 1);
					vsSummary.CellBorder(Color.Black, 0, 1, 0, 0, 0, 2);
					vsSummary.Select(0, 0);
					vsSummary.TextMatrix(4, 0, "Total accounts:");
					// .TextMatrix(4, 1) = ar30DayNotice.lngBalanceZero + ar30DayNotice.lngBalancePos + ar30DayNotice.lngBalanceNeg
				}
				// lblStatusInstructions.Caption = "This is the account breakdown.  To print the 30 Day Notices press F10.  This will also update the status of the accounts found."
				// lblStatusInstructions.Caption = "Account Status:"
			}
			else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS"))
			{
				wid = vsSummary.WidthOriginal;
				vsSummary.Cols = 3;
				vsSummary.ColWidth(0, FCConvert.ToInt32(wid * 0.75));
				vsSummary.ColWidth(1, FCConvert.ToInt32(wid * 0.2));
				vsSummary.ColWidth(2, 0);
				vsSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				if (!boolResize)
				{
					// clear the grid
					if (boolClear)
					{
						vsSummary.Rows = 1;
					}
					vsSummary.Rows = 5;
					vsSummary.RowHeight(0, 0);
					vsSummary.TextMatrix(1, 0, "Zero balance accounts:");
					// .TextMatrix(1, 1) = ar30DayNotice.lngBalanceZero
					vsSummary.TextMatrix(2, 0, "Negative balance accounts:");
					// .TextMatrix(2, 1) = ar30DayNotice.lngBalanceNeg
					vsSummary.TextMatrix(3, 0, "Positive balance accounts:");
					// .TextMatrix(3, 1) = ar30DayNotice.lngBalancePos
					// this will look like a line between the values and the total
					vsSummary.Select(4, 1);
					vsSummary.CellBorder(Color.Black, 0, 1, 0, 0, 0, 2);
					vsSummary.Select(0, 0);
					vsSummary.TextMatrix(4, 0, "Total accounts:");
				}
				// lblStatusInstructions.Caption = "This is the account breakdown.  To print the Lien Notices press F12.  This will also update the status of the accounts found."
			}
			else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS"))
			{
				wid = vsSummary.WidthOriginal;
				vsSummary.Cols = 3;
				vsSummary.ColWidth(0, FCConvert.ToInt32(wid * 0.75));
				vsSummary.ColWidth(1, FCConvert.ToInt32(wid * 0.2));
				vsSummary.ColWidth(2, 0);
				vsSummary.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignRightCenter);
				if (!boolResize)
				{
					if (boolClear)
					{
						vsSummary.Rows = 1;
					}
					vsSummary.Rows = 5;
					vsSummary.RowHeight(0, 0);
					vsSummary.TextMatrix(1, 0, "Zero balance accounts:");
					// .TextMatrix(1, 1) = ar30DayNotice.lngBalanceZero
					vsSummary.TextMatrix(2, 0, "Negative balance accounts:");
					// .TextMatrix(2, 1) = ar30DayNotice.lngBalanceNeg
					vsSummary.TextMatrix(3, 0, "Positive balance accounts:");
					// .TextMatrix(3, 1) = ar30DayNotice.lngBalancePos
					// this will look like a line between the values and the total
					vsSummary.Select(4, 1);
					vsSummary.CellBorder(Color.Black, 0, 1, 0, 0, 0, 2);
					vsSummary.Select(0, 0);
					vsSummary.TextMatrix(4, 0, "Total accounts:");
					// .TextMatrix(4, 1) = ar30DayNotice.lngBalanceZero + ar30DayNotice.lngBalancePos + ar30DayNotice.lngBalanceNeg
				}
				// lblStatusInstructions.Caption = "This is the account breakdown.  To print the Lien Maturity Notices press F12.  This will also update the status of the accounts found."
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "RNFORM")
			{
				vsSummary.Visible = false;
			}
		}

		private void RefreshHardCodes()
		{
			// this will check the whole document for missing CRLF or the <CRLF> tag and replace any that are missing
			string strText;
			int lngNextCRLF;
			int lngLast = 1;
			strText = rtbData.Text;
			// find the hard codes and make sure that there is a tag before it
			//FC:FINAL:DDU:#i887 - fix text without \r
			lngNextCRLF = Strings.InStr(lngLast, strText, "\n");
			while (!(lngNextCRLF == 0))
			{
				if (lngNextCRLF > 6)
				{
					if (Strings.Mid(strText, lngNextCRLF - 6, 6) != "<CRLF>")
					{
						strText = Strings.Left(strText, lngNextCRLF - 1) + "<CRLF>" + Strings.Right(strText, strText.Length - (lngNextCRLF - 1));
					}
				}
				else
				{
					// it is too close to the beginning to have a tag before it
					// so put one in
					strText = Strings.Left(strText, lngNextCRLF - 1) + "<CRLF>" + Strings.Right(strText, strText.Length - (lngNextCRLF - 1));
				}
				lngLast = lngNextCRLF + 1;
				lngNextCRLF = Strings.InStr(lngLast, strText, "\n");
			}
			// find all of the tags and make sure that there is a hard code after it
			lngLast = 1;
			lngNextCRLF = Strings.InStr(lngLast, strText, "<CRLF>");
			while (!(lngNextCRLF == 0))
			{
				if (strText.Length >= lngNextCRLF + 6)
				{
					if (Strings.Mid(strText, lngNextCRLF + 6, 1) != "\n")
					{
						strText = Strings.Left(strText, lngNextCRLF + 5) + "\n" + Strings.Right(strText, (strText.Length - (lngNextCRLF + 5)));
					}
				}
				else
				{
					// is this the last thing...if so, then add a hard return at the end
					strText += "\n";
				}
				lngLast = lngNextCRLF + 1;
				lngNextCRLF = Strings.InStr(lngLast, strText, "<CRLF>");
			}
			rtbData.Text = strText;
		}

		private void SetPsuedoReportLabel()
		{
			// create the header
			// lblLeftHeader.Caption = MuniName & vbCrLf & Format(Time, "hh:mm tt")
			// lblRightHeader.Caption = Format(Date, "MM/dd/yyyy")
			if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES"))
			{
				// MAL@20070917: Corrected check for report type (was 30DAYNOTICE)
				if (chkRecommitment.CheckState == Wisej.Web.CheckState.Unchecked)
				{
					// lblPseudoHeader.Caption = "State of Maine" & vbCrLf & "Tax Collector's Notice, Lien Claim and Demand" '& vbCrLf & "30 Day Notice"
					lblPseudoHeader.Text = "State of Maine" + "\r\n" + "Notice and Demand for Payment";
					// & vbCrLf & "30 Day Notice"
				}
				else
				{
					lblPseudoHeader.Text = "30 Day Notice" + "\r\n" + "Recommitment";
				}
				// create the footer
				lblPsuedoFooter[0].Text = "Principal" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Interest" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Cert Mail Fee" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Total";
				lblPsuedoFooter[1].Text = "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00";
				lblPsuedoFooter[2].Text = "___________________________" + "\r\n";
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1) + "\r\n";
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + "Collector Of Taxes" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
				}
				else
				{
					lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + " " + modGlobalConstants.Statics.MuniName;
				}
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESS")
			{
				if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
				{
					// lblPseudoHeader.Caption = "State of Maine" & vbCrLf & "Tax Lien Certificate"
					lblPseudoHeader.Text = "State of Maine" + "\r\n" + "Utility Lien Certificate";
				}
				else
				{
					lblPseudoHeader.Text = "State of Maine" + "\r\n" + "Tax Lien Certificate";
				}
				// there is nothing automatically added at the bottom of report
				lblPsuedoFooter[0].Text = "";
				lblPsuedoFooter[1].Text = "";
				lblPsuedoFooter[2].Text = "";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITY")
			{
				lblPseudoHeader.Text = "State of Maine" + "\r\n" + "Notice of Impending Automatic Foreclosure";
				// lblPsuedoFooter(0).Caption = ""
				// lblPsuedoFooter(1).Caption = ""
				// lblPsuedoFooter(2).Caption = ""
				// create the footer
				lblPsuedoFooter[0].Text = "Lien" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Interest" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Lien Cost" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Certified Mail Fee" + "\r\n";
				lblPsuedoFooter[0].Text = lblPsuedoFooter[0].Text + "Total";
				lblPsuedoFooter[1].Text = "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00" + "\r\n" + "0.00";
				lblPsuedoFooter[2].Text = "___________________________" + "\r\n";
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + "Tax Collector Name" + "\r\n";
				// vsVariableSetup.TextMatrix(frfFreeReport(4).RowNumber, 1) 'this is the collector name
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + "Collector Of Taxes" + "\r\n";
				if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
				{
					lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName + "\r\n";
				}
				else
				{
					lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + " " + modGlobalConstants.Statics.MuniName + "\r\n";
				}
				lblPsuedoFooter[2].Text = lblPsuedoFooter[2].Text + "Amount due is as of " + vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1) + ".";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "RNFORM")
			{
				lblPseudoHeader.Alignment = 0;
				lblPseudoHeader.Text = "Account: 0" + "\r\n" + "Owner Name" + "\r\n" + "Address" + "\r\n" + "\r\n";
				lblPseudoHeader.Text = lblPseudoHeader.Text + "    Bill        Principal     Tax     Interest      Cost" + "\r\n" + 
                                                              "     123        3020.00    0.00       45.30      7.42";
				lblPsuedoFooter[0].Text = "";
				lblPsuedoFooter[1].Text = "";
				lblPsuedoFooter[2].Text = "";
			}
			//FC:FINAL:DDU:#i922 - Align controls
			if (lblPsuedoFooter[0].Text == "" || lblPsuedoFooter[1].Text == "" || lblPsuedoFooter[2].Text == "")
			{
				//FC:FINAL:DDU:#1148 - aligned top controls
				if (lblPseudoHeader.Text.Trim() == "")
				{
					rtbData.Top = 30;
					lblPseudoHeader.Visible = false;
				}
				fraSave.Top = rtbData.Bottom + 20;
				lstVariable.Top = fraSave.Top;
				//FC:FINAL:MSH - issue #1116: increase height of the listbox to show more items
				lstVariable.Height = fraSave.Height;
				//rtbData.Width = fraSave.Right - 30;
				lblPseudoHeader.Width = rtbData.Width;
			}
			//FC:FINAL:MSH - i.issue #1005: set alignments for controls if pseudofooter has values
			else
			{
				int maxBottom = int.MinValue;
				for (int i = 0; i < lblPsuedoFooter.Count; i++)
				{
					if (lblPsuedoFooter[i].Bottom > maxBottom)
					{
						maxBottom = lblPsuedoFooter[i].Bottom;
					}
				}
				maxBottom += 10;
				fraSave.Top = maxBottom;
				lstVariable.Top = maxBottom;
				//FC:FINAL:MSH - issue #1116: increase height of the listbox to show more items
				lstVariable.Height = fraSave.Height;
				rtbData.Width = fraSave.Right - 30;
				lblPseudoHeader.Width = rtbData.Width;
			}
			//FC:FINAL:DDU:#1062 - show text in both columns
			if (lstVariable.Columns.Count == 2)
			{
				lstVariable.Columns[0].Width = lstVariable.Width / 2 + 30;
				lstVariable.Columns[1].Width = lstVariable.Width / 2 - 50;
			}
			lblPseudoHeader.BackColor = Color.White;
			//FC:FINAL:MSH - i.issue #1005: change height of fraEdit for correct displaying lstVariable if it is located lower than fraEdit height
			fraEdit.Height = lstVariable.Bottom + 10;
			if (fraEdit.Height > ClientArea.Height)
			{
				//FC:FINAL:MSH - i.issue #1005: change height of clientArea if fraEdit is bigger than the last one for displaying scrollbars
				ClientArea.Height = fraEdit.Height;
			}
			fraEdit.ScrollBars = ScrollBars.Both;
		}

		private bool SaveControlInformation()
		{
			bool SaveControlInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save a control record into the database
				clsDRWrapper rsControl = new clsDRWrapper();
				clsDRWrapper rsRate = new clsDRWrapper();
				SaveControlInformation = true;
				vsVariableSetup.Select(0, 0);
				if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES"))
				{
					// ******************
					rsControl.OpenRecordset("SELECT * FROM " + strWS + "Control_30DayNotice");
					if (rsControl.EndOfFile())
					{
						rsControl.AddNew();
					}
					else
					{
						rsControl.Edit();
					}
					rsControl.Set_Fields("DateCreated", DateTime.Today);
					rsControl.Set_Fields("User", modGlobalConstants.Statics.gstrUserID);
					// .Fields("BillingYear") = Val(FormatYear(frmRateRecChoice.cmbYear.Text))
					rsControl.Set_Fields("MinimumAmount", FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text));
					dblMinimumAmount = FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text);
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)))
						{
							rsControl.Set_Fields("MailDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)));
						}
						else
						{
							MessageBox.Show("Enter a valid Mail Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							SaveControlInformation = false;
							return SaveControlInformation;
						}
					}
					else
					{
						MessageBox.Show("Enter a valid Mail Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("CollectorName", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Tax Collector's name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					// **************
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("CollectorTitle", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Collector's Title.", "Invalid Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MuniTitle", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Utility Title.", "Invalid Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("Muni", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Municipality Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("County", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the County Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("State", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the State.", "Invalid State", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("Zip", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Zip Code.", "Invalid Zip", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)) == 0)
					{
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1, "0.00");
						rsControl.Set_Fields("Demand", 0);
					}
					else
					{
						rsControl.Set_Fields("Demand", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)));
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1)) == 0)
					{
						rsControl.Set_Fields("CertMailFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1, "0.00");
					}
					else
					{
						rsControl.Set_Fields("CertMailFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1)));
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MapPreparer", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Map Preparer's Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MapPrepareDate", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Map Prepared Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("LegalDescription", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Legal Description", "Invalid Reference", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1), 8) == "Owner at")
					{
						rsControl.Set_Fields("MaiLTo", 0);
					}
					else
					{
						rsControl.Set_Fields("MailTo", 1);
					}
					rsControl.Set_Fields("PayCertMailFee", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1), 2) == "Ye"));
					rsControl.Set_Fields("SendCopyToMortHolder", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						rsControl.Set_Fields("ChargeForMortHolder", FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1), 9) == "Yes, char"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForMortHolder", false);
					}
					rsControl.Set_Fields("SendCopyToNewOwner", Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1)));
					if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsControl.Set_Fields("OldCollector", vsRecommitment.TextMatrix(0, 1));
						if (Information.IsDate(vsRecommitment.TextMatrix(1, 1)))
						{
							rsControl.Set_Fields("RecommitmentDate", vsRecommitment.TextMatrix(1, 1));
						}
						else
						{
							rsControl.Set_Fields("RecommitmentDate", 0);
						}
					}
					else
					{
						rsControl.Set_Fields("OldCollector", "");
						rsControl.Set_Fields("RecommitmentDate", 0);
					}
					rsControl.Set_Fields("DateUpdated", DateTime.Today);
					// MAL@20080813: Save new default sort order
					// Tracker Reference: 14183
					if (cmbSortOrder.SelectedIndex == 0)
					{
						rsControl.Set_Fields("DefaultSort", "A");
					}
					else
					{
						rsControl.Set_Fields("DefaultSort", "N");
					}
					rsControl.Update();
				}
				else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW"))
				{
					// ******************
					rsControl.OpenRecordset("SELECT * FROM " + strWS + "Control_LienProcess");
					if (rsControl.EndOfFile())
					{
						rsControl.AddNew();
					}
					else
					{
						rsControl.Edit();
					}
					rsControl.Set_Fields("DateCreated", DateTime.Today);
					rsControl.Set_Fields("User", modGlobalConstants.Statics.gstrUserID);
					// .Fields("BillingYear") = Val(FormatYear(frmRateRecChoice.cmbYear.Text))
					rsControl.Set_Fields("MinimumAmount", FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text));
					dblMinimumAmount = FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text);
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)))
						{
							rsControl.Set_Fields("FilingDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1)));
						}
						else
						{
							MessageBox.Show("Enter a valid Filing Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							SaveControlInformation = false;
							return SaveControlInformation;
						}
					}
					else
					{
						MessageBox.Show("Enter a valid Filing Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("CollectorName", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[4].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter a tax Collector's name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1) != "")
					{
						// kk08022016 trout-1238
						rsControl.Set_Fields("CollectorTitle", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Collector's Title.", "Invalid Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MuniTitle", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Utility Title.", "Invalid Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("Muni", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Municipality Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("County", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the County Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("State", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the State.", "Invalid State", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					// If vsVariableSetup.TextMatrix(frfFreeReport(10).RowNumber, 1) <> "" Then
					rsControl.Set_Fields("Designation", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1));
					// Else
					// MsgBox "Enter the Designation.", vbExclamation, "Invalid Title"
					// SaveControlInformation = False
					// Exit Function
					// End If
					// If vsVariableSetup.TextMatrix(frfFreeReport(11).RowNumber, 1) <> "" Then
					rsControl.Set_Fields("Signer", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1));
					// Else
					// MsgBox "Enter the Signer's Name.", vbExclamation, "Invalid Name"
					// SaveControlInformation = False
					// Exit Function
					// End If
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)))
						{
							rsControl.Set_Fields("CommissionExpirationDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1)));
						}
						else
						{
							// MsgBox "Enter a valid Commission Expiration Date or leave blank.", vbExclamation, "Invalid Date"
							MessageBox.Show("Enter a valid Commission Expiration Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							SaveControlInformation = false;
							return SaveControlInformation;
						}
					}
					else
					{
						// MAL@20071210
						// .Fields("CommissionExpirationDate") = 0
						MessageBox.Show("Enter a valid Commission Expiration Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1)) == 0)
					{
						rsControl.Set_Fields("FilingFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1, "0.00");
					}
					else
					{
						rsControl.Set_Fields("FilingFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[14].RowNumber, 1)));
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)) == 0)
					{
						rsControl.Set_Fields("CertMailFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, "0.00");
					}
					else
					{
						rsControl.Set_Fields("CertMailFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)));
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MapPreparer", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Map Preparer's Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MapPrepareDate", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Map Prepared Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("LegalDescription", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Legal Description", "Invalid Reference", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[20].RowNumber, 1), 8) == "Owner at")
					{
						rsControl.Set_Fields("MaiLTo", 0);
					}
					else
					{
						rsControl.Set_Fields("MailTo", 1);
					}
					rsControl.Set_Fields("PayCertMailFee", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[21].RowNumber, 1), 2) == "Ye"));
					rsControl.Set_Fields("SendCopyToMortHolder", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						rsControl.Set_Fields("ChargeForMortHolder", FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 9) == "Yes, char"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForMortHolder", false);
					}
					rsControl.Set_Fields("SendCopyToNewOwner", Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[38].RowNumber, 1)));
					if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
					{
						rsControl.Set_Fields("OldCollector", vsRecommitment.TextMatrix(0, 1));
						rsControl.Set_Fields("RecommitmentDate", vsRecommitment.TextMatrix(1, 1));
					}
					else
					{
						rsControl.Set_Fields("OldCollector", "");
						rsControl.Set_Fields("RecommitmentDate", 0);
					}
					rsControl.Set_Fields("DateUpdated", Strings.Format(DateTime.Today, "MM/dd/yyyy"));
					// MAL@20080813: Save new default sort order
					// Tracker Reference: 14183
					if (cmbSortOrder.SelectedIndex == 0)
					{
						rsControl.Set_Fields("DefaultSort", "A");
					}
					else
					{
						rsControl.Set_Fields("DefaultSort", "N");
					}
					rsControl.Update();
				}
				else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW"))
				{
					// ******************
					rsControl.OpenRecordset("SELECT * FROM " + strWS + "Control_LienMaturity");
					if (rsControl.EndOfFile())
					{
						rsControl.AddNew();
					}
					else
					{
						rsControl.Edit();
					}
					rsControl.Set_Fields("DateCreated", DateTime.Today);
					rsControl.Set_Fields("User", modGlobalConstants.Statics.gstrUserID);
					// .Fields("BillingYear") = Val(FormatYear(frmRateRecChoice.cmbYear.Text))
					rsControl.Set_Fields("MinimumAmount", FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text));
					dblMinimumAmount = FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text);
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)))
						{
							rsControl.Set_Fields("MailDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[1].RowNumber, 1)));
						}
						else
						{
							MessageBox.Show("Enter a valid Mail Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							SaveControlInformation = false;
							return SaveControlInformation;
						}
					}
					else
					{
						MessageBox.Show("Enter a valid Mail Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1)))
						{
							rsControl.Set_Fields("LienFilingDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[2].RowNumber, 1)));
						}
						else
						{
							MessageBox.Show("Enter a valid Lien Filing Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							SaveControlInformation = false;
							return SaveControlInformation;
						}
					}
					else
					{
						MessageBox.Show("Enter a valid Lien Filing Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("MuniTitle", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[5].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Utility Title.", "Invalid Title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("Muni", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[6].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Municipality Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("County", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[7].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the County Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("State", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[8].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the State.", "Invalid State", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					// If vsVariableSetup.TextMatrix(frfFreeReport(9).RowNumber, 1) <> "" Then
					rsControl.Set_Fields("Designation", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[9].RowNumber, 1));
					// Else
					// MsgBox "Enter the Designation.", vbExclamation, "Invalid Title"
					// SaveControlInformation = False
					// Exit Function
					// End If
					// If vsVariableSetup.TextMatrix(frfFreeReport(10).RowNumber, 1) <> "" Then
					rsControl.Set_Fields("Signer", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[10].RowNumber, 1));
					// Else
					// MsgBox "Enter the Signer's Name.", vbExclamation, "Invalid Name"
					// SaveControlInformation = False
					// Exit Function
					// End If
					// If vsVariableSetup.TextMatrix(frfFreeReport(11).RowNumber, 1) <> "" Then
					rsControl.Set_Fields("SignerPhone", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1));
					// Else
					// MsgBox "Enter the Signer's Phone.", vbExclamation, "Invalid Phone"
					// SaveControlInformation = False
					// Exit Function
					// End If
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("CollectorPhone", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[12].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Collector's Phone.", "Invalid Phone", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1)) != 0)
					{
						rsControl.Set_Fields("CertMailFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1)));
					}
					else
					{
						rsControl.Set_Fields("CertMailFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[13].RowNumber, 1, "0.00");
					}
					if (Conversion.Val(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)) == 0)
					{
						rsControl.Set_Fields("ForeclosureFee", 0);
						vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1, "0.00");
					}
					else
					{
						rsControl.Set_Fields("ForeclosureFee", FCConvert.ToDouble(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[15].RowNumber, 1)));
					}
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1)))
						{
							rsControl.Set_Fields("ForeclosureDate", DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[16].RowNumber, 1)));
						}
						else
						{
							MessageBox.Show("Enter a valid Foreclosure Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							SaveControlInformation = false;
							return SaveControlInformation;
						}
					}
					else
					{
						MessageBox.Show("Enter a valid Foreclosure Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1) != "")
					{
						rsControl.Set_Fields("LegalDescription", vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[17].RowNumber, 1));
					}
					else
					{
						MessageBox.Show("Enter the Legal Description", "Invalid Reference", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					if (Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1), 8) == "Owner at")
					{
						rsControl.Set_Fields("MailTo", 0);
					}
					else
					{
						rsControl.Set_Fields("MailTo", 1);
					}
					rsControl.Set_Fields("PayCertMailFee", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1), 2) == "Ye"));
					rsControl.Set_Fields("SendCopyToMortHolder", FCConvert.CBool(Strings.Left(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1), 2) == "Ye"));
					if (FCConvert.ToBoolean(rsControl.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						rsControl.Set_Fields("ChargeForMortHolder", FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[31].RowNumber, 1), 9) == "Yes, char"));
					}
					else
					{
						rsControl.Set_Fields("ChargeForMortHolder", false);
					}
					rsControl.Set_Fields("SendCopyToNewOwner", Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1)));
					rsControl.Set_Fields("DateUpdated", DateTime.Today);
					// need to save the interest question here
					rsControl.Set_Fields("UseMailDateForMaturity", modUTLien.Statics.gboolUseMailDateForMaturity);
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1)) != "")
					{
						rsControl.Set_Fields("Organization", Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[36].RowNumber, 1)));
					}
					else
					{
						MessageBox.Show("Enter a valid organization.", "Invalid Entry", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveControlInformation = false;
						return SaveControlInformation;
					}
					// MAL@20080813: Save new default sort order
					// Tracker Reference: 14183
					if (cmbSortOrder.SelectedIndex == 0)
					{
						rsControl.Set_Fields("DefaultSort", "A");
					}
					else
					{
						rsControl.Set_Fields("DefaultSort", "N");
					}
					rsControl.Update();
				}
				return SaveControlInformation;
			}
			catch (Exception ex)
			{
				
				SaveControlInformation = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Control Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveControlInformation;
		}

		private void SetCellValue()
		{
			// this sub will make sure that any edit text is put into the grid so that
			// the GetVariableValue function can find a recently edited value correctly
			if (vsVariableSetup.EditText != "")
			{
				vsVariableSetup.TextMatrix(vsVariableSetup.Row, vsVariableSetup.Col, vsVariableSetup.EditText);
				vsVariableSetup.Select(0, 0);
			}
			if (vsRecommitment.EditText != "")
			{
				vsRecommitment.TextMatrix(vsRecommitment.Row, vsRecommitment.Col, vsRecommitment.EditText);
				vsRecommitment.Select(0, 0);
			}
		}

		private void InitialSettings()
		{
			// this will reset the LienStatusEligibility field of the bills
			// to the previous and check to see if it is still eligibly for this step
			clsDRWrapper rsInitial = new clsDRWrapper();
			clsDRWrapper rsInitialLien = new clsDRWrapper();
			clsDRWrapper rsUT = new clsDRWrapper();
			string strSQL;
			string strAccountList;
			string strNoNEligibleAccounts;
			string strBankruptcyAccounts;
			bool boolEligible;
			int lngMaxAccount;
			int lngCount;
			string strTemp = "";
			string strFields = "";
			int lngOldestRK = 0;
			string[] strArr = null;
			int intCT;
			int lngLastEligibleAccount = 0;
			// Debug.Print Time & " - Start Initial Settings"
			strSQL = "";
			strAccountList = "";
			strNoNEligibleAccounts = "";
			strBankruptcyAccounts = "";
			lngMaxAccount = 0;
			boolEligible = false;
			// this will set all of the accounts that are currently eligible back to the last eligibility
			if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW")
			{
				strSQL = "UPDATE Bill SET WLienStatusEligibility = 0 WHERE BillStatus = 'B' AND (WLienProcessStatus = 1 OR WLienProcessStatus = 0) ";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW")
			{
				strSQL = "UPDATE Bill SET WLienStatusEligibility = 2 WHERE BillStatus = 'B' AND (WLienProcessStatus = 3 OR WLienProcessStatus = 2) ";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW")
			{
				strSQL = "UPDATE Bill SET WLienStatusEligibility = 4 WHERE BillStatus = 'B' AND (WLienProcessStatus = 5 OR WLienProcessStatus = 4) ";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES")
			{
				strSQL = "UPDATE Bill SET SLienStatusEligibility = 0 WHERE BillStatus = 'B' AND (SLienProcessStatus = 1 OR SLienProcessStatus = 0) ";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS")
			{
				strSQL = "UPDATE Bill SET SLienStatusEligibility = 2 WHERE BillStatus = 'B' AND (SLienProcessStatus = 3 OR SLienProcessStatus = 2) ";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS")
			{
				strSQL = "UPDATE Bill SET SLienStatusEligibility = 4 WHERE BillStatus = 'B' AND (SLienProcessStatus = 5 OR SLienProcessStatus = 4) ";
			}
			if (rsInitial.Execute(strSQL, modExtraModules.strUTDatabase))
			{
				if (strRateKeyList != "")
				{
					if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS" || Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW")
					{
						strTemp = " AND (RateKey IN " + strRateKeyList + " OR BillingRateKey IN " + strRateKeyList + ")";
					}
					else
					{
						strTemp = " AND BillingRateKey IN " + strRateKeyList;
					}
				}
				else
				{
					strTemp = "";
				}
				if (strBookList != "")
				{
					strTemp += " AND (" + strBookList + ")";
				}
				else
				{
					// strTemp = ""
				}

				if (strBookList != "")
				{
					strSavedBookList = strBookList;

					if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW" || Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS")
					{
						strBookList = AdjustedBookListString();
					}

					strTemp = strTemp + " AND (" + strBookList + ")";

					strBookList = strSavedBookList;
				}


				// Check all of the accounts that have a certain eligibility to recalculate that eligibility (check to see if the account was paid off)
				if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW")
				{
					// if initial run
					strFields = " (WPrinOwed + WTaxOwed + WIntOwed + WCostOwed) ";
					strSQL = "SELECT *, " + strFields + " AS Principal FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND WLienStatusEligibility = 0 AND NOT (LienProcessExclusion = 1)" + strTemp + " ORDER BY ActualAccountNumber, BillDate";
				}
				else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW")
				{
					strFields = " (WPrinOwed + WTaxOwed + WIntOwed + WCostOwed) ";
					strSQL = "SELECT *, " + strFields + " AS Principal FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND WLienStatusEligibility = 2 AND NOT (LienProcessExclusion = 1)" + strTemp + " ORDER BY ActualAccountNumber, BillDate";
				}
				else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW")
				{
					strFields = " (WPrinOwed + WTaxOwed + WIntOwed + WCostOwed) ";
					strSQL = "SELECT WIntAdded, WCostOwed, WCostAdded, WPrinPaid, WIntPaid, WCostPaid, BillingRateKey, AccountKey, InBankruptcy, Bill, " + strFields + " AS Principal FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND WLienStatusEligibility = 4 AND NOT (LienProcessExclusion = 1)" + strTemp + " ORDER BY ActualAccountNumber, BillDate";
					rsInitialLien.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
					// this will store the Lien Records
					rsUT.OpenRecordset("SELECT * FROM Master", modExtraModules.strUTDatabase);
				}
				else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES")
				{
					// if initial run
					strFields = " (SPrinOwed + STaxOwed + SIntOwed + SCostOwed) ";
					strSQL = "SELECT *, " + strFields + " AS Principal FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND SLienStatusEligibility = 0 AND NOT (LienProcessExclusion = 1)" + strTemp + " ORDER BY ActualAccountNumber, BillDate";
				}
				else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS")
				{
					strFields = " (SPrinOwed + STaxOwed + SIntOwed + SCostOwed) ";
					strSQL = "SELECT *, " + strFields + " AS Principal FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND SLienStatusEligibility = 2 AND NOT (LienProcessExclusion = 1)" + strTemp + " ORDER BY ActualAccountNumber, BillDate";
				}
				else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS")
				{
					strFields = " (SPrinOwed + STaxOwed + SIntOwed + SCostOwed) ";
					strSQL = "SELECT SIntAdded, SCostOwed, SCostAdded, SPrinPaid, SIntPaid, SCostPaid, BillingRateKey, AccountKey, InBankruptcy, Bill, " + strFields + " AS Principal FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND SLienStatusEligibility = 4 AND NOT (LienProcessExclusion = 1)" + strTemp + " ORDER BY ActualAccountNumber, BillDate";
					rsInitialLien.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
					// this will store the Lien Records
					rsUT.OpenRecordset("SELECT * FROM Master", modExtraModules.strUTDatabase);
				}
				strTemp = "";
				if (boolCheckOnlyOldestBill)
				{
					lngOldestRK = modMain.DetermineOldestRateKey(ref strRateKeyList);
					// If Len(strRateKeyList) > 0 Then
					// strSQL = "SELECT TOP 1 * FROM (" & strSQL & ") ORDER BY Bill"
					// find the oldest rate key to only check for eligibilty for
					// strArr = Split(Mid(strRateKeyList, 2, Len(strRateKeyList) - 2), ",")
					// 
					// lngOldestRK = strArr(0)
					// For intCT = 0 To UBound(strArr)
					// If strArr(intCT) < lngOldestRK Then
					// lngOldestRK = strArr(intCT)
					// End If
					// Next
					// Else
					// lngOldestRK = 0
					// End If
				}
				rsInitial.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				// select all of the accounts that are eligible to have thier eligibility recalculated
				if (rsInitial.EndOfFile() != true)
				{
					lngMaxAccount = rsInitial.RecordCount();
				}
				else
				{
					lngCount = 0;
				}
				lngCount = 0;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Calculating Eligibility", true, lngMaxAccount, true);
				this.Refresh();
				while (!rsInitial.EndOfFile())
				{
					// cycle through them and find the accounts that are still eligible
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					boolEligible = false;
					if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES"))
					{
						// MAL@20070917: Changed from 30DAYNOTICE
						// check in the BillingMaster to see if there is any outstanding balances
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						if (rsInitial.Get_Fields("Principal") - rsInitial.Get_Fields(strWS + "IntAdded") + rsInitial.Get_Fields(strWS + "CostOwed") - rsInitial.Get_Fields(strWS + "CostAdded") > rsInitial.Get_Fields(strWS + "PrinPaid") + rsInitial.Get_Fields(strWS + "IntPaid") + rsInitial.Get_Fields(strWS + "CostPaid"))
						{
							if (boolCheckOnlyOldestBill)
							{
								if (FCConvert.ToInt32(rsInitial.Get_Fields_Int32("BillingRateKey")) == lngOldestRK)
								{
									boolEligible = true;
									lngLastEligibleAccount = FCConvert.ToInt32(rsInitial.Get_Fields_Int32("AccountKey"));
								}
								else
								{
									if (lngLastEligibleAccount == FCConvert.ToInt32(rsInitial.Get_Fields_Int32("AccountKey")))
									{
										boolEligible = true;
									}
									else
									{
										boolEligible = false;
									}
								}
							}
							else
							{
								boolEligible = true;
							}
						}
						else
						{
							boolEligible = false;
						}
					}
					else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS"))
					{
						// If rsInitial.Fields("ActualAccountNumber") = 76 Then
						// frmWait.Top = -2000
						// MsgBox "stop"
						// End If
						// check in the BillingMaster to see if there is any outstanding balances
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						if (rsInitial.Get_Fields("Principal") - rsInitial.Get_Fields(strWS + "IntAdded") + rsInitial.Get_Fields(strWS + "CostOwed") - rsInitial.Get_Fields(strWS + "CostAdded") > rsInitial.Get_Fields(strWS + "PrinPaid") + rsInitial.Get_Fields(strWS + "IntPaid") + rsInitial.Get_Fields(strWS + "CostPaid"))
						{
							if (boolCheckOnlyOldestBill)
							{
								if (FCConvert.ToInt32(rsInitial.Get_Fields_Int32("BillingRateKey")) == lngOldestRK)
								{
									boolEligible = true;
									lngLastEligibleAccount = FCConvert.ToInt32(rsInitial.Get_Fields_Int32("AccountKey"));
								}
								else
								{
									if (lngLastEligibleAccount == FCConvert.ToInt32(rsInitial.Get_Fields_Int32("AccountKey")))
									{
										boolEligible = true;
									}
									else
									{
										boolEligible = false;
									}
								}
							}
							else
							{
								boolEligible = true;
							}
						}
						else
						{
							boolEligible = false;
						}
					}
					else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS"))
					{
						// check in the LienRec to see if there is any outstanding balances
						if (FCConvert.ToBoolean(rsInitial.Get_Fields_Boolean("InBankruptcy")))
						{
							boolEligible = false;
							strBankruptcyAccounts += rsInitial.Get_Fields_Int32("AccountKey") + ",";
						}
						else
						{
							if (boolCheckOnlyOldestBill)
							{
								if (FCConvert.ToInt32(rsInitial.Get_Fields_Int32("BillingRateKey")) == lngOldestRK)
								{
									boolEligible = true;
								}
								else
								{
									boolEligible = false;
								}
							}
							else
							{
								boolEligible = true;
							}
						}
					}
					if (boolEligible)
					{
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strAccountList += rsInitial.Get_Fields("Bill");
						// add this billkey to the account list
					}
					else
					{
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strNoNEligibleAccounts += rsInitial.Get_Fields("Bill") + ",";
					}
					if (strAccountList.Length > 0)
					{
						if (Strings.Right(strAccountList, 1) == ",")
						{
							strAccountList = Strings.Left(strAccountList, strAccountList.Length - 1);
						}
						if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICEW")
						{
							strSQL = "UPDATE Bill SET WLienStatusEligibility = 1 WHERE ID = " + strAccountList;
						}
						else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSW")
						{
							strSQL = "UPDATE Bill SET WLienStatusEligibility = 3 WHERE ID = " + strAccountList;
						}
						else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYW")
						{
							strSQL = "UPDATE Bill SET WLienStatusEligibility = 5 WHERE ID = " + strAccountList;
						}
						else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICES")
						{
							strSQL = "UPDATE Bill SET SLienStatusEligibility = 1 WHERE ID = " + strAccountList;
						}
						else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESSS")
						{
							strSQL = "UPDATE Bill SET SLienStatusEligibility = 3 WHERE ID = " + strAccountList;
						}
						else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITYS")
						{
							strSQL = "UPDATE Bill SET SLienStatusEligibility = 5 WHERE ID = " + strAccountList;
						}
						if (strAccountList != "")
						{
							rsInitial.Execute(strSQL, modExtraModules.strUTDatabase);
							// set the account status eligibility
						}
						strAccountList = "";
						// this will reset the account list
					}
					// move to the next record
					rsInitial.MoveNext();
				}
				rsInitial.Reset();
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Setting Eligibility");
				// this will take the comma off of the end of the string if there is one
				if (strBankruptcyAccounts.Length > 0)
				{
					strBankruptcyAccounts = Strings.Left(strBankruptcyAccounts, strBankruptcyAccounts.Length - 1);
					frmWait.InstancePtr.Unload();
					MessageBox.Show("The following is a list of accounts that will not print Lien Maturity Notices due to bankruptcy.", "Bankrupt Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
					// MsgBox "Some accounts will not be processed due to bankruptcy.  Run the 'Print Bankruptcy Report' to see the accounts no eligible for Lien Maturity.", vbInformation, "Bankrupt Accounts"
					// this is where I actually run and save the report, It can be shown later from the Lien Maturity Menu
					// kk04062015 trout-1047  Fix the bankruptcy report
					// rptBankruptcy.Init "SELECT * FROM Master WHERE ID IN (" & strBankruptcyAccounts & ")"
					rptBankruptcy.InstancePtr.Init("SELECT m.AccountNumber, p.FullNameLF AS OwnerName, p.Address1 AS OAddress1, p.Address2 AS OAddress2, p.Address3 AS OAddress3, " + " p.City AS OCity, p.State AS OState, p.Zip AS OZip " + " FROM Master m INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView p ON m.OwnerPartyID = p.PartyID WHERE m.ID IN (" + strBankruptcyAccounts + ")", true);
					// rptBankruptcy.Run True
				}
				// If Len(strAccountList) > 0 Then
				// strAccountList = Left$(strAccountList, Len(strAccountList) - 1)
				// 
				// Select Case UCase(strReportType)
				// Case "30DAYNOTICE"
				// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 1 WHERE BillKey IN (" & strAccountList & ")"
				// Case "LIENPROCESS"
				// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 3 WHERE BillKey IN (" & strAccountList & ")"
				// Case "LIENMATURITY"
				// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 5 WHERE BillKey IN (" & strAccountList & ")"
				// End Select
				// 
				// rsInitial.Execute strSQL            'set the account status eligibility
				// End If
				if (strNoNEligibleAccounts.Length > 0)
				{
					strNoNEligibleAccounts = Strings.Left(strNoNEligibleAccounts, strNoNEligibleAccounts.Length - 1);
				}
				// Added 12/01/2003
				// set all the tax aquired properties back to the last eligibility level, this should stop any more lien activity against tax aquired property
				// rsInitial.OpenRecordset "SELECT * FROM BillingMaster INNER JOIN Master ON BillingMaster.Account = Master.RSAccount WHERE TaxAcquired = TRUE"
				// Do Until rsInitial.EndOfFile
				// rsInitial.Edit
				// Select Case UCase(strReportType)
				// Case "30DAYNOTICE"
				// rsInitial.Fields("LienStatusEligibility") = 0
				// rsInitial.Fields("LienProcessStatus") = 0
				// 
				// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 1 WHERE BillKey = " & strAccountList
				// Case "LIENPROCESS"
				// rsInitial.Fields("LienStatusEligibility") = 2
				// rsInitial.Fields("LienProcessStatus") = 2
				// 
				// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 3 WHERE BillKey = " & strAccountList
				// Case "LIENMATURITY"
				// rsInitial.Fields("LienStatusEligibility") = 4
				// rsInitial.Fields("LienProcessStatus") = 4
				// 
				// strSQL = "UPDATE BillingMaster SET LienStatusEligibility = 5 WHERE BillKey = " & strAccountList
				// End Select
				// rsInitial.Update
				// Loop
			}
			else
			{
				MessageBox.Show("An ERROR occured while updating the database.", "Lien Status Eligibility", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string AdjustedBookListString()
		{
			int intListLen;
			int intPdPos;
			int intOrPos;
			string tempString;
			string strNewBookList = "";

			intListLen = strSavedBookList.Length;
			intPdPos = Strings.InStr(strSavedBookList, ".");
			intOrPos = Strings.InStr(strSavedBookList, "R");
			strBookList = "";


			if (intOrPos == 0)
			{
				strBookList = strSavedBookList.Right(strSavedBookList.Length - intPdPos);
			}
			else
			{
				strNewBookList = strSavedBookList;

				while (intOrPos != 0)
				{
					tempString = strNewBookList.Left(intOrPos - 2);
					tempString = tempString.Substring(intPdPos, tempString.Length - intPdPos);

					strBookList = strBookList + tempString + " OR ";


					strNewBookList = strSavedBookList.Right(intListLen - intOrPos);

					intListLen = strNewBookList.Length;

					intPdPos = Strings.InStr(strNewBookList, ".");
					intOrPos = Strings.InStr(strNewBookList, "R");
				}

				tempString = strNewBookList.Right(intListLen - intPdPos);

				strBookList = strBookList + tempString;
			}

			return strBookList;
		}

		private void SetPositions()
		{
			//FC:FINAL:DDU: Were set in designer already
			//int lngTemp;
			//// this will set the positions of the frames and other controls on the form
			//fraEdit.Top = 0;
			//fraEdit.Left = FCConvert.ToInt32((this.Width - fraEdit.Width) / 2.0);
			//fraVariableSetup.Top = FCConvert.ToInt32((this.Height - fraVariableSetup.Height) / 2.0);
			//fraVariableSetup.Left = FCConvert.ToInt32((this.Width - fraVariableSetup.Width) / 2.0);
			//// fraRecommitment.Top = (fraSummary.Top + fraSummary.Height) + ((Me.Height - (fraSummary.Top + fraSummary.Height)) - fraRecommitment.Height) / 2
			//// fraRecommitment.Top = (fraVariableSetup.Top + vsVariableSetup.Top + vsVariableSetup.Height)
			//lngTemp = (fraVariableSetup.Top + vsVariableSetup.Height); // this is the bottom of the grid
			//                                                           // fraRecommitment.Top = lngTemp + ((Me.Height - lngTemp) / 2) - fraRecommitment.Height
			//                                                           // fraRecommitment.Left = (Me.Width - fraRecommitment.Width) / 2
			//                                                           // fraRecommitment.Left = vsVariableSetup.Left
			//                                                           // fraSigFile.Top = fraRecommitment.Top
			//                                                           // set the summary grid left and top
			//fraSummary.Top = FCConvert.ToInt32((this.Height - fraSummary.Height) / 2.0);
			//fraSummary.Left = FCConvert.ToInt32((this.Width - fraSummary.Width) / 2.0);
		}

		private void SaveDefaultNotice_8(short intType, string strName)
		{
			SaveDefaultNotice(ref intType, ref strName);
		}

		private void SaveDefaultNotice(ref short intType, ref string strName)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDef = new clsDRWrapper();
				// this will save the default notice into the database
				rsDef.OpenRecordset("SELECT * FROM CollectionControl", modExtraModules.strUTDatabase);
				if (!rsDef.EndOfFile())
				{
					rsDef.Edit();
				}
				else
				{
					rsDef.AddNew();
				}
				if (boolWater)
				{
					if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
					{
						switch (intType)
						{
							case 1:
								{
									// 30DN
									rsDef.Set_Fields("W30DayNoticeR", strName);
									break;
								}
							case 2:
								{
									// Lien
									rsDef.Set_Fields("WLienNoticeR", strName);
									break;
								}
							case 3:
								{
									// Maturity
									rsDef.Set_Fields("WMaturityNoticeR", strName);
									break;
								}
						}
						//end switch
					}
					else
					{
						switch (intType)
						{
							case 1:
								{
									// 30DN
									rsDef.Set_Fields("W30DayNotice", strName);
									break;
								}
							case 2:
								{
									// Lien
									rsDef.Set_Fields("WLienNotice", strName);
									break;
								}
							case 3:
								{
									// Maturity
									rsDef.Set_Fields("WMaturityNotice", strName);
									break;
								}
						}
						//end switch
					}
				}
				else
				{
					if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
					{
						switch (intType)
						{
							case 1:
								{
									// 30DN
									rsDef.Set_Fields("S30DayNoticeR", strName);
									break;
								}
							case 2:
								{
									// Lien
									rsDef.Set_Fields("SLienNoticeR", strName);
									break;
								}
							case 3:
								{
									// Maturity
									rsDef.Set_Fields("SMaturityNoticeR", strName);
									break;
								}
						}
						//end switch
					}
					else
					{
						switch (intType)
						{
							case 1:
								{
									// 30DN
									rsDef.Set_Fields("S30DayNotice", strName);
									break;
								}
							case 2:
								{
									// Lien
									rsDef.Set_Fields("SLienNotice", strName);
									break;
								}
							case 3:
								{
									// Maturity
									rsDef.Set_Fields("SMaturityNotice", strName);
									break;
								}
						}
						//end switch
					}
				}
				rsDef.Update();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Save Default Notice", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string GetDefaultNotice_2(short intType)
		{
			return GetDefaultNotice(ref intType);
		}

		private string GetDefaultNotice(ref short intType)
		{
			string GetDefaultNotice = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the string of the name of the default report
				clsDRWrapper rsDef = new clsDRWrapper();
				string strName = "";
				string strWS = "";
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				rsDef.OpenRecordset("SELECT * FROM CollectionControl", modExtraModules.strUTDatabase);
				if (!rsDef.EndOfFile())
				{
					if (chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
					{
						switch (intType)
						{
							case 1:
								{
									// 30DN
									strName = FCConvert.ToString(rsDef.Get_Fields(strWS + "30DayNoticeR"));
									break;
								}
							case 2:
								{
									// Lien
									strName = FCConvert.ToString(rsDef.Get_Fields(strWS + "LienNoticeR"));
									break;
								}
							case 3:
								{
									// Maturity
									strName = FCConvert.ToString(rsDef.Get_Fields(strWS + "MaturityNoticeR"));
									break;
								}
							case 10:
								{
									// Reminder Notice Form
									strName = FCConvert.ToString(rsDef.Get_Fields_String("RNForm"));
									break;
								}
						}
						//end switch
					}
					else
					{
						switch (intType)
						{
							case 1:
								{
									// 30DN
									strName = FCConvert.ToString(rsDef.Get_Fields(strWS + "30DayNotice"));
									break;
								}
							case 2:
								{
									// Lien
									strName = FCConvert.ToString(rsDef.Get_Fields(strWS + "LienNotice"));
									break;
								}
							case 3:
								{
									// Maturity
									strName = FCConvert.ToString(rsDef.Get_Fields(strWS + "MaturityNotice"));
									break;
								}
							case 10:
								{
									// Reminder Notice Form
									strName = FCConvert.ToString(rsDef.Get_Fields_String("RNForm"));
									break;
								}
						}
						//end switch
					}
				}
				GetDefaultNotice = strName;
				return GetDefaultNotice;
			}
			catch (Exception ex)
			{
				
				GetDefaultNotice = "";
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Get Default Notice", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetDefaultNotice;
		}

		private void SaveRateRecInformation()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will save the dates in the rate record
				// save the dates into the Rate record
				DateTime dtDate = default(DateTime);
				clsDRWrapper rsSaveRate = new clsDRWrapper();
				clsDRWrapper rsGetKeys = new clsDRWrapper();
				string strField = "";
				string strRateKeys = "";
				string strWS = "";
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				// Unload frmWait
				// MsgBox ("This information will be saved into the Rate record and can be edited manually.")
				if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICE" + strWS)
				{
					dtDate = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1));
					strField = "30DNDate";
				}
				else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESS" + strWS)
				{
					dtDate = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1));
					strField = "LienDate";
				}
				else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITY" + strWS)
				{
					dtDate = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[0].RowNumber, 1));
					strField = "MaturityDate";
				}
				rsGetKeys.OpenRecordset("SELECT DISTINCT BillingRateKey FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY");
				while (!rsGetKeys.EndOfFile())
				{
					strRateKeys += rsGetKeys.Get_Fields_Int32("BillingRateKey") + ",";
					rsGetKeys.MoveNext();
				}
				if (Strings.Trim(strRateKeys) != "")
				{
					strRateKeys = "(" + Strings.Left(strRateKeys, strRateKeys.Length - 1) + ")";
					rsSaveRate.Execute("UPDATE RateKeys Set [" + strField + strWS + "] = '" + Strings.Format(dtDate, "MM/dd/yyyy") + "' WHERE [" + strField + strWS + "] = 0 AND ID IN " + strRateKeys, modExtraModules.strUTDatabase);
				}
				else
				{
					// do nothing...no keys
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Rate Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckFormMandatoryVariables_2(short intType, string strText, ref string strReturnVariables)
		{
			return CheckFormMandatoryVariables(ref intType, ref strText, ref strReturnVariables);
		}

		private bool CheckFormMandatoryVariables(ref short intType, ref string strText, ref string strReturnVariables)
		{
			bool CheckFormMandatoryVariables = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will check all of the text in the string saved by the user to make
				// sure that all of the mandatory information has been shown to the owner/MH.
				// It will return True if all of the variables have been used and false otherwise
				CheckFormMandatoryVariables = true;
				// set it to true and try to prove it wrong
				switch (intType)
				{
					case 1:
						{
							// 30 Day Notice
							if (Strings.InStr(1, strText, "<MUNI>", CompareConstants.vbBinaryCompare) == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "MUNI, ";
							}
							if (Strings.InStr(1, strText, "<MAILDATE>", CompareConstants.vbBinaryCompare) == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "MAILDATE, ";
							}
							if (Strings.InStr(1, strText, "<NAMEADDRESS>", CompareConstants.vbBinaryCompare) == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "NAMEADDRESS, ";
							}
							// If InStr(1, strText, "<PRINCIPAL>") = 0 Then
							// CheckFormMandatoryVariables = False
							// strReturnVariables = strReturnVariables & "TOTALDUE, "
							// End If
							if (Strings.InStr(1, strText, "<LOCATION>", CompareConstants.vbBinaryCompare) == 0)
							{
								CheckFormMandatoryVariables = false;
								strReturnVariables += "LOCATION, ";
							}
							break;
						}
					case 2:
						{
							// Lien
							break;
						}
					case 3:
						{
							// Lien Maturity
							break;
						}
				}
				//end switch
				return CheckFormMandatoryVariables;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Rate Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckFormMandatoryVariables;
		}

		private bool ContainsThisString(ref string strVariable, ref object strTotalString)
		{
			bool ContainsThisString = false;
			return ContainsThisString;
		}

		private bool ValidateUserVariables()
		{
			bool ValidateUserVariables = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				DateTime dtDate1;
				DateTime dtDate2;
				ValidateUserVariables = true;
				if (Strings.Left(Strings.UCase(modUTStatusList.Statics.strReportType), modUTStatusList.Statics.strReportType.Length - 1) == "30DAYNOTICE")
				{
				}
				else if (Strings.Left(Strings.UCase(modUTStatusList.Statics.strReportType), modUTStatusList.Statics.strReportType.Length - 1) == "LIENPROCESS")
				{
					if (Strings.Trim(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1)) != "")
					{
						if (Information.IsDate(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1)))
						{
							dtDate1 = DateAndTime.DateValue(vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[11].RowNumber, 1));
							if (DateTime.Today.ToOADate() > dtDate1.ToOADate())
							{
								MessageBox.Show("The commissioner's expiration is before the current date.", "Expired Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								ValidateUserVariables = false;
							}
						}
						else
						{
						}
					}
				}
				else if (Strings.Left(Strings.UCase(modUTStatusList.Statics.strReportType), modUTStatusList.Statics.strReportType.Length - 1) == "LIENMATURITY")
				{
				}
				return ValidateUserVariables;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Rate Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateUserVariables;
		}        
    }
}
