﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptBillNonTransferReportMaster.
	/// </summary>
	public partial class rptBillNonTransferReportMaster : BaseSectionReport
	{
		public rptBillNonTransferReportMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Accounts Not Billed Report";
		}

		public static rptBillNonTransferReportMaster InstancePtr
		{
			get
			{
				return (rptBillNonTransferReportMaster)Sys.GetInstance(typeof(rptBillNonTransferReportMaster));
			}
		}

		protected rptBillNonTransferReportMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillNonTransferReportMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/07/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/12/2006              *
		// ********************************************************
		int[] BookArray = null;
		public int lngRateKey;
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsParty = new clsDRWrapper();
		public double dblTotalSumAmount0;
		public double dblTotalSumAmount1;
		public double dblTotalSumAmount2;
		public double dblTotalSumAmount3;
		public double dblTotalSumAmount4;
		public double dblTotalSumAmount5;
		public double dblTotalSumAmount6;
		public int lngBillCount;
		int lngCT;
		public DateTime dtBillingDate;
		public int lngOrder;
		public string strBooks = string.Empty;
		bool boolDone;

		public void Init(ref int[] BA, ref int lngRK, ref DateTime dtBillDate)
		{
			try
			{
				BookArray = BA;
				lngRateKey = lngRK;
				dtBillingDate = dtBillDate;
				SetBookString();
				frmReportViewer.InstancePtr.Init(this);
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = FCConvert.CBool(lngCT > Information.UBound(BookArray, 1));
				if (lngOrder == 0)
				{
					boolDone = eArgs.EOF;
				}
				else
				{
					boolDone = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
                using (clsDRWrapper rsOrder = new clsDRWrapper())
                {
                    rsOrder.OpenRecordset("SELECT BillingRepSeq FROM UtilityBilling", modExtraModules.strUTDatabase);
                    if (!rsOrder.EndOfFile())
                    {
                        lngOrder = FCConvert.ToInt32(
                            Math.Round(Conversion.Val(rsOrder.Get_Fields_Int32("BillingRepSeq"))));
                    }

                    this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;

                    lngCT = 1;
                    if (Information.UBound(BookArray, 1) > 0)
                    {
                        if (LoadAccounts())
                        {
                            FillHeaderLabels();
                            lblReportType.Text = "Rate Key : " + FCConvert.ToString(lngRateKey);
                        }
                        else
                        {
                            MessageBox.Show(
                                "No eligible accounts found for rate key " + FCConvert.ToString(lngRateKey) + ".",
                                "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            Cancel();
                            this.Close();
                        }
                    }
                    else
                    {
                        MessageBox.Show(
                            "No eligible accounts found for rate key " + FCConvert.ToString(lngRateKey) + ".",
                            "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Cancel();
                        this.Close();
                    }
                }
            }
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
                using (clsDRWrapper rsBill = new clsDRWrapper())
                {
                    if (lngOrder == 0)
                    {
                        CheckAgain:
                        if (lngCT <= Information.UBound(BookArray, 1))
                        {
                            // Check that this book has bills combined with this rate key
                            rsBill.OpenRecordset(
                                "SELECT Count(ID) as TotalBills FROM Bill WHERE BillingRateKey = " +
                                FCConvert.ToString(lngRateKey) + " AND Book = " + FCConvert.ToString(BookArray[lngCT]),
                                modExtraModules.strUTDatabase);
                            if (rsBill.Get_Fields("TotalBills") > 0)
                            {
                                srptBookDetail.Report = new rptBillNonTransferReport();
                                srptBookDetail.Report.UserData =
                                    FCConvert.ToString(BookArray[lngCT]) + "|" + FCConvert.ToString(lngRateKey) + "|" +
                                    FCConvert.ToString(dtBillingDate) + "|" + FCConvert.ToString(lngOrder);
                                lngCT += 1;
                            }
                            else
                            {
                                lngCT += 1;
                                goto CheckAgain;
                            }

                            rsBill.Reset();
                        }
                        else
                        {
                            srptBookDetail.Report = null;
                            Detail.Height = 0;
                        }
                    }
                    else
                    {
                        SetBookString();
                        // Check that this book has bills combined with this rate key
                        rsBill.OpenRecordset(
                            "SELECT Count(ID) as TotalBills FROM Bill WHERE BillingRateKey = " +
                            FCConvert.ToString(lngRateKey) + " AND Book IN (" + strBooks + ")",
                            modExtraModules.strUTDatabase);
                        if (rsBill.Get_Fields("TotalBills") > 0)
                        {
                            srptBookDetail.Report = new rptBillNonTransferReport();
                            srptBookDetail.Report.UserData =
                                strBooks + "|" + FCConvert.ToString(lngRateKey) + "|" +
                                FCConvert.ToString(dtBillingDate) + "|" + FCConvert.ToString(lngOrder);
                        }
                        else
                        {
                            srptBookDetail.Report = null;
                            Detail.Height = 0;
                        }

                        rsBill.Reset();
                    }
                }
            }
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTotals()
		{
			try
			{
				fldTotalCons.Text = Strings.Format(dblTotalSumAmount0, "#,##0");
				fldTotalRegular.Text = Strings.Format(dblTotalSumAmount1, "#,##0.00");
				fldTotalMisc.Text = Strings.Format(dblTotalSumAmount2, "#,##0.00");
				fldTotalTax.Text = Strings.Format(dblTotalSumAmount3, "#,##0.00");
				fldTotalPastDue.Text = Strings.Format(dblTotalSumAmount4, "#,##0.00");
				fldTotalInterest.Text = Strings.Format(dblTotalSumAmount5, "#,##0.00");
				fldTotalAmount.Text = Strings.Format(dblTotalSumAmount6, "#,##0.00");
				if (lngBillCount == 1)
				{
					fldFooter.Text = "Total: " + "\r\n" + "1 bill";
				}
				else
				{
					fldFooter.Text = "Total: " + "\r\n" + FCConvert.ToString(lngBillCount) + " bills";
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Totals", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			FillTotals();
		}

		private void SetBookString()
		{
			int intCT;
			strBooks = "";
			for (intCT = 1; intCT <= Information.UBound(BookArray, 1); intCT++)
			{
				strBooks += FCConvert.ToString(BookArray[intCT]) + ",";
			}
			strBooks = Strings.Left(strBooks, strBooks.Length - 1);
		}

		private bool LoadAccounts()
		{
			bool LoadAccounts = false;
			try
			{
				string strSQL;
				string strFields;
				string strFieldsNoBill;
				string strSeq = "";

				strFields =
					"AccountNumber, TotalWBillAmount, TotalSBillAmount, Bill.Book, Sequence, Consumption, WFlatAmount, WUnitsAmount, WConsumptionAmount, WAdjustAmount, " +
					"SAdjustAmount, WDEAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount, WMiscAmount, SMiscAmount, WTax, STax, " +
					"Bill.AccountKey AS AcctKey, (WPrinPaid + WTaxPaid + WIntPaid + WCostPaid + SPrinPaid + STaxPaid + SIntPaid + SCostPaid) AS TotPaid, " +
					"WHasOverride, SHasOverride, Bill.BillStatus, Master.Deleted, MeterTable.ID, Bill.NoBill, Bill.Final, Master.MapLot, Master.StreetName, Master.StreetNumber ";

				strFieldsNoBill =
					"AccountNumber, 0 as totalWbillAmount, 0 as totalSbillAmount, booknumber as book, Sequence, 0 as consumption, " +
					"0 as WFlatAmount, 0 as WUnitsAmount, 0 as WConsumptionAmount, 0 as WAdjustAmount, 0 as SAdjustAmount, 0 as WDEAdjustAmount, " +
					"0 as SDEAdjustAmount, 0 as SFlatAmount, 0 as SUnitsAmount, 0 as SConsumptionAmount, 0 as WMiscAmount, 0 as SMiscAmount, 0 as WTax, 0 as STax, " +
					"AcctKey, 0 AS TotPaid, 0 as WHasOverride, 0 as SHasOverride, '' as BillStatus, Deleted, MtrKey as ID, mq.NoBill, FinalBill as Final, mq.MapLot, mq.StreetName, mq.StreetNumber ";

				if (lngOrder == 0)
				{
					// Book/Seq
					strSeq = " ORDER BY Bill.Book, MeterTable.Sequence, AccountNumber";
				}
				else
				{
					strSeq = "";
				}

				strSQL = "SELECT " + strFields + " FROM (Bill LEFT OUTER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID " +
				         "WHERE BillingRateKey = " + FCConvert.ToString(lngRateKey) + " AND Bill.Book IN (" + strBooks + ") AND Combine = 'N' AND (Bill.BillStatus <> 'B' OR TotalWBillAmount + TotalSBillAmount = 0)";
				strSQL = strSQL + " UNION ALL " + 
				         "SELECT " + strFieldsNoBill + " FROM (SELECT m.AccountNumber, m.ID AS AcctKey, m.OwnerPartyID, m.Deleted, m.NoBill, m.FinalBill, m.MapLot, m.StreetName, m.StreetNumber, t.Sequence, t.ID as MtrKey, t.MeterNumber, t.BookNumber, t.Combine " +
				         "FROM MeterTable t INNER JOIN Master m ON t.AccountKey = m.ID WHERE MeterNumber = 1) as mq LEFT JOIN (SELECT * FROM Bill " +
				         "WHERE BillingRateKey = " + FCConvert.ToString(lngRateKey) + " OR BillingRateKey = 0) AS bq ON mq.MtrKey = bq.MeterKey WHERE ISNULL(bq.ID,0) = 0 AND BookNumber IN (" + strBooks + ")";

				strSQL = strSQL + strSeq;
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);

				if (!rsData.EndOfFile())
				{
					return true;
				}
				else
				{
					return false;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadAccounts;
		}
	}
}
