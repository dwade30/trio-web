﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptUTBookZeroListing.
	/// </summary>
	public partial class srptUTBookZeroListing : FCSectionReport
	{
		public srptUTBookZeroListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "srptUTBookZeroListing";
		}

		public static srptUTBookZeroListing InstancePtr
		{
			get
			{
				return (srptUTBookZeroListing)Sys.GetInstance(typeof(srptUTBookZeroListing));
			}
		}

		protected srptUTBookZeroListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsAddress.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptUTBookZeroListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/20/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsAddress = new clsDRWrapper();
		int lngAcctCount;
		bool boolShowMeterInformation;
		int lngCurrentAcct;
		bool blnFirstRecord;
		// this is the account summary report, to pass the SQL string in, there is a public string variable - strPrintSQL
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					lngCurrentAcct = FCConvert.ToInt32(rsData.Get_Fields("AccountNumber"));
					eArgs.EOF = false;
				}
			}
			else
			{
				CheckAgain:
				;
				rsData.MoveNext();
				if (rsData.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					if (lngCurrentAcct != FCConvert.ToInt32(rsData.Get_Fields("AccountNumber")))
					{
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						lngCurrentAcct = FCConvert.ToInt32(rsData.Get_Fields("AccountNumber"));
						eArgs.EOF = false;
					}
					else
					{
						goto CheckAgain;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strWType;
				string strSType;
				string strWRT;
				string strSRT;
				string Code = "";
				int ct;
				string strMSQL = "";
				bool boolDone = false;
				string strAddr1;
				string strAddr2;
				string strAddr3;
				strWType = "";
				strSType = "";
				strWRT = "";
				strSRT = "";
				if (boolShowMeterInformation)
				{
					// make a comma delimeted list of meter keys to show
					strMSQL = "";
					while (!boolDone)
					{
						if (Conversion.Val(rsData.Get_Fields_Int32("MeterKey") + "") != 0)
						{
							strMSQL += "ID = " + FCConvert.ToString(rsData.Get_Fields_Int32("MeterKey")) + " OR ";
						}
						rsData.MoveNext();
						if (!rsData.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							if (lngCurrentAcct != FCConvert.ToInt32(rsData.Get_Fields("AccountNumber")))
							{
								boolDone = true;
							}
							else
							{
								// add this meter too
							}
						}
						else
						{
							boolDone = true;
						}
					}
					// this will move the recordset back to the last record for the current account
					rsData.MovePrevious();
					if (Strings.Trim(strMSQL) != "")
					{
						strMSQL = Strings.Left(strMSQL, strMSQL.Length - 3);
						// take off the last AND
						// create the order and the SQL statement for the meters
						strMSQL = "SELECT * FROM MeterTable WHERE " + strMSQL + frmAccountListing.InstancePtr.strMeterSort;
						// set the subreport to show the correct info
						srptAccountListingMDetailOb.Visible = true;
						srptAccountListingMDetailOb.Report = new srptAccountListingMDetail();
						srptAccountListingMDetailOb.Report.UserData = strMSQL;
					}
					else
					{
						srptAccountListingMDetailOb.Visible = false;
					}
				}
				// keep a running total
				lngAcctCount += 1;
				// primary line
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccountNum.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
				fldTenantName.Text = FCConvert.ToString(rsData.Get_Fields_String("Name"));
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Deleted")))
				{
					fldTenantName.Text = "*" + fldTenantName.Text;
				}
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = Strings.Trim(rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
				}
				else
				{
					fldLocation.Text = Strings.Trim(" " + rsData.Get_Fields_String("StreetName"));
				}
				// secondary lines
				// Address
				// kk 06282013 trouts-21  Break up the query to avoid timeout.  Added index on MeterTable.AccountKey also
				rsAddress.OpenRecordset("SELECT Address1, Address2, Address3, City, State, Zip FROM Addresses WHERE PartyID = " + rsData.Get_Fields_Int32("BillingPartyID"), "CentralParties");
				// strAddr1 = Trim(rsData.Fields("BAddress1"))
				// strAddr2 = Trim(rsData.Fields("BAddress2"))
				// If Trim(rsData.Fields("BZip4")) <> "" Then
				// strAddr3 = Trim(rsData.Fields("BAddress3")) & " " & Trim(rsData.Fields("BCity") & ", " & rsData.Fields("BState") & " " & rsData.Fields("BZip") & "-" & rsData.Fields("BZip4"))
				// Else
				// strAddr3 = Trim(rsData.Fields("BAddress3")) & " " & Trim(rsData.Fields("BCity") & ", " & rsData.Fields("BState") & " " & rsData.Fields("BZip"))
				// End If
				strAddr1 = Strings.Trim(FCConvert.ToString(rsAddress.Get_Fields_String("Address1")));
				strAddr2 = Strings.Trim(FCConvert.ToString(rsAddress.Get_Fields_String("Address2")));
				strAddr3 = Strings.Trim(FCConvert.ToString(rsAddress.Get_Fields_String("Address3"))) + " " + Strings.Trim(rsAddress.Get_Fields_String("City") + ", " + rsAddress.Get_Fields_String("State") + " " + rsAddress.Get_Fields_String("Zip"));
				if (Strings.Trim(strAddr2) == "")
				{
					strAddr2 = strAddr3;
					strAddr3 = "";
				}
				if (Strings.Trim(strAddr1) == "")
				{
					strAddr1 = strAddr2;
					strAddr2 = strAddr3;
					strAddr3 = "";
				}
				fldAddress1.Text = Strings.Trim(strAddr1);
				fldAddress2.Text = Strings.Trim(strAddr2);
				fldAddress3.Text = Strings.Trim(strAddr3);
				fldPhoneNumber.Text = "Phone Number : " + FCConvert.ToString(rsData.Get_Fields_String("Telephone"));
				fldMapLot.Text = "Map Lot : " + FCConvert.ToString(rsData.Get_Fields_String("MapLot"));
				// MAL@20071211
				fldUseREAccount.Text = "Use RE Account : " + FCConvert.ToString(FCConvert.CBool(rsData.Get_Fields_Boolean("UseREAccount")));
				// MAL@20080527: Tracker Reference: 13713
				fldWaterTax.Text = "Water Tax Percent : " + FCConvert.ToString(rsData.Get_Fields_Int32("WaterTaxPercent"));
				fldSewerTax.Text = "Sewer Tax Percent : " + FCConvert.ToString(rsData.Get_Fields_Int32("SewerTaxPercent"));
				// BillMessage
				fldBillMessage.Text = "Bill Message : " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BillMessage")));
				// Bill To Same As Owner
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("SameBillOwner")))
				{
					fldBillToSameAsOwner.Text = "Tenant and Owner are the same.";
				}
				else
				{
					fldBillToSameAsOwner.Text = "Tenant and Owner are not the same.";
				}
				// Book
				fldBook.Text = "Book : " + rsData.Get_Fields_Int32("BookNumber");
				// Data Entry Message
				fldDataEntryMessage.Text = "DE Message : " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("DataEntry")));
				// Final Bill
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("FinalBill")))
				{
					fldFinalBill.Text = "This account it Final Billed.";
				}
				else
				{
					fldFinalBill.Text = "This account is Active.";
				}
				// No Bill
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("NoBill")))
				{
					fldNoBill.Text = "No Bill for this account.";
				}
				else
				{
					fldNoBill.Text = "Bill this account.";
				}
				// RE Account
				fldRealEstateAccount.Text = "Assoc RE Account : " + FCConvert.ToString(rsData.Get_Fields_Int32("REAccount"));
				// Sewer Override Account
				fldSewerMasterAccount.Text = "Sewer Override Account : " + FCConvert.ToString(rsData.Get_Fields_String("SewerAccount"));
				// Sewer Override Category
				fldSewerCategory.Text = "Sewer Category : " + FCConvert.ToString(rsData.Get_Fields_Int32("SewerCategory"));
				// Water Override Account
				fldWaterMasterAccount.Text = "Water Override Account : " + FCConvert.ToString(rsData.Get_Fields_String("WaterAccount"));
				// Water Override Category
				fldWaterCategory.Text = "Water Category : " + FCConvert.ToString(rsData.Get_Fields_Int32("WaterCategory"));
				// Owner Name
				// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("SecondOwnerName"))) != "")
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					fldOwnerName.Text = "Owner Name : " + FCConvert.ToString(rsData.Get_Fields("OwnerName") + " and " + rsData.Get_Fields("SecondOwnerName"));
				}
				else
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					fldOwnerName.Text = "Owner Name : " + FCConvert.ToString(rsData.Get_Fields("OwnerName"));
				}
				fldSerialNumber.Text = "Serial Number : " + FCConvert.ToString(rsData.Get_Fields_String("SerialNumber"));
				fldRemoteNumber.Text = "Remote Number : " + FCConvert.ToString(rsData.Get_Fields_String("Remote"));
				fldMXUNumber.Text = "MXU Number : " + FCConvert.ToString(rsData.Get_Fields_String("MXU"));
				if (Conversion.Val(rsData.Get_Fields_Int32("RadioAccessType") + "") == 0)
				{
					fldReadType.Text = "Meter Reading Type : Manual";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int32("RadioAccessType") + "") == 1)
				{
					fldReadType.Text = "Meter Reading Type : Auto, Non Radio";
				}
				else if (Conversion.Val(rsData.Get_Fields_Int32("RadioAccessType") + "") == 2)
				{
					fldReadType.Text = "Meter Reading Type : Auto, Radio";
				}
				fldLatitude.Text = "Latitude : " + FCConvert.ToString(rsData.Get_Fields_String("Lat"));
				fldLongitude.Text = "Longitude : " + FCConvert.ToString(rsData.Get_Fields_String("Long"));
				// trouts-6 Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					fldWaterTax.Text = "Stormwater Tax Percent : " + FCConvert.ToString(rsData.Get_Fields_Int32("WaterTaxPercent"));
					fldWaterMasterAccount.Text = "Stormwater Override Account : " + FCConvert.ToString(rsData.Get_Fields_String("WaterAccount"));
					fldWaterCategory.Text = "Stormwater Category : " + FCConvert.ToString(rsData.Get_Fields_Int32("WaterCategory"));
				}
				// SetFieldTops
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// Data.RecordSource = strPrintSQL
			strSQL = BuildSQL();
			blnFirstRecord = true;
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			SetupDetailFormat();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// This function will create the string for this report
			BuildSQL = "SELECT * FROM Master LEFT JOIN MeterTable ON Master.ID = MeterTable.AccountKey WHERE UTBook = 0";
			return BuildSQL;
		}

		private void SetupDetailFormat()
		{
			// this sub will get all of the users options from the Account Listing Form adjust the fields accordingly
			// As it keeps track of how many fields/lines the report will need for each account, the height
			// of each group will be adjusted
			int lngLine;
			float lngHeight;
			int lngStarting;
			float lngCurrentTop = 0;
			int intCT;
			int Xtra = 0;
			// extra lines
			lngStarting = 0;
			lngLine = 1;
			lngHeight = 240;
			boolShowMeterInformation = false;
			HideAllFields();
			// gets info from the last form that is hidden, not unloaded...
			for (intCT = 0; intCT <= frmAccountListing.InstancePtr.lstFields.Items.Count - 1; intCT++)
			{
				if (frmAccountListing.InstancePtr.lstFields.Selected(intCT))
				{
					lngCurrentTop = ((lngLine * lngHeight) + lngStarting) / 1440F;
					switch (frmAccountListing.InstancePtr.lstFields.ItemData(intCT))
					{
						case 1:
							{
								// Address                'Master
								fldAddress1.Visible = true;
								fldAddress2.Visible = true;
								fldAddress3.Visible = true;
								fldAddress1.Top = lngCurrentTop;
								lngLine += 1;
								lngCurrentTop = ((lngLine * lngHeight) + lngStarting) / 1440F;
								fldAddress2.Top = lngCurrentTop;
								lngLine += 1;
								lngCurrentTop = ((lngLine * lngHeight) + lngStarting) / 1440F;
								fldAddress3.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 2:
							{
								// Book                   'Master
								fldBook.Visible = true;
								fldBook.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 3:
							{
								// Billing Type           'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 4:
							{
								// Bill Message           'Master
								fldBillMessage.Visible = true;
								fldBillMessage.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 5:
							{
								// Bill To Same AS Owner  'Master
								fldBillToSameAsOwner.Visible = true;
								fldBillToSameAsOwner.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 6:
							{
								// Combination Type       'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 7:
							{
								// Current Reading        'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 8:
							{
								// Data Entry Message     'Master
								fldDataEntryMessage.Visible = true;
								fldDataEntryMessage.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 9:
							{
								// Meter Final Bill       'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 10:
							{
								// Meter Size             'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 11:
							{
								// Meter No Bill          'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 12:
							{
								// Owner Name             'Master
								fldOwnerName.Visible = true;
								fldOwnerName.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 13:
							{
								// Previous Reading       'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 14:
							{
								// RE Account             'Master
								fldRealEstateAccount.Visible = true;
								fldRealEstateAccount.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 15:
							{
								// Sequence               'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 16:
							{
								// Service                'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 17:
							{
								// Sewer Master Account   'Master
								fldSewerMasterAccount.Visible = true;
								fldSewerMasterAccount.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 18:
							{
								// Sewer RT               'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 19:
							{
								// Water Master Account   'Master
								fldWaterMasterAccount.Visible = true;
								fldWaterMasterAccount.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 20:
							{
								// Water RT               'Meter
								boolShowMeterInformation = true;
								break;
							}
						case 21:
							{
								// Account Final Bill     'Master
								fldFinalBill.Visible = true;
								fldFinalBill.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 22:
							{
								// Account No Bill        'Master
								fldNoBill.Visible = true;
								fldNoBill.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 23:
							{
								fldWaterCategory.Visible = true;
								fldWaterCategory.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 24:
							{
								fldSewerCategory.Visible = true;
								fldSewerCategory.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 25:
							{
								fldMapLot.Visible = true;
								fldMapLot.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 26:
							{
								fldPhoneNumber.Visible = true;
								fldPhoneNumber.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 27:
							{
								fldUseREAccount.Visible = true;
								fldUseREAccount.Top = lngCurrentTop;
								lngLine += 1;
								// MAL@20080527: 13713
								break;
							}
						case 28:
							{
								fldWaterTax.Visible = true;
								fldWaterTax.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 29:
							{
								fldSewerTax.Visible = true;
								fldSewerTax.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 30:
							{
								fldSerialNumber.Visible = true;
								fldSerialNumber.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 31:
							{
								fldRemoteNumber.Visible = true;
								fldRemoteNumber.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 32:
							{
								fldMXUNumber.Visible = true;
								fldMXUNumber.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 33:
							{
								fldReadType.Visible = true;
								fldReadType.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 34:
							{
								fldLatitude.Visible = true;
								fldLatitude.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						case 35:
							{
								fldLongitude.Visible = true;
								fldLongitude.Top = lngCurrentTop;
								lngLine += 1;
								break;
							}
						default:
							{
								// do nothing
								break;
							}
					}
					//end switch
				}
			}
			Xtra = 1;
			// Val(.cmbSpace.List(.cmbSpace.ListIndex))
			// this sets the heights of the detail section of the report to one line past the last entry
			if (boolShowMeterInformation)
			{
				Detail.CanGrow = true;
			}
			else
			{
				IncreaseFieldWidth();
				Detail.CanGrow = true;
			}
			Detail.Height = ((lngLine + Xtra) * lngHeight) / 1440F;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// MAL@20080227: Add check for empty recordset
			// Call Reference: 101984
			if (!rsData.EndOfFile())
			{
				BindFields();
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			lblFooter.Text = FCConvert.ToString(lngAcctCount) + " accounts.";
		}

		private void IncreaseFieldWidth()
		{
			// This will force the fields to be wider because the meter information is not there
			float lngWidth;
			lngWidth = 4700 / 1440F;
			fldAddress1.Width = lngWidth;
			fldAddress2.Width = lngWidth;
			fldAddress3.Width = lngWidth;
			fldBook.Width = lngWidth;
			fldBillMessage.Width = lngWidth;
			fldBillToSameAsOwner.Width = lngWidth;
			fldDataEntryMessage.Width = lngWidth;
			fldOwnerName.Width = lngWidth;
			fldRealEstateAccount.Width = lngWidth;
			fldSewerMasterAccount.Width = lngWidth;
			fldWaterMasterAccount.Width = lngWidth;
			fldFinalBill.Width = lngWidth;
			fldNoBill.Width = lngWidth;
			fldMapLot.Width = lngWidth;
			fldPhoneNumber.Width = lngWidth;
			fldUseREAccount.Width = lngWidth;
			fldLocation.Width = lngWidth;
			fldMXUNumber.Width = lngWidth;
			fldReadType.Width = lngWidth;
			fldRemoteNumber.Width = lngWidth;
			fldSerialNumber.Width = lngWidth;
			fldSerialNumber.Width = lngWidth;
			fldSewerTax.Width = lngWidth;
			fldTenantName.Width = lngWidth;
			fldWaterTax.Width = lngWidth;
			fldLatitude.Width = lngWidth;
			fldLongitude.Width = lngWidth;
		}

		private void HideAllFields()
		{
			// This will hide and move all the fields to top = 0
			fldAddress1.Top = 0;
			fldAddress2.Top = 0;
			fldAddress3.Top = 0;
			fldBook.Top = 0;
			fldBillMessage.Top = 0;
			fldBillToSameAsOwner.Top = 0;
			fldDataEntryMessage.Top = 0;
			fldFinalBill.Top = 0;
			fldNoBill.Top = 0;
			fldOwnerName.Top = 0;
			fldRealEstateAccount.Top = 0;
			fldSewerMasterAccount.Top = 0;
			fldWaterMasterAccount.Top = 0;
			fldSewerCategory.Top = 0;
			fldWaterCategory.Top = 0;
			fldMapLot.Top = 0;
			fldPhoneNumber.Top = 0;
			fldUseREAccount.Top = 0;
			fldMXUNumber.Top = 0;
			fldReadType.Top = 0;
			fldRemoteNumber.Top = 0;
			fldSerialNumber.Top = 0;
			fldSerialNumber.Top = 0;
			fldSewerTax.Top = 0;
			fldWaterTax.Top = 0;
			fldLatitude.Top = 0;
			fldLongitude.Top = 0;
			fldAddress1.Visible = false;
			fldAddress2.Visible = false;
			fldAddress3.Visible = false;
			fldBook.Visible = false;
			fldBillMessage.Visible = false;
			fldBillToSameAsOwner.Visible = false;
			fldDataEntryMessage.Visible = false;
			fldFinalBill.Visible = false;
			fldNoBill.Visible = false;
			fldOwnerName.Visible = false;
			fldRealEstateAccount.Visible = false;
			fldSewerMasterAccount.Visible = false;
			fldWaterMasterAccount.Visible = false;
			fldSewerCategory.Visible = false;
			fldWaterCategory.Visible = false;
			fldMapLot.Visible = false;
			fldPhoneNumber.Visible = false;
			fldUseREAccount.Visible = false;
			fldMXUNumber.Visible = false;
			fldReadType.Visible = false;
			fldRemoteNumber.Visible = false;
			fldSerialNumber.Visible = false;
			fldSerialNumber.Visible = false;
			fldSewerTax.Visible = false;
			fldWaterTax.Visible = false;
			fldLatitude.Visible = false;
			fldLongitude.Visible = false;
		}

		private void SetFieldTops()
		{
			float lngTop;
			lngTop = fldTenantName.Top + fldTenantName.Height;
			if (fldAddress1.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldAddress1.Top = lngTop;
				lngTop += fldAddress1.Height;
			}
			if (fldAddress2.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldAddress2.Top = lngTop;
				lngTop += fldAddress2.Height;
			}
			if (fldAddress3.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldAddress3.Top = lngTop;
				lngTop += fldAddress3.Height;
			}
			if (fldBook.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldBook.Top = lngTop;
				lngTop += fldBook.Height;
			}
			if (fldBillMessage.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldBillMessage.Top = lngTop;
				lngTop += fldBillMessage.Height;
			}
			if (fldBillToSameAsOwner.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldBillToSameAsOwner.Top = lngTop;
				lngTop += fldBillToSameAsOwner.Height;
			}
			if (fldDataEntryMessage.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldDataEntryMessage.Top = lngTop;
				lngTop += fldDataEntryMessage.Height;
			}
			if (fldFinalBill.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldFinalBill.Top = lngTop;
				lngTop += fldFinalBill.Height;
			}
			if (fldNoBill.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldNoBill.Top = lngTop;
				lngTop += fldNoBill.Height;
			}
			if (fldOwnerName.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldOwnerName.Top = lngTop;
				lngTop += fldOwnerName.Height;
			}
			if (fldRealEstateAccount.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldRealEstateAccount.Top = lngTop;
				lngTop += fldRealEstateAccount.Height;
			}
			if (fldSewerMasterAccount.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldSewerMasterAccount.Top = lngTop;
				lngTop += fldSewerMasterAccount.Height;
			}
			if (fldWaterMasterAccount.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldWaterMasterAccount.Top = lngTop;
				lngTop += fldWaterMasterAccount.Height;
			}
			if (fldSewerCategory.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldSewerCategory.Top = lngTop;
				lngTop += fldSewerCategory.Height;
			}
			if (fldWaterCategory.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldWaterCategory.Top = lngTop;
				lngTop += fldWaterCategory.Height;
			}
			if (fldMapLot.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldMapLot.Top = lngTop;
				lngTop += fldMapLot.Height;
			}
			if (fldMapLot.Visible != false)
			{
				fldMapLot.Height += 240 / 1440f;
				return;
			}
			if (fldPhoneNumber.Visible == false)
			{
				// do nothing
			}
			else
			{
				fldPhoneNumber.Top = lngTop;
				lngTop += fldPhoneNumber.Height;
			}
			if (fldPhoneNumber.Visible != false)
			{
				fldPhoneNumber.Height += 240 / 1440f;
				return;
			}
			if (fldUseREAccount.Visible == false)
			{
				// Do Nothing
			}
			else
			{
				fldUseREAccount.Top = lngTop;
				lngTop += fldUseREAccount.Height;
			}
			if (fldUseREAccount.Visible != false)
			{
				fldUseREAccount.Height += 240 / 1440f;
				return;
			}
			if (fldWaterCategory.Visible != false)
			{
				fldWaterCategory.Height += 240 / 1440f;
				return;
			}
			if (fldSewerCategory.Visible != false)
			{
				fldSewerCategory.Height += 240 / 1440f;
				return;
			}
			if (fldWaterMasterAccount.Visible != false)
			{
				fldWaterMasterAccount.Height += 240 / 1440f;
				return;
			}
			if (fldSewerMasterAccount.Visible != false)
			{
				fldSewerMasterAccount.Height += 240 / 1440f;
				return;
			}
			if (fldRealEstateAccount.Visible != false)
			{
				fldRealEstateAccount.Height += 240 / 1440f;
				return;
			}
			if (fldOwnerName.Visible != false)
			{
				fldOwnerName.Height += 240 / 1440f;
				return;
			}
			if (fldNoBill.Visible != false)
			{
				fldNoBill.Height += 240 / 1440f;
				return;
			}
			if (fldFinalBill.Visible != false)
			{
				fldFinalBill.Height += 240 / 1440f;
				return;
			}
			if (fldDataEntryMessage.Visible != false)
			{
				fldDataEntryMessage.Height += 240 / 1440f;
				return;
			}
			if (fldBillToSameAsOwner.Visible != false)
			{
				fldBillToSameAsOwner.Height += 240 / 1440f;
				return;
			}
			if (fldBillMessage.Visible != false)
			{
				fldBillMessage.Height += 240 / 1440f;
				return;
			}
			if (fldBook.Visible != false)
			{
				fldBook.Height += 240 / 1440f;
				return;
			}
			if (fldAddress3.Visible != false)
			{
				fldAddress3.Height += 240 / 1440f;
				return;
			}
			if (fldAddress2.Visible != false)
			{
				fldAddress2.Height += 240 / 1440f;
				return;
			}
			if (fldAddress1.Visible != false)
			{
				fldAddress1.Height += 240 / 1440f;
				return;
			}
		}

		private void srptUTBookZeroListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptUTBookZeroListing properties;
			//srptUTBookZeroListing.Caption	= "srptUTBookZeroListing";
			//srptUTBookZeroListing.Left	= 0;
			//srptUTBookZeroListing.Top	= 0;
			//srptUTBookZeroListing.Width	= 18660;
			//srptUTBookZeroListing.Height	= 6645;
			//srptUTBookZeroListing.StartUpPosition	= 3;
			//srptUTBookZeroListing.SectionData	= "srptUTBookZeroListing.dsx":0000;
			//End Unmaped Properties
		}
	}
}
