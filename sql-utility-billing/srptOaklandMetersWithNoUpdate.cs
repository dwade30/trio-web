﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using System;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptOaklandMetersWithNoUpdate.
	/// </summary>
	public partial class srptOaklandMetersWithNoUpdate : FCSectionReport
	{
		public srptOaklandMetersWithNoUpdate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Daily Audit Detail";
		}

		public static srptOaklandMetersWithNoUpdate InstancePtr
		{
			get
			{
				return (srptOaklandMetersWithNoUpdate)Sys.GetInstance(typeof(srptOaklandMetersWithNoUpdate));
			}
		}

		protected srptOaklandMetersWithNoUpdate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptOaklandMetersWithNoUpdate	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/10/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/15/2004              *
		// ********************************************************
		bool blnFirstRecord;
		int lngCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Consumption == -1)
				{
					eArgs.EOF = false;
				}
				else
				{
					//TODO
					//goto MoveNext;
				}
			}
			else
			{
				MoveNext:
				;
				lngCounter += 1;
				if (lngCounter <= Information.UBound(modTSCONSUM.Statics.OaklandReportInfo, 1))
				{
					if (modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Consumption == -1)
					{
						eArgs.EOF = false;
					}
					else
					{
						goto MoveNext;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngCounter = 0;
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldSerialNumber.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].SerialNumber;
			fldName.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].OwnerName;
			fldAccountNumber.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].AccountNumber.ToString();
			fldBook.Text = Strings.Format(modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Book, "0000");
			fldSequence.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Sequence.ToString();
		}

		private void srptOaklandMetersWithNoUpdate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptOaklandMetersWithNoUpdate properties;
			//srptOaklandMetersWithNoUpdate.Caption	= "Daily Audit Detail";
			//srptOaklandMetersWithNoUpdate.Icon	= "srptOaklandMetersWithNoUpdate.dsx":0000";
			//srptOaklandMetersWithNoUpdate.Left	= 0;
			//srptOaklandMetersWithNoUpdate.Top	= 0;
			//srptOaklandMetersWithNoUpdate.Width	= 11880;
			//srptOaklandMetersWithNoUpdate.Height	= 8175;
			//srptOaklandMetersWithNoUpdate.StartUpPosition	= 3;
			//srptOaklandMetersWithNoUpdate.SectionData	= "srptOaklandMetersWithNoUpdate.dsx":058A;
			//End Unmaped Properties
		}
	}
}
