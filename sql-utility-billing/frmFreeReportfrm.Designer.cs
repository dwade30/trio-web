﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmFreeReport.
	/// </summary>
	partial class frmFreeReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSortOrder;
		public fecherFoundation.FCLabel lblSortOrder;
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCLabel lblReport;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblPsuedoFooter;
		public fecherFoundation.FCPanel fraSummary;
		public fecherFoundation.FCGrid vsReturnInfo;
		public FCGrid vsSummary;
		public fecherFoundation.FCLabel lblTitle;
		public fecherFoundation.FCPanel fraVariableSetup;
		public fecherFoundation.FCFrame fraSigFile;
		public fecherFoundation.FCCheckBox chkUseSig;
		public fecherFoundation.FCPictureBox imgSig;
		public fecherFoundation.FCFrame fraRecommitment;
		public fecherFoundation.FCPanel fraRecommitmentInfo;
		public FCGrid vsRecommitment;
		public fecherFoundation.FCCheckBox chkRecommitment;
		public FCGrid vsVariableSetup;
		public fecherFoundation.FCPanel fraEdit;
		public fecherFoundation.FCRichTextBox rtbData;
		public fecherFoundation.FCFrame fraSave;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCListBox lstVariable;
		public fecherFoundation.FCButton cmdAddVariable;
		public fecherFoundation.FCLabel lblPsuedoFooter_1;
		public fecherFoundation.FCLabel lblPsuedoFooter_2;
		public fecherFoundation.FCLabel lblPsuedoFooter_0;
		//FC:FINAL:MSH - Issue #939: replacing label by textBox and changing style of last same as the label style, because label automatically removes white spaces.
		public fecherFoundation.FCTextBox lblPseudoHeader;
		//public fecherFoundation.FCLabel lblPseudoHeader;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintCode;
		public fecherFoundation.FCToolStripMenuItem mnuFileRefresh;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFreeReport));
            this.cmbSortOrder = new fecherFoundation.FCComboBox();
            this.lblSortOrder = new fecherFoundation.FCLabel();
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.lblReport = new fecherFoundation.FCLabel();
            this.fraSummary = new fecherFoundation.FCPanel();
            this.vsReturnInfo = new fecherFoundation.FCGrid();
            this.vsSummary = new fecherFoundation.FCGrid();
            this.lblTitle = new fecherFoundation.FCLabel();
            this.fraVariableSetup = new fecherFoundation.FCPanel();
            this.fraSigFile = new fecherFoundation.FCFrame();
            this.chkUseSig = new fecherFoundation.FCCheckBox();
            this.imgSig = new fecherFoundation.FCPictureBox();
            this.fraRecommitment = new fecherFoundation.FCFrame();
            this.fraRecommitmentInfo = new fecherFoundation.FCPanel();
            this.vsRecommitment = new fecherFoundation.FCGrid();
            this.chkRecommitment = new fecherFoundation.FCCheckBox();
            this.vsVariableSetup = new fecherFoundation.FCGrid();
            this.fraEdit = new fecherFoundation.FCPanel();
            this.rtbData = new fecherFoundation.FCRichTextBox();
            this.fraSave = new fecherFoundation.FCFrame();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.lstVariable = new fecherFoundation.FCListBox();
            this.cmdAddVariable = new fecherFoundation.FCButton();
            this.lblPsuedoFooter_1 = new fecherFoundation.FCLabel();
            this.lblPsuedoFooter_2 = new fecherFoundation.FCLabel();
            this.lblPsuedoFooter_0 = new fecherFoundation.FCLabel();
            this.lblPseudoHeader = new fecherFoundation.FCTextBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrintCode = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileRefresh = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdFilePrintPreview = new fecherFoundation.FCButton();
            this.cmdFileChangeAct = new fecherFoundation.FCButton();
            this.cmdFileExclusion = new fecherFoundation.FCButton();
            this.cmdShowCodes = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).BeginInit();
            this.fraSummary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsReturnInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariableSetup)).BeginInit();
            this.fraVariableSetup.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSigFile)).BeginInit();
            this.fraSigFile.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSig)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecommitment)).BeginInit();
            this.fraRecommitment.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecommitmentInfo)).BeginInit();
            this.fraRecommitmentInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRecommitment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecommitment)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVariableSetup)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEdit)).BeginInit();
            this.fraEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).BeginInit();
            this.fraSave.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddVariable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileChangeAct)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileExclusion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdShowCodes)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFilePrintPreview);
            this.BottomPanel.Location = new System.Drawing.Point(0, 779);
            this.BottomPanel.Size = new System.Drawing.Size(1082, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraEdit);
            this.ClientArea.Controls.Add(this.fraSummary);
            this.ClientArea.Controls.Add(this.fraVariableSetup);
            this.ClientArea.Size = new System.Drawing.Size(1082, 719);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdShowCodes);
            this.TopPanel.Controls.Add(this.cmdFileExclusion);
            this.TopPanel.Controls.Add(this.cmdFileChangeAct);
            this.TopPanel.Size = new System.Drawing.Size(1082, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileChangeAct, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileExclusion, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdShowCodes, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(223, 30);
            this.HeaderText.Text = "Edit Custom Notice";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // cmbSortOrder
            // 
            this.cmbSortOrder.Items.AddRange(new object[] {
            "Account",
            "Name"});
            this.cmbSortOrder.Location = new System.Drawing.Point(183, 474);
            this.cmbSortOrder.Name = "cmbSortOrder";
            this.cmbSortOrder.Size = new System.Drawing.Size(166, 40);
            this.cmbSortOrder.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbSortOrder, null);
            // 
            // lblSortOrder
            // 
            this.lblSortOrder.AutoSize = true;
            this.lblSortOrder.Location = new System.Drawing.Point(30, 490);
            this.lblSortOrder.Name = "lblSortOrder";
            this.lblSortOrder.Size = new System.Drawing.Size(90, 15);
            this.lblSortOrder.TabIndex = 1;
            this.lblSortOrder.Text = "SORT ORDER";
            this.ToolTip1.SetToolTip(this.lblSortOrder, null);
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Edit or Create New Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(86, 30);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(233, 40);
            this.cmbReport.TabIndex = 1;
            this.cmbReport.Text = "Edit or Create New Report";
            this.ToolTip1.SetToolTip(this.cmbReport, null);
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // lblReport
            // 
            this.lblReport.AutoSize = true;
            this.lblReport.Location = new System.Drawing.Point(20, 44);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(59, 15);
            this.lblReport.TabIndex = 3;
            this.lblReport.Text = "REPORT";
            this.ToolTip1.SetToolTip(this.lblReport, null);
            // 
            // fraSummary
            // 
            this.fraSummary.AppearanceKey = "groupBoxNoBorders";
            this.fraSummary.Controls.Add(this.vsReturnInfo);
            this.fraSummary.Controls.Add(this.vsSummary);
            this.fraSummary.Controls.Add(this.lblTitle);
            this.fraSummary.Name = "fraSummary";
            this.fraSummary.Size = new System.Drawing.Size(670, 680);
            this.fraSummary.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.fraSummary, null);
            this.fraSummary.Visible = false;
            // 
            // vsReturnInfo
            // 
            this.vsReturnInfo.Cols = 10;
            this.vsReturnInfo.Location = new System.Drawing.Point(30, 252);
            this.vsReturnInfo.Name = "vsReturnInfo";
            this.vsReturnInfo.Rows = 50;
            this.vsReturnInfo.ShowFocusCell = false;
            this.vsReturnInfo.Size = new System.Drawing.Size(620, 373);
            this.vsReturnInfo.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.vsReturnInfo, null);
            this.vsReturnInfo.Visible = false;
            // 
            // vsSummary
            // 
            this.vsSummary.Cols = 4;
            this.vsSummary.ColumnHeadersVisible = false;
            this.vsSummary.ExtendLastCol = true;
            this.vsSummary.FixedCols = 0;
            this.vsSummary.FixedRows = 0;
            this.vsSummary.Location = new System.Drawing.Point(30, 60);
            this.vsSummary.Name = "vsSummary";
            this.vsSummary.RowHeadersVisible = false;
            this.vsSummary.Rows = 5;
            this.vsSummary.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.vsSummary.ShowFocusCell = false;
            this.vsSummary.Size = new System.Drawing.Size(620, 173);
            this.vsSummary.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.vsSummary, null);
            this.vsSummary.Click += new System.EventHandler(this.vsSummary_ClickEvent);
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(30, 30);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(193, 18);
            this.lblTitle.TabIndex = 3;
            this.lblTitle.Text = "ACCOUNT SUMMARY";
            this.ToolTip1.SetToolTip(this.lblTitle, null);
            // 
            // fraVariableSetup
            // 
            this.fraVariableSetup.AppearanceKey = "groupBoxNoBorders";
            this.fraVariableSetup.Controls.Add(this.fraSigFile);
            this.fraVariableSetup.Controls.Add(this.cmbSortOrder);
            this.fraVariableSetup.Controls.Add(this.lblSortOrder);
            this.fraVariableSetup.Controls.Add(this.fraRecommitment);
            this.fraVariableSetup.Controls.Add(this.vsVariableSetup);
            this.fraVariableSetup.Name = "fraVariableSetup";
            this.fraVariableSetup.Size = new System.Drawing.Size(952, 660);
            this.fraVariableSetup.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.fraVariableSetup, null);
            this.fraVariableSetup.Visible = false;
            // 
            // fraSigFile
            // 
            this.fraSigFile.Controls.Add(this.chkUseSig);
            this.fraSigFile.Controls.Add(this.imgSig);
            this.fraSigFile.Location = new System.Drawing.Point(650, 490);
            this.fraSigFile.Name = "fraSigFile";
            this.fraSigFile.Size = new System.Drawing.Size(278, 139);
            this.fraSigFile.TabIndex = 5;
            this.fraSigFile.Text = "Digital Signature";
            this.ToolTip1.SetToolTip(this.fraSigFile, null);
            // 
            // chkUseSig
            // 
            this.chkUseSig.Location = new System.Drawing.Point(20, 30);
            this.chkUseSig.Name = "chkUseSig";
            this.chkUseSig.Size = new System.Drawing.Size(182, 27);
            this.chkUseSig.Text = "Use Digital Signature";
            this.ToolTip1.SetToolTip(this.chkUseSig, null);
            this.chkUseSig.CheckedChanged += new System.EventHandler(this.chkUseSig_CheckedChanged);
            // 
            // imgSig
            // 
            this.imgSig.BorderStyle = Wisej.Web.BorderStyle.None;
            this.imgSig.Image = ((System.Drawing.Image)(resources.GetObject("imgSig.Image")));
            this.imgSig.Location = new System.Drawing.Point(20, 70);
            this.imgSig.Name = "imgSig";
            this.imgSig.Size = new System.Drawing.Size(242, 52);
            this.imgSig.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.ToolTip1.SetToolTip(this.imgSig, "This may be a distorted view");
            // 
            // fraRecommitment
            // 
            this.fraRecommitment.Controls.Add(this.fraRecommitmentInfo);
            this.fraRecommitment.Controls.Add(this.chkRecommitment);
            this.fraRecommitment.Location = new System.Drawing.Point(30, 506);
            this.fraRecommitment.Name = "fraRecommitment";
            this.fraRecommitment.Size = new System.Drawing.Size(522, 139);
            this.fraRecommitment.TabIndex = 4;
            this.fraRecommitment.Text = "Recommitment Information";
            this.ToolTip1.SetToolTip(this.fraRecommitment, null);
            // 
            // fraRecommitmentInfo
            // 
            this.fraRecommitmentInfo.AppearanceKey = "groupBoxNoBorders";
            this.fraRecommitmentInfo.Controls.Add(this.vsRecommitment);
            this.fraRecommitmentInfo.Location = new System.Drawing.Point(1, 67);
            this.fraRecommitmentInfo.Name = "fraRecommitmentInfo";
            this.fraRecommitmentInfo.Size = new System.Drawing.Size(521, 68);
            this.fraRecommitmentInfo.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.fraRecommitmentInfo, null);
            // 
            // vsRecommitment
            // 
            this.vsRecommitment.Cols = 4;
            this.vsRecommitment.ColumnHeadersVisible = false;
            this.vsRecommitment.FixedRows = 0;
            this.vsRecommitment.Location = new System.Drawing.Point(20, 0);
            this.vsRecommitment.Name = "vsRecommitment";
            this.vsRecommitment.ShowFocusCell = false;
            this.vsRecommitment.Size = new System.Drawing.Size(480, 63);
            this.ToolTip1.SetToolTip(this.vsRecommitment, null);
            this.vsRecommitment.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsRecommitment_AfterEdit);
            this.vsRecommitment.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRecommitment_BeforeEdit);
            this.vsRecommitment.CurrentCellChanged += new System.EventHandler(this.vsRecommitment_RowColChange);
            this.vsRecommitment.Click += new System.EventHandler(this.vsRecommitment_ClickEvent);
            // 
            // chkRecommitment
            // 
            this.chkRecommitment.Location = new System.Drawing.Point(20, 30);
            this.chkRecommitment.Name = "chkRecommitment";
            this.chkRecommitment.Size = new System.Drawing.Size(180, 27);
            this.chkRecommitment.TabIndex = 2;
            this.chkRecommitment.Text = "This a recommitment";
            this.ToolTip1.SetToolTip(this.chkRecommitment, null);
            this.chkRecommitment.CheckedChanged += new System.EventHandler(this.chkRecommitment_CheckedChanged);
            // 
            // vsVariableSetup
            // 
            this.vsVariableSetup.Cols = 4;
            this.vsVariableSetup.ColumnHeadersVisible = false;
            this.vsVariableSetup.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsVariableSetup.ExtendLastCol = true;
            this.vsVariableSetup.FixedRows = 0;
            this.vsVariableSetup.Location = new System.Drawing.Point(30, 30);
            this.vsVariableSetup.Name = "vsVariableSetup";
            this.vsVariableSetup.ReadOnly = false;
            this.vsVariableSetup.Rows = 17;
            this.vsVariableSetup.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.vsVariableSetup.ShowFocusCell = false;
            this.vsVariableSetup.Size = new System.Drawing.Size(899, 440);
            this.vsVariableSetup.StandardTab = false;
            this.vsVariableSetup.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsVariableSetup.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.vsVariableSetup, null);
            this.vsVariableSetup.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsVariableSetup_KeyPressEdit);
            this.vsVariableSetup.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsVariableSetup_AfterEdit);
            this.vsVariableSetup.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsVariableSetup_BeforeEdit);
            this.vsVariableSetup.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsVariableSetup_ValidateEdit);
            this.vsVariableSetup.CurrentCellChanged += new System.EventHandler(this.vsVariableSetup_RowColChange);
            this.vsVariableSetup.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsVariableSetup_MouseMoveEvent);
            this.vsVariableSetup.Enter += new System.EventHandler(this.vsVariableSetup_Enter);
            // 
            // fraEdit
            // 
            this.fraEdit.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraEdit.AppearanceKey = "groupBoxNoBorders";
            this.fraEdit.Controls.Add(this.rtbData);
            this.fraEdit.Controls.Add(this.fraSave);
            this.fraEdit.Controls.Add(this.lstVariable);
            this.fraEdit.Controls.Add(this.cmdAddVariable);
            this.fraEdit.Controls.Add(this.lblPsuedoFooter_1);
            this.fraEdit.Controls.Add(this.lblPsuedoFooter_2);
            this.fraEdit.Controls.Add(this.lblPsuedoFooter_0);
            this.fraEdit.Controls.Add(this.lblPseudoHeader);
            this.fraEdit.Name = "fraEdit";
            this.fraEdit.Size = new System.Drawing.Size(1079, 683);
            this.ToolTip1.SetToolTip(this.fraEdit, null);
            this.fraEdit.Visible = false;
            // 
            // rtbData
            // 
            this.rtbData.Location = new System.Drawing.Point(30, 126);
            this.rtbData.Name = "rtbData";
            this.rtbData.Size = new System.Drawing.Size(1012, 318);
            this.rtbData.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.rtbData, null);
            this.rtbData.KeyDown += new Wisej.Web.KeyEventHandler(this.rtbData_KeyDown);
            // 
            // fraSave
            // 
            this.fraSave.Controls.Add(this.cboSavedReport);
            this.fraSave.Controls.Add(this.cmbReport);
            this.fraSave.Controls.Add(this.lblReport);
            this.fraSave.Controls.Add(this.cmdAdd);
            this.fraSave.Location = new System.Drawing.Point(503, 450);
            this.fraSave.Name = "fraSave";
            this.fraSave.Size = new System.Drawing.Size(339, 210);
            this.fraSave.TabIndex = 7;
            this.fraSave.Text = "Report";
            this.ToolTip1.SetToolTip(this.fraSave, null);
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.Location = new System.Drawing.Point(20, 90);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(299, 40);
            this.cboSavedReport.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cboSavedReport, null);
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.ForeColor = System.Drawing.Color.White;
            this.cmdAdd.Location = new System.Drawing.Point(20, 150);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(299, 40);
            this.cmdAdd.TabIndex = 3;
            this.cmdAdd.Text = "Add Custom report to Library";
            this.ToolTip1.SetToolTip(this.cmdAdd, null);
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // lstVariable
            // 
            this.lstVariable.BackColor = System.Drawing.SystemColors.Window;
            this.lstVariable.Location = new System.Drawing.Point(30, 520);
            this.lstVariable.Name = "lstVariable";
            this.lstVariable.Size = new System.Drawing.Size(449, 139);
            this.lstVariable.Sorted = true;
            this.lstVariable.Sorting = Wisej.Web.SortOrder.Ascending;
            this.lstVariable.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.lstVariable, null);
            this.lstVariable.DoubleClick += new System.EventHandler(this.lstVariable_DoubleClick);
            // 
            // cmdAddVariable
            // 
            this.cmdAddVariable.AppearanceKey = "actionButton";
            this.cmdAddVariable.ForeColor = System.Drawing.Color.White;
            this.cmdAddVariable.Location = new System.Drawing.Point(30, 460);
            this.cmdAddVariable.Name = "cmdAddVariable";
            this.cmdAddVariable.Size = new System.Drawing.Size(113, 40);
            this.cmdAddVariable.TabIndex = 5;
            this.cmdAddVariable.Text = "Add Variable";
            this.ToolTip1.SetToolTip(this.cmdAddVariable, "Select a variable and click this button to insert that field wherever the cursor " +
        "is below");
            this.cmdAddVariable.Visible = false;
            this.cmdAddVariable.Click += new System.EventHandler(this.cmdAddVariable_Click);
            // 
            // lblPsuedoFooter_1
            // 
            this.lblPsuedoFooter_1.Location = new System.Drawing.Point(199, 335);
            this.lblPsuedoFooter_1.Name = "lblPsuedoFooter_1";
            this.lblPsuedoFooter_1.Size = new System.Drawing.Size(100, 100);
            this.lblPsuedoFooter_1.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.lblPsuedoFooter_1, null);
            // 
            // lblPsuedoFooter_2
            // 
            this.lblPsuedoFooter_2.Location = new System.Drawing.Point(323, 335);
            this.lblPsuedoFooter_2.Name = "lblPsuedoFooter_2";
            this.lblPsuedoFooter_2.Size = new System.Drawing.Size(252, 100);
            this.lblPsuedoFooter_2.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.lblPsuedoFooter_2, null);
            // 
            // lblPsuedoFooter_0
            // 
            this.lblPsuedoFooter_0.Location = new System.Drawing.Point(30, 335);
            this.lblPsuedoFooter_0.Name = "lblPsuedoFooter_0";
            this.lblPsuedoFooter_0.Size = new System.Drawing.Size(147, 100);
            this.lblPsuedoFooter_0.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.lblPsuedoFooter_0, null);
            // 
            // lblPseudoHeader
            // 
            this.lblPseudoHeader.Appearance = 0;
            this.lblPseudoHeader.BackColor = System.Drawing.SystemColors.Window;
            this.lblPseudoHeader.BorderStyle = Wisej.Web.BorderStyle.None;
            this.lblPseudoHeader.Cursor = Wisej.Web.Cursors.Default;
            this.lblPseudoHeader.Enabled = false;
            this.lblPseudoHeader.Focusable = false;
            this.lblPseudoHeader.Location = new System.Drawing.Point(30, 0);
            this.lblPseudoHeader.Multiline = true;
            this.lblPseudoHeader.Name = "lblPseudoHeader";
            this.lblPseudoHeader.ScrollBars = Wisej.Web.ScrollBars.None;
            this.lblPseudoHeader.Size = new System.Drawing.Size(1012, 120);
            this.lblPseudoHeader.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.lblPseudoHeader, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrintCode,
            this.mnuFileRefresh,
            this.mnuSeperator,
            this.Seperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFilePrintCode
            // 
            this.mnuFilePrintCode.Index = 0;
            this.mnuFilePrintCode.Name = "mnuFilePrintCode";
            this.mnuFilePrintCode.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuFilePrintCode.Text = "Print Code";
            this.mnuFilePrintCode.Visible = false;
            this.mnuFilePrintCode.Click += new System.EventHandler(this.mnuFilePrintCode_Click);
            // 
            // mnuFileRefresh
            // 
            this.mnuFileRefresh.Index = 1;
            this.mnuFileRefresh.Name = "mnuFileRefresh";
            this.mnuFileRefresh.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuFileRefresh.Text = "Show Codes";
            this.mnuFileRefresh.Visible = false;
            this.mnuFileRefresh.Click += new System.EventHandler(this.mnuFileRefresh_Click);
            // 
            // mnuSeperator
            // 
            this.mnuSeperator.Index = 2;
            this.mnuSeperator.Name = "mnuSeperator";
            this.mnuSeperator.Text = "-";
            // 
            // Seperator
            // 
            this.Seperator.Index = 3;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 4;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdFilePrintPreview
            // 
            this.cmdFilePrintPreview.AppearanceKey = "acceptButton";
            this.cmdFilePrintPreview.Location = new System.Drawing.Point(481, 30);
            this.cmdFilePrintPreview.Name = "cmdFilePrintPreview";
            this.cmdFilePrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFilePrintPreview.Size = new System.Drawing.Size(153, 48);
            this.cmdFilePrintPreview.Text = "Save & Preview";
            this.cmdFilePrintPreview.Click += new System.EventHandler(this.mnuFilePrintPreview_Click);
            // 
            // cmdFileChangeAct
            // 
            this.cmdFileChangeAct.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileChangeAct.Location = new System.Drawing.Point(951, 29);
            this.cmdFileChangeAct.Name = "cmdFileChangeAct";
            this.cmdFileChangeAct.Size = new System.Drawing.Size(91, 24);
            this.cmdFileChangeAct.TabIndex = 1;
            this.cmdFileChangeAct.Text = "Show Edit";
            this.cmdFileChangeAct.Click += new System.EventHandler(this.mnuFileChangeAct_Click);
            // 
            // cmdFileExclusion
            // 
            this.cmdFileExclusion.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileExclusion.Location = new System.Drawing.Point(791, 29);
            this.cmdFileExclusion.Name = "cmdFileExclusion";
            this.cmdFileExclusion.Size = new System.Drawing.Size(155, 24);
            this.cmdFileExclusion.TabIndex = 2;
            this.cmdFileExclusion.Text = "Update Exclusion List";
            this.cmdFileExclusion.Click += new System.EventHandler(this.mnuFileExclusion_Click);
            // 
            // cmdShowCodes
            // 
            this.cmdShowCodes.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdShowCodes.Location = new System.Drawing.Point(695, 29);
            this.cmdShowCodes.Name = "cmdShowCodes";
            this.cmdShowCodes.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdShowCodes.Size = new System.Drawing.Size(90, 24);
            this.cmdShowCodes.TabIndex = 3;
            this.cmdShowCodes.Text = "Show Codes";
            this.cmdShowCodes.Click += new System.EventHandler(this.mnuFileRefresh_Click);
            // 
            // frmFreeReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1082, 887);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmFreeReport";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Edit Custom Notice";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmFreeReport_Load);
            this.Activated += new System.EventHandler(this.frmFreeReport_Activated);
            this.Resize += new System.EventHandler(this.frmFreeReport_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmFreeReport_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmFreeReport_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSummary)).EndInit();
            this.fraSummary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsReturnInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraVariableSetup)).EndInit();
            this.fraVariableSetup.ResumeLayout(false);
            this.fraVariableSetup.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSigFile)).EndInit();
            this.fraSigFile.ResumeLayout(false);
            this.fraSigFile.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseSig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgSig)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecommitment)).EndInit();
            this.fraRecommitment.ResumeLayout(false);
            this.fraRecommitment.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecommitmentInfo)).EndInit();
            this.fraRecommitmentInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsRecommitment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkRecommitment)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsVariableSetup)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraEdit)).EndInit();
            this.fraEdit.ResumeLayout(false);
            this.fraEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rtbData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).EndInit();
            this.fraSave.ResumeLayout(false);
            this.fraSave.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddVariable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileChangeAct)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileExclusion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdShowCodes)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		public FCButton cmdFilePrintPreview;
		public FCButton cmdFileExclusion;
		public FCButton cmdFileChangeAct;
		public FCButton cmdShowCodes;
	}
}
