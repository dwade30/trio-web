﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmLoadBack.
	/// </summary>
	partial class frmLoadBack : BaseForm
	{
		public fecherFoundation.FCFrame fraAccountSearch;
		public fecherFoundation.FCTextBox txtSearchMapLot;
		public fecherFoundation.FCTextBox txtSearchName;
		public fecherFoundation.FCGrid vsAccountSearch;
		public fecherFoundation.FCLabel lblSearchMapLot;
		public fecherFoundation.FCLabel lblSearchName;
		public fecherFoundation.FCFrame fraAccountInfo;
		public fecherFoundation.FCListViewComboBox cboRateKeys;
		public Global.T2KDateBox txtDefaultDate;
		public fecherFoundation.FCLabel lblInterestPTD;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCFrame fraAccounts;
		public fecherFoundation.FCGrid vsAccounts;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileAdd;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator3;
		public fecherFoundation.FCToolStripMenuItem mnuFileNew;
		public fecherFoundation.FCToolStripMenuItem mnuFileDelete;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperatyor;
		public fecherFoundation.FCToolStripMenuItem mnuFileSearch;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLoadBack));
			this.fraAccountSearch = new fecherFoundation.FCFrame();
			this.txtSearchMapLot = new fecherFoundation.FCTextBox();
			this.txtSearchName = new fecherFoundation.FCTextBox();
			this.vsAccountSearch = new fecherFoundation.FCGrid();
			this.lblSearchMapLot = new fecherFoundation.FCLabel();
			this.lblSearchName = new fecherFoundation.FCLabel();
			this.fraAccountInfo = new fecherFoundation.FCFrame();
			this.cboRateKeys = new fecherFoundation.FCListViewComboBox();
			this.txtDefaultDate = new Global.T2KDateBox();
			this.lblInterestPTD = new fecherFoundation.FCLabel();
			this.lblYear = new fecherFoundation.FCLabel();
			this.fraAccounts = new fecherFoundation.FCFrame();
			this.vsAccounts = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileAdd = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator3 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperatyor = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.cmdFileAdd = new fecherFoundation.FCButton();
			this.cmdFileSearch = new fecherFoundation.FCButton();
			this.cmdFileDelete = new fecherFoundation.FCButton();
			this.cmdFileNew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).BeginInit();
			this.fraAccountSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountInfo)).BeginInit();
			this.fraAccountInfo.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccounts)).BeginInit();
			this.fraAccounts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileAdd)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileNew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraAccountSearch);
			this.ClientArea.Controls.Add(this.fraAccountInfo);
			this.ClientArea.Controls.Add(this.fraAccounts);
			this.ClientArea.Size = new System.Drawing.Size(1078, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileNew);
			this.TopPanel.Controls.Add(this.cmdFileDelete);
			this.TopPanel.Controls.Add(this.cmdFileSearch);
			this.TopPanel.Controls.Add(this.cmdFileAdd);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileAdd, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSearch, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(286, 30);
			//FC:FINAL:CHN - issue #1118: remove text.
			// this.HeaderText.Text = "Load of Back Information";
			this.HeaderText.Text = string.Empty;
			// 
			// fraAccountSearch
			// 
			this.fraAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraAccountSearch.Controls.Add(this.txtSearchMapLot);
			this.fraAccountSearch.Controls.Add(this.txtSearchName);
			this.fraAccountSearch.Controls.Add(this.vsAccountSearch);
			this.fraAccountSearch.Controls.Add(this.lblSearchMapLot);
			this.fraAccountSearch.Controls.Add(this.lblSearchName);
			this.fraAccountSearch.Location = new System.Drawing.Point(30, 140);
			this.fraAccountSearch.Name = "fraAccountSearch";
			this.fraAccountSearch.Size = new System.Drawing.Size(1018, 294);
			this.fraAccountSearch.TabIndex = 2;
			this.fraAccountSearch.Text = "Account Search";
			this.fraAccountSearch.Visible = false;
			// 
			// txtSearchMapLot
			// 
			this.txtSearchMapLot.AutoSize = false;
			this.txtSearchMapLot.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearchMapLot.LinkItem = null;
			this.txtSearchMapLot.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSearchMapLot.LinkTopic = null;
			this.txtSearchMapLot.Location = new System.Drawing.Point(478, 30);
			this.txtSearchMapLot.Name = "txtSearchMapLot";
			this.txtSearchMapLot.Size = new System.Drawing.Size(122, 40);
			this.txtSearchMapLot.TabIndex = 3;
			// 
			// txtSearchName
			// 
			this.txtSearchName.AutoSize = false;
			this.txtSearchName.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearchName.LinkItem = null;
			this.txtSearchName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSearchName.LinkTopic = null;
			this.txtSearchName.Location = new System.Drawing.Point(125, 30);
			this.txtSearchName.Name = "txtSearchName";
			this.txtSearchName.Size = new System.Drawing.Size(170, 40);
			this.txtSearchName.TabIndex = 1;
			this.txtSearchName.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearchName_KeyDown);
			// 
			// vsAccountSearch
			// 
			this.vsAccountSearch.AllowSelection = false;
			this.vsAccountSearch.AllowUserToResizeColumns = false;
			this.vsAccountSearch.AllowUserToResizeRows = false;
			this.vsAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsAccountSearch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAccountSearch.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorSel = System.Drawing.Color.Empty;
			this.vsAccountSearch.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAccountSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsAccountSearch.ColumnHeadersHeight = 30;
			this.vsAccountSearch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAccountSearch.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsAccountSearch.DragIcon = null;
			this.vsAccountSearch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAccountSearch.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAccountSearch.FrozenCols = 0;
			this.vsAccountSearch.GridColor = System.Drawing.Color.Empty;
			this.vsAccountSearch.GridColorFixed = System.Drawing.Color.Empty;
			this.vsAccountSearch.Location = new System.Drawing.Point(20, 90);
			this.vsAccountSearch.Name = "vsAccountSearch";
			this.vsAccountSearch.ReadOnly = true;
			this.vsAccountSearch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAccountSearch.RowHeightMin = 0;
			this.vsAccountSearch.Rows = 1;
			this.vsAccountSearch.ScrollTipText = null;
			this.vsAccountSearch.ShowColumnVisibilityMenu = false;
			this.vsAccountSearch.Size = new System.Drawing.Size(973, 184);
			this.vsAccountSearch.StandardTab = true;
			this.vsAccountSearch.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsAccountSearch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsAccountSearch.TabIndex = 4;
			this.vsAccountSearch.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsAccountSearch_KeyPressEvent);
			this.vsAccountSearch.DoubleClick += new System.EventHandler(this.vsAccountSearch_DblClick);
			// 
			// lblSearchMapLot
			// 
			this.lblSearchMapLot.Location = new System.Drawing.Point(356, 44);
			this.lblSearchMapLot.Name = "lblSearchMapLot";
			this.lblSearchMapLot.Size = new System.Drawing.Size(58, 22);
			this.lblSearchMapLot.TabIndex = 2;
			this.lblSearchMapLot.Text = "MAP LOT";
			// 
			// lblSearchName
			// 
			this.lblSearchName.Location = new System.Drawing.Point(20, 44);
			this.lblSearchName.Name = "lblSearchName";
			this.lblSearchName.Size = new System.Drawing.Size(54, 22);
			this.lblSearchName.TabIndex = 0;
			this.lblSearchName.Text = "NAME";
			// 
			// fraAccountInfo
			// 
			this.fraAccountInfo.Controls.Add(this.cboRateKeys);
			this.fraAccountInfo.Controls.Add(this.txtDefaultDate);
			this.fraAccountInfo.Controls.Add(this.lblInterestPTD);
			this.fraAccountInfo.Controls.Add(this.lblYear);
			this.fraAccountInfo.Location = new System.Drawing.Point(30, 30);
			this.fraAccountInfo.Name = "fraAccountInfo";
			this.fraAccountInfo.Size = new System.Drawing.Size(1018, 93);
			this.fraAccountInfo.TabIndex = 0;
			this.fraAccountInfo.Text = "Default Information";
			// 
			// cboRateKeys
			// 
			this.cboRateKeys.AutoSize = false;
			this.cboRateKeys.BackColor = System.Drawing.SystemColors.Window;
			this.cboRateKeys.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboRateKeys.FormattingEnabled = true;
			this.cboRateKeys.Location = new System.Drawing.Point(128, 30);
			this.cboRateKeys.Name = "cboRateKeys";
			this.cboRateKeys.Size = new System.Drawing.Size(416, 40);
			this.cboRateKeys.TabIndex = 1;
			this.cboRateKeys.SelectedIndexChanged += new System.EventHandler(this.cboRateKeys_SelectedIndexChanged);
			// 
			// txtDefaultDate
			// 
			this.txtDefaultDate.Location = new System.Drawing.Point(870, 30);
			this.txtDefaultDate.Mask = "##/##/####";
			this.txtDefaultDate.MaxLength = 10;
			this.txtDefaultDate.Name = "txtDefaultDate";
			this.txtDefaultDate.Size = new System.Drawing.Size(115, 40);
			this.txtDefaultDate.TabIndex = 3;
			this.txtDefaultDate.Text = "  /  /";
			this.txtDefaultDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtDefaultDate_Validate);
			// 
			// lblInterestPTD
			// 
			this.lblInterestPTD.Location = new System.Drawing.Point(604, 44);
			this.lblInterestPTD.Name = "lblInterestPTD";
			this.lblInterestPTD.Size = new System.Drawing.Size(256, 20);
			this.lblInterestPTD.TabIndex = 2;
			this.lblInterestPTD.Text = "DEFAULT INTEREST PAID THROUGH DATE";
			// 
			// lblYear
			// 
			this.lblYear.Location = new System.Drawing.Point(20, 44);
			this.lblYear.Name = "lblYear";
			this.lblYear.Size = new System.Drawing.Size(104, 13);
			this.lblYear.TabIndex = 0;
			this.lblYear.Text = "RATE RECORD";
			// 
			// fraAccounts
			// 
			this.fraAccounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraAccounts.Controls.Add(this.vsAccounts);
			this.fraAccounts.Enabled = false;
			this.fraAccounts.Location = new System.Drawing.Point(30, 140);
			this.fraAccounts.Name = "fraAccounts";
			this.fraAccounts.Size = new System.Drawing.Size(1018, 294);
			this.fraAccounts.TabIndex = 1;
			this.fraAccounts.Text = "Add Accounts";
			// 
			// vsAccounts
			// 
			this.vsAccounts.AllowSelection = false;
			this.vsAccounts.AllowUserToResizeColumns = false;
			this.vsAccounts.AllowUserToResizeRows = false;
			this.vsAccounts.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsAccounts.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAccounts.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.BackColorSel = System.Drawing.Color.Empty;
			this.vsAccounts.Cols = 3;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAccounts.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsAccounts.ColumnHeadersHeight = 30;
			this.vsAccounts.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAccounts.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsAccounts.DragIcon = null;
			this.vsAccounts.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAccounts.FixedCols = 0;
			this.vsAccounts.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.FrozenCols = 0;
			this.vsAccounts.GridColor = System.Drawing.Color.Empty;
			this.vsAccounts.GridColorFixed = System.Drawing.Color.Empty;
			this.vsAccounts.Location = new System.Drawing.Point(20, 30);
			this.vsAccounts.Name = "vsAccounts";
			this.vsAccounts.ReadOnly = true;
			this.vsAccounts.RowHeadersVisible = false;
			this.vsAccounts.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAccounts.RowHeightMin = 0;
			this.vsAccounts.Rows = 1;
			this.vsAccounts.ScrollTipText = null;
			this.vsAccounts.ShowColumnVisibilityMenu = false;
			this.vsAccounts.Size = new System.Drawing.Size(970, 235);
			this.vsAccounts.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsAccounts.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsAccounts.TabIndex = 0;
			this.vsAccounts.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsAccounts_KeyPressEdit);
			this.vsAccounts.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsAccounts_BeforeEdit);
			this.vsAccounts.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsAccounts_ValidateEdit);
			this.vsAccounts.CurrentCellChanged += new System.EventHandler(this.vsAccounts_RowColChange);
			this.vsAccounts.CellFormatting += new DataGridViewCellFormattingEventHandler(this.vsAccounts_MouseMoveEvent);
			this.vsAccounts.KeyDown += new Wisej.Web.KeyEventHandler(this.vsAccounts_KeyDownEvent);
			this.vsAccounts.KeyPress += new Wisej.Web.KeyPressEventHandler(this.vsAccounts_KeyPressEvent);
			this.vsAccounts.Enter += new System.EventHandler(this.vsAccounts_Enter);
			this.vsAccounts.DoubleClick += new System.EventHandler(this.vsAccounts_DblClick);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileAdd,
				this.mnuFileSeperator3,
				this.mnuFileNew,
				this.mnuFileDelete,
				this.mnuFileSeperatyor,
				this.mnuFileSearch,
				this.mnuFileSeperator2,
				this.mnuFileSave,
				this.mnuFileSaveExit,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileAdd
			// 
			this.mnuFileAdd.Index = 0;
			this.mnuFileAdd.Name = "mnuFileAdd";
			this.mnuFileAdd.Shortcut = Wisej.Web.Shortcut.CtrlA;
			this.mnuFileAdd.Text = "Add Rate Record";
			this.mnuFileAdd.Click += new System.EventHandler(this.mnuFileAdd_Click);
			// 
			// mnuFileSeperator3
			// 
			this.mnuFileSeperator3.Index = 1;
			this.mnuFileSeperator3.Name = "mnuFileSeperator3";
			this.mnuFileSeperator3.Text = "-";
			// 
			// mnuFileNew
			// 
			this.mnuFileNew.Index = 2;
			this.mnuFileNew.Name = "mnuFileNew";
			this.mnuFileNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.mnuFileNew.Text = "New";
			this.mnuFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
			// 
			// mnuFileDelete
			// 
			this.mnuFileDelete.Index = 3;
			this.mnuFileDelete.Name = "mnuFileDelete";
			this.mnuFileDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
			this.mnuFileDelete.Text = "Delete";
			this.mnuFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// mnuFileSeperatyor
			// 
			this.mnuFileSeperatyor.Index = 4;
			this.mnuFileSeperatyor.Name = "mnuFileSeperatyor";
			this.mnuFileSeperatyor.Text = "-";
			// 
			// mnuFileSearch
			// 
			this.mnuFileSearch.Index = 5;
			this.mnuFileSearch.Name = "mnuFileSearch";
			this.mnuFileSearch.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuFileSearch.Text = "Search";
			this.mnuFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 6;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 7;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 8;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Exit";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 9;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 10;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "acceptButton";
			this.cmdFileSave.Location = new System.Drawing.Point(379, 36);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSave.Size = new System.Drawing.Size(100, 48);
			this.cmdFileSave.TabIndex = 0;
			this.cmdFileSave.Text = "Save";
			this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// cmdFileAdd
			// 
			this.cmdFileAdd.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileAdd.AppearanceKey = "toolbarButton";
			this.cmdFileAdd.Location = new System.Drawing.Point(908, 29);
			this.cmdFileAdd.Name = "cmdFileAdd";
			this.cmdFileAdd.Shortcut = Wisej.Web.Shortcut.CtrlA;
			this.cmdFileAdd.Size = new System.Drawing.Size(130, 24);
			this.cmdFileAdd.TabIndex = 1;
			this.cmdFileAdd.Text = "Add Rate Record";
			this.cmdFileAdd.Click += new System.EventHandler(this.mnuFileAdd_Click);
			// 
			// cmdFileSearch
			// 
			this.cmdFileSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSearch.AppearanceKey = "toolbarButton";
            this.cmdFileSearch.ImageSource = "button-search";
            this.cmdFileSearch.Location = new System.Drawing.Point(831, 29);
			this.cmdFileSearch.Name = "cmdFileSearch";
			this.cmdFileSearch.Shortcut = Wisej.Web.Shortcut.F5;
			this.cmdFileSearch.Size = new System.Drawing.Size(81, 24);
			this.cmdFileSearch.TabIndex = 2;
			this.cmdFileSearch.Text = "Search";
			this.cmdFileSearch.Click += new System.EventHandler(this.mnuFileSearch_Click);
			// 
			// cmdFileDelete
			// 
			this.cmdFileDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileDelete.AppearanceKey = "toolbarButton";
			this.cmdFileDelete.Location = new System.Drawing.Point(759, 29);
			this.cmdFileDelete.Name = "cmdFileDelete";
			this.cmdFileDelete.Shortcut = Wisej.Web.Shortcut.CtrlDelete;
			this.cmdFileDelete.Size = new System.Drawing.Size(67, 24);
			this.cmdFileDelete.TabIndex = 3;
			this.cmdFileDelete.Text = "Delete";
			this.cmdFileDelete.Click += new System.EventHandler(this.mnuFileDelete_Click);
			// 
			// cmdFileNew
			// 
			this.cmdFileNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileNew.AppearanceKey = "toolbarButton";
			this.cmdFileNew.Location = new System.Drawing.Point(702, 29);
			this.cmdFileNew.Name = "cmdFileNew";
			this.cmdFileNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
			this.cmdFileNew.Size = new System.Drawing.Size(52, 24);
			this.cmdFileNew.TabIndex = 4;
			this.cmdFileNew.Text = "New";
			this.cmdFileNew.Click += new System.EventHandler(this.mnuFileNew_Click);
			// 
			// frmLoadBack
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLoadBack";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Load of Back Information";
			this.Load += new System.EventHandler(this.frmLoadBack_Load);
			this.Activated += new System.EventHandler(this.frmLoadBack_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmLoadBack_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLoadBack_KeyPress);
			this.Resize += new System.EventHandler(this.frmLoadBack_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).EndInit();
			this.fraAccountSearch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountInfo)).EndInit();
			this.fraAccountInfo.ResumeLayout(false);
			this.fraAccountInfo.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDefaultDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccounts)).EndInit();
			this.fraAccounts.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAccounts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileAdd)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileNew)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSave;
		private FCButton cmdFileAdd;
		private FCButton cmdFileNew;
		private FCButton cmdFileDelete;
		private FCButton cmdFileSearch;
	}
}
