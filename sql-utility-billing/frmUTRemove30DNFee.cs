﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmRemove30DNFee.
	/// </summary>
	public partial class frmRemove30DNFee : BaseForm
	{
		public frmRemove30DNFee()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRemove30DNFee InstancePtr
		{
			get
			{
				return (frmRemove30DNFee)Sys.GetInstance(typeof(frmRemove30DNFee));
			}
		}

		protected frmRemove30DNFee _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/24/2006              *
		// ********************************************************
		int lngRE;
		int lngPP;
		int lngBL;
		DateTime dtDate;
		int lngAcct;
		string strWS = "";

		private void cmbYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						// return
						mnuFilePrint_Click();
						break;
					}
			}
			//end switch
		}

		private void frmRemove30DNFee_Activated(object sender, System.EventArgs e)
		{
			lblDate.Text = "Select the account to remove the 30 Day Notice fees from.";
			ShowFrame(ref fraDate);
		}

		private void frmRemove30DNFee_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRemove30DNFee properties;
			//frmRemove30DNFee.FillStyle	= 0;
			//frmRemove30DNFee.ScaleWidth	= 3885;
			//frmRemove30DNFee.ScaleHeight	= 2340;
			//frmRemove30DNFee.LinkTopic	= "Form2";
			//frmRemove30DNFee.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Remove 30 Day Notice Fee";
			if (modUTStatusPayments.Statics.TownService == "W")
			{
				cmbWS.Clear();
				cmbWS.Items.Add("Water");
				cmbWS.Text = "Water";
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				cmbWS.Clear();
				cmbWS.Items.Add("Sewer");
				cmbWS.Text = "Sewer";
			}
			else if (modUTStatusPayments.Statics.TownService == "B")
			{
				if (modUTStatusPayments.Statics.gboolPayWaterFirst)
				{
					cmbWS.Text = "Water";
				}
				else
				{
					cmbWS.Text = "Sewer";
				}
			}
		}

		private void frmRemove30DNFee_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowFrame(ref FCFrame fra)
		{
			string vbPorterVar = fra.GetName();
			//if (vbPorterVar == "fraDate")
			//{
			//    fraDate.Top = 100; // (Me.Height - fraDate.Height) / 3
			//}
			//else
			//{
			//}
			//fra.Left = FCConvert.ToInt32((this.Width - fra.Width) / 2.0);
			fra.Visible = true;
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			FilePrint();
		}

		public void mnuFilePrint_Click()
		{
			mnuFilePrint_Click(mnuFilePrint, new System.EventArgs());
		}

		private void txtAccount_TextChanged(object sender, System.EventArgs e)
		{
			// turn this off so that the user will have to select the year each time
			// and I will have time to fill it for each account selected
			cmbYear.SelectedIndex = -1;
			// cmbYear.Enabled = False
		}

		private void FillYearCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsYear = new clsDRWrapper();
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
				cmbYear.Clear();
				if (lngAcct > 0)
				{
					rsYear.OpenRecordset("SELECT BillingRateKey, BillDate FROM (SELECT AccountNumber, " + strWS + "CostAdded, " + strWS + "LienRecordNumber, BillingRateKey FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID) AS BillMast INNER JOIN RateKeys ON BillMast.BillingRateKey = RateKeys.ID WHERE AccountNumber = " + FCConvert.ToString(lngAcct) + " AND " + strWS + "CostAdded <> 0 AND " + strWS + "LienRecordNumber = 0 ORDER BY BillingRateKey desc", modExtraModules.strUTDatabase);
					if (!rsYear.EndOfFile())
					{
						while (!rsYear.EndOfFile())
						{
							//FC:FINAL:DDU:#i1296 - removed time from RateKey
							if (Information.IsDate(rsYear.Get_Fields("BillDate")))
							{
								DateTime temp = Convert.ToDateTime(rsYear.Get_Fields_DateTime("BillDate"));
								cmbYear.AddItem(Strings.Format(rsYear.Get_Fields_Int32("BillingRateKey"), "00000") + "  " + Strings.Format(temp.Date, "MM/dd/yyyy"));
							}
							else
							{
								cmbYear.AddItem(Strings.Format(rsYear.Get_Fields_Int32("BillingRateKey"), "00000") + "  " + FCConvert.ToString(rsYear.Get_Fields_DateTime("BillDate")));
								// get this from the ratekey
							}
							cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsYear.Get_Fields_Int32("BillingRateKey")));
							// put the rate key into the itemdata to use later
							rsYear.MoveNext();
						}
					}
					else
					{
						MessageBox.Show("This account does not have any 30 Day Notice Fees applied.", "Valid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						// cmbYear.Enabled = False
						if (txtAccount.Enabled)
						{
							txtAccount.Focus();
						}
					}
				}
				else
				{
					MessageBox.Show("Please enter a valid account.", "Valid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					// cmbYear.Enabled = False
					if (txtAccount.Enabled)
					{
						txtAccount.Focus();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			// Select Case KeyCode
			// Case 13 'return
			// mnuFilePrint_Click
			// End Select
		}

		private void txtAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			FilePrint_2(true);
		}

		private void FilePrint_2(bool boolFromValidate)
		{
			FilePrint(boolFromValidate);
		}

		private void FilePrint(bool boolFromValidate = false)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsPay = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				int lngEl = 0;
				int lngSt = 0;
				int lngDemandGroup = 0;
				int lngAcctKey = 0;
				if (cmbWS.Text == "Water")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				// save routine
				if (Conversion.Val(txtAccount.Text) != 0)
				{
					intErr = 1;
					if (cmbYear.SelectedIndex != -1)
					{
						intErr = 2;
						if (MessageBox.Show("Are you sure that you would want to remove the demand fees from this account?", "Remove 30 Day Notice Fees", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) != DialogResult.Yes)
						{
							return;
						}
						// CYA entry
						modGlobalFunctions.AddCYAEntry_80("UT", "Removing 30 DN fees.", "Account: " + FCConvert.ToString(lngAcct), "RateKey: " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)));
						intErr = 3;
						// remove the fees
						// find the billing record
						rsBill.OpenRecordset("SELECT Bill.ID, " + strWS + "LienStatusEligibility, " + strWS + "LienProcessStatus, " + strWS + "DemandGroupID, AccountKey FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE AccountNumber = " + FCConvert.ToString(lngAcct) + " AND BillingRateKey = " + FCConvert.ToString(cmbYear.ItemData(cmbYear.SelectedIndex)), modExtraModules.strUTDatabase);
						if (!rsBill.EndOfFile())
						{
							// this will find the fee from the payment rec to delete
							intErr = 4;
							rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsBill.Get_Fields_Int32("ID") + " AND Code = '3' AND Service = '" + strWS + "'", modExtraModules.strUTDatabase);
							if (!rsPay.EndOfFile())
							{
								intErr = 5;
								modGlobalFunctions.AddCYAEntry_728("UT", "Removing 30 DN Record.", "EID: " + rsPay.Get_Fields_DateTime("EffectiveInterestDate"), "RTD: " + rsPay.Get_Fields_DateTime("RecordedTransactionDate"), Strings.Format(rsPay.Get_Fields_Decimal("LienCost"), "$#,##0.00"), "Bill Key = " + rsPay.Get_Fields_Int32("BillKey"));
								// found ... delete it
								// rsPay.Delete
								rsPay.Execute("DELETE FROM PaymentRec WHERE ID = " + rsPay.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
							}
							else
							{
								// not found...
								MessageBox.Show("Payment for this was not found and not deleted.", "Payment Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							intErr = 6;
							lngEl = FCConvert.ToInt32(rsBill.Get_Fields(strWS + "LienStatusEligibility"));
							lngSt = FCConvert.ToInt32(rsBill.Get_Fields(strWS + "LienProcessStatus"));
							lngDemandGroup = FCConvert.ToInt32(rsBill.Get_Fields(strWS + "DemandGroupID"));
							lngAcctKey = FCConvert.ToInt32(rsBill.Get_Fields_Int32("AccountKey"));
							// set the eligibility back to be able to run the notices/apply fees again if needed
							// rsBill.Edit
							// rsBill.Fields(strWS & "LienStatusEligibility") = 1
							// rsBill.Fields(strWS & "LienProcessStatus") = 1
							// rsBill.Fields(strWS & "CostAdded") = 0
							// rsBill.Set_Fields(strWS & "DemandGroupID", 0              'MAL@20071217: Add update to Demand Group ); Tracker Reference 11664 ; 'MAL@20080303: Change to break out by service ; Tracker Reference: 12580
							// rsBill.Update True
							rsBill.Execute("UPDATE Bill SET " + strWS + "LienStatusEligibility = 1, " + strWS + "LienProcessStatus = 1, " + strWS + "CostAdded = 0, " + strWS + "DemandGroupID = 0 WHERE ID = " + rsBill.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
							// check for secondary bills that are the same status and need to be reset as well
							// MAL@20070911: Corrected SQL statement to limit to current account ; it was changing all account with the status and eligibility
							// Call Reference: 116833
							// rsBill.OpenRecordset "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.Key WHERE AccountNumber = " & lngAcct & " AND " & strWS & "CostAdded = 0 AND " & strWS & "LienStatusEligibility = " & lngEl & " OR " & strWS & "LienProcessStatus = " & lngSt, strUTDatabase
							// rsBill.OpenRecordset "SELECT * FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID WHERE AccountNumber = " & lngAcct & " AND " & strWS & "CostAdded = 0 AND (" & strWS & "LienStatusEligibility = " & lngEl & " OR " & strWS & "LienProcessStatus = " & lngSt & ")", strUTDatabase
							rsBill.OpenRecordset("SELECT ID, " + strWS + "DemandGroupID FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND " + strWS + "CostAdded = 0 AND (" + strWS + "LienStatusEligibility = " + FCConvert.ToString(lngEl) + " OR " + strWS + "LienProcessStatus = " + FCConvert.ToString(lngSt) + ")", modExtraModules.strUTDatabase);
							while (!rsBill.EndOfFile())
							{
								// MAL@20080715: Changed to only change the status of those bills that are in the same group
								// Tracker Reference: 14570
								if (FCConvert.ToInt32(rsBill.Get_Fields(strWS + "DemandGroupID")) == lngDemandGroup)
								{
									// rsBill.Edit
									// rsBill.Fields(strWS & "LienStatusEligibility") = 1
									// rsBill.Fields(strWS & "LienProcessStatus") = 1
									// rsBill.Fields(strWS & "CostAdded") = 0
									// rsBill.Fields(strWS & "DemandGroupID") = 0          'MAL@20080715: Corrected to update the Demand Group ID as well
									// rsBill.Update True
									rsBill.Execute("UPDATE Bill SET " + strWS + "LienStatusEligibility = 1, " + strWS + "LienProcessStatus = 1, " + strWS + "CostAdded = 0, " + strWS + "DemandGroupID = 0 WHERE ID = " + rsBill.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
								}
								rsBill.MoveNext();
							}
							MessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " from " + cmbYear.Items[cmbYear.SelectedIndex].ToString() + " has been updated.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							this.Unload();
						}
						else
						{
							intErr = 7;
							MessageBox.Show("Error finding bill record, please try again.", "Missing Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							// cmbYear.Enabled = False
							if (txtAccount.Enabled)
							{
								txtAccount.Focus();
							}
						}
					}
					else
					{
						// If cmbYear.Enabled Then
						// MsgBox "Please select a year.", vbCritical, "Data Input"
						// End If
						intErr = 8;
						// fill the grid
						FillYearCombo();
						intErr = 9;
						if (cmbYear.Items.Count > 0)
						{
							intErr = 10;
							// cmbYear.Enabled = True
							cmbYear.SelectedIndex = 0;
							cmbYear.Focus();
						}
						else
						{
							intErr = 11;
							// cmbYear.Enabled = False
							if (txtAccount.Enabled)
							{
								txtAccount.Focus();
							}
						}
					}
				}
				else
				{
					if (!boolFromValidate)
					{
						// enter an account
						MessageBox.Show("Please enter a valid account.", "Valid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Removing 30 DN Fees - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
