//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmRateRecChoice.
	/// </summary>
	partial class frmRateRecChoice : BaseForm
	{
		public fecherFoundation.FCComboBox cmbFilterContains;
		public fecherFoundation.FCLabel lblFilterContains;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbReminderLien;
		public fecherFoundation.FCLabel lblReminderLien;
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public fecherFoundation.FCComboBox cmbPrint;
		public fecherFoundation.FCLabel lblPrint;
		public fecherFoundation.FCComboBox cmbLien;
		public fecherFoundation.FCComboBox cmbPeriod;
		public fecherFoundation.FCLabel lblPeriod;
		public System.Collections.Generic.List<T2KDateBox> txtBillingDate;
		public System.Collections.Generic.List<T2KDateBox> txtDueDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblStartDate;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblEndDate;
		public System.Collections.Generic.List<fecherFoundation.FCTextBox> txtRange;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkPrint;
		public fecherFoundation.FCFrame fraRateInfo;
		public FCGrid vsRateInfo;
		public fecherFoundation.FCButton cmdRIClose;
		public fecherFoundation.FCFrame fraFilter;
		public fecherFoundation.FCButton cmdCancelFilter;
		public fecherFoundation.FCTextBox txtFilterEnd;
		public fecherFoundation.FCTextBox txtFilterStart;
		public fecherFoundation.FCButton cmdClearFilter;
		public fecherFoundation.FCButton cmdApplyFilter;
		public Global.T2KDateBox txtFilterStartDate;
		public Global.T2KDateBox txtFilterEndDate;
		public fecherFoundation.FCLabel lblFilterBy;
		public fecherFoundation.FCLabel lblFilterStart;
		public fecherFoundation.FCLabel lblFilterEnd;
		public fecherFoundation.FCFrame fraDateRange;
		public Global.T2KDateBox txtBillingDate_0;
		public Global.T2KDateBox txtBillingDate_1;
		public Global.T2KDateBox txtDueDate_0;
		public Global.T2KDateBox txtDueDate_1;
		public fecherFoundation.FCLabel lblStartDate_1;
		public fecherFoundation.FCLabel lblEndDate_1;
		public fecherFoundation.FCLabel lblEndDate_0;
		public fecherFoundation.FCLabel lblStartDate_0;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCTextBox txtMinimumAmount;
		public fecherFoundation.FCFrame fraTextRange;
		public fecherFoundation.FCTextBox txtRange_1;
		public fecherFoundation.FCTextBox txtRange_0;
		public fecherFoundation.FCLabel lblMinAmount;
		public fecherFoundation.FCFrame fraPrintOptions;
		public fecherFoundation.FCComboBox cmbExtraLines;
		public fecherFoundation.FCCheckBox chkPrint_3;
		public fecherFoundation.FCCheckBox chkPrint_2;
		public fecherFoundation.FCCheckBox chkPrint_1;
		public fecherFoundation.FCCheckBox chkPrint_0;
		public fecherFoundation.FCLabel lblExtraLines;
		public fecherFoundation.FCFrame fraRate;
		public FCGrid vsRate;
		public fecherFoundation.FCFrame fraQuestions;
		public fecherFoundation.FCLabel lblEligible;
		public fecherFoundation.FCFrame fraPreLienEdit;
		public fecherFoundation.FCComboBox cmbReportDetail;
		public Global.T2KDateBox txtMailDate;
		public fecherFoundation.FCLabel lblReportLen;
		public fecherFoundation.FCLabel lblMailDate;
		public fecherFoundation.FCLabel lblInstructions;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelectAll;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileAddRateKey;
		public fecherFoundation.FCToolStripMenuItem mnuDateRanges;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            Wisej.Web.JavaScript.ClientEvent clientEvent1 = new Wisej.Web.JavaScript.ClientEvent();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRateRecChoice));
            this.javaScriptIntegerNumbersOnly = new Wisej.Web.JavaScript(this.components);
            this.txtRange_1 = new fecherFoundation.FCTextBox();
            this.txtRange_0 = new fecherFoundation.FCTextBox();
            this.cmbFilterContains = new fecherFoundation.FCComboBox();
            this.lblFilterContains = new fecherFoundation.FCLabel();
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.cmbReminderLien = new fecherFoundation.FCComboBox();
            this.lblReminderLien = new fecherFoundation.FCLabel();
            this.cmbWS = new fecherFoundation.FCComboBox();
            this.lblWS = new fecherFoundation.FCLabel();
            this.cmbPrint = new fecherFoundation.FCComboBox();
            this.lblPrint = new fecherFoundation.FCLabel();
            this.cmbLien = new fecherFoundation.FCComboBox();
            this.cmbPeriod = new fecherFoundation.FCComboBox();
            this.lblPeriod = new fecherFoundation.FCLabel();
            this.fraRateInfo = new fecherFoundation.FCFrame();
            this.vsRateInfo = new fecherFoundation.FCGrid();
            this.cmdRIClose = new fecherFoundation.FCButton();
            this.fraFilter = new fecherFoundation.FCFrame();
            this.cmdCancelFilter = new fecherFoundation.FCButton();
            this.txtFilterEnd = new fecherFoundation.FCTextBox();
            this.txtFilterStart = new fecherFoundation.FCTextBox();
            this.cmdClearFilter = new fecherFoundation.FCButton();
            this.cmdApplyFilter = new fecherFoundation.FCButton();
            this.txtFilterStartDate = new Global.T2KDateBox();
            this.txtFilterEndDate = new Global.T2KDateBox();
            this.lblFilterBy = new fecherFoundation.FCLabel();
            this.lblFilterStart = new fecherFoundation.FCLabel();
            this.lblFilterEnd = new fecherFoundation.FCLabel();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.txtBillingDate_0 = new Global.T2KDateBox();
            this.txtBillingDate_1 = new Global.T2KDateBox();
            this.txtDueDate_0 = new Global.T2KDateBox();
            this.txtDueDate_1 = new Global.T2KDateBox();
            this.lblStartDate_1 = new fecherFoundation.FCLabel();
            this.lblEndDate_1 = new fecherFoundation.FCLabel();
            this.lblEndDate_0 = new fecherFoundation.FCLabel();
            this.lblStartDate_0 = new fecherFoundation.FCLabel();
            this.fraRange = new fecherFoundation.FCFrame();
            this.txtMinimumAmount = new fecherFoundation.FCTextBox();
            this.fraTextRange = new fecherFoundation.FCFrame();
            this.lblMinAmount = new fecherFoundation.FCLabel();
            this.fraPrintOptions = new fecherFoundation.FCFrame();
            this.cmbExtraLines = new fecherFoundation.FCComboBox();
            this.chkPrint_3 = new fecherFoundation.FCCheckBox();
            this.chkPrint_2 = new fecherFoundation.FCCheckBox();
            this.chkPrint_1 = new fecherFoundation.FCCheckBox();
            this.chkPrint_0 = new fecherFoundation.FCCheckBox();
            this.lblExtraLines = new fecherFoundation.FCLabel();
            this.fraRate = new fecherFoundation.FCFrame();
            this.vsRate = new fecherFoundation.FCGrid();
            this.fraQuestions = new fecherFoundation.FCFrame();
            this.lblEligible = new fecherFoundation.FCLabel();
            this.fraPreLienEdit = new fecherFoundation.FCFrame();
            this.cmbReportDetail = new fecherFoundation.FCComboBox();
            this.txtMailDate = new Global.T2KDateBox();
            this.lblReportLen = new fecherFoundation.FCLabel();
            this.lblMailDate = new fecherFoundation.FCLabel();
            this.lblInstructions = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSelectAll = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileAddRateKey = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDateRanges = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdFileSave = new fecherFoundation.FCButton();
            this.cmdFileClear = new fecherFoundation.FCButton();
            this.cmdFileAddRateKey = new fecherFoundation.FCButton();
            this.cmdDateRanges = new fecherFoundation.FCButton();
            this.fcFrameWS = new fecherFoundation.FCFrame();
            this.fcFrameReminderLien = new fecherFoundation.FCFrame();
            this.fcFramePrint = new fecherFoundation.FCFrame();
            this.fcFramePeriod = new fecherFoundation.FCFrame();
            this.cmdClearAllFilters = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).BeginInit();
            this.fraRateInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFilter)).BeginInit();
            this.fraFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdApplyFilter)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingDate_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingDate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
            this.fraRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraTextRange)).BeginInit();
            this.fraTextRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPrintOptions)).BeginInit();
            this.fraPrintOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRate)).BeginInit();
            this.fraRate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
            this.fraQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPreLienEdit)).BeginInit();
            this.fraPreLienEdit.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAddRateKey)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDateRanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrameWS)).BeginInit();
            this.fcFrameWS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrameReminderLien)).BeginInit();
            this.fcFrameReminderLien.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFramePrint)).BeginInit();
            this.fcFramePrint.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFramePeriod)).BeginInit();
            this.fcFramePeriod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAllFilters)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileSave);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraRate);
            this.ClientArea.Controls.Add(this.fcFramePeriod);
            this.ClientArea.Controls.Add(this.fcFramePrint);
            this.ClientArea.Controls.Add(this.fcFrameReminderLien);
            this.ClientArea.Controls.Add(this.fraRateInfo);
            this.ClientArea.Controls.Add(this.fraFilter);
            this.ClientArea.Controls.Add(this.fraDateRange);
            this.ClientArea.Controls.Add(this.fraRange);
            this.ClientArea.Controls.Add(this.fraPrintOptions);
            this.ClientArea.Controls.Add(this.fraQuestions);
            this.ClientArea.Controls.Add(this.fraPreLienEdit);
            this.ClientArea.Controls.Add(this.lblInstructions);
            this.ClientArea.Controls.Add(this.fcFrameWS);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdSelectAll);
            this.TopPanel.Controls.Add(this.cmdClearAllFilters);
            this.TopPanel.Controls.Add(this.cmdDateRanges);
            this.TopPanel.Controls.Add(this.cmdFileAddRateKey);
            this.TopPanel.Controls.Add(this.cmdFileClear);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileAddRateKey, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDateRanges, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClearAllFilters, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdSelectAll, 0);
            // 
            // txtRange_1
            // 
            this.txtRange_1.BackColor = System.Drawing.SystemColors.Window;
            this.txtRange_1.Enabled = false;
            clientEvent1.Event = "keypress";
            clientEvent1.JavaScript = resources.GetString("clientEvent1.JavaScript");
            this.javaScriptIntegerNumbersOnly.GetJavaScriptEvents(this.txtRange_1).Add(clientEvent1);
            this.txtRange_1.Location = new System.Drawing.Point(214, 20);
            this.txtRange_1.Name = "txtRange_1";
            this.txtRange_1.Size = new System.Drawing.Size(174, 40);
            this.txtRange_1.TabIndex = 2;
            this.txtRange_1.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRange_KeyPress);
            // 
            // txtRange_0
            // 
            this.txtRange_0.BackColor = System.Drawing.SystemColors.Window;
            this.txtRange_0.Enabled = false;
            this.javaScriptIntegerNumbersOnly.GetJavaScriptEvents(this.txtRange_0).Add(clientEvent1);
            this.txtRange_0.Location = new System.Drawing.Point(20, 20);
            this.txtRange_0.Name = "txtRange_0";
            this.txtRange_0.Size = new System.Drawing.Size(174, 40);
            this.txtRange_0.TabIndex = 1;
            this.txtRange_0.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtRange_KeyPress);
            // 
            // cmbFilterContains
            // 
            this.cmbFilterContains.Items.AddRange(new object[] {
            "Contains",
            "Does Not Contain"});
            this.cmbFilterContains.Location = new System.Drawing.Point(141, 110);
            this.cmbFilterContains.Name = "cmbFilterContains";
            this.cmbFilterContains.Size = new System.Drawing.Size(217, 40);
            this.cmbFilterContains.TabIndex = 8;
            // 
            // lblFilterContains
            // 
            this.lblFilterContains.AutoSize = true;
            this.lblFilterContains.Location = new System.Drawing.Point(20, 124);
            this.lblFilterContains.Name = "lblFilterContains";
            this.lblFilterContains.Size = new System.Drawing.Size(117, 15);
            this.lblFilterContains.TabIndex = 7;
            this.lblFilterContains.Text = "FILTER CONTAINS";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "All Accounts",
            "Range by Name",
            "Range by Account"});
            this.cmbRange.Location = new System.Drawing.Point(101, 30);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(287, 40);
            this.cmbRange.TabIndex = 1;
            this.cmbRange.Text = "All Accounts";
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(20, 44);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(51, 15);
            this.lblRange.TabIndex = 2;
            this.lblRange.Text = "RANGE";
            // 
            // cmbReminderLien
            // 
            this.cmbReminderLien.Items.AddRange(new object[] {
            "Regular",
            "Lien"});
            this.cmbReminderLien.Location = new System.Drawing.Point(125, 0);
            this.cmbReminderLien.Name = "cmbReminderLien";
            this.cmbReminderLien.Size = new System.Drawing.Size(196, 40);
            this.cmbReminderLien.TabIndex = 3;
            this.cmbReminderLien.Text = "Regular";
            this.cmbReminderLien.SelectedIndexChanged += new System.EventHandler(this.optReminderLien_CheckedChanged);
            // 
            // lblReminderLien
            // 
            this.lblReminderLien.AutoSize = true;
            this.lblReminderLien.Location = new System.Drawing.Point(0, 14);
            this.lblReminderLien.Name = "lblReminderLien";
            this.lblReminderLien.Size = new System.Drawing.Size(76, 15);
            this.lblReminderLien.TabIndex = 2;
            this.lblReminderLien.Text = "RATE TYPE";
            this.lblReminderLien.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbWS
            // 
            this.cmbWS.Items.AddRange(new object[] {
            "Water",
            "Sewer"});
            this.cmbWS.Location = new System.Drawing.Point(125, 0);
            this.cmbWS.Name = "cmbWS";
            this.cmbWS.Size = new System.Drawing.Size(166, 40);
            this.cmbWS.TabIndex = 9;
            this.cmbWS.Text = "Water";
            this.cmbWS.SelectedIndexChanged += new System.EventHandler(this.optWS_CheckedChanged);
            // 
            // lblWS
            // 
            this.lblWS.AutoSize = true;
            this.lblWS.Location = new System.Drawing.Point(0, 14);
            this.lblWS.Name = "lblWS";
            this.lblWS.Size = new System.Drawing.Size(107, 15);
            this.lblWS.TabIndex = 8;
            this.lblWS.Text = "WATER / SEWER";
            this.lblWS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbPrint
            // 
            this.cmbPrint.Items.AddRange(new object[] {
            "Labels",
            "Certified Mail Forms"});
            this.cmbPrint.Location = new System.Drawing.Point(125, 0);
            this.cmbPrint.Name = "cmbPrint";
            this.cmbPrint.Size = new System.Drawing.Size(196, 40);
            this.cmbPrint.TabIndex = 5;
            this.cmbPrint.Text = "Labels";
            // 
            // lblPrint
            // 
            this.lblPrint.AutoSize = true;
            this.lblPrint.Location = new System.Drawing.Point(0, 14);
            this.lblPrint.Name = "lblPrint";
            this.lblPrint.Size = new System.Drawing.Size(45, 15);
            this.lblPrint.TabIndex = 4;
            this.lblPrint.Text = "PRINT";
            this.lblPrint.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbLien
            // 
            this.cmbLien.Items.AddRange(new object[] {
            "All Records",
            "Pre Lien Only",
            "Liened Only",
            "30 Day Notice",
            "Lien",
            "Lien Maturity"});
            this.cmbLien.Location = new System.Drawing.Point(125, 0);
            this.cmbLien.Name = "cmbLien";
            this.cmbLien.Size = new System.Drawing.Size(166, 40);
            this.cmbLien.TabIndex = 1;
            this.cmbLien.Text = "Pre Lien Only";
            this.cmbLien.SelectedIndexChanged += new System.EventHandler(this.optLien_CheckedChanged);
            // 
            // cmbPeriod
            // 
            this.cmbPeriod.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cmbPeriod.Location = new System.Drawing.Point(125, 0);
            this.cmbPeriod.Name = "cmbPeriod";
            this.cmbPeriod.Size = new System.Drawing.Size(166, 40);
            this.cmbPeriod.TabIndex = 7;
            this.cmbPeriod.Text = "1";
            // 
            // lblPeriod
            // 
            this.lblPeriod.AutoSize = true;
            this.lblPeriod.Location = new System.Drawing.Point(0, 14);
            this.lblPeriod.Name = "lblPeriod";
            this.lblPeriod.Size = new System.Drawing.Size(55, 15);
            this.lblPeriod.TabIndex = 6;
            this.lblPeriod.Text = "PERIOD";
            this.lblPeriod.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraRateInfo
            // 
            this.fraRateInfo.BackColor = System.Drawing.Color.White;
            this.fraRateInfo.Controls.Add(this.vsRateInfo);
            this.fraRateInfo.Controls.Add(this.cmdRIClose);
            this.fraRateInfo.Location = new System.Drawing.Point(444, 104);
            this.fraRateInfo.Name = "fraRateInfo";
            this.fraRateInfo.Size = new System.Drawing.Size(708, 504);
            this.fraRateInfo.TabIndex = 11;
            this.fraRateInfo.Text = "Year Information";
            this.fraRateInfo.Visible = false;
            // 
            // vsRateInfo
            // 
            this.vsRateInfo.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsRateInfo.Cols = 3;
            this.vsRateInfo.ColumnHeadersVisible = false;
            this.vsRateInfo.FixedCols = 0;
            this.vsRateInfo.FixedRows = 0;
            this.vsRateInfo.Location = new System.Drawing.Point(20, 30);
            this.vsRateInfo.Name = "vsRateInfo";
            this.vsRateInfo.RowHeadersVisible = false;
            this.vsRateInfo.Rows = 12;
            this.vsRateInfo.ShowFocusCell = false;
            this.vsRateInfo.Size = new System.Drawing.Size(668, 404);
            // 
            // cmdRIClose
            // 
            this.cmdRIClose.AppearanceKey = "actionButton";
            this.cmdRIClose.ForeColor = System.Drawing.Color.White;
            this.cmdRIClose.Location = new System.Drawing.Point(305, 445);
            this.cmdRIClose.Name = "cmdRIClose";
            this.cmdRIClose.Size = new System.Drawing.Size(98, 40);
            this.cmdRIClose.TabIndex = 1;
            this.cmdRIClose.Text = "Close";
            this.cmdRIClose.Click += new System.EventHandler(this.cmdRIClose_Click);
            // 
            // fraFilter
            // 
            this.fraFilter.BackColor = System.Drawing.Color.White;
            this.fraFilter.Controls.Add(this.cmdCancelFilter);
            this.fraFilter.Controls.Add(this.cmbFilterContains);
            this.fraFilter.Controls.Add(this.lblFilterContains);
            this.fraFilter.Controls.Add(this.txtFilterEnd);
            this.fraFilter.Controls.Add(this.txtFilterStart);
            this.fraFilter.Controls.Add(this.cmdClearFilter);
            this.fraFilter.Controls.Add(this.cmdApplyFilter);
            this.fraFilter.Controls.Add(this.txtFilterStartDate);
            this.fraFilter.Controls.Add(this.txtFilterEndDate);
            this.fraFilter.Controls.Add(this.lblFilterBy);
            this.fraFilter.Controls.Add(this.lblFilterStart);
            this.fraFilter.Controls.Add(this.lblFilterEnd);
            this.fraFilter.Location = new System.Drawing.Point(30, 826);
            this.fraFilter.Name = "fraFilter";
            this.fraFilter.Size = new System.Drawing.Size(490, 230);
            this.fraFilter.TabIndex = 13;
            this.fraFilter.Text = "Filter";
            this.fraFilter.Visible = false;
            // 
            // cmdCancelFilter
            // 
            this.cmdCancelFilter.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdCancelFilter.AppearanceKey = "actionButton";
            this.cmdCancelFilter.ForeColor = System.Drawing.Color.White;
            this.cmdCancelFilter.Location = new System.Drawing.Point(255, 170);
            this.cmdCancelFilter.Name = "cmdCancelFilter";
            this.cmdCancelFilter.Size = new System.Drawing.Size(87, 40);
            this.cmdCancelFilter.TabIndex = 11;
            this.cmdCancelFilter.Text = "Cancel";
            this.cmdCancelFilter.Click += new System.EventHandler(this.cmdCancelFilter_Click);
            // 
            // txtFilterEnd
            // 
            this.txtFilterEnd.BackColor = System.Drawing.SystemColors.Window;
            this.txtFilterEnd.Location = new System.Drawing.Point(311, 50);
            this.txtFilterEnd.Name = "txtFilterEnd";
            this.txtFilterEnd.Size = new System.Drawing.Size(140, 40);
            this.txtFilterEnd.TabIndex = 6;
            this.txtFilterEnd.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFilterEnd_KeyPress);
            // 
            // txtFilterStart
            // 
            this.txtFilterStart.BackColor = System.Drawing.SystemColors.Window;
            this.txtFilterStart.Location = new System.Drawing.Point(141, 50);
            this.txtFilterStart.Name = "txtFilterStart";
            this.txtFilterStart.Size = new System.Drawing.Size(217, 40);
            this.txtFilterStart.TabIndex = 5;
            this.txtFilterStart.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFilterStart_KeyPress);
            // 
            // cmdClearFilter
            // 
            this.cmdClearFilter.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdClearFilter.AppearanceKey = "actionButton";
            this.cmdClearFilter.ForeColor = System.Drawing.Color.White;
            this.cmdClearFilter.Location = new System.Drawing.Point(20, 170);
            this.cmdClearFilter.Name = "cmdClearFilter";
            this.cmdClearFilter.Size = new System.Drawing.Size(97, 40);
            this.cmdClearFilter.TabIndex = 9;
            this.cmdClearFilter.Text = "Clear";
            this.cmdClearFilter.Click += new System.EventHandler(this.cmdClearFilter_Click);
            // 
            // cmdApplyFilter
            // 
            this.cmdApplyFilter.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Bottom | Wisej.Web.AnchorStyles.Left)));
            this.cmdApplyFilter.AppearanceKey = "actionButton";
            this.cmdApplyFilter.ForeColor = System.Drawing.Color.White;
            this.cmdApplyFilter.Location = new System.Drawing.Point(141, 170);
            this.cmdApplyFilter.Name = "cmdApplyFilter";
            this.cmdApplyFilter.Size = new System.Drawing.Size(87, 40);
            this.cmdApplyFilter.TabIndex = 10;
            this.cmdApplyFilter.Text = "Apply";
            this.cmdApplyFilter.Click += new System.EventHandler(this.cmdApplyFilter_Click);
            // 
            // txtFilterStartDate
            // 
            this.txtFilterStartDate.Location = new System.Drawing.Point(141, 50);
            this.txtFilterStartDate.Mask = "##/##/####";
            this.txtFilterStartDate.Name = "txtFilterStartDate";
            this.txtFilterStartDate.Size = new System.Drawing.Size(140, 40);
            this.txtFilterStartDate.TabIndex = 3;
            // 
            // txtFilterEndDate
            // 
            this.txtFilterEndDate.Location = new System.Drawing.Point(312, 50);
            this.txtFilterEndDate.Mask = "##/##/####";
            this.txtFilterEndDate.Name = "txtFilterEndDate";
            this.txtFilterEndDate.Size = new System.Drawing.Size(140, 40);
            this.txtFilterEndDate.TabIndex = 4;
            // 
            // lblFilterBy
            // 
            this.lblFilterBy.Location = new System.Drawing.Point(20, 64);
            this.lblFilterBy.Name = "lblFilterBy";
            this.lblFilterBy.Size = new System.Drawing.Size(81, 27);
            this.lblFilterBy.TabIndex = 2;
            this.lblFilterBy.Text = "BILL DATE";
            // 
            // lblFilterStart
            // 
            this.lblFilterStart.Location = new System.Drawing.Point(135, 30);
            this.lblFilterStart.Name = "lblFilterStart";
            this.lblFilterStart.Size = new System.Drawing.Size(124, 14);
            this.lblFilterStart.TabIndex = 12;
            this.lblFilterStart.Text = "START";
            this.lblFilterStart.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblFilterEnd
            // 
            this.lblFilterEnd.Location = new System.Drawing.Point(303, 30);
            this.lblFilterEnd.Name = "lblFilterEnd";
            this.lblFilterEnd.Size = new System.Drawing.Size(116, 23);
            this.lblFilterEnd.TabIndex = 1;
            this.lblFilterEnd.Text = "END";
            this.lblFilterEnd.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // fraDateRange
            // 
            this.fraDateRange.Controls.Add(this.txtBillingDate_0);
            this.fraDateRange.Controls.Add(this.txtBillingDate_1);
            this.fraDateRange.Controls.Add(this.txtDueDate_0);
            this.fraDateRange.Controls.Add(this.txtDueDate_1);
            this.fraDateRange.Controls.Add(this.lblStartDate_1);
            this.fraDateRange.Controls.Add(this.lblEndDate_1);
            this.fraDateRange.Controls.Add(this.lblEndDate_0);
            this.fraDateRange.Controls.Add(this.lblStartDate_0);
            this.fraDateRange.Location = new System.Drawing.Point(30, 148);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(510, 160);
            this.fraDateRange.TabIndex = 15;
            this.fraDateRange.Text = "Rate Key Date Range";
            this.fraDateRange.Visible = false;
            this.fraDateRange.DragDrop += new Wisej.Web.DragEventHandler(this.fraDateRange_DragDrop);
            // 
            // txtBillingDate_0
            // 
            this.txtBillingDate_0.Location = new System.Drawing.Point(129, 50);
            this.txtBillingDate_0.Name = "txtBillingDate_0";
            this.txtBillingDate_0.Size = new System.Drawing.Size(115, 40);
            this.txtBillingDate_0.TabIndex = 3;
            // 
            // txtBillingDate_1
            // 
            this.txtBillingDate_1.Location = new System.Drawing.Point(266, 50);
            this.txtBillingDate_1.Name = "txtBillingDate_1";
            this.txtBillingDate_1.Size = new System.Drawing.Size(115, 40);
            this.txtBillingDate_1.TabIndex = 4;
            // 
            // txtDueDate_0
            // 
            this.txtDueDate_0.Location = new System.Drawing.Point(129, 100);
            this.txtDueDate_0.Name = "txtDueDate_0";
            this.txtDueDate_0.Size = new System.Drawing.Size(115, 40);
            this.txtDueDate_0.TabIndex = 6;
            // 
            // txtDueDate_1
            // 
            this.txtDueDate_1.Location = new System.Drawing.Point(266, 100);
            this.txtDueDate_1.Name = "txtDueDate_1";
            this.txtDueDate_1.Size = new System.Drawing.Size(115, 40);
            this.txtDueDate_1.TabIndex = 7;
            // 
            // lblStartDate_1
            // 
            this.lblStartDate_1.Location = new System.Drawing.Point(20, 114);
            this.lblStartDate_1.Name = "lblStartDate_1";
            this.lblStartDate_1.Size = new System.Drawing.Size(94, 27);
            this.lblStartDate_1.TabIndex = 5;
            this.lblStartDate_1.Text = "DUE DATE";
            // 
            // lblEndDate_1
            // 
            this.lblEndDate_1.Location = new System.Drawing.Point(260, 30);
            this.lblEndDate_1.Name = "lblEndDate_1";
            this.lblEndDate_1.Size = new System.Drawing.Size(116, 16);
            this.lblEndDate_1.TabIndex = 1;
            this.lblEndDate_1.Text = "END";
            this.lblEndDate_1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblEndDate_0
            // 
            this.lblEndDate_0.Location = new System.Drawing.Point(123, 30);
            this.lblEndDate_0.Name = "lblEndDate_0";
            this.lblEndDate_0.Size = new System.Drawing.Size(124, 16);
            this.lblEndDate_0.TabIndex = 8;
            this.lblEndDate_0.Text = "START";
            this.lblEndDate_0.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // lblStartDate_0
            // 
            this.lblStartDate_0.Location = new System.Drawing.Point(20, 64);
            this.lblStartDate_0.Name = "lblStartDate_0";
            this.lblStartDate_0.Size = new System.Drawing.Size(107, 27);
            this.lblStartDate_0.TabIndex = 2;
            this.lblStartDate_0.Text = "BILLING DATE";
            // 
            // fraRange
            // 
            this.fraRange.Controls.Add(this.cmbRange);
            this.fraRange.Controls.Add(this.lblRange);
            this.fraRange.Controls.Add(this.txtMinimumAmount);
            this.fraRange.Controls.Add(this.fraTextRange);
            this.fraRange.Controls.Add(this.lblMinAmount);
            this.fraRange.Location = new System.Drawing.Point(560, 505);
            this.fraRange.Name = "fraRange";
            this.fraRange.Size = new System.Drawing.Size(408, 203);
            this.fraRange.TabIndex = 16;
            this.fraRange.Text = "Selection Criteria";
            this.fraRange.Visible = false;
            // 
            // txtMinimumAmount
            // 
            this.txtMinimumAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMinimumAmount.Location = new System.Drawing.Point(303, 150);
            this.txtMinimumAmount.Name = "txtMinimumAmount";
            this.txtMinimumAmount.Size = new System.Drawing.Size(82, 40);
            this.txtMinimumAmount.TabIndex = 4;
            this.txtMinimumAmount.Text = "0.00";
            this.txtMinimumAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtMinimumAmount, "Minimum principal due in order to process.");
            this.txtMinimumAmount.Enter += new System.EventHandler(this.txtMinimumAmount_Enter);
            this.txtMinimumAmount.Validating += new System.ComponentModel.CancelEventHandler(this.txtMinimumAmount_Validating);
            this.txtMinimumAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMinimumAmount_KeyPress);
            // 
            // fraTextRange
            // 
            this.fraTextRange.AppearanceKey = "groupBoxNoBorders";
            this.fraTextRange.Controls.Add(this.txtRange_1);
            this.fraTextRange.Controls.Add(this.txtRange_0);
            this.fraTextRange.Location = new System.Drawing.Point(1, 71);
            this.fraTextRange.Name = "fraTextRange";
            this.fraTextRange.Size = new System.Drawing.Size(390, 79);
            this.fraTextRange.TabIndex = 2;
            // 
            // lblMinAmount
            // 
            this.lblMinAmount.Location = new System.Drawing.Point(20, 164);
            this.lblMinAmount.Name = "lblMinAmount";
            this.lblMinAmount.Size = new System.Drawing.Size(273, 18);
            this.lblMinAmount.TabIndex = 3;
            this.lblMinAmount.Text = "MINIMUM PRINCIPAL AMOUNT TO PROCESS";
            // 
            // fraPrintOptions
            // 
            this.fraPrintOptions.Controls.Add(this.cmbExtraLines);
            this.fraPrintOptions.Controls.Add(this.chkPrint_3);
            this.fraPrintOptions.Controls.Add(this.chkPrint_2);
            this.fraPrintOptions.Controls.Add(this.chkPrint_1);
            this.fraPrintOptions.Controls.Add(this.chkPrint_0);
            this.fraPrintOptions.Controls.Add(this.lblExtraLines);
            this.fraPrintOptions.Location = new System.Drawing.Point(30, 647);
            this.fraPrintOptions.Name = "fraPrintOptions";
            this.fraPrintOptions.Size = new System.Drawing.Size(510, 159);
            this.fraPrintOptions.TabIndex = 14;
            this.fraPrintOptions.Text = "Print Options";
            this.fraPrintOptions.Visible = false;
            // 
            // cmbExtraLines
            // 
            this.cmbExtraLines.BackColor = System.Drawing.SystemColors.Window;
            this.cmbExtraLines.Location = new System.Drawing.Point(146, 30);
            this.cmbExtraLines.Name = "cmbExtraLines";
            this.cmbExtraLines.Size = new System.Drawing.Size(160, 40);
            this.cmbExtraLines.TabIndex = 1;
            // 
            // chkPrint_3
            // 
            this.chkPrint_3.Location = new System.Drawing.Point(188, 120);
            this.chkPrint_3.Name = "chkPrint_3";
            this.chkPrint_3.Size = new System.Drawing.Size(87, 27);
            this.chkPrint_3.TabIndex = 39;
            this.chkPrint_3.Text = "Address";
            // 
            // chkPrint_2
            // 
            this.chkPrint_2.Location = new System.Drawing.Point(188, 80);
            this.chkPrint_2.Name = "chkPrint_2";
            this.chkPrint_2.Size = new System.Drawing.Size(89, 27);
            this.chkPrint_2.TabIndex = 3;
            this.chkPrint_2.Text = "Location";
            // 
            // chkPrint_1
            // 
            this.chkPrint_1.Location = new System.Drawing.Point(20, 120);
            this.chkPrint_1.Name = "chkPrint_1";
            this.chkPrint_1.Size = new System.Drawing.Size(107, 27);
            this.chkPrint_1.TabIndex = 4;
            this.chkPrint_1.Text = "Book Page";
            // 
            // chkPrint_0
            // 
            this.chkPrint_0.Location = new System.Drawing.Point(20, 80);
            this.chkPrint_0.Name = "chkPrint_0";
            this.chkPrint_0.Size = new System.Drawing.Size(86, 27);
            this.chkPrint_0.TabIndex = 2;
            this.chkPrint_0.Text = "Map Lot";
            // 
            // lblExtraLines
            // 
            this.lblExtraLines.Location = new System.Drawing.Point(20, 44);
            this.lblExtraLines.Name = "lblExtraLines";
            this.lblExtraLines.Size = new System.Drawing.Size(78, 26);
            this.lblExtraLines.TabIndex = 40;
            this.lblExtraLines.Text = "EXTRA LINES";
            // 
            // fraRate
            // 
            this.fraRate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraRate.Controls.Add(this.vsRate);
            this.fraRate.Location = new System.Drawing.Point(30, 148);
            this.fraRate.Name = "fraRate";
            this.fraRate.Size = new System.Drawing.Size(845, 334);
            this.fraRate.TabIndex = 10;
            this.fraRate.Text = "Select Rate Record";
            this.fraRate.Visible = false;
            // 
            // vsRate
            // 
            this.vsRate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsRate.Cols = 5;
            this.vsRate.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
            this.vsRate.ExtendLastCol = true;
            this.vsRate.FixedCols = 0;
            this.vsRate.Location = new System.Drawing.Point(20, 30);
            this.vsRate.Name = "vsRate";
            this.vsRate.RowHeadersVisible = false;
            this.vsRate.Rows = 1;
            this.vsRate.ShowFocusCell = false;
            this.vsRate.Size = new System.Drawing.Size(805, 284);
            this.vsRate.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsRate_CellChanged);
            this.vsRate.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRate_BeforeEdit);
            this.vsRate.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRate_ValidateEdit);
            this.vsRate.CurrentCellChanged += new System.EventHandler(this.vsRate_RowColChange);
            this.vsRate.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsRate_MouseDownEvent);
            this.vsRate.Enter += new System.EventHandler(this.vsRate_Enter);
            this.vsRate.Click += new System.EventHandler(this.vsRate_ClickEvent);
            this.vsRate.DoubleClick += new System.EventHandler(this.vsRate_DblClick);
            this.vsRate.MouseDown += new Wisej.Web.MouseEventHandler(this.vsRate_BeforeMouseDown);
            // 
            // fraQuestions
            // 
            this.fraQuestions.AppearanceKey = "groupBoxNoBorders";
            this.fraQuestions.Controls.Add(this.cmbLien);
            this.fraQuestions.Controls.Add(this.lblEligible);
            this.fraQuestions.Location = new System.Drawing.Point(30, 30);
            this.fraQuestions.Name = "fraQuestions";
            this.fraQuestions.Size = new System.Drawing.Size(291, 40);
            this.fraQuestions.TabIndex = 1;
            this.fraQuestions.Visible = false;
            // 
            // lblEligible
            // 
            this.lblEligible.Location = new System.Drawing.Point(0, 14);
            this.lblEligible.Name = "lblEligible";
            this.lblEligible.Size = new System.Drawing.Size(94, 15);
            this.lblEligible.TabIndex = 2;
            this.lblEligible.Text = "ELIGIBLE FOR";
            this.lblEligible.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraPreLienEdit
            // 
            this.fraPreLienEdit.Controls.Add(this.cmbReportDetail);
            this.fraPreLienEdit.Controls.Add(this.txtMailDate);
            this.fraPreLienEdit.Controls.Add(this.lblReportLen);
            this.fraPreLienEdit.Controls.Add(this.lblMailDate);
            this.fraPreLienEdit.Location = new System.Drawing.Point(30, 505);
            this.fraPreLienEdit.Name = "fraPreLienEdit";
            this.fraPreLienEdit.Size = new System.Drawing.Size(510, 130);
            this.fraPreLienEdit.TabIndex = 12;
            this.fraPreLienEdit.Text = "Lien Edit Report Format";
            this.fraPreLienEdit.Visible = false;
            // 
            // cmbReportDetail
            // 
            this.cmbReportDetail.BackColor = System.Drawing.SystemColors.Window;
            this.cmbReportDetail.Location = new System.Drawing.Point(180, 80);
            this.cmbReportDetail.Name = "cmbReportDetail";
            this.cmbReportDetail.Size = new System.Drawing.Size(310, 40);
            this.cmbReportDetail.TabIndex = 3;
            this.cmbReportDetail.DropDown += new System.EventHandler(this.cmbReportDetail_DropDown);
            this.cmbReportDetail.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbReportDetail_KeyDown);
            // 
            // txtMailDate
            // 
            this.txtMailDate.Location = new System.Drawing.Point(180, 30);
            this.txtMailDate.Mask = "##/##/####";
            this.txtMailDate.Name = "txtMailDate";
            this.txtMailDate.Size = new System.Drawing.Size(126, 40);
            this.txtMailDate.TabIndex = 1;
            this.txtMailDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtMailDate_Validate);
            // 
            // lblReportLen
            // 
            this.lblReportLen.Location = new System.Drawing.Point(20, 94);
            this.lblReportLen.Name = "lblReportLen";
            this.lblReportLen.Size = new System.Drawing.Size(114, 31);
            this.lblReportLen.TabIndex = 2;
            this.lblReportLen.Text = "REPORT DETAIL";
            // 
            // lblMailDate
            // 
            this.lblMailDate.Location = new System.Drawing.Point(20, 44);
            this.lblMailDate.Name = "lblMailDate";
            this.lblMailDate.Size = new System.Drawing.Size(130, 31);
            this.lblMailDate.TabIndex = 4;
            this.lblMailDate.Text = "MAIL / INTEREST DATE";
            // 
            // lblInstructions
            // 
            this.lblInstructions.Location = new System.Drawing.Point(30, 30);
            this.lblInstructions.Name = "lblInstructions";
            this.lblInstructions.Size = new System.Drawing.Size(482, 52);
            this.lblInstructions.TabIndex = 21;
            this.lblInstructions.Visible = false;
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSelectAll,
            this.mnuFileClear,
            this.mnuFileAddRateKey,
            this.mnuDateRanges,
            this.mnuFileSeperator2,
            this.mnuFileSave,
            this.mnuFileSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSelectAll
            // 
            this.mnuFileSelectAll.Index = 0;
            this.mnuFileSelectAll.Name = "mnuFileSelectAll";
            this.mnuFileSelectAll.Shortcut = Wisej.Web.Shortcut.CtrlA;
            this.mnuFileSelectAll.Text = "Select All";
            this.mnuFileSelectAll.Visible = false;
            this.mnuFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
            // 
            // mnuFileClear
            // 
            this.mnuFileClear.Index = 1;
            this.mnuFileClear.Name = "mnuFileClear";
            this.mnuFileClear.Text = "Clear Selection";
            this.mnuFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
            // 
            // mnuFileAddRateKey
            // 
            this.mnuFileAddRateKey.Index = 2;
            this.mnuFileAddRateKey.Name = "mnuFileAddRateKey";
            this.mnuFileAddRateKey.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.mnuFileAddRateKey.Text = "New Rate Key";
            this.mnuFileAddRateKey.Click += new System.EventHandler(this.mnuFileAddRateKey_Click);
            // 
            // mnuDateRanges
            // 
            this.mnuDateRanges.Index = 3;
            this.mnuDateRanges.Name = "mnuDateRanges";
            this.mnuDateRanges.Text = "Rate Key Date Range Selection";
            this.mnuDateRanges.Click += new System.EventHandler(this.mnuDateRanges_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 4;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 5;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSave.Text = "Continue";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 6;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 7;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdFileSave
            // 
            this.cmdFileSave.AppearanceKey = "acceptButton";
            this.cmdFileSave.Location = new System.Drawing.Point(449, 30);
            this.cmdFileSave.Name = "cmdFileSave";
            this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileSave.Size = new System.Drawing.Size(112, 48);
            this.cmdFileSave.Text = "Continue";
            this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // cmdFileClear
            // 
            this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileClear.Location = new System.Drawing.Point(620, 29);
            this.cmdFileClear.Name = "cmdFileClear";
            this.cmdFileClear.Size = new System.Drawing.Size(110, 24);
            this.cmdFileClear.TabIndex = 1;
            this.cmdFileClear.Text = "Clear Selection";
            this.cmdFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
            // 
            // cmdFileAddRateKey
            // 
            this.cmdFileAddRateKey.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileAddRateKey.Location = new System.Drawing.Point(736, 29);
            this.cmdFileAddRateKey.Name = "cmdFileAddRateKey";
            this.cmdFileAddRateKey.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.cmdFileAddRateKey.Size = new System.Drawing.Size(102, 24);
            this.cmdFileAddRateKey.TabIndex = 2;
            this.cmdFileAddRateKey.Text = "New Rate Key";
            this.cmdFileAddRateKey.Click += new System.EventHandler(this.mnuFileAddRateKey_Click);
            // 
            // cmdDateRanges
            // 
            this.cmdDateRanges.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDateRanges.Location = new System.Drawing.Point(844, 29);
            this.cmdDateRanges.Name = "cmdDateRanges";
            this.cmdDateRanges.Size = new System.Drawing.Size(206, 24);
            this.cmdDateRanges.TabIndex = 3;
            this.cmdDateRanges.Text = "Rate Key Date Range Selection";
            this.cmdDateRanges.Click += new System.EventHandler(this.mnuDateRanges_Click);
            // 
            // fcFrameWS
            // 
            this.fcFrameWS.AppearanceKey = "groupBoxNoBorders";
            this.fcFrameWS.Controls.Add(this.lblWS);
            this.fcFrameWS.Controls.Add(this.cmbWS);
            this.fcFrameWS.Location = new System.Drawing.Point(30, 88);
            this.fcFrameWS.Name = "fcFrameWS";
            this.fcFrameWS.Size = new System.Drawing.Size(291, 40);
            this.fcFrameWS.TabIndex = 17;
            this.fcFrameWS.Visible = false;
            // 
            // fcFrameReminderLien
            // 
            this.fcFrameReminderLien.AppearanceKey = "groupBoxNoBorders";
            this.fcFrameReminderLien.Controls.Add(this.lblReminderLien);
            this.fcFrameReminderLien.Controls.Add(this.cmbReminderLien);
            this.fcFrameReminderLien.Location = new System.Drawing.Point(351, 30);
            this.fcFrameReminderLien.Name = "fcFrameReminderLien";
            this.fcFrameReminderLien.Size = new System.Drawing.Size(321, 40);
            this.fcFrameReminderLien.TabIndex = 18;
            this.fcFrameReminderLien.Visible = false;
            // 
            // fcFramePrint
            // 
            this.fcFramePrint.AppearanceKey = "groupBoxNoBorders";
            this.fcFramePrint.Controls.Add(this.lblPrint);
            this.fcFramePrint.Controls.Add(this.cmbPrint);
            this.fcFramePrint.Location = new System.Drawing.Point(351, 88);
            this.fcFramePrint.Name = "fcFramePrint";
            this.fcFramePrint.Size = new System.Drawing.Size(321, 40);
            this.fcFramePrint.TabIndex = 19;
            this.fcFramePrint.Visible = false;
            // 
            // fcFramePeriod
            // 
            this.fcFramePeriod.AppearanceKey = "groupBoxNoBorders";
            this.fcFramePeriod.Controls.Add(this.lblPeriod);
            this.fcFramePeriod.Controls.Add(this.cmbPeriod);
            this.fcFramePeriod.Location = new System.Drawing.Point(702, 30);
            this.fcFramePeriod.Name = "fcFramePeriod";
            this.fcFramePeriod.Size = new System.Drawing.Size(291, 40);
            this.fcFramePeriod.TabIndex = 20;
            this.fcFramePeriod.Visible = false;
            // 
            // cmdClearAllFilters
            // 
            this.cmdClearAllFilters.Anchor = Wisej.Web.AnchorStyles.Right;
            this.cmdClearAllFilters.Location = new System.Drawing.Point(504, 29);
            this.cmdClearAllFilters.Name = "cmdClearAllFilters";
            this.cmdClearAllFilters.Size = new System.Drawing.Size(110, 24);
            this.cmdClearAllFilters.TabIndex = 4;
            this.cmdClearAllFilters.Text = "Clear Filters";
            this.cmdClearAllFilters.Click += new System.EventHandler(this.cmdClearAllFilters_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.Anchor = Wisej.Web.AnchorStyles.Right;
            this.cmdSelectAll.Location = new System.Drawing.Point(484, 29);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(110, 24);
            this.cmdSelectAll.TabIndex = 7;
            this.cmdSelectAll.Text = "Select All";
            this.cmdSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
            // 
            // frmRateRecChoice
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmRateRecChoice";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Select Rate Record";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmRateRecChoice_Load);
            this.Activated += new System.EventHandler(this.frmRateRecChoice_Activated);
            this.Enter += new System.EventHandler(this.frmRateRecChoice_Enter);
            this.Resize += new System.EventHandler(this.frmRateRecChoice_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmRateRecChoice_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRateInfo)).EndInit();
            this.fraRateInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsRateInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRIClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFilter)).EndInit();
            this.fraFilter.ResumeLayout(false);
            this.fraFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancelFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdApplyFilter)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFilterEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            this.fraDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingDate_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBillingDate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
            this.fraRange.ResumeLayout(false);
            this.fraRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraTextRange)).EndInit();
            this.fraTextRange.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraPrintOptions)).EndInit();
            this.fraPrintOptions.ResumeLayout(false);
            this.fraPrintOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrint_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraRate)).EndInit();
            this.fraRate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
            this.fraQuestions.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraPreLienEdit)).EndInit();
            this.fraPreLienEdit.ResumeLayout(false);
            this.fraPreLienEdit.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtMailDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileAddRateKey)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDateRanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrameWS)).EndInit();
            this.fcFrameWS.ResumeLayout(false);
            this.fcFrameWS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFrameReminderLien)).EndInit();
            this.fcFrameReminderLien.ResumeLayout(false);
            this.fcFrameReminderLien.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFramePrint)).EndInit();
            this.fcFramePrint.ResumeLayout(false);
            this.fcFramePrint.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fcFramePeriod)).EndInit();
            this.fcFramePeriod.ResumeLayout(false);
            this.fcFramePeriod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAllFilters)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSave;
		private FCButton cmdFileClear;
		private FCButton cmdFileAddRateKey;
		private FCButton cmdDateRanges;
		public FCFrame fcFrameWS;
		public FCFrame fcFramePeriod;
		public FCFrame fcFramePrint;
		public FCFrame fcFrameReminderLien;
		//FC:FINAL:CHN - issue #1102: allow numbers only to input (Javascript).
		private JavaScript javaScriptIntegerNumbersOnly;
		private Wisej.Web.JavaScript.ClientEvent clientEventKeyPressIntegerOnly;
        private FCButton cmdClearAllFilters;
        private FCButton cmdSelectAll;
    }
}