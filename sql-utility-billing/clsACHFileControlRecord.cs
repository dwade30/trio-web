//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class clsACHFileControlRecord
	{

		public string RecordCode
		{
			get
			{
					string RecordCode = "";
				RecordCode = "9";
				return RecordCode;
			}
		}

        public int BatchCount { get; set; } = 0;

        public int BlockCount { get; set; } = 0;

		public int EntryAddendaCount { get; set; }

        public double TotalDebit { get; set; } = 0;


        public double TotalCredit { get; set; } = 0;

        public string LastError { get; set; } = "";

        public double Hash { get; set; } = 0;




		public string OutputLine()
		{
			string OutputLine = "";
			LastError = "";
			string strLine;
			string strTemp = "";
			try
			{	// On Error GoTo ErrorHandler
				strLine = RecordCode;
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(6, "0")+FCConvert.ToString(BatchCount), 6);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(6, "0")+FCConvert.ToString(BlockCount), 6);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(8, "0")+FCConvert.ToString(EntryAddendaCount), 8);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(10, "0")+FCConvert.ToString(Hash), 10);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(12, "0")+FCConvert.ToString(TotalDebit*100), 12);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(12, "0")+FCConvert.ToString(TotalCredit*100), 12);
				strLine += fecherFoundation.Strings.StrDup(39, " "); // reserved
				OutputLine = strLine;
				return OutputLine;
			}
			catch (Exception ex)
			{	// ErrorHandler:
				LastError = "Could not build file control record."+"\n"+ex.Message;
			}
			return OutputLine;
		}

	}
}
