﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmRemoveLienMatFee.
	/// </summary>
	public partial class frmRemoveLienMatFee : BaseForm
	{
		public frmRemoveLienMatFee()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRemoveLienMatFee InstancePtr
		{
			get
			{
				return (frmRemoveLienMatFee)Sys.GetInstance(typeof(frmRemoveLienMatFee));
			}
		}

		protected frmRemoveLienMatFee _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/15/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/16/2005              *
		// ********************************************************
		int lngRE;
		int lngPP;
		int lngLN;
		DateTime dtDate;
		int lngAcct;
		string strWS = "";
		int lngAcctKey;
		// MAL@20080805: Added to distinguish between user friendly account num and record ID
		private void cmbYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						// return
						mnuFilePrint_Click();
						break;
					}
			}
			//end switch
		}

		private void frmRemoveLienMatFee_Activated(object sender, System.EventArgs e)
		{
			lblDate.Text = "Select the account to remove the lien fees from.";
			ShowFrame(ref fraDate);
		}

		private void frmRemoveLienMatFee_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRemoveLienMatFee properties;
			//frmRemoveLienMatFee.FillStyle	= 0;
			//frmRemoveLienMatFee.ScaleWidth	= 3885;
			//frmRemoveLienMatFee.ScaleHeight	= 2310;
			//frmRemoveLienMatFee.LinkTopic	= "Form2";
			//frmRemoveLienMatFee.LockControls	= true;
			//frmRemoveLienMatFee.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Remove Lien Maturity Fee";
			if (modUTStatusPayments.Statics.TownService == "W")
			{
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = false;
				//optWS[0].Checked = true;
				cmbWS.Clear();
				cmbWS.Items.Add("Water");
				cmbWS.Text = "Water";
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				//optWS[0].Enabled = false;
				//optWS[1].Enabled = true;
				//optWS[1].Checked = true;
				cmbWS.Clear();
				cmbWS.Items.Add("Sewer");
				cmbWS.Text = "Sewer";
			}
			else if (modUTStatusPayments.Statics.TownService == "B")
			{
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = true;
				if (modUTStatusPayments.Statics.gboolPayWaterFirst)
				{
					//optWS[0].Checked = true;
					cmbWS.Text = "Water";
				}
				else
				{
					//optWS[1].Checked = true;
					cmbWS.Text = "Sewer";
				}
			}
		}

		private void frmRemoveLienMatFee_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowFrame(ref FCFrame fra)
		{
			string vbPorterVar = fra.GetName();
			//if (vbPorterVar == "fraDate")
			//{
			//    fraDate.Top = 100; // (Me.Height - fraDate.Height) / 3
			//}
			//else
			//{
			//}
			//fra.Left = FCConvert.ToInt32((this.Width - fra.Width) / 2.0);
			fra.Visible = true;
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			int intErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsPay = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				int lngRK = 0;
				double dblFee = 0;
				string strFee = "";
				//if (optWS[0].Checked) {
				if (cmbWS.Text == "Water")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				// save routine
				if (Conversion.Val(txtAccount.Text) != 0)
				{
					intErr = 1;
					if (cmbYear.SelectedIndex != -1)
					{
						intErr = 2;
						lngRK = cmbYear.ItemData(cmbYear.SelectedIndex);
						if (MessageBox.Show("Are you sure that you would want to remove the maturity fees from this account?", "Remove Lien Maturity Fees", MessageBoxButtons.YesNo, MessageBoxIcon.Information) != DialogResult.Yes)
						{
							return;
						}
						// CYA entry
						modGlobalFunctions.AddCYAEntry_80("UT", "Removing lien maturity fees.", "Account: " + FCConvert.ToString(lngAcct), "RK: " + FCConvert.ToString(lngRK));
						intErr = 3;
						// remove the fees
						// find the billing/lien record
						rsBill.OpenRecordset("SELECT Bill.ID AS Bill,SCombinationLienKey,WCombinationLienKey,Lien.ID AS LienKey,MaturityFee FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND RateKey = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
						if (!rsBill.EndOfFile())
						{
							// this will find the fee from the payment rec to delete
							// MAL@20071218: Added check for Code of 'L' to avoid deleting charged interest lines.
							// Tracker Reference: 11675
							// rsPay.OpenRecordset "SELECT * FROM PaymentRec WHERE BillKey = " & rsBill.Fields("LienKey") & " AND Lien = TRUE", strUTDatabase
							rsPay.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsBill.Get_Fields_Int32("LienKey") + " AND ISNULL(Lien,0) = 1 And Code = 'L'", modExtraModules.strUTDatabase);
							if (!rsPay.EndOfFile())
							{
								dblFee = FCConvert.ToDouble(rsPay.Get_Fields_Decimal("LienCost"));
								modGlobalFunctions.AddCYAEntry_728("UT", "Removing Lien Maturity Payment Record.", "EID: " + rsPay.Get_Fields_DateTime("EffectiveInterestDate"), "RTD: " + rsPay.Get_Fields_DateTime("RecordedTransactionDate"), Strings.Format(rsPay.Get_Fields_Decimal("LienCost"), "$#,##0.00"), "Lien Record Number = " + rsPay.Get_Fields_Int32("BillKey"));
								// found ...not delete it
								// rsPay.Delete
								rsPay.Execute("DELETE FROM PaymentRec WHERE ID = " + rsPay.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
							}
							else
							{
								// not found...
								MessageBox.Show("Payment for this was not found and not deleted.", "Payment Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							// set the eligibility back to be able to run the notices/apply fees again if needed
							do
							{
								// kgk rsBill.Edit   'can't edit a join
								// rsBill.Fields(strWS & "LienStatusEligibility") = 5
								// rsBill.Fields(strWS & "LienProcessStatus") = 4
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								rsPay.Execute("UPDATE Bill SET " + strWS + "LienStatusEligibility = 5, " + strWS + "LienProcessStatus = 4 WHERE ID = " + rsBill.Get_Fields("Bill"), modExtraModules.strUTDatabase);
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								if (rsBill.Get_Fields(strWS + "CombinationLienKey") == rsBill.Get_Fields("Bill"))
								{
									// rsBill.Fields("MaturityFee") = rsBill.Fields("MaturityFee") - dblFee
									// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
									rsPay.Execute("UPDATE Lien SET MaturityFee = " + FCConvert.ToString(rsBill.Get_Fields("MaturityFee") - dblFee) + " WHERE ID = " + rsBill.Get_Fields_Int32("LienKey"), modExtraModules.strUTDatabase);
								}
								// rsBill.Update True
								rsBill.MoveNext();
							}
							while (rsBill.EndOfFile() != true);
							MessageBox.Show("Account " + FCConvert.ToString(lngAcct) + " and ratekey " + cmbYear.Items[cmbYear.SelectedIndex].ToString() + " has been updated.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							this.Unload();
						}
						else
						{
							MessageBox.Show("Error finding Lien record, please try again.", "Missing Lien Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							cmbYear.Enabled = false;
							if (txtAccount.Enabled)
							{
								txtAccount.Focus();
							}
						}
					}
					else
					{
						if (cmbYear.Enabled)
						{
							MessageBox.Show("Please select a year.", "Data Input", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
						// fill the grid
						FillYearCombo();
						if (cmbYear.Items.Count > 0)
						{
							cmbYear.Enabled = true;
							cmbYear.Focus();
						}
						else
						{
							cmbYear.Enabled = false;
							if (txtAccount.Enabled)
							{
								txtAccount.Focus();
							}
						}
					}
				}
				else
				{
					// enter an account
					MessageBox.Show("Please enter a valid account.", "Valid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Removing Lien Mat Fees - " + FCConvert.ToString(intErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void mnuFilePrint_Click()
		{
			mnuFilePrint_Click(mnuFilePrint, new System.EventArgs());
		}

		private void optWS_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			cmbYear.Clear();
		}

		private void optWS_CheckedChanged(object sender, System.EventArgs e)
		{
			//short index = optWS.GetIndex((FCRadioButton)sender);
			int index = cmbWS.SelectedIndex;
			optWS_CheckedChanged(index, sender, e);
		}

		private void txtAccount_TextChanged(object sender, System.EventArgs e)
		{
			// turn this off so that the user will have to select the year each time
			// and I will have time to fill it for each account selected
			cmbYear.SelectedIndex = -1;
			cmbYear.Enabled = false;
		}

		private void FillYearCombo()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsYear = new clsDRWrapper();
				clsDRWrapper rsBillDate = new clsDRWrapper();
				int lngLastRK = 0;
				//if (optWS[0].Checked) {
				if (cmbWS.Text == "Water")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text)));
				lngAcctKey = modUTStatusPayments.GetAccountKeyUT(ref lngAcct);
				cmbYear.Clear();
				if (lngAcct > 0)
				{
					rsYear.OpenRecordset("SELECT Bill.ID, Lien.RateKey FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND MaturityFee <> 0 ORDER BY RateKey desc, Bill.ID");
					if (!rsYear.EndOfFile())
					{
						while (!rsYear.EndOfFile())
						{
							// MAL@20071218: Changed to refer to the Lien Rate Key instead of the original Billing Rate Key
							// Tracker Reference: 11675
							// rsBillDate.OpenRecordset "SELECT * FROM RateKeys WHERE RateKey = " & rsYear.Fields("BillingRateKey")
							// cmbYear.AddItem Format(rsYear.Fields("BillingRateKey"), "00000") & "  " & Format(rsBillDate.Fields("BillDate"))
							// cmbYear.ItemData(cmbYear.NewIndex) = rsYear.Fields("BillingRateKey")
							// MAL@20080310: Add check for rate key that is already in the list
							// Tracker Reference: 12626
							if (FCConvert.ToInt32(rsYear.Get_Fields_Int32("RateKey")) == lngLastRK)
							{
								rsYear.MoveNext();
							}
							else
							{
								rsBillDate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + rsYear.Get_Fields_Int32("RateKey"));
								cmbYear.AddItem(Strings.Format(rsYear.Get_Fields_Int32("RateKey"), "00000") + "  " + Strings.Format(rsBillDate.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy"));
								cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsYear.Get_Fields_Int32("RateKey")));
								lngLastRK = FCConvert.ToInt32(rsYear.Get_Fields_Int32("RateKey"));
								rsYear.MoveNext();
							}
						}
						cmbYear.Enabled = true;
					}
					else
					{
						MessageBox.Show("This account does not have any lien maturity fees applied.", "Valid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						cmbYear.Enabled = false;
						if (txtAccount.Enabled)
						{
							txtAccount.Focus();
						}
					}
				}
				else
				{
					MessageBox.Show("Please enter a valid account.", "Valid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					cmbYear.Enabled = false;
					if (txtAccount.Enabled)
					{
						txtAccount.Focus();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Combo", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void txtAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						// return
						// MAL@20080310: Fix the way this processes
						cmbYear.Clear();
						FillYearCombo();
						cmbYear.Enabled = true;
						// mnuFilePrint_Click
						break;
					}
				case Keys.Tab:
					{
						// mnuFilePrint_Click
						cmbYear.Clear();
						FillYearCombo();
						cmbYear.Enabled = true;
						break;
					}
			}
			//end switch
		}

		private void cmbWS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbWS.SelectedIndex == 0)
			{
				optWS_CheckedChanged(sender, e);
			}
		}
	}
}
