﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmHighestConsumptionReport.
	/// </summary>
	partial class frmHighestConsumptionReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCButton cmdPreview;
		public fecherFoundation.FCPanel fraChoice;
		public fecherFoundation.FCComboBox cboTop;
		public fecherFoundation.FCTextBox txtEndDate;
		public fecherFoundation.FCFrame fraRange;
		public fecherFoundation.FCTextBox txtStartDate;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmHighestConsumptionReport));
            this.cmbRange = new fecherFoundation.FCComboBox();
            this.lblRange = new fecherFoundation.FCLabel();
            this.cmdPreview = new fecherFoundation.FCButton();
            this.fraChoice = new fecherFoundation.FCPanel();
            this.cboTop = new fecherFoundation.FCComboBox();
            this.fraRange = new fecherFoundation.FCFrame();
            this.txtStartDate = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.txtEndDate = new fecherFoundation.FCTextBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraChoice)).BeginInit();
            this.fraChoice.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).BeginInit();
            this.fraRange.SuspendLayout();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 379);
            this.BottomPanel.Size = new System.Drawing.Size(599, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdPreview);
            this.ClientArea.Controls.Add(this.fraChoice);
            this.ClientArea.Size = new System.Drawing.Size(599, 319);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(599, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(329, 30);
            this.HeaderText.Text = "Highest Consumption Report";
            // 
            // cmbRange
            // 
            this.cmbRange.Items.AddRange(new object[] {
            "All Dates",
            "Range"});
            this.cmbRange.Location = new System.Drawing.Point(137, 90);
            this.cmbRange.Name = "cmbRange";
            this.cmbRange.Size = new System.Drawing.Size(154, 40);
            this.cmbRange.TabIndex = 3;
            this.cmbRange.Text = "All Dates";
            this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.cmbRange_SelectedIndexChanged);
            // 
            // lblRange
            // 
            this.lblRange.AutoSize = true;
            this.lblRange.Location = new System.Drawing.Point(30, 104);
            this.lblRange.Name = "lblRange";
            this.lblRange.Size = new System.Drawing.Size(51, 15);
            this.lblRange.TabIndex = 2;
            this.lblRange.Text = "RANGE";
            this.lblRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdPreview
            // 
            this.cmdPreview.AppearanceKey = "acceptButton";
            this.cmdPreview.ForeColor = System.Drawing.Color.White;
            this.cmdPreview.Location = new System.Drawing.Point(30, 248);
            this.cmdPreview.Name = "cmdPreview";
            this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPreview.Size = new System.Drawing.Size(112, 48);
            this.cmdPreview.TabIndex = 1;
            this.cmdPreview.Text = "Preview";
            this.cmdPreview.Click += new System.EventHandler(this.cmdPreview_Click);
            // 
            // fraChoice
            // 
            this.fraChoice.AppearanceKey = "groupBoxNoBorders";
            this.fraChoice.Controls.Add(this.cboTop);
            this.fraChoice.Controls.Add(this.cmbRange);
            this.fraChoice.Controls.Add(this.lblRange);
            this.fraChoice.Controls.Add(this.fraRange);
            this.fraChoice.Controls.Add(this.Label1);
            this.fraChoice.Name = "fraChoice";
            this.fraChoice.Size = new System.Drawing.Size(600, 224);
            this.fraChoice.TabIndex = 2;
            // 
            // cboTop
            // 
            this.cboTop.BackColor = System.Drawing.SystemColors.Window;
            this.cboTop.Location = new System.Drawing.Point(137, 30);
            this.cboTop.Name = "cboTop";
            this.cboTop.Size = new System.Drawing.Size(154, 40);
            this.cboTop.TabIndex = 1;
            // 
            // fraRange
            // 
            this.fraRange.AppearanceKey = "groupBoxNoBorders";
            this.fraRange.Controls.Add(this.txtStartDate);
            this.fraRange.Controls.Add(this.Label3);
            this.fraRange.Controls.Add(this.Label2);
            this.fraRange.Controls.Add(this.txtEndDate);
            this.fraRange.Location = new System.Drawing.Point(0, 132);
            this.fraRange.Name = "fraRange";
            this.fraRange.Size = new System.Drawing.Size(599, 86);
            this.fraRange.TabIndex = 4;
            // 
            // txtStartDate
            // 
            this.txtStartDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtStartDate.Enabled = false;
            this.txtStartDate.Location = new System.Drawing.Point(137, 20);
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(154, 40);
            this.txtStartDate.TabIndex = 1;
            this.txtStartDate.BackColor = Color.LightGray;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(310, 34);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(36, 18);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "END";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(30, 34);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(50, 18);
            this.Label2.TabIndex = 3;
            this.Label2.Text = "START";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtEndDate
            // 
            this.txtEndDate.BackColor = System.Drawing.SystemColors.Window;
            this.txtEndDate.Enabled = false;
            this.txtEndDate.Location = new System.Drawing.Point(401, 20);
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(154, 40);
            this.txtEndDate.TabIndex = 3;
            this.txtEndDate.BackColor = Color.LightGray;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(30, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(39, 19);
            this.Label1.TabIndex = 5;
            this.Label1.Text = "TOP";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePreview,
            this.mnuFileSeperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 0;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePreview.Text = "Print Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 1;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 2;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // frmHighestConsumptionReport
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(599, 487);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmHighestConsumptionReport";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Highest Consumption Report";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmHighestConsumptionReport_Load);
            this.Resize += new System.EventHandler(this.frmHighestConsumptionReport_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmHighestConsumptionReport_KeyDown);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraChoice)).EndInit();
            this.fraChoice.ResumeLayout(false);
            this.fraChoice.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRange)).EndInit();
            this.fraRange.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
