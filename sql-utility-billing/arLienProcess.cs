﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arLienProcess.
	/// </summary>
	public partial class arLienProcess : BaseSectionReport
	{
		public arLienProcess()
		{
			//
			// Required for Windows Form Designer support
			//
			this.Name = "Print Lien Notices";
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static arLienProcess InstancePtr
		{
			get
			{
				return (arLienProcess)Sys.GetInstance(typeof(arLienProcess));
			}
		}

		protected arLienProcess _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                if (rsData != null)
					rsData.Dispose();
                if (rsCMFNumbers != null)
					rsCMFNumbers.Dispose();
                if (rsCert != null)
					rsCert.Dispose();
                if (rsUT != null)
					rsUT.Dispose();
                if (rsRate != null)
					rsRate.Dispose();
                if (rsBook != null)
					rsBook.Dispose();
                if (rsMort != null)
					rsMort.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arLienProcess	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		const string strDefFontName = "Courier New";
		// kk02082016  Add font constants, can add option to set it in the future
		const int intDefFontSize = 10;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsCMFNumbers = new clsDRWrapper();
		string strSQL;
		string strText = "";
		// vbPorter upgrade warning: intYear As short --> As int	OnRead(string)
		short intYear;
		bool boolPayCert;
		bool boolMort;
		bool boolPayMortCert;
		bool boolPayNewOwner;
		bool boolAddressCO;
		clsDRWrapper rsCert;
		int lngCopies;
		// this is to keep track of the number of mortgage holder copies left
		bool boolCopy;
		// is this a copy or the original
		int intNewOwnerChoice;
		// this is if the user wants to send to any new owners 0 = Do not send any, 1 =
		clsDRWrapper rsMort = new clsDRWrapper();
		clsDRWrapper rsUT = new clsDRWrapper();
		clsDRWrapper rsRate = new clsDRWrapper();
		clsDRWrapper rsPayments = new clsDRWrapper();
		bool boolNewOwner;
		// this is true if there is a new owner
		bool boolAtLeastOneRecord;
		// this is true if there is at least one record to see
		string strAccountNumber = "";
		string strAcctList = "";
		clsDRWrapper rsBook = new clsDRWrapper();
		int lngArrayIndex;
		bool boolRemoveLastPage;
		bool boolSkipAccount;
		// vbPorter upgrade warning: strDateCheck As string	OnWrite(string, DateTime)
		DateTime strDateCheck;
		bool boolWater;
		string strWS = "";
		int lngLastAccountKey;
		bool boolCombinedBills;
		string strRKList = "";
		// Dim lngLastSigTop               As Long
		string str30DNQuery = "";
		double dblFilingFee;
		// these variables are for counting the accounts
		public int lngBalanceZero;
		public int lngBalanceNeg;
		public int lngBalancePos;
		public int lngDemandFeesApplied;
		string strBalanceZero = "";
		string strBalanceNeg = "";
		string strBalancePos = "";
		string strDemandFeesApplied = "";
		int lngCMFBillKey;
		string strAddressPrefix = "";
		int intCurrentIndex;
		bool ShowAtEnd = false;
        //FC:FINAL:SBE - #3984 - add flag to check if blank page was generated. If FetchData was not executed, empty page is generated. Then we have to delete it later
        private bool boolFetchDataExecuted = false;

        private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			//FC:FINAL:RPU:#i994 - Changed the name of field, because the "-" character is not allowed in the name
			//Fields.Add("Account-Copy");
			Fields.Add("AccountCopy");
			GroupHeader1.DataField = "AccountCopy";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
            //FC:FINAL:SBE - #3984 - add flag to check if blank page was generated. If FetchData was not executed, empty page is generated. Then we have to delete it later
            boolFetchDataExecuted = true;
			eArgs.EOF = intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1);
			if (eArgs.EOF)
			{
				EndReport();
			}
			else
			{
				if (boolCopy)
				{
					Fields["AccountCopy"].Value = FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey) + "-" + FCConvert.ToString(lngCopies);
				}
				else
				{
					Fields["AccountCopy"].Value = FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey) + "-0";
				}
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void EndReport()
		{
			// this will set all of the bill records to status of 1
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 1, FCConvert.ToString(lngBalanceZero));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 1, FCConvert.ToString(lngBalanceNeg));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 1, FCConvert.ToString(lngBalancePos));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 1, FCConvert.ToString(lngBalanceZero + lngBalanceNeg + lngBalancePos));
			// this will take the trailing comma off
			if (strBalanceZero.Length > 0)
			{
				strBalanceZero = Strings.Left(strBalanceZero, strBalanceZero.Length - 1);
			}
			if (strBalanceNeg.Length > 0)
			{
				strBalanceNeg = Strings.Left(strBalanceNeg, strBalanceNeg.Length - 1);
			}
			if (strBalancePos.Length > 0)
			{
				strBalancePos = Strings.Left(strBalancePos, strBalancePos.Length - 1);
			}
			if (strDemandFeesApplied.Length > 0)
			{
				strDemandFeesApplied = Strings.Left(strDemandFeesApplied, strDemandFeesApplied.Length - 1);
			}
			// set the strings of accounts for the return grid to show
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 2, strBalanceZero);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 2, strBalanceNeg);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 2, strBalancePos);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 2, strDemandFeesApplied);
			if (lngDemandFeesApplied > 0)
			{
				// show a special label that is highlighted to show this information
				// frmFreeReport.vsSummary.AddItem ""
				// frmFreeReport.vsSummary.AddItem ""
				if ((((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70 ) < (frmFreeReport.InstancePtr.fraSummary.HeightOriginal - 200))
				{
					frmFreeReport.InstancePtr.vsSummary.HeightOriginal = ((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70;
				}
				else
				{
					frmFreeReport.InstancePtr.vsSummary.HeightOriginal = frmFreeReport.InstancePtr.fraSummary.HeightOriginal - 200;
				}
			}

			frmWait.InstancePtr.Unload();
			frmFreeReport.InstancePtr.Focus();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modUTBilling.AcctInfo[] typAccountInfo = null;  // - "AutoDim"
			//FC:FINAL:DDU:#i1301 - set flag to show another report at end of this one
			ShowAtEnd = true;
			int intCounter = 0;
			int intDefSel = 0;
			string strTemp = "";
			boolWater = frmFreeReport.InstancePtr.boolWater;
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			intCurrentIndex = 0;
			frmWait.InstancePtr.Hide();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			// kk05082014 trout-1082  Select paper size beforehand since the Printer is selected later
			// kk09192014 trout-1082  Save and load the selection as the default, default to Letter size
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT", "LienPaperSize", ref strTemp);
			if (strTemp == "Legal")
			{
				intDefSel = 1;
			}
			else
			{
				intDefSel = 0;
			}
			frmQuestion.InstancePtr.Init(10, "Letter (8.5 x 11)", "Legal (8.5 x 14)", "Select Paper Size", intDefSel);
			if (modUTBilling.Statics.gintPassQuestion == 0)
			{
				strTemp = "Letter";
			}
			else
			{
				strTemp = "Legal";
			}
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT", "LienPaperSize", strTemp);
			if (modUTBilling.Statics.gintPassQuestion == 0)
			{
				FCGlobal.Printer.PaperSize = 1;
				// Letter size
			}
			else
			{
				FCGlobal.Printer.PaperSize = 5;
				// Legal size
			}
			frmQuestion.InstancePtr.Unload();
			//Application.DoEvents();
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			frmWait.InstancePtr.Show();
			// kk02042016 trout-1197  Margins effective 10/2015   1.5" top 1st page, 1.5" bottom last page, 0.75" left and right margins
			this.PageSettings.Margins.Top = (1.5F) + FCConvert.ToSingle(modMain.Statics.gdblLienAdjustmentTop);
			this.PageSettings.Margins.Bottom = (1.5F) + FCConvert.ToSingle(modMain.Statics.gdblLienAdjustmentBottom);
			// kk04212014 trocl-1156
			this.PageSettings.Margins.Left = (0.75F) + FCConvert.ToSingle(modMain.Statics.gdblLienAdjustmentSide);
			this.PageSettings.Margins.Right = this.PageSettings.Margins.Left;
			this.PrintWidth = (8.5F) - (this.PageSettings.Margins.Left + this.PageSettings.Margins.Right);
			fldHeader.Width = this.PrintWidth;
			rtbText.Width = this.PrintWidth;
			rtbText2.Width = this.PrintWidth;
			SetReportFont();
			this.Hide();
			lngBalanceNeg = 0;
			lngBalancePos = 0;
			lngBalanceZero = 0;
			lngDemandFeesApplied = 0;
			boolPayNewOwner = false;
			// corey 08/22/2007
			boolPayCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[21].RowNumber, 1), 2) == "Ye");
			boolMort = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 2) == "Ye");
			boolAddressCO = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[20].RowNumber, 1), 8) == "Owner at");
			// MAL@20080423: Check which address will need to print if user adds the Address fields to the notice
			// Tracker Reference:
			if (boolAddressCO)
			{
				strAddressPrefix = "O";
			}
			else
			{
				strAddressPrefix = "B";
			}
			if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[38].RowNumber, 1), 2) == "Ye")
			{
				if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[38].RowNumber, 1), 7) == "Yes, ch")
				{
					intNewOwnerChoice = 2;
					boolPayNewOwner = true;
				}
				else
				{
					intNewOwnerChoice = 1;
				}
			}
			else
			{
				intNewOwnerChoice = 0;
			}
			boolNewOwner = false;
			if (boolMort)
			{
				boolPayMortCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[34].RowNumber, 1), 9) == "Yes, char");
			}
			else
			{
				boolPayMortCert = false;
			}
			if (frmFreeReport.InstancePtr.chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				// make sure that the recommitment variables are set with whatever the user typed in
				modRhymalReporting.Statics.frfFreeReport[35].DatabaseFieldName = frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(0, 1);
				if (Information.IsDate(frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1)))
				{
					//FC:FINAL:MSH - i.issue #1005: incorrect date format
					modRhymalReporting.Statics.frfFreeReport[36].DatabaseFieldName = Strings.Format(frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1), "MMMM d yyyy");
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1005: incorrect date format
					modRhymalReporting.Statics.frfFreeReport[36].DatabaseFieldName = Strings.Format(DateTime.Today, "MMMM d, yyyy");
				}
			}
			// this will add a picture of the signature of the treasurer to the 30 Day Notice
			if (modMain.Statics.gboolUseSigFile)
			{
				imgSig.Visible = true;
				// MAL@20071018: Updated placement of signature image
				//rtbText.ZOrder(0);
				imgSig.SizeMode = GrapeCity.ActiveReports.SectionReportModel.SizeModes.Zoom;
				//imgSig.ZOrder(1);
				// Line2.ZOrder 0
				imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrUtilitySigPath);
			}
			else
			{
				imgSig.Visible = false;
			}
			rsUT.OpenRecordset("SELECT Master.ID, Apt, StreetNumber, StreetName, MapLot, UseREAccount, REAccount, OwnerPartyID, " 
			                   + "DeedName1 AS OwnerName, DeedName2 AS SecondOwnerName, " + "pOwn.Address1 AS OAddress1, pOwn.Address2 AS OAddress2, pOwn.Address3 AS OAddress3, " + "pOwn.City AS OCity, pOwn.State AS OState, pOwn.Zip AS OZip " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID " + "LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID");
			rsRate.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
			strSQL = SetupSQL();
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (rsData.EndOfFile() != true)
			{
				// lngMaxNumber = rsData.RecordCount
				rsCMFNumbers.OpenRecordset("SELECT * FROM CMFNumbers", modExtraModules.strUTDatabase);
				modUTBilling.Statics.typAccountInfo = new modUTBilling.AcctInfo[0 + 1];
				modUTBilling.Statics.typAccountInfo[0].AccountKey = rsData.Get_Fields_Int32("AccountKey");
				if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "LIEN"))
				{
					modMain.GetLatestOwnerInformation_59022(rsData.Get_Fields_Int32("AccountKey"), "LIEN", ref modUTBilling.Statics.typAccountInfo[0].AccountName, "", "", "", "", "", "", "");
				}
				else
				{
					modUTBilling.Statics.typAccountInfo[0].AccountName = rsData.Get_Fields_String("OName");
				}
				intCounter = 1;
				rsData.MoveNext();
				while (rsData.EndOfFile() != true)
				{
					if (modUTBilling.Statics.typAccountInfo[intCounter - 1].AccountKey != rsData.Get_Fields_Int32("AccountKey"))
					{
						Array.Resize(ref modUTBilling.Statics.typAccountInfo, intCounter + 1);
						modUTBilling.Statics.typAccountInfo[intCounter].AccountKey = rsData.Get_Fields_Int32("AccountKey");
						if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "LIEN"))
						{
							modMain.GetLatestOwnerInformation_59022(rsData.Get_Fields_Int32("AccountKey"), "LIEN", ref modUTBilling.Statics.typAccountInfo[intCounter].AccountName, "", "", "", "", "", "", "");
						}
						else
						{
							modUTBilling.Statics.typAccountInfo[intCounter].AccountName = rsData.Get_Fields_String("OName");
						}
						intCounter += 1;
					}
					rsData.MoveNext();
				}
				rsData.MoveFirst();
				if (frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account")
				{
					// do nothing the data is already in account number order
				}
				else
				{
					modUTBilling.strSort_24(modUTBilling.Statics.typAccountInfo, true, true);
				}
				modGlobalConstants.Statics.blnHasRecords = true;
			}
			else
			{
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("No eligible accounts were found.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
				modGlobalConstants.Statics.blnHasRecords = false;
				return;
			}
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, rsData.RecordCount());
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToDouble(
		private short FindLineNumber()
		{
			short FindLineNumber = 0;
			// this will attempt to find the number of lines to the top of the signature line
			// by counting the number of soft and hard returns until the signature line is found
			string strText;
			int lngLines;
			int lngSigLine = 0;
			string strSigText;
			int lngDiff = 0;
			int intCharsPerLine;
			int lngSoftReturns = 0;
			double dblSigAdjust;
			string strTemp = "";
			strText = rtbText.Text;
			// MAL@20071018: Adjusted for longer town names
			// Call Reference: 117168
			strSigText = "Demand Fees       :";
			// strSigText = "Certified Mail Fee:"
			// 
			intCharsPerLine = 80;
			lngLines = 0;
			// push the sig file onto the line
			do
			{
				lngDiff = Strings.InStr(1, strText, "\r\n", CompareConstants.vbBinaryCompare);
				// find the next hard return
				lngSigLine = Strings.InStr(1, strText, strSigText, CompareConstants.vbBinaryCompare);
				// find the signature line
				if (lngDiff == 0)
				{
					lngLines += (strText.Length / intCharsPerLine);
					// this counts soft returns
				}
				else
				{
					if (lngSigLine > lngDiff)
					{
						// if the signature line is closer than the next hard return
						lngSoftReturns = (lngDiff / intCharsPerLine);
						// this counts soft returns
						lngLines += lngSoftReturns;
						if (lngDiff > intCharsPerLine && lngDiff % intCharsPerLine < 10)
						{
							// do not add a hard return because it already has calculated for it
							// Stop
						}
						else
						{
							lngLines += 1;
							// this will add the hard return
						}
					}
					else
					{
						lngLines += (lngSigLine / intCharsPerLine);
						// this counts soft returns
						lngDiff = 0;
						// this will end the loop
					}
					strText = Strings.Right(strText, strText.Length - (lngDiff + 1));
					// remove the text that I have already counted
				}
			}
			while (!(lngDiff == 0));
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "SignatureAdjustLien", ref strTemp);
			dblSigAdjust = FCConvert.ToDouble(strTemp);
			FindLineNumber = FCConvert.ToInt16(lngLines + dblSigAdjust);
			return FindLineNumber;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			DateTime dtDate;
			// this will create another active report with the summary information on it so the user can print if the feel like it
			//FC:FINAL:DDU:#i1301 - show report by variable, not when form is closed already all time
			//if (frmReportViewer.InstancePtr.Visible)
			if (ShowAtEnd)
			{
				ShowAtEnd = false;
				if (Information.IsDate(strDateCheck))
				{
					dtDate = strDateCheck;
				}
				else
				{
					dtDate = DateTime.Today;
				}
                //rptLienNoticeSummary.InstancePtr.Unload();
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                Int32 tempRepType = 1;
                rptLienNoticeSummary.InstancePtr.Init(ref strAcctList, "Lien Notice Summary", "", ref intYear, ref dtDate, ref boolWater, FCConvert.CBool(frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account"), 1, ref str30DNQuery, ref boolAddressCO);
            }
			frmFreeReport.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			//FC:FINAL:SBE - #3984 - move code in Page_Start
            // kk04282016 remove the previous page is it is blank
			//if (boolSkipAccount)
			//{
			//	if (this.Document.Pages.Count > 0)
			//	{
			//		this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
			//	}
			//}
			BindFields();
		}

		private void BindFields()
		{
			int intLines;
			string strText = "";
			int intSigStart = 0;
			int intSigEnd = 0;

			if (intCurrentIndex <= Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
			{
				boolSkipAccount = false;
				// kk04282016 trout-1228
				// calculate fields
				CalculateVariableTotals();
				if (!boolSkipAccount)
				{
					// kk04282016 trout-1228
					// If rsData.EndOfFile Then Exit Sub
					if (intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
						return;
					frmWait.InstancePtr.IncrementProgress();
					strText = Strings.RTrim(frmFreeReport.InstancePtr.rtbData.Text);
					intSigStart = Strings.InStr(strText, "<SIGNATURELINE>", CompareConstants.vbBinaryCompare);
					if (intSigStart > 0)
					{
						intSigEnd = intSigStart + "<SIGNATURELINE>".Length + 2;
						rtbText.SetHtmlText(SetupVariablesInString_2(Strings.Left(strText, intSigStart - 3)));
						// The first half, to just before "CrLf<SIGNATURELINE>"
						SetupNonRTFCostBreakdownAndSignature();
						rtbText2.SetHtmlText(Strings.Trim(SetupVariablesInString_2(Strings.Mid(strText, intSigEnd))));
						// The second half, after "<SIGNATURELINE>CrLf"
					}
					else
					{
						rtbText.SetHtmlText(SetupVariablesInString(ref strText));
					}
					if (modMain.Statics.gboolUseSigFile)
					{
						// kk02052016 trout-1197  Rework lien notice. Change the way signature line is located.
						// The adjustments are applied before the richtext section is expanded, so we have to limit the adjustment amount.
						// If the signature moves into the rtbText area or above it, the signature will not be pushed down when the rtbText is expanded.
						if (modMain.Statics.gdblLienSigAdjust < 0)
						{
							// The digital signature starts out directly below the rich text box
							imgSig.Top = rtbText.Top + rtbText.Height;
							// Don't allow it to go any higher
						}
						else
						{
							if (imgSig.Top + (modMain.Statics.gdblLienSigAdjust) < Line2.Y1)
							{
								// The farthest it can move is so the top is even with the signature line
								imgSig.Top += FCConvert.ToSingle(modMain.Statics.gdblLienSigAdjust);
							}
							else
							{
								imgSig.Top = Line2.Y1;
							}
						}
					}
					if (lngCopies > 1)
					{
						// this is to produce multiple copies for certain parties (mortgage holders)
						lngCopies -= 1;
						boolCopy = true;
					}
					else
					{
						// rsData.MoveNext
						intCurrentIndex += 1;
						boolCopy = false;
					}
				}
				else
				{
					// kk04282016 trout-1228 Change handling of skipped accounts to get rid of GOTOs
					intCurrentIndex += 1;
					boolCopy = false;
					LayoutAction = GrapeCity.ActiveReports.LayoutAction.NextRecord;
					// Don't render the rest of the page
					//Canvas.Clear(); // Worst case we print a blank page
				}
			}
		}

		private void SetHeader()
		{
			string strTemp = "";
			if (boolWater)
			{
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					// kk06232015 trouts-154
					strTemp = "STORMWATER LIEN CERTIFICATE";
				}
				else
				{
					strTemp = "WATER LIEN CERTIFICATE";
				}
			}
			else
			{
				strTemp = "SEWER LIEN CERTIFICATE";
			}
			if (boolCopy)
			{
				strTemp = " * * * COPY * * *  " + Strings.Trim(strTemp) + "  * * * COPY * * * ";
			}
			fldHeader.Text = "STATE OF MAINE" + "\r\n" + GetVariableValue_2("UTILITYTITLE") + "\r\n" + strTemp + "\r\n" + GetVariableValue_2("LEGALDESCRIPTION");
		}

		private string SetupSQL()
		{
			string SetupSQL = "";
			// this will return the SQL statement for this batch of reports
			string strWhereClause = "";
			int intCT;
			string strOrderBy = "";
			// MAL@20080702: Added new sort order option
			// Tracker Reference: 11834/14183
			if (frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account")
			{
				strOrderBy = " ORDER BY ActualAccountNumber, OName, BillingRateKey desc";
			}
			else
			{
				strOrderBy = " ORDER BY OName, ActualAccountNumber, BillingRateKey desc";
			}
			if (Strings.Trim(frmRateRecChoice.InstancePtr.strRateKeyList) != "")
			{
				strRKList = " AND BillingRateKey IN " + frmRateRecChoice.InstancePtr.strRateKeyList;
			}
			else
			{
				strRKList = "";
			}
			if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Account")
			{
				// range of accounts
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
					}
				}
			}
			else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
			{
				// range of names
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
					}
					else
					{
						// first full second empty
						strWhereClause = " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "'";
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "'";
					}
					else
					{
						// both empty
						strWhereClause = "";
					}
				}
			}
			else
			{
				strWhereClause = "";
			}
			modMain.Statics.gstrProcessLienQuery = "SELECT *, ID AS Bill FROM Bill WHERE BillStatus = 'B' AND NOT (LienProcessExclusion = 1) AND (" + strWS + "LienProcessStatus = 2 OR " + strWS + "LienProcessStatus = 3) AND (" + strWS + "LienStatusEligibility = 3 OR " + strWS + "LienStatusEligibility = 4) " + strWhereClause + strRKList;
			// kgk     & " " & strOrderBy
			str30DNQuery = "SELECT *, ID AS Bill FROM Bill WHERE BillStatus = 'B' AND NOT (LienProcessExclusion = 1) AND (" + strWS + "LienProcessStatus = 2 OR " + strWS + "LienProcessStatus = 3) AND (" + strWS + "LienStatusEligibility = 3 OR " + strWS + "LienStatusEligibility = 4) " + strWhereClause + strRKList;
			// kgk     & " " & strOrderBy
			SetupSQL = "SELECT * FROM (" + modMain.Statics.gstrProcessLienQuery + ") AS qTmpY ORDER BY ActualAccountNumber";
			return SetupSQL;
		}

		private string SetupVariablesInString_2(string strOriginal)
		{
			return SetupVariablesInString(ref strOriginal);
		}

		private string SetupVariablesInString(ref string strOriginal)
		{
			string SetupVariablesInString = "";
			// this will replace all of the variables with the information needed
			string strBuildString;
			// this is the string that will be built and returned at the end
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			lngEndOfLastVariable = 0;
			lngNextVariable = 1;
			strBuildString = "";
			// priming read
			if (Strings.InStr(1, strOriginal, "<", CompareConstants.vbBinaryCompare) > 0)
			{
				lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<", CompareConstants.vbBinaryCompare);
				// do until there are no more variables left
				do
				{
					// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
					strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
					// set the end pointer
					lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">", CompareConstants.vbBinaryCompare);
					// replace the variable
					strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
					// check for another variable
					lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<", CompareConstants.vbBinaryCompare);
				}
				while (!(lngNextVariable == 0));
			}
			// take the last of the string and add it to the end
			strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
			// check for variables
			if (Strings.InStr(1, "<", strOriginal, CompareConstants.vbBinaryCompare) > 0)
			{
				// strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
				MessageBox.Show("ERROR: There are still variables in the report string.", "SetupVariablesInString Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else
			{
				SetupVariablesInString = strBuildString;
			}
			return SetupVariablesInString;
		}

		private string SetupCostBreakdownAndSignatureLine()
		{
			string SetupCostBreakdownAndSignatureLine = "";
			// this will setup the bottom part of the detail section
			string strLineText;
			double dblTotal;
			string strCollector = "";
			double dblDemand = 0;
			double dblCertTotal = 0;
			if (frmFreeReport.InstancePtr.chkRecommitment.Checked)
			{
				strCollector = GetVariableValue_2("NEWCOLLECTOR");
			}
			else
			{
				strCollector = GetVariableValue_2("COLLECTOR");
			}
			if (Conversion.Val(GetVariableValue_2("DEMAND")) != 0)
			{
				dblDemand = FCConvert.ToDouble(GetVariableValue_2("DEMAND"));
			}
			else
			{
				dblDemand = 0;
			}
			if (Conversion.Val(GetVariableValue_2("CERTTOTAL")) != 0)
			{
				dblCertTotal = FCConvert.ToDouble(GetVariableValue_2("CERTTOTAL"));
			}
			else
			{
				dblCertTotal = 0;
			}
			dblTotal = dblDemand + dblFilingFee + dblCertTotal + Conversion.Val(GetVariableValue_2("PRINCIPAL").Trim('$')) + Conversion.Val(GetVariableValue_2("INTEREST").Trim('$')) + Conversion.Val(GetVariableValue_2("TAX").Trim('$'));
			// MAL@20080221: Add Tax : Tracker Reference: 12107 '- rsData.Fields(strWS & "PrinPaid")  'AMSD 05/03/2006 #93473
			strLineText = "Costs to be paid:" + "\r\n";
			strLineText += "Filing Fee, Recording Fee and" + "\r\n";
			strLineText += "Discharging Lien  : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblFilingFee, "$#,##0.00"), 11, true) + "\r\n";
			strLineText += "Demand Fees       : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblDemand, "$#,##0.00"), 11, true) + "\r\n";
			strLineText += "Certified Mail Fee: " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblCertTotal, "$#,##0.00"), 11, true) + "\r\n";
			// XXXX     & "          _________________________" & vbCrLf
			strLineText += "Principal         : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(GetVariableValue_2("PRINCIPAL"), "$#,##0.00"), 11, true) + "\r\n";
			// XXXX     & "          " & strCollector & vbCrLf     '- rsData.Fields(strWS & "PrinPaid")  Not sure why this was in here, but it is taken out now
			strLineText += "Tax               : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(GetVariableValue_2("TAX"), "$#,##0.00"), 11, true) + "\r\n";
			// XXXX     & "          " & GetVariableValue("MUNI") & vbCrLf       'MAL@20080221: Tracker Reference: 12107
			strLineText += "Interest          : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(GetVariableValue_2("INTEREST"), "$#,##0.00"), 11, true) + "\r\n";
			// & "          " & GetVariableValue("MUNI") & vbCrLf         ''& "          Tax Collector" & vbCrLf
			strLineText += "                    " + Strings.StrDup(11, "-") + "\r\n";
			// & "          " & GetVariableValue("MUNI") & vbCrLf
			strLineText += "Total             : " + modGlobalFunctions.PadStringWithSpaces(Strings.Format(dblTotal, "$#,##0.00"), 11, true);
			SetupCostBreakdownAndSignatureLine = strLineText;
			return SetupCostBreakdownAndSignatureLine;
		}

		private string SetupNonRTFCostBreakdownAndSignature()
		{
			string SetupNonRTFCostBreakdownAndSignature = "";
			// this will setup the bottom part of the detail section
			string strLineText = "";
			double dblTotal;
			string strCollector = "";
			string strTitle;
			double dblDemand = 0;
			string strCityTown;
			double dblCertTotal = 0;
			if (frmFreeReport.InstancePtr.chkRecommitment.Checked)
			{
				strCollector = GetVariableValue_2("NEWCOLLECTOR");
			}
			else
			{
				strCollector = GetVariableValue_2("COLLECTOR");
			}
			strTitle = GetVariableValue_2("TITLE");
			if (Conversion.Val(GetVariableValue_2("DEMAND")) != 0)
			{
				dblDemand = FCConvert.ToDouble(GetVariableValue_2("DEMAND"));
			}
			else
			{
				dblDemand = 0;
			}
			if (Conversion.Val(GetVariableValue_2("FILINGFEE")) != 0)
			{
				dblFilingFee = FCConvert.ToDouble(GetVariableValue_2("FILINGFEE"));
			}
			else
			{
				dblFilingFee = 0;
			}
			if (Conversion.Val(GetVariableValue_2("CERTTOTAL")) != 0)
			{
				dblCertTotal = FCConvert.ToDouble(GetVariableValue_2("CERTTOTAL"));
			}
			else
			{
				dblCertTotal = 0;
			}
			strCityTown = GetVariableValue_2("CITYTOWNOF");
			// dblTotal = dblDemand + dblFilingFee + dblCertTotal + CDbl(GetVariableValue("PRINCIPAL")) - rsData.Fields("PrincipalPaid") + CDbl(GetVariableValue("INTEREST"))
			//FC:FINAL:DDU:#i1300 - fixed conversion errors
			dblTotal = dblDemand + dblFilingFee + dblCertTotal + Conversion.Val(GetVariableValue_2("PRINCIPAL").Trim('$')) + Conversion.Val(GetVariableValue_2("INTEREST").Trim('$')) + Conversion.Val(GetVariableValue_2("TAX").Trim('$'));
			// MAL@20080221: Add Tax : Tracker Reference: 12107 '- rsData.Fields(strWS & "PrinPaid")  'AMSD 05/03/2006 #93473
			// Statutory Fees and Mailing Costs
			fldCosts.Text = Strings.Format(dblFilingFee + dblDemand + dblCertTotal, "$#,##0.00");
			// Principal
			fldPrincipal.Text = Strings.Format(GetVariableValue_2("PRINCIPAL"), "$#,##0.00");
			// - rsData.Fields(strWS & "PrinPaid")  Not sure why this was in here, but it is taken out now
			// Interest
			fldInterest.Text = Strings.Format(GetVariableValue_2("INTEREST"), "$#,##0.00");
			// Total
			fldTotal.Text = Strings.Format(dblTotal, "$#,##0.00");
			fldCollector.Text = strCollector;
			fldTitle.Text = strTitle;
			if (Strings.Trim(strCityTown) != "")
			{
				fldMuni.Text = Strings.Trim(strCityTown) + " of " + GetVariableValue_2("MUNI");
			}
			else
			{
				fldMuni.Text = GetVariableValue_2("MUNI");
			}
			return SetupNonRTFCostBreakdownAndSignature;
		}

		private string GetVariableValue_2(string strVarName)
		{
			return GetVariableValue(ref strVarName);
		}

		private string GetVariableValue(ref string strVarName)
		{
			string GetVariableValue = "";
			// this function will take a variable name and find it in the array, then get the value and return it as a string
			int intCT;
			string strValue = "";
			// this is a dummy code to show the user where the hard codes are
			if (strVarName == "CRLF")
			{
				GetVariableValue = "";
				return GetVariableValue;
			}
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
				{
					switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
					{
						case 0:
							{
								// value from the static variables in the grid
								switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
								{
								// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
									case 0:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 1:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
											break;
										}
									case 2:
										{
											//FC:FINAL:MSH - i.issue #1005: incorrect date format
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM d, yyyy");
											break;
										}
									case 3:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 4:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
								}
								//end switch
								break;
								break;
							}
						case 1:
							{
								// value from the dynamic variables in the database
								// MAL@20080423: Test for address variables and add appropriate prefix
								// Tracker Reference:
								if (Strings.UCase(Strings.Left(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName, 4)) == "ADDR")
								{
									strValue = FCConvert.ToString(rsData.Get_Fields(strAddressPrefix + modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
								}
								else
								{
									strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
								}
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								break;
								break;
							}
						case 2:
							{
								// questions at the bottom of the grid
								strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
								break;
								break;
							}
						case 3:
							{
								// calculated values that are stored in the DatabaseFieldName field
								strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS" && Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "CITYTOWNOF" && Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "ADDRESS1" && Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "ADDRESS2" && Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "ADDRESS3")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								break;
								break;
							}
					}
					//end switch
				}
			}
			GetVariableValue = strValue;
			return GetVariableValue;
		}

		private void CalculateVariableTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will calculate all of the fields that need it and store
				// the value in the DatabaseFieldName string in the frfFreeReport
				// struct that these fields include principal, interest, costs,
				// total due and certified mail fee
				//clsDRWrapper rsSetCert;
				clsDRWrapper rsCombineBills = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				double dblTotalDue = 0;
				double dblInt = 0;
				double dblPrin = 0;
				double dblCost = 0;
				double dblTax = 0;
				double dblPayments = 0;
				double dblDemand = 0;
				double dblCertMailFee = 0;
				int lngMortHolder = 0;
				string strAddressBar = "";
				string strBillDate = "";
				string strLessPaymentsLine = "";
				string strLocation = "";
				string strTemp = "";
				string strTemp2 = "";
				string strTemp3 = "";
				string strTemp4 = "";
				string strTemp5 = "";
				string strOwnerName = "";
				string strMapLot = "";
				string strBookPage = "";
				string strBP = "";
				string strTrueFalse = "";
				string strWS = "";
				double dblBillDue = 0;
				double dblBillInt = 0;
				double dblBillCurInt = 0;
				string strCommissionExpirationDate = "";
				// Latest Tenant/Owner Information
				string strLastOwner;
				string strLastOwner2;
				string strLastAddress;
				string strLastAddress2;
				string strLastAddress3;
				string strLastCity;
				string strLastState;
				string strLastZip;
				// for CMFNumbers
				string strCMFName = "";
				int lngCMFMHNumber = 0;
				bool boolCMFNewOwner;
				bool boolPrintCMF;
				// true if a CMF should be printed for that person
				string strCMFAddr1;
				string strCMFAddr2;
				string strCMFAddr3;
				string strCMFAddr4;
				int lngBillCount = 0;
				string strBillList = "";
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				TRYAGAIN:
				;
				strCMFAddr1 = "";
				strCMFAddr2 = "";
				strCMFAddr3 = "";
				strCMFAddr4 = "";
				strLastOwner = "";
				strLastOwner2 = "";
				strLastAddress = "";
				strLastAddress2 = "";
				strLastAddress3 = "";
				strLastCity = "";
				strLastState = "";
				strLastZip = "";
				// MAL@20070927: Check for multiple owners and get the latest
				// Changed code from this point forward to refer to new string variables rather then rsData.Fields
				rsData.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrProcessLienQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey));
				if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "LIEN"))
				{
					modMain.GetLatestOwnerInformation(rsData.Get_Fields_Int32("AccountKey"), "LIEN", ref strLastOwner, ref strLastOwner2, ref strLastAddress, ref strLastAddress2, ref strLastAddress3, ref strLastCity, ref strLastState, ref strLastZip);
				}
				else
				{
					strLastOwner = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName")));
					strLastOwner2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName2")));
					strLastAddress = FCConvert.ToString(rsData.Get_Fields_String("OAddress1"));
					strLastAddress2 = FCConvert.ToString(rsData.Get_Fields_String("OAddress2"));
					strLastAddress3 = FCConvert.ToString(rsData.Get_Fields_String("OAddress3"));
					strLastCity = FCConvert.ToString(rsData.Get_Fields_String("OCity"));
					strLastState = FCConvert.ToString(rsData.Get_Fields_String("OState"));
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OZip4"))) != "")
					{
						strLastZip = rsData.Get_Fields_String("OZip") + "-" + rsData.Get_Fields_String("OZip4");
					}
					else
					{
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OZip4"))) != "")
						{
							strLastZip = rsData.Get_Fields_String("OZip") + "-" + rsData.Get_Fields_String("OZip4");
						}
						else
						{
							strLastZip = FCConvert.ToString(rsData.Get_Fields_String("OZip"));
						}
					}
				}
				if (!boolCopy)
				{
					dblTotalDue = 0;
					boolNewOwner = false;
					strDateCheck = Convert.ToDateTime(GetVariableValue_2("FILINGDATE"));
					if (Strings.Trim(FCConvert.ToString(strDateCheck)) == "")
					{
						strDateCheck = DateTime.Today;
					}
					if (Conversion.Val(GetVariableValue_2("FILINGFEE")) != 0)
					{
						dblFilingFee = FCConvert.ToDouble(GetVariableValue_2("FILINGFEE"));
					}
					else
					{
						dblFilingFee = 0;
					}
					strCommissionExpirationDate = GetVariableValue_2("COMMISSIONEXPIRATIONDATE");
					if (boolCombinedBills)
					{
						lngLastAccountKey = modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey;
						strBillList = "";
						// End If
					}
					else
					{
						// set this back since it is a new account
						// lngLastAccountKey = rsData.Fields("AccountKey")
						lngLastAccountKey = modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey;
						boolCombinedBills = false;
					}
					lngBillCount = 0;
					rsCombineBills.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrProcessLienQuery + ") AS qTmpY WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
					if (rsCombineBills.RecordCount() > 1)
					{
						boolCombinedBills = true;
						while (!rsCombineBills.EndOfFile())
						{
							// this will calculate the principal, interest and total due
							dblBillInt = 0;
							dblBillCurInt = 0;
                            if (FCConvert.ToInt32(rsCombineBills.Get_Fields(strWS + "LienRecordNumber")) == 0)
                            {
                                dblBillDue = modUTCalculations.CalculateAccountUT(rsCombineBills, ref strDateCheck,
                                    ref dblBillCurInt, boolWater, ref dblBillInt);
                            }
                            
                            if (dblBillDue > 0)
							{
								// count this bill
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								strAcctList += rsCombineBills.Get_Fields("Bill") + ",";
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								strBillList += rsCombineBills.Get_Fields("Bill") + ",";
								lngBillCount += 1;
								dblInt += dblBillInt;
								dblTotalDue += dblBillDue;
							}
							rsCombineBills.MoveNext();
						}
						if (lngBillCount > 1)
						{
							boolCombinedBills = true;
						}
						else
						{
							boolCombinedBills = false;
						}
					}
					else
					{
						lngBillCount = 1;
						boolCombinedBills = false;
					}
					if (!boolCombinedBills)
                    {
                        dblInt = 0;
						// this will calculate the principal, interest and total due on a single bill
                        if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                        {
                            // if a blank date is returned, then this line will crash
                            dblTotalDue = modUTCalculations.CalculateAccountUT(rsData, ref strDateCheck, ref dblInt,
                                boolWater, ref dblBillCurInt);
                            dblInt = dblInt;
                        }                        
                    }
					if (FCUtils.Round(dblTotalDue, 2) == 0 || (frmFreeReport.InstancePtr.dblMinimumAmount > 0 && FCUtils.Round(dblTotalDue, 2) < frmFreeReport.InstancePtr.dblMinimumAmount))
					{
						lngBalanceZero += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strBalanceZero += rsData.Get_Fields("Bill") + ", ";
						boolSkipAccount = true;
						return;
					}
					if (rsData.Get_Fields(strWS + "LienProcessStatus") > 3)
					{
						boolSkipAccount = true;
						return;
					}
					if (dblTotalDue > 0)
					{
						// these are the account that will be demanded
						lngBalancePos += 1;
						if (!boolCombinedBills)
						{
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							strAcctList += rsData.Get_Fields("Bill") + ",";
						}
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strBalancePos += rsData.Get_Fields("Bill") + ", ";
					}
					else if (dblTotalDue < 0)
					{
						lngBalanceNeg += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strBalanceNeg += rsData.Get_Fields("Bill") + ", ";
						boolSkipAccount = true;
						return;
					}
					// this will find out how many mortgage holders this account has and how many copies to print
					if (boolMort)
					{
						lngMortHolder = CalculateMortgageHolders();
						lngCopies = lngMortHolder + 1;
					}
					else
					{
						lngMortHolder = 0;
						lngCopies = 1;
					}
					// this will find out the certified mail fee
					if (boolPayCert)
					{
						lngMortHolder += 1;
						if (boolPayMortCert)
						{
							dblCertMailFee = lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
						else
						{
							dblCertMailFee = FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
					}
					else
					{
						if (boolPayMortCert)
						{
							dblCertMailFee = lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
						else
						{
							dblCertMailFee = 0;
						}
					}
					// check to see if one is going to be sent to the next owner
					bool boolUTMatch = false;
                    //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
					//if (intNewOwnerChoice != 0 && modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
					if (intNewOwnerChoice != 0 & modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
					{
						// if there has been a new owner, then make a copy for the new owner as well
						if (!boolNewOwner)
						{
							if (boolPayNewOwner)
							{
								// charge for the new owner's copy
								dblCertMailFee += FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
							}
							lngCopies += 1;
							boolNewOwner = true;
						}
					}
					if (Strings.Trim(strLastOwner2) != "")
					{
						strOwnerName = strLastOwner + " and " + strLastOwner2;
					}
					else
					{
						strOwnerName = strLastOwner;
					}
					dblBillInt = 0;
					if (boolCombinedBills)
					{
						rsCombineBills.MoveFirst();
						while (!rsCombineBills.EndOfFile())
						{
							// this will cycle through the bills and find out how much is owed for all of them and roll it into the lien
							dblPrin += rsCombineBills.Get_Fields(strWS + "PrinOwed");
							dblPayments += rsCombineBills.Get_Fields(strWS + "PrinPaid");
							dblBillInt += rsCombineBills.Get_Fields(strWS + "IntOwed") + (rsCombineBills.Get_Fields(strWS + "IntAdded") * -1) - rsCombineBills.Get_Fields(strWS + "IntPaid");
							dblCost += rsCombineBills.Get_Fields(strWS + "CostOwed") - rsCombineBills.Get_Fields(strWS + "CostAdded") - rsCombineBills.Get_Fields(strWS + "CostPaid");
							dblTax += rsCombineBills.Get_Fields(strWS + "TaxOwed") - rsCombineBills.Get_Fields(strWS + "TaxPaid");
							rsCombineBills.MoveNext();
						}
						// allow dblInt to stay the same (ie calculated from above)
					}
					else
					{
						dblPrin = rsData.Get_Fields(strWS + "PrinOwed");
						// + rsData.Fields(strWS & "TaxOwed")
						dblPayments = rsData.Get_Fields(strWS + "PrinPaid");
						dblInt = rsData.Get_Fields(strWS + "IntOwed") + (rsData.Get_Fields(strWS + "IntAdded") * -1) - rsData.Get_Fields(strWS + "IntPaid") + dblInt;
						dblCost = rsData.Get_Fields(strWS + "CostOwed") - rsData.Get_Fields(strWS + "CostAdded") - rsData.Get_Fields(strWS + "CostPaid");
						dblTax = rsData.Get_Fields(strWS + "TaxOwed") - rsData.Get_Fields(strWS + "TaxPaid");
					}
					// Less Payments Line
					if (FCUtils.Round(dblPayments, 2) != 0)
					{
						// only show this when there are payments
						if (dblPayments < 0)
						{
							// if the principal paid is a negative number then some adjustment has happened (supplemental) this should not happen later in the code life
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsData.Get_Fields("Bill") + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')", modExtraModules.strUTDatabase);
							if (rsPayments.EndOfFile())
							{
								strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
							else
							{
								strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
						}
						else
						{
							// principal paid is greater then zero
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsData.Get_Fields("Bill") + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')", modExtraModules.strUTDatabase);
							if (rsPayments.EndOfFile())
							{
								strLessPaymentsLine = "less payment and adjustment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
							else
							{
								strLessPaymentsLine = "less payment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
						}
					}
					else
					{
						strLessPaymentsLine = "";
					}
					strBookPage = "";
					if (FCConvert.ToString(rsData.Get_Fields_String("Bookpage")) != "")
					{
						strBP = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Bookpage")));
						strBookPage = strBP;
					}
					// Location
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					strLocation = Strings.Trim(Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("StreetNumber")))) + " " + rsUT.Get_Fields_String("StreetName");
					// MapLot
					strMapLot = Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("MAPLOT")));
					if (Strings.Trim(strMapLot) == "")
					{
						strMapLot = "________________";
					}
					if (Strings.Trim(strLocation) == "")
					{
						strLocation = "________________";
					}
					if (Strings.Trim(strBookPage) == "")
					{
						strBookPage = "________________";
					}
					// Commitment Date
					rsRate.FindFirstRecord("ID", rsData.Get_Fields_Int32("BillingRateKey"));
					if (!rsRate.NoMatch)
					{
						//FC:FINAL:MSH - i.issue #1005: incorrect converting from date to int
						//if (FCConvert.ToInt32(rsRate.Get_Fields("BillDate")) != 0)
						if (FCConvert.ToDateTime(rsRate.Get_Fields_DateTime("BillDate") as object).ToOADate() != 0)
						{
							strBillDate = Strings.Format(rsRate.Get_Fields_DateTime("BillDate"), "MMMM d, yyyy");
						}
						else
						{
							strBillDate = "__________";
						}
					}
					else
					{
						strBillDate = "__________";
					}
					strAddressBar = "    " + strLastOwner;
					if (Strings.Trim(strLastOwner2) != "")
					{
						strAddressBar += " and " + strLastOwner2;
					}
					if (boolAddressCO || (strLastAddress == "" && strLastAddress2 == "" && strLastAddress3 == ""))
					{
						// this is the owner at time of billing C/O the new owner and the new owner address
						// or if there is no information in the CL fields (Should only happen the first year after conversion)
						if (boolAddressCO )
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							if (rsUT.Get_Fields("OwnerName") != rsData.Get_Fields_String("OName"))
							{
								// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
								strAddressBar += "\r\n" + "    C/O " + rsUT.Get_Fields("OwnerName");
								// name on the current RE account
								// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName"))) != "")
								{
									// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
									strAddressBar += " and " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName")));
								}
							}
						}
						else if (!boolNewOwner)
						{
							// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName"))) != "")
							{
								// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
								strOwnerName = rsUT.Get_Fields("OwnerName") + " and " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName")));
							}
							else
							{
								// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
								strOwnerName = FCConvert.ToString(rsUT.Get_Fields("OwnerName"));
							}
						}
						// if the row needs to be added, then do it
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
						}
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							strAddressBar += " " + rsUT.Get_Fields_String("OZip");
						}
						else
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							strAddressBar += rsUT.Get_Fields_String("OZip");
						}
						if (boolAddressCO )
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							if (rsUT.Get_Fields("OwnerName") != rsData.Get_Fields_String("OName"))
							{
								// this is information for the Certified Mailing List
								// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
								strCMFAddr1 = "C/O " + rsUT.Get_Fields("OwnerName");
								strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
								strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
								// MAL@20080325: Tracker Reference: 12766
								if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
								}
								else
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
									// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
									// strCMFAddr4 = strCMFAddr4 & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
									// Else
									strCMFAddr4 += rsUT.Get_Fields_String("OZip");
									// End If
								}
							}
							else
							{
								// this is information for the Certified Mailing List
								strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
								strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
								strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
								// MAL@20080325: Tracker Reference: 12766
								if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
									// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
									// strCMFAddr4 = strCMFAddr4 & " " & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
									// Else
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
									// End If
								}
								else
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
									// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
									// strCMFAddr4 = strCMFAddr4 & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
									// Else
									strCMFAddr4 += rsUT.Get_Fields_String("OZip");
									// End If
								}
							}
						}
						else
						{
							// this is information for the Certified Mailing List
							strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
							strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
							strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
								// strCMFAddr4 = strCMFAddr4 & " " & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
								// Else
								strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
								// End If
							}
							else
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
								// strCMFAddr4 = strCMFAddr4 & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
								// Else
								strCMFAddr4 += rsUT.Get_Fields_String("OZip");
								// End If
							}
						}
					}
					else
					{
						// this is the owner name at the time of billing and the address at the time of billing
						// If (rsData.Fields("OAddress1") = "" And rsData.Fields("OAddress2") = "" And rsData.Fields("OAddress3") = "") Then
						if (strLastAddress == "" && strLastAddress2 == "" && strLastAddress3 == "")
						{
							// if the row needs to be added, then do it
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
							}
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
							}
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
							}
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
								// strAddressBar = strAddressBar & " " & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
								// Else
								strAddressBar += " " + rsUT.Get_Fields_String("OZip");
								// End If
							}
							else
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
								// strAddressBar = strAddressBar & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
								// Else
								strAddressBar += rsUT.Get_Fields_String("OZip");
								// End If
							}
							// this is information for the Certified Mailing List
							strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
							strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
							strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
								// strCMFAddr4 = strCMFAddr4 & " " & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
								// Else
								strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
								// End If
							}
							else
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
								// strCMFAddr4 = strCMFAddr4 & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
								// Else
								strCMFAddr4 += rsUT.Get_Fields_String("OZip");
								// End If
							}
						}
						else
						{
							if (Strings.Trim(strLastAddress) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress;
							}
							if (Strings.Trim(strLastAddress2) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress2;
							}
							if (Strings.Trim(strLastAddress3) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress3;
							}
							// MAL@20080320: Tracker Reference: 12766
							if (strLastCity != "")
							{
								strLastCity += ", ";
							}
							else
							{
								strLastCity = strLastCity;
							}
							if (strLastState != "")
							{
								strLastState += " ";
							}
							else
							{
								strLastState = strLastState;
							}
							strAddressBar += "\r\n" + "    " + strLastCity + strLastState + strLastZip;
							// this is information for the Certified Mailing List
							strCMFAddr1 = strLastAddress;
							strCMFAddr2 = strLastAddress2;
							strCMFAddr3 = strLastAddress3;
							strCMFAddr4 = strLastCity + strLastState + strLastZip;
						}
					}
					dblDemand = dblCost;
					// End If
					// info for CMFNumbers table
					strCMFName = strOwnerName;
					lngCMFMHNumber = 0;
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("Bill"));
					// Billing year (1)
					modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = FCConvert.ToString(intYear);
					// MapLot
					modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = strMapLot;
					// BookPage
					modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = strBookPage;
					// Commission Expiration Date (12)
					modRhymalReporting.Statics.frfFreeReport[12].DatabaseFieldName = strCommissionExpirationDate;
					// Commitment Date (13)
					modRhymalReporting.Statics.frfFreeReport[13].DatabaseFieldName = strBillDate;
					// Address Bar (16)
					modRhymalReporting.Statics.frfFreeReport[16].DatabaseFieldName = strAddressBar;
					// principal (22)
					modRhymalReporting.Statics.frfFreeReport[22].DatabaseFieldName = Strings.Format(dblPrin - dblPayments, "$#,##0.00");
					// - dblPayments was put back in for Bethel call #91378  Not sure why it was taking out in the first place
					// Tax (39)
					modRhymalReporting.Statics.frfFreeReport[39].DatabaseFieldName = Strings.Format(dblTax, "$#,##0.00");
					// interest (23)
					modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = Strings.Format(dblInt, "$#,##0.00");
					// less payments (24)
					modRhymalReporting.Statics.frfFreeReport[24].DatabaseFieldName = strLessPaymentsLine;
					// city/town (25)
					modRhymalReporting.Statics.frfFreeReport[25].DatabaseFieldName = modGlobalConstants.Statics.gstrCityTown;
					// owner name (26)
					modRhymalReporting.Statics.frfFreeReport[26].DatabaseFieldName = strOwnerName;
					// add1 (27)
					modRhymalReporting.Statics.frfFreeReport[27].DatabaseFieldName = strCMFAddr1;
					// add2 (28)
					modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = strCMFAddr2;
					// add3 (29)
					modRhymalReporting.Statics.frfFreeReport[29].DatabaseFieldName = strCMFAddr3;
					// total due (30)
					modRhymalReporting.Statics.frfFreeReport[30].DatabaseFieldName = Strings.Format(dblTotalDue + dblCertMailFee + dblDemand, "#,##0.00");
					// location (31)
					modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = strLocation;
					// Demand Fees (37)   'demand and CMF needs to be calculated before the signature line is used
					modRhymalReporting.Statics.frfFreeReport[37].DatabaseFieldName = Strings.Format(dblDemand, "#,##0.00");
					// total certified mail fee (33)
					modRhymalReporting.Statics.frfFreeReport[33].DatabaseFieldName = Strings.Format(dblCertMailFee, "#,##0.00");
					// cost and signature line (32)
					modRhymalReporting.Statics.frfFreeReport[32].DatabaseFieldName = SetupCostBreakdownAndSignatureLine();
					// OldCollector (35)
					modRhymalReporting.Statics.frfFreeReport[35].DatabaseFieldName = frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(0, 1);
					// RecommitmentDate (36)
					modRhymalReporting.Statics.frfFreeReport[36].DatabaseFieldName = frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1);
					Array.Resize(ref modUTLien.Statics.arrDemand, lngArrayIndex + 1);
					// this will make sure that there are enough elements to use
					modUTLien.Statics.arrDemand[lngArrayIndex].Used = true;
					modUTLien.Statics.arrDemand[lngArrayIndex].Processed = false;
					modUTLien.Statics.arrDemand[lngArrayIndex].Account = modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"));
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					modUTLien.Statics.arrDemand[lngArrayIndex].BillKey = FCConvert.ToInt32(rsData.Get_Fields("Bill"));
					// arrDemand(lngArrayIndex).Name = rsData.Fields("OName")
					modUTLien.Statics.arrDemand[lngArrayIndex].Name = strLastOwner;
					modUTLien.Statics.arrDemand[lngArrayIndex].Fee = dblFilingFee;
					// dblDemand
					modUTLien.Statics.arrDemand[lngArrayIndex].CertifiedMailFee = dblCertMailFee;
					lngArrayIndex += 1;
					// MAL@20080602: Changed to update all the related bill records with the right number of copies
					// Tracker Reference: 13749
					if (boolCombinedBills)
					{
						if (Strings.Right(strBillList, 1) == ",")
						{
							strBillList = Strings.Left(strBillList, strBillList.Length - 1);
						}
						UpdateCopies(strBillList);
					}
					else
					{
						rsTemp.Execute("UPDATE Bill SET Copies = " + FCConvert.ToString(lngCopies) + " WHERE ID = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
					}
				}
				else
				{
					// this is the difference when it is a copy
					if (boolPayMortCert)
					{
						// if the user is getting charged for certified mail, then save thier info to print on Cert Mail Forms
						boolPrintCMF = true;
					}
					else
					{
						boolPrintCMF = false;
					}
					// If rsData.Fields("Account") = 1138 Then Stop
					// address bar
					if (rsMort.EndOfFile() != true)
					{
						// info for CMFNumbers table
						strCMFName = FCConvert.ToString(rsMort.Get_Fields_String("Name"));
						lngCMFMHNumber = FCConvert.ToInt32(rsMort.Get_Fields_Int32("MortgageHolderID"));
						// boolNewOwner = False
						lngCMFBillKey = 0;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("Bill"));
						if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1"))) != "")
						{
							strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address1"));
							if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
							{
								strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address2"));
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr4 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr4 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
						}
						else
						{
							if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
							{
								strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address2"));
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr1 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr1 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
						}
						strAddressBar = "    " + modGlobalFunctions.PadStringWithSpaces(rsMort.Get_Fields_String("Name"), 36, false) + modGlobalFunctions.PadStringWithSpaces(" ", 11, false) + strLastOwner;

						if (strLastCity != "")
						{
							strLastCity += ", ";
						}
						else
						{
							strLastCity = strLastCity;
						}
						if (strLastState != "")
						{
							strLastState += " ";
						}
						else
						{
							strLastState = strLastState;
						}
						if (Strings.Trim(strLastOwner2) != "")
						{
							strTemp = strLastOwner2;
							strTemp2 = strLastAddress;
							strTemp3 = strLastAddress2;
							strTemp4 = strLastAddress3;
							strTemp5 = strLastCity + strLastState + strLastZip;
						}
						else
						{
							if (Strings.Trim(strLastAddress) != "")
							{
								strTemp = strLastAddress;
								strTemp2 = strLastAddress2;
								strTemp3 = strLastAddress3;
								strTemp4 = strLastCity + strLastState + strLastZip;
								strTemp5 = "";
							}
							else
							{
								strTemp = strLastAddress2;
								strTemp2 = strLastAddress3;
								strTemp3 = strLastCity + strLastState + strLastZip;
								strTemp4 = "";
								strTemp5 = "";
							}
						}
						if (Strings.Trim(strTemp4) == "")
						{
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp3) == "")
						{
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp2) == "")
						{
							strTemp2 = strTemp3;
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp) == "")
						{
							strTemp = strTemp2;
							strTemp2 = strTemp3;
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr1, 47, false) + strTemp;
						strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr2, 47, false) + strTemp2;
						if (Strings.Trim(strCMFAddr3) != "" || Strings.Trim(strTemp3) != "")
						{
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr3, 47, false) + strTemp3;
						}
						if (Strings.Trim(strCMFAddr4) != "" || Strings.Trim(strTemp4) != "")
						{
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr4, 47, false) + strTemp4;
						}
						if (Strings.Trim(strTemp5) != "")
						{
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces("", 47, false) + strTemp5;
						}
						// set name and address bar for a mortgage holder
						modRhymalReporting.Statics.frfFreeReport[16].DatabaseFieldName = strAddressBar;
						rsMort.MoveNext();
					}
					else if (boolNewOwner)
					{
						// this is the last copy to be made, it is for the new owner
						// boolNewOwner = False                            'this will set it off for the next account
						// info for CMFNumbers table
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						strCMFName = FCConvert.ToString(rsUT.Get_Fields("OwnerName"));
						// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName"))) != "")
						{
							// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
							strCMFName += " " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName")));
						}
						lngCMFMHNumber = 0;
						// boolNewOwner = True
						lngCMFBillKey = 0;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("Bill"));
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						strAddressBar = "    " + rsUT.Get_Fields("OwnerName");
						// name at time of billing
						// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName"))) != "")
						{
							// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
							strAddressBar += " " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName")));
						}
						// strAddressBar = strAddressBar & vbCrLf & "    C/O " & rsUT.Fields("RSName")   'name on the current RE account
						// if the row needs to be added, then do it
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
						}
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
							// strAddressBar = strAddressBar & " " & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
							// Else
							strAddressBar += " " + rsUT.Get_Fields_String("OZip");
							// End If
						}
						else
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
							// strAddressBar = strAddressBar & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
							// Else
							strAddressBar += rsUT.Get_Fields_String("OZip");
							// End If
						}
						// this is information for the Certified Mailing List
						strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
						strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
						strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
							// strCMFAddr4 = strCMFAddr4 & " " & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
							// Else
							strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
							// End If
						}
						else
						{
							strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							// If Trim(rsUT.Fields("OZip4")) <> "" Then    'kk04162015
							// strCMFAddr4 = strCMFAddr4 & rsUT.Fields("OZip") & "-" & rsUT.Fields("OZip4")
							// Else
							strCMFAddr4 += rsUT.Get_Fields_String("OZip");
							// End If
						}
						// set name and address bar for a New Owner
						modRhymalReporting.Statics.frfFreeReport[16].DatabaseFieldName = strAddressBar;
						rsUT.MoveNext();
					}
					else
					{
						strAddressBar = "    " + rsData.Get_Fields_String("Name1");
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OAddress1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsData.Get_Fields_String("OAddress1");
						}
						strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP");
						// set name and address bar for a mortgage holder
						modRhymalReporting.Statics.frfFreeReport[16].DatabaseFieldName = strAddressBar;
					}
				}
				if (boolWater)
				{
					strTrueFalse = "1";
					// "TRUE"
				}
				else
				{
					strTrueFalse = "0";
					// "FALSE"
				}
				// this will add the information to the CMFNumber table so that it will be
				// easier to figure out which accounts need certified mail forms to be printed for them
				// MAL@20071016: Added support for new Process Type
				// Call Reference: 114824
				if (lngCMFMHNumber != 0)
				{
					// rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 21 AND MortgageHolder = " & lngCMFMHNumber & " AND MortgageHolder <> 0 AND boolWater = " & strTrueFalse
					// kgk        rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 21 AND MortgageHolder = " & lngCMFMHNumber & " AND MortgageHolder <> 0 AND boolWater = " & strTrueFalse & " AND ProcessType = 'LIEN'"
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					rsCMFNumbers.FindFirstRecord2("BillKey,Type,MortgageHolder,boolWater,ProcessType", rsData.Get_Fields("Bill") + ",21," + FCConvert.ToString(lngCMFMHNumber) + "," + strTrueFalse + ",LIEN", ",");
					// kk08122014 trocls-49   ' ",'LIEN'"
				}
				else
				{
					if (boolCopy && boolNewOwner)
					{
						// rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 21 AND NewOwner = TRUE AND boolWater = " & strTrueFalse
						// kgk            rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 21 AND NewOwner = TRUE AND boolWater = " & strTrueFalse & " AND ProcessType = 'LIEN'"
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsCMFNumbers.FindFirstRecord2("BillKey,Type,NewOwner,boolWater,ProcessType", rsData.Get_Fields("Bill") + ",21,True," + strTrueFalse + ",LIEN", ",");
						// kk08122014 trocls-49   ' ",'LIEN'"
					}
					else
					{
						// rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 21 AND NewOwner = FALSE AND boolWater = " & strTrueFalse
						// kgk            rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 21 AND NewOwner = FALSE AND boolWater = " & strTrueFalse & " AND ProcessType = 'LIEN'"
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsCMFNumbers.FindFirstRecord2("BillKey,Type,NewOwner,boolWater,ProcessType", rsData.Get_Fields("Bill") + ",21,False," + strTrueFalse + ",LIEN", ",");
						// kk08122014 trocls-49   ' ",'LIEN'"
					}
				}
				if (rsCMFNumbers.NoMatch)
				{
					// Or (boolCopy And boolPrintCMF) Then
					rsCMFNumbers.AddNew();
				}
				else
				{
					rsCMFNumbers.Edit();
				}
				rsCMFNumbers.Set_Fields("boolWater", boolWater);
				rsCMFNumbers.Set_Fields("Name", strCMFName);
				rsCMFNumbers.Set_Fields("Address1", strCMFAddr1);
				rsCMFNumbers.Set_Fields("Address2", strCMFAddr2);
				rsCMFNumbers.Set_Fields("Address3", strCMFAddr3);
				rsCMFNumbers.Set_Fields("Address4", strCMFAddr4);
				rsCMFNumbers.Set_Fields("Billkey", lngCMFBillKey);
				rsCMFNumbers.Set_Fields("MortgageHolder", lngCMFMHNumber);
				rsCMFNumbers.Set_Fields("Account", rsData.Get_Fields_Int32("AccountKey"));
				rsCMFNumbers.Set_Fields("Service", rsData.Get_Fields_String("Service"));
				// MAL@20071114: Changed to use LastOwner Name if BName is blank (Pre-conversion)
				// rsCMFNumbers.Fields("BName") = rsData.Fields("BName")
				if (fecherFoundation.FCUtils.IsNull(rsData.Get_Fields_String("BName")) || FCConvert.ToString(rsData.Get_Fields_String("BName")) == "")
				{
					rsCMFNumbers.Set_Fields("BName", strCMFName);
				}
				else
				{
					rsCMFNumbers.Set_Fields("BName", rsData.Get_Fields_String("BName"));
				}
				rsCMFNumbers.Set_Fields("ActualAccountNumber", rsData.Get_Fields_Int32("ActualAccountNumber"));
				if (lngCMFMHNumber != 0)
				{
					// mort holder
					// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not boolPayMortCert
				}
				else
				{
					rsCMFNumbers.Set_Fields("NewOwner", false);
					if (boolCopy && boolNewOwner)
					{
						rsCMFNumbers.Set_Fields("NewOwner", true);
						// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not (intNewOwnerChoice = 2 Or intNewOwnerChoice = 3)   'Not boolPayNewOwner
						boolNewOwner = false;
					}
					else
					{
						// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not boolPayCert
					}
				}
				rsCMFNumbers.Set_Fields("Type", 21);
				rsCMFNumbers.Set_Fields("ProcessType", "LIEN");
				// MAL@20071016
				rsCMFNumbers.Update(true);
				rsCombineBills.Dispose();
				rsTemp.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Calculate Variable Error - " + rsData.Get_Fields_Int32("BillKey"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				bool boolUseRE = false;
				clsDRWrapper rsM = new clsDRWrapper();
				rsM.OpenRecordset("SELECT UseREAccount, UseMortgageHolder, REAccount, ID FROM Master WHERE ID = " + rsData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
				if (!rsM.EndOfFile())
				{
					boolUseRE = rsM.Get_Fields_Boolean("UseMortgageHolder") && FCConvert.ToInt32(rsM.Get_Fields_Int32("REAccount")) != 0;
				}
				// MAL@20081106: Change to take the Receive Copies option into account
				// Tracker Reference: 16047
				if (boolUseRE)
				{
					// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & rsM.Fields("REAccount") & " AND Module = 'RE'", strGNDatabase
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + rsM.Get_Fields_Int32("REAccount") + " AND Module = 'RE' AND ISNULL(ReceiveCopies,0) = 1", "CentralData");
				}
				else
				{
					// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & GetUTAccountNumber(rsData.Fields("AccountKey")) & " AND Module = 'UT'", strGNDatabase
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(modUTStatusPayments.GetUTAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"))) + " AND Module = 'UT' AND ISNULL(ReceiveCopies,0) = 1", "CentralData");
				}
				if (rsMort.EndOfFile() != true)
				{
					CalculateMortgageHolders = rsMort.RecordCount();
				}
				else
				{
					CalculateMortgageHolders = 0;
				}
				rsM.Dispose();
				return CalculateMortgageHolders;
			}
			catch
			{
				
				frmWait.InstancePtr.Unload();
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}

		public void SaveLienStatus()
		{
			string strTemp;
			string strAcct = "";
			int lngCommaPosition = 0;
			if (Strings.Right(strAcctList, 1) == ",")
			{
				strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
			}
			strTemp = strAcctList;
			// this statement will change the status of the accounts that have been process
			if (Strings.Trim(strAcctList) != "")
			{
				while (!(strTemp == ""))
				{
					lngCommaPosition = Strings.InStr(1, strTemp, ",", CompareConstants.vbBinaryCompare);
					if (lngCommaPosition > 0)
					{
						strAcct = Strings.Left(strTemp, lngCommaPosition - 1);
					}
					else
					{
						strAcct = strTemp;
						strTemp = "";
					}
					strTemp = Strings.Right(strTemp, strTemp.Length - lngCommaPosition);
					if (Strings.Trim(strAcct) != "")
					{
						rsData.OpenRecordset("SELECT * FROM Bill WHERE ID = " + strAcct, modExtraModules.strUTDatabase);
						if (!rsData.EndOfFile())
						{
							// rsData.OpenRecordset "SELECT * FROM Bill WHERE Bill = " & strAcct, strUTDatabase
							// rsData.Execute "UPDATE Bill SET " & strWS & "LienProcessStatus = 3, " & strWS & "LienStatusEligibility = 4 WHERE , strUTDatabase"
							rsData.Edit();
							rsData.Set_Fields(strWS + "LienProcessStatus", 3);
							rsData.Set_Fields(strWS + "LienStatusEligibility", 4);
							rsData.Update();
						}
					}
				}
			}
		}

		private void UpdateCopies(string strAcctList)
		{
			clsDRWrapper rsCopy = new clsDRWrapper();
			rsCopy.OpenRecordset("SELECT * FROM Bill WHERE ID IN(" + strAcctList + ")", modExtraModules.strUTDatabase);
			if (rsCopy.RecordCount() > 0)
			{
				rsCopy.MoveFirst();
				while (!rsCopy.EndOfFile())
				{
					rsCopy.Edit();
					rsCopy.Set_Fields("Copies", lngCopies);
					rsCopy.Update();
					rsCopy.MoveNext();
				}
			}
			rsCopy.Dispose();
		}

		private void SetReportFont()
		{
			fldHeader.Font = new Font(strDefFontName, fldHeader.Font.Size);
			fldHeader.Font = new Font(fldHeader.Font.Name, intDefFontSize);
			rtbText.Font = new Font(strDefFontName, rtbText.Font.Size);
			rtbText.Font = new Font(rtbText.Font.Name, intDefFontSize);
			rtbText2.Font = new Font(strDefFontName, rtbText2.Font.Size);
			rtbText2.Font = new Font(rtbText2.Font.Name, intDefFontSize);
			lblBreakdown.Font = new Font(strDefFontName, lblBreakdown.Font.Size);
			lblBreakdown.Font = new Font(lblBreakdown.Font.Name, intDefFontSize);
			lblCosts1.Font = new Font(strDefFontName, lblCosts1.Font.Size);
			lblCosts1.Font = new Font(lblCosts1.Font.Name, intDefFontSize);
			lblCosts2.Font = new Font(strDefFontName, lblCosts2.Font.Size);
			lblCosts2.Font = new Font(lblCosts2.Font.Name, intDefFontSize);
			lblPrincipal.Font = new Font(strDefFontName, lblPrincipal.Font.Size);
			lblPrincipal.Font = new Font(lblPrincipal.Font.Name, intDefFontSize);
			lblInterest.Font = new Font(strDefFontName, lblInterest.Font.Size);
			lblInterest.Font = new Font(lblInterest.Font.Name, intDefFontSize);
			lblTotal.Font = new Font(strDefFontName, lblTotal.Font.Size);
			lblTotal.Font = new Font(lblTotal.Font.Name, intDefFontSize);
			fldCollector.Font = new Font(strDefFontName, fldCollector.Font.Size);
			fldCollector.Font = new Font(fldCollector.Font.Name, intDefFontSize);
			fldTitle.Font = new Font(strDefFontName, fldTitle.Font.Size);
			fldTitle.Font = new Font(fldTitle.Font.Name, intDefFontSize);
			fldMuni.Font = new Font(strDefFontName, fldMuni.Font.Size);
			fldMuni.Font = new Font(fldMuni.Font.Name, intDefFontSize);
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			lblAccount.Text = modMain.PadToString_8(modUTStatusPayments.GetAccountNumber(ref modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey), 6);
			// kk04282016 trout-1228  PadToString(rsData.Fields("ActualAccountNumber"), 6)
			SetHeader();
		}

        private void ActiveReport_PageEnd(object sender, EventArgs e)
        {
            ////FC:FINAL:SBE - #3984 - add flag to check if blank page was generated. If FetchData was not execuyted, empty page is generated. Then we have to delete it later
            //if (!boolFetchDataExecuted)
            //{
            //    boolSkipAccount = true;
            //}
        }

        private void ActiveReport_PageStart(object sender, EventArgs e)
        {
            //FC:FINAL:SBE - #3984 - add flag to check if blank page was generated
            boolFetchDataExecuted = false;
            //FC:FINAL:SBE - #3984 - moved code from Detail_Format, to delete the previous page, if account was not valid
            if (boolSkipAccount)
            {
                if (this.Document.Pages.Count > 0)
                {
                    this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
                }
            }
        }


        private void arLienProcess_Load(object sender, System.EventArgs e)
		{
		}

		private void arLienProcess_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
