﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptRateTablesCond.
	/// </summary>
	partial class rptRateTablesCond
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptRateTablesCond));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuni = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRateTableDescr = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCons = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCharge = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblThru = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOver = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblStep = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblFlat = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblConsRate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fService = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fMinCharge = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fMinCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fMisc1Account = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fMisc1Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fMisc2Desc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fMisc1Amount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fMisc2Amount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fMisc2Account = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fThru1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fStep1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fRate1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fThru2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fStep2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fRate2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fThru3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fStep3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fRate3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fThru4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fStep4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fRate4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fThru5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fStep5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fRate5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fThru6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fStep6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fRate6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fThru7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fStep7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fRate7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fThru8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fStep8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fRate8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblRate1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRate2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRate3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRate4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRate7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRate6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRate5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRate8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fFlat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fFrom1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fFrom2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fFrom3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fFrom4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fFrom5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fFrom6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fFrom7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fFrom8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMiscCharges = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateTableDescr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCharge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblThru)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOver)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStep)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFlat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fService)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fMinCharge)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fMinCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc1Account)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc1Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc2Desc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc1Amount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc2Amount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc2Account)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFlat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscCharges)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fMinCharge,
				this.fMinCons,
				this.fMisc1Account,
				this.fMisc1Desc,
				this.fMisc2Desc,
				this.fMisc1Amount,
				this.fMisc2Amount,
				this.fMisc2Account,
				this.fDesc,
				this.fThru1,
				this.fStep1,
				this.fRate1,
				this.fThru2,
				this.fStep2,
				this.fRate2,
				this.fThru3,
				this.fStep3,
				this.fRate3,
				this.fThru4,
				this.fStep4,
				this.fRate4,
				this.fThru5,
				this.fStep5,
				this.fRate5,
				this.fThru6,
				this.fStep6,
				this.fRate6,
				this.fThru7,
				this.fStep7,
				this.fRate7,
				this.fThru8,
				this.fStep8,
				this.fRate8,
				this.lblRate1,
				this.lblRate2,
				this.lblRate3,
				this.lblRate4,
				this.lblRate7,
				this.lblRate6,
				this.lblRate5,
				this.lblRate8,
				this.fFlat,
				this.fFrom1,
				this.fFrom2,
				this.fFrom3,
				this.fFrom4,
				this.fFrom5,
				this.fFrom6,
				this.fFrom7,
				this.fFrom8,
				this.lblMiscCharges
			});
			this.Detail.Height = 1.510417F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblMuni,
				this.lblPage,
				this.lblTime,
				this.lblRateTableDescr,
				this.lblCons,
				this.lblCharge,
				this.Line1,
				this.lblThru,
				this.lblOver,
				this.lblStep,
				this.lblRate,
				this.lblFlat,
				this.lblConsRate
			});
			this.PageHeader.Height = 0.6666667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.CanGrow = false;
			this.GroupHeader1.ColumnLayout = false;
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fService
			});
			this.GroupHeader1.DataField = "Service";
			this.GroupHeader1.Height = 0.34375F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0.03125F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Rate Table Listing";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 10F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 0F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblDate.Text = null;
			this.lblDate.Top = 0.1875F;
			this.lblDate.Width = 1.5F;
			// 
			// lblMuni
			// 
			this.lblMuni.Height = 0.1875F;
			this.lblMuni.HyperLink = null;
			this.lblMuni.Left = 0F;
			this.lblMuni.Name = "lblMuni";
			this.lblMuni.Style = "font-family: \'Tahoma\'; ddo-char-set: 0";
			this.lblMuni.Text = null;
			this.lblMuni.Top = 0F;
			this.lblMuni.Width = 1.5F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 8.6875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.3125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 8.6875F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 0";
			this.lblTime.Text = null;
			this.lblTime.Top = 0F;
			this.lblTime.Width = 1.3125F;
			// 
			// lblRateTableDescr
			// 
			this.lblRateTableDescr.Height = 0.1875F;
			this.lblRateTableDescr.HyperLink = null;
			this.lblRateTableDescr.Left = 0.125F;
			this.lblRateTableDescr.Name = "lblRateTableDescr";
			this.lblRateTableDescr.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.lblRateTableDescr.Text = "Description";
			this.lblRateTableDescr.Top = 0.4375F;
			this.lblRateTableDescr.Width = 0.6875F;
			// 
			// lblCons
			// 
			this.lblCons.Height = 0.1875F;
			this.lblCons.HyperLink = null;
			this.lblCons.Left = 1.9375F;
			this.lblCons.Name = "lblCons";
			this.lblCons.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblCons.Text = "Min Cons";
			this.lblCons.Top = 0.4375F;
			this.lblCons.Width = 0.625F;
			// 
			// lblCharge
			// 
			this.lblCharge.Height = 0.1875F;
			this.lblCharge.HyperLink = null;
			this.lblCharge.Left = 3.0625F;
			this.lblCharge.Name = "lblCharge";
			this.lblCharge.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblCharge.Text = "Min Charge";
			this.lblCharge.Top = 0.4375F;
			this.lblCharge.Width = 0.625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.625F;
			this.Line1.Width = 10F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10F;
			this.Line1.Y1 = 0.625F;
			this.Line1.Y2 = 0.625F;
			// 
			// lblThru
			// 
			this.lblThru.Height = 0.1875F;
			this.lblThru.HyperLink = null;
			this.lblThru.Left = 7.375F;
			this.lblThru.Name = "lblThru";
			this.lblThru.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblThru.Text = "Thru";
			this.lblThru.Top = 0.4375F;
			this.lblThru.Width = 0.625F;
			// 
			// lblOver
			// 
			this.lblOver.Height = 0.1875F;
			this.lblOver.HyperLink = null;
			this.lblOver.Left = 6.1875F;
			this.lblOver.Name = "lblOver";
			this.lblOver.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblOver.Text = "Over";
			this.lblOver.Top = 0.4375F;
			this.lblOver.Width = 0.75F;
			// 
			// lblStep
			// 
			this.lblStep.Height = 0.1875F;
			this.lblStep.HyperLink = null;
			this.lblStep.Left = 8.9375F;
			this.lblStep.Name = "lblStep";
			this.lblStep.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblStep.Text = "Step";
			this.lblStep.Top = 0.4375F;
			this.lblStep.Width = 0.3125F;
			// 
			// lblRate
			// 
			this.lblRate.Height = 0.1875F;
			this.lblRate.HyperLink = null;
			this.lblRate.Left = 8F;
			this.lblRate.Name = "lblRate";
			this.lblRate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblRate.Text = "Rate";
			this.lblRate.Top = 0.4375F;
			this.lblRate.Width = 0.75F;
			// 
			// lblFlat
			// 
			this.lblFlat.Height = 0.1875F;
			this.lblFlat.HyperLink = null;
			this.lblFlat.Left = 4.25F;
			this.lblFlat.Name = "lblFlat";
			this.lblFlat.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblFlat.Text = "Flat Rate";
			this.lblFlat.Top = 0.4375F;
			this.lblFlat.Width = 0.625F;
			// 
			// lblConsRate
			// 
			this.lblConsRate.Height = 0.1875F;
			this.lblConsRate.HyperLink = null;
			this.lblConsRate.Left = 5.4375F;
			this.lblConsRate.Name = "lblConsRate";
			this.lblConsRate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblConsRate.Text = "Cons Rate";
			this.lblConsRate.Top = 0.4375F;
			this.lblConsRate.Width = 0.625F;
			// 
			// fService
			// 
			this.fService.Height = 0.1875F;
			this.fService.Left = 0F;
			this.fService.Name = "fService";
			this.fService.Style = "font-family: \'Tahoma\'; text-align: center";
			this.fService.Text = "Water";
			this.fService.Top = 0.125F;
			this.fService.Width = 10F;
			// 
			// fMinCharge
			// 
			this.fMinCharge.CanGrow = false;
			this.fMinCharge.Height = 0.1875F;
			this.fMinCharge.Left = 3F;
			this.fMinCharge.Name = "fMinCharge";
			this.fMinCharge.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fMinCharge.Text = "20.33";
			this.fMinCharge.Top = 0F;
			this.fMinCharge.Width = 0.6875F;
			// 
			// fMinCons
			// 
			this.fMinCons.CanGrow = false;
			this.fMinCons.Height = 0.1875F;
			this.fMinCons.Left = 1.9375F;
			this.fMinCons.Name = "fMinCons";
			this.fMinCons.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fMinCons.Text = "200";
			this.fMinCons.Top = 0F;
			this.fMinCons.Width = 0.625F;
			// 
			// fMisc1Account
			// 
			this.fMisc1Account.CanGrow = false;
			this.fMisc1Account.Height = 0.1875F;
			this.fMisc1Account.Left = 2.6875F;
			this.fMisc1Account.Name = "fMisc1Account";
			this.fMisc1Account.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fMisc1Account.Text = "G 1-0000-00";
			this.fMisc1Account.Top = 0.1875F;
			this.fMisc1Account.Width = 0.9375F;
			// 
			// fMisc1Desc
			// 
			this.fMisc1Desc.CanGrow = false;
			this.fMisc1Desc.Height = 0.1875F;
			this.fMisc1Desc.Left = 1.625F;
			this.fMisc1Desc.Name = "fMisc1Desc";
			this.fMisc1Desc.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fMisc1Desc.Text = "200";
			this.fMisc1Desc.Top = 0.1875F;
			this.fMisc1Desc.Width = 1.0625F;
			// 
			// fMisc2Desc
			// 
			this.fMisc2Desc.CanGrow = false;
			this.fMisc2Desc.Height = 0.1875F;
			this.fMisc2Desc.Left = 1.625F;
			this.fMisc2Desc.Name = "fMisc2Desc";
			this.fMisc2Desc.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fMisc2Desc.Text = "20.33";
			this.fMisc2Desc.Top = 0.375F;
			this.fMisc2Desc.Width = 1.0625F;
			// 
			// fMisc1Amount
			// 
			this.fMisc1Amount.CanGrow = false;
			this.fMisc1Amount.Height = 0.1875F;
			this.fMisc1Amount.Left = 3.625F;
			this.fMisc1Amount.Name = "fMisc1Amount";
			this.fMisc1Amount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fMisc1Amount.Text = "20.33";
			this.fMisc1Amount.Top = 0.1875F;
			this.fMisc1Amount.Width = 0.6875F;
			// 
			// fMisc2Amount
			// 
			this.fMisc2Amount.CanGrow = false;
			this.fMisc2Amount.Height = 0.1875F;
			this.fMisc2Amount.Left = 3.625F;
			this.fMisc2Amount.Name = "fMisc2Amount";
			this.fMisc2Amount.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fMisc2Amount.Text = "20.33";
			this.fMisc2Amount.Top = 0.375F;
			this.fMisc2Amount.Width = 0.6875F;
			// 
			// fMisc2Account
			// 
			this.fMisc2Account.CanGrow = false;
			this.fMisc2Account.Height = 0.1875F;
			this.fMisc2Account.Left = 2.6875F;
			this.fMisc2Account.Name = "fMisc2Account";
			this.fMisc2Account.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fMisc2Account.Text = "200";
			this.fMisc2Account.Top = 0.375F;
			this.fMisc2Account.Width = 0.9375F;
			// 
			// fDesc
			// 
			this.fDesc.CanGrow = false;
			this.fDesc.Height = 0.1875F;
			this.fDesc.Left = 0.125F;
			this.fDesc.Name = "fDesc";
			this.fDesc.Style = "font-family: \'Tahoma\'";
			this.fDesc.Text = "5/8\" Monthly";
			this.fDesc.Top = 0F;
			this.fDesc.Width = 2F;
			// 
			// fThru1
			// 
			this.fThru1.CanGrow = false;
			this.fThru1.Height = 0.1875F;
			this.fThru1.Left = 7F;
			this.fThru1.Name = "fThru1";
			this.fThru1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fThru1.Text = "999,999,999";
			this.fThru1.Top = 0F;
			this.fThru1.Width = 1F;
			// 
			// fStep1
			// 
			this.fStep1.CanGrow = false;
			this.fStep1.Height = 0.1875F;
			this.fStep1.Left = 8.8125F;
			this.fStep1.Name = "fStep1";
			this.fStep1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fStep1.Text = "1";
			this.fStep1.Top = 0F;
			this.fStep1.Width = 0.4375F;
			// 
			// fRate1
			// 
			this.fRate1.CanGrow = false;
			this.fRate1.Height = 0.1875F;
			this.fRate1.Left = 8F;
			this.fRate1.Name = "fRate1";
			this.fRate1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fRate1.Text = "0.0000";
			this.fRate1.Top = 0F;
			this.fRate1.Width = 0.75F;
			// 
			// fThru2
			// 
			this.fThru2.CanGrow = false;
			this.fThru2.Height = 0.1875F;
			this.fThru2.Left = 7F;
			this.fThru2.Name = "fThru2";
			this.fThru2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fThru2.Text = ">72000";
			this.fThru2.Top = 0.1875F;
			this.fThru2.Width = 1F;
			// 
			// fStep2
			// 
			this.fStep2.CanGrow = false;
			this.fStep2.Height = 0.1875F;
			this.fStep2.Left = 8.8125F;
			this.fStep2.Name = "fStep2";
			this.fStep2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fStep2.Text = "1";
			this.fStep2.Top = 0.1875F;
			this.fStep2.Width = 0.4375F;
			// 
			// fRate2
			// 
			this.fRate2.CanGrow = false;
			this.fRate2.Height = 0.1875F;
			this.fRate2.Left = 8F;
			this.fRate2.Name = "fRate2";
			this.fRate2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fRate2.Text = "0.0641";
			this.fRate2.Top = 0.1875F;
			this.fRate2.Width = 0.75F;
			// 
			// fThru3
			// 
			this.fThru3.CanGrow = false;
			this.fThru3.Height = 0.1875F;
			this.fThru3.Left = 7F;
			this.fThru3.Name = "fThru3";
			this.fThru3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fThru3.Text = "200000-750000";
			this.fThru3.Top = 0.375F;
			this.fThru3.Width = 1F;
			// 
			// fStep3
			// 
			this.fStep3.CanGrow = false;
			this.fStep3.Height = 0.1875F;
			this.fStep3.Left = 8.8125F;
			this.fStep3.Name = "fStep3";
			this.fStep3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fStep3.Text = "1";
			this.fStep3.Top = 0.375F;
			this.fStep3.Width = 0.4375F;
			// 
			// fRate3
			// 
			this.fRate3.CanGrow = false;
			this.fRate3.Height = 0.1875F;
			this.fRate3.Left = 8F;
			this.fRate3.Name = "fRate3";
			this.fRate3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fRate3.Text = "0.0641";
			this.fRate3.Top = 0.375F;
			this.fRate3.Width = 0.75F;
			// 
			// fThru4
			// 
			this.fThru4.CanGrow = false;
			this.fThru4.Height = 0.1875F;
			this.fThru4.Left = 7F;
			this.fThru4.Name = "fThru4";
			this.fThru4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fThru4.Text = ">200";
			this.fThru4.Top = 0.5625F;
			this.fThru4.Width = 1F;
			// 
			// fStep4
			// 
			this.fStep4.CanGrow = false;
			this.fStep4.Height = 0.1875F;
			this.fStep4.Left = 8.8125F;
			this.fStep4.Name = "fStep4";
			this.fStep4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fStep4.Text = "1";
			this.fStep4.Top = 0.5625F;
			this.fStep4.Width = 0.4375F;
			// 
			// fRate4
			// 
			this.fRate4.CanGrow = false;
			this.fRate4.Height = 0.1875F;
			this.fRate4.Left = 8F;
			this.fRate4.Name = "fRate4";
			this.fRate4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fRate4.Text = "0.0641";
			this.fRate4.Top = 0.5625F;
			this.fRate4.Width = 0.75F;
			// 
			// fThru5
			// 
			this.fThru5.CanGrow = false;
			this.fThru5.Height = 0.1875F;
			this.fThru5.Left = 7F;
			this.fThru5.Name = "fThru5";
			this.fThru5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fThru5.Text = "0-72000";
			this.fThru5.Top = 0.75F;
			this.fThru5.Width = 1F;
			// 
			// fStep5
			// 
			this.fStep5.CanGrow = false;
			this.fStep5.Height = 0.1875F;
			this.fStep5.Left = 8.8125F;
			this.fStep5.Name = "fStep5";
			this.fStep5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fStep5.Text = "1";
			this.fStep5.Top = 0.75F;
			this.fStep5.Width = 0.4375F;
			// 
			// fRate5
			// 
			this.fRate5.CanGrow = false;
			this.fRate5.Height = 0.1875F;
			this.fRate5.Left = 8F;
			this.fRate5.Name = "fRate5";
			this.fRate5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fRate5.Text = "0.0000";
			this.fRate5.Top = 0.75F;
			this.fRate5.Width = 0.75F;
			// 
			// fThru6
			// 
			this.fThru6.CanGrow = false;
			this.fThru6.Height = 0.1875F;
			this.fThru6.Left = 7F;
			this.fThru6.Name = "fThru6";
			this.fThru6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fThru6.Text = ">72000";
			this.fThru6.Top = 0.9375F;
			this.fThru6.Width = 1F;
			// 
			// fStep6
			// 
			this.fStep6.CanGrow = false;
			this.fStep6.Height = 0.1875F;
			this.fStep6.Left = 8.8125F;
			this.fStep6.Name = "fStep6";
			this.fStep6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fStep6.Text = "1";
			this.fStep6.Top = 0.9375F;
			this.fStep6.Width = 0.4375F;
			// 
			// fRate6
			// 
			this.fRate6.CanGrow = false;
			this.fRate6.Height = 0.1875F;
			this.fRate6.Left = 8F;
			this.fRate6.Name = "fRate6";
			this.fRate6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fRate6.Text = "0.0641";
			this.fRate6.Top = 0.9375F;
			this.fRate6.Width = 0.75F;
			// 
			// fThru7
			// 
			this.fThru7.CanGrow = false;
			this.fThru7.Height = 0.1875F;
			this.fThru7.Left = 7F;
			this.fThru7.Name = "fThru7";
			this.fThru7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fThru7.Text = "200000-750000";
			this.fThru7.Top = 1.125F;
			this.fThru7.Width = 1F;
			// 
			// fStep7
			// 
			this.fStep7.CanGrow = false;
			this.fStep7.Height = 0.1875F;
			this.fStep7.Left = 8.8125F;
			this.fStep7.Name = "fStep7";
			this.fStep7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fStep7.Text = "1";
			this.fStep7.Top = 1.125F;
			this.fStep7.Width = 0.4375F;
			// 
			// fRate7
			// 
			this.fRate7.CanGrow = false;
			this.fRate7.Height = 0.1875F;
			this.fRate7.Left = 8F;
			this.fRate7.Name = "fRate7";
			this.fRate7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fRate7.Text = "0.0641";
			this.fRate7.Top = 1.125F;
			this.fRate7.Width = 0.75F;
			// 
			// fThru8
			// 
			this.fThru8.CanGrow = false;
			this.fThru8.Height = 0.1875F;
			this.fThru8.Left = 7F;
			this.fThru8.Name = "fThru8";
			this.fThru8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fThru8.Text = ">200";
			this.fThru8.Top = 1.3125F;
			this.fThru8.Width = 1F;
			// 
			// fStep8
			// 
			this.fStep8.CanGrow = false;
			this.fStep8.Height = 0.1875F;
			this.fStep8.Left = 8.8125F;
			this.fStep8.Name = "fStep8";
			this.fStep8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fStep8.Text = "1";
			this.fStep8.Top = 1.3125F;
			this.fStep8.Width = 0.4375F;
			// 
			// fRate8
			// 
			this.fRate8.CanGrow = false;
			this.fRate8.Height = 0.1875F;
			this.fRate8.Left = 8F;
			this.fRate8.Name = "fRate8";
			this.fRate8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fRate8.Text = "0.0641";
			this.fRate8.Top = 1.3125F;
			this.fRate8.Width = 0.75F;
			// 
			// lblRate1
			// 
			this.lblRate1.Height = 0.1875F;
			this.lblRate1.HyperLink = null;
			this.lblRate1.Left = 5.6875F;
			this.lblRate1.Name = "lblRate1";
			this.lblRate1.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblRate1.Text = "1)";
			this.lblRate1.Top = 0F;
			this.lblRate1.Width = 0.1875F;
			// 
			// lblRate2
			// 
			this.lblRate2.Height = 0.1875F;
			this.lblRate2.HyperLink = null;
			this.lblRate2.Left = 5.6875F;
			this.lblRate2.Name = "lblRate2";
			this.lblRate2.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblRate2.Text = "2)";
			this.lblRate2.Top = 0.1875F;
			this.lblRate2.Width = 0.1875F;
			// 
			// lblRate3
			// 
			this.lblRate3.Height = 0.1875F;
			this.lblRate3.HyperLink = null;
			this.lblRate3.Left = 5.6875F;
			this.lblRate3.Name = "lblRate3";
			this.lblRate3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblRate3.Text = "3)";
			this.lblRate3.Top = 0.375F;
			this.lblRate3.Width = 0.1875F;
			// 
			// lblRate4
			// 
			this.lblRate4.Height = 0.1875F;
			this.lblRate4.HyperLink = null;
			this.lblRate4.Left = 5.6875F;
			this.lblRate4.Name = "lblRate4";
			this.lblRate4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblRate4.Text = "4)";
			this.lblRate4.Top = 0.5625F;
			this.lblRate4.Width = 0.1875F;
			// 
			// lblRate7
			// 
			this.lblRate7.Height = 0.1875F;
			this.lblRate7.HyperLink = null;
			this.lblRate7.Left = 5.6875F;
			this.lblRate7.Name = "lblRate7";
			this.lblRate7.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblRate7.Text = "7)";
			this.lblRate7.Top = 1.125F;
			this.lblRate7.Width = 0.1875F;
			// 
			// lblRate6
			// 
			this.lblRate6.Height = 0.1875F;
			this.lblRate6.HyperLink = null;
			this.lblRate6.Left = 5.6875F;
			this.lblRate6.Name = "lblRate6";
			this.lblRate6.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblRate6.Text = "6)";
			this.lblRate6.Top = 0.9375F;
			this.lblRate6.Width = 0.1875F;
			// 
			// lblRate5
			// 
			this.lblRate5.Height = 0.1875F;
			this.lblRate5.HyperLink = null;
			this.lblRate5.Left = 5.6875F;
			this.lblRate5.Name = "lblRate5";
			this.lblRate5.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblRate5.Text = "5)";
			this.lblRate5.Top = 0.75F;
			this.lblRate5.Width = 0.1875F;
			// 
			// lblRate8
			// 
			this.lblRate8.Height = 0.1875F;
			this.lblRate8.HyperLink = null;
			this.lblRate8.Left = 5.6875F;
			this.lblRate8.Name = "lblRate8";
			this.lblRate8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.lblRate8.Text = "8)";
			this.lblRate8.Top = 1.3125F;
			this.lblRate8.Width = 0.1875F;
			// 
			// fFlat
			// 
			this.fFlat.CanGrow = false;
			this.fFlat.Height = 0.1875F;
			this.fFlat.Left = 4.1875F;
			this.fFlat.Name = "fFlat";
			this.fFlat.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFlat.Text = "20.33";
			this.fFlat.Top = 0F;
			this.fFlat.Width = 0.6875F;
			// 
			// fFrom1
			// 
			this.fFrom1.CanGrow = false;
			this.fFrom1.Height = 0.1875F;
			this.fFrom1.Left = 5.9375F;
			this.fFrom1.Name = "fFrom1";
			this.fFrom1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFrom1.Text = "999,999,999";
			this.fFrom1.Top = 0F;
			this.fFrom1.Width = 1F;
			// 
			// fFrom2
			// 
			this.fFrom2.CanGrow = false;
			this.fFrom2.Height = 0.1875F;
			this.fFrom2.Left = 5.9375F;
			this.fFrom2.Name = "fFrom2";
			this.fFrom2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFrom2.Text = ">72000";
			this.fFrom2.Top = 0.1875F;
			this.fFrom2.Width = 1F;
			// 
			// fFrom3
			// 
			this.fFrom3.CanGrow = false;
			this.fFrom3.Height = 0.1875F;
			this.fFrom3.Left = 5.9375F;
			this.fFrom3.Name = "fFrom3";
			this.fFrom3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFrom3.Text = "200000-750000";
			this.fFrom3.Top = 0.375F;
			this.fFrom3.Width = 1F;
			// 
			// fFrom4
			// 
			this.fFrom4.CanGrow = false;
			this.fFrom4.Height = 0.1875F;
			this.fFrom4.Left = 5.9375F;
			this.fFrom4.Name = "fFrom4";
			this.fFrom4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFrom4.Text = ">200";
			this.fFrom4.Top = 0.5625F;
			this.fFrom4.Width = 1F;
			// 
			// fFrom5
			// 
			this.fFrom5.CanGrow = false;
			this.fFrom5.Height = 0.1875F;
			this.fFrom5.Left = 5.9375F;
			this.fFrom5.Name = "fFrom5";
			this.fFrom5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFrom5.Text = "0-72000";
			this.fFrom5.Top = 0.75F;
			this.fFrom5.Width = 1F;
			// 
			// fFrom6
			// 
			this.fFrom6.CanGrow = false;
			this.fFrom6.Height = 0.1875F;
			this.fFrom6.Left = 5.9375F;
			this.fFrom6.Name = "fFrom6";
			this.fFrom6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFrom6.Text = ">72000";
			this.fFrom6.Top = 0.9375F;
			this.fFrom6.Width = 1F;
			// 
			// fFrom7
			// 
			this.fFrom7.CanGrow = false;
			this.fFrom7.Height = 0.1875F;
			this.fFrom7.Left = 5.9375F;
			this.fFrom7.Name = "fFrom7";
			this.fFrom7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFrom7.Text = "200000-750000";
			this.fFrom7.Top = 1.125F;
			this.fFrom7.Width = 1F;
			// 
			// fFrom8
			// 
			this.fFrom8.CanGrow = false;
			this.fFrom8.Height = 0.1875F;
			this.fFrom8.Left = 5.9375F;
			this.fFrom8.Name = "fFrom8";
			this.fFrom8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fFrom8.Text = ">200";
			this.fFrom8.Top = 1.3125F;
			this.fFrom8.Width = 1F;
			// 
			// lblMiscCharges
			// 
			this.lblMiscCharges.Height = 0.1875F;
			this.lblMiscCharges.HyperLink = null;
			this.lblMiscCharges.Left = 0.625F;
			this.lblMiscCharges.Name = "lblMiscCharges";
			this.lblMiscCharges.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left";
			this.lblMiscCharges.Text = "Misc Charges:";
			this.lblMiscCharges.Top = 0.1875F;
			this.lblMiscCharges.Width = 0.9375F;
			// 
			// rptRateTablesCond
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuni)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateTableDescr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCharge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblThru)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOver)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblStep)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFlat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblConsRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fService)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fMinCharge)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fMinCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc1Account)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc1Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc2Desc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc1Amount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc2Amount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fMisc2Account)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fThru8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fStep8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fRate8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRate8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFlat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fFrom8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMiscCharges)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fMinCharge;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fMinCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fMisc1Account;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fMisc1Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fMisc2Desc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fMisc1Amount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fMisc2Amount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fMisc2Account;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fThru1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fStep1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fRate1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fThru2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fStep2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fRate2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fThru3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fStep3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fRate3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fThru4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fStep4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fRate4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fThru5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fStep5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fRate5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fThru6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fStep6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fRate6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fThru7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fStep7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fRate7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fThru8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fStep8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fRate8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFlat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFrom1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFrom2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFrom3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFrom4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFrom5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFrom6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFrom7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fFrom8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMiscCharges;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuni;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRateTableDescr;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCons;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCharge;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblThru;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOver;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblStep;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFlat;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblConsRate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fService;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
