﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptReminderMailer.
	/// </summary>
	partial class rptReminderMailer
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReminderMailer));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.lblReturnAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReturnAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReturnAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblReturnAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPreMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMailingAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBulk = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPreMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBulk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.CanGrow = false;
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblReturnAddress1,
            this.lblReturnAddress2,
            this.lblReturnAddress3,
            this.lblReturnAddress4,
            this.lblName,
            this.lblMailingAddress1,
            this.lblMailingAddress2,
            this.lblMailingAddress3,
            this.lblAccount,
            this.lblMessage,
            this.lblHeader,
            this.lblPreMessage,
            this.lblAmount,
            this.lblMailingAddress4,
            this.lblBulk});
            this.Detail.Height = 4.385417F;
            this.Detail.Name = "Detail";
            this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // lblReturnAddress1
            // 
            this.lblReturnAddress1.Height = 0.1770833F;
            this.lblReturnAddress1.HyperLink = null;
            this.lblReturnAddress1.Left = 0F;
            this.lblReturnAddress1.MultiLine = false;
            this.lblReturnAddress1.Name = "lblReturnAddress1";
            this.lblReturnAddress1.Style = "font-family: \'Courier New\'";
            this.lblReturnAddress1.Tag = "TEXT";
            this.lblReturnAddress1.Text = " ";
            this.lblReturnAddress1.Top = 0F;
            this.lblReturnAddress1.Width = 3.6F;
            // 
            // lblReturnAddress2
            // 
            this.lblReturnAddress2.Height = 0.1770833F;
            this.lblReturnAddress2.HyperLink = null;
            this.lblReturnAddress2.Left = 0F;
            this.lblReturnAddress2.MultiLine = false;
            this.lblReturnAddress2.Name = "lblReturnAddress2";
            this.lblReturnAddress2.Style = "font-family: \'Courier New\'";
            this.lblReturnAddress2.Tag = "TEXT";
            this.lblReturnAddress2.Text = " ";
            this.lblReturnAddress2.Top = 0.1770833F;
            this.lblReturnAddress2.Width = 3.6F;
            // 
            // lblReturnAddress3
            // 
            this.lblReturnAddress3.Height = 0.1770833F;
            this.lblReturnAddress3.HyperLink = null;
            this.lblReturnAddress3.Left = 0F;
            this.lblReturnAddress3.MultiLine = false;
            this.lblReturnAddress3.Name = "lblReturnAddress3";
            this.lblReturnAddress3.Style = "font-family: \'Courier New\'";
            this.lblReturnAddress3.Tag = "TEXT";
            this.lblReturnAddress3.Text = " ";
            this.lblReturnAddress3.Top = 0.3541667F;
            this.lblReturnAddress3.Width = 3.6F;
            // 
            // lblReturnAddress4
            // 
            this.lblReturnAddress4.Height = 0.1770833F;
            this.lblReturnAddress4.HyperLink = null;
            this.lblReturnAddress4.Left = 0F;
            this.lblReturnAddress4.MultiLine = false;
            this.lblReturnAddress4.Name = "lblReturnAddress4";
            this.lblReturnAddress4.Style = "font-family: \'Courier New\'";
            this.lblReturnAddress4.Tag = "TEXT";
            this.lblReturnAddress4.Text = " ";
            this.lblReturnAddress4.Top = 0.53125F;
            this.lblReturnAddress4.Width = 3.6F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.18F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 3.5F;
            this.lblName.MultiLine = false;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Courier New\'";
            this.lblName.Tag = "TEXT";
            this.lblName.Text = " ";
            this.lblName.Top = 3.485F;
            this.lblName.Width = 3.6875F;
            // 
            // lblMailingAddress1
            // 
            this.lblMailingAddress1.Height = 0.18F;
            this.lblMailingAddress1.HyperLink = null;
            this.lblMailingAddress1.Left = 3.5F;
            this.lblMailingAddress1.MultiLine = false;
            this.lblMailingAddress1.Name = "lblMailingAddress1";
            this.lblMailingAddress1.Style = "font-family: \'Courier New\'";
            this.lblMailingAddress1.Tag = "TEXT";
            this.lblMailingAddress1.Text = " ";
            this.lblMailingAddress1.Top = 3.665F;
            this.lblMailingAddress1.Width = 3.6875F;
            // 
            // lblMailingAddress2
            // 
            this.lblMailingAddress2.Height = 0.18F;
            this.lblMailingAddress2.HyperLink = null;
            this.lblMailingAddress2.Left = 3.5F;
            this.lblMailingAddress2.MultiLine = false;
            this.lblMailingAddress2.Name = "lblMailingAddress2";
            this.lblMailingAddress2.Style = "font-family: \'Courier New\'";
            this.lblMailingAddress2.Tag = "TEXT";
            this.lblMailingAddress2.Text = " ";
            this.lblMailingAddress2.Top = 3.845F;
            this.lblMailingAddress2.Width = 3.6875F;
            // 
            // lblMailingAddress3
            // 
            this.lblMailingAddress3.Height = 0.18F;
            this.lblMailingAddress3.HyperLink = null;
            this.lblMailingAddress3.Left = 3.5F;
            this.lblMailingAddress3.MultiLine = false;
            this.lblMailingAddress3.Name = "lblMailingAddress3";
            this.lblMailingAddress3.Style = "font-family: \'Courier New\'";
            this.lblMailingAddress3.Tag = "TEXT";
            this.lblMailingAddress3.Text = " ";
            this.lblMailingAddress3.Top = 4.025F;
            this.lblMailingAddress3.Width = 3.6875F;
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.15F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0.35F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Courier New\'";
            this.lblAccount.Tag = "TEXT";
            this.lblAccount.Text = " ";
            this.lblAccount.Top = 1.05F;
            this.lblAccount.Width = 4.75F;
            // 
            // lblMessage
            // 
            this.lblMessage.Height = 1.09375F;
            this.lblMessage.HyperLink = null;
            this.lblMessage.Left = 0F;
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Style = "font-family: \'Courier New\'";
            this.lblMessage.Tag = "TEXT";
            this.lblMessage.Text = " ";
            this.lblMessage.Top = 2.28125F;
            this.lblMessage.Width = 7.0625F;
            // 
            // lblHeader
            // 
            this.lblHeader.Height = 0.2F;
            this.lblHeader.HyperLink = null;
            this.lblHeader.Left = 0F;
            this.lblHeader.Name = "lblHeader";
            this.lblHeader.Style = "font-family: \'Courier New\'";
            this.lblHeader.Tag = "TEXT";
            this.lblHeader.Text = " ";
            this.lblHeader.Top = 1.2F;
            this.lblHeader.Width = 7.05F;
            // 
            // lblPreMessage
            // 
            this.lblPreMessage.Height = 0.58125F;
            this.lblPreMessage.HyperLink = null;
            this.lblPreMessage.Left = 0F;
            this.lblPreMessage.Name = "lblPreMessage";
            this.lblPreMessage.Style = "font-family: \'Courier New\'";
            this.lblPreMessage.Tag = "TEXT";
            this.lblPreMessage.Text = " ";
            this.lblPreMessage.Top = 1.45F;
            this.lblPreMessage.Width = 7.0625F;
            // 
            // lblAmount
            // 
            this.lblAmount.Height = 0.19F;
            this.lblAmount.HyperLink = null;
            this.lblAmount.Left = 0F;
            this.lblAmount.Name = "lblAmount";
            this.lblAmount.Style = "font-family: \'Courier New\'";
            this.lblAmount.Tag = "TEXT";
            this.lblAmount.Text = " ";
            this.lblAmount.Top = 2.0625F;
            this.lblAmount.Width = 7.0625F;
            // 
            // lblMailingAddress4
            // 
            this.lblMailingAddress4.Height = 0.18F;
            this.lblMailingAddress4.HyperLink = null;
            this.lblMailingAddress4.Left = 3.5F;
            this.lblMailingAddress4.MultiLine = false;
            this.lblMailingAddress4.Name = "lblMailingAddress4";
            this.lblMailingAddress4.Style = "font-family: \'Courier New\'";
            this.lblMailingAddress4.Tag = "TEXT";
            this.lblMailingAddress4.Text = "  ";
            this.lblMailingAddress4.Top = 4.205F;
            this.lblMailingAddress4.Width = 3.6875F;
            // 
            // lblBulk
            // 
            this.lblBulk.Height = 1F;
            this.lblBulk.HyperLink = null;
            this.lblBulk.Left = 4.375F;
            this.lblBulk.Name = "lblBulk";
            this.lblBulk.Style = "font-family: \'Courier New\'";
            this.lblBulk.Tag = "TEXT";
            this.lblBulk.Text = " ";
            this.lblBulk.Top = 0F;
            this.lblBulk.Width = 3.125F;
            // 
            // PageHeader
            // 
            this.PageHeader.Height = 0F;
            this.PageHeader.Name = "PageHeader";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // rptReminderMailer
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.25F;
            this.PageSettings.Margins.Top = 0F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.510417F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.Disposed += new System.EventHandler(this.ActiveReport_Terminate);
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPreMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBulk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPreMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBulk;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
