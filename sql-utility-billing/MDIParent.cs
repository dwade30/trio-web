﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using modUTStatusPayments = Global.modUTFunctions;
using System.IO;
using SharedApplication.Messaging;
using TWSharedLibrary;

namespace TWUT0000
{
	public class MDIParent
	{
        public fecherFoundation.FCCommonDialog CommonDialog1;
        public MDIParent()
		{
			//
			// Required for Windows Form Designer support
			//
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            this.CommonDialog1 = new FCCommonDialog();
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;

		private int[] LabelNumber = new int[200 + 1];
		bool boolShowDemand;
		int lngFCode;
		bool boolDisable;
		bool boolLoaded;
		const string strTrio = "TRIO Software";
		private CommandDispatcher commandDispatcher;

        private cUTMDIView mainView;

		public void Init(CommandDispatcher commandDispatcher)
		{
			App.MainForm.NavigationMenu.Owner = this;
			App.MainForm.menuTree.ImageList = CreateImageList();
			App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
            App.MainForm.Caption = "UTILITY BILLING";
            App.MainForm.NavigationMenu.OriginName = "Utility Billing";
            //FC:FINAL:SBE - #3722 - add back code removed in SVN revision 9703 (reimplementation)
            //The same code was called from Init() -> SetMenuOptions() -> SetButtons() -> GetUtilityMenu()
            App.MainForm.ApplicationIcon = "icon-utility-billing";
            string assemblyName = this.GetType().Assembly.GetName().Name;
            if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
            {
                App.MainForm.ApplicationIcons.Add(assemblyName, "icon-utility-billing");
            }

            string strReturn = "";
			if (strReturn == string.Empty)
				strReturn = "False";
			modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
            mainView = new cUTMDIView();
            mainView.PanelChanged += MainView_PanelChanged;
			//SetupCustomMenuOptions(ref mainView);
            mainView.Reset();
            this.RefreshMenu();
            modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "Maximize Forms", ref strReturn);
		}

        private void RefreshMenu()
        {
            int CurRow = 0;
            cGenericCollection menuColl;
            menuColl = mainView.CurrentMenu;
            if (!(menuColl == null))
            {
                cMenuChoice tItem;
                menuColl.MoveFirst();
                CurRow = 1;
                while (menuColl.IsCurrent())
                {
                    tItem = (cMenuChoice)menuColl.GetCurrentItem();
                    AddMenuItem(ref CurRow, FCConvert.ToInt16(tItem.CommandCode), tItem.Description, tItem.SecurityCode, tItem.MenuCaption, tItem.ToolTip, tItem.CommandName, tItem.Enabled);
                    menuColl.MoveNext();
                }
            }
        }
        private void MainView_PanelChanged()
        {
            App.MainForm.StatusBarText3 = mainView.PanelMessage;
        }

        private void AddMenuItem(ref int intRow, short intCode, string strCaption, int lngFCode = 0, string strForceOption = "", string strToolTip = "", string strCommand = "", bool boolEnabled = true)
        {
            // vbPorter upgrade warning: strOption As string	OnWrite(string, short)
            string strOption;
            string imageKey = "";
            string itemName = "";

            bool hasSubItems = false;
            switch (intCode)
            {
                case (int)UT_Commands.Account_Meter_Update:
                    imageKey = "account-meter-update";
                    break;
                case (int)UT_Commands.Menu_BillingProcess:
                    mainView.SetMenu("Billing");
                    imageKey = "billing-process";
                    hasSubItems = true;
                    break;
                case (int)UT_Commands.Menu_CollectionProcess:
                    mainView.SetMenu("collections");
                    imageKey = "collections";
                    hasSubItems = true;
                    break;
                case (int)UT_Commands.Menu_Printing:
                    mainView.SetMenu("printing");
                    imageKey = "printing";
                    hasSubItems = true;
                    break;
                case (int)UT_Commands.TableFileSetup:
                    imageKey = "table-file-setup";
                    break;
                case (int)UT_Commands.Menu_FileMaintenance:
                    mainView.SetMenu("file");
                    imageKey = "file-maintenance";
                    hasSubItems = true;
                    break;
                case (int)UT_Commands.ExtractForRemoteReader:
                    imageKey = "extract-remote-reader";
                    break;
                case (int)UT_Commands.Menu_Custom:
                    mainView.SetMenu("custom");
                    imageKey = "street-maintenance";
                    hasSubItems = true;
                    break;
            }
           
            FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strCaption, "Execute:" + intCode, "main", boolEnabled, 1, imageKey);
            if (hasSubItems)
            {
                cGenericCollection menuColl;
                menuColl = mainView.CurrentMenu;
                if (!(menuColl == null))
                {
                    cMenuChoice tItem;
                    menuColl.MoveFirst();
                    int row = 1;
                    itemName = mainView.CurrentMenuName;
                    while (menuColl.IsCurrent())
                    {
                        
                        tItem = (cMenuChoice)menuColl.GetCurrentItem();
                        FCMenuItem newItem2 = newItem.SubItems.Add(tItem.Description, "Execute:" + tItem.CommandCode, itemName, boolEnabled, 2);
                        bool hasSubItems2 = false;
                        cGenericCollection menuColl2 = null;
                        switch (tItem.CommandCode)
                        {
                            case (int)UT_Commands.Menu_LienProcess:
                                
                                if (modGlobalConstants.Statics.clsSecurityClass.Check_Permissions(modMain.UTSECLIENPROCESS) == "F")
                                {
                                    menuColl2 = mainView.GetLienMenu();
                                    hasSubItems2 = true;
                                }

                                break;
                            case (int)UT_Commands.MeterReports:
                                menuColl2 = mainView.GetPrintMeterMenu();
                                hasSubItems2 = true;
                                break;
                            case (int)UT_Commands.Menu_FileMaintenance:
                                menuColl2 = mainView.GetFileMenu();
                                hasSubItems2 = true;
                                break;
                            case (int)UT_Commands.Menu_Custom:
                                menuColl2 = mainView.GetCustomMenu();
                                hasSubItems2 = true;
                                break;
                            case (int)UT_Commands.Menu_MeterFunctions:
                                menuColl2 = mainView.GetMeterFunctionsMenu();
                                hasSubItems2 = true;
                                break;
                        }
                        
                        if (hasSubItems2)
                        {
                            if (!(menuColl2 == null))
                            {
                                cMenuChoice tItem2;
                                menuColl2.MoveFirst();
                                var hasSubItems3 = false;
                                while (menuColl2.IsCurrent())
                                {
                                    var subMenuName = "";
                                    cGenericCollection menuColl3 = null;
                                    tItem2 = (cMenuChoice)menuColl2.GetCurrentItem();
                                    FCMenuItem newItem3 = newItem2.SubItems.Add(tItem2.Description, "Execute:" + tItem2.CommandCode, mainView.CurrentMenuName, tItem2.Enabled, 3);
                                    switch (tItem2.CommandCode)
                                    {
                                        case (int) UT_Commands.Menu_30DayNoticeProcess:
                                            menuColl3 = mainView.Get30DNMenu();
                                            subMenuName = "30DN";
                                            hasSubItems3 = true;
                                            break;
                                        case (int)UT_Commands.Menu_LienMaturity:
                                            subMenuName = "Maturity";
                                            menuColl3 = mainView.GetLienMaturityMenu();
                                            hasSubItems3 = true;
                                            break;
                                        case (int)UT_Commands.Menu_LienNotice:
                                            menuColl3 = mainView.GetLienNoticeMenu();
                                            subMenuName = "LIENNOTICE";
                                            hasSubItems3 = true;
                                            break;
                                    }

                                    if (hasSubItems3 && menuColl3 != null)
                                    {
                                        menuColl3.MoveFirst();
                                        while (menuColl3.IsCurrent())
                                        {
                                            var tItem3 = (cMenuChoice)menuColl3.GetCurrentItem();
                                            newItem3.SubItems.Add(tItem3.Description, "Execute:" + tItem3.CommandCode, subMenuName, tItem3.Enabled, 4);
                                            menuColl3.MoveNext();
                                        }
                                    }
                                    menuColl2.MoveNext();
                                }


                            }
                        }
                        row++;
                        menuColl.MoveNext();
                    }
                }
            }
            intRow += 1;
        }

  //      public void SetButtons()
		//{
		//	GetUtilityMenu();
			
		//}

		private void MDIForm_Unload_2(short Cancel)
		{
			MDIForm_Unload(new object(), new FCFormClosingEventArgs(FCCloseReason.FormCode, FCConvert.ToBoolean(Cancel)));
		}

		private void MDIForm_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				modBlockEntry.WriteYY();
                App.MainForm.OpenModule("TWGNENTY");
				FCUtils.CloseFormsOnProject();
				boolLoaded = false;
				Application.Exit();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Unloading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		



		public ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-account-meter-update", "account-meter-update"),
				new ImageListEntry("menutree-billing-process", "billing-process"),
				new ImageListEntry("menutree-collections", "collections"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-table-file-setup", "table-file-setup"),
				new ImageListEntry("menutree-extract-remote-reader", "extract-remote-reader"),
				new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
				new ImageListEntry("menutree-account-meter-update-active", "account-meter-update-active"),
				new ImageListEntry("menutree-billing-process-active", "billing-process-active"),
				new ImageListEntry("menutree-collections-active", "collections-active"),
				new ImageListEntry("menutree-printing-active", "printing-active"),
				new ImageListEntry("menutree-table-file-setup-active", "table-file-setup-active"),
				new ImageListEntry("menutree-extract-remote-reader-active", "extract-remote-reader-active"),
				new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
				new ImageListEntry("menutree-street-maintenance", "street-maintenance")
			});
			return imageList;
		}

		public void UTAutoSizeMenu_6(FCGrid Grid, short intLabels)
		{
			UTAutoSizeMenu(ref Grid, ref intLabels);
		}

		public void UTAutoSizeMenu(ref FCGrid Grid, ref short intLabels)
		{
			int i;
			int FullRowHeight = 0;
			FullRowHeight = FCConvert.ToInt32(Grid.Height / intLabels);
			// find the average height of the row
			for (i = 1; i <= 20; i++)
			{
				// this just assigns the row height
				Grid.RowHeight(i, FullRowHeight);
			}
			Grid.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			Grid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

        public void Execute(int code)
        {
            mainView.ExecuteMenuCommand(App.MainForm.NavigationMenu.CurrentItem.Menu, code);
        }

        public class StaticVariables
		{
			/// </summary>
			/// Summary description for MDIParent.
			/// <summary>
			public int R = 0, c;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
