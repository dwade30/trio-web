﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTAuditInfo.
	/// </summary>
	public partial class frmUTAuditInfo : BaseForm
	{
		public frmUTAuditInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            txtSIntRate.AllowOnlyNumericInput(true);
            txtWIntRate.AllowOnlyNumericInput(true);
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTAuditInfo InstancePtr
		{
			get
			{
				return (frmUTAuditInfo)Sys.GetInstance(typeof(frmUTAuditInfo));
			}
		}

		protected frmUTAuditInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/19/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/29/2006              *
		// ********************************************************
		public int lngRKey;
		bool boolLoaded;
		int intType;
		DateTime dtLienDate;
		DateTime dtBillDate;
		DateTime dtStartDate;
		bool boolNew;
		int lngRateKey;
		bool boolDirty;
		// vbPorter upgrade warning: intIncomingType As short	OnWriteFCConvert.ToInt32(
		public void Init(short intIncomingType, DateTime dtPassLienDate, DateTime dtPassBillDate, DateTime dtPassStartDate, ref int lngPassRateKey)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				intType = intIncomingType;
				// 10 is a lien rec, 1 is a load back, 2 is a final billed
				boolNew = true;
				//optRateRecType[0].Enabled = true;
				cmbRateRecType.Text = "Regular";
				switch (intType)
				{
					case 1:
						{
							// Load Back
							//optRateRecType[0].Checked = true;
							cmbRateRecType.Visible = true;
							//FC:FINAL:MSH - i.issue #1168: change visibility of label
							lblRateRecType.Visible = true;
							if (modUTStatusPayments.Statics.TownService != "B")
							{
								if (modUTStatusPayments.Statics.TownService == "W")
								{
									txtSIntRate.Enabled = false;
								}
								else
								{
									txtSIntRate.Enabled = true;
								}
								if (modUTStatusPayments.Statics.TownService == "S")
								{
									txtWIntRate.Enabled = false;
								}
								else
								{
									txtWIntRate.Enabled = true;
								}
							}
							else
							{
								txtSIntRate.Enabled = true;
								txtWIntRate.Enabled = true;
							}
							lblWIntRateTitle.Enabled = txtWIntRate.Enabled;
							lblSIntRateTitle.Enabled = txtSIntRate.Enabled;
							break;
						}
					case 2:
						{
							// Final Billed
							cmbRateRecType.Visible = false;
							//FC:FINAL:MSH - i.issue #1168: change visibility of label
							lblRateRecType.Visible = false;
							//optRateRecType[1].Checked = false;
							//optRateRecType[0].Enabled = true;
							//optRateRecType[0].Checked = true;
							txtWIntRate.Text = "0.00";
							txtSIntRate.Text = "0.00";
							if (modUTStatusPayments.Statics.TownService != "B")
							{
								if (modUTStatusPayments.Statics.TownService == "W")
								{
									txtSIntRate.Enabled = false;
								}
								else
								{
									txtSIntRate.Enabled = true;
								}
								if (modUTStatusPayments.Statics.TownService == "S")
								{
									txtWIntRate.Enabled = false;
								}
								else
								{
									txtWIntRate.Enabled = true;
								}
							}
							else
							{
								txtSIntRate.Enabled = true;
								txtWIntRate.Enabled = true;
							}
							break;
						}
					case 10:
						{
							// Lien Rec
							cmbRateRecType.Visible = false;
							//FC:FINAL:MSH - i.issue #1168: change visibility of label
							lblRateRecType.Visible = false;
							cmbRateRecType.Items.RemoveAt(0);
							cmbRateRecType.Text = "Lien";
							//optRateRecType[1].Checked = true;
							//optRateRecType[0].Enabled = false;
							txtWIntRate.Text = "0.00";
							txtSIntRate.Text = "0.00";
							break;
						}
					case 100:
						{
							// Edit Key
							lngRateKey = lngPassRateKey;
							LoadRateInformation();
							cmbRateRecType.Enabled = false;
							this.Text = "Update Rate Record";
							boolNew = false;
							// this will allow the user to edit an existing rate key
							break;
						}
					case 101:
						{
							// Create Bills
							cmbRateRecType.Visible = false;
							//FC:FINAL:MSH - i.issue #1168: change visibility of label
							lblRateRecType.Visible = false;
							//optRateRecType[1].Checked = false;
							//optRateRecType[0].Enabled = true;
							//optRateRecType[0].Checked = true;
							txtWIntRate.Text = "0.00";
							txtSIntRate.Text = "0.00";
							this.Text = "Create Rate Record";
							txtDescription.Text = "Regular";
							break;
						}
					case 110:
						{
							// Split Bills
							cmbRateRecType.Visible = false;
							//FC:FINAL:MSH - i.issue #1168: change visibility of label
							lblRateRecType.Visible = false;
							//optRateRecType[1].Checked = false;
							//optRateRecType[0].Enabled = true;
							//optRateRecType[0].Checked = true;
							txtWIntRate.Text = "0.00";
							txtSIntRate.Text = "0.00";
							this.Text = "Create Rate Record";
							txtDescription.Text = "Regular Split";
							break;
						}
					default:
						{
							cmbRateRecType.Visible = true;
							//FC:FINAL:MSH - i.issue #1168: change visibility of label
							lblRateRecType.Visible = true;
							cmbRateRecType.Text = "Regular";
							;
							break;
						}
				}
				//end switch
				if (boolNew)
				{
					dtLienDate = dtPassLienDate;
					dtBillDate = dtPassBillDate;
					dtStartDate = dtPassStartDate;
				}
				if (intType == 110)
				{
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
					this.Show(FCForm.FormShowEnum.Modal);
					lngPassRateKey = lngRateKey;
				}
				else
				{
					this.Show(App.MainForm);
					//Application.DoEvents();
					this.Focus();
				}
				//FC:FINAL:MSH - i.issue #1168: replace header text by text from form title
				this.HeaderText.Text = this.Text;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmUTAuditInfo_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				switch (intType)
				{
					case 10:
						{
							// this is coming from the transfer tax to lien screen
							lblBillDate.Text = "Lien Date:";
							if (dtLienDate.ToOADate() != 0)
							{
								t2kBilldate.Text = Strings.Format(dtLienDate, "MM/dd/yyyy");
								txtEndDate.Text = Strings.Format(dtLienDate, "MM/dd/yyyy");
								txtIntDate.Text = Strings.Format(DateAndTime.DateAdd("D", 1, dtLienDate), "MM/dd/yyyy");
							}
							if (dtBillDate.ToOADate() != 0)
							{
								t2kBilldate.Text = Strings.Format(dtLienDate, "MM/dd/yyyy");
								txtEndDate.Text = Strings.Format(dtLienDate, "MM/dd/yyyy");
							}
							if (dtLienDate.ToOADate() != 0)
							{
								txtIntDate.Text = Strings.Format(dtBillDate, "MM/dd/yyyy");
							}
							// only do this for liens so that they water and sewer liens can be seperated
							if (frmRateRecChoice.InstancePtr.cmbWS.Text == "Water")
							{
								// water
								txtDescription.Text = "Water Tax Liens";
								txtWIntRate.Enabled = true;
								lblWIntRateTitle.Enabled = true;
								txtSIntRate.Enabled = false;
								lblSIntRateTitle.Enabled = false;
							}
							else
							{
								// sewer
								txtDescription.Text = "Sewer Tax Liens";
								txtWIntRate.Enabled = false;
								lblWIntRateTitle.Enabled = false;
								txtSIntRate.Enabled = true;
								lblSIntRateTitle.Enabled = true;
							}
							break;
						}
				}
				//end switch
			}
		}

		private void frmUTAuditInfo_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Escape)
			{
				KeyCode = (Keys)0;
				mnuFileExit_Click();
			}
		}

		private void frmUTAuditInfo_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				if (this.ActiveControl is FCTextBox || this.ActiveControl is FCComboBox || this.ActiveControl is Global.T2KDateBox)
				{
					Support.SendKeys("{TAB}", false);
				}
			}
			else if (KeyAscii == Keys.F16 || KeyAscii == Keys.Back)
			{
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmUTAuditInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTAuditInfo properties;
			//frmUTAuditInfo.ScaleWidth	= 9300;
			//frmUTAuditInfo.ScaleHeight	= 7455;
			//frmUTAuditInfo.LinkTopic	= "Form1";
			//frmUTAuditInfo.LockControls	= true;
			//End Unmaped Properties
			modGlobalFunctions.SetTRIOColors(this);
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblWIntRateTitle.Text = "Stormwater Interest Rate:";
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				switch (intType)
				{
					case 110:
						{
							// do nothing
							break;
						}
					case 100:
						{
							frmRateRecChoice.InstancePtr.Unload();
							break;
						}
					case 10:
						{
							frmRateRecChoice.InstancePtr.intRateType = 6;
							frmRateRecChoice.InstancePtr.Show(App.MainForm);
							break;
						}
					case 1:
					case 2:
						{
							// reload the combo on the other screen
							frmLoadBack.InstancePtr.LoadRateKeys();
							// find the correct rate key
							for (lngCT = 0; lngCT <= frmLoadBack.InstancePtr.cboRateKeys.Items.Count - 1; lngCT++)
							{
								if (Conversion.Val(Strings.Left(frmLoadBack.InstancePtr.cboRateKeys.Items[lngCT].ToString(), 5)) == lngRateKey)
								{
									frmLoadBack.InstancePtr.cboRateKeys.SelectedIndex = lngCT;
									break;
								}
							}
							frmLoadBack.InstancePtr.Show(App.MainForm);
							break;
						}
					default:
						{
							frmRateRecChoice.InstancePtr.Show(App.MainForm);
							break;
						}
				}
				//end switch
				boolLoaded = false;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Query Unload", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuFileExit_Click()
		{
			mnuFileExit_Click(mnuFileExit, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (CheckEntries())
			{
				SaveRateRec();
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (CheckEntries())
			{
				SaveRateRec();
			}
		}

		private void SaveRateRec()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will save the new Rate Record
				clsDRWrapper rsRate = new clsDRWrapper();
				string strNumofBills = "";
				bool blnUpdateBillDate;
				clsDRWrapper rsBill = new clsDRWrapper();
				if (intType == 100)
				{
					rsRate.OpenRecordset("SELECT Count(ID) AS Num FROM Bill WHERE BillingRateKey = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
					if (!rsRate.EndOfFile())
					{
						// TODO Get_Fields: Field [Num] not found!! (maybe it is an alias?)
						if (rsRate.Get_Fields("Num") > 1)
						{
							// TODO Get_Fields: Field [Num] not found!! (maybe it is an alias?)
							strNumofBills = rsRate.Get_Fields("Num") + " bills.";
						}
						// TODO Get_Fields: Field [Num] not found!! (maybe it is an alias?)
						else if (FCConvert.ToInt32(rsRate.Get_Fields("Num")) == 1)
						{
							strNumofBills = "one bill.";
						}
						else
						{
							strNumofBills = "no bills.";
						}
					}
					else
					{
						strNumofBills = "at least one bill.";
					}
					switch (MessageBox.Show("This will affect " + strNumofBills + "  Are you sure that you would like to change this rate record?", "Change Rate Information", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
					{
						case DialogResult.Yes:
							{
								// keep rocking
								modGlobalFunctions.AddCYAEntry_26("UT", "Changing Rate Record", "Rate Record = " + FCConvert.ToString(lngRateKey));
								break;
							}
						case DialogResult.No:
						case DialogResult.Cancel:
							{
								MessageBox.Show("Save cancelled.", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
					}
					//end switch
					rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
					if (rsRate.EndOfFile())
					{
						MessageBox.Show("Cannot find Rate Record #" + FCConvert.ToString(lngRateKey) + ".", "Missing Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					rsRate.Edit();
				}
				else
				{
					rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = 0", modExtraModules.strUTDatabase);
					rsRate.AddNew();
					// lngRateKey = .Fields("ID")
				}
				switch (intType)
				{
					case 100:
						{
							// editing a rate rec
							if (cmbRateRecType.Text == "Lien")
							{
								rsRate.Set_Fields("RateType", "L");
							}
							else if (cmbRateRecType.Text == "Regular")
							{
								rsRate.Set_Fields("RateType", "R");
							}
							else
							{
								rsRate.Set_Fields("RateType", "S");
							}
							break;
						}
					case 10:
						{
							// lien rate rec for transfer to lien
							rsRate.Set_Fields("RateType", "L");
							break;
						}
					case 1:
						{
							// this is when a load back occurs
							if (cmbRateRecType.Text == "Lien")
							{
								rsRate.Set_Fields("RateType", "L");
							}
							else if (cmbRateRecType.Text == "Regular")
							{
								rsRate.Set_Fields("RateType", "R");
							}
							else
							{
								rsRate.Set_Fields("RateType", "S");
							}
							break;
						}
					default:
						{
							rsRate.Set_Fields("RateType", "R");
							break;
						}
				}
				//end switch
				// blnUpdateBillDate = (.Fields("BillDate") <> CDate(t2kBilldate.Text))        'MAL@20070917: Check to see if Bill Date has changed
				rsRate.Set_Fields("BillDate", DateAndTime.DateValue(t2kBilldate.Text));
				rsRate.Set_Fields("Start", DateAndTime.DateValue(txtStartDate.Text));
				rsRate.Set_Fields("End", DateAndTime.DateValue(txtEndDate.Text));
				rsRate.Set_Fields("DateCreated", DateTime.Now);
				rsRate.Set_Fields("IntStart", DateAndTime.DateValue(txtIntDate.Text));
				rsRate.Set_Fields("DueDate", DateAndTime.DateValue(txtDueDate.Text));
				// kgk 08-17-2012  Let's not leave these NULL
				rsRate.Set_Fields("30DNDateW", "12/30/1899");
				rsRate.Set_Fields("LienDateW", "12/30/1899");
				rsRate.Set_Fields("MaturityDateW", "12/30/1899");
				rsRate.Set_Fields("30DNDateS", "12/30/1899");
				rsRate.Set_Fields("LienDateS", "12/30/1899");
				rsRate.Set_Fields("MaturityDateS", "12/30/1899");
				if (Conversion.Val(txtSIntRate.Text) == 0)
				{
					rsRate.Set_Fields("SIntRate", 0);
				}
				else
				{
					rsRate.Set_Fields("SIntRate", FCConvert.ToDouble(txtSIntRate.Text) / 100);
				}
				if (Conversion.Val(txtWIntRate.Text) == 0)
				{
					rsRate.Set_Fields("WIntRate", 0);
				}
				else
				{
					rsRate.Set_Fields("WIntRate", FCConvert.ToDouble(txtWIntRate.Text) / 100);
				}
				rsRate.Set_Fields("Description", Strings.Trim(txtDescription.Text));
				rsRate.Update();
				lngRateKey = FCConvert.ToInt32(rsRate.Get_Fields_Int32("ID"));
				// MAL@20070917: Update Bill table with new Bill Date
				// MAL@20071012: Changed to loop through even if the date has not been changed in case there are records previously missed
				// Call Reference: 10881
				// If blnUpdateBillDate Then
				rsBill.OpenRecordset("SELECT * FROM Bill WHERE BillingRateKey = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
				if (rsBill.RecordCount() > 0)
				{
					rsBill.MoveFirst();
					while (!rsBill.EndOfFile())
					{
						if (rsBill.Get_Fields_DateTime("BillDate").ToOADate() != DateAndTime.DateValue(t2kBilldate.Text).ToOADate())
						{
							rsBill.Edit();
							rsBill.Set_Fields("BillDate", DateAndTime.DateValue(t2kBilldate.Text));
							rsBill.Update();
						}
						rsBill.MoveNext();
					}
				}
				// End If
				if (intType != 100)
				{
					// when this saves correctly, unload the frmRateRecChoice form and
					// reload it so that the new rate rec is included in the year
					//Application.DoEvents();
					frmRateRecChoice.InstancePtr.Unload();
					//Application.DoEvents();
					frmRateRecChoice.InstancePtr.intRateType = intType;
					// frmRateRecChoice.Show
				}
				switch (intType)
				{
				// return nothing to the rate rec choice screen
					case 10:
						{
							// lien record
							frmRateRecChoice.InstancePtr.intRateType = 3;
							// 100
							break;
						}
					case 1:
						{
							// load back
							frmRateRecChoice.InstancePtr.intRateType = 100;
							break;
						}
					case 2:
						{
							// load back
							frmRateRecChoice.InstancePtr.intRateType = 201;
							break;
						}
					case 101:
						{
							frmRateRecChoice.InstancePtr.boolAddedRK = true;
							break;
						}
				}
				//end switch
				boolDirty = false;
				frmRateRecChoice.InstancePtr.Unload();
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("ERROR #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Save Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckEntries()
		{
			bool CheckEntries = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp;
				int X;
				string strIDate = "";
				string strDDate = "";
				DateTime dtTemp1;
				DateTime dtTemp2;
				CheckEntries = false;
				strTemp = txtWIntRate.Text;
				if (modUTStatusPayments.Statics.TownService != "S")
				{
					if (Conversion.Val(strTemp) < 0 || Strings.Trim(strTemp) == "" || Conversion.Val(strTemp) > 100)
					{
						// kk trouts-6 03012013  Change Water to Stormwater for Bangor
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							MessageBox.Show("The Stormwater Interest Rate is not valid. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						else
						{
							MessageBox.Show("The Water Interest Rate is not valid. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						txtWIntRate.Focus();
						return CheckEntries;
					}
				}
				strTemp = txtSIntRate.Text;
				if (modUTStatusPayments.Statics.TownService != "W")
				{
					if (Conversion.Val(strTemp) < 0 || Strings.Trim(strTemp) == "" || Conversion.Val(strTemp) > 100)
					{
						MessageBox.Show("The Sewer Interest Rate is not valid. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						txtSIntRate.Focus();
						return CheckEntries;
					}
				}
				strTemp = t2kBilldate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The Billing Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					t2kBilldate.Focus();
					return CheckEntries;
				}
				strTemp = txtStartDate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The Start Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtStartDate.Focus();
					return CheckEntries;
				}
				strTemp = txtEndDate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The End Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEndDate.Focus();
					return CheckEntries;
				}
				strTemp = txtIntDate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The Interest Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtIntDate.Focus();
					return CheckEntries;
				}
				strTemp = txtDueDate.Text;
				if (!Information.IsDate(strTemp))
				{
					MessageBox.Show("The Due Date is not a valid date. Save Failed" + ".", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtDueDate.Focus();
					return CheckEntries;
				}
				CheckEntries = true;
				return CheckEntries;
			}
			catch (Exception ex)
			{
				
				CheckEntries = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "." + "\r\n" + "Occured in Check Entries.", "Validation Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckEntries;
		}

		private bool LoadRateInformation()
		{
			bool LoadRateInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsLoad = new clsDRWrapper();
				int intCT;
				rsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
				if (!rsLoad.EndOfFile())
				{
					if (FCConvert.ToString(rsLoad.Get_Fields_String("RateType")) == "R")
					{
						cmbRateRecType.Text = "Regular";
						//optRateRecType[0].Checked = true;
						//optRateRecType[1].Checked = false;
						//optRateRecType[2].Checked = false;
					}
					else if (rsLoad.Get_Fields_String("RateType") == "L")
					{
						//optRateRecType[0].Checked = false;
						//optRateRecType[1].Checked = true;
						//optRateRecType[2].Checked = false;
						cmbRateRecType.Text = "Lien";
					}
					else
					{
						//optRateRecType[0].Checked = false;
						//optRateRecType[1].Checked = false;
						//optRateRecType[2].Checked = true;
						cmbRateRecType.Text = "Supplemental";
					}
					t2kBilldate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
					txtIntDate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy");
					// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
					txtDueDate.Text = Strings.Format(rsLoad.Get_Fields("DueDate"), "MM/dd/yyyy");
					txtStartDate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("Start"), "MM/dd/yyyy");
					// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
					txtEndDate.Text = Strings.Format(rsLoad.Get_Fields("End"), "MM/dd/yyyy");
					txtWIntRate.Text = FCConvert.ToString(rsLoad.Get_Fields_Double("WIntRate") * 100);
					txtSIntRate.Text = FCConvert.ToString(rsLoad.Get_Fields_Double("SIntRate") * 100);
					txtDescription.Text = FCConvert.ToString(rsLoad.Get_Fields_String("Description"));
					LoadRateInformation = true;
				}
				else
				{
					MessageBox.Show("Cannot Load Rate Record #" + FCConvert.ToString(lngRateKey) + ".", "Missing Rate Record", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					LoadRateInformation = false;
					return LoadRateInformation;
				}
				return LoadRateInformation;
			}
			catch (Exception ex)
			{
				
				LoadRateInformation = false;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "." + "\r\n" + "Occured in Check Entries.", "Load Record Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadRateInformation;
		}

		private void t2kBilldate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtDescription_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtEndDate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtIntDate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtDueDate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtSIntRate_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtSIntRate_Enter(object sender, System.EventArgs e)
		{
			txtSIntRate.SelectionStart = 0;
			txtSIntRate.SelectionLength = txtSIntRate.Text.Length;
		}

		private void txtSIntRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Delete)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtSIntRate.Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtSIntRate.SelectionStart < lngDecPlace && txtSIntRate.SelectionLength + txtSIntRate.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStartDate_Change(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtWIntRate_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtWIntRate_Enter(object sender, System.EventArgs e)
		{
			txtWIntRate.SelectionStart = 0;
			txtWIntRate.SelectionLength = txtWIntRate.Text.Length;
		}

		private void txtWIntRate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Insert))
			{
				// do nothing
			}
			else if (KeyAscii == Keys.Delete)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtWIntRate.Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtWIntRate.SelectionStart < lngDecPlace && txtWIntRate.SelectionLength + txtWIntRate.SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
