﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTCustomize.
	/// </summary>
	partial class frmUTCustomize : BaseForm
	{
		public fecherFoundation.FCComboBox cmbSBillTo;
		public fecherFoundation.FCComboBox cmbWBillTo;
		public fecherFoundation.FCComboBox cmbPayFirst;
		public fecherFoundation.FCComboBox cmbBasis;
		public fecherFoundation.FCComboBox cmbInterestType;
		public fecherFoundation.FCComboBox cmbReceiptSize;
		public fecherFoundation.FCLabel lblReceiptSize;
		public fecherFoundation.FCComboBox cmbExportFormat;
		public fecherFoundation.FCLabel lblExportFormat;
		public fecherFoundation.FCComboBox cmbSeq;
		public fecherFoundation.FCComboBox cmbDEType;
		public fecherFoundation.FCComboBox cmbOptions;
		public fecherFoundation.FCLabel lblOptions;
		public fecherFoundation.FCComboBox cmbBillingRepSeq;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblTaxRate;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkInterest;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblConsIncrease;
		public fecherFoundation.FCFrame fraBillOptions;
		public fecherFoundation.FCCheckBox chkInclChangeOutCons;
		public fecherFoundation.FCCheckBox chkShowLienAmtOnBill;
		public fecherFoundation.FCCheckBox chkChpt660;
		public fecherFoundation.FCComboBox cmbUnitsOnBill;
		public fecherFoundation.FCFrame fraTaxRate;
		public fecherFoundation.FCTextBox txtTaxRate;
		public fecherFoundation.FCLabel lblTaxRate_0;
		public fecherFoundation.FCLabel lblTaxRate_1;
		public fecherFoundation.FCComboBox cmbDefaultBill;
		public FCGrid txtPrinAcctS;
		public FCGrid txtTaxAcctS;
		public FCGrid txtPrinAcctW;
		public FCGrid txtTaxAcctW;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblPrinAcctW;
		public fecherFoundation.FCLabel lblTaxAcctW;
		public fecherFoundation.FCLabel lblPrinAcctS;
		public fecherFoundation.FCLabel lblTaxAcctS;
		public fecherFoundation.FCLabel lblSBillTo;
		public fecherFoundation.FCLabel lblWBillTo;
		public fecherFoundation.FCLabel lblDefaultBill;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCFrame fraAdjustments;
		public fecherFoundation.FCTextBox txtLDNSideAdj;
		public fecherFoundation.FCTextBox txtLienSideAdj;
		public fecherFoundation.FCTextBox txtLDNBottomAdj;
		public fecherFoundation.FCTextBox txtLienBottomAdj;
		public fecherFoundation.FCTextBox txtLienTopAdj;
		public fecherFoundation.FCTextBox txtLDNTopAdj;
		public fecherFoundation.FCTextBox txtSigAdjust_Lien;
		public fecherFoundation.FCTextBox txtMeterSlipsHAdj;
		public fecherFoundation.FCTextBox txtMeterSlipsVAdj;
		public fecherFoundation.FCTextBox txtDMVLabelAdjust;
		public fecherFoundation.FCTextBox txtDMHLabelAdjust;
		public fecherFoundation.FCTextBox txtCMFAdjust;
		public fecherFoundation.FCTextBox txtLabelAdjustment;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel lblLienSideAdj;
		public fecherFoundation.FCLabel Label17;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel lblSigAdjust_Lien;
		public fecherFoundation.FCLabel lblMeterSlipsHAdj;
		public fecherFoundation.FCLabel lblMeterSlipsVAdj;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel lblDMVLabelAdjust;
		public fecherFoundation.FCLabel lblDMHLabelAdjust;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel lblCMFAdjust;
		public fecherFoundation.FCLabel lblLabelAdjustment;
		public fecherFoundation.FCFrame fraPaymentOptions;
		public fecherFoundation.FCCheckBox chkSkipLDNPrompt;
		public fecherFoundation.FCCheckBox chkDisableAutoPmtFill;
		public fecherFoundation.FCCheckBox chkDefaultUTRTDDate;
		public fecherFoundation.FCComboBox cboAutopay;
		public fecherFoundation.FCCheckBox chkDefaultAccount;
		public fecherFoundation.FCTextBox txtDiscountPercent;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel lblPayFirst;
		public fecherFoundation.FCLabel lblBasis;
		public fecherFoundation.FCLabel lblDiscountPercent;
		public fecherFoundation.FCFrame fraInterest;
		public fecherFoundation.FCCheckBox chkTACheckRE;
		public fecherFoundation.FCCheckBox chkTAUpdateRE;
		public fecherFoundation.FCCheckBox chkShowTaxAcquired;
		public fecherFoundation.FCCheckBox chkUseBookForLiens;
		public fecherFoundation.FCCheckBox chkOnlyCheckOldestLien;
		public fecherFoundation.FCTextBox txtPPayInterestRate;
		public fecherFoundation.FCCheckBox chkInterest_3;
		public fecherFoundation.FCCheckBox chkInterest_2;
		public fecherFoundation.FCCheckBox chkInterest_1;
		public fecherFoundation.FCCheckBox chkInterest_0;
		public fecherFoundation.FCTextBox txtFlatInterestAmount;
		public fecherFoundation.FCLabel lblPPayInterestRate;
		public fecherFoundation.FCLabel lblChargeInterestOn;
		public fecherFoundation.FCLabel lblFlatInterestRate;
		public fecherFoundation.FCLabel lblInterestType;
		public fecherFoundation.FCFrame fraOther;
		public fecherFoundation.FCFrame fraReturnAddress;
		public FCGrid vsReturnAddress;
		public fecherFoundation.FCFrame fraMultiplier;
		public fecherFoundation.FCComboBox cmbMultiplier;
		public fecherFoundation.FCLabel lblMultiplier;
		public fecherFoundation.FCLabel lblMultiplier2;
		public fecherFoundation.FCFrame fraTownService;
		public fecherFoundation.FCComboBox cmbService;
		public fecherFoundation.FCLabel lblService;
		public fecherFoundation.FCFrame fraRemoteReader;
		public fecherFoundation.FCCheckBox chkForceReadDate;
		public fecherFoundation.FCCheckBox chkShowLocation;
		public fecherFoundation.FCComboBox cmbExtractFileType;
		public fecherFoundation.FCLabel lblExtractFileType;
		public fecherFoundation.FCFrame fraReportSequence;
		public fecherFoundation.FCCheckBox chkMapLot;
		public fecherFoundation.FCLabel LblSeq;
		public fecherFoundation.FCFrame fraIConnect;
		public fecherFoundation.FCTextBox txtInvStartDate;
		public fecherFoundation.FCButton cmdSetupICAccounts;
		public fecherFoundation.FCCheckBox chkUseCRAccounts;
		public FCGrid txtMFeeAccount;
		public fecherFoundation.FCLabel lblInvStartDate;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCFrame fraDefaultOptions;
		public fecherFoundation.FCCheckBox chkDefaultMHInfo;
		public fecherFoundation.FCCheckBox chkDefaultRE;
		public fecherFoundation.FCComboBox cmbState;
		public fecherFoundation.FCTextBox txtDefaultZip;
		public fecherFoundation.FCTextBox txtDefaultCity;
		public fecherFoundation.FCComboBox cmbDefaultCategoryS;
		public fecherFoundation.FCComboBox cmbDefaultFrequency;
		public fecherFoundation.FCComboBox cmbDefaultDigits;
		public fecherFoundation.FCComboBox cmbDefaultSize;
		public fecherFoundation.FCComboBox cmbDefaultCategoryW;
		public FCGrid txtDefaultAccountW;
		public FCGrid txtDefaultAccountS;
		public fecherFoundation.FCLabel lblDefaultCity;
		public fecherFoundation.FCLabel lblDefaultState;
		public fecherFoundation.FCLabel lblDefaultZip;
		public fecherFoundation.FCLabel lblDefaultAccountW;
		public fecherFoundation.FCLabel lblDefaultAccountS;
		public fecherFoundation.FCLabel lblDefaultCategoryS;
		public fecherFoundation.FCLabel lblDefaultMeter;
		public fecherFoundation.FCLabel lblDDefaultFrequency;
		public fecherFoundation.FCLabel lblDefaultDigits;
		public fecherFoundation.FCLabel lblDefaultSize;
		public fecherFoundation.FCLabel lblDefaultAccount;
		public fecherFoundation.FCLabel lblDefaultCategoryW;
		public fecherFoundation.FCFrame fraBilling;
		public fecherFoundation.FCCheckBox chkBillStormwaterFee;
		public fecherFoundation.FCCheckBox chkLimitConsBill;
		public fecherFoundation.FCComboBox cboApplyTo;
		public fecherFoundation.FCCheckBox chkAutoPrepayments;
		public fecherFoundation.FCCheckBox chkExportNoBillRecords;
		public fecherFoundation.FCCheckBox chkStopOnConsumption;
		public fecherFoundation.FCComboBox cmbDEOrder;
		public fecherFoundation.FCFrame fraConsIncrease;
		public fecherFoundation.FCCheckBox chkMinVarWarning;
		public fecherFoundation.FCTextBox txtConsIncrease;
		public fecherFoundation.FCLabel lblConsIncrease_1;
		public fecherFoundation.FCLabel lblConsIncrease_0;
		public fecherFoundation.FCLabel lblApplyTo;
		public fecherFoundation.FCLabel lblDEType;
		public fecherFoundation.FCLabel lblDEOrder;
		public fecherFoundation.FCFrame fraBillDetDesc;
		public fecherFoundation.FCTextBox txtAdj;
		public fecherFoundation.FCTextBox txtMisc;
		public fecherFoundation.FCTextBox txtTax;
		public fecherFoundation.FCTextBox txtLiens;
		public fecherFoundation.FCTextBox txtInterest;
		public fecherFoundation.FCTextBox txtOverride;
		public fecherFoundation.FCTextBox txtRegular;
		public fecherFoundation.FCTextBox txtCredit;
		public fecherFoundation.FCTextBox txtPastDue;
		public fecherFoundation.FCLabel lblAdj;
		public fecherFoundation.FCLabel lblMisc;
		public fecherFoundation.FCLabel lblTax;
		public fecherFoundation.FCLabel lblLiens;
		public fecherFoundation.FCLabel lblInterest;
		public fecherFoundation.FCLabel lblOverride;
		public fecherFoundation.FCLabel lblRegular;
		public fecherFoundation.FCLabel lblCredit;
		public fecherFoundation.FCLabel lblPastDue;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCFrame fraDefaultAnalysisReports;
		public fecherFoundation.FCCheckBox chkSalesTaxS;
		public fecherFoundation.FCCheckBox chkSalesTaxW;
		public fecherFoundation.FCFrame fraReportSeq;
		public fecherFoundation.FCLabel lblBillingRepSeq;
		public fecherFoundation.FCCheckBox chkMeterReportW;
		public fecherFoundation.FCCheckBox chkBillCountW;
		public fecherFoundation.FCCheckBox chkConsumptionsW;
		public fecherFoundation.FCCheckBox chkDollarAmountsW;
		public fecherFoundation.FCCheckBox chkBillingSummary;
		public fecherFoundation.FCCheckBox chkDollarAmountsS;
		public fecherFoundation.FCCheckBox chkConsumptionsS;
		public fecherFoundation.FCCheckBox chkBillCountS;
		public fecherFoundation.FCCheckBox chkMeterReportS;
		public fecherFoundation.FCLabel lblWaterHeader;
		public fecherFoundation.FCLabel lblSewerHeader;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTCustomize));
            this.txtTaxRate = new fecherFoundation.FCTextBox();
            this.txtLDNSideAdj = new fecherFoundation.FCTextBox();
            this.txtLienSideAdj = new fecherFoundation.FCTextBox();
            this.txtLDNBottomAdj = new fecherFoundation.FCTextBox();
            this.txtLienBottomAdj = new fecherFoundation.FCTextBox();
            this.txtLienTopAdj = new fecherFoundation.FCTextBox();
            this.txtLDNTopAdj = new fecherFoundation.FCTextBox();
            this.txtPPayInterestRate = new fecherFoundation.FCTextBox();
            this.txtFlatInterestAmount = new fecherFoundation.FCTextBox();
            this.cmbSBillTo = new fecherFoundation.FCComboBox();
            this.cmbWBillTo = new fecherFoundation.FCComboBox();
            this.cmbPayFirst = new fecherFoundation.FCComboBox();
            this.cmbBasis = new fecherFoundation.FCComboBox();
            this.cmbInterestType = new fecherFoundation.FCComboBox();
            this.cmbReceiptSize = new fecherFoundation.FCComboBox();
            this.lblReceiptSize = new fecherFoundation.FCLabel();
            this.cmbExportFormat = new fecherFoundation.FCComboBox();
            this.lblExportFormat = new fecherFoundation.FCLabel();
            this.cmbSeq = new fecherFoundation.FCComboBox();
            this.cmbDEType = new fecherFoundation.FCComboBox();
            this.cmbOptions = new fecherFoundation.FCComboBox();
            this.lblOptions = new fecherFoundation.FCLabel();
            this.cmbBillingRepSeq = new fecherFoundation.FCComboBox();
            this.fraBillOptions = new fecherFoundation.FCFrame();
            this.chkSendCopies = new fecherFoundation.FCCheckBox();
            this.chkInclChangeOutCons = new fecherFoundation.FCCheckBox();
            this.chkShowLienAmtOnBill = new fecherFoundation.FCCheckBox();
            this.chkChpt660 = new fecherFoundation.FCCheckBox();
            this.cmbUnitsOnBill = new fecherFoundation.FCComboBox();
            this.fraTaxRate = new fecherFoundation.FCFrame();
            this.lblTaxRate_0 = new fecherFoundation.FCLabel();
            this.lblTaxRate_1 = new fecherFoundation.FCLabel();
            this.cmbDefaultBill = new fecherFoundation.FCComboBox();
            this.txtPrinAcctS = new fecherFoundation.FCGrid();
            this.txtTaxAcctS = new fecherFoundation.FCGrid();
            this.txtPrinAcctW = new fecherFoundation.FCGrid();
            this.txtTaxAcctW = new fecherFoundation.FCGrid();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.lblPrinAcctW = new fecherFoundation.FCLabel();
            this.lblTaxAcctW = new fecherFoundation.FCLabel();
            this.lblPrinAcctS = new fecherFoundation.FCLabel();
            this.lblTaxAcctS = new fecherFoundation.FCLabel();
            this.lblSBillTo = new fecherFoundation.FCLabel();
            this.lblWBillTo = new fecherFoundation.FCLabel();
            this.lblDefaultBill = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.fraAdjustments = new fecherFoundation.FCFrame();
            this.txtSigAdjust_Lien = new fecherFoundation.FCTextBox();
            this.txtMeterSlipsHAdj = new fecherFoundation.FCTextBox();
            this.txtMeterSlipsVAdj = new fecherFoundation.FCTextBox();
            this.txtDMVLabelAdjust = new fecherFoundation.FCTextBox();
            this.txtDMHLabelAdjust = new fecherFoundation.FCTextBox();
            this.txtCMFAdjust = new fecherFoundation.FCTextBox();
            this.txtLabelAdjustment = new fecherFoundation.FCTextBox();
            this.Label18 = new fecherFoundation.FCLabel();
            this.lblLienSideAdj = new fecherFoundation.FCLabel();
            this.Label17 = new fecherFoundation.FCLabel();
            this.Label16 = new fecherFoundation.FCLabel();
            this.Label15 = new fecherFoundation.FCLabel();
            this.Label14 = new fecherFoundation.FCLabel();
            this.Label13 = new fecherFoundation.FCLabel();
            this.lblSigAdjust_Lien = new fecherFoundation.FCLabel();
            this.lblMeterSlipsHAdj = new fecherFoundation.FCLabel();
            this.lblMeterSlipsVAdj = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.lblDMVLabelAdjust = new fecherFoundation.FCLabel();
            this.lblDMHLabelAdjust = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.lblCMFAdjust = new fecherFoundation.FCLabel();
            this.lblLabelAdjustment = new fecherFoundation.FCLabel();
            this.fraPaymentOptions = new fecherFoundation.FCFrame();
            this.chkSkipLDNPrompt = new fecherFoundation.FCCheckBox();
            this.chkDisableAutoPmtFill = new fecherFoundation.FCCheckBox();
            this.chkDefaultUTRTDDate = new fecherFoundation.FCCheckBox();
            this.cboAutopay = new fecherFoundation.FCComboBox();
            this.chkDefaultAccount = new fecherFoundation.FCCheckBox();
            this.txtDiscountPercent = new fecherFoundation.FCTextBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.lblPayFirst = new fecherFoundation.FCLabel();
            this.lblBasis = new fecherFoundation.FCLabel();
            this.lblDiscountPercent = new fecherFoundation.FCLabel();
            this.fraInterest = new fecherFoundation.FCFrame();
            this.chkTACheckRE = new fecherFoundation.FCCheckBox();
            this.chkTAUpdateRE = new fecherFoundation.FCCheckBox();
            this.chkShowTaxAcquired = new fecherFoundation.FCCheckBox();
            this.chkUseBookForLiens = new fecherFoundation.FCCheckBox();
            this.chkOnlyCheckOldestLien = new fecherFoundation.FCCheckBox();
            this.chkInterest_3 = new fecherFoundation.FCCheckBox();
            this.chkInterest_2 = new fecherFoundation.FCCheckBox();
            this.chkInterest_1 = new fecherFoundation.FCCheckBox();
            this.chkInterest_0 = new fecherFoundation.FCCheckBox();
            this.lblPPayInterestRate = new fecherFoundation.FCLabel();
            this.lblChargeInterestOn = new fecherFoundation.FCLabel();
            this.lblFlatInterestRate = new fecherFoundation.FCLabel();
            this.lblInterestType = new fecherFoundation.FCLabel();
            this.fraOther = new fecherFoundation.FCFrame();
            this.fraReturnAddress = new fecherFoundation.FCFrame();
            this.vsReturnAddress = new fecherFoundation.FCGrid();
            this.fraMultiplier = new fecherFoundation.FCFrame();
            this.cmbMultiplier = new fecherFoundation.FCComboBox();
            this.lblMultiplier = new fecherFoundation.FCLabel();
            this.lblMultiplier2 = new fecherFoundation.FCLabel();
            this.fraTownService = new fecherFoundation.FCFrame();
            this.cmbService = new fecherFoundation.FCComboBox();
            this.lblService = new fecherFoundation.FCLabel();
            this.fraRemoteReader = new fecherFoundation.FCFrame();
            this.txtVFlexSiteCode = new fecherFoundation.FCTextBox();
            this.chkForceReadDate = new fecherFoundation.FCCheckBox();
            this.chkShowLocation = new fecherFoundation.FCCheckBox();
            this.cmbExtractFileType = new fecherFoundation.FCComboBox();
            this.lblExtractFileType = new fecherFoundation.FCLabel();
            this.fraReportSequence = new fecherFoundation.FCFrame();
            this.chkMapLot = new fecherFoundation.FCCheckBox();
            this.LblSeq = new fecherFoundation.FCLabel();
            this.fraIConnect = new fecherFoundation.FCFrame();
            this.txtInvStartDate = new fecherFoundation.FCTextBox();
            this.cmdSetupICAccounts = new fecherFoundation.FCButton();
            this.chkUseCRAccounts = new fecherFoundation.FCCheckBox();
            this.txtMFeeAccount = new fecherFoundation.FCGrid();
            this.lblInvStartDate = new fecherFoundation.FCLabel();
            this.Label11 = new fecherFoundation.FCLabel();
            this.fraDefaultOptions = new fecherFoundation.FCFrame();
            this.chkDefaultMHInfo = new fecherFoundation.FCCheckBox();
            this.chkDefaultRE = new fecherFoundation.FCCheckBox();
            this.cmbState = new fecherFoundation.FCComboBox();
            this.txtDefaultZip = new fecherFoundation.FCTextBox();
            this.txtDefaultCity = new fecherFoundation.FCTextBox();
            this.cmbDefaultCategoryS = new fecherFoundation.FCComboBox();
            this.cmbDefaultFrequency = new fecherFoundation.FCComboBox();
            this.cmbDefaultDigits = new fecherFoundation.FCComboBox();
            this.cmbDefaultSize = new fecherFoundation.FCComboBox();
            this.cmbDefaultCategoryW = new fecherFoundation.FCComboBox();
            this.txtDefaultAccountW = new fecherFoundation.FCGrid();
            this.txtDefaultAccountS = new fecherFoundation.FCGrid();
            this.lblDefaultCity = new fecherFoundation.FCLabel();
            this.lblDefaultState = new fecherFoundation.FCLabel();
            this.lblDefaultZip = new fecherFoundation.FCLabel();
            this.lblDefaultAccountW = new fecherFoundation.FCLabel();
            this.lblDefaultAccountS = new fecherFoundation.FCLabel();
            this.lblDefaultCategoryS = new fecherFoundation.FCLabel();
            this.lblDefaultMeter = new fecherFoundation.FCLabel();
            this.lblDDefaultFrequency = new fecherFoundation.FCLabel();
            this.lblDefaultDigits = new fecherFoundation.FCLabel();
            this.lblDefaultSize = new fecherFoundation.FCLabel();
            this.lblDefaultAccount = new fecherFoundation.FCLabel();
            this.lblDefaultCategoryW = new fecherFoundation.FCLabel();
            this.fraBilling = new fecherFoundation.FCFrame();
            this.fraConsIncrease = new fecherFoundation.FCFrame();
            this.chkMinVarWarning = new fecherFoundation.FCCheckBox();
            this.txtConsIncrease = new fecherFoundation.FCTextBox();
            this.lblConsIncrease_1 = new fecherFoundation.FCLabel();
            this.lblConsIncrease_0 = new fecherFoundation.FCLabel();
            this.chkDefDisplayHistory = new fecherFoundation.FCCheckBox();
            this.chkBillStormwaterFee = new fecherFoundation.FCCheckBox();
            this.chkLimitConsBill = new fecherFoundation.FCCheckBox();
            this.cboApplyTo = new fecherFoundation.FCComboBox();
            this.chkAutoPrepayments = new fecherFoundation.FCCheckBox();
            this.chkExportNoBillRecords = new fecherFoundation.FCCheckBox();
            this.chkStopOnConsumption = new fecherFoundation.FCCheckBox();
            this.cmbDEOrder = new fecherFoundation.FCComboBox();
            this.lblApplyTo = new fecherFoundation.FCLabel();
            this.lblDEType = new fecherFoundation.FCLabel();
            this.lblDEOrder = new fecherFoundation.FCLabel();
            this.fraBillDetDesc = new fecherFoundation.FCFrame();
            this.txtAdj = new fecherFoundation.FCTextBox();
            this.txtMisc = new fecherFoundation.FCTextBox();
            this.txtTax = new fecherFoundation.FCTextBox();
            this.txtLiens = new fecherFoundation.FCTextBox();
            this.txtInterest = new fecherFoundation.FCTextBox();
            this.txtOverride = new fecherFoundation.FCTextBox();
            this.txtRegular = new fecherFoundation.FCTextBox();
            this.txtCredit = new fecherFoundation.FCTextBox();
            this.txtPastDue = new fecherFoundation.FCTextBox();
            this.lblAdj = new fecherFoundation.FCLabel();
            this.lblMisc = new fecherFoundation.FCLabel();
            this.lblTax = new fecherFoundation.FCLabel();
            this.lblLiens = new fecherFoundation.FCLabel();
            this.lblInterest = new fecherFoundation.FCLabel();
            this.lblOverride = new fecherFoundation.FCLabel();
            this.lblRegular = new fecherFoundation.FCLabel();
            this.lblCredit = new fecherFoundation.FCLabel();
            this.lblPastDue = new fecherFoundation.FCLabel();
            this.Label12 = new fecherFoundation.FCLabel();
            this.fraDefaultAnalysisReports = new fecherFoundation.FCFrame();
            this.chkSalesTaxS = new fecherFoundation.FCCheckBox();
            this.chkSalesTaxW = new fecherFoundation.FCCheckBox();
            this.fraReportSeq = new fecherFoundation.FCFrame();
            this.lblBillingRepSeq = new fecherFoundation.FCLabel();
            this.chkMeterReportW = new fecherFoundation.FCCheckBox();
            this.chkBillCountW = new fecherFoundation.FCCheckBox();
            this.chkConsumptionsW = new fecherFoundation.FCCheckBox();
            this.chkDollarAmountsW = new fecherFoundation.FCCheckBox();
            this.chkBillingSummary = new fecherFoundation.FCCheckBox();
            this.chkDollarAmountsS = new fecherFoundation.FCCheckBox();
            this.chkConsumptionsS = new fecherFoundation.FCCheckBox();
            this.chkBillCountS = new fecherFoundation.FCCheckBox();
            this.chkMeterReportS = new fecherFoundation.FCCheckBox();
            this.lblWaterHeader = new fecherFoundation.FCLabel();
            this.lblSewerHeader = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBillOptions)).BeginInit();
            this.fraBillOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSendCopies)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclChangeOutCons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowLienAmtOnBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChpt660)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTaxRate)).BeginInit();
            this.fraTaxRate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinAcctS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxAcctS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinAcctW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxAcctW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments)).BeginInit();
            this.fraAdjustments.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraPaymentOptions)).BeginInit();
            this.fraPaymentOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSkipLDNPrompt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisableAutoPmtFill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultUTRTDDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraInterest)).BeginInit();
            this.fraInterest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTACheckRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTAUpdateRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowTaxAcquired)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseBookForLiens)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlyCheckOldestLien)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOther)).BeginInit();
            this.fraOther.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraReturnAddress)).BeginInit();
            this.fraReturnAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsReturnAddress)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMultiplier)).BeginInit();
            this.fraMultiplier.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraTownService)).BeginInit();
            this.fraTownService.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRemoteReader)).BeginInit();
            this.fraRemoteReader.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkForceReadDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReportSequence)).BeginInit();
            this.fraReportSequence.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraIConnect)).BeginInit();
            this.fraIConnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSetupICAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseCRAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMFeeAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultOptions)).BeginInit();
            this.fraDefaultOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultMHInfo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccountW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccountS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBilling)).BeginInit();
            this.fraBilling.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraConsIncrease)).BeginInit();
            this.fraConsIncrease.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMinVarWarning)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefDisplayHistory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillStormwaterFee)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLimitConsBill)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoPrepayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportNoBillRecords)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStopOnConsumption)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBillDetDesc)).BeginInit();
            this.fraBillDetDesc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultAnalysisReports)).BeginInit();
            this.fraDefaultAnalysisReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReportSeq)).BeginInit();
            this.fraReportSeq.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMeterReportW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillCountW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsumptionsW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDollarAmountsW)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillingSummary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDollarAmountsS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsumptionsS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillCountS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMeterReportS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 580);
            this.BottomPanel.Size = new System.Drawing.Size(898, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraIConnect);
            this.ClientArea.Controls.Add(this.fraAdjustments);
            this.ClientArea.Controls.Add(this.fraDefaultAnalysisReports);
            this.ClientArea.Controls.Add(this.fraBillDetDesc);
            this.ClientArea.Controls.Add(this.fraInterest);
            this.ClientArea.Controls.Add(this.fraPaymentOptions);
            this.ClientArea.Controls.Add(this.cmbOptions);
            this.ClientArea.Controls.Add(this.lblOptions);
            this.ClientArea.Controls.Add(this.fraBilling);
            this.ClientArea.Controls.Add(this.fraOther);
            this.ClientArea.Controls.Add(this.fraBillOptions);
            this.ClientArea.Controls.Add(this.fraDefaultOptions);
            this.ClientArea.Size = new System.Drawing.Size(898, 520);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(898, 60);
            this.TopPanel.TabIndex = 0;
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Customize";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // txtTaxRate
            // 
            this.txtTaxRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtTaxRate.Location = new System.Drawing.Point(133, 30);
            this.txtTaxRate.Name = "txtTaxRate";
            this.txtTaxRate.Size = new System.Drawing.Size(61, 40);
            this.txtTaxRate.TabIndex = 1;
            this.txtTaxRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtTaxRate, null);
            this.txtTaxRate.Enter += new System.EventHandler(this.txtTaxRate_Enter);
            this.txtTaxRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtTaxRate_KeyPress);
            // 
            // txtLDNSideAdj
            // 
            this.txtLDNSideAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNSideAdj.Location = new System.Drawing.Point(610, 305);
            this.txtLDNSideAdj.MaxLength = 5;
            this.txtLDNSideAdj.Name = "txtLDNSideAdj";
            this.txtLDNSideAdj.Size = new System.Drawing.Size(73, 40);
            this.txtLDNSideAdj.TabIndex = 28;
            this.txtLDNSideAdj.Text = "0";
            this.txtLDNSideAdj.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLDNSideAdj, null);
            this.txtLDNSideAdj.Enter += new System.EventHandler(this.txtLDNSideAdj_Enter);
            this.txtLDNSideAdj.TextChanged += new System.EventHandler(this.txtLDNSideAdj_TextChanged);
            this.txtLDNSideAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLDNSideAdj_KeyPress);
            // 
            // txtLienSideAdj
            // 
            this.txtLienSideAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienSideAdj.Location = new System.Drawing.Point(610, 155);
            this.txtLienSideAdj.MaxLength = 5;
            this.txtLienSideAdj.Name = "txtLienSideAdj";
            this.txtLienSideAdj.Size = new System.Drawing.Size(73, 40);
            this.txtLienSideAdj.TabIndex = 22;
            this.txtLienSideAdj.Text = "0";
            this.txtLienSideAdj.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLienSideAdj, null);
            this.txtLienSideAdj.Enter += new System.EventHandler(this.txtLienSideAdj_Enter);
            this.txtLienSideAdj.TextChanged += new System.EventHandler(this.txtLienSideAdj_TextChanged);
            this.txtLienSideAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLienSideAdj_KeyPress);
            // 
            // txtLDNBottomAdj
            // 
            this.txtLDNBottomAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNBottomAdj.Location = new System.Drawing.Point(610, 255);
            this.txtLDNBottomAdj.MaxLength = 5;
            this.txtLDNBottomAdj.Name = "txtLDNBottomAdj";
            this.txtLDNBottomAdj.Size = new System.Drawing.Size(73, 40);
            this.txtLDNBottomAdj.TabIndex = 26;
            this.txtLDNBottomAdj.Text = "0";
            this.txtLDNBottomAdj.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLDNBottomAdj, null);
            this.txtLDNBottomAdj.Enter += new System.EventHandler(this.txtLDNBottomAdj_Enter);
            this.txtLDNBottomAdj.TextChanged += new System.EventHandler(this.txtLDNBottomAdj_TextChanged);
            this.txtLDNBottomAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLDNBottomAdj_KeyPress);
            // 
            // txtLienBottomAdj
            // 
            this.txtLienBottomAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienBottomAdj.Location = new System.Drawing.Point(610, 105);
            this.txtLienBottomAdj.MaxLength = 5;
            this.txtLienBottomAdj.Name = "txtLienBottomAdj";
            this.txtLienBottomAdj.Size = new System.Drawing.Size(73, 40);
            this.txtLienBottomAdj.TabIndex = 20;
            this.txtLienBottomAdj.Text = "0";
            this.txtLienBottomAdj.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLienBottomAdj, null);
            this.txtLienBottomAdj.Enter += new System.EventHandler(this.txtLienBottomAdj_Enter);
            this.txtLienBottomAdj.TextChanged += new System.EventHandler(this.txtLienBottomAdj_TextChanged);
            this.txtLienBottomAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLienBottomAdj_KeyPress);
            // 
            // txtLienTopAdj
            // 
            this.txtLienTopAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtLienTopAdj.Location = new System.Drawing.Point(610, 55);
            this.txtLienTopAdj.MaxLength = 5;
            this.txtLienTopAdj.Name = "txtLienTopAdj";
            this.txtLienTopAdj.Size = new System.Drawing.Size(73, 40);
            this.txtLienTopAdj.TabIndex = 18;
            this.txtLienTopAdj.Text = "0";
            this.txtLienTopAdj.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLienTopAdj, null);
            this.txtLienTopAdj.Enter += new System.EventHandler(this.txtLienTopAdj_Enter);
            this.txtLienTopAdj.TextChanged += new System.EventHandler(this.txtLienTopAdj_TextChanged);
            this.txtLienTopAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLienTopAdj_KeyPress);
            // 
            // txtLDNTopAdj
            // 
            this.txtLDNTopAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtLDNTopAdj.Location = new System.Drawing.Point(610, 205);
            this.txtLDNTopAdj.MaxLength = 5;
            this.txtLDNTopAdj.Name = "txtLDNTopAdj";
            this.txtLDNTopAdj.Size = new System.Drawing.Size(73, 40);
            this.txtLDNTopAdj.TabIndex = 24;
            this.txtLDNTopAdj.Text = "0";
            this.txtLDNTopAdj.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLDNTopAdj, null);
            this.txtLDNTopAdj.Enter += new System.EventHandler(this.txtLDNTopAdj_Enter);
            this.txtLDNTopAdj.TextChanged += new System.EventHandler(this.txtLDNTopAdj_TextChanged);
            this.txtLDNTopAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLDNTopAdj_KeyPress);
            // 
            // txtPPayInterestRate
            // 
            this.txtPPayInterestRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtPPayInterestRate.Location = new System.Drawing.Point(231, 30);
            this.txtPPayInterestRate.Name = "txtPPayInterestRate";
            this.txtPPayInterestRate.Size = new System.Drawing.Size(135, 40);
            this.txtPPayInterestRate.TabIndex = 21;
            this.txtPPayInterestRate.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtPPayInterestRate, null);
            this.txtPPayInterestRate.Enter += new System.EventHandler(this.txtPPayInterestRate_Enter);
            this.txtPPayInterestRate.TextChanged += new System.EventHandler(this.txtPPayInterestRate_TextChanged);
            this.txtPPayInterestRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPPayInterestRate_KeyPress);
            // 
            // txtFlatInterestAmount
            // 
            this.txtFlatInterestAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtFlatInterestAmount.Enabled = false;
            this.txtFlatInterestAmount.Location = new System.Drawing.Point(231, 130);
            this.txtFlatInterestAmount.Name = "txtFlatInterestAmount";
            this.txtFlatInterestAmount.Size = new System.Drawing.Size(135, 40);
            this.txtFlatInterestAmount.TabIndex = 26;
            this.txtFlatInterestAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtFlatInterestAmount, null);
            this.txtFlatInterestAmount.Enter += new System.EventHandler(this.txtFlatInterestAmount_Enter);
            this.txtFlatInterestAmount.TextChanged += new System.EventHandler(this.txtFlatInterestAmount_TextChanged);
            this.txtFlatInterestAmount.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtFlatInterestAmount_KeyPress);
            // 
            // cmbSBillTo
            // 
            this.cmbSBillTo.Items.AddRange(new object[] {
            "Owner",
            "Tenant"});
            this.cmbSBillTo.Location = new System.Drawing.Point(177, 190);
            this.cmbSBillTo.Name = "cmbSBillTo";
            this.cmbSBillTo.Size = new System.Drawing.Size(185, 40);
            this.cmbSBillTo.TabIndex = 6;
            this.cmbSBillTo.Text = "Owner";
            this.ToolTip1.SetToolTip(this.cmbSBillTo, null);
            this.cmbSBillTo.SelectedIndexChanged += new System.EventHandler(this.optSBillTo_CheckedChanged);
            // 
            // cmbWBillTo
            // 
            this.cmbWBillTo.Items.AddRange(new object[] {
            "Owner",
            "Tenant"});
            this.cmbWBillTo.Location = new System.Drawing.Point(549, 190);
            this.cmbWBillTo.Name = "cmbWBillTo";
            this.cmbWBillTo.Size = new System.Drawing.Size(175, 40);
            this.cmbWBillTo.TabIndex = 13;
            this.cmbWBillTo.Text = "Owner";
            this.ToolTip1.SetToolTip(this.cmbWBillTo, null);
            this.cmbWBillTo.SelectedIndexChanged += new System.EventHandler(this.optWBillTo_CheckedChanged);
            // 
            // cmbPayFirst
            // 
            this.cmbPayFirst.Items.AddRange(new object[] {
            "Water",
            "Sewer"});
            this.cmbPayFirst.Location = new System.Drawing.Point(205, 80);
            this.cmbPayFirst.Name = "cmbPayFirst";
            this.cmbPayFirst.Size = new System.Drawing.Size(215, 40);
            this.cmbPayFirst.TabIndex = 3;
            this.cmbPayFirst.Text = "Water";
            this.ToolTip1.SetToolTip(this.cmbPayFirst, null);
            this.cmbPayFirst.SelectedIndexChanged += new System.EventHandler(this.optPayFirst_CheckedChanged);
            // 
            // cmbBasis
            // 
            this.cmbBasis.Items.AddRange(new object[] {
            "365 Days",
            "360 Days"});
            this.cmbBasis.Location = new System.Drawing.Point(205, 130);
            this.cmbBasis.Name = "cmbBasis";
            this.cmbBasis.Size = new System.Drawing.Size(215, 40);
            this.cmbBasis.TabIndex = 5;
            this.cmbBasis.Text = "360 Days";
            this.ToolTip1.SetToolTip(this.cmbBasis, null);
            this.cmbBasis.SelectedIndexChanged += new System.EventHandler(this.optBasis_CheckedChanged);
            // 
            // cmbInterestType
            // 
            this.cmbInterestType.Items.AddRange(new object[] {
            "Per Diem",
            "Per Billing",
            "Flat Rate",
            "On Demand"});
            this.cmbInterestType.Location = new System.Drawing.Point(231, 80);
            this.cmbInterestType.Name = "cmbInterestType";
            this.cmbInterestType.Size = new System.Drawing.Size(135, 40);
            this.cmbInterestType.TabIndex = 179;
            this.ToolTip1.SetToolTip(this.cmbInterestType, null);
            this.cmbInterestType.SelectedIndexChanged += new System.EventHandler(this.optInterestType_CheckedChanged);
            // 
            // cmbReceiptSize
            // 
            this.cmbReceiptSize.Items.AddRange(new object[] {
            "Wide",
            "Narrow"});
            this.cmbReceiptSize.Location = new System.Drawing.Point(195, 521);
            this.cmbReceiptSize.Name = "cmbReceiptSize";
            this.cmbReceiptSize.Size = new System.Drawing.Size(252, 40);
            this.cmbReceiptSize.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cmbReceiptSize, null);
            this.cmbReceiptSize.SelectedIndexChanged += new System.EventHandler(this.optReceiptSize_CheckedChanged);
            // 
            // lblReceiptSize
            // 
            this.lblReceiptSize.Location = new System.Drawing.Point(20, 535);
            this.lblReceiptSize.Name = "lblReceiptSize";
            this.lblReceiptSize.Size = new System.Drawing.Size(100, 18);
            this.lblReceiptSize.TabIndex = 3;
            this.lblReceiptSize.Text = "CL RECEIPT SIZE";
            this.ToolTip1.SetToolTip(this.lblReceiptSize, null);
            // 
            // cmbExportFormat
            // 
            this.cmbExportFormat.Items.AddRange(new object[] {
            "Original",
            "Extended"});
            this.cmbExportFormat.Location = new System.Drawing.Point(175, 80);
            this.cmbExportFormat.Name = "cmbExportFormat";
            this.cmbExportFormat.Size = new System.Drawing.Size(232, 40);
            this.cmbExportFormat.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbExportFormat, null);
            this.cmbExportFormat.SelectedIndexChanged += new System.EventHandler(this.optExportFormat_CheckedChanged);
            // 
            // lblExportFormat
            // 
            this.lblExportFormat.Location = new System.Drawing.Point(20, 94);
            this.lblExportFormat.Name = "lblExportFormat";
            this.lblExportFormat.Size = new System.Drawing.Size(138, 18);
            this.lblExportFormat.TabIndex = 2;
            this.lblExportFormat.Text = "EXPORT FORMAT";
            this.ToolTip1.SetToolTip(this.lblExportFormat, null);
            // 
            // cmbSeq
            // 
            this.cmbSeq.Items.AddRange(new object[] {
            "Receipt Number",
            "Account Number"});
            this.cmbSeq.Location = new System.Drawing.Point(175, 30);
            this.cmbSeq.Name = "cmbSeq";
            this.cmbSeq.Size = new System.Drawing.Size(232, 40);
            this.cmbSeq.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbSeq, null);
            this.cmbSeq.SelectedIndexChanged += new System.EventHandler(this.optSeq_CheckedChanged);
            // 
            // cmbDEType
            // 
            this.cmbDEType.Items.AddRange(new object[] {
            "Reading",
            "Consumption"});
            this.cmbDEType.Location = new System.Drawing.Point(213, 154);
            this.cmbDEType.Name = "cmbDEType";
            this.cmbDEType.Size = new System.Drawing.Size(171, 40);
            this.cmbDEType.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.cmbDEType, null);
            this.cmbDEType.SelectedIndexChanged += new System.EventHandler(this.optDEType_CheckedChanged);
            // 
            // cmbOptions
            // 
            this.cmbOptions.Items.AddRange(new object[] {
            "Adjustments",
            "Billing Options",
            "Payment Options",
            "Interest / Lien Options",
            "Other Options",
            "Default Reports",
            "Bill Options",
            "Default Account Options",
            "IConnect Options",
            "Bill Detail Descriptions"});
            this.cmbOptions.Location = new System.Drawing.Point(128, 30);
            this.cmbOptions.Name = "cmbOptions";
            this.cmbOptions.Size = new System.Drawing.Size(220, 40);
            this.cmbOptions.TabIndex = 1;
            this.cmbOptions.Text = "Adjustments";
            this.ToolTip1.SetToolTip(this.cmbOptions, null);
            this.cmbOptions.SelectedIndexChanged += new System.EventHandler(this.optOptions_CheckedChanged);
            // 
            // lblOptions
            // 
            this.lblOptions.AutoSize = true;
            this.lblOptions.Location = new System.Drawing.Point(30, 44);
            this.lblOptions.Name = "lblOptions";
            this.lblOptions.Size = new System.Drawing.Size(63, 15);
            this.lblOptions.TabIndex = 12;
            this.lblOptions.Text = "OPTIONS";
            this.ToolTip1.SetToolTip(this.lblOptions, null);
            // 
            // cmbBillingRepSeq
            // 
            this.cmbBillingRepSeq.Items.AddRange(new object[] {
            "Book and Sequence",
            "Account Number",
            "Name",
            "Location",
            "Map Lot"});
            this.cmbBillingRepSeq.Location = new System.Drawing.Point(145, 30);
            this.cmbBillingRepSeq.Name = "cmbBillingRepSeq";
            this.cmbBillingRepSeq.Size = new System.Drawing.Size(190, 40);
            this.cmbBillingRepSeq.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbBillingRepSeq, null);
            // 
            // fraBillOptions
            // 
            this.fraBillOptions.AppearanceKey = "groupBoxLeftBorder";
            this.fraBillOptions.Controls.Add(this.chkSendCopies);
            this.fraBillOptions.Controls.Add(this.chkInclChangeOutCons);
            this.fraBillOptions.Controls.Add(this.cmbSBillTo);
            this.fraBillOptions.Controls.Add(this.cmbWBillTo);
            this.fraBillOptions.Controls.Add(this.chkShowLienAmtOnBill);
            this.fraBillOptions.Controls.Add(this.chkChpt660);
            this.fraBillOptions.Controls.Add(this.cmbUnitsOnBill);
            this.fraBillOptions.Controls.Add(this.fraTaxRate);
            this.fraBillOptions.Controls.Add(this.cmbDefaultBill);
            this.fraBillOptions.Controls.Add(this.txtPrinAcctS);
            this.fraBillOptions.Controls.Add(this.txtTaxAcctS);
            this.fraBillOptions.Controls.Add(this.txtPrinAcctW);
            this.fraBillOptions.Controls.Add(this.txtTaxAcctW);
            this.fraBillOptions.Controls.Add(this.Label8);
            this.fraBillOptions.Controls.Add(this.Label6);
            this.fraBillOptions.Controls.Add(this.lblPrinAcctW);
            this.fraBillOptions.Controls.Add(this.lblTaxAcctW);
            this.fraBillOptions.Controls.Add(this.lblPrinAcctS);
            this.fraBillOptions.Controls.Add(this.lblTaxAcctS);
            this.fraBillOptions.Controls.Add(this.lblSBillTo);
            this.fraBillOptions.Controls.Add(this.lblWBillTo);
            this.fraBillOptions.Controls.Add(this.lblDefaultBill);
            this.fraBillOptions.Controls.Add(this.Label4);
            this.fraBillOptions.Controls.Add(this.Label7);
            this.fraBillOptions.Location = new System.Drawing.Point(30, 122);
            this.fraBillOptions.Name = "fraBillOptions";
            this.fraBillOptions.Size = new System.Drawing.Size(755, 469);
            this.fraBillOptions.TabIndex = 2;
            this.fraBillOptions.Text = "Bill Options";
            this.ToolTip1.SetToolTip(this.fraBillOptions, null);
            this.fraBillOptions.Visible = false;
            // 
            // chkSendCopies
            // 
            this.chkSendCopies.Location = new System.Drawing.Point(392, 104);
            this.chkSendCopies.Name = "chkSendCopies";
            this.chkSendCopies.Size = new System.Drawing.Size(282, 26);
            this.chkSendCopies.TabIndex = 23;
            this.chkSendCopies.Text = "Send copy if tenant and owner are different";
            this.ToolTip1.SetToolTip(this.chkSendCopies, null);
            // 
            // chkInclChangeOutCons
            // 
            this.chkInclChangeOutCons.Location = new System.Drawing.Point(392, 68);
            this.chkInclChangeOutCons.Name = "chkInclChangeOutCons";
            this.chkInclChangeOutCons.Size = new System.Drawing.Size(265, 26);
            this.chkInclChangeOutCons.TabIndex = 4;
            this.chkInclChangeOutCons.Text = "Include Meter Change Out Consumption";
            this.ToolTip1.SetToolTip(this.chkInclChangeOutCons, null);
            this.chkInclChangeOutCons.CheckedChanged += new System.EventHandler(this.chkInclChangeOutCons_CheckedChanged);
            // 
            // chkShowLienAmtOnBill
            // 
            this.chkShowLienAmtOnBill.Location = new System.Drawing.Point(392, 368);
            this.chkShowLienAmtOnBill.Name = "chkShowLienAmtOnBill";
            this.chkShowLienAmtOnBill.Size = new System.Drawing.Size(188, 26);
            this.chkShowLienAmtOnBill.TabIndex = 18;
            this.chkShowLienAmtOnBill.Text = "Show Lien Amounts On Bill";
            this.ToolTip1.SetToolTip(this.chkShowLienAmtOnBill, null);
            this.chkShowLienAmtOnBill.CheckedChanged += new System.EventHandler(this.chkShowLienAmtOnBill_CheckedChanged);
            // 
            // chkChpt660
            // 
            this.chkChpt660.Location = new System.Drawing.Point(392, 31);
            this.chkChpt660.Name = "chkChpt660";
            this.chkChpt660.Size = new System.Drawing.Size(206, 26);
            this.chkChpt660.TabIndex = 3;
            this.chkChpt660.Text = "Extended Chapter 660 Format";
            this.ToolTip1.SetToolTip(this.chkChpt660, null);
            this.chkChpt660.CheckedChanged += new System.EventHandler(this.chkChpt660_CheckedChanged);
            // 
            // cmbUnitsOnBill
            // 
            this.cmbUnitsOnBill.BackColor = System.Drawing.SystemColors.Window;
            this.cmbUnitsOnBill.Location = new System.Drawing.Point(549, 411);
            this.cmbUnitsOnBill.Name = "cmbUnitsOnBill";
            this.cmbUnitsOnBill.Size = new System.Drawing.Size(83, 40);
            this.cmbUnitsOnBill.TabIndex = 20;
            this.ToolTip1.SetToolTip(this.cmbUnitsOnBill, null);
            // 
            // fraTaxRate
            // 
            this.fraTaxRate.Controls.Add(this.txtTaxRate);
            this.fraTaxRate.Controls.Add(this.lblTaxRate_0);
            this.fraTaxRate.Controls.Add(this.lblTaxRate_1);
            this.fraTaxRate.Location = new System.Drawing.Point(20, 372);
            this.fraTaxRate.Name = "fraTaxRate";
            this.fraTaxRate.Size = new System.Drawing.Size(262, 90);
            this.fraTaxRate.TabIndex = 22;
            this.fraTaxRate.Text = "Tax Rate";
            this.ToolTip1.SetToolTip(this.fraTaxRate, null);
            // 
            // lblTaxRate_0
            // 
            this.lblTaxRate_0.Location = new System.Drawing.Point(20, 44);
            this.lblTaxRate_0.Name = "lblTaxRate_0";
            this.lblTaxRate_0.Size = new System.Drawing.Size(60, 18);
            this.lblTaxRate_0.TabIndex = 2;
            this.lblTaxRate_0.Text = "TAX RATE";
            this.ToolTip1.SetToolTip(this.lblTaxRate_0, null);
            // 
            // lblTaxRate_1
            // 
            this.lblTaxRate_1.Location = new System.Drawing.Point(229, 44);
            this.lblTaxRate_1.Name = "lblTaxRate_1";
            this.lblTaxRate_1.Size = new System.Drawing.Size(13, 18);
            this.lblTaxRate_1.TabIndex = 2;
            this.lblTaxRate_1.Text = "%";
            this.ToolTip1.SetToolTip(this.lblTaxRate_1, null);
            // 
            // cmbDefaultBill
            // 
            this.cmbDefaultBill.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDefaultBill.Location = new System.Drawing.Point(177, 30);
            this.cmbDefaultBill.Name = "cmbDefaultBill";
            this.cmbDefaultBill.Size = new System.Drawing.Size(185, 40);
            this.cmbDefaultBill.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbDefaultBill, null);
            this.cmbDefaultBill.SelectedIndexChanged += new System.EventHandler(this.cmbDefaultBill_SelectedIndexChanged);
            this.cmbDefaultBill.DropDown += new System.EventHandler(this.cmbDefaultBill_DropDown);
            this.cmbDefaultBill.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbDefaultBill_KeyDown);
            // 
            // txtPrinAcctS
            // 
            this.txtPrinAcctS.Cols = 1;
            this.txtPrinAcctS.ColumnHeadersVisible = false;
            this.txtPrinAcctS.ExtendLastCol = true;
            this.txtPrinAcctS.FixedCols = 0;
            this.txtPrinAcctS.FixedRows = 0;
            this.txtPrinAcctS.Location = new System.Drawing.Point(177, 249);
            this.txtPrinAcctS.Name = "txtPrinAcctS";
            this.txtPrinAcctS.RowHeadersVisible = false;
            this.txtPrinAcctS.Rows = 1;
            this.txtPrinAcctS.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtPrinAcctS.ShowFocusCell = false;
            this.txtPrinAcctS.Size = new System.Drawing.Size(185, 41);
            this.txtPrinAcctS.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtPrinAcctS, null);
            this.txtPrinAcctS.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPrinAcctS_KeyDownEvent);
            // 
            // txtTaxAcctS
            // 
            this.txtTaxAcctS.Cols = 1;
            this.txtTaxAcctS.ColumnHeadersVisible = false;
            this.txtTaxAcctS.ExtendLastCol = true;
            this.txtTaxAcctS.FixedCols = 0;
            this.txtTaxAcctS.FixedRows = 0;
            this.txtTaxAcctS.Location = new System.Drawing.Point(177, 310);
            this.txtTaxAcctS.Name = "txtTaxAcctS";
            this.txtTaxAcctS.RowHeadersVisible = false;
            this.txtTaxAcctS.Rows = 1;
            this.txtTaxAcctS.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtTaxAcctS.ShowFocusCell = false;
            this.txtTaxAcctS.Size = new System.Drawing.Size(185, 41);
            this.txtTaxAcctS.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtTaxAcctS, null);
            this.txtTaxAcctS.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTaxAcctS_KeyDownEvent);
            // 
            // txtPrinAcctW
            // 
            this.txtPrinAcctW.Cols = 1;
            this.txtPrinAcctW.ColumnHeadersVisible = false;
            this.txtPrinAcctW.ExtendLastCol = true;
            this.txtPrinAcctW.FixedCols = 0;
            this.txtPrinAcctW.FixedRows = 0;
            this.txtPrinAcctW.Location = new System.Drawing.Point(549, 249);
            this.txtPrinAcctW.Name = "txtPrinAcctW";
            this.txtPrinAcctW.RowHeadersVisible = false;
            this.txtPrinAcctW.Rows = 1;
            this.txtPrinAcctW.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtPrinAcctW.ShowFocusCell = false;
            this.txtPrinAcctW.Size = new System.Drawing.Size(175, 41);
            this.txtPrinAcctW.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.txtPrinAcctW, null);
            this.txtPrinAcctW.KeyDown += new Wisej.Web.KeyEventHandler(this.txtPrinAcctW_KeyDownEvent);
            // 
            // txtTaxAcctW
            // 
            this.txtTaxAcctW.Cols = 1;
            this.txtTaxAcctW.ColumnHeadersVisible = false;
            this.txtTaxAcctW.ExtendLastCol = true;
            this.txtTaxAcctW.FixedCols = 0;
            this.txtTaxAcctW.FixedRows = 0;
            this.txtTaxAcctW.Location = new System.Drawing.Point(549, 310);
            this.txtTaxAcctW.Name = "txtTaxAcctW";
            this.txtTaxAcctW.RowHeadersVisible = false;
            this.txtTaxAcctW.Rows = 1;
            this.txtTaxAcctW.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtTaxAcctW.ShowFocusCell = false;
            this.txtTaxAcctW.Size = new System.Drawing.Size(175, 41);
            this.txtTaxAcctW.TabIndex = 17;
            this.ToolTip1.SetToolTip(this.txtTaxAcctW, null);
            this.txtTaxAcctW.KeyDown += new Wisej.Web.KeyEventHandler(this.txtTaxAcctW_KeyDownEvent);
            // 
            // Label8
            // 
            this.Label8.Location = new System.Drawing.Point(652, 425);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(116, 18);
            this.Label8.TabIndex = 21;
            this.Label8.Text = "CUBIC FT. ON BILLS";
            this.ToolTip1.SetToolTip(this.Label8, null);
            // 
            // Label6
            // 
            this.Label6.Location = new System.Drawing.Point(392, 425);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(150, 18);
            this.Label6.TabIndex = 19;
            this.Label6.Text = "SHOW CONSUMPTION IN";
            this.ToolTip1.SetToolTip(this.Label6, null);
            // 
            // lblPrinAcctW
            // 
            this.lblPrinAcctW.Location = new System.Drawing.Point(392, 263);
            this.lblPrinAcctW.Name = "lblPrinAcctW";
            this.lblPrinAcctW.Size = new System.Drawing.Size(130, 18);
            this.lblPrinAcctW.TabIndex = 14;
            this.lblPrinAcctW.Text = "PRINCIPAL ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblPrinAcctW, null);
            // 
            // lblTaxAcctW
            // 
            this.lblTaxAcctW.Location = new System.Drawing.Point(392, 324);
            this.lblTaxAcctW.Name = "lblTaxAcctW";
            this.lblTaxAcctW.Size = new System.Drawing.Size(100, 18);
            this.lblTaxAcctW.TabIndex = 16;
            this.lblTaxAcctW.Text = "TAX ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblTaxAcctW, null);
            // 
            // lblPrinAcctS
            // 
            this.lblPrinAcctS.Location = new System.Drawing.Point(20, 263);
            this.lblPrinAcctS.Name = "lblPrinAcctS";
            this.lblPrinAcctS.Size = new System.Drawing.Size(130, 18);
            this.lblPrinAcctS.TabIndex = 7;
            this.lblPrinAcctS.Text = "PRINCIPAL ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblPrinAcctS, null);
            // 
            // lblTaxAcctS
            // 
            this.lblTaxAcctS.Location = new System.Drawing.Point(20, 324);
            this.lblTaxAcctS.Name = "lblTaxAcctS";
            this.lblTaxAcctS.Size = new System.Drawing.Size(100, 18);
            this.lblTaxAcctS.TabIndex = 9;
            this.lblTaxAcctS.Text = "TAX ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblTaxAcctS, null);
            // 
            // lblSBillTo
            // 
            this.lblSBillTo.Location = new System.Drawing.Point(20, 204);
            this.lblSBillTo.Name = "lblSBillTo";
            this.lblSBillTo.Size = new System.Drawing.Size(54, 18);
            this.lblSBillTo.TabIndex = 5;
            this.lblSBillTo.Text = "BILL TO";
            this.ToolTip1.SetToolTip(this.lblSBillTo, null);
            // 
            // lblWBillTo
            // 
            this.lblWBillTo.Location = new System.Drawing.Point(392, 204);
            this.lblWBillTo.Name = "lblWBillTo";
            this.lblWBillTo.Size = new System.Drawing.Size(60, 18);
            this.lblWBillTo.TabIndex = 12;
            this.lblWBillTo.Text = "BILL TO";
            this.ToolTip1.SetToolTip(this.lblWBillTo, null);
            // 
            // lblDefaultBill
            // 
            this.lblDefaultBill.Location = new System.Drawing.Point(20, 44);
            this.lblDefaultBill.Name = "lblDefaultBill";
            this.lblDefaultBill.Size = new System.Drawing.Size(120, 18);
            this.lblDefaultBill.TabIndex = 24;
            this.lblDefaultBill.Text = "DEFAULT BILL TYPE";
            this.ToolTip1.SetToolTip(this.lblDefaultBill, null);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 158);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(340, 18);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "SEWER";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label4, null);
            // 
            // Label7
            // 
            this.Label7.Location = new System.Drawing.Point(392, 158);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(330, 15);
            this.Label7.TabIndex = 11;
            this.Label7.Text = "WATER";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label7, null);
            // 
            // fraAdjustments
            // 
            this.fraAdjustments.Controls.Add(this.txtLDNSideAdj);
            this.fraAdjustments.Controls.Add(this.txtLienSideAdj);
            this.fraAdjustments.Controls.Add(this.txtLDNBottomAdj);
            this.fraAdjustments.Controls.Add(this.txtLienBottomAdj);
            this.fraAdjustments.Controls.Add(this.txtLienTopAdj);
            this.fraAdjustments.Controls.Add(this.txtLDNTopAdj);
            this.fraAdjustments.Controls.Add(this.txtSigAdjust_Lien);
            this.fraAdjustments.Controls.Add(this.txtMeterSlipsHAdj);
            this.fraAdjustments.Controls.Add(this.txtMeterSlipsVAdj);
            this.fraAdjustments.Controls.Add(this.txtDMVLabelAdjust);
            this.fraAdjustments.Controls.Add(this.txtDMHLabelAdjust);
            this.fraAdjustments.Controls.Add(this.txtCMFAdjust);
            this.fraAdjustments.Controls.Add(this.txtLabelAdjustment);
            this.fraAdjustments.Controls.Add(this.Label18);
            this.fraAdjustments.Controls.Add(this.lblLienSideAdj);
            this.fraAdjustments.Controls.Add(this.Label17);
            this.fraAdjustments.Controls.Add(this.Label16);
            this.fraAdjustments.Controls.Add(this.Label15);
            this.fraAdjustments.Controls.Add(this.Label14);
            this.fraAdjustments.Controls.Add(this.Label13);
            this.fraAdjustments.Controls.Add(this.lblSigAdjust_Lien);
            this.fraAdjustments.Controls.Add(this.lblMeterSlipsHAdj);
            this.fraAdjustments.Controls.Add(this.lblMeterSlipsVAdj);
            this.fraAdjustments.Controls.Add(this.Label5);
            this.fraAdjustments.Controls.Add(this.Label3);
            this.fraAdjustments.Controls.Add(this.lblDMVLabelAdjust);
            this.fraAdjustments.Controls.Add(this.lblDMHLabelAdjust);
            this.fraAdjustments.Controls.Add(this.Label2);
            this.fraAdjustments.Controls.Add(this.lblCMFAdjust);
            this.fraAdjustments.Controls.Add(this.lblLabelAdjustment);
            this.fraAdjustments.Location = new System.Drawing.Point(30, 122);
            this.fraAdjustments.Name = "fraAdjustments";
            this.fraAdjustments.Size = new System.Drawing.Size(703, 465);
            this.fraAdjustments.TabIndex = 3;
            this.fraAdjustments.Text = "Adjustments";
            this.ToolTip1.SetToolTip(this.fraAdjustments, null);
            this.fraAdjustments.Visible = false;
            // 
            // txtSigAdjust_Lien
            // 
            this.txtSigAdjust_Lien.BackColor = System.Drawing.SystemColors.Window;
            this.txtSigAdjust_Lien.Location = new System.Drawing.Point(247, 155);
            this.txtSigAdjust_Lien.MaxLength = 5;
            this.txtSigAdjust_Lien.Name = "txtSigAdjust_Lien";
            this.txtSigAdjust_Lien.Size = new System.Drawing.Size(73, 40);
            this.txtSigAdjust_Lien.TabIndex = 6;
            this.txtSigAdjust_Lien.Text = "0";
            this.txtSigAdjust_Lien.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtSigAdjust_Lien, null);
            this.txtSigAdjust_Lien.Enter += new System.EventHandler(this.txtSigAdjust_Lien_Enter);
            this.txtSigAdjust_Lien.TextChanged += new System.EventHandler(this.txtSigAdjust_Lien_TextChanged);
            this.txtSigAdjust_Lien.Validating += new System.ComponentModel.CancelEventHandler(this.txtSigAdjust_Lien_Validating);
            this.txtSigAdjust_Lien.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSigAdjust_Lien_KeyPress);
            // 
            // txtMeterSlipsHAdj
            // 
            this.txtMeterSlipsHAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtMeterSlipsHAdj.Location = new System.Drawing.Point(247, 355);
            this.txtMeterSlipsHAdj.MaxLength = 5;
            this.txtMeterSlipsHAdj.Name = "txtMeterSlipsHAdj";
            this.txtMeterSlipsHAdj.Size = new System.Drawing.Size(73, 40);
            this.txtMeterSlipsHAdj.TabIndex = 13;
            this.txtMeterSlipsHAdj.Text = "0";
            this.txtMeterSlipsHAdj.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtMeterSlipsHAdj, null);
            this.txtMeterSlipsHAdj.Validating += new System.ComponentModel.CancelEventHandler(this.txtMeterSlipsHAdj_Validating);
            this.txtMeterSlipsHAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMeterSlipsHAdj_KeyPress);
            // 
            // txtMeterSlipsVAdj
            // 
            this.txtMeterSlipsVAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtMeterSlipsVAdj.Location = new System.Drawing.Point(247, 405);
            this.txtMeterSlipsVAdj.MaxLength = 5;
            this.txtMeterSlipsVAdj.Name = "txtMeterSlipsVAdj";
            this.txtMeterSlipsVAdj.Size = new System.Drawing.Size(73, 40);
            this.txtMeterSlipsVAdj.TabIndex = 15;
            this.txtMeterSlipsVAdj.Text = "0";
            this.txtMeterSlipsVAdj.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtMeterSlipsVAdj, null);
            this.txtMeterSlipsVAdj.Validating += new System.ComponentModel.CancelEventHandler(this.txtMeterSlipsVAdj_Validating);
            this.txtMeterSlipsVAdj.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtMeterSlipsVAdj_KeyPress);
            // 
            // txtDMVLabelAdjust
            // 
            this.txtDMVLabelAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtDMVLabelAdjust.Location = new System.Drawing.Point(247, 280);
            this.txtDMVLabelAdjust.MaxLength = 5;
            this.txtDMVLabelAdjust.Name = "txtDMVLabelAdjust";
            this.txtDMVLabelAdjust.Size = new System.Drawing.Size(73, 40);
            this.txtDMVLabelAdjust.TabIndex = 11;
            this.txtDMVLabelAdjust.Text = "0";
            this.txtDMVLabelAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtDMVLabelAdjust, null);
            this.txtDMVLabelAdjust.Enter += new System.EventHandler(this.txtDMVLabelAdjust_Enter);
            this.txtDMVLabelAdjust.TextChanged += new System.EventHandler(this.txtDMVLabelAdjust_TextChanged);
            this.txtDMVLabelAdjust.Validating += new System.ComponentModel.CancelEventHandler(this.txtDMVLabelAdjust_Validating);
            this.txtDMVLabelAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDMVLabelAdjust_KeyPress);
            // 
            // txtDMHLabelAdjust
            // 
            this.txtDMHLabelAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtDMHLabelAdjust.Location = new System.Drawing.Point(247, 230);
            this.txtDMHLabelAdjust.MaxLength = 5;
            this.txtDMHLabelAdjust.Name = "txtDMHLabelAdjust";
            this.txtDMHLabelAdjust.Size = new System.Drawing.Size(73, 40);
            this.txtDMHLabelAdjust.TabIndex = 9;
            this.txtDMHLabelAdjust.Text = "0";
            this.txtDMHLabelAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtDMHLabelAdjust, null);
            this.txtDMHLabelAdjust.Enter += new System.EventHandler(this.txtDMHLabelAdjust_Enter);
            this.txtDMHLabelAdjust.TextChanged += new System.EventHandler(this.txtDMHLabelAdjust_TextChanged);
            this.txtDMHLabelAdjust.Validating += new System.ComponentModel.CancelEventHandler(this.txtDMHLabelAdjust_Validating);
            this.txtDMHLabelAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDMHLabelAdjust_KeyPress);
            // 
            // txtCMFAdjust
            // 
            this.txtCMFAdjust.BackColor = System.Drawing.SystemColors.Window;
            this.txtCMFAdjust.Location = new System.Drawing.Point(247, 55);
            this.txtCMFAdjust.MaxLength = 5;
            this.txtCMFAdjust.Name = "txtCMFAdjust";
            this.txtCMFAdjust.Size = new System.Drawing.Size(73, 40);
            this.txtCMFAdjust.TabIndex = 2;
            this.txtCMFAdjust.Text = "0";
            this.txtCMFAdjust.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtCMFAdjust, null);
            this.txtCMFAdjust.Enter += new System.EventHandler(this.txtCMFAdjust_Enter);
            this.txtCMFAdjust.TextChanged += new System.EventHandler(this.txtCMFAdjust_TextChanged);
            this.txtCMFAdjust.Validating += new System.ComponentModel.CancelEventHandler(this.txtCMFAdjust_Validating);
            this.txtCMFAdjust.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtCMFAdjust_KeyPress);
            // 
            // txtLabelAdjustment
            // 
            this.txtLabelAdjustment.BackColor = System.Drawing.SystemColors.Window;
            this.txtLabelAdjustment.Location = new System.Drawing.Point(247, 105);
            this.txtLabelAdjustment.MaxLength = 5;
            this.txtLabelAdjustment.Name = "txtLabelAdjustment";
            this.txtLabelAdjustment.Size = new System.Drawing.Size(73, 40);
            this.txtLabelAdjustment.TabIndex = 4;
            this.txtLabelAdjustment.Text = "0";
            this.txtLabelAdjustment.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtLabelAdjustment, null);
            this.txtLabelAdjustment.Enter += new System.EventHandler(this.txtLabelAdjustment_Enter);
            this.txtLabelAdjustment.TextChanged += new System.EventHandler(this.txtLabelAdjustment_TextChanged);
            this.txtLabelAdjustment.Validating += new System.ComponentModel.CancelEventHandler(this.txtLabelAdjustment_Validating);
            this.txtLabelAdjustment.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtLabelAdjustment_KeyPress);
            // 
            // Label18
            // 
            this.Label18.Location = new System.Drawing.Point(350, 319);
            this.Label18.Name = "Label18";
            this.Label18.Size = new System.Drawing.Size(145, 18);
            this.Label18.TabIndex = 27;
            this.Label18.Text = "LIEN SIDE ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.Label18, null);
            // 
            // lblLienSideAdj
            // 
            this.lblLienSideAdj.Location = new System.Drawing.Point(350, 169);
            this.lblLienSideAdj.Name = "lblLienSideAdj";
            this.lblLienSideAdj.Size = new System.Drawing.Size(150, 18);
            this.lblLienSideAdj.TabIndex = 21;
            this.lblLienSideAdj.Text = "LIEN SIDE ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.lblLienSideAdj, null);
            // 
            // Label17
            // 
            this.Label17.Location = new System.Drawing.Point(350, 269);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(230, 18);
            this.Label17.TabIndex = 25;
            this.Label17.Text = "LIEN DISCHARGE BOTTOM ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.Label17, null);
            // 
            // Label16
            // 
            this.Label16.Location = new System.Drawing.Point(350, 119);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(165, 18);
            this.Label16.TabIndex = 19;
            this.Label16.Text = "LIEN BOTTOM ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.Label16, null);
            // 
            // Label15
            // 
            this.Label15.Location = new System.Drawing.Point(350, 69);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(186, 17);
            this.Label15.TabIndex = 17;
            this.Label15.Text = "LIEN TOP ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.Label15, null);
            // 
            // Label14
            // 
            this.Label14.Location = new System.Drawing.Point(350, 30);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(333, 18);
            this.Label14.TabIndex = 16;
            this.Label14.Text = "REGISTRY ADJUSTMENTS (INCHES)";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label14, null);
            // 
            // Label13
            // 
            this.Label13.Location = new System.Drawing.Point(350, 219);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(210, 18);
            this.Label13.TabIndex = 23;
            this.Label13.Text = "LIEN DISCHARGE TOP ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.Label13, null);
            // 
            // lblSigAdjust_Lien
            // 
            this.lblSigAdjust_Lien.Location = new System.Drawing.Point(20, 169);
            this.lblSigAdjust_Lien.Name = "lblSigAdjust_Lien";
            this.lblSigAdjust_Lien.Size = new System.Drawing.Size(170, 18);
            this.lblSigAdjust_Lien.TabIndex = 5;
            this.lblSigAdjust_Lien.Text = "LIEN NOTICE SIGNATURE";
            this.ToolTip1.SetToolTip(this.lblSigAdjust_Lien, null);
            // 
            // lblMeterSlipsHAdj
            // 
            this.lblMeterSlipsHAdj.Location = new System.Drawing.Point(20, 369);
            this.lblMeterSlipsHAdj.Name = "lblMeterSlipsHAdj";
            this.lblMeterSlipsHAdj.Size = new System.Drawing.Size(165, 18);
            this.lblMeterSlipsHAdj.TabIndex = 12;
            this.lblMeterSlipsHAdj.Text = "HORIZONTAL ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.lblMeterSlipsHAdj, null);
            // 
            // lblMeterSlipsVAdj
            // 
            this.lblMeterSlipsVAdj.Location = new System.Drawing.Point(20, 419);
            this.lblMeterSlipsVAdj.Name = "lblMeterSlipsVAdj";
            this.lblMeterSlipsVAdj.Size = new System.Drawing.Size(145, 18);
            this.lblMeterSlipsVAdj.TabIndex = 14;
            this.lblMeterSlipsVAdj.Text = "VERTICAL ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.lblMeterSlipsVAdj, null);
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 330);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(302, 15);
            this.Label5.TabIndex = 7;
            this.Label5.Text = "METER READING SLIP ADJUSTMENTS";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label5, null);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 205);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(298, 15);
            this.Label3.TabIndex = 66;
            this.Label3.Text = "DOT MATRIX ADJUSTMENTS";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label3, null);
            // 
            // lblDMVLabelAdjust
            // 
            this.lblDMVLabelAdjust.Location = new System.Drawing.Point(20, 294);
            this.lblDMVLabelAdjust.Name = "lblDMVLabelAdjust";
            this.lblDMVLabelAdjust.Size = new System.Drawing.Size(186, 18);
            this.lblDMVLabelAdjust.TabIndex = 10;
            this.lblDMVLabelAdjust.Text = "LABEL VERTICAL ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.lblDMVLabelAdjust, null);
            // 
            // lblDMHLabelAdjust
            // 
            this.lblDMHLabelAdjust.Location = new System.Drawing.Point(20, 244);
            this.lblDMHLabelAdjust.Name = "lblDMHLabelAdjust";
            this.lblDMHLabelAdjust.Size = new System.Drawing.Size(194, 18);
            this.lblDMHLabelAdjust.TabIndex = 8;
            this.lblDMHLabelAdjust.Text = "LABEL HORIZONTAL ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.lblDMHLabelAdjust, null);
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 30);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(298, 15);
            this.Label2.TabIndex = 67;
            this.Label2.Text = "LASER ADJUSTMENTS";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.Label2, null);
            // 
            // lblCMFAdjust
            // 
            this.lblCMFAdjust.Location = new System.Drawing.Point(20, 69);
            this.lblCMFAdjust.Name = "lblCMFAdjust";
            this.lblCMFAdjust.Size = new System.Drawing.Size(180, 18);
            this.lblCMFAdjust.TabIndex = 1;
            this.lblCMFAdjust.Text = "CERTIFIED MAIL FORMS";
            this.ToolTip1.SetToolTip(this.lblCMFAdjust, null);
            // 
            // lblLabelAdjustment
            // 
            this.lblLabelAdjustment.Location = new System.Drawing.Point(20, 119);
            this.lblLabelAdjustment.Name = "lblLabelAdjustment";
            this.lblLabelAdjustment.Size = new System.Drawing.Size(186, 18);
            this.lblLabelAdjustment.TabIndex = 3;
            this.lblLabelAdjustment.Text = "LABEL VERTICAL ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.lblLabelAdjustment, null);
            // 
            // fraPaymentOptions
            // 
            this.fraPaymentOptions.Controls.Add(this.chkSkipLDNPrompt);
            this.fraPaymentOptions.Controls.Add(this.cmbPayFirst);
            this.fraPaymentOptions.Controls.Add(this.cmbBasis);
            this.fraPaymentOptions.Controls.Add(this.chkDisableAutoPmtFill);
            this.fraPaymentOptions.Controls.Add(this.chkDefaultUTRTDDate);
            this.fraPaymentOptions.Controls.Add(this.cboAutopay);
            this.fraPaymentOptions.Controls.Add(this.chkDefaultAccount);
            this.fraPaymentOptions.Controls.Add(this.txtDiscountPercent);
            this.fraPaymentOptions.Controls.Add(this.Label10);
            this.fraPaymentOptions.Controls.Add(this.lblPayFirst);
            this.fraPaymentOptions.Controls.Add(this.lblBasis);
            this.fraPaymentOptions.Controls.Add(this.lblDiscountPercent);
            this.fraPaymentOptions.Location = new System.Drawing.Point(30, 122);
            this.fraPaymentOptions.Name = "fraPaymentOptions";
            this.fraPaymentOptions.Size = new System.Drawing.Size(573, 388);
            this.fraPaymentOptions.TabIndex = 4;
            this.fraPaymentOptions.Text = "Payment Options";
            this.ToolTip1.SetToolTip(this.fraPaymentOptions, null);
            this.fraPaymentOptions.Visible = false;
            // 
            // chkSkipLDNPrompt
            // 
            this.chkSkipLDNPrompt.Location = new System.Drawing.Point(20, 341);
            this.chkSkipLDNPrompt.Name = "chkSkipLDNPrompt";
            this.chkSkipLDNPrompt.Size = new System.Drawing.Size(438, 26);
            this.chkSkipLDNPrompt.TabIndex = 11;
            this.chkSkipLDNPrompt.Text = "Automatically save LDN without prompting to print in Cash Receipting";
            this.ToolTip1.SetToolTip(this.chkSkipLDNPrompt, null);
            this.chkSkipLDNPrompt.CheckedChanged += new System.EventHandler(this.chkSkipLDNPrompt_CheckedChanged);
            // 
            // chkDisableAutoPmtFill
            // 
            this.chkDisableAutoPmtFill.Location = new System.Drawing.Point(20, 304);
            this.chkDisableAutoPmtFill.Name = "chkDisableAutoPmtFill";
            this.chkDisableAutoPmtFill.Size = new System.Drawing.Size(248, 26);
            this.chkDisableAutoPmtFill.TabIndex = 10;
            this.chkDisableAutoPmtFill.Text = "Disable autofill payment on Insert key";
            this.ToolTip1.SetToolTip(this.chkDisableAutoPmtFill, "Disable using the Insert key or double click to automatically fill the payment am" +
        "ount.");
            this.chkDisableAutoPmtFill.CheckedChanged += new System.EventHandler(this.chkDisableAutoPmtFill_CheckedChanged);
            // 
            // chkDefaultUTRTDDate
            // 
            this.chkDefaultUTRTDDate.Location = new System.Drawing.Point(20, 267);
            this.chkDefaultUTRTDDate.Name = "chkDefaultUTRTDDate";
            this.chkDefaultUTRTDDate.Size = new System.Drawing.Size(242, 26);
            this.chkDefaultUTRTDDate.TabIndex = 9;
            this.chkDefaultUTRTDDate.Text = "Automatically change RTD with ETD";
            this.ToolTip1.SetToolTip(this.chkDefaultUTRTDDate, "Automatically change Recorded Transaction Date when changing the Effective Transa" +
        "ction Date on the Payment Screen.");
            // 
            // cboAutopay
            // 
            this.cboAutopay.BackColor = System.Drawing.SystemColors.Window;
            this.cboAutopay.Items.AddRange(new object[] {
            "1 - Oldest Bill First",
            "2 - Primary Service First"});
            this.cboAutopay.Location = new System.Drawing.Point(205, 180);
            this.cboAutopay.Name = "cboAutopay";
            this.cboAutopay.Size = new System.Drawing.Size(215, 40);
            this.cboAutopay.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.cboAutopay, null);
            // 
            // chkDefaultAccount
            // 
            this.chkDefaultAccount.Location = new System.Drawing.Point(20, 230);
            this.chkDefaultAccount.Name = "chkDefaultAccount";
            this.chkDefaultAccount.Size = new System.Drawing.Size(415, 26);
            this.chkDefaultAccount.TabIndex = 8;
            this.chkDefaultAccount.Text = "Show last account accessed when entering from Cash Receipting";
            this.ToolTip1.SetToolTip(this.chkDefaultAccount, null);
            this.chkDefaultAccount.CheckedChanged += new System.EventHandler(this.chkDefaultAccount_CheckedChanged);
            // 
            // txtDiscountPercent
            // 
            this.txtDiscountPercent.BackColor = System.Drawing.SystemColors.Window;
            this.txtDiscountPercent.Location = new System.Drawing.Point(205, 30);
            this.txtDiscountPercent.Name = "txtDiscountPercent";
            this.txtDiscountPercent.Size = new System.Drawing.Size(215, 40);
            this.txtDiscountPercent.TabIndex = 1;
            this.txtDiscountPercent.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtDiscountPercent, null);
            this.txtDiscountPercent.Visible = false;
            this.txtDiscountPercent.Enter += new System.EventHandler(this.txtDiscountPercent_Enter);
            this.txtDiscountPercent.TextChanged += new System.EventHandler(this.txtDiscountPercent_TextChanged);
            this.txtDiscountPercent.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtDiscountPercent_KeyPress);
            // 
            // Label10
            // 
            this.Label10.Location = new System.Drawing.Point(20, 194);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(110, 18);
            this.Label10.TabIndex = 6;
            this.Label10.Text = "AUTOPAY OPTION";
            this.ToolTip1.SetToolTip(this.Label10, null);
            // 
            // lblPayFirst
            // 
            this.lblPayFirst.Location = new System.Drawing.Point(20, 94);
            this.lblPayFirst.Name = "lblPayFirst";
            this.lblPayFirst.Size = new System.Drawing.Size(69, 18);
            this.lblPayFirst.TabIndex = 2;
            this.lblPayFirst.Text = "PAY FIRST";
            this.ToolTip1.SetToolTip(this.lblPayFirst, null);
            // 
            // lblBasis
            // 
            this.lblBasis.Location = new System.Drawing.Point(20, 144);
            this.lblBasis.Name = "lblBasis";
            this.lblBasis.Size = new System.Drawing.Size(41, 18);
            this.lblBasis.TabIndex = 4;
            this.lblBasis.Text = "BASIS";
            this.ToolTip1.SetToolTip(this.lblBasis, null);
            // 
            // lblDiscountPercent
            // 
            this.lblDiscountPercent.Location = new System.Drawing.Point(20, 44);
            this.lblDiscountPercent.Name = "lblDiscountPercent";
            this.lblDiscountPercent.Size = new System.Drawing.Size(150, 18);
            this.lblDiscountPercent.TabIndex = 12;
            this.lblDiscountPercent.Text = "DISCOUNT PERCENTAGE";
            this.ToolTip1.SetToolTip(this.lblDiscountPercent, null);
            this.lblDiscountPercent.Visible = false;
            // 
            // fraInterest
            // 
            this.fraInterest.Controls.Add(this.chkTACheckRE);
            this.fraInterest.Controls.Add(this.cmbInterestType);
            this.fraInterest.Controls.Add(this.chkTAUpdateRE);
            this.fraInterest.Controls.Add(this.chkShowTaxAcquired);
            this.fraInterest.Controls.Add(this.chkUseBookForLiens);
            this.fraInterest.Controls.Add(this.chkOnlyCheckOldestLien);
            this.fraInterest.Controls.Add(this.txtPPayInterestRate);
            this.fraInterest.Controls.Add(this.chkInterest_3);
            this.fraInterest.Controls.Add(this.chkInterest_2);
            this.fraInterest.Controls.Add(this.chkInterest_1);
            this.fraInterest.Controls.Add(this.chkInterest_0);
            this.fraInterest.Controls.Add(this.txtFlatInterestAmount);
            this.fraInterest.Controls.Add(this.lblPPayInterestRate);
            this.fraInterest.Controls.Add(this.lblChargeInterestOn);
            this.fraInterest.Controls.Add(this.lblFlatInterestRate);
            this.fraInterest.Controls.Add(this.lblInterestType);
            this.fraInterest.Location = new System.Drawing.Point(30, 122);
            this.fraInterest.Name = "fraInterest";
            this.fraInterest.Size = new System.Drawing.Size(386, 450);
            this.fraInterest.TabIndex = 5;
            this.fraInterest.Text = "Interest Options";
            this.ToolTip1.SetToolTip(this.fraInterest, null);
            this.fraInterest.Visible = false;
            // 
            // chkTACheckRE
            // 
            this.chkTACheckRE.Location = new System.Drawing.Point(20, 402);
            this.chkTACheckRE.Name = "chkTACheckRE";
            this.chkTACheckRE.Size = new System.Drawing.Size(210, 26);
            this.chkTACheckRE.TabIndex = 178;
            this.chkTACheckRE.Text = "Check RE Tax Acquired Status";
            this.ToolTip1.SetToolTip(this.chkTACheckRE, null);
            this.chkTACheckRE.CheckedChanged += new System.EventHandler(this.chkTACheckRE_CheckedChanged);
            // 
            // chkTAUpdateRE
            // 
            this.chkTAUpdateRE.Location = new System.Drawing.Point(20, 365);
            this.chkTAUpdateRE.Name = "chkTAUpdateRE";
            this.chkTAUpdateRE.Size = new System.Drawing.Size(248, 26);
            this.chkTAUpdateRE.TabIndex = 172;
            this.chkTAUpdateRE.Text = "Auto-Update RE Tax Acquired Status";
            this.ToolTip1.SetToolTip(this.chkTAUpdateRE, null);
            this.chkTAUpdateRE.CheckedChanged += new System.EventHandler(this.chkTAUpdateRE_CheckedChanged);
            // 
            // chkShowTaxAcquired
            // 
            this.chkShowTaxAcquired.Location = new System.Drawing.Point(20, 328);
            this.chkShowTaxAcquired.Name = "chkShowTaxAcquired";
            this.chkShowTaxAcquired.Size = new System.Drawing.Size(236, 26);
            this.chkShowTaxAcquired.TabIndex = 171;
            this.chkShowTaxAcquired.Text = "Show \'Tax Acquired\' on FC Notices";
            this.ToolTip1.SetToolTip(this.chkShowTaxAcquired, null);
            // 
            // chkUseBookForLiens
            // 
            this.chkUseBookForLiens.Location = new System.Drawing.Point(20, 291);
            this.chkUseBookForLiens.Name = "chkUseBookForLiens";
            this.chkUseBookForLiens.Size = new System.Drawing.Size(102, 26);
            this.chkUseBookForLiens.TabIndex = 32;
            this.chkUseBookForLiens.Text = "Lien by book";
            this.ToolTip1.SetToolTip(this.chkUseBookForLiens, null);
            // 
            // chkOnlyCheckOldestLien
            // 
            this.chkOnlyCheckOldestLien.Location = new System.Drawing.Point(20, 254);
            this.chkOnlyCheckOldestLien.Name = "chkOnlyCheckOldestLien";
            this.chkOnlyCheckOldestLien.Size = new System.Drawing.Size(272, 26);
            this.chkOnlyCheckOldestLien.TabIndex = 31;
            this.chkOnlyCheckOldestLien.Text = "Only check the oldest bill for lien eligibility";
            this.ToolTip1.SetToolTip(this.chkOnlyCheckOldestLien, null);
            this.chkOnlyCheckOldestLien.CheckedChanged += new System.EventHandler(this.chkOnlyCheckOldestLien_CheckedChanged);
            // 
            // chkInterest_3
            // 
            this.chkInterest_3.Location = new System.Drawing.Point(274, 217);
            this.chkInterest_3.Name = "chkInterest_3";
            this.chkInterest_3.Size = new System.Drawing.Size(60, 26);
            this.chkInterest_3.TabIndex = 30;
            this.chkInterest_3.Text = "Costs";
            this.ToolTip1.SetToolTip(this.chkInterest_3, null);
            this.chkInterest_3.CheckedChanged += new System.EventHandler(this.chkInterest_CheckedChanged);
            // 
            // chkInterest_2
            // 
            this.chkInterest_2.Location = new System.Drawing.Point(164, 217);
            this.chkInterest_2.Name = "chkInterest_2";
            this.chkInterest_2.Size = new System.Drawing.Size(70, 26);
            this.chkInterest_2.TabIndex = 28;
            this.chkInterest_2.Text = "Interest";
            this.ToolTip1.SetToolTip(this.chkInterest_2, null);
            this.chkInterest_2.CheckedChanged += new System.EventHandler(this.chkInterest_CheckedChanged);
            // 
            // chkInterest_1
            // 
            this.chkInterest_1.Location = new System.Drawing.Point(274, 180);
            this.chkInterest_1.Name = "chkInterest_1";
            this.chkInterest_1.Size = new System.Drawing.Size(48, 26);
            this.chkInterest_1.TabIndex = 29;
            this.chkInterest_1.Text = "Tax";
            this.ToolTip1.SetToolTip(this.chkInterest_1, null);
            this.chkInterest_1.CheckedChanged += new System.EventHandler(this.chkInterest_CheckedChanged);
            // 
            // chkInterest_0
            // 
            this.chkInterest_0.Location = new System.Drawing.Point(164, 180);
            this.chkInterest_0.Name = "chkInterest_0";
            this.chkInterest_0.Size = new System.Drawing.Size(77, 26);
            this.chkInterest_0.TabIndex = 27;
            this.chkInterest_0.Text = "Principal";
            this.ToolTip1.SetToolTip(this.chkInterest_0, null);
            this.chkInterest_0.CheckedChanged += new System.EventHandler(this.chkInterest_CheckedChanged);
            // 
            // lblPPayInterestRate
            // 
            this.lblPPayInterestRate.Location = new System.Drawing.Point(20, 44);
            this.lblPPayInterestRate.Name = "lblPPayInterestRate";
            this.lblPPayInterestRate.Size = new System.Drawing.Size(180, 18);
            this.lblPPayInterestRate.TabIndex = 93;
            this.lblPPayInterestRate.Text = "PREPAYMENT INTEREST RATE";
            this.ToolTip1.SetToolTip(this.lblPPayInterestRate, null);
            // 
            // lblChargeInterestOn
            // 
            this.lblChargeInterestOn.Location = new System.Drawing.Point(20, 187);
            this.lblChargeInterestOn.Name = "lblChargeInterestOn";
            this.lblChargeInterestOn.Size = new System.Drawing.Size(129, 22);
            this.lblChargeInterestOn.TabIndex = 92;
            this.lblChargeInterestOn.Text = "CHARGE INTEREST ON";
            this.ToolTip1.SetToolTip(this.lblChargeInterestOn, null);
            // 
            // lblFlatInterestRate
            // 
            this.lblFlatInterestRate.Enabled = false;
            this.lblFlatInterestRate.Location = new System.Drawing.Point(20, 144);
            this.lblFlatInterestRate.Name = "lblFlatInterestRate";
            this.lblFlatInterestRate.Size = new System.Drawing.Size(158, 18);
            this.lblFlatInterestRate.TabIndex = 91;
            this.lblFlatInterestRate.Text = "FLAT INTEREST AMOUNT";
            this.ToolTip1.SetToolTip(this.lblFlatInterestRate, null);
            // 
            // lblInterestType
            // 
            this.lblInterestType.Location = new System.Drawing.Point(20, 94);
            this.lblInterestType.Name = "lblInterestType";
            this.lblInterestType.Size = new System.Drawing.Size(100, 18);
            this.lblInterestType.TabIndex = 89;
            this.lblInterestType.Text = "INTEREST TYPE";
            this.ToolTip1.SetToolTip(this.lblInterestType, null);
            // 
            // fraOther
            // 
            this.fraOther.AppearanceKey = "groupBoxLeftBorder";
            this.fraOther.Controls.Add(this.fraReturnAddress);
            this.fraOther.Controls.Add(this.cmbReceiptSize);
            this.fraOther.Controls.Add(this.lblReceiptSize);
            this.fraOther.Controls.Add(this.fraMultiplier);
            this.fraOther.Controls.Add(this.fraTownService);
            this.fraOther.Controls.Add(this.fraRemoteReader);
            this.fraOther.Controls.Add(this.fraReportSequence);
            this.fraOther.Location = new System.Drawing.Point(30, 122);
            this.fraOther.Name = "fraOther";
            this.fraOther.Size = new System.Drawing.Size(827, 561);
            this.fraOther.TabIndex = 6;
            this.fraOther.Text = "Other Options";
            this.ToolTip1.SetToolTip(this.fraOther, null);
            this.fraOther.Visible = false;
            // 
            // fraReturnAddress
            // 
            this.fraReturnAddress.Controls.Add(this.vsReturnAddress);
            this.fraReturnAddress.Location = new System.Drawing.Point(477, 140);
            this.fraReturnAddress.Name = "fraReturnAddress";
            this.fraReturnAddress.Size = new System.Drawing.Size(350, 362);
            this.fraReturnAddress.TabIndex = 6;
            this.fraReturnAddress.Text = "Return Address";
            this.ToolTip1.SetToolTip(this.fraReturnAddress, null);
            // 
            // vsReturnAddress
            // 
            this.vsReturnAddress.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsReturnAddress.Cols = 1;
            this.vsReturnAddress.ColumnHeadersVisible = false;
            this.vsReturnAddress.ExtendLastCol = true;
            this.vsReturnAddress.FixedCols = 0;
            this.vsReturnAddress.FixedRows = 0;
            this.vsReturnAddress.Location = new System.Drawing.Point(20, 30);
            this.vsReturnAddress.Name = "vsReturnAddress";
            this.vsReturnAddress.RowHeadersVisible = false;
            this.vsReturnAddress.Rows = 4;
            this.vsReturnAddress.ShowFocusCell = false;
            this.vsReturnAddress.Size = new System.Drawing.Size(310, 312);
            this.ToolTip1.SetToolTip(this.vsReturnAddress, null);
            this.vsReturnAddress.CurrentCellChanged += new System.EventHandler(this.vsReturnAddress_RowColChange);
            this.vsReturnAddress.Enter += new System.EventHandler(this.vsReturnAddress_Enter);
            // 
            // fraMultiplier
            // 
            this.fraMultiplier.Controls.Add(this.cmbMultiplier);
            this.fraMultiplier.Controls.Add(this.lblMultiplier);
            this.fraMultiplier.Controls.Add(this.lblMultiplier2);
            this.fraMultiplier.Location = new System.Drawing.Point(477, 30);
            this.fraMultiplier.Name = "fraMultiplier";
            this.fraMultiplier.Size = new System.Drawing.Size(350, 90);
            this.fraMultiplier.TabIndex = 5;
            this.fraMultiplier.Text = "Reading Units";
            this.ToolTip1.SetToolTip(this.fraMultiplier, null);
            // 
            // cmbMultiplier
            // 
            this.cmbMultiplier.BackColor = System.Drawing.SystemColors.Window;
            this.cmbMultiplier.Location = new System.Drawing.Point(175, 30);
            this.cmbMultiplier.Name = "cmbMultiplier";
            this.cmbMultiplier.Size = new System.Drawing.Size(81, 40);
            this.cmbMultiplier.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbMultiplier, null);
            this.cmbMultiplier.SelectedIndexChanged += new System.EventHandler(this.cmbMultiplier_SelectedIndexChanged);
            // 
            // lblMultiplier
            // 
            this.lblMultiplier.Location = new System.Drawing.Point(20, 44);
            this.lblMultiplier.Name = "lblMultiplier";
            this.lblMultiplier.Size = new System.Drawing.Size(140, 18);
            this.lblMultiplier.TabIndex = 2;
            this.lblMultiplier.Text = "RATE TABLES SETUP IN";
            this.ToolTip1.SetToolTip(this.lblMultiplier, null);
            // 
            // lblMultiplier2
            // 
            this.lblMultiplier2.Location = new System.Drawing.Point(270, 44);
            this.lblMultiplier2.Name = "lblMultiplier2";
            this.lblMultiplier2.Size = new System.Drawing.Size(60, 18);
            this.lblMultiplier2.TabIndex = 2;
            this.lblMultiplier2.Text = "CUBIC FT";
            this.ToolTip1.SetToolTip(this.lblMultiplier2, null);
            // 
            // fraTownService
            // 
            this.fraTownService.Controls.Add(this.cmbService);
            this.fraTownService.Controls.Add(this.lblService);
            this.fraTownService.Location = new System.Drawing.Point(20, 30);
            this.fraTownService.Name = "fraTownService";
            this.fraTownService.Size = new System.Drawing.Size(427, 90);
            this.fraTownService.TabIndex = 7;
            this.fraTownService.Text = "Town Service";
            this.ToolTip1.SetToolTip(this.fraTownService, null);
            // 
            // cmbService
            // 
            this.cmbService.BackColor = System.Drawing.SystemColors.Window;
            this.cmbService.Location = new System.Drawing.Point(175, 30);
            this.cmbService.Name = "cmbService";
            this.cmbService.Size = new System.Drawing.Size(232, 40);
            this.cmbService.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbService, null);
            this.cmbService.SelectedIndexChanged += new System.EventHandler(this.cmbService_SelectedIndexChanged);
            // 
            // lblService
            // 
            this.lblService.Location = new System.Drawing.Point(20, 44);
            this.lblService.Name = "lblService";
            this.lblService.Size = new System.Drawing.Size(55, 18);
            this.lblService.TabIndex = 2;
            this.lblService.Text = "SERVICE";
            this.ToolTip1.SetToolTip(this.lblService, null);
            // 
            // fraRemoteReader
            // 
            this.fraRemoteReader.Controls.Add(this.txtVFlexSiteCode);
            this.fraRemoteReader.Controls.Add(this.chkForceReadDate);
            this.fraRemoteReader.Controls.Add(this.cmbExportFormat);
            this.fraRemoteReader.Controls.Add(this.lblExportFormat);
            this.fraRemoteReader.Controls.Add(this.chkShowLocation);
            this.fraRemoteReader.Controls.Add(this.cmbExtractFileType);
            this.fraRemoteReader.Controls.Add(this.lblExtractFileType);
            this.fraRemoteReader.Location = new System.Drawing.Point(20, 287);
            this.fraRemoteReader.Name = "fraRemoteReader";
            this.fraRemoteReader.Size = new System.Drawing.Size(427, 214);
            this.fraRemoteReader.TabIndex = 2;
            this.fraRemoteReader.Text = "Remote Reader / Extract Format";
            this.ToolTip1.SetToolTip(this.fraRemoteReader, null);
            // 
            // txtVFlexSiteCode
            // 
            this.txtVFlexSiteCode.Location = new System.Drawing.Point(175, 81);
            this.txtVFlexSiteCode.Name = "txtVFlexSiteCode";
            this.txtVFlexSiteCode.Size = new System.Drawing.Size(232, 36);
            this.txtVFlexSiteCode.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.txtVFlexSiteCode, null);
            this.txtVFlexSiteCode.Visible = false;
            // 
            // chkForceReadDate
            // 
            this.chkForceReadDate.Location = new System.Drawing.Point(20, 167);
            this.chkForceReadDate.Name = "chkForceReadDate";
            this.chkForceReadDate.Size = new System.Drawing.Size(189, 26);
            this.chkForceReadDate.TabIndex = 5;
            this.chkForceReadDate.Text = "Ignore Reading Date in File";
            this.ToolTip1.SetToolTip(this.chkForceReadDate, null);
            // 
            // chkShowLocation
            // 
            this.chkShowLocation.Location = new System.Drawing.Point(20, 130);
            this.chkShowLocation.Name = "chkShowLocation";
            this.chkShowLocation.Size = new System.Drawing.Size(185, 26);
            this.chkShowLocation.TabIndex = 4;
            this.chkShowLocation.Text = "Location in Comment Field";
            this.ToolTip1.SetToolTip(this.chkShowLocation, null);
            this.chkShowLocation.CheckedChanged += new System.EventHandler(this.chkShowLocation_CheckedChanged);
            // 
            // cmbExtractFileType
            // 
            this.cmbExtractFileType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbExtractFileType.Location = new System.Drawing.Point(175, 30);
            this.cmbExtractFileType.Name = "cmbExtractFileType";
            this.cmbExtractFileType.Size = new System.Drawing.Size(232, 40);
            this.cmbExtractFileType.TabIndex = 1;
            this.ToolTip1.SetToolTip(this.cmbExtractFileType, null);
            this.cmbExtractFileType.SelectedIndexChanged += new System.EventHandler(this.cmbExtractFileType_SelectedIndexChanged);
            // 
            // lblExtractFileType
            // 
            this.lblExtractFileType.Location = new System.Drawing.Point(20, 44);
            this.lblExtractFileType.Name = "lblExtractFileType";
            this.lblExtractFileType.Size = new System.Drawing.Size(120, 18);
            this.lblExtractFileType.TabIndex = 6;
            this.lblExtractFileType.Text = "EXTRACT FILE TYPE";
            this.ToolTip1.SetToolTip(this.lblExtractFileType, null);
            // 
            // fraReportSequence
            // 
            this.fraReportSequence.Controls.Add(this.chkMapLot);
            this.fraReportSequence.Controls.Add(this.cmbSeq);
            this.fraReportSequence.Controls.Add(this.LblSeq);
            this.fraReportSequence.Location = new System.Drawing.Point(20, 140);
            this.fraReportSequence.Name = "fraReportSequence";
            this.fraReportSequence.Size = new System.Drawing.Size(427, 127);
            this.fraReportSequence.TabIndex = 1;
            this.fraReportSequence.Text = "Audit Sequence";
            this.ToolTip1.SetToolTip(this.fraReportSequence, null);
            // 
            // chkMapLot
            // 
            this.chkMapLot.Location = new System.Drawing.Point(20, 80);
            this.chkMapLot.Name = "chkMapLot";
            this.chkMapLot.Size = new System.Drawing.Size(152, 26);
            this.chkMapLot.TabIndex = 2;
            this.chkMapLot.Text = "Show Name on Audit";
            this.ToolTip1.SetToolTip(this.chkMapLot, null);
            this.chkMapLot.CheckedChanged += new System.EventHandler(this.chkMapLot_CheckedChanged);
            // 
            // LblSeq
            // 
            this.LblSeq.Location = new System.Drawing.Point(20, 44);
            this.LblSeq.Name = "LblSeq";
            this.LblSeq.Size = new System.Drawing.Size(90, 18);
            this.LblSeq.TabIndex = 3;
            this.LblSeq.Text = "SEQUENCE BY";
            this.ToolTip1.SetToolTip(this.LblSeq, null);
            // 
            // fraIConnect
            // 
            this.fraIConnect.Controls.Add(this.txtInvStartDate);
            this.fraIConnect.Controls.Add(this.cmdSetupICAccounts);
            this.fraIConnect.Controls.Add(this.chkUseCRAccounts);
            this.fraIConnect.Controls.Add(this.txtMFeeAccount);
            this.fraIConnect.Controls.Add(this.lblInvStartDate);
            this.fraIConnect.Controls.Add(this.Label11);
            this.fraIConnect.Location = new System.Drawing.Point(30, 122);
            this.fraIConnect.Name = "fraIConnect";
            this.fraIConnect.Size = new System.Drawing.Size(420, 237);
            this.fraIConnect.TabIndex = 6;
            this.fraIConnect.Text = "Iconnect Options";
            this.ToolTip1.SetToolTip(this.fraIConnect, null);
            this.fraIConnect.Visible = false;
            // 
            // txtInvStartDate
            // 
            this.txtInvStartDate.Location = new System.Drawing.Point(207, 177);
            this.txtInvStartDate.Name = "txtInvStartDate";
            this.txtInvStartDate.Size = new System.Drawing.Size(113, 40);
            this.txtInvStartDate.TabIndex = 5;
            this.ToolTip1.SetToolTip(this.txtInvStartDate, null);
            this.txtInvStartDate.TextChanged += new System.EventHandler(this.txtInvStartDate_TextChanged);
            this.txtInvStartDate.Validating += new System.ComponentModel.CancelEventHandler(this.txtInvStartDate_Validating);
            // 
            // cmdSetupICAccounts
            // 
            this.cmdSetupICAccounts.AppearanceKey = "actionButton";
            this.cmdSetupICAccounts.Location = new System.Drawing.Point(20, 67);
            this.cmdSetupICAccounts.Name = "cmdSetupICAccounts";
            this.cmdSetupICAccounts.Size = new System.Drawing.Size(156, 40);
            this.cmdSetupICAccounts.TabIndex = 1;
            this.cmdSetupICAccounts.Text = "Setup Accounts";
            this.ToolTip1.SetToolTip(this.cmdSetupICAccounts, null);
            this.cmdSetupICAccounts.Click += new System.EventHandler(this.cmdSetupICAccounts_Click);
            // 
            // chkUseCRAccounts
            // 
            this.chkUseCRAccounts.Location = new System.Drawing.Point(20, 30);
            this.chkUseCRAccounts.Name = "chkUseCRAccounts";
            this.chkUseCRAccounts.Size = new System.Drawing.Size(156, 27);
            this.chkUseCRAccounts.TabIndex = 6;
            this.chkUseCRAccounts.Text = "Use CR Accounts";
            this.ToolTip1.SetToolTip(this.chkUseCRAccounts, null);
            this.chkUseCRAccounts.CheckedChanged += new System.EventHandler(this.chkUseCRAccounts_CheckedChanged);
            // 
            // txtMFeeAccount
            // 
            this.txtMFeeAccount.Cols = 1;
            this.txtMFeeAccount.ColumnHeadersVisible = false;
            this.txtMFeeAccount.ExtendLastCol = true;
            this.txtMFeeAccount.FixedCols = 0;
            this.txtMFeeAccount.FixedRows = 0;
            this.txtMFeeAccount.Location = new System.Drawing.Point(207, 127);
            this.txtMFeeAccount.Name = "txtMFeeAccount";
            this.txtMFeeAccount.RowHeadersVisible = false;
            this.txtMFeeAccount.Rows = 1;
            this.txtMFeeAccount.ShowFocusCell = false;
            this.txtMFeeAccount.Size = new System.Drawing.Size(195, 42);
            this.txtMFeeAccount.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.txtMFeeAccount, null);
            this.txtMFeeAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtMFeeAccount_KeyDownEvent);
            // 
            // lblInvStartDate
            // 
            this.lblInvStartDate.Location = new System.Drawing.Point(20, 191);
            this.lblInvStartDate.Name = "lblInvStartDate";
            this.lblInvStartDate.Size = new System.Drawing.Size(135, 18);
            this.lblInvStartDate.TabIndex = 4;
            this.lblInvStartDate.Text = "UPLOAD BILLS AFTER";
            this.ToolTip1.SetToolTip(this.lblInvStartDate, null);
            // 
            // Label11
            // 
            this.Label11.Location = new System.Drawing.Point(20, 141);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(160, 18);
            this.Label11.TabIndex = 2;
            this.Label11.Text = "MERCHANT FEE ACCOUNT";
            this.ToolTip1.SetToolTip(this.Label11, null);
            // 
            // fraDefaultOptions
            // 
            this.fraDefaultOptions.Controls.Add(this.chkDefaultMHInfo);
            this.fraDefaultOptions.Controls.Add(this.chkDefaultRE);
            this.fraDefaultOptions.Controls.Add(this.cmbState);
            this.fraDefaultOptions.Controls.Add(this.txtDefaultZip);
            this.fraDefaultOptions.Controls.Add(this.txtDefaultCity);
            this.fraDefaultOptions.Controls.Add(this.cmbDefaultCategoryS);
            this.fraDefaultOptions.Controls.Add(this.cmbDefaultFrequency);
            this.fraDefaultOptions.Controls.Add(this.cmbDefaultDigits);
            this.fraDefaultOptions.Controls.Add(this.cmbDefaultSize);
            this.fraDefaultOptions.Controls.Add(this.cmbDefaultCategoryW);
            this.fraDefaultOptions.Controls.Add(this.txtDefaultAccountW);
            this.fraDefaultOptions.Controls.Add(this.txtDefaultAccountS);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultCity);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultState);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultZip);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultAccountW);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultAccountS);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultCategoryS);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultMeter);
            this.fraDefaultOptions.Controls.Add(this.lblDDefaultFrequency);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultDigits);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultSize);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultAccount);
            this.fraDefaultOptions.Controls.Add(this.lblDefaultCategoryW);
            this.fraDefaultOptions.Location = new System.Drawing.Point(30, 122);
            this.fraDefaultOptions.Name = "fraDefaultOptions";
            this.fraDefaultOptions.Size = new System.Drawing.Size(760, 492);
            this.fraDefaultOptions.TabIndex = 8;
            this.fraDefaultOptions.Text = "Default Values";
            this.ToolTip1.SetToolTip(this.fraDefaultOptions, null);
            this.fraDefaultOptions.Visible = false;
            // 
            // chkDefaultMHInfo
            // 
            this.chkDefaultMHInfo.Location = new System.Drawing.Point(20, 445);
            this.chkDefaultMHInfo.Name = "chkDefaultMHInfo";
            this.chkDefaultMHInfo.Size = new System.Drawing.Size(152, 26);
            this.chkDefaultMHInfo.TabIndex = 16;
            this.chkDefaultMHInfo.Text = "Mortgage Holder Info";
            this.ToolTip1.SetToolTip(this.chkDefaultMHInfo, null);
            // 
            // chkDefaultRE
            // 
            this.chkDefaultRE.Location = new System.Drawing.Point(20, 408);
            this.chkDefaultRE.Name = "chkDefaultRE";
            this.chkDefaultRE.Size = new System.Drawing.Size(98, 26);
            this.chkDefaultRE.TabIndex = 15;
            this.chkDefaultRE.Text = "Use RE Info";
            this.ToolTip1.SetToolTip(this.chkDefaultRE, null);
            // 
            // cmbState
            // 
            this.cmbState.BackColor = System.Drawing.SystemColors.Window;
            this.cmbState.Location = new System.Drawing.Point(167, 308);
            this.cmbState.Name = "cmbState";
            this.cmbState.Size = new System.Drawing.Size(223, 40);
            this.cmbState.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.cmbState, null);
            this.cmbState.SelectedIndexChanged += new System.EventHandler(this.cmbState_SelectedIndexChanged);
            // 
            // txtDefaultZip
            // 
            this.txtDefaultZip.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultZip.Location = new System.Drawing.Point(167, 358);
            this.txtDefaultZip.MaxLength = 5;
            this.txtDefaultZip.Name = "txtDefaultZip";
            this.txtDefaultZip.Size = new System.Drawing.Size(220, 40);
            this.txtDefaultZip.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.txtDefaultZip, null);
            // 
            // txtDefaultCity
            // 
            this.txtDefaultCity.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultCity.Location = new System.Drawing.Point(167, 258);
            this.txtDefaultCity.Name = "txtDefaultCity";
            this.txtDefaultCity.Size = new System.Drawing.Size(220, 40);
            this.txtDefaultCity.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtDefaultCity, null);
            // 
            // cmbDefaultCategoryS
            // 
            this.cmbDefaultCategoryS.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDefaultCategoryS.Location = new System.Drawing.Point(167, 108);
            this.cmbDefaultCategoryS.Name = "cmbDefaultCategoryS";
            this.cmbDefaultCategoryS.Size = new System.Drawing.Size(220, 40);
            this.cmbDefaultCategoryS.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.cmbDefaultCategoryS, null);
            this.cmbDefaultCategoryS.SelectedIndexChanged += new System.EventHandler(this.cmbDefaultCategoryS_SelectedIndexChanged);
            // 
            // cmbDefaultFrequency
            // 
            this.cmbDefaultFrequency.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDefaultFrequency.Location = new System.Drawing.Point(567, 158);
            this.cmbDefaultFrequency.Name = "cmbDefaultFrequency";
            this.cmbDefaultFrequency.Size = new System.Drawing.Size(173, 40);
            this.cmbDefaultFrequency.TabIndex = 23;
            this.ToolTip1.SetToolTip(this.cmbDefaultFrequency, null);
            this.cmbDefaultFrequency.SelectedIndexChanged += new System.EventHandler(this.cmbDefaultFrequency_SelectedIndexChanged);
            // 
            // cmbDefaultDigits
            // 
            this.cmbDefaultDigits.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDefaultDigits.Location = new System.Drawing.Point(567, 108);
            this.cmbDefaultDigits.Name = "cmbDefaultDigits";
            this.cmbDefaultDigits.Size = new System.Drawing.Size(173, 40);
            this.cmbDefaultDigits.TabIndex = 21;
            this.ToolTip1.SetToolTip(this.cmbDefaultDigits, null);
            this.cmbDefaultDigits.SelectedIndexChanged += new System.EventHandler(this.cmbDefaultDigits_SelectedIndexChanged);
            // 
            // cmbDefaultSize
            // 
            this.cmbDefaultSize.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDefaultSize.Location = new System.Drawing.Point(567, 58);
            this.cmbDefaultSize.Name = "cmbDefaultSize";
            this.cmbDefaultSize.Size = new System.Drawing.Size(173, 40);
            this.cmbDefaultSize.TabIndex = 19;
            this.ToolTip1.SetToolTip(this.cmbDefaultSize, null);
            this.cmbDefaultSize.SelectedIndexChanged += new System.EventHandler(this.cmbDefaultSize_SelectedIndexChanged);
            // 
            // cmbDefaultCategoryW
            // 
            this.cmbDefaultCategoryW.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDefaultCategoryW.Location = new System.Drawing.Point(167, 58);
            this.cmbDefaultCategoryW.Name = "cmbDefaultCategoryW";
            this.cmbDefaultCategoryW.Size = new System.Drawing.Size(220, 40);
            this.cmbDefaultCategoryW.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.cmbDefaultCategoryW, null);
            this.cmbDefaultCategoryW.SelectedIndexChanged += new System.EventHandler(this.cmbDefaultCategoryW_SelectedIndexChanged);
            // 
            // txtDefaultAccountW
            // 
            this.txtDefaultAccountW.Cols = 1;
            this.txtDefaultAccountW.ColumnHeadersVisible = false;
            this.txtDefaultAccountW.ExtendLastCol = true;
            this.txtDefaultAccountW.FixedCols = 0;
            this.txtDefaultAccountW.FixedRows = 0;
            this.txtDefaultAccountW.Location = new System.Drawing.Point(167, 158);
            this.txtDefaultAccountW.Name = "txtDefaultAccountW";
            this.txtDefaultAccountW.RowHeadersVisible = false;
            this.txtDefaultAccountW.Rows = 1;
            this.txtDefaultAccountW.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtDefaultAccountW.ShowFocusCell = false;
            this.txtDefaultAccountW.Size = new System.Drawing.Size(220, 40);
            this.txtDefaultAccountW.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtDefaultAccountW, null);
            // 
            // txtDefaultAccountS
            // 
            this.txtDefaultAccountS.Cols = 1;
            this.txtDefaultAccountS.ColumnHeadersVisible = false;
            this.txtDefaultAccountS.ExtendLastCol = true;
            this.txtDefaultAccountS.FixedCols = 0;
            this.txtDefaultAccountS.FixedRows = 0;
            this.txtDefaultAccountS.Location = new System.Drawing.Point(167, 208);
            this.txtDefaultAccountS.Name = "txtDefaultAccountS";
            this.txtDefaultAccountS.RowHeadersVisible = false;
            this.txtDefaultAccountS.Rows = 1;
            this.txtDefaultAccountS.ScrollBars = fecherFoundation.FCGrid.ScrollBarsSettings.flexScrollNone;
            this.txtDefaultAccountS.ShowFocusCell = false;
            this.txtDefaultAccountS.Size = new System.Drawing.Size(220, 40);
            this.txtDefaultAccountS.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtDefaultAccountS, null);
            // 
            // lblDefaultCity
            // 
            this.lblDefaultCity.Location = new System.Drawing.Point(20, 272);
            this.lblDefaultCity.Name = "lblDefaultCity";
            this.lblDefaultCity.Size = new System.Drawing.Size(85, 22);
            this.lblDefaultCity.TabIndex = 9;
            this.lblDefaultCity.Text = "CITY";
            this.ToolTip1.SetToolTip(this.lblDefaultCity, null);
            // 
            // lblDefaultState
            // 
            this.lblDefaultState.Location = new System.Drawing.Point(20, 322);
            this.lblDefaultState.Name = "lblDefaultState";
            this.lblDefaultState.Size = new System.Drawing.Size(85, 22);
            this.lblDefaultState.TabIndex = 11;
            this.lblDefaultState.Text = "STATE";
            this.ToolTip1.SetToolTip(this.lblDefaultState, null);
            // 
            // lblDefaultZip
            // 
            this.lblDefaultZip.Location = new System.Drawing.Point(20, 372);
            this.lblDefaultZip.Name = "lblDefaultZip";
            this.lblDefaultZip.Size = new System.Drawing.Size(85, 22);
            this.lblDefaultZip.TabIndex = 13;
            this.lblDefaultZip.Text = "ZIP";
            this.ToolTip1.SetToolTip(this.lblDefaultZip, null);
            // 
            // lblDefaultAccountW
            // 
            this.lblDefaultAccountW.Location = new System.Drawing.Point(20, 172);
            this.lblDefaultAccountW.Name = "lblDefaultAccountW";
            this.lblDefaultAccountW.Size = new System.Drawing.Size(113, 18);
            this.lblDefaultAccountW.TabIndex = 5;
            this.lblDefaultAccountW.Text = "WATER ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblDefaultAccountW, null);
            // 
            // lblDefaultAccountS
            // 
            this.lblDefaultAccountS.Location = new System.Drawing.Point(20, 222);
            this.lblDefaultAccountS.Name = "lblDefaultAccountS";
            this.lblDefaultAccountS.Size = new System.Drawing.Size(113, 18);
            this.lblDefaultAccountS.TabIndex = 7;
            this.lblDefaultAccountS.Text = "SEWER ACCOUNT";
            this.ToolTip1.SetToolTip(this.lblDefaultAccountS, null);
            // 
            // lblDefaultCategoryS
            // 
            this.lblDefaultCategoryS.Location = new System.Drawing.Point(20, 122);
            this.lblDefaultCategoryS.Name = "lblDefaultCategoryS";
            this.lblDefaultCategoryS.Size = new System.Drawing.Size(113, 18);
            this.lblDefaultCategoryS.TabIndex = 3;
            this.lblDefaultCategoryS.Text = "SEWER CATEGORY";
            this.ToolTip1.SetToolTip(this.lblDefaultCategoryS, null);
            // 
            // lblDefaultMeter
            // 
            this.lblDefaultMeter.Location = new System.Drawing.Point(370, 30);
            this.lblDefaultMeter.Name = "lblDefaultMeter";
            this.lblDefaultMeter.Size = new System.Drawing.Size(320, 18);
            this.lblDefaultMeter.TabIndex = 17;
            this.lblDefaultMeter.Text = "METER INFORMATION";
            this.lblDefaultMeter.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblDefaultMeter, null);
            // 
            // lblDDefaultFrequency
            // 
            this.lblDDefaultFrequency.Location = new System.Drawing.Point(420, 172);
            this.lblDDefaultFrequency.Name = "lblDDefaultFrequency";
            this.lblDDefaultFrequency.Size = new System.Drawing.Size(85, 22);
            this.lblDDefaultFrequency.TabIndex = 22;
            this.lblDDefaultFrequency.Text = "FREQUENCY";
            this.ToolTip1.SetToolTip(this.lblDDefaultFrequency, null);
            // 
            // lblDefaultDigits
            // 
            this.lblDefaultDigits.Location = new System.Drawing.Point(420, 122);
            this.lblDefaultDigits.Name = "lblDefaultDigits";
            this.lblDefaultDigits.Size = new System.Drawing.Size(85, 22);
            this.lblDefaultDigits.TabIndex = 20;
            this.lblDefaultDigits.Text = "DIGITS";
            this.ToolTip1.SetToolTip(this.lblDefaultDigits, null);
            // 
            // lblDefaultSize
            // 
            this.lblDefaultSize.Location = new System.Drawing.Point(420, 72);
            this.lblDefaultSize.Name = "lblDefaultSize";
            this.lblDefaultSize.Size = new System.Drawing.Size(85, 22);
            this.lblDefaultSize.TabIndex = 18;
            this.lblDefaultSize.Text = "SIZE";
            this.ToolTip1.SetToolTip(this.lblDefaultSize, null);
            // 
            // lblDefaultAccount
            // 
            this.lblDefaultAccount.Location = new System.Drawing.Point(20, 30);
            this.lblDefaultAccount.Name = "lblDefaultAccount";
            this.lblDefaultAccount.Size = new System.Drawing.Size(320, 18);
            this.lblDefaultAccount.TabIndex = 24;
            this.lblDefaultAccount.Text = "ACCOUNT INFORMATION";
            this.lblDefaultAccount.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblDefaultAccount, null);
            // 
            // lblDefaultCategoryW
            // 
            this.lblDefaultCategoryW.Location = new System.Drawing.Point(20, 72);
            this.lblDefaultCategoryW.Name = "lblDefaultCategoryW";
            this.lblDefaultCategoryW.Size = new System.Drawing.Size(113, 18);
            this.lblDefaultCategoryW.TabIndex = 1;
            this.lblDefaultCategoryW.Text = "WATER CATEGORY";
            this.ToolTip1.SetToolTip(this.lblDefaultCategoryW, null);
            // 
            // fraBilling
            // 
            this.fraBilling.AppearanceKey = "groupBoxLeftBorder";
            this.fraBilling.Controls.Add(this.fraConsIncrease);
            this.fraBilling.Controls.Add(this.chkDefDisplayHistory);
            this.fraBilling.Controls.Add(this.chkBillStormwaterFee);
            this.fraBilling.Controls.Add(this.cmbDEType);
            this.fraBilling.Controls.Add(this.chkLimitConsBill);
            this.fraBilling.Controls.Add(this.cboApplyTo);
            this.fraBilling.Controls.Add(this.chkAutoPrepayments);
            this.fraBilling.Controls.Add(this.chkExportNoBillRecords);
            this.fraBilling.Controls.Add(this.chkStopOnConsumption);
            this.fraBilling.Controls.Add(this.cmbDEOrder);
            this.fraBilling.Controls.Add(this.lblApplyTo);
            this.fraBilling.Controls.Add(this.lblDEType);
            this.fraBilling.Controls.Add(this.lblDEOrder);
            this.fraBilling.Location = new System.Drawing.Point(30, 122);
            this.fraBilling.Name = "fraBilling";
            this.fraBilling.Size = new System.Drawing.Size(507, 553);
            this.fraBilling.TabIndex = 9;
            this.fraBilling.Text = "Billing Options";
            this.ToolTip1.SetToolTip(this.fraBilling, null);
            this.fraBilling.Visible = false;
            // 
            // fraConsIncrease
            // 
            this.fraConsIncrease.Controls.Add(this.chkMinVarWarning);
            this.fraConsIncrease.Controls.Add(this.txtConsIncrease);
            this.fraConsIncrease.Controls.Add(this.lblConsIncrease_1);
            this.fraConsIncrease.Controls.Add(this.lblConsIncrease_0);
            this.fraConsIncrease.Location = new System.Drawing.Point(20, 259);
            this.fraConsIncrease.Name = "fraConsIncrease";
            this.fraConsIncrease.Size = new System.Drawing.Size(467, 127);
            this.fraConsIncrease.TabIndex = 6;
            this.fraConsIncrease.Text = "Consumption Variance Warning";
            this.ToolTip1.SetToolTip(this.fraConsIncrease, null);
            // 
            // chkMinVarWarning
            // 
            this.chkMinVarWarning.Location = new System.Drawing.Point(20, 30);
            this.chkMinVarWarning.Name = "chkMinVarWarning";
            this.chkMinVarWarning.Size = new System.Drawing.Size(352, 26);
            this.chkMinVarWarning.Text = "Hide warning when consumption is below the minimum";
            this.ToolTip1.SetToolTip(this.chkMinVarWarning, "In Date Entry, stop on the consumption field");
            this.chkMinVarWarning.CheckedChanged += new System.EventHandler(this.chkMinVarWarning_CheckedChanged);
            // 
            // txtConsIncrease
            // 
            this.txtConsIncrease.BackColor = System.Drawing.SystemColors.Window;
            this.txtConsIncrease.Location = new System.Drawing.Point(149, 67);
            this.txtConsIncrease.Name = "txtConsIncrease";
            this.txtConsIncrease.Size = new System.Drawing.Size(71, 40);
            this.txtConsIncrease.TabIndex = 2;
            this.txtConsIncrease.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.ToolTip1.SetToolTip(this.txtConsIncrease, null);
            this.txtConsIncrease.Enter += new System.EventHandler(this.txtConsIncrease_Enter);
            this.txtConsIncrease.TextChanged += new System.EventHandler(this.txtConsIncrease_TextChanged);
            this.txtConsIncrease.Validating += new System.ComponentModel.CancelEventHandler(this.txtConsIncrease_Validating);
            this.txtConsIncrease.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtConsIncrease_KeyPress);
            // 
            // lblConsIncrease_1
            // 
            this.lblConsIncrease_1.Location = new System.Drawing.Point(237, 81);
            this.lblConsIncrease_1.Name = "lblConsIncrease_1";
            this.lblConsIncrease_1.Size = new System.Drawing.Size(89, 18);
            this.lblConsIncrease_1.TabIndex = 3;
            this.lblConsIncrease_1.Text = "% CHANGE";
            this.ToolTip1.SetToolTip(this.lblConsIncrease_1, null);
            // 
            // lblConsIncrease_0
            // 
            this.lblConsIncrease_0.Location = new System.Drawing.Point(20, 81);
            this.lblConsIncrease_0.Name = "lblConsIncrease_0";
            this.lblConsIncrease_0.Size = new System.Drawing.Size(121, 18);
            this.lblConsIncrease_0.TabIndex = 1;
            this.lblConsIncrease_0.Text = "ISSUE WARNING AT";
            this.ToolTip1.SetToolTip(this.lblConsIncrease_0, null);
            // 
            // chkDefDisplayHistory
            // 
            this.chkDefDisplayHistory.Location = new System.Drawing.Point(20, 223);
            this.chkDefDisplayHistory.Name = "chkDefDisplayHistory";
            this.chkDefDisplayHistory.Size = new System.Drawing.Size(179, 26);
            this.chkDefDisplayHistory.TabIndex = 6;
            this.chkDefDisplayHistory.Text = "Display History by Default";
            this.ToolTip1.SetToolTip(this.chkDefDisplayHistory, "In Date Entry, stop on the consumption field.");
            // 
            // chkBillStormwaterFee
            // 
            this.chkBillStormwaterFee.Location = new System.Drawing.Point(20, 520);
            this.chkBillStormwaterFee.Name = "chkBillStormwaterFee";
            this.chkBillStormwaterFee.Size = new System.Drawing.Size(143, 26);
            this.chkBillStormwaterFee.TabIndex = 11;
            this.chkBillStormwaterFee.Text = "Bill Stormwater Fee";
            this.ToolTip1.SetToolTip(this.chkBillStormwaterFee, null);
            this.chkBillStormwaterFee.CheckedChanged += new System.EventHandler(this.chkBillStormwaterFee_CheckedChanged);
            // 
            // chkLimitConsBill
            // 
            this.chkLimitConsBill.Enabled = false;
            this.chkLimitConsBill.Location = new System.Drawing.Point(20, 483);
            this.chkLimitConsBill.Name = "chkLimitConsBill";
            this.chkLimitConsBill.Size = new System.Drawing.Size(206, 26);
            this.chkLimitConsBill.TabIndex = 10;
            this.chkLimitConsBill.Text = "Limit First Consumption Billing";
            this.ToolTip1.SetToolTip(this.chkLimitConsBill, null);
            this.chkLimitConsBill.Visible = false;
            // 
            // cboApplyTo
            // 
            this.cboApplyTo.BackColor = System.Drawing.SystemColors.Window;
            this.cboApplyTo.Enabled = false;
            this.cboApplyTo.Location = new System.Drawing.Point(213, 433);
            this.cboApplyTo.Name = "cboApplyTo";
            this.cboApplyTo.Size = new System.Drawing.Size(171, 40);
            this.cboApplyTo.TabIndex = 9;
            this.ToolTip1.SetToolTip(this.cboApplyTo, null);
            this.cboApplyTo.TextChanged += new System.EventHandler(this.cboApplyTo_TextChanged);
            // 
            // chkAutoPrepayments
            // 
            this.chkAutoPrepayments.Location = new System.Drawing.Point(20, 396);
            this.chkAutoPrepayments.Name = "chkAutoPrepayments";
            this.chkAutoPrepayments.Size = new System.Drawing.Size(364, 26);
            this.chkAutoPrepayments.TabIndex = 7;
            this.chkAutoPrepayments.Text = "Automatically Create Pre-Payments with Account Credits";
            this.ToolTip1.SetToolTip(this.chkAutoPrepayments, "In Date Entry, stop on the consumption field.");
            this.chkAutoPrepayments.CheckedChanged += new System.EventHandler(this.chkAutoPrepayments_CheckedChanged);
            // 
            // chkExportNoBillRecords
            // 
            this.chkExportNoBillRecords.Location = new System.Drawing.Point(20, 67);
            this.chkExportNoBillRecords.Name = "chkExportNoBillRecords";
            this.chkExportNoBillRecords.Size = new System.Drawing.Size(292, 26);
            this.chkExportNoBillRecords.TabIndex = 1;
            this.chkExportNoBillRecords.Text = "Export No Bill meters to the electronic reader";
            this.ToolTip1.SetToolTip(this.chkExportNoBillRecords, "In Date Entry, stop on the consumption field.");
            this.chkExportNoBillRecords.CheckedChanged += new System.EventHandler(this.chkExportNoBillRecords_CheckedChanged);
            // 
            // chkStopOnConsumption
            // 
            this.chkStopOnConsumption.Location = new System.Drawing.Point(20, 30);
            this.chkStopOnConsumption.Name = "chkStopOnConsumption";
            this.chkStopOnConsumption.Size = new System.Drawing.Size(158, 26);
            this.chkStopOnConsumption.TabIndex = 12;
            this.chkStopOnConsumption.Text = "Stop On Consumption";
            this.ToolTip1.SetToolTip(this.chkStopOnConsumption, "In Date Entry, stop on the consumption field.");
            this.chkStopOnConsumption.CheckedChanged += new System.EventHandler(this.chkStopOnConsumption_CheckedChanged);
            // 
            // cmbDEOrder
            // 
            this.cmbDEOrder.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDEOrder.Location = new System.Drawing.Point(213, 104);
            this.cmbDEOrder.Name = "cmbDEOrder";
            this.cmbDEOrder.Size = new System.Drawing.Size(171, 40);
            this.cmbDEOrder.TabIndex = 3;
            this.ToolTip1.SetToolTip(this.cmbDEOrder, null);
            this.cmbDEOrder.SelectedIndexChanged += new System.EventHandler(this.cmbDEOrder_SelectedIndexChanged);
            // 
            // lblApplyTo
            // 
            this.lblApplyTo.Enabled = false;
            this.lblApplyTo.Location = new System.Drawing.Point(20, 447);
            this.lblApplyTo.Name = "lblApplyTo";
            this.lblApplyTo.Size = new System.Drawing.Size(160, 18);
            this.lblApplyTo.TabIndex = 8;
            this.lblApplyTo.Text = "APPLY PRE-PAYMENTS TO";
            this.ToolTip1.SetToolTip(this.lblApplyTo, null);
            // 
            // lblDEType
            // 
            this.lblDEType.Location = new System.Drawing.Point(20, 168);
            this.lblDEType.Name = "lblDEType";
            this.lblDEType.Size = new System.Drawing.Size(150, 18);
            this.lblDEType.TabIndex = 4;
            this.lblDEType.Text = "DATA ENTRY TYPE";
            this.ToolTip1.SetToolTip(this.lblDEType, null);
            // 
            // lblDEOrder
            // 
            this.lblDEOrder.Location = new System.Drawing.Point(20, 118);
            this.lblDEOrder.Name = "lblDEOrder";
            this.lblDEOrder.Size = new System.Drawing.Size(160, 18);
            this.lblDEOrder.TabIndex = 2;
            this.lblDEOrder.Text = "DATA ENTRY SEQ";
            this.ToolTip1.SetToolTip(this.lblDEOrder, null);
            // 
            // fraBillDetDesc
            // 
            this.fraBillDetDesc.Controls.Add(this.txtAdj);
            this.fraBillDetDesc.Controls.Add(this.txtMisc);
            this.fraBillDetDesc.Controls.Add(this.txtTax);
            this.fraBillDetDesc.Controls.Add(this.txtLiens);
            this.fraBillDetDesc.Controls.Add(this.txtInterest);
            this.fraBillDetDesc.Controls.Add(this.txtOverride);
            this.fraBillDetDesc.Controls.Add(this.txtRegular);
            this.fraBillDetDesc.Controls.Add(this.txtCredit);
            this.fraBillDetDesc.Controls.Add(this.txtPastDue);
            this.fraBillDetDesc.Controls.Add(this.lblAdj);
            this.fraBillDetDesc.Controls.Add(this.lblMisc);
            this.fraBillDetDesc.Controls.Add(this.lblTax);
            this.fraBillDetDesc.Controls.Add(this.lblLiens);
            this.fraBillDetDesc.Controls.Add(this.lblInterest);
            this.fraBillDetDesc.Controls.Add(this.lblOverride);
            this.fraBillDetDesc.Controls.Add(this.lblRegular);
            this.fraBillDetDesc.Controls.Add(this.lblCredit);
            this.fraBillDetDesc.Controls.Add(this.lblPastDue);
            this.fraBillDetDesc.Controls.Add(this.Label12);
            this.fraBillDetDesc.Location = new System.Drawing.Point(30, 122);
            this.fraBillDetDesc.Name = "fraBillDetDesc";
            this.fraBillDetDesc.Size = new System.Drawing.Size(425, 540);
            this.fraBillDetDesc.TabIndex = 10;
            this.fraBillDetDesc.Text = "Bill Detail Descriptions";
            this.ToolTip1.SetToolTip(this.fraBillDetDesc, null);
            this.fraBillDetDesc.Visible = false;
            // 
            // txtAdj
            // 
            this.txtAdj.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdj.Location = new System.Drawing.Point(140, 458);
            this.txtAdj.Name = "txtAdj";
            this.txtAdj.Size = new System.Drawing.Size(202, 40);
            this.txtAdj.TabIndex = 18;
            this.ToolTip1.SetToolTip(this.txtAdj, null);
            // 
            // txtMisc
            // 
            this.txtMisc.BackColor = System.Drawing.SystemColors.Window;
            this.txtMisc.Location = new System.Drawing.Point(140, 408);
            this.txtMisc.Name = "txtMisc";
            this.txtMisc.Size = new System.Drawing.Size(202, 40);
            this.txtMisc.TabIndex = 16;
            this.ToolTip1.SetToolTip(this.txtMisc, null);
            // 
            // txtTax
            // 
            this.txtTax.BackColor = System.Drawing.SystemColors.Window;
            this.txtTax.Location = new System.Drawing.Point(140, 358);
            this.txtTax.Name = "txtTax";
            this.txtTax.Size = new System.Drawing.Size(202, 40);
            this.txtTax.TabIndex = 14;
            this.ToolTip1.SetToolTip(this.txtTax, null);
            // 
            // txtLiens
            // 
            this.txtLiens.BackColor = System.Drawing.SystemColors.Window;
            this.txtLiens.Location = new System.Drawing.Point(140, 308);
            this.txtLiens.Name = "txtLiens";
            this.txtLiens.Size = new System.Drawing.Size(202, 40);
            this.txtLiens.TabIndex = 12;
            this.ToolTip1.SetToolTip(this.txtLiens, null);
            // 
            // txtInterest
            // 
            this.txtInterest.BackColor = System.Drawing.SystemColors.Window;
            this.txtInterest.Location = new System.Drawing.Point(140, 258);
            this.txtInterest.Name = "txtInterest";
            this.txtInterest.Size = new System.Drawing.Size(202, 40);
            this.txtInterest.TabIndex = 10;
            this.ToolTip1.SetToolTip(this.txtInterest, null);
            // 
            // txtOverride
            // 
            this.txtOverride.BackColor = System.Drawing.SystemColors.Window;
            this.txtOverride.Location = new System.Drawing.Point(140, 208);
            this.txtOverride.Name = "txtOverride";
            this.txtOverride.Size = new System.Drawing.Size(202, 40);
            this.txtOverride.TabIndex = 8;
            this.ToolTip1.SetToolTip(this.txtOverride, null);
            // 
            // txtRegular
            // 
            this.txtRegular.BackColor = System.Drawing.SystemColors.Window;
            this.txtRegular.Location = new System.Drawing.Point(140, 158);
            this.txtRegular.Name = "txtRegular";
            this.txtRegular.Size = new System.Drawing.Size(202, 40);
            this.txtRegular.TabIndex = 6;
            this.ToolTip1.SetToolTip(this.txtRegular, null);
            // 
            // txtCredit
            // 
            this.txtCredit.BackColor = System.Drawing.SystemColors.Window;
            this.txtCredit.Location = new System.Drawing.Point(140, 108);
            this.txtCredit.Name = "txtCredit";
            this.txtCredit.Size = new System.Drawing.Size(202, 40);
            this.txtCredit.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.txtCredit, null);
            // 
            // txtPastDue
            // 
            this.txtPastDue.BackColor = System.Drawing.SystemColors.Window;
            this.txtPastDue.Location = new System.Drawing.Point(140, 58);
            this.txtPastDue.Name = "txtPastDue";
            this.txtPastDue.Size = new System.Drawing.Size(202, 40);
            this.txtPastDue.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.txtPastDue, null);
            // 
            // lblAdj
            // 
            this.lblAdj.Location = new System.Drawing.Point(20, 472);
            this.lblAdj.Name = "lblAdj";
            this.lblAdj.Size = new System.Drawing.Size(85, 18);
            this.lblAdj.TabIndex = 17;
            this.lblAdj.Text = "ADJUSTMENT";
            this.ToolTip1.SetToolTip(this.lblAdj, null);
            // 
            // lblMisc
            // 
            this.lblMisc.Location = new System.Drawing.Point(20, 422);
            this.lblMisc.Name = "lblMisc";
            this.lblMisc.Size = new System.Drawing.Size(65, 18);
            this.lblMisc.TabIndex = 15;
            this.lblMisc.Text = "MISC";
            this.ToolTip1.SetToolTip(this.lblMisc, null);
            // 
            // lblTax
            // 
            this.lblTax.Location = new System.Drawing.Point(20, 372);
            this.lblTax.Name = "lblTax";
            this.lblTax.Size = new System.Drawing.Size(65, 18);
            this.lblTax.TabIndex = 13;
            this.lblTax.Text = "TAX";
            this.ToolTip1.SetToolTip(this.lblTax, null);
            // 
            // lblLiens
            // 
            this.lblLiens.Location = new System.Drawing.Point(20, 322);
            this.lblLiens.Name = "lblLiens";
            this.lblLiens.Size = new System.Drawing.Size(65, 18);
            this.lblLiens.TabIndex = 11;
            this.lblLiens.Text = "LIENS";
            this.ToolTip1.SetToolTip(this.lblLiens, null);
            // 
            // lblInterest
            // 
            this.lblInterest.Location = new System.Drawing.Point(20, 272);
            this.lblInterest.Name = "lblInterest";
            this.lblInterest.Size = new System.Drawing.Size(65, 18);
            this.lblInterest.TabIndex = 9;
            this.lblInterest.Text = "INTEREST";
            this.ToolTip1.SetToolTip(this.lblInterest, null);
            // 
            // lblOverride
            // 
            this.lblOverride.Location = new System.Drawing.Point(20, 222);
            this.lblOverride.Name = "lblOverride";
            this.lblOverride.Size = new System.Drawing.Size(65, 18);
            this.lblOverride.TabIndex = 7;
            this.lblOverride.Text = "OVERRIDE";
            this.ToolTip1.SetToolTip(this.lblOverride, null);
            // 
            // lblRegular
            // 
            this.lblRegular.Location = new System.Drawing.Point(20, 172);
            this.lblRegular.Name = "lblRegular";
            this.lblRegular.Size = new System.Drawing.Size(65, 18);
            this.lblRegular.TabIndex = 5;
            this.lblRegular.Text = "REGULAR";
            this.ToolTip1.SetToolTip(this.lblRegular, null);
            // 
            // lblCredit
            // 
            this.lblCredit.Location = new System.Drawing.Point(20, 122);
            this.lblCredit.Name = "lblCredit";
            this.lblCredit.Size = new System.Drawing.Size(65, 18);
            this.lblCredit.TabIndex = 3;
            this.lblCredit.Text = "CREDIT";
            this.ToolTip1.SetToolTip(this.lblCredit, null);
            // 
            // lblPastDue
            // 
            this.lblPastDue.Location = new System.Drawing.Point(20, 72);
            this.lblPastDue.Name = "lblPastDue";
            this.lblPastDue.Size = new System.Drawing.Size(65, 18);
            this.lblPastDue.TabIndex = 1;
            this.lblPastDue.Text = "PAST DUE";
            this.ToolTip1.SetToolTip(this.lblPastDue, null);
            // 
            // Label12
            // 
            this.Label12.Location = new System.Drawing.Point(20, 30);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(370, 18);
            this.Label12.TabIndex = 19;
            this.Label12.Text = "REPLACE THE DEFAULT DETAIL DESCRIPTIONS ON SEWER BILLS";
            this.ToolTip1.SetToolTip(this.Label12, null);
            // 
            // fraDefaultAnalysisReports
            // 
            this.fraDefaultAnalysisReports.AppearanceKey = "groupBoxLeftBorder";
            this.fraDefaultAnalysisReports.Controls.Add(this.chkSalesTaxS);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkSalesTaxW);
            this.fraDefaultAnalysisReports.Controls.Add(this.fraReportSeq);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkMeterReportW);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkBillCountW);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkConsumptionsW);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkDollarAmountsW);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkBillingSummary);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkDollarAmountsS);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkConsumptionsS);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkBillCountS);
            this.fraDefaultAnalysisReports.Controls.Add(this.chkMeterReportS);
            this.fraDefaultAnalysisReports.Controls.Add(this.lblWaterHeader);
            this.fraDefaultAnalysisReports.Controls.Add(this.lblSewerHeader);
            this.fraDefaultAnalysisReports.Location = new System.Drawing.Point(30, 122);
            this.fraDefaultAnalysisReports.Name = "fraDefaultAnalysisReports";
            this.fraDefaultAnalysisReports.Size = new System.Drawing.Size(377, 390);
            this.fraDefaultAnalysisReports.TabIndex = 11;
            this.fraDefaultAnalysisReports.Text = "Default Reports";
            this.ToolTip1.SetToolTip(this.fraDefaultAnalysisReports, null);
            this.fraDefaultAnalysisReports.Visible = false;
            // 
            // chkSalesTaxS
            // 
            this.chkSalesTaxS.Location = new System.Drawing.Point(190, 243);
            this.chkSalesTaxS.Name = "chkSalesTaxS";
            this.chkSalesTaxS.Size = new System.Drawing.Size(129, 26);
            this.chkSalesTaxS.TabIndex = 12;
            this.chkSalesTaxS.Text = "Sales Tax Report";
            this.ToolTip1.SetToolTip(this.chkSalesTaxS, null);
            this.chkSalesTaxS.CheckedChanged += new System.EventHandler(this.chkSalesTaxS_CheckedChanged);
            // 
            // chkSalesTaxW
            // 
            this.chkSalesTaxW.Location = new System.Drawing.Point(20, 243);
            this.chkSalesTaxW.Name = "chkSalesTaxW";
            this.chkSalesTaxW.Size = new System.Drawing.Size(129, 26);
            this.chkSalesTaxW.TabIndex = 11;
            this.chkSalesTaxW.Text = "Sales Tax Report";
            this.ToolTip1.SetToolTip(this.chkSalesTaxW, null);
            this.chkSalesTaxW.CheckedChanged += new System.EventHandler(this.chkSalesTaxW_CheckedChanged);
            // 
            // fraReportSeq
            // 
            this.fraReportSeq.Controls.Add(this.lblBillingRepSeq);
            this.fraReportSeq.Controls.Add(this.cmbBillingRepSeq);
            this.fraReportSeq.Location = new System.Drawing.Point(20, 280);
            this.fraReportSeq.Name = "fraReportSeq";
            this.fraReportSeq.Size = new System.Drawing.Size(355, 90);
            this.fraReportSeq.TabIndex = 13;
            this.fraReportSeq.Text = "Billing Report Sequence";
            this.ToolTip1.SetToolTip(this.fraReportSeq, null);
            // 
            // lblBillingRepSeq
            // 
            this.lblBillingRepSeq.Location = new System.Drawing.Point(20, 44);
            this.lblBillingRepSeq.Name = "lblBillingRepSeq";
            this.lblBillingRepSeq.Size = new System.Drawing.Size(90, 18);
            this.lblBillingRepSeq.Text = "SEQUENCE BY";
            this.ToolTip1.SetToolTip(this.lblBillingRepSeq, null);
            // 
            // chkMeterReportW
            // 
            this.chkMeterReportW.Location = new System.Drawing.Point(20, 206);
            this.chkMeterReportW.Name = "chkMeterReportW";
            this.chkMeterReportW.Size = new System.Drawing.Size(104, 26);
            this.chkMeterReportW.TabIndex = 9;
            this.chkMeterReportW.Text = "Meter Report";
            this.ToolTip1.SetToolTip(this.chkMeterReportW, null);
            this.chkMeterReportW.CheckedChanged += new System.EventHandler(this.chkMeterReportW_CheckedChanged);
            // 
            // chkBillCountW
            // 
            this.chkBillCountW.Location = new System.Drawing.Point(20, 169);
            this.chkBillCountW.Name = "chkBillCountW";
            this.chkBillCountW.Size = new System.Drawing.Size(83, 26);
            this.chkBillCountW.TabIndex = 7;
            this.chkBillCountW.Text = "Bill Count";
            this.ToolTip1.SetToolTip(this.chkBillCountW, null);
            this.chkBillCountW.CheckedChanged += new System.EventHandler(this.chkBillCountW_CheckedChanged);
            // 
            // chkConsumptionsW
            // 
            this.chkConsumptionsW.Location = new System.Drawing.Point(20, 132);
            this.chkConsumptionsW.Name = "chkConsumptionsW";
            this.chkConsumptionsW.Size = new System.Drawing.Size(111, 26);
            this.chkConsumptionsW.TabIndex = 5;
            this.chkConsumptionsW.Text = "Consumptions";
            this.ToolTip1.SetToolTip(this.chkConsumptionsW, null);
            this.chkConsumptionsW.CheckedChanged += new System.EventHandler(this.chkConsumptionsW_CheckedChanged);
            // 
            // chkDollarAmountsW
            // 
            this.chkDollarAmountsW.Location = new System.Drawing.Point(20, 95);
            this.chkDollarAmountsW.Name = "chkDollarAmountsW";
            this.chkDollarAmountsW.Size = new System.Drawing.Size(117, 26);
            this.chkDollarAmountsW.TabIndex = 3;
            this.chkDollarAmountsW.Text = "Dollar Amounts";
            this.ToolTip1.SetToolTip(this.chkDollarAmountsW, null);
            this.chkDollarAmountsW.CheckedChanged += new System.EventHandler(this.chkDollarAmountsW_CheckedChanged);
            // 
            // chkBillingSummary
            // 
            this.chkBillingSummary.Location = new System.Drawing.Point(20, 30);
            this.chkBillingSummary.Name = "chkBillingSummary";
            this.chkBillingSummary.Size = new System.Drawing.Size(132, 26);
            this.chkBillingSummary.TabIndex = 14;
            this.chkBillingSummary.Text = "Billing Edit Report";
            this.ToolTip1.SetToolTip(this.chkBillingSummary, null);
            this.chkBillingSummary.CheckedChanged += new System.EventHandler(this.chkBillingSummary_CheckedChanged);
            // 
            // chkDollarAmountsS
            // 
            this.chkDollarAmountsS.Location = new System.Drawing.Point(190, 95);
            this.chkDollarAmountsS.Name = "chkDollarAmountsS";
            this.chkDollarAmountsS.Size = new System.Drawing.Size(117, 26);
            this.chkDollarAmountsS.TabIndex = 4;
            this.chkDollarAmountsS.Text = "Dollar Amounts";
            this.ToolTip1.SetToolTip(this.chkDollarAmountsS, null);
            this.chkDollarAmountsS.CheckedChanged += new System.EventHandler(this.chkDollarAmountsS_CheckedChanged);
            // 
            // chkConsumptionsS
            // 
            this.chkConsumptionsS.Location = new System.Drawing.Point(190, 132);
            this.chkConsumptionsS.Name = "chkConsumptionsS";
            this.chkConsumptionsS.Size = new System.Drawing.Size(111, 26);
            this.chkConsumptionsS.TabIndex = 6;
            this.chkConsumptionsS.Text = "Consumptions";
            this.ToolTip1.SetToolTip(this.chkConsumptionsS, null);
            this.chkConsumptionsS.CheckedChanged += new System.EventHandler(this.chkConsumptionsS_CheckedChanged);
            // 
            // chkBillCountS
            // 
            this.chkBillCountS.Location = new System.Drawing.Point(190, 169);
            this.chkBillCountS.Name = "chkBillCountS";
            this.chkBillCountS.Size = new System.Drawing.Size(83, 26);
            this.chkBillCountS.TabIndex = 8;
            this.chkBillCountS.Text = "Bill Count";
            this.ToolTip1.SetToolTip(this.chkBillCountS, null);
            this.chkBillCountS.CheckedChanged += new System.EventHandler(this.chkBillCountS_CheckedChanged);
            // 
            // chkMeterReportS
            // 
            this.chkMeterReportS.Location = new System.Drawing.Point(190, 206);
            this.chkMeterReportS.Name = "chkMeterReportS";
            this.chkMeterReportS.Size = new System.Drawing.Size(104, 26);
            this.chkMeterReportS.TabIndex = 10;
            this.chkMeterReportS.Text = "Meter Report";
            this.ToolTip1.SetToolTip(this.chkMeterReportS, null);
            this.chkMeterReportS.CheckedChanged += new System.EventHandler(this.chkMeterReportS_CheckedChanged);
            // 
            // lblWaterHeader
            // 
            this.lblWaterHeader.Location = new System.Drawing.Point(20, 67);
            this.lblWaterHeader.Name = "lblWaterHeader";
            this.lblWaterHeader.Size = new System.Drawing.Size(113, 18);
            this.lblWaterHeader.TabIndex = 1;
            this.lblWaterHeader.Text = "WATER";
            this.lblWaterHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblWaterHeader, null);
            // 
            // lblSewerHeader
            // 
            this.lblSewerHeader.Location = new System.Drawing.Point(190, 67);
            this.lblSewerHeader.Name = "lblSewerHeader";
            this.lblSewerHeader.Size = new System.Drawing.Size(113, 18);
            this.lblSewerHeader.TabIndex = 2;
            this.lblSewerHeader.Text = "SEWER";
            this.lblSewerHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ToolTip1.SetToolTip(this.lblSewerHeader, null);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSave,
            this.mnuFileSaveExit,
            this.Seperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 0;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSaveExit
            // 
            this.mnuFileSaveExit.Index = 1;
            this.mnuFileSaveExit.Name = "mnuFileSaveExit";
            this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveExit.Text = "Save & Exit";
            this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 3;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(345, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmUTCustomize
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(898, 688);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmUTCustomize";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Customize";
            this.ToolTip1.SetToolTip(this, null);
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmUTCustomize_Load);
            this.Activated += new System.EventHandler(this.frmUTCustomize_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTCustomize_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraBillOptions)).EndInit();
            this.fraBillOptions.ResumeLayout(false);
            this.fraBillOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSendCopies)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclChangeOutCons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowLienAmtOnBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkChpt660)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraTaxRate)).EndInit();
            this.fraTaxRate.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinAcctS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxAcctS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPrinAcctW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTaxAcctW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraAdjustments)).EndInit();
            this.fraAdjustments.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraPaymentOptions)).EndInit();
            this.fraPaymentOptions.ResumeLayout(false);
            this.fraPaymentOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSkipLDNPrompt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDisableAutoPmtFill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultUTRTDDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraInterest)).EndInit();
            this.fraInterest.ResumeLayout(false);
            this.fraInterest.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkTACheckRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkTAUpdateRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowTaxAcquired)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseBookForLiens)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkOnlyCheckOldestLien)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInterest_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraOther)).EndInit();
            this.fraOther.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraReturnAddress)).EndInit();
            this.fraReturnAddress.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsReturnAddress)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraMultiplier)).EndInit();
            this.fraMultiplier.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraTownService)).EndInit();
            this.fraTownService.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraRemoteReader)).EndInit();
            this.fraRemoteReader.ResumeLayout(false);
            this.fraRemoteReader.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkForceReadDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReportSequence)).EndInit();
            this.fraReportSequence.ResumeLayout(false);
            this.fraReportSequence.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMapLot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraIConnect)).EndInit();
            this.fraIConnect.ResumeLayout(false);
            this.fraIConnect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSetupICAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseCRAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMFeeAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultOptions)).EndInit();
            this.fraDefaultOptions.ResumeLayout(false);
            this.fraDefaultOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultMHInfo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefaultRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccountW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDefaultAccountS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBilling)).EndInit();
            this.fraBilling.ResumeLayout(false);
            this.fraBilling.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraConsIncrease)).EndInit();
            this.fraConsIncrease.ResumeLayout(false);
            this.fraConsIncrease.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkMinVarWarning)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDefDisplayHistory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillStormwaterFee)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkLimitConsBill)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkAutoPrepayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExportNoBillRecords)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkStopOnConsumption)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraBillDetDesc)).EndInit();
            this.fraBillDetDesc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultAnalysisReports)).EndInit();
            this.fraDefaultAnalysisReports.ResumeLayout(false);
            this.fraDefaultAnalysisReports.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReportSeq)).EndInit();
            this.fraReportSeq.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkMeterReportW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillCountW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsumptionsW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDollarAmountsW)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillingSummary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDollarAmountsS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkConsumptionsS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkBillCountS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkMeterReportS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
        public FCCheckBox chkSendCopies;
        private FCTextBox txtVFlexSiteCode;
        public FCCheckBox chkDefDisplayHistory;
    }
}
