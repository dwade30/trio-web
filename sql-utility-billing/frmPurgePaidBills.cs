﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmPurgePaidBills.
	/// </summary>
	public partial class frmPurgePaidBills : BaseForm
	{
		public frmPurgePaidBills()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmPurgePaidBills InstancePtr
		{
			get
			{
				return (frmPurgePaidBills)Sys.GetInstance(typeof(frmPurgePaidBills));
			}
		}

		protected frmPurgePaidBills _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               10/19/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/20/2006              *
		// ********************************************************
		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			mnuProcessQuit_Click();
		}

		private void cmdPurge_Click(object sender, System.EventArgs e)
		{
			DateTime tempdate;
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			if (!Information.IsDate(txtDate.Text))
			{
				MessageBox.Show("This is not a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else if (DateAndTime.DateValue(txtDate.Text).ToOADate() > (DateAndTime.DateAdd("yyyy", -1, DateTime.Today)).ToOADate())
			{
				MessageBox.Show("You must enter a date that is at least a year in the past.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			tempdate = DateAndTime.DateValue(txtDate.Text);
			ans = MessageBox.Show("All bills created on or before the date of " + Strings.Format(tempdate, "MM/dd/yyyy") + " which have been paid off will be deleted.  Do you wish to continue?", "Delete Records?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
			if (ans == DialogResult.Yes)
			{
				// CYA Entry
				modGlobalFunctions.AddCYAEntry_8("UT", "Purged Paid Bills routine run for the date of " + FCConvert.ToString(tempdate));
				frmWait.InstancePtr.Show();
				frmWait.InstancePtr.lblMessage.Text = "Please Wait..." + "\r\n" + "Deleting Records";
				frmWait.InstancePtr.Refresh();
				//FC:FINAL:DDU:#1194 - close this form to remain just the report
				//FC:FINAL:CHN - issue #1213: Error during process.
				// this.Close();
				// if use close here, form will be disposed and entered data will be lose
				frmReportViewer.InstancePtr.Init(rptPurgePaidBills.InstancePtr);
			}
		}

		public void cmdPurge_Click()
		{
			cmdPurge_Click(cmdPurge, new System.EventArgs());
		}

		private void frmPurgePaidBills_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmPurgePaidBills_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmPurgePaidBills properties;
			//frmPurgePaidBills.FillStyle	= 0;
			//frmPurgePaidBills.ScaleWidth	= 3885;
			//frmPurgePaidBills.ScaleHeight	= 2445;
			//frmPurgePaidBills.LinkTopic	= "Form2";
			//frmPurgePaidBills.LockControls	= true;
			//frmPurgePaidBills.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strMessage;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
			txtDate.ToolTipText = "All paid bills created on or before this date will be purged.";
			lblInstructions.Text = "All paid bills created on or before this date will be purged";
			strMessage = "Please make sure that you have a current permanent backup of the database before proceeding." + "\r\n";
			strMessage += "Make sure that everyone is out of TRIO when running this function." + "\r\n";
			strMessage += "This operation is memory intensive so please only purge one year at a time as to keep the load on your system down.";
			MessageBox.Show(strMessage, "Backup!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
		}

		private void frmPurgePaidBills_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			cmdPurge_Click();
		}
	}
}
