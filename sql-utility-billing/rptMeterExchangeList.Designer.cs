﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptMeterExchangeList.
	/// </summary>
	partial class rptMeterExchangeList
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMeterExchangeList));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblBooks = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSerialNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRemoteNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSize = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBackflow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSetDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPhone = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBooks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemoteNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBackflow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSetDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldName,
				this.fldLocation,
				this.fldSerialNumber,
				this.fldRemoteNumber,
				this.fldSize,
				this.fldBackflow,
				this.fldSetDate,
				this.fldPhone
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblAccount,
				this.Line1,
				this.lblBooks,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.lblDate
			});
			this.PageHeader.Height = 0.9270833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label16,
				this.fldTotal
			});
			this.GroupFooter1.Height = 0.40625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Meter Exchange List";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.15625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.65625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.65625F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.71875F;
			this.lblAccount.Width = 0.40625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.90625F;
			this.Line1.Width = 7.96875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.96875F;
			this.Line1.Y1 = 0.90625F;
			this.Line1.Y2 = 0.90625F;
			// 
			// lblBooks
			// 
			this.lblBooks.Height = 0.1875F;
			this.lblBooks.HyperLink = null;
			this.lblBooks.Left = 1.5F;
			this.lblBooks.Name = "lblBooks";
			this.lblBooks.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblBooks.Text = "Meter Exchange List";
			this.lblBooks.Top = 0.21875F;
			this.lblBooks.Width = 5.15625F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 0.5F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label8.Text = "Name";
			this.Label8.Top = 0.71875F;
			this.Label8.Width = 1.5625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2.15625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label9.Text = "Location";
			this.Label9.Top = 0.71875F;
			this.Label9.Width = 1.6875F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.90625F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label10.Text = "Serial #";
			this.Label10.Top = 0.71875F;
			this.Label10.Width = 0.71875F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.6875F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label11.Text = "Remote #";
			this.Label11.Top = 0.71875F;
			this.Label11.Width = 0.78125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 5.53125F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label12.Text = "Size";
			this.Label12.Top = 0.71875F;
			this.Label12.Width = 0.3125F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 5.9375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label13.Text = "BF";
			this.Label13.Top = 0.71875F;
			this.Label13.Width = 0.21875F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 6.21875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.Label14.Text = "Date";
			this.Label14.Top = 0.71875F;
			this.Label14.Width = 0.65625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 6.9375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.Label15.Text = "Phone";
			this.Label15.Top = 0.71875F;
			this.Label15.Width = 1.03125F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1.5F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblDate.Text = "Meter Exchange List";
			this.lblDate.Top = 0.40625F;
			this.lblDate.Width = 5.15625F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.19F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.40625F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.19F;
			this.fldName.Left = 0.5F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldName.Text = "Field2";
			this.fldName.Top = 0F;
			this.fldName.Width = 1.5625F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.19F;
			this.fldLocation.Left = 2.15625F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldLocation.Text = "Field1";
			this.fldLocation.Top = 0F;
			this.fldLocation.Width = 1.6875F;
			// 
			// fldSerialNumber
			// 
			this.fldSerialNumber.Height = 0.19F;
			this.fldSerialNumber.Left = 3.90625F;
			this.fldSerialNumber.Name = "fldSerialNumber";
			this.fldSerialNumber.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldSerialNumber.Text = "Field2";
			this.fldSerialNumber.Top = 0F;
			this.fldSerialNumber.Width = 0.71875F;
			// 
			// fldRemoteNumber
			// 
			this.fldRemoteNumber.Height = 0.19F;
			this.fldRemoteNumber.Left = 4.6875F;
			this.fldRemoteNumber.Name = "fldRemoteNumber";
			this.fldRemoteNumber.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldRemoteNumber.Text = "Field2";
			this.fldRemoteNumber.Top = 0F;
			this.fldRemoteNumber.Width = 0.78125F;
			// 
			// fldSize
			// 
			this.fldSize.Height = 0.19F;
			this.fldSize.Left = 5.53125F;
			this.fldSize.Name = "fldSize";
			this.fldSize.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldSize.Text = "Field2";
			this.fldSize.Top = 0F;
			this.fldSize.Width = 0.3125F;
			// 
			// fldBackflow
			// 
			this.fldBackflow.Height = 0.19F;
			this.fldBackflow.Left = 5.9375F;
			this.fldBackflow.Name = "fldBackflow";
			this.fldBackflow.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldBackflow.Text = "Field2";
			this.fldBackflow.Top = 0F;
			this.fldBackflow.Width = 0.21875F;
			// 
			// fldSetDate
			// 
			this.fldSetDate.Height = 0.19F;
			this.fldSetDate.Left = 6.21875F;
			this.fldSetDate.Name = "fldSetDate";
			this.fldSetDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldSetDate.Text = "Field2";
			this.fldSetDate.Top = 0F;
			this.fldSetDate.Width = 0.65625F;
			// 
			// fldPhone
			// 
			this.fldPhone.Height = 0.19F;
			this.fldPhone.Left = 6.9375F;
			this.fldPhone.Name = "fldPhone";
			this.fldPhone.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldPhone.Text = "Field2";
			this.fldPhone.Top = 0F;
			this.fldPhone.Width = 1.03125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.0625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.Label16.Text = "TotalCount:";
			this.Label16.Top = 0.1875F;
			this.Label16.Width = 1F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 4.125F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0.1875F;
			this.fldTotal.Width = 0.53125F;
			// 
			// rptMeterExchangeList
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.979167F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBooks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemoteNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBackflow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSetDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSerialNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRemoteNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSize;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBackflow;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSetDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhone;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBooks;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
	}
}
