//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTPmtActivityReport.
	/// </summary>
	partial class frmUTPmtActivityReport : BaseForm
	{
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraQuestions;
		public fecherFoundation.FCCheckBox chkEpmtOnly;
		public fecherFoundation.FCFrame fraInclude;
		public fecherFoundation.FCCheckBox chkInterest;
		public fecherFoundation.FCCheckBox chkDiscount;
		public fecherFoundation.FCCheckBox chkCorrection;
		public fecherFoundation.FCCheckBox chkPrePay;
		public fecherFoundation.FCCheckBox chkPayment;
		public fecherFoundation.FCCheckBox chkDemand;
		public fecherFoundation.FCCheckBox chkMaturityFee;
		public fecherFoundation.FCCheckBox chkAbatement;
		public fecherFoundation.FCCheckBox chkSummaryOnly;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTPmtActivityReport));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
			this.fraWhere = new fecherFoundation.FCFrame();
			this.vsWhere = new fecherFoundation.FCGrid();
			this.cmdClear = new fecherFoundation.FCButton();
			this.fraSort = new fecherFoundation.FCFrame();
			this.lstSort = new fecherFoundation.FCListBox();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.fraQuestions = new fecherFoundation.FCFrame();
			this.chkEpmtOnly = new fecherFoundation.FCCheckBox();
			this.fraInclude = new fecherFoundation.FCFrame();
			this.chkInterest = new fecherFoundation.FCCheckBox();
			this.chkDiscount = new fecherFoundation.FCCheckBox();
			this.chkCorrection = new fecherFoundation.FCCheckBox();
			this.chkPrePay = new fecherFoundation.FCCheckBox();
			this.chkPayment = new fecherFoundation.FCCheckBox();
			this.chkDemand = new fecherFoundation.FCCheckBox();
			this.chkMaturityFee = new fecherFoundation.FCCheckBox();
			this.chkAbatement = new fecherFoundation.FCCheckBox();
			this.chkSummaryOnly = new fecherFoundation.FCCheckBox();
			this.ImageList1 = new Wisej.Web.ImageList();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
			this.fraWhere.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
			this.fraSort.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
			this.fraQuestions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEpmtOnly)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraInclude)).BeginInit();
			this.fraInclude.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDiscount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrePay)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPayment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMaturityFee)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAbatement)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 571);
			this.BottomPanel.Size = new System.Drawing.Size(878, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraWhere);
			this.ClientArea.Controls.Add(this.fraSort);
			this.ClientArea.Controls.Add(this.fraQuestions);
			this.ClientArea.Size = new System.Drawing.Size(898, 628);
			this.ClientArea.Controls.SetChildIndex(this.fraQuestions, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraSort, 0);
			this.ClientArea.Controls.SetChildIndex(this.fraWhere, 0);
			this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdClear);
			this.TopPanel.Size = new System.Drawing.Size(898, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Size = new System.Drawing.Size(275, 30);
			this.HeaderText.Text = "Payment Activity Report";
			// 
			// fraWhere
			// 
			this.fraWhere.Controls.Add(this.vsWhere);
			this.fraWhere.Location = new System.Drawing.Point(30, 271);
			this.fraWhere.Name = "fraWhere";
			this.fraWhere.Size = new System.Drawing.Size(420, 300);
			this.fraWhere.TabIndex = 1;
			this.fraWhere.Text = "Select Search Criteria";
			this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
			// 
			// vsWhere
			// 
			this.vsWhere.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.vsWhere.Cols = 3;
			this.vsWhere.ColumnHeadersVisible = false;
			this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsWhere.ExtendLastCol = true;
			this.vsWhere.FixedRows = 0;
			this.vsWhere.Location = new System.Drawing.Point(20, 30);
			this.vsWhere.Name = "vsWhere";
			this.vsWhere.ReadOnly = false;
			this.vsWhere.Rows = 0;
			this.vsWhere.ShowFocusCell = false;
			this.vsWhere.Size = new System.Drawing.Size(379, 250);
			this.vsWhere.StandardTab = false;
			this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsWhere.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEdit);
			this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
			this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
			this.vsWhere.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsWhere_MouseMoveEvent);
			this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
			this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
			this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
			this.vsWhere.Leave += new System.EventHandler(this.vsWhere_Leave);
			this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
			// 
			// cmdClear
			// 
			this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdClear.Location = new System.Drawing.Point(727, 29);
			this.cmdClear.Name = "cmdClear";
			this.cmdClear.Size = new System.Drawing.Size(143, 24);
			this.cmdClear.TabIndex = 1;
			this.cmdClear.Text = "Clear Search Criteria";
			this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
			// 
			// fraSort
			// 
			this.fraSort.Controls.Add(this.lstSort);
			this.fraSort.Location = new System.Drawing.Point(480, 271);
			this.fraSort.Name = "fraSort";
			this.fraSort.Size = new System.Drawing.Size(317, 300);
			this.fraSort.TabIndex = 2;
			this.fraSort.Text = "Fields To Sort By";
			this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
			// 
			// lstSort
			// 
			this.lstSort.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
			this.lstSort.BackColor = System.Drawing.SystemColors.Window;
			this.lstSort.CheckBoxes = true;
			this.lstSort.Location = new System.Drawing.Point(20, 30);
			this.lstSort.Name = "lstSort";
			this.lstSort.Size = new System.Drawing.Size(277, 250);
			this.lstSort.Style = 1;
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.ForeColor = System.Drawing.Color.White;
			this.cmdPrint.Location = new System.Drawing.Point(396, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(60, 48);
			this.cmdPrint.Text = "Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// fraQuestions
			// 
			this.fraQuestions.AppearanceKey = "groupBoxNoBorders";
			this.fraQuestions.Controls.Add(this.chkEpmtOnly);
			this.fraQuestions.Controls.Add(this.fraInclude);
			this.fraQuestions.Controls.Add(this.chkSummaryOnly);
			this.fraQuestions.Name = "fraQuestions";
			this.fraQuestions.Size = new System.Drawing.Size(788, 251);
			this.fraQuestions.TabIndex = 1001;
			// 
			// chkEpmtOnly
			// 
			this.chkEpmtOnly.Location = new System.Drawing.Point(480, 98);
			this.chkEpmtOnly.Name = "chkEpmtOnly";
			this.chkEpmtOnly.Size = new System.Drawing.Size(196, 18);
			this.chkEpmtOnly.TabIndex = 2;
			this.chkEpmtOnly.Text = "Show Online Payments Only";
			this.chkEpmtOnly.CheckedChanged += new System.EventHandler(this.chkEpmtOnly_CheckedChanged);
			// 
			// fraInclude
			// 
			this.fraInclude.Controls.Add(this.chkInterest);
			this.fraInclude.Controls.Add(this.chkDiscount);
			this.fraInclude.Controls.Add(this.chkCorrection);
			this.fraInclude.Controls.Add(this.chkPrePay);
			this.fraInclude.Controls.Add(this.chkPayment);
			this.fraInclude.Controls.Add(this.chkDemand);
			this.fraInclude.Controls.Add(this.chkMaturityFee);
			this.fraInclude.Controls.Add(this.chkAbatement);
			this.fraInclude.Location = new System.Drawing.Point(30, 30);
			this.fraInclude.Name = "fraInclude";
			this.fraInclude.Size = new System.Drawing.Size(420, 221);
			this.fraInclude.TabIndex = 3;
			this.fraInclude.Text = "Include";
			// 
			// chkInterest
			// 
			this.chkInterest.Location = new System.Drawing.Point(232, 30);
			this.chkInterest.Name = "chkInterest";
			this.chkInterest.Size = new System.Drawing.Size(70, 18);
			this.chkInterest.TabIndex = 5;
			this.chkInterest.Text = "Interest";
			this.ToolTip1.SetToolTip(this.chkInterest, "Show Costs on the report.");
			// 
			// chkDiscount
			// 
			this.chkDiscount.Checked = true;
			this.chkDiscount.CheckState = Wisej.Web.CheckState.Checked;
			this.chkDiscount.Location = new System.Drawing.Point(20, 182);
			this.chkDiscount.Name = "chkDiscount";
			this.chkDiscount.Size = new System.Drawing.Size(78, 18);
			this.chkDiscount.TabIndex = 4;
			this.chkDiscount.Text = "Discount";
			this.ToolTip1.SetToolTip(this.chkDiscount, "Show Costs on the report.");
			// 
			// chkCorrection
			// 
			this.chkCorrection.Checked = true;
			this.chkCorrection.CheckState = Wisej.Web.CheckState.Checked;
			this.chkCorrection.Location = new System.Drawing.Point(20, 106);
			this.chkCorrection.Name = "chkCorrection";
			this.chkCorrection.Size = new System.Drawing.Size(88, 18);
			this.chkCorrection.TabIndex = 2;
			this.chkCorrection.Text = "Correction";
			this.ToolTip1.SetToolTip(this.chkCorrection, "Show Interest on the report.");
			// 
			// chkPrePay
			// 
			this.chkPrePay.Checked = true;
			this.chkPrePay.CheckState = Wisej.Web.CheckState.Checked;
			this.chkPrePay.Location = new System.Drawing.Point(20, 68);
			this.chkPrePay.Name = "chkPrePay";
			this.chkPrePay.Size = new System.Drawing.Size(104, 18);
			this.chkPrePay.TabIndex = 1;
			this.chkPrePay.Text = "Pre-Payment";
			this.ToolTip1.SetToolTip(this.chkPrePay, "Show Tax on the report.");
			// 
			// chkPayment
			// 
			this.chkPayment.Checked = true;
			this.chkPayment.CheckState = Wisej.Web.CheckState.Checked;
			this.chkPayment.Location = new System.Drawing.Point(20, 30);
			this.chkPayment.Name = "chkPayment";
			this.chkPayment.Size = new System.Drawing.Size(79, 18);
			this.chkPayment.TabIndex = 6;
			this.chkPayment.Text = "Payment";
			this.ToolTip1.SetToolTip(this.chkPayment, "Show Principal on the report.");
			// 
			// chkDemand
			// 
			this.chkDemand.Location = new System.Drawing.Point(232, 68);
			this.chkDemand.Name = "chkDemand";
			this.chkDemand.Size = new System.Drawing.Size(103, 18);
			this.chkDemand.TabIndex = 6;
			this.chkDemand.Text = "Demand Fee";
			this.ToolTip1.SetToolTip(this.chkDemand, "Show Costs on the report.");
			// 
			// chkMaturityFee
			// 
			this.chkMaturityFee.Location = new System.Drawing.Point(232, 106);
			this.chkMaturityFee.Name = "chkMaturityFee";
			this.chkMaturityFee.Size = new System.Drawing.Size(100, 18);
			this.chkMaturityFee.TabIndex = 7;
			this.chkMaturityFee.Text = "Maturity Fee";
			this.ToolTip1.SetToolTip(this.chkMaturityFee, "Show Costs on the report.");
			// 
			// chkAbatement
			// 
			this.chkAbatement.Checked = true;
			this.chkAbatement.CheckState = Wisej.Web.CheckState.Checked;
			this.chkAbatement.Location = new System.Drawing.Point(20, 144);
			this.chkAbatement.Name = "chkAbatement";
			this.chkAbatement.Size = new System.Drawing.Size(91, 18);
			this.chkAbatement.TabIndex = 3;
			this.chkAbatement.Text = "Abatement";
			this.ToolTip1.SetToolTip(this.chkAbatement, "Show Costs on the report.");
			// 
			// chkSummaryOnly
			// 
			this.chkSummaryOnly.Location = new System.Drawing.Point(480, 60);
			this.chkSummaryOnly.Name = "chkSummaryOnly";
			this.chkSummaryOnly.Size = new System.Drawing.Size(152, 18);
			this.chkSummaryOnly.TabIndex = 1;
			this.chkSummaryOnly.Text = "Show Summary Only";
			this.chkSummaryOnly.Visible = false;
			// 
			// ImageList1
			// 
			this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
			this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
			this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuFileSeperator2,
            this.mnuPrint,
            this.mnuSP1,
            this.mnuExit});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuClear
			// 
			this.mnuClear.Index = 0;
			this.mnuClear.Name = "mnuClear";
			this.mnuClear.Text = "Clear Search Criteria";
			this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 1;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuPrint
			// 
			this.mnuPrint.Index = 2;
			this.mnuPrint.Name = "mnuPrint";
			this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuPrint.Text = "Process";
			this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
			// 
			// mnuSP1
			// 
			this.mnuSP1.Index = 3;
			this.mnuSP1.Name = "mnuSP1";
			this.mnuSP1.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// mnuLayout
			// 
			this.mnuLayout.Enabled = false;
			this.mnuLayout.Index = -1;
			this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
			this.mnuLayout.Name = "mnuLayout";
			this.mnuLayout.Text = "Layout";
			this.mnuLayout.Visible = false;
			// 
			// mnuAddRow
			// 
			this.mnuAddRow.Index = 0;
			this.mnuAddRow.Name = "mnuAddRow";
			this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
			this.mnuAddRow.Text = "Add Row";
			// 
			// mnuAddColumn
			// 
			this.mnuAddColumn.Index = 1;
			this.mnuAddColumn.Name = "mnuAddColumn";
			this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
			this.mnuAddColumn.Text = "Add Column";
			// 
			// mnuDeleteRow
			// 
			this.mnuDeleteRow.Index = 2;
			this.mnuDeleteRow.Name = "mnuDeleteRow";
			this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
			this.mnuDeleteRow.Text = "Delete Row";
			// 
			// mnuDeleteColumn
			// 
			this.mnuDeleteColumn.Index = 3;
			this.mnuDeleteColumn.Name = "mnuDeleteColumn";
			this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuDeleteColumn.Text = "Delete Column";
			// 
			// frmUTPmtActivityReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(898, 688);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmUTPmtActivityReport";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Payment Activity Report";
			this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
			this.Load += new System.EventHandler(this.frmUTPmtActivityReport_Load);
			this.Activated += new System.EventHandler(this.frmUTPmtActivityReport_Activated);
			this.Resize += new System.EventHandler(this.frmUTPmtActivityReport_Resize);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTPmtActivityReport_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
			this.fraWhere.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
			this.fraSort.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
			this.fraQuestions.ResumeLayout(false);
			this.fraQuestions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkEpmtOnly)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraInclude)).EndInit();
			this.fraInclude.ResumeLayout(false);
			this.fraInclude.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDiscount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkCorrection)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPrePay)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkPayment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMaturityFee)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAbatement)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}