﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmAnalysisReports.
	/// </summary>
	partial class frmAnalysisReports : BaseForm
	{
		public fecherFoundation.FCComboBox cmbOrientation;
		public fecherFoundation.FCLabel lblOrientation;
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public fecherFoundation.FCFrame fraType;
		public fecherFoundation.FCCheckBox chkSalesTaxS;
		public fecherFoundation.FCCheckBox chkSalesTaxW;
		public fecherFoundation.FCCheckBox chkMeterReportS;
		public fecherFoundation.FCCheckBox chkBillCountS;
		public fecherFoundation.FCCheckBox chkConsumptionsS;
		public fecherFoundation.FCCheckBox chkDollarAmountsS;
		public fecherFoundation.FCCheckBox chkBillingSummary;
		public fecherFoundation.FCCheckBox chkDollarAmountsW;
		public fecherFoundation.FCCheckBox chkConsumptionsW;
		public fecherFoundation.FCCheckBox chkBillCountW;
		public fecherFoundation.FCCheckBox chkMeterReportW;
		public fecherFoundation.FCLabel lblSewerHeader;
		public fecherFoundation.FCLabel lblWaterHeader;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAnalysisReports));
			this.cmbOrientation = new fecherFoundation.FCComboBox();
			this.lblOrientation = new fecherFoundation.FCLabel();
			this.cmbWS = new fecherFoundation.FCComboBox();
			this.lblWS = new fecherFoundation.FCLabel();
			this.fraType = new fecherFoundation.FCFrame();
			this.chkSalesTaxS = new fecherFoundation.FCCheckBox();
			this.chkSalesTaxW = new fecherFoundation.FCCheckBox();
			this.chkMeterReportS = new fecherFoundation.FCCheckBox();
			this.chkBillCountS = new fecherFoundation.FCCheckBox();
			this.chkConsumptionsS = new fecherFoundation.FCCheckBox();
			this.chkDollarAmountsS = new fecherFoundation.FCCheckBox();
			this.chkBillingSummary = new fecherFoundation.FCCheckBox();
			this.chkDollarAmountsW = new fecherFoundation.FCCheckBox();
			this.chkConsumptionsW = new fecherFoundation.FCCheckBox();
			this.chkBillCountW = new fecherFoundation.FCCheckBox();
			this.chkMeterReportW = new fecherFoundation.FCCheckBox();
			this.lblSewerHeader = new fecherFoundation.FCLabel();
			this.lblWaterHeader = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraType)).BeginInit();
			this.fraType.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMeterReportS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBillCountS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkConsumptionsS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDollarAmountsS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBillingSummary)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDollarAmountsW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkConsumptionsW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBillCountW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMeterReportW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 570);
			this.BottomPanel.Size = new System.Drawing.Size(436, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbOrientation);
			this.ClientArea.Controls.Add(this.lblOrientation);
			this.ClientArea.Controls.Add(this.fraType);
			this.ClientArea.Controls.Add(this.cmbWS);
			this.ClientArea.Controls.Add(this.lblWS);
			this.ClientArea.Size = new System.Drawing.Size(436, 510);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(436, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(197, 30);
			this.HeaderText.Text = "Analysis Reports";
			// 
			// cmbOrientation
			// 
			this.cmbOrientation.AutoSize = false;
			this.cmbOrientation.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOrientation.FormattingEnabled = true;
			this.cmbOrientation.Items.AddRange(new object[] {
				"Portrait",
				"Landscape"
			});
			this.cmbOrientation.Location = new System.Drawing.Point(175, 90);
			this.cmbOrientation.Name = "cmbOrientation";
			this.cmbOrientation.Size = new System.Drawing.Size(233, 40);
			this.cmbOrientation.TabIndex = 0;
			this.cmbOrientation.Text = "Portrait";
			// 
			// lblOrientation
			// 
			this.lblOrientation.AutoSize = true;
			this.lblOrientation.Location = new System.Drawing.Point(30, 104);
			this.lblOrientation.Name = "lblOrientation";
			this.lblOrientation.Size = new System.Drawing.Size(92, 15);
			this.lblOrientation.TabIndex = 1;
			this.lblOrientation.Text = "ORIENTATION";
			// 
			// cmbWS
			// 
			this.cmbWS.AutoSize = false;
			this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWS.FormattingEnabled = true;
			this.cmbWS.Items.AddRange(new object[] {
				"Water",
				"Sewer",
				"Both"
			});
			this.cmbWS.Location = new System.Drawing.Point(175, 30);
			this.cmbWS.Name = "cmbWS";
			this.cmbWS.Size = new System.Drawing.Size(233, 40);
			this.cmbWS.TabIndex = 12;
			this.cmbWS.SelectedIndexChanged += new System.EventHandler(this.optWS_CheckedChanged);
			// 
			// lblWS
			// 
			this.lblWS.AutoSize = true;
			this.lblWS.Location = new System.Drawing.Point(30, 44);
			this.lblWS.Name = "lblWS";
			this.lblWS.Size = new System.Drawing.Size(62, 15);
			this.lblWS.TabIndex = 13;
			this.lblWS.Text = "SERVICE";
			// 
			// fraType
			// 
			this.fraType.Controls.Add(this.chkSalesTaxS);
			this.fraType.Controls.Add(this.chkSalesTaxW);
			this.fraType.Controls.Add(this.chkMeterReportS);
			this.fraType.Controls.Add(this.chkBillCountS);
			this.fraType.Controls.Add(this.chkConsumptionsS);
			this.fraType.Controls.Add(this.chkDollarAmountsS);
			this.fraType.Controls.Add(this.chkBillingSummary);
			this.fraType.Controls.Add(this.chkDollarAmountsW);
			this.fraType.Controls.Add(this.chkConsumptionsW);
			this.fraType.Controls.Add(this.chkBillCountW);
			this.fraType.Controls.Add(this.chkMeterReportW);
			this.fraType.Controls.Add(this.lblSewerHeader);
			this.fraType.Controls.Add(this.lblWaterHeader);
			this.fraType.Location = new System.Drawing.Point(30, 150);
			this.fraType.Name = "fraType";
			this.fraType.Size = new System.Drawing.Size(378, 345);
			this.fraType.TabIndex = 11;
			this.fraType.Text = "Show Types";
			this.fraType.DoubleClick += new System.EventHandler(this.fraType_DoubleClick);
			// 
			// chkSalesTaxS
			// 
			this.chkSalesTaxS.Location = new System.Drawing.Point(204, 298);
			this.chkSalesTaxS.Name = "chkSalesTaxS";
			this.chkSalesTaxS.Size = new System.Drawing.Size(154, 27);
			this.chkSalesTaxS.TabIndex = 19;
			this.chkSalesTaxS.Text = "Sales Tax Report";
			// 
			// chkSalesTaxW
			// 
			this.chkSalesTaxW.Location = new System.Drawing.Point(20, 298);
			this.chkSalesTaxW.Name = "chkSalesTaxW";
			this.chkSalesTaxW.Size = new System.Drawing.Size(154, 27);
			this.chkSalesTaxW.TabIndex = 20;
			this.chkSalesTaxW.Text = "Sales Tax Report";
			// 
			// chkMeterReportS
			// 
			this.chkMeterReportS.Location = new System.Drawing.Point(204, 251);
			this.chkMeterReportS.Name = "chkMeterReportS";
			this.chkMeterReportS.Size = new System.Drawing.Size(123, 27);
			this.chkMeterReportS.TabIndex = 16;
			this.chkMeterReportS.Text = "Meter Report";
			// 
			// chkBillCountS
			// 
			this.chkBillCountS.Location = new System.Drawing.Point(204, 204);
			this.chkBillCountS.Name = "chkBillCountS";
			this.chkBillCountS.Size = new System.Drawing.Size(97, 27);
			this.chkBillCountS.TabIndex = 15;
			this.chkBillCountS.Text = "Bill Count";
			// 
			// chkConsumptionsS
			// 
			this.chkConsumptionsS.Location = new System.Drawing.Point(204, 157);
			this.chkConsumptionsS.Name = "chkConsumptionsS";
			this.chkConsumptionsS.Size = new System.Drawing.Size(132, 27);
			this.chkConsumptionsS.TabIndex = 14;
			this.chkConsumptionsS.Text = "Consumptions";
			// 
			// chkDollarAmountsS
			// 
			this.chkDollarAmountsS.Location = new System.Drawing.Point(204, 110);
			this.chkDollarAmountsS.Name = "chkDollarAmountsS";
			this.chkDollarAmountsS.Size = new System.Drawing.Size(139, 27);
			this.chkDollarAmountsS.TabIndex = 13;
			this.chkDollarAmountsS.Text = "Dollar Amounts";
			// 
			// chkBillingSummary
			// 
			this.chkBillingSummary.Location = new System.Drawing.Point(20, 30);
			this.chkBillingSummary.Name = "chkBillingSummary";
			this.chkBillingSummary.Size = new System.Drawing.Size(146, 27);
			this.chkBillingSummary.TabIndex = 5;
			this.chkBillingSummary.Text = "Billing Summary";
			// 
			// chkDollarAmountsW
			// 
			this.chkDollarAmountsW.Location = new System.Drawing.Point(20, 110);
			this.chkDollarAmountsW.Name = "chkDollarAmountsW";
			this.chkDollarAmountsW.Size = new System.Drawing.Size(139, 27);
			this.chkDollarAmountsW.TabIndex = 6;
			this.chkDollarAmountsW.Text = "Dollar Amounts";
			// 
			// chkConsumptionsW
			// 
			this.chkConsumptionsW.Location = new System.Drawing.Point(20, 157);
			this.chkConsumptionsW.Name = "chkConsumptionsW";
			this.chkConsumptionsW.Size = new System.Drawing.Size(132, 27);
			this.chkConsumptionsW.TabIndex = 7;
			this.chkConsumptionsW.Text = "Consumptions";
			// 
			// chkBillCountW
			// 
			this.chkBillCountW.Location = new System.Drawing.Point(20, 204);
			this.chkBillCountW.Name = "chkBillCountW";
			this.chkBillCountW.Size = new System.Drawing.Size(97, 27);
			this.chkBillCountW.TabIndex = 8;
			this.chkBillCountW.Text = "Bill Count";
			// 
			// chkMeterReportW
			// 
			this.chkMeterReportW.Location = new System.Drawing.Point(20, 251);
			this.chkMeterReportW.Name = "chkMeterReportW";
			this.chkMeterReportW.Size = new System.Drawing.Size(123, 27);
			this.chkMeterReportW.TabIndex = 9;
			this.chkMeterReportW.Text = "Meter Report";
			// 
			// lblSewerHeader
			// 
			this.lblSewerHeader.Location = new System.Drawing.Point(204, 77);
			this.lblSewerHeader.Name = "lblSewerHeader";
			this.lblSewerHeader.Size = new System.Drawing.Size(154, 23);
			this.lblSewerHeader.TabIndex = 18;
			this.lblSewerHeader.Text = "SEWER";
			this.lblSewerHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// lblWaterHeader
			// 
			this.lblWaterHeader.Location = new System.Drawing.Point(20, 77);
			this.lblWaterHeader.Name = "lblWaterHeader";
			this.lblWaterHeader.Size = new System.Drawing.Size(154, 23);
			this.lblWaterHeader.TabIndex = 17;
			this.lblWaterHeader.Text = "WATER";
			this.lblWaterHeader.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePrint.Text = "Save & Preview";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(156, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmAnalysisReports
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(436, 678);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmAnalysisReports";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Analysis Reports";
			this.Load += new System.EventHandler(this.frmAnalysisReports_Load);
			this.Activated += new System.EventHandler(this.frmAnalysisReports_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmAnalysisReports_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraType)).EndInit();
			this.fraType.ResumeLayout(false);
			this.fraType.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkSalesTaxW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMeterReportS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBillCountS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkConsumptionsS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDollarAmountsS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBillingSummary)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkDollarAmountsW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkConsumptionsW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkBillCountW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkMeterReportW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdSave;
		private System.ComponentModel.IContainer components;
	}
}
