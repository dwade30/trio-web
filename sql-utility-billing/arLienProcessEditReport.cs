﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using SharedApplication.Extensions;
using TWSharedLibrary;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arLienProcessEditReport.
	/// </summary>
	public partial class arLienProcessEditReport : BaseSectionReport
	{
		public arLienProcessEditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Process Edit Report";
		}

		public static arLienProcessEditReport InstancePtr
		{
			get
			{
				return (arLienProcessEditReport)Sys.GetInstance(typeof(arLienProcessEditReport));
			}
		}

		protected arLienProcessEditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsLN.Dispose();
				rsMast.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arLienProcessEditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/16/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/16/2007              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		clsDRWrapper rsMast = new clsDRWrapper();
		//clsDRWrapper rsBook = new clsDRWrapper();
		string strSQL;
		double[] dblTotals = new double[3 + 1];
		// vbPorter upgrade warning: dblMinimumAmount As double	OnWrite(string)
		double dblMinimumAmount;
		int lngPosBal;
		int lngZeroBal;
		int lngNegBal;
		int lngDemandAlready;
		DateTime dtMailDate;
		public int intReportDetail;
		int intLienAccts;
		string strReportDesc = "";
		int lngNumOfAccounts;
		string strRateKeyList;
		bool boolShowMapLot;
		bool boolShowBookPage;
		bool boolShowLocation;
		bool boolShowMailingAddress;
		int lngExtraLines;
		bool boolWater;
		string strWS = "";
		string strWaterSewer = "";
		string strRKSQL = "";
		string strBookList;
		bool boolCheckOnlyOldestBill;
		string strOldestRateKey = "";
		double dblCheckTotal;
		Dictionary<object, object> objDictionary = new Dictionary<object, object>();
		int lngOldestRK;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}
		// vbPorter upgrade warning: intLienedAccts As short	OnWriteFCConvert.ToInt32(
		public void StartLienEditReportQuery(ref int intLienedAccts, ref bool boolPassWater, string strRateList = "", string strPassBookList = "", string strPassDateRanges = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDefaults = new clsDRWrapper();
				int intListLen;
				int intPdPos;
				int intOrPos;
				string tempString;
				string strNewBookList = "";

				if (intLienedAccts == 0 || intLienedAccts == 1 || intLienedAccts == 2 || intLienedAccts == 3 ||
				    intLienedAccts == 4)
				{
					intListLen = strPassBookList.Length;

					intPdPos = Strings.InStr(strPassBookList, ".");
					intOrPos = Strings.InStr(strPassBookList, "R");

					strBookList = "";

					if (intOrPos == 0)
					{
						strBookList = strPassBookList.Right(strPassBookList.Length - intPdPos);
					}
					else
					{
						strNewBookList = strPassBookList;

						while (intOrPos != 0)
						{
							tempString = strNewBookList.Left(intOrPos - 2);
							tempString = tempString.Substring(intPdPos, tempString.Length - intPdPos);

							strBookList = strBookList + tempString + " OR ";


							strNewBookList = strPassBookList.Right(intListLen - intOrPos);

							intListLen = strNewBookList.Length;

							intPdPos = Strings.InStr(strNewBookList, ".");
							intOrPos = Strings.InStr(strNewBookList, "R");
						}

						tempString = strNewBookList.Right(intListLen - intPdPos);

						strBookList = strBookList + tempString;
					}
				}
				else
				{
					strBookList = strPassBookList;
				}
				
				rsDefaults.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!rsDefaults.EndOfFile())
				{
					boolCheckOnlyOldestBill = FCConvert.ToBoolean(rsDefaults.Get_Fields_Boolean("OnlyCheckOldestBillForLien"));
				}
				boolWater = boolPassWater;
				if (boolWater)
				{
					strWS = "W";
					strWaterSewer = "Water";
				}
				else
				{
					strWS = "S";
					strWaterSewer = "Sewer";
				}
				dblMinimumAmount = FCConvert.ToDouble(frmRateRecChoice.InstancePtr.txtMinimumAmount.Text);
				intLienAccts = intLienedAccts;
				// build the SQL statement
				strRateKeyList = strRateList;
				if (boolCheckOnlyOldestBill)
				{
					lngOldestRK = modMain.DetermineOldestRateKey(ref strRateKeyList);
					// If Len(strRateKeyList) > 0 Then
					// strSQL = "SELECT TOP 1 * FROM (" & strSQL & ") ORDER BY Bill"
					// find the oldest rate key to only check for eligibilty for
					// strArr = Split(Mid(Trim(strRateKeyList), 2, Len(Trim(strRateKeyList)) - 2), ",")
					// 
					// lngOldestRK = strArr(0)
					// For intCT = 0 To UBound(strArr)
					// If strArr(intCT) < lngOldestRK Then
					// lngOldestRK = strArr(intCT)
					// End If
					// Next
					// 
					// strRKSQL = " BillingRateKey = " & lngOldestRK
					// Else
					// lngOldestRK = -1
					// strRKSQL = ""
					// End If
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
				//Application.DoEvents();
				strSQL = BuildSQL();
				rsData.OpenRecordset(strSQL);
				if (rsData.EndOfFile())
				{
					lblAccount.Left = 0;
					lblAccount.Width = this.PrintWidth;
					lblAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
					lblAccount.Text = "No records were found.";
					HideAllFields();
				}
				lngNumOfAccounts = 0;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, rsData.RecordCount() + 1);
				dtMailDate = DateAndTime.DateValue(frmRateRecChoice.InstancePtr.txtMailDate.Text);
				intReportDetail = frmRateRecChoice.InstancePtr.cmbReportDetail.ItemData(frmRateRecChoice.InstancePtr.cmbReportDetail.SelectedIndex);
				if (!(strPassDateRanges == ""))
				{
					// trouts-227 5.5.2017 Date Range selection option
					lblReportType.Text = strPassDateRanges + "  Interest as of " + Strings.Format(dtMailDate, "MM/dd/yyyy") + "\r\n" + strReportDesc;
				}
				else
				{
					lblReportType.Text = "Rate Keys: " + strRateKeyList + "  Interest as of " + Strings.Format(dtMailDate, "MM/dd/yyyy") + "\r\n" + strReportDesc;
				}
				rsLN.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
				frmWait.InstancePtr.IncrementProgress();
				// If gboolRE Then
				// if the user has Real Estate, then use the information from the database
				// kgk 07312012   rsMast.OpenRecordset "SELECT * FROM Master", strUTDatabase
				rsMast.OpenRecordset("SELECT ID, INBankruptcy, StreetNumber, Apt, StreetName, MapLot FROM Master", modExtraModules.strUTDatabase);
				// End If
				//Application.DoEvents();
				// Me.Show , MDIParent
				frmWait.InstancePtr.Show(App.MainForm);
				rsDefaults.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Start Lien Edit Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// format the report
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngPosBal = 0;
			lngZeroBal = 0;
			lngNegBal = 0;
			lngDemandAlready = 0;
			boolShowMapLot = FCConvert.CBool(frmRateRecChoice.InstancePtr.chkPrint[0].CheckState == Wisej.Web.CheckState.Checked);
			boolShowBookPage = FCConvert.CBool(frmRateRecChoice.InstancePtr.chkPrint[1].CheckState == Wisej.Web.CheckState.Checked);
			boolShowLocation = FCConvert.CBool(frmRateRecChoice.InstancePtr.chkPrint[2].CheckState == Wisej.Web.CheckState.Checked);
			boolShowMailingAddress = FCConvert.CBool(frmRateRecChoice.InstancePtr.chkPrint[3].CheckState == Wisej.Web.CheckState.Checked);
			lngExtraLines = FCConvert.ToInt32(Math.Round(Conversion.Val(frmRateRecChoice.InstancePtr.cmbExtraLines.Items[frmRateRecChoice.InstancePtr.cmbExtraLines.SelectedIndex].ToString())));
			SetupDetailSection();
			lblFooter.Left = 0;
			lblFooter.Width = this.PrintWidth - 10 / 1440F;
			lblFooter.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
			lblFooter.Text = "* - This account is currently liened.";
			switch (intLienAccts)
			{
				case 0:
				case 2:
				case 5:
					{
						lblFooter.Visible = true;
						break;
					}
				case 1:
				case 3:
				case 4:
					{
						lblFooter.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
			frmRateRecChoice.InstancePtr.Unload();
			// frmRateRecChoice.Show , MDIParent
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will return the correct SQL string
				string strFields = "";
				string strWhereClause = "";
				if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Account")
				{
					// range of accounts
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							strWhereClause = " ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
							strReportDesc += "Account " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause = " ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
							strReportDesc += "Account Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
							strReportDesc += "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// both empty
							strWhereClause = "";
							strReportDesc += "";
						}
					}
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
				{
					// range of names
					if (strWhereClause != "")
					{
						strWhereClause += " AND ";
					}
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// both full
							// strWhereClause = " AND Name1 BETWEEN '" & .txtRange(0).Text & "' AND '" & .txtRange(1).Text & "'"
							strWhereClause = " BName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   ' AND BName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
							strReportDesc += "Name " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						}
						else
						{
							// first full second empty
							strWhereClause = " BName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "  '";
							strReportDesc += "Name Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
					}
					else
					{
						if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
						{
							// first empty second full
							strWhereClause = " BName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZ'";
							strReportDesc += "Account Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						}
						else
						{
							// both empty
							strWhereClause = "";
							strReportDesc += "";
						}
					}
				}
				else
				{
					strWhereClause = "";
					strReportDesc = "";
				}
				switch (intLienAccts)
				{
					case 0:
						{
							// this is all accounts
							break;
						}
					case 1:
						{
							strReportDesc = "Non-Liened Accounts" + "\r\n";
							if (Strings.Trim(strWhereClause) != "")
								strWhereClause += " AND ";
							strWhereClause += strWS + "LienRecordNumber = 0";
							break;
						}
					case 2:
						{
							strReportDesc = "Liened Accounts" + "\r\n";
							if (Strings.Trim(strWhereClause) != "")
								strWhereClause += " AND ";
							strWhereClause += strWS + "LienRecordNumber <> 0";
							break;
						}
					case 3:
						{
							strReportDesc = "Accounts Eligible for 30 Day Notice" + "\r\n";
							if (Strings.Trim(strWhereClause) != "")
								strWhereClause += " AND ";
							strWhereClause += " (" + strWS + "LienProcessStatus = 0 OR " + strWS + "LienProcessStatus = 1) AND (" + strWS + "LienStatusEligibility = 1 OR " + strWS + "LienStatusEligibility = 0) AND " + strWS + "LienRecordNumber = 0";
							break;
						}
					case 4:
						{
							strReportDesc = "Accounts Eligible for Lien" + "\r\n";
							if (Strings.Trim(strWhereClause) != "")
								strWhereClause += " AND ";
							strWhereClause += " (" + strWS + "LienProcessStatus = 2 OR " + strWS + "LienProcessStatus = 3) AND " + strWS + "LienStatusEligibility = 3";
							break;
						}
					case 5:
						{
							strReportDesc = "Accounts Eligible for Lien Maturity" + "\r\n";
							if (Strings.Trim(strWhereClause) != "")
								strWhereClause += " AND ";
							if (strRateKeyList != "")
							{
								strWhereClause += " (" + strWS + "LienProcessStatus = 4 OR " + strWS + "LienProcessStatus = 5) AND Lien.RateKey IN " + strRateKeyList;
								// AND LienStatusEligibility = 5"
							}
							else
							{
								strWhereClause += "  (" + strWS + "LienProcessStatus = 4 OR " + strWS + "LienProcessStatus = 5) ";
							}
							// MAL@20080221: Change to ignore those where the LDN is already printed
							// Tracker Reference: 12308
							// DJW@20090501: Taking this check out and just using Prin Amount still owed to determine if they should get this or not
							// Tracker Reference: 18357
							if (Strings.Trim(strWhereClause) != "")
							{
								strWhereClause += " AND (Principal - Lien.PrinPaid) > 0";
								// strWhereClause = strWhereClause & " AND PrintedLDN = False"
							}
							break;
						}
				}
				//end switch
				if (Strings.Trim(strWhereClause) != "")
				{
					strWhereClause += " AND (Service = 'B' OR Service = '" + strWS + "')";
				}
				else
				{
					strWhereClause = " (Service = 'B' OR Service = '" + strWS + "')";
				}
				if (Strings.Trim(strRKSQL) != "")
				{
					if (Strings.Trim(strWhereClause) != "")
					{
						strWhereClause += " AND " + strRKSQL;
					}
					else
					{
						strWhereClause += strRKSQL;
					}
				}
				if (Strings.Trim(strBookList) != "")
				{
					if (Strings.Trim(strWhereClause) != "")
					{
						strWhereClause += " AND (" + strBookList + ") ";
					}
					else
					{
						strWhereClause += strBookList;
					}
				}
				// MAL@20070907: Added new order by clause to help with duplicate check
				// Call Reference: 117109
				if (intLienAccts == 5)
				{
					// k       strSQL = "SELECT Bill." & strWS & "LienRecordNumber AS LRN, * FROM Bill INNER JOIN Lien ON Bill." & strWS & "LienRecordNumber = Lien.ID WHERE " & strWhereClause      'kgk & " ORDER BY OName, ActualAccountNumber"
					strSQL = "SELECT Bill." + strWS + "LienRecordNumber AS LRN, Bill.*, Bill.ID AS Bill, Lien.ID AS LienKey, Lien.IntPaidDate, Lien.Principal, Lien.Tax, Lien.Interest, Lien.IntAdded, Lien.Costs, Lien.PrinPaid, Lien.TaxPaid, Lien.PLIPaid, Lien.IntPaid, Lien.CostPaid, Lien.MaturityFee, Lien.DateCreated, Lien.Status, Lien.RateKey, Lien.Water " + "FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE " + strWhereClause;
				}
				else if (intLienAccts == 2)
				{
					// MAL@20070918: Found this testing another process
					// strSQL = "SELECT " & strWS & "LienRecordNumber AS LRN, * FROM BillingMasterYear WHERE " & Right(strWhereClause, Len(strWhereClause) - 4) & " ORDER BY OName, ActualAccountNumber"
					// kgk 07-31  strSQL = "SELECT " & strWS & "LienRecordNumber AS LRN, * FROM BillingMasterYear WHERE " & strWhereClause & " ORDER BY OName, ActualAccountNumber"
					strSQL = "SELECT " + strWS + "LienRecordNumber AS LRN, * FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmp WHERE " + strWhereClause;
					// kgk& " ORDER BY OName, ActualAccountNumber"
				}
				else
				{
					// XXXXXXX BILLINGMASTERYEAR XXXXXXXXXXXXXXX
					if (Strings.Trim(strWhereClause) != "")
					{
						// kgk          strSQL = "SELECT " & strWS & "LienRecordNumber AS LRN, * FROM BillingMasterYear WHERE " & strWhereClause & " ORDER BY OName, ActualAccountNumber"
						strSQL = "SELECT " + strWS + "LienRecordNumber AS LRN, * FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmp WHERE " + strWhereClause;
						// kgk & " ORDER BY OName, ActualAccountNumber"
					}
					else
					{
						// kgk          strSQL = "SELECT " & strWS & "LienRecordNumber AS LRN, * FROM BillingMasterYear ORDER BY OName, ActualAccountNumber"
						strSQL = "SELECT " + strWS + "LienRecordNumber AS LRN, * FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmp";
						// kgk ORDER BY OName, ActualAccountNumber"
					}
				}
				// kgk  rsData.CreateStoredProcedure "LienEditReportQuery", strSQL, strUTDatabase
				modMain.Statics.gstrLienEditReportQuery = strSQL;
				//Application.DoEvents();
				// kgk  strSQL = "SELECT * FROM LienEditReportQuery"
				// XXX    strSQL = "SELECT * FROM (" & gstrLienEditReportQuery & ") AS qTmp2"
				strSQL = modMain.Statics.gstrLienEditReportQuery + " ORDER BY OName, ActualAccountNumber";
				BuildSQL = strSQL;
				return BuildSQL;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building SQL", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildSQL;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the fields in
				double dblCurInt = 0;
				double dblTotal = 0;
				double dblPrin = 0;
				double dblInt = 0;
				double dblCosts = 0;
				float lngNextLine = 0;
				string strMapLot = "";
				string strLocation = "";
				string strBookPage = "";
				string strAddress = "";
				bool boolCO = false;
				int lngLRN = 0;
				clsDRWrapper rsMulti = new clsDRWrapper();
				bool boolDiscardAcct;
				clsDRWrapper rsTotals = new clsDRWrapper();
				clsDRWrapper rsTmp = new clsDRWrapper();
				int lngAccount = 0;
				// Latest Tenant/Owner Information
				string strLastOwner;
				string strLastOwner2;
				string strLastAddress;
				string strLastAddress2;
				string strLastAddress3;
				string strLastCity;
				string strLastState;
				string strLastZip;
				clsDRWrapper rsRecords = new clsDRWrapper();
				string strWS;
				double dblOldestAmount;
				double dblTempInt = 0;
				// kgk 06-01-11  trout-693  Current int and PLI mixed up on report for liens
				TRYAGAIN:
				// this is where the program will start again if the current account is not acceptable
				dblOldestAmount = 0;
				fldCO.Top = 0;
				fldCO.Visible = false;
				fldCO.Text = "";
				strLastOwner = "";
				strLastOwner2 = "";
				strLastAddress = "";
				strLastAddress2 = "";
				strLastAddress3 = "";
				strLastCity = "";
				strLastState = "";
				strLastZip = "";
				strWS = Strings.Left(strWaterSewer, 1);
				boolDiscardAcct = false;
				if (rsData.EndOfFile() != true)
				{
					// If rsData.Fields("ActualAccountNumber") = 73 Then
					// MsgBox "Stop"
					// End If
					// MAL@20071015: Add support for using latest owner name
					// Call Reference: 115027
					if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "LIENEDIT"))
					{
						modMain.GetLatestOwnerInformation(rsData.Get_Fields_Int32("AccountKey"), "LIENEDIT", ref strLastOwner, ref strLastOwner2, ref strLastAddress, ref strLastAddress2, ref strLastAddress3, ref strLastCity, ref strLastState, ref strLastZip);
					}
					else
					{
						strLastOwner = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName")));
						strLastOwner2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName2")));
						strLastAddress = FCConvert.ToString(rsData.Get_Fields_String("OAddress1"));
						strLastAddress2 = FCConvert.ToString(rsData.Get_Fields_String("OAddress2"));
						strLastAddress3 = FCConvert.ToString(rsData.Get_Fields_String("OAddress3"));
						strLastCity = FCConvert.ToString(rsData.Get_Fields_String("OCity"));
						strLastState = FCConvert.ToString(rsData.Get_Fields_String("OState"));
						// kgk 07-31-2012  GetLatestOwnerInformation adds Zip+4 to strLastZip    strLastZip = rsData.Fields("OZip")
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OZip4"))) != "")
						{
							strLastZip = rsData.Get_Fields_String("OZip") + "-" + rsData.Get_Fields_String("OZip4");
						}
						else
						{
							strLastZip = FCConvert.ToString(rsData.Get_Fields_String("OZip"));
						}
					}
					frmWait.InstancePtr.IncrementProgress();
					// If rsData.Fields(strWS & "LienRecordNumber") <> 0 Then
					// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
					lngLRN = FCConvert.ToInt32(rsData.Get_Fields("LRN"));
					if (lngLRN != 0)
					{
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						if (rsData.Get_Fields(strWS + "CombinationLienKey") == rsData.Get_Fields("Bill"))
						{
							if (intLienAccts == 2)
							{
								rsLN.FindFirstRecord("ID", rsData.Get_Fields_Int32("LienKey"));
							}
							else
							{
								rsLN.FindFirstRecord("ID", lngLRN);
							}
						}
						else
						{
							rsData.MoveNext();
							goto TRYAGAIN;
						}
						if (!rsLN.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
							dblPrin = rsLN.Get_Fields("Principal");
							// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
							dblInt = rsLN.Get_Fields("Interest") - rsLN.Get_Fields("IntAdded");
							// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							dblCosts = rsLN.Get_Fields("MaturityFee");
							dblCurInt = 0;
							// MAL@20081118: Add loop for multiple rate keys to get total
							// Tracker Reference: 16227
							lngAccount = FCConvert.ToInt32(rsData.Get_Fields_Int32("ActualAccountNumber"));
							rsRecords.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrLienEditReportQuery + ") AS qTmpY WHERE ActualAccountNumber = " + FCConvert.ToString(lngAccount), modExtraModules.strUTDatabase);
							dblTotal = 0;
							// MAL@20071105: Change the way this loops through to grab all account records
							if (rsRecords.RecordCount() > 0)
							{
								rsRecords.MoveFirst();
								while (!rsRecords.EndOfFile())
								{
									// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
									if (FCConvert.ToInt32(rsRecords.Get_Fields("LRN")) != 0)
									{
										// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
										if (rsRecords.Get_Fields(strWS + "CombinationLienKey") == rsRecords.Get_Fields("Bill"))
										{
											// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
											rsLN.FindFirstRecord("ID", rsRecords.Get_Fields("LRN"));
											dblTotal += modUTCalculations.CalculateAccountUTLien(rsLN, dtMailDate, ref dblCurInt, boolWater);
											dblInt += dblCurInt;
										}
									}
									else
									{
										dblTotal += modUTCalculations.CalculateAccountUT(rsRecords, dtMailDate, ref dblCurInt, boolWater);
										dblInt += dblCurInt;
									}
									rsRecords.MoveNext();
								}
							}
							// dblTotal = CalculateAccountUTLien(rsLN, dtMailDate, dblCurInt, boolWater)
							if (FCUtils.Round(dblTotal, 2) <= 0)
							{
								// MAL@20080212: Change to not allow accounts with credit amounts to print
								rsData.MoveNext();
								goto TRYAGAIN;
							}
							else
							{
								if (intLienAccts == 5 && dblCosts != 0)
								{
									// this will not show accounts taht have lien maturity fees added to it already
									rsData.MoveNext();
									goto TRYAGAIN;
								}
								// MAL@20071018: Check for duplicate account
								// Tracker Reference: 11053
								// MAL@20071217: Changed location of duplicate check
								// Tracker Reference: 11654
								if (objDictionary.ContainsKey(rsData.Get_Fields_Int32("AccountKey")))
								{
									rsData.MoveNext();
									goto TRYAGAIN;
								}
								else
								{
									objDictionary.Add(rsData.Get_Fields_Int32("AccountKey"), objDictionary.Count + 1);
								}
								lngNumOfAccounts += 1;
								// MAL@20071015: From this point forward, replaced all rsData.Fields calls with string values for name and address
								fldAccount.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey")));
								// MAL@20081118: Loop through all records to get true total
								// Tracker Reference: 16227
								// reset the totals
								dblPrin = 0;
								dblInt = 0;
								dblCurInt = 0;
								dblTotal = 0;
								dblCurInt = 0;
								dblCosts = 0;
								rsMulti.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrLienEditReportQuery + ") AS qTmpY WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND RateKey IN " + strRateKeyList, modExtraModules.strUTDatabase);
								while (!rsMulti.EndOfFile())
								{
									switch (intLienAccts)
									{
										case 0:
											{
												// all
												// fldName.Text = "*" & rsData.Fields("OName1")
												fldName.Text = "*" + strLastOwner;
												// dblPrin = dblPrin - rsData.Fields(rsData.Fields("Service") & "PrinPaid")
												// dblInt = dblInt - rsData.Fields(rsData.Fields("Service") & "IntPaid")
												dblPrin += (rsMulti.Get_Fields(strWS + "PrinOwed") + rsMulti.Get_Fields(strWS + "TaxOwed") - rsMulti.Get_Fields(strWS + "PrinPaid") - rsMulti.Get_Fields(strWS + "TaxPaid"));
												dblCurInt += dblInt + (rsMulti.Get_Fields(strWS + "IntOwed") + (rsMulti.Get_Fields(strWS + "IntAdded") * -1) - rsMulti.Get_Fields(strWS + "IntPaid"));
												dblCosts += rsMulti.Get_Fields(strWS + "CostOwed") - rsMulti.Get_Fields(strWS + "CostAdded") - rsMulti.Get_Fields(strWS + "CostPaid");
												break;
											}
										case 1:
											{
												// prelien
												// fldName.Text = rsData.Fields("OName")
												fldName.Text = strLastOwner;
												// dblPrin = dblPrin - rsData.Fields(rsData.Fields("Service") & "PrinPaid")
												// dblInt = dblInt - rsData.Fields(rsData.Fields("Service") & "IntPaid")
												dblPrin += (rsMulti.Get_Fields(strWS + "PrinOwed") + rsMulti.Get_Fields(strWS + "TaxOwed") - rsMulti.Get_Fields(strWS + "PrinPaid") - rsMulti.Get_Fields(strWS + "TaxPaid"));
												dblCurInt += dblInt + (rsMulti.Get_Fields(strWS + "IntOwed") + (rsMulti.Get_Fields(strWS + "IntAdded") * -1) - rsMulti.Get_Fields(strWS + "IntPaid"));
												dblCosts += rsMulti.Get_Fields(strWS + "CostOwed") - rsMulti.Get_Fields(strWS + "CostAdded") - rsMulti.Get_Fields(strWS + "CostPaid");
												break;
											}
										case 2:
											{
												// liened
												// fldName.Text = "*" & rsData.Fields("OName")
												fldName.Text = "*" + strLastOwner;
												// dblPrin = dblPrin + (rsMulti.Fields(strWS & "PrinOwed") + rsMulti.Fields(strWS & "TaxOwed") - rsMulti.Fields(strWS & "PrinPaid") - rsMulti.Fields(strWS & "TaxPaid"))
												// dblCurInt = dblCurInt + dblInt + (rsMulti.Fields(strWS & "IntOwed") + (rsMulti.Fields(strWS & "IntAdded") * -1) - rsMulti.Fields(strWS & "IntPaid"))
												// dblCosts = dblCosts + rsMulti.Fields(strWS & "CostOwed") - rsMulti.Fields(strWS & "CostAdded") - rsMulti.Fields(strWS & "CostPaid")
												// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
												if (rsMulti.Get_Fields(strWS + "CombinationLienKey") == rsMulti.Get_Fields("Bill"))
												{
													// kgk 06-01-11  trout-693  Current int and PLI mixed up on report, Principal incorrect for multiple liens
													// dblPrin = dblPrin + (rsLN.Fields("Principal") + rsLN.Fields("Tax") - rsLN.Fields("PrinPaid") - rsLN.Fields("TaxPaid"))
													// dblCurInt = dblCurInt + dblInt + (rsLN.Fields("Interest") + (rsLN.Fields("IntAdded") * -1) - rsLN.Fields("IntPaid") - rsLN.Fields("PLIPaid"))
													// dblCosts = dblCosts + rsLN.Fields("Costs") - rsLN.Fields("CostPaid")
													// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
													dblPrin += (rsMulti.Get_Fields("Principal") + rsMulti.Get_Fields("Tax") - rsMulti.Get_Fields("PrinPaid") - rsMulti.Get_Fields("TaxPaid"));
													// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
													dblInt += (rsMulti.Get_Fields("Interest") - rsMulti.Get_Fields("PLIPaid"));
													// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
													dblCurInt += ((rsMulti.Get_Fields("IntAdded") * -1) - rsMulti.Get_Fields("IntPaid"));
													// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
													dblCosts += rsMulti.Get_Fields("Costs") - rsMulti.Get_Fields_Double("CostPaid");
												}
												break;
											}
										case 3:
											{
												// eligible for 30 day notice
												// fldName.Text = rsData.Fields("OName")
												fldName.Text = strLastOwner;
												// dblPrin = dblPrin - rsData.Fields(rsData.Fields("Service") & "PrinPaid")
												// dblInt = dblInt - rsData.Fields(rsData.Fields("Service") & "IntPaid")
												dblPrin += (rsMulti.Get_Fields(strWS + "PrinOwed") + rsMulti.Get_Fields(strWS + "TaxOwed") - rsMulti.Get_Fields(strWS + "PrinPaid") - rsMulti.Get_Fields(strWS + "TaxPaid"));
												dblCurInt += dblInt + (rsMulti.Get_Fields(strWS + "IntOwed") + (rsMulti.Get_Fields(strWS + "IntAdded") * -1) - rsMulti.Get_Fields(strWS + "IntPaid"));
												dblCosts += rsMulti.Get_Fields(strWS + "CostOwed") - rsMulti.Get_Fields(strWS + "CostAdded") - rsMulti.Get_Fields(strWS + "CostPaid");
												break;
											}
										case 4:
											{
												// eligible for lien
												// fldName.Text = rsData.Fields("OName1")
												fldName.Text = strLastOwner;
												// MAL@20080422
												// dblPrin = dblPrin - rsData.Fields(rsData.Fields("Service") & "PrinPaid")
												// dblInt = dblInt - rsData.Fields(rsData.Fields("Service") & "IntPaid")
												dblPrin += (rsMulti.Get_Fields(strWS + "PrinOwed") + rsMulti.Get_Fields(strWS + "TaxOwed") - rsMulti.Get_Fields(strWS + "PrinPaid") - rsMulti.Get_Fields(strWS + "TaxPaid"));
												dblCurInt += dblInt + (rsMulti.Get_Fields(strWS + "IntOwed") + (rsMulti.Get_Fields(strWS + "IntAdded") * -1) - rsMulti.Get_Fields(strWS + "IntPaid"));
												dblCosts += rsMulti.Get_Fields(strWS + "CostOwed") - rsMulti.Get_Fields(strWS + "CostAdded") - rsMulti.Get_Fields(strWS + "CostPaid");
												break;
											}
										case 5:
											{
												// eligible for lien maturity
												// fldName.Text = "*" & rsData.Fields("OName")
												fldName.Text = "*" + strLastOwner;
												// dblPrin = dblPrin + (rsMulti.Fields(strWS & "PrinOwed") + rsMulti.Fields(strWS & "TaxOwed") - rsMulti.Fields(strWS & "PrinPaid") - rsMulti.Fields(strWS & "TaxPaid"))
												// dblCurInt = dblCurInt + dblInt + (rsMulti.Fields(strWS & "IntOwed") + (rsMulti.Fields(strWS & "IntAdded") * -1) - rsMulti.Fields(strWS & "IntPaid"))
												// dblCosts = dblCosts + rsMulti.Fields(strWS & "CostOwed") - rsMulti.Fields(strWS & "CostAdded") - rsMulti.Fields(strWS & "CostPaid")
												// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
												if (rsMulti.Get_Fields(strWS + "CombinationLienKey") == rsMulti.Get_Fields("Bill"))
												{
													// kgk 06-01-11  trout-693  Current int and PLI mixed up on report, Principal incorrect for multiple liens
													// dblPrin = dblPrin + (rsLN.Fields("Principal") + rsLN.Fields("Tax") - rsLN.Fields("PrinPaid") - rsLN.Fields("TaxPaid"))
													// dblCurInt = dblCurInt + dblInt + (rsLN.Fields("Interest") + (rsLN.Fields("IntAdded") * -1) - rsLN.Fields("IntPaid") - rsLN.Fields("PLIPaid"))
													// dblCosts = dblCosts + rsLN.Fields("Costs") - rsLN.Fields("CostPaid")
													// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
													dblPrin += (rsMulti.Get_Fields("Principal") + rsMulti.Get_Fields("Tax") - rsMulti.Get_Fields("PrinPaid") - rsMulti.Get_Fields("TaxPaid"));
													// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
													dblInt += (rsMulti.Get_Fields("Interest") - rsMulti.Get_Fields("PLIPaid"));
													// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
													// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
													dblCurInt += ((rsMulti.Get_Fields("IntAdded") * -1) - rsMulti.Get_Fields("IntPaid"));
													// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
													dblCosts += rsMulti.Get_Fields("Costs") - rsMulti.Get_Fields_Double("CostPaid");
												}
												break;
											}
									}
									//end switch
									// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
									if (rsMulti.Get_Fields(strWS + "CombinationLienKey") == rsMulti.Get_Fields("Bill"))
									{
										// kgk 06-01-11  trout-693  Current int and PLI mixed up on report for liens
										// dblTotal = dblTotal + CalculateAccountUTLien(rsMulti, dtMailDate, dblInt, boolWater)
										dblTotal += modUTCalculations.CalculateAccountUTLien(rsMulti, dtMailDate, ref dblTempInt, boolWater);
										dblCurInt += dblTempInt;
									}
									rsMulti.MoveNext();
								}
								fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
								fldPLInt.Text = Strings.Format(dblInt, "#,##0.00");
								fldCosts.Text = Strings.Format(dblCosts, "#,##0.00");
								fldCurrentInt.Text = Strings.Format(dblCurInt, "#,##0.00");
								dblTotals[0] += dblPrin;
								dblTotals[1] += dblInt;
								dblTotals[2] += dblCosts;
								dblTotals[3] += dblCurInt;
								if (FCUtils.Round(dblTotal, 2) == FCUtils.Round(dblPrin + dblInt + dblCosts + dblCurInt, 2))
								{
									fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
								}
								else
								{
									fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
									// "*" & Format(dblPrin + dblInt + dblcosts + dblCurInt , "#,##0.00")
								}
							}
						}
						else
						{
							fldTotal.Text = "ERROR";
						}
					}
					else
					{
						// If rsData.Fields("ActualAccountNumber") = 57 Then
						// MsgBox "Stop"
						// End If
						// MAL@20080229: Added check for duplicate here as well
						// Tracker Reference: 12524
						if (objDictionary.ContainsKey(rsData.Get_Fields_Int32("AccountKey")))
						{
							rsData.MoveNext();
							goto TRYAGAIN;
						}
						else
						{
							objDictionary.Add(rsData.Get_Fields_Int32("AccountKey"), objDictionary.Count + 1);
						}
						if (intLienAccts == 3)
						{
							rsTotals.OpenRecordset("SELECT ActualAccountNumber, SUM(" + strWS + "PrinOwed) as TotalPrinOwed, SUM(" + strWS + "TaxOwed) as TotalTaxOwed, SUM(" + strWS + "IntOwed) as TotalIntOwed, SUM(" + strWS + "IntAdded) as TotalIntAdded, SUM(" + strWS + "CostOwed) as TotalCostOwed, SUM(" + strWS + "CostAdded) as TotalCostAdded, SUM(" + strWS + "PrinPaid) as TotalPrinPaid, SUM(" + strWS + "TaxPaid) as TotalTaxPaid, SUM(" + strWS + "IntPaid) as TotalIntPaid, SUM(" + strWS + "CostPaid) as TotalCostPaid FROM (" + modMain.Statics.gstrLienEditReportQuery + ") AS qTmpY WHERE ISNULL(" + strWS + "DemandGroupID,0) = 0 AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " GROUP By ActualAccountNumber");
						}
						else
						{
							rsTotals.OpenRecordset("SELECT ActualAccountNumber, SUM(" + strWS + "PrinOwed) as TotalPrinOwed, SUM(" + strWS + "TaxOwed) as TotalTaxOwed, SUM(" + strWS + "IntOwed) as TotalIntOwed, SUM(" + strWS + "IntAdded) as TotalIntAdded, SUM(" + strWS + "CostOwed) as TotalCostOwed, SUM(" + strWS + "CostAdded) as TotalCostAdded, SUM(" + strWS + "PrinPaid) as TotalPrinPaid, SUM(" + strWS + "TaxPaid) as TotalTaxPaid, SUM(" + strWS + "IntPaid) as TotalIntPaid, SUM(" + strWS + "CostPaid) as TotalCostPaid FROM (" + modMain.Statics.gstrLienEditReportQuery + ") AS qTmpY WHERE ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " GROUP By ActualAccountNumber");
						}
						if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
						{
							// TODO Get_Fields: Field [TotalPrinOwed] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalTaxOwed] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalPrinPaid] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalTaxPaid] not found!! (maybe it is an alias?)
							dblPrin = rsTotals.Get_Fields("TotalPrinOwed") + rsTotals.Get_Fields("TotalTaxOwed") - rsTotals.Get_Fields("TotalPrinPaid") - rsTotals.Get_Fields("TotalTaxPaid");
						}
						else
						{
							dblPrin = 0;
						}
						dblInt = 0;
						dblCurInt = 0;
						lngAccount = FCConvert.ToInt32(rsData.Get_Fields_Int32("ActualAccountNumber"));
						// kgk 07-31    rsRecords.OpenRecordset "SELECT * FROM LienEditReportQuery WHERE ActualAccountNumber = " & lngAccount, strUTDatabase
						rsRecords.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrLienEditReportQuery + ") AS qTmpY WHERE ActualAccountNumber = " + FCConvert.ToString(lngAccount), modExtraModules.strUTDatabase);
						dblTotal = 0;
						// MAL@20071105: Change the way this loops through to grab all account records
						if (rsRecords.RecordCount() > 0)
						{
							rsRecords.MoveFirst();
							while (!rsRecords.EndOfFile())
							{
								if (boolCheckOnlyOldestBill)
								{
									if (lngOldestRK == FCConvert.ToInt32(rsRecords.Get_Fields_Int32("BillingRateKey")))
									{
										dblOldestAmount = modUTCalculations.CalculateAccountUT(rsRecords, dtMailDate, ref dblCurInt, boolWater);
										dblTotal += dblOldestAmount;
										dblInt += dblCurInt;
									}
									else
									{
										dblTotal += modUTCalculations.CalculateAccountUT(rsRecords, dtMailDate, ref dblCurInt, boolWater);
										dblInt += dblCurInt;
									}
								}
								else
								{
									dblTotal += modUTCalculations.CalculateAccountUT(rsRecords, dtMailDate, ref dblCurInt, boolWater);
									dblInt += dblCurInt;
								}
								rsRecords.MoveNext();
							}
						}
						// Do
						// dblTotal = dblTotal + CalculateAccountUT(rsData, dtMailDate, dblCurInt, boolWater)
						// dblInt = dblInt + dblCurInt
						// rsData.MoveNext
						// If rsData.EndOfFile Then
						// Exit Do
						// End If
						// Loop While rsData.Fields("ActualAccountNumber") = lngAccount
						// rsData.MovePrevious
						if (rsTotals.EndOfFile() != true && rsTotals.BeginningOfFile() != true)
						{
							// TODO Get_Fields: Field [TotalIntOwed] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalIntAdded] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalIntPaid] not found!! (maybe it is an alias?)
							dblCurInt = dblInt + (rsTotals.Get_Fields("TotalIntOwed") + (rsTotals.Get_Fields("TotalIntAdded") * -1) - rsTotals.Get_Fields("TotalIntPaid"));
						}
						else
						{
							dblCurInt = dblInt;
						}
						dblInt = 0;
						// If Round(dblTotal, 2) <= dblMinimumAmount Or (boolCheckOnlyOldestBill And dblOldestAmount = 0) Then      'if the total for the account = 0 then do not show this account
						if (FCUtils.Round(dblTotal, 2) <= dblMinimumAmount || (boolCheckOnlyOldestBill && dblOldestAmount <= 0))
						{
							// if the total for the account = 0 then do not show this account
							if (FCUtils.Round(dblTotal, 2) == 0)
							{
								lngZeroBal += 1;
							}
							else
							{
								lngNegBal += 1;
							}
							rsData.MoveNext();
							goto TRYAGAIN;
						}
						else
						{
							lngNumOfAccounts += 1;
							if (Strings.InStr(1, strRateKeyList, ",", CompareConstants.vbBinaryCompare) != 0 && (intLienAccts == 2 || intLienAccts == 3 || intLienAccts == 5 || intLienAccts == 4))
							{
								fldAccount.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey")));
								fldName.Text = strLastOwner;
								// Trim(rsData.Fields("OName"))
								// reset the totals
								dblPrin = 0;
								dblInt = 0;
								dblCurInt = 0;
								dblTotal = 0;
								dblCurInt = 0;
								dblCosts = 0;
								if (intLienAccts == 3)
								{
									rsMulti.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrLienEditReportQuery + ") AS LERQ WHERE ISNULL(" + strWS + "DemandGroupID,0) = 0 AND AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND BillingRateKey IN " + strRateKeyList, modExtraModules.strUTDatabase);
								}
								else
								{
									rsMulti.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstrLienEditReportQuery + ") AS LERQ WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND BillingRateKey IN " + strRateKeyList, modExtraModules.strUTDatabase);
								}
								while (!rsMulti.EndOfFile())
								{
									// TODO Get_Fields: Field [LRN] not found!! (maybe it is an alias?)
									if (FCConvert.ToInt32(rsMulti.Get_Fields("LRN")) == 0)
									{
										dblPrin += rsMulti.Get_Fields(strWS + "PrinOwed") + rsMulti.Get_Fields(strWS + "TaxOwed") - rsMulti.Get_Fields(strWS + "PrinPaid") - rsMulti.Get_Fields(strWS + "TaxPaid");
										dblInt = 0;
										// dblCurInt = 0
										dblTotal += modUTCalculations.CalculateAccountUT(rsMulti, dtMailDate, ref dblInt, boolWater);
										dblCurInt += dblInt + (rsMulti.Get_Fields(strWS + "IntOwed") + (rsMulti.Get_Fields(strWS + "IntAdded") * -1) - rsMulti.Get_Fields(strWS + "IntPaid"));
										dblCosts += rsMulti.Get_Fields(strWS + "CostOwed") - rsMulti.Get_Fields(strWS + "CostAdded") - rsMulti.Get_Fields(strWS + "CostPaid");
										// set the eligibility to 1 so that this account can get to the next step of the lien process if it is at a zero eligibility
										// do not set the other BACK from a higher eligibility
										if (Conversion.Val(rsMulti.Get_Fields(strWS + "LienStatusEligibility")) == 0)
										{
											// k                               rsMulti.Edit
											// rsMulti.Fields(strWS & "LienStatusEligibility") = 1
											// rsMulti.Update True
											rsTmp.Execute("UPDATE Bill SET " + strWS + "LienStatusEligibility = 1 WHERE Bill.ID = " + rsData.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
										}
									}
									rsMulti.MoveNext();
								}
								dblInt = 0;
								// there should be no PLI, this is just an accumulator for dblCutInt
								fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
								fldPLInt.Text = Strings.Format(dblInt, "#,##0.00");
								fldCosts.Text = Strings.Format(dblCosts, "#,##0.00");
							}
							else
							{
								// set the eligibility to 1 so that this account can get to the next step of the lien process if it is at a zero eligibility
								// do not set the other BACK from a higher eligibility
								if (Conversion.Val(rsData.Get_Fields(strWS + "LienStatusEligibility")) == 0)
								{
									// k                       rsData.Edit
									// rsData.Fields(strWS & "LienStatusEligibility") = 1
									// rsData.Update True
									rsTmp.Execute("UPDATE Bill SET " + strWS + "LienStatusEligibility = 1 WHERE Bill.ID = " + rsData.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
								}
								fldAccount.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey")));
								// fldName.Text = Trim(rsData.Fields("OName"))
								fldName.Text = strLastOwner;
								fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
								fldPLInt.Text = Strings.Format(dblInt, "#,##0.00");
								// TODO Get_Fields: Field [TotalCostOwed] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalCostAdded] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalCostPaid] not found!! (maybe it is an alias?)
								fldCosts.Text = Strings.Format(rsTotals.Get_Fields("TotalCostOwed") - rsTotals.Get_Fields("TotalCostAdded") - rsTotals.Get_Fields("TotalCostPaid"), "#,##0.00");
							}
							// TODO Get_Fields: Field [TotalCostAdded] not found!! (maybe it is an alias?)
							if (rsTotals.Get_Fields("TotalCostAdded") < 0)
							{
								// keep track of the number of accounts dealt with (show this later)
								lngDemandAlready += 1;
							}
							else
							{
								lngPosBal += 1;
							}
							// dblCurInt = dblCurInt * -1
							fldCurrentInt.Text = Strings.Format(dblCurInt, "#,##0.00");
							dblTotals[0] += dblPrin;
							dblTotals[1] += dblInt;
							// TODO Get_Fields: Field [TotalCostOwed] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCostAdded] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCostPaid] not found!! (maybe it is an alias?)
							dblTotals[2] += rsTotals.Get_Fields("TotalCostOwed") - rsTotals.Get_Fields("TotalCostAdded") - rsTotals.Get_Fields("TotalCostPaid");
							dblTotals[3] += dblCurInt;
							// TODO Get_Fields: Field [TotalCostOwed] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCostAdded] not found!! (maybe it is an alias?)
							// TODO Get_Fields: Field [TotalCostPaid] not found!! (maybe it is an alias?)
							if (FCUtils.Round(dblTotal, 2) == FCUtils.Round((dblPrin + dblInt + rsTotals.Get_Fields("TotalCostOwed") - rsTotals.Get_Fields("TotalCostAdded") - rsTotals.Get_Fields("TotalCostPaid") + dblCurInt), 2))
							{
								// - (rsData.Fields("PrincipalPaid") + rsData.Fields("InterestPaid") + rsData.Fields("DemandFeesPaid")), 2) Then
								fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
							}
							else
							{
								// TODO Get_Fields: Field [TotalCostOwed] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalCostAdded] not found!! (maybe it is an alias?)
								// TODO Get_Fields: Field [TotalCostPaid] not found!! (maybe it is an alias?)
								fldTotal.Text = Strings.Format((dblPrin + dblInt + rsTotals.Get_Fields("TotalCostOwed") - rsTotals.Get_Fields("TotalCostAdded") - rsTotals.Get_Fields("TotalCostPaid") + dblCurInt), "#,##0.00");
								// fldTotal.Text = Format(dblTotal, "#,##0.00") '"*" & Format((dblPrin + dblInt + rsData.Fields("DemandFees") + dblCurInt), "#,##0.00") '- (rsData.Fields("PrincipalPaid") + rsData.Fields("InterestPaid") + rsData.Fields("DemandFeesPaid")), "#,##0.00")
							}
						}
						// If intLienAccts = 3 Or intLienAccts = 4 Then
						boolDiscardAcct = true;
						// End If
					}
					fldCO.Text = "";
					boolCO = false;
					lngNextLine = 240 / 1440F;
					fldCO.Visible = true;
					rsMast.FindFirstRecord("ID", rsData.Get_Fields_Int32("AccountKey"));
					if (!rsMast.NoMatch)
					{
						if (FCConvert.ToBoolean(rsMast.Get_Fields_Boolean("INBankruptcy")))
						{
							// if the account is in bankruptcy then put parenthesis around the account number and show the disclaimer
							fldAccount.Text = "(" + fldAccount.Text + ")";
							lblBankruptDisclaimer.Visible = true;
						}
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						strLocation = "Location: " + Strings.Trim(rsMast.Get_Fields("StreetNumber") + " " + Strings.Trim(rsMast.Get_Fields_String("Apt") + " " + rsMast.Get_Fields_String("StreetName")));
						strMapLot = "Map Lot : " + Strings.Trim(FCConvert.ToString(rsMast.Get_Fields_String("MapLot")));
						strAddress = "";
						if (strLastAddress != "")
						{
							strAddress = strLastAddress + "\r\n";
						}
						if (strLastAddress2 != "")
						{
							strAddress += strLastAddress2 + "\r\n";
						}
						if (strLastAddress3 != "")
						{
							strAddress += strLastAddress;
						}
						strAddress += " " + Strings.Trim(strLastState + " " + strLastZip);
						// kgk 07-31-2012  strLastZip should include Zip+4
						// If rsMast.Fields("OZip4") <> "" Then
						// strAddress = strAddress & "-" & rsMast.Fields("OZip4")
						// End If
						if (Strings.Trim(strLastOwner2) != "")
						{
							fldCO.Top = 240 / 1440F;
							fldCO.Visible = true;
							fldCO.Text = Strings.Trim(strLastOwner2) + " ";
							lngNextLine = 480 / 1440F;
							boolCO = true;
						}
						// MAL@20071018: Commented out to avoid duplicating last owner name
						// If Trim(strLastOwner) = Trim(rsData.Fields("BName")) Then
						// fldCO.Top = 0
						// fldCO.Visible = False
						// fldCO.Text = ""
						// lngNextLine = 240
						// Else
						// fldCO.Top = 240
						// fldCO.Visible = True
						// fldCO.Text = fldCO.Text & "C/O " & Trim(strLastOwner & " " & strLastOwner2)
						// lngNextLine = 480
						// boolCO = True
						// End If
						// If rsMast.Fields("OAddress1") <> "" Then
						// strAddress = rsMast.Fields("OAddress1") & vbCrLf
						// End If
						// If rsMast.Fields("OAddress2") <> "" Then
						// strAddress = strAddress & rsMast.Fields("OAddress2") & vbCrLf
						// End If
						// If rsMast.Fields("OAddress3") <> "" Then
						// strAddress = strAddress & rsMast.Fields("OAddress3")
						// End If
						// strAddress = strAddress & " " & Trim(rsMast.Fields("OState") & " " & rsMast.Fields("OZip"))
						// If rsMast.Fields("OZip4") <> "" Then
						// strAddress = strAddress & "-" & rsMast.Fields("OZip4")
						// End If
						// If Trim(rsData.Fields("OName2")) <> "" Then
						// fldCO.Top = 240
						// fldCO.Visible = True
						// fldCO.Text = Trim(rsData.Fields("OName2")) & " "
						// lngNextLine = 480
						// boolCO = True
						// End If
						// If Trim(rsMast.Fields("OwnerName")) = Trim(rsData.Fields("BName")) Then
						// fldCO.Top = 0
						// fldCO.Visible = False
						// fldCO.Text = ""
						// lngNextLine = 240
						// Else
						// fldCO.Top = 240
						// fldCO.Visible = True
						// fldCO.Text = fldCO.Text & "C/O " & Trim(rsMast.Fields("OwnerName") & " " & rsMast.Fields("SecondOwnerName"))
						// lngNextLine = 480
						// boolCO = True
						// End If
					}
					else
					{
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BName2"))) != "")
						{
							fldCO.Top = 240 / 1440F;
							fldCO.Visible = true;
							fldCO.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BName2"))) + " ";
							lngNextLine = 480 / 1440F;
							boolCO = true;
						}
						else
						{
							fldCO.Top = 0;
							fldCO.Visible = false;
							fldCO.Text = "";
							lngNextLine = 240 / 1440F;
							boolCO = false;
						}
					}
					if (boolShowMailingAddress)
					{
						fldAddress.Top = lngNextLine;
						fldAddress.Visible = true;
						fldAddress.Text = strAddress;
					}
					if (boolShowMapLot)
					{
						fldMapLot.Visible = true;
						fldMapLot.Text = strMapLot;
						fldMapLot.Top = lngNextLine;
						lngNextLine += 240 / 1440F;
					}
					if (boolShowLocation)
					{
						fldLocation.Top = lngNextLine;
						lngNextLine += 240 / 1440F;
						fldLocation.Visible = true;
						fldLocation.Text = strLocation;
					}
					if (boolShowBookPage)
					{
						// find the bookpage string for this account
						strBookPage = Strings.Trim(rsData.Get_Fields_String("BookPage") + " ");
						fldBookPage.Top = lngNextLine;
						lngNextLine += 240 / 1440F;
						fldBookPage.Visible = true;
						fldBookPage.Text = strBookPage;
					}
					if (boolShowMailingAddress)
					{
						if (boolCO)
						{
							lngNextLine = 1080 / 1440F;
						}
						else
						{
							lngNextLine = 900 / 1440F;
						}
					}
					if (lngExtraLines > 0)
					{
						lngNextLine += (lngExtraLines * 180) / 1440F;
					}
					Detail.Height = lngNextLine;
					switch (intReportDetail)
					{
						case 1:
						case 2:
							{
								sarLienEditMortHolders.Top = lngNextLine + (100 / 1440F);
								sarLienEditMortHolders.Report.UserData = modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"));
								break;
							}
						default:
							{
								Detail.Height = lngNextLine;
								break;
							}
					}
					//end switch
					lblExtraLines.Top = lngNextLine - lblExtraLines.Height;
					rsData.MoveNext();
					if (!rsData.EndOfFile())
					{
						if (rsData.Get_Fields_Int32("ActualAccountNumber") == Conversion.Val(fldAccount.Text))
						{
							// MAL@20070907: Add check to see if new record account matches previous before looping through
							while (!(rsData.Get_Fields_Int32("ActualAccountNumber") != Conversion.Val(fldAccount.Text)))
							{
								rsData.MoveNext();
								if (rsData.EndOfFile())
								{
									break;
								}
							}
						}
					}
				}
				else
				{
					fldTotal.Text = "";
					fldPrincipal.Text = "";
					fldAccount.Text = "";
					fldCosts.Text = "";
					fldCurrentInt.Text = "";
					fldPLInt.Text = "";
					fldName.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// fill the totals line in
			lblTotals.Text = "Count: " + FCConvert.ToString(lngNumOfAccounts) + "   Totals:";
			fldTotalPrincipal.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotalPLInt.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldTotalCurrentInt.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[3], "#,##0.00");
		}

		private void HideAllFields()
		{
			lblCosts.Visible = false;
			lblCurrentInt.Visible = false;
			lblName.Visible = false;
			lblPLInt.Visible = false;
			lblPrincipal.Visible = false;
			lblReportType.Visible = false;
			lblTotal.Visible = false;
			lblTotals.Visible = false;
			Line1.Visible = false;
			Line2.Visible = false;
			fldTotalCosts.Visible = false;
			fldTotalCurrentInt.Visible = false;
			fldTotalPLInt.Visible = false;
			fldTotalPrincipal.Visible = false;
			fldTotalTotal.Visible = false;
		}

		private void SetupDetailSection()
		{
			// this will set the height of the detail section and the visibility of the sub report
			switch (intReportDetail)
			{
				case 0:
					{
						sarLienEditMortHolders.Visible = false;
						sarLienEditMortHolders.Top = 0;
						break;
					}
				case 1:
				case 2:
					{
						sarLienEditMortHolders.Visible = true;
						sarLienEditMortHolders.Top = sarLienEditMortHolders.Top;
						sarLienEditMortHolders.Report = new sarLienEditMortHolders();
						break;
					}
			}
			//end switch
		}

		private void arLienProcessEditReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arLienProcessEditReport properties;
			//arLienProcessEditReport.Caption	= "Lien Process Edit Report";
			//arLienProcessEditReport.Icon	= "arLienProcessEditReport.dsx":0000";
			//arLienProcessEditReport.Left	= 0;
			//arLienProcessEditReport.Top	= 0;
			//arLienProcessEditReport.Width	= 18585;
			//arLienProcessEditReport.Height	= 10515;
			//arLienProcessEditReport.StartUpPosition	= 3;
			//arLienProcessEditReport.SectionData	= "arLienProcessEditReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
