﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using Wisej.Web;

namespace TWUT0000
{
    /// <summary>
    /// Summary description for frmBillsPreview.
    /// </summary>
    public partial class frmBillsPreview : BaseForm
    {
        public frmBillsPreview()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmBillsPreview InstancePtr
        {
            get
            {
                return (frmBillsPreview)Sys.GetInstance(typeof(frmBillsPreview));
            }
        }

        protected frmBillsPreview _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               12/13/2004              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               09/25/2006              *
        // ********************************************************
        bool FormLvlUp;
        // True if form is exiting to higher level in the form hierarchy, false if moving lower
        bool FillGrid;
        int ErrorSeq;
        // holds the sequence that had the error in it
        DateTime dtBDate;
        // this will hold the bill date to show on the transfer report
        bool boolSaved;
        // if the form is being unloaded w/o saving the bills then show a message
        int CurrentBook;
        // holds the current book value
        string strSQL;
        int Consumption;
        // Calculated field (Current Reading - Previous Reading)
        int MaxRows;
        // holds the total rows in the grid
        int i;
        // generic counting variable
        string BillingCode = "";
        // billing code Flat, Units, Consumption
        bool CollapseFlag;
        // True = Collapsing rows do not allow collapse event to happen, False = Allow collapse event to happen
        int CurRow;
        // holds the current row number of the last level 0 row
        int ErrorCheck;
        // flag to watch for errors
        double BookWAdjustment;
        // these variables are to gather info for the calculation
        double BookWMisc;
        // summary screen then pass the information to the summary
        double BookWTax;
        // screen when it is loaded
        int BookWCount;
        double BookWFlat;
        double BookWCons;
        double BookWUnits;
        double BookWOverride;
        double BookSAdjustment;
        double BookSMisc;
        double BookSTax;
        int BookSCount;
        double BookSFlat;
        double BookSCons;
        double BookSUnits;
        double BookSOverride;
        double STotal;
        // holds the sewer total for the meter
        double WTotal;
        // holds the water total for the meter
        double dblSGrandTotal;
        // holds the sewer total for the grid
        double dblWGrandTotal;
        // holds the water total for the grid
        int lngGridColCheck;
        int lngGridColBookNumber;
        int lngGridColWater;
        int lngGridColSewer;
        int lngGRIDCOLHidden;
        int lngGridColBillCount;
        int lngGridColDescription;
        int lngGridColAccountNumber;
        int lngGridColSequence;
        int[] BookArray = null;
        int intCount;
        bool boolCalculate;
        int lngRateKey;
        bool boolCalculatedCorrectly;
        bool boolFinal;
        bool blnWinterBilling;
        clsDRWrapper rsMeter;

        const string numericFormat = "#,##0.00";

        // vbPorter upgrade warning: intCT As short	OnWriteFCConvert.ToInt32(
        public void Init(int intCT, ref int[] PassBookArray, bool boolPassInitial, ref int lngPassRK, bool boolPassFinal = false)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsRate = new clsDRWrapper();
                //FC:FINAL:MSH - issue #1038: in original before changing caption of the form will be called Form_Load() and only after this will be changed caption
                LoadForm();
                Text = "Create Bill Records";
                // "Billing Preview"
                //FC:FINAL:MSH - issue #1038: change header text
                HeaderText.Text = "Create Bill Records";
                intCount = intCT;
                BookArray = PassBookArray;
                boolCalculate = false;
                lngRateKey = lngPassRK;
                boolFinal = boolPassFinal;
                rsRate.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
                dtBDate = rsRate.EndOfFile() ? DateTime.Today : rsRate.Get_Fields_DateTime("BillDate");
                lblWarning.Text = "Press F12 to save the bills and complete the transfer";
                blnWinterBilling = Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON" && MessageBox.Show("Is this a winter billing?", "Winter Billing?", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes;

                if (modUTBilling.CheckBDAccountsSetup())
                {
                    Show(App.MainForm);
                }
                else
                {
                    MessageBox.Show("Please enter the Commitment Account in Budgetary.", "Missing Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Unload();
                }
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void frmBillsPreview_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // catches the escape and enter keys
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                Unload();
            }
            else if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmBillsPreview_Activated(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                if (modMain.FormExist(this))
                    return;
                FormLvlUp = false;
                // initialize variables
                if (FillGrid)
                {
                    Format_Grid();
                    // sets the grid up
                    //Application.DoEvents();
                    if (modGlobalConstants.Statics.gboolRE || modGlobalConstants.Statics.gboolBL)
                    {
                        modMain.UpdateAllUTAccountsWithREInfo();
                    }
                    StartBookCalculation();
                    CollapseFlag = false;
                    // initialize the collapse flag
                }
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Form Activate", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void frmBillsPreview_Load(object sender, System.EventArgs e)
        {
 
            lngGridColCheck = 0;
            lngGridColBookNumber = 1;
            lngGridColSequence = 2;
            lngGridColAccountNumber = 3;
            lngGridColBillCount = 4;
            lngGridColWater = 5;
            lngGridColSewer = 6;
            lngGRIDCOLHidden = 7;
            lngGridColDescription = 8;
            FillGrid = true;
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            if (boolCalculate)
            {
                Text = "Book Calculation & Edit";
                //FC:FINAL:MSH - issue #1038: change header text
                HeaderText.Text = "Book Calculation & Edit";
            }
            else
            {
                Text = "Billed Edit";
                //FC:FINAL:MSH - issue #1038: change header text
                HeaderText.Text = "Billed Edit";
            }
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            if (!boolSaved)
            {
                MessageBox.Show("The bills were not created.  You will have to return to this screen and save the results.", "Bills Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void frmBillsPreview_Resize(object sender, System.EventArgs e)
        {
            Format_Grid();
            vsPreview_Collapsed();
        }

        private void mnuFileExit_Click(object sender, System.EventArgs e)
        {
            Unload();
        }

        private void mnuFileQuit_Click()
        {
            FormLvlUp = true;
            Unload();
        }

        private void mnuFileSave_Click(object sender, System.EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will transfer all of the records
                int lngCT = 0;
                string strType = "";
                int lngBNum = 0;
                int lngI;
                string strBooks = "";
                string strBooksNotBilled = "";
                mnuFileSave.Enabled = false;
                if (boolCalculatedCorrectly)
                {
                    // make a backup right now
                    // kk 07172013   BackupUTDatabase
                    //Application.DoEvents();
                    modGlobalFunctions.AddCYAEntry_26("UT", "Transfered Billing Records", "Rate Key : " + FCConvert.ToString(lngRateKey));
                    lngCT = modUTBilling.FinishBillingRecords(ref lngRateKey, ref BookArray, boolFinal, ref strBooksNotBilled, blnWinterBilling);
                    if (lngCT > 0)
                    {
                        // make sure at least one bill got created
                        // this will set the ending values at the end of the whole process...
                        for (lngI = 1; lngI <= Information.UBound(BookArray, 1); lngI++)
                        {
                            // get the book number from the array
                            lngBNum = BookArray[lngI];

                            if (Strings.InStr(1, strBooksNotBilled, "," + FCConvert.ToString(lngBNum) + ",", CompareConstants.vbBinaryCompare) != 0) 
                                continue;

                            strBooks += FCConvert.ToString(lngBNum) + ",";
                            strType = "BE";
                            if (lngBNum < 10000 && lngBNum > 0)
                            {
                                // make sure that this is a valid book number
                                SetStatus(ref lngBNum, ref strType);
                                // this will set the book status to Billed Finished
                            }
                        }
                        if (lngCT > 1)
                        {
                            MessageBox.Show("There were " + FCConvert.ToString(lngCT) + " bills created.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        else
                        {
                            MessageBox.Show("There was 1 bill created.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                        if (modExtraModules.Statics.gstrRegularIntMethod == "B" && !boolFinal)
                        {
                            // this will calculate all interest on all accounts and can take a few minutes
                            modGlobalFunctions.AddCYAEntry_8("UT", "Calculated All Interest");
                            strBooks = Strings.Left(strBooks, strBooks.Length - 1);
                            // MAL@20080716: Add Bill Date to the passing parameters
                            // Tracker Reference: 14640
                            modUTBilling.CalculateInterestAtBilling_18(strBooks, dtBDate, " AND BillingrateKey <> " + FCConvert.ToString(lngRateKey));
                        }

                        if (modUTCalculations.Statics.gUTSettings.AllowAutoPay)
                        {
                            lngCT = modUTBilling.CreateAutoPayRecs(lngRateKey, BookArray, boolFinal);
                            if (lngCT > 1)
                            {
                                MessageBox.Show(lngCT + " Auto-Pay records were created.", "Records Created", MessageBoxButtons.OK,
                                    MessageBoxIcon.None);
                            }
                            else
                            {
                                MessageBox.Show("1 Auto-Pay record was created.", "Records Created", MessageBoxButtons.OK,
                                    MessageBoxIcon.None);
                            }
                        }
                        //FC:FINAL:DSE:#i1120 Close the current form before opening the new form to set the focus correctly
                        boolSaved = true;
                        Unload();
                        rptBillTransferReportMaster.InstancePtr.Init(ref BookArray, ref lngRateKey, ref dtBDate);
                        // MAL@20080123: Clear Auto Pre-Pay Table
                        // Tracker Reference: 113597
                        modUTBilling.ClearPrepaymentTable();
                    }
                    else
                    {
                        MessageBox.Show("No bills were created.", "No Bills Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //FC:FINAL:DSE:#i1120 Close the current form before opening the new form to set the focus correctly
                        boolSaved = true;
                        Unload();
                    }
                    //FC:FINAL:DSE:#i1120 Close the current form before opening the new form to set the focus correctly
                    //boolSaved = true;
                    //this.Unload();
                }
                else
                {
                    MessageBox.Show("No bills were created.  A book was not calculated correctly.", "No Bills Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    mnuFileSave.Enabled = true;
                }
                return;
            }
            catch (Exception ex)
            {
                
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Bills", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void mnuFileSummary_Click(object sender, System.EventArgs e)
        {
            // this needs to call the billing summary form
        }

        private bool GetBookInfo(ref int BookNum)
        {
            bool ret = false;
            var rsBook = new clsDRWrapper();

            try
            {
                // will show all book information in the flexgrid

                rsBook.OpenRecordset($"SELECT * FROM Book WHERE BookNumber = {FCConvert.ToString(BookNum)}", modExtraModules.strUTDatabase);
                var strCurStatus = Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1);

                if (strCurStatus == "E" || strCurStatus == "B" || boolFinal)
                {
                    AddBookLine_24(BookNum, rsBook.Get_Fields_String("Description"), 0);

                    if (rsBook.RecordCount() <= 0) return false;

                    if (Fill_Grid_18(vsPreview, BookNum, 1))
                    {
                        // calculate each meter and fill the grid with calculated figures
                        if (PreviewBook() || boolFinal)
                        {
                            // if all meters are calculated correctly then
                            ret = true;
                        }
                        else
                        {
                            frmWait.InstancePtr.Unload();
                            MessageBox.Show($"Book #{BookNum} was not processed.  Check that all meter data has been entered.\r\nError in Sequence #{FCConvert.ToString(ErrorSeq)}.", "Status Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                    else
                    {
                        if (!rsMeter.EndOfFile())
                        {
                            MessageBox.Show($"Book #{BookNum} sequence {ErrorSeq} was not processed.", "Error in Book Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("The book chosen is not ready to have the bills previewed." + "\r\n" + "Check the status and try again.", "Status Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                return ret;
            }
            catch (Exception ex)
            {
                
                ret = false;
                MessageBox.Show($"Error #{FCConvert.ToString(Information.Err(ex).Number)} - {Information.Err(ex).Description}.", "Error Getting Book Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsBook.DisposeOf();
            }
            return ret;
        }

        private bool PreviewBook()
        {
            bool ret = false;

            rsMeter = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                string strType = "";

                // this function returns true if all meters are calculated correctly
                strSQL = $"SELECT * FROM MeterTable WHERE BookNumber = {CurrentBook} AND ISNULL(FinalBilled,0) = 0 AND ISNULL(NoBill,0) = 0 ORDER BY Sequence";

                // builds the SQL statement to select all records
                rsMeter.OpenRecordset(strSQL);

                // selects all the meter records
                if (rsMeter.RecordCount() > 0 && rsMeter.EndOfFile() != true && rsMeter.BeginningOfFile() != true)
                {
                    while (!rsMeter.EndOfFile())
                    {
                        // check each meter for the correct billing status
                        var billingStatus = rsMeter.Get_Fields_String("BillingStatus");

                        if (fecherFoundation.FCUtils.IsNull(billingStatus) == false
                            && !(Strings.UCase(FCConvert.ToString(billingStatus)) == "E" || Strings.UCase(FCConvert.ToString(billingStatus)) == "B"))
                        {

                            ErrorSeq = FCConvert.ToInt32(rsMeter.Get_Fields("Sequence"));
                            return false;
                        }

                        rsMeter.MoveNext();
                    }
                }

                ret = true;

                return ret;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Book Status", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

            return ret;
        }

        private void Format_Grid()
        {
            vsPreview.Cols = 9;
            //FC:FINAL:MSH - issue #1038: change columns size for fully displaying text in 'Book' column after reducing size of the grid(form resizing)
            vsPreview.ColWidth(lngGridColCheck, FCConvert.ToInt32(vsPreview.WidthOriginal * 0.01));
            vsPreview.ColWidth(lngGridColBookNumber, FCConvert.ToInt32(vsPreview.WidthOriginal * 0.15));
            vsPreview.ColWidth(lngGridColSequence, FCConvert.ToInt32(vsPreview.WidthOriginal * 0.1));
            vsPreview.ColWidth(lngGridColAccountNumber, FCConvert.ToInt32(vsPreview.WidthOriginal * 0.1));
            vsPreview.ColWidth(lngGridColWater, FCConvert.ToInt32(vsPreview.WidthOriginal * 0.16));
            vsPreview.ColWidth(lngGridColSewer, FCConvert.ToInt32(vsPreview.WidthOriginal * 0.16));
            vsPreview.ColWidth(lngGRIDCOLHidden, 0);
            vsPreview.ColWidth(lngGridColBillCount, FCConvert.ToInt32(vsPreview.WidthOriginal * 0.1));
            vsPreview.ColWidth(lngGridColDescription, FCConvert.ToInt32(vsPreview.WidthOriginal * 0.2));
            vsPreview.ExtendLastCol = true;
            // .MergeCells = flexMergeSpill
            vsPreview.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
            vsPreview.TextMatrix(0, lngGridColBookNumber, "Book");
            vsPreview.TextMatrix(0, lngGridColAccountNumber, "Account");
            vsPreview.TextMatrix(0, lngGridColSequence, "Sequence");
            // kk trouts-6 03012013  Change Water to Stormwater for Bangor
            if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
            {
                vsPreview.TextMatrix(0, lngGridColWater, "Stormwater");
            }
            else
            {
                vsPreview.TextMatrix(0, lngGridColWater, "Water");
            }
            vsPreview.TextMatrix(0, lngGridColSewer, "Sewer");
            vsPreview.TextMatrix(0, lngGRIDCOLHidden, "");
            vsPreview.TextMatrix(0, lngGridColBillCount, "Bill Count");
            vsPreview.TextMatrix(0, lngGridColDescription, "Description");
            //vsPreview.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsPreview.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            vsPreview.ColFormat(lngGridColWater, "#,###.00");
            vsPreview.ColFormat(lngGridColSewer, "#,###.00");
            vsPreview.ColAlignment(lngGridColWater, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsPreview.ColAlignment(lngGridColSewer, FCGrid.AlignmentSettings.flexAlignRightCenter);
            // .FixedCols = 0
            // .ColDataType(lngGridColCheck) = flexDTBoolean
        }

        private void vsPreview_AfterCollapse(object sender, DataGridViewRowEventArgs e)
        {
            vsPreview_Collapsed();
        }
        //private void vsPreview_BeforeScrollTip(object sender, FCGrid._IVSFlexGridEvents_BeforeScrollTipEvent e)
        //{
        //	// this sets the scroll tip to the row and sequence that is at the top of the visible grid
        //	// If vsPreview.RowOutlineLevel(Row) = 0 Then
        //	// vsPreview.ScrollTipText = "Row " & Row & " : Seq #" & vsPreview.TextMatrix(Row, lngGridColSeq)
        //	// End If
        //}
        private void vsPreview_Collapsed()
        {
            // when the a row is collapsed or expanded
            int ExposedRows = 0;
            // keeps track of rows that are not collapsed
            if (CollapseFlag == false)
            {
                ExposedRows = 1;
                for (i = 1; i <= vsPreview.Rows - 1; i++)
                {
                    // check each row
                    if (vsPreview.RowOutlineLevel(i) == 0)
                    {
                        // if it is the upper level row
                        // if row is collapsed
                        if (vsPreview.IsCollapsed(i) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                        {
                            ExposedRows += 1;
                            // count the exposed row (Upper level)
                            i += 1;
                            // move to the next row
                            // check each row for subordinate rows and do
                            // not count the subordinate rows because they are collapsed
                            while (vsPreview.RowOutlineLevel(i) != 0)
                            {
                                if (i < vsPreview.Rows - 1)
                                {
                                    i += 1;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            i -= 1;
                        }
                        else
                        {
                            // if the row is not collapsed then
                            ExposedRows += 1;
                            // count a row
                        }
                    }
                    else
                    {
                        if (vsPreview.RowOutlineLevel(i) == 1)
                        {
                            // if it is the upper level row but not a level 0
                            // if row is collapsed
                            if (vsPreview.IsCollapsed(i) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                            {
                                ExposedRows += 1;
                                // count the exposed row (Upper level)
                                i += 1;
                                // move to the next row
                                // check each row for subordinate rows and do
                                // not count the subordinate rows because they are collapsed
                                while (vsPreview.RowOutlineLevel(i) != 1)
                                {
                                    if (i < vsPreview.Rows - 1)
                                    {
                                        i += 1;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                i -= 1;
                            }
                            else
                            {
                                // if the row is not collapsed then
                                ExposedRows += 1;
                                // count a row
                            }
                        }
                        else
                        {
                            ExposedRows += 1;
                            // if the row is a subordinate row, then count it
                        }
                    }
                }
                // ExposedRows = ExposedRows + 1                      'add one row for the last row
                // this will calculate the height of the flex grid depending on how many rows are open with a max height of the 25% of the form height
                //if (((vsPreview.RowHeight(0) * ExposedRows)) + (vsPreview.RowHeight(0) / 4.0) < (frmBillsPreview.InstancePtr.Height * 0.73) + vsPreview.RowHeight(0))
                //{
                //    vsPreview.Height = FCConvert.ToInt32((vsPreview.RowHeight(0) * ExposedRows) + (vsPreview.RowHeight(0) / 4.0));
                //}
                //else
                //{
                //    vsPreview.Height = FCConvert.ToInt32((frmBillsPreview.InstancePtr.Height * 0.73) + vsPreview.RowHeight(0));
                //}
            }
        }
        // vbPorter upgrade warning: Grid As object	OnWrite(FCGridCtl.VSFlexGrid)
        private bool Fill_Grid_18(FCGrid Grid, int lngBookNum, int intRowOutLineLevel = 0)
        {
            return Fill_Grid(ref Grid, ref lngBookNum, intRowOutLineLevel);
        }

        private bool Fill_Grid(ref FCGrid Grid, ref int lngBookNum, int intRowOutLineLevel = 0)
        {
            bool ret = false;
            var rsB = new clsDRWrapper();

            try
            {

                // temporary variable
                double dblWTotal = 0;
                double dblSTotal = 0;
                double dblWSum = 0;
                double dblSSum = 0;

                // pass in the grid and the book number then this function will fill the grid
                // get all of the information for 1 book
                CurrentBook = lngBookNum;

                // set the book number
                ErrorCheck = 0;
                var book = FCConvert.ToString(CurrentBook);

                strSQL = boolFinal
                    ? $"SELECT * FROM Bill WHERE Book = {book} AND ISNULL(Final,0) = 1 AND BillStatus <> 'B' ORDER BY ActualAccountNumber, ID"
                    : $"SELECT * FROM Bill WHERE Book = {book} AND BillStatus = 'E' ORDER BY ActualAccountNumber, ID";

                // builds the SQL statement to select all records
                rsB.OpenRecordset(strSQL, modExtraModules.strUTDatabase);

                // selects all the bill records
                // fill the information
                if (rsB.RecordCount() > 0)
                {
                    frmWait.InstancePtr.Init("Loading Data." + "\r\n" + "  Please Wait...", true, rsB.RecordCount());

                    if (rsB.EndOfFile() != true && rsB.BeginningOfFile() != true)
                    {
                        // if not at the beginning or the end of the recordset
                        do
                        {
                            frmWait.InstancePtr.IncrementProgress();

                            //Application.DoEvents();
                            // fill the grid
                            AddBillInformation_2(rsB.Get_Fields_Int32("ID"), Grid, intRowOutLineLevel, Grid.Rows, ref dblWTotal, ref dblSTotal);

                            // adds meter data and any combined meters and will return the water and sewer total
                            dblWSum += dblWTotal;

                            // these are the totals of the meter
                            dblSSum += dblSTotal;
                            rsB.MoveNext();
                        } while (!rsB.EndOfFile());
                    }

                    // add a book total line
                    CurRow = Grid.Rows;
                    AddCurrentRow(CurRow, intRowOutLineLevel);
                    Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CurRow, 1, CurRow, Grid.Cols - 1, Color.White);
                    Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CurRow, lngGridColWater, CurRow, lngGridColSewer, 0xFF8080);
                    //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurRow, lngGridColWater, CurRow, lngGridColSewer, Color.White);
                    Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, CurRow, lngGridColWater, CurRow, lngGridColSewer, true);
                    Grid.TextMatrix(CurRow, lngGridColBookNumber, "Book Totals:");

                    // book total for col 5 (water)
                    Grid.TextMatrix(CurRow, lngGridColWater, dblWSum);

                    // book total for col 6 (sewer)
                    Grid.TextMatrix(CurRow, lngGridColSewer, dblSSum);
                    dblWGrandTotal += dblWSum;
                    dblSGrandTotal += dblSSum;
                }
                else
                {
                    frmWait.InstancePtr.Unload();

                    if (!boolFinal)
                    {
                        MessageBox.Show($"No bills in Book {FCConvert.ToString(lngBookNum)} have the correct status.", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        MessageBox.Show("No final bills have the correct status.", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Unload();
                    }

                    return false;
                }
                //FC:FINAL:SBE - #3823 - apply color scheme to grid level tree
                modColorScheme.ColorGrid(Grid);
                ret = true;
            }
            catch (Exception ex)
            {
                ret = false;
                MessageBox.Show($"Error #{FCConvert.ToString(Information.Err(ex).Number)} - {Information.Err(ex).Description}.", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsB.DisposeOf();
            }
            return ret;
        }

        private void CollapseRows(ref bool Bool)
        {
            try
            {
                // if Bool = True then collapse all of the rows starting with the subordinate rows
                // if Bool = False then open all rows starting with the subordinate rows
                int Row;
                CollapseFlag = true;
                switch (Bool)
                {
                    case true:
                        {
                            for (Row = 2; Row <= vsPreview.Rows - 1; Row++)
                            {
                                if (vsPreview.RowOutlineLevel(Row) == 1 && vsPreview.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
                                {
                                    vsPreview.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
                                }
                            }
                            for (Row = 2; Row <= vsPreview.Rows - 1; Row++)
                            {
                                if (vsPreview.RowOutlineLevel(Row) == 0 && vsPreview.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
                                {
                                    vsPreview.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
                                }
                            }

                            break;
                        }

                    case false:
                        {
                            for (Row = 2; Row <= vsPreview.Rows - 1; Row++)
                            {
                                if (vsPreview.RowOutlineLevel(Row) == 0 && vsPreview.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineExpanded)
                                {
                                    vsPreview.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
                                }
                            }
                            for (Row = 2; Row <= vsPreview.Rows - 1; Row++)
                            {
                                if (vsPreview.RowOutlineLevel(Row) == 1 && vsPreview.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineExpanded)
                                {
                                    vsPreview.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
                                }
                            }

                            break;
                        }
                }
                vsPreview_Collapsed();
                CollapseFlag = false;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Collapsing Rows", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: Row As object	OnWriteFCConvert.ToInt32(
        // vbPorter upgrade warning: lvl As object	OnWriteFCConvert.ToInt16(
        private void AddCurrentRow_18(int Row, int lvl, bool boolSubtotal = true)
        {
            AddCurrentRow(Row, lvl, boolSubtotal);
        }

        private void AddCurrentRow_24(int Row, int lvl, bool boolSubtotal = true)
        {
            AddCurrentRow(Row, lvl, boolSubtotal);
        }

        private void AddCurrentRow(int Row, int lvl, bool boolSubtotal = true)
        {
            // set the row as a group
            if (MaxRows == 0)
            {
                MaxRows = 2;
            }
            MaxRows += 1;
            vsPreview.AddItem("", Row);
            // increments the number of rows
            switch (lvl)
            {
                case 0:
                    {
                        vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsPreview.Cols - 1, 0x407000);
                        // &H8080FF &H8000000F&
                        //vsPreview.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, 1, Row, vsPreview.Cols - 1, Color.White);
                        break;
                    }
                case 1:
                    {
                        //FC:FINAL:BSE #3898 backcolor should be white
                        //vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsPreview.Cols - 1, 0x80000018);
                        vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsPreview.Cols - 1, Color.White);
                        break;
                    }
                case 2:
                    {
                        vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsPreview.Cols - 1, Color.White);
                        break;
                    }
                default:
                    {
                        vsPreview.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, lngGridColWater, Row, vsPreview.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                        vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsPreview.Cols - 1, 0x80000016);
                        //vsPreview.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGridColBookNumber, Row, lngGridColBookNumber, 0x80000016);
                        break;
                    }
            }
            //end switch
            // set the indentation level of the group
            vsPreview.RowOutlineLevel(Row, FCConvert.ToInt16(lvl));
            // If lvl = 0 Then
            vsPreview.IsSubtotal(Row, boolSubtotal);
        }

        private void Level(int Row, ref object lvl)
        {
            // set the row as a group
            if (MaxRows == 0)
            {
                MaxRows = 2;
            }
            MaxRows += 1;
            vsPreview.Rows = MaxRows + 1;
            // increments the number of rows
            var intLvl = FCConvert.ToInt32(lvl);

            switch (intLvl)
            {
                case 0:
                    vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsPreview.Cols - 1, 0x80000018);

                    break;
                case 1:
                    vsPreview.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, lngGridColWater, Row, vsPreview.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                    vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsPreview.Cols - 1, 0x80000016);
                    //vsPreview.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, 1, Row, 1, 0x80000016);

                    break;
                default:
                    vsPreview.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsPreview.Cols - 1, Color.White);

                    break;
            }
            // set the indentation level of the group
            vsPreview.RowOutlineLevel(Row, FCConvert.ToInt16(lvl));
            if (intLvl == 0)
                vsPreview.IsSubtotal(Row, true);
        }

        private void AddBillInformation_2(int lngBillKey, object Grid, int intStartOutlineLevel, int lngNextRow, ref double dblTotalW, ref double dblTotals)
        {
            AddBillInformation(ref lngBillKey, ref Grid, ref intStartOutlineLevel, ref lngNextRow, ref dblTotalW, ref dblTotals);
        }

        private void AddBillInformation(ref int lngBillKey, ref object Grid, ref int intStartOutlineLevel, ref int lngNextRow, ref double dblTotalW, ref double dblTotals)
        {
            clsDRWrapper rsFindBill = new clsDRWrapper();
            try
            {
                // this will add meter data to the grid passed in
                double dblWTotal = 0;
                double dblSTotal = 0;


                rsFindBill.OpenRecordset($"SELECT * FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE Bill.ID = {FCConvert.ToString(lngBillKey)} AND BillStatus = 'E'", modExtraModules.strUTDatabase);
                if (!rsFindBill.EndOfFile())
                {
                    // fills the primary level of the summary for the meter
                    AddCurrentRow_18(lngNextRow, intStartOutlineLevel, false);
                    dblWTotal = FCConvert.ToDouble(rsFindBill.Get_Fields("WTax") + rsFindBill.Get_Fields_Decimal("WMiscAmount") + rsFindBill.Get_Fields_Decimal("WAdjustAmount") + rsFindBill.Get_Fields_Decimal("WDEAdjustAmount") + rsFindBill.Get_Fields_Decimal("WFlatAmount") + rsFindBill.Get_Fields_Decimal("WUnitsAmount") + rsFindBill.Get_Fields_Decimal("WConsumptionAmount"));
                    dblSTotal = FCConvert.ToDouble(rsFindBill.Get_Fields("STax") + rsFindBill.Get_Fields_Decimal("SMiscAmount") + rsFindBill.Get_Fields_Decimal("SAdjustAmount") + rsFindBill.Get_Fields_Decimal("SDEAdjustAmount") + rsFindBill.Get_Fields_Decimal("SFlatAmount") + rsFindBill.Get_Fields_Decimal("SUnitsAmount") + rsFindBill.Get_Fields_Decimal("SConsumptionAmount"));
                    // send this information back out for the sum totals
                    dblTotalW = dblWTotal;
                    dblTotals = dblSTotal;
                    // add the billing information here
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColAccountNumber, FCConvert.ToString(rsFindBill.Get_Fields_Int32("ActualAccountNumber")));
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColSequence, FCConvert.ToString(rsFindBill.Get_Fields("Sequence")));
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColWater, Strings.Format(dblWTotal, numericFormat));
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColSewer, Strings.Format(dblSTotal, numericFormat));
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColDescription, FCConvert.ToString(rsFindBill.Get_Fields_String("Note")));

                    rsFindBill.Execute($"UPDATE Bill SET TotalWBillAmount = {FCConvert.ToString(dblWTotal)}, TotalSBillAmount = {FCConvert.ToString(dblSTotal)} WHERE ID = {FCConvert.ToString(lngBillKey)}", modExtraModules.strUTDatabase);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show($"Error #{FCConvert.ToString(Information.Err(ex).Number)} - {Information.Err(ex).Description}.", "Error Adding Bill Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsFindBill.DisposeOf();
            }
        }


        private void SetStatus(ref int BookNum, ref string NewStatus)
        {
            clsDRWrapper rsStatus = new clsDRWrapper();
            try
            {
                // this sub will change the status of the book, meter and current bill of the book designated in BookNum
                string strStatus = "";

                rsStatus.DefaultDB = modExtraModules.strUTDatabase;

                var bookNum = FCConvert.ToString(BookNum);
                strSQL = $"SELECT * FROM Book WHERE BookNumber = {bookNum}";
                rsStatus.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                if (!rsStatus.EndOfFile())
                {
                    rsStatus.Edit();
                    rsStatus.Set_Fields("CurrentStatus", NewStatus);
                    rsStatus.Set_Fields("BDate", DateTime.Now);
                    rsStatus.Set_Fields("DateUpdated", DateTime.Now);
                    rsStatus.Update();
                }

                strSQL = $"SELECT * FROM MeterTable WHERE BookNumber = {bookNum}";
                rsStatus.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                while (!rsStatus.EndOfFile())
                {
                    rsStatus.Edit();
                    rsStatus.Set_Fields("BillingStatus", Strings.Left(NewStatus, 1));
                    rsStatus.Update();
                    rsStatus.MoveNext();
                }

            }
            catch (Exception ex)
            {
                
                MessageBox.Show($"Error #{FCConvert.ToString(Information.Err(ex).Number)} - {Information.Err(ex).Description}.", "Error Setting Status", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsStatus.DisposeOf();
            }
        }

        private bool StartBookCalculation()
        {
            bool ret = false;

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this validation event just calls the function to get the book info and start the calculation
                // only valid books are shown in the combo box
                // valid book numbers are from 0001 to 9999
                int lngBNum = 0;
                int intCT;
                string strType = "";
                frmWait.InstancePtr.Init("Loading Data." + "\r\n" + "  Please Wait...", true, intCount);
                FillGrid = false;
                vsPreview.Visible = false;
                vsPreview.Rows = 1;
                mnuFileSummary.Enabled = false;
                boolCalculatedCorrectly = true;
                // go through the book array and show each book
                for (intCT = 1; intCT <= intCount; intCT++)
                {
                    //Application.DoEvents();
                    // get the book number from the array
                    lngBNum = BookArray[intCT];
                    if (lngBNum < 10000 && lngBNum > 0)
                    {
                        // make sure that this is a valid book number
                        boolCalculatedCorrectly = GetBookInfo(ref lngBNum);
                        // fill the grid with this book information
                    }
                    if (!boolCalculatedCorrectly)
                    {
                        break;
                    }
                    frmWait.InstancePtr.IncrementProgress();
                    // moves the progress bar
                }

                if (intCount > 1)
                {
                    // this is when more than one book is selected, if there are multiple
                    // books shown then this grid will need a grand total line at the bottom
                    AddCurrentRow_24(vsPreview.Rows, 0, true);
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColWater, Strings.Format(dblWGrandTotal, numericFormat));
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColSewer, Strings.Format(dblSGrandTotal, numericFormat));
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColBookNumber, "Totals:");
                }
                mnuFileSummary.Enabled = true;
                vsPreview.Visible = true;
                frmWait.InstancePtr.Unload();
                ret = boolCalculatedCorrectly;
                vsPreview_Collapsed();
                modColorScheme.ColorGrid(vsPreview, blnTotal: true);
            }
            catch (Exception ex)
            {
                
                ret = false;
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Book Calculation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return ret;
        }

        private void AddBookLine_24(int lngBookNum, string strDesc, short intROL)
        {
            AddBookLine(ref lngBookNum, ref strDesc, ref intROL);
        }

        private void AddBookLine(ref int lngBookNum, ref string strDesc, ref short intROL)
        {
            clsDRWrapper rsBills = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                AddCurrentRow(vsPreview.Rows, intROL);
                vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColBookNumber, Strings.Format(lngBookNum, "0000"));
                vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColDescription, strDesc);

                // put the amount of bills here
                rsBills.OpenRecordset($"SELECT Count(ID) AS BillCount FROM Bill WHERE Book = {FCConvert.ToString(lngBookNum)} AND BillStatus = 'E'", modExtraModules.strUTDatabase);

                if (!rsBills.EndOfFile())
                {
                    // TODO Get_Fields: Field [BillCount] not found!! (maybe it is an alias?)
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColBillCount, rsBills.Get_Fields("BillCount") + " bills");
                }
                else
                {
                    vsPreview.TextMatrix(vsPreview.Rows - 1, lngGridColBillCount, "No bills.");
                }

            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", $"Error Adding Book Line - {FCConvert.ToString(lngBookNum)}", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsBills.DisposeOf();
            }
        }
    }
}
