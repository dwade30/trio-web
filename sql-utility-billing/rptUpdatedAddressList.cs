﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUpdatedAddressList.
	/// </summary>
	public partial class rptUpdatedAddressList : BaseSectionReport
	{
		public rptUpdatedAddressList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Updated Address List";
		}

		public static rptUpdatedAddressList InstancePtr
		{
			get
			{
				return (rptUpdatedAddressList)Sys.GetInstance(typeof(rptUpdatedAddressList));
			}
		}

		protected rptUpdatedAddressList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUpdatedAddressList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               05/30/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               05/30/2006              *
		// ********************************************************
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		bool boolDone;

		public void Init(ref int lngPassTotalAccounts, string strYear)
		{
			lngTotalAccounts = lngPassTotalAccounts;
			lblYear.Text = "Rate Record: " + strYear;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!modUTLien.Statics.arrDemand[lngCount].Used && boolDone)
			{
				eArgs.EOF = true;
				lblNew.Visible = false;
				lblOld.Visible = false;
				Detail.Height = 0;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("LastUpdateAddress");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastUpdateAddress1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblHeader.Text = "Update Address List";
			lngCount = 0;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			if (!boolDone)
			{
				// this will find the next available account that was processed
				while (!(modUTLien.Statics.arrDemand[lngCount].Processed || lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1)))
				{
					lngCount += 1;
				}
				// check to see if we are at the end of the list
				if (lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1))
				{
					boolDone = true;
					// clear the fields so there are no repeats
					fldAcct.Text = "";
					fldName.Text = "";
					fldOld.Text = "";
					fldNew.Text = "";
					lblNew.Visible = false;
					lblOld.Visible = false;
					Detail.Height = 0;
					return;
				}
				fldAcct.Text = modUTLien.Statics.arrDemand[lngCount].Account.ToString();
				fldName.Text = modUTLien.Statics.arrDemand[lngCount].Name;
				fldOld.Text = modUTLien.Statics.arrDemand[lngCount].OldAddress;
				fldNew.Text = modUTLien.Statics.arrDemand[lngCount].NewAddress;
				// move to the next record in the array
				lngCount += 1;
			}
			else
			{
				// clear the fields
				fldAcct.Text = "";
				fldName.Text = "";
				fldOld.Text = "";
				fldNew.Text = "";
				lblNew.Visible = false;
				lblOld.Visible = false;
				Detail.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// this sub will fill in the footer line at the bottom of the report
			if (lngTotalAccounts == 1)
			{
				lblNumberOfAccounts.Text = "There was " + FCConvert.ToString(lngTotalAccounts) + " account processed.";
			}
			else
			{
				lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			}
		}

		private void rptUpdatedAddressList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUpdatedAddressList properties;
			//rptUpdatedAddressList.Caption	= "Updated Address List";
			//rptUpdatedAddressList.Icon	= "rptUpdatedAddressList.dsx":0000";
			//rptUpdatedAddressList.Left	= 0;
			//rptUpdatedAddressList.Top	= 0;
			//rptUpdatedAddressList.Width	= 11880;
			//rptUpdatedAddressList.Height	= 8595;
			//rptUpdatedAddressList.StartUpPosition	= 3;
			//rptUpdatedAddressList.SectionData	= "rptUpdatedAddressList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
