﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptTaxAcquiredList.
	/// </summary>
	public partial class rptTaxAcquiredList : BaseSectionReport
	{
		public rptTaxAcquiredList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Acquired List";
		}

		public static rptTaxAcquiredList InstancePtr
		{
			get
			{
				return (rptTaxAcquiredList)Sys.GetInstance(typeof(rptTaxAcquiredList));
			}
		}

		protected rptTaxAcquiredList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsLN.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTaxAcquiredList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/11/2007              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/01/2007              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		bool boolDone;
		double dblTotalPrin;
		double dblTotal;
		double dblLTotal;
		bool boolReverse;
		short intSumRows;

		public void Init(ref int lngPassTotalAccounts)
		{
			lngTotalAccounts = lngPassTotalAccounts;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!modUTLien.Statics.arrDemand[lngCount].Used && boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				if (boolReverse)
				{
					modGlobalFunctions.IncrementSavedReports("LastCLReverseDemand");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLReverseDemand1.RDF"));
				}
				else
				{
					modGlobalFunctions.IncrementSavedReports("LastCLDemandFeesList");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastCLDemandFeesList1.RDF"));
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblHeader.Text = "Tax Acquired Properties";
			lngCount = 0;
			//FC:FINAL:DDU:#i983 create on the demand fields here
			SetupTotals();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				if (!boolDone)
				{
					// this will find the next available account that was processed
					while (!(modUTLien.Statics.arrDemand[lngCount].Processed || lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1)))
					{
						lngCount += 1;
					}
					// check to see if we are at the end of the list
					if (lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1))
					{
						boolDone = true;
						// clear the fields so there are no repeats
						fldAcct.Text = "";
						fldName.Text = "";
						fldTotal.Text = "";
						return;
					}
					rsData.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(modUTLien.Statics.arrDemand[lngCount].Account), modExtraModules.strUTDatabase);
					while (!rsData.EndOfFile())
					{
						// sewer
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SLienRecordNumber")) != 0)
						{
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							if (FCConvert.ToString(rsData.Get_Fields_Int32("SCombinationLienKey")) == FCConvert.ToString(rsData.Get_Fields("Bill")))
							{
								rsLN.OpenRecordset("SELECT * FROM Lien WHERE LienKey = " + rsData.Get_Fields_Int32("SLienRecordNumber"), modExtraModules.strUTDatabase);
								if (!rsLN.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
									modUTLien.Statics.arrDemand[lngCount].Fee += (Conversion.Val(rsLN.Get_Fields("Principal")) - Conversion.Val(rsLN.Get_Fields("PrinPaid")));
								}
							}
							else
							{
								// do nothing this is a secondary liened bill
							}
						}
						else
						{
							modUTLien.Statics.arrDemand[lngCount].Fee += (Conversion.Val(rsData.Get_Fields_Double("SPrinOwed")) - Conversion.Val(rsData.Get_Fields_Double("SPrinPaid")));
						}
						// water
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("WLienRecordNumber")) != 0)
						{
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							if (FCConvert.ToString(rsData.Get_Fields_Int32("WCombinationLienKey")) == FCConvert.ToString(rsData.Get_Fields("Bill")))
							{
								rsLN.OpenRecordset("SELECT * FROM Lien WHERE LienKey = " + rsData.Get_Fields_Int32("WLienRecordNumber"), modExtraModules.strUTDatabase);
								if (!rsLN.EndOfFile())
								{
									// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
									// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
									modUTLien.Statics.arrDemand[lngCount].Fee += (Conversion.Val(rsLN.Get_Fields("Principal")) - Conversion.Val(rsLN.Get_Fields("PrinPaid")));
								}
							}
						}
						else
						{
							modUTLien.Statics.arrDemand[lngCount].Fee += (Conversion.Val(rsData.Get_Fields_Double("WPrinOwed")) - Conversion.Val(rsData.Get_Fields_Double("WPrinPaid")));
						}
						rsData.MoveNext();
					}
					fldAcct.Text = modUTLien.Statics.arrDemand[lngCount].Account.ToString();
					fldName.Text = modUTLien.Statics.arrDemand[lngCount].Name;
					fldTotal.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].Fee, "#,##0.00");
					dblTotalPrin += modUTLien.Statics.arrDemand[lngCount].Fee;
					// move to the next record in the array
					lngCount += 1;
				}
				else
				{
					// clear the fields
					fldAcct.Text = "";
					fldName.Text = "";
					fldTotal.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
			// this sub will fill in the footer line at the bottom of the report
			int intCT;
			// setup the summary
			intSumRows = 1;
			for (intCT = 0; intCT <= Information.UBound(modMain.Statics.arrTALienPrincipalW, 1); intCT++)
			{
				// If Not gboolMultipleTowns Then
				if (modMain.Statics.arrTAPrincipalW[intCT] != 0 || modMain.Statics.arrTALienPrincipalW[intCT] != 0)
				{
					AddSummaryRow(ref intSumRows, ref modMain.Statics.arrTAPrincipalW[intCT], ref modMain.Statics.arrTALienPrincipalW[intCT], ref intCT);
					intSumRows += 1;
				}
				if (modMain.Statics.arrTAPrincipalS[intCT] != 0 || modMain.Statics.arrTALienPrincipalS[intCT] != 0)
				{
					AddSummaryRow(ref intSumRows, ref modMain.Statics.arrTAPrincipalS[intCT], ref modMain.Statics.arrTALienPrincipalS[intCT], ref intCT);
					intSumRows += 1;
				}
				// Else
				// If (arrTAPrincipal(intCT, 1) <> 0 Or arrTALienPrincipal(intCT, 1) <> 0) Or (arrTAPrincipal(intCT, 2) <> 0 Or arrTALienPrincipal(intCT, 2) <> 0) Or (arrTAPrincipal(intCT, 3) <> 0 Or arrTALienPrincipal(intCT, 3) <> 0) Or (arrTAPrincipal(intCT, 4) <> 0 Or arrTALienPrincipal(intCT, 4) <> 0) Or (arrTAPrincipal(intCT, 5) <> 0 Or arrTALienPrincipal(intCT, 5) <> 0) Or (arrTAPrincipal(intCT, 6) <> 0 Or arrTALienPrincipal(intCT, 6) <> 0) Or (arrTAPrincipal(intCT, 7) <> 0 Or arrTALienPrincipal(intCT, 7) <> 0) Or (arrTAPrincipal(intCT, 8) <> 0 Or arrTALienPrincipal(intCT, 8) <> 0) Or (arrTAPrincipal(intCT, 9) <> 0 Or arrTALienPrincipal(intCT, 9) <> 0) Then
				// AddSummaryRow intSumRows, arrTAPrincipal(intCT, 1) + arrTAPrincipal(intCT, 2) + arrTAPrincipal(intCT, 3) + arrTAPrincipal(intCT, 4) + arrTAPrincipal(intCT, 5) + arrTAPrincipal(intCT, 6) + arrTAPrincipal(intCT, 7) + arrTAPrincipal(intCT, 8) + arrTAPrincipal(intCT, 9), arrTALienPrincipal(intCT, 1) + arrTALienPrincipal(intCT, 2) + arrTALienPrincipal(intCT, 3) + arrTALienPrincipal(intCT, 4) + arrTALienPrincipal(intCT, 5) + arrTALienPrincipal(intCT, 6) + arrTALienPrincipal(intCT, 7) + arrTALienPrincipal(intCT, 8) + arrTALienPrincipal(intCT, 9), intCT + DEFAULTSUBTRACTIONVALUE
				// intSumRows = intSumRows + 1
				// End If
				// End If
			}
			// add a label
			obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			obNew.Name = "lblTotalLine";
			obNew.Top = lblYear1.Top + ((intSumRows - 1) * lblYear1.Height);
			obNew.Left = lblYear1.Left;
			obNew.Font = lblYear1.Font;
			obNew.Text = "Total";
			ReportFooter.Controls.Add(obNew);
			GrapeCity.ActiveReports.SectionReportModel.Label obLabel;
			// add a regular amount total label
			obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			obLabel.Name = "lblAmountTotal";
			obLabel.Top = lblYear1.Top + ((intSumRows - 1) * lblYear1.Height);
			obLabel.Left = lblAmount1.Left;
			obLabel.Width = lblAmount1.Width;
			obLabel.Font = lblAmount1.Font;
			obLabel.Text = Strings.Format(dblTotal, "#,##0.00");
			obLabel.Alignment = lblLAmount1.Alignment;
			ReportFooter.Controls.Add(obLabel);
			// add a lien amount total label
			obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			obLabel.Name = "lblLAmountTotal";
			obLabel.Top = lblYear1.Top + ((intSumRows - 1) * lblYear1.Height);
			obLabel.Left = lblLAmount1.Left;
			obLabel.Width = lblLAmount1.Width;
			obLabel.Font = lblLAmount1.Font;
			obLabel.Text = Strings.Format(dblLTotal, "#,##0.00");
			obLabel.Alignment = lblLAmount1.Alignment;
			ReportFooter.Controls.Add(obLabel);
			// add a line
			GrapeCity.ActiveReports.SectionReportModel.Line obLine;
			obLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
			obLine.Name = "lnFooterSummaryTotal";
			obLine.X1 = lblYear1.Left;
			obLine.X2 = lblLAmount1.Left + lblLAmount1.Width;
			obLine.Y1 = lblYear1.Top + ((intSumRows - 1) * lblYear1.Height);
			obLine.Y2 = obLine.Y1;
			ReportFooter.Controls.Add(obLine);
			ReportFooter.Height = lblYear1.Top + (intSumRows * lblYear1.Height);
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			//SetupTotals();
			lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed.";
			lblTotalTotal.Text = Strings.Format(dblTotalPrin, "#,##0.00");
		}
		// vbPorter upgrade warning: intRNum As short	OnWriteFCConvert.ToInt32(
		private void AddSummaryRow(ref short intRNum, ref double dblAmount, ref double dblLAmount, ref int lngYear)
		{
			GrapeCity.ActiveReports.SectionReportModel.Label obNew;
			// this will add another per diem line in the report footer
			if (intRNum == 1)
			{
				lblAmount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				lblAmount1.Text = Strings.Format(dblAmount, "#,##0.00");
				lblLAmount1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
				lblLAmount1.Text = Strings.Format(dblLAmount, "#,##0.00");
				lblYear1.Text = lngYear.ToString();
			}
			else
			{
				// add a year label
				obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
				obNew.Name = "lblYear" + FCConvert.ToString(intRNum);
				obNew.Top = lblYear1.Top + ((intRNum - 1) * lblYear1.Height);
				obNew.Left = lblYear1.Left;
				obNew.Font = lblYear1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Text = lngYear.ToString();
				ReportFooter.Controls.Add(obNew);
				// add a regular amount label
				obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
				obNew.Name = "lblAmount" + FCConvert.ToString(intRNum);
				obNew.Top = lblYear1.Top + ((intRNum - 1) * lblYear1.Height);
				obNew.Left = lblAmount1.Left;
				obNew.Width = lblAmount1.Width;
				obNew.Font = lblAmount1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Alignment = lblLAmount1.Alignment;
				obNew.Text = Strings.Format(dblAmount, "#,##0.00");
				ReportFooter.Controls.Add(obNew);
				// add a lien amount label
				obNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
				obNew.Name = "lblLAmount" + FCConvert.ToString(intRNum);
				obNew.Top = lblYear1.Top + ((intRNum - 1) * lblYear1.Height);
				obNew.Left = lblLAmount1.Left;
				obNew.Width = lblLAmount1.Width;
				obNew.Font = lblLAmount1.Font;
				// this sets the font to the same as the field that is already created
				obNew.Alignment = lblLAmount1.Alignment;
				obNew.Text = Strings.Format(dblLAmount, "#,##0.00");
				ReportFooter.Controls.Add(obNew);
			}
			dblTotal += dblAmount;
			dblLTotal += dblLAmount;
		}

		private void rptTaxAcquiredList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptTaxAcquiredList properties;
			//rptTaxAcquiredList.Caption	= "Tax Acquired List";
			//rptTaxAcquiredList.Icon	= "rptTaxAcquiredList.dsx":0000";
			//rptTaxAcquiredList.Left	= 0;
			//rptTaxAcquiredList.Top	= 0;
			//rptTaxAcquiredList.Width	= 11880;
			//rptTaxAcquiredList.Height	= 8595;
			//rptTaxAcquiredList.StartUpPosition	= 3;
			//rptTaxAcquiredList.SectionData	= "rptTaxAcquiredList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
