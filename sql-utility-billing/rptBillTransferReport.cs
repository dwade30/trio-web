﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptBillTransferReport.
	/// </summary>
	public partial class rptBillTransferReport : BaseSectionReport
	{
		public static rptBillTransferReport InstancePtr
		{
			get
			{
				return (rptBillTransferReport)Sys.GetInstance(typeof(rptBillTransferReport));
			}
		}

		protected rptBillTransferReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		public rptBillTransferReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		// nObj = 1
		//   0	rptBillTransferReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/31/2006              *
		// ********************************************************
		int lngRateKey;
		clsDRWrapper rsData = new clsDRWrapper();
		int lngBillCount;
		int lngBook;
		double[] dblTotalSumAmount = new double[6 + 1];
		DateTime dtIntDate;
		int lngOrder;
		string strBook = "";
		bool blnShowDOSInfo;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				dtIntDate = rptBillTransferReportMaster.InstancePtr.dtBillingDate;
				lngOrder = rptBillTransferReportMaster.InstancePtr.lngOrder;
				if (lngOrder == 0)
				{
					lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
				}
				else
				{
					strBook = FCConvert.ToString(this.UserData);
				}
				lngRateKey = rptBillTransferReportMaster.InstancePtr.lngRateKey;
				lblReportType.Text = "Rate Key = " + FCConvert.ToString(lngRateKey) + "\r\n" + "Book = " + FCConvert.ToString(lngBook);
				if (modExtraModules.Statics.gstrRegularIntMethod == "B")
				{
					// Trim(UCase(MuniName)) = "RICHMOND UT" Or Trim(UCase(MuniName)) = "BOWDOINHAM UT" Then
					blnShowDOSInfo = true;
				}
				else
				{
					blnShowDOSInfo = false;
				}
				// force this report to be landscape
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				;
				// this loads the data
				if (LoadAccounts())
				{
					// rock on
				}
				else
				{
					this.Cancel();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Sub Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			if (lngOrder == 0)
			{
				FillTotals();
			}
			else
			{
				GroupFooter1.Visible = false;
			}
		}

		private bool LoadAccounts()
		{
			bool LoadAccounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				string strFields;
				string strSeq = "";
				// MAL@20071212: Add override flag fields to the selection
				// Tracker Reference: 10973
				// strFields = "AccountNumber, Name, OwnerName, Master.BAddress1, Master.Baddress2, TotalWBillAmount, TotalSBillAmount, Bill.Book, Sequence, Consumption, WFlatAmount, WUnitsAmount, WConsumptionAmount, WAdjustAmount, SAdjustAmount, WDEAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount, WMiscAmount, SMiscAmount, WTax, STax, Bill.AccountKey AS AcctKey, (WPrinPaid + WTaxPaid + WIntPaid + WCostPaid + SPrinPaid + STaxPaid + SIntPaid + SCostPaid) AS TotPaid "
				// kgk    strFields = "Master.Key as AccountKey, AccountNumber, Name, OwnerName, Master.BAddress1, Master.Baddress2, TotalWBillAmount, TotalSBillAmount, Bill.Book, Sequence, Consumption, WFlatAmount, WUnitsAmount, WConsumptionAmount, WAdjustAmount, SAdjustAmount, WDEAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount, WMiscAmount, SMiscAmount, WTax, STax, Bill.AccountKey AS AcctKey, Bill, (WPrinPaid + WTaxPaid + WIntPaid + WCostPaid + SPrinPaid + STaxPaid + SIntPaid + SCostPaid) AS TotPaid, WHasOverride, SHasOverride "
				strFields = "Master.ID as AccountKey, AccountNumber, BName, OName, BAddress1, Baddress2, TotalWBillAmount, TotalSBillAmount, Bill.Book, Sequence, Consumption, WFlatAmount, WUnitsAmount, WConsumptionAmount, WAdjustAmount, SAdjustAmount, WDEAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount, WMiscAmount, SMiscAmount, WTax, STax, Bill.AccountKey AS AcctKey, Bill.ID AS Bill, (WPrinPaid + WTaxPaid + WIntPaid + WCostPaid + SPrinPaid + STaxPaid + SIntPaid + SCostPaid) AS TotPaid, WHasOverride, SHasOverride ";
				switch (lngOrder)
				{
					case 0:
						{
							// Book/Seq
							strSeq = " ORDER BY Bill.Book, MeterTable.Sequence, AccountNumber";
							break;
						}
					case 1:
						{
							// Account Number
							strSeq = " ORDER BY AccountNumber";
							break;
						}
					case 2:
						{
							// Name
							strSeq = " ORDER BY BName";
							// Name
							break;
						}
					case 3:
						{
							// Location
							strSeq = " ORDER BY StreetName, StreetNumber";
							break;
						}
					case 4:
						{
							// Map Lot
							strSeq = " ORDER BY Master.MapLot";
							break;
						}
				}
				//end switch
				if (lngOrder == 0)
				{
					rsData.OpenRecordset("SELECT " + strFields + " FROM (Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON MeterTable.AccountKey = Master.ID WHERE BillingRateKey = " + FCConvert.ToString(lngRateKey) + " AND Bill.Book = " + FCConvert.ToString(lngBook) + " AND Combine = 'N' ORDER BY MeterTable.Sequence, ActualAccountNumber", modExtraModules.strUTDatabase);
				}
				else
				{
					rsData.OpenRecordset("SELECT " + strFields + " FROM (Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON MeterTable.AccountKey = Master.ID WHERE BillingRateKey = " + FCConvert.ToString(lngRateKey) + " AND Bill.Book " + strBook + " AND Combine = 'N' " + strSeq, modExtraModules.strUTDatabase);
				}
				if (!rsData.EndOfFile())
				{
					LoadAccounts = true;
					if (lngOrder == 0)
					{
						lblReportType.Text = "Book : " + FCConvert.ToString(lngBook);
					}
					else
					{
						lblReportType.Text = "Books : " + strBook;
					}
				}
				else
				{
					LoadAccounts = false;
				}
				return LoadAccounts;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadAccounts;
		}

		private void BindFields()
		{
			var rsPrePaid = new clsDRWrapper();
			var rsChargedInt = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                double dblAmount = 0;
                double dblWCurInt = 0;
                double dblWCHGInt = 0;
                double dblSCurInt = 0;
                double dblSCHGInt = 0;
                double dblTotal = 0;
                double dblPaid = 0;

                double dblTodaysInt = 0;

                if (!rsData.EndOfFile())
                {
                    dblSCurInt = 0;
                    dblSCHGInt = 0;
                    dblWCurInt = 0;
                    dblWCHGInt = 0;
                    dblPaid = 0;
                    // If rsData.Fields("AccountNumber") = 16 Then Stop
                    //FC:FINAL:MSH - can't implicitly convert from int to string
                    // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                    fldAcctNum.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
                    if (modUTCalculations.Statics.gboolShowOwner)
                    {
                        fldName.Text = FCConvert.ToString(rsData.Get_Fields_String("OName"));
                        // ("OwnerName")
                    }
                    else
                    {
                        fldName.Text = FCConvert.ToString(rsData.Get_Fields_String("BName"));
                        // ("Name")
                    }

                    // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                    fldBook.Text = FCConvert.ToString(rsData.Get_Fields("Book"));
                    if (FCConvert.CBool(rsData.Get_Fields_Boolean("WHasOverride")) == true ||
                        FCConvert.CBool(rsData.Get_Fields_Boolean("SHasOverride")) == true)
                    {
                        fldBook.Text = "*" + fldBook.Text;
                    }

                    // TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
                    fldSeq.Text = FCConvert.ToString(rsData.Get_Fields("Sequence"));
                    fldCons.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Consumption"));
                    fldRegular.Text = Strings.Format(
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WFlatAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WUnitsAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WConsumptionAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SFlatAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SUnitsAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SConsumptionAmount")), "#,##0.00");
                    fldMisc.Text = Strings.Format(
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WMiscAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SMiscAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WAdjustAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SAdjustAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WDEAdjustAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SDEAdjustAmount")), "#,##0.00");
                    // TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
                    // TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
                    fldTax.Text =
                        Strings.Format(
                            FCConvert.ToDouble(rsData.Get_Fields("WTax")) +
                            FCConvert.ToDouble(rsData.Get_Fields("STax")), "#,##0.00");
                    // TODO Get_Fields: Field [TotPaid] not found!! (maybe it is an alias?)
                    dblPaid = FCConvert.ToDouble(rsData.Get_Fields("TotPaid"));
                    // TODO Get_Fields: Field [AcctKey] not found!! (maybe it is an alias?)
                    dblTotal = modUTCalculations.CalculateAccountUTTotal_3(rsData.Get_Fields("AcctKey"), true,
                        modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblWCurInt, ref dblWCHGInt,
                        ref dtIntDate);
                    // TODO Get_Fields: Field [AcctKey] not found!! (maybe it is an alias?)
                    dblTotal += modUTCalculations.CalculateAccountUTTotal_3(rsData.Get_Fields("AcctKey"), false,
                        modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblSCurInt, ref dblSCHGInt,
                        ref dtIntDate);
                    // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                    rsPrePaid.OpenRecordset(
                        "SELECT SUM(Principal) as PrePayTotal FROM PaymentRec WHERE Code = 'Y' AND BillKey = " +
                        rsData.Get_Fields("Bill"));
                    if (rsPrePaid.EndOfFile() != true && rsPrePaid.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Field [PrePayTotal] not found!! (maybe it is an alias?)
                        dblTotal -= Conversion.Val(rsPrePaid.Get_Fields("PrePayTotal") + "");
                    }

                    if (dblTotal != 0)
                    {
                        if (blnShowDOSInfo)
                        {
                            dblTodaysInt = 0;
                            rsChargedInt.OpenRecordset("SELECT * FROM PaymentRec WHERE Code = 'I' AND AccountKey = " +
                                                       rsData.Get_Fields_Int32("AccountKey") +
                                                       " AND Reference = 'CHGINT' AND ActualSystemDate = '" +
                                                       DateTime.Today.ToShortDateString() + "'");
                            if (rsChargedInt.EndOfFile() != true && rsChargedInt.BeginningOfFile() != true)
                            {
                                do
                                {
                                    dblTodaysInt +=
                                        FCConvert.ToDouble(rsChargedInt.Get_Fields_Decimal("CurrentInterest") * -1);
                                    rsChargedInt.MoveNext();
                                } while (rsChargedInt.EndOfFile() != true);
                            }

                            fldPastDue.Text =
                                Strings.Format(
                                    (dblTotal - dblTodaysInt - FCConvert.ToDouble(fldRegular.Text) -
                                     FCConvert.ToDouble(fldTax.Text) - FCConvert.ToDouble(fldMisc.Text)) + dblPaid,
                                    "#,##0.00");
                            // remove the totals from this bill and the interest amounts
                            fldInterest.Text = Strings.Format(dblTodaysInt, "#,##0.00");
                        }
                        else
                        {
                            fldPastDue.Text = Strings.Format(
                                (dblTotal - (dblWCurInt + dblWCHGInt + dblSCurInt + dblSCHGInt) -
                                 FCConvert.ToDouble(fldRegular.Text) - FCConvert.ToDouble(fldTax.Text) -
                                 FCConvert.ToDouble(fldMisc.Text)) + dblPaid, "#,##0.00");
                            // remove the totals from this bill and the interest amounts
                            fldInterest.Text = Strings.Format(dblWCurInt + dblWCHGInt + dblSCurInt + dblSCHGInt,
                                "#,##0.00");
                        }

                        // If fldInterest.Text < 0 Then
                        // MsgBox "Stop"
                        // End If
                    }
                    else
                    {
                        fldPastDue.Text = "0.00";
                        fldInterest.Text = "0.00";
                    }

                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount0W +=
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WFlatAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WUnitsAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WConsumptionAmount"));
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount1W +=
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WMiscAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WAdjustAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("WDEAdjustAmount"));
                    // TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount2W +=
                        FCConvert.ToDouble(rsData.Get_Fields("WTax"));
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount3W += dblWCurInt + dblWCHGInt;
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount0S +=
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SFlatAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SUnitsAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SConsumptionAmount"));
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount1S +=
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SMiscAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SAdjustAmount")) +
                        FCConvert.ToDouble(rsData.Get_Fields_Decimal("SDEAdjustAmount"));
                    // TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount2S +=
                        FCConvert.ToDouble(rsData.Get_Fields("STax"));
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount3S += dblSCurInt + dblSCHGInt;
                    // dblAmount = rsData.Fields("TotalWBillAmount") + rsData.Fields("TotalSBillAmount")   'get the total bill amount
                    dblAmount = FCConvert.ToDouble(fldRegular.Text) + FCConvert.ToDouble(fldTax.Text) +
                                FCConvert.ToDouble(fldMisc.Text) + FCConvert.ToDouble(fldPastDue.Text) +
                                FCConvert.ToDouble(fldInterest.Text);
                    fldAmount.Text = Strings.Format(dblAmount, "#,##0.00");
                    // fill the local (group) totals
                    dblTotalSumAmount[0] += Conversion.Val(fldCons.Text);
                    dblTotalSumAmount[1] += FCConvert.ToDouble(fldRegular.Text);
                    dblTotalSumAmount[2] += FCConvert.ToDouble(fldMisc.Text);
                    dblTotalSumAmount[3] += FCConvert.ToDouble(fldTax.Text);
                    dblTotalSumAmount[4] += FCConvert.ToDouble(fldPastDue.Text);
                    dblTotalSumAmount[5] += FCConvert.ToDouble(fldInterest.Text);
                    dblTotalSumAmount[6] += dblAmount;
                    lngBillCount += 1;
                    // update the master totals
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount0 += Conversion.Val(fldCons.Text);
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount1 += FCConvert.ToDouble(fldRegular.Text);
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount2 += FCConvert.ToDouble(fldMisc.Text);
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount3 += FCConvert.ToDouble(fldTax.Text);
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount4 += FCConvert.ToDouble(fldPastDue.Text);
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount5 += FCConvert.ToDouble(fldInterest.Text);
                    rptBillTransferReportMaster.InstancePtr.dblTotalSumAmount6 += FCConvert.ToDouble(fldAmount.Text);
                    rptBillTransferReportMaster.InstancePtr.lngBillCount += 1;
                    rsData.MoveNext();
                }

                return;
            }
            catch (Exception ex)
            {

                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				rsChargedInt.Dispose();
				rsPrePaid.Dispose();
            }
		}

		private void FillTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fldTotalCons.Text = Strings.Format(dblTotalSumAmount[0], "#,##0");
				fldTotalRegular.Text = Strings.Format(dblTotalSumAmount[1], "#,##0.00");
				fldTotalMisc.Text = Strings.Format(dblTotalSumAmount[2], "#,##0.00");
				fldTotalTax.Text = Strings.Format(dblTotalSumAmount[3], "#,##0.00");
				fldTotalPastDue.Text = Strings.Format(dblTotalSumAmount[4], "#,##0.00");
				fldTotalInterest.Text = Strings.Format(dblTotalSumAmount[5], "#,##0.00");
				fldTotalAmount.Text = Strings.Format(dblTotalSumAmount[6], "#,##0.00");
				if (lngBillCount == 1)
				{
					fldFooter.Text = "Book: " + FCConvert.ToString(lngBook) + "\r\n" + "1 bill";
				}
				else
				{
					fldFooter.Text = "Book: " + FCConvert.ToString(lngBook) + "\r\n" + FCConvert.ToString(lngBillCount) + " bills";
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Totals", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		
	}
}
