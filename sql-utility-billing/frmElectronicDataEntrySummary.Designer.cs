﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmElectronicDataEntrySummary.
	/// </summary>
	partial class frmElectronicDataEntrySummary : BaseForm
	{
		public fecherFoundation.FCTextBox txtNoMatchRecords;
		public fecherFoundation.FCTextBox txtRecordsUpdated;
		public fecherFoundation.FCTextBox txtDuplicateReadings;
		public fecherFoundation.FCTextBox txtMetersUpdated;
		public fecherFoundation.FCGrid vsNoMatch;
		public fecherFoundation.FCTextBox txtDuplicateMeters;
		public fecherFoundation.FCTextBox txtNotUpdatedMeters;
		public fecherFoundation.FCTextBox txtRecordsWithDupMeters;
		public fecherFoundation.FCTextBox txtTotalMeters;
		public fecherFoundation.FCTextBox txtTotalRecords;
		public fecherFoundation.FCGrid vsNotUpdated;
		public fecherFoundation.FCGrid vsDuplicates;
		public fecherFoundation.FCLabel lblNoMatchRecords;
		public fecherFoundation.FCLabel lblRecordsUpdated;
		public fecherFoundation.FCLabel lblDuplicateReadings;
		public fecherFoundation.FCLabel lblMetersUpdated;
		public fecherFoundation.FCLine Line5;
		public fecherFoundation.FCLine Line4;
		public fecherFoundation.FCLine Line3;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCLine Line1;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel lblDuplicateMeters;
		public fecherFoundation.FCLabel lblNotUpdatedMeters;
		public fecherFoundation.FCLabel lblRecordsWithDupMeters;
		public fecherFoundation.FCLabel lblTotalMeters;
		public fecherFoundation.FCLabel lblTotalRecords;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmElectronicDataEntrySummary));
			this.txtNoMatchRecords = new fecherFoundation.FCTextBox();
			this.txtRecordsUpdated = new fecherFoundation.FCTextBox();
			this.txtDuplicateReadings = new fecherFoundation.FCTextBox();
			this.txtMetersUpdated = new fecherFoundation.FCTextBox();
			this.vsNoMatch = new fecherFoundation.FCGrid();
			this.txtDuplicateMeters = new fecherFoundation.FCTextBox();
			this.txtNotUpdatedMeters = new fecherFoundation.FCTextBox();
			this.txtRecordsWithDupMeters = new fecherFoundation.FCTextBox();
			this.txtTotalMeters = new fecherFoundation.FCTextBox();
			this.txtTotalRecords = new fecherFoundation.FCTextBox();
			this.vsNotUpdated = new fecherFoundation.FCGrid();
			this.vsDuplicates = new fecherFoundation.FCGrid();
			this.lblNoMatchRecords = new fecherFoundation.FCLabel();
			this.lblRecordsUpdated = new fecherFoundation.FCLabel();
			this.lblDuplicateReadings = new fecherFoundation.FCLabel();
			this.lblMetersUpdated = new fecherFoundation.FCLabel();
			this.Line5 = new fecherFoundation.FCLine();
			this.Line4 = new fecherFoundation.FCLine();
			this.Line3 = new fecherFoundation.FCLine();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Line2 = new fecherFoundation.FCLine();
			this.Line1 = new fecherFoundation.FCLine();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.lblDuplicateMeters = new fecherFoundation.FCLabel();
			this.lblNotUpdatedMeters = new fecherFoundation.FCLabel();
			this.lblRecordsWithDupMeters = new fecherFoundation.FCLabel();
			this.lblTotalMeters = new fecherFoundation.FCLabel();
			this.lblTotalRecords = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessPrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFilePreiew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsNoMatch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsNotUpdated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsDuplicates)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreiew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreiew);
			this.BottomPanel.Location = new System.Drawing.Point(0, 598);
			this.BottomPanel.Size = new System.Drawing.Size(882, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.txtNoMatchRecords);
			this.ClientArea.Controls.Add(this.txtRecordsUpdated);
			this.ClientArea.Controls.Add(this.txtDuplicateReadings);
			this.ClientArea.Controls.Add(this.txtMetersUpdated);
			this.ClientArea.Controls.Add(this.vsNoMatch);
			this.ClientArea.Controls.Add(this.txtDuplicateMeters);
			this.ClientArea.Controls.Add(this.txtNotUpdatedMeters);
			this.ClientArea.Controls.Add(this.txtRecordsWithDupMeters);
			this.ClientArea.Controls.Add(this.txtTotalMeters);
			this.ClientArea.Controls.Add(this.txtTotalRecords);
			this.ClientArea.Controls.Add(this.vsNotUpdated);
			this.ClientArea.Controls.Add(this.vsDuplicates);
			this.ClientArea.Controls.Add(this.lblNoMatchRecords);
			this.ClientArea.Controls.Add(this.lblRecordsUpdated);
			this.ClientArea.Controls.Add(this.lblDuplicateReadings);
			this.ClientArea.Controls.Add(this.lblMetersUpdated);
			this.ClientArea.Controls.Add(this.Line5);
			this.ClientArea.Controls.Add(this.Line4);
			this.ClientArea.Controls.Add(this.Line3);
			this.ClientArea.Controls.Add(this.Label10);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Controls.Add(this.Label8);
			this.ClientArea.Controls.Add(this.Line2);
			this.ClientArea.Controls.Add(this.Line1);
			this.ClientArea.Controls.Add(this.Label7);
			this.ClientArea.Controls.Add(this.Label6);
			this.ClientArea.Controls.Add(this.lblDuplicateMeters);
			this.ClientArea.Controls.Add(this.lblNotUpdatedMeters);
			this.ClientArea.Controls.Add(this.lblRecordsWithDupMeters);
			this.ClientArea.Controls.Add(this.lblTotalMeters);
			this.ClientArea.Controls.Add(this.lblTotalRecords);
			this.ClientArea.Size = new System.Drawing.Size(882, 538);
			this.ClientArea.TabIndex = 0;
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(882, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(356, 30);
			this.HeaderText.Text = "Electronic Data Entry Summary";
			// 
			// txtNoMatchRecords
			// 
			this.txtNoMatchRecords.AutoSize = false;
			this.txtNoMatchRecords.BackColor = System.Drawing.SystemColors.Window;
			this.txtNoMatchRecords.LinkItem = null;
			this.txtNoMatchRecords.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNoMatchRecords.LinkTopic = null;
			this.txtNoMatchRecords.Location = new System.Drawing.Point(338, 200);
			this.txtNoMatchRecords.LockedOriginal = true;
			this.txtNoMatchRecords.Name = "txtNoMatchRecords";
			this.txtNoMatchRecords.ReadOnly = true;
			this.txtNoMatchRecords.Size = new System.Drawing.Size(80, 40);
			this.txtNoMatchRecords.TabIndex = 6;
			this.txtNoMatchRecords.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtRecordsUpdated
			// 
			this.txtRecordsUpdated.AutoSize = false;
			this.txtRecordsUpdated.BackColor = System.Drawing.SystemColors.Window;
			this.txtRecordsUpdated.LinkItem = null;
			this.txtRecordsUpdated.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRecordsUpdated.LinkTopic = null;
			this.txtRecordsUpdated.Location = new System.Drawing.Point(338, 140);
			this.txtRecordsUpdated.LockedOriginal = true;
			this.txtRecordsUpdated.Name = "txtRecordsUpdated";
			this.txtRecordsUpdated.ReadOnly = true;
			this.txtRecordsUpdated.Size = new System.Drawing.Size(80, 40);
			this.txtRecordsUpdated.TabIndex = 4;
			this.txtRecordsUpdated.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtDuplicateReadings
			// 
			this.txtDuplicateReadings.AutoSize = false;
			this.txtDuplicateReadings.BackColor = System.Drawing.SystemColors.Window;
			this.txtDuplicateReadings.LinkItem = null;
			this.txtDuplicateReadings.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDuplicateReadings.LinkTopic = null;
			this.txtDuplicateReadings.Location = new System.Drawing.Point(338, 320);
			this.txtDuplicateReadings.LockedOriginal = true;
			this.txtDuplicateReadings.Name = "txtDuplicateReadings";
			this.txtDuplicateReadings.ReadOnly = true;
			this.txtDuplicateReadings.Size = new System.Drawing.Size(80, 40);
			this.txtDuplicateReadings.TabIndex = 10;
			this.txtDuplicateReadings.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtMetersUpdated
			// 
			this.txtMetersUpdated.AutoSize = false;
			this.txtMetersUpdated.BackColor = System.Drawing.SystemColors.Window;
			this.txtMetersUpdated.LinkItem = null;
			this.txtMetersUpdated.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtMetersUpdated.LinkTopic = null;
			this.txtMetersUpdated.Location = new System.Drawing.Point(753, 140);
			this.txtMetersUpdated.LockedOriginal = true;
			this.txtMetersUpdated.Name = "txtMetersUpdated";
			this.txtMetersUpdated.ReadOnly = true;
			this.txtMetersUpdated.Size = new System.Drawing.Size(80, 40);
			this.txtMetersUpdated.TabIndex = 14;
			this.txtMetersUpdated.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// vsNoMatch
			// 
			this.vsNoMatch.AllowSelection = false;
			this.vsNoMatch.AllowUserToResizeColumns = false;
			this.vsNoMatch.AllowUserToResizeRows = false;
			this.vsNoMatch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsNoMatch.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsNoMatch.BackColorBkg = System.Drawing.Color.Empty;
			this.vsNoMatch.BackColorFixed = System.Drawing.Color.Empty;
			this.vsNoMatch.BackColorSel = System.Drawing.Color.Empty;
			this.vsNoMatch.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsNoMatch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsNoMatch.ColumnHeadersHeight = 30;
			this.vsNoMatch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsNoMatch.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsNoMatch.DragIcon = null;
			this.vsNoMatch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsNoMatch.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsNoMatch.ExtendLastCol = true;
			this.vsNoMatch.FixedCols = 0;
			this.vsNoMatch.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsNoMatch.FrozenCols = 0;
			this.vsNoMatch.GridColor = System.Drawing.Color.Empty;
			this.vsNoMatch.GridColorFixed = System.Drawing.Color.Empty;
			this.vsNoMatch.Location = new System.Drawing.Point(30, 466);
			this.vsNoMatch.Name = "vsNoMatch";
			this.vsNoMatch.ReadOnly = true;
			this.vsNoMatch.RowHeadersVisible = false;
			this.vsNoMatch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsNoMatch.RowHeightMin = 0;
			this.vsNoMatch.Rows = 1;
			this.vsNoMatch.ScrollTipText = null;
			this.vsNoMatch.ShowColumnVisibilityMenu = false;
			this.vsNoMatch.Size = new System.Drawing.Size(186, 217);
			this.vsNoMatch.StandardTab = true;
			this.vsNoMatch.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsNoMatch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsNoMatch.TabIndex = 21;
			// 
			// txtDuplicateMeters
			// 
			this.txtDuplicateMeters.AutoSize = false;
			this.txtDuplicateMeters.BackColor = System.Drawing.SystemColors.Window;
			this.txtDuplicateMeters.LinkItem = null;
			this.txtDuplicateMeters.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtDuplicateMeters.LinkTopic = null;
			this.txtDuplicateMeters.Location = new System.Drawing.Point(753, 260);
			this.txtDuplicateMeters.LockedOriginal = true;
			this.txtDuplicateMeters.Name = "txtDuplicateMeters";
			this.txtDuplicateMeters.ReadOnly = true;
			this.txtDuplicateMeters.Size = new System.Drawing.Size(80, 40);
			this.txtDuplicateMeters.TabIndex = 18;
			this.txtDuplicateMeters.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtNotUpdatedMeters
			// 
			this.txtNotUpdatedMeters.AutoSize = false;
			this.txtNotUpdatedMeters.BackColor = System.Drawing.SystemColors.Window;
			this.txtNotUpdatedMeters.LinkItem = null;
			this.txtNotUpdatedMeters.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtNotUpdatedMeters.LinkTopic = null;
			this.txtNotUpdatedMeters.Location = new System.Drawing.Point(753, 200);
			this.txtNotUpdatedMeters.LockedOriginal = true;
			this.txtNotUpdatedMeters.Name = "txtNotUpdatedMeters";
			this.txtNotUpdatedMeters.ReadOnly = true;
			this.txtNotUpdatedMeters.Size = new System.Drawing.Size(80, 40);
			this.txtNotUpdatedMeters.TabIndex = 16;
			this.txtNotUpdatedMeters.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtRecordsWithDupMeters
			// 
			this.txtRecordsWithDupMeters.AutoSize = false;
			this.txtRecordsWithDupMeters.BackColor = System.Drawing.SystemColors.Window;
			this.txtRecordsWithDupMeters.LinkItem = null;
			this.txtRecordsWithDupMeters.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtRecordsWithDupMeters.LinkTopic = null;
			this.txtRecordsWithDupMeters.Location = new System.Drawing.Point(338, 260);
			this.txtRecordsWithDupMeters.LockedOriginal = true;
			this.txtRecordsWithDupMeters.Name = "txtRecordsWithDupMeters";
			this.txtRecordsWithDupMeters.ReadOnly = true;
			this.txtRecordsWithDupMeters.Size = new System.Drawing.Size(80, 40);
			this.txtRecordsWithDupMeters.TabIndex = 8;
			this.txtRecordsWithDupMeters.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtTotalMeters
			// 
			this.txtTotalMeters.AutoSize = false;
			this.txtTotalMeters.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotalMeters.LinkItem = null;
			this.txtTotalMeters.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTotalMeters.LinkTopic = null;
			this.txtTotalMeters.Location = new System.Drawing.Point(753, 80);
			this.txtTotalMeters.LockedOriginal = true;
			this.txtTotalMeters.Name = "txtTotalMeters";
			this.txtTotalMeters.ReadOnly = true;
			this.txtTotalMeters.Size = new System.Drawing.Size(80, 40);
			this.txtTotalMeters.TabIndex = 12;
			this.txtTotalMeters.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtTotalRecords
			// 
			this.txtTotalRecords.AutoSize = false;
			this.txtTotalRecords.BackColor = System.Drawing.SystemColors.Window;
			this.txtTotalRecords.LinkItem = null;
			this.txtTotalRecords.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtTotalRecords.LinkTopic = null;
			this.txtTotalRecords.Location = new System.Drawing.Point(338, 80);
			this.txtTotalRecords.LockedOriginal = true;
			this.txtTotalRecords.Name = "txtTotalRecords";
			this.txtTotalRecords.ReadOnly = true;
			this.txtTotalRecords.Size = new System.Drawing.Size(80, 40);
			this.txtTotalRecords.TabIndex = 2;
			this.txtTotalRecords.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// vsNotUpdated
			// 
			this.vsNotUpdated.AllowSelection = false;
			this.vsNotUpdated.AllowUserToResizeColumns = false;
			this.vsNotUpdated.AllowUserToResizeRows = false;
			this.vsNotUpdated.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsNotUpdated.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsNotUpdated.BackColorBkg = System.Drawing.Color.Empty;
			this.vsNotUpdated.BackColorFixed = System.Drawing.Color.Empty;
			this.vsNotUpdated.BackColorSel = System.Drawing.Color.Empty;
			this.vsNotUpdated.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsNotUpdated.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsNotUpdated.ColumnHeadersHeight = 30;
			this.vsNotUpdated.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsNotUpdated.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsNotUpdated.DragIcon = null;
			this.vsNotUpdated.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsNotUpdated.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsNotUpdated.ExtendLastCol = true;
			this.vsNotUpdated.FixedCols = 0;
			this.vsNotUpdated.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsNotUpdated.FrozenCols = 0;
			this.vsNotUpdated.GridColor = System.Drawing.Color.Empty;
			this.vsNotUpdated.GridColorFixed = System.Drawing.Color.Empty;
			this.vsNotUpdated.Location = new System.Drawing.Point(238, 466);
			this.vsNotUpdated.Name = "vsNotUpdated";
			this.vsNotUpdated.ReadOnly = true;
			this.vsNotUpdated.RowHeadersVisible = false;
			this.vsNotUpdated.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsNotUpdated.RowHeightMin = 0;
			this.vsNotUpdated.Rows = 1;
			this.vsNotUpdated.ScrollTipText = null;
			this.vsNotUpdated.ShowColumnVisibilityMenu = false;
			this.vsNotUpdated.Size = new System.Drawing.Size(199, 217);
			this.vsNotUpdated.StandardTab = true;
			this.vsNotUpdated.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsNotUpdated.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsNotUpdated.TabIndex = 23;
			// 
			// vsDuplicates
			// 
			this.vsDuplicates.AllowSelection = false;
			this.vsDuplicates.AllowUserToResizeColumns = false;
			this.vsDuplicates.AllowUserToResizeRows = false;
			this.vsDuplicates.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDuplicates.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDuplicates.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDuplicates.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDuplicates.BackColorSel = System.Drawing.Color.Empty;
			this.vsDuplicates.Cols = 4;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDuplicates.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsDuplicates.ColumnHeadersHeight = 30;
			this.vsDuplicates.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDuplicates.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsDuplicates.DragIcon = null;
			this.vsDuplicates.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDuplicates.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsDuplicates.ExtendLastCol = true;
			this.vsDuplicates.FixedCols = 0;
			this.vsDuplicates.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDuplicates.FrozenCols = 0;
			this.vsDuplicates.GridColor = System.Drawing.Color.Empty;
			this.vsDuplicates.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDuplicates.Location = new System.Drawing.Point(461, 466);
			this.vsDuplicates.Name = "vsDuplicates";
			this.vsDuplicates.ReadOnly = true;
			this.vsDuplicates.RowHeadersVisible = false;
			this.vsDuplicates.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDuplicates.RowHeightMin = 0;
			this.vsDuplicates.Rows = 1;
			this.vsDuplicates.ScrollTipText = null;
			this.vsDuplicates.ShowColumnVisibilityMenu = false;
			this.vsDuplicates.Size = new System.Drawing.Size(359, 217);
			this.vsDuplicates.StandardTab = true;
			this.vsDuplicates.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDuplicates.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDuplicates.TabIndex = 25;
			// 
			// lblNoMatchRecords
			// 
			this.lblNoMatchRecords.Location = new System.Drawing.Point(30, 214);
			this.lblNoMatchRecords.Name = "lblNoMatchRecords";
			this.lblNoMatchRecords.Size = new System.Drawing.Size(254, 18);
			this.lblNoMatchRecords.TabIndex = 5;
			this.lblNoMatchRecords.Text = "TOTAL RECORDS WITH NO MATCH FOUND";
			// 
			// lblRecordsUpdated
			// 
			this.lblRecordsUpdated.Location = new System.Drawing.Point(30, 154);
			this.lblRecordsUpdated.Name = "lblRecordsUpdated";
			this.lblRecordsUpdated.Size = new System.Drawing.Size(203, 18);
			this.lblRecordsUpdated.TabIndex = 3;
			this.lblRecordsUpdated.Text = "TOTAL RECORDS UPDATED";
			// 
			// lblDuplicateReadings
			// 
			this.lblDuplicateReadings.Location = new System.Drawing.Point(30, 334);
			this.lblDuplicateReadings.Name = "lblDuplicateReadings";
			this.lblDuplicateReadings.Size = new System.Drawing.Size(203, 18);
			this.lblDuplicateReadings.TabIndex = 9;
			this.lblDuplicateReadings.Text = "TOTAL DUPLICATE READINGS";
			// 
			// lblMetersUpdated
			// 
			this.lblMetersUpdated.Location = new System.Drawing.Point(480, 154);
			this.lblMetersUpdated.Name = "lblMetersUpdated";
			this.lblMetersUpdated.Size = new System.Drawing.Size(201, 18);
			this.lblMetersUpdated.TabIndex = 13;
			this.lblMetersUpdated.Text = "TOTAL METERS UPDATED";
			// 
			// Line5
			// 
			this.Line5.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line5.BorderWidth = ((short)(1));
			this.Line5.LineWidth = 0;
			this.Line5.Location = new System.Drawing.Point(2250, 3857);
			this.Line5.Name = "Line5";
			this.Line5.Size = new System.Drawing.Size(1230, 1);
			this.Line5.X1 = 3480F;
			this.Line5.X2 = 2250F;
			this.Line5.Y1 = 3857F;
			this.Line5.Y2 = 3857F;
			// 
			// Line4
			// 
			this.Line4.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line4.BorderWidth = ((short)(1));
			this.Line4.LineWidth = 0;
			this.Line4.Location = new System.Drawing.Point(5190, 3857);
			this.Line4.Name = "Line4";
			this.Line4.Size = new System.Drawing.Size(2010, 1);
			this.Line4.X1 = 7200F;
			this.Line4.X2 = 5190F;
			this.Line4.Y1 = 3857F;
			this.Line4.Y2 = 3857F;
			// 
			// Line3
			// 
			this.Line3.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line3.BorderWidth = ((short)(1));
			this.Line3.LineWidth = 0;
			this.Line3.Location = new System.Drawing.Point(60, 3857);
			this.Line3.Name = "Line3";
			this.Line3.Size = new System.Drawing.Size(2010, 1);
			this.Line3.X1 = 2070F;
			this.Line3.X2 = 60F;
			this.Line3.Y1 = 3857F;
			this.Line3.Y2 = 3857F;
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(578, 436);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(137, 18);
			this.Label10.TabIndex = 24;
			this.Label10.Text = "DUPLICATE RECORDS";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(297, 436);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(88, 18);
			this.Label9.TabIndex = 22;
			this.Label9.Text = "NOT UPDATED";
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(30, 436);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(132, 18);
			this.Label8.TabIndex = 20;
			this.Label8.Text = "NO MATCH FOUND";
			// 
			// Line2
			// 
			this.Line2.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line2.BorderWidth = ((short)(1));
			this.Line2.LineWidth = 0;
			this.Line2.Location = new System.Drawing.Point(3450, 3575);
			this.Line2.Name = "Line2";
			this.Line2.Size = new System.Drawing.Size(2010, 1);
			this.Line2.X1 = 5460F;
			this.Line2.X2 = 3450F;
			this.Line2.Y1 = 3575F;
			this.Line2.Y2 = 3575F;
			// 
			// Line1
			// 
			this.Line1.BorderColor = System.Drawing.SystemColors.WindowText;
			this.Line1.BorderWidth = ((short)(1));
			this.Line1.LineWidth = 0;
			this.Line1.Location = new System.Drawing.Point(3510, 251);
			this.Line1.Name = "Line1";
			this.Line1.Size = new System.Drawing.Size(2010, 1);
			this.Line1.X1 = 5520F;
			this.Line1.X2 = 3510F;
			this.Line1.Y1 = 251F;
			this.Line1.Y2 = 251F;
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(30, 30);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(134, 20);
			this.Label7.TabIndex = 1;
			this.Label7.Text = "SUMMARY";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(30, 394);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(134, 20);
			this.Label6.TabIndex = 19;
			this.Label6.Text = "DETAIL";
			// 
			// lblDuplicateMeters
			// 
			this.lblDuplicateMeters.Location = new System.Drawing.Point(480, 274);
			this.lblDuplicateMeters.Name = "lblDuplicateMeters";
			this.lblDuplicateMeters.Size = new System.Drawing.Size(201, 18);
			this.lblDuplicateMeters.TabIndex = 17;
			this.lblDuplicateMeters.Text = "TOTAL DUPLICATE METERS";
			// 
			// lblNotUpdatedMeters
			// 
			this.lblNotUpdatedMeters.Location = new System.Drawing.Point(480, 214);
			this.lblNotUpdatedMeters.Name = "lblNotUpdatedMeters";
			this.lblNotUpdatedMeters.Size = new System.Drawing.Size(201, 18);
			this.lblNotUpdatedMeters.TabIndex = 15;
			this.lblNotUpdatedMeters.Text = "TOTAL METERS NOT UPDATED";
			// 
			// lblRecordsWithDupMeters
			// 
			this.lblRecordsWithDupMeters.Location = new System.Drawing.Point(30, 274);
			this.lblRecordsWithDupMeters.Name = "lblRecordsWithDupMeters";
			this.lblRecordsWithDupMeters.Size = new System.Drawing.Size(203, 18);
			this.lblRecordsWithDupMeters.TabIndex = 7;
			this.lblRecordsWithDupMeters.Text = "TOTAL RECORDS NOT UPDATED";
			// 
			// lblTotalMeters
			// 
			this.lblTotalMeters.Location = new System.Drawing.Point(480, 94);
			this.lblTotalMeters.Name = "lblTotalMeters";
			this.lblTotalMeters.Size = new System.Drawing.Size(228, 18);
			this.lblTotalMeters.TabIndex = 11;
			this.lblTotalMeters.Text = "TOTAL METERS IN SELECTED BOOKS";
			// 
			// lblTotalRecords
			// 
			this.lblTotalRecords.Location = new System.Drawing.Point(30, 94);
			this.lblTotalRecords.Name = "lblTotalRecords";
			this.lblTotalRecords.Size = new System.Drawing.Size(201, 18);
			this.lblTotalRecords.TabIndex = 1;
			this.lblTotalRecords.Text = "TOTAL RECORDS IN FILE";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessPrint,
				this.mnuFilePreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessPrint
			// 
			this.mnuProcessPrint.Index = 0;
			this.mnuProcessPrint.Name = "mnuProcessPrint";
			this.mnuProcessPrint.Text = "Print";
			this.mnuProcessPrint.Click += new System.EventHandler(this.mnuProcessPrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFilePreiew
			// 
			this.cmdFilePreiew.AppearanceKey = "acceptButton";
			this.cmdFilePreiew.Location = new System.Drawing.Point(287, 36);
			this.cmdFilePreiew.Name = "cmdFilePreiew";
			this.cmdFilePreiew.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreiew.Size = new System.Drawing.Size(155, 48);
			this.cmdFilePreiew.TabIndex = 0;
			this.cmdFilePreiew.Text = "Print Preview";
			this.cmdFilePreiew.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// frmElectronicDataEntrySummary
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(882, 706);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmElectronicDataEntrySummary";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Electronic Data Entry Summary";
			this.Load += new System.EventHandler(this.frmElectronicDataEntrySummary_Load);
			this.Activated += new System.EventHandler(this.frmElectronicDataEntrySummary_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmElectronicDataEntrySummary_KeyPress);
			this.Resize += new System.EventHandler(this.frmElectronicDataEntrySummary_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsNoMatch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsNotUpdated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsDuplicates)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreiew)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFilePreiew;
	}
}
