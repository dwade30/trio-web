﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptLienDateChart.
	/// </summary>
	public partial class rptLienDateChart : BaseSectionReport
	{
		public rptLienDateChart()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Date Chart";
		}

		public static rptLienDateChart InstancePtr
		{
			get
			{
				return (rptLienDateChart)Sys.GetInstance(typeof(rptLienDateChart));
			}
		}

		protected rptLienDateChart _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLienDateChart	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int lngRowCounter;
		int lngColRateKey;
		int lngColType;
		int lngColDescription;
		int lngColCommitmentDate;
		int lngColDueDate;
		int lngCol30DNDate1;
		int lngCol30DNDate2;
		int lngCOl30DNDate;
		int lngColLien1;
		int lngColLien2;
		int lngColLienDate;
		int lngColMaturityDate;
		int lngColMaturityNotice1;
		int lngColMaturityNotice2;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				lngRowCounter += 1;
				if (lngRowCounter <= frmUTLienDates.InstancePtr.vsRate.Rows - 1)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As Variant --> As string
			// vbPorter upgrade warning: Label3 As Variant --> As string
			// vbPorter upgrade warning: Label7 As Variant --> As string
			//string Label2, Label3, Label7;  // - "AutoDim"
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			lngRowCounter = 1;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			this.Document.Printer.DefaultPageSettings.Landscape = true;
			lngColRateKey = 0;
			lngColType = 4;
			lngColDescription = 5;
			lngColCommitmentDate = 6;
			lngColDueDate = 7;
			lngCol30DNDate1 = 8;
			lngCol30DNDate2 = 9;
			lngCOl30DNDate = 10;
			lngColLien1 = 11;
			lngColLien2 = 12;
			lngColLienDate = 13;
			lngColMaturityNotice1 = 14;
			lngColMaturityNotice2 = 15;
			lngColMaturityDate = 16;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldKey As Variant --> As string
			// vbPorter upgrade warning: fldType As Variant --> As string
			// vbPorter upgrade warning: fldDescription As Variant --> As string
			// vbPorter upgrade warning: fldCommitment As Variant --> As string
			// vbPorter upgrade warning: fldDueDate As Variant --> As string
			// vbPorter upgrade warning: fldStart30DN As Variant --> As string
			// vbPorter upgrade warning: fldEnd30DN As Variant --> As string
			// vbPorter upgrade warning: fld30DNDate As Variant --> As string
			// vbPorter upgrade warning: fldStartLien As Variant --> As string
			// vbPorter upgrade warning: fldEndLien As Variant --> As string
			// vbPorter upgrade warning: fldLienDate As Variant --> As string
			// vbPorter upgrade warning: fldStartMat As Variant --> As string
			// vbPorter upgrade warning: fldEndMat As Variant --> As string
			// vbPorter upgrade warning: fldMatDate As Variant --> As string
			//string fldKey = "", fldType = "", fldDescription = "", fldCommitment = "", fldDueDate = "", fldStart30DN = "", fldEnd30DN = "", fld30DNDate = "";
			//string fldStartLien = "", fldEndLien = "", fldLienDate = "", fldStartMat = "", fldEndMat = "", fldMatDate = ""; // - "AutoDim"
			fldKey.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColRateKey);
			fldType.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColType);
			fldDescription.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColDescription);
			fldCommitment.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColCommitmentDate);
			fldDueDate.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColDueDate);
			fldStart30DN.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngCol30DNDate1);
			fldEnd30DN.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngCol30DNDate2);
			fld30DNDate.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngCOl30DNDate);
			fldStartLien.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColLien1);
			fldEndLien.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColLien2);
			fldLienDate.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColLienDate);
			fldStartMat.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColMaturityNotice1);
			fldEndMat.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColMaturityNotice2);
			fldMatDate.Text = frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRowCounter, lngColMaturityDate);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As Variant --> As string
			//string Label4;  // - "AutoDim"
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptLienDateChart_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLienDateChart properties;
			//rptLienDateChart.Caption	= "Lien Date Chart";
			//rptLienDateChart.Left	= 0;
			//rptLienDateChart.Top	= 0;
			//rptLienDateChart.Width	= 15240;
			//rptLienDateChart.Height	= 11115;
			//rptLienDateChart.StartUpPosition	= 3;
			//rptLienDateChart.SectionData	= "rptLienDateChart.dsx":0000;
			//End Unmaped Properties
		}
	}
}
