namespace UtilityBillingReports.Reports
{
	/// <summary>
	/// Summary description for rptConsumptionHistory.
	/// </summary>
	partial class rptConsumptionHistory
	{

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptConsumptionHistory));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblMeterNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrev = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurr = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCriteria = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNeg = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMeterNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCurr = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrev = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldConsumption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNeg = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNegConsumption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNegConsTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrev)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCriteria)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNeg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurr)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrev)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNeg)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNegConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNegConsTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldMeterNumber,
            this.fldBillDate,
            this.fldCurr,
            this.fldPrev,
            this.fldConsumption,
            this.fldNeg});
			this.Detail.Height = 0.2395833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldTotal,
            this.Line1,
            this.fldTotalCount,
            this.fldNegConsumption,
            this.fldNegConsTitle});
			this.ReportFooter.Height = 0.5625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblMeterNumber,
            this.lblBillDate,
            this.lblPrev,
            this.lblCurr,
            this.lblReportHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPageNumber,
            this.Line2,
            this.Label2,
            this.lblCriteria,
            this.lblNeg});
			this.PageHeader.Height = 0.8125F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblMeterNumber
			// 
			this.lblMeterNumber.Height = 0.1875F;
			this.lblMeterNumber.HyperLink = null;
			this.lblMeterNumber.Left = 0.125F;
			this.lblMeterNumber.Name = "lblMeterNumber";
			this.lblMeterNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblMeterNumber.Text = "Meter";
			this.lblMeterNumber.Top = 0.625F;
			this.lblMeterNumber.Width = 0.625F;
			// 
			// lblBillDate
			// 
			this.lblBillDate.Height = 0.1875F;
			this.lblBillDate.HyperLink = null;
			this.lblBillDate.Left = 1.4375F;
			this.lblBillDate.Name = "lblBillDate";
			this.lblBillDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblBillDate.Text = "Bill Date";
			this.lblBillDate.Top = 0.625F;
			this.lblBillDate.Width = 1.5625F;
			// 
			// lblPrev
			// 
			this.lblPrev.Height = 0.1875F;
			this.lblPrev.HyperLink = null;
			this.lblPrev.Left = 3.3125F;
			this.lblPrev.Name = "lblPrev";
			this.lblPrev.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 0";
			this.lblPrev.Text = "Previous";
			this.lblPrev.Top = 0.625F;
			this.lblPrev.Width = 1F;
			// 
			// lblCurr
			// 
			this.lblCurr.Height = 0.1875F;
			this.lblCurr.HyperLink = null;
			this.lblCurr.Left = 4.5F;
			this.lblCurr.Name = "lblCurr";
			this.lblCurr.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 0";
			this.lblCurr.Text = "Current";
			this.lblCurr.Top = 0.625F;
			this.lblCurr.Width = 1F;
			// 
			// lblReportHeader
			// 
			this.lblReportHeader.Height = 0.25F;
			this.lblReportHeader.HyperLink = null;
			this.lblReportHeader.Left = 0F;
			this.lblReportHeader.Name = "lblReportHeader";
			this.lblReportHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
			this.lblReportHeader.Text = "Consumption History";
			this.lblReportHeader.Top = 0F;
			this.lblReportHeader.Width = 7F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.8125F;
			this.Line2.Width = 7F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7F;
			this.Line2.Y1 = 0.8125F;
			this.Line2.Y2 = 0.8125F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 5.6875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 0";
			this.Label2.Text = "Consumption";
			this.Label2.Top = 0.625F;
			this.Label2.Width = 1F;
			// 
			// lblCriteria
			// 
			this.lblCriteria.Height = 0.375F;
			this.lblCriteria.HyperLink = null;
			this.lblCriteria.Left = 0F;
			this.lblCriteria.Name = "lblCriteria";
			this.lblCriteria.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblCriteria.Text = null;
			this.lblCriteria.Top = 0.25F;
			this.lblCriteria.Width = 7F;
			// 
			// lblNeg
			// 
			this.lblNeg.Height = 0.1875F;
			this.lblNeg.HyperLink = null;
			this.lblNeg.Left = 0.875F;
			this.lblNeg.Name = "lblNeg";
			this.lblNeg.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblNeg.Text = "Neg";
			this.lblNeg.Top = 0.625F;
			this.lblNeg.Width = 0.375F;
			// 
			// fldMeterNumber
			// 
			this.fldMeterNumber.Height = 0.1875F;
			this.fldMeterNumber.Left = 0.125F;
			this.fldMeterNumber.Name = "fldMeterNumber";
			this.fldMeterNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldMeterNumber.Text = null;
			this.fldMeterNumber.Top = 0F;
			this.fldMeterNumber.Width = 0.625F;
			// 
			// fldBillDate
			// 
			this.fldBillDate.Height = 0.1875F;
			this.fldBillDate.Left = 1.4375F;
			this.fldBillDate.Name = "fldBillDate";
			this.fldBillDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldBillDate.Text = null;
			this.fldBillDate.Top = 0F;
			this.fldBillDate.Width = 1.5625F;
			// 
			// fldCurr
			// 
			this.fldCurr.Height = 0.1875F;
			this.fldCurr.Left = 4.5F;
			this.fldCurr.Name = "fldCurr";
			this.fldCurr.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldCurr.Text = null;
			this.fldCurr.Top = 0F;
			this.fldCurr.Width = 1F;
			// 
			// fldPrev
			// 
			this.fldPrev.Height = 0.1875F;
			this.fldPrev.Left = 3.3125F;
			this.fldPrev.Name = "fldPrev";
			this.fldPrev.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldPrev.Text = null;
			this.fldPrev.Top = 0F;
			this.fldPrev.Width = 1F;
			// 
			// fldConsumption
			// 
			this.fldConsumption.Height = 0.1875F;
			this.fldConsumption.Left = 5.75F;
			this.fldConsumption.Name = "fldConsumption";
			this.fldConsumption.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldConsumption.Text = null;
			this.fldConsumption.Top = 0F;
			this.fldConsumption.Width = 0.9375F;
			// 
			// fldNeg
			// 
			this.fldNeg.Height = 0.1875F;
			this.fldNeg.Left = 0.875F;
			this.fldNeg.Name = "fldNeg";
			this.fldNeg.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldNeg.Text = null;
			this.fldNeg.Top = 0F;
			this.fldNeg.Width = 0.375F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 5.6875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 5.8125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0F;
			this.Line1.Width = 0.9375F;
			this.Line1.X1 = 5.8125F;
			this.Line1.X2 = 6.75F;
			this.Line1.Y1 = 0F;
			this.Line1.Y2 = 0F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 3.0625F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 0";
			this.fldTotalCount.Text = "Total Consumption:";
			this.fldTotalCount.Top = 0F;
			this.fldTotalCount.Width = 2.5F;
			// 
			// fldNegConsumption
			// 
			this.fldNegConsumption.Height = 0.1875F;
			this.fldNegConsumption.Left = 5.6875F;
			this.fldNegConsumption.Name = "fldNegConsumption";
			this.fldNegConsumption.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldNegConsumption.Text = null;
			this.fldNegConsumption.Top = 0.25F;
			this.fldNegConsumption.Width = 1F;
			// 
			// fldNegConsTitle
			// 
			this.fldNegConsTitle.Height = 0.1875F;
			this.fldNegConsTitle.Left = 3.0625F;
			this.fldNegConsTitle.Name = "fldNegConsTitle";
			this.fldNegConsTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 0";
			this.fldNegConsTitle.Text = "Total Negative Consumption:";
			this.fldNegConsTitle.Top = 0.25F;
			this.fldNegConsTitle.Width = 2.5F;
			// 
			// rptConsumptionHistory
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.010417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblMeterNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrev)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCriteria)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNeg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCurr)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrev)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNeg)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNegConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNegConsTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMeterNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCurr;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrev;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConsumption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNeg;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNegConsumption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNegConsTitle;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMeterNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrev;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurr;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCriteria;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNeg;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
