﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.ComponentModel;
using SharedApplication.Extensions;
using modUTStatusPayments = Global.modUTFunctions;
using Wisej.Core;
using Wisej.Web.Ext.CustomProperties;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmDataEntry.
	/// </summary>
	public partial class frmDataEntry : BaseForm
	{
		public frmDataEntry()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            txtDate.AllowOnlyNumericInput();
            txtReading.AllowOnlyNumericInput();
            txtConsumption.AllowOnlyNumericInput();
            txtOverrideSC.AllowOnlyNumericInput();
            txtOverrideWC.AllowOnlyNumericInput();
            txtOverrideWA.AllowOnlyNumericInput();
            txtOverrideSA.AllowOnlyNumericInput();
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDataEntry InstancePtr
		{
			get
			{
				return (frmDataEntry)Sys.GetInstance(typeof(frmDataEntry));
			}
		}

		protected frmDataEntry _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/25/2006              *
		// ********************************************************
		clsDRWrapper rsBook;
		// holds book information
		clsDRWrapper rsMeter;
		// holds metertable information
		clsDRWrapper rsAcct;
		// holds master information
		clsDRWrapper rsHistory;
		// holds bill information
		int i;
		// generic counter
		int MaxRows;
		// holds the total number of rows in the history flexgrid
		int lngAcctNum;
		// holds the current account number
		int lngMeterKey;
		// holds the current meter number
		bool Reading;
		// True if entering Reading, False if entering Consumption
		bool Calculation;
		// True if stop on calculation, False to continue
		bool Dirty;
		string strSQL = "";
		string BillingCode = "";
		bool BillingCodeFlag;
		bool boolMeterUseFlat;
		bool boolMeterUseCons;
		bool boolMeterUseAdjust;
		bool boolMeterUseUnit;
		string strAdjustmentCombo;
		bool boolLockBoxes;
		int lngBookNumber;
		int lngDEOrder;
		int lngTownUnits;
		bool boolDoNotSaveAgain;
		bool boolDoNotValidate;
		// Used when meters are rolled over - avoids double prompt for verification
		string strAccountsNotIncluded = "";

        private bool boolExpandHist = false;
		// Accounts that cannot have pre-payments created because existing bills have amounts owed
		int lngRateColWaterTitle;
		int lngRateColWaterType;
		int lngRateColWaterRK;
		int lngRateColSewerTitle;
		int lngRateColSewerType;
		int lngRateColSewerRK;
		int lngAdjColCode;
		int lngAdjColDescription;
		int lngAdjColAmount;
		int lngAdjColAccount;
		int lngAdjColServiceAllowed;
		int lngAdjColService;
		bool boolBangor;
		// kk08252016 trouts-203  Don't automatically create a bill for Bangor
		bool isTxtReadingModifiedNedeed = false;
        bool acctSameBillOwner;
        bool acctWBillToOwner;
        bool acctSBillToOwner;

        public void Init(ref int lngPassBookNumber)
		{
            try
            {
                // On Error GoTo ERROR_HANDLER
                lngBookNumber = lngPassBookNumber;
				ShowBook();
				lblBookNumber.Text = FCConvert.ToString(lngBookNumber);
				if (modUTBilling.CheckForUTAccountsSetup())
				{
					this.Show(App.MainForm);
					if (!Reading)
					{
						if (txtConsumption.Enabled && txtConsumption.Visible)
						{
							txtConsumption.Focus();
						}
					}
					else
					{
						////FC:FINAL:MSH - Issue #940: send focus to txtReading, because after closing previous form focus won't be correct and it 
						//// will be sended to comboBox.
						//if (txtReading.Enabled && txtReading.Visible)
						//{
						//	txtReading.Focus();
      //                      FCUtils.ApplicationUpdate(txtReading);
						//}
					}
				}
				else
				{
					MessageBox.Show("Before continuing, please setup your UT Accounts in File Maintenance > Customize.", "Missing Valid Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Unload();
				}
				return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

		private void FrmDataEntry_Appear(object sender, EventArgs e)
		{
			//FC:FINAL:MSH - Issue #940: after closing the previous form focus in our form will be incorrect. We send focus to appropriate box and if this box is 
			// txtReading, we must change Modified property, because in fecherFoundadion in OnValidating method if Modified == false validating from the our
			// form won't be called
			this.Focus();
			if (!Reading)
			{
				if (txtConsumption.Enabled && txtConsumption.Visible)
				{
					txtConsumption.Focus();
				}
			}
			else
			{
				//FC:FINAL:MSH - Issue #940: sends focus to txtReading, because after closing previous form focus won't be correct and it 
				// will be sended to comboBox.
				if (txtReading.Enabled && txtReading.Visible)
				{
					isTxtReadingModifiedNedeed = true;
					txtReading.Focus();
				}
			}
		}

		private void History_Check()
		{
			// this sub will check to see if the history checkbox has been checked,
			// if so then the history for the meter will be displayed
			// if not the history gets cleared
			switch (chkHistory.CheckState)
			{
				case Wisej.Web.CheckState.Checked:
					{
						MaxRows = 0;
						//FC:FINAL:DDU:#1202 - add the expand button
						vsHistory.AddExpandButton();
						FillHistoryGrid();
						break;
					}
				case Wisej.Web.CheckState.Unchecked:
					{
						//FC:FINAL:BSE:#3728 clear all header controls 
                        vsHistory.ClearHeaderControls();
						vsHistory.Rows = 1;
						// by setting rows to 1 then 7, this will erase all the data in the cells
						FormatHistoryGrid();
						vsHistory_Collapsed();
						break;
					}
			}
			//end switch
		}

		private void chkHistory_CheckedChanged(object sender, System.EventArgs e)
		{
			// checks the checkbox
			History_Check();
		}

		private void ShowBook()
		{
			// valid book numbers are from 1 to 9999
			string strDupSeq = "";
			//FC:FINAL:MSH - Issue #940: in VB6 will be executed LoadForm on enabling cmdNext.
			this.LoadForm();
			cmdNext.Enabled = true;
			cmdPrevious.Enabled = true;
			cmdSave.Enabled = true;
			LockEditBoxes_2(false);
			GetBookInfo(ref lngBookNumber);
			if (CheckForDuplicateSequence(ref lngBookNumber, ref strDupSeq))
			{
				arDuplicateSequence.InstancePtr.Init(ref strDupSeq, this.Modal);
			}
		}

		private void chkNoBill_CheckedChanged(object sender, System.EventArgs e)
		{
			Dirty = true;
		}

		private void cmbSeq_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				for (intCT = 1; intCT <= 4; intCT++)
				{
					if (Conversion.Val(vsAdjust.TextMatrix(intCT, lngAdjColAmount)) != 0)
					{
						SaveInfo();
					}
				}
				GetMeterInfo_2(cmbSeq.ItemData(cmbSeq.SelectedIndex));
				GetAcctInfo_2(rsMeter.Get_Fields_Int32("AccountKey"));
				if (chkNoBill.CheckState == Wisej.Web.CheckState.Checked)
				{
					Dirty = true;
				}
				else
				{
					Dirty = false;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Seq Combo Click", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmbSeq_DropDown(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - Issue #940: leaves handler if ActiveControl is txtReading to prevent multi handling of txtReading_Validating.
			if (this.ActiveControl == txtReading)
			{
				return;
			}
			// MAL@20081016: Change to autosize based on text width not a fixed width
			// Tracker Reference: 15615
			modMain.AutoSizeComboBoxDropDown(this.cmbSeq);
			// SendMessageByNum Me.cmbSeq.hWnd, CB_SETDROPPEDWIDTH, 500, 0
		}

		private void cmbSeq_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
				// any key other than backspace or number keys are not allowed
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDataEntry_Activated(object sender, System.EventArgs e)
		{
			// MAL@20081016: Change to refresh for when returning from Account Master screen
			// Tracker Reference: 15612
			// If FormExist(Me) Then Exit Sub
			DynamicObject customClientData = new DynamicObject();
			if (modMain.FormExist(this))
			{
				GetAcctInfo_2(modUTStatusPayments.GetAccountKeyUT_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text))));
				// XXXXX         rsMeter.Refresh
				GetMeterInfo_2(cmbSeq.ItemData(cmbSeq.SelectedIndex));
				//FC:FINAL:MSH - issue #987: update and send Service value to JavaScript
				customClientData["serviceValue"] = rsMeter.Get_Fields_String("Service");
				customProperties.SetCustomPropertiesValue(txtOverrideSA, customClientData);
				customProperties.SetCustomPropertiesValue(txtOverrideSC, customClientData);
				customProperties.SetCustomPropertiesValue(txtOverrideWA, customClientData);
				customProperties.SetCustomPropertiesValue(txtOverrideWC, customClientData);
				return;
			}
			GetSettings();
			// locks either reading or consumption boxes, tabstop settings
			// LockEditBoxes True
			vsHistory.Rows = 1;
			// by setting rows to 1 then 7, this will erase all the data in the cells
			FormatHistoryGrid();
			FormatRateGrid();
			FormatAdjustGrid();
			//FC:FINAL:MSH - issue #987: save and send Service value to JavaScript
			customClientData["serviceValue"] = rsMeter.Get_Fields_String("Service");
			customProperties.SetCustomPropertiesValue(txtOverrideSA, customClientData);
			customProperties.SetCustomPropertiesValue(txtOverrideSC, customClientData);
			customProperties.SetCustomPropertiesValue(txtOverrideWA, customClientData);
			customProperties.SetCustomPropertiesValue(txtOverrideWC, customClientData);
		}

		private void frmDataEntry_Load(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsWUT = new clsDRWrapper();
				lngRateColWaterTitle = 0;
				lngRateColWaterType = 1;
				lngRateColWaterRK = 2;
				lngRateColSewerTitle = 3;
				lngRateColSewerType = 4;
				lngRateColSewerRK = 5;
				lngAdjColCode = 0;
				lngAdjColDescription = 1;
				lngAdjColAmount = 2;
				lngAdjColAccount = 3;
				lngAdjColServiceAllowed = 4;
				lngAdjColService = 5;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				this.Text = "Data Entry";
				FormatRateGrid();
				FormatAdjustGrid();
				strAdjustmentCombo = GetAdjustList();
				rsWUT.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (rsWUT.EndOfFile() != true && rsWUT.BeginningOfFile() != true)
				{
					rsWUT.MoveFirst();
					if (FCConvert.ToString(rsWUT.Get_Fields_String("DEMethod")) == "C")
					{
						// C = Meter billed by consumption, R = Meter billed by reading
						Reading = false;
					}
					else
					{
						Reading = true;
					}
					if (FCConvert.ToBoolean(rsWUT.Get_Fields_Boolean("ShowDEConsumption")))
					{
						// True = the user wants to see the calculated consumption
						Calculation = true;
						// before they switch to the next meter
					}
					else
					{
						Calculation = false;
					}
					lngDEOrder = FCConvert.ToInt32(Math.Round(Conversion.Val("0" + rsWUT.Get_Fields_Int32("DEOrder"))));
					switch (lngDEOrder)
					{
						case 0:
							{
								// Sequence
								// Disable nothing
								break;
							}
						case 1:
							{
								// Owner
								txtBillTo.Enabled = false;
								break;
							}
						case 2:
							{
								// Tenant
								txtOwner.Enabled = false;
								break;
							}
					}

                    if (rsWUT.Get_Fields_Boolean("DefDisplayHistory"))
                    {
                        boolExpandHist = true;
                        chkHistory.Value = FCCheckBox.ValueSettings.Checked;
                    }
                    else
                    {
                        boolExpandHist = false;
                        chkHistory.Value = FCCheckBox.ValueSettings.Unchecked;
                    }
					Dirty = false;
				}
				// kk trouts-6 03012013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					lblOvrdWater.Text = "Stmwater";
					boolBangor = true;
				}
				else
				{
					boolBangor = false;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Form Load", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			int intCT;
			bool boolAdjustment = false;
			// check for any adjustment
			for (intCT = 1; intCT <= 4; intCT++)
			{
				if (Conversion.Val(vsAdjust.TextMatrix(intCT, lngAdjColAmount)) != 0)
				{
					boolAdjustment = true;
					break;
				}
			}
			if (boolAdjustment)
			{
				SaveInfo();
			}
		}

		private void frmDataEntry_Resize(object sender, System.EventArgs e)
		{
			FormatHistoryGrid();
			FormatRateGrid();
			FormatAdjustGrid();
			vsHistory_Collapsed();
		}

		private void frmDataEntry_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			bool boolDiffCons = false;
			int intCT;
			bool boolAdjustment = false;
			try
			{
				// On Error GoTo HANDLER
				if (KeyAscii == Keys.Escape)
				{
					KeyAscii = (Keys)0;
					this.Unload();
				}
				if (KeyAscii == Keys.Return)
				{
					if (Calculation && this.ActiveControl.GetName() == "txtReading")
					{
						// if calculation is set, the user has to hit enter
						// twice to move to the next meter so that they can see the consumption calculation
						txtConsumption.TabStop = true;
						Support.SendKeys("{tab}", false);
						KeyAscii = (Keys)0;
						return;
					}
					// check for any adjustment
					for (intCT = 1; intCT <= 4; intCT++)
					{
						if (Conversion.Val(vsAdjust.TextMatrix(intCT, lngAdjColAmount)) != 0)
						{
							boolAdjustment = true;
							break;
						}
					}
					if (this.ActiveControl.GetName() == "cmbSeq")
					{
						Support.SendKeys("{tab}", false);
						// if focus is in the comboboxes, the enter will move to the next field
					}
					else
					{
						boolDiffCons = false;
						// boolDiffCons = boolDoNotValidate        'MAL@20070831  ; MAL@20080104
						if (Reading)
						{
							txtReading_Validate(ref boolDiffCons);
						}
						else
						{
							txtConsumption_Validate(ref boolDiffCons);
						}
						// Move to the next meter in the sequence
						if (!boolDiffCons || BilledType_8(3, false))
						{
							// Or (boolAdjustment Or Dirty) Or BilledType(2, True) Or BilledType(3, True) Then
							// Move to the next meter in sequence
							NextMeter_2(true);
							boolDoNotValidate = false;
							// MAL@20080104: Reset the value after going to the next meter
						}
						// Else
						// NextMeter False
						// End If
						KeyAscii = (Keys)0;
					}
				}
				/*? On Error GoTo 0 */
				return;
			}
			catch
			{
				// HANDLER:
				if (KeyAscii == Keys.Return)
				{
					KeyAscii = (Keys)0;
					boolDiffCons = false;
					txtReading_Validate(ref boolDiffCons);
					if (boolDiffCons != true || boolAdjustment)
					{
						// Move to the next meter in sequence
						NextMeter_2(true);
					}
				}
				e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
			}
		}

		private void mnuFileMaster_Click(object sender, System.EventArgs e)
		{
			if (Conversion.Val(txtAccount.Text) != 0)
			{
				frmAccountMaster.InstancePtr.Init(modUTStatusPayments.GetAccountKeyUT_2(FCConvert.ToInt32(Conversion.Val(txtAccount.Text))), 0, true);
			}
		}

		private void mnuNext_Click(object sender, System.EventArgs e)
		{
			int intCT;
			bool boolAdjustment = false;
			for (intCT = 1; intCT <= 4; intCT++)
			{
				if (Conversion.Val(vsAdjust.TextMatrix(intCT, lngAdjColAmount)) != 0)
				{
					boolAdjustment = true;
					break;
				}
			}
			// kk11112014 trout-871  Force validation if de is by consumption
			if (Reading)
			{
				// MAL@20081016: Force Reading Validate before moving on
				// Tracker Reference: 15612
				boolDoNotValidate = false;
				txtReading_Validate_2(false);
			}
			else
			{
				txtConsumption_Validate_2(false);
			}
			NextMeter(ref boolAdjustment);
		}

		private void mnuPrevious_Click(object sender, System.EventArgs e)
		{
			// kk11112014 trout-871  Force validation if de is by consumption
			if (Reading)
			{
				// MAL@20081016: Force Reading validation before moving on
				// Tracker Reference: 15612
				boolDoNotValidate = false;
				txtReading_Validate_2(false);
			}
			else
			{
				txtConsumption_Validate_2(false);
			}
			PreviousMeter();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			// kk11112014 trout-871  Force validation before moving on here also
			// F12 Comes here
			bool boolValFail;
			boolValFail = false;
			if (BilledType_8(1, true))
			{
				// kk08042015 trout-1138  Only validate if billed by consumption
				if (Reading)
				{
					boolDoNotValidate = false;
                    //FC:FINAL:AM:#2790 - avoid triggering 2nd validation
                    if (txtReading.Modified)
                    {
                        txtReading_Validate(ref boolValFail);
                    }
				}
				else
				{
					txtConsumption_Validate(ref boolValFail);
				}
			}
			if (!boolValFail)
			{
				// saves the info
				NextMeter_2(true);
			}
		}

		private void txtConsumption_Change(object sender, System.EventArgs e)
		{
			Dirty = true;
		}

		private void txtConsumption_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if (KeyAscii == 101)
			{
				txtReadingCode.Text = "E";
				txtReadingCode_Validate_2(false);
				KeyAscii = 0;
			}
			else if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
				// any key other than backspace or number keys are not allowed
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtConsumption_Validate_2(bool Cancel)
		{
			txtConsumption_Validate(ref Cancel);
		}

		private void txtConsumption_Validate(ref bool Cancel)
		{
			var cancelArgs = new CancelEventArgs(Cancel);
			txtConsumption_Validate(txtConsumption, cancelArgs);
			Cancel = cancelArgs.Cancel;
		}

		private void txtConsumption_Validate(object sender, CancelEventArgs e)
		{
			if (!Reading)
			{
				// fill the reading field
				txtReading.Text = FCConvert.ToString(Conversion.Val(lblPreviousReadingText.Text) + Conversion.Val(txtConsumption.Text));
			}
		}

		private void txtDate_Change(object sender, System.EventArgs e)
		{
			Dirty = false;
		}

		private void txtDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			string strTest;
			strTest = txtDate.Text;
			if (!Information.IsDate(strTest))
			{
				MessageBox.Show("Please enter a valid date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void txtOverrideSA_Change(object sender, System.EventArgs e)
		{
			Dirty = true;
		}

		private void txtOverrideSA_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if ((KeyAscii < 46 || KeyAscii > 57) && KeyAscii != 8 || FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "W")
			{
				KeyAscii = 0;
				// any key other than backspace or number keys are not allowed
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtOverrideSC_Change(object sender, System.EventArgs e)
		{
			Dirty = true;
		}

		private void txtOverrideSC_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8 || FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "W")
			{
				KeyAscii = 0;
				// any key other than backspace or number keys are not allowed
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtOverrideWA_Change(object sender, System.EventArgs e)
		{
			Dirty = true;
		}

		private void txtOverrideWA_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if (((KeyAscii < 46 || KeyAscii > 57) && KeyAscii != 8) || FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "S")
			{
				KeyAscii = 0;
				// any key other than backspace or number keys are not allowed
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtOverrideWC_Change(object sender, System.EventArgs e)
		{
			Dirty = true;
		}

		private void txtOverrideWC_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8 || FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "S")
			{
				KeyAscii = 0;
				// any key other than backspace or number keys are not allowed
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtReading_Change(object sender, System.EventArgs e)
		{
			Dirty = true;
		}

		private void txtReading_Enter(object sender, System.EventArgs e)
		{
			txtReading.BackColor = ColorTranslator.FromOle(0xD2FFFF);
			// tan
			//FC:FINAL:MSH - Issue #940: change modified if the txtReading was leaved with the empty Text and will got focus again (as in original app)
			if (isTxtReadingModifiedNedeed && string.IsNullOrEmpty(txtReading.Text))
			{
				txtReading.Modified = true;
			}
		}

		private void txtReading_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if (KeyAscii == 101)
			{
				txtReadingCode.Text = "E";
				txtReadingCode_Validate_2(false);
				KeyAscii = 0;
			}
			else if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
			{
				KeyAscii = 0;
				// any key other than backspace or number keys are not allowed
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtReading_Leave(object sender, System.EventArgs e)
		{
			txtReading.BackColor = Color.White;
		}

        private void txtReading_Validate_2(bool Cancel)
        {
            txtReading_Validate(txtReading, new CancelEventArgs(Cancel));
        }

        //FC:FINAL:CHN - issue #958: Choosing not to continue still advanced to next.
        private void txtReading_Validate(ref bool Cancel)
		{
			var eventArgs = new CancelEventArgs(Cancel);
			txtReading_Validate(txtReading, eventArgs);
			Cancel = eventArgs.Cancel;
		}

		private void txtReading_Validate(object sender, CancelEventArgs e)
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_ErrorHandler = 1;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				// checks to make sure that the new reading entered is not below the previous reading
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				// vbPorter upgrade warning: RollOverCons As int	OnWriteFCConvert.ToDouble(
				int RollOverCons = 0;
				clsDRWrapper rsDefaults = new clsDRWrapper();
				int lngAdj = 0;
				// MAL@20081015: Add check for Dirty status before validation
				// Tracker Reference: 15612
				// If Not Dirty Then
				// boolDoNotValidate = True
				// Else
				// boolDoNotValidate = boolDoNotValidate
				// End If
				// 
				// MAL@20080104: Add check for do not validate flag
				// If boolDoNotValidate Then
				// Don't Do Anything - Exit Sub
				// Else
				// kk01072016 trouts-130  Don't validate readings for flat and unit based meters
				if (BilledType_8(1, true))
				{
					vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler;
					/* On Error GoTo ErrorHandler */
					if (txtReading.Text == "")
					{
						//FC:FINAL:MSH - Issue #940: after click on combobox ActiveControl will be changed to combobox
						//if (Reading && this.ActiveControl.GetName() == "txtReading")
						if (Reading)
						{
							ans = MessageBox.Show("The Current Reading is blank.  Would you like to continue?", "Input Error", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
							// yes = 6 no = 7 cancel = 2
							if (ans == DialogResult.Yes)
							{
								// do nothing
							}
							else
							{
								e.Cancel = true;
							}
						}
					}
					else
					{
						// MAL@20070831: Check for Cancel Before Continuing with Validation
						if (!e.Cancel)
						{
							if (Conversion.Val(txtReading.Text) < Conversion.Val(lblPreviousReadingText.Text) && Reading)
							{
								// yes = 6 no = 7 cancel = 2
								lngAdj = 0;
								if (rsMeter.Get_Fields_Int32("Multiplier") == 100000)
								{
									lngAdj = 5;
								}
								else if (rsMeter.Get_Fields_Int32("Multiplier") == 10000)
								{
									lngAdj = 4;
								}
								else if (rsMeter.Get_Fields_Int32("Multiplier") == 1000)
								{
									lngAdj = 3;
								}
								else if (rsMeter.Get_Fields_Int32("Multiplier") == 100)
								{
									lngAdj = 2;
								}
								else if (rsMeter.Get_Fields_Int32("Multiplier") == 10)
								{
									lngAdj = 1;
								}
								else if (rsMeter.Get_Fields_Int32("Multiplier") == 1)
								{
									lngAdj = 0;
								}
								else
								{
									lngAdj = 0;
								}
								switch (modExtraModules.Statics.glngTownReadingUnits)
								{
									case 100000:
										{
											lngAdj -= 5;
											break;
										}
									case 10000:
										{
											lngAdj -= 4;
											break;
										}
									case 1000:
										{
											lngAdj -= 3;
											break;
										}
									case 100:
										{
											lngAdj -= 2;
											break;
										}
									case 10:
										{
											lngAdj -= 1;
											break;
										}
									case 1:
										{
											lngAdj = lngAdj;
											break;
										}
									default:
										{
											lngAdj = lngAdj;
											break;
										}
								}
								//end switch
								RollOverCons = FCConvert.ToInt32(Conversion.Val(txtReading.Text) + ((Math.Pow(10, (rsMeter.Get_Fields_Int32("Digits") - lngAdj))) - Conversion.Val(lblPreviousReadingText.Text)));
								if (RollOverCons != Conversion.Val(txtConsumption.Text))
								{
									ans = MessageBox.Show("The Current Reading is less than Previous Reading.  Has the meter rolled over?", "Input Error", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
									// yes = 6 no = 7 cancel = 2
									if (ans == DialogResult.Yes)
									{
										lngAdj = 0;
										if (rsMeter.Get_Fields_Int32("Multiplier") == 100000)
										{
											lngAdj = 5;
										}
										else if (rsMeter.Get_Fields_Int32("Multiplier") == 10000)
										{
											lngAdj = 4;
										}
										else if (rsMeter.Get_Fields_Int32("Multiplier") == 1000)
										{
											lngAdj = 3;
										}
										else if (rsMeter.Get_Fields_Int32("Multiplier") == 100)
										{
											lngAdj = 2;
										}
										else if (rsMeter.Get_Fields_Int32("Multiplier") == 10)
										{
											lngAdj = 1;
										}
										else if (rsMeter.Get_Fields_Int32("Multiplier") == 1)
										{
											lngAdj = 0;
										}
										else
										{
											lngAdj = 0;
										}
										switch (modExtraModules.Statics.glngTownReadingUnits)
										{
											case 100000:
												{
													lngAdj -= 5;
													break;
												}
											case 10000:
												{
													lngAdj -= 4;
													break;
												}
											case 1000:
												{
													lngAdj -= 3;
													break;
												}
											case 100:
												{
													lngAdj -= 2;
													break;
												}
											case 10:
												{
													lngAdj -= 1;
													break;
												}
											case 1:
												{
													lngAdj = lngAdj;
													break;
												}
											default:
												{
													lngAdj = lngAdj;
													break;
												}
										}
										//end switch
										RollOverCons = FCConvert.ToInt32(Conversion.Val(txtReading.Text) + ((Math.Pow(10, (rsMeter.Get_Fields_Int32("Digits") - lngAdj))) - Conversion.Val(lblPreviousReadingText.Text)));
										// RollOverCons = Val(txtReading.Text) + ((10 ^ (rsMeter.Fields("Digits") - (rsMeter.Fields("Multiplier") - glngTownReadingUnits))) - Val(lblPreviousReadingText.Caption))
										ans = MessageBox.Show("The total consumption is now " + FCConvert.ToString(RollOverCons) + ".  Is this correct?", "Meter Roll Over", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
										if (ans == DialogResult.Yes)
										{
											txtConsumption.Text = FCConvert.ToString(RollOverCons);
											e.Cancel = false;
											boolDoNotValidate = true;
											// MAL@20070831
										}
										else
										{
											txtConsumption.Text = FCConvert.ToString(0);
											e.Cancel = true;
											boolDoNotValidate = false;
											// MAL@20070831
										}
									}
									else
									{
										txtConsumption.Text = FCConvert.ToString(0);
										e.Cancel = true;
									}
								}
							}
							else if (Calculation)
							{
								txtConsumption.Text = FCConvert.ToString(Conversion.Val(txtReading.Text) - Conversion.Val(lblPreviousReadingText.Text));
								// txtConsumption.SetFocus
								txtConsumption.TabStop = true;
							}
							else if (!Reading)
							{
								txtReading.Text = FCConvert.ToString(Conversion.Val(txtConsumption.Text) + Conversion.Val(lblPreviousReadingText.Text));
							}
							else
							{
								if (Conversion.Val(txtReading.Text) - Conversion.Val(lblPreviousReadingText.Text) > 0)
								{
									txtConsumption.Text = FCConvert.ToString(Conversion.Val(txtReading.Text) - Conversion.Val(lblPreviousReadingText.Text));
								}
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				//ErrorHandler: ;
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + "\r\n" + "\r\n" + Information.Err(ex).Description);
			}
		}

		private bool SaveInfo_2(bool boolForceNewBill)
		{
			return SaveInfo(boolForceNewBill);
		}

		private bool SaveInfo(bool boolForceNewBill = false)
		{
			bool SaveInfoRet = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this updates the record in the meter table and updates the information including the status
				// after billing status is set to 'D' then the user must calculate next
				bool NeedBill = false;
				int intCT;
				bool boolAdjustment;
				bool blnCreatePrePay = false;
				double dblCredit = 0;
				clsDRWrapper rsMtrEdit = new clsDRWrapper();
				for (intCT = 1; intCT <= 4; intCT++)
				{
					if (Conversion.Val(vsAdjust.TextMatrix(intCT, lngAdjColAmount)) != 0)
					{
						boolAdjustment = true;
						break;
					}
				}
				// MAL@20080117: Create Pre-Payments with Credits
				// Call Reference: 113597
				if (modGlobalConstants.Statics.gblnAutoPrepayments)
				{
					blnCreatePrePay = modUTBilling.CalculatePrePayments(rsMeter.Get_Fields_Int32("AccountKey"), rsMeter.Get_Fields_Int32("ID"), ref dblCredit);
					if (!blnCreatePrePay && dblCredit < 0)
					{
						// No Credit Created
						modUTBilling.AddAccounttoPrePayTable(rsMeter.Get_Fields_Int32("AccountKey"), dblCredit, false);
					}
					else if (blnCreatePrePay && dblCredit < 0)
					{
						// Credit Created
						modUTBilling.AddAccounttoPrePayTable(rsMeter.Get_Fields_Int32("AccountKey"), dblCredit, true);
					}
				}
				if (!boolDoNotSaveAgain)
				{
					rsMtrEdit.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + rsMeter.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
					rsMtrEdit.Edit();
					if (FCConvert.ToString(rsMeter.Get_Fields_String("BillingStatus")) == "C" || FCConvert.ToString(rsMeter.Get_Fields_String("BillingStatus")) == "" || boolForceNewBill)
					{
						// if the meter status is cleared then no bill has been created yet
						UpdateBillRecord_2(true);
						// create bill record
						// CreateBillRecord
					}
					else
					{
						// If Dirty Or boolAdjustment Or ((boolMeterUseFlat Or boolMeterUseUnit) And Not boolMeterUseCons) Then              'if a bill already exists and something has been changed,
						NeedBill = UpdateBillRecord();
						// update the current bill record
					}
					// XXXX .Edit doesn't do anything so don't need this
					// If .EditMode = 0 Then           'checks the mode of the recordset
					// .Edit
					// End If
					if (NeedBill == true)
					{
						// checks the status of the bill record
						// kgk  .Fields("BillingStatus") = "C"  'if there is no bill (somehow) this will set the status back to "C"
						rsMtrEdit.Set_Fields("BillingStatus", "C");
						boolDoNotSaveAgain = boolForceNewBill;
						SaveInfoRet = SaveInfo_2(true);
						// a bill will be created when this sub is called again (slight recursion)
						boolDoNotSaveAgain = boolForceNewBill;
						return SaveInfoRet;
						// exit to avoid infinite loop
					}
					else
					{
						// kgk .Fields("BillingStatus") = "D"        'all is good, move status to Data Entered
						rsMtrEdit.Set_Fields("BillingStatus", "D");
					}
					// input values
					if (txtReading.Text == "")
					{
						rsMtrEdit.Set_Fields("CurrentReading", -1);
						// kk 110812 trout-884  Change from 0 to -1 for no read
					}
					else
					{
						rsMtrEdit.Set_Fields("CurrentReading", txtReading.Text);
					}
					if (txtDate.Text == "")
					{
						rsMtrEdit.Set_Fields("CurrentReadingDate", DateTime.Today);
					}
					else
					{
						if (Information.IsDate(txtDate.Text))
						{
							rsMtrEdit.Set_Fields("CurrentReadingDate", txtDate.Text);
						}
						else
						{
							rsMtrEdit.Set_Fields("CurrentReadingDate", DateTime.Today);
						}
					}
					if (txtReadingCode.Text == "")
					{
						rsMtrEdit.Set_Fields("CurrentCode", "A");
					}
					else
					{
						rsMtrEdit.Set_Fields("CurrentCode", txtReadingCode.Text);
					}
					// update the recordset
					// kgk .Update
					rsMtrEdit.Update();
					// set book status
					// XXXX .Edit doesn't do anything so don't need this
					// If .EditMode = 0 Then
					// .Edit
					// .Fields("CurrentStatus") = "DE"
					// .Fields("dDate") = Now
					// .Update
					// Else
					rsBook.Set_Fields("CurrentStatus", "DE");
					rsBook.Set_Fields("dDate", DateTime.Now);
					rsBook.Update();
					// End If
					Dirty = false;
					SaveInfoRet = true;
				}
				else
				{
					// exit the function
					SaveInfoRet = true;
				}
				return SaveInfoRet;
			}
			catch (Exception ex)
			{
				
				SaveInfoRet = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfoRet;
		}
		// vbPorter upgrade warning: BType As short	OnWriteFCConvert.ToInt32(
		private bool BilledType_8(int BType, bool boolTypeCheck = false)
		{
			return BilledType(ref BType, boolTypeCheck);
		}

		private bool BilledType(ref int BType, bool boolTypeCheck = false)
		{
			bool BilledType = false;
			// if boolTypecheck is false then this function will return True if the meter uses this line
			// else this function will return True is the number BType is in one of the types of
			// charges in the current meter, False otherwise
			// 1 = Consumption
			// 2 = Flat Rate
			// 3 = Units
			int intCT;
			if (boolTypeCheck)
			{
				for (intCT = 1; intCT <= 5; intCT++)
				{
					// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
					if ((FCConvert.ToInt32(rsMeter.Get_Fields("WaterType" + FCConvert.ToString(intCT))) == BType && FCConvert.ToString(rsMeter.Get_Fields_String("Service")) != "S") || (FCConvert.ToInt32(rsMeter.Get_Fields("SewerType" + FCConvert.ToString(intCT))) == BType && FCConvert.ToString(rsMeter.Get_Fields_String("Service")) != "W"))
					{
						BilledType = true;
						return BilledType;
					}
				}
			}
			else
			{
				// TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
				BilledType = FCConvert.ToBoolean(rsMeter.Get_Fields("UseRate" + FCConvert.ToString(BType)));
			}
			return BilledType;
		}

		private void GetSettings()
		{
			// locks either reading or consumption boxes, tabstop settings
			if (Reading)
			{
				txtReading.TabStop = true;
				txtReading.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				txtConsumption.BackColor = Color.White;
				txtConsumption.TabStop = false;
			}
			else
			{
				txtReading.TabStop = false;
				txtReading.BackColor = Color.White;
				txtConsumption.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORHIGHLIGHT);
				txtConsumption.TabStop = true;
			}
			if (Calculation == true)
			{
				txtConsumption.TabStop = true;
			}
		}

		private void LockEditBoxes_2(bool Bool)
		{
			LockEditBoxes(ref Bool);
		}

		private void LockEditBoxes(ref bool Bool)
		{
			// this locks/unlocks all of the boxes that will be allowed to be editable
			int i;
			txtReadingCode.Locked = Bool;
			txtDate.Locked = Bool;
			if (Reading)
			{
				txtReading.Locked = Bool;
				txtConsumption.Locked = !Bool;
			}
			else
			{
				txtReading.Locked = !Bool;
				txtConsumption.Locked = Bool;
			}
			txtOverrideSC.Locked = Bool;
			txtOverrideWC.Locked = Bool;
			//cmbSeq.Enabled/*Locked*/ = Bool;
			// FC:FINAL:VGE - #i881 Using implemented property.
			cmbSeq.Locked = Bool;
			chkNoBill.Enabled = !Bool;
			boolLockBoxes = Bool;
		}

		private void GetBookInfo(ref int BookNum)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				rsBook = new clsDRWrapper();
				rsBook.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				lngTownUnits = FCConvert.ToInt32(rsBook.Get_Fields_Int32("ReadingUnits"));
				// get all of the information for 1 book
				rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(BookNum), modExtraModules.strUTDatabase);
				if (rsBook.EndOfFile() != true && rsBook.BeginningOfFile() != true)
				{
					rsBook.MoveLast();
					rsBook.MoveFirst();
					// checks the status of the Book, B = Billed
					if (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "B")
					{
						MessageBox.Show("The Book chosen is not ready to have data entered.", "Status Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					}
					else
					{
						if (rsBook.RecordCount() > 0)
						{
							rsMeter = new clsDRWrapper();
							// if there is a book with the correct status (ie C, D, X, or E)
							switch (lngDEOrder)
							{
								case 0:
									{
										// Sequence										
										rsMeter.OpenRecordset("SELECT * FROM MeterTable INNER JOIN (SELECT m.*, m.DeedName1 AS OwnerName, pBill.FullNameLF AS Name FROM Master m INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = m.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID) AS qMaster ON MeterTable.AccountKey = qMaster.ID WHERE BookNumber = " + FCConvert.ToString(BookNum) + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0 ORDER BY Sequence, AccountNumber asc");
										break;
									}
								case 1:
									{
										// Owner										
										rsMeter.OpenRecordset("SELECT * FROM MeterTable INNER JOIN (SELECT m.*,m.DeedName1 AS OwnerName, pBill.FullNameLF AS Name FROM Master m INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = m.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID) AS qMaster ON MeterTable.AccountKey = qMaster.ID WHERE BookNumber = " + FCConvert.ToString(BookNum) + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0 ORDER BY OwnerName, AccountNumber, Sequence asc");
										// kk11112014 trout-871  Add Sequence to order
										break;
									}
								case 2:
									{
										// Tenant										
										rsMeter.OpenRecordset("SELECT * FROM MeterTable INNER JOIN (SELECT m.*, m.DeedName1 AS OwnerName, pBill.FullNameLF AS Name FROM Master m INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = m.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID) AS qMaster ON MeterTable.AccountKey = qMaster.ID WHERE BookNumber = " + FCConvert.ToString(BookNum) + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0 ORDER BY Name, AccountNumber, Sequence asc");
										// kk11112014 trout-871  Add Sequence to order
										break;
									}
								case 3:
									{
										// Account Number										
										rsMeter.OpenRecordset("SELECT * FROM MeterTable INNER JOIN (SELECT m.*, m.DeedName1 AS OwnerName, pBill.FullNameLF AS Name FROM Master m INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = m.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID) AS qMaster ON MeterTable.AccountKey = qMaster.ID WHERE BookNumber = " + FCConvert.ToString(BookNum) + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0 ORDER BY AccountNumber, Sequence asc");
										break;
									}
							}

							// get all the meters from the metertable
							if (!rsMeter.EndOfFile())
							{
								cmbSeq.Clear();
								while (!rsMeter.EndOfFile())
								{
									// If rsMeter.Fields("AccountKey") = 364 Then Stop
									if (lngDEOrder == 2)
									{
										// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
										cmbSeq.AddItem((modMain.PadToString_8(rsMeter.Get_Fields("Sequence"), 6)) + " - " + (modMain.PadToString_8(rsMeter.Get_Fields("AccountNumber"), 6)) + " - " + rsMeter.Get_Fields_String("Name"));
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
										// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
										cmbSeq.AddItem((modMain.PadToString_8(rsMeter.Get_Fields("Sequence"), 6)) + " - " + (modMain.PadToString_8(rsMeter.Get_Fields("AccountNumber"), 6)) + " - " + rsMeter.Get_Fields("OwnerName"));
									}
									cmbSeq.ItemData(cmbSeq.NewIndex, FCConvert.ToInt32(rsMeter.Get_Fields_Int32("ID")));
									// Debug.Print GetAccountNumber(rsMeter.Fields("AccountKey"))
									rsMeter.MoveNext();
								}
								rsMeter.MoveFirst();
								if (cmbSeq.Items.Count > 0)
								{
									cmbSeq.SelectedIndex = 0;
								}
								//FC:FINAL:DDU:#1208 - fixed 0 date from DB
								if (!rsMeter.IsFieldNull("CurrentReadingDate") && rsMeter.Get_Fields("CurrentReadingDate").ToOADate() != 0)
								{
									txtDate.Text = Strings.Format(rsMeter.Get_Fields_DateTime("CurrentReadingDate"), "MM/dd/yyyy");
								}
								else
								{
									txtDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
								}
								LockEditBoxes_2(false);
							}
							else
							{
								clsDRWrapper rsFix = new clsDRWrapper();
								cmbSeq.Clear();
								GetAcctInfo_2(0);
								// zero will clear the boxes
								GetMeterInfo_2(0);
								txtDate.Text = "";
								txtReading.Text = "";
								txtConsumption.Text = "";
								txtReadingCode.Text = "";
								MessageBox.Show("No meters in this book.", "No Meters", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								rsFix.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(lngBookNumber), modExtraModules.strUTDatabase);
								if (rsFix.BeginningOfFile() || rsFix.EndOfFile())
								{
									// do nothing
								}
								else
								{
									rsFix.MoveLast();
									rsFix.MoveFirst();
									rsFix.Edit();
									rsFix.Set_Fields("CurrentStatus", "CE");
									rsFix.Update();
								}
								cmdNext.Enabled = false;
								cmdPrevious.Enabled = false;
								cmdSave.Enabled = false;
								return;
							}
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Getting Book Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GetMeterInfo_2(int lngMK)
		{
			GetMeterInfo(ref lngMK);
		}

		private void GetMeterInfo(ref int lngMK)
        {
            var rsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intTemp;
				// this sub will get all the information about one meter and display it on the screen
				if (lngMK == 0)
				{
					for (i = 1; i <= 5; i++)
					{
						vsRate.TextMatrix(i, 0, "");
						// clear the title, type and all the other grid elements
						vsRate.TextMatrix(i, 1, "");
						// clear the textboxes (water)
						vsRate.TextMatrix(i, 2, "");
						// clear the textboxes (sewer)
						vsRate.TextMatrix(i, 3, "");
						vsRate.TextMatrix(i, 4, "");
						vsRate.TextMatrix(i, 5, "");
						if (i < 5)
						{
							vsAdjust.TextMatrix(i, 0, "");
							// clear the title, type and all the other grid elements
							vsAdjust.TextMatrix(i, 1, "");
							// clear the textboxes (water)
							vsAdjust.TextMatrix(i, 2, "");
							// clear the textboxes (sewer)
							vsAdjust.TextMatrix(i, 3, "");
						}
					}
					txtSerial.Text = "";
					txtRemote.Text = "";
				}
				else
				{
					if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("ID")) != lngMK)
					{
						rsMeter.FindFirstRecord("ID", lngMK);
						if (rsMeter.NoMatch)
						{
							// if the correct meter is found
							MessageBox.Show("Cannot find meter key.", "Record Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return;
						}
					}

                    rsLoad.OpenRecordset("select * from metertable where id = " + lngMK, "UtilityBilling");
					lngMeterKey = FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ID"));
					// set the meterkey and account number
					// TODO Get_Fields: Check the table for the column [Accountnumber] and replace with corresponding Get_Field method
					lngAcctNum = FCConvert.ToInt32(rsMeter.Get_Fields("Accountnumber"));
					txtPrevReading.Text = rsLoad.Get_Fields_Int32("CurrentReading") + " " + rsLoad.Get_Fields_String("BillingStatus");
					lblPreviousReadingText.Text = FCConvert.ToString(rsLoad.Get_Fields_Int32("PreviousReading"));
					// get all current fields and display them on the screen
					boolMeterUseCons = false;
					boolMeterUseFlat = false;
					boolMeterUseUnit = false;
					for (intTemp = 1; intTemp <= 5; intTemp++)
					{
						if (BilledType(ref intTemp))
						{
							if (FCConvert.ToString(rsLoad.Get_Fields_String("Service")) != "S")
							{
								if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intTemp))) == false)
								{
									vsRate.TextMatrix(intTemp, lngRateColWaterRK, FCConvert.ToString(rsLoad.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intTemp))));
									// get the type of the charges for this payment
									// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
									vsRate.TextMatrix(intTemp, lngRateColWaterTitle, modMain.TypeTitleText_2(rsLoad.Get_Fields("WaterType" + FCConvert.ToString(intTemp))));
									// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
									vsRate.TextMatrix(intTemp, lngRateColWaterType, FCConvert.ToString(rsLoad.Get_Fields("WaterType" + FCConvert.ToString(intTemp))));
									// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
									if (rsLoad.Get_Fields("WaterType" + FCConvert.ToString(intTemp)) == 1)
									{
										boolMeterUseCons = true;
									}
									// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
									else if (rsLoad.Get_Fields("WaterType" + FCConvert.ToString(intTemp)) == 2)
									{
										boolMeterUseFlat = true;
									}
										// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
										else if (rsLoad.Get_Fields("WaterType" + FCConvert.ToString(intTemp)) == 3)
									{
										boolMeterUseUnit = true;
									}
								}
								else
								{
									vsRate.TextMatrix(intTemp, lngRateColWaterRK, "");
									vsRate.TextMatrix(intTemp, lngRateColWaterTitle, "");
									vsRate.TextMatrix(intTemp, lngRateColWaterType, "");
								}
							}
							else
							{
								vsRate.TextMatrix(intTemp, lngRateColWaterRK, "");
								vsRate.TextMatrix(intTemp, lngRateColWaterTitle, "");
								vsRate.TextMatrix(intTemp, lngRateColWaterType, "");
							}
							if (FCConvert.ToString(rsLoad.Get_Fields_String("Service")) != "W")
							{
								if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intTemp))) == false)
								{
									vsRate.TextMatrix(intTemp, lngRateColSewerRK, FCConvert.ToString(rsLoad.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intTemp))));
									// get the type of the charges for this payment
									// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
									vsRate.TextMatrix(intTemp, lngRateColSewerTitle, modMain.TypeTitleText_2(rsLoad.Get_Fields("SewerType" + FCConvert.ToString(intTemp))));
									// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
									vsRate.TextMatrix(intTemp, lngRateColSewerType, FCConvert.ToString(rsLoad.Get_Fields("SewerType" + FCConvert.ToString(intTemp))));
									// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
									if (rsLoad.Get_Fields("SewerType" + FCConvert.ToString(intTemp)) == 1)
									{
										boolMeterUseCons = true;
									}
									// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
									else if (rsLoad.Get_Fields("SewerType" + FCConvert.ToString(intTemp)) == 2)
									{
										boolMeterUseFlat = true;
									}
										// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
										else if (rsMeter.Get_Fields("SewerType" + FCConvert.ToString(intTemp)) == 3)
									{
										boolMeterUseUnit = true;
									}
								}
								else
								{
									vsRate.TextMatrix(intTemp, lngRateColSewerRK, "");
									vsRate.TextMatrix(intTemp, lngRateColSewerTitle, "");
									vsRate.TextMatrix(intTemp, lngRateColSewerType, "");
								}
							}
							else
							{
								vsRate.TextMatrix(intTemp, lngRateColSewerRK, "");
								vsRate.TextMatrix(intTemp, lngRateColSewerTitle, "");
								vsRate.TextMatrix(intTemp, lngRateColSewerType, "");
							}
						}
						else
						{
							vsRate.TextMatrix(intTemp, lngRateColWaterRK, "");
							vsRate.TextMatrix(intTemp, lngRateColWaterTitle, "");
							vsRate.TextMatrix(intTemp, lngRateColWaterType, "");
							vsRate.TextMatrix(intTemp, lngRateColSewerRK, "");
							vsRate.TextMatrix(intTemp, lngRateColSewerTitle, "");
							vsRate.TextMatrix(intTemp, lngRateColSewerType, "");
						}
					}
					lblDEUnitsWarning.Visible = FCConvert.CBool(lngTownUnits != FCConvert.ToInt32(rsLoad.Get_Fields_Int32("Multiplier")));
					// MAL@20080129: Disable Fields if Meter is Combined
					// Tracker Reference: 12111
					lblCombinedMeter.Visible = FCConvert.CBool(rsLoad.Get_Fields_String("Combine") == "C");
					txtOverrideWC.Enabled = !FCConvert.CBool(rsLoad.Get_Fields_String("Combine") == "C");
					txtOverrideSC.Enabled = !FCConvert.CBool(rsLoad.Get_Fields_String("Combine") == "C");
					txtOverrideWA.Enabled = !FCConvert.CBool(rsLoad.Get_Fields_String("Combine") == "C");
					txtOverrideSA.Enabled = !FCConvert.CBool(rsLoad.Get_Fields_String("Combine") == "C");
					if (FCConvert.ToString(rsLoad.Get_Fields_String("Combine")) == "C")
					{
						txtOverrideWC.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						txtOverrideSC.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						txtOverrideWA.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
						txtOverrideSA.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
					}
					else
					{
						txtOverrideWC.BackColor = Color.White;
						txtOverrideSC.BackColor = Color.White;
						txtOverrideWA.BackColor = Color.White;
						txtOverrideSA.BackColor = Color.White;
					}
					if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_String("SerialNumber")) == false)
					{
						txtSerial.Text = FCConvert.ToString(rsLoad.Get_Fields_String("SerialNumber"));
					}
					else
					{
						txtSerial.Text = "";
					}
					if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_String("Remote")) == false)
					{
						txtRemote.Text = FCConvert.ToString(rsLoad.Get_Fields_String("Remote"));
					}
					else
					{
						txtRemote.Text = "";
					}
					if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("NoBill")))
					{
						// "MeterTable.NoBill"
						chkNoBill.CheckState = Wisej.Web.CheckState.Checked;
					}
					else
					{
						chkNoBill.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					// kk 08282013 trout-984  Indicate change out consumption
					lblChangeOut.Visible = false;
					if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("UseMeterChangeOut")))
					{
						if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("ConsumptionOfChangedOutMeter")) != 0)
						{
							lblChangeOut.Text = "The original meter was changed out.  The consumption amount of " + rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter") + " will be added.";
							lblChangeOut.Visible = true;
						}
					}
					//FC:FINAL:DDU:#1208 - fixed 0 date from DB
					if (!rsLoad.IsFieldNull("CurrentReadingDate") && rsLoad.Get_Fields("CurrentReadingDate").ToOADate() != 0)
					{
						txtDate.Text = Strings.Format(rsLoad.Get_Fields_DateTime("CurrentReadingDate"), "MM/dd/yyyy");						
					}
                    else
                    {
                        txtDate.Text = DateTime.Today.FormatAndPadShortDate();
                    }
					if (FCConvert.ToString(rsLoad.Get_Fields_String("BillingStatus")) == "C")
					{
						// if this account bill has not been updated at all since being cleared then
						txtReadingCode.Text = "A";
						// .Fields("CurrentCode")
						txtConsumption.Text = "";
						if (FCConvert.ToInt32(rsLoad.Get_Fields_Int32("CurrentReading")) != -1)
						{
							// kk 110812 trout-884 Change from <> 0 to <> -1 for no read
							txtReading.Text = FCConvert.ToString(rsLoad.Get_Fields_Int32("CurrentReading"));
						}
						else
						{
							txtReading.Text = "";
						}
					}
					else
					{
						// otherwise this bill already has new information in it but needs to be edited
						if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_String("CurrentCode")) == false)
						{
							txtReadingCode.Text = FCConvert.ToString(rsLoad.Get_Fields_String("CurrentCode"));
						}
						else
						{
							txtReadingCode.Text = "A";
						}
						if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_Int32("PreviousReading")) == false)
						{
							txtPrevReading.Text = FCConvert.ToString(rsLoad.Get_Fields_Int32("PreviousReading"));
						}
						if (fecherFoundation.FCUtils.IsNull(rsLoad.Get_Fields_Int32("CurrentReading")) == false && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("CurrentReading")) != -1)
						{
							// kk 110812 trout-884  Add check for -1 (no read)
							txtReading.Text = FCConvert.ToString(rsLoad.Get_Fields_Int32("CurrentReading"));
							// xxxxxx Check out Reading flag and what happens with a meter rollover?
							if (!Reading && FCConvert.ToInt32(rsLoad.Get_Fields_Int32("CurrentReading")) != 0 && rsLoad.Get_Fields_Int32("CurrentReading") - rsLoad.Get_Fields_Int32("PreviousReading") > 0)
							{
								txtConsumption.Text = FCConvert.ToString(rsLoad.Get_Fields_Int32("CurrentReading") - rsLoad.Get_Fields_Int32("PreviousReading"));
							}
							else
							{
								txtConsumption.Text = "";
							}
						}
						else
						{
							txtReading.Text = "";
							txtConsumption.Text = "";
						}
					}
				}
				if (lngMK != 0)
				{
					clsDRWrapper rsBillCheck = new clsDRWrapper();
					// check for a current bill
					rsBillCheck.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsMeter.Get_Fields_Int32("ID") + " AND BillStatus <> 'B' AND BillStatus <> 'C'");
					// if there is already a bill record created, then display the information
					// from the first data entry or overrides from the meter/master screen
					if (rsBillCheck.RecordCount() > 0)
					{
						// This will fill in the adjustment grid
						FillAdjustmentGrid_6(lngMeterKey, rsBillCheck.Get_Fields_Int32("ID"));
						// This will clear all the breakdown records including the DE adjustments
						modUTBilling.ClearBreakDownRecords_26(true, rsBillCheck.Get_Fields_Int32("ID"), true);
						rsBillCheck.Edit();
						rsBillCheck.Set_Fields("WDEAdjustAmount", 0);
						rsBillCheck.Set_Fields("SDEAdjustAmount", 0);
						rsBillCheck.Update();
						// This will override the meter defaults because it has an actual bill record created for it now
						if (FCConvert.ToBoolean(rsBillCheck.Get_Fields_Boolean("NoBill")))
						{
							chkNoBill.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkNoBill.CheckState = Wisej.Web.CheckState.Unchecked;
						}
						if (rsBillCheck.Get_Fields_Int32("Consumption") > 0)
						{
							txtConsumption.Text = FCConvert.ToString(rsBillCheck.Get_Fields_Int32("Consumption"));
						}
						else
						{
							txtConsumption.Text = "";
						}
						if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_Int32("WaterConsumptionOverride")) == false)
						{
							if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("WaterConsumptionOverride")) == 0)
							{
								if (fecherFoundation.FCUtils.IsNull(rsBillCheck.Get_Fields_Int32("WaterOverrideCons")) == false)
								{
									if (FCConvert.ToInt32(rsBillCheck.Get_Fields_Int32("WaterOverrideCons")) != 0)
									{
										txtOverrideWC.Text = FCConvert.ToString(rsBillCheck.Get_Fields_Int32("WaterOverrideCons"));
									}
									else
									{
										txtOverrideWC.Text = "";
									}
								}
							}
						}
						if (fecherFoundation.FCUtils.IsNull(rsBillCheck.Get_Fields_Double("WaterOverrideAmount")) == false)
						{
							txtOverrideWA.Text = FCConvert.ToString(rsBillCheck.Get_Fields_Double("WaterOverrideAmount"));
						}
						if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_Int32("SewerConsumptionOverride")) == false)
						{
							if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("SewerConsumptionOverride")) == 0)
							{
								if (fecherFoundation.FCUtils.IsNull(rsBillCheck.Get_Fields_Int32("SewerOverrideCons")) == false)
								{
									if (FCConvert.ToInt32(rsBillCheck.Get_Fields_Int32("SewerOverrideCons")) != 0)
									{
										txtOverrideSC.Text = FCConvert.ToString(rsBillCheck.Get_Fields_Int32("SewerOverrideCons"));
									}
									else
									{
										txtOverrideSC.Text = "";
									}
								}
							}
						}
						if (fecherFoundation.FCUtils.IsNull(rsBillCheck.Get_Fields_Double("SewerOverrideAmount")) == false)
						{
							txtOverrideSA.Text = FCConvert.ToString(rsBillCheck.Get_Fields_Double("SewerOverrideAmount"));
						}
					}
					else
					{
						ClearAdjustmentTable();
						txtOverrideWC.Text = "";
						txtOverrideSC.Text = "";
						txtOverrideWA.Text = "";
						txtOverrideSA.Text = "";
					}
					BillingCodeFlag = false;
					// reset the flag so that the user will see the error in needed
					History_Check();
					// checks the history checkbox then fills the grid if necessary
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Meter - " + FCConvert.ToString(lngMK), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void GetAcctInfo_2(int lngKey)
		{
			GetAcctInfo(ref lngKey);
		}

		private void GetAcctInfo(ref int lngKey)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (lngKey == 0)
				{
					// clears the textboxes
					txtBillTo.Text = "";
					txtOwner.Text = "";
					txtLocation.Text = "";
					txtAccount.Text = "";
				}
				else
				{
					rsAcct = new clsDRWrapper();
					// Find the correct account
					rsAcct.OpenRecordset(
                        "SELECT m.AccountNumber,m.StreetName,m.StreetNumber,m.NoBill,m.DeedName1 AS OwnerName,pBill.FullNameLF AS Name,m.SameBillOwner,m.WBillToOwner,m.SBillToOwner " +
                        "FROM Master m INNER JOIN " + modMain.Statics.strDbCP +
                        " PartyNameView pOwn on pOwn.ID = m.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP +
                        "PartyNameView pBill ON pBill.ID = m.BillingPartyID " + "Where m.ID = " +
                        FCConvert.ToString(lngKey));
					if (rsAcct.RecordCount() != 0)
					{
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						txtAccount.Text = modMain.PadToString_8(rsAcct.Get_Fields("AccountNumber"), 6);
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						lngAcctNum = FCConvert.ToInt32(rsAcct.Get_Fields("AccountNumber"));
						// AccountNumber
						if (fecherFoundation.FCUtils.IsNull(rsAcct.Get_Fields_String("Name")) == false)
						{
							txtBillTo.Text = FCConvert.ToString(rsAcct.Get_Fields_String("Name"));
						}
						else
						{
							txtBillTo.Text = "";
						}
						// Owner name
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						if (fecherFoundation.FCUtils.IsNull(rsAcct.Get_Fields("OwnerName")) == false)
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							txtOwner.Text = FCConvert.ToString(rsAcct.Get_Fields("OwnerName"));
						}
						else
						{
							txtOwner.Text = "";
						}
						// Location (street number, street name)
						if (fecherFoundation.FCUtils.IsNull(rsAcct.Get_Fields_String("StreetName")) == false)
						{
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							txtLocation.Text = rsAcct.Get_Fields("StreetNumber") + " " + rsAcct.Get_Fields_String("StreetName");
						}
						else
						{
							txtLocation.Text = "";
						}
						if (rsAcct.Get_Fields_Boolean("NoBill") || chkNoBill.CheckState == Wisej.Web.CheckState.Checked)
						{
							// check to see if the account itself is nobilled
							chkNoBill.CheckState = Wisej.Web.CheckState.Checked;
						}
						else
						{
							chkNoBill.CheckState = Wisej.Web.CheckState.Unchecked;
						}

                        acctSameBillOwner = rsAcct.Get_Fields_Boolean("SameBillOwner");
                        acctWBillToOwner = rsAcct.Get_Fields_Boolean("WBillToOwner");
                        acctSBillToOwner = rsAcct.Get_Fields_Boolean("SBillToOwner");

                        CheckForDEMessage();
						// If Trim(.Fields("DataEntry")) <> "" Then
						// MsgBox Trim(.Fields("DataEntry")), vbExclamation, "Date Entry Message"
						// End If
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Get Account Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void NextMeter_2(bool boolSave)
		{
			NextMeter(ref boolSave);
		}

		private void NextMeter(ref bool boolSave)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this sub will change all of the meter information to the next meter while saving the
				// last meters information, this is the only place in TRIO Software where you do
				// not need an F10 KeyPress to save information
				if (boolSave)
				{
					// kk08262016 trouts-203  Modify behavior for Bangor's req that accounts not billed in Elec Data Entry don't get billed
					if (boolBangor)
					{
						if (Bangor_ShouldBillBeSaved())
						{
							if (SaveInfo())
							{
							}
							else
							{
								MessageBox.Show("Save not successful.", "Save Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								// do not move to the next meter
							}
						}
					}
					else
					{
						if (SaveInfo())
						{
						}
						else
						{
							MessageBox.Show("Save not successful.", "Save Unsuccessful", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							// do not move to the next meter
						}
					}
				}
				if (cmbSeq.SelectedIndex < cmbSeq.Items.Count - 1)
				{
					boolDoNotSaveAgain = true;
					cmbSeq.SelectedIndex = (cmbSeq.SelectedIndex + 1);
					boolDoNotSaveAgain = false;
					if (BilledType_8(1, true))
					{
						// check to see if meter is billed by consumption
					}
					else
					{
						// if the meter is not billed by consumption then
						if (BilledType_8(2, true))
						{
							// then check to see if the meter is billed by a Flat Rate
							if (chkFlat.CheckState == Wisej.Web.CheckState.Checked)
							{
								// if chkFlat is checked (ie stop on flat rate bills)
								goto KEEPMETER;
								// if so then let the meter stay, otherwise move to the next meter
							}
							else
							{
								// kk08262016 trouts-203  Modify behavior for Bangor's req that accounts not billed in Elec Data Entry don't get billed
								if (boolBangor)
								{
									if (Bangor_ShouldBillBeSaved())
									{
										UpdateBillRecord_2(true);
									}
								}
								else
								{
									UpdateBillRecord_2(true);
								}
							}
						}
						if (BilledType_8(3, true))
						{
							if (chkUnits.CheckState == Wisej.Web.CheckState.Checked)
							{
								goto KEEPMETER;
							}
							else
							{
								// kk08262016 trouts-203  Modify behavior for Bangor's req that accounts not billed in Elec Data Entry don't get billed
								if (boolBangor)
								{
									if (Bangor_ShouldBillBeSaved())
									{
										UpdateBillRecord_2(true);
									}
								}
								else
								{
									UpdateBillRecord_2(true);
								}
							}
						}
						NextMeter_2(false);
						if (Reading)
						{
							if (txtReading.Enabled && txtReading.Visible)
							{
								txtReading.Focus();
							}
							// txtConsumption.Text = ""
						}
						else
						{
							txtConsumption.Text = FCConvert.ToString(0);
							if (txtConsumption.Enabled && txtConsumption.Visible)
							{
								txtConsumption.Focus();
							}
						}
						return;
					}
					KEEPMETER:
					;
					rsMeter.Edit();
					if (Reading)
					{
						if (txtReading.Visible && txtReading.Enabled)
						{
							txtReading.Focus();
						}
						// DJW@20090130 blanks out consumption for rolled over meters that have already been set
						// If Val(txtReading.Text) - Val(lblPreviousReadingText.Caption) > 0 Then
						// txtConsumption.Text = Val(txtReading.Text) - Val(lblPreviousReadingText.Caption)
						// Else
						// txtConsumption.Text = ""
						// End If
					}
					else
					{
						if (txtConsumption.Visible && txtConsumption.Enabled)
						{
							txtConsumption.Focus();
						}
						// txtConsumption.Text = ""
					}
					CheckForDEMessage();
					History_Check();
				}
				else
				{
					// XXXX  .Edit does nothing so no need to cancel the update.
					// If rsAcct.EditMode <> 0 Then
					// rsAcct.Update           'forces the cancel update anywhere it is needed
					// End If
					// If rsBook.EditMode <> 0 Then
					// rsBook.Update
					// End If
					// If rsMeter.EditMode <> 0 Then
					// rsMeter.Update
					// End If
					this.Unload();
				}
				BillingCodeFlag = false;
				// reset the flag so that the user will see the error in needed
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Move Next", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PreviousMeter()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (txtReading.Text != "")
				{
					// kk08262016 trouts-203  Modify behavior for Bangor's req that accounts not billed in Elec Data Entry don't get billed
					if (boolBangor)
					{
						if (Bangor_ShouldBillBeSaved())
						{
							SaveInfo();
						}
					}
					else
					{
						SaveInfo();
					}
				}
				Dirty = false;
				if (cmbSeq.SelectedIndex > 0)
				{
					if (boolBangor)
					{
						if (Bangor_ShouldBillBeSaved())
						{
							SaveInfo();
						}
					}
					else
					{
						SaveInfo();
					}
					cmbSeq.Text = cmbSeq.Items[cmbSeq.SelectedIndex - 1].ToString();
					if (BilledType_8(1, true))
					{
						// check to see if meter is billed by consumption
					}
					else
					{
						// if the meter is not billed by consumption then
						if (chkFlat.CheckState == Wisej.Web.CheckState.Checked)
						{
							// if chkFlat is checked (ie stop on flat rate bills)
							if (BilledType_8(2, true))
							{
								// then check to see if the meter is billed by a Flat Rate
								goto KEEPMETER;
								// if so then let the meter stay, otherwise move to the next meter
							}
						}
						if (chkUnits.CheckState == Wisej.Web.CheckState.Checked)
						{
							if (BilledType_8(3, true))
							{
								goto KEEPMETER;
							}
						}
						PreviousMeter();
						if (Reading)
						{
							txtReading.Focus();
							// txtConsumption.Text = ""
						}
						else
						{
							txtConsumption.Text = FCConvert.ToString(0);
							txtConsumption.Focus();
						}
						return;
					}
					KEEPMETER:
					;
				}
				rsMeter.Edit();
				if (Reading)
				{
					txtReading.Focus();
					// txtConsumption.Text = ""
				}
				else
				{
					txtConsumption.Focus();
					// txtConsumption.Text = ""
				}
				History_Check();
				BillingCodeFlag = false;
				// reset the flag so that the user will see the error in needed
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Previous Meter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHistoryGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// open all the previous bills for a certain account and fills the rsHistory grid
				DateTime CurDate;
				// vbPorter upgrade warning: CurYear As short --> As int	OnWrite(string)
				int CurYear = 0;
				int i = 0;
				rsHistory = new clsDRWrapper();
				// get all the bills accociated with this meter and sort them by data, newest first
				rsHistory.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAcctNum)) + " AND MeterKey = " + FCConvert.ToString(lngMeterKey) + " ORDER BY CurDate Desc");
				if (rsHistory.RecordCount() > 0)
				{
					CurDate = (DateTime)rsHistory.Get_Fields_DateTime("CurDate");
					CurYear = FCConvert.ToInt32(Strings.Format(CurDate, "yyyy"));
					i = 1;
					Level_6(i, 0);
					vsHistory.TextMatrix(i, 1, FCConvert.ToString(CurYear));
					i += 1;
					while (!(rsHistory.EndOfFile() == true))
					{
						CurDate = (DateTime)rsHistory.Get_Fields_DateTime("CurDate");
						if (CurYear > FCConvert.ToDouble(Strings.Format(CurDate, "yyyy")))
						{
							// if there is a new year then
							CurYear = FCConvert.ToInt32(Strings.Format(CurDate, "yyyy"));
							Level_6(i, 0);
							vsHistory.TextMatrix(i, 1, FCConvert.ToString(CurYear));
							i += 1;
						}
						else
						{
							// same year bills
							Level_6(i, 1);
							vsHistory.TextMatrix(i, 2, Strings.Format(CurDate, "MM/dd/yyyy"));
							// kk trout-884 111312 Add check for -1 (No Reading)
							if (fecherFoundation.FCUtils.IsNull(rsHistory.Get_Fields_Int32("CurReading")) == false && FCConvert.ToInt32(rsHistory.Get_Fields_Int32("CurReading")) != -1)
							{
								vsHistory.TextMatrix(i, 3, FCConvert.ToString(rsHistory.Get_Fields_Int32("CurReading")));
							}
							else
							{
								vsHistory.TextMatrix(i, 3, "");
							}
							if (fecherFoundation.FCUtils.IsNull(rsHistory.Get_Fields_Int32("Consumption")) == false)
							{
								vsHistory.TextMatrix(i, 4, FCConvert.ToString(rsHistory.Get_Fields_Int32("Consumption")));
							}
							else
							{
								vsHistory.TextMatrix(i, 4, "");
							}
							i += 1;
							rsHistory.MoveNext();
						}
					}

                    if (boolExpandHist)
                    {
                        CollapseRows_2(false);
                    }
                    else
                    {
                        CollapseRows_2(true);
                    }
                }
				else
				{
					vsHistory.Rows = 1;
					// by setting rows to 1 then 7, this will erase all the data in the cells
					vsHistory.Rows = 7;
					FormatHistoryGrid();
					vsHistory_Collapsed();
				}
                modColorScheme.ColorGrid(vsHistory);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling History", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: Row As object	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lvl As object	OnWriteFCConvert.ToInt16(
		private void Level_6(int Row, int lvl)
		{
			Level(ref Row, ref lvl);
		}

		private void Level(ref int Row, ref int lvl)
		{
			// set the row as a group
			MaxRows += 1;
			vsHistory.Rows = MaxRows + 1;
			if (FCConvert.ToInt32(lvl) == 0)
			{
				//vsHistory.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, 4, 0x80000018);
			}
			else if (FCConvert.ToInt32(lvl) == 1)
			{
				vsHistory.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, 3, Row, 4, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//vsHistory.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, 4, 0x80000016);
			}
			else
			{
				//vsHistory.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, 4, Color.White);
			}
			// set the indentation level of the group
			vsHistory.RowOutlineLevel(Row, FCConvert.ToInt16(lvl));
			if (FCConvert.ToInt32(lvl) == 0)
				vsHistory.IsSubtotal(Row, true);
		}

		private void CollapseRows_2(bool Bool)
		{
			CollapseRows(ref Bool);
		}

		private void CollapseRows(ref bool Bool)
		{
			// if Bool = True then collapse all of the rows starting with the subordinate rows
			// if Bool = False then open all rows starting with the subordinate rows
			int Row;
			if (Bool == true)
			{
				for (Row = 1; Row <= vsHistory.Rows - 1; Row++)
				{
					if (vsHistory.RowOutlineLevel(Row) == 1)
					{
						if (vsHistory.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							vsHistory.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
						}
					}
				}
				for (Row = 1; Row <= vsHistory.Rows - 1; Row++)
				{
					if (vsHistory.RowOutlineLevel(Row) == 0)
					{
						if (vsHistory.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							vsHistory.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
						}
					}
				}
			}
			else if (Bool == false)
			{
				for (Row = 1; Row <= vsHistory.Rows - 1; Row++)
				{
					if (vsHistory.RowOutlineLevel(Row) == 0)
					{
						if (vsHistory.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineExpanded)
						{
							vsHistory.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
						}
					}
				}
				for (Row = 1; Row <= vsHistory.Rows - 1; Row++)
				{
					if (vsHistory.RowOutlineLevel(Row) == 1)
					{
						if (vsHistory.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineExpanded)
						{
							vsHistory.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
						}
					}
				}
			}
		}

		private void txtReadingCode_Change(object sender, System.EventArgs e)
		{
			Dirty = true;
		}

		private void txtReadingCode_DblClick(object sender, System.EventArgs e)
		{
			if (txtReadingCode.Text == "A")
			{
				txtReadingCode.Text = "E";
			}
			else
			{
				txtReadingCode.Text = "A";
			}
		}

		private void txtReadingCode_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			switch ((Keys)KeyAscii)
			{
				case Keys.A:
				case Keys.A + 32:
					{
						txtReadingCode.Text = "A";
						KeyAscii = 0;
						break;
					}
				case Keys.E:
				case Keys.E + 32:
					{
						txtReadingCode.Text = "E";
						KeyAscii = 0;
						break;
					}
				default:
					{
						KeyAscii = 0;
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void txtReadingCode_Validate_2(bool Cancel)
		{
			txtReadingCode_Validate(new object(), new CancelEventArgs(Cancel));
		}

		private void txtReadingCode_Validate(object sender, CancelEventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsR = new clsDRWrapper();
				if (txtReadingCode.Text == "E")
				{
					// find the estimated amount
					int lngAccountNumber = FCConvert.ToInt32(txtAccount.Text);
					rsR.OpenRecordset("SELECT AVG(Consumption) AS AVGCons FROM Bill WHERE AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAccountNumber)), modExtraModules.strUTDatabase);
					if (!rsR.EndOfFile())
					{
						// TODO Get_Fields: Field [AVGCons] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsR.Get_Fields("AVGCons") + "") == 0)
						{
							if (Conversion.Val(lblPreviousReadingText.Text) != Conversion.Val(txtReading.Text))
							{
								// do nothing
							}
							else
							{
								txtReading.Text = lblPreviousReading.Text;
								txtConsumption.Text = FCConvert.ToString(0);
							}
						}
						else
						{
							// TODO Get_Fields: Field [AVGCons] not found!! (maybe it is an alias?)
							//FC:FINAL:MSH - issue #967: replace Get_Fields to Get_Fields_Double to prevent error in call Round, which can take as input param double or decimal
							//if (FCUtils.Round(rsR.Get_Fields("AVGCons"), 0) == 0 && Conversion.Val(lblPreviousReadingText.Text) != Conversion.Val(txtReading.Text))
							if (FCUtils.Round(rsR.Get_Fields_Double("AVGCons"), 0) == 0 && Conversion.Val(lblPreviousReadingText.Text) != Conversion.Val(txtReading.Text))
							{
								// do nothing
							}
							else
							{
								// TODO Get_Fields: Field [AVGCons] not found!! (maybe it is an alias?)
								//txtReading.Text = FCConvert.ToString(Conversion.Val(lblPreviousReadingText.Text) + FCUtils.Round(rsR.Get_Fields("AVGCons"), 0));
								txtReading.Text = FCConvert.ToString(Conversion.Val(lblPreviousReadingText.Text) + FCUtils.Round(rsR.Get_Fields_Double("AVGCons"), 0));
								// TODO Get_Fields: Field [AVGCons] not found!! (maybe it is an alias?)
								//txtConsumption.Text = FCConvert.ToString(FCUtils.Round(rsR.Get_Fields("AVGCons"), 0));
								txtConsumption.Text = FCConvert.ToString(FCUtils.Round(rsR.Get_Fields_Double("AVGCons"), 0));
							}
						}
					}
					else
					{
						if (Conversion.Val(lblPreviousReadingText.Text) != Conversion.Val(txtReading.Text))
						{
							// do nothing
						}
						else
						{
							txtReading.Text = lblPreviousReading.Text;
							txtConsumption.Text = FCConvert.ToString(0);
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Estimated Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsAdjust_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			string strText = "";
			if (vsAdjust.Col == lngAdjColService)
			{
				strText = vsAdjust.EditText;
				if (Strings.Trim(strText) != "")
				{
					vsAdjust.TextMatrix(vsAdjust.Row, vsAdjust.Col, Strings.Left(strText, 1));
				}
			}
		}

		private void vsAdjust_CellChanged(object sender, DataGridViewCellEventArgs e)
		{
			Dirty = true;
		}

		private void vsAdjust_Enter(object sender, System.EventArgs e)
		{
			if (vsAdjust.Col == lngAdjColCode)
			{
				// drop down
				vsAdjust.ComboList = strAdjustmentCombo;
				vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else if (vsAdjust.Col == lngAdjColDescription)
			{
				vsAdjust.ComboList = "";
				vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else if (vsAdjust.Col == lngAdjColAmount)
			{
				vsAdjust.ComboList = "";
				if (Conversion.Val(vsAdjust.TextMatrix(vsAdjust.Row, lngAdjColCode)) == 0)
				{
					vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
			}
			else if (vsAdjust.Col == lngAdjColService)
			{
				// service drop down
				vsAdjust.ComboList = GetAdjustServiceList(vsAdjust.TextMatrix(vsAdjust.Row, lngAdjColServiceAllowed));
				if (Conversion.Val(vsAdjust.TextMatrix(vsAdjust.Row, lngAdjColCode)) == 0)
				{
					vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
			}
			else
			{
				vsAdjust.ComboList = "";
				vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsAdjust_RowColChange(object sender, System.EventArgs e)
		{
			if (vsAdjust.Col == lngAdjColCode)
			{
				// drop down
				vsAdjust.ComboList = strAdjustmentCombo;
				vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else if (vsAdjust.Col == lngAdjColDescription)
			{
				vsAdjust.ComboList = "";
				vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			else if (vsAdjust.Col == lngAdjColAmount)
			{
				vsAdjust.ComboList = "";
				if (Conversion.Val(vsAdjust.TextMatrix(vsAdjust.Row, lngAdjColCode)) == 0)
				{
					vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
			}
			else if (vsAdjust.Col == lngAdjColService)
			{
				// service drop down
				vsAdjust.ComboList = GetAdjustServiceList(vsAdjust.TextMatrix(vsAdjust.Row, lngAdjColServiceAllowed));
				if (Conversion.Val(vsAdjust.TextMatrix(vsAdjust.Row, lngAdjColCode)) == 0)
				{
					vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
				}
				else
				{
					vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				}
			}
			else
			{
				vsAdjust.ComboList = "";
				vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
			}
			if (boolLockBoxes)
			{
				vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsAdjust_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = vsAdjust.GetFlexRowIndex(e.RowIndex);
			int col = vsAdjust.GetFlexColIndex(e.ColumnIndex);
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAdj = new clsDRWrapper();
				string strText;
				strText = vsAdjust.EditText;
				if (col == lngAdjColCode)
				{
					if (Conversion.Val(strText) > 0)
					{
						rsAdj.OpenRecordset("SELECT * FROM Adjust WHERE Code = " + strText, modExtraModules.strUTDatabase);
						if (!rsAdj.EndOfFile())
						{
							// fill the row with the information
							vsAdjust.TextMatrix(row, lngAdjColDescription, rsAdj.Get_Fields_String("ShortDescription"));
							vsAdjust.TextMatrix(row, lngAdjColServiceAllowed, rsAdj.Get_Fields_String("Service"));
							vsAdjust.TextMatrix(row, lngAdjColService, rsAdj.Get_Fields_String("Service"));
							// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
							vsAdjust.TextMatrix(row, lngAdjColAmount, rsAdj.Get_Fields_String("Amount"));
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							vsAdjust.TextMatrix(row, lngAdjColAccount, rsAdj.Get_Fields_String("Account"));
						}
						else
						{
							vsAdjust.EditText = "0";
							vsAdjust.TextMatrix(row, lngAdjColCode, "0");
							vsAdjust.TextMatrix(row, lngAdjColDescription, "");
							// "Manual Entry"
							vsAdjust.TextMatrix(row, lngAdjColServiceAllowed, "B");
							vsAdjust.TextMatrix(row, lngAdjColService, "B");
							vsAdjust.TextMatrix(row, lngAdjColAmount, "0.00");
							vsAdjust.TextMatrix(row, lngAdjColAccount, "");
						}
					}
					else
					{
						vsAdjust.EditText = "0";
						vsAdjust.TextMatrix(row, lngAdjColCode, "0");
						vsAdjust.TextMatrix(row, lngAdjColDescription, "");
						// "Manual Entry"
						vsAdjust.TextMatrix(row, lngAdjColServiceAllowed, "B");
						vsAdjust.TextMatrix(row, lngAdjColService, "B");
						vsAdjust.TextMatrix(row, lngAdjColAmount, "0.00");
						vsAdjust.TextMatrix(row, lngAdjColAccount, "");
					}
				}
				else if (col == lngAdjColAmount)
				{
					if (Conversion.Val(strText) != 0)
					{
						vsAdjust.EditText = FCConvert.ToString(Conversion.Val(strText));
					}
					else
					{
						vsAdjust.EditText = "0.00";
					}
				}
				else if (col == lngAdjColService)
				{
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error in vsAdjust Validate Edit - " + FCConvert.ToString(row) + "." + FCConvert.ToString(col), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsHistory_Collapsed()
		{
			int ExposedRows;
			// keeps track of rows that are not collapsed
			ExposedRows = 1;
			for (i = 1; i <= vsHistory.Rows - 1; i++)
			{
				// check each row
				if (vsHistory.RowOutlineLevel(i) == 0)
				{
					// if it is the upper level row
					// if row is collapsed
					if (vsHistory.IsCollapsed(i) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						ExposedRows += 1;
						// count the exposed row (Upper level)
						i += 1;
						// move to the next row
						// check each row for subordinate rows and do
						// not count the subordinate rows because they are collapsed
						while (vsHistory.RowOutlineLevel(i) != 0)
						{
							if (i < vsHistory.Rows - 1)
							{
								i += 1;
							}
							else
							{
								break;
							}
						}
						i -= 1;
					}
					else
					{
						// if the row is not collapsed then
						ExposedRows += 1;
						// count a row
					}
				}
				else
				{
					ExposedRows += 1;
					// if the row is a subordinate row, then count it
				}
			}
			// this will calculate the height of the flex grid depending on how many rows are open with a max height of the 25% of the form height
			//if (((vsHistory.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows) + 75) < (frmDataEntry.InstancePtr.Height * 0.25))
			//{
			//    vsHistory.Height = vsHistory.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows + 75;
			//}
			//else
			//{
			//    vsHistory.Height = FCConvert.ToInt32((frmDataEntry.InstancePtr.Height * 0.25));
			//}
		}

		private void vsHistory_AfterCollapse(object sender, DataGridViewRowEventArgs e)
		{
			vsHistory_Collapsed();
		}

		private void vsHistory_DblClick(object sender, System.EventArgs e)
		{
			// allow collapsing/expanding with double-clicks
			if (vsHistory.Row > 0)
			{
				if (vsHistory.IsCollapsed(vsHistory.Row) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
				{
					vsHistory.IsCollapsed(vsHistory.Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
				}
				else
				{
					vsHistory.IsCollapsed(vsHistory.Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
				}
			}
		}

		private bool UpdateBillRecord_2(bool boolCreateNew, bool boolNoBill = false)
		{
			return UpdateBillRecord(boolCreateNew, boolNoBill);
		}

		private bool UpdateBillRecord(bool boolCreateNew = false, bool boolNoBill = false)
		{
			bool UpdateBillRecord = false;
			// this function will create/edit the bill record for this meter
			// editing the major info like meter key, account key, service
			// and also editing the DE Adjustment amounts and rate keys
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intTemp;
				string Service = "";
				string BillingCode = "";
				clsDRWrapper rsRate = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				double dblWAdjust = 0;
				double dblSAdjust = 0;
				if (boolCreateNew && (FCConvert.ToString(rsMeter.Get_Fields_String("Combine")) != "" || FCConvert.ToString(rsMeter.Get_Fields_String("Combine")) != "N") || FCConvert.ToInt32(rsMeter.Get_Fields_Int32("MeterNumber")) == 1)
				{
					rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND BillStatus <> 'B'", modExtraModules.strUTDatabase);
				}
				else
				{
					rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + FCConvert.ToString(lngMeterKey) + " AND BillStatus <> 'B' ORDER BY CurDate Asc", modExtraModules.strUTDatabase);
				}
				if (rsBill.RecordCount() > 0 || boolCreateNew)
				{
					if (!boolNoBill)
					{
						if (boolCreateNew && rsBill.RecordCount() == 0)
						{
							rsBill.AddNew();
							// kk 06172013 trouts-19  Bill Fields with Nulls messing up prepays
							rsBill.Set_Fields("BillNumber", 0);
							rsBill.Set_Fields("BillingRateKey", 0);
							rsBill.Set_Fields("WLienStatusEligibility", 0);
							rsBill.Set_Fields("WLienProcessStatus", 0);
							rsBill.Set_Fields("WLienRecordNumber", 0);
							rsBill.Set_Fields("WCombinationLienKey", 0);
							rsBill.Set_Fields("SLienStatusEligibility", 0);
							rsBill.Set_Fields("SLienProcessStatus", 0);
							rsBill.Set_Fields("SLienRecordNumber", 0);
							rsBill.Set_Fields("SCombinationLienKey", 0);
							rsBill.Set_Fields("WIntPaidDate", Convert.ToDateTime("01/01/1970"));
							rsBill.Set_Fields("WIntOwed", 0);
							rsBill.Set_Fields("WIntAdded", 0);
							rsBill.Set_Fields("WCostOwed", 0);
							rsBill.Set_Fields("WCostAdded", 0);
							rsBill.Set_Fields("WPrinPaid", 0);
							rsBill.Set_Fields("WTaxPaid", 0);
							rsBill.Set_Fields("WIntPaid", 0);
							rsBill.Set_Fields("WCostPaid", 0);
							rsBill.Set_Fields("SIntPaidDate", Convert.ToDateTime("01/01/1970"));
							rsBill.Set_Fields("SIntOwed", 0);
							rsBill.Set_Fields("SIntAdded", 0);
							rsBill.Set_Fields("SCostOwed", 0);
							rsBill.Set_Fields("SCostAdded", 0);
							rsBill.Set_Fields("SPrinPaid", 0);
							rsBill.Set_Fields("STaxPaid", 0);
							rsBill.Set_Fields("SIntPaid", 0);
							rsBill.Set_Fields("SCostPaid", 0);
							rsBill.Set_Fields("Copies", 0);
							rsBill.Set_Fields("Loadback", 0);
							rsBill.Set_Fields("Final", 0);
							rsBill.Set_Fields("SHasOverride", 0);
							rsBill.Set_Fields("WHasOverride", 0);
							rsBill.Set_Fields("SDemandGroupID", 0);
							rsBill.Set_Fields("WDemandGroupID", 0);
							rsBill.Set_Fields("SendEBill", 0);
							rsBill.Set_Fields("WinterBill", 0);
							rsBill.Set_Fields("SUploaded", 0);
							rsBill.Set_Fields("WUploaded", 0);
							rsBill.Update();
							// get next bill number
							// .Fields("BillNumber") = GetNextBillNumber(rsMeter.Fields("AccountKey"))
						}
						else
						{
							rsBill.Edit();
						}
						// add the final bill dates
						if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("Final")))
						{
							rsBill.Set_Fields("FinalEndDate", rsMeter.Get_Fields_DateTime("FinalEndDate"));
							rsBill.Set_Fields("FinalStartDate", rsMeter.Get_Fields_DateTime("FinalStartDate"));
							rsBill.Set_Fields("FinalBillDate", rsMeter.Get_Fields_DateTime("FinalBillDate"));
						}
						rsBill.Set_Fields("AccountKey", rsMeter.Get_Fields_Int32("AccountKey"));
						// .Fields("Seq") = rsMeter.Fields("Sequence")
						rsBill.Set_Fields("ActualAccountNumber", modUTStatusPayments.GetAccountNumber_2(rsMeter.Get_Fields_Int32("AccountKey")));
						rsBill.Set_Fields("Book", rsMeter.Get_Fields_Int32("BookNumber"));
						if ((FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "S" && rsBill.Get_Fields_Double("WPrinPaid") != 0) || (FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "W" && rsBill.Get_Fields_Double("SPrinPaid") != 0))
						{
							// kk04222014 trout-1081  Don't change service if a prepay exists on other service
							rsBill.Set_Fields("Service", "B");
						}
						else
						{
							if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_String("Service")) == false)
							{
								rsBill.Set_Fields("Service", rsMeter.Get_Fields_String("Service"));
							}
							else
							{
								rsBill.Set_Fields("Service", "B");
							}
						}
						Service = FCConvert.ToString(rsBill.Get_Fields_String("Service"));
						rsBill.Set_Fields("BillStatus", "D");

                        if (acctSameBillOwner)
                        {
                            rsBill.Set_Fields("WBillOwner", true);
                            rsBill.Set_Fields("SBillOwner", true);
                        }
                        else
                        {
                            rsBill.Set_Fields("WBillOwner", acctWBillToOwner);
                            rsBill.Set_Fields("SBillOwner", acctSBillToOwner);
                        }

                        // rsMeter.Fields("Combine") <> "C"
                        if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("MeterNumber")) == 1)
						{
							rsBill.Set_Fields("MeterKey", lngMeterKey);
						}
                        if (rsMeter.Get_Fields_Int32("MeterNumber") == 1)						
						{
							// this will clear all the breakdown records for this meter in the temp table
							modUTBilling.ClearBreakDownRecords_26(true, rsBill.Get_Fields_Int32("ID"), true, lngMeterKey);
							// .fields("CollectionKey = Wait until it is created for this bill record, then fill this field in
							if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_String("BillingCode")) == false)
							{
								// 0 - Flat, 1 - Units, 2 - Consumption, 3 - Adjustment
								if (Strings.Trim(FCConvert.ToString(rsMeter.Get_Fields_String("BillingCode"))) != "")
								{
									BillingCode = FCConvert.ToString(rsMeter.Get_Fields_String("BillingCode"));
								}
								else
								{
									BillingCode = "2";
								}
							}
							else
							{
								BillingCode = "2";
							}
							// set the bill's combination type from the meter
							if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_String("Combine")) == false)
							{
								if (Strings.Trim(FCConvert.ToString(rsMeter.Get_Fields_String("Combine"))) != "")
								{
									rsBill.Set_Fields("CombinationCode", rsMeter.Get_Fields_String("Combine"));
								}
								else
								{
									rsBill.Set_Fields("CombinationCode", "N");
								}
							}
							else
							{
								rsBill.Set_Fields("CombinationCode", "N");
							}
							rsBill.Set_Fields("NoBill", FCConvert.CBool(chkNoBill.CheckState == Wisej.Web.CheckState.Checked));
							if (txtDate.Text == "")
							{
								rsBill.Set_Fields("CurDate", DateTime.Today);
							}
							else
							{
								if (Information.IsDate(txtDate.Text))
								{
									rsBill.Set_Fields("CurDate", txtDate.Text);
								}
								else
								{
									rsBill.Set_Fields("CurDate", DateTime.Today);
								}
							}
							if (Reading)
							{
								if (txtReading.Text == "")
								{
									// kk trout-884 111312  Check for No Reading
									rsBill.Set_Fields("CurReading", -1);
								}
								else
								{
									rsBill.Set_Fields("CurReading", FCConvert.ToString(Conversion.Val(txtReading.Text)));
								}
							}
							else
							{
								rsBill.Set_Fields("CurReading", Conversion.Val(lblPreviousReadingText.Text) + Conversion.Val(txtConsumption.Text));
								// kk11112014 trout-871  There is no reason to do this here    txtReading.Text = Val(lblPreviousReadingText.Caption) + Val(txtConsumption.Text)
							}
							if (!BilledType_8(2, true) && FCConvert.ToInt32(rsBill.Get_Fields_Int32("CurReading")) == -1)
							{
								// kk trout-884 111312  Change from 0 to -1 for No Read
								rsBill.Set_Fields("CurReading", FCConvert.ToString(Conversion.Val(rsMeter.Get_Fields_Int32("PreviousReading"))));
							}
							rsBill.Set_Fields("CurCode", txtReadingCode.Text);
							if (Information.IsDate(rsMeter.Get_Fields("PreviousReadingDate")))
							{
								rsBill.Set_Fields("PrevDate", rsMeter.Get_Fields_DateTime("PreviousReadingDate"));
							}
							rsBill.Set_Fields("PrevReading", FCConvert.ToString(Conversion.Val(rsMeter.Get_Fields_Int32("PreviousReading"))));
							if (Strings.Trim(rsMeter.Get_Fields_String("PreviousCode") + " ") == "E")
							{
								rsBill.Set_Fields("PrevCode", "E");
							}
							else
							{
								rsBill.Set_Fields("PrevCode", "A");
							}
						}
						// kgk 09052012 trout-849
						if (FCConvert.ToString(rsMeter.Get_Fields_String("Combine")) == "N" && rsMeter.Get_Fields_Boolean("NegativeConsumption") == false)
						{
							if (txtConsumption.Text == "")
							{
								if (FCConvert.ToInt32(rsBill.Get_Fields_Int32("CurReading")) != -1)
								{
									// kk trout-884 111312  Add check for No Read
									rsBill.Set_Fields("Consumption", Conversion.Val(rsBill.Get_Fields_Int32("CurReading")) - Conversion.Val(rsBill.Get_Fields_Int32("PrevReading")));
								}
							}
							else
							{
								rsBill.Set_Fields("Consumption", txtConsumption.Text);
							}
						}
						if (!(chkNoBill.CheckState == Wisej.Web.CheckState.Checked))
						{
							if (FCConvert.ToString(rsMeter.Get_Fields_String("Combine")) == "N" && rsMeter.Get_Fields_Boolean("NegativeConsumption") == false)
							{
								rsBill.Set_Fields("WaterOverrideCons", FCConvert.ToString(Conversion.Val(txtOverrideWC.Text)));
								rsBill.Set_Fields("WaterOverrideAmount", FCConvert.ToString(Conversion.Val(txtOverrideWA.Text)));
								rsBill.Set_Fields("SewerOverrideCons", FCConvert.ToString(Conversion.Val(txtOverrideSC.Text)));
								rsBill.Set_Fields("SewerOverrideAmount", FCConvert.ToString(Conversion.Val(txtOverrideSA.Text)));
								// MAL@20071129: Add check for override amounts to flag new field
								// Tracker Reference: 10973
								if (txtOverrideWC.Text != "" || Strings.CompareString(txtOverrideWA.Text, ">", "0"))
								{
									rsBill.Set_Fields("WHasOverride", true);
								}
								else
								{
									rsBill.Set_Fields("WHasOverride", false);
								}
								if (txtOverrideSC.Text != "" || Strings.CompareString(txtOverrideSA.Text, ">", "0"))
								{
									rsBill.Set_Fields("SHasOverride", true);
								}
								else
								{
									rsBill.Set_Fields("SHasOverride", false);
								}
							}
							for (intTemp = 1; intTemp <= 4; intTemp++)
							{
								// check each row of the adjustment and fill the approprtiate information into the bill record
								if (Conversion.Val(vsAdjust.TextMatrix(intTemp, lngAdjColAmount)) != 0)
								{
									string vbPorterVar = vsAdjust.TextMatrix(intTemp, lngAdjColService);
									// kgk 11-11-2011 trout-781  Add Account Number to AddBreakDownRecord calls to stop
									// RTE 3021 No Current Record because bill record has not been created yet
									if (vbPorterVar == "B")
									{
										// Amount
										// .Fields("WDEAdjustment" & intTemp) = vsAdjust.TextMatrix(intTemp, lngAdjColAmount)
										modUTBilling.AddBreakDownRecord_452708(true, rsBill.Get_Fields_Int32("ID"), rsBill.Get_Fields_Int32("MeterKey"), "W", "Adj - " + vsAdjust.TextMatrix(intTemp, lngAdjColDescription), FCConvert.ToInt32(Conversion.Val(vsAdjust.TextMatrix(intTemp, lngAdjColCode))), Strings.Trim(vsAdjust.TextMatrix(intTemp, lngAdjColAccount)), FCConvert.ToDouble(vsAdjust.TextMatrix(intTemp, lngAdjColAmount)), "D", rsBill.Get_Fields_Int32("ActualAccountNumber"));
										dblWAdjust += FCConvert.ToDouble(vsAdjust.TextMatrix(intTemp, lngAdjColAmount));
										// .Fields("DESAdjustment" & intTemp) = vsAdjust.TextMatrix(intTemp, lngAdjColAmount)
										modUTBilling.AddBreakDownRecord_452708(true, rsBill.Get_Fields_Int32("ID"), rsBill.Get_Fields_Int32("MeterKey"), "S", "Adj - " + vsAdjust.TextMatrix(intTemp, lngAdjColDescription), FCConvert.ToInt32(Conversion.Val(vsAdjust.TextMatrix(intTemp, lngAdjColCode))), Strings.Trim(vsAdjust.TextMatrix(intTemp, lngAdjColAccount)), FCConvert.ToDouble(vsAdjust.TextMatrix(intTemp, lngAdjColAmount)), "D", rsBill.Get_Fields_Int32("ActualAccountNumber"));
										dblSAdjust += FCConvert.ToDouble(vsAdjust.TextMatrix(intTemp, lngAdjColAmount));
									}
									else if (vbPorterVar == "W")
									{
										// Amount
										// .Fields("DEWAdjustment" & intTemp) = vsAdjust.TextMatrix(intTemp, lngAdjColAmount)
										modUTBilling.AddBreakDownRecord_452708(true, rsBill.Get_Fields_Int32("ID"), rsBill.Get_Fields_Int32("MeterKey"), "W", "Adj - " + vsAdjust.TextMatrix(intTemp, lngAdjColDescription), FCConvert.ToInt32(Conversion.Val(vsAdjust.TextMatrix(intTemp, lngAdjColCode))), Strings.Trim(vsAdjust.TextMatrix(intTemp, lngAdjColAccount)), FCConvert.ToDouble(vsAdjust.TextMatrix(intTemp, lngAdjColAmount)), "D", rsBill.Get_Fields_Int32("ActualAccountNumber"));
										dblWAdjust += FCConvert.ToDouble(vsAdjust.TextMatrix(intTemp, lngAdjColAmount));
										// .Fields("DESAdjustment" & intTemp) = 0
									}
									else if (vbPorterVar == "S")
									{
										// Amount
										// .Fields("DEWAdjustment" & intTemp) = 0
										// .Fields("DESAdjustment" & intTemp) = vsAdjust.TextMatrix(intTemp, lngAdjColAmount)
										modUTBilling.AddBreakDownRecord_452708(true, rsBill.Get_Fields_Int32("ID"), rsBill.Get_Fields_Int32("MeterKey"), "S", "Adj - " + vsAdjust.TextMatrix(intTemp, lngAdjColDescription), FCConvert.ToInt32(Conversion.Val(vsAdjust.TextMatrix(intTemp, lngAdjColCode))), Strings.Trim(vsAdjust.TextMatrix(intTemp, lngAdjColAccount)), FCConvert.ToDouble(vsAdjust.TextMatrix(intTemp, lngAdjColAmount)), "D", rsBill.Get_Fields_Int32("ActualAccountNumber"));
										dblSAdjust += FCConvert.ToDouble(vsAdjust.TextMatrix(intTemp, lngAdjColAmount));
									}
								}
							}
						}
						if (FCConvert.ToString(rsMeter.Get_Fields_String("Combine")) == "N" && rsMeter.Get_Fields_Boolean("NegativeConsumption") == false)
						{
							rsBill.Set_Fields("BillingCode", rsMeter.Get_Fields_String("BillingCode"));
							rsBill.Set_Fields("WDEAdjustAmount", dblWAdjust);
							rsBill.Set_Fields("SDEAdjustAmount", dblSAdjust);
						}
						for (intTemp = 1; intTemp <= 5; intTemp++)
						{
							// rate table information
							if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intTemp))) == false)
							{
								rsBill.Set_Fields("WRT" + intTemp, rsMeter.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intTemp)));
							}
							else
							{
								rsBill.Set_Fields("WRT" + intTemp, 0);
							}
							if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intTemp))) == false)
							{
								rsBill.Set_Fields("SRT" + intTemp, rsMeter.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intTemp)));
							}
							else
							{
								rsBill.Set_Fields("SRT" + intTemp, 0);
							}
						}
						rsBill.Update();
						UpdateBillRecord = false;
					}
					else
					{
						// remove this bill because it is no billed or final billed
						rsBill.Delete();
					}
				}
				else
				{
					UpdateBillRecord = true;
				}
				// clear adjustment table
				// ClearAdjustmentTable
				return UpdateBillRecord;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
			return UpdateBillRecord;
		}

		private void FormatHistoryGrid()
		{
			// this sub will reset data, column widths and headings
			vsHistory.ColWidth(0, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.04));
			vsHistory.ColWidth(1, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.15));
			vsHistory.ColWidth(2, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.26));
			vsHistory.ColWidth(3, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.26));
			vsHistory.ColWidth(4, FCConvert.ToInt32(vsHistory.WidthOriginal * 0.26));
			vsHistory.TextMatrix(0, 1, "Year");
			vsHistory.TextMatrix(0, 2, "Date");
			vsHistory.TextMatrix(0, 3, "Reading");
			vsHistory.TextMatrix(0, 4, "Consumption");
			//vsHistory.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 4, FCGrid.AlignmentSettings.flexAlignCenterCenter);
		}

		private void FormatRateGrid()
		{
			int lngWid = 0;
			vsRate.Cols = 6;
			vsRate.Rows = 6;
			vsRate.Select(1, lngRateColSewerTitle, vsRate.Rows - 1);
			vsRate.CellBorder(Color.Black, 2, 0, 0, 0, 2, 0);
			//FC:FINAL:MSH - issue #968: change alignment for displaying header over two columns
			//vsRate.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsRate.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsRate.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsRate.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsRate.Select(0, 0);
			vsRate.TextMatrix(0, lngRateColWaterTitle, "Water");
			//FC:FINAL:MSH - issue #968: remove extra headers
			//vsRate.TextMatrix(0, lngRateColWaterRK, "Water");
			//vsRate.TextMatrix(0, lngRateColWaterType, "Water");
			vsRate.TextMatrix(0, lngRateColSewerTitle, "Sewer");
			//vsRate.TextMatrix(0, lngRateColSewerType, "Sewer");
			//vsRate.TextMatrix(0, lngRateColSewerRK, "Sewer");
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				vsRate.TextMatrix(0, lngRateColWaterTitle, "Stormwater");
				//vsRate.TextMatrix(0, lngRateColWaterRK, "Stormwater");
				//vsRate.TextMatrix(0, lngRateColWaterType, "Stormwater");
			}
			vsRate.MergeCells = FCGrid.MergeCellsSettings.flexMergeFixedOnly;
			vsRate.MergeRow(0, true);
			lngWid = vsRate.WidthOriginal;
			vsRate.ColWidth(lngRateColWaterTitle, FCConvert.ToInt32(0.305 * lngWid));
			vsRate.ColWidth(lngRateColWaterType, 0);
			vsRate.ColWidth(lngRateColWaterRK, FCConvert.ToInt32(0.19 * lngWid));
			vsRate.ColWidth(lngRateColSewerTitle, FCConvert.ToInt32(0.305 * lngWid));
			vsRate.ColWidth(lngRateColSewerType, 0);
			vsRate.ColWidth(lngRateColSewerRK, FCConvert.ToInt32(0.19 * lngWid));
			// this will set the height of the grid
			//vsRate.Height = FCConvert.ToInt32((vsRate.Rows * vsRate.RowHeight(0)) + (vsRate.RowHeight(0) * 0.2));
		}

		private void FormatAdjustGrid()
		{
			int lngWid = 0;
			vsAdjust.Cols = 6;
			vsAdjust.Rows = 5;
			vsAdjust.TextMatrix(0, lngAdjColCode, "Code");
			vsAdjust.TextMatrix(0, lngAdjColDescription, "Description");
			vsAdjust.TextMatrix(0, lngAdjColAmount, "Amount");
			vsAdjust.TextMatrix(0, lngAdjColService, "Srv");
            vsAdjust.ColAlignment(lngAdjColAmount, FCGrid.AlignmentSettings.flexAlignRightCenter);
			lngWid = vsAdjust.WidthOriginal;
			vsAdjust.ColWidth(lngAdjColCode, FCConvert.ToInt32(0.148 * lngWid));
			vsAdjust.ColWidth(lngAdjColDescription, FCConvert.ToInt32(0.4 * lngWid));
			vsAdjust.ColWidth(lngAdjColAmount, FCConvert.ToInt32(0.3 * lngWid));
			vsAdjust.ColWidth(lngAdjColServiceAllowed, 0);
			vsAdjust.ColWidth(lngAdjColAccount, 0);
			vsAdjust.ColWidth(lngAdjColService, FCConvert.ToInt32(0.148 * lngWid));
			vsAdjust.ColFormat(lngAdjColAmount, "#,##0.00");
			vsAdjust.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//vsAdjust.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsAdjust.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// this will set the height of the grid
			//vsAdjust.Height = FCConvert.ToInt32((vsAdjust.Rows * vsAdjust.RowHeight(0)) + (vsAdjust.RowHeight(0) * 0.2));
			vsAdjust.EditingControlShowing -= VsAdjust_EditingControlShowing;
			vsAdjust.EditingControlShowing += VsAdjust_EditingControlShowing;
		}

		private void VsAdjust_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - issue #967: assign JavaScript to control if current cell is 'Amount' to allow enter only numeric values
				if (vsAdjust.Col == lngAdjColAmount)
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
						box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
                    }
				}
			}
		}

		private string GetAdjustList()
		{
			string GetAdjustList = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsAdj = new clsDRWrapper();
				GetAdjustList = "#0;0" + "\t" + "|";
				rsAdj.OpenRecordset("SELECT * FROM Adjust", modExtraModules.strUTDatabase);
				while (!rsAdj.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					GetAdjustList = GetAdjustList + "#" + rsAdj.Get_Fields("Code") + ";" + rsAdj.Get_Fields("Code") + "\t" + rsAdj.Get_Fields_String("ShortDescription") + "|";
					rsAdj.MoveNext();
				}
				// strip off the last pipe
				GetAdjustList = Strings.Left(GetAdjustList, GetAdjustList.Length - 1);
				return GetAdjustList;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Adjustment List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAdjustList;
		}

		private string GetAdjustServiceList(string strServ)
		{
			string GetAdjustServiceList = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (strServ == "B")
				{
					GetAdjustServiceList = "#0;Both|#1;Water|#2;Sewer";
				}
				else if (strServ == "W")
				{
					GetAdjustServiceList = "#1;Water";
				}
				else if (strServ == "S")
				{
					GetAdjustServiceList = "#2;Sewer";
				}
				else
				{
					GetAdjustServiceList = "#0;Both|#1;Water|#2;Sewer";
				}
				return GetAdjustServiceList;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Adjustment List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetAdjustServiceList;
		}

		private void vsRate_RowColChange(object sender, System.EventArgs e)
		{
			// If boolLockBoxes Then
			vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
			// Else
			// vsRate.Editable = flexEDKbdMouse
			// End If
		}

		private bool CheckForDuplicateSequence(ref int lngBook, ref string strList)
		{
			bool CheckForDuplicateSequence = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will check a book for Duplicate Sequence Numbers
				// if there are duplicates the function will return True and a comma delimited
				// string with the meter keys of the duplicates, if there are none, then this
				// function will return False and an empty string
				clsDRWrapper rsSeq = new clsDRWrapper();
				clsDRWrapper rsMeter = new clsDRWrapper();
				clsDRWrapper rsAccount = new clsDRWrapper();
				string strDeleted = "";
				rsSeq.OpenRecordset("SELECT * FROM (SELECT Count(ID) AS Meters, Sequence FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(lngBook) + " GROUP BY Sequence) AS qTmp WHERE Meters > 1", modExtraModules.strUTDatabase);
				if (!rsSeq.EndOfFile())
				{
					while (!rsSeq.EndOfFile())
					{
						// MAL@20080104: Check for deleted accounts before indicating that the meter sequence is truly a duplicate
						// Tracker Reference: 11863
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						rsAccount.OpenRecordset("SELECT * FROM Master INNER JOIN MeterTable ON Master.ID = MeterTable.AccountKey WHERE BookNumber = " + FCConvert.ToString(lngBook) + " AND Sequence = " + rsSeq.Get_Fields("Sequence"), modExtraModules.strUTDatabase);
						if (rsAccount.RecordCount() > 0)
						{
							rsAccount.MoveFirst();
							while (!rsAccount.EndOfFile())
							{
								if (FCConvert.ToBoolean(rsAccount.Get_Fields_Boolean("Deleted")))
								{
									if (strDeleted != "")
									{
										// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
										strDeleted += "," + rsAccount.Get_Fields("Sequence");
									}
									else
									{
										// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
										strDeleted = FCConvert.ToString(rsAccount.Get_Fields("Sequence"));
									}
								}
								else
								{
									strDeleted = strDeleted;
								}
								rsAccount.MoveNext();
							}
						}
						if (strDeleted != "")
						{
							// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
							rsMeter.OpenRecordset("SELECT ID FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(lngBook) + " AND Sequence = " + rsSeq.Get_Fields("Sequence") + " AND Sequence NOT IN(" + strDeleted + ")", modExtraModules.strUTDatabase);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
							rsMeter.OpenRecordset("SELECT ID FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(lngBook) + " AND Sequence = " + rsSeq.Get_Fields("Sequence"), modExtraModules.strUTDatabase);
						}
						if (rsMeter.RecordCount() > 0)
						{
							while (!rsMeter.EndOfFile())
							{
								CheckForDuplicateSequence = true;
								strList += rsMeter.Get_Fields_Int32("ID") + ",";
								rsMeter.MoveNext();
							}
						}
						rsSeq.MoveNext();
					}
				}
				else
				{
					CheckForDuplicateSequence = false;
					strList = "";
				}
				return CheckForDuplicateSequence;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Sequence Numbers", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckForDuplicateSequence;
		}

		private void ClearAdjustmentTable()
		{
			// this will clear the text out of the adjustment table
			int X;
			int Y;
			for (X = 0; X <= vsAdjust.Cols - 1; X++)
			{
				for (Y = 1; Y <= vsAdjust.Rows - 1; Y++)
				{
					vsAdjust.TextMatrix(Y, X, "");
				}
			}
		}

		private void CheckForDEMessage()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMaster = new clsDRWrapper();
				rsMaster.OpenRecordset("SELECT DataEntry FROM Master WHERE ID = " + rsMeter.Get_Fields_Int32("AccountKey"));
				if (!rsMaster.EndOfFile())
				{
					if (FCConvert.ToString(rsMaster.Get_Fields_String("DataEntry")) != "")
					{
						if (DialogResult.Yes == MessageBox.Show(Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("DataEntry"))) + "\r\n" + "\r\n" + "\r\n" + "Would you like to delete this message?", "Date Entry Message", MessageBoxButtons.YesNo, MessageBoxIcon.Warning))
						{
							{
								modGlobalFunctions.AddCYAEntry_80("UT", "Deleting DE Message", "Account Key : " + rsMeter.Get_Fields_Int32("AccountKey"), Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("DataEntry"))));
								rsMaster.Edit();
								rsMaster.Set_Fields("DataEntry", "");
								rsMaster.Update();
							}
						}
					}
					return;
				}
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking DE Message", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillAdjustmentGrid_6(int lngMK, int lngBK)
		{
			FillAdjustmentGrid(ref lngMK, ref lngBK);
		}

		private void FillAdjustmentGrid(ref int lngMK, ref int lngBK)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will fill the adjustment grid with the adjustments that have already been created for this meter
				clsDRWrapper rsAdj = new clsDRWrapper();
				int lngRW;
				ClearAdjustmentTable();
				lngRW = 1;
				rsAdj.OpenRecordset("SELECT * FROM TempBreakDown WHERE MeterKey = " + FCConvert.ToString(lngMK) + " AND Type = 'D' AND BillKey = " + FCConvert.ToString(lngBK), modExtraModules.strUTDatabase);
				while (!rsAdj.EndOfFile())
				{
					vsAdjust.TextMatrix(lngRW, lngAdjColCode, FCConvert.ToString(rsAdj.Get_Fields_Int32("AdjCode")));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					vsAdjust.TextMatrix(lngRW, lngAdjColAmount, FCConvert.ToString(rsAdj.Get_Fields("Amount")));
					vsAdjust.TextMatrix(lngRW, lngAdjColService, FCConvert.ToString(rsAdj.Get_Fields_String("Service")));
					vsAdjust.TextMatrix(lngRW, lngAdjColDescription, Strings.Trim(Strings.Right(FCConvert.ToString(rsAdj.Get_Fields_String("Description")), FCConvert.ToString(rsAdj.Get_Fields_String("Description")).Length - 6)));
					lngRW += 1;
					rsAdj.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Adjustments", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool Bangor_ShouldBillBeSaved()
		{
			bool Bangor_ShouldBillBeSaved = false;
			// Determine if there the Save/Update/Create bill routines should be called
			// Return True if -
			// 1) A bill record already exists
			// 2) A reading was entered for the account
			// 3) Adjustments/Charges were added for this account
			// 4) Flat or Units based billing and none of the above then prompt
			clsDRWrapper rsBillCheck = new clsDRWrapper();
			int intCT;
			bool boolAdjustment = false;
			bool boolReturn;
			try
			{
				boolReturn = false;
				rsBillCheck.OpenRecordset("SELECT ID FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsMeter.Get_Fields_Int32("ID") + " AND BillStatus <> 'B' AND BillStatus <> 'C'");
				if (!rsBillCheck.EndOfFile())
				{
					// A bill already exists - update it
					boolReturn = true;
				}
				else
				{
					if (txtReading.Text != "")
					{
						// The user entered a reading - create a bill
						boolReturn = true;
					}
					else
					{
						// check for any adjustment
						for (intCT = 1; intCT <= 4; intCT++)
						{
							if (Conversion.Val(vsAdjust.TextMatrix(intCT, lngAdjColAmount)) != 0)
							{
								boolAdjustment = true;
								break;
							}
						}
						if (boolAdjustment)
						{
							// The user entered adjustments - create a bill
							boolReturn = true;
						}
						else
						{
							if (!BilledType_8(1, true))
							{
								// Flat or Unit based and none of the above criteria have been met, then we must prompt
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								boolReturn = FCConvert.CBool(DialogResult.Yes == MessageBox.Show("Account " + rsMeter.Get_Fields("AccountNumber") + " has not been billed. Do you want to force a bill to be created?", "Force Bill?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2));
							}
						}
					}
				}
				Bangor_ShouldBillBeSaved = boolReturn;
				return Bangor_ShouldBillBeSaved;
			}
			catch (Exception ex)
			{
				//ERROR_HANDLER: ;
				Bangor_ShouldBillBeSaved = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Evaluating Bangor Reqs", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Bangor_ShouldBillBeSaved;
		}
	}
}
