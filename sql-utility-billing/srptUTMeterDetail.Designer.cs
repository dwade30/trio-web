namespace UtilityBillingReports.Reports
{
	/// <summary>
	/// Summary description for srptUTMeterDetail.
	/// </summary>
	partial class srptUTMeterDetail
	{

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
			}
			base.Dispose(disposing);
		}

		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptUTMeterDetail));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblRegular = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBillDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCons = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMisc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbStatus = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAdj = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReading = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAmountW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegularW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMiscW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAdjW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReading = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmountS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegularS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMiscS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTaxS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAdjS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldServiceW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldServiceS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldActualCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalMisc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAdj = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalActualCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmountW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegularW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAdjW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatus)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmountS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegularS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAdjS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldServiceW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldServiceS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActualCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalActualCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAmountW,
            this.fldBillDate,
            this.fldCons,
            this.fldRegularW,
            this.fldMiscW,
            this.fldTaxW,
            this.fldAdjW,
            this.fldStatus,
            this.fldReading,
            this.fldAmountS,
            this.fldRegularS,
            this.fldMiscS,
            this.fldTaxS,
            this.fldAdjS,
            this.fldServiceW,
            this.fldServiceS,
            this.fldActualCons});
			this.Detail.Height = 0.1979167F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblRegular,
            this.lblAmount,
            this.lblBillDate,
            this.lblCons,
            this.lblMisc,
            this.lblTax,
            this.lbStatus,
            this.lblAdj,
            this.lblReading,
            this.Label1});
			this.GroupHeader1.Height = 0.3229167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldFooter,
            this.fldTotalAmount,
            this.Line2,
            this.fldTotalCons,
            this.fldTotalRegular,
            this.fldTotalMisc,
            this.fldTotalTax,
            this.fldTotalAdj,
            this.fldTotalActualCons});
			this.GroupFooter1.Height = 0.5F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblRegular
			// 
			this.lblRegular.Height = 0.1875F;
			this.lblRegular.HyperLink = null;
			this.lblRegular.Left = 4.21875F;
			this.lblRegular.Name = "lblRegular";
			this.lblRegular.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblRegular.Text = "Regular";
			this.lblRegular.Top = 0.125F;
			this.lblRegular.Width = 0.9375F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 7.71875F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.125F;
			this.lblAmount.Width = 1.125F;
			// 
			// lblBillDate
			// 
			this.lblBillDate.Height = 0.1875F;
			this.lblBillDate.HyperLink = null;
			this.lblBillDate.Left = 0F;
			this.lblBillDate.Name = "lblBillDate";
			this.lblBillDate.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblBillDate.Text = "Date";
			this.lblBillDate.Top = 0.125F;
			this.lblBillDate.Width = 0.9375F;
			// 
			// lblCons
			// 
			this.lblCons.Height = 0.1875F;
			this.lblCons.HyperLink = null;
			this.lblCons.Left = 3.21875F;
			this.lblCons.Name = "lblCons";
			this.lblCons.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblCons.Text = "Billed Cons";
			this.lblCons.Top = 0.125F;
			this.lblCons.Width = 0.96875F;
			// 
			// lblMisc
			// 
			this.lblMisc.Height = 0.1875F;
			this.lblMisc.HyperLink = null;
			this.lblMisc.Left = 5.15625F;
			this.lblMisc.Name = "lblMisc";
			this.lblMisc.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblMisc.Text = "Misc";
			this.lblMisc.Top = 0.125F;
			this.lblMisc.Width = 0.875F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 6.03125F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.125F;
			this.lblTax.Width = 0.8125F;
			// 
			// lbStatus
			// 
			this.lbStatus.Height = 0.1875F;
			this.lbStatus.HyperLink = null;
			this.lbStatus.Left = 0.9375F;
			this.lbStatus.Name = "lbStatus";
			this.lbStatus.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lbStatus.Text = "Status";
			this.lbStatus.Top = 0.125F;
			this.lbStatus.Width = 0.5625F;
			// 
			// lblAdj
			// 
			this.lblAdj.Height = 0.1875F;
			this.lblAdj.HyperLink = null;
			this.lblAdj.Left = 6.84375F;
			this.lblAdj.Name = "lblAdj";
			this.lblAdj.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblAdj.Text = "Adjust";
			this.lblAdj.Top = 0.125F;
			this.lblAdj.Width = 0.875F;
			// 
			// lblReading
			// 
			this.lblReading.Height = 0.1875F;
			this.lblReading.HyperLink = null;
			this.lblReading.Left = 1.5F;
			this.lblReading.Name = "lblReading";
			this.lblReading.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblReading.Text = "Reading";
			this.lblReading.Top = 0.125F;
			this.lblReading.Width = 0.625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 2.21875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.Label1.Text = "Actual Cons";
			this.Label1.Top = 0.125F;
			this.Label1.Width = 0.96875F;
			// 
			// fldAmountW
			// 
			this.fldAmountW.Height = 0.1875F;
			this.fldAmountW.Left = 7.71875F;
			this.fldAmountW.Name = "fldAmountW";
			this.fldAmountW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAmountW.Text = null;
			this.fldAmountW.Top = 0F;
			this.fldAmountW.Width = 1.125F;
			// 
			// fldBillDate
			// 
			this.fldBillDate.Height = 0.1875F;
			this.fldBillDate.Left = 0F;
			this.fldBillDate.Name = "fldBillDate";
			this.fldBillDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldBillDate.Text = null;
			this.fldBillDate.Top = 0F;
			this.fldBillDate.Width = 0.9375F;
			// 
			// fldCons
			// 
			this.fldCons.Height = 0.1875F;
			this.fldCons.Left = 3.21875F;
			this.fldCons.Name = "fldCons";
			this.fldCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldCons.Text = null;
			this.fldCons.Top = 0F;
			this.fldCons.Width = 0.96875F;
			// 
			// fldRegularW
			// 
			this.fldRegularW.Height = 0.1875F;
			this.fldRegularW.Left = 4.21875F;
			this.fldRegularW.Name = "fldRegularW";
			this.fldRegularW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldRegularW.Text = null;
			this.fldRegularW.Top = 0F;
			this.fldRegularW.Width = 0.9375F;
			// 
			// fldMiscW
			// 
			this.fldMiscW.Height = 0.1875F;
			this.fldMiscW.Left = 5.15625F;
			this.fldMiscW.Name = "fldMiscW";
			this.fldMiscW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldMiscW.Text = null;
			this.fldMiscW.Top = 0F;
			this.fldMiscW.Width = 0.875F;
			// 
			// fldTaxW
			// 
			this.fldTaxW.Height = 0.1875F;
			this.fldTaxW.Left = 6.03125F;
			this.fldTaxW.Name = "fldTaxW";
			this.fldTaxW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTaxW.Text = null;
			this.fldTaxW.Top = 0F;
			this.fldTaxW.Width = 0.8125F;
			// 
			// fldAdjW
			// 
			this.fldAdjW.Height = 0.1875F;
			this.fldAdjW.Left = 6.84375F;
			this.fldAdjW.Name = "fldAdjW";
			this.fldAdjW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAdjW.Text = null;
			this.fldAdjW.Top = 0F;
			this.fldAdjW.Width = 0.875F;
			// 
			// fldStatus
			// 
			this.fldStatus.Height = 0.1875F;
			this.fldStatus.Left = 0.9375F;
			this.fldStatus.Name = "fldStatus";
			this.fldStatus.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldStatus.Text = null;
			this.fldStatus.Top = 0F;
			this.fldStatus.Width = 0.3125F;
			// 
			// fldReading
			// 
			this.fldReading.Height = 0.1875F;
			this.fldReading.Left = 1.5F;
			this.fldReading.Name = "fldReading";
			this.fldReading.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldReading.Text = null;
			this.fldReading.Top = 0F;
			this.fldReading.Width = 0.625F;
			// 
			// fldAmountS
			// 
			this.fldAmountS.Height = 0.1875F;
			this.fldAmountS.Left = 7.71875F;
			this.fldAmountS.Name = "fldAmountS";
			this.fldAmountS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAmountS.Text = null;
			this.fldAmountS.Top = 0.1875F;
			this.fldAmountS.Visible = false;
			this.fldAmountS.Width = 1.125F;
			// 
			// fldRegularS
			// 
			this.fldRegularS.Height = 0.1875F;
			this.fldRegularS.Left = 4.21875F;
			this.fldRegularS.Name = "fldRegularS";
			this.fldRegularS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldRegularS.Text = null;
			this.fldRegularS.Top = 0.1875F;
			this.fldRegularS.Visible = false;
			this.fldRegularS.Width = 0.9375F;
			// 
			// fldMiscS
			// 
			this.fldMiscS.Height = 0.1875F;
			this.fldMiscS.Left = 5.15625F;
			this.fldMiscS.Name = "fldMiscS";
			this.fldMiscS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldMiscS.Text = null;
			this.fldMiscS.Top = 0.1875F;
			this.fldMiscS.Visible = false;
			this.fldMiscS.Width = 0.875F;
			// 
			// fldTaxS
			// 
			this.fldTaxS.Height = 0.1875F;
			this.fldTaxS.Left = 6.03125F;
			this.fldTaxS.Name = "fldTaxS";
			this.fldTaxS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTaxS.Text = null;
			this.fldTaxS.Top = 0.1875F;
			this.fldTaxS.Visible = false;
			this.fldTaxS.Width = 0.8125F;
			// 
			// fldAdjS
			// 
			this.fldAdjS.Height = 0.1875F;
			this.fldAdjS.Left = 6.84375F;
			this.fldAdjS.Name = "fldAdjS";
			this.fldAdjS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAdjS.Text = null;
			this.fldAdjS.Top = 0.1875F;
			this.fldAdjS.Visible = false;
			this.fldAdjS.Width = 0.875F;
			// 
			// fldServiceW
			// 
			this.fldServiceW.Height = 0.1875F;
			this.fldServiceW.Left = 1.25F;
			this.fldServiceW.Name = "fldServiceW";
			this.fldServiceW.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldServiceW.Text = null;
			this.fldServiceW.Top = 0F;
			this.fldServiceW.Width = 0.25F;
			// 
			// fldServiceS
			// 
			this.fldServiceS.Height = 0.1875F;
			this.fldServiceS.Left = 1.25F;
			this.fldServiceS.Name = "fldServiceS";
			this.fldServiceS.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldServiceS.Text = null;
			this.fldServiceS.Top = 0.1875F;
			this.fldServiceS.Visible = false;
			this.fldServiceS.Width = 0.25F;
			// 
			// fldActualCons
			// 
			this.fldActualCons.Height = 0.1875F;
			this.fldActualCons.Left = 2.21875F;
			this.fldActualCons.Name = "fldActualCons";
			this.fldActualCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldActualCons.Text = null;
			this.fldActualCons.Top = 0F;
			this.fldActualCons.Width = 0.96875F;
			// 
			// fldFooter
			// 
			this.fldFooter.Height = 0.1875F;
			this.fldFooter.Left = 0F;
			this.fldFooter.Name = "fldFooter";
			this.fldFooter.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.fldFooter.Text = null;
			this.fldFooter.Top = 0.25F;
			this.fldFooter.Width = 2.125F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 7.71875F;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalAmount.Text = null;
			this.fldTotalAmount.Top = 0.25F;
			this.fldTotalAmount.Width = 1.125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.125F;
			this.Line2.Width = 6.78125F;
			this.Line2.X1 = 2.125F;
			this.Line2.X2 = 8.90625F;
			this.Line2.Y1 = 0.125F;
			this.Line2.Y2 = 0.125F;
			// 
			// fldTotalCons
			// 
			this.fldTotalCons.Height = 0.1875F;
			this.fldTotalCons.Left = 3.21875F;
			this.fldTotalCons.Name = "fldTotalCons";
			this.fldTotalCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalCons.Text = null;
			this.fldTotalCons.Top = 0.25F;
			this.fldTotalCons.Width = 0.96875F;
			// 
			// fldTotalRegular
			// 
			this.fldTotalRegular.Height = 0.1875F;
			this.fldTotalRegular.Left = 4.21875F;
			this.fldTotalRegular.Name = "fldTotalRegular";
			this.fldTotalRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalRegular.Text = null;
			this.fldTotalRegular.Top = 0.25F;
			this.fldTotalRegular.Width = 0.9375F;
			// 
			// fldTotalMisc
			// 
			this.fldTotalMisc.Height = 0.1875F;
			this.fldTotalMisc.Left = 5.15625F;
			this.fldTotalMisc.Name = "fldTotalMisc";
			this.fldTotalMisc.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalMisc.Text = null;
			this.fldTotalMisc.Top = 0.25F;
			this.fldTotalMisc.Width = 0.875F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 6.03125F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalTax.Text = null;
			this.fldTotalTax.Top = 0.25F;
			this.fldTotalTax.Width = 0.8125F;
			// 
			// fldTotalAdj
			// 
			this.fldTotalAdj.Height = 0.1875F;
			this.fldTotalAdj.Left = 6.84375F;
			this.fldTotalAdj.Name = "fldTotalAdj";
			this.fldTotalAdj.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalAdj.Text = null;
			this.fldTotalAdj.Top = 0.25F;
			this.fldTotalAdj.Width = 0.875F;
			// 
			// fldTotalActualCons
			// 
			this.fldTotalActualCons.Height = 0.1875F;
			this.fldTotalActualCons.Left = 2.21875F;
			this.fldTotalActualCons.Name = "fldTotalActualCons";
			this.fldTotalActualCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalActualCons.Text = null;
			this.fldTotalActualCons.Top = 0.25F;
			this.fldTotalActualCons.Width = 0.96875F;
			// 
			// srptUTMeterDetail
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmountW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegularW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAdjW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStatus)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmountS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegularS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMiscS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTaxS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAdjS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldServiceW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldServiceS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldActualCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalActualCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmountW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatus;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReading;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmountS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegularS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMiscS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTaxS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAdjS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldServiceW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldServiceS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldActualCons;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRegular;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCons;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMisc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbStatus;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAdj;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReading;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMisc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAdj;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalActualCons;
	}
}
