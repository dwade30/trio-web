﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTREAcctSync.
	/// </summary>
	public partial class rptUTREAcctSync : BaseSectionReport
	{
		public rptUTREAcctSync()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "UT/RE Account Owner Sync";
		}

		public static rptUTREAcctSync InstancePtr
		{
			get
			{
				return (rptUTREAcctSync)Sys.GetInstance(typeof(rptUTREAcctSync));
			}
		}

		protected rptUTREAcctSync _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsCP.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUTREAcctSync	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		clsDRWrapper rsCP = new clsDRWrapper();
		int lngRecCnt;
		int lngProcCnt;
		int lngRptPass;

		public void Init(int lngPass, bool modalDialog)
		{
			lngRptPass = lngPass;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngRptPass == 1)
			{
				eArgs.EOF = FCConvert.CBool(lngRecCnt > Information.UBound(modStormwater.Statics.AcctSyncRpt, 1));
			}
			else
			{
				eArgs.EOF = FCConvert.CBool(lngRecCnt > Information.UBound(modStormwater.Statics.MultiREAcct, 1));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngRecCnt = 1;
			lngProcCnt = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngRptPass == 1)
			{
				if (lngRecCnt > Information.UBound(modStormwater.Statics.AcctSyncRpt, 1))
				{
					Detail.Visible = false;
					return;
				}
				fldUTAcct.Text = modStormwater.Statics.AcctSyncRpt[lngRecCnt].UTAcctNum.ToString();
				fldREAcct.Text = modStormwater.Statics.AcctSyncRpt[lngRecCnt].REAcctNum.ToString();
				rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(modStormwater.Statics.AcctSyncRpt[lngRecCnt].UTOwnID1), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					fldOldOwn1.Text = rsCP.Get_Fields_String("FullNameLF");
				}
				else
				{
					fldOldOwn1.Text = "";
				}
				rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(modStormwater.Statics.AcctSyncRpt[lngRecCnt].REOwnID1), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					fldNewOwn1.Text = rsCP.Get_Fields_String("FullNameLF");
				}
				else
				{
					fldNewOwn1.Text = "";
				}
				rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(modStormwater.Statics.AcctSyncRpt[lngRecCnt].UTOwnID2), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					fldOldOwn2.Text = rsCP.Get_Fields_String("FullNameLF");
				}
				else
				{
					fldOldOwn2.Text = "";
				}
				rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(modStormwater.Statics.AcctSyncRpt[lngRecCnt].REOwnID2), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					fldNewOwn2.Text = rsCP.Get_Fields_String("FullNameLF");
				}
				else
				{
					fldNewOwn2.Text = "";
				}
				fldLocation.Text = modStormwater.Statics.AcctSyncRpt[lngRecCnt].Location;
			}
			else
			{
				if (lngRecCnt > Information.UBound(modStormwater.Statics.MultiREAcct, 1))
				{
					Detail.Visible = false;
					return;
				}
				fldUTAcct.Text = modStormwater.Statics.MultiREAcct[lngRecCnt].UTAcctNum.ToString();
				fldREAcct.Text = modStormwater.Statics.MultiREAcct[lngRecCnt].REAcctNum.ToString();
				rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(modStormwater.Statics.MultiREAcct[lngRecCnt].UTOwnID1), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					fldOldOwn1.Text = rsCP.Get_Fields_String("FullNameLF");
				}
				else
				{
					fldOldOwn1.Text = "";
				}
				rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(modStormwater.Statics.MultiREAcct[lngRecCnt].REOwnID1), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					fldNewOwn1.Text = rsCP.Get_Fields_String("FullNameLF");
				}
				else
				{
					fldNewOwn1.Text = "";
				}
				rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(modStormwater.Statics.MultiREAcct[lngRecCnt].UTOwnID2), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					fldOldOwn2.Text = rsCP.Get_Fields_String("FullNameLF");
				}
				else
				{
					fldOldOwn2.Text = "";
				}
				rsCP.OpenRecordset("SELECT * FROM PartyNameView WHERE ID = " + FCConvert.ToString(modStormwater.Statics.MultiREAcct[lngRecCnt].REOwnID2), "CentralParties");
				if (!rsCP.EndOfFile())
				{
					fldNewOwn2.Text = rsCP.Get_Fields_String("FullNameLF");
				}
				else
				{
					fldNewOwn2.Text = "";
				}
				fldLocation.Text = modStormwater.Statics.MultiREAcct[lngRecCnt].Location;
			}
			lngProcCnt += 1;
			lngRecCnt += 1;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldProcCnt.Text = lngProcCnt.ToString();
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			fldDate.Text = Strings.Format(DateTime.Now, "MM/dd/yyyy");
			fldUserID.Text = modGlobalConstants.Statics.gstrUserID;
			if (lngRptPass == 2)
			{
				lblCaption.Text = "Multiple RE Accounts";
			}
		}

		private void rptUTREAcctSync_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUTREAcctSync properties;
			//rptUTREAcctSync.Caption	= "UT/RE Account Owner Sync";
			//rptUTREAcctSync.Left	= 0;
			//rptUTREAcctSync.Top	= 0;
			//rptUTREAcctSync.Width	= 14040;
			//rptUTREAcctSync.Height	= 7095;
			//rptUTREAcctSync.StartUpPosition	= 3;
			//rptUTREAcctSync.SectionData	= "rptUTREAcctSync.dsx":0000;
			//End Unmaped Properties
		}
	}
}
