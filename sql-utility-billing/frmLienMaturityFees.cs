﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmLienMaturityFees.
	/// </summary>
	public partial class frmLienMaturityFees : BaseForm
	{
		public frmLienMaturityFees()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLienMaturityFees InstancePtr
		{
			get
			{
				return (frmLienMaturityFees)Sys.GetInstance(typeof(frmLienMaturityFees));
			}
		}

		protected frmLienMaturityFees _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/28/2008              *
		// ********************************************************
		int intAction;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		clsDRWrapper rsValidate = new clsDRWrapper();
		bool boolLoaded;
		double dblFilingFee;
		double dblCertMailFee;
		bool boolChargeMort;
		bool boolChargeCert;
		// vbPorter upgrade warning: dtLMFDate As DateTime	OnWrite(string)
		DateTime dtLMFDate;
		int lngRK;
		bool boolDONOTACTIVATE;
		bool boolWater;
		string strWS = "";
		string strBookList;
		string strRateKeysList;
		string strRateKeys;
		Dictionary<object, object> objDictionary = new Dictionary<object, object>();
		bool boolChargeForNewOwner;
		bool boolSendToNewOwner;

		public void Init(ref string strPassBookList, ref string strRateKeyList)
		{
			strBookList = strPassBookList;
			strRateKeysList = strRateKeyList;
			this.Show(App.MainForm);
		}

		private void chkIntApplied_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkIntApplied.CheckState == Wisej.Web.CheckState.Checked)
			{
				txtIntDate.Enabled = true;
			}
			else
			{
				txtIntDate.Enabled = false;
			}
		}

		private void frmLienMaturityFees_Activated(object sender, System.EventArgs e)
		{
			// MAL@20070918: Changed to allow for multiple rate key selection
			// lngRK = frmRateRecChoice.lngRateRecNumber
			// If lngRK > 0 Then
			if (Strings.Trim(strRateKeysList) != "")
			{
				if (!boolLoaded)
				{
					FormatGrid();
					ShowGridFrame();
					this.Text = "Add Lien Maturity Fees";
					lblInstruction.Text = "To add lien maturity fees:" + "\r\n" + "1. Check the box beside the account." + "\r\n" + "2. Select 'Add Lien Maturity Fees' or press F12.";
					lblValidateInstruction.Text = "If these values are correct, Press F12 to advance.  If not, then please rerun your Lien Maturity Notices.";
					SetAct_2(0);
					FillValidateGrid();
					boolLoaded = true;
					if (!boolDONOTACTIVATE)
					{
						if (!FillDemandGrid())
						{
							boolDONOTACTIVATE = true;
							this.Unload();
						}
						else
						{
							boolDONOTACTIVATE = false;
						}
					}
					else
					{
						// MsgBox "Stop"
					}
				}
			}
			else
			{
				// MsgBox "The rate record selected is 0.  Please choose another rate record.", vbInformation, "Rate Record Violation"
				MessageBox.Show("No rate record selected. Please choose a rate record.", "Rate Record Violation", MessageBoxButtons.OK, MessageBoxIcon.Information);
				frmRateRecChoice.InstancePtr.Show(App.MainForm);
				this.Unload();
				return;
			}
		}

		private void frmLienMaturityFees_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLienMaturityFees properties;
			//frmLienMaturityFees.FillStyle	= 0;
			//frmLienMaturityFees.ScaleWidth	= 9045;
			//frmLienMaturityFees.ScaleHeight	= 6930;
			//frmLienMaturityFees.LinkTopic	= "Form2";
			//frmLienMaturityFees.LockControls	= true;
			//frmLienMaturityFees.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			boolWater = frmRateRecChoice.InstancePtr.cmbWS.Text == "Water";
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			boolLoaded = false;
			// clear the array of user defined types
			FCUtils.EraseSafe(modUTLien.Statics.arrDemand);
		}

		private void frmLienMaturityFees_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
				return;
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
			boolDONOTACTIVATE = false;
			objDictionary = null;
		}

		private void frmLienMaturityFees_Resize(object sender, System.EventArgs e)
		{
			if (fraGrid.Visible)
			{
				ShowGridFrame();
				// this sets the height of the grid
				if ((vsDemand.Rows * vsDemand.RowHeight(0)) + 70 > fraGrid.Height - vsDemand.Top - 300)
				{
					//vsDemand.Height = fraGrid.Height - vsDemand.Top - 300;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
				else
				{
					//vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
			}
			else
			{
				ShowValidateFrame();
			}
		}

		private void mnuFileClear_Click(object sender, System.EventArgs e)
		{
			// this will clear all account checkboxes in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(0));
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void ShowValidateFrame()
		{
			// this will show/center the frame with the grid on it
			fraValidate.Top = FCConvert.ToInt32((this.Height - fraValidate.Height) / 3.0);
			fraValidate.Left = FCConvert.ToInt32((this.Width - fraValidate.Width) / 2.0);
			fraValidate.Visible = true;
		}

		private void ShowGridFrame()
		{
			// this will show/center the frame with the grid on it
			//fraGrid.Top = FCConvert.ToInt32((this.Height - fraGrid.Height) / 3.0);
			//fraGrid.Left = FCConvert.ToInt32((this.Width - fraGrid.Width) / 2.0);
			fraGrid.Visible = true;
		}

		private void FormatGrid()
		{
			int wid = 0;
			vsDemand.Cols = 7;
			vsDemand.Rows = 1;
			wid = vsDemand.WidthOriginal;
			vsDemand.ColWidth(0, FCConvert.ToInt32(wid * 0.1));
			// Checkbox
			vsDemand.ColWidth(1, FCConvert.ToInt32(wid * 0.15));
			// Acct
			vsDemand.ColWidth(2, FCConvert.ToInt32(wid * 0.45));
			// Name
			vsDemand.ColWidth(3, FCConvert.ToInt32(wid * 0.15));
			// Total Notices Sent
			vsDemand.ColWidth(4, FCConvert.ToInt32(wid * 0.1));
			// Total Lien Costs
			vsDemand.ColWidth(5, 0);
			// Hidden Key Field
			vsDemand.ColWidth(6, 0);
			// Hidden Code Field - 0 is ok to process...anything else is bad
			vsDemand.ColFormat(4, "#,##0.00");
			vsDemand.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsDemand.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(3, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsDemand.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsDemand.TextMatrix(0, 1, "Account");
			vsDemand.TextMatrix(0, 2, "Name");
			vsDemand.TextMatrix(0, 3, "Notices");
			vsDemand.TextMatrix(0, 4, "Amount");
			vsValidate.Cols = 3;
			wid = vsValidate.WidthOriginal;
			vsValidate.ColWidth(0, FCConvert.ToInt32(wid * 0.6));
			// Question
			vsValidate.ColWidth(1, FCConvert.ToInt32(wid * 0.32));
			// Answer
			vsValidate.ColWidth(2, 0);
			// Hidden field
			vsValidate.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsValidate.TextMatrix(0, 1, "Parameters");
			vsValidate.TextMatrix(0, 2, "Value");
		}

		private bool FillDemandGrid()
		{
			bool FillDemandGrid = false;
			int lngError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with eligible accounts
				string strSQL = "";
				int lngIndex;
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsRateKeys = new clsDRWrapper();
				int lngLastAccountKey;
				clsDRWrapper rsTemp = new clsDRWrapper();
				double dblXInt = 0;
				double dblTotalDue = 0;
				string strSQLFields;
				FillDemandGrid = true;
				rsMaster.OpenRecordset("SELECT * FROM Master", modExtraModules.strUTDatabase);
				lngError = 1;
				if (Strings.Trim(strBookList) != "")
				{
					strBookList = " AND (" + strBookList + ")";
				}
				vsDemand.Rows = 1;
				lngError = 2;
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Info");
				lngError = 3;
				// kk 10032013
				strSQLFields = "BillingRateKey, OName, Bill.BillDate, " + strWS + "LienRecordNumber, " + strWS + "CombinationLienKey, " + "Copies, AccountKey, Bill.ID, BillingYear";
				// MAL@20081222: Added Rate Key to the Order By so that fees are applied to the oldest rate key first
				// Tracker Reference: 16214
				if (frmRateRecChoice.InstancePtr.cmbRange.Text == "All Accounts")
				{
					// MAL@20070918
					// strSQL = "SELECT * FROM Bill INNER JOIN Lien ON Bill." & strWS & "LienRecordNumber = Lien.LienKey WHERE Lien.RateKey = " & lngRK & " AND " & strWS & "LienProcessStatus = 5 AND " & strWS & "LienStatusEligibility >= 5 AND Bill = " & strWS & "CombinationLienKey " & strBookList & " ORDER BY OName"      'this will be all the records that have had Lien Maturity Notices
					// MAL@20071218: Added check for those where the LDN has not been printed
					// Tracker Reference: 11675
					// DJW@20090501: Taking this check out and just using Prin Amount still owed to determine if they should get this or not
					// Tracker Reference: 18357
					// strSQL = "SELECT * FROM Bill INNER JOIN Lien ON Bill." & strWS & "LienRecordNumber = Lien.LienKey WHERE Lien.RateKey IN " & strRateKeysList & " AND " & strWS & "LienProcessStatus = 5 AND " & strWS & "LienStatusEligibility >= 5 " & strBookList & " ORDER BY OName"
					strSQL = "SELECT " + strSQLFields + " FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE Bill.ID = " + strWS + "CombinationLienKey AND Lien.RateKey IN " + strRateKeysList + " AND " + strWS + "LienProcessStatus = 5 AND " + strWS + "LienStatusEligibility >= 5 AND NOT (LienProcessExclusion = 1) " + strBookList + " AND (Principal - Lien.PrinPaid) > 0";
					// kk 10032013   ORDER BY OName, Bill.BillDate"
				}
				else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
				{
					// strSQL = "SELECT * FROM Bill INNER JOIN Lien ON Bill." & strWS & "LienRecordNumber = Lien.LienKey WHERE Lien.RateKey = " & lngRK & " AND " & strWS & "LienProcessStatus = 5 AND " & strWS & "LienStatusEligibility >= 5 AND Bill = " & strWS & "CombinationLienKey " & strBookList & " AND OName > '" & frmRateRecChoice.txtRange(0).Text & "    ' AND OName <= '" & frmRateRecChoice.txtRange(1).Text & "ZZZZ' ORDER BY OName"
					strSQL = "SELECT " + strSQLFields + " FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE Bill.ID = " + strWS + "CombinationLienKey AND Lien.RateKey IN " + strRateKeysList + " AND " + strWS + "LienProcessStatus = 5 AND " + strWS + "LienStatusEligibility >= 5 AND NOT (LienProcessExclusion = 1) " + strBookList + " AND OName > '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ' AND (Principal - Lien.PrinPaid) > 0";
					// kk 10032013   ORDER BY OName, Bill.BillDate"
				}
				else
				{
					// strSQL = "SELECT * FROM Bill INNER JOIN Lien ON Bill." & strWS & "LienRecordNumber = Lien.LienKey WHERE Lien.RateKey = " & lngRK & " AND " & strWS & "LienProcessStatus = 5 AND " & strWS & "LienStatusEligibility >= 5 AND Bill = " & strWS & "CombinationLienKey " & strBookList & " AND Account >= " & frmRateRecChoice.txtRange(0).Text & " AND Account <= " & frmRateRecChoice.txtRange(1).Text & " ORDER BY OName"
					strSQL = "SELECT " + strSQLFields + " FROM Bill INNER JOIN Lien ON Bill." + strWS + "LienRecordNumber = Lien.ID WHERE Bill.ID = " + strWS + "CombinationLienKey AND Lien.RateKey IN " + strRateKeysList + " AND " + strWS + "LienProcessStatus = 5 AND " + strWS + "LienStatusEligibility >= 5 AND NOT (LienProcessExclusion = 1) " + strBookList + " AND ActualAccountNumber >= " + frmRateRecChoice.InstancePtr.txtRange[0].Text + " AND ActualAccountNumber <= " + frmRateRecChoice.InstancePtr.txtRange[1].Text + " AND (Principal - Lien.PrinPaid) > 0";
					// kk 10032013   ORDER BY OName, Bill.BillDate"
				}
				strRateKeys = "";
				rsRateKeys.OpenRecordset("SELECT DISTINCT BillingRateKey FROM (" + strSQL + ") AS qTmp", "TWUT0000.vb1");
				if (rsRateKeys.EndOfFile() != true && rsRateKeys.BeginningOfFile() != true)
				{
					do
					{
						//Application.DoEvents();
						strRateKeys += rsRateKeys.Get_Fields_Int32("BillingRateKey") + ", ";
						rsRateKeys.MoveNext();
					}
					while (rsRateKeys.EndOfFile() != true);
				}
				if (strRateKeys != "")
				{
					strRateKeys = Strings.Left(strRateKeys, strRateKeys.Length - 2);
				}
				lngError = 4;
				// kk 10032013  Move order by, can't order by in a subquery
				strSQL += " ORDER BY OName, Bill.BillDate";
				// strSQL = "SELECT * FROM BillingMaster WHERE LienProcessStatus = 5 AND LienStatusEligibility = 5 ORDER BY Name1"      'this will be all the records that have had 30 Day Notices printed
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				rsLData.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
				// this is a recordset of all the lien records
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					FillDemandGrid = false;
					MessageBox.Show("There are no accounts eligible to transfer to apply Lien Maturity Fees.", "No Eligible Accounts - " + FCConvert.ToString(lngRK), MessageBoxButtons.OK, MessageBoxIcon.Information);
					return FillDemandGrid;
				}
				lngError = 5;
				while (!rsData.EndOfFile())
				{
					// find the first lien record
					lngError = 11;
					//Application.DoEvents();
					rsLData.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
					if (rsLData.NoMatch)
					{
						// if there is no match, then report the error
						lngError = 12;
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Cannot find Lien Record #" + rsData.Get_Fields(strWS + "LienRecordNumber") + ".", "Missing Lien Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
						if (FCConvert.ToInt32(rsData.Get_Fields("Copies")) == 0)
						{
							//TODO
							//goto SKIP;
						}
						else
						{
							rsMaster.FindFirstRecord("ID", rsData.Get_Fields_Int32("AccountKey"));
							if (rsMaster.NoMatch)
							{
								lngError = 12;
								frmWait.InstancePtr.Unload();
								MessageBox.Show("Cannot find Master Record for account #" + rsData.Get_Fields_Int32("AccountKey") + ".", "Missing Master Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
							else
							{
								// MAL@20080314: Change to use dictionary object to store accounts used rather than last account key
								// Tracker Reference: 12626
								if (objDictionary.ContainsKey(rsMaster.Get_Fields_Int32("ID")))
								{
									// If rsMaster.Fields("Key") = lngLastAccountKey Then
									goto SKIP;
								}
								else
								{
									// MAL@20080314
									// lngLastAccountKey = rsMaster.Fields("Key")
									objDictionary.Add(rsMaster.Get_Fields_Int32("ID"), rsMaster.Get_Fields_Int32("ID"));
									if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("InBankruptcy")))
									{
										// this account is not eligible for LMFs
										goto SKIP;
									}
									lngError = 13;
									// MAL@20080422: Check that account has not been paid off
									// Tracker Reference: 12837
									rsTemp.OpenRecordset("SELECT * FROM Lien WHERE ID = " + rsData.Get_Fields(strWS + "LienRecordNumber"), modExtraModules.strUTDatabase);
									dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsTemp, DateTime.Today, ref dblXInt, FCConvert.CBool(strWS == "W"));
									// kk01292015 This date is an outgoing parameter   ', , , , , , rsTemp.Fields("IntPaidDate"))
									if (dblTotalDue <= 0)
									{
										// get the next record
										goto SKIP;
									}
									else
									{
										vsDemand.AddItem("");
										Array.Resize(ref modUTLien.Statics.arrDemand, vsDemand.Rows - 1 + 1);
										// this will make sure that there are enough elements to use
										lngIndex = vsDemand.Rows - 2;
										modUTLien.Statics.arrDemand[lngIndex].Used = true;
										modUTLien.Statics.arrDemand[lngIndex].Processed = false;
										vsDemand.TextMatrix(vsDemand.Rows - 1, 1, FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"))));
										// account number
										modUTLien.Statics.arrDemand[lngIndex].Account = modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"));
										vsDemand.TextMatrix(vsDemand.Rows - 1, 2, FCConvert.ToString(rsData.Get_Fields_String("OName")));
										// name
										modUTLien.Statics.arrDemand[lngIndex].Name = FCConvert.ToString(rsData.Get_Fields_String("OName"));
										// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
										vsDemand.TextMatrix(vsDemand.Rows - 1, 3, FCConvert.ToString(rsData.Get_Fields("Copies")));
										// number of copies
										// amount
										// If rsLData.Fields("Costs") = 0 Then
										// highlight the row and set the value = zero
										// .Cell(FCGrid.CellPropertySettings.flexcpForeColor, .rows - 1, 0, .rows - 1, .Cols - 1) = vbRed
										// .TextMatrix(.rows - 1, 4) = 0
										// .TextMatrix(.rows - 1, 6) = -1
										// Else
										// gonna have to figure out the amount
										// If boolChargeCert Then
										// If boolChargeMort Then
										// .TextMatrix(.rows - 1, 4) = (rsData.Fields("Copies") * dblCertMailFee) + dblFilingFee
										// arrDemand(lngIndex).CertifiedMailFee = CDbl(.TextMatrix(.rows - 1, 4))
										// Else    'may have to add the new owner charge here...
										// .TextMatrix(.rows - 1, 4) = dblCertMailFee + dblFilingFee
										// arrDemand(lngIndex).CertifiedMailFee = dblCertMailFee
										// End If
										// Else
										// .TextMatrix(.rows - 1, 4) = dblFilingFee
										// arrDemand(lngIndex).CertifiedMailFee = 0
										// End If
										if (boolChargeCert)
										{
											if (boolChargeMort)
											{
												if (boolChargeForNewOwner)
												{
													// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
													vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString((rsData.Get_Fields("Copies") * dblCertMailFee) + dblFilingFee));
												}
												else
												{
													bool boolUTMatch = false;
                                                    //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
													//if (boolSendToNewOwner && modMain.NewOwnerUT(ref rsData, ref rsMaster, ref boolUTMatch))
													if (boolSendToNewOwner & modMain.NewOwnerUT(ref rsData, ref rsMaster, ref boolUTMatch))
													{
														// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
														vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee));
													}
													else
													{
														// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
														vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString((rsData.Get_Fields("Copies") * dblCertMailFee) + dblFilingFee));
													}
												}
											}
											else
											{
												// may have to add the new owner charge here...
												bool boolUTMatch = false;
                                                //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
												//if (boolChargeForNewOwner && modMain.NewOwnerUT(ref rsData, ref rsMaster, ref boolUTMatch))
												if (boolChargeForNewOwner & modMain.NewOwnerUT(ref rsData, ref rsMaster, ref boolUTMatch))
												{
													vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString((dblCertMailFee * 2) + dblFilingFee));
												}
												else
												{
													vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblCertMailFee + dblFilingFee));
												}
											}
										}
										else
										{
											if (boolChargeMort)
											{
												if (boolChargeForNewOwner)
												{
													// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
													vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee));
												}
												else
												{
													bool boolUTMatch = false;
                                                    //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
													//if (boolSendToNewOwner && modMain.NewOwnerUT(ref rsData, ref rsMaster, ref boolUTMatch))
													if (boolSendToNewOwner & modMain.NewOwnerUT(ref rsData, ref rsMaster, ref boolUTMatch))
													{
														// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
														vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(((rsData.Get_Fields("Copies") - 2) * dblCertMailFee) + dblFilingFee));
													}
													else
													{
														// TODO Get_Fields: Check the table for the column [Copies] and replace with corresponding Get_Field method
														vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(((rsData.Get_Fields("Copies") - 1) * dblCertMailFee) + dblFilingFee));
													}
												}
											}
											else
											{
												bool boolUTMatch = false;
                                                //FC:FINAL:MSH - use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original) (same with issue #1627)
												//if (boolChargeForNewOwner && modMain.NewOwnerUT(ref rsData, ref rsMaster, ref boolUTMatch))
												if (boolChargeForNewOwner & modMain.NewOwnerUT(ref rsData, ref rsMaster, ref boolUTMatch))
												{
													vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblCertMailFee + dblFilingFee));
												}
												else
												{
													vsDemand.TextMatrix(vsDemand.Rows - 1, 4, FCConvert.ToString(dblFilingFee));
												}
											}
										}
										modUTLien.Statics.arrDemand[lngIndex].CertifiedMailFee = FCConvert.ToDouble(vsDemand.TextMatrix(vsDemand.Rows - 1, 4)) - dblFilingFee;
										modUTLien.Statics.arrDemand[lngIndex].Fee = dblFilingFee;
										// End If
										vsDemand.TextMatrix(vsDemand.Rows - 1, 5, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
										// This has to be Bill.ID  'billkey
										lngError = 14;
									}
								}
							}
							SKIP:
							;
							rsData.MoveNext();
						}
					}
				}
				lngError = 9;
				// check all of the accounts
				mnuFileSelectAll_Click();
				lngError = 10;
				// this sets the height of the grid
				if ((vsDemand.Rows * vsDemand.RowHeight(0)) + 70 > fraGrid.Height - vsDemand.Top - 300)
				{
					//vsDemand.Height = fraGrid.Height - vsDemand.Top - 300;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				}
				else
				{
					//vsDemand.Height = (vsDemand.Rows * vsDemand.RowHeight(0)) + 70;
					vsDemand.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				}
				lngError = 11;
				frmWait.InstancePtr.Unload();
				return FillDemandGrid;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				FillDemandGrid = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
			return FillDemandGrid;
		}

		private void FillValidateGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the validate grid with the information from the Control_30DayNotice table
				// which is the information that was used to print the 30 Day Notices the last time
				vsValidate.Rows = 1;
				rsValidate.OpenRecordset("SELECT * FROM " + strWS + "Control_LienMaturity");
				if (rsValidate.EndOfFile())
				{
					MessageBox.Show("Please return to and run 'Print Lien Maturity Notice'.", "No Control Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Unload();
					return;
				}
				else
				{
					if (boolWater)
					{
						vsValidate.AddItem("Billing Type" + "\t" + "Water");
						// .Fields("BillingYear")
					}
					else
					{
						vsValidate.AddItem("Billing Type" + "\t" + "Sewer");
					}
					dtLMFDate = FCConvert.ToDateTime(Strings.Format(rsValidate.Get_Fields_DateTime("MailDate"), "MM/dd/yyyy"));
					vsValidate.AddItem("Mailing Date" + "\t" + Strings.Format(dtLMFDate, "MM/dd/yyyy"));
					vsValidate.AddItem("Filing Date" + "\t" + Strings.Format(rsValidate.Get_Fields_DateTime("LienFilingDate"), "MM/dd/yyyy"));
					// vsValidate.AddItem "Certified Mail Fee" & vbTab & Format(.Fields("CertMailFee"), "#,##0.00")
					dblFilingFee = rsValidate.Get_Fields_Double("ForeclosureFee");
					vsValidate.AddItem("Foreclosure Fee" + "\t" + Strings.Format(dblFilingFee, "#,##0.00"));
					boolChargeCert = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("PayCertMailFee"));
					boolChargeMort = FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("ChargeForMortHolder"));
					if (boolChargeCert)
					{
						vsValidate.AddItem("Certified Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
						dblCertMailFee = rsValidate.Get_Fields_Double("CertMailFee");
						vsValidate.AddItem("Charge for Cert Mail Fee?" + "\t" + "Yes");
					}
					else
					{
						if (boolChargeMort)
						{
							vsValidate.AddItem("Certified Mail Fee" + "\t" + Strings.Format(rsValidate.Get_Fields_Double("CertMailFee"), "#,##0.00"));
						}
						else
						{
							vsValidate.AddItem("Certified Mail Fee" + "\t" + "0.00");
						}
						vsValidate.AddItem("Charge for Cert Mail Fee?" + "\t" + "No");
						dblCertMailFee = 0;
					}
					if (FCConvert.ToBoolean(rsValidate.Get_Fields_Boolean("SendCopyToMortHolder")))
					{
						if (boolChargeMort)
						{
							vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "Yes");
						}
						else
						{
							vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
						}
					}
					else
					{
						vsValidate.AddItem("Charge for each mortgage holder?" + "\t" + "No");
					}
					// If .Fields("SendCopyToNewOwner") <> "" Then
					// If Left(.Fields("SendCopyToNewOwner"), 2) = "Ye" Then
					// vsValidate.AddItem "Send to New Owner" & vbTab & "Yes"
					// Else
					// vsValidate.AddItem "Send to New Owner" & vbTab & "No"
					// End If
					// Else
					// vsValidate.AddItem "Send to New Owner" & vbTab & "No"
					// End If
					if (FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")) != "")
					{
						if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 2) == "Ye")
						{
							if (Strings.Left(FCConvert.ToString(rsValidate.Get_Fields_String("SendCopyToNewOwner")), 8) == "Yes, cha")
							{
								vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, charge CMF.");
								boolChargeForNewOwner = true;
								boolSendToNewOwner = true;
							}
							else
							{
								vsValidate.AddItem("Send to New Owner" + "\t" + "Yes, with no charge.");
								boolChargeForNewOwner = false;
								boolSendToNewOwner = true;
							}
						}
						else
						{
							vsValidate.AddItem("Send to New Owner" + "\t" + "No");
							boolSendToNewOwner = false;
						}
					}
					else
					{
						vsValidate.AddItem("Send to New Owner" + "\t" + "No");
						boolSendToNewOwner = false;
					}
					// gboolUseMailDateForMaturity = CBool(.Fields("UseMailDateForMaturity"))
				}
				// this will set the default date
				txtIntDate.Text = FCConvert.ToString(dtLMFDate);
				//FC:FINAL:DDU:#1175 - set grid  height to be variable based on # of rows
				// set the height of the grid
				//if (vsValidate.Rows * vsValidate.RowHeight(0) > fraValidate.Height - 300)
				//{
				//    //vsValidate.Height = fraValidate.Height - 300;
				//    vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				//}
				//else
				//{
				//    //vsValidate.Height = vsValidate.Rows * vsValidate.RowHeight(0) + 70;
				//    vsValidate.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				//}
				vsValidate.Height = vsValidate.Rows * 40 - 8;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Filling Validation Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetAct_2(short intAct)
		{
			SetAct(ref intAct);
		}

		private void SetAct(ref short intAct)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will change all of the menu options
				switch (intAct)
				{
					case 0:
						{
							ShowValidateFrame();
							fraGrid.Visible = false;
							mnuFileSave.Text = "Accept Parameters";
							cmdFileSave.Text = "Accept Parameters";
							cmdFileSave.Width = 175;
							mnuFileSelectAll.Visible = false;
							intAction = 0;
							break;
						}
					case 1:
						{
							ShowGridFrame();
							fraValidate.Visible = false;
							mnuFileSelectAll.Visible = true;
							mnuFileSave.Text = "Add Maturity Fees";
							cmdFileSave.Text = "Add Maturity Fees";
							cmdFileSave.Width = 168;
							intAction = 1;
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Set Action", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			switch (intAction)
			{
				case 0:
					{
						// this will say that the user accepts the parameters shown
						SetAct_2(1);
						break;
					}
				case 1:
					{
						// Apply the Lien Maturity Fees for the accounts selected
						if (ApplyMaturityFees())
						{
							this.Unload();
						}
						return;
					}
			}
			//end switch
		}

		private bool ApplyMaturityFees()
		{
			bool ApplyMaturityFees = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				int lngDemandCount = 0;
				ApplyMaturityFees = true;
				if (chkIntApplied.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (Information.IsDate(txtIntDate.Text))
					{
						if (DateAndTime.DateDiff("D", DateAndTime.DateValue(txtIntDate.Text), dtLMFDate) <= 0)
						{
							// date is after or same as the LMF date
							// is ok...keep rockin
						}
						else
						{
							// date before LMF Date
							MessageBox.Show("Interest Applied Through Date is before the the Lien Maturity Fee Date of " + FCConvert.ToString(dtLMFDate) + ".  Please enter a date that is the same or later than the Lien Maturity Fee Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							ApplyMaturityFees = false;
							return ApplyMaturityFees;
						}
					}
					else
					{
						MessageBox.Show("Invalid Interest Applied Through Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						ApplyMaturityFees = false;
						return ApplyMaturityFees;
					}
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Applying Fees");
				for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
				{
					// for each account in the grid
					//Application.DoEvents();
					if (Conversion.Val(vsDemand.TextMatrix(lngCT, 0)) == -1)
					{
						// check to see if the check box if checked
						if (Conversion.Val(vsDemand.TextMatrix(lngCT, 6)) >= 0)
						{
							// then find the bill record
							rsData.FindFirstRecord("ID", vsDemand.TextMatrix(lngCT, 5));
							// this has to be Bill.ID
							if (rsData.NoMatch)
							{
								MessageBox.Show("Error processing account " + vsDemand.TextMatrix(lngCT, 1) + ".  No lien record was found.", "Cannot Find BillKey - " + vsDemand.TextMatrix(lngCT, 5), MessageBoxButtons.OK, MessageBoxIcon.Hand);
							}
							else
							{
								modUTLien.Statics.arrDemand[lngCT - 1].Processed = true;
								CreateFeeRecord_2(FCConvert.ToDouble(vsDemand.TextMatrix(lngCT, 4)));
								lngDemandCount += 1;
							}
						}
						else
						{
							// not eligible...do nothing
						}
					}
					else
					{
						// not selected...do nothing
					}
				}
				if (boolWater)
				{
					modMain.SetRateKeyDates_8("W", "M", strRateKeys);
				}
				else
				{
					modMain.SetRateKeyDates_8("S", "M", strRateKeys);
				}
				frmWait.InstancePtr.Unload();
				MessageBox.Show(FCConvert.ToString(lngDemandCount) + " accounts were affected.", "Liened Accounts Added", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// If lngDemandCount > 0 Then
				// print the list of accounts that have been affected
				rptLienMaturityFees.InstancePtr.Init(ref lngDemandCount, ref dblFilingFee, ref boolWater);
				frmReportViewer.InstancePtr.Init(rptLienMaturityFees.InstancePtr);
				// End If
				return ApplyMaturityFees;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Applying Fees", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ApplyMaturityFees;
		}

		private void mnuFileSelectAll_Click(object sender, System.EventArgs e)
		{
			// this will select all account in the grid
			int lngCT;
			for (lngCT = 1; lngCT <= vsDemand.Rows - 1; lngCT++)
			{
				vsDemand.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
			}
		}

		public void mnuFileSelectAll_Click()
		{
			mnuFileSelectAll_Click(mnuFileSelectAll, new System.EventArgs());
		}

		private int CreateFeeRecord_2(double dblTotalFee)
		{
			return CreateFeeRecord(ref dblTotalFee);
		}

		private int CreateFeeRecord(ref double dblTotalFee)
		{
			int CreateFeeRecord = 0;
			// this will calculate the CHGINT line for the account and then return
			// the CHGINT number to store in the payment record
			// this will use rsData which is set to the correct record
			double dblTotalDue = 0;
			double dblInt = 0;
			double dblFee = 0;
			clsDRWrapper rsFee = new clsDRWrapper();
			rsLData.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
			if (rsLData.NoMatch)
			{
				// no match for the lien record...do not process the account
			}
			else
            {
                var correlationID = Guid.NewGuid();
                var transactionID = Guid.NewGuid();
				// calculate the current interest to this date
				dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLData, dtLMFDate, ref dblInt, boolWater);
				if (dblTotalDue > 0)
				{
					// if there is interest due, then
					// this will actually create the payment line
					rsFee.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strUTDatabase);
					rsFee.AddNew();
					rsFee.Set_Fields("AccountKey", rsData.Get_Fields_Int32("AccountKey"));
					// .Fields("Year") = rsData.Fields("BillingYear")
					rsFee.Set_Fields("BillKey", rsData.Get_Fields(strWS + "LienRecordNumber"));
					// kgk  rsData.Fields("LienKey")
                    var interestId = AddCHGINTForFee(ref dblInt, transactionID, correlationID);
                    rsFee.Set_Fields("CHGINTNumber", interestId);
					rsFee.Set_Fields("CHGINTDate", dtLMFDate);
					rsFee.Set_Fields("ActualSystemDate", DateTime.Today);
					rsFee.Set_Fields("EffectiveInterestDate", dtLMFDate);
					rsFee.Set_Fields("RecordedTransactionDate", dtLMFDate);
					rsFee.Set_Fields("Teller", Strings.UCase(Strings.Left(modGlobalConstants.Statics.gstrUserID, 3)));
					rsFee.Set_Fields("Reference", "FCFEES");
					rsFee.Set_Fields("Period", "A");
					rsFee.Set_Fields("Code", "L");
					rsFee.Set_Fields("ReceiptNumber", 0);
					rsFee.Set_Fields("Principal", 0);
					rsFee.Set_Fields("PreLienInterest", 0);
					rsFee.Set_Fields("CurrentInterest", 0);
					dblFee = dblTotalFee;
					rsFee.Set_Fields("LienCost", dblFee * -1);
					// this has to be negative so that the status screen knows that it is charged not paid
					rsFee.Set_Fields("TransNumber", 0);
					rsFee.Set_Fields("PaidBy", "Automatic/Computer");
					rsFee.Set_Fields("Comments", "Lien Maturity Fee");
					rsFee.Set_Fields("CashDrawer", "N");
					rsFee.Set_Fields("GeneralLedger", "N");
					rsFee.Set_Fields("BudgetaryAccountNumber", "");
					rsFee.Set_Fields("Service", strWS);
					rsFee.Set_Fields("Lien", true);
					rsFee.Set_Fields("DailyCloseOut", false);
                    rsFee.Set_Fields("CorrelationIdentifier",correlationID);
                    rsFee.Set_Fields("TransactionIdentifier",transactionID);
					rsFee.Update();
					// this will edit the Lien Record
					rsLData.Edit();
					// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
					rsLData.Set_Fields("IntAdded", (rsLData.Get_Fields("IntAdded") - dblInt));
					// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
					rsLData.Set_Fields("MaturityFee", (rsLData.Get_Fields("MaturityFee") - dblFee));
					if (chkIntApplied.CheckState == Wisej.Web.CheckState.Checked)
					{
						if (Information.IsDate(txtIntDate.Text))
						{
							// add a CYA entry for every Lien with a different Int PD THR date
							modGlobalFunctions.AddCYAEntry_62("CL", "Changing Interest Applied Through Date on Lien Maturity Notice.", txtIntDate.Text, FCConvert.ToString(rsLData.Get_Fields_Int32("LienRecordNumber")));
							// allow the interest date that was entered instead of just updating the InterestAppliedThroughDate
							rsLData.Set_Fields("IntPaidDate", DateAndTime.DateValue(txtIntDate.Text));
						}
						else
						{
							rsLData.Set_Fields("IntPaidDate", dtLMFDate);
						}
					}
					else
					{
						rsLData.Set_Fields("IntPaidDate", dtLMFDate);
					}
					rsLData.Update();
					// this will edit the Lien Record
					rsFee.OpenRecordset("SELECT * FROM Bill WHERE " + strWS + "CombinationLienKey = " + rsData.Get_Fields(strWS + "CombinationLienKey"));
					while (!rsFee.EndOfFile())
					{
						rsFee.Edit();
						// .Fields("InterestAppliedThroughDate") = dtLMFDate
						rsFee.Set_Fields(strWS + "LienProcessStatus", 6);
						// set the status to 'Lien Maturity Fees Applied'
						rsFee.Update();
						rsFee.MoveNext();
					}
				}
			}
			return CreateFeeRecord;
		}

		private void vsDemand_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsDemand[e.ColumnIndex, e.RowIndex];
            int lngMR;
			int lngMC;
			lngMR = vsDemand.GetFlexRowIndex(e.RowIndex);
			lngMC = vsDemand.GetFlexColIndex(e.ColumnIndex);
			if (lngMR > 0 && lngMC > 0)
			{
				if (Conversion.Val(vsDemand.TextMatrix(lngMR, 6)) == -1)
				{
					// vsDemand.ToolTipText = "A Lien Record has already been created for this account.  This account is not eligible."
				}
				else
				{
                    //ToolTip1.SetToolTip(vsDemand, "");
                    cell.ToolTipText = "";
				}
			}
		}

		private void vsDemand_RowColChange(object sender, System.EventArgs e)
		{
			switch (vsDemand.Col)
			{
				case 0:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						break;
					}
				default:
					{
						vsDemand.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
			}
			//end switch
		}

		private int AddCHGINTForFee(ref double dblCurInt, Guid transactionId, Guid correlationId)
		{
			// this will calculate the CHGINT line for the account and then return
			// the CHGINT number to store in the payment record
			// this will use rsData which is set to the correct record
			clsDRWrapper rsCHGINT = new clsDRWrapper();
            if (dblCurInt > 0)
			{
                // if there is interest due, then
                // this will actually create the payment line
                rsCHGINT.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strUTDatabase);
				rsCHGINT.AddNew();
				rsCHGINT.Set_Fields("AccountKey", rsData.Get_Fields_Int32("AccountKey"));
				// .Fields("Year") = rsData.Fields("BillingYear")
				rsCHGINT.Set_Fields("BillKey", rsData.Get_Fields(strWS + "LienRecordNumber"));
				// kgk   rsData.Fields("LienKey")
				rsCHGINT.Set_Fields("CHGINTNumber", 0);
				rsCHGINT.Set_Fields("CHGINTDate", rsLData.Get_Fields_DateTime("IntPaidDate"));
				rsCHGINT.Set_Fields("ActualSystemDate", DateTime.Today);
				rsCHGINT.Set_Fields("EffectiveInterestDate", dtLMFDate);
				rsCHGINT.Set_Fields("RecordedTransactionDate", dtLMFDate);
				rsCHGINT.Set_Fields("Teller", "");
				rsCHGINT.Set_Fields("Reference", "CHGINT");
				rsCHGINT.Set_Fields("Period", "A");
				rsCHGINT.Set_Fields("Code", "I");
				rsCHGINT.Set_Fields("ReceiptNumber", 0);
				rsCHGINT.Set_Fields("Principal", 0);
				rsCHGINT.Set_Fields("PreLienInterest", 0);
				rsCHGINT.Set_Fields("CurrentInterest", dblCurInt * -1);
				rsCHGINT.Set_Fields("LienCost", 0);
				rsCHGINT.Set_Fields("TransNumber", 0);
				rsCHGINT.Set_Fields("PaidBy", 0);
				rsCHGINT.Set_Fields("Comments", "Demand Fees");
				rsCHGINT.Set_Fields("CashDrawer", "N");
				rsCHGINT.Set_Fields("GeneralLedger", "Y");
				rsCHGINT.Set_Fields("BudgetaryAccountNumber", "");
				rsCHGINT.Set_Fields("Service", strWS);
				rsCHGINT.Set_Fields("Lien", true);                
                rsCHGINT.Set_Fields("CorrelationIdentifier", correlationId);
                rsCHGINT.Set_Fields("TransactionIdentifier", transactionId);
                rsCHGINT.Update();				
			}

            return rsCHGINT.Get_Fields_Int32("ID");
        }
	}
}
