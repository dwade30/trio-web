﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arDuplicateSequence.
	/// </summary>
	public partial class arDuplicateSequence : BaseSectionReport
	{
		public arDuplicateSequence()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Meter Count Summary";
		}

		public static arDuplicateSequence InstancePtr
		{
			get
			{
				return (arDuplicateSequence)Sys.GetInstance(typeof(arDuplicateSequence));
			}
		}

		protected arDuplicateSequence _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arDuplicateSequence	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/08/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/08/2004              *
		// ********************************************************
		string[] arrSeq = null;
		// This array will fill will sequence numbers that have duplicate
		bool boolDone;
		int lngIndex;
		int lngTotDup;
		int lngLastSeq;

		public void Init(ref string strPassList, bool modalDialog)
		{
			if (Strings.Trim(strPassList) != "")
			{
				arrSeq = Strings.Split(strPassList, ",", -1, CompareConstants.vbBinaryCompare);
				frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
				lngTotDup = 1;
				// MAL@20070905: Start count at 1 to get accurate duplicate count
			}
			else
			{
				this.Close();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (frmReportViewer.InstancePtr.Visible)
			{
				frmReportViewer.InstancePtr.Focus();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			FillHeaderLabels();
			lngLastSeq = -1;
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
			// show the current index values
			lngIndex += 1;
			// increment the pointer
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsData = new clsDRWrapper();
				int lngAcct = 0;
				int lngBook = 0;
				int lngSeq = 0;
				int lngMK = 0;
				int lngMNum = 0;
				string strLocation = "";
				if (lngIndex < Information.UBound(arrSeq, 1))
				{
					lngMK = FCConvert.ToInt32(Math.Round(Conversion.Val(arrSeq[lngIndex])));
					if (lngMK > 0)
					{
						rsData.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(lngMK), modExtraModules.strUTDatabase);
						if (!rsData.EndOfFile())
						{
							lngBook = FCConvert.ToInt32(rsData.Get_Fields_Int32("BookNumber"));
							// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
							lngSeq = FCConvert.ToInt32(rsData.Get_Fields("Sequence"));
							if (lngLastSeq == lngSeq)
							{
								lngTotDup += 1;
							}
							lngLastSeq = lngSeq;
							lngAcct = modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"));
							lngMNum = FCConvert.ToInt32(rsData.Get_Fields_Int32("MeterNumber"));
							strLocation = FCConvert.ToString(rsData.Get_Fields_String("Location"));
							// If Val(rsData.Fields("StreetNumber")) <> 0 Then
							// strLocation = Val(rsData.Fields("StreetNumber")) & " " & rsData.Fields("StreetName")
							// Else
							// strLocation = rsData.Fields("StreetName")
							// End If
							fldBook.Text = lngBook.ToString();
							fldSeq.Text = lngSeq.ToString();
							fldAccount.Text = lngAcct.ToString();
							fldMeterNumber.Text = lngMNum.ToString();
							fldLocation.Text = strLocation;
						}
						else
						{
							// clear the fields
							fldBook.Text = "";
							fldSeq.Text = "";
							fldAccount.Text = "";
							fldMeterNumber.Text = "";
							fldLocation.Text = "";
							Detail.Height = 0;
						}
					}
					else
					{
						// clear the fields
						fldBook.Text = "";
						fldSeq.Text = "";
						fldAccount.Text = "";
						fldMeterNumber.Text = "";
						fldLocation.Text = "";
						Detail.Height = 0;
					}
				}
				else
				{
					// clear the fields
					boolDone = true;
					// this will end the report
					fldBook.Text = "";
					fldSeq.Text = "";
					fldAccount.Text = "";
					fldMeterNumber.Text = "";
					fldLocation.Text = "";
					Detail.Height = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldFooter.Text = "There were " + FCConvert.ToString(lngTotDup) + " duplicate meters.";
		}

		private void arDuplicateSequence_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arDuplicateSequence properties;
			//arDuplicateSequence.Caption	= "Meter Count Summary";
			//arDuplicateSequence.Icon	= "arDuplicateSequence.dsx":0000";
			//arDuplicateSequence.Left	= 0;
			//arDuplicateSequence.Top	= 0;
			//arDuplicateSequence.Width	= 15240;
			//arDuplicateSequence.Height	= 11115;
			//arDuplicateSequence.WindowState	= 2;
			//arDuplicateSequence.SectionData	= "arDuplicateSequence.dsx":058A;
			//End Unmaped Properties
		}
	}
}
