﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCustomLisbonConsumption.
	/// </summary>
	public partial class frmCustomLisbonConsumption : BaseForm
	{
		public frmCustomLisbonConsumption()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCustomLisbonConsumption InstancePtr
		{
			get
			{
				return (frmCustomLisbonConsumption)Sys.GetInstance(typeof(frmCustomLisbonConsumption));
			}
		}

		protected frmCustomLisbonConsumption _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmCustomLisbonConsumption_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCustomLisbonConsumption_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCustomLisbonConsumption properties;
			//frmCustomLisbonConsumption.FillStyle	= 0;
			//frmCustomLisbonConsumption.ScaleWidth	= 3885;
			//frmCustomLisbonConsumption.ScaleHeight	= 2145;
			//frmCustomLisbonConsumption.LinkTopic	= "Form2";
			//frmCustomLisbonConsumption.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			FillBooksCombo();
			if (cboBooks.Items.Count > 0)
			{
				cboBooks.SelectedIndex = 0;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCustomLisbonConsumption_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void FillBooksCombo()
		{
			clsDRWrapper rs = new clsDRWrapper();
			cboBooks.Clear();
			rs.OpenRecordset("SELECT * FROM Book WHERE CurrentStatus = 'DE' ORDER BY BookNumber");
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				do
				{
					cboBooks.AddItem(Strings.Format(rs.Get_Fields_Int32("BookNumber"), "000") + " - " + rs.Get_Fields_String("Description"));
					cboBooks.ItemData(cboBooks.NewIndex, FCConvert.ToInt32(rs.Get_Fields_Int32("BookNumber")));
					rs.MoveNext();
				}
				while (rs.EndOfFile() != true);
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsConsumptionInfo = new clsDRWrapper();
			clsDRWrapper rsInfo = new clsDRWrapper();
			// vbPorter upgrade warning: lngCurrentConsumption As int	OnWrite(int, double)
			int lngCurrentConsumption = 0;
			int[] lngReading = new int[1 + 1];
			int counter;
			int lngPrev = 0;
			int lngCurr = 0;
			int lngAdj = 0;
			int lngDigits = 0;
			int intReadings = 0;
			// vbPorter upgrade warning: lngAverage As int	OnWriteFCConvert.ToDouble(
			int lngAverage = 0;
			int lngHighConsumption;
			bool blnHasCurrent;
			// MAL@20071022
			DateTime dtCurrReadDate;
			clsDRWrapper rsBill = new clsDRWrapper();
			DateTime datCheckDate;
			FCUtils.EraseSafe(modCustomPrograms.Statics.cciInfo);
			modCustomPrograms.Statics.lngAcctCounter = 0;
			rsInfo.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(cboBooks.ItemData(cboBooks.SelectedIndex)));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				modCustomPrograms.Statics.lngAcctCounter = 1;
				do
				{
					for (counter = 0; counter <= 1; counter++)
					{
						lngReading[counter] = 0;
					}
					intReadings = 0;
					counter = 0;
					lngCurrentConsumption = 0;
					if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("CurrentReading")) != -1)
					{
						// kk 110812 trout-884  Change from 0 to -1 for No Read
						lngPrev = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("PreviousReading"));
						lngCurr = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("CurrentReading"));
						lngDigits = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("Digits"));
						// MAL@20071022: Added to track current reading date for later use
						blnHasCurrent = true;
						dtCurrReadDate = (DateTime)rsInfo.Get_Fields_DateTime("CurrentReadingDate");
						if (lngCurr >= lngPrev)
						{
							lngCurrentConsumption = lngCurr - lngPrev;
						}
						else
						{
							switch (modExtraModules.Statics.glngTownReadingUnits)
							{
								case 100000:
									{
										lngAdj = 5;
										break;
									}
								case 10000:
									{
										lngAdj = 4;
										break;
									}
								case 1000:
									{
										lngAdj = 3;
										break;
									}
								case 100:
									{
										lngAdj = 2;
										break;
									}
								case 10:
									{
										lngAdj = 1;
										break;
									}
								case 1:
									{
										lngAdj = 0;
										break;
									}
								default:
									{
										lngAdj = 0;
										break;
									}
							}
							//end switch
							// this must be a rolled over meter
							lngCurrentConsumption = FCConvert.ToInt32(lngCurr + ((Math.Pow(10, (lngDigits - lngAdj))) - lngPrev));
						}
					}
					rsConsumptionInfo.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsInfo.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsInfo.Get_Fields_Int32("MeterKey") + " AND WinterBill = true ORDER BY BillDate DESC");
					if (rsConsumptionInfo.EndOfFile() != true && rsConsumptionInfo.BeginningOfFile() != true)
					{
						datCheckDate = (DateTime)rsConsumptionInfo.Get_Fields_DateTime("BillDate");
						do
						{
							if (counter < 2)
							{
								if (Math.Abs(DateAndTime.DateDiff("m", datCheckDate, (DateTime)rsConsumptionInfo.Get_Fields_DateTime("BillDate"))) > 6)
								{
									break;
								}
								else
								{
									datCheckDate = (DateTime)rsConsumptionInfo.Get_Fields_DateTime("BillDate");
									lngReading[counter] = FCConvert.ToInt32(rsConsumptionInfo.Get_Fields_Int32("Consumption"));
									counter += 1;
									intReadings += 1;
								}
							}
							else
							{
								break;
							}
							rsConsumptionInfo.MoveNext();
						}
						while (rsConsumptionInfo.EndOfFile() != true);
					}
					if (intReadings == 2)
					{
						lngAverage = FCConvert.ToInt32((lngReading[0] + lngReading[1]) / 2.0);
						if (lngAverage < lngCurrentConsumption)
						{
							rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsInfo.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsInfo.Get_Fields_Int32("MeterKey") + " AND BillStatus = 'D'");
							if (rsBill.EndOfFile() != true && rsBill.BeginningOfFile() != true)
							{
								rsBill.Edit();
								rsBill.Set_Fields("SewerOverrideCons", lngAverage);
								rsBill.Set_Fields("SHasOverride", true);
								rsBill.Update();
								Array.Resize(ref modCustomPrograms.Statics.cciInfo, modCustomPrograms.Statics.lngAcctCounter - 1 + 1);
								modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngAccountKey = rsInfo.Get_Fields_Int32("AccountKey");
								modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngMeterKey = rsInfo.Get_Fields_Int32("MeterKey");
								modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngConsumptionOverride = lngAverage;
							}
							else
							{
								Array.Resize(ref modCustomPrograms.Statics.cciInfo, modCustomPrograms.Statics.lngAcctCounter - 1 + 1);
								modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngAccountKey = rsInfo.Get_Fields_Int32("AccountKey");
								modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngMeterKey = rsInfo.Get_Fields_Int32("MeterKey");
								modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngConsumptionOverride = -1;
							}
						}
						else
						{
							Array.Resize(ref modCustomPrograms.Statics.cciInfo, modCustomPrograms.Statics.lngAcctCounter - 1 + 1);
							modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngAccountKey = rsInfo.Get_Fields_Int32("AccountKey");
							modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngMeterKey = rsInfo.Get_Fields_Int32("MeterKey");
							modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngConsumptionOverride = -1;
						}
					}
					else
					{
						Array.Resize(ref modCustomPrograms.Statics.cciInfo, modCustomPrograms.Statics.lngAcctCounter - 1 + 1);
						modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngAccountKey = rsInfo.Get_Fields_Int32("AccountKey");
						modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngMeterKey = rsInfo.Get_Fields_Int32("MeterKey");
						modCustomPrograms.Statics.cciInfo[modCustomPrograms.Statics.lngAcctCounter - 1].lngConsumptionOverride = -1;
					}
					modCustomPrograms.Statics.lngAcctCounter += 1;
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				modCustomPrograms.Statics.lngAcctCounter -= 1;
			}
			if (modCustomPrograms.Statics.lngAcctCounter > 0)
			{
				frmReportViewer.InstancePtr.Init(rptCustomConsumptionCalculationReport.InstancePtr);
			}
			MessageBox.Show("Consumption Overrides Set", "Calculations Completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
	}
}
