﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptConsumptionHistoryComplete.
	/// </summary>
	partial class rptConsumptionHistoryComplete
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptConsumptionHistoryComplete));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldConsumption = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblOptions = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBooksRateKeys = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCountHeading = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.fldFinalConsumptionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalBillAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFinalCountLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblTitle2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGroupConsumptionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGroupBillAmountTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblGroupTotalsLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblGroupCountLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOptions)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBooksRateKeys)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCountHeading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalConsumptionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalBillAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalCountLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupConsumptionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupBillAmountTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupTotalsLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupCountLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldName,
				this.fldBillDate,
				this.fldSequence,
				this.fldConsumption,
				this.fldBillAmount,
				this.fldBook
			});
			this.Detail.Height = 0.19F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.19F;
			this.fldAccount.Left = 1.125F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldAccount.Text = "Field1";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.875F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.19F;
			this.fldName.Left = 2.875F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 1";
			this.fldName.Text = "Field2";
			this.fldName.Top = 0F;
			this.fldName.Width = 2.1875F;
			// 
			// fldBillDate
			// 
			this.fldBillDate.Height = 0.19F;
			this.fldBillDate.Left = 2.0625F;
			this.fldBillDate.Name = "fldBillDate";
			this.fldBillDate.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.fldBillDate.Text = "Field1";
			this.fldBillDate.Top = 0F;
			this.fldBillDate.Width = 0.75F;
			// 
			// fldSequence
			// 
			this.fldSequence.Height = 0.19F;
			this.fldSequence.Left = 0.625F;
			this.fldSequence.Name = "fldSequence";
			this.fldSequence.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldSequence.Text = "Field1";
			this.fldSequence.Top = 0F;
			this.fldSequence.Width = 0.5F;
			// 
			// fldConsumption
			// 
			this.fldConsumption.Height = 0.19F;
			this.fldConsumption.Left = 5.125F;
			this.fldConsumption.Name = "fldConsumption";
			this.fldConsumption.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldConsumption.Text = "Field1";
			this.fldConsumption.Top = 0F;
			this.fldConsumption.Width = 0.84375F;
			// 
			// fldBillAmount
			// 
			this.fldBillAmount.Height = 0.19F;
			this.fldBillAmount.Left = 6.062F;
			this.fldBillAmount.Name = "fldBillAmount";
			this.fldBillAmount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.fldBillAmount.Text = "Field1";
			this.fldBillAmount.Top = 0F;
			this.fldBillAmount.Width = 0.9379997F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.19F;
			this.fldBook.Left = 0.125F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.fldBook.Text = "Field1";
			this.fldBook.Top = 0F;
			this.fldBook.Width = 0.5F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblAccount,
				this.Line1,
				this.Label8,
				this.Label9,
				this.Label14,
				this.Label15,
				this.Label16,
				this.lblOptions,
				this.Label1,
				this.lblBooksRateKeys,
				this.lblCountHeading,
				this.Label20
			});
			this.PageHeader.Height = 1.0625F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.6875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 1.1875F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 0.84375F;
			this.lblAccount.Width = 0.625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.03125F;
			this.Line1.Width = 8F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 8F;
			this.Line1.Y1 = 1.03125F;
			this.Line1.Y2 = 1.03125F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.875F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Label8.Text = "Name";
			this.Label8.Top = 0.84375F;
			this.Label8.Width = 2.1875F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 2.0625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.Label9.Text = "Bill Date";
			this.Label9.Top = 0.84375F;
			this.Label9.Width = 0.75F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 5.125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label14.Text = "Consumption";
			this.Label14.Top = 0.84375F;
			this.Label14.Width = 0.84375F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 6.1875F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label15.Text = "Bill Amount";
			this.Label15.Top = 0.84375F;
			this.Label15.Width = 0.8125F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 0.625F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Label16.Text = "Seq #";
			this.Label16.Top = 0.84375F;
			this.Label16.Width = 0.5F;
			// 
			// lblOptions
			// 
			this.lblOptions.Height = 0.34375F;
			this.lblOptions.HyperLink = null;
			this.lblOptions.Left = 0.375F;
			this.lblOptions.Name = "lblOptions";
			this.lblOptions.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblOptions.Text = "Meter Exchange List";
			this.lblOptions.Top = 0.40625F;
			this.lblOptions.Width = 7.46875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.53125F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Consumption History Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.15625F;
			// 
			// lblBooksRateKeys
			// 
			this.lblBooksRateKeys.Height = 0.1875F;
			this.lblBooksRateKeys.HyperLink = null;
			this.lblBooksRateKeys.Left = 1.53125F;
			this.lblBooksRateKeys.Name = "lblBooksRateKeys";
			this.lblBooksRateKeys.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblBooksRateKeys.Text = "Meter Exchange List";
			this.lblBooksRateKeys.Top = 0.21875F;
			this.lblBooksRateKeys.Width = 5.15625F;
			// 
			// lblCountHeading
			// 
			this.lblCountHeading.Height = 0.1875F;
			this.lblCountHeading.HyperLink = null;
			this.lblCountHeading.Left = 7.15625F;
			this.lblCountHeading.Name = "lblCountHeading";
			this.lblCountHeading.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.lblCountHeading.Text = "Count";
			this.lblCountHeading.Top = 0.84375F;
			this.lblCountHeading.Visible = false;
			this.lblCountHeading.Width = 0.8125F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0.125F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 1";
			this.Label20.Text = "Book";
			this.Label20.Top = 0.84375F;
			this.Label20.Width = 0.5F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldFinalConsumptionTotal,
				this.fldFinalBillAmountTotal,
				this.Line3,
				this.Label19,
				this.fldFinalCount,
				this.lblFinalCountLabel
			});
			this.GroupFooter2.Height = 0.21875F;
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// fldFinalConsumptionTotal
			// 
			this.fldFinalConsumptionTotal.Height = 0.19F;
			this.fldFinalConsumptionTotal.Left = 5.15625F;
			this.fldFinalConsumptionTotal.Name = "fldFinalConsumptionTotal";
			this.fldFinalConsumptionTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldFinalConsumptionTotal.Text = "Field1";
			this.fldFinalConsumptionTotal.Top = 0.03125F;
			this.fldFinalConsumptionTotal.Width = 0.84375F;
			// 
			// fldFinalBillAmountTotal
			// 
			this.fldFinalBillAmountTotal.Height = 0.19F;
			this.fldFinalBillAmountTotal.Left = 6.062F;
			this.fldFinalBillAmountTotal.Name = "fldFinalBillAmountTotal";
			this.fldFinalBillAmountTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldFinalBillAmountTotal.Text = "Field1";
			this.fldFinalBillAmountTotal.Top = 0.03125F;
			this.fldFinalBillAmountTotal.Width = 0.9379997F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4.125F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 2.875F;
			this.Line3.X1 = 4.125F;
			this.Line3.X2 = 7F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 4.03125F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Label19.Text = "Final Totals:";
			this.Label19.Top = 0.03125F;
			this.Label19.Width = 1F;
			// 
			// fldFinalCount
			// 
			this.fldFinalCount.Height = 0.19F;
			this.fldFinalCount.Left = 2.125F;
			this.fldFinalCount.Name = "fldFinalCount";
			this.fldFinalCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldFinalCount.Text = "Field3";
			this.fldFinalCount.Top = 0.03125F;
			this.fldFinalCount.Width = 0.8125F;
			// 
			// lblFinalCountLabel
			// 
			this.lblFinalCountLabel.Height = 0.19F;
			this.lblFinalCountLabel.HyperLink = null;
			this.lblFinalCountLabel.Left = 1.40625F;
			this.lblFinalCountLabel.Name = "lblFinalCountLabel";
			this.lblFinalCountLabel.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left; ddo-c" + "har-set: 1";
			this.lblFinalCountLabel.Text = "Count:";
			this.lblFinalCountLabel.Top = 0.03125F;
			this.lblFinalCountLabel.Width = 0.59375F;
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle,
				this.Binder
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.1666667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			// 
			// lblTitle
			// 
			this.lblTitle.Height = 0.19F;
			this.lblTitle.HyperLink = null;
			this.lblTitle.Left = 0F;
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left; ddo-c" + "har-set: 1";
			this.lblTitle.Text = "Seq #";
			this.lblTitle.Top = 0F;
			this.lblTitle.Width = 4.8125F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.19F;
			this.Binder.Left = 3.25F;
			this.Binder.Name = "Binder";
			this.Binder.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.8125F;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.CanShrink = true;
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTitle2,
				this.fldGroupConsumptionTotal,
				this.fldGroupBillAmountTotal,
				this.Line2,
				this.lblGroupTotalsLabel,
				this.fldCount,
				this.lblGroupCountLabel
			});
			this.GroupFooter1.Height = 0.1979167F;
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// lblTitle2
			// 
			this.lblTitle2.Height = 0.19F;
			this.lblTitle2.HyperLink = null;
			this.lblTitle2.Left = 0F;
			this.lblTitle2.Name = "lblTitle2";
			this.lblTitle2.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left; ddo-c" + "har-set: 1";
			this.lblTitle2.Text = "Seq #";
			this.lblTitle2.Top = 0.03125F;
			this.lblTitle2.Visible = false;
			this.lblTitle2.Width = 4.8125F;
			// 
			// fldGroupConsumptionTotal
			// 
			this.fldGroupConsumptionTotal.Height = 0.19F;
			this.fldGroupConsumptionTotal.Left = 5.15625F;
			this.fldGroupConsumptionTotal.Name = "fldGroupConsumptionTotal";
			this.fldGroupConsumptionTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldGroupConsumptionTotal.Text = "Field1";
			this.fldGroupConsumptionTotal.Top = 0.03125F;
			this.fldGroupConsumptionTotal.Width = 0.84375F;
			// 
			// fldGroupBillAmountTotal
			// 
			this.fldGroupBillAmountTotal.Height = 0.19F;
			this.fldGroupBillAmountTotal.Left = 6.062F;
			this.fldGroupBillAmountTotal.Name = "fldGroupBillAmountTotal";
			this.fldGroupBillAmountTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldGroupBillAmountTotal.Text = "Field1";
			this.fldGroupBillAmountTotal.Top = 0.03125F;
			this.fldGroupBillAmountTotal.Width = 0.9379997F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4.125F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 2.875F;
			this.Line2.X1 = 4.125F;
			this.Line2.X2 = 7F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// lblGroupTotalsLabel
			// 
			this.lblGroupTotalsLabel.Height = 0.19F;
			this.lblGroupTotalsLabel.HyperLink = null;
			this.lblGroupTotalsLabel.Left = 4.4375F;
			this.lblGroupTotalsLabel.Name = "lblGroupTotalsLabel";
			this.lblGroupTotalsLabel.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.lblGroupTotalsLabel.Text = "Totals:";
			this.lblGroupTotalsLabel.Top = 0.03125F;
			this.lblGroupTotalsLabel.Width = 0.59375F;
			// 
			// fldCount
			// 
			this.fldCount.Height = 0.19F;
			this.fldCount.Left = 2.125F;
			this.fldCount.Name = "fldCount";
			this.fldCount.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldCount.Text = "Field3";
			this.fldCount.Top = 0.03125F;
			this.fldCount.Width = 0.8125F;
			// 
			// lblGroupCountLabel
			// 
			this.lblGroupCountLabel.Height = 0.19F;
			this.lblGroupCountLabel.HyperLink = null;
			this.lblGroupCountLabel.Left = 1.40625F;
			this.lblGroupCountLabel.Name = "lblGroupCountLabel";
			this.lblGroupCountLabel.Style = "font-family: \'Tahoma\'; font-size: 9pt; font-weight: bold; text-align: left; ddo-c" + "har-set: 1";
			this.lblGroupCountLabel.Text = "Count:";
			this.lblGroupCountLabel.Top = 0.03125F;
			this.lblGroupCountLabel.Width = 0.59375F;
			// 
			// rptConsumptionHistoryComplete
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8.010417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptConsumptionHistoryComplete_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldConsumption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOptions)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBooksRateKeys)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCountHeading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalConsumptionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalBillAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalCountLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTitle2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupConsumptionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGroupBillAmountTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupTotalsLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblGroupCountLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldConsumption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOptions;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBooksRateKeys;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCountHeading;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalConsumptionTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalBillAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFinalCountLabel;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTitle2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupConsumptionTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGroupBillAmountTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGroupTotalsLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblGroupCountLabel;
	}
}
