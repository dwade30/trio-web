﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTLienDates.
	/// </summary>
	public partial class frmUTLienDates : BaseForm
	{
		public frmUTLienDates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTLienDates InstancePtr
		{
			get
			{
				return (frmUTLienDates)Sys.GetInstance(typeof(frmUTLienDates));
			}
		}

		protected frmUTLienDates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/15/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/15/2005              *
		// ********************************************************
		clsDRWrapper rsSettings = new clsDRWrapper();
		bool boolLoaded;
		public string strWS = "";
		int lngColRateKey;
		int lngColYear;
		int lngColHiddenDueNow;
		int lngColHiddenDue30Days;
		int lngColType;
		int lngColCommitmentDate;
		int lngColDueDate;
		int lngCol30DNDate1;
		int lngCol30DNDate2;
		int lngCOl30DNDate;
		int lngColLien1;
		int lngColLien2;
		int lngColLienDate;
		int lngColMaturityDate;
		int lngColMaturityNotice1;
		int lngColMaturityNotice2;
		int lngColDescription;
		public string strReportHeader = string.Empty;
		// this will be the header for the report
		private void frmUTLienDates_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				LoadSettings();
				boolLoaded = true;
			}
			else
			{
			}
		}

		private void frmUTLienDates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTLienDates properties;
			//frmUTLienDates.FillStyle	= 0;
			//frmUTLienDates.ScaleWidth	= 9300;
			//frmUTLienDates.ScaleHeight	= 7755;
			//frmUTLienDates.LinkTopic	= "Form2";
			//frmUTLienDates.LockControls	= true;
			//frmUTLienDates.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			// set the columns
			lngColRateKey = 0;
			// Rate Key, Year and the two hidden fields should stay the same
			lngColYear = 1;
			// because the reports use them w/o the variables, the rest can be shuffled
			lngColHiddenDueNow = 2;
			lngColHiddenDue30Days = 3;
			// ******** Changeable *********
			lngColType = 4;
			lngColDescription = 5;
			lngColCommitmentDate = 6;
			lngColDueDate = 7;
			lngCol30DNDate1 = 8;
			lngCol30DNDate2 = 9;
			lngCOl30DNDate = 10;
			lngColLien1 = 11;
			lngColLien2 = 12;
			lngColLienDate = 13;
			lngColMaturityNotice1 = 14;
			lngColMaturityNotice2 = 15;
			lngColMaturityDate = 16;
			if (modUTStatusPayments.Statics.TownService == "W")
			{
				cmbWS.Clear();
				cmbWS.Items.Add("Water");
				cmbWS.Text = "Water";
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = false;
				//optWS[0].Checked = true;
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				cmbWS.Clear();
				cmbWS.Items.Add("Sewer");
				cmbWS.Text = "Sewer";
				//optWS[1].Enabled = true;
				//optWS[0].Enabled = false;
				//optWS[1].Checked = true;
			}
			else if (modUTStatusPayments.Statics.TownService == "B")
			{
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = true;
				if (modUTStatusPayments.Statics.gboolPayWaterFirst)
				{
					cmbWS.Text = "Water";
				}
				else
				{
					cmbWS.Text = "Sewer";
				}
			}
		}

		private void FormatGrid_2(bool boolReset)
		{
			FormatGrid(ref boolReset);
		}

		private void FormatGrid(ref bool boolReset)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngWid = 0;
				if (boolReset)
				{
					vsRate.Rows = 1;
				}
				vsRate.Cols = 17;
				lngWid = vsRate.WidthOriginal;
				vsRate.ColWidth(lngColRateKey, FCConvert.ToInt32(lngWid * 0.07));
				vsRate.ColWidth(lngColYear, 0);
				vsRate.ColWidth(lngColHiddenDueNow, 0);
				vsRate.ColWidth(lngColHiddenDue30Days, 0);
				vsRate.ColWidth(lngColType, FCConvert.ToInt32(lngWid * 0.06));
				vsRate.ColWidth(lngColDescription, FCConvert.ToInt32(lngWid * 0.23));
				vsRate.ColWidth(lngColCommitmentDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColDueDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngCol30DNDate1, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngCol30DNDate2, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngCOl30DNDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColLien1, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColLien2, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColLienDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColMaturityNotice1, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColMaturityNotice2, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.ColWidth(lngColMaturityDate, FCConvert.ToInt32(lngWid * 0.13));
				vsRate.TextMatrix(0, lngColRateKey, "Key");
				vsRate.TextMatrix(0, lngColYear, "Year");
				vsRate.TextMatrix(0, lngColType, "Type");
				vsRate.TextMatrix(0, lngColDescription, "Description");
				vsRate.TextMatrix(0, lngColCommitmentDate, "Commitment");
				vsRate.TextMatrix(0, lngColDueDate, "Due Date");
				vsRate.TextMatrix(0, lngCol30DNDate1, "Start 30DN");
				vsRate.TextMatrix(0, lngCol30DNDate2, "End 30DN");
				vsRate.TextMatrix(0, lngCOl30DNDate, "30DN Date");
				vsRate.TextMatrix(0, lngColLien1, "Start Lien");
				vsRate.TextMatrix(0, lngColLien2, "End Lien");
				vsRate.TextMatrix(0, lngColLienDate, "Lien Date");
				vsRate.TextMatrix(0, lngColMaturityNotice1, "Start Mat");
				vsRate.TextMatrix(0, lngColMaturityNotice2, "End Mat");
				vsRate.TextMatrix(0, lngColMaturityDate, "Mat Date");
				// formatting
				vsRate.ColAlignment(lngColYear, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsRate.ColAlignment(lngColType, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				//FC:FINAL:CHN - issue #1207: Incorrect column alignment.
				vsRate.ColDataType(0, FCGrid.DataTypeSettings.flexDTLong);
				SetGridHeight();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Formatting Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetGridHeight()
		{
			// this will set the correct grid height compared to how many rows are included in the grid
			if ((((vsRate.Rows + 1) * vsRate.RowHeight(0)) + vsRate.Top) > frmUTLienDates.InstancePtr.Height)
			{
				vsRate.Height = FCConvert.ToInt32(((frmUTLienDates.InstancePtr.Height - vsRate.Top) * 0.9) - vsRate.Top);
				// vsRate.Height = (frmLienDates.Height * 0.92) - vsRate.Top
			}
			else
			{
				vsRate.Height = ((vsRate.Rows + 1) * vsRate.RowHeight(0));
			}
		}

		private void frmUTLienDates_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmUTLienDates_Resize(object sender, System.EventArgs e)
		{
			SetGridHeight();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			frmUTPrintLienDates.InstancePtr.Show(App.MainForm);
		}

		private void mnuFilePrintDateChart_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptLienDateChart.InstancePtr);
			//rptLienDateChart.InstancePtr.Show(App.MainForm);
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			RefreshGrid();
		}

		private void SaveSettings()
		{
			int intError = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngRW;
				bool boolUpdate = false;
				// go through each record and check to make sure each date has not changed...if it has, then update it
				// just update each one
				// force the end of the edit if there is any
				vsRate.Select(0, 0);
				rsSettings.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
				for (lngRW = 1; lngRW <= vsRate.Rows - 1; lngRW++)
				{
					intError = 1;
					boolUpdate = false;
					rsSettings.FindFirstRecord("ID", Conversion.Val(vsRate.TextMatrix(lngRW, lngColRateKey)));
					if (!rsSettings.NoMatch)
					{
						intError = 2;
						rsSettings.Edit();
						if (Strings.Trim(vsRate.TextMatrix(lngRW, lngCOl30DNDate)) != "")
						{
							if (Information.IsDate(vsRate.TextMatrix(lngRW, lngCOl30DNDate)))
							{
								//FC:FINAL:MSH - I.Issue #986: rsSettings.Get_Fields("30DNDate" + strWS) can be equal to an empty string and will be throwed an exception
								//if (rsSettings.Get_Fields("30DNDate" + strWS) != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngCOl30DNDate)).ToOADate())
								if (FCConvert.ToDateTime(rsSettings.Get_Fields_DateTime("30DNDate" + strWS) as object).ToOADate() != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngCOl30DNDate)).ToOADate())
								{
									boolUpdate = true;
									rsSettings.Set_Fields("30DNDate" + strWS, DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngCOl30DNDate)));
								}
							}
							else
							{
								rsSettings.Set_Fields("30DNDate" + strWS, 0);
								// set these to zero after
							}
						}
						else
						{
							rsSettings.Set_Fields("30DNDate" + strWS, 0);
						}
						if (Strings.Trim(vsRate.TextMatrix(lngRW, lngColLienDate)) != "")
						{
							if (Information.IsDate(vsRate.TextMatrix(lngRW, lngColLienDate)))
							{
								//FC:FINAL:MSH - I.Issue #986: rsSettings.Get_Fields("LienDate" + strWS) can be equal to an empty string and will be throwed an exception
								//if (rsSettings.Get_Fields("LienDate" + strWS) != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColLienDate)).ToOADate())
								if (FCConvert.ToDateTime(rsSettings.Get_Fields_DateTime("LienDate" + strWS) as object).ToOADate() != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColLienDate)).ToOADate())
								{
									boolUpdate = true;
									rsSettings.Set_Fields("LienDate" + strWS, DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColLienDate)));
								}
							}
							else
							{
								rsSettings.Set_Fields("LienDate" + strWS, 0);
								// set these to zero after
							}
						}
						else
						{
							rsSettings.Set_Fields("LienDate" + strWS, 0);
						}
						if (Strings.Trim(vsRate.TextMatrix(lngRW, lngColMaturityDate)) != "")
						{
							if (Information.IsDate(vsRate.TextMatrix(lngRW, lngColMaturityDate)))
							{
								//FC:FINAL:MSH - I.Issue #986: rsSettings.Get_Fields("MaturityDate" + strWS) can be equal to an empty string and will be throwed an exception
								//if (rsSettings.Get_Fields("MaturityDate" + strWS) != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColMaturityDate)).ToOADate())
								if (FCConvert.ToDateTime(rsSettings.Get_Fields_DateTime("MaturityDate" + strWS) as object).ToOADate() != DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColMaturityDate)).ToOADate())
								{
									boolUpdate = true;
									rsSettings.Set_Fields("MaturityDate" + strWS, DateAndTime.DateValue(vsRate.TextMatrix(lngRW, lngColMaturityDate)));
								}
							}
							else
							{
								rsSettings.Set_Fields("MaturityDate" + strWS, 0);
								// set these to zero after
							}
						}
						else
						{
							rsSettings.Set_Fields("MaturityDate" + strWS, 0);
						}
						rsSettings.Update(true);
					}
					if (boolUpdate)
					{
						// add a CYA entry
						modGlobalFunctions.AddCYAEntry_242("CL", "Edit Lien Dates", "30DN = " + vsRate.TextMatrix(lngRW, lngCOl30DNDate), "Lien = " + vsRate.TextMatrix(lngRW, lngColLienDate), "Maturity = " + vsRate.TextMatrix(lngRW, lngColMaturityDate), strWS);
					}
				}
				intError = 20;
				MessageBox.Show("Save Successful.", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Settings - " + FCConvert.ToString(intError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void LoadSettings()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will load the settings from the database
				// vbPorter upgrade warning: dt30DN As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dt30DN;
				// vbPorter upgrade warning: dtLien As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtLien;
				// vbPorter upgrade warning: dtMat As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtMat;
				// vbPorter upgrade warning: dtDueDate As DateTime	OnWriteFCConvert.ToInt16(
				DateTime dtDueDate;
				// kgk trout-692
				vsRate.Rows = 1;
				FormatGrid_2(true);
				// this will get all the Regular Rate Records
				rsSettings.OpenRecordset("SELECT ID AS RK, * FROM RateKeys WHERE RateType <> 'L' ORDER BY RateType, BillDate, ID", modExtraModules.strUTDatabase);
				while (!rsSettings.EndOfFile())
				{
					vsRate.AddItem("");
					// TODO Get_Fields: Field [RK] not found!! (maybe it is an alias?)
					vsRate.TextMatrix(vsRate.Rows - 1, lngColRateKey, FCConvert.ToString(rsSettings.Get_Fields("RK")));
					vsRate.TextMatrix(vsRate.Rows - 1, lngColYear, Strings.Format(rsSettings.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy"));
					vsRate.TextMatrix(vsRate.Rows - 1, lngColType, FCConvert.ToString(rsSettings.Get_Fields_String("RateType")));
					vsRate.TextMatrix(vsRate.Rows - 1, lngColDescription, FCConvert.ToString(rsSettings.Get_Fields_String("Description")));
					if (!rsSettings.IsFieldNull("30DNDate" + strWS))
					{
						dt30DN = (DateTime)rsSettings.Get_Fields_DateTime("30DNDate" + strWS);
					}
					else
					{
						dt30DN = DateTime.FromOADate(0);
					}
					if (!rsSettings.IsFieldNull("LienDate" + strWS))
					{
						dtLien = (DateTime)rsSettings.Get_Fields_DateTime("LienDate" + strWS);
					}
					else
					{
						dtLien = DateTime.FromOADate(0);
					}
					if (!rsSettings.IsFieldNull("MaturityDate" + strWS))
					{
						dtMat = (DateTime)rsSettings.Get_Fields_DateTime("MaturityDate" + strWS);
					}
					else
					{
						dtMat = DateTime.FromOADate(0);
					}
					// kgk 06-01-2011 trout-692  Blank due date from loadback process causes type mismatch error
					if (!rsSettings.IsFieldNull("DueDate"))
					{
						// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
						dtDueDate = (DateTime)rsSettings.Get_Fields("DueDate");
					}
					else
					{
						// TODO Get_Fields: Field [RK] not found!! (maybe it is an alias?)
						MessageBox.Show("Unable to calculate process dates for rate key " + rsSettings.Get_Fields("RK") + " because the due date is not set.", "Error Calculating Dates", MessageBoxButtons.OK, MessageBoxIcon.Information);
						dtDueDate = DateTime.FromOADate(0);
					}
					// rsSettings.Fields("DueDate")
					CalculateLienProcessDates_26(vsRate.Rows - 1, FCConvert.CBool(rsSettings.Get_Fields_String("RateType") == "L"), rsSettings.Get_Fields_DateTime("BillDate"), dtDueDate, dt30DN, dtLien, dtMat);
					rsSettings.MoveNext();
				}
				// this will get all the LoadBack/Lien Rate Records
				// rsSettings.OpenRecordset "SELECT * FROM LoadBackOriginal", strUTDatabase
				// If Not rsSettings.EndOfFile Then
				// rsSettings.OpenRecordset "SELECT RateKeys.RateKey AS RK, * FROM RateKeys INNER JOIN (Bill INNER JOIN Lien ON Bill.LienRecordNumber = Lien.LienKey) ON Ratekeys.RateKey = Lien.RateKey WHERE Bill.RateKey = Lien.RateKey ORDER BY RateType, RateKey", strUTDatabase
				// 
				// Do Until rsSettings.EndOfFile
				// vsRate.AddItem ""
				// 
				// vsRate.TextMatrix(vsRate.rows - 1, lngColRateKey) = rsSettings.Fields("RK")
				// vsRate.TextMatrix(vsRate.rows - 1, lngColYear) = Format(rsSettings.Fields("BillDate"), "MM/dd/yyyy")
				// vsRate.TextMatrix(vsRate.rows - 1, lngColType) = rsSettings.Fields("RateType")
				// vsRate.TextMatrix(vsRate.rows - 1, lngColDescription) = rsSettings.Fields("Description")
				// 
				// If rsSettings.Fields("30DNDate" & strWS) <> "" Then
				// dt30DN = rsSettings.Fields("30DNDate" & strWS)
				// Else
				// dt30DN = 0
				// End If
				// 
				// If rsSettings.Fields("LienDate" & strWS) <> "" Then
				// dtLien = rsSettings.Fields("LienDate" & strWS)
				// Else
				// dtLien = 0
				// End If
				// 
				// If rsSettings.Fields("MaturityDate" & strWS) <> "" Then
				// dtMat = rsSettings.Fields("MaturityDate" & strWS)
				// Else
				// dtMat = 0
				// End If
				// 
				// 30DNDate, LienDate, MaturityDate
				// CalculateLienProcessDates vsRate.rows - 1, True, rsSettings.Fields("BillDate"), dt30DN, dtLien, dtMat
				// 
				// rsSettings.MoveNext
				// Loop
				// End If
				// 
				SetGridHeight();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Default Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void RefreshGrid()
		{
			// this will refresht he grid contents
			FormatGrid_2(true);
			LoadSettings();
		}

		private void CalculateLienProcessDates_26(int lngRow, bool boolLien, DateTime dtComDate, DateTime dtDueDate, DateTime dt30DNDate, DateTime dtLienDate, DateTime dtMatDate)
		{
			CalculateLienProcessDates(ref lngRow, ref boolLien, ref dtComDate, ref dtDueDate, ref dt30DNDate, ref dtLienDate, ref dtMatDate);
		}

		private void CalculateLienProcessDates(ref int lngRow, ref bool boolLien, ref DateTime dtComDate, ref DateTime dtDueDate, ref DateTime dt30DNDate, ref DateTime dtLienDate, ref DateTime dtMatDate)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// calculate these values from the dates that are in the records
				DateTime dt30DN1;
				DateTime dt30DN2;
				DateTime dtLien1;
				DateTime dtLien2;
				DateTime dtMatNot1;
				DateTime dtMatNot2;
				bool boolDue;
				bool bool30Days;
				boolDue = false;
				bool30Days = false;
				if (!boolLien)
				{
					if (dtComDate.ToOADate() != 0 && dtDueDate.ToOADate() != 0)
					{
						// kgk 06-01-2011 trout-692 Add check for missing due date
						// Commitment Date
						// 30 Day Notice should be sent 90 days after due date
						dt30DN1 = DateAndTime.DateAdd("D", 90, dtDueDate);
						// End 30 DN time 1 year after the due date
						dt30DN2 = DateAndTime.DateAdd("yyyy", 1, dtDueDate);
						vsRate.TextMatrix(lngRow, lngColCommitmentDate, Strings.Format(dtComDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColDueDate, Strings.Format(dtDueDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngCol30DNDate1, Strings.Format(dt30DN1, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngCol30DNDate2, Strings.Format(dt30DN2, "MM/dd/yyyy"));
						if (DateAndTime.DateDiff("d", DateTime.Today, dt30DN1) <= 0 && DateAndTime.DateDiff("d", DateTime.Today, dt30DN2) >= 0)
						{
							// color the dates so the user can see that they are in affect
							boolDue = true;
							vsRate.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngCol30DNDate1, lngRow, lngCol30DNDate2, modGlobalConstants.Statics.TRIOCOLORBLUE);
						}
						if ((DateAndTime.DateDiff("d", DateTime.Today, dt30DN1) > 0 && DateAndTime.DateDiff("d", DateTime.Today, dt30DN1) < 30) && DateAndTime.DateDiff("d", DateAndTime.DateAdd("D", 30, DateTime.Today), dt30DN2) >= 0)
						{
							bool30Days = true;
						}
					}
					else
					{
						vsRate.TextMatrix(vsRate.Rows - 1, lngColCommitmentDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColDueDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngCol30DNDate1, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngCol30DNDate2, "");
					}
					// 30 DN Date
					if (dt30DNDate.ToOADate() != 0)
					{
						// Begin Lien Window  30 Days after notice date
						dtLien1 = DateAndTime.DateAdd("d", 30, dt30DNDate);
						// End Lien Window (1 Year after notice date)
						dtLien2 = DateAndTime.DateAdd("yyyy", 1, dt30DNDate);
						vsRate.TextMatrix(lngRow, lngCOl30DNDate, Strings.Format(dt30DNDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColLien1, Strings.Format(dtLien1, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColLien2, Strings.Format(dtLien2, "MM/dd/yyyy"));
						if (DateAndTime.DateDiff("d", DateTime.Today, dtLien1) <= 0 && DateAndTime.DateDiff("d", DateTime.Today, dtLien2) >= 0)
						{
							// color the dates so the user can see that they are in affect
							boolDue = true;
							vsRate.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngColLien1, lngRow, lngColLien2, modGlobalConstants.Statics.TRIOCOLORBLUE);
						}
						if ((DateAndTime.DateDiff("d", DateTime.Today, dtLien1) > 0 && DateAndTime.DateDiff("d", DateTime.Today, dtLien1) < 30) && DateAndTime.DateDiff("d", DateAndTime.DateAdd("D", 30, DateTime.Today), dtLien2) >= 0)
						{
							bool30Days = true;
						}
					}
					else
					{
						vsRate.TextMatrix(vsRate.Rows - 1, lngCOl30DNDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColLien1, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColLien2, "");
					}
					// Lien Date
					if (dtLienDate.ToOADate() != 0)
					{
						// Maturity Notice Start
						dtMatNot1 = DateAndTime.DateAdd("M", 18, dtLienDate);
						dtMatNot1 = DateAndTime.DateAdd("d", -45, dtMatNot1);
						// Maturity Notice End
						dtMatNot2 = DateAndTime.DateAdd("d", 15, dtMatNot1);
						vsRate.TextMatrix(lngRow, lngColLienDate, Strings.Format(dtLienDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColMaturityNotice1, Strings.Format(dtMatNot1, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColMaturityNotice2, Strings.Format(dtMatNot2, "MM/dd/yyyy"));
						if (DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) <= 0 && DateAndTime.DateDiff("d", DateTime.Today, dtMatNot2) >= 0)
						{
							// color the dates so the user can see that they are in affect
							boolDue = true;
							vsRate.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngColMaturityNotice1, lngRow, lngColMaturityNotice2, modGlobalConstants.Statics.TRIOCOLORBLUE);
						}
						if ((DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) > 0 && DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) < 30) && DateAndTime.DateDiff("d", DateAndTime.DateAdd("D", 30, DateTime.Today), dtMatNot2) >= 0)
						{
							bool30Days = true;
						}
					}
					else
					{
						vsRate.TextMatrix(vsRate.Rows - 1, lngColLienDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColMaturityNotice1, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColMaturityNotice2, "");
					}
					// Maturity Date
					if (dtMatDate.ToOADate() != 0)
					{
						vsRate.TextMatrix(lngRow, lngColMaturityDate, Strings.Format(dtMatDate, "MM/dd/yyyy"));
					}
					else
					{
						vsRate.TextMatrix(lngRow, lngColMaturityDate, "");
					}
				}
				else
				{
					// Lien Date
					if (dtLienDate.ToOADate() != 0 && dtDueDate.ToOADate() != 0)
					{
						// kgk 06-01-2011 trout-692 Add check for missing due date
						vsRate.TextMatrix(lngRow, lngColCommitmentDate, Strings.Format(dtLienDate, "MM/dd/yyyy"));
						// Maturity Notice Start  'Mat Notices are not mandatory for sewer liens
						dtMatNot1 = DateAndTime.DateAdd("M", 18, dtLienDate);
						dtMatNot1 = DateAndTime.DateAdd("d", -45, dtMatNot1);
						// Maturity Notice End
						dtMatNot2 = DateAndTime.DateAdd("d", 15, dtMatNot1);
						vsRate.TextMatrix(lngRow, lngColLienDate, Strings.Format(dtLienDate, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColMaturityNotice1, Strings.Format(dtMatNot1, "MM/dd/yyyy"));
						vsRate.TextMatrix(lngRow, lngColMaturityNotice2, Strings.Format(dtMatNot2, "MM/dd/yyyy"));
						if (DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) <= 0 && DateAndTime.DateDiff("d", DateTime.Today, dtMatNot2) >= 0)
						{
							// color the dates so the user can see that they are in affect
							boolDue = true;
							vsRate.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngColMaturityNotice1, lngRow, lngColMaturityNotice2, modGlobalConstants.Statics.TRIOCOLORBLUE);
						}
						if ((DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) > 0 && DateAndTime.DateDiff("d", DateTime.Today, dtMatNot1) < 30) && DateAndTime.DateDiff("d", DateAndTime.DateAdd("D", 30, DateTime.Today), dtMatNot2) >= 0)
						{
							bool30Days = true;
						}
					}
					else
					{
						vsRate.TextMatrix(vsRate.Rows - 1, lngColLienDate, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColMaturityNotice1, "");
						vsRate.TextMatrix(vsRate.Rows - 1, lngColMaturityNotice2, "");
					}
					// Maturity Date
					if (dtMatDate.ToOADate() != 0)
					{
						vsRate.TextMatrix(lngRow, lngColMaturityDate, Strings.Format(dtMatDate, "MM/dd/yyyy"));
					}
					else
					{
						vsRate.TextMatrix(lngRow, lngColMaturityDate, "");
					}
				}
				if (boolDue)
				{
					vsRate.TextMatrix(lngRow, lngColHiddenDueNow, FCConvert.ToString(-1));
				}
				else
				{
					vsRate.TextMatrix(lngRow, lngColHiddenDueNow, FCConvert.ToString(0));
				}
				if (bool30Days)
				{
					vsRate.TextMatrix(lngRow, lngColHiddenDue30Days, FCConvert.ToString(-1));
				}
				else
				{
					vsRate.TextMatrix(lngRow, lngColHiddenDue30Days, FCConvert.ToString(0));
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Dates", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveSettings();
			this.Unload();
		}
		//FC:FINAL:DDU:#i984 - moved code to cmbWS_SelectedIndexChanged
		//private void optWS_CheckedChanged(int Index, object sender, System.EventArgs e)
		//      {
		//          if (Index == 0)
		//          {
		//              if (strWS == "S")
		//              {
		//                  strWS = "W";
		//                  LoadSettings();
		//              }
		//              else
		//              {
		//                  strWS = "W";
		//              }
		//          }
		//          else
		//          {
		//              if (strWS == "W")
		//              {
		//                  strWS = "S";
		//                  LoadSettings();
		//              }
		//              else
		//              {
		//                  strWS = "S";
		//              }
		//          }
		//      }
		//      private void optWS_CheckedChanged(object sender, System.EventArgs e)
		//      {
		//          int index = cmbWS.SelectedIndex;
		//          optWS_CheckedChanged(index, sender, e);
		//      }
		private void vsRate_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			if (vsRate.Col == lngCOl30DNDate || vsRate.Col == lngColLienDate || vsRate.Col == lngColMaturityDate)
			{
				if (Strings.Trim(vsRate.TextMatrix(vsRate.Row, vsRate.Col)) == "__/__/____")
				{
					vsRate.TextMatrix(vsRate.Row, vsRate.Col, string.Empty);
				}
			}
		}

		private void vsRate_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsRate.Row > 0)
			{
				if (vsRate.Col == lngCOl30DNDate || vsRate.Col == lngColLienDate || vsRate.Col == lngColMaturityDate)
				{
					// allow editing
					vsRate.EditMask = "##/##/####";
				}
				else
				{
					// do not allow editing
					vsRate.EditMask = "";
				}
			}
		}

		private void vsRate_DblClick(object sender, System.EventArgs e)
		{
			// this will show the rate key that is clicked on
			ShowChart_2(vsRate.TextMatrix(vsRate.Row, lngColRateKey) + ",");
		}

		private void vsRate_RowColChange(object sender, System.EventArgs e)
		{
			if (vsRate.Row > 0)
			{
				if (vsRate.Col == lngCOl30DNDate || vsRate.Col == lngColLienDate || vsRate.Col == lngColMaturityDate)
				{
					// allow editing
					vsRate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					vsRate.EditCell();
				}
				else
				{
					// do not allow editing
					vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
				}
			}
		}

		private void vsRate_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (Information.IsDate(vsRate.EditText) && Strings.Trim(vsRate.EditText).Length == 10)
			{
				// let the value stand and only update at the end
				// SaveNewDateValue vsRate.TextMatrix(Row, lngColRateKey), CDate(vsRate.EditText), Col
			}
			else if (vsRate.EditText == "  /  /    " || Strings.Trim(vsRate.EditText) == "/  /")
			{
				vsRate.EditText = "";
			}
			else
			{
				switch (MessageBox.Show("Invalid date. Would you like to continue?", "Invalid Date", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							vsRate.EditText = "";
							break;
						}
					case DialogResult.No:
					case DialogResult.Cancel:
						{
							e.Cancel = true;
							break;
						}
				}
				//end switch
			}
		}

		private void SaveNewDateValue(ref DateTime dtDate, ref int lngRK, ref int lngCol)
		{
			clsDRWrapper rsSave = new clsDRWrapper();
			rsSave.OpenRecordset("SELECT * FROM RateRec WHERE ID = " + FCConvert.ToString(lngRK));
			if (!rsSave.EndOfFile())
			{
				rsSave.Edit();
				if (lngCol == lngCOl30DNDate)
				{
					rsSave.Set_Fields("30DNDate" + strWS, dtDate);
				}
				else if (lngCol == lngColLienDate)
				{
					rsSave.Set_Fields("LienDate" + strWS, dtDate);
				}
				else if (lngCol == lngColMaturityDate)
				{
					rsSave.Set_Fields("MaturityDate" + strWS, dtDate);
				}
				rsSave.Update();
			}
		}

		private void ShowChart_2(string strString)
		{
			ShowChart(ref strString);
		}

		private void ShowChart(ref string strString)
		{
			// show them in the viewer
			frmUTLienDates.InstancePtr.strReportHeader = "Lien Dates Timeline";
			rptUTLienDateLine.InstancePtr.Init(ref strString, ref strWS);
			frmReportViewer.InstancePtr.Init(rptUTLienDateLine.InstancePtr);
		}

		private void cmbWS_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			//FC:FINAL:DDU:#i984 - moved code from optWS_Click
			if (cmbWS.Text == "Water")
			{
				if (strWS == "S")
				{
					strWS = "W";
					LoadSettings();
				}
				else
				{
					strWS = "W";
				}
			}
			else
			{
				if (strWS == "W")
				{
					strWS = "S";
					LoadSettings();
				}
				else
				{
					strWS = "S";
				}
			}
		}
	}
}
