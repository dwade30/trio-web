//Fecher vbPorter - Version 1.0.0.40


using fecherFoundation;
using Global;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptOaklandDisketteNoMatch.
	/// </summary>
	public class srptOaklandDisketteNoMatch : fecherFoundation.FCForm
	{

// nObj = 1
//   0	srptOaklandDisketteNoMatch	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/10/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/15/2004              *
		// ********************************************************
		bool blnFirstRecord;
		int lngCounter;

		private void ActiveReport_FetchData(ref bool EOF)
		{
			// MAL@20070924: Rework to test for accounts that already exist in Bad Numbers table
			// Call Reference: 117160

			if (blnFirstRecord) {
				blnFirstRecord = false;
				if (lngCounter<=modTSCONSUM.lngNoMatchCounter-1) {
					EOF = false;
				} else {
					EOF = true;
				}
			} else {
				lngCounter += 1;
				if (lngCounter<=modTSCONSUM.lngNoMatchCounter-1) {
					EOF = false;
				} else {
					EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart()
		{
			lngCounter = 0;
			blnFirstRecord = true;
		}

		private void Detail_Format()
		{
			// MAL@20070924: Added check for existence of serial number in new table
			// Call Reference: 117160

			if (lngCounter>modTSCONSUM.lngNoMatchCounter-1) {
				LayoutAction = ddLANextRecord;
				// fldSerialNumber = ""
				// fldName = ""
				// fldAddress = ""
				// fldConsumption = ""
				// fldDate = ""
			} else {
				if (IsInBadTable(modTSCONSUM.NoAccountFound[lngCounter].SerialNumber)) {
					LayoutAction = ddLANextRecord;
					// fldSerialNumber = ""
					// fldName = ""
					// fldAddress = ""
					// fldConsumption = ""
					// fldDate = ""
				} else {
					fldSerialNumber.Text = modTSCONSUM.NoAccountFound[lngCounter].SerialNumber;
					fldName.Text = modTSCONSUM.NoAccountFound[lngCounter].OwnerName;
					fldAddress.Text = modTSCONSUM.NoAccountFound[lngCounter].OwnerAddress;
					fldConsumption.Text = Strings.Format(Conversion.Val(modTSCONSUM.NoAccountFound[lngCounter].Consumption)/100, "#,##0");
					fldDate.Text = Strings.Left(new string(modTSCONSUM.NoAccountFound[lngCounter].ReadingDate), 2)+"/"+Strings.Mid(new string(modTSCONSUM.NoAccountFound[lngCounter].ReadingDate), 3, 2)+"/"+Strings.Right(new string(modTSCONSUM.NoAccountFound[lngCounter].ReadingDate), 2);
				}
			}
		}

		// vbPorter upgrade warning: lngSerialNumber As Variant --> As FixedString
		private bool IsInBadTable(FixedString[] lngSerialNumber)
		{
			bool IsInBadTable = false;
			// MAL@20070924: Added function to check for serial number in table

			bool blnResult;
			clsDRWrapper rsBadNum = new clsDRWrapper();

			rsBadNum.OpenRecordset("SELECT * FROM tblBadNumbers", modExtraModules.strUTDatabase);

			rsBadNum.FindFirstRecord("BadAccountNumber", lngSerialNumber);

			blnResult = !rsBadNum.NoMatch;

			IsInBadTable = blnResult;

			return IsInBadTable;
		}


		private void srptOaklandDisketteNoMatch_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptOaklandDisketteNoMatch properties;
			//srptOaklandDisketteNoMatch.Caption	= "Daily Audit Detail";
			//srptOaklandDisketteNoMatch.Icon	= "srptDisketteNoMatch.dsx":0000";
			//srptOaklandDisketteNoMatch.Left	= 0;
			//srptOaklandDisketteNoMatch.Top	= 0;
			//srptOaklandDisketteNoMatch.Width	= 11880;
			//srptOaklandDisketteNoMatch.Height	= 8175;
			//srptOaklandDisketteNoMatch.StartUpPosition	= 3;
			//srptOaklandDisketteNoMatch.SectionData	= "srptDisketteNoMatch.dsx":058A;
			//End Unmaped Properties
		}
	}
}