//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class cUTBillRateInformation
	{

		//=========================================================

		private string strPeriodDescription;
		private DateTime dtStartDate;
		private DateTime dtEndDate;
		private DateTime dtDueDate;
		private DateTime dtInterestStartDate;

		public DateTime InterestStartDate
		{
			set
			{
				dtInterestStartDate = value;
			}

			get
			{
					DateTime InterestStartDate = System.DateTime.Now;
				InterestStartDate = dtInterestStartDate;
				return InterestStartDate;
			}
		}



		public DateTime DueDate
		{
			set
			{
				dtDueDate = value;
			}

			get
			{
					DateTime DueDate = System.DateTime.Now;
				DueDate = dtDueDate;
				return DueDate;
			}
		}




		public DateTime StartDate
		{
			set
			{
				dtStartDate = value;
			}

			get
			{
					DateTime StartDate = System.DateTime.Now;
				StartDate = dtStartDate;
				return StartDate;
			}
		}



		public DateTime EndDate
		{
			set
			{
				dtEndDate = value;
			}

			get
			{
					DateTime EndDate = System.DateTime.Now;
				EndDate = dtEndDate;
				return EndDate;
			}
		}



		public string PeriodDescription
		{
			set
			{
				strPeriodDescription = value;
			}

			get
			{
					string PeriodDescription = "";
				PeriodDescription = strPeriodDescription;
				return PeriodDescription;
			}
		}



	}
}
