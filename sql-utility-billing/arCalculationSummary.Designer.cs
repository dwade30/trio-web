﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arCalculationSummary.
	/// </summary>
	partial class arCalculationSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arCalculationSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWOv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWFlat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWMisc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWAdj = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSOv = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSFlat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSMisc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSAdj = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblWaterCalc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWaterSum = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lbl = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label27 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label28 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label29 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.CustomVsWSum = new GrapeCity.ActiveReports.SectionReportModel.CustomControl();
			this.CustomVsSSum = new GrapeCity.ActiveReports.SectionReportModel.CustomControl();
			this.lnCat = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line6 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line7 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblHiddenSummaryHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWOv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWFlat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSOv)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSFlat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSAdj)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterCalc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterSum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHiddenSummaryHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label8,
				this.Label9,
				this.fldWOv,
				this.fldWFlat,
				this.fldWUnits,
				this.fldWCons,
				this.fldWMisc,
				this.fldWAdj,
				this.fldWTax,
				this.fldWTotal,
				this.fldSOv,
				this.fldSFlat,
				this.fldSUnits,
				this.fldSCons,
				this.fldSMisc,
				this.fldSAdj,
				this.fldSTax,
				this.fldSTotal,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17,
				this.Line1,
				this.lblWaterCalc,
				this.Label19,
				this.Line2,
				this.Label20,
				this.lblWaterSum,
				this.Label22,
				this.lbl,
				this.Label23,
				this.Label24,
				this.Label25,
				this.Label26,
				this.Label27,
				this.Label28,
				this.Label29,
				this.CustomVsWSum,
				this.CustomVsSSum,
				this.lnCat,
				this.Line3,
				this.Line4,
				this.Line5,
				this.Line6,
				this.Line7,
				this.lblHiddenSummaryHeader
			});
			this.Detail.Height = 4.6875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.fldBook,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber
			});
			this.PageHeader.Height = 0.4166667F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.Label1.Text = "Calculation Summary Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.5F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.fldBook.Text = null;
			this.fldBook.Top = 0.25F;
			this.fldBook.Width = 7.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.0625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.5F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6.5F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 1.75F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label2.Text = "Override";
			this.Label2.Top = 0.625F;
			this.Label2.Width = 1F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1.75F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label3.Text = "Flat";
			this.Label3.Top = 0.8125F;
			this.Label3.Width = 1F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 1.75F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label4.Text = "Units";
			this.Label4.Top = 1F;
			this.Label4.Width = 1F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 1.75F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label5.Text = "Consumption";
			this.Label5.Top = 1.1875F;
			this.Label5.Width = 1F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 1.75F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label6.Text = "Miscellaneous";
			this.Label6.Top = 1.375F;
			this.Label6.Width = 1F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 1.75F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label7.Text = "Adjustments";
			this.Label7.Top = 1.5625F;
			this.Label7.Width = 1F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 1.75F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label8.Text = "Tax";
			this.Label8.Top = 1.75F;
			this.Label8.Width = 1F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 1.75F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label9.Text = "Total";
			this.Label9.Top = 2F;
			this.Label9.Width = 1F;
			// 
			// fldWOv
			// 
			this.fldWOv.Height = 0.1875F;
			this.fldWOv.Left = 2.75F;
			this.fldWOv.Name = "fldWOv";
			this.fldWOv.OutputFormat = resources.GetString("fldWOv.OutputFormat");
			this.fldWOv.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWOv.Text = null;
			this.fldWOv.Top = 0.625F;
			this.fldWOv.Width = 0.9375F;
			// 
			// fldWFlat
			// 
			this.fldWFlat.Height = 0.1875F;
			this.fldWFlat.Left = 2.75F;
			this.fldWFlat.Name = "fldWFlat";
			this.fldWFlat.OutputFormat = resources.GetString("fldWFlat.OutputFormat");
			this.fldWFlat.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWFlat.Text = null;
			this.fldWFlat.Top = 0.8125F;
			this.fldWFlat.Width = 0.9375F;
			// 
			// fldWUnits
			// 
			this.fldWUnits.Height = 0.1875F;
			this.fldWUnits.Left = 2.75F;
			this.fldWUnits.Name = "fldWUnits";
			this.fldWUnits.OutputFormat = resources.GetString("fldWUnits.OutputFormat");
			this.fldWUnits.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWUnits.Text = null;
			this.fldWUnits.Top = 1F;
			this.fldWUnits.Width = 0.9375F;
			// 
			// fldWCons
			// 
			this.fldWCons.Height = 0.1875F;
			this.fldWCons.Left = 2.75F;
			this.fldWCons.Name = "fldWCons";
			this.fldWCons.OutputFormat = resources.GetString("fldWCons.OutputFormat");
			this.fldWCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWCons.Text = null;
			this.fldWCons.Top = 1.1875F;
			this.fldWCons.Width = 0.9375F;
			// 
			// fldWMisc
			// 
			this.fldWMisc.Height = 0.1875F;
			this.fldWMisc.Left = 2.75F;
			this.fldWMisc.Name = "fldWMisc";
			this.fldWMisc.OutputFormat = resources.GetString("fldWMisc.OutputFormat");
			this.fldWMisc.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWMisc.Text = null;
			this.fldWMisc.Top = 1.375F;
			this.fldWMisc.Width = 0.9375F;
			// 
			// fldWAdj
			// 
			this.fldWAdj.Height = 0.1875F;
			this.fldWAdj.Left = 2.75F;
			this.fldWAdj.Name = "fldWAdj";
			this.fldWAdj.OutputFormat = resources.GetString("fldWAdj.OutputFormat");
			this.fldWAdj.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWAdj.Text = null;
			this.fldWAdj.Top = 1.5625F;
			this.fldWAdj.Width = 0.9375F;
			// 
			// fldWTax
			// 
			this.fldWTax.Height = 0.1875F;
			this.fldWTax.Left = 2.75F;
			this.fldWTax.Name = "fldWTax";
			this.fldWTax.OutputFormat = resources.GetString("fldWTax.OutputFormat");
			this.fldWTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWTax.Text = null;
			this.fldWTax.Top = 1.75F;
			this.fldWTax.Width = 0.9375F;
			// 
			// fldWTotal
			// 
			this.fldWTotal.Height = 0.1875F;
			this.fldWTotal.Left = 2.75F;
			this.fldWTotal.Name = "fldWTotal";
			this.fldWTotal.OutputFormat = resources.GetString("fldWTotal.OutputFormat");
			this.fldWTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldWTotal.Text = null;
			this.fldWTotal.Top = 2F;
			this.fldWTotal.Width = 0.9375F;
			// 
			// fldSOv
			// 
			this.fldSOv.Height = 0.1875F;
			this.fldSOv.Left = 4.9375F;
			this.fldSOv.Name = "fldSOv";
			this.fldSOv.OutputFormat = resources.GetString("fldSOv.OutputFormat");
			this.fldSOv.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSOv.Text = null;
			this.fldSOv.Top = 0.625F;
			this.fldSOv.Width = 0.9375F;
			// 
			// fldSFlat
			// 
			this.fldSFlat.Height = 0.1875F;
			this.fldSFlat.Left = 4.9375F;
			this.fldSFlat.Name = "fldSFlat";
			this.fldSFlat.OutputFormat = resources.GetString("fldSFlat.OutputFormat");
			this.fldSFlat.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSFlat.Text = null;
			this.fldSFlat.Top = 0.8125F;
			this.fldSFlat.Width = 0.9375F;
			// 
			// fldSUnits
			// 
			this.fldSUnits.Height = 0.1875F;
			this.fldSUnits.Left = 4.9375F;
			this.fldSUnits.Name = "fldSUnits";
			this.fldSUnits.OutputFormat = resources.GetString("fldSUnits.OutputFormat");
			this.fldSUnits.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSUnits.Text = null;
			this.fldSUnits.Top = 1F;
			this.fldSUnits.Width = 0.9375F;
			// 
			// fldSCons
			// 
			this.fldSCons.Height = 0.1875F;
			this.fldSCons.Left = 4.9375F;
			this.fldSCons.Name = "fldSCons";
			this.fldSCons.OutputFormat = resources.GetString("fldSCons.OutputFormat");
			this.fldSCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSCons.Text = null;
			this.fldSCons.Top = 1.1875F;
			this.fldSCons.Width = 0.9375F;
			// 
			// fldSMisc
			// 
			this.fldSMisc.Height = 0.1875F;
			this.fldSMisc.Left = 4.9375F;
			this.fldSMisc.Name = "fldSMisc";
			this.fldSMisc.OutputFormat = resources.GetString("fldSMisc.OutputFormat");
			this.fldSMisc.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSMisc.Text = null;
			this.fldSMisc.Top = 1.375F;
			this.fldSMisc.Width = 0.9375F;
			// 
			// fldSAdj
			// 
			this.fldSAdj.Height = 0.1875F;
			this.fldSAdj.Left = 4.9375F;
			this.fldSAdj.Name = "fldSAdj";
			this.fldSAdj.OutputFormat = resources.GetString("fldSAdj.OutputFormat");
			this.fldSAdj.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSAdj.Text = null;
			this.fldSAdj.Top = 1.5625F;
			this.fldSAdj.Width = 0.9375F;
			// 
			// fldSTax
			// 
			this.fldSTax.Height = 0.1875F;
			this.fldSTax.Left = 4.9375F;
			this.fldSTax.Name = "fldSTax";
			this.fldSTax.OutputFormat = resources.GetString("fldSTax.OutputFormat");
			this.fldSTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSTax.Text = null;
			this.fldSTax.Top = 1.75F;
			this.fldSTax.Width = 0.9375F;
			// 
			// fldSTotal
			// 
			this.fldSTotal.Height = 0.1875F;
			this.fldSTotal.Left = 4.9375F;
			this.fldSTotal.Name = "fldSTotal";
			this.fldSTotal.OutputFormat = resources.GetString("fldSTotal.OutputFormat");
			this.fldSTotal.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSTotal.Text = null;
			this.fldSTotal.Top = 2F;
			this.fldSTotal.Width = 0.9375F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 3.9375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label10.Text = "Override";
			this.Label10.Top = 0.625F;
			this.Label10.Width = 1F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 3.9375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label11.Text = "Flat";
			this.Label11.Top = 0.8125F;
			this.Label11.Width = 1F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.9375F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label12.Text = "Units";
			this.Label12.Top = 1F;
			this.Label12.Width = 1F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 3.9375F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label13.Text = "Consumption";
			this.Label13.Top = 1.1875F;
			this.Label13.Width = 1F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.9375F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label14.Text = "Miscellaneous";
			this.Label14.Top = 1.375F;
			this.Label14.Width = 1F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 3.9375F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label15.Text = "Adjustments";
			this.Label15.Top = 1.5625F;
			this.Label15.Width = 1F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 3.9375F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label16.Text = "Tax";
			this.Label16.Top = 1.75F;
			this.Label16.Width = 1F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 3.9375F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label17.Text = "Total";
			this.Label17.Top = 2F;
			this.Label17.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1.9375F;
			this.Line1.Width = 4.75F;
			this.Line1.X1 = 1.375F;
			this.Line1.X2 = 6.125F;
			this.Line1.Y1 = 1.9375F;
			this.Line1.Y2 = 1.9375F;
			// 
			// lblWaterCalc
			// 
			this.lblWaterCalc.Height = 0.25F;
			this.lblWaterCalc.HyperLink = null;
			this.lblWaterCalc.Left = 1.75F;
			this.lblWaterCalc.Name = "lblWaterCalc";
			this.lblWaterCalc.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.lblWaterCalc.Text = "Water";
			this.lblWaterCalc.Top = 0.3125F;
			this.lblWaterCalc.Width = 1.9375F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.25F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 3.9375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.Label19.Text = "Sewer";
			this.Label19.Top = 0.3125F;
			this.Label19.Width = 1.9375F;
			// 
			// Line2
			// 
			this.Line2.Height = 1.875F;
			this.Line2.Left = 3.75F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.3125F;
			this.Line2.Width = 0F;
			this.Line2.X1 = 3.75F;
			this.Line2.X2 = 3.75F;
			this.Line2.Y1 = 0.3125F;
			this.Line2.Y2 = 2.1875F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.1875F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 0F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center; text-decoration: un" + "derline; ddo-char-set: 0";
			this.Label20.Text = "User Category Summary";
			this.Label20.Top = 2.4375F;
			this.Label20.Width = 7.5F;
			// 
			// lblWaterSum
			// 
			this.lblWaterSum.Height = 0.1875F;
			this.lblWaterSum.HyperLink = null;
			this.lblWaterSum.Left = 0.0625F;
			this.lblWaterSum.Name = "lblWaterSum";
			this.lblWaterSum.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.lblWaterSum.Text = "Water";
			this.lblWaterSum.Top = 2.625F;
			this.lblWaterSum.Width = 3.625F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.1875F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 3.8125F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.Label22.Text = "Sewer";
			this.Label22.Top = 2.625F;
			this.Label22.Width = 3.625F;
			// 
			// lbl
			// 
			this.lbl.Height = 0.1875F;
			this.lbl.HyperLink = null;
			this.lbl.Left = 0.0625F;
			this.lbl.Name = "lbl";
			this.lbl.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lbl.Text = "Category";
			this.lbl.Top = 2.8125F;
			this.lbl.Width = 1.5625F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 1.625F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label23.Text = "Count";
			this.Label23.Top = 2.8125F;
			this.Label23.Width = 0.5F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 2.125F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label24.Text = "Cons";
			this.Label24.Top = 2.8125F;
			this.Label24.Width = 0.625F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 2.75F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label25.Text = "Amount";
			this.Label25.Top = 2.8125F;
			this.Label25.Width = 0.9375F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 3.8125F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label26.Text = "Category";
			this.Label26.Top = 2.8125F;
			this.Label26.Width = 1.5625F;
			// 
			// Label27
			// 
			this.Label27.Height = 0.1875F;
			this.Label27.HyperLink = null;
			this.Label27.Left = 5.375F;
			this.Label27.Name = "Label27";
			this.Label27.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label27.Text = "Count";
			this.Label27.Top = 2.8125F;
			this.Label27.Width = 0.5F;
			// 
			// Label28
			// 
			this.Label28.Height = 0.1875F;
			this.Label28.HyperLink = null;
			this.Label28.Left = 5.875F;
			this.Label28.Name = "Label28";
			this.Label28.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label28.Text = "Cons";
			this.Label28.Top = 2.8125F;
			this.Label28.Width = 0.625F;
			// 
			// Label29
			// 
			this.Label29.Height = 0.1875F;
			this.Label29.HyperLink = null;
			this.Label29.Left = 6.5F;
			this.Label29.Name = "Label29";
			this.Label29.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label29.Text = "Amount";
			this.Label29.Top = 2.8125F;
			this.Label29.Width = 0.9375F;
			// 
			// vsWSum
			// 
			this.CustomVsWSum.Height = 0.1875F;
			this.CustomVsWSum.Left = 0.0625F;
			this.CustomVsWSum.Name = "vsWSum";
			this.CustomVsWSum.Top = 3F;
			this.CustomVsWSum.Width = 3.625F;
			this.CustomVsWSum.Type = typeof(fecherFoundation.FCReportGrid);
			// 
			// vsSSum
			// 
			this.CustomVsSSum.Height = 0.1875F;
			this.CustomVsSSum.Left = 3.8125F;
			this.CustomVsSSum.Name = "vsSSum";
			this.CustomVsSSum.Top = 3F;
			this.CustomVsSSum.Width = 3.625F;
			this.CustomVsSSum.Type = typeof(fecherFoundation.FCReportGrid);
			// 
			// lnCat
			// 
			this.lnCat.Height = 0.5625F;
			this.lnCat.Left = 3.75F;
			this.lnCat.LineWeight = 1F;
			this.lnCat.Name = "lnCat";
			this.lnCat.Top = 2.625F;
			this.lnCat.Width = 0F;
			this.lnCat.X1 = 3.75F;
			this.lnCat.X2 = 3.75F;
			this.lnCat.Y1 = 2.625F;
			this.lnCat.Y2 = 3.1875F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 1.375F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 2.1875F;
			this.Line3.Width = 4.75F;
			this.Line3.X1 = 1.375F;
			this.Line3.X2 = 6.125F;
			this.Line3.Y1 = 2.1875F;
			this.Line3.Y2 = 2.1875F;
			// 
			// Line4
			// 
			this.Line4.Height = 1.875F;
			this.Line4.Left = 1.375F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.3125F;
			this.Line4.Width = 0F;
			this.Line4.X1 = 1.375F;
			this.Line4.X2 = 1.375F;
			this.Line4.Y1 = 2.1875F;
			this.Line4.Y2 = 0.3125F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 1.375F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.3125F;
			this.Line5.Width = 4.75F;
			this.Line5.X1 = 1.375F;
			this.Line5.X2 = 6.125F;
			this.Line5.Y1 = 0.3125F;
			this.Line5.Y2 = 0.3125F;
			// 
			// Line6
			// 
			this.Line6.Height = 1.875F;
			this.Line6.Left = 6.125F;
			this.Line6.LineWeight = 1F;
			this.Line6.Name = "Line6";
			this.Line6.Top = 0.3125F;
			this.Line6.Width = 0F;
			this.Line6.X1 = 6.125F;
			this.Line6.X2 = 6.125F;
			this.Line6.Y1 = 2.1875F;
			this.Line6.Y2 = 0.3125F;
			// 
			// Line7
			// 
			this.Line7.Height = 0F;
			this.Line7.Left = 1.375F;
			this.Line7.LineWeight = 1F;
			this.Line7.Name = "Line7";
			this.Line7.Top = 0.5625F;
			this.Line7.Width = 4.75F;
			this.Line7.X1 = 1.375F;
			this.Line7.X2 = 6.125F;
			this.Line7.Y1 = 0.5625F;
			this.Line7.Y2 = 0.5625F;
			// 
			// lblHiddenSummaryHeader
			// 
			this.lblHiddenSummaryHeader.Height = 0.25F;
			this.lblHiddenSummaryHeader.HyperLink = null;
			this.lblHiddenSummaryHeader.Left = 0F;
			this.lblHiddenSummaryHeader.Name = "lblHiddenSummaryHeader";
			this.lblHiddenSummaryHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.lblHiddenSummaryHeader.Text = "Calculation Summary Report";
			this.lblHiddenSummaryHeader.Top = 0F;
			this.lblHiddenSummaryHeader.Visible = false;
			this.lblHiddenSummaryHeader.Width = 7.5F;
			// 
			// arCalculationSummary
			// 
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWOv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWFlat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSOv)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSFlat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSAdj)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterCalc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterSum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHiddenSummaryHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWOv;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWFlat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWMisc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWAdj;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSOv;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSFlat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSMisc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSAdj;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWaterCalc;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWaterSum;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label27;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label28;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label29;
		private GrapeCity.ActiveReports.SectionReportModel.CustomControl CustomVsWSum;
		private GrapeCity.ActiveReports.SectionReportModel.CustomControl CustomVsSSum;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnCat;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line6;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHiddenSummaryHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
