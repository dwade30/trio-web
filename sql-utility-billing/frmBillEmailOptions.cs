﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBillEmailOptions.
	/// </summary>
	public partial class frmBillEmailOptions : BaseForm
	{
		public frmBillEmailOptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillEmailOptions InstancePtr
		{
			get
			{
				return (frmBillEmailOptions)Sys.GetInstance(typeof(frmBillEmailOptions));
			}
		}

		protected frmBillEmailOptions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int IDCol;
		int AccountCol;
		int NameCol;
		int EmailCol;
		int SelectCol;
		clsDRWrapper rsInfo = new clsDRWrapper();
		int lngRateKeyToPrint;
		string strListOfBooks;
		clsDRWrapper rsSettings = new clsDRWrapper();

		public void Init(ref int lngRateKey, ref string strBookList)
		{
			lngRateKeyToPrint = lngRateKey;
			strListOfBooks = strBookList;
			this.Show(App.MainForm);
		}

		private void cmdClearAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= fgrdAccounts.Rows - 1; counter++)
			{
				fgrdAccounts.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void cmdSelectAll_Click(object sender, System.EventArgs e)
		{
			int counter;
			for (counter = 1; counter <= fgrdAccounts.Rows - 1; counter++)
			{
				fgrdAccounts.TextMatrix(counter, SelectCol, FCConvert.ToString(true));
			}
		}

		private void frmBillEmailOptions_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmBillEmailOptions_Load(object sender, System.EventArgs e)
		{
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			FormatSelectionGrid();
			FillGrid();
			if (modUTStatusPayments.Statics.TownService == "B")
			{
				//optWaterSewer[2].Checked = true;
				cmbWaterSewer.Text = "Both";
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				cmbWaterSewer.Clear();
				cmbWaterSewer.Items.Add("Sewer");
				cmbWaterSewer.Text = "Sewer";
			}
			else if (modUTStatusPayments.Statics.TownService == "W")
			{
				cmbWaterSewer.Clear();
				cmbWaterSewer.Items.Add("Water");
				cmbWaterSewer.Text = "Water";
			}
		}

		private void FillGrid()
		{
			string strAccts;
			clsDRWrapper rsBills = new clsDRWrapper();
			cPartyController pc = new cPartyController();
			cParty tempParty;
			strAccts = "(";
			rsBills.OpenRecordset("SELECT DISTINCT AccountKey FROM Bill WHERE BillNumber = " + FCConvert.ToString(lngRateKeyToPrint) + " AND (" + strListOfBooks + ")", "TWUT0000.vb1");
			if (rsBills.EndOfFile() != true && rsBills.BeginningOfFile() != true)
			{
				do
				{
					strAccts += rsBills.Get_Fields_Int32("AccountKey") + ", ";
					rsBills.MoveNext();
				}
				while (rsBills.EndOfFile() != true);
			}
			if (strAccts != "(")
			{
				strAccts = Strings.Left(strAccts, strAccts.Length - 2) + ")";
				rsInfo.OpenRecordset("SELECT * FROM Master WHERE ID IN " + strAccts + " AND ISNULL(Email,'') <> ''", "TWUT0000.vb1");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						tempParty = pc.GetParty(rsInfo.Get_Fields_Int32("billingpartyid"), true);
						if (!(tempParty == null))
						{
							fgrdAccounts.Rows += 1;
							fgrdAccounts.TextMatrix(fgrdAccounts.Rows - 1, IDCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							fgrdAccounts.TextMatrix(fgrdAccounts.Rows - 1, AccountCol, FCConvert.ToString(rsInfo.Get_Fields("AccountNumber")));							
                            if (rsInfo.Get_Fields_Boolean("SameBillOwner"))
                            {
                                fgrdAccounts.TextMatrix(fgrdAccounts.Rows - 1, NameCol,
                                    rsInfo.Get_Fields_String("Deedname1"));
                            }
                            else
                            {
                                fgrdAccounts.TextMatrix(fgrdAccounts.Rows - 1, NameCol, tempParty.FullName);
                            }
							fgrdAccounts.TextMatrix(fgrdAccounts.Rows - 1, EmailCol, FCConvert.ToString(rsInfo.Get_Fields_String("Email")));
							if (rsInfo.Get_Fields_Boolean("EmailBill") == true)
							{
								fgrdAccounts.TextMatrix(fgrdAccounts.Rows - 1, SelectCol, FCConvert.ToString(true));
							}
							else
							{
								fgrdAccounts.TextMatrix(fgrdAccounts.Rows - 1, SelectCol, FCConvert.ToString(false));
							}
						}
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				else
				{
					MessageBox.Show("No accounts in the selected rate key and books have been set up with E-Mail addresses.  You will need to do so before you may continue.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
			else
			{
				MessageBox.Show("No bills were found in the selected rate key and books.", "No Bills", MessageBoxButtons.OK, MessageBoxIcon.Information);
				// kk09302014 trout-624  "Key IN ()"  throws syntax error in Access so show warning
			}
		}

		private void FormatSelectionGrid()
		{
			IDCol = 0;
			AccountCol = 1;
			NameCol = 2;
			EmailCol = 3;
			SelectCol = 4;
			fgrdAccounts.TextMatrix(0, AccountCol, "Account #");
			fgrdAccounts.TextMatrix(0, NameCol, "Name");
			fgrdAccounts.TextMatrix(0, EmailCol, "E-Mail Address");
			fgrdAccounts.TextMatrix(0, SelectCol, "Select");
			fgrdAccounts.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			fgrdAccounts.ColHidden(IDCol, true);
			fgrdAccounts.ColWidth(AccountCol, FCConvert.ToInt32(fgrdAccounts.WidthOriginal * 0.15));
			fgrdAccounts.ColWidth(NameCol, FCConvert.ToInt32(fgrdAccounts.WidthOriginal * 0.35));
			fgrdAccounts.ColWidth(EmailCol, FCConvert.ToInt32(fgrdAccounts.WidthOriginal * 0.35));
			fgrdAccounts.ColAlignment(AccountCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			fgrdAccounts.ColAlignment(SelectCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			fgrdAccounts.ExtendLastCol = true;
		}

		private void frmBillEmailOptions_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmBillEmailOptions_Resize(object sender, System.EventArgs e)
		{
			ResizeGrid();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void fgrdAccounts_ClickEvent(object sender, System.EventArgs e)
		{
			int counter;
			if (fgrdAccounts.Row > 0)
			{
				if (FCConvert.CBool(fgrdAccounts.TextMatrix(fgrdAccounts.Row, SelectCol)) == true)
				{
					fgrdAccounts.TextMatrix(fgrdAccounts.Row, SelectCol, FCConvert.ToString(false));
				}
				else
				{
					fgrdAccounts.TextMatrix(fgrdAccounts.Row, SelectCol, FCConvert.ToString(true));
				}
			}
		}

		private void fgrdAccounts_KeyDownEvent(object sender, KeyEventArgs e)
		{
			int counter;
			Keys KeyCode = e.KeyCode;
			if (fgrdAccounts.Row > 0)
			{
				if (KeyCode == Keys.Space)
				{
					KeyCode = 0;
					if (FCConvert.CBool(fgrdAccounts.TextMatrix(fgrdAccounts.Row, SelectCol)) == true)
					{
						fgrdAccounts.TextMatrix(fgrdAccounts.Row, SelectCol, FCConvert.ToString(false));
					}
					else
					{
						fgrdAccounts.TextMatrix(fgrdAccounts.Row, SelectCol, FCConvert.ToString(true));
					}
				}
			}
		}

		private bool ValidateChoices()
		{
			bool ValidateChoices = false;
			int counter;
			bool blnFound;
			if (Strings.Trim(txtEmailMessage.Text) == "")
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("You must enter an email message before you may continue.", "No Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
				ValidateChoices = false;
				return ValidateChoices;
			}
			blnFound = false;
			for (counter = 1; counter <= fgrdAccounts.Rows - 1; counter++)
			{
				//Application.DoEvents();
				if (FCConvert.CBool(fgrdAccounts.TextMatrix(counter, SelectCol)) == true)
				{
					blnFound = true;
					break;
				}
			}
			if (!blnFound)
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("You must select at least 1 account before you may continue.", "No Accounts Selected", MessageBoxButtons.OK, MessageBoxIcon.Information);
				ValidateChoices = false;
			}
			else
			{
				ValidateChoices = true;
			}
			return ValidateChoices;
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			string strSql;
			string strOPSQL = "";
			clsCustomUTBill CBill = new clsCustomUTBill();
			string strWhere;
			string strList = "";
			int x;
			string strOrderBy = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			int intType;
			DateTime dtStatementDate;
			string strSQLTemp = "";
			string strBillFields;
			int counter;
			GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport pe = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
			string strDirectory = "";
			string strBillQryFields;
			//frmBusy bWait = null;
			FCUtils.StartTask(this, () =>
			{
				/*- bWait = null; */
				strBillQryFields = "BillID as bill, qTmp.AccountKey, ActualAccountNumber, BillNumber, MeterKey, Book, qTmp.Service, BillStatus, BillingRateKey, qTmp.NoBill, CombinationCode, CurDate, CurReading, CurCode, PrevDate, PrevReading, PrevCode";
				strBillQryFields += ", Consumption, WaterOverrideCons, WaterOverrideAmount, SewerOverrideCons, SewerOverrideAmount, WMiscAmount, WAdjustAmount, WDEAdjustAmount, WFlatAmount, WUnitsAmount, WConsumptionAmount";
				strBillQryFields += ", SMiscAmount, SAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount, WTax, STax, TotalWBillAmount, TotalSBillAmount";
				strBillQryFields += ", WIntPaidDate, WPrinOwed, WTaxOwed, WIntOwed, WIntAdded, WCostOwed, WCostAdded, WPrinPaid, WTaxPaid, WIntPaid, WCostPaid, SIntPaidDate, SPrinOwed, STaxOwed, SIntOwed, SIntAdded, SCostOwed, SCostAdded, SPrinPaid, STaxPaid, SIntPaid, SCostPaid";
				strBillQryFields += ", WRT1, WRT2, WRT3, WRT4, WRT5, SRT1, SRT2, SRT3, SRT4, SRT5, qTmp.Location, BName, BName2, BAddress1, BAddress2, BAddress3, BCity, BState, BZip, BZip4, OName, OName2, OAddress1, OAddress2, OAddress3, OCity, OState, OZip, OZip4";
				strBillQryFields += ", Note, MapLot, BookPage, Telephone, Email, qTmp.WCat, qTmp.SCat, BillMessage, qTmp.FinalEndDate, qTmp.FinalStartDate, qTmp.FinalBillDate, qTmp.Final, WBillOwner, SBillOwner, BillDate, ReadingUnits";
				strBillQryFields += ", WOrigBillAmount, SOrigBillAmount, SHasOverride , WHasOverride, SDemandGroupID, WDemandGroupID, SendEBill";
				// MAL@20080806: Specify which fields get pulled from the Bill table
				// Tracker Reference: 12862
				strBillFields = " Bill.id as BillID, Bill.AccountKey, ActualAccountNumber, BillNumber, Bill.MeterKey, Bill.Book, Bill.Service, Bill.BillStatus, Bill.BillingRateKey, Bill.NoBill";
				strBillFields += ", Bill.CombinationCode, Bill.CurDate, Bill.CurReading, Bill.CurCode, Bill.PrevDate, Bill.PrevReading, Bill.PrevCode, Bill.Consumption";
				strBillFields += ", WaterOverrideCons, WaterOverrideAmount, SewerOverrideCons, SewerOverrideAmount, WMiscAmount";
				strBillFields += ", WAdjustAmount, WDEAdjustAmount, WFlatAmount, WUnitsAmount, WConsumptionAmount, SMiscAmount, SAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount";
				strBillFields += ", WTax, STax, TotalWBillAmount, TotalSBillAmount, WIntPaidDate, WPrinOwed, WTaxOwed, WIntOwed, WIntAdded, WCostOwed, WCostAdded, WPrinPaid, WTaxPaid, WIntPaid, WCostPaid";
				strBillFields += ", SIntPaidDate, SPrinOwed, STaxOwed, SIntOwed, SIntAdded, SCostOwed, SCostAdded, SPrinPaid, STaxPaid, SIntPaid, SCostPaid";
				strBillFields += ", WRT1, WRT2, WRT3, WRT4, WRT5, SRT1, SRT2, SRT3, SRT4, SRT5, Bill.Location";
				strBillFields += ", Bill.BName, Bill.BName2, Bill.BAddress1, Bill.BAddress2, Bill.BAddress3, Bill.BCity, Bill.BState, Bill.BZip, Bill.BZip4, Bill.OName, Bill.OName2, Bill.OAddress1, Bill.OAddress2, Bill.OAddress3, Bill.OCity, Bill.OState, Bill.OZip, Bill.OZip4";
				strBillFields += ", Bill.Note, Bill.MapLot, Bill.BookPage, Bill.Telephone, Bill.Email, Bill.WCat, Bill.SCat, Bill.BillMessage, Bill.FinalEndDate, Bill.FinalStartDate, Bill.FinalBillDate, Bill.Final";
				strBillFields += ", WBillOwner, SBillOwner, BillDate, ReadingUnits, WOrigBillAmount, SOrigBillAmount, SHasOverride, WHasOverride, SDemandGroupID, WDemandGroupID, SendEBill";
				try
				{
					// On Error GoTo ErrorHandler
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
					//Application.DoEvents();
					if (!ValidateChoices())
					{
						return;
					}
					for (x = 1; x <= fgrdAccounts.Rows - 1; x++)
					{
						//Application.DoEvents();
						if (FCConvert.CBool(fgrdAccounts.TextMatrix(x, SelectCol)) == true)
						{
							strList += FCConvert.ToString(Conversion.Val(fgrdAccounts.TextMatrix(x, AccountCol))) + ",";
						}
					}
					// x
					if (strList != string.Empty)
					{
						strList = Strings.Mid(strList, 1, strList.Length - 1);
					}
					strWhere = " AND ActualAccountNumber in (" + strList + ") ";
					strSql = " (" + strListOfBooks + ") ";
					if (cmbWaterSewer.Text != "Both")
					{
						strSql = "select bill.id as bill, " + strBillFields + ",master.accountnumber,METERtable.sequence, tblIConnectInfo.AcceptEbill FROM (metertable INNER JOIN (bill INNER JOIN master ON bill.AccountKey = master.Id) ON metertable.Id = bill.MeterKey) INNER JOIN tblIConnectInfo ON bill.AccountKey = tblIConnectInfo.AccountKey WHERE billstatus = 'B' and billingratekey = " + FCConvert.ToString(lngRateKeyToPrint) + strWhere + " AND " + strSql + strOrderBy;
						strOPSQL = strSql;
					}
					else
					{
						modUTBilling.Statics.gstrQryCombinedBillInfo = "SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPECOMBINED) + " as PrintBillType, " + strBillFields + " FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'O') WHERE (WBillOwner = 1 AND SBillOwner = 1) AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL) " + "UNION ALL SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPECOMBINED) + " AS PrintBillType, " + strBillFields + " FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'T') WHERE (WBillOwner = 0 AND SBillOwner = 0) AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL)";
						modUTBilling.Statics.gstrQryWaterBillOwnerInfo = "SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPESEWER) + " as PrintBillType, " + strBillFields + " FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'T') WHERE WBillOwner = 1 AND SBillOwner = 0 AND Service <> 'W' AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL) " + "UNION ALL SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPEWATER) + " AS PrintBillType, " + strBillFields + " FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'O') WHERE WBillOwner = 1 AND SBillOwner = 0 AND Service <> 'S' AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL)";
						modUTBilling.Statics.gstrQrySewerBillOwnerInfo = "SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPESEWER) + " as PrintBillType, " + strBillFields + " from Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'O') WHERE WBillOwner = 0 AND SBillOwner = 1 AND Service <> 'W' AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL) " + "UNION ALL SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPEWATER) + " AS PrintBillType, " + strBillFields + " FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'T') WHERE WBillOwner = 0 AND SBillOwner = 1 AND Service <> 'S' AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL)";
						modUTBilling.Statics.gstrQryCombinedSeparateBillInfo = "SELECT * from (" + modUTBilling.Statics.gstrQrySewerBillOwnerInfo + ") AS qTmpS union all select * from (" + modUTBilling.Statics.gstrQryWaterBillOwnerInfo + ") AS qTmpW";
						modUTBilling.Statics.gstrQryPrintBill = "SELECT * from (" + modUTBilling.Statics.gstrQryCombinedSeparateBillInfo + ") AS qTmpSep union all select * from (" + modUTBilling.Statics.gstrQryCombinedBillInfo + ") AS qTmpComb";
						strOPSQL = "SELECT PrintBillType, " + strBillQryFields + " ,MeterTable.Sequence from MeterTable INNER JOIN (" + modUTBilling.Statics.gstrQryPrintBill + ") AS qTmp on MeterTable.ID = qTmp.MeterKey WHERE BillStatus = 'B' AND Combine = 'N' and BillingRateKey = " + FCConvert.ToString(lngRateKeyToPrint) + strWhere + " and PrintBillType <> 2 AND " + strSql + strOrderBy;
						strSql = "SELECT PrintBillType, " + strBillQryFields + " ,MeterTable.Sequence from MeterTable INNER JOIN (" + modUTBilling.Statics.gstrQryPrintBill + ") AS qTmp on MeterTable.ID = qTmp.MeterKey WHERE BillStatus = 'B' AND Combine = 'N' and BillingRateKey = " + FCConvert.ToString(lngRateKeyToPrint) + strWhere + " and " + strSql + strOrderBy;
					}
					frmWait.InstancePtr.Unload();
					CBill.FormatID = frmBillParameters.InstancePtr.Init(cmbWaterSewer.Text == "Both");
					if (CBill.FormatID > 0)
					{
						CBill.PrinterName = "";
						CBill.DBFile = "TWUT0000.vb1";
						CBill.Module = "UT";
						CBill.SQL = strSql;
						if (cmbWaterSewer.Text == "Water")
						{
							CBill.UseWaterBills = true;
							CBill.UseSewerBills = false;
						}
						else if (cmbWaterSewer.Text == "Sewer")
						{
							CBill.UseWaterBills = false;
							CBill.UseSewerBills = true;
						}
						else
						{
							CBill.UseWaterBills = true;
							CBill.UseSewerBills = true;
						}
						CBill.SkipZeroNegativeBills = false;
						rptCustomBill.InstancePtr.Unload();
						rptCustomBill.InstancePtr.Init(CBill, true, true);
						rptCustomBill.InstancePtr.Run(false);
						strDirectory = FCFileSystem.Statics.UserDataFolder;
						if (Strings.Right(strDirectory, 1) != "\\")
						{
							strDirectory += "\\";
						}
						this.ShowWait();
                        //FC:FINAL:SBE - #3947 - store pages tag in a separate list
                        List<string> pagesTag = rptCustomBill.InstancePtr.UserData as List<string>;
                        for (counter = 0; counter <= rptCustomBill.InstancePtr.Document.Pages.Count - 1; counter++)
						{
							// Debug.Print counter & vbTab & rptCustomBill.Pages(counter).Tag
							pe = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
							//Application.DoEvents();
							string fileName = strDirectory + "EBill.pdf";
							pe.Export(rptCustomBill.InstancePtr.Document, fileName, FCConvert.ToString(counter + 1) + "-" + "1");
                            //FC:FINAL:SBE - #3947 - store pages tag in a separate list
                            //frmEMail.InstancePtr.Init(fileName, FCConvert.ToString(rptCustomBill.InstancePtr.UserData), "Utility E-Bill", txtEmailMessage.Text, false, false, true, true, true, false, showAsModalForm: true);
                            frmEMail.InstancePtr.Init(fileName, pagesTag[counter], "Utility E-Bill", txtEmailMessage.Text, false, false, true, true, true, false, showAsModalForm: true);
                        }
						this.EndWait();
						MessageBox.Show("The selected accounts have been processed.", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
						// DJW@01092013 TROUT-624 Added summary report for email bills option
						rptEmailSummaryReport.InstancePtr.strSQL = strSql;
						frmReportViewer.InstancePtr.Init(rptEmailSummaryReport.InstancePtr);
						return;
					}
					return;
				}
				catch (Exception ex)
				{
					// ErrorHandler:
					frmWait.InstancePtr.Unload();
					this.EndWait();
					MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MnuSaveContinue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			});
		}

		private void ResizeGrid()
		{
			int lngGridWidth;
			//FC:FINAL:CHN: Change using "Width" property on "WidthOriginal" to get same behavior with original app.
			// lngGridWidth = fgrdAccounts.Width;
			lngGridWidth = fgrdAccounts.WidthOriginal;
			fgrdAccounts.ColWidth(AccountCol, FCConvert.ToInt32(lngGridWidth * 0.15));
			fgrdAccounts.ColWidth(NameCol, FCConvert.ToInt32(lngGridWidth * 0.35));
			fgrdAccounts.ColWidth(EmailCol, FCConvert.ToInt32(lngGridWidth * 0.35));
		}
	}
}
