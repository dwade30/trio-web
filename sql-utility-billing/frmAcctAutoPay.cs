//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmAcctAutoPay.
	/// </summary>
	public partial class frmAcctAutoPay : BaseForm
	{

		//=========================================================

		clsDRWrapper rsACH = new/*AsNew*/ clsDRWrapper();
		int lngAcctKey;
		int lngRecID;
		string strService = "";
		string strAcctService;
		bool boolDirty;

		// vbPorter upgrade warning: strPassAcctNum As string	OnWrite(double)
		public void Init( int lngPassAcctKey,  string strPassAcctNum,  string strPassService)
		{
			lngAcctKey = lngPassAcctKey;
			strAcctService = strPassService;

			boolDirty = false;

			this.Show(FCForm.FormShowEnum.Modal, App.MainForm);
		}

		private void frmAcctAutoPay_Load(object sender, System.EventArgs e)
		{
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            loadAutoPay();
		}

		private void frmAcctAutoPay_Unload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intTemp As short	OnWrite(DialogResult)
			

			if (boolDirty) {
				var answer = MessageBox.Show("Do you want to save the ACH information?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (answer == DialogResult.Yes) { // yes
					// save it and exit
					SaveAutoPay();
				} else if (answer == DialogResult.No) { // no
					// exit w/o saving
				} else if (answer == DialogResult.Cancel) { // cancel
					// go back to the screen and
					e.Cancel = true;
					return;
				}
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (SaveAutoPay()) {
				MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Unload();
			}
		}

		private void mnuProcessExit_Click(object sender, System.EventArgs e)
		{
			Unload();
		}
		public void mnuProcessExit_Click()
		{
			mnuProcessExit_Click(mnuProcessExit, new System.EventArgs());
		}


		private void frmAcctAutoPay_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = ((int)e.KeyData) / 0x10000;

			switch (KeyCode) {
				
				case Keys.Escape:
				{
					KeyCode = (Keys)0;
					mnuProcessExit_Click();
					break;
				}
			} //end switch
		}

		private void loadAutoPay()
		{
			try
			{	// On Error GoTo ErrorHandler

				string strWhere = "";

				rsACH.OpenRecordset("SELECT TOP 1 * FROM tblAcctACH WHERE AccountKey = "+FCConvert.ToString(lngAcctKey)+" ORDER BY Service", modExtraModules.strUTDatabase);
				if (!rsACH.EndOfFile()) {
					lngRecID = Convert.ToInt32(rsACH.Get_Fields("ID"));

					strService = FCConvert.ToString(rsACH.Get_Fields("Service"));

					txtTransitRouting.Text = FCConvert.ToString(rsACH.Get_Fields("ACHBankRT"));
					txtAccountNumber.Text = FCConvert.ToString(rsACH.Get_Fields("ACHAcctNumber"));

                    if (Convert.ToInt32(rsACH.Get_Fields("ACHAcctType")) == 3)
                    {
                        //savings
                        cmbAccountType.SelectedIndex = 1;
                    }
                    else
                    {
                        cmbAccountType.SelectedIndex = 0;
                    }
                    

					if (Convert.ToInt32(rsACH.Get_Fields("ACHLimit")) != 0) {
						txtLimit.Text = FCConvert.ToString(rsACH.Get_Fields("ACHLimit"));
					} else {
						txtLimit.Text = "";
					}

					if (FCConvert.ToBoolean(rsACH.Get_Fields("ACHPreNote"))) {
						chkPreNote.CheckState = Wisej.Web.CheckState.Checked;
					} else {
						chkPreNote.CheckState = Wisej.Web.CheckState.Unchecked;
					}
					if (FCConvert.ToBoolean(rsACH.Get_Fields("ACHActive"))) {
						chkActive.CheckState = Wisej.Web.CheckState.Checked;
					} else {
						chkActive.CheckState = Wisej.Web.CheckState.Unchecked;
					}
				} else {
					lngRecID = 0;

					strService = strAcctService;

					txtTransitRouting.Text = "";
					txtAccountNumber.Text = "";

                    cmbAccountType.SelectedIndex = 0;

					txtLimit.Text = "";
					chkPreNote.CheckState = Wisej.Web.CheckState.Checked;
					chkActive.CheckState = Wisej.Web.CheckState.Checked;
				}

				boolDirty = false;

				return;
			}
			catch (Exception ex)
			{	// ErrorHandler:
				MessageBox.Show("Error Number "+FCConvert.ToString(fecherFoundation.Information.Err(ex).Number)+"  "+fecherFoundation.Information.Err(ex).Description+"\n"+"In LoadAutoPay", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool SaveAutoPay()
		{
            try
            {
                if (txtTransitRouting.Text.Length < 9)
                {
                    MessageBox.Show("Routing Number must be 9 digits.", "Invalid Data", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }

                if (txtAccountNumber.Text == "")
                {
                    MessageBox.Show("Account Number is required.", "Invalid Data", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }

                if (fecherFoundation.Conversion.Val(txtLimit.Text) < 0)
                {
                    MessageBox.Show("Payment Limit cannot be less than zero.", "Invalid Data", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return false;
                }

                rsACH.OpenRecordset("SELECT * FROM tblAcctACH WHERE ID = " + FCConvert.ToString(lngRecID),
                    modExtraModules.strUTDatabase);
                if (rsACH.EndOfFile())
                {
                    rsACH.AddNew();

                    rsACH.Set_Fields("AccountKey", lngAcctKey);
                }
                else
                {
                    rsACH.Edit();
                }

                rsACH.Set_Fields("Service", strService);

                rsACH.Set_Fields("ACHBankRT", txtTransitRouting.Text);
                rsACH.Set_Fields("ACHAcctNumber", txtAccountNumber.Text);
                if (cmbAccountType.Text.ToLower().Trim() == "checking")
                {
                    rsACH.Set_Fields("ACHAcctType", 2);
                }
                else if (cmbAccountType.Text.ToLower().Trim()== "savings") 
                {
                    rsACH.Set_Fields("ACHAcctType", 3);
                }

                rsACH.Set_Fields("ACHLimit", FCConvert.ToString(fecherFoundation.Conversion.Val(txtLimit.Text)));
                if (chkPreNote.CheckState == Wisej.Web.CheckState.Checked)
                {
                    rsACH.Set_Fields("ACHPreNote", true);
                }
                else
                {
                    rsACH.Set_Fields("ACHPreNote", false);
                }

                if (chkActive.CheckState == Wisej.Web.CheckState.Checked)
                {
                    rsACH.Set_Fields("ACHActive", true);
                }
                else
                {
                    rsACH.Set_Fields("ACHActive", false);
                }

                rsACH.Update();
                lngRecID = Convert.ToInt32(rsACH.Get_Fields("ID"));

                boolDirty = false;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\n" + "In SaveAutoPay", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
		}

		private void chkActive_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void chkPreNote_CheckedChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLimit_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtLimit_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

			// traps the backspace key and all keys that are non numeric and not "."
			if ((KeyAscii<Keys.D0 || KeyAscii>Keys.D9) && KeyAscii!=Keys.Back && KeyAscii!=Keys.Decimal && KeyAscii!=Keys.OemPeriod)
            {
				KeyAscii = (Keys)0; // keys other than backspace, decimal or number keys are not allowed
			}

			e.KeyChar = Strings.Chr((int)KeyAscii);
		}

		private void txtTransitRouting_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

		private void txtTransitRouting_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);

			// traps the backspace key and all keys that are non numeric
			if ((KeyAscii<Keys.D0 || KeyAscii>Keys.D9) && KeyAscii!=Keys.Back) {
				KeyAscii = (Keys)0; // keys other than backspace or number keys are not allowed
			}

			e.KeyChar = Strings.Chr((int)KeyAscii);
		}

		private void txtAccountNumber_TextChanged(object sender, System.EventArgs e)
		{
			boolDirty = true;
		}

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (SaveAutoPay())
            {
                MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Unload();
            }
        }
    }
}
