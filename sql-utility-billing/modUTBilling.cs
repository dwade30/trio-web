﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;
using modUTStatusPayments = Global.modUTFunctions;
using Microsoft.VisualBasic.ApplicationServices;
using System.IO;

namespace TWUT0000
{
	public class modUTBilling
	{
		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}

		public class StaticVariables
		{
			//=========================================================
			// ********************************************************
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// *
			// WRITTEN BY     :               Jim Bertolino           *
			// DATE           :               05/13/2004              *
			// *
			// MODIFIED BY    :               Jim Bertolino           *
			// LAST UPDATED   :               01/19/2007              *
			// ********************************************************
			// here are some global variables that will get filled during the billing process
			public int glngBillingRateKey;
			public int glngBillType;
			public int gintPassQuestion;
			// Change Stored Procedures to SQL Code
			public string gstrQryCombinedBillInfo = "";
			public string gstrQryWaterBillOwnerInfo = "";
			public string gstrQrySewerBillOwnerInfo = "";
			public string gstrQryCombinedSeparateBillInfo = "";
			public string gstrQryPrintBill = "";
			public WarningStruct[] WarningArray = null;
			public AcctInfo[] typAccountInfo = null;
		}

		public struct WarningStruct
		{
			public int BillKey;
			// This is the Billkey associated with the warning
			public string Message;
			// This is the actual message of the warning
			public string Type;
			// This is the type: (W)arning, (N)ote, (E)rror
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public WarningStruct(int unusedParam)
			{
				this.BillKey = 0;
				this.Message = string.Empty;
				this.Type = string.Empty;
			}
		};

		public struct AcctInfo
		{
			public int AccountKey;
			public string AccountName;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public AcctInfo(int unusedParam)
			{
				this.AccountKey = 0;
				this.AccountName = string.Empty;
			}
		};
		// vbPorter upgrade warning: lngADJCode As int	OnWrite(double, short)
		// vbPorter upgrade warning: dblAmount As double	OnWrite(double, GroupConsts?)
		public static void AddBreakDownRecord_15128(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngRateKey = 0, double dblValue = 0, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, lngRateKey, dblValue, lngAccount);
		}

		public static void AddBreakDownRecord_15290(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngRateKey = 0, double dblValue = 0, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, lngRateKey, dblValue, lngAccount);
		}

		public static void AddBreakDownRecord_19664(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngRateKey = 0, double dblValue = 0, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, lngRateKey, dblValue, lngAccount);
		}

		public static void AddBreakDownRecord_19682(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngRateKey = 0, double dblValue = 0, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, lngRateKey, dblValue, lngAccount);
		}

		public static void AddBreakDownRecord_133388(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngRateKey = 0, double dblValue = 0, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, lngRateKey, dblValue, lngAccount);
		}

		public static void AddBreakDownRecord_432863(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngRateKey = 0, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, lngRateKey, 0, lngAccount);
		}

		public static void AddBreakDownRecord_433025(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngRateKey = 0, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, lngRateKey, 0, lngAccount);
		}

		public static void AddBreakDownRecord_448334(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, 0, 0, lngAccount);
		}

		public static void AddBreakDownRecord_452708(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, 0, 0, lngAccount);
		}

		public static void AddBreakDownRecord_487700(bool boolTemporary, int lngBillKey, int lngMeterKey, string strService, string strDescription, int lngADJCode, string strAccountNumber, double dblAmount, string strType, int lngRateKey = 0, double dblValue = 0, int lngAccount = 0)
		{
			AddBreakDownRecord(ref boolTemporary, ref lngBillKey, ref lngMeterKey, ref strService, ref strDescription, ref lngADJCode, ref strAccountNumber, ref dblAmount, strType, lngRateKey, dblValue, lngAccount);
		}

		public static void AddBreakDownRecord(ref bool boolTemporary, ref int lngBillKey, ref int lngMeterKey, ref string strService, ref string strDescription, ref int lngADJCode, ref string strAccountNumber, ref double dblAmount, string strType, int lngRateKey = 0, double dblValue = 0, int lngAccount = 0)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will create a record in the BreakDown or the TempBreakDown table
				clsDRWrapper rsBD = new clsDRWrapper();
				string strTemp = "";
				if (boolTemporary)
				{
					// set the string to change the table
					strTemp = "Temp";
				}
				else
				{
					strTemp = "";
				}
				if (strAccountNumber == "")
				{
					// find the correct account number to use
					strAccountNumber = FindAccountNumber_60(lngMeterKey, FCConvert.CBool(strService == "W"), lngRateKey, FCConvert.CBool(strType == "T"));
				}
				if (strType == "T" && (Strings.Trim(strAccountNumber) == "M" || Strings.Trim(strAccountNumber) == ""))
				{
					// do not add an entry for the tax
				}
				else
				{
					rsBD.OpenRecordset("SELECT * FROM " + strTemp + "BreakDown WHERE ID = 0", modExtraModules.strUTDatabase);
					rsBD.AddNew();
					rsBD.Set_Fields("BillKey", lngBillKey);
					// Billkey
					rsBD.Set_Fields("MeterKey", lngMeterKey);
					// Meter Key
					rsBD.Set_Fields("ADJCode", lngADJCode);
					// Adjustment Code
					rsBD.Set_Fields("Description", strDescription);
					// Description
					rsBD.Set_Fields("Value", dblValue);
					// Number of units or consumption calculated with
					rsBD.Set_Fields("AccountNumber", strAccountNumber);
					// BD account number to affect
					rsBD.Set_Fields("Amount", dblAmount);
					// Total amount
					rsBD.Set_Fields("Type", strType);
					// (C)onsumption, (F)lat, (U)nits, (M)isc, (D)ata Entry Adjustments, (M)eter Adjustments, (T)ax
					rsBD.Set_Fields("RateKey", lngRateKey);
					// Rate Key
					rsBD.Set_Fields("Service", strService);
					// (W)ater or (S)ewer
					rsBD.Set_Fields("DateCreated", DateTime.Today);
					// Date that this record was created
					// MAL@20080310: Add TaxAcquired Field
					// Tracker Reference: 12615
					if (lngAccount != 0)
					{
						rsBD.Set_Fields("TaxAcquired", modUTStatusPayments.IsAccountTaxAcquired(0, lngAccount));
					}
					else
					{
						rsBD.Set_Fields("TaxAcquired", modUTStatusPayments.IsAccountTaxAcquired(lngBillKey));
					}
					rsBD.Update();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Breakdown Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ClearBreakDownRecords_20(bool boolTemporary, int lngBillKey, bool boolAllRecords, int lngMeterKey = 0)
		{
			ClearBreakDownRecords(boolTemporary,  lngBillKey,  boolAllRecords, lngMeterKey);
		}

		public static void ClearBreakDownRecords_26(bool boolTemporary, int lngBillKey, bool boolAllRecords, int lngMeterKey = 0)
		{
			ClearBreakDownRecords( boolTemporary,  lngBillKey,  boolAllRecords, lngMeterKey);
		}

		public static void ClearBreakDownRecords_80(bool boolTemporary, int lngBillKey, bool boolAllRecords, int lngMeterKey = 0)
		{
			ClearBreakDownRecords( boolTemporary,  lngBillKey,  boolAllRecords, lngMeterKey);
		}

		public static void ClearBreakDownRecords( bool boolTemporary,  int lngBillKey,  bool boolAllRecords, int lngMeterKey = 0)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will delete all of the breakdown records for a certain bill
				// in the BreakDown or the TempBreakDown table
				// this will be used to remove from the temp table or purge from the regular table
				// if boolAllRecords = TRUE then delete all the records for this bill, else
				// only delete the calculation breakdown entries
				clsDRWrapper rsBD = new clsDRWrapper();
				string strTemp = "";
				string strWhereString = "";
				string strMeterString = "";
				if (boolTemporary)
				{
					// set the string to change the table
					strTemp = "Temp";
				}
				else
				{
					strTemp = "";
				}
				if (boolAllRecords)
				{
					strWhereString = "";
				}
				else
				{
					strWhereString = " AND Type <> 'D'";
				}
				if (lngMeterKey != 0)
				{
					strMeterString = " AND MeterKey = " + FCConvert.ToString(lngMeterKey);
				}
				else
				{
					strMeterString = "";
				}
				// delete " ORDER BY Service, Type, Description"
				rsBD.Execute("DELETE FROM " + strTemp + "BreakDown WHERE BillKey = " + FCConvert.ToString(lngBillKey) + strWhereString + strMeterString, modExtraModules.strUTDatabase);
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Breakdown Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static int FindTotalMeterConsumption_6(int lngAK, bool boolWater, int lngOverrideCons = 0)
		{
			return FindTotalMeterConsumption(ref lngAK, ref boolWater, lngOverrideCons);
		}

		public static int FindTotalMeterConsumption_8(int lngAK, bool boolWater, int lngOverrideCons = 0)
		{
			return FindTotalMeterConsumption(ref lngAK, ref boolWater, lngOverrideCons);
		}

		public static int FindTotalMeterConsumption_26(int lngAK, bool boolWater, int lngOverrideCons = 0)
		{
			return FindTotalMeterConsumption(ref lngAK, ref boolWater, lngOverrideCons);
		}

		public static int FindTotalMeterConsumption(ref int lngAK, ref bool boolWater, int lngOverrideCons = 0)
		{
			int FindTotalMeterConsumption = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will find the total consumption for the meters combined by consumption
				clsDRWrapper rsCons = new clsDRWrapper();
				// vbPorter upgrade warning: lngCons As int	OnWrite(double, int)
				int lngCons = 0;
				int lngBookNumber;
				int intCT;
				int lngOverride = 0;
				string strWaterSewer = "";
				int lngMaxAmount;
				int lngAdj = 0;
				if (boolWater)
				{
					strWaterSewer = "Water";
				}
				else
				{
					strWaterSewer = "Sewer";
				}
				// get all of the meters that are combined by consumption and the main meter                                  'kk06282017 trouts-244  Changed Check for Neg Cons to True
				rsCons.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAK) + " AND (MeterNumber = 1 OR Combine = 'C' OR ISNULL(NegativeConsumption,0) = 1) ORDER BY MeterNumber", modExtraModules.strUTDatabase);
				while (!rsCons.EndOfFile())
				{
					// MAL@20080729: Add check that current meter service matches passed in service
					// Tracker Reference: 14769
					if (FCConvert.ToString(rsCons.Get_Fields_String("Service")) == "B" || FCConvert.ToString(rsCons.Get_Fields_String("Service")) == Strings.Left(strWaterSewer, 1))
					{
						// add up all of the consumptions of the combined meters
						// check to see if the meter has any overrides
						lngOverride = 0;
						if (lngOverrideCons != 0 && FCConvert.ToInt32(rsCons.Get_Fields_Int32("MeterNumber")) == 1)
						{
							lngOverride = lngOverrideCons;
						}
						else
						{
							for (intCT = 1; intCT <= 5; intCT++)
							{
								// TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
								if (rsCons.Get_Fields("UseRate" + FCConvert.ToString(intCT)) && FCConvert.ToInt32(rsCons.Get_Fields(strWaterSewer + "Type" + FCConvert.ToString(intCT))) == 1 && FCConvert.ToInt32(rsCons.Get_Fields(strWaterSewer + "Amount" + FCConvert.ToString(intCT))) != 0)
								{
									lngOverride += rsCons.Get_Fields(strWaterSewer + "Amount" + FCConvert.ToString(intCT));
								}
							}
						}
						if (lngOverride != 0)
						{
							// if there is an override then add that
							if (FCConvert.ToBoolean(rsCons.Get_Fields_Boolean("NegativeConsumption")))
							{
								if (!boolWater)
								{
									lngCons -= (lngOverride * (rsCons.Get_Fields(strWaterSewer + "Percent") / 100));
								}
							}
							else
							{
								lngCons += (lngOverride * (rsCons.Get_Fields(strWaterSewer + "Percent") / 100));
							}
						}
						else
						{
							// otherwise add the real consumption
							if (FCConvert.ToInt32(rsCons.Get_Fields_Int32("CurrentReading")) != -1)
							{
								// kk 110812 trout-884 Add check for No Read
								if (rsCons.Get_Fields_Int32("CurrentReading") >= rsCons.Get_Fields_Int32("PreviousReading"))
								{
									if (FCConvert.ToBoolean(rsCons.Get_Fields_Boolean("NegativeConsumption")))
									{
										if (!boolWater)
										{
											// + rsCons.Fields("ReplacementConsumption")
											lngCons -= ((rsCons.Get_Fields_Int32("CurrentReading") - rsCons.Get_Fields_Int32("PreviousReading"))) * (rsCons.Get_Fields(strWaterSewer + "Percent") / 100);
										}
									}
									else
									{
										lngCons += ((rsCons.Get_Fields_Int32("CurrentReading") - rsCons.Get_Fields_Int32("PreviousReading"))) * (rsCons.Get_Fields(strWaterSewer + "Percent") / 100);
									}
								}
								else
								{
									if (rsCons.Get_Fields_Int32("Multiplier") == 100000)
									{
										lngAdj = 5;
									}
									else if (rsCons.Get_Fields_Int32("Multiplier") == 10000)
									{
										lngAdj = 4;
									}
									else if (rsCons.Get_Fields_Int32("Multiplier") == 1000)
									{
										lngAdj = 3;
									}
									else if (rsCons.Get_Fields_Int32("Multiplier") == 100)
									{
										lngAdj = 2;
									}
									else if (rsCons.Get_Fields_Int32("Multiplier") == 10)
									{
										lngAdj = 1;
									}
									else if (rsCons.Get_Fields_Int32("Multiplier") == 1)
									{
										lngAdj = 0;
									}
									else
									{
										lngAdj = 0;
									}
									switch (modExtraModules.Statics.glngTownReadingUnits)
									{
										case 100000:
											{
												lngAdj -= 5;
												break;
											}
										case 10000:
											{
												lngAdj -= 4;
												break;
											}
										case 1000:
											{
												lngAdj -= 3;
												break;
											}
										case 100:
											{
												lngAdj -= 2;
												break;
											}
										case 10:
											{
												lngAdj -= 1;
												break;
											}
										case 1:
											{
												lngAdj = lngAdj;
												break;
											}
										default:
											{
												lngAdj = lngAdj;
												break;
											}
									}
									//end switch
									// this must be a rolled over meter
									// lngMaxAmount = Val(txtReading.Text) + ((10 ^ (rsMeter.Fields("Digits") - lngAdj)) - Val(lblPreviousReadingText.Caption))
									// lngMaxAmount = Val(String((rsCons.Fields("Digits") - lngAdj), "9"))     'this finds the most it could possibly be
									if (FCConvert.ToBoolean(rsCons.Get_Fields_Boolean("NegativeConsumption")))
									{
										if (!boolWater)
										{
											lngCons -= ((Conversion.Val(rsCons.Get_Fields_Int32("CurrentReading")) + ((Math.Pow(10, (rsCons.Get_Fields_Int32("Digits") - lngAdj))) - Conversion.Val(rsCons.Get_Fields_Int32("PreviousReading"))))) * (rsCons.Get_Fields(strWaterSewer + "Percent") / 100);
										}
									}
									else
									{
										// lngCons = lngCons + (lngMaxAmount + (rsCons.Fields("CurrentReading") - rsCons.Fields("PreviousReading"))) * (rsCons.Fields(strWaterSewer & "Percent") / 100)
										lngCons += ((Conversion.Val(rsCons.Get_Fields_Int32("CurrentReading")) + ((Math.Pow(10, (rsCons.Get_Fields_Int32("Digits") - lngAdj))) - Conversion.Val(rsCons.Get_Fields_Int32("PreviousReading"))))) * (rsCons.Get_Fields(strWaterSewer + "Percent") / 100);
									}
								}
							}
							else
							{
								lngCons = 0;
							}
						}
						if (FCConvert.ToBoolean(rsCons.Get_Fields_Boolean("UseMeterChangeOut")))
						{
							if (FCConvert.ToBoolean(rsCons.Get_Fields_Boolean("NegativeConsumption")))
							{
								lngCons -= rsCons.Get_Fields_Int32("ConsumptionOfChangedOutMeter");
							}
							else
							{
								lngCons += rsCons.Get_Fields_Int32("ConsumptionOfChangedOutMeter");
							}
						}
					}
					rsCons.MoveNext();
				}
				FindTotalMeterConsumption = lngCons;
				return FindTotalMeterConsumption;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Total Consumption", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindTotalMeterConsumption;
		}
		// vbPorter upgrade warning: lngCons As int	OnWrite(int, double)
		public static double CalculateConsumptionBased_6554(int lngCons, int lngRT, bool boolWater, double dblMisc1, double dblMisc2, double dblTaxableAmount, string strMisc1Desc, string strMisc2Desc, double dblPercentage = 1, bool boolMin = false)
		{
			return CalculateConsumptionBased(ref lngCons,  lngRT,  boolWater, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, dblPercentage, ref boolMin);
		}

		public static double CalculateConsumptionBased_6560(int lngCons, int lngRT, bool boolWater, double dblMisc1, double dblMisc2, double dblTaxableAmount, string strMisc1Desc, string strMisc2Desc, double dblPercentage = 1, bool boolMin = false)
		{
			return CalculateConsumptionBased(ref lngCons,  lngRT,  boolWater,ref  dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, dblPercentage, ref boolMin);
		}

		public static double CalculateConsumptionBased_13140(ref int lngCons, int lngRT, bool boolWater, ref double dblMisc1, ref double dblMisc2, ref double dblTaxableAmount, ref string strMisc1Desc, ref string strMisc2Desc, double dblPercentage/* = 1 */, ref bool boolMin/* = false */)
		{
			return CalculateConsumptionBased(ref lngCons,  lngRT,  boolWater, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, dblPercentage, ref boolMin);
		}

        public static double CalculateConsumptionBased(ref int lngCons, int lngRT, bool boolWater)
        {
            double dblMisc1 = 0;
            double dblMisc2 = 0;
            double dblTaxableAmount = 0;
            var strMisc1Desc = "";
            var strMisc2Desc = "";
            var boolMin = false;
            return CalculateConsumptionBased(ref lngCons, lngRT, boolWater, ref dblMisc1, ref dblMisc2,
                ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, 0, ref boolMin);
        }

        public static double CalculateConsumptionBased(ref int lngCons,  int lngRT,  bool boolWater,  ref double dblMisc1,ref  double dblMisc2, ref double dblTaxableAmount, ref string strMisc1Desc, ref string strMisc2Desc, double dblPercentage/* = 1*/, ref bool boolMin/*= false*/)
		{
			double CalculateConsumptionBased = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the consumption total for the amount lngCons and this rate table lngRT
				string strWS = "";
				clsDRWrapper rsRate = new clsDRWrapper();
				double dblTotalAmount = 0;
				int intCT;
				bool boolZero;
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				boolZero = false;
				rsRate.OpenRecordset("SELECT * FROM RateTable WHERE RateTableNumber = " + FCConvert.ToString(lngRT), modExtraModules.strUTDatabase);
				if (!rsRate.EndOfFile())
				{
					// return the misc charges
					// TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
					dblMisc1 = (rsRate.Get_Fields("MiscChargeAmount" + strWS + "1") * dblPercentage);
					// TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
					dblMisc2 = (rsRate.Get_Fields("MiscChargeAmount" + strWS + "2") * dblPercentage);
					// TODO Get_Fields: Field [MiscCharge] not found!! (maybe it is an alias?)
					strMisc1Desc = "Misc - " + rsRate.Get_Fields("MiscCharge" + strWS + "1");
					// TODO Get_Fields: Field [MiscCharge] not found!! (maybe it is an alias?)
					strMisc2Desc = "Misc - " + rsRate.Get_Fields("MiscCharge" + strWS + "2");
					// If rsRate.Fields("Taxable" & strWS & "1") Then
					dblTaxableAmount += dblMisc1;
					// End If
					// If rsRate.Fields("Taxable" & strWS & "2") Then
					dblTaxableAmount += dblMisc2;
					// End If
					// make sure that the consumption is above the minimum
					// TODO Get_Fields: Field [MinCons] not found!! (maybe it is an alias?)
					if (lngCons < FCConvert.ToInt32(rsRate.Get_Fields("MinCons" + strWS)))
					{
						if (lngCons == 0 && Strings.UCase(modGlobalConstants.Statics.MuniName) == "MACHIAS")
						{
							boolZero = true;
						}
						// MAL@20080228: Change to not prorate the consumption and to set the boolean correctly
						// Tracker Reference: 12561
						// lngCons = (rsRate.Fields("MinCons" & strWS) * dblPercentage) 'set it to the minimum
						// TODO Get_Fields: Field [MinCons] not found!! (maybe it is an alias?)
						lngCons = FCConvert.ToInt32(rsRate.Get_Fields("MinCons" + strWS));
						// set it to the minimum
						boolMin = true;
					}
					else
					{
						boolMin = false;
					}
					for (intCT = 1; intCT <= 8; intCT++)
					{
						if (Conversion.Val(rsRate.Get_Fields(strWS + "Step" + FCConvert.ToString(intCT))) > 1)
						{
							// a step will exit the for and calculate the rest with a step
							if (intCT == 1)
							{
								// calculate the total amount charged for the amount of consumption divided by the step multiplied by the rate
								dblTotalAmount = (rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * (fecherFoundation.FCUtils.iDiv(lngCons, Conversion.Val(rsRate.Get_Fields(strWS + "Step" + FCConvert.ToString(intCT))))));
								break;
							}
							else
							{
								// this will add the total amount charged for the amount of leftover consumption divided by the step multiplied by the rate
								dblTotalAmount += (rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * (fecherFoundation.FCUtils.iDiv((lngCons - FCConvert.ToInt32(rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT - 1)))), Conversion.Val(rsRate.Get_Fields(strWS + "Step" + FCConvert.ToString(intCT))))));
								break;
							}
						}
						else
						{
							// calculate the total amount charged for
							if (lngCons > rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT)))
							{
								// calculate the total range
								if (intCT == 1)
								{
									dblTotalAmount += (rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT)));
								}
								else
								{
									dblTotalAmount += (rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * (rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT)) - (rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT - 1)))));
								}
							}
							else
							{
								// calculate the leftover, this will be the last row needed to calculate
								if (intCT == 1)
								{
									dblTotalAmount += (rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * lngCons);
								}
								else
								{
									dblTotalAmount += (rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * (lngCons - FCConvert.ToInt32(rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT - 1)))));
								}
								break;
							}
						}
					}
					// check to make sure that the total is more than the min amount to be charged
					// If dblTotalAmount < rsRate.Fields("MinCharge" & strWS) Then
					// dblTotalAmount = rsRate.Fields("MinCharge" & strWS)  'set it to the minimum
					if (boolMin)
					{
						if (!boolZero)
						{
							// TODO Get_Fields: Field [MinCharge] not found!! (maybe it is an alias?)
							dblTotalAmount += (FCConvert.ToDouble(rsRate.Get_Fields("MinCharge" + strWS)) * dblPercentage);
							// This will always be added to Consumption or Units based types
						}
					}
					else
					{
						// TODO Get_Fields: Field [MinCharge] not found!! (maybe it is an alias?)
						dblTotalAmount += FCConvert.ToDouble(rsRate.Get_Fields("MinCharge" + strWS));
					}
					if (FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("CombineToMisc1")))
					{
						dblMisc1 += dblTotalAmount;
						dblTotalAmount = 0;
					}
					// End If
				}
				dblTaxableAmount += dblTotalAmount;
				CalculateConsumptionBased = dblTotalAmount;
				return CalculateConsumptionBased;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Consumption Total", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateConsumptionBased;
		}

		public static double CalculateUnitBased_13140(double dblUnit, int lngRT, bool boolWater, ref double dblMisc1, ref double dblMisc2, ref double dblTaxableAmount, ref string strMisc1Desc, ref string strMisc2Desc, double dblFinalPercentage = 1)
		{
			return CalculateUnitBased(ref dblUnit, ref lngRT, ref boolWater, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, dblFinalPercentage);
		}

		public static double CalculateUnitBased(ref double dblUnit, ref int lngRT, ref bool boolWater, ref double dblMisc1, ref double dblMisc2, ref double dblTaxableAmount, ref string strMisc1Desc, ref string strMisc2Desc, double dblFinalPercentage = 1)
		{
			double CalculateUnitBased = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the units total for the amount dblunit and this rate table lngRT
				string strWS = "";
				clsDRWrapper rsRate = new clsDRWrapper();
				double dblTotalAmount = 0;
				int intCT;
				// Dim boolMin                 As Boolean
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				rsRate.OpenRecordset("SELECT * FROM RateTable WHERE RateTableNumber = " + FCConvert.ToString(lngRT), modExtraModules.strUTDatabase);
				if (!rsRate.EndOfFile())
				{
					// return the misc charges
					// TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
					dblMisc1 = rsRate.Get_Fields("MiscChargeAmount" + strWS + "1") * dblFinalPercentage;
					// TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
					dblMisc2 = rsRate.Get_Fields("MiscChargeAmount" + strWS + "2") * dblFinalPercentage;
					// TODO Get_Fields: Field [MiscCharge] not found!! (maybe it is an alias?)
					strMisc1Desc = "Misc - " + rsRate.Get_Fields("MiscCharge" + strWS + "1");
					// TODO Get_Fields: Field [MiscCharge] not found!! (maybe it is an alias?)
					strMisc2Desc = "Misc - " + rsRate.Get_Fields("MiscCharge" + strWS + "2");
					// If rsRate.Fields("Taxable" & strWS & "1") Then
					dblTaxableAmount += dblMisc1;
					// End If
					// If rsRate.Fields("Taxable" & strWS & "2") Then
					dblTaxableAmount += dblMisc2;

					for (intCT = 1; intCT <= 8; intCT++)
					{
						if (Conversion.Val(rsRate.Get_Fields(strWS + "Step" + FCConvert.ToString(intCT))) > 1)
						{
							// a step will exit the for and calculate the rest with a step
							if (intCT == 1)
							{
								// calculate the total amount charged for the amount of consumption divided by the step multiplied by the rate
								dblTotalAmount = (rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * (dblUnit / Conversion.Val(rsRate.Get_Fields(strWS + "Step" + FCConvert.ToString(intCT)))));
								break;
							}
							else
							{
								// this will add the total amount charged for the amount of leftover consumption divided by the step multiplied by the rate
								dblTotalAmount += (rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * (dblUnit - ((rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT - 1))) / Conversion.Val(rsRate.Get_Fields(strWS + "Step" + FCConvert.ToString(intCT))))));
								break;
							}
						}
						else
						{
							// calculate the total amount charged for
							if (dblUnit > rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT)))
							{
								// calculate the total range
								if (intCT == 1)
								{
									dblTotalAmount += ((rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT))) * dblFinalPercentage);
								}
								else
								{
									dblTotalAmount += ((rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * (rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT)) - (rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT - 1))))) * dblFinalPercentage);
								}
							}
							else
							{
								// calculate the leftover, this will be the last row needed to calculate
								if (intCT == 1)
								{
									dblTotalAmount += ((rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * dblUnit) * dblFinalPercentage);
								}
								else
								{
									dblTotalAmount += ((rsRate.Get_Fields(strWS + "Rate" + FCConvert.ToString(intCT)) * (dblUnit - (rsRate.Get_Fields(strWS + "Thru" + FCConvert.ToString(intCT - 1))))) * dblFinalPercentage);
								}
								break;
							}
						}
					}

					// TODO Get_Fields: Field [MinCharge] not found!! (maybe it is an alias?)
					dblTotalAmount += (rsRate.Get_Fields("MinCharge" + strWS) * dblFinalPercentage);

				}
				dblTaxableAmount += dblTotalAmount;
				CalculateUnitBased = dblTotalAmount;
				return CalculateUnitBased;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Consumption Total", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateUnitBased;
		}

		public static double CalculateFlatBased_6(int lngRT, bool boolWater, ref double dblMisc1, ref double dblMisc2, ref double dblTaxableAmount, double dblOverrideAmount/* = 0 */, ref string strMisc1Desc/* = "" */, ref string strMisc2Desc/* = "" */)
		{
			return CalculateFlatBased(ref lngRT, ref boolWater, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, dblOverrideAmount, ref strMisc1Desc, ref strMisc2Desc);
		}

		public static double CalculateFlatBased(ref int lngRT, ref bool boolWater, ref double dblMisc1, ref double dblMisc2, ref double dblTaxableAmount, double dblOverrideAmount/* = 0 */, ref string strMisc1Desc/* = "" */, ref string strMisc2Desc/* = "" */)
		{
			double CalculateFlatBased = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the flat total for the ratetable lngRT
				string strWS = "";
				clsDRWrapper rsRate = new clsDRWrapper();
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				if (dblOverrideAmount != 0)
				{
					dblTaxableAmount += dblOverrideAmount;
					CalculateFlatBased = dblOverrideAmount;
				}
				else
				{
					rsRate.OpenRecordset("SELECT * FROM RateTable WHERE RateTableNumber = " + FCConvert.ToString(lngRT), modExtraModules.strUTDatabase);
					if (!rsRate.EndOfFile())
					{
						// return the misc charges
						// TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
						dblMisc1 = rsRate.Get_Fields("MiscChargeAmount" + strWS + "1");
						// TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
						dblMisc2 = rsRate.Get_Fields("MiscChargeAmount" + strWS + "2");
						// TODO Get_Fields: Field [MiscCharge] not found!! (maybe it is an alias?)
						strMisc1Desc = "Misc - " + rsRate.Get_Fields("MiscCharge" + strWS + "1");
						// TODO Get_Fields: Field [MiscCharge] not found!! (maybe it is an alias?)
						strMisc2Desc = "Misc - " + rsRate.Get_Fields("MiscCharge" + strWS + "2");
						// TODO Get_Fields: Field [Taxable] not found!! (maybe it is an alias?)
						if (FCConvert.ToBoolean(rsRate.Get_Fields("Taxable" + strWS + "1")))
						{
							dblTaxableAmount += dblMisc1;
						}
						// TODO Get_Fields: Field [Taxable] not found!! (maybe it is an alias?)
						if (FCConvert.ToBoolean(rsRate.Get_Fields("Taxable" + strWS + "2")))
						{
							dblTaxableAmount += dblMisc2;
						}
						if (dblOverrideAmount != 0)
						{
							dblTaxableAmount += dblOverrideAmount;
							CalculateFlatBased = dblOverrideAmount;
						}
						else
						{
							// return the taxable amount
							dblTaxableAmount += FCConvert.ToDouble(rsRate.Get_Fields_Decimal("FlatRate" + strWS));
							CalculateFlatBased = FCConvert.ToDouble(rsRate.Get_Fields_Decimal("FlatRate" + strWS));
						}
					}
				}
				return CalculateFlatBased;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Flat Total", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateFlatBased;
		}

		public static int GetNextBillNumber(ref int lngAcctKey)
		{
			int GetNextBillNumber = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the next bill number in sequence for this account
				clsDRWrapper rsBN = new clsDRWrapper();
				rsBN.OpenRecordset("SELECT TOP 1 BillNumber FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " ORDER BY BillNumber Desc", modExtraModules.strUTDatabase);
				if (!rsBN.EndOfFile())
				{
					GetNextBillNumber = rsBN.Get_Fields_Int32("BillNumber") + 1;
				}
				else
				{
					GetNextBillNumber = 1;
				}
				return GetNextBillNumber;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Get Next Bill Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetNextBillNumber;
		}

		public static int FinishBillingRecords(ref int lngRateKey, ref int[] arrBookArray, bool boolFinal/* = false */, ref string strDidNotBill/* = "" */, bool blnWinterBilling = false)
		{
			int FinishBillingRecords = 0;
			int lngCount = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will cycle through all of the bills in these books and associate the ratekey passed
				// in with each as well as transfer all of the totals to Owed in the bill (this will let UTCL show them)
				int lngCT;
				clsDRWrapper rsBills = new clsDRWrapper();
				string strBillList = "";
				bool boolOverwriteBills = false;
				int intFinal = 0;
				if (boolFinal)
				{
					intFinal = 1;
				}
				else
				{
					intFinal = 0;
				}
				strDidNotBill = ",";
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Checking for duplicate records.");
				// cycle through the books selected by the user
				for (lngCT = 1; lngCT <= Information.UBound(arrBookArray, 1); lngCT++)
				{
					//Application.DoEvents();
					// only select bills that are eligible to be billed from the correct book
					rsBills.OpenRecordset("SELECT * FROM Bill WHERE Book = " + FCConvert.ToString(arrBookArray[lngCT]) + " AND BillStatus = 'B' AND BillingRateKey = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
					// this will check to see if any accounts in this book have already had a bill created with this rate key
					if (rsBills.EndOfFile())
					{
						// keep rocking
						boolOverwriteBills = false;
					}
					else
					{
						frmWait.InstancePtr.Unload();
						MessageBox.Show("Book #" + FCConvert.ToString(arrBookArray[lngCT]) + " cannot be transferred because there is already a bill with this rate key.  Please create a new rate key and run this process again.", "Duplicate Rate Key", MessageBoxButtons.OK, MessageBoxIcon.Warning);

						FinishBillingRecords = 0;
						return FinishBillingRecords;
						// End Select
					}
				}
				for (lngCT = 1; lngCT <= Information.UBound(arrBookArray, 1); lngCT++)
				{
					// Dave 07/24/07 'Added criteria to match Final
					rsBills.OpenRecordset("SELECT * FROM Bill WHERE Book = " + FCConvert.ToString(arrBookArray[lngCT]) + " AND BillStatus = 'E' AND ISNULL(Final,0) = " + FCConvert.ToString(intFinal), modExtraModules.strUTDatabase);
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Creating Bill Records" + "\r\n" + "Book : " + FCConvert.ToString(arrBookArray[lngCT]), true, rsBills.RecordCount());
					while (!rsBills.EndOfFile())
					{
						//Application.DoEvents();
						frmWait.InstancePtr.IncrementProgress();
						// increment the progress bar
						strBillList += rsBills.Get_Fields_Int32("ID") + ",";
						if (TransferBillingAmounts_2(rsBills.Get_Fields_Int32("ID"), lngRateKey, boolFinal, boolOverwriteBills, blnWinterBilling))
						{
							// transfer the amounts
							lngCount += 1;
							// count the total bills
						}
						rsBills.MoveNext();
					}
				}
				if (strBillList != "")
				{
					// remove the last comma
					strBillList = Strings.Left(strBillList, strBillList.Length - 1);
					if (modGlobalConstants.Statics.gboolBD)
					{
						if (!modMain.CreateUTJournalEntry(ref strBillList))
						{
							frmWait.InstancePtr.Unload();
							MessageBox.Show("An error occurred while creating the UT journal.  Please fix the problem and run this again.", "Error Creating Journal", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
					}
				}
				frmWait.InstancePtr.Unload();
				FinishBillingRecords = lngCount;
				// return the total number of bills created
				return FinishBillingRecords;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				FinishBillingRecords = lngCount;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Transferring Billing Amounts - " + FCConvert.ToString(lngCount), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FinishBillingRecords;
		}

		public static bool TransferBillingAmounts_2(int lngBillKey, int lngRK = 0, bool boolFinal = false, bool boolOverwriteBills = false, bool blnWinterBilling = false)
		{
			return TransferBillingAmounts(ref lngBillKey, lngRK, boolFinal, boolOverwriteBills, blnWinterBilling);
		}

		public static bool TransferBillingAmounts(ref int lngBillKey, int lngRK = 0, bool boolFinal = false, bool boolOverwriteBills = false, bool blnWinterBilling = false)
		{
			bool TransferBillingAmounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will transfer one bill's calculated amounts to actually be owed
				// so that UT collections can see the bills this will also move all of the
				// temporary break down records to the permanent table
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsMeter = new clsDRWrapper();
				clsDRWrapper rsBillTo = new clsDRWrapper();
				clsDRWrapper rsPrevBill = new clsDRWrapper();
				string strEBillType = "";
				clsDRWrapper rsAddMeters = new clsDRWrapper();
				rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strUTDatabase);
				if (!rsBill.EndOfFile())
				{

					rsMaster.OpenRecordset(modUTStatusPayments.UTMasterQuery(rsBill.Get_Fields_Int32("AccountKey")), modExtraModules.strUTDatabase);
					rsBill.Edit();
					if (!rsMaster.EndOfFile())
					{
						// add the information from the master record to be stored in the bill record
						rsBill.Set_Fields("BName", rsMaster.Get_Fields_String("Name"));
						rsBill.Set_Fields("BName2", rsMaster.Get_Fields_String("Name2"));
						rsBill.Set_Fields("BAddress1", rsMaster.Get_Fields_String("BAddress1"));
						rsBill.Set_Fields("BAddress2", rsMaster.Get_Fields_String("BAddress2"));
						rsBill.Set_Fields("BAddress3", rsMaster.Get_Fields_String("BAddress3"));
						rsBill.Set_Fields("BCity", rsMaster.Get_Fields_String("BCity"));
						rsBill.Set_Fields("BState", rsMaster.Get_Fields_String("BState"));
						rsBill.Set_Fields("BZip", rsMaster.Get_Fields_String("BZip"));
						rsBill.Set_Fields("BZip4", rsMaster.Get_Fields_String("BZip4"));
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						rsBill.Set_Fields("OName", rsMaster.Get_Fields("OwnerName"));
						// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
						rsBill.Set_Fields("OName2", rsMaster.Get_Fields("SecondOwnerName"));
						rsBill.Set_Fields("OAddress1", rsMaster.Get_Fields_String("OAddress1"));
						rsBill.Set_Fields("OAddress2", rsMaster.Get_Fields_String("OAddress2"));
						rsBill.Set_Fields("OAddress3", rsMaster.Get_Fields_String("OAddress3"));
						rsBill.Set_Fields("OCity", rsMaster.Get_Fields_String("OCity"));
						rsBill.Set_Fields("OState", rsMaster.Get_Fields_String("OState"));
						rsBill.Set_Fields("OZip", rsMaster.Get_Fields_String("OZip"));
						rsBill.Set_Fields("OZip4", rsMaster.Get_Fields_String("OZip4"));
						rsBill.Set_Fields("BillMessage", rsMaster.Get_Fields_String("BillMessage") + " ");
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("StreetNumber"))) != "")
						{
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							rsBill.Set_Fields("Location", Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("StreetNumber"))) + " " + rsMaster.Get_Fields_String("StreetName"));
						}
						else
						{
							rsBill.Set_Fields("Location", rsMaster.Get_Fields_String("StreetName"));
						}
						rsBill.Set_Fields("MapLot", rsMaster.Get_Fields_String("MapLot"));
						if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BookPage"))) != "B0P0")
						{
							// kk02122016 trout-1205  Transfer Book & Page to Bill record
							rsBill.Set_Fields("BookPage", rsMaster.Get_Fields_String("BookPage"));
						}
						rsBill.Set_Fields("Telephone", rsMaster.Get_Fields_String("Telephone"));
						rsBill.Set_Fields("Email", rsMaster.Get_Fields_String("Email"));
						rsBillTo.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
						// This will keep track of who will get the bill
						if (!rsBillTo.EndOfFile())
						{
							if (FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("SameBillOwner")))
							{
								// this will force the use of the owner name
								rsBill.Set_Fields("WBillOwner", true);
								rsBill.Set_Fields("SBillOwner", true);
							}
							else
							{
								// This will allow the splitting of bills
								// trout-718 08-01-2011  change bill to options to the Master account screen
								// rsBill.Fields("WBillOwner") = rsBillTo.Fields("WBillToOwner")
								// rsBill.Fields("SBillOwner") = rsBillTo.Fields("SBillToOwner")
								rsBill.Set_Fields("WBillOwner", rsMaster.Get_Fields_Boolean("WBillToOwner"));
								rsBill.Set_Fields("SBillOwner", rsMaster.Get_Fields_Boolean("SBillToOwner"));
							}
						}
					}
					else
					{
						// MsgBox "The master record for this bill could not be transfered.", vbExclamation, "Missing Master Record"
					}
					if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
					{
						rsBill.Set_Fields("WinterBill", blnWinterBilling);
					}
					else
					{
						rsBill.Set_Fields("WinterBill", false);
					}
					rsBill.Set_Fields("WPrinOwed", FCUtils.Round(Conversion.Val(rsBill.Get_Fields_Decimal("WMiscAmount") + rsBill.Get_Fields_Decimal("WAdjustAmount") + rsBill.Get_Fields_Decimal("WDEAdjustAmount") + rsBill.Get_Fields_Decimal("WFlatAmount") + rsBill.Get_Fields_Decimal("WUnitsAmount") + rsBill.Get_Fields_Decimal("WConsumptionAmount")), 2));
					// TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
					rsBill.Set_Fields("WTaxOwed", FCUtils.Round(Conversion.Val(rsBill.Get_Fields("WTax")), 2));
					rsBill.Set_Fields("WIntOwed", 0);
					rsBill.Set_Fields("WCostOwed", 0);
					rsBill.Set_Fields("SPrinOwed", FCUtils.Round(Conversion.Val(rsBill.Get_Fields_Decimal("SMiscAmount") + rsBill.Get_Fields_Decimal("SAdjustAmount") + rsBill.Get_Fields_Decimal("SDEAdjustAmount") + rsBill.Get_Fields_Decimal("SFlatAmount") + rsBill.Get_Fields_Decimal("SUnitsAmount") + rsBill.Get_Fields_Decimal("SConsumptionAmount")), 2));
					// TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
					rsBill.Set_Fields("STaxOwed", FCUtils.Round(Conversion.Val(rsBill.Get_Fields("STax")), 2));
					rsBill.Set_Fields("SIntOwed", 0);
					rsBill.Set_Fields("SCostOwed", 0);
					rsBill.Set_Fields("WOrigBillAmount", rsBill.Get_Fields_Double("WPrinOwed") + rsBill.Get_Fields_Double("WTaxOwed"));
					rsBill.Set_Fields("SOrigBillAmount", rsBill.Get_Fields_Double("SPrinOwed") + rsBill.Get_Fields_Double("STaxOwed"));
					rsBill.Set_Fields("CreationDate", DateTime.Now);
					rsBill.Set_Fields("BillStatus", "B");
					// this will set the status to Billed and now it can be printed
					TransferBreakDownRecords_2(rsBill.Get_Fields_Int32("ID"));
					if (lngRK != 0)
					{
						rsBill.Set_Fields("BillingRateKey", lngRK);
						rsBill.Set_Fields("BillNumber", lngRK);
					}
					// add the bill date to the bill record
					rsMeter.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
					if (!rsMeter.EndOfFile())
					{
						if (Information.IsDate(rsMeter.Get_Fields("BillDate")))
						{
							rsBill.Set_Fields("BillDate", rsMeter.Get_Fields_DateTime("BillDate"));
						}
						else
						{
							rsBill.Set_Fields("BillDate", DateTime.Today);
						}
					}
					else
					{
						rsBill.Set_Fields("BillDate", DateTime.Today);
					}
					// MAL@20080806: Add check for IConnect E-Bill option
					// Tracker Reference: 10680
					if (modGlobalConstants.Statics.gboolIC)
					{
						rsBill.Set_Fields("SendEBill", AcceptsEBills(rsBill.Get_Fields_Int32("AccountKey"), ref strEBillType));
					}
					// check the meter record and make sure to reset the meter change out
					rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + rsBill.Get_Fields_Int32("MeterKey"), modExtraModules.strUTDatabase);
					if (!rsMeter.EndOfFile())
					{
						UpdateMeterConsumption_26(rsMeter.Get_Fields_Int32("AccountKey"), rsBill.Get_Fields_DateTime("BillDate"), rsBill.Get_Fields_Int32("ID"));
						// kk01232015 trouts-135  add bill date and id
						if (boolFinal)
						{
							rsMeter.Edit();

							rsMeter.Set_Fields("FinalEndDate", rsBill.Get_Fields_DateTime("FinalEndDate"));
							rsMeter.Set_Fields("FinalStartDate", rsBill.Get_Fields_DateTime("FinalStartDate"));
							rsMeter.Set_Fields("FinalBillDate", rsBill.Get_Fields_DateTime("FinalBillDate"));
							rsMeter.Set_Fields("Final", true);
							// kgk 10-18-2011 trout-765  Save the MeterChangeOut for bill printing
							if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("PrevChangeOut")))
							{
								rsMeter.Set_Fields("PrevChangeOut", false);
								rsMeter.Set_Fields("PrevChangeOutConsumption", 0);
								// kjr 11-7-2016 trout-1118
							}
							if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseMeterChangeOut")))
							{
								rsMeter.Set_Fields("UseMeterChangeOut", false);
								rsMeter.Set_Fields("PrevChangeOut", true);
								// kgk 10-18-2011 trout-765
								rsMeter.Set_Fields("PrevChangeOutConsumption", rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter"));
								// kjr 11-7-2016 trout-1118
								rsMeter.Set_Fields("ConsumptionOfChangedOutMeter", 0);
								rsMeter.Set_Fields("ReplacementConsumption", 0);
								rsBill.Set_Fields("MeterChangedOut", true);
								// kgk 12-07-2011
								// TODO Get_Fields: Field [PrevChangeOutConsumption] not found!! (maybe it is an alias?)
								rsBill.Set_Fields("MeterChangedOutConsumption", rsMeter.Get_Fields("PrevChangeOutConsumption"));
								// kjr 11-7-2016 trout-1118
							}
							rsMeter.Update();
							if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("MeterNumber")) == 1)
							{
								rsAddMeters.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterNumber <> 1 and (Combine = 'C' or ISNULL(NegativeConsumption,0) = 1)", "TWUT0000.vb1");
								if (rsAddMeters.EndOfFile() != true && rsAddMeters.BeginningOfFile() != true)
								{
									do
									{
										rsAddMeters.Edit();

										rsAddMeters.Set_Fields("FinalEndDate", rsBill.Get_Fields_DateTime("FinalEndDate"));
										rsAddMeters.Set_Fields("FinalStartDate", rsBill.Get_Fields_DateTime("FinalStartDate"));
										rsAddMeters.Set_Fields("FinalBillDate", rsBill.Get_Fields_DateTime("FinalBillDate"));
										rsAddMeters.Set_Fields("Final", true);
										// kgk 10-18-2011 trout-765  Save the MeterChangeOut for bill printing
										if (FCConvert.ToBoolean(rsAddMeters.Get_Fields_Boolean("PrevChangeOut")))
										{
											rsAddMeters.Set_Fields("PrevChangeOut", false);
											rsAddMeters.Set_Fields("PrevChangeOutConsumption", 0);
											// kjr 11-7-2016 trout-1118
										}
										if (FCConvert.ToBoolean(rsAddMeters.Get_Fields_Boolean("UseMeterChangeOut")))
										{
											rsAddMeters.Set_Fields("UseMeterChangeOut", false);
											rsAddMeters.Set_Fields("PrevChangeOut", true);
											// kgk 10-18-2011 trout-765
											rsAddMeters.Set_Fields("PrevChangeOutConsumption", rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter"));
											// kjr 11-7-2016 trout-1118
											rsAddMeters.Set_Fields("ConsumptionOfChangedOutMeter", 0);
											rsAddMeters.Set_Fields("ReplacementConsumption", 0);
											rsBill.Set_Fields("MeterChangedOut", true);
											// kgk 12-07-2011
											// TODO Get_Fields: Field [PrevChangeOutConsumption] not found!! (maybe it is an alias?)
											rsBill.Set_Fields("MeterChangedOutConsumption", rsMeter.Get_Fields("PrevChangeOutConsumption"));
											// kjr 11-7-2016 trout-1118
										}
										rsAddMeters.Update();
										rsAddMeters.MoveNext();
									}
									while (rsAddMeters.EndOfFile() != true);
								}
							}
						}
						else
						{
							rsMeter.Edit();
							// kgk 10-18-2011 trout-765  Save the MeterChangeOut for bill printing
							if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("PrevChangeOut")))
							{
								rsMeter.Set_Fields("PrevChangeOut", false);
								rsMeter.Set_Fields("PrevChangeOutConsumption", 0);
								// kjr 11-7-2016 trout-1118
							}
							if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseMeterChangeOut")))
							{
								rsMeter.Set_Fields("UseMeterChangeOut", false);
								rsMeter.Set_Fields("PrevChangeOut", true);
								rsMeter.Set_Fields("PrevChangeOutConsumption", rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter"));
								// kjr 11-7-2016 trout-1118
								rsMeter.Set_Fields("ConsumptionOfChangedOutMeter", 0);
								rsMeter.Set_Fields("ReplacementConsumption", 0);
								// TODO Get_Fields: Field [PrevChangeOutConsumption] not found!! (maybe it is an alias?)
								rsBill.Set_Fields("MeterChangedOutConsumption", rsMeter.Get_Fields("PrevChangeOutConsumption"));
								// kjr 11-7-2016 trout-1118
							}
							rsMeter.Set_Fields("FinalEndDate", 0);
							rsMeter.Set_Fields("FinalStartDate", 0);
							rsMeter.Set_Fields("FinalBillDate", 0);
							rsMeter.Set_Fields("Final", false);
							rsMeter.Update();
							if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("MeterNumber")) == 1)
							{
								rsAddMeters.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterNumber <> 1 and (Combine = 'C' or ISNULL(NegativeConsumption,0) = 1)", "TWUT0000.vb1");
								if (rsAddMeters.EndOfFile() != true && rsAddMeters.BeginningOfFile() != true)
								{
									do
									{
										rsAddMeters.Edit();
										// kgk 10-18-2011 trout-765  Save the MeterChangeOut for bill printing
										if (FCConvert.ToBoolean(rsAddMeters.Get_Fields_Boolean("PrevChangeOut")))
										{
											rsAddMeters.Set_Fields("PrevChangeOut", false);
											rsAddMeters.Set_Fields("PrevChangeOutConsumption", 0);
											// kjr 11-7-2016 trout-1118
										}
										if (FCConvert.ToBoolean(rsAddMeters.Get_Fields_Boolean("UseMeterChangeOut")))
										{
											rsAddMeters.Set_Fields("UseMeterChangeOut", false);
											rsAddMeters.Set_Fields("PrevChangeOut", true);
											rsAddMeters.Set_Fields("PrevChangeOutConsumption", rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter"));
											// kjr 11-7-2016 trout-1118
											rsAddMeters.Set_Fields("ConsumptionOfChangedOutMeter", 0);
											rsAddMeters.Set_Fields("ReplacementConsumption", 0);
										}
										rsAddMeters.Set_Fields("FinalEndDate", 0);
										rsAddMeters.Set_Fields("FinalStartDate", 0);
										rsAddMeters.Set_Fields("FinalBillDate", 0);
										rsAddMeters.Set_Fields("Final", false);
										rsAddMeters.Update();
										rsAddMeters.MoveNext();
									}
									while (rsAddMeters.EndOfFile() != true);
								}
							}
						}
						rsBill.Set_Fields("ReadingUnits", rsMeter.Get_Fields_Int32("Multiplier"));
						// this will save the multiplier on the bill
						if ((FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "S" && rsBill.Get_Fields_Double("WPrinPaid") != 0) || (FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "W" && rsBill.Get_Fields_Double("SPrinPaid") != 0))
						{
							// kk04222014 trout-1081  Don't change service if a prepay exists on other service
							rsBill.Set_Fields("Service", "B");
						}
						else
						{
							if (FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "")
							{
								rsBill.Set_Fields("Service", modUTStatusPayments.Statics.TownService);
							}
							else
							{
								rsBill.Set_Fields("Service", rsMeter.Get_Fields_String("Service"));
							}
						}
					}
					else
					{
						// if no meters found then default the service to
						rsBill.Set_Fields("Service", modUTStatusPayments.Statics.TownService);
					}
					if (rsBill.Update())
					{
						TransferBillingAmounts = true;
					}
				}
				else
				{
					// bill not found, skip it
					TransferBillingAmounts = false;
				}
				return TransferBillingAmounts;
			}
			catch (Exception ex)
			{
				
				TransferBillingAmounts = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Transferring Billing Amounts - " + FCConvert.ToString(lngBillKey), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return TransferBillingAmounts;
		}

		private static void TransferBreakDownRecords_2(int lngBK)
		{
			TransferBreakDownRecords(ref lngBK);
		}

		private static void TransferBreakDownRecords(ref int lngBK)
		{
			int lngErr = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This routine will move all of the BreakDown records from the temporary table to the permanent one
				clsDRWrapper rsBreakDown = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strInput = "";
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsMaster = new clsDRWrapper();
				rsTemp.OpenRecordset("SELECT * FROM TempBreakDown WHERE BillKey = " + FCConvert.ToString(lngBK) + " ORDER BY Service, Type, Description", modExtraModules.strUTDatabase);
				rsBreakDown.OpenRecordset("SELECT * FROM BreakDown WHERE BillKey = 0", modExtraModules.strUTDatabase);
				while (!rsTemp.EndOfFile())
				{
					// TODO Get_Fields: Field [Key] not found!! (maybe it is an alias?)
					lngErr = FCConvert.ToInt32(Math.Round(Conversion.Val(rsTemp.Get_Fields("Key"))));
					rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + rsTemp.Get_Fields_Int32("BillKey"));
					rsMaster.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsBill.Get_Fields_Int32("AccountKey"));

					strInput = "INSERT INTO BreakDown (BillKey, MeterKey, Description, [Value], ADJCode, AccountNumber, Amount, Type, RateKey, Service, DateCreated, TaxAcquired)" + " VALUES (" + rsTemp.Get_Fields_Int32("BillKey") + "," + rsTemp.Get_Fields_Int32("MeterKey") + ",'" + rsTemp.Get_Fields_String("Description") + "'," + rsTemp.Get_Fields("Value") + "," + rsTemp.Get_Fields_Int32("ADJCode") + ",'" + rsTemp.Get_Fields("AccountNumber") + "'," + rsTemp.Get_Fields("Amount") + ",'" + rsTemp.Get_Fields("Type") + "'," + rsTemp.Get_Fields_Int32("RateKey") + ",'" + rsTemp.Get_Fields_String("Service") + "','" + rsTemp.Get_Fields_DateTime("DateCreated") + "'," + FCConvert.ToInt32(modUTStatusPayments.IsAccountTaxAcquired_7(rsMaster.Get_Fields("AccountNumber"))) + ")";
					// save a new record for this
					rsBreakDown.Execute(strInput, modExtraModules.strUTDatabase);
					rsTemp.MoveNext();
				}
				// kk07142014 trouts-106  Final billing hangs
				rsTemp.Execute("DELETE FROM TempBreakDown WHERE BillKey = " + FCConvert.ToString(lngBK), modExtraModules.strUTDatabase);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Transferring Breakdown Records - " + FCConvert.ToString(lngErr), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static bool CalculateInterestAtBilling_2(string strBooks, DateTime dtBillDate/* = DateTime.Now */, string strLastBill = "", bool boolFlateRateChange = false)
		{
			return CalculateInterestAtBilling( strBooks, dtBillDate, strLastBill, boolFlateRateChange);
		}

		public static bool CalculateInterestAtBilling_18(string strBooks,DateTime dtBillDate/* = DateTime.Now */, string strLastBill = "", bool boolFlateRateChange = false)
		{
			return CalculateInterestAtBilling( strBooks,  dtBillDate, strLastBill, boolFlateRateChange);
		}

		public static bool CalculateInterestAtBilling_68(string strBooks, bool boolFlateRateChange = false)
		{
			DateTime dtBillDate = DateTime.Now;
			return CalculateInterestAtBilling( strBooks,  dtBillDate, "", boolFlateRateChange);
		}

		public static bool CalculateInterestAtBilling(string strBooks,  DateTime dtBillDate, string strLastBill = "", bool boolFlateRateChange = false)
		{
			bool CalculateInterestAtBilling = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will go through the each bill and charge any current interest to each
				clsDRWrapper rsBills = new clsDRWrapper();
				clsDRWrapper rsLiens = new clsDRWrapper();
				clsDRWrapper rsF = new clsDRWrapper();
				double dblTotalDue;
				double dblCurrentInterest = 0;
				int lngKey = 0;
				bool boolLien = false;
				bool blnForceBillIntDate = false;
				// MAL@20080716 ; Tracker Reference: 14640
				if (fecherFoundation.FCUtils.IsNull(dtBillDate))
				{
					dtBillDate = DateTime.Today;
				}
				//else if (dtBillDate.ToOADate() == FCConvert.ToDateTime("12:00:00 AM").ToOADate())
				else if (dtBillDate.ToOADate() == 0)
				{
					dtBillDate = DateTime.Today;
				}
				if (Strings.Trim(Strings.UCase(modGlobalConstants.Statics.MuniName)) == "RICHMOND UT" || Strings.Trim(Strings.UCase(modGlobalConstants.Statics.MuniName)) == "BOWDOINHAM UT")
				{
					blnForceBillIntDate = true;
				}
				else
				{
					blnForceBillIntDate = false;
				}
				modGlobalFunctions.AddCYAEntry_8("UT", "Interest On Demand");
				rsF.OpenRecordset("SELECT AccountNumber, ID, FinalBill FROM Master");
				if (strBooks != "")
				{
					rsBills.OpenRecordset("SELECT * FROM Bill WHERE Book IN (" + strBooks + ")" + strLastBill, modExtraModules.strUTDatabase);
				}
				else
				{
					rsBills.OpenRecordset("SELECT * FROM Bill" + strLastBill, modExtraModules.strUTDatabase);
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Calculating Interest", true, rsBills.RecordCount(), true);
				while (!rsBills.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					// If .Fields("Bill") = 69 Then
					// MsgBox "hi"
					// End If
					rsF.FindFirstRecord("ID", rsBills.Get_Fields_Int32("AccountKey"));
					if (!rsF.NoMatch)
					{
						if (FCConvert.ToBoolean(rsF.Get_Fields_Boolean("FinalBill")))
						{
							goto SKIPBILL;
						}
					}
					// check the water first
					if (FCConvert.ToString(rsBills.Get_Fields_String("Service")) != "S")
					{
						// If .Fields("WPrinOwed") > .Fields("WPrinPaid") Then
						// frmWait.Top = -2000
						// MsgBox "stop"
						// End If
						dblCurrentInterest = 0;
						boolLien = false;
						if (FCConvert.ToInt32(rsBills.Get_Fields_Int32("WLienRecordNumber")) == 0)
						{
							double dblCurPrin = 0;
							double dblCurInt = 0;
							double dblCurCost = 0;
							double dblCurTax = 0;
							double dblPerDiem = 0;
							if (!boolFlateRateChange)
							{
								// MAL@20080716: Change to use the bill date as the interest calculation date - not the current date
								// Tracker Reference: 14640
								// dblTotalDue = CalculateAccountUT(rsBills, Date, dblCurrentInterest, True, , , , , , , , , True)
								bool boolForcePerDiem = true;
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsBills, ref dtBillDate, ref dblCurrentInterest, true, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, DateTime.Now, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
							}
							else
							{
								// dblTotalDue = CalculateAccountUT(rsBills, Date, dblCurrentInterest, True)
								bool boolForcePerDiem = false;
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsBills, ref dtBillDate, ref dblCurrentInterest, true, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, DateTime.Now, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
							}
							lngKey = FCConvert.ToInt32(rsBills.Get_Fields_Int32("ID"));
						}
						else
						{
							lngKey = FCConvert.ToInt32(rsBills.Get_Fields_Int32("WLienRecordNumber"));
							rsLiens.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
							if (!rsLiens.EndOfFile())
							{
								boolLien = true;
								// dblTotalDue = CalculateAccountUTLien(rsLiens, Date, dblCurrentInterest, True)
								dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLiens, dtBillDate, ref dblCurrentInterest, true, 0, 0, 0, false, false, null, 0, 0, 0, blnForceBillIntDate);
							}
						}
						if (dblCurrentInterest != 0)
						{
							if (boolLien)
							{
								// this will edit the lien records charged interest
								rsLiens.Edit();
								rsLiens.Set_Fields("IntPaidDate", DateTime.Today);
								// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
								rsLiens.Set_Fields("IntAdded", rsLiens.Get_Fields("IntAdded") - dblCurrentInterest);
								rsLiens.Update();
							}
							else
							{
								// this will edit the bill records charged interest
								rsBills.Edit();
								rsBills.Set_Fields("WIntPaidDate", DateTime.Today);
								rsBills.Set_Fields("WIntAdded", rsBills.Get_Fields_Double("WIntAdded") - dblCurrentInterest);
								rsBills.Update();
							}
							modUTStatusPayments.CreateCHGINTForBill_20(true, lngKey, rsBills.Get_Fields_Int32("AccountKey"), boolLien, dblCurrentInterest);
						}
					}
					// check the sewer
					if (FCConvert.ToString(rsBills.Get_Fields_String("Service")) != "W")
					{
						dblCurrentInterest = 0;
						boolLien = false;
						// check the water amount and charge any interest that has accrued
						if (FCConvert.ToInt32(rsBills.Get_Fields_Int32("SLienRecordNumber")) == 0)
						{
							double dblCurPrin = 0;
							double dblCurInt = 0;
							double dblCurCost = 0;
							double dblCurTax = 0;
							double dblPerDiem = 0;
							if (!boolFlateRateChange)
							{
								// dblTotalDue = CalculateAccountUT(rsBills, Date, dblCurrentInterest, False, , , , , , , , , True)
								bool boolForcePerDiem = true;
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsBills, ref dtBillDate, ref dblCurrentInterest, false, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, DateTime.Now, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
							}
							else
							{
								// dblTotalDue = CalculateAccountUT(rsBills, Date, dblCurrentInterest, False)
								bool boolForcePerDiem = false;
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsBills, ref dtBillDate, ref dblCurrentInterest, false, ref dblCurPrin, ref dblCurInt, ref dblCurCost, false, false, DateTime.Now, ref dblCurTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
							}
							lngKey = FCConvert.ToInt32(rsBills.Get_Fields_Int32("ID"));
						}
						else
						{
							lngKey = FCConvert.ToInt32(rsBills.Get_Fields_Int32("SLienRecordNumber"));
							rsLiens.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngKey), modExtraModules.strUTDatabase);
							if (!rsLiens.EndOfFile())
							{
								boolLien = true;
								// dblTotalDue = CalculateAccountUTLien(rsLiens, Date, dblCurrentInterest, False)
								dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsLiens, dtBillDate, ref dblCurrentInterest, false, 0, 0, 0, false, false, null, 0, 0, 0, blnForceBillIntDate);
							}
						}
						if (dblCurrentInterest != 0)
						{
							if (boolLien)
							{
								// this will edit the lien records charged interest
								rsLiens.Edit();
								// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
								rsLiens.Set_Fields("IntAdded", rsLiens.Get_Fields("IntAdded") - dblCurrentInterest);
								rsLiens.Set_Fields("IntPaidDate", DateTime.Today);
								rsLiens.Update();
							}
							else
							{
								// this will edit the bill records charged interest
								rsBills.Edit();
								rsBills.Set_Fields("SIntPaidDate", DateTime.Today);
								rsBills.Set_Fields("SIntAdded", rsBills.Get_Fields_Double("SIntAdded") - dblCurrentInterest);
								rsBills.Update();
							}
							modUTStatusPayments.CreateCHGINTForBill_20(false, lngKey, rsBills.Get_Fields_Int32("AccountKey"), boolLien, dblCurrentInterest);
						}
					}
					SKIPBILL:
					;
					rsBills.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				CalculateInterestAtBilling = true;
				return CalculateInterestAtBilling;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				CalculateInterestAtBilling = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + FCConvert.ToString(Information.Err(ex).Number) + ".", "Error Calculating Interest", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateInterestAtBilling;
		}

		public static string FindAccountNumber_60(int lngMeterKey, bool boolWater, int lngRateKey, bool boolTaxAccount = false)
		{
			return FindAccountNumber(ref lngMeterKey, ref boolWater, ref lngRateKey, boolTaxAccount);
		}

		public static string FindAccountNumber(ref int lngMeterKey, ref bool boolWater, ref int lngRateKey, bool boolTaxAccount = false)
		{
			string FindAccountNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				int lngAcctKey;
				int intCT;
				string strWS = "";
				int lngCat = 0;
				if (boolWater)
				{
					strWS = "Water";
				}
				else
				{
					strWS = "Sewer";
				}
				if (!boolTaxAccount)
				{
					// do not check these overrides for tax
					rsM.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(lngMeterKey), modExtraModules.strUTDatabase);
					if (!rsM.EndOfFile())
					{
						if (lngRateKey != 0)
						{
							for (intCT = 1; intCT <= 5; intCT++)
							{
								// TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
								if (FCConvert.ToBoolean(rsM.Get_Fields("UseRate" + FCConvert.ToString(intCT))))
								{
									if (FCConvert.ToInt32(rsM.Get_Fields(strWS + "Key" + FCConvert.ToString(intCT))) == lngRateKey)
									{
										FindAccountNumber = FCConvert.ToString(rsM.Get_Fields(strWS + "Account" + FCConvert.ToString(intCT)));
										break;
									}
								}
							}
						}
						if (Strings.Trim(FindAccountNumber) == "")
						{
							// nothing found in the meter...
							// Now check the account
							rsM.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsM.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
							if (!rsM.EndOfFile())
							{
								if (boolWater)
								{
									lngCat = FCConvert.ToInt32(rsM.Get_Fields_Int32("WaterCategory"));
								}
								else
								{
									lngCat = FCConvert.ToInt32(rsM.Get_Fields_Int32("SewerCategory"));
								}
								if (FCConvert.ToString(rsM.Get_Fields(strWS + "Account")) != "")
								{
									FindAccountNumber = FCConvert.ToString(rsM.Get_Fields(strWS + "Account"));
								}
							}
						}
					}
					if (Strings.Trim(FindAccountNumber) == "" || Strings.InStr(1, FindAccountNumber, "_", CompareConstants.vbBinaryCompare) != 0)
					{
						// if it is blank or has an invalid account then use the default from the Category Screen if any
						rsM.OpenRecordset("SELECT * FROM Category WHERE Code = " + FCConvert.ToString(lngCat), modExtraModules.strUTDatabase);
						if (!rsM.EndOfFile())
						{
							if (boolWater)
							{
								FindAccountNumber = FCConvert.ToString(rsM.Get_Fields_String("WaterAccount"));
							}
							else
							{
								FindAccountNumber = FCConvert.ToString(rsM.Get_Fields_String("SewerAccount"));
							}
						}
					}
				}
				// last ditch is to check the UT defaults from the customize screen
				if (Strings.Trim(FindAccountNumber) == "" || Strings.InStr(1, FindAccountNumber, "_", CompareConstants.vbBinaryCompare) != 0)
				{
					// if it is blank or has an invalid account then use the default from the UT Customize screen
					rsM.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
					if (!rsM.EndOfFile())
					{
						if (boolTaxAccount)
						{
							if (boolWater)
							{
								FindAccountNumber = FCConvert.ToString(rsM.Get_Fields_String("WaterTaxAccount"));
							}
							else
							{
								FindAccountNumber = FCConvert.ToString(rsM.Get_Fields_String("SewerTaxAccount"));
							}
						}
						else
						{
							if (boolWater)
							{
								FindAccountNumber = FCConvert.ToString(rsM.Get_Fields_String("WaterPrincipalAccount"));
							}
							else
							{
								FindAccountNumber = FCConvert.ToString(rsM.Get_Fields_String("SewerPrincipalAccount"));
							}
						}
					}
				}
				return FindAccountNumber;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Account Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindAccountNumber;
		}

		public static bool CheckForUTAccountsSetup()
		{
			bool CheckForUTAccountsSetup = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This function will return true if all of the accounts needed are setup
				clsDRWrapper rsAcct = new clsDRWrapper();
				CheckForUTAccountsSetup = true;
				// start with this true
				rsAcct.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!rsAcct.EndOfFile())
				{
                    if (modGlobalConstants.Statics.gboolBD)
                    {
                        modValidateAccount.SetBDFormats();
                    }
					if (modUTStatusPayments.Statics.TownService != "W")
					{
						CheckForUTAccountsSetup = CheckForUTAccountsSetup && modValidateAccount.AccountValidate(rsAcct.Get_Fields_String("SewerTaxAccount"));
						CheckForUTAccountsSetup = CheckForUTAccountsSetup && modValidateAccount.AccountValidate(rsAcct.Get_Fields_String("SewerPrincipalAccount"));
					}
					if (modUTStatusPayments.Statics.TownService != "S")
					{
						CheckForUTAccountsSetup = CheckForUTAccountsSetup && modValidateAccount.AccountValidate(rsAcct.Get_Fields_String("WaterPrincipalAccount"));
						CheckForUTAccountsSetup = CheckForUTAccountsSetup && modValidateAccount.AccountValidate(rsAcct.Get_Fields_String("WaterTaxAccount"));
					}
				}
				return CheckForUTAccountsSetup;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckForUTAccountsSetup;
		}
		// vbPorter upgrade warning: intType As short	OnWriteFCConvert.ToInt32(
		public static bool BuildOutPrintingFile_162(string strSQL, int intType, int lngRK, DateTime dtStatementDate, bool blnExcludeZeroBills, bool modalDialog)
		{
			return BuildOutPrintingFile(ref strSQL, ref intType, ref lngRK, ref dtStatementDate, ref blnExcludeZeroBills, modalDialog);
		}

		public static bool BuildOutPrintingFile(ref string strSQL, ref int intType, ref int lngRK, ref DateTime dtStatementDate, ref bool blnExcludeZeroBills, bool modalDialog)
		{
			bool BuildOutPrintingFile = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This function will create a flat file with all of the bill amounts for the RK and books selected
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsRK = new clsDRWrapper();
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsCat = new clsDRWrapper();
				clsDRWrapper rsMeter = new clsDRWrapper();
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strBillPeriod = "";
				string strLocation = "";
				string strCatString = "";
				double dblWRegular = 0;
				double dblWTax = 0;
				double dblWPastDue = 0;
				double dblWTotalDue = 0;
				double dblWInterest = 0;
				double dblWPastDueInterest = 0;
				double dblWPreLienInterest = 0;
				double dblWLienAmount = 0;
				double dblWCurrentDue = 0;
				double dblWPastDueCurrentInterest = 0;
				double dblWPastDueCurrentLInterest = 0;
				double dblSRegular = 0;
				double dblSTax = 0;
				double dblSPastDue = 0;
				double dblSTotalDue = 0;
				double dblSInterest = 0;
				double dblSPastDueInterest = 0;
				double dblSPreLienInterest = 0;
				double dblSLienAmount = 0;
				double dblSCurrentDue = 0;
				double dblSPastDueCurrentInterest = 0;
				double dblSPastDueCurrentLInterest = 0;
				double dblOverallTotalDue = 0;
				double dblOverallTotalCurrent = 0;
				double dblOverallTotalPastDue = 0;
				int intAmountFieldLength;
				int lngCount;
				double dblWUnitCount = 0;
				double dblSUnitCount = 0;
				double dblWConsum = 0;
				double dblWUnits = 0;
				double dblWFlat = 0;
				double dblWAdjust = 0;
				double dblSConsum = 0;
				double dblSUnits = 0;
				double dblSFlat = 0;
				double dblSAdjust = 0;
				int lngCT;
				string strBillMessage = "";
				bool boolSecondPass = false;
				bool boolSameOwner = false;
				bool boolShowOwnerInfo = false;
				bool boolWBillOwner = false;
				bool boolSBillOwner = false;
				bool boolSplitBill = false;
				// vbPorter upgrade warning: lngCons As int	OnWrite(int, double)
				int[] lngCons = new int[3 + 1];
				// vbPorter upgrade warning: lngPrev As int	OnWrite(short, double)
				int[] lngPrev = new int[3 + 1];
				// vbPorter upgrade warning: lngCurr As int	OnWrite(short, double)
				int[] lngCurr = new int[3 + 1];
				string[] strEstim = new string[3 + 1];
				string[] strChgOut = new string[3 + 1];
				int lngWCons = 0;
				int lngSCons = 0;
				string strRet1 = "";
				string strRet2 = "";
				string strRet3 = "";
				string strRet4 = "";
				// vbPorter upgrade warning: dblWSplit1 As string	OnWriteFCConvert.ToInt32(
				string dblWSplit1 = "";
				// vbPorter upgrade warning: dblWSplit2 As string	OnWriteFCConvert.ToInt32(
				string dblWSplit2 = "";
				// vbPorter upgrade warning: dblSSplit1 As string	OnWriteFCConvert.ToInt32(
				string dblSSplit1 = "";
				// vbPorter upgrade warning: dblSSplit2 As string	OnWriteFCConvert.ToInt32(
				string dblSSplit2 = "";
				double dblWPrinPaid = 0;
				double dblSPrinPaid = 0;
				int lngUnitsToShow = 0;
				string[,] aryWAdjItmz = new string[5 + 1, 2 + 1];
				string[,] arySAdjItmz = new string[5 + 1, 2 + 1];
				bool boolChpt660 = false;
				string strEBillType = "";
				bool blnContinue = false;
				double dblSActualCons = 0;
				// trout-684
				int[] lngChgOutCons = new int[3 + 1];
				// trout-1118
				bool boolInclChangeOutCons = false;
				intAmountFieldLength = 8;
				rsRK.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				if (!rsRK.EndOfFile())
				{
					boolWBillOwner = FCConvert.ToBoolean(rsRK.Get_Fields_Boolean("WBillToOwner"));
					boolSBillOwner = FCConvert.ToBoolean(rsRK.Get_Fields_Boolean("SBillToOwner"));
					strRet1 = FCConvert.ToString(rsRK.Get_Fields_String("UTReturnAddress1"));
					strRet2 = FCConvert.ToString(rsRK.Get_Fields_String("UTReturnAddress2"));
					strRet3 = FCConvert.ToString(rsRK.Get_Fields_String("UTReturnAddress3"));
					strRet4 = FCConvert.ToString(rsRK.Get_Fields_String("UTReturnAddress4"));
					if (Conversion.Val(rsRK.Get_Fields_Int32("ReadingUnitsOnBill")) != 0)
					{
						lngUnitsToShow = FCConvert.ToInt32(Math.Round(Conversion.Val(rsRK.Get_Fields_Int32("ReadingUnitsOnBill"))));
					}
					else
					{
						lngUnitsToShow = 1;
					}
					boolChpt660 = FCConvert.ToBoolean(rsRK.Get_Fields_Boolean("OutprintChapt660"));
					boolInclChangeOutCons = FCConvert.ToBoolean(rsRK.Get_Fields_Int32("OutPrintIncludeChangeOutCons"));
					// trout-1118
				}
				else
				{
					boolWBillOwner = true;
					boolSBillOwner = true;
				}
				rsMeter.OpenRecordset("SELECT * FROM MeterTable", modExtraModules.strUTDatabase);
				rsCat.OpenRecordset("SELECT * FROM Category", modExtraModules.strUTDatabase);
				rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
				if (!rsRK.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
					strBillPeriod = Strings.Left(Strings.Format(rsRK.Get_Fields_DateTime("Start"), "MMM"), 3) + "-" + Strings.Left(Strings.Format(rsRK.Get_Fields("End"), "MMM"), 3);
				}
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Creating Outprinting File", true, rsData.RecordCount(), true);
				// Open "TWUTBillExport.DAT" For Output As #1
				FCFileSystem.FileOpen(2, "TWUTBillExport.CSV", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				// Build a header row for Wise with Muni Name and Program Version starting with an
				FCFileSystem.Write(2, "TRIOHEADER");
				FCFileSystem.Write(2, "Name : " + modGlobalConstants.Statics.MuniName);
				FCFileSystem.Write(2, "UT Version : " + Strings.Trim(FCConvert.ToString((new WindowsFormsApplicationBase()).Info.Version.Major) + "." + FCConvert.ToString((new WindowsFormsApplicationBase()).Info.Version.Minor) + "." + FCConvert.ToString((new WindowsFormsApplicationBase()).Info.Version.Revision)));
				FCFileSystem.Write(2, strRet1);
				FCFileSystem.Write(2, strRet2);
				FCFileSystem.Write(2, strRet3);
				FCFileSystem.WriteLine(2, strRet4);
				// Print header row
				FCFileSystem.Write(2, "Account");
				FCFileSystem.Write(2, "Billed Name");
				FCFileSystem.Write(2, "Billed Name 2");
				FCFileSystem.Write(2, "Address 1");
				FCFileSystem.Write(2, "Address 2");
				FCFileSystem.Write(2, "Address 3");
				FCFileSystem.Write(2, "City");
				FCFileSystem.Write(2, "State");
				FCFileSystem.Write(2, "Zip");
				FCFileSystem.Write(2, "Zip 4");
				FCFileSystem.Write(2, "Category");
				FCFileSystem.Write(2, "Map Lot");
				FCFileSystem.Write(2, "Location");
				FCFileSystem.Write(2, "Bill Message");
				FCFileSystem.Write(2, "Statement Date");
				FCFileSystem.Write(2, "Billing Period");
				FCFileSystem.Write(2, "Reading Date");
				FCFileSystem.Write(2, "Start Date");
				FCFileSystem.Write(2, "End Date");
				FCFileSystem.Write(2, "Interest Date");
				FCFileSystem.Write(2, "Total Overall Current");
				FCFileSystem.Write(2, "Total Overall Past Due");
				FCFileSystem.Write(2, "Overall Total");
				FCFileSystem.Write(2, "Book");
				FCFileSystem.Write(2, "Sequence");
				FCFileSystem.Write(2, "Water Consumption");
				FCFileSystem.Write(2, "Water Unit Amount");
				FCFileSystem.Write(2, "Water Consumption Total");
				FCFileSystem.Write(2, "Water Units Total");
				FCFileSystem.Write(2, "Water Flat Total");
				FCFileSystem.Write(2, "Water Adjustment Total");
				FCFileSystem.Write(2, "Water Total w/o Adjs");
				FCFileSystem.Write(2, "Water Regular");
				FCFileSystem.Write(2, "Water Tax");
				FCFileSystem.Write(2, "Water Past Due");
				FCFileSystem.Write(2, "Water Credits");
				FCFileSystem.Write(2, "Water Total Amount Due");
				FCFileSystem.Write(2, "Water Interest");
				FCFileSystem.Write(2, "Water Lien Amount");
				FCFileSystem.Write(2, "Water Current Due");
				FCFileSystem.Write(2, "Total Water Past Due");
				FCFileSystem.Write(2, "Sewer Consumption");
				FCFileSystem.Write(2, "Sewer Unit Amount");
				FCFileSystem.Write(2, "Sewer Consumption Total");
				FCFileSystem.Write(2, "Sewer Units Total");
				FCFileSystem.Write(2, "Sewer Flat Total");
				FCFileSystem.Write(2, "Sewer Adjustment Total");
				FCFileSystem.Write(2, "Sewer Total w/o Adjs");
				FCFileSystem.Write(2, "Sewer Regular");
				FCFileSystem.Write(2, "Sewer Tax");
				FCFileSystem.Write(2, "Sewer Past Due");
				FCFileSystem.Write(2, "Sewer Credits");
				FCFileSystem.Write(2, "Sewer Total Amount Due");
				FCFileSystem.Write(2, "Sewer Interest");
				FCFileSystem.Write(2, "Sewer Lien Amount");
				FCFileSystem.Write(2, "Sewer Current Due");
				FCFileSystem.Write(2, "Total Sewer Past Due");
				FCFileSystem.Write(2, "Meter 1 Consumption");
				FCFileSystem.Write(2, "Meter 1 Previous");
				FCFileSystem.Write(2, "Meter 1 Current");
				FCFileSystem.Write(2, "Meter 2 Consumption");
				FCFileSystem.Write(2, "Meter 2 Previous");
				FCFileSystem.Write(2, "Meter 2 Current");
				FCFileSystem.Write(2, "Meter 3 Consumption");
				FCFileSystem.Write(2, "Meter 3 Previous");
				FCFileSystem.Write(2, "Meter 3 Current");
				FCFileSystem.Write(2, "Amount A");
				// split 1 for water
				FCFileSystem.Write(2, "Amount B");
				// split 2 for water
				FCFileSystem.Write(2, "Amount C");
				// split 1 for sewer
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
				{
					FCFileSystem.Write(2, "Amount D");
					// split 2 for sewer
					FCFileSystem.Write(2, "Note");
				}
				else
				{
					FCFileSystem.Write(2, "Amount D");
					// split 2 for sewer
					// kgk trout-684 04-21-2011  Add original consumption amount to outprint
					if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
					{
						FCFileSystem.Write(2, "Actual Consumption Amount");
					}
				}
				if (!boolChpt660)
				{
					if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR" || boolInclChangeOutCons)
					{
						FCFileSystem.Write(2, "Due Date");
					}
					else
					{
						FCFileSystem.WriteLine(2, "Due Date");
					}
				}
				else
				{
					FCFileSystem.Write(2, "Due Date");
					// kgk trout-764 & 765 10-18-2011  Write the estimated indicator and meter changeout indicator
					// Based on an option in Customize or a separate Outprinting File option in the combobox?
					FCFileSystem.Write(2, "Meter 1 Est/Act");
					FCFileSystem.Write(2, "Meter 1 Changed Out");
					FCFileSystem.Write(2, "Meter 2 Est/Act");
					FCFileSystem.Write(2, "Meter 2 Changed Out");
					FCFileSystem.Write(2, "Meter 3 Est/Act");
					FCFileSystem.Write(2, "Meter 3 Changed Out");
					// Itemize Adjustments
					// We can loop through the adjust table and write all of the adjustments for the service(s)
					// or we can do Adj1,Desc1,Adj2,Desc2,...
					for (lngCT = 1; lngCT <= Information.UBound(aryWAdjItmz, 1); lngCT++)
					{
						//Application.DoEvents();
						FCFileSystem.Write(2, "Water Adjustment " + FCConvert.ToString(lngCT) + " Descr");
						FCFileSystem.Write(2, "Water Adjustment " + FCConvert.ToString(lngCT) + " Amount");
					}
					for (lngCT = 1; lngCT <= Information.UBound(arySAdjItmz, 1); lngCT++)
					{
						//Application.DoEvents();
						FCFileSystem.Write(2, "Sewer Adjustment " + FCConvert.ToString(lngCT) + " Descr");
						if (lngCT < Information.UBound(arySAdjItmz, 1))
						{
							FCFileSystem.Write(2, "Sewer Adjustment " + FCConvert.ToString(lngCT) + " Amount");
						}
						else
						{
							if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR" || boolInclChangeOutCons)
							{
								FCFileSystem.Write(2, "Sewer Adjustment " + FCConvert.ToString(lngCT) + " Amount");
							}
							else
							{
								FCFileSystem.WriteLine(2, "Sewer Adjustment " + FCConvert.ToString(lngCT) + " Amount");
							}
						}
					}
				}
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
				{
					FCFileSystem.Write(2, "Owner Name");
					// kjr 11/7/16 trout-1118
					if (boolInclChangeOutCons)
					{
						FCFileSystem.Write(2, "Owner Name 2");
					}
					else
					{
						FCFileSystem.WriteLine(2, "Owner Name 2");
					}
				}
				else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
				{
					FCFileSystem.Write(2, "Impervious Surface");
					// kjr 11/7/16 trout-1118
					if (boolInclChangeOutCons)
					{
						FCFileSystem.Write(2, "Previous Reading Date");
						// kk07272016 trouts-169  Add previous reading date
					}
					else
					{
						FCFileSystem.WriteLine(2, "Previous Reading Date");
						// kk07272016 trouts-169  Add previous reading date
					}
				}
				// kjr 11/7/16 trout-1118
				if (boolInclChangeOutCons)
				{
					FCFileSystem.Write(2, "Meter 1 Change Out Consumption");
					FCFileSystem.Write(2, "Meter 2 Change Out Consumption");
					FCFileSystem.WriteLine(2, "Meter 3 Change Out Consumption");
				}
				while (!rsData.EndOfFile())
				{
					//Application.DoEvents();
					if (!boolSecondPass)
					{
						frmWait.InstancePtr.IncrementProgress();
					}
					// SECONDBILL:
					// If rsData.Fields("AccountNumber") = 333760 Then
					// MsgBox "Stop"
					// End If
					// MAL@20080806: Check for IConnect E-bill Option
					// Tracker Reference: 10680
					if (modGlobalConstants.Statics.gboolIC)
					{
						if (AcceptsEBills(rsData.Get_Fields_Int32("AccountKey"), ref strEBillType))
						{
							if (strEBillType == "B")
							{
								// Continue
								blnContinue = true;
							}
							else
							{
								// E-Bill Only - Don't Include
								blnContinue = false;
							}
						}
						else
						{
							// Continue - Paper Bills
							blnContinue = true;
						}
					}
					else
					{
						// Continue
						blnContinue = true;
					}
					if (blnContinue)
					{
						boolSameOwner = false;
						boolSplitBill = false;
						rsMaster.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
						if (!rsMaster.EndOfFile())
						{
							boolSameOwner = FCConvert.ToBoolean(rsMaster.Get_Fields_Boolean("SameBillOwner"));
							// trout-718 08-02-2011 kgk  Bill to owner / tenant option by account and service
							boolWBillOwner = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("WBillOwner"));
							boolSBillOwner = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("SBillOwner"));
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("StreetNumber"))) != "")
							{
								// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
								strLocation = Strings.Trim(rsMaster.Get_Fields("StreetNumber") + " " + rsMaster.Get_Fields_String("StreetName"));
							}
							else
							{
								strLocation = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("StreetName")));
							}
							strBillMessage = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BillMessage")));
						}
						else
						{
							strLocation = "";
						}
						if (intType == 1)
						{
							rsCat.FindFirstRecord("Category", rsData.Get_Fields_Int32("SCat"));
							if (rsCat.NoMatch)
							{
								rsCat.FindFirstRecord("Category", rsData.Get_Fields_Int32("WCat"));
								if (rsCat.NoMatch)
								{
									strCatString = Strings.StrDup(25, " ");
								}
								else
								{
									strCatString = modGlobalFunctions.PadStringWithSpaces(rsCat.Get_Fields_String("LongDescription"), 25);
								}
							}
							else
							{
								strCatString = modGlobalFunctions.PadStringWithSpaces(rsCat.Get_Fields_String("LongDescription"), 25);
							}
						}
						else
						{
							rsCat.FindFirstRecord("Code", rsData.Get_Fields_Int32("WCat"));
							if (rsCat.NoMatch)
							{
								rsCat.FindFirstRecord("Code", rsData.Get_Fields_Int32("SCat"));
								if (rsCat.NoMatch)
								{
									strCatString = "";
								}
								else
								{
									strCatString = Strings.Trim(FCConvert.ToString(rsCat.Get_Fields_String("LongDescription")));
								}
							}
							else
							{
								strCatString = Strings.Trim(FCConvert.ToString(rsCat.Get_Fields_String("LongDescription")));
							}
						}
						strCatString = Strings.Trim(strCatString);
						if (boolSameOwner)
						{
							// this will combine the bill
							boolShowOwnerInfo = true;
							boolSplitBill = false;
						}
						else
						{
							if (modUTStatusPayments.Statics.TownService == "B")
							{
								if (boolWBillOwner != boolSBillOwner)
								{
									boolSplitBill = true;
									if (boolSecondPass)
									{
										if (modUTStatusPayments.Statics.gboolPayWaterFirst)
										{
											boolShowOwnerInfo = boolSBillOwner;
										}
										else
										{
											boolShowOwnerInfo = boolWBillOwner;
										}
									}
									else
									{
										if (FCConvert.ToString(rsData.Get_Fields_String("Service")) == "B")
										{
											if (modUTStatusPayments.Statics.gboolPayWaterFirst)
											{
												boolShowOwnerInfo = boolWBillOwner;
											}
											else
											{
												boolShowOwnerInfo = boolSBillOwner;
											}
										}
										else
										{
											boolSplitBill = false;
											if (FCConvert.ToString(rsData.Get_Fields_String("Service")) == "W")
											{
												boolShowOwnerInfo = boolWBillOwner;
											}
											else
											{
												boolShowOwnerInfo = boolSBillOwner;
											}
										}
									}
								}
								else
								{
									boolSameOwner = true;
									boolShowOwnerInfo = boolSBillOwner;
									boolSplitBill = false;
								}
							}
							else
							{
								if (FCConvert.ToString(rsData.Get_Fields_String("Service")) == "W")
								{
									boolShowOwnerInfo = boolWBillOwner;
								}
								else
								{
									boolShowOwnerInfo = boolSBillOwner;
								}
								boolSplitBill = false;
							}
						}
						// fills both, then these can be set to 0 if this bill gets split
						// MAL@20071106: Change how these values are being filled
						// Tracker Reference: 10947
						// lngSCons = rsData.Fields("Consumption") * glngTownReadingUnits / lngUnitsToShow
						// lngWCons = rsData.Fields("Consumption") * glngTownReadingUnits / lngUnitsToShow
						dblOverallTotalCurrent = 0;
						dblOverallTotalDue = 0;
						dblOverallTotalPastDue = 0;
						// Water
						dblWInterest = 0;
						dblWPastDueInterest = 0;
						dblWCurrentDue = 0;
						dblWRegular = 0;
						dblWPastDue = 0;
						dblWTotalDue = 0;
						dblWLienAmount = 0;
						dblWPastDueCurrentInterest = 0;
						dblWPastDueCurrentLInterest = 0;
						dblWPreLienInterest = 0;
						if (!boolSplitBill || ((modUTStatusPayments.Statics.gboolPayWaterFirst && !boolSecondPass) || (!modUTStatusPayments.Statics.gboolPayWaterFirst && boolSecondPass)))
						{
							if (intType != 1)
							{
								// This will get the current bill amount
								dblWRegular = rsData.Get_Fields_Double("WPrinOwed") + rsData.Get_Fields_Double("WIntOwed") + rsData.Get_Fields_Double("WCostOwed");
								dblWTax = rsData.Get_Fields_Double("WTaxOwed");
								dblWPrinPaid = rsData.Get_Fields_Double("WPrinPaid") + rsData.Get_Fields_Double("WTaxPaid") + rsData.Get_Fields_Double("WIntPaid") + rsData.Get_Fields_Double("WCostPaid");
								dblWCurrentDue = (rsData.Get_Fields_Double("WPrinOwed") + rsData.Get_Fields_Double("WTaxOwed") + rsData.Get_Fields_Double("WIntOwed") + rsData.Get_Fields_Double("WCostOwed") - rsData.Get_Fields_Double("WCostAdded") - rsData.Get_Fields_Double("WIntAdded") + dblWInterest) - (dblWPrinPaid);
								dblOverallTotalCurrent = dblWCurrentDue;
								dblWLienAmount = modUTCalculations.CalculateAccountUTTotal_4(rsData.Get_Fields_Int32("AccountKey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblWPastDueCurrentLInterest, ref dblWPastDueInterest, ref dtStatementDate, true, ref dblWPreLienInterest);
								dblWPastDueInterest = 0;
								// kk 10022013 trout-999  dblWPastDueInterest is getting doubled
								dblWTotalDue = modUTCalculations.CalculateAccountUTTotal_3(rsData.Get_Fields_Int32("AccountKey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblWPastDueCurrentInterest, ref dblWPastDueInterest, ref dtStatementDate);
								// this will calculate the non lien past due by taking the total and subtracting the current and the lien amount
								dblWPastDue = dblWTotalDue - dblWCurrentDue - dblWLienAmount - (dblWPastDueCurrentInterest - dblWPastDueCurrentLInterest);
							}
							dblOverallTotalPastDue += dblWPastDue + dblWPastDueCurrentInterest + dblWLienAmount - dblWPastDueCurrentLInterest;
							// Overall Totals
							dblOverallTotalDue += FCUtils.Round(dblWTotalDue, 2);
							if (modUTCalculations.Statics.gboolSplitBills)
							{
								dblWSplit1 = FCConvert.ToString(FCUtils.Round(dblWCurrentDue / 2, 2));
								dblWSplit2 = FCConvert.ToString(FCUtils.Round(dblWCurrentDue - FCConvert.ToDouble(dblWSplit1), 2));
							}
							else
							{
								dblWSplit1 = FCConvert.ToString(FCUtils.Round(dblWCurrentDue, 2));
								dblWSplit2 = FCConvert.ToString(0);
							}
						}
						else
						{
							// put in all zeros for all the strings
							dblWRegular = 0;
							dblWTax = 0;
							dblWCurrentDue = 0;
							dblWLienAmount = 0;
							dblWTotalDue = 0;
							dblWPastDue = 0;
							dblWPastDueCurrentInterest = 0;
							dblWCurrentDue = 0;
							lngWCons = 0;
							dblWSplit1 = FCConvert.ToString(0);
							dblWSplit2 = FCConvert.ToString(0);
						}
						// Sewer
						dblSInterest = 0;
						dblSPastDueInterest = 0;
						dblSCurrentDue = 0;
						dblSRegular = 0;
						dblSPastDue = 0;
						dblSTotalDue = 0;
						dblSLienAmount = 0;
						dblSPastDueCurrentInterest = 0;
						dblSPastDueCurrentLInterest = 0;
						dblSPreLienInterest = 0;
						if (!boolSplitBill || ((modUTStatusPayments.Statics.gboolPayWaterFirst && boolSecondPass) || (!modUTStatusPayments.Statics.gboolPayWaterFirst && !boolSecondPass)))
						{
							if (intType != 0)
							{
								// This will get the current bill amount
								dblSRegular = rsData.Get_Fields_Double("SPrinOwed") + rsData.Get_Fields_Double("SIntOwed") + rsData.Get_Fields_Double("SCostOwed");
								dblSTax = rsData.Get_Fields_Double("STaxOwed");
								dblSPrinPaid = rsData.Get_Fields_Double("SPrinPaid") + rsData.Get_Fields_Double("STaxPaid") + rsData.Get_Fields_Double("SIntPaid") + rsData.Get_Fields_Double("SCostPaid");
								dblSCurrentDue = (rsData.Get_Fields_Double("SPrinOwed") + rsData.Get_Fields_Double("STaxOwed") + rsData.Get_Fields_Double("SIntOwed") + rsData.Get_Fields_Double("SCostOwed") - rsData.Get_Fields_Double("SCostAdded") - rsData.Get_Fields_Double("SIntAdded") + dblSInterest) - (dblSPrinPaid);
								dblOverallTotalCurrent += dblSCurrentDue;
								dblSLienAmount = modUTCalculations.CalculateAccountUTTotal_4(rsData.Get_Fields_Int32("AccountKey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblSPastDueCurrentLInterest, ref dblSPastDueInterest, ref dtStatementDate, true, ref dblSPreLienInterest);
								dblSPastDueInterest = 0;
								// kk 04172013 trout-999  dblSPastDueInterest is getting doubled
								dblSTotalDue = modUTCalculations.CalculateAccountUTTotal_3(rsData.Get_Fields_Int32("AccountKey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblSPastDueCurrentInterest, ref dblSPastDueInterest, ref dtStatementDate);
								// this will calculate the non lien past due by taking the total and subtracting the current and the lien amount
								dblSPastDue = dblSTotalDue - dblSCurrentDue - dblSLienAmount - (dblSPastDueCurrentInterest - dblSPastDueCurrentLInterest);
							}
							dblOverallTotalPastDue += dblSPastDue + dblSPastDueCurrentInterest + dblSLienAmount - dblSPastDueCurrentLInterest;
							if (modUTCalculations.Statics.gboolSplitBills)
							{
								dblSSplit1 = FCConvert.ToString(FCUtils.Round(dblSCurrentDue / 2, 2));
								dblSSplit2 = FCConvert.ToString(FCUtils.Round(dblSCurrentDue - FCConvert.ToDouble(dblSSplit1), 2));
							}
							else
							{
								dblSSplit1 = FCConvert.ToString(FCUtils.Round(dblSCurrentDue, 2));
								dblSSplit2 = FCConvert.ToString(0);
							}
						}
						else
						{
							// put in all zeros for all the strings
							dblSRegular = 0;
							dblSTax = 0;
							dblSCurrentDue = 0;
							dblSLienAmount = 0;
							dblSTotalDue = 0;
							dblSPastDue = 0;
							dblSPastDueCurrentInterest = 0;
							dblSCurrentDue = 0;
							lngSCons = 0;
							dblSSplit1 = FCConvert.ToString(0);
							dblSSplit2 = FCConvert.ToString(0);
						}
						// Overall Totals
						dblOverallTotalDue += FCUtils.Round(dblSTotalDue, 2);
						dblWUnitCount = 0;
						dblSUnitCount = 0;
						dblWConsum = 0;
						dblWUnits = 0;
						dblWFlat = 0;
						dblWAdjust = 0;
						dblSConsum = 0;
						dblSUnits = 0;
						dblSFlat = 0;
						dblSAdjust = 0;
						lngCons[1] = 0;
						lngPrev[1] = 0;
						lngCurr[1] = 0;
						lngCons[2] = 0;
						lngPrev[2] = 0;
						lngCurr[2] = 0;
						lngCons[3] = 0;
						lngPrev[3] = 0;
						lngCurr[3] = 0;
						rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + rsData.Get_Fields_Int32("MeterKey"), modExtraModules.strUTDatabase);
						if (!rsMeter.EndOfFile())
						{
							// rsMeter.FindFirstRecord , , "MeterKey = " & rsData.Fields("MeterKey")
							// If Not rsMeter.NoMatch Then
							for (lngCT = 1; lngCT <= 5; lngCT++)
							{
								// TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
								if (FCConvert.ToBoolean(rsMeter.Get_Fields("UseRate" + FCConvert.ToString(lngCT))))
								{
									// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
									if (FCConvert.ToInt32(rsMeter.Get_Fields("WaterType" + FCConvert.ToString(lngCT))) == 3)
									{
										// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
										dblWUnitCount = rsMeter.Get_Fields("WaterAmount" + FCConvert.ToString(lngCT));
									}
									// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
									if (FCConvert.ToInt32(rsMeter.Get_Fields("SewerType" + FCConvert.ToString(lngCT))) == 3)
									{
										// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
										dblSUnitCount = rsMeter.Get_Fields("SewerAmount" + FCConvert.ToString(lngCT));
									}
								}
							}
						}
						rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND (MeterNumber = 1 OR Combine <> 'N')", modExtraModules.strUTDatabase);
						while (!rsMeter.EndOfFile())
						{
							//Application.DoEvents();
							if (rsMeter.Get_Fields_Int32("MeterNumber") == 1)// kgk 09052012 trout-865  Rewrite this section to handle meter roll-over
							{
								if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading")) == -1)
								{
									// kk 110812 trout-884 Change from 0 to -1 for No Read
									lngCons[1] = 0;
									lngCurr[1] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									lngPrev[1] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
								}
								else
								{
									if (rsMeter.Get_Fields_Int32("CurrentReading") >= rsMeter.Get_Fields_Int32("PreviousReading"))
									{
										lngCons[1] = FCConvert.ToInt32((rsMeter.Get_Fields_Int32("CurrentReading") - rsMeter.Get_Fields_Int32("PreviousReading")) * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									}
									else
									{
										// Need to handle meter roll-over
										lngCons[1] = FCConvert.ToInt32(Math.Pow(10, (rsMeter.Get_Fields_Int32("Digits"))) - rsMeter.Get_Fields_Int32("PreviousReading"));
										// Consumption up to all 9s, right before meter rolled over
										lngCons[1] = FCConvert.ToInt32((lngCons[1] + rsMeter.Get_Fields_Int32("CurrentReading")) * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
										// Consumption since meter rolled over to zero
									}
									lngCurr[1] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									lngPrev[1] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
								}
								// kgk trout-764 & 765  10-18-2011  Add Estimated and Change Out indicators to outprintfile
								strEstim[1] = FCConvert.ToString(rsMeter.Get_Fields_String("CurrentCode"));
								// Code is A or E
								if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("PrevChangeOut")))
								{
									strChgOut[1] = "Y";
									// TODO Get_Fields: Field [PrevChangeOutConsumption] not found!! (maybe it is an alias?)
									lngChgOutCons[1] = FCConvert.ToInt32(rsMeter.Get_Fields("PrevChangeOutConsumption"));
									// kjr trout-1118 11/7/2016
								}
								else
								{
									strChgOut[1] = "N";
								}
							}
							else if (rsMeter.Get_Fields_Int32("MeterNumber") == 2)
							{
								if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading")) == -1)
								{
									// kk 110812 trout-884 Change from 0 to -1 for No Read
									lngCons[2] = 0;
									lngCurr[2] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									lngPrev[2] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
								}
								else
								{
									if (rsMeter.Get_Fields_Int32("CurrentReading") >= rsMeter.Get_Fields_Int32("PreviousReading"))
									{
										lngCons[2] = FCConvert.ToInt32((rsMeter.Get_Fields_Int32("CurrentReading") - rsMeter.Get_Fields_Int32("PreviousReading")) * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									}
									else
									{
										lngCons[2] = FCConvert.ToInt32(Math.Pow(10, (rsMeter.Get_Fields_Int32("Digits"))) - rsMeter.Get_Fields_Int32("PreviousReading"));
										// Consumption up to all 9s, right before meter rolled over
										lngCons[2] = FCConvert.ToInt32((lngCons[2] + rsMeter.Get_Fields_Int32("CurrentReading")) * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
										// Consumption since meter rolled over to zero
									}
									lngCurr[2] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									lngPrev[2] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("NegativeConsumption")))
									{
										lngCons[2] *= -1;
									}
								}
								strEstim[2] = FCConvert.ToString(rsMeter.Get_Fields_String("CurrentCode"));
								// Code is A or E
								if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("PrevChangeOut")))
								{
									strChgOut[2] = "Y";
									// TODO Get_Fields: Field [PrevChangeOutConsumption] not found!! (maybe it is an alias?)
									lngChgOutCons[2] = FCConvert.ToInt32(rsMeter.Get_Fields("PrevChangeOutConsumption"));
									// kjr trout-1118 11/7/2016
								}
								else
								{
									strChgOut[2] = "N";
								}
							}
							else if (rsMeter.Get_Fields_Int32("MeterNumber") == 3)
							{
								if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading")) == -1)
								{
									// kk 110812 trout-884 Change from 0 to -1 for No Read
									lngCons[3] = 0;
									lngCurr[3] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									lngPrev[3] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
								}
								else
								{
									if (rsMeter.Get_Fields_Int32("CurrentReading") >= rsMeter.Get_Fields_Int32("PreviousReading"))
									{
										lngCons[3] = FCConvert.ToInt32((rsMeter.Get_Fields_Int32("CurrentReading") - rsMeter.Get_Fields_Int32("PreviousReading")) * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									}
									else
									{
										lngCons[3] = FCConvert.ToInt32(Math.Pow(10, (rsMeter.Get_Fields_Int32("Digits"))) - rsMeter.Get_Fields_Int32("PreviousReading"));
										// Consumption up to all 9s, right before meter rolled over
										lngCons[3] = FCConvert.ToInt32((lngCons[3] + rsMeter.Get_Fields_Int32("CurrentReading")) * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
										// Consumption since meter rolled over to zero
									}
									lngCurr[3] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									lngPrev[3] = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("PreviousReading") * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("NegativeConsumption")))
									{
										lngCons[3] *= -1;
									}
								}
								strEstim[3] = FCConvert.ToString(rsMeter.Get_Fields_String("CurrentCode"));
								// Code is A or E
								if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("PrevChangeOut")))
								{
									strChgOut[3] = "Y";
									// TODO Get_Fields: Field [PrevChangeOutConsumption] not found!! (maybe it is an alias?)
									lngChgOutCons[3] = FCConvert.ToInt32(rsMeter.Get_Fields("PrevChangeOutConsumption"));
									// kjr trout-1118 11/7/2016
								}
								else
								{
									strChgOut[3] = "N";
								}
							}
							else
							{
								if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading")) != -1)
								{
									// kk 110812 trout-884 Change from 0 to -1 for No Read
									if (FCConvert.ToInt32(rsMeter.Get_Fields_Boolean("NegativeConsumption")) == 0)
									{
										lngCons[3] += FCConvert.ToInt32(((rsMeter.Get_Fields_Int32("CurrentReading") - rsMeter.Get_Fields_Int32("PreviousReading")) * -1) * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									}
									else
									{
										lngCons[3] = FCConvert.ToInt32((lngCons[3] + rsMeter.Get_Fields_Int32("CurrentReading") - rsMeter.Get_Fields_Int32("PreviousReading")) * FCConvert.ToDouble(modExtraModules.Statics.glngTownReadingUnits) / lngUnitsToShow);
									}
									lngCurr[3] = 0;
									// leave these as 0 if there are more than 3 meters
									lngPrev[3] = 0;
								}
								strEstim[3] = FCConvert.ToString(rsMeter.Get_Fields_String("CurrentCode"));
								// Code is A or E
								if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("PrevChangeOut")))
								{
									strChgOut[3] = "Y";
									// TODO Get_Fields: Field [PrevChangeOutConsumption] not found!! (maybe it is an alias?)
									lngChgOutCons[3] = FCConvert.ToInt32(rsMeter.Get_Fields("PrevChangeOutConsumption"));
									// kjr trout-1118 11/7/2016
								}
								else
								{
									strChgOut[3] = "N";
								}
								break;
							}
							rsMeter.MoveNext();
						}
						//end switch
					}
					// MAL@20071106
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerOverrideCons")) != 0)
					{
						lngSCons = FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerOverrideCons"));
					}
					else
					{
						lngSCons = (lngCons[1] + lngCons[2] + lngCons[3]);
					}
					if (FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterOverrideCons")) != 0)
					{
						lngWCons = FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterOverrideCons"));
					}
					else
					{
						lngWCons = (lngCons[1] + lngCons[2] + lngCons[3]);
					}
					if (!boolSplitBill || ((modUTStatusPayments.Statics.gboolPayWaterFirst && !boolSecondPass) || (!modUTStatusPayments.Statics.gboolPayWaterFirst && boolSecondPass)))
					{
						dblWConsum = FCConvert.ToDouble(rsData.Get_Fields_Decimal("WConsumptionAmount"));
						dblWUnits = FCConvert.ToDouble(rsData.Get_Fields_Decimal("WUnitsAmount"));
						dblWFlat = FCConvert.ToDouble(rsData.Get_Fields_Decimal("WFlatAmount"));
						dblWAdjust = FCConvert.ToDouble(rsData.Get_Fields_Decimal("WAdjustAmount") + rsData.Get_Fields_Decimal("WDEAdjustAmount") + rsData.Get_Fields_Decimal("WMiscAmount"));
					}
					if (!boolSplitBill || ((modUTStatusPayments.Statics.gboolPayWaterFirst && boolSecondPass) || (!modUTStatusPayments.Statics.gboolPayWaterFirst && !boolSecondPass)))
					{
						dblSConsum = FCConvert.ToDouble(rsData.Get_Fields_Decimal("SConsumptionAmount"));
						dblSUnits = FCConvert.ToDouble(rsData.Get_Fields_Decimal("SUnitsAmount"));
						dblSFlat = FCConvert.ToDouble(rsData.Get_Fields_Decimal("SFlatAmount"));
						dblSAdjust = FCConvert.ToDouble(rsData.Get_Fields_Decimal("SAdjustAmount") + rsData.Get_Fields_Decimal("SDEAdjustAmount") + rsData.Get_Fields_Decimal("SMiscAmount"));
					}
					// Itemize Adjustments
					// Loop through adjustments and write a descr/amt for each
					// kgk 10-19-2011 trout-766  Add adj detail for Chapt 660 compliance
					// Itemize Water Adjustments
					for (lngCT = 1; lngCT <= Information.UBound(aryWAdjItmz, 1); lngCT++)
					{
						//Application.DoEvents();
						aryWAdjItmz[lngCT, 1] = "";
						aryWAdjItmz[lngCT, 2] = "0";
					}
					if (dblWAdjust != 0)
					{
						lngCT = 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsTemp.OpenRecordset("SELECT Breakdown.Amount as AdjAmount, Adjust.* FROM Breakdown INNER JOIN Adjust ON Adjust.Code = Breakdown.AdjCode WHERE Breakdown.BillKey = " + rsData.Get_Fields("Bill") + " AND Breakdown.Service = 'W'", modExtraModules.strUTDatabase);
						if (rsTemp.RecordCount() > 0)
						{
							for (lngCT = 1; lngCT <= rsTemp.RecordCount(); lngCT++)
							{
								//Application.DoEvents();
								if (lngCT <= Information.UBound(aryWAdjItmz, 1))
								{
									// What do we do if there are more then 6 adjustments?
									aryWAdjItmz[lngCT, 1] = FCConvert.ToString(rsTemp.Get_Fields_String("LongDescription"));
									// TODO Get_Fields: Field [AdjAmount] not found!! (maybe it is an alias?)
									aryWAdjItmz[lngCT, 2] = /*Round*/(rsTemp.Get_Fields("AdjAmount") * 100).ToString();
								}
								rsTemp.MoveNext();
							}
						}
						if (lngCT <= Information.UBound(aryWAdjItmz, 1) && (rsData.Get_Fields_Decimal("WDEAdjustAmount") > 0 || rsData.Get_Fields_Decimal("WMiscAmount") > 0))
						{
							aryWAdjItmz[lngCT, 1] = "Misc Adj";
							aryWAdjItmz[lngCT, 2] = FCUtils.Round(FCConvert.ToDouble(rsData.Get_Fields_Decimal("WDEAdjustAmount") + rsData.Get_Fields_Decimal("WMiscAmount")) * 100, 0).ToString();
						}
					}
					// Itemize Sewer Adjustments
					for (lngCT = 1; lngCT <= Information.UBound(arySAdjItmz, 1); lngCT++)
					{
						//Application.DoEvents();
						arySAdjItmz[lngCT, 1] = "";
						arySAdjItmz[lngCT, 2] = "0";
					}
					if (dblSAdjust != 0)
					{
						lngCT = 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsTemp.OpenRecordset("SELECT Breakdown.Amount as AdjAmount, Adjust.* FROM Breakdown INNER JOIN Adjust ON Adjust.Code = Breakdown.AdjCode WHERE Breakdown.BillKey = " + rsData.Get_Fields("Bill") + " AND Breakdown.Service = 'S'", modExtraModules.strUTDatabase);
						if (rsTemp.RecordCount() > 0)
						{
							for (lngCT = 1; lngCT <= rsTemp.RecordCount(); lngCT++)
							{
								if (lngCT <= Information.UBound(arySAdjItmz, 1))
								{
									// What do we do if there are more adjustments?
									arySAdjItmz[lngCT, 1] = FCConvert.ToString(rsTemp.Get_Fields_String("LongDescription"));
									// TODO Get_Fields: Field [AdjAmount] not found!! (maybe it is an alias?)
									arySAdjItmz[lngCT, 2] = /*Round*/(rsTemp.Get_Fields("AdjAmount") * 100).ToString();
								}
								rsTemp.MoveNext();
							}
						}
						if (lngCT <= Information.UBound(arySAdjItmz, 1) && (rsData.Get_Fields_Decimal("SDEAdjustAmount") > 0 || rsData.Get_Fields_Decimal("SMiscAmount") > 0))
						{
							arySAdjItmz[lngCT, 1] = "Misc Adj";
							arySAdjItmz[lngCT, 2] = FCUtils.Round(FCConvert.ToDouble(rsData.Get_Fields_Decimal("SDEAdjustAmount") + rsData.Get_Fields_Decimal("SMiscAmount")) * 100, 0).ToString();
						}
					}
					// kgk 09052912 trout-865  Change to match bill print   Or (dblWRegular + dblWTax + dblSRegular + dblSTax > 0) Then
					if (!blnExcludeZeroBills || dblOverallTotalDue > 0)
					{
						// Start Writing after this line
						// **********************************************************************************************
						// 1
						// kgk 06-05-2012 trout-834  Invoice Cloud account number for Bath Outprint File
						// kgk 09-25-2012            Invoice Cloud added the ability to sign up using the TRIO account number
						// If UCase(MuniName) = "BATH WATER DISTRICT" Then
						// If boolShowOwnerInfo Then
						// Write #2, "UT-" & CStr(rsData.Get_Fields("ActualAccountNumber"));
						// Else
						// Write #2, "UT-" & CStr(rsData.Fields("ActualAccountNumber")) & "A";
						// End If
						// Else
						FCFileSystem.Write(2, rsData.Get_Fields_Int32("ActualAccountNumber").ToString());
						// End If
						// 2-10
						if (boolShowOwnerInfo)
						{
							// if this is the first time through this account and the owner is same as bill to
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OName"), 34, false));
							// 9 i
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OName2"), 34, false));
							// 10 j
							// Write #2, PadStringWithSpaces(rsData.Get_Fields("OName"), 34, False);                   '11 k
							// Write #2, PadStringWithSpaces(rsData.Get_Fields("OName2"), 34, False);                  '12 l
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OAddress1"), 34, false));
							// 13 m
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OAddress2"), 34, false));
							// 14 n
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OAddress3"), 34, false));
							// 15 o
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OCity"), 24, false));
							// 16 p
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OState"), 2, false));
							// 17 q
							// kk10282014 trout-974  Change handling of Zip and Zip+4 to be like RE - don't try to pad with zeros to 5 digits.
							// If Val(rsData.Fields("OZip")) = 0 Then
							// If Len(rsData.Fields("OZip")) >= 5 Then
							// Write #2, Left(rsData.Get_Fields("OZip"), 5);                                   '18 r
							// Write #2, rsData.Get_Fields("OZip");                                             '18 r      'kk05082014 trouts-88  Not printing Canadian zips
							// Else
							// Write #2, rsData.Get_Fields("OZip") & String(5 - Len(rsData.Get_Fields("OZip")), " "); '18r
							// End If
							// Else
							// If Len(rsData.Fields("OZip")) >= 5 Then
							// Write #2, Left(rsData.Get_Fields("OZip"), 5);                                   '18
							// Else
							// Write #2, Format(rsData.Get_Fields("OZip"), "00000");                           '18
							// End If
							// End If
							FCFileSystem.Write(2, rsData.Get_Fields_String("OZip"));
							// 18 r      'kk05082014 trouts-88  Not printing Canadian zips
							// kk10282014 trout-974  Change handling of Zip and Zip+4 to be like RE - don't try to pad with zeros
							if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OZip4"))) != "")
							{
								// If Val(rsData.Fields("OZip4")) <> 0 Then
								FCFileSystem.Write(2, "-" + rsData.Get_Fields_String("OZip4"));
								// 19 s    '    Write #2, "-" & Format(Val(Left(rsData.Get_Fields_String("OZip4"), 4)), "0000");
							}
							else
							{
								FCFileSystem.Write(2, Strings.StrDup(5, " "));
							}
						}
						else
						{
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("BName"), 34, false));
							// 9 i
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("BName2"), 34, false));
							// 10 j
							// Write #2, PadStringWithSpaces(rsData.Get_Fields("OName"), 34, False);                   '11 k
							// Write #2, PadStringWithSpaces(rsData.Get_Fields("OName2"), 34, False);                  '12 l
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("BAddress1"), 34, false));
							// 13 m
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("BAddress2"), 34, false));
							// 14 n
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("BAddress3"), 34, false));
							// 15 o
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("BCity"), 24, false));
							// 16 p
							FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("BState"), 2, false));
							// 17 q
							// kk10282014 trout-974  Change handling of Zip and Zip+4 to be like RE - don't try to pad with zeros to 5 digits.
							// If Val(rsData.Fields("BZip")) = 0 Then
							// If Len(rsData.Fields("BZip")) >= 5 Then
							// Write #2, Left(rsData.Get_Fields("BZip"), 5);                                   '18 r
							// Write #2, rsData.Get_Fields("BZip");                                             '18 r      'kk05082014 trouts-88  Not printing Canadian zips
							// Else
							// Write #2, rsData.Get_Fields("BZip") & String(5 - Len(rsData.Get_Fields("BZip")), " "); '18
							// End If
							// Else
							// If Len(rsData.Fields("BZip")) >= 5 Then
							// Write #2, Left(rsData.Get_Fields("BZip"), 5);                                   '18
							// Else
							// Write #2, Format(rsData.Get_Fields("BZip"), "00000");                           '18
							// End If
							// End If
							FCFileSystem.Write(2, rsData.Get_Fields_String("BZip"));
							// 18 r      'kk05082014 trouts-88  Not printing Canadian zips
							// kk10282014 trout-974  Change handling of Zip and Zip+4 to be like RE - don't try to pad with zeros
							if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BZip4"))) != "")
							{
								// If Val(rsData.Fields("BZip4")) <> 0 Then
								FCFileSystem.Write(2, "-" + rsData.Get_Fields_String("BZip4"));
								// 19 s    '    Write #2, "-" & Format(Val(Left(rsData.Get_Fields_String("BZip4"), 4)), "0000");
							}
							else
							{
								FCFileSystem.Write(2, Strings.StrDup(5, " "));
							}
						}
						// 11
						FCFileSystem.Write(2, strCatString);
						// 22 v
						// 12
						FCFileSystem.Write(2, rsData.Get_Fields_String("MapLot"));
						// 13
						FCFileSystem.Write(2, strLocation);
						// 8"h
						// 14     'Bill Message
						FCFileSystem.Write(2, strBillMessage);
						// 15
						FCFileSystem.Write(2, Strings.Format(dtStatementDate, "MMddyyyy"));
						// 16
						FCFileSystem.Write(2, strBillPeriod);
						// 17
						if (rsData.Get_Fields_DateTime("CurDate").ToOADate() != 0)
						{
							if (rsData.Get_Fields_DateTime("CurDate").ToOADate() == DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
							{
								// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
								FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields("End"), "MMddyyyy"));
							}
							else
							{
								FCFileSystem.Write(2, Strings.Format(rsData.Get_Fields_DateTime("CurDate"), "MMddyyyy"));
								// 7"g
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
							FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields("End"), "MMddyyyy"));
						}
						// 18, 19, 20
						if (!rsRK.EndOfFile())
						{
							FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields_DateTime("Start"), "MMddyyyy"));
							// 25 y
							// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
							FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields("End"), "MMddyyyy"));
							// 26 z
							FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields_DateTime("IntStart"), "MMddyyyy"));
							// 27 aa
						}
						else
						{
							// empty string
							FCFileSystem.Write(2, Strings.StrDup(8, " "));
							// 25, 26, 27
							FCFileSystem.Write(2, Strings.StrDup(8, " "));
							// 25, 26, 27
							FCFileSystem.Write(2, Strings.StrDup(8, " "));
							// 25, 26, 27
						}
						// 21
						// Overall Current Due
						FCFileSystem.Write(2, FCUtils.Round(dblOverallTotalCurrent * 100, 0).ToString());
						// 42
						// 22
						// Overall Past Due
						FCFileSystem.Write(2, FCUtils.Round(dblOverallTotalPastDue * 100, 0).ToString());
						// 43
						// 23
						// Overall Total Due
						FCFileSystem.Write(2, FCUtils.Round(dblOverallTotalDue * 100, 0).ToString());
						// 44
						// 24
						// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
						FCFileSystem.Write(2, rsData.Get_Fields("Book").ToString());
						// 20 t
						// 25
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						FCFileSystem.Write(2, Strings.Left(FCConvert.ToString(rsData.Get_Fields("Sequence")), 5));
						// 21 u
						// WATER
						// 26
						if (modUTStatusPayments.Statics.TownService != "S")
						{
							if (FCConvert.ToString(rsData.Get_Fields_String("Service")) != "S")
							{
								FCFileSystem.Write(2, lngWCons.ToString());
							}
							else
							{
								FCFileSystem.Write(2, "0");
							}
						}
						else
						{
							FCFileSystem.Write(2, "0");
						}
						// 27
						// Water Unit Amount
						FCFileSystem.Write(2, FCUtils.Round(dblWUnitCount, 2).ToString());
						// 28
						// Water Consumption Total
						FCFileSystem.Write(2, FCUtils.Round(dblWConsum * 100, 0).ToString());
						// 29
						// Water Units Total
						FCFileSystem.Write(2, FCUtils.Round(dblWUnits * 100, 0).ToString());
						// 30
						// Water Flat Total
						FCFileSystem.Write(2, FCUtils.Round(dblWFlat * 100, 0).ToString());
						// 31
						// Water Adjustment Total
						FCFileSystem.Write(2, FCUtils.Round(dblWAdjust * 100, 0).ToString());
						// 32
						// Water Cons + Flat + Units
						FCFileSystem.Write(2, FCUtils.Round((dblWConsum + dblWUnits + dblWFlat) * 100, 0).ToString());
						// 33
						// Regular                'charged amount on bill
						FCFileSystem.Write(2, FCUtils.Round(dblWRegular * 100, 0).ToString());
						// 28
						// 34
						// Tax                    'charged tax amount on bill
						FCFileSystem.Write(2, FCUtils.Round(dblWTax * 100, 0).ToString());
						// 35, 36
						// Past Due and Credit Field  'total due from previous non lien bills
						if (dblWPastDue >= 0)
						{
							FCFileSystem.Write(2, FCUtils.Round(dblWPastDue * 100, 0).ToString());
							FCFileSystem.Write(2, FCUtils.Round(dblWPrinPaid * 100, 0).ToString());
						}
						else
						{
							FCFileSystem.Write(2, /*Round*/(0).ToString());
							FCFileSystem.Write(2, FCUtils.Round((dblWPastDue * -100) + (dblWPrinPaid * 100), 0).ToString());
						}
						// 37
						// Total Amount Due       'current due + total due from previous bills
						FCFileSystem.Write(2, FCUtils.Round(dblWTotalDue * 100, 0).ToString());
						// 31
						// 38
						// Interest               'current interest on ?
						// DJW@01112013 TROUT-900 Added in charged interest to the outprinting file
						// kk 04162014  trout-999 The Water Interest field is current interest only; charged int is included in the past due amount
						// Write #2, CStr(Round((dblWPastDueCurrentInterest + dblWPastDueInterest) * 100, 0));
						FCFileSystem.Write(2, FCUtils.Round((dblWPastDueCurrentInterest) * 100, 0).ToString());
						// 39
						// Lien Amount            'total lien amount for this account
						FCFileSystem.Write(2, FCUtils.Round((dblWLienAmount - dblWPastDueCurrentLInterest) * 100, 0).ToString());
						// 32
						// 40
						// Current Due            'amount due from the current bill
						FCFileSystem.Write(2, FCUtils.Round(dblWCurrentDue * 100, 0).ToString());
						// 41
						// Total Past Due         'total past due       'kk10292014 trouts-172  Subtract out Lien Current Interest so it's not doubled
						FCFileSystem.Write(2, FCUtils.Round((dblWPastDue + dblWLienAmount + dblWPastDueCurrentInterest - dblWPastDueCurrentLInterest) * 100, 0).ToString());
						// 34
						// SEWER
						// 42
						if (modUTStatusPayments.Statics.TownService != "W")
						{
							if (FCConvert.ToString(rsData.Get_Fields_String("Service")) != "W")
							{
								FCFileSystem.Write(2, lngSCons.ToString());
							}
							else
							{
								FCFileSystem.Write(2, "0");
							}
						}
						else
						{
							FCFileSystem.Write(2, "0");
						}
						// 43
						// Sewer Unit Amount
						FCFileSystem.Write(2, FCUtils.Round(dblSUnitCount, 2).ToString());
						// 44
						// Sewer Consumption Total
						FCFileSystem.Write(2, FCUtils.Round(dblSConsum * 100, 0).ToString());
						// 45
						// Sewer Units Total
						FCFileSystem.Write(2, FCUtils.Round(dblSUnits * 100, 0).ToString());
						// 46
						// Sewer Flat Total
						FCFileSystem.Write(2, FCUtils.Round(dblSFlat * 100, 0).ToString());
						// 47
						// Sewer Adjust Total
						FCFileSystem.Write(2, FCUtils.Round(dblSAdjust * 100, 0).ToString());
						// 48
						// Sewer Cons + Flat + Units
						FCFileSystem.Write(2, FCUtils.Round((dblSConsum + dblSUnits + dblSFlat) * 100, 0).ToString());
						// 49
						// Regular
						FCFileSystem.Write(2, FCUtils.Round(dblSRegular * 100, 0).ToString());
						// 50
						// Tax
						FCFileSystem.Write(2, FCUtils.Round(dblSTax * 100, 0).ToString());
						// 51, 52
						// Past Due
						if (dblSPastDue >= 0)
						{
							FCFileSystem.Write(2, FCUtils.Round(dblSPastDue * 100, 0).ToString());
							FCFileSystem.Write(2, FCUtils.Round(dblSPrinPaid * 100, 0).ToString());
						}
						else
						{
							FCFileSystem.Write(2, /*Round*/(0).ToString());
							FCFileSystem.Write(2, FCUtils.Round((dblSPastDue * -100) + (dblSPrinPaid * 100), 0).ToString());
						}
						// 53
						// Total Amount Due
						FCFileSystem.Write(2, FCUtils.Round(dblSTotalDue * 100, 0).ToString());
						// 54
						// Interest
						// DJW@01112013 TROUT-900 Added in charged interest to the outprinting file
						// kk 04162014  trout-999 The Sewer Interest field is current interest only; charged int is included in the past due amount
						// Also the dblSPastDueInterest includes PLI
						// Write #2, CStr(Round((dblSPastDueCurrentInterest + dblSPastDueInterest) * 100, 0));
						FCFileSystem.Write(2, FCUtils.Round((dblSPastDueCurrentInterest) * 100, 0).ToString());
						// 55
						// Lien Amount
						FCFileSystem.Write(2, FCUtils.Round((dblSLienAmount - dblSPastDueCurrentLInterest) * 100, 0).ToString());
						// 56
						// Current Due
						FCFileSystem.Write(2, FCUtils.Round(dblSCurrentDue * 100, 0).ToString());
						// 57
						// Total Past Due
						FCFileSystem.Write(2, FCUtils.Round((dblSPastDue + dblSLienAmount + dblSPastDueCurrentInterest - dblSPastDueCurrentLInterest) * 100, 0).ToString());
						// 41
						// 58, 59, 60
						// print meter prev,curr,cons
						FCFileSystem.Write(2, lngCons[1].ToString());
						FCFileSystem.Write(2, lngPrev[1].ToString());
						FCFileSystem.Write(2, lngCurr[1].ToString());
						// 61, 62, 63
						FCFileSystem.Write(2, lngCons[2].ToString());
						FCFileSystem.Write(2, lngPrev[2].ToString());
						FCFileSystem.Write(2, lngCurr[2].ToString());
						// 64, 65, 66
						FCFileSystem.Write(2, lngCons[3].ToString());
						FCFileSystem.Write(2, lngPrev[3].ToString());
						FCFileSystem.Write(2, lngCurr[3].ToString());
						// 67, 68, 69
						FCFileSystem.Write(2, dblWSplit1);
						FCFileSystem.Write(2, dblWSplit2);
						FCFileSystem.Write(2, dblSSplit1);
						FCFileSystem.Write(2, dblSSplit2);
						if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "LISBON")
						{
							FCFileSystem.Write(2, rsMaster.Get_Fields_String("Comment"));
						}
						// kgk trout-684 04-21-2011  Add original consumption amount to outprint
						if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
						{
							dblSActualCons = FCUtils.Round(CalculateConsumptionBased_6560(FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields_Int32("Consumption"))), rsData.Get_Fields_Int32("SRT1"), false, 0, 0, 0, "", ""), 2);
							FCFileSystem.Write(2, dblSActualCons.ToString());
						}
						if (!boolChpt660)
						{
							if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
							{
								// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
								FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields("DueDate"), "MMddyyyy"));
							}
							else
							{
								// Write #2, Format(rsRK.Fields("DueDate"), "MMddyyyy")
								if (!boolInclChangeOutCons)
								{
									// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
									FCFileSystem.WriteLine(2, Strings.Format(rsRK.Get_Fields("DueDate"), "MMddyyyy"));
								}
								else
								{
									// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
									FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields("DueDate"), "MMddyyyy"));
									// kjr 11/7/16 trout-1118
									FCFileSystem.Write(2, lngChgOutCons[1].ToString());
									FCFileSystem.Write(2, lngChgOutCons[2].ToString());
									FCFileSystem.WriteLine(2, lngChgOutCons[3].ToString());
								}
							}
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
							FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields("DueDate"), "MMddyyyy"));
							// kgk trout-764 & 765  10-18-2011  Add Estimated indicator and Meter change out indicator
							FCFileSystem.Write(2, strEstim[1]);
							FCFileSystem.Write(2, strChgOut[1]);
							FCFileSystem.Write(2, strEstim[2]);
							FCFileSystem.Write(2, strChgOut[2]);
							FCFileSystem.Write(2, strEstim[3]);
							FCFileSystem.Write(2, strChgOut[3]);
							for (lngCT = 1; lngCT <= Information.UBound(aryWAdjItmz, 1); lngCT++)
							{
								FCFileSystem.Write(2, aryWAdjItmz[lngCT, 1]);
								FCFileSystem.Write(2, aryWAdjItmz[lngCT, 2]);
							}
							for (lngCT = 1; lngCT <= Information.UBound(arySAdjItmz, 1); lngCT++)
							{
								FCFileSystem.Write(2, arySAdjItmz[lngCT, 1]);
								if (lngCT < Information.UBound(arySAdjItmz, 1) || boolInclChangeOutCons)
								{
									// kjr 11/7/16 trout-1118
									FCFileSystem.Write(2, arySAdjItmz[lngCT, 2]);
								}
								else
								{
									if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
									{
										FCFileSystem.Write(2, arySAdjItmz[lngCT, 2]);
									}
									else
									{
										FCFileSystem.WriteLine(2, arySAdjItmz[lngCT, 2]);
									}
								}
							}
							// kjr 11/7/16 trout-1118
							if (Strings.UCase(modGlobalConstants.Statics.MuniName) != "GARDINER" && Strings.UCase(modGlobalConstants.Statics.MuniName) != "BANGOR")
							{
								if (boolInclChangeOutCons)
								{
									FCFileSystem.Write(2, lngChgOutCons[1].ToString());
									FCFileSystem.Write(2, lngChgOutCons[2].ToString());
									FCFileSystem.WriteLine(2, lngChgOutCons[3].ToString());
								}
							}
						}
						if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
						{
							// kjr 11/7/16 trout-1118
							if (boolInclChangeOutCons)
							{
								FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OName"), 34, false));
								FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OName2"), 34, false));
								FCFileSystem.Write(2, lngChgOutCons[1].ToString());
								FCFileSystem.Write(2, lngChgOutCons[2].ToString());
								FCFileSystem.WriteLine(2, lngChgOutCons[3].ToString());
							}
							else
							{
								FCFileSystem.Write(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OName"), 34, false));
								FCFileSystem.WriteLine(2, modGlobalFunctions.PadStringWithSpaces(rsData.Get_Fields_String("OName2"), 34, false));
							}
						}
						else if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
						{
							if (rsMaster.Get_Fields_Decimal("ImpervSurfArea") >= 0)
							{
								// kk07082015 trouts-149  Indicate account is not billed for SW by setting the Value to -1
								FCFileSystem.Write(2, rsMaster.Get_Fields_Decimal("ImpervSurfArea").ToString());
							}
							else
							{
								FCFileSystem.Write(2, "0");
							}
							// kjr 11/7/16 trout-1118
							if (!boolInclChangeOutCons)
							{
								// kk07272016 trouts-169  Add previous reading date to outprint file
								if (rsData.Get_Fields_DateTime("PrevDate").ToOADate() != 0)
								{
									if (rsData.Get_Fields_DateTime("PrevDate").ToOADate() == DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
									{
										FCFileSystem.WriteLine(2, Strings.Format(rsRK.Get_Fields_DateTime("Start"), "MMddyyyy"));
									}
									else
									{
										FCFileSystem.WriteLine(2, Strings.Format(rsData.Get_Fields_DateTime("PrevDate"), "MMddyyyy"));
									}
								}
								else
								{
									FCFileSystem.WriteLine(2, Strings.Format(rsRK.Get_Fields_DateTime("Start"), "MMddyyyy"));
								}
							}
							else
							{
								// kk07272016 trouts-169  Add previous reading date to outprint file
								if (rsData.Get_Fields_DateTime("PrevDate").ToOADate() != 0)
								{
									if (rsData.Get_Fields_DateTime("PrevDate").ToOADate() == DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
									{
										FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields_DateTime("Start"), "MMddyyyy"));
									}
									else
									{
										FCFileSystem.Write(2, Strings.Format(rsData.Get_Fields_DateTime("PrevDate"), "MMddyyyy"));
									}
								}
								else
								{
									FCFileSystem.Write(2, Strings.Format(rsRK.Get_Fields_DateTime("Start"), "MMddyyyy"));
								}
								FCFileSystem.Write(2, lngChgOutCons[1].ToString());
								FCFileSystem.Write(2, lngChgOutCons[2].ToString());
								FCFileSystem.WriteLine(2, lngChgOutCons[3].ToString());
							}
						}
						// check to see if the owner is different, and the townserveice and bill service is both
						if (!boolSameOwner && modUTStatusPayments.Statics.TownService == "B" && ((modUTStatusPayments.Statics.gboolPayWaterFirst && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "W") || (!modUTStatusPayments.Statics.gboolPayWaterFirst && FCConvert.ToString(rsData.Get_Fields_String("Service")) != "S")))
						{
							if (boolSecondPass == true)
							{
								rsData.MoveNext();
								boolSecondPass = false;
								// kk11072014 trouts-117  Some circumstances boolSecondPass is not reset
							}
							else
							{
								// 
								boolSecondPass = true;
								// 
							}
							// kk11072014 trouts-117       'boolSecondPass = Not boolSecondPass
						}
						else
						{
							boolSecondPass = false;
							rsData.MoveNext();
						}
					}
					else
					{
						boolSecondPass = false;
						// kk11072014 trouts-117
						rsData.MoveNext();
					}
					//} else {
					//	boolSecondPass = false; // kk11072014 trouts-117
					//	rsData.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				FCFileSystem.FileClose(2);
				if (MessageBox.Show("Would you like to see the validation report in full detail?", "Validation Report", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
				{
					rptOutprintingFile.InstancePtr.Init(true, modalDialog);
				}
				else
				{
					rptOutprintingFile.InstancePtr.Init(false, modalDialog);
				}
				frmReportViewer.InstancePtr.Init(rptOutprintingFormat.InstancePtr, showModal: modalDialog);
				//Application.DoEvents();
				MessageBox.Show("TWUTBillExport.CSV has been created successfully.", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TWUTBillExport.CSV"), "TWUTBillExport.CSV");
				return BuildOutPrintingFile;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				FCFileSystem.FileClose(2);
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building Outprinting File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildOutPrintingFile;
		}

		public static bool CheckBDAccountsSetup()
		{
			bool CheckBDAccountsSetup = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strSAccountNumber = "";
				string strWAccountNumber = "";
				clsDRWrapper Master = new clsDRWrapper();
				if (modGlobalConstants.Statics.gboolBD)
				{
					// MAL@20081104: Temporary message to see if we can track down a problem
					// Tracker Reference: 15874
					if (modGlobalConstants.Statics.MuniName == "Fairfield")
					{
						MessageBox.Show("CheckBDAccountsSetup has been called", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					Master.OpenRecordset("SELECT * FROM StandardAccounts", modExtraModules.strBDDatabase);
					if (!Master.EndOfFile())
					{
						Master.FindFirstRecord("Code", "SR");
						if (!Master.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strSAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
						}
						Master.FindFirstRecord("Code", "WR");
						if (!Master.NoMatch)
						{
							// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
							strWAccountNumber = FCConvert.ToString(Master.Get_Fields("Account"));
						}
					}
					if (modUTStatusPayments.Statics.TownService != "W")
					{
						if (Conversion.Val(strSAccountNumber) == 0)
						{
							return CheckBDAccountsSetup;
						}
					}
					if (modUTStatusPayments.Statics.TownService != "S")
					{
						if (Conversion.Val(strWAccountNumber) == 0)
						{
							return CheckBDAccountsSetup;
						}
					}
				}
				CheckBDAccountsSetup = true;
				return CheckBDAccountsSetup;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking BD Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckBDAccountsSetup;
		}

		public static double FindFinalPercentage(ref clsDRWrapper rsBill, ref bool boolFinal)
		{
			double FindFinalPercentage = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will returnt he percentage to bill apon
				// if it is a final billing then it will calculate from the start date to the bill date
				// if it is not then it will calculate from the bill date until the end date
				// vbPorter upgrade warning: lngDays As int --> As long
				long lngDays;
				// vbPorter upgrade warning: lngPercentageDays As int --> As long
				long lngPercentageDays;
				if (rsBill.IsFieldNull("FinalStartDate") || rsBill.IsFieldNull("FinalEndDate") || rsBill.IsFieldNull("FinalBillDate"))
				{
					FindFinalPercentage = 1;
					return FindFinalPercentage;
				}
				// kk03132014   Not sure why this was adding an extra day, it makes the total < 100%
				lngDays = DateAndTime.DateDiff("D", rsBill.Get_Fields_DateTime("FinalStartDate"), rsBill.Get_Fields_DateTime("FinalEndDate"));
				// + 1
				if (boolFinal)
				{
					// MAL@20080129: Check for bill date being after last day
					// Tracker Reference: 12152
					if (Convert.ToDateTime(rsBill.Get_Fields_DateTime("FinalBillDate")) > Convert.ToDateTime(rsBill.Get_Fields_DateTime("FinalEndDate")))
					{
						lngPercentageDays = lngDays;
					}
					else
					{
						lngPercentageDays = DateAndTime.DateDiff("D", Convert.ToDateTime(rsBill.Get_Fields_DateTime("FinalStartDate")), Convert.ToDateTime(rsBill.Get_Fields_DateTime("FinalBillDate")));
					}
				}
				else
				{
					lngPercentageDays = DateAndTime.DateDiff("D", Convert.ToDateTime(rsBill.Get_Fields_DateTime("FinalBillDate")), Convert.ToDateTime(rsBill.Get_Fields_DateTime("FinalEndDate")));
				}
				if (lngPercentageDays > 0 && lngDays != 0)
				{
					FindFinalPercentage = FCUtils.Round(FCConvert.ToDouble(lngPercentageDays) / FCConvert.ToDouble(lngDays), 4);
				}
				else
				{
					FindFinalPercentage = 1;
				}
				return FindFinalPercentage;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Percentage", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindFinalPercentage;
		}
		// kk04242014 trouts-64  Add strSQL parameter so we can reset bills by Book(s)
		public static void ResetCurrentBilling(ref string strBookSel)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This will remove all of the bills that do not have any payment records and reset the book/meter status back to Cleared
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsD = new clsDRWrapper();
				int intCT = 0;
				// MAL@20070830: Added Default to 'No' button in the message box
				if (MessageBox.Show("Are you sure that you want to reset the selected current billings?", "Reset Bills", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
				{
					rsData.DefaultDB = "TWUT0000.VB1";
					rsD.DefaultDB = "TWUT0000.VB1";
					modGlobalFunctions.AddCYAEntry_8("UT", "Reset Current Billing");
					frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Removing Bills");
					// kk04242014 trouts-64  Change to reset by Book(s)
					// This will remove all of the bills that are not billed
					// rsData.OpenRecordset "SELECT *, Bill.ID AS Bill FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE BillingRateKey = 0 AND SPrinOwed = 0 AND WPrinOwed = 0 AND WLienRecordNumber = 0 AND SLienRecordNumber = 0 AND SPrinPaid = 0 AND WPrinPaid = 0"
					// kk03302015 trouts-20  Add checks for Null values
					rsData.OpenRecordset("SELECT *, Bill.ID AS Bill FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE Book " + strBookSel + " AND ISNULL(BillingRateKey,0) = 0 " + "AND ISNULL(SPrinOwed,0) = 0 AND ISNULL(SLienRecordNumber,0) = 0 AND ISNULL(SPrinPaid,0) = 0 " + "AND ISNULL(WPrinOwed,0) = 0 AND ISNULL(WLienRecordNumber,0) = 0 AND ISNULL(WPrinPaid,0) = 0");
					while (!rsData.EndOfFile())
					{
						//Application.DoEvents();
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsD.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + rsData.Get_Fields("Bill"));
						if (rsD.EndOfFile())
						{
							intCT += 1;
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							rsD.Execute("DELETE FROM TempBreakDown WHERE Billkey = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							rsD.Execute("DELETE FROM Bill WHERE ID = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
						}
						rsData.MoveNext();
					}
					// This will remove all of the final bills that are not billed
					// kk03302015 trouts-20  Add checks for Null values
					rsData.OpenRecordset("SELECT *, Bill.ID AS Bill FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE Book " + strBookSel + " AND ISNULL(Bill.Final,0) = 1 AND BillStatus <> 'B' " + "AND ISNULL(SPrinOwed,0) = 0 AND ISNULL(SLienRecordNumber,0) = 0 AND ISNULL(SPrinPaid,0) = 0 " + "AND ISNULL(WPrinOwed,0) = 0 AND ISNULL(WLienRecordNumber,0) = 0 AND ISNULL(WPrinPaid,0) = 0");
                    while (!rsData.EndOfFile())
                    {
                        rsD.OpenRecordset("Select * from PaymentRec where billkey = " + rsData.Get_Fields_Int32("Bill"),
                            "UtilityBilling");
                        if (rsD.EndOfFile())
                        {
                        intCT += 1;
                        // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                        rsD.Execute("DELETE FROM TempBreakDown WHERE Billkey = " + rsData.Get_Fields("Bill"),
                            modExtraModules.strUTDatabase);
                        // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                        rsD.Execute("DELETE FROM Bill WHERE ID = " + rsData.Get_Fields("Bill"),
                            modExtraModules.strUTDatabase);
                        }

                        rsData.MoveNext();
					}
					// This will remove all of the bills that are not billed and also not associated with a meter
					// kk03302015 trouts-20  Add checks for Null values
					rsData.OpenRecordset("SELECT *, Bill.ID AS Bill FROM Bill WHERE Book " + strBookSel + " AND ISNULL(BillingRateKey,0) = 0 AND ISNULL(MeterKey,0) = 0 " + "AND ISNULL(SPrinOwed,0) = 0 AND ISNULL(SLienRecordNumber,0) = 0 AND ISNULL(SPrinPaid,0) = 0 " + "AND ISNULL(WPrinOwed,0) = 0 AND ISNULL(WLienRecordNumber,0) = 0 AND ISNULL(WPrinPaid,0) = 0");
					while (!rsData.EndOfFile())
					{
						//Application.DoEvents();
						intCT += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsD.Execute("DELETE FROM TempBreakDown WHERE Billkey = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsD.Execute("DELETE FROM Bill WHERE ID = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
						rsData.MoveNext();
					}
					// This will remove all of the bills that are not billed and also not associated with an account
					// kk03302015 trouts-20  Add checks for Null values
					rsData.OpenRecordset("SELECT *, Bill.ID AS Bill FROM Bill WHERE Book " + strBookSel + " AND ISNULL(AccountKey,0) = 0 AND ISNULL(BillingRateKey,0) = 0 " + "AND ISNULL(SPrinOwed,0) = 0 AND ISNULL(SLienrecordNumber,0) = 0 AND ISNULL(SPrinPaid,0) = 0 " + "AND ISNULL(WPrinOwed,0) = 0 AND ISNULL(WLienrecordNumber,0) = 0 AND ISNULL(WPrinPaid,0) = 0");
					while (!rsData.EndOfFile())
					{
						//Application.DoEvents();
						intCT += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsD.Execute("DELETE FROM TempBreakDown WHERE Billkey = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsD.Execute("DELETE FROM Bill WHERE ID = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
						rsData.MoveNext();
					}
					// This will remove all of the final bills that are not billed
					// kk03302015 trouts-20  Add checks for Null values
					rsData.OpenRecordset("SELECT *, Bill.ID AS Bill FROM Bill WHERE Book " + strBookSel + " AND ISNULL(Final,0) = 1 AND BillStatus <> 'B' " + "AND ISNULL(SPrinOwed,0) = 0 AND ISNULL(SLienrecordNumber,0) = 0 AND ISNULL(SPrinPaid,0) = 0 " + "AND ISNULL(WPrinOwed,0) = 0 AND ISNULL(WLienrecordNumber,0) = 0 AND ISNULL(WPrinPaid,0) = 0");
					if (!rsData.EndOfFile())
					{
						frmWait.InstancePtr.Unload();
						if (MessageBox.Show("Would you like to reset the Final Bills?", "Reset Final Bills", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
						{
							modGlobalFunctions.AddCYAEntry_8("UT", "Reset Final Bills");
							while (!rsData.EndOfFile())
							{
								//Application.DoEvents();
								intCT += 1;
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								rsD.Execute("DELETE FROM TempBreakDown WHERE Billkey = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								rsD.Execute("DELETE FROM Bill WHERE ID = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
								rsData.MoveNext();
							}
						}
					}
					// Reset book status
					rsData.OpenRecordset("SELECT * FROM Book WHERE BookNumber " + strBookSel + " AND CurrentStatus <> 'BE'", modExtraModules.strUTDatabase);
					while (!rsData.EndOfFile())
					{
						//Application.DoEvents();
						rsData.Edit();
						rsData.Set_Fields("CurrentStatus", "CE");
						rsData.Update();
						rsData.MoveNext();
					}
					rsData.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber " + strBookSel + " AND BillingStatus <> 'B'", modExtraModules.strUTDatabase);
					while (!rsData.EndOfFile())
					{
						//Application.DoEvents();
						rsData.Edit();
						rsData.Set_Fields("BillingStatus", "C");
						rsData.Update();
						rsData.MoveNext();
					}
					// kk04302014 trouts-64  Add option to go back to last billed state
					frmWait.InstancePtr.Hide();
					//Application.DoEvents();
					if (MessageBox.Show("Would you like to revert readings to the last billed status?", "Revert Readings", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						frmWait.InstancePtr.Show();
						//Application.DoEvents();
						RevertMeterReadings(ref strBookSel);
					}
					frmWait.InstancePtr.Unload();
				}
				frmWait.InstancePtr.Unload();
				MessageBox.Show(FCConvert.ToString(intCT) + " bills were removed.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				// MAL@20070830: Added Reminder to Re-enter Adjustments
				if (intCT > 0)
				{
					//FC:FINAL:CHN - issue #1209: Adjust format of reminder message.
					//MessageBox.Show("Any adjustments entered for the accounts " + "\r\n" + "\r\n" + "that have been reset will need to be re-entered.", "Reminder", MessageBoxButtons.OK, MessageBoxIcon.Information);
					MessageBox.Show("Any adjustments entered for the accounts " + "\r\n" + "that have been reset will need to be re-entered.", "Reminder", MessageBoxButtons.OK, MessageBoxIcon.Information, MessageBoxDefaultButton.Button1);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Resetting Current Billing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ResetFinalBilling_2(string strSQL)
		{
			ResetFinalBilling(strSQL);
		}

		public static void ResetFinalBilling(string strSQL = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsData = new clsDRWrapper();
				clsDRWrapper rsD = new clsDRWrapper();
				int intCT = 0;
				// This will remove all of the final bills that are not billed
				rsData.OpenRecordset("SELECT *, Bill.ID AS Bill FROM Bill WHERE ISNULL(Final,0) = 1 AND BillStatus <> 'B' " + "AND ISNULL(SPrinOwed,0) = 0 AND ISNULL(SLienRecordNumber,0) = 0  " + "AND ISNULL(WPrinOwed,0) = 0 AND ISNULL(WLienRecordNumber,0) = 0  " + strSQL);
				if (!rsData.EndOfFile())
				{
					if (MessageBox.Show("Are you sure that you would like to reset the Final Bills?", "Reset Final Bills", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_8("UT", "Reset Just Final Bills", strSQL);
						frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Resetting Final Bills", true, rsData.RecordCount(), true);
						while (!rsData.EndOfFile())
						{
							frmWait.InstancePtr.IncrementProgress();
							//Application.DoEvents();
							intCT += 1;
							rsD.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + rsData.Get_Fields_Int32("MeterKey"), modExtraModules.strUTDatabase);
							if (!rsD.EndOfFile())
							{
								rsD.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
								while (!rsD.EndOfFile())
								{
									//Application.DoEvents();
									rsD.Edit();
									rsD.Set_Fields("BillingStatus", "C");
									rsD.Update();
									rsD.MoveNext();
								}
							}
							rsD.Execute("UPDATE MeterTable SET BillingStatus = 'C' WHERE ID = " + rsData.Get_Fields_Int32("MeterKey"), modExtraModules.strUTDatabase);
                            rsD.OpenRecordset(
                                "select * from paymentrec where billkey = " + rsData.Get_Fields_Int32("Bill"),
                                "UtilityBilling");
                            if (rsD.EndOfFile())
                            {
                                rsD.Execute("DELETE FROM TempBreakDown WHERE Billkey = " + rsData.Get_Fields("Bill"),
                                    modExtraModules.strUTDatabase);
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                rsD.Execute("DELETE FROM Bill WHERE ID = " + rsData.Get_Fields("Bill"),
                                    modExtraModules.strUTDatabase);
                            }
                            else
                            {
                                rsD.Execute(
                                    "update bill set billstatus = 'C', billnumber = 0, billingratekey = 0,final = 0, finalenddate = NULL, finalstartdate = NULL, finalbilldate = null where id = " +
                                    rsData.Get_Fields_Int32("Bill"), "UtilityBilling");
                            }
                            rsData.MoveNext();
						}
					}
				}
				frmWait.InstancePtr.Unload();
				if (intCT == 1)
				{
					MessageBox.Show(FCConvert.ToString(intCT) + " bill was removed.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(intCT) + " bills were removed.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Resetting Final Billing", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static bool SplitBills(ref int lngRK)
		{
			bool SplitBills = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This function will split existing bills and create another bill for each account that is approx half of the
				// original bill and associated with another rate key with different due/int dates
				clsDRWrapper rsS = new clsDRWrapper();
				clsDRWrapper rsNew = new clsDRWrapper();
				int lngCT = 0;
				double dblWPOrig = 0;
				double dblWPSplit1 = 0;
				double dblWPSplit2 = 0;
				double dblWTOrig = 0;
				double dblWTSplit1 = 0;
				double dblWTSplit2 = 0;
				double dblSPOrig = 0;
				double dblSPSplit1 = 0;
				double dblSPSplit2 = 0;
				double dblSTOrig = 0;
				double dblSTSplit1 = 0;
				double dblSTSplit2 = 0;
				int lngNewRK = 0;
				int lngNewBillKey = 0;
				// kgk
				frmUTAuditInfo.InstancePtr.Init(110, DateTime.Now, DateTime.Now, DateTime.Now, ref lngNewRK);
				if (lngNewRK > 0)
				{
					rsNew.OpenRecordset("SELECT * FROM Bill WHERE ID = 0");
					rsS.OpenRecordset("SELECT * FROM Bill WHERE BillingRateKey = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
					modGlobalFunctions.AddCYAEntry_80("UT", "Split Bills", "RK : " + FCConvert.ToString(lngRK) + " and " + FCConvert.ToString(lngNewRK), FCConvert.ToString(rsS.RecordCount()) + " bills to split.");
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Splitting Bills", true, rsS.RecordCount(), true);
					while (!rsS.EndOfFile())
					{
						frmWait.InstancePtr.IncrementProgress();
						//Application.DoEvents();
						if (rsS.Get_Fields_Double("WOrigBillAmount") == rsS.Get_Fields_Double("WPrinOwed") + rsS.Get_Fields_Double("WTaxOwed") && rsS.Get_Fields_Double("SOrigBillAmount") == rsS.Get_Fields_Double("SPrinOwed") + rsS.Get_Fields_Double("STaxOwed"))
						{
							// if they are the same then this bill needs to be split
							dblWPOrig = rsS.Get_Fields_Double("WPrinOwed");
							dblWPSplit1 = FCUtils.Round(dblWPOrig / 2, 2);
							dblWPSplit2 = dblWPOrig - dblWPSplit1;
							dblWTOrig = rsS.Get_Fields_Double("WTaxOwed");
							dblWTSplit1 = FCUtils.Round(dblWTOrig / 2, 2);
							dblWTSplit2 = dblWTOrig - dblWTSplit1;
							dblSPOrig = rsS.Get_Fields_Double("SPrinOwed");
							dblSPSplit1 = FCUtils.Round(dblSPOrig / 2, 2);
							dblSPSplit2 = dblSPOrig - dblSPSplit1;
							dblSTOrig = rsS.Get_Fields_Double("STaxOwed");
							dblSTSplit1 = FCUtils.Round(dblSTOrig / 2, 2);
							dblSTSplit2 = dblSTOrig - dblSTSplit1;
							// create the new bill
							rsNew.AddNew();
							rsNew.Set_Fields("AccountKey", rsS.Get_Fields_Int32("AccountKey"));
							rsNew.Set_Fields("ActualAccountNumber", rsS.Get_Fields_Int32("ActualAccountNumber"));
							rsNew.Set_Fields("BillNumber", lngNewRK);
							rsNew.Set_Fields("BillingYear", rsS.Get_Fields_Int32("BillingYear"));
							rsNew.Set_Fields("MeterKey", rsS.Get_Fields_Int32("MeterKey"));
							// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
							rsNew.Set_Fields("Book", rsS.Get_Fields("Book"));
							rsNew.Set_Fields("Service", rsS.Get_Fields_String("Service"));
							rsNew.Set_Fields("BillStatus", rsS.Get_Fields_String("BillStatus"));
							rsNew.Set_Fields("BillingRateKey", lngNewRK);
							rsNew.Set_Fields("NoBill", rsS.Get_Fields_Boolean("NoBill"));
							rsNew.Set_Fields("CurDate", rsS.Get_Fields_DateTime("CurDate"));
							rsNew.Set_Fields("CurReading", rsS.Get_Fields_Int32("CurReading"));
							rsNew.Set_Fields("CurCode", rsS.Get_Fields_String("CurCode"));
							rsNew.Set_Fields("PrevDate", rsS.Get_Fields_DateTime("PrevDate"));
							rsNew.Set_Fields("PrevReading", rsS.Get_Fields_Int32("PrevReading"));
							rsNew.Set_Fields("PrevCode", rsS.Get_Fields_String("PrevCode"));
							rsNew.Set_Fields("BillingCode", rsS.Get_Fields_String("BillingCode"));
							rsNew.Set_Fields("Consumption", rsS.Get_Fields_Int32("Consumption"));
							rsNew.Set_Fields("WaterOverrideCons", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Int32("WaterOverrideCons")) / 2, 2));
							rsNew.Set_Fields("WaterOverrideAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("WaterOverrideAmount") / 2), 2));
							rsNew.Set_Fields("SewerOverrideCons", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Int32("SewerOverrideCons")) / 2, 2));
							rsNew.Set_Fields("SewerOverrideAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("SewerOverrideAmount") / 2), 2));
							rsNew.Set_Fields("WMiscAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("WMiscAmount") / 2), 2));
							rsNew.Set_Fields("WAdjustAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("WAdjustAmount") / 2), 2));
							rsNew.Set_Fields("WDEAdjustAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("WDEAdjustAmount") / 2), 2));
							rsNew.Set_Fields("WFlatAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("WFlatAmount") / 2), 2));
							rsNew.Set_Fields("WUnitsAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("WUnitsAmount") / 2), 2));
							rsNew.Set_Fields("WConsumptionAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("WConsumptionAmount") / 2), 2));
							rsNew.Set_Fields("SMiscAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("SMiscAmount") / 2), 2));
							rsNew.Set_Fields("SAdjustAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("SAdjustAmount") / 2), 2));
							rsNew.Set_Fields("SDEAdjustAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("SDEAdjustAmount") / 2), 2));
							rsNew.Set_Fields("SFlatAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("SFlatAmount") / 2), 2));
							rsNew.Set_Fields("SUnitsAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("SUnitsAmount") / 2), 2));
							rsNew.Set_Fields("SConsumptionAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("SConsumptionAmount") / 2), 2));
							// TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
							rsNew.Set_Fields("WTax", FCUtils.Round(rsS.Get_Fields("WTax") / 2, 2));
							// TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
							rsNew.Set_Fields("STax", FCUtils.Round(rsS.Get_Fields("STax") / 2, 2));
							rsNew.Set_Fields("TotalWBillAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("TotalWBillAmount") / 2), 2));
							rsNew.Set_Fields("TotalSBillAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("TotalSBillAmount") / 2), 2));
							rsNew.Set_Fields("WRT1", rsS.Get_Fields_Int32("WRT1"));
							rsNew.Set_Fields("WRT2", rsS.Get_Fields_Int32("WRT2"));
							rsNew.Set_Fields("WRT3", rsS.Get_Fields_Int32("WRT3"));
							rsNew.Set_Fields("WRT4", rsS.Get_Fields_Int32("WRT4"));
							rsNew.Set_Fields("WRT5", rsS.Get_Fields_Int32("WRT5"));
							rsNew.Set_Fields("SRT1", rsS.Get_Fields_Int32("SRT1"));
							rsNew.Set_Fields("SRT2", rsS.Get_Fields_Int32("SRT2"));
							rsNew.Set_Fields("SRT3", rsS.Get_Fields_Int32("SRT3"));
							rsNew.Set_Fields("SRT4", rsS.Get_Fields_Int32("SRT4"));
							rsNew.Set_Fields("SRT5", rsS.Get_Fields_Int32("SRT5"));
							rsNew.Set_Fields("Location", rsS.Get_Fields_String("Location"));
							rsNew.Set_Fields("CreationDate", rsS.Get_Fields_DateTime("CreationDate"));
							rsNew.Set_Fields("LastUpdatedDate", DateTime.Today);
							rsNew.Set_Fields("BName", rsS.Get_Fields_String("BName"));
							rsNew.Set_Fields("BName2", rsS.Get_Fields_String("BName2"));
							rsNew.Set_Fields("BAddress1", rsS.Get_Fields_String("BAddress1"));
							rsNew.Set_Fields("BAddress2", rsS.Get_Fields_String("BAddress2"));
							rsNew.Set_Fields("BAddress3", rsS.Get_Fields_String("BAddress3"));
							rsNew.Set_Fields("BCity", rsS.Get_Fields_String("BCity"));
							rsNew.Set_Fields("BState", rsS.Get_Fields_String("BState"));
							rsNew.Set_Fields("BZip", rsS.Get_Fields_String("BZip"));
							rsNew.Set_Fields("BZip4", rsS.Get_Fields_String("BZip4"));
							rsNew.Set_Fields("OName", rsS.Get_Fields_String("OName"));
							rsNew.Set_Fields("OName2", rsS.Get_Fields_String("OName2"));
							rsNew.Set_Fields("OAddress1", rsS.Get_Fields_String("OAddress1"));
							rsNew.Set_Fields("OAddress2", rsS.Get_Fields_String("OAddress2"));
							rsNew.Set_Fields("OAddress3", rsS.Get_Fields_String("OAddress3"));
							rsNew.Set_Fields("OCity", rsS.Get_Fields_String("OCity"));
							rsNew.Set_Fields("OState", rsS.Get_Fields_String("OState"));
							rsNew.Set_Fields("OZip", rsS.Get_Fields_String("OZip"));
							rsNew.Set_Fields("OZip4", rsS.Get_Fields_String("OZip4"));
							rsNew.Set_Fields("Note", rsS.Get_Fields_String("Note"));
							rsNew.Set_Fields("MapLot", rsS.Get_Fields_String("MapLot"));
							rsNew.Set_Fields("BookPage", rsS.Get_Fields_String("BookPage"));
							rsNew.Set_Fields("Telephone", rsS.Get_Fields_String("Telephone"));
							rsNew.Set_Fields("Email", rsS.Get_Fields_String("Email"));
							rsNew.Set_Fields("WCat", rsS.Get_Fields_Int32("WCat"));
							rsNew.Set_Fields("SCat", rsS.Get_Fields_Int32("SCat"));
							rsNew.Set_Fields("BillMessage", rsS.Get_Fields_String("BillMessage"));
							rsNew.Set_Fields("WBillOwner", rsS.Get_Fields_Boolean("WBillOwner"));
							rsNew.Set_Fields("SBillOwner", rsS.Get_Fields_Boolean("SBillOwner"));
							if (!rsS.IsFieldNull("BillDate"))
							{
								rsNew.Set_Fields("BillDate", rsS.Get_Fields_DateTime("BillDate"));
							}
							rsNew.Set_Fields("ReadingUnits", rsS.Get_Fields_Int32("ReadingUnits"));
							rsNew.Set_Fields("WOrigBillAmount", rsS.Get_Fields_Double("WOrigBillAmount"));
							rsNew.Set_Fields("SOrigBillAmount", rsS.Get_Fields_Double("SOrigBillAmount"));
							rsNew.Set_Fields("WPrinOwed", dblWPSplit2);
							rsNew.Set_Fields("WTaxOwed", dblWTSplit2);
							rsNew.Set_Fields("SPrinOwed", dblSPSplit2);
							rsNew.Set_Fields("STaxOwed", dblSTSplit2);
							rsNew.Set_Fields("MeterChangedOut", rsS.Get_Fields_Boolean("MeterChangedOut"));
							// kk 06172013 trouts-19  Bill Fields with Nulls messing up prepays
							rsNew.Set_Fields("WLienStatusEligibility", 0);
							rsNew.Set_Fields("WLienProcessStatus", 0);
							rsNew.Set_Fields("WLienRecordNumber", 0);
							rsNew.Set_Fields("WCombinationLienKey", 0);
							rsNew.Set_Fields("SLienStatusEligibility", 0);
							rsNew.Set_Fields("SLienProcessStatus", 0);
							rsNew.Set_Fields("SLienRecordNumber", 0);
							rsNew.Set_Fields("SCombinationLienKey", 0);
							rsNew.Set_Fields("WIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
							rsNew.Set_Fields("WIntOwed", 0);
							rsNew.Set_Fields("WIntAdded", 0);
							rsNew.Set_Fields("WCostOwed", 0);
							rsNew.Set_Fields("WCostAdded", 0);
							rsNew.Set_Fields("WPrinPaid", 0);
							rsNew.Set_Fields("WTaxPaid", 0);
							rsNew.Set_Fields("WIntPaid", 0);
							rsNew.Set_Fields("WCostPaid", 0);
							rsNew.Set_Fields("SIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
							rsNew.Set_Fields("SIntOwed", 0);
							rsNew.Set_Fields("SIntAdded", 0);
							rsNew.Set_Fields("SCostOwed", 0);
							rsNew.Set_Fields("SCostAdded", 0);
							rsNew.Set_Fields("SPrinPaid", 0);
							rsNew.Set_Fields("STaxPaid", 0);
							rsNew.Set_Fields("SIntPaid", 0);
							rsNew.Set_Fields("SCostPaid", 0);
							rsNew.Set_Fields("Copies", 0);
							rsNew.Set_Fields("Loadback", 0);
							rsNew.Set_Fields("Final", 0);
							rsNew.Set_Fields("SHasOverride", 0);
							rsNew.Set_Fields("WHasOverride", 0);
							rsNew.Set_Fields("SDemandGroupID", 0);
							rsNew.Set_Fields("WDemandGroupID", 0);
							rsNew.Set_Fields("SendEBill", 0);
							rsNew.Set_Fields("WinterBill", 0);
							rsNew.Set_Fields("SUploaded", 0);
							rsNew.Set_Fields("WUploaded", 0);
							// TODO Get_Fields: Field [MeterChangedOutConsumption] not found!! (maybe it is an alias?)
							if (FCConvert.ToString(rsS.Get_Fields("MeterChangedOutConsumption")) != "")
							{
								// TODO Get_Fields: Field [MeterChangedOutConsumption] not found!! (maybe it is an alias?)
								rsNew.Set_Fields("MeterChangedOutConsumption", rsS.Get_Fields("MeterChangedOutConsumption"));
								// kjr trout-1118 11/7/2016
							}
							rsNew.Update();
							lngNewBillKey = FCConvert.ToInt32(Math.Round(Conversion.Val(rsNew.Get_Fields_Int32("ID"))));
							// kgk trout-684  This is loading a different record in some cases    rsNew.MoveFirst
							rsNew.OpenRecordset("SELECT * FROM Bill WHERE id = " + FCConvert.ToString(lngNewBillKey), modExtraModules.strUTDatabase);
							// update the original record
							rsS.Edit();
							rsS.Set_Fields("WPrinOwed", dblWPSplit1);
							rsS.Set_Fields("WTaxOwed", dblWTSplit1);
							rsS.Set_Fields("SPrinOwed", dblSPSplit1);
							rsS.Set_Fields("STaxOwed", dblSTSplit1);
							rsS.Set_Fields("WaterOverrideCons", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Int32("WaterOverrideCons")) - rsNew.Get_Fields_Int32("WaterOverrideCons"), 2));
							rsS.Set_Fields("WaterOverrideAmount", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Decimal("WaterOverrideAmount") - rsNew.Get_Fields_Decimal("WaterOverrideAmount")), 2));
							rsS.Set_Fields("SewerOverrideCons", FCUtils.Round(FCConvert.ToDouble(rsS.Get_Fields_Int32("SewerOverrideCons")) - rsNew.Get_Fields_Int32("SewerOverrideCons"), 2));
							rsS.Set_Fields("SewerOverrideAmount", FCUtils.Round(rsS.Get_Fields_Decimal("SewerOverrideAmount") - rsNew.Get_Fields_Decimal("SewerOverrideAmount"), 2));
							rsS.Set_Fields("WMiscAmount", FCUtils.Round(rsS.Get_Fields_Decimal("WMiscAmount") - rsNew.Get_Fields_Decimal("WMiscAmount"), 2));
							rsS.Set_Fields("WAdjustAmount", FCUtils.Round(rsS.Get_Fields_Decimal("WAdjustAmount") - rsNew.Get_Fields_Decimal("WAdjustAmount"), 2));
							rsS.Set_Fields("WDEAdjustAmount", FCUtils.Round(rsS.Get_Fields_Decimal("WDEAdjustAmount") - rsNew.Get_Fields_Decimal("WDEAdjustAmount"), 2));
							rsS.Set_Fields("WFlatAmount", FCUtils.Round(rsS.Get_Fields_Decimal("WFlatAmount") - rsNew.Get_Fields_Decimal("WFlatAmount"), 2));
							rsS.Set_Fields("WUnitsAmount", FCUtils.Round(rsS.Get_Fields_Decimal("WUnitsAmount") - rsNew.Get_Fields_Decimal("WUnitsAmount"), 2));
							rsS.Set_Fields("WConsumptionAmount", FCUtils.Round(rsS.Get_Fields_Decimal("WConsumptionAmount") - rsNew.Get_Fields_Decimal("WConsumptionAmount"), 2));
							rsS.Set_Fields("SMiscAmount", FCUtils.Round(rsS.Get_Fields_Decimal("SMiscAmount") - rsNew.Get_Fields_Decimal("SMiscAmount"), 2));
							rsS.Set_Fields("SAdjustAmount", FCUtils.Round(rsS.Get_Fields_Decimal("SAdjustAmount") - rsNew.Get_Fields_Decimal("SAdjustAmount"), 2));
							rsS.Set_Fields("SDEAdjustAmount", FCUtils.Round(rsS.Get_Fields_Decimal("SDEAdjustAmount") - rsNew.Get_Fields_Decimal("SDEAdjustAmount"), 2));
							rsS.Set_Fields("SFlatAmount", FCUtils.Round(rsS.Get_Fields_Decimal("SFlatAmount") - rsNew.Get_Fields_Decimal("SFlatAmount"), 2));
							rsS.Set_Fields("SUnitsAmount", FCUtils.Round(rsS.Get_Fields_Decimal("SUnitsAmount") - rsNew.Get_Fields_Decimal("SUnitsAmount"), 2));
							rsS.Set_Fields("SConsumptionAmount", FCUtils.Round(rsS.Get_Fields_Decimal("SConsumptionAmount") - rsNew.Get_Fields_Decimal("SConsumptionAmount"), 2));
							// TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
							rsS.Set_Fields("WTax", FCUtils.Round(rsS.Get_Fields("WTax") - rsNew.Get_Fields("WTax"), 2));
							// TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
							rsS.Set_Fields("STax", FCUtils.Round(rsS.Get_Fields("STax") - rsNew.Get_Fields("STax"), 2));
							rsS.Set_Fields("TotalWBillAmount", FCUtils.Round(rsS.Get_Fields_Decimal("TotalWBillAmount") - rsNew.Get_Fields_Decimal("TotalWBillAmount"), 2));
							rsS.Set_Fields("TotalSBillAmount", FCUtils.Round(rsS.Get_Fields_Decimal("TotalSBillAmount") - rsNew.Get_Fields_Decimal("TotalSBillAmount"), 2));
							rsS.Update();
							lngCT += 1;
						}
						rsS.MoveNext();
					}
					frmWait.InstancePtr.Unload();
					MessageBox.Show(FCConvert.ToString(lngCT) + " bills were split.", "Finished", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Split was cancelled.", "Cancel", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return SplitBills;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Splitting Bills", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SplitBills;
		}

		private static void UpdateMeterConsumption_26(int lngAccountKey, DateTime dtBillDate, int lngBillKey)
		{
			UpdateMeterConsumption(ref lngAccountKey, ref dtBillDate, ref lngBillKey);
		}

		private static void UpdateMeterConsumption(ref int lngAccountKey, ref DateTime dtBillDate, ref int lngBillKey)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will update all of the meter consumption records
				clsDRWrapper rsC = new clsDRWrapper();
				clsDRWrapper rsM = new clsDRWrapper();
				int lngAdj = 0;
				int lngMeterKey = 0;
				int lngPrev = 0;
				int lngCurr = 0;
				int lngDigits = 0;
				DateTime dtPrevious = default(DateTime);
				DateTime dtCurrent = default(DateTime);
				string strPrevCode = "";
				string strCurCode = "";
				bool boolNegMeter = false;
				rsC.OpenRecordset("SELECT * FROM MeterConsumption WHERE ID = 0", modExtraModules.strUTDatabase);
				rsM.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
				while (!rsM.EndOfFile())
				{
					//Application.DoEvents();
					if (FCConvert.ToInt32(rsM.Get_Fields_Int32("CurrentReading")) != -1)
					{
						// kk 110812 trout-884 Change from 0 to -1 for No Read
						lngMeterKey = FCConvert.ToInt32(rsM.Get_Fields_Int32("ID"));
						lngPrev = FCConvert.ToInt32(rsM.Get_Fields_Int32("PreviousReading"));
						lngCurr = FCConvert.ToInt32(rsM.Get_Fields_Int32("CurrentReading"));
						boolNegMeter = FCConvert.ToBoolean(rsM.Get_Fields_Boolean("NegativeConsumption"));
						lngDigits = FCConvert.ToInt32(rsM.Get_Fields_Int32("Digits"));
						// MAL@20080805: Add previous and current reading dates and codes to the history
						// Tracker Reference: 14929
						if (!rsM.IsFieldNull("PreviousReadingDate"))
						{
							dtPrevious = (DateTime)rsM.Get_Fields_DateTime("PreviousReadingDate");
						}
						if (!rsM.IsFieldNull("CurrentReadingDate"))
						{
							dtCurrent = (DateTime)rsM.Get_Fields_DateTime("CurrentReadingDate");
						}
						strPrevCode = FCConvert.ToString(rsM.Get_Fields_String("PreviousCode"));
						strCurCode = FCConvert.ToString(rsM.Get_Fields_String("CurrentCode"));
						rsC.AddNew();
						rsC.Set_Fields("AccountKey", lngAccountKey);
						rsC.Set_Fields("MeterKey", lngMeterKey);
						rsC.Set_Fields("MeterNumber", rsM.Get_Fields_Int32("MeterNumber"));
						rsC.Set_Fields("Begin", lngPrev);
						rsC.Set_Fields("End", lngCurr);
						if (lngCurr >= lngPrev)
						{
							rsC.Set_Fields("Consumption", lngCurr - lngPrev);
						}
						else
						{
							switch (modExtraModules.Statics.glngTownReadingUnits)
							{
								case 100000:
									{
										lngAdj = 5;
										break;
									}
								case 10000:
									{
										lngAdj = 4;
										break;
									}
								case 1000:
									{
										lngAdj = 3;
										break;
									}
								case 100:
									{
										lngAdj = 2;
										break;
									}
								case 10:
									{
										lngAdj = 1;
										break;
									}
								case 1:
									{
										lngAdj = 0;
										break;
									}
								default:
									{
										lngAdj = 0;
										break;
									}
							}
							//end switch
							// this must be a rolled over meter
							rsC.Set_Fields("Consumption", lngCurr + ((Math.Pow(10, (lngDigits - lngAdj))) - lngPrev));
						}
						rsC.Set_Fields("NegativeMeter", boolNegMeter);
						rsC.Set_Fields("BillDate", dtBillDate);
						// Date  'kk01232015 trout-1061/trouts-135  BillDate needs to match Bill.BillDate
						// kk01232015 trout-1061/trout-135  Why not add the bill key also?
						rsC.Set_Fields("BillKey", lngBillKey);
						rsC.Set_Fields("PreviousReadingDate", dtPrevious);
						rsC.Set_Fields("CurrentReadingDate", dtCurrent);
						rsC.Set_Fields("PreviousReadingCode", strPrevCode);
						rsC.Set_Fields("CurrentReadingCode", strCurCode);
						rsC.Update();
					}
					rsM.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating Meter Consumptions", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static bool CalculatePrePayments(int lngAccountKey, int lngMeterKey, ref double dblCredit)
		{
			bool CalculatePrePayments = false;
			// MAL@20080117: Create Pre-Payments with Account Credits
			// Call Reference: 113597
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool blnCreate;
				bool blnCredit;
				bool blnOwes = false;
				bool blnSeparate = false;
				bool blnWater = false;
				clsDRWrapper rsBill = new clsDRWrapper();
				clsDRWrapper rsPayment = new clsDRWrapper();
				clsDRWrapper rsNewBill = new clsDRWrapper();
				clsDRWrapper rsLien = new clsDRWrapper();
				int lngAccountNum;
				int lngBillNum = 0;
				int lngNewBill = 0;
				double dblPrincipal = 0;
				double dblCosts = 0;
				double dblInterest = 0;
				double dblTax = 0;
				double dblPLI = 0;
				double dblPrinPaid = 0;
				double dblCostPaid = 0;
				double dblIntPaid = 0;
				double dblTaxPaid = 0;
				double dblPLIPaid = 0;
				double dblPrinOwed = 0;
				double dblCostOwed = 0;
				double dblIntOwed = 0;
				double dblTaxOwed = 0;
				double dblPLIOwed = 0;
				double dblTotalOwed = 0;
				double dblCreditAmt = 0;
				double dblWCreditAmt = 0;
				double dblSCreditAmt = 0;
				string strCurrService = "";
				string strService = "";
				blnCreate = true;
				// Optimistic
				lngAccountNum = modUTStatusPayments.GetAccountNumber(ref lngAccountKey);
				if (modUTStatusPayments.Statics.TownService == "W")
				{
					blnWater = true;
					dblTotalOwed = modUTCalculations.CalculateAccountUTTotal_5(lngAccountKey, blnWater, true, DateTime.Today, false);
				}
				else if (modUTStatusPayments.Statics.TownService == "S")
				{
					blnWater = false;
					dblTotalOwed = modUTCalculations.CalculateAccountUTTotal_5(lngAccountKey, blnWater, true, DateTime.Today, false);
				}
				else if (modUTStatusPayments.Statics.TownService == "B")
				{
					blnWater = true;
					dblTotalOwed = modUTCalculations.CalculateAccountUTTotal_5(lngAccountKey, blnWater, true, DateTime.Today, false);
					blnWater = false;
					dblTotalOwed += modUTCalculations.CalculateAccountUTTotal_5(lngAccountKey, blnWater, true, DateTime.Today, false);
				}
				if (dblTotalOwed >= 0)
				{
					blnOwes = true;
				}
				else
				{
					blnOwes = false;
				}
				if (!blnOwes)
				{
					rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillStatus = 'B'", modExtraModules.strUTDatabase);
					if (rsBill.RecordCount() > 0)
					{
						rsBill.MoveFirst();
						while (!rsBill.EndOfFile())
						{
							//Application.DoEvents();
							strCurrService = FCConvert.ToString(rsBill.Get_Fields_String("Service"));
							lngBillNum = FCConvert.ToInt32(rsBill.Get_Fields_Int32("ID"));
							// If .Fields("BillNumber") = 105 Then
							// MsgBox "hi"
							// End If
							dblPrinPaid = 0;
							dblCostPaid = 0;
							dblIntPaid = 0;
							dblPLIPaid = 0;
							dblTaxPaid = 0;
							if (strCurrService == "B")
							{
								if (modGlobalConstants.Statics.gintApplyPrepayTo == 1)
								{
									// Primary Service
									if (modUTStatusPayments.Statics.gboolPayWaterFirst)
									{
										strService = "W";
										blnSeparate = false;
									}
									else
									{
										strService = "S";
										blnSeparate = false;
									}
								}
								else
								{
									strService = strCurrService;
									blnSeparate = true;
								}
							}
							else
							{
								strService = strCurrService;
								blnSeparate = false;
							}
							if (FCConvert.ToInt32(rsBill.Get_Fields_Int32("WLienRecordNumber")) == 0 && FCConvert.ToInt32(rsBill.Get_Fields_Int32("SLienRecordNumber")) == 0)
							{
								// Not Liened
								if (strService == "W" || strService == "B")
								{
									// Get Water Totals
									dblPrincipal = rsBill.Get_Fields_Double("WPrinOwed");
									dblCosts = rsBill.Get_Fields_Double("WCostOwed");
									dblInterest = rsBill.Get_Fields_Double("WIntOwed");
									dblTax = rsBill.Get_Fields_Double("WTaxOwed");
									rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillNum) + " AND ISNULL(Lien,0) = 0 AND AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND Service = 'W'", modExtraModules.strUTDatabase);
									if (rsPayment.RecordCount() > 0)
									{
										rsPayment.MoveFirst();
										while (!rsPayment.EndOfFile())
										{
											//Application.DoEvents();
											// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
											dblPrinPaid += FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
											dblCostPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
											dblIntPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
											// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
											dblTaxPaid += FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
											rsPayment.MoveNext();
										}
										dblPrinOwed = dblPrincipal - dblPrinPaid;
										dblCostOwed = dblCosts - dblCostPaid;
										dblIntOwed = dblInterest - dblIntPaid;
										dblTaxOwed = dblTax - dblTaxPaid;
										dblTotalOwed = FCUtils.Round((dblPrinOwed + dblCostOwed + dblIntOwed + dblTaxOwed), 2);
										if (dblTotalOwed < 0)
										{
											// Credit Exists - Create Correction Line
											rsPayment.AddNew();
											rsPayment.Set_Fields("AccountKey", lngAccountKey);
											rsPayment.Set_Fields("BillKey", lngBillNum);
											rsPayment.Set_Fields("ActualSystemDate", DateTime.Today);
											rsPayment.Set_Fields("EffectiveInterestDate", DateTime.Today);
											rsPayment.Set_Fields("RecordedTransactionDate", DateTime.Today);
											rsPayment.Set_Fields("Reference", "CORREC");
											rsPayment.Set_Fields("Period", "");
											rsPayment.Set_Fields("Code", "C");
											rsPayment.Set_Fields("Principal", FCUtils.Round(dblPrinOwed, 2));
											rsPayment.Set_Fields("CurrentInterest", FCUtils.Round(dblIntOwed, 2));
											rsPayment.Set_Fields("LienCost", FCUtils.Round(dblCostOwed, 2));
											rsPayment.Set_Fields("Tax", FCUtils.Round(dblTaxOwed, 2));
											rsPayment.Set_Fields("PaidBy", "ADMIN");
											rsPayment.Set_Fields("CashDrawer", "N");
											rsPayment.Set_Fields("GeneralLedger", "N");
											rsPayment.Set_Fields("Service", "W");
											rsPayment.Update();
											// Update the Bill Record
											rsBill.Edit();
											rsBill.Set_Fields("WPrinPaid", rsBill.Get_Fields_Double("WPrinPaid") + dblPrinOwed);
											rsBill.Set_Fields("WIntPaid", rsBill.Get_Fields_Double("WIntPaid") + dblIntOwed);
											rsBill.Set_Fields("WCostPaid", rsBill.Get_Fields_Double("WCostPaid") + dblCostOwed);
											rsBill.Set_Fields("WTaxPaid", rsBill.Get_Fields_Double("WTaxPaid") + dblTaxOwed);
											rsBill.Update();
											dblWCreditAmt += dblTotalOwed;
											dblCreditAmt += dblTotalOwed;
										}
									}
									else
									{
										// No Payments Made - Do Nothing
									}
								}
								if (strService == "S" || strService == "B")
								{
									// Get Sewer Totals
									dblPrinPaid = 0;
									dblCostPaid = 0;
									dblIntPaid = 0;
									dblPLIPaid = 0;
									dblTaxPaid = 0;
									dblPrincipal = rsBill.Get_Fields_Double("SPrinOwed");
									dblCosts = rsBill.Get_Fields_Double("SCostOwed");
									dblInterest = rsBill.Get_Fields_Double("SIntOwed");
									dblTax = rsBill.Get_Fields_Double("STaxOwed");
									rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngBillNum) + " AND ISNULL(Lien,0) = 0 AND AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND Service = 'S'", modExtraModules.strUTDatabase);
									if (rsPayment.RecordCount() > 0)
									{
										rsPayment.MoveFirst();
										while (!rsPayment.EndOfFile())
										{
											//Application.DoEvents();
											// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
											dblPrinPaid += FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
											dblCostPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
											dblIntPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
											// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
											dblTaxPaid += FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
											rsPayment.MoveNext();
										}
										dblPrinOwed = dblPrincipal - dblPrinPaid;
										dblCostOwed = dblCosts - dblCostPaid;
										dblIntOwed = dblInterest - dblIntPaid;
										dblTaxOwed = dblTax - dblTaxPaid;
										dblTotalOwed = FCUtils.Round((dblPrinOwed + dblCostOwed + dblIntOwed + dblTaxOwed), 2);
										if (dblTotalOwed < 0)
										{
											// Credit Exists - Create Correction Line
											rsPayment.AddNew();
											rsPayment.Set_Fields("AccountKey", lngAccountKey);
											rsPayment.Set_Fields("BillKey", lngBillNum);
											rsPayment.Set_Fields("ActualSystemDate", DateTime.Today);
											rsPayment.Set_Fields("EffectiveInterestDate", DateTime.Today);
											rsPayment.Set_Fields("RecordedTransactionDate", DateTime.Today);
											rsPayment.Set_Fields("Reference", "CORREC");
											rsPayment.Set_Fields("Period", "");
											rsPayment.Set_Fields("Code", "C");
											rsPayment.Set_Fields("Principal", FCUtils.Round(dblPrinOwed, 2));
											rsPayment.Set_Fields("CurrentInterest", FCUtils.Round(dblIntOwed, 2));
											rsPayment.Set_Fields("LienCost", FCUtils.Round(dblCostOwed, 2));
											rsPayment.Set_Fields("Tax", FCUtils.Round(dblTaxOwed, 2));
											rsPayment.Set_Fields("PaidBy", "ADMIN");
											rsPayment.Set_Fields("CashDrawer", "N");
											rsPayment.Set_Fields("GeneralLedger", "N");
											rsPayment.Set_Fields("Service", "S");
											rsPayment.Update();
											// Update the Bill Record
											rsBill.Edit();
											rsBill.Set_Fields("SPrinPaid", rsBill.Get_Fields_Double("SPrinPaid") + dblPrinOwed);
											rsBill.Set_Fields("SIntPaid", rsBill.Get_Fields_Double("SIntPaid") + dblIntOwed);
											rsBill.Set_Fields("SCostPaid", rsBill.Get_Fields_Double("SCostPaid") + dblCostOwed);
											rsBill.Set_Fields("STaxPaid", rsBill.Get_Fields_Double("STaxPaid") + dblTaxOwed);
											rsBill.Update();
											dblSCreditAmt += dblTotalOwed;
											dblCreditAmt += dblTotalOwed;
										}
									}
									else
									{
										// No Payments Made - Do Nothing
									}
								}
							}
							else
							{
								// Water Lien Record
								lngBillNum = FCConvert.ToInt32(rsBill.Get_Fields_Int32("WLienRecordNumber"));
								if (lngBillNum > 0)
								{
									// Reset Values
									dblPrinPaid = 0;
									dblCostPaid = 0;
									dblIntPaid = 0;
									dblPLIPaid = 0;
									dblTaxPaid = 0;
									rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngBillNum), modExtraModules.strUTDatabase);
									if (rsLien.RecordCount() > 0)
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblPrincipal = rsLien.Get_Fields("Principal");
										// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
										dblCosts = rsLien.Get_Fields("Costs");
										// TODO Get_Fields: Field [InterestAdded] not found!! (maybe it is an alias?)
										dblInterest = rsLien.Get_Fields("InterestAdded") * -1;
										// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
										dblPLI = rsLien.Get_Fields("Interest");
										// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
										dblTax = rsLien.Get_Fields("Tax");
										rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Reference <> 'CHGINT' AND BillKey = " + FCConvert.ToString(lngBillNum) + " AND ISNULL(Lien,0) = 1 AND Service = 'W'", modExtraModules.strUTDatabase);
										if (rsPayment.RecordCount() > 0)
										{
											rsPayment.MoveFirst();
											while (!rsPayment.EndOfFile())
											{
												//Application.DoEvents();
												// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
												dblPrinPaid += FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
												dblCostPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
												dblIntPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
												dblPLIPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest"));
												// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
												dblTaxPaid += FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
												rsPayment.MoveNext();
											}
											dblPrinOwed = dblPrincipal - dblPrinPaid;
											dblCostOwed = dblCosts - dblCostPaid;
											dblIntOwed = dblInterest - dblIntPaid;
											dblPLIOwed = dblPLI - dblPLIPaid;
											dblTaxOwed = dblTax - dblTaxPaid;
											dblTotalOwed = (dblPrinOwed + dblCostOwed + dblIntOwed + dblPLIOwed + dblTaxOwed);
											if (dblTotalOwed < 0 && !blnOwes)
											{
												// Credit Exists - Create Correction Line
												rsPayment.AddNew();
												rsPayment.Set_Fields("AccountKey", lngAccountKey);
												rsPayment.Set_Fields("BillKey", lngBillNum);
												rsPayment.Set_Fields("ActualSystemDate", DateTime.Today);
												rsPayment.Set_Fields("EffectiveInterestDate", DateTime.Today);
												rsPayment.Set_Fields("RecordedTransactionDate", DateTime.Today);
												rsPayment.Set_Fields("Reference", "CORREC");
												rsPayment.Set_Fields("Period", "");
												rsPayment.Set_Fields("Code", "C");
												rsPayment.Set_Fields("Principal", FCUtils.Round(dblPrinOwed, 2));
												rsPayment.Set_Fields("CurrentInterest", FCUtils.Round(dblIntOwed, 2));
												rsPayment.Set_Fields("PreLienInterest", FCUtils.Round(dblPLIOwed, 2));
												rsPayment.Set_Fields("LienCost", FCUtils.Round(dblCostOwed, 2));
												rsPayment.Set_Fields("Tax", FCUtils.Round(dblTaxOwed, 2));
												rsPayment.Set_Fields("PaidBy", "ADMIN");
												rsPayment.Set_Fields("CashDrawer", "N");
												rsPayment.Set_Fields("Service", "W");
												rsPayment.Update();
												// Update the Lien Record
												rsLien.Edit();
												// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
												rsLien.Set_Fields("PrinPaid", rsLien.Get_Fields("PrinPaid") + dblPrinOwed);
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												rsLien.Set_Fields("IntPaid", rsLien.Get_Fields("IntPaid") + dblIntOwed);
												rsLien.Set_Fields("CostPaid", rsLien.Get_Fields_Double("CostPaid") + dblCostOwed);
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												rsLien.Set_Fields("PLIPaid", rsLien.Get_Fields("PLIPaid") + dblPLIOwed);
												// kk07082015 trouts-151
												// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
												rsLien.Set_Fields("TaxPaid", rsLien.Get_Fields("TaxPaid") + dblTaxOwed);
												rsLien.Update();
												dblCreditAmt += dblTotalOwed;
												dblWCreditAmt += dblTotalOwed;
											}
										}
										else
										{
											// No Payments Made
										}
									}
								}
								// Sewer Lien Record
								lngBillNum = FCConvert.ToInt32(rsBill.Get_Fields_Int32("SLienRecordNumber"));
								if (lngBillNum > 0)
								{
									// Reset Values
									dblPrinPaid = 0;
									dblCostPaid = 0;
									dblIntPaid = 0;
									dblPLIPaid = 0;
									dblTaxPaid = 0;
									rsLien.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngBillNum), modExtraModules.strUTDatabase);
									if (rsLien.RecordCount() > 0)
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										dblPrincipal = rsLien.Get_Fields("Principal");
										// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
										dblCosts = rsLien.Get_Fields("Costs");
										// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
										dblInterest = rsLien.Get_Fields("IntAdded") * -1;
										// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
										dblPLI = rsLien.Get_Fields("Interest");
										// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
										dblTax = rsLien.Get_Fields("Tax");
										rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE Reference <> 'CHGINT' AND BillKey = " + FCConvert.ToString(lngBillNum) + " AND ISNULL(Lien,0) = 1 AND Service = 'S'", modExtraModules.strUTDatabase);
										if (rsPayment.RecordCount() > 0)
										{
											rsPayment.MoveFirst();
											while (!rsPayment.EndOfFile())
											{
												//Application.DoEvents();
												// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
												dblPrinPaid += FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
												dblCostPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
												dblIntPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
												dblPLIPaid += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest"));
												// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
												dblTaxPaid += FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
												rsPayment.MoveNext();
											}
											dblPrinOwed = FCUtils.Round(dblPrincipal - dblPrinPaid, 2);
											dblCostOwed = FCUtils.Round(dblCosts - dblCostPaid, 2);
											dblIntOwed = FCUtils.Round(dblInterest - dblIntPaid, 2);
											dblPLIOwed = FCUtils.Round(dblPLI - dblPLIPaid, 2);
											dblTaxOwed = FCUtils.Round(dblTax - dblTaxPaid, 2);
											dblTotalOwed = (dblPrinOwed + dblCostOwed + dblIntOwed + dblPLIOwed + dblTaxOwed);
											if (dblTotalOwed < 0 && !blnOwes)
											{
												// Credit Exists - Create Correction Line
												rsPayment.AddNew();
												rsPayment.Set_Fields("AccountKey", lngAccountKey);
												rsPayment.Set_Fields("BillKey", lngBillNum);
												rsPayment.Set_Fields("ActualSystemDate", DateTime.Today);
												rsPayment.Set_Fields("EffectiveInterestDate", DateTime.Today);
												rsPayment.Set_Fields("RecordedTransactionDate", DateTime.Today);
												rsPayment.Set_Fields("Reference", "CORREC");
												rsPayment.Set_Fields("Period", "");
												rsPayment.Set_Fields("Code", "C");
												rsPayment.Set_Fields("Principal", FCUtils.Round(dblPrinOwed, 2));
												rsPayment.Set_Fields("CurrentInterest", FCUtils.Round(dblIntOwed, 2));
												rsPayment.Set_Fields("PreLienInterest", FCUtils.Round(dblPLIOwed, 2));
												rsPayment.Set_Fields("LienCost", FCUtils.Round(dblCostOwed, 2));
												rsPayment.Set_Fields("Tax", FCUtils.Round(dblTaxOwed, 2));
												rsPayment.Set_Fields("PaidBy", "ADMIN");
												rsPayment.Set_Fields("CashDrawer", "N");
												rsPayment.Set_Fields("GeneralLedger", "N");
												rsPayment.Set_Fields("Service", "S");
												rsPayment.Update();
												// Update the Lien Record
												rsLien.Edit();
												// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
												rsLien.Set_Fields("PrinPaid", rsLien.Get_Fields("PrinPaid") + dblPrinOwed);
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												rsLien.Set_Fields("IntPaid", rsLien.Get_Fields("IntPaid") + dblIntOwed);
												rsLien.Set_Fields("CostPaid", rsLien.Get_Fields_Double("CostPaid") + dblCostOwed);
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												rsLien.Set_Fields("PLIPaid", rsLien.Get_Fields("PLIPaid") + dblPLIOwed);
												// kk07082015 trouts-151
												// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
												rsLien.Set_Fields("TaxPaid", rsLien.Get_Fields("TaxPaid") + dblTaxOwed);
												rsLien.Update();
												dblCreditAmt += dblTotalOwed;
												dblSCreditAmt += dblTotalOwed;
											}
										}
										else
										{
											// No Payments Made
										}
									}
								}
							}
							rsBill.MoveNext();
						}
					}
					if (dblCreditAmt < 0 && !blnOwes)
					{
						// Create Pre-Payment Record
						if (!blnSeparate)
						{
							// Combine All Together
							lngNewBill = modUTStatusPayments.CreateBlankUTBill(ref lngAccountKey, ref lngMeterKey);
							if (lngNewBill > 0)
							{
								rsNewBill.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngNewBill), modExtraModules.strUTDatabase);
								if (rsNewBill.RecordCount() == 0)
								{
									rsNewBill.AddNew();
									rsNewBill.Set_Fields("AccountKey", lngAccountKey);
									rsNewBill.Set_Fields("Year", 0);
									rsNewBill.Set_Fields("BillKey", lngNewBill);
									rsNewBill.Set_Fields("ActualSystemDate", DateTime.Today);
									rsNewBill.Set_Fields("EffectiveInterestDate", DateTime.Today);
									rsNewBill.Set_Fields("RecordedTransactionDate", DateTime.Today);
									rsNewBill.Set_Fields("Teller", "");
									rsNewBill.Set_Fields("Reference", "PREPAY-A");
									rsNewBill.Set_Fields("Period", "A");
									rsNewBill.Set_Fields("Code", "Y");
									rsNewBill.Set_Fields("ReceiptNumber", 0);
									rsNewBill.Set_Fields("Principal", FCUtils.Round(dblCreditAmt * -1, 2));
									rsNewBill.Set_Fields("PaidBy", "ADMIN");
									rsNewBill.Set_Fields("CashDrawer", "N");
									rsNewBill.Set_Fields("GeneralLedger", "N");
									rsNewBill.Set_Fields("Service", strService);
									rsNewBill.Update();
									blnCreate = true;
								}
								else
								{
									blnCreate = false;
								}
								// Update Bill Record with Amount
								rsNewBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngNewBill), modExtraModules.strUTDatabase);
								if (rsNewBill.RecordCount() > 0)
								{
									rsNewBill.Edit();
									rsNewBill.Set_Fields(strService + "PrinPaid", FCUtils.Round(dblCreditAmt * -1, 2));
									rsNewBill.Update();
								}
								blnCreate = true;
							}
							else
							{
								blnCreate = false;
							}
						}
						else
						{
							lngNewBill = 0;
							// Separate between services
							if (dblSCreditAmt < 0)
							{
								lngNewBill = modUTStatusPayments.CreateBlankUTBill(ref lngAccountKey, ref lngMeterKey);
								if (lngNewBill > 0)
								{
									rsNewBill.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngNewBill), modExtraModules.strUTDatabase);
									if (rsNewBill.RecordCount() == 0)
									{
										rsNewBill.AddNew();
										rsNewBill.Set_Fields("AccountKey", lngAccountKey);
										rsNewBill.Set_Fields("Year", 0);
										rsNewBill.Set_Fields("BillKey", lngNewBill);
										rsNewBill.Set_Fields("ActualSystemDate", DateTime.Today);
										rsNewBill.Set_Fields("EffectiveInterestDate", DateTime.Today);
										rsNewBill.Set_Fields("RecordedTransactionDate", DateTime.Today);
										rsNewBill.Set_Fields("Teller", "");
										rsNewBill.Set_Fields("Reference", "PREPAY-A");
										rsNewBill.Set_Fields("Period", "A");
										rsNewBill.Set_Fields("Code", "Y");
										rsNewBill.Set_Fields("ReceiptNumber", 0);
										rsNewBill.Set_Fields("Principal", FCUtils.Round(dblSCreditAmt * -1, 2));
										rsNewBill.Set_Fields("PaidBy", "ADMIN");
										rsNewBill.Set_Fields("CashDrawer", "N");
										rsNewBill.Set_Fields("GeneralLedger", "N");
										rsNewBill.Set_Fields("Service", "S");
										rsNewBill.Update();
										blnCreate = true;
									}
									else
									{
										blnCreate = false;
									}
									// kgk 2-15-11 trout-689  Bill not updated with prepay amount
									// Update Bill Record with Amount
									rsNewBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngNewBill), modExtraModules.strUTDatabase);
									if (rsNewBill.RecordCount() > 0)
									{
										rsNewBill.Edit();
										rsNewBill.Set_Fields("SPrinPaid", FCUtils.Round(dblSCreditAmt * -1, 2));
										rsNewBill.Update();
									}
									blnCreate = true;
								}
								else
								{
									blnCreate = false;
								}
							}
							//Application.DoEvents();
							if (dblWCreditAmt < 0)
							{
								if (lngNewBill == 0)
								{
									lngNewBill = modUTStatusPayments.CreateBlankUTBill(ref lngAccountKey, ref lngMeterKey);
								}
								if (lngNewBill > 0)
								{
									rsNewBill.OpenRecordset("SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngNewBill), modExtraModules.strUTDatabase);
									// If .RecordCount = 0 Then
									rsNewBill.AddNew();
									rsNewBill.Set_Fields("AccountKey", lngAccountKey);
									rsNewBill.Set_Fields("Year", 0);
									rsNewBill.Set_Fields("BillKey", lngNewBill);
									rsNewBill.Set_Fields("ActualSystemDate", DateTime.Today);
									rsNewBill.Set_Fields("EffectiveInterestDate", DateTime.Today);
									rsNewBill.Set_Fields("RecordedTransactionDate", DateTime.Today);
									rsNewBill.Set_Fields("Teller", "");
									rsNewBill.Set_Fields("Reference", "PREPAY-A");
									rsNewBill.Set_Fields("Period", "A");
									rsNewBill.Set_Fields("Code", "Y");
									rsNewBill.Set_Fields("ReceiptNumber", 0);
									rsNewBill.Set_Fields("Principal", FCUtils.Round(dblWCreditAmt * -1, 2));
									rsNewBill.Set_Fields("PaidBy", "ADMIN");
									rsNewBill.Set_Fields("CashDrawer", "N");
									rsNewBill.Set_Fields("GeneralLedger", "N");
									rsNewBill.Set_Fields("Service", "W");
									rsNewBill.Update();
									blnCreate = true;
									// Else
									// blnCreate = False
									// End If
									// kgk 2-15-11 trout-689  Bill not updated with prepay amount
									// Update Bill Record with Amount
									rsNewBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngNewBill), modExtraModules.strUTDatabase);
									if (rsNewBill.RecordCount() > 0)
									{
										rsNewBill.Edit();
										rsNewBill.Set_Fields("WPrinPaid", FCUtils.Round(dblWCreditAmt * -1, 2));
										rsNewBill.Update();
									}
									blnCreate = true;
								}
								else
								{
									blnCreate = false;
								}
							}
						}
					}
					else if (dblCreditAmt == 0)
					{
						blnCreate = true;
					}
					else
					{
						blnCreate = false;
					}
				}
				else
				{
					blnCreate = false;
				}
				dblCredit = dblCreditAmt;
				CalculatePrePayments = blnCreate;
				return CalculatePrePayments;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Pre-Payments", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculatePrePayments;
		}

		public static void AddAccounttoPrePayTable(int lngAccountKey, double dblCredit, bool blnCreate)
		{
			// Call Reference: 113597
			clsDRWrapper rsTable = new clsDRWrapper();
			rsTable.OpenRecordset("SELECT * FROM tblAutoPrepayAccounts WHERE AccountKey = " + FCConvert.ToString(lngAccountKey), modExtraModules.strUTDatabase);
			if (rsTable.RecordCount() == 0)
			{
				rsTable.AddNew();
			}
			else
			{
				rsTable.Edit();
			}
			rsTable.Set_Fields("AccountKey", lngAccountKey);
			rsTable.Set_Fields("CreditAmt", dblCredit);
			rsTable.Set_Fields("CreditCreated", blnCreate);
			rsTable.Update();
		}

		public static void PrintPrePayReport()
		{
			// Call Reference: 113597
			clsDRWrapper rsAccount = new clsDRWrapper();
			rsAccount.OpenRecordset("SELECT * FROM tblAutoPrepayAccounts", modExtraModules.strUTDatabase);
			if (rsAccount.RecordCount() > 0)
			{
				rptAutoPrepayAccounts.InstancePtr.Init();
				frmReportViewer.InstancePtr.Init(rptAutoPrepayAccounts.InstancePtr);
			}
		}

		public static void ClearPrepaymentTable()
		{
			// Call Reference: 113597
			clsDRWrapper rsTable = new clsDRWrapper();
			rsTable.Execute("DELETE FROM tblAutoPrepayAccounts", modExtraModules.strUTDatabase);
		}

		public static bool AcceptsEBills(int lngAcctKey, ref string strEBillType)
		{
			bool AcceptsEBills = false;
			// Tracker Reference: 10680
			bool blnResult = false;
			clsDRWrapper rsIC = new clsDRWrapper();
			rsIC.OpenRecordset("SELECT * FROM tblIConnectInfo WHERE AccountKey = " + FCConvert.ToString(lngAcctKey) + " AND IConnectAccountKey > 0", modExtraModules.strUTDatabase);
			if (rsIC.RecordCount() > 0)
			{
				strEBillType = FCConvert.ToString(rsIC.Get_Fields_String("AcceptEBill"));
				if ((strEBillType == "E") || (strEBillType == "B"))
				{
					blnResult = true;
				}
				else if (strEBillType == "P")
				{
					blnResult = false;
				}
				else
				{
					blnResult = false;
				}
			}
			else
			{
				blnResult = false;
			}
			AcceptsEBills = blnResult;
			return AcceptsEBills;
		}

		public static void strSort_24(AcctInfo[] words, bool Ascending, bool AllLowerCase)
		{
			strSort(ref words, ref Ascending, ref AllLowerCase);
		}

		public static void strSort(ref AcctInfo[] words, ref bool Ascending, ref bool AllLowerCase)
		{
			// Pass in string array you want to sort by reference and
			// read it back
			// Set Ascending to True to sort ascending, '
			// false to sort descending
			// If AllLowerCase is True, strings will be sorted
			// without regard to case.  Otherwise, upper
			// case characters take precedence over lower
			// case characters
			int i;
			int j;
			// vbPorter upgrade warning: NumInArray As Variant --> As int
			int NumInArray, LowerBound;
			NumInArray = Information.UBound(words, 1);
			LowerBound = Information.LBound(words);
			for (i = LowerBound; i <= NumInArray; i++)
			{
				j = 0;
				for (j = LowerBound; j <= NumInArray; j++)
				{
					//Application.DoEvents();
					if (AllLowerCase == true)
					{
						if (Ascending == true)
						{
							if (Strings.StrComp(Strings.LCase(words[i].AccountName), Strings.LCase(words[j].AccountName), CompareConstants.vbBinaryCompare) == -1)
							{
								Swap(ref words[i], ref words[j]);
							}
						}
						else
						{
							if (Strings.StrComp(Strings.LCase(words[i].AccountName), Strings.LCase(words[j].AccountName), CompareConstants.vbBinaryCompare) == 1)
							{
								Swap(ref words[i], ref words[j]);
							}
						}
					}
					else
					{
						if (Ascending == true)
						{
							if (Strings.StrComp(words[i].AccountName, words[j].AccountName, CompareConstants.vbBinaryCompare) == -1)
							{
								Swap(ref words[i], ref words[j]);
							}
						}
						else
						{
							if (Strings.StrComp(words[i].AccountName, words[j].AccountName, CompareConstants.vbBinaryCompare) == 1)
							{
								Swap(ref words[i], ref words[j]);
							}
						}
					}
				}
				// j
			}
			// i
		}

		private static void Swap(ref AcctInfo var1, ref AcctInfo var2)
		{
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			AcctInfo x = new AcctInfo(0);
			x.AccountKey = var1.AccountKey;
			x.AccountName = var1.AccountName;
			var1.AccountKey = var2.AccountKey;
			var1.AccountName = var2.AccountName;
			var2.AccountKey = x.AccountKey;
			var2.AccountName = x.AccountName;
		}

		public static double GetStormwaterFee_6(double dblImpervSurf, bool boolMonthly)
		{
			return GetStormwaterFee(ref dblImpervSurf, ref boolMonthly);
		}

		public static double GetStormwaterFee_8(double dblImpervSurf, bool boolMonthly)
		{
			return GetStormwaterFee(ref dblImpervSurf, ref boolMonthly);
		}

		public static double GetStormwaterFee(ref double dblImpervSurf, ref bool boolMonthly)
		{
			double GetStormwaterFee = 0;
			int lngExemptLimit;
			int lngTier1Limit;
			int lngTierNLimit;
			double dblTier1Fee;
			double dblTierNFee;
			double dblCalcTmp = 0;
			// THESE NEED TO BE CONFIGURABLE
			lngExemptLimit = 500;
			lngTier1Limit = 3500;
			lngTierNLimit = 1000;
			dblTier1Fee = 22.0;
			dblTierNFee = 11.0;
			if (dblImpervSurf < lngExemptLimit)
			{
				GetStormwaterFee = 0;
			}
			else if (dblImpervSurf < lngTier1Limit)
			{
				if (!boolMonthly)
				{
					GetStormwaterFee = FCUtils.Round(dblTier1Fee / 4, 2);
				}
				else
				{
					GetStormwaterFee = FCUtils.Round(dblTier1Fee / 12, 2);
					// If month endif
					// Add round up once per quarter on monthly bills
					// Endif
				}
			}
			else
			{
				dblCalcTmp = (FCUtils.Round(dblImpervSurf / lngTierNLimit) - 1) * dblTierNFee;
				if (!boolMonthly)
				{
					GetStormwaterFee = FCUtils.Round(dblCalcTmp / 4, 2);
				}
				else
				{
					GetStormwaterFee = FCUtils.Round(dblCalcTmp / 12, 2);
					// If month endif
					// Add round up once per quarter on monthly bills
					// Endif
				}
			}
			return GetStormwaterFee;
		}
		// kk04302014 trouts-64  Add option to revert status to last billed status
		private static void RevertMeterReadings(ref string strBookSel)
		{
			clsDRWrapper rsMeters = new clsDRWrapper();
			clsDRWrapper rsHistory = new clsDRWrapper();
			DateTime dtBillDate;
			rsMeters.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber " + strBookSel + " AND BillingStatus <> 'B'", modExtraModules.strUTDatabase);
			while (!rsMeters.EndOfFile())
			{
				rsHistory.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + rsMeters.Get_Fields_Int32("BookNumber"), modExtraModules.strUTDatabase);
				if (!rsHistory.IsFieldNull("BDate"))
				{
					dtBillDate = (DateTime)rsHistory.Get_Fields_DateTime("BDate");
				}
				else
				{
					dtBillDate = DateTime.FromOADate(0);
					// DateAndTime.DateValue(FCConvert.ToString(0));
				}
				rsMeters.Edit();
				rsMeters.Set_Fields("BillingStatus", "B");
				rsHistory.OpenRecordset("SELECT * FROM MeterConsumption WHERE MeterKey = " + rsMeters.Get_Fields_Int32("ID") + " AND AccountKey = " + rsMeters.Get_Fields_Int32("AccountKey") + " AND BillDate = '" + Strings.Format(dtBillDate, "MM/dd/yyyy") + "'", modExtraModules.strUTDatabase);
				if (!rsHistory.EndOfFile() && !rsHistory.BeginningOfFile())
				{
					if (!rsHistory.IsFieldNull("CurrentReadingDate"))
					{
						rsMeters.Set_Fields("CurrentReadingDate", rsHistory.Get_Fields_DateTime("CurrentReadingDate"));
					}
					else
					{
						rsMeters.Set_Fields("CurrentReadingDate", DateTime.FromOADate(0));
						// DateAndTime.DateValue(FCConvert.ToString(0)));
					}
					// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
					rsMeters.Set_Fields("CurrentReading", FCConvert.ToString(Conversion.Val(rsHistory.Get_Fields("End"))));
					rsMeters.Set_Fields("CurrentCode", rsHistory.Get_Fields_String("CurrentReadingCode"));
					if (!rsHistory.IsFieldNull("PreviousReadingDate"))
					{
						rsMeters.Set_Fields("PreviousReadingDate", rsHistory.Get_Fields_DateTime("PreviousReadingDate"));
					}
					else
					{
						rsMeters.Set_Fields("PreviousReadingDate", DateAndTime.DateValue(FCConvert.ToString(0)));
					}
					rsMeters.Set_Fields("PreviousReading", FCConvert.ToString(Conversion.Val(rsHistory.Get_Fields_Int32("Begin"))));
					rsMeters.Set_Fields("PreviousCode", rsHistory.Get_Fields_String("PreviousReadingCode"));
				}
				else
				{
					// Create an error log for a report?
					rsMeters.Set_Fields("CurrentReading", -1);
					rsMeters.Set_Fields("CurrentCode", "");
					rsMeters.Set_Fields("CurrentReadingDate", DateTime.FromOADate(0));
					//DateAndTime.DateValue(FCConvert.ToString(0)));
				}
				rsMeters.Update();
				rsMeters.MoveNext();
			}
		}

        public static int CreateAutoPayRecs(int lngRateKey,  int[] arrBookArray,  bool boolFinal = false)
        {
            int CreateAutoPayRecs = 0;
            int lngCount = 0;
            // trouts-266 code freeze
            try
            {   // On Error GoTo ERROR_HANDLER
                // this function will cycle through all of the bills in these books and associate the ratekey passed
                // in with each as well as transfer all of the totals to Owed in the bill (this will let UTCL show them)
                int lngCT;
                
                clsDRWrapper rsBills = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsTemp = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsAP = new/*AsNew*/ clsDRWrapper();
                DateTime dtBatchDate;
                string strBatchNumber = "";
                DateTime dtPmtDate;
                double dblXtraInt = 0;
                double dblPrinDue = 0;
                double dblIntDue = 0;
                double dblPLIDue = 0;
                double dblTaxDue;
                double dblCostDue = 0;
                double dblTotalDue = 0;

                dtPmtDate = DateTime.FromOADate(0);
                // frmWait.Init "Please Wait..." & vbCrLf & "Checking for duplicate records."
                lngCount = 0;

                // cycle through the books selected by the user
                for (lngCT = 1; lngCT <= fecherFoundation.Information.UBound(arrBookArray, 1); lngCT++)
                {
                    // only select bills that received a bill
                    rsBills.OpenRecordset("SELECT bill.id as BillID, BName, ActualAccountNumber, BillDate, BillNumber, tblacctach.id as AcctACHID tblAcctACH.* FROM Bill INNER JOIN tblAcctACH ON Bill.AccountKey = tblAcctACH.AccountKey WHERE ACHActive = 1 AND NOT ACHPreNote = 1 AND Book = " + FCConvert.ToString(arrBookArray[lngCT]) + " AND BillStatus = 'B' AND BillingRateKey = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
                    while (!rsBills.EndOfFile())
                    {
                        //////Application.DoEvents();
                        if (dtPmtDate.ToOADate() == 0)
                        {
                            dtPmtDate = (DateTime)rsBills.Get_Fields("BillDate");
                            object tempDate = dtPmtDate;
                            frmInput.InstancePtr.Init(ref tempDate, "Auto-Pay Payment Date", "Enter the Direct Withdrawl Date.", 1500, false,  modGlobalConstants.InputDTypes.idtDate);
                            dtPmtDate = (DateTime)tempDate;
                            if (dtPmtDate.ToOADate() == 0)
                            {
                                dtPmtDate = (DateTime)rsBills.Get_Fields("BillDate");
                            }
                            dtBatchDate = DateTime.Now;
                            strBatchNumber = fecherFoundation.Strings.Format(dtBatchDate, "YYMMDDHhNn") + "-" + fecherFoundation.Strings.Format(rsBills.Get_Fields("BillNumber"), "0000");
                        }

                        dblPrinDue = 0.0;
                        dblXtraInt = 0.0;
                        dblIntDue = 0.0;
                        dblPLIDue = 0.0;
                        dblTaxDue = 0.0;
                        dblCostDue = 0.0;
                        dblTotalDue = 0.0;

                        // kk03052014  Change to Account Balance
                        // rsTemp.OpenRecordset "SELECT * FROM Bill WHERE Bill = " & rsBills.Fields("Bill"), strUTDatabase
                        dblTotalDue = modUTCalculations.CalculateAccountUTTotal(rsBills.Get_Fields("AccountKey"), FCConvert.ToString(rsBills.Get_Fields("Service")) == "W", true, ref dblXtraInt, ref dblIntDue, ref dtPmtDate, false, 0,false, ref dblCostDue, ref dblPLIDue);
                        dblPrinDue = dblTotalDue - (dblCostDue + dblXtraInt + dblIntDue + dblPLIDue); // Equals Prin + Tax

                        if (dblTotalDue > 0.0)
                        {
                            // Check for ACH setup and active

                            rsAP.OpenRecordset("SELECT * FROM tblAutoPay WHERE ID = -1", modExtraModules.strUTDatabase);
                            rsAP.AddNew();
                            rsAP.Set_Fields("BatchNumber", strBatchNumber);
                            rsAP.Set_Fields("BillKey", rsBills.Get_Fields("BillId"));
                            rsAP.Set_Fields("Service", rsBills.Get_Fields("Service"));
                            rsAP.Set_Fields("AccountNumber", rsBills.Get_Fields("ActualAccountNumber"));
                            rsAP.Set_Fields("Name", rsBills.Get_Fields("BName")); // XXX ????
                            rsAP.Set_Fields("AcctACHID", rsBills.Get_Fields("AcctACHID"));
                            // kk03052014  Change to Account Balance
                            // .Fields("ACHAmount") = CalculateAccountUT(rsTemp, dtPmtDate, dblXtraInt, rsBills.Fields("Service") = "W", dblCurPrin, dblCurInt, dblCurCost, , , , dblCurTax)
                            rsAP.Set_Fields("ACHAmount", dblTotalDue);
                            rsAP.Set_Fields("Prin", dblPrinDue);
                            rsAP.Set_Fields("Int", dblXtraInt + dblIntDue + dblPLIDue);
                            rsAP.Set_Fields("Cost", dblCostDue);
                            rsAP.Set_Fields("Tax", 0.0); // dblCurTax
                            rsAP.Set_Fields("BillDate", fecherFoundation.Strings.Format(rsBills.Get_Fields("BillDate"), "MM/DD/YYYY"));
                            rsAP.Set_Fields("ACHDueDate", fecherFoundation.Strings.Format(dtPmtDate, "MM/DD/YYYY"));
                            rsAP.Set_Fields("ACHDate", fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0)));
                            rsAP.Set_Fields("ACHDone", false);
                            rsAP.Set_Fields("EffPmtDate", fecherFoundation.Strings.Format(dtPmtDate, "MM/DD/YYYY"));
                            rsAP.Set_Fields("PostedDate", fecherFoundation.DateAndTime.DateValue(FCConvert.ToString(0)));
                            rsAP.Set_Fields("Posted", false);
                            rsAP.Set_Fields("LineNumber", 0);
                            rsAP.Set_Fields("PaymentID", 0);
                            rsAP.Set_Fields("ACHFlag", rsBills.Get_Fields("ACHActive"));
                            rsAP.Update();

                            lngCount += 1;
                            // Should we flag the bill as done?

                        }

                        rsBills.MoveNext();
                    }
                }

                if (lngCount > 0)
                {
                    arUTAutoPayEditReport.InstancePtr.Init( autoPayRptType.autoPayRptTypeProc, false,strBatchNumber);
                }

                frmWait.InstancePtr.Unload();
                CreateAutoPayRecs = lngCount; // return the total number of Auto-Pay records created
                return CreateAutoPayRecs;
            }
            catch (Exception ex)
            {   
                frmWait.InstancePtr.Unload();
                CreateAutoPayRecs = lngCount;
                MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Error Transferring Billing Amounts - " + FCConvert.ToString(lngCount), MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CreateAutoPayRecs;
        }
    }
}
