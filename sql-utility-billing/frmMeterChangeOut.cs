﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmMeterChangeOut.
	/// </summary>
	public partial class frmMeterChangeOut : BaseForm
	{
		public frmMeterChangeOut()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            txtNewMeterReading.AllowOnlyNumericInput();
            txtOldMeterReading.AllowOnlyNumericInput();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMeterChangeOut InstancePtr
		{
			get
			{
				return (frmMeterChangeOut)Sys.GetInstance(typeof(frmMeterChangeOut));
			}
		}

		protected frmMeterChangeOut _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               10/13/2004              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/06/2007              *
		// ********************************************************
		int KeyCol;
		int DigitsCol;
		int BookCol;
		int SequenceCol;
		int MeterNumberCol;
		int SizeCol;
		int SerialNumberCol;
		int CheckBoxCol;
		// vbPorter upgrade warning: lngMeterKey As int	OnWrite(short, string)
		int lngMeterKey;
		int lngLastReadingCol;
		bool boolReset;

		private void cboAccounts_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			if (!boolReset)
			{
				fraInformation.Visible = false;
				cmdFileNameSearch.Enabled = true;
				// MAL@20070914
				ClearInformation();
				vsMeters.Rows = 1;
				rsInfo.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(cboAccounts.ItemData(cboAccounts.SelectedIndex)));
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						vsMeters.Rows += 1;
						vsMeters.TextMatrix(vsMeters.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("ID")));
						vsMeters.TextMatrix(vsMeters.Rows - 1, DigitsCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("Digits")));
						vsMeters.TextMatrix(vsMeters.Rows - 1, MeterNumberCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("MeterNumber")));
						vsMeters.TextMatrix(vsMeters.Rows - 1, BookCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("BookNumber")));
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						vsMeters.TextMatrix(vsMeters.Rows - 1, SequenceCol, FCConvert.ToString(rsInfo.Get_Fields("Sequence")));
						vsMeters.TextMatrix(vsMeters.Rows - 1, SizeCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("Size")));
						vsMeters.TextMatrix(vsMeters.Rows - 1, SerialNumberCol, FCConvert.ToString(rsInfo.Get_Fields_String("SerialNumber")));
						// kk 110812 Change to use -1 for No Read
						// kk10222014 trouts-93  Change this so we don't let the -1 go through
						if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("CurrentReading")) == -1)
						{
							// And rsInfo.Fields("PreviousReading") > 0
							if (rsInfo.Get_Fields_Int32("PreviousReading") > 0)
							{
								vsMeters.TextMatrix(vsMeters.Rows - 1, lngLastReadingCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("PreviousReading")));
							}
							else
							{
								vsMeters.TextMatrix(vsMeters.Rows - 1, lngLastReadingCol, FCConvert.ToString(0));
							}
						}
						else
						{
							vsMeters.TextMatrix(vsMeters.Rows - 1, lngLastReadingCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("CurrentReading")));
						}
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				Frame1.Visible = true;
				fraAccountSearch.Visible = false;
			}
		}

		private void cboBackflow_DropDown(object sender, System.EventArgs e)
		{
			// SendMessageByNum Me.cboBackflow.hwnd, CB_SETDROPPEDWIDTH, 300, 0
		}

		private void cboDgits_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (txtOldMeterReading.Text.Length > Conversion.Val(cboDgits.Text))
			{
				txtOldMeterReading.Text = "";
				txtNewMeterReading.Text = "";
			}
			// txtOldMeterReading.MaxLength = Val(cboDgits.Text)
			txtNewMeterReading.MaxLength = FCConvert.ToInt32(Conversion.Val(cboDgits.Text));
		}

		private void cboSize_DropDown(object sender, System.EventArgs e)
		{
			modAPIsConst.SendMessageByNum(this.cboSize.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 300, 0);
		}

		private void frmMeterChangeOut_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmMeterChangeOut_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMeterChangeOut properties;
			//frmMeterChangeOut.FillStyle	= 0;
			//frmMeterChangeOut.ScaleWidth	= 9405;
			//frmMeterChangeOut.ScaleHeight	= 7710;
			//frmMeterChangeOut.LinkTopic	= "Form2";
			//frmMeterChangeOut.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			KeyCol = 0;
			DigitsCol = 1;
			MeterNumberCol = 2;
			BookCol = 3;
			SequenceCol = 4;
			SizeCol = 5;
			lngLastReadingCol = 6;
			SerialNumberCol = 7;
			lngMeterKey = 0;
			FormatGrid();
			cboBackflow.Clear();
			cboBackflow.AddItem("Yes");
			cboBackflow.AddItem("No");
			cboDgits.Clear();
			cboDgits.AddItem("3");
			cboDgits.AddItem("4");
			cboDgits.AddItem("5");
			cboDgits.AddItem("6");
			cboDgits.AddItem("7");
			cboDgits.AddItem("8");
			cboDgits.AddItem("9");
			LoadAccounts();
			LoadSizesCombo();
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
            //FC:FINAL:CHN - issue #1058: order by key is not correct
            vsAccountSearch.Cols = 2;
			vsAccountSearch.ColDataType(1, FCGrid.DataTypeSettings.flexDTLong);
		}

		private void frmMeterChangeOut_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFileNameSearch_Click(object sender, System.EventArgs e)
		{
			// MAL@20070906: Added search objects and functions
			// this will show the search frame
			cmdFileNameSearch.Enabled = false;
			mnuProcessSave.Enabled = false;
			fraAccountSearch.Top = Frame1.Top;
			fraAccountSearch.Left = FCConvert.ToInt32((this.Width - fraAccountSearch.Width) / 2.0);
			Frame1.Visible = false;
			fraAccountSearch.Visible = true;
			if (txtSearchName.Enabled && txtSearchName.Visible)
			{
				txtSearchName.Focus();
			}
			txtSearchName.Text = "";
			txtSearchAccount.Text = "";
			vsAccountSearch.Visible = false;
			vsAccountSearch.Rows = 1;
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void LoadAccounts()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT AccountNumber, Master.ID, pBill.FullNameLF AS Name FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID ORDER By AccountNumber", modExtraModules.strUTDatabase);
			cboAccounts.Clear();
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					cboAccounts.AddItem(Strings.Format(rsInfo.Get_Fields("AccountNumber"), "000000") + " - " + rsInfo.Get_Fields_String("Name"));
					cboAccounts.ItemData(cboAccounts.NewIndex, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void SetAccountCombo(ref int lngAccount)
		{
			int counter;
			for (counter = 0; counter <= cboAccounts.Items.Count - 1; counter++)
			{
				if (Strings.Format(lngAccount, "000000") == Strings.Left(cboAccounts.Items[counter].ToString(), 6))
				{
					cboAccounts.SelectedIndex = counter;
					break;
				}
			}
		}
		// vbPorter upgrade warning: strDigits As int	OnWrite(string)
		private void SetDigitsCombo(int strDigits)
		{
			int counter;
			for (counter = 0; counter <= cboDgits.Items.Count - 1; counter++)
			{
				if (strDigits == FCConvert.ToDouble(cboDgits.Items[counter].ToString()))
				{
					cboDgits.SelectedIndex = counter;
					break;
				}
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (fraInformation.Visible == true)
			{
				if (lngMeterKey != 0)
				{
					if (SaveInfo())
					{
						// kk02042015 trouts-112  Keep meter change out screen open   -  Unload Me
						ResetForm();
					}
				}
			}
			else if (vsMeters.Row > 0)
			{
				// select the meter and show the info frame
				vsMeters_DblClick(new object(), new EventArgs());
			}
			else
			{
				MessageBox.Show("Select a meter.", "Select Meter", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}

		private void txtNewMeterReading_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtOldMeterReading_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtSearchAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						SearchForAccounts();
						break;
					}
			}
			//end switch
		}

		private void vsMeters_DblClick(object sender, System.EventArgs e)
		{
			if (vsMeters.Row > 0)
			{
				LoadMeterInformation();
			}
		}

		private void vsMeters_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (vsMeters.Row > 0)
			{
				if (KeyCode == Keys.Return)
				{
					KeyCode = 0;
					LoadMeterInformation();
				}
			}
		}

		private void LoadMeterInformation()
		{
			lngMeterKey = FCConvert.ToInt32(vsMeters.TextMatrix(vsMeters.Row, KeyCol));
			txtNewMeterReading.Text = "";
			txtSerialNumber.Text = "";
			txtRemoteNumber.Text = "";
			// MAL@20080528 ; Tracker Reference: 13828
			txtReason.Text = "";
			if (cboBackflow.Items.Count > 0)
			{
				cboBackflow.SelectedIndex = 0;
			}
			if (cboSize.Items.Count > 0)
			{
				cboSize.SelectedIndex = 0;
			}
			SetDigitsCombo(FCConvert.ToInt32(vsMeters.TextMatrix(vsMeters.Row, DigitsCol)));
			txtOldMeterReading.MaxLength = FCConvert.ToInt32(vsMeters.TextMatrix(vsMeters.Row, DigitsCol));
			txtNewMeterReading.MaxLength = FCConvert.ToInt32(vsMeters.TextMatrix(vsMeters.Row, DigitsCol));
			txtOldMeterReading.Text = "";
			txtSetDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblPreviousReading.Text = "Previous: " + "\r\n" + vsMeters.TextMatrix(vsMeters.Row, lngLastReadingCol);
			Frame1.Visible = false;
			cmdFileNameSearch.Enabled = false;
			// MAL@20070912
			fraInformation.Visible = true;
		}

		private void LoadSizesCombo()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM MeterSizes ORDER BY Code", modExtraModules.strUTDatabase);
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					cboSize.AddItem(Strings.Format(rsInfo.Get_Fields("Code"), "00") + " - " + rsInfo.Get_Fields_String("ShortDescription"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private bool SaveInfo()
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				clsDRWrapper rsCons = new clsDRWrapper();
				clsDRWrapper rsPrevious = new clsDRWrapper();
				int lngConsumption = 0;
				SaveInfo = true;
				if (Strings.Trim(txtOldMeterReading.Text) == "")
				{
					MessageBox.Show("You must enter an old meter reading before you may continue.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveInfo = false;
					txtOldMeterReading.Focus();
					return SaveInfo;
				}
				if (Strings.Trim(txtNewMeterReading.Text) == "")
				{
					MessageBox.Show("You must enter an new meter reading before you may continue.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveInfo = false;
					txtNewMeterReading.Focus();
					return SaveInfo;
				}
				if (Strings.Trim(txtSerialNumber.Text) == "")
				{
					MessageBox.Show("You must enter a serial number before you may continue.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveInfo = false;
					txtSerialNumber.Focus();
					return SaveInfo;
				}
				if (cboDgits.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter the meter digits before you may continue.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveInfo = false;
					cboDgits.Focus();
					return SaveInfo;
				}
				if (cboBackflow.SelectedIndex == -1)
				{
					MessageBox.Show("You must select a backflow option before you may continue.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveInfo = false;
					cboBackflow.Focus();
					return SaveInfo;
				}
				if (cboSize.SelectedIndex == -1)
				{
					MessageBox.Show("You must enter the meter size before you may continue.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveInfo = false;
					cboSize.Focus();
					return SaveInfo;
				}
				if (!Information.IsDate(txtSetDate.Text))
				{
					MessageBox.Show("You must enter a valid set date before you may continue.", "Invalid Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					SaveInfo = false;
					txtSetDate.Focus();
					return SaveInfo;
				}
				rsMeterInfo.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(lngMeterKey), modExtraModules.strUTDatabase);
				if (!rsMeterInfo.EndOfFile())
				{
					//lngConsumption = modProcessTWUTXX41File.CalculateMeterConsumption_26(FCConvert.ToInt32(Math.Round(Conversion.Val(vsMeters.TextMatrix(vsMeters.Row, lngLastReadingCol))), FCConvert.ToInt32(Math.Round(Conversion.Val(txtOldMeterReading.Text)), rsMeterInfo.Get_Fields("Digits"));
					// FC:FINAL:VGE - #i975 Converting to Int16/short for parameter matching.
					lngConsumption = modProcessTWUTXX41File.CalculateMeterConsumption_26(FCConvert.ToInt32(Math.Round(Conversion.Val(vsMeters.TextMatrix(vsMeters.Row, lngLastReadingCol)))), FCConvert.ToInt32(Math.Round(Conversion.Val(txtOldMeterReading.Text))), FCConvert.ToInt16(rsMeterInfo.Get_Fields_Int32("Digits")));
					if (MessageBox.Show("Consumption for this meter is calculated at : " + FCConvert.ToString(lngConsumption) + "  Is this correct?", "Correct Consumption", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
					{
						if (rsMeterInfo.EndOfFile() != true && rsMeterInfo.BeginningOfFile() != true)
						{
							rsCons.OpenRecordset("SELECT * FROM MeterConsumption WHERE ID = 0");
							rsCons.AddNew();
							rsCons.Set_Fields("Accountkey", rsMeterInfo.Get_Fields_Int32("AccountKey"));
							rsCons.Set_Fields("MeterKey", lngMeterKey);
							rsCons.Set_Fields("MeterNumber", rsMeterInfo.Get_Fields_Int32("MeterNumber"));
							rsCons.Set_Fields("Begin", FCConvert.ToString(Conversion.Val(vsMeters.TextMatrix(vsMeters.Row, lngLastReadingCol))));
							rsCons.Set_Fields("End", FCConvert.ToString(Conversion.Val(txtOldMeterReading.Text)));
							rsCons.Set_Fields("Consumption", lngConsumption);
							rsCons.Set_Fields("NegativeMeter", rsMeterInfo.Get_Fields_Boolean("NegativeConsumption"));
							rsCons.Set_Fields("BillDate", DateTime.Today);
							rsCons.Update();
							// MAL@20080111: Add previous information to history table
							// Call Reference: 116727
							rsPrevious.OpenRecordset("SELECT * FROM tblPreviousMeter", modExtraModules.strUTDatabase);
							rsPrevious.AddNew();
							rsPrevious.Set_Fields("AccountKey", rsMeterInfo.Get_Fields_Int32("AccountKey"));
							rsPrevious.Set_Fields("PreviousMeterKey", lngMeterKey);
							rsPrevious.Set_Fields("SerialNumber", rsMeterInfo.Get_Fields_String("SerialNumber"));
							rsPrevious.Set_Fields("Size", rsMeterInfo.Get_Fields_Int32("Size"));
							rsPrevious.Set_Fields("Digits", rsMeterInfo.Get_Fields_Int32("Digits"));
							rsPrevious.Set_Fields("Multiplier", rsMeterInfo.Get_Fields_Int32("Multiplier"));
							rsPrevious.Set_Fields("Remote", rsMeterInfo.Get_Fields_String("Remote"));
							rsPrevious.Set_Fields("SetDate", rsMeterInfo.Get_Fields_DateTime("SetDate"));
							rsPrevious.Set_Fields("LastReading", Strings.Trim(txtOldMeterReading.Text));
							rsPrevious.Set_Fields("ChangeOutReason", Strings.Trim(txtReason.Text));
							rsPrevious.Update();
							rsMeterInfo.Edit();
							// rsMeterInfo.Fields("ConsumptionOfChangedOutMeter") = CalculateMeterConsumption(rsMeterInfo.Fields("CurrentReading"), Val(txtOldMeterReading.Text), rsMeterInfo.Fields("Digits"))
							//rsMeterInfo.Set_Fields("ConsumptionOfChangedOutMeter", modProcessTWUTXX41File.CalculateMeterConsumption_26(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(vsMeters.TextMatrix(vsMeters.Row, lngLastReadingCol)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(txtOldMeterReading.Text))), rsMeterInfo.Get_Fields("Digits")));
							// FC:FINAL:VGE - #i975 Converting to Int16/short for parameter matching.
							rsMeterInfo.Set_Fields("ConsumptionOfChangedOutMeter", modProcessTWUTXX41File.CalculateMeterConsumption_26(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(vsMeters.TextMatrix(vsMeters.Row, lngLastReadingCol)))), FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(txtOldMeterReading.Text))), FCConvert.ToInt16(rsMeterInfo.Get_Fields_Int32("Digits"))));
							rsMeterInfo.Set_Fields("ReplacementConsumption", rsMeterInfo.Get_Fields_Int32("ConsumptionOfChangedOutMeter"));
							rsMeterInfo.Set_Fields("ReplacementDate", DateTime.Today);
							rsMeterInfo.Set_Fields("SetDate", DateTime.Today);
							rsMeterInfo.Set_Fields("PreviousReading", FCConvert.ToString(Conversion.Val(txtNewMeterReading.Text)));
							rsMeterInfo.Set_Fields("CurrentReading", -1);
							// kk10222014    Start new meter at -1 (No Read)   '   = 0
							rsMeterInfo.Set_Fields("UseMeterChangeOut", true);
							rsMeterInfo.Set_Fields("Remote", Strings.Trim(txtRemoteNumber.Text));
							rsMeterInfo.Set_Fields("SerialNumber", Strings.Trim(txtSerialNumber.Text));
							rsMeterInfo.Set_Fields("Size", FCConvert.ToString(Conversion.Val(Strings.Left(cboSize.Text, 2))));
							rsMeterInfo.Set_Fields("Digits", FCConvert.ToString(Conversion.Val(cboDgits.Text)));
							rsMeterInfo.Set_Fields("SetDate", DateAndTime.DateValue(txtSetDate.Text));
							if (Strings.Left(cboBackflow.Text, 1) == "Y")
							{
								rsMeterInfo.Set_Fields("Backflow", true);
							}
							else
							{
								rsMeterInfo.Set_Fields("Backflow", false);
							}
							modGlobalFunctions.AddCYAEntry_728("UT", "Meter Changed Out", "Acct Key : " + rsMeterInfo.Get_Fields_Int32("AccountKey"), "Meter Key : " + rsMeterInfo.Get_Fields_Int32("MeterKey"), "Previous : " + rsMeterInfo.Get_Fields_Int32("PreviousReading"), "Current : " + rsMeterInfo.Get_Fields_Int32("CurrentReading") + "  Replacement Consumption : " + rsMeterInfo.Get_Fields_Int32("ConsumptionOfChangedOutMeter"));
							rsMeterInfo.Update();
							MessageBox.Show("Meter changed out successfully", "Save Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("Meter not found in file", "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							SaveInfo = false;
						}
					}
					else
					{
						MessageBox.Show("Meter not changed out.", "Canceled", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						SaveInfo = false;
					}
				}
				else
				{
					MessageBox.Show("Meter not changed out.  Error finding meter.", "Canceled", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					SaveInfo = false;
				}
				return SaveInfo;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Meter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private void ClearInformation()
		{
			txtOldMeterReading.Text = "";
			txtNewMeterReading.Text = "";
			txtRemoteNumber.Text = "";
			txtSerialNumber.Text = "";
			txtReason.Text = "";
		}

		private void FormatGrid()
		{
			vsMeters.Cols = 8;
			vsMeters.ColHidden(KeyCol, true);
			vsMeters.ColHidden(DigitsCol, true);
			// .ColHidden(lngLastReadingCol) = True
			vsMeters.TextMatrix(0, MeterNumberCol, "Meter #");
			vsMeters.TextMatrix(0, BookCol, "Book");
			vsMeters.TextMatrix(0, SequenceCol, "Sequence");
			vsMeters.TextMatrix(0, SizeCol, "Size");
			vsMeters.TextMatrix(0, SerialNumberCol, "Serial #");
		}

		private void vsAccountSearch_DblClick(object sender, System.EventArgs e)
		{
			// selecting an account to use
			int lngRW;
			int lngAccount;
			int lngIndex;
			lngRW = vsAccountSearch.MouseRow;
			//Application.DoEvents();
			lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(lngRW, 0))));
			lngIndex = GetListIndex(lngAccount);
			ReturnFromSearch(ref lngIndex);
		}

		private void SearchForAccounts()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsSearch = new clsDRWrapper();
				string strSQL = "";
				string strWhere = "";
				string strName = "";
				string strAccount = "";
				int lngIndex = 0;
				int lngMousePos;
				FormatSearchGrid();
				if (Strings.Trim(txtSearchName.Text) != "")
				{
					strName = "Name > '" + Strings.Trim(txtSearchName.Text) + "     ' AND Name < '" + Strings.Trim(txtSearchName.Text) + "ZZZZZ'";
				}
				else
				{
					strName = "";
				}
				if (Strings.Trim(txtSearchAccount.Text) != "")
				{
					strAccount = "AccountNumber LIKE '%" + Strings.Trim(txtSearchAccount.Text) + "%'";
				}
				else
				{
					strAccount = "";
				}
				if (strName != "")
				{
					strWhere = strName;
				}
				if (strAccount != "")
				{
					if (strWhere != "")
					{
						strWhere += " AND " + strAccount;
					}
					else
					{
						strWhere = strAccount;
					}
				}
				// rsSearch.OpenRecordset "SELECT * FROM Master WHERE Name > '" & Trim(txtSearchName.Text) & "     ' AND Name < '" & Trim(txtSearchName.Text) & "ZZZZZ' ORDER BY AccountNumber"
				// rsSearch.OpenRecordset "SELECT * FROM Master WHERE " & strWhere & " ORDER BY AccountNumber"
				rsSearch.OpenRecordset("SELECT * FROM (SELECT Master.ID, AccountNumber, pBill.FullNameLF AS Name FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID) AS qTmp WHERE " + strWhere + " ORDER BY AccountNumber");
				if (rsSearch.EndOfFile())
				{
					// no accounts
					MessageBox.Show("No accounts match the search criteria.", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (rsSearch.RecordCount() == 1)
					{
						// Automatically populate with a single find
						lngIndex = GetListIndex(rsSearch.Get_Fields_Int32("ID"));
						ReturnFromSearch(ref lngIndex);
					}
					else
					{
						vsAccountSearch.Rows = 1;
						while (!rsSearch.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							vsAccountSearch.AddItem(rsSearch.Get_Fields_Int32("ID") + "\t" + rsSearch.Get_Fields("AccountNumber") + "\t" + rsSearch.Get_Fields_String("Name"));
							rsSearch.MoveNext();
						}
						vsAccountSearch.Visible = true;
						// resize the grid
						SetSearchGridHeight();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Searching For Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int MakeDWord(ref short LoWord, ref short HiWord)
		{
			int MakeDWord = 0;
			// MAL@20070921
			MakeDWord = (HiWord * 0x10000) | (LoWord & 0xFFFF);
			return MakeDWord;
		}

		private void FormatSearchGrid()
		{
			vsAccountSearch.Cols = 3;
			vsAccountSearch.ColWidth(0, 0);
			// Account Key
			vsAccountSearch.ColWidth(1, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.12));
			// account number
			vsAccountSearch.ColWidth(2, FCConvert.ToInt32(vsAccountSearch.Width * 0.5));
			// name
			vsAccountSearch.ExtendLastCol = true;
			vsAccountSearch.TextMatrix(0, 1, "Account");
			vsAccountSearch.TextMatrix(0, 2, "Name");
			vsAccountSearch.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSort;
		}

		private void SetSearchGridHeight()
		{
			if ((vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70 > fraAccountSearch.Height * 0.7)
			{
				// too many rows to be shown
				vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				vsAccountSearch.Height = FCConvert.ToInt32(fraAccountSearch.Height * 0.7);
			}
			else
			{
				vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				vsAccountSearch.Height = (vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70;
			}
		}

		private void txtSearchName_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						SearchForAccounts();
						break;
					}
			}
			//end switch
		}

		private void ReturnFromSearch(ref int lngIndex)
		{
			fraAccountSearch.Visible = false;
			Frame1.Visible = true;
			mnuProcessSave.Enabled = true;
			cmdFileNameSearch.Enabled = true;
			if (lngIndex == -1)
			{
				// No Account Selected
				cboAccounts.Focus();
			}
			else
			{
				cboAccounts.SelectedIndex = lngIndex;
			}
		}

		private int GetListIndex(int lngKey)
		{
			int GetListIndex = 0;
			// MAL@20070907: Gets List Index Based on Account Key
			FCRecordset rsTemp = null;
			int lngResult = 0;
			int intCount;
			int i;
			intCount = cboAccounts.Items.Count;
			for (i = 0; i <= intCount - 1; i++)
			{
				if (cboAccounts.ItemData(i) == lngKey)
				{
					lngResult = i;
					break;
				}
			}
			// i
			GetListIndex = lngResult;
			return GetListIndex;
		}

		private void ResetForm()
		{
			// kk02042015 trouts-112  Keep the change out screen open, reset to initial state
			lngMeterKey = 0;
			ClearInformation();
			vsMeters.Rows = 1;
			fraInformation.Visible = false;
			Frame1.Visible = false;
			fraAccountSearch.Visible = false;
			mnuProcessSave.Enabled = true;
			cmdFileNameSearch.Enabled = true;
			boolReset = true;
			cboAccounts.SelectedIndex = -1;
			cboAccounts.Focus();
			boolReset = false;
		}
	}
}
