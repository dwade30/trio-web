﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTPmtActivityReport.
	/// </summary>
	public partial class frmUTPmtActivityReport : BaseForm
	{
		public frmUTPmtActivityReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTPmtActivityReport InstancePtr
		{
			get
			{
				return (frmUTPmtActivityReport)Sys.GetInstance(typeof(frmUTPmtActivityReport));
			}
		}

		protected frmUTPmtActivityReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Kevin Kelly             *
		// DATE           :               10/21/2014              *
		// *
		// MODIFIED BY    :               Kevin Kelly             *
		// LAST UPDATED   :               09/21/2016              *
		// ********************************************************
		int intCounter;
		int intStart;
		int intEnd;
		int intID;
		string strTemp = "";
		bool boolSaveReport;
		string strBillChoice = "";
		string strTownChoice = "";
		bool boolLoaded;
		string strGridToolTipText = "";
		public int lngRowHeader;
		public int lngRowWS;
		public int lngRowAccount;
		public int lngRowShowLien;
		public int lngRowDateRange;
		public int lngRowRef;
		public int lngRowName;
		public int lngRowBill;
		public int lngRowBook;
		public int lngRowTownCode;

		private void chkEpmtOnly_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkEpmtOnly.CheckState == Wisej.Web.CheckState.Checked)
			{
				vsWhere.TextMatrix(lngRowRef, 1, "EPYMT");
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowRef, 1, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			}
			else
			{
				vsWhere.TextMatrix(lngRowRef, 1, "");
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowRef, 1, Information.RGB(255, 255, 255));
			}
		}

		private void cmdClear_Click(object sender, System.EventArgs e)
		{
			// CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
			int intCounter;
			for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
			{
				vsWhere.TextMatrix(intCounter, 1, string.Empty);
				vsWhere.TextMatrix(intCounter, 2, string.Empty);
				vsWhere.TextMatrix(intCounter, 3, string.Empty);
			}
			vsWhere.EditText = "";
		}

		public void cmdClear_Click()
		{
			cmdClear_Click(cmdClear, new System.EventArgs());
		}

		private void cmdExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void cmdExit_Click()
		{
			//cmdExit_Click(cmdExit, new System.EventArgs());
		}

		private void cmdPrint_Click(object sender, System.EventArgs e)
		{
			// THIS ROUTINE WORKS TO PRINT OUT THE REPORT
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				rptUTPmtActivity oRpt = null;
				string strCodes;
				string strSort;
				bool boolAllPmtTypes;
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				// set focus to another object in order to update the last edit
				vsWhere.Select(0, 0);
				// If Not ValidateWhereGrid Then
				// Exit Sub
				// End If
				// 
				// SetExtraFields
				// 
				// SHOW THE REPORT
				// If chkHardCode.Value = vbChecked Then
				// If cmbHardCode.ListIndex > -1 Then
				// ShowHardCodedReport cmbHardCode.ItemData(cmbHardCode.ListIndex)
				// Else
				// MsgBox "Please select a report to show.", vbInformation, "No Selection"
				// cmbHardCode.SetFocus
				// End If
				// Else
				// Make sure any old report is unloaded and start a new one
				frmReportViewer.InstancePtr.Unload();
				oRpt = new rptUTPmtActivity();
				// "L" - Lien, "R" - Regular, "" - Both
				if (vsWhere.TextMatrix(lngRowShowLien, 1) == "Regular")
				{
					oRpt.BillType = "R";
				}
				else if (vsWhere.TextMatrix(lngRowShowLien, 1) == "Lien")
				{
					oRpt.BillType = "L";
				}
				else
				{
					oRpt.BillType = "";
				}
				// "S" - Sewer, "W" - Water, "B" or "" - Both
				if (vsWhere.TextMatrix(lngRowWS, 1) == "Water")
				{
					oRpt.Service = "W";
				}
				else if (vsWhere.TextMatrix(lngRowWS, 1) == "Sewer")
				{
					oRpt.Service = "S";
				}
				else
				{
					oRpt.Service = "";
				}
				// Start Date - End Date
				if (vsWhere.TextMatrix(lngRowDateRange, 1) != "")
				{
					if (Information.IsDate(vsWhere.TextMatrix(lngRowDateRange, 1)))
					{
						oRpt.StartDate = FCConvert.ToDateTime(vsWhere.TextMatrix(lngRowDateRange, 1));
					}
				}
				if (vsWhere.TextMatrix(lngRowDateRange, 2) != "")
				{
					if (Information.IsDate(vsWhere.TextMatrix(lngRowDateRange, 2)))
					{
						oRpt.EndDate = FCConvert.ToDateTime(vsWhere.TextMatrix(lngRowDateRange, 2));
					}
				}
				// Start Account - End Account
				if (vsWhere.TextMatrix(lngRowAccount, 1) != "")
				{
					if (Information.IsNumeric(vsWhere.TextMatrix(lngRowAccount, 1)))
					{
						oRpt.StartAccount = FCConvert.ToInt32(vsWhere.TextMatrix(lngRowAccount, 1));
					}
				}
				if (vsWhere.TextMatrix(lngRowAccount, 2) != "")
				{
					if (Information.IsNumeric(vsWhere.TextMatrix(lngRowAccount, 2)))
					{
						oRpt.EndAccount = FCConvert.ToInt32(vsWhere.TextMatrix(lngRowAccount, 2));
					}
				}
				// Reference Filter
				if (Strings.Trim(vsWhere.TextMatrix(lngRowRef, 1)) != "")
				{
					oRpt.Reference = Strings.Trim(vsWhere.TextMatrix(lngRowRef, 1));
				}
				else
				{
					oRpt.Reference = "";
					// Invoice Cloud - Reference = "EPYMT"
				}
				// Start Bill - End Bill
				if (vsWhere.TextMatrix(lngRowBill, 1) != "")
				{
					if (Information.IsNumeric(vsWhere.TextMatrix(lngRowBill, 1)))
					{
						oRpt.StartBill = FCConvert.ToInt32(vsWhere.TextMatrix(lngRowBill, 1));
					}
				}
				if (vsWhere.TextMatrix(lngRowBill, 2) != "")
				{
					if (Information.IsNumeric(vsWhere.TextMatrix(lngRowBill, 2)))
					{
						oRpt.EndBill = FCConvert.ToInt32(vsWhere.TextMatrix(lngRowBill, 2));
					}
				}
				// Town Code
				// oRpt.TownCode = 0
				// If gboolMultipleTowns Then
				// If Trim(vsWhere.TextMatrix(lngRowTownCode, 1)) <> "" Then
				// oRpt.TownCode = Left(vsWhere.TextMatrix(lngRowTownCode, 1), 2)
				// End If
				// End If
				// Sort options
				strSort = "";
				for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
				{
					if (lstSort.Selected(intCounter))
					{
						// it is checked
						if (strSort != "")
							strSort += ", ";
						if (Strings.Left(lstSort.Items[intCounter].Text, 2) == "Ac")
						{
							strSort += "ActualAccountNumber";
						}
						else if (Strings.Left(lstSort.Items[intCounter].Text, 2) == "Tr")
						{
							strSort += "RecordedTransactionDate";
							// Case "Na"
							// If intShowOwnerType = 0 Or intShowOwnerType = 2 Or boolAgedList Then
							// strSort = strSort & "Name"
							// Else
							// strSort = strSort & "BName"
							// End If
							// Case "Bi"
							// If Not boolAgedList Then
							// strSort = strSort & "BillingRateKey"
							// Else
							// If strSort <> " " Then strSort = Left(strSort, Len(strSort) - 2)
							// End If
							// Case "Bo"
							// If Not boolAgedList Then
							// strSort = strSort & "UTBook"
							// Else
							// If strSort <> " " Then strSort = Left(strSort, Len(strSort) - 2)
							// End If
							// Case "Ma"            'kgk trout-743  Add Map/Lot to sort
							// If Not boolAgedList Then
							// strSort = strSort & "Bill.MapLot"  'kk 06052013 - This is backwards   "Master.MapLot"
							// Else
							// strSort = strSort & "Master.MapLot"  '"Bill.MapLot"
							// End If
						}
					}
				}
				oRpt.SortOrder = strSort;
				boolAllPmtTypes = true;
				strCodes = "";
				if (chkPayment.CheckState == Wisej.Web.CheckState.Checked)
				{
					strCodes = "P";
				}
				else
				{
					boolAllPmtTypes = false;
				}
				if (chkPrePay.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strCodes == "")
					{
						strCodes = "Y";
					}
					else
					{
						strCodes += ",Y";
					}
				}
				else
				{
					boolAllPmtTypes = false;
				}
				if (chkCorrection.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strCodes == "")
					{
						strCodes = "C";
					}
					else
					{
						strCodes += ",C";
					}
				}
				else
				{
					boolAllPmtTypes = false;
				}
				if (chkAbatement.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strCodes == "")
					{
						strCodes = "A";
					}
					else
					{
						strCodes += ",A";
					}
				}
				else
				{
					boolAllPmtTypes = false;
				}
				if (chkDiscount.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strCodes == "")
					{
						strCodes = "D";
					}
					else
					{
						strCodes += ",D";
					}
				}
				else
				{
					boolAllPmtTypes = false;
				}
				if (chkDemand.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strCodes == "")
					{
						strCodes = "3";
					}
					else
					{
						strCodes += ",3";
					}
				}
				else
				{
					boolAllPmtTypes = false;
				}
				if (chkMaturityFee.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strCodes == "")
					{
						strCodes = "L";
					}
					else
					{
						strCodes += ",L";
					}
				}
				else
				{
					boolAllPmtTypes = false;
				}
				if (chkInterest.CheckState == Wisej.Web.CheckState.Checked)
				{
					if (strCodes == "")
					{
						strCodes = "I";
					}
					else
					{
						strCodes += ",I";
					}
				}
				else
				{
					boolAllPmtTypes = false;
				}
				if (boolAllPmtTypes)
				{
					oRpt.PaymentTypes = "";
				}
				else
				{
					oRpt.PaymentTypes = strCodes;
				}
				frmReportViewer.InstancePtr.Init(oRpt);
				// , , , , , , , , , , , , , True
				return;
			}
			catch (Exception ex)
			{
				
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				MessageBox.Show("ERROR #:" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Print Status Lists ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public void cmdPrint_Click()
		{
			cmdPrint_Click(cmdPrint, new System.EventArgs());
		}

		private void frmUTPmtActivityReport_Activated(object sender, System.EventArgs e)
		{
			if (boolLoaded)
			{
			}
			else
			{
				// lngMax = 7
				EnableFrames_2(true);
				//Application.DoEvents();
				Format_WhereGrid_2(true);
				// ShowAutomaticFields
				// SetupStatusListCombos True
				FindAllBills();
				// If gboolMultipleTowns Then
				// FindAllTowns
				// End If
				Fill_Lists();
				boolLoaded = true;
			}
		}

		private void frmUTPmtActivityReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Unload();
						break;
					}
			}
			//end switch
		}

		private void frmUTPmtActivityReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTPmtActivityReport properties;
			//frmUTPmtActivityReport.ScaleWidth	= 9045;
			//frmUTPmtActivityReport.ScaleHeight	= 7440;
			//frmUTPmtActivityReport.LinkTopic	= "Form1";
			//End Unmaped Properties
			// LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			this.Text = "Payment Activity Report";
			lngRowWS = 0;
			lngRowAccount = 1;
			lngRowShowLien = 2;
			lngRowDateRange = 3;
			lngRowBill = 4;
			lngRowRef = 5;
			// lngRowName = 2
			// lngRowBook = 4
			// lngRowTownCode = 7
			// CheckReportTable
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmUTPmtActivityReport_Resize(object sender, System.EventArgs e)
		{
			Format_WhereGrid_2(false);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void fraSort_DoubleClick(object sender, System.EventArgs e)
		{
			int intCounter;
			for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
			{
				lstSort.SetSelected(intCounter, true);
			}
		}

		private void fraWhere_DoubleClick(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}
		//private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
		private void lstSort_MouseUp(object sender, Wisej.Web.ItemClickEventArgs e)
		{
			MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
			int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
			//float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
			//float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
			// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
			// ITEMS THAT ARE TO BE SWAPED
			// 
			// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
			// WILL BE DISPLAYED ON THE REPORT ITSELF
			bool boolSecondSelected = false;
			// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
			if (intStart != lstSort.SelectedIndex)
			{
				// SAVE THE CAPTION AND ID FOR THE NEW ITEM
				strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
				intID = lstSort.ItemData(lstSort.SelectedIndex);
				boolSecondSelected = lstSort.Selected(lstSort.SelectedIndex);
				// CHANGE THE NEW ITEM
				//FC:FINAL:SBE - #i925 - assign text (default property) from items
				lstSort.Items[lstSort.ListIndex].Text = lstSort.Items[intStart].Text;
				lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
				// SAVE THE OLD ITEM
				lstSort.Items[intStart].Text = strTemp;
				lstSort.ItemData(intStart, intID);
				// SET BOTH ITEMS TO BE SELECTED
				lstSort.SetSelected(lstSort.ListIndex, true);
				lstSort.SetSelected(intStart, boolSecondSelected);
			}
		}

		private void mnuClear_Click(object sender, System.EventArgs e)
		{
			cmdClear_Click();
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			cmdExit_Click();
		}

		private void mnuPrint_Click(object sender, System.EventArgs e)
		{
			cmdPrint_Click();
		}

		private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
			// GIRD THEN WE NEED TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
			// 
			// THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
			// AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
			// 
			// THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
			// vsWhere.TextMatrix(Row, 2) = vsWhere.ComboData
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
			}
			if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
			{
				vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
			}
		}

		private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsWhere.EditMask = string.Empty;
			vsWhere.ComboList = string.Empty;
			if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
			{
				if (vsWhere.Row == lngRowWS)
				{
					if (vsWhere.Col == 1)
					{
						if (modUTStatusPayments.Statics.TownService == "B")
						{
							vsWhere.ComboList = "#0;Water|#1;Sewer";
						}
						else if (modUTStatusPayments.Statics.TownService == "W")
						{
							vsWhere.ComboList = "#0;Water";
						}
						else if (modUTStatusPayments.Statics.TownService == "S")
						{
							vsWhere.ComboList = "#1;Sewer";
						}
					}
				}
				else if (vsWhere.Row == lngRowShowLien)
				{
					if (vsWhere.Col == 1)
					{
						vsWhere.ComboList = "#0;Regular|#1;Lien";
					}
				}
				else if (vsWhere.Row == lngRowAccount || vsWhere.Row == lngRowRef)
				{
					// lngRowName, lngRowBill, lngRowBook,
					if (vsWhere.Col == 1 || vsWhere.Col == 2)
					{
						vsWhere.ComboList = "";
					}
				}
				else if (vsWhere.Row == lngRowBill)
				{
					if (vsWhere.Col == 1 || vsWhere.Col == 2)
					{
						vsWhere.ComboList = strBillChoice;
					}
				}
				else if (vsWhere.Row == lngRowDateRange)
				{
					vsWhere.EditMask = "##/##/####";
					// Case lngRowTownCode
					// vsWhere.ComboList = strTownChoice
				}
				else
				{
				}
			}
		}

		private void vsWhere_ChangeEdit(object sender, System.EventArgs e)
		{
			// ShowAutomaticFields
		}

		private void vsWhere_KeyDownEvent(object sender, KeyEventArgs e)
		{
			if (vsWhere.Col > 0)
			{
				switch (e.KeyCode)
				{
					case Keys.Delete:
						{
							vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
							break;
						}
				}
				//end switch
			}
		}

		private void vsWhere_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
						break;
					}
				case Keys.Escape:
					{
						vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
		}

		private void vsWhere_Leave(object sender, System.EventArgs e)
		{
			vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			vsWhere.Select(0, 1);
		}

		private void vsWhere_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = e.RowIndex;
			lngMC = e.ColumnIndex;
			//FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
			if (lngMR > -1 && lngMC > -1)
			{
				DataGridViewCell cell = vsWhere[lngMC, lngMR];
				cell.ToolTipText = "";
				if (lngMC == 0)
				{
					if (lngMR == lngRowWS)
					{
						cell.ToolTipText = "This will show the Water or Sewer bills.";
					}
					else if (lngMR == lngRowAccount)
					{
						cell.ToolTipText = "Show a range of accounts.";
					}
					else if (lngMR == lngRowName)
					{
						cell.ToolTipText = "Show a range of names.";
					}
					else if (lngMR == lngRowBill)
					{
						cell.ToolTipText = "Show a range by bill number.";
					}
					else if (lngMR == lngRowBook)
					{
						cell.ToolTipText = "Show a range by book number.";
					}
					else if (lngMR == lngRowShowLien)
					{
						cell.ToolTipText = "Show regular or liened accounts.";
					}
					else if (lngMR == lngRowTownCode)
					{
						cell.ToolTipText = "Show accounts that are associated to this town.";
					}
					else if (lngMR == lngRowDateRange)
					{
						cell.ToolTipText = "Show payments recorded within this range of dates.";
						// Case lngRowTownCode
						// vsWhere.ToolTipText = "Residence code."
					}
				}
			}
		}

		private void vsWhere_RowColChange(object sender, System.EventArgs e)
		{
			// SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
			// IF COMBO...ADD THE LIST OF OPTIONS
			int lngRow;
			int lngCol;
			lngRow = vsWhere.Row;
			lngCol = vsWhere.Col;
			if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngCol) != modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
			{
				if (lngRow == vsWhere.Rows - 1)
				{
					vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
				}
				else
				{
					vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				}
				// If strComboList(lngRow, 0) <> vbNullString Then
				vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				// vsWhere.ComboList = strComboList(lngRow, 0)
				// End If
			}
			else
			{
                vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
                vsWhere.Editable = FCGrid.EditableSettings.flexEDNone;
				//Support.SendKeys("{Tab}", false);
			}
		}

		private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - issue #1012: save and use correct indexes of the cell
			int row = vsWhere.GetFlexRowIndex(e.RowIndex);
			int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (row == lngRowDateRange)
				{
					if (Strings.Trim(vsWhere.EditText) == "/  /")
					{
						vsWhere.EditMask = string.Empty;
						vsWhere.EditText = string.Empty;
						vsWhere.TextMatrix(row, col, string.Empty);
						vsWhere.Refresh();
						return;
					}
				}
				// ShowAutomaticFields
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validate Edit", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void EnableFrames_2(bool boolEnable)
		{
			EnableFrames(ref boolEnable);
		}

		private void EnableFrames(ref bool boolEnable)
		{
			fraSort.Enabled = boolEnable;
			fraWhere.Enabled = boolEnable;
			vsWhere.Enabled = boolEnable;
			lstSort.Enabled = boolEnable;
		}
		// Private Sub Set_Note_Text()
		// Dim strTemp             As String
		//
		// strTemp = "1. The Fields To Sort By section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." & vbCrLf & vbCrLf
		// strTemp = strTemp & "2. Set any criteria needed in the Select Search Criteria list." & vbCrLf & vbCrLf
		// strTemp = strTemp & "Other Notes:" & vbCrLf & "Some fields will be printed automatically." & vbCrLf & vbCrLf
		// strTemp = strTemp & vbCrLf & "If you choose a default report, only the account number and the tax year criteria will affect the report.  Any changes in the other fields will not be used in the creation of the report." & vbCrLf
		// txtNotes.Text = strTemp
		// End Sub
		private void FindAllBills()
		{
			// this will fill the variable strBillChoice with all of the possible bills and an All Bills Option
			clsDRWrapper rsBills = new clsDRWrapper();
			int intNum = 0;
			rsBills.OpenRecordset("SELECT DISTINCT ID FROM RateKeys WHERE RateType = 'R' ORDER BY ID DESC", modExtraModules.strUTDatabase);
			if (rsBills.EndOfFile() != true && rsBills.BeginningOfFile() != true)
			{
				strBillChoice = "";
				intNum = 0;
				while (!rsBills.EndOfFile())
				{
					strBillChoice += "#" + FCConvert.ToString(intNum) + ";" + rsBills.Get_Fields_Int32("ID") + "|";
					intNum += 1;
					rsBills.MoveNext();
				}
				// take off the last pipe character '|'
				strBillChoice = Strings.Left(strBillChoice, strBillChoice.Length - 1);
			}
		}
		// Private Sub CheckReportTable()
		// Dim rsCreateTable As New clsDataConnection
		//
		// With rsCreateTable
		// CREATE A NEW TABLE IF IT DOESN'T EXIST
		// If .CreateNewDatabaseTable("SavedStatusReports", DEFAULTDATABASE) Then
		// CREATE THE NEW FIELDS
		// .CreateTableField "ID", dbLong
		// .CreateTableField "ReportName", dbText
		// .CreateTableField "Type", dbText
		// .CreateTableField "SQL", dbMemo
		// .CreateTableField "LastUpdated", dbDate
		//
		// .CreateTableField "WhereSelection", dbText
		// .CreateTableField "SortSelection", dbText
		// .CreateTableField "FieldConstraint0", dbText
		// .CreateTableField "FieldConstraint1", dbText
		// .CreateTableField "FieldConstraint2", dbText
		// .CreateTableField "FieldConstraint3", dbText
		// .CreateTableField "FieldConstraint4", dbText
		// .CreateTableField "FieldConstraint5", dbText
		// .CreateTableField "FieldConstraint6", dbText
		// .CreateTableField "FieldConstraint7", dbText
		// .CreateTableField "FieldConstraint8", dbText
		// .CreateTableField "FieldConstraint9", dbText
		//
		// SET THE PROPERTIES OF THE NEW FIELDS
		// .SetFieldAttribute "ID", dbAutoIncrField
		// .SetFieldDefaultValue "LastUpdated", "NOW()"
		// .SetFieldAllowZeroLength "ReportName", True
		// .SetFieldAllowZeroLength "Type", True
		// .SetFieldAllowZeroLength "SQL", True
		//
		// .SetFieldAllowZeroLength "WhereSelection", True
		// .SetFieldAllowZeroLength "SortSelection", True
		// .SetFieldAllowZeroLength "FieldConstraint0", True
		// .SetFieldAllowZeroLength "FieldConstraint1", True
		// .SetFieldAllowZeroLength "FieldConstraint2", True
		// .SetFieldAllowZeroLength "FieldConstraint3", True
		// .SetFieldAllowZeroLength "FieldConstraint4", True
		// .SetFieldAllowZeroLength "FieldConstraint5", True
		// .SetFieldAllowZeroLength "FieldConstraint6", True
		// .SetFieldAllowZeroLength "FieldConstraint7", True
		// .SetFieldAllowZeroLength "FieldConstraint8", True
		// .SetFieldAllowZeroLength "FieldConstraint9", True
		//
		// DO THE ACTUAL CREATION OF THE TABLE
		// .UpdateTableCreation
		// End If
		// End With
		//
		// With rsCreateTable
		// CREATE A NEW TABLE IF IT DOESN'T EXIST
		// If .CreateNewDatabaseTable("tblReportLayout", DEFAULTDATABASE) Then
		// CREATE THE NEW FIELDS
		// .CreateTableField "AutoID", dbLong
		// .CreateTableField "ReportID", dbInteger
		// .CreateTableField "RowID", dbInteger
		// .CreateTableField "ColumnID", dbInteger
		// .CreateTableField "FieldID", dbInteger
		// .CreateTableField "Width", dbInteger
		// .CreateTableField "DisplayText", dbText
		// .CreateTableField "LastUpdated", dbDate
		//
		// SET THE PROPERTIES OF THE NEW FIELDS
		// .SetFieldAttribute "AutoID", dbAutoIncrField
		// .SetFieldDefaultValue "LastUpdated", "NOW()"
		// .SetFieldAllowZeroLength "DisplayText", True
		//
		// DO THE ACTUAL CREATION OF THE TABLE
		// .UpdateTableCreation
		// End If
		// End With
		// End Sub
		private string GetWhereConstraint(ref short intRow)
		{
			string GetWhereConstraint = "";
			// this will go to the grid and get the constraint for that row and return it
			// if there is none, then it will return a nullstring
			int lngType;
			string strTemp;
			strTemp = vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 1) + "|||" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 2);
			GetWhereConstraint = strTemp;
			return GetWhereConstraint;
		}

		private bool ItemInSortList(ref short intCounter)
		{
			bool ItemInSortList = false;
			// this will return true if the item has already been added to the Sort List
			int intTemp;
			ItemInSortList = false;
			for (intTemp = 0; intTemp <= lstSort.Items.Count - 1; intTemp++)
			{
				if (lstSort.ItemData(intTemp) == intCounter)
				{
					ItemInSortList = true;
					break;
				}
			}
			return ItemInSortList;
		}

		public void LoadSortList()
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			lstSort.Clear();
			lstSort.AddItem("Account Number");
			lstSort.ItemData(lstSort.NewIndex, 0);
			lstSort.AddItem("Transaction Date");
			lstSort.ItemData(lstSort.NewIndex, 1);
		}
		// Private Sub FillHardCodeCombo()
		// this will fill the hard coded combo list with the list of reports that the user can choose
		// cmbHardCode.Clear
		// cmbHardCode.AddItem "Non Zero Balance on All Accounts"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 2
		// cmbHardCode.AddItem "Non Zero Balance on Non Lien Accounts"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 0
		// cmbHardCode.AddItem "Non Zero Balance on Lien Accounts"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 1
		// cmbHardCode.AddItem "Lien Breakdown"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 3
		// cmbHardCode.AddItem "Zero Balance Report"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 4
		// cmbHardCode.AddItem "Negative Balance Report"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 5
		// cmbHardCode.AddItem "Supplemental Outstanding Balance Report"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 6
		// cmbHardCode.AddItem "Supplemental Negative Balance Report"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 7
		// cmbHardCode.AddItem "Supplemental Zero Balance Report"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 8
		// cmbHardCode.AddItem "Outstanding Balance By Period"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 9
		// cmbHardCode.AddItem "Account Detail Report"
		// cmbHardCode.ItemData(cmbHardCode.NewIndex) = 10
		//
		// this will also fill the name options combo
		// cmbNameOption.Clear
		// cmbNameOption.AddItem "Show Current Owner"
		// cmbNameOption.ItemData(cmbNameOption.NewIndex) = 0
		// cmbNameOption.AddItem "Show Billed Owner"
		// cmbNameOption.ItemData(cmbNameOption.NewIndex) = 1
		// cmbNameOption.AddItem "Show Billed Owner C\O Current Owner"
		// cmbNameOption.ItemData(cmbNameOption.NewIndex) = 2
		// cmbNameOption.AddItem "Show Current Owner Bill= Billed Owner"
		// cmbNameOption.ItemData(cmbNameOption.NewIndex) = 3
		//
		// cmbNameOption.ListIndex = 0
		// End Sub
		//
		// Private Sub ShowHardCodedReport(intindex As Integer)
		// On Error GoTo ERROR_HANDLER
		// Select Case intindex
		// Case 0  'Non Zero Balance on Non Lien Accounts
		// frmReportViewer.Init rptUTOutstandingBalances
		// Case 1  'Non Zero Balance on Lien Accounts
		// frmReportViewer.Init rptUTOutstandingLienBalances
		// Case 2  'Non Zero Balance on Lien Accounts
		// frmReportViewer.Init rptUTOutstandingBalancesAll
		// intMasterReportType = 0
		// Case 3  'Lien Breakdown Report
		// frmReportViewer.Init arUTLienStatusReport
		// Case 4  'Zero Balance Report
		// intMasterReportType = 4
		// frmReportViewer.Init rptUTOutstandingBalancesAll
		// Case 5  'Negative Balance Report
		// intMasterReportType = 5
		// frmReportViewer.Init rptUTOutstandingBalancesAll
		// Case 6  'Supplemental Outstanding Balance Report
		// intMasterReportType = 6
		// frmReportViewer.Init rptUTOutstandingBalancesAll
		// Case 7  'Supplemental Negative Balance Report
		// intMasterReportType = 7
		// frmReportViewer.Init rptUTOutstandingBalancesAll
		// Case 8  'Supplemental Zero Balance Report
		// intMasterReportType = 8
		// frmReportViewer.Init rptUTOutstandingBalancesAll
		// Case 9  'Outstanding Balance By Period
		// frmRateRecChoice.intRateType = 50
		// frmRateRecChoice.Show
		// Case 10
		// intMasterReportType = 10
		// frmReportViewer.Init rptUTStatusListAccountDetail
		// Case Else
		// End Select
		// Exit Sub
		
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Showing Hard Coded Report"
		// End Sub
		// Private Sub ShowAutomaticFields()
		//
		// End Sub
		// Private Function ValidateWhereGrid() As Boolean
		// On Error GoTo ERROR_HANDLER
		// Dim strSwap                 As String
		//
		// lngRowWS
		// Combo box - don't need to validate
		//
		// lngRowAccount
		// If Val(vsWhere.TextMatrix(lngRowAccount, 1)) > 0 And Val(vsWhere.TextMatrix(lngRowAccount, 2)) > 0 Then
		// If Val(vsWhere.TextMatrix(lngRowAccount, 1)) > Val(vsWhere.TextMatrix(lngRowAccount, 2)) Then
		// strSwap = vsWhere.TextMatrix(0, 1)
		// vsWhere.TextMatrix(lngRowAccount, 1) = vsWhere.TextMatrix(lngRowAccount, 2)
		// vsWhere.TextMatrix(lngRowAccount, 2) = strSwap
		// End If
		// End If
		//
		// lngRowName
		//
		// lngRowBill
		// If Val(vsWhere.TextMatrix(lngRowBill, 1)) > 0 And Val(vsWhere.TextMatrix(lngRowBill, 2)) > 0 Then
		// If Val(vsWhere.TextMatrix(lngRowBill, 1)) > Val(vsWhere.TextMatrix(lngRowBill, 2)) Then
		// strSwap = vsWhere.TextMatrix(lngRowBill, 1)
		// vsWhere.TextMatrix(lngRowBill, 1) = vsWhere.TextMatrix(lngRowBill, 2)
		// vsWhere.TextMatrix(lngRowBill, 2) = strSwap
		// End If
		// End If
		//
		// lngRowShowLien
		// Combo box - don't need to validate
		//
		// Show Payments from Date range
		// gboolUTSLDateRange = False
		// If Trim(vsWhere.TextMatrix(lngRowDateRange, 1)) <> "" Then    'make sure that there is something in the field
		// If IsDate(vsWhere.TextMatrix(lngRowDateRange, 1)) Then    'make sure that it is a date
		// If Trim(vsWhere.TextMatrix(lngRowDateRange, 2)) <> "" Then
		// If IsDate(vsWhere.TextMatrix(lngRowDateRange, 2)) Then
		// Else
		// End If
		// Else
		// End If
		// End If
		// End If
		//
		// ValidateWhereGrid = True
		// Exit Function
		
		// ValidateWhereGrid = False
		// MsgBox "Error #" & Err.Number & " - " & Err.Description & ".", vbCritical, "Error Validating Where Grid"
		// End Function
		public void Format_WhereGrid_2(bool boolReset)
		{
			Format_WhereGrid(boolReset);
		}

		public void Format_WhereGrid(bool boolReset = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCounter;
				int lngWid = 0;
				if (boolReset)
				{
					vsWhere.Rows = 0;
				}
				vsWhere.Rows = 6;
				vsWhere.Cols = 4;
				lngWid = vsWhere.WidthOriginal;
				vsWhere.ColWidth(0, FCConvert.ToInt32(lngWid * 0.45));
				vsWhere.ColWidth(1, FCConvert.ToInt32(lngWid * 0.275));
				vsWhere.ColWidth(2, FCConvert.ToInt32(lngWid * 0.27));
				vsWhere.ColWidth(3, 0);
				// Water/Sewer/Both
				vsWhere.TextMatrix(lngRowWS, 0, "Water or Sewer");
				//FC:FINAL:MSH - issue #1012: replace incorrect BackColor
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowWS, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// Account Range
				vsWhere.TextMatrix(lngRowAccount, 0, "Account");
				// Bill Range
				vsWhere.TextMatrix(lngRowBill, 0, "Bill Number");
				// Bill Types (ie Regular, Lien, Both)
				vsWhere.TextMatrix(lngRowShowLien, 0, "Show Account Types");
				//FC:FINAL:MSH - issue #1012: replace incorrect BackColor
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowShowLien, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// Show Payment From
				vsWhere.TextMatrix(lngRowDateRange, 0, "Show Payments From");
				// Referenece filter
				vsWhere.TextMatrix(lngRowRef, 0, "Reference");
				//FC:FINAL:MSH - issue #1012: replace incorrect BackColor
				vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowRef, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				// Name Range
				// .TextMatrix(lngRowName, 0) = "Name"
				// Book Range
				// .TextMatrix(lngRowBook, 0) = "Book"
				// Town Code
				// .TextMatrix(lngRowTownCode, 0) = "Town Code"
				// .Height = (.rows * .RowHeight(0)) + 70
				vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				// set the defaults
				if (boolReset)
				{
					if (modUTStatusPayments.Statics.TownService == "W")
					{
						vsWhere.TextMatrix(lngRowWS, 1, "Water");
					}
					else if (modUTStatusPayments.Statics.TownService == "S")
					{
						vsWhere.TextMatrix(lngRowWS, 1, "Sewer");
					}
					else if (modUTStatusPayments.Statics.TownService == "B")
					{
						vsWhere.TextMatrix(lngRowWS, 1, "");
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Where Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Fill_Lists()
		{
			// this routine will fill the extra fields and the sort lists
			// With lstFields
			// .Clear
			// .AddItem "Location"
			// .AddItem "Map / Lot"
			// End With
			lstSort.Clear();
			lstSort.AddItem("Account Number");
			lstSort.AddItem("Transaction Date");
			// .AddItem "Name"
			// .AddItem "Bill Number"
			// .AddItem "Book Number"
			// .AddItem "Map / Lot"      'kgk trout-743  Add Map/Lot to sort
			lstSort.SetSelected(1, true);
			// automatically select Date
		}
	}
}
