﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arLienTransferReport.
	/// </summary>
	partial class arLienTransferReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arLienTransferReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPLInt = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCosts = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblDemand = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPLInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.sarLienEditMortHolders = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.fldCO = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDemand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalPrincipal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPLInt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCosts = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalDemand = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPLInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPLInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCO)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPLInt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldPrincipal,
				this.fldName,
				this.fldPLInt,
				this.fldCosts,
				this.fldTotal,
				this.sarLienEditMortHolders,
				this.fldCO,
				this.fldDemand,
				this.fldTax
			});
			this.Detail.Height = 0.3854167F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblTotals,
				this.fldTotalPrincipal,
				this.fldTotalPLInt,
				this.fldTotalCosts,
				this.fldTotalTotal,
				this.Line2,
				this.fldTotalDemand,
				this.fldTotalTax
			});
			this.ReportFooter.Height = 0.3125F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lblAccount,
				this.lblPrincipal,
				this.lblName,
				this.lblPLInt,
				this.lblCosts,
				this.lblTotal,
				this.lblReportType,
				this.Line1,
				this.lblDemand,
				this.lblTax
			});
			this.PageHeader.Height = 1F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3125F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Lien Transfer Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.9375F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.8125F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.125F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.8125F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.3125F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.8125F;
			this.lblAccount.Width = 0.625F;
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 2.9375F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPrincipal.Text = "Principal";
			this.lblPrincipal.Top = 0.8125F;
			this.lblPrincipal.Width = 1F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.9375F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.8125F;
			this.lblName.Width = 2F;
			// 
			// lblPLInt
			// 
			this.lblPLInt.Height = 0.1875F;
			this.lblPLInt.HyperLink = null;
			this.lblPLInt.Left = 4.75F;
			this.lblPLInt.Name = "lblPLInt";
			this.lblPLInt.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPLInt.Text = "Interest";
			this.lblPLInt.Top = 0.8125F;
			this.lblPLInt.Width = 0.8125F;
			// 
			// lblCosts
			// 
			this.lblCosts.Height = 0.1875F;
			this.lblCosts.HyperLink = null;
			this.lblCosts.Left = 5.5625F;
			this.lblCosts.Name = "lblCosts";
			this.lblCosts.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblCosts.Text = "Costs";
			this.lblCosts.Top = 0.8125F;
			this.lblCosts.Width = 0.625F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 6.875F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.8125F;
			this.lblTotal.Width = 1.0625F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.4375F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblReportType.Text = null;
			this.lblReportType.Top = 0.3125F;
			this.lblReportType.Width = 7.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 1F;
			this.Line1.Width = 7.625F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.625F;
			this.Line1.Y1 = 1F;
			this.Line1.Y2 = 1F;
			// 
			// lblDemand
			// 
			this.lblDemand.Height = 0.1875F;
			this.lblDemand.HyperLink = null;
			this.lblDemand.Left = 6.1875F;
			this.lblDemand.Name = "lblDemand";
			this.lblDemand.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDemand.Text = "Demand";
			this.lblDemand.Top = 0.8125F;
			this.lblDemand.Width = 0.6875F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 3.9375F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.8125F;
			this.lblTax.Width = 0.8125F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.9375F;
			// 
			// fldPrincipal
			// 
			this.fldPrincipal.Height = 0.1875F;
			this.fldPrincipal.Left = 2.9375F;
			this.fldPrincipal.Name = "fldPrincipal";
			this.fldPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPrincipal.Text = null;
			this.fldPrincipal.Top = 0F;
			this.fldPrincipal.Width = 1F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.9375F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 2F;
			// 
			// fldPLInt
			// 
			this.fldPLInt.Height = 0.1875F;
			this.fldPLInt.Left = 4.75F;
			this.fldPLInt.Name = "fldPLInt";
			this.fldPLInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldPLInt.Text = null;
			this.fldPLInt.Top = 0F;
			this.fldPLInt.Width = 0.8125F;
			// 
			// fldCosts
			// 
			this.fldCosts.Height = 0.1875F;
			this.fldCosts.Left = 5.5625F;
			this.fldCosts.Name = "fldCosts";
			this.fldCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldCosts.Text = null;
			this.fldCosts.Top = 0F;
			this.fldCosts.Width = 0.625F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 1.0625F;
			// 
			// sarLienEditMortHolders
			// 
			this.sarLienEditMortHolders.CloseBorder = false;
			this.sarLienEditMortHolders.Height = 0.125F;
			this.sarLienEditMortHolders.Left = 0.5625F;
			this.sarLienEditMortHolders.Name = "sarLienEditMortHolders";
			this.sarLienEditMortHolders.Report = null;
			this.sarLienEditMortHolders.Top = 0.1875F;
			this.sarLienEditMortHolders.Width = 7.3125F;
			// 
			// fldCO
			// 
			this.fldCO.Height = 0.1875F;
			this.fldCO.Left = 1.0625F;
			this.fldCO.Name = "fldCO";
			this.fldCO.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldCO.Text = null;
			this.fldCO.Top = 0.125F;
			this.fldCO.Width = 6.75F;
			// 
			// fldDemand
			// 
			this.fldDemand.Height = 0.1875F;
			this.fldDemand.Left = 6.1875F;
			this.fldDemand.Name = "fldDemand";
			this.fldDemand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldDemand.Text = null;
			this.fldDemand.Top = 0F;
			this.fldDemand.Width = 0.6875F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 3.9375F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 0.8125F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 0.625F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0F;
			this.lblTotals.Width = 2.3125F;
			// 
			// fldTotalPrincipal
			// 
			this.fldTotalPrincipal.Height = 0.1875F;
			this.fldTotalPrincipal.Left = 2.9375F;
			this.fldTotalPrincipal.Name = "fldTotalPrincipal";
			this.fldTotalPrincipal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPrincipal.Text = "0.00";
			this.fldTotalPrincipal.Top = 0F;
			this.fldTotalPrincipal.Width = 1F;
			// 
			// fldTotalPLInt
			// 
			this.fldTotalPLInt.Height = 0.1875F;
			this.fldTotalPLInt.Left = 4.75F;
			this.fldTotalPLInt.Name = "fldTotalPLInt";
			this.fldTotalPLInt.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPLInt.Text = "0.00";
			this.fldTotalPLInt.Top = 0F;
			this.fldTotalPLInt.Width = 0.8125F;
			// 
			// fldTotalCosts
			// 
			this.fldTotalCosts.Height = 0.1875F;
			this.fldTotalCosts.Left = 5.5625F;
			this.fldTotalCosts.Name = "fldTotalCosts";
			this.fldTotalCosts.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalCosts.Text = "0.00";
			this.fldTotalCosts.Top = 0F;
			this.fldTotalCosts.Width = 0.625F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 6.875F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTotal.Text = "0.00";
			this.fldTotalTotal.Top = 0F;
			this.fldTotalTotal.Width = 1.0625F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 2.5F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 5.4375F;
			this.Line2.X1 = 2.5F;
			this.Line2.X2 = 7.9375F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// fldTotalDemand
			// 
			this.fldTotalDemand.Height = 0.1875F;
			this.fldTotalDemand.Left = 6.1875F;
			this.fldTotalDemand.Name = "fldTotalDemand";
			this.fldTotalDemand.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalDemand.Text = "0.00";
			this.fldTotalDemand.Top = 0F;
			this.fldTotalDemand.Width = 0.6875F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 3.9375F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTax.Text = "0.00";
			this.fldTotalTax.Top = 0F;
			this.fldTotalTax.Width = 0.8125F;
			// 
			// arLienTransferReport
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.9375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPLInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPLInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCO)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPLInt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCosts)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarLienEditMortHolders;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCO;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDemand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCosts;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDemand;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPLInt;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCosts;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDemand;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
