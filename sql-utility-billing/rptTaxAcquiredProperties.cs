﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptTaxAcquiredProperties.
	/// </summary>
	public partial class rptTaxAcquiredProperties : BaseSectionReport
	{
		public rptTaxAcquiredProperties()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Tax Acquired Properties";
		}

		public static rptTaxAcquiredProperties InstancePtr
		{
			get
			{
				return (rptTaxAcquiredProperties)Sys.GetInstance(typeof(rptTaxAcquiredProperties));
			}
		}

		protected rptTaxAcquiredProperties _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptTaxAcquiredProperties	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/11/2007              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/11/2007              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsLN = new clsDRWrapper();
		int lngNumOfAccts;
		double dblPDTotal;
		int lngPDNumber;
		int intArrIndex;
		int lngPONumber;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
			// catch the esc key and unload the report
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			intArrIndex = 1;
			if (modGlobalConstants.Statics.gblnCheckRETaxAcq && modGlobalConstants.Statics.gboolRE)
			{
				rsData.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, pOwn2.FullNameLF AS SecondOwnerName, MapLot, pOwn.Address1 AS OAddress1, pOwn.Address2 AS OAddress2, pOwn.Address3 AS OAddress3, pOwn.City AS OCity, pOwn.State AS OState, pOwn.Zip AS OZip, StreetNumber, StreetName, TaxAcquired " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID WHERE AccountNumber IN (" + modUTStatusPayments.GetTaxAcquiredAccountList() + ")", modExtraModules.strUTDatabase);
			}
			else
			{
				rsData.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, pOwn2.FullNameLF AS SecondOwnerName, MapLot, pOwn.Address1 AS OAddress1, pOwn.Address2 AS OAddress2, pOwn.Address3 AS OAddress3, pOwn.City AS OCity, pOwn.State AS OState, pOwn.Zip AS OZip, StreetNumber, StreetName, TaxAcquired " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID WHERE ISNULL(TaxAcquired,0) = 1", modExtraModules.strUTDatabase);
			}
			if (rsData.EndOfFile())
			{
				MessageBox.Show("There are no accounts that have been saved as Tax Acquired.", "No Accounts Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					//Application.DoEvents();
					BindFields();
					sarDetailOB.Report = new sarTaxAcquiredDetail();
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					sarDetailOB.Report.UserData = rsData.Get_Fields("AccountNumber");
					lngNumOfAccts += 1;
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				rsData.MoveLast();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Detail Format Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			if (!rsData.EndOfFile())
			{
				// us the correct new information
				//FC:FINAL:MSH - can't implicitly convert from int to string
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAcct.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
				// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
				fldName.Text = FCConvert.ToString(rsData.Get_Fields("OwnerName"));
				// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
				if (FCConvert.ToString(rsData.Get_Fields("SecondOwnerName")) != "")
				{
					// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					lblName.Text = lblName.Text + " & " + rsData.Get_Fields("SecondOwnerName");
				}
				fldMapLot.Text = FCConvert.ToString(rsData.Get_Fields_String("MapLot"));
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = Strings.Trim(rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
				}
				else
				{
					fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
				}
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (lngNumOfAccts == 1)
			{
				fldFooter.Text = "1 account processed.";
			}
			else
			{
				fldFooter.Text = FCConvert.ToString(lngNumOfAccts) + " accounts processed.";
			}
		}

		private void rptTaxAcquiredProperties_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptTaxAcquiredProperties properties;
			//rptTaxAcquiredProperties.Caption	= "Tax Acquired Properties";
			//rptTaxAcquiredProperties.Icon	= "rptTaxAcquiredProperties.dsx":0000";
			//rptTaxAcquiredProperties.Left	= 0;
			//rptTaxAcquiredProperties.Top	= 0;
			//rptTaxAcquiredProperties.Width	= 11880;
			//rptTaxAcquiredProperties.Height	= 8595;
			//rptTaxAcquiredProperties.StartUpPosition	= 3;
			//rptTaxAcquiredProperties.SectionData	= "rptTaxAcquiredProperties.dsx":058A;
			//End Unmaped Properties
		}

        private void rptTaxAcquiredProperties_ReportEndedAndCanceled(object sender, EventArgs e)
        {
            frmReportViewer.InstancePtr.Unload();
        }
    }
}
