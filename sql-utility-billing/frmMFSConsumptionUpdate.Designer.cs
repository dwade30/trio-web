﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmMFSConsumptionUpdate.
	/// </summary>
	partial class frmMFSConsumptionUpdate : BaseForm
	{
		public fecherFoundation.FCComboBox cboMeters;
		public fecherFoundation.FCComboBox cboAccount;
		public fecherFoundation.FCFrame fraConsumptionAmounts;
		public FCGrid vsGrid;
		public fecherFoundation.FCFrame fraAccountSearch;
		public fecherFoundation.FCTextBox txtSearchName;
		public fecherFoundation.FCTextBox txtSearchAccount;
		public fecherFoundation.FCGrid vsAccountSearch;
		public fecherFoundation.FCLabel lblSearchName;
		public fecherFoundation.FCLabel lblSearchAccount;
		public fecherFoundation.FCLabel lblMeter;
		public fecherFoundation.FCLabel Label9;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuAccountSearch;
		public fecherFoundation.FCToolStripMenuItem mnuSepar1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMFSConsumptionUpdate));
			this.cboMeters = new fecherFoundation.FCComboBox();
			this.cboAccount = new fecherFoundation.FCComboBox();
			this.fraConsumptionAmounts = new fecherFoundation.FCFrame();
			this.vsGrid = new fecherFoundation.FCGrid();
			this.fraAccountSearch = new fecherFoundation.FCFrame();
			this.txtSearchName = new fecherFoundation.FCTextBox();
			this.txtSearchAccount = new fecherFoundation.FCTextBox();
			this.vsAccountSearch = new fecherFoundation.FCGrid();
			this.lblSearchName = new fecherFoundation.FCLabel();
			this.lblSearchAccount = new fecherFoundation.FCLabel();
			this.lblMeter = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAccountSearch = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar1 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcess = new fecherFoundation.FCButton();
			this.cmdAccountSearch = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraConsumptionAmounts)).BeginInit();
			this.fraConsumptionAmounts.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).BeginInit();
			this.fraAccountSearch.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAccountSearch)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcess);
			this.BottomPanel.Location = new System.Drawing.Point(0, 552);
			this.BottomPanel.Size = new System.Drawing.Size(619, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboMeters);
			this.ClientArea.Controls.Add(this.cboAccount);
			this.ClientArea.Controls.Add(this.fraConsumptionAmounts);
			this.ClientArea.Controls.Add(this.fraAccountSearch);
			this.ClientArea.Controls.Add(this.lblMeter);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Size = new System.Drawing.Size(619, 492);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdAccountSearch);
			this.TopPanel.Size = new System.Drawing.Size(619, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAccountSearch, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(202, 30);
			this.HeaderText.Text = "Update Readings";
			// 
			// cboMeters
			// 
			this.cboMeters.AutoSize = false;
			this.cboMeters.BackColor = System.Drawing.SystemColors.Window;
			this.cboMeters.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboMeters.FormattingEnabled = true;
			this.cboMeters.Location = new System.Drawing.Point(211, 90);
			this.cboMeters.Name = "cboMeters";
			this.cboMeters.Size = new System.Drawing.Size(199, 40);
			this.cboMeters.TabIndex = 3;
			this.cboMeters.Text = "cboMeters";
			this.cboMeters.SelectedIndexChanged += new System.EventHandler(this.cboMeters_SelectedIndexChanged);
			// 
			// cboAccount
			// 
			this.cboAccount.AutoSize = false;
			this.cboAccount.BackColor = System.Drawing.SystemColors.Window;
			this.cboAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboAccount.FormattingEnabled = true;
			this.cboAccount.Location = new System.Drawing.Point(211, 30);
			this.cboAccount.Name = "cboAccount";
			this.cboAccount.Size = new System.Drawing.Size(199, 40);
			this.cboAccount.TabIndex = 1;
			this.cboAccount.SelectedIndexChanged += new System.EventHandler(this.cboAccount_SelectedIndexChanged);
			// 
			// fraConsumptionAmounts
			// 
			this.fraConsumptionAmounts.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraConsumptionAmounts.Controls.Add(this.vsGrid);
			this.fraConsumptionAmounts.Location = new System.Drawing.Point(30, 157);
			this.fraConsumptionAmounts.Name = "fraConsumptionAmounts";
			this.fraConsumptionAmounts.Size = new System.Drawing.Size(553, 182);
			this.fraConsumptionAmounts.TabIndex = 5;
			this.fraConsumptionAmounts.Text = "Readings";
			this.fraConsumptionAmounts.Visible = false;
			// 
			// vsGrid
			// 
			this.vsGrid.AllowSelection = false;
			this.vsGrid.AllowUserToResizeColumns = false;
			this.vsGrid.AllowUserToResizeRows = false;
			this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrid.Cols = 3;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsGrid.ColumnHeadersHeight = 30;
			this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrid.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsGrid.DragIcon = null;
			this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrid.ExtendLastCol = true;
			this.vsGrid.FixedCols = 0;
			this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.FrozenCols = 0;
			this.vsGrid.GridColor = System.Drawing.Color.Empty;
			this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.Location = new System.Drawing.Point(20, 30);
			this.vsGrid.Name = "vsGrid";
			this.vsGrid.ReadOnly = true;
			this.vsGrid.RowHeadersVisible = false;
			this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrid.RowHeightMin = 0;
			this.vsGrid.Rows = 1;
			this.vsGrid.ScrollTipText = null;
			this.vsGrid.ShowColumnVisibilityMenu = false;
			this.vsGrid.Size = new System.Drawing.Size(511, 139);
			this.vsGrid.StandardTab = true;
			this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsGrid.TabIndex = 0;
			this.vsGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsGrid_AfterEdit);
			this.vsGrid.CurrentCellChanged += new System.EventHandler(this.vsGrid_RowColChange);
			// 
			// fraAccountSearch
			// 
			this.fraAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraAccountSearch.Controls.Add(this.txtSearchName);
			this.fraAccountSearch.Controls.Add(this.txtSearchAccount);
			this.fraAccountSearch.Controls.Add(this.vsAccountSearch);
			this.fraAccountSearch.Controls.Add(this.lblSearchName);
			this.fraAccountSearch.Controls.Add(this.lblSearchAccount);
			this.fraAccountSearch.Location = new System.Drawing.Point(30, 157);
			this.fraAccountSearch.Name = "fraAccountSearch";
			this.fraAccountSearch.Size = new System.Drawing.Size(553, 238);
			this.fraAccountSearch.TabIndex = 4;
			this.fraAccountSearch.Text = "Account Search";
			this.fraAccountSearch.Visible = false;
			// 
			// txtSearchName
			// 
			this.txtSearchName.AutoSize = false;
			this.txtSearchName.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearchName.LinkItem = null;
			this.txtSearchName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSearchName.LinkTopic = null;
			this.txtSearchName.Location = new System.Drawing.Point(120, 30);
			this.txtSearchName.Name = "txtSearchName";
			this.txtSearchName.Size = new System.Drawing.Size(96, 40);
			this.txtSearchName.TabIndex = 1;
			this.txtSearchName.TabStop = false;
			this.txtSearchName.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearchName_KeyDown);
			// 
			// txtSearchAccount
			// 
			this.txtSearchAccount.AutoSize = false;
			this.txtSearchAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtSearchAccount.LinkItem = null;
			this.txtSearchAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSearchAccount.LinkTopic = null;
			this.txtSearchAccount.Location = new System.Drawing.Point(421, 30);
			this.txtSearchAccount.Name = "txtSearchAccount";
			this.txtSearchAccount.Size = new System.Drawing.Size(93, 40);
			this.txtSearchAccount.TabIndex = 3;
			this.txtSearchAccount.TabStop = false;
			this.txtSearchAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtSearchAccount_KeyDown);
			// 
			// vsAccountSearch
			// 
			this.vsAccountSearch.AllowSelection = false;
			this.vsAccountSearch.AllowUserToResizeColumns = false;
			this.vsAccountSearch.AllowUserToResizeRows = false;
			this.vsAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsAccountSearch.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsAccountSearch.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorBkg = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorFixed = System.Drawing.Color.Empty;
			this.vsAccountSearch.BackColorSel = System.Drawing.Color.Empty;
			this.vsAccountSearch.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsAccountSearch.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsAccountSearch.ColumnHeadersHeight = 30;
			this.vsAccountSearch.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsAccountSearch.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsAccountSearch.DragIcon = null;
			this.vsAccountSearch.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsAccountSearch.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsAccountSearch.FrozenCols = 0;
			this.vsAccountSearch.GridColor = System.Drawing.Color.Empty;
			this.vsAccountSearch.GridColorFixed = System.Drawing.Color.Empty;
			this.vsAccountSearch.Location = new System.Drawing.Point(20, 90);
			this.vsAccountSearch.Name = "vsAccountSearch";
			this.vsAccountSearch.ReadOnly = true;
			this.vsAccountSearch.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsAccountSearch.RowHeightMin = 0;
			this.vsAccountSearch.Rows = 1;
			this.vsAccountSearch.ScrollTipText = null;
			this.vsAccountSearch.ShowColumnVisibilityMenu = false;
			this.vsAccountSearch.Size = new System.Drawing.Size(511, 135);
			this.vsAccountSearch.StandardTab = true;
			this.vsAccountSearch.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsAccountSearch.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsAccountSearch.TabIndex = 4;
			this.vsAccountSearch.DoubleClick += new System.EventHandler(this.vsAccountSearch_DblClick);
			// 
			// lblSearchName
			// 
			this.lblSearchName.Location = new System.Drawing.Point(20, 44);
			this.lblSearchName.Name = "lblSearchName";
			this.lblSearchName.Size = new System.Drawing.Size(48, 24);
			this.lblSearchName.TabIndex = 0;
			this.lblSearchName.Text = "NAME";
			// 
			// lblSearchAccount
			// 
			this.lblSearchAccount.Location = new System.Drawing.Point(293, 44);
			this.lblSearchAccount.Name = "lblSearchAccount";
			this.lblSearchAccount.Size = new System.Drawing.Size(68, 24);
			this.lblSearchAccount.TabIndex = 2;
			this.lblSearchAccount.Text = "ACCOUNT";
			// 
			// lblMeter
			// 
			this.lblMeter.Location = new System.Drawing.Point(30, 104);
			this.lblMeter.Name = "lblMeter";
			this.lblMeter.Size = new System.Drawing.Size(99, 18);
			this.lblMeter.TabIndex = 2;
			this.lblMeter.Text = "SELECT METER";
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(30, 44);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(148, 18);
			this.Label9.TabIndex = 0;
			this.Label9.Text = "SELECT ACCOUNT";
			// 
			// MainMenu1
			// 
			this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFile
			});
			this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = 0;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAccountSearch,
				this.mnuSepar1,
				this.mnuProcess,
				this.mnuSepar2,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuAccountSearch
			// 
			this.mnuAccountSearch.Index = 0;
			this.mnuAccountSearch.Name = "mnuAccountSearch";
			this.mnuAccountSearch.Shortcut = Wisej.Web.Shortcut.F5;
			this.mnuAccountSearch.Text = "Account Search";
			this.mnuAccountSearch.Click += new System.EventHandler(this.mnuAccountSearch_Click);
			// 
			// mnuSepar1
			// 
			this.mnuSepar1.Index = 1;
			this.mnuSepar1.Name = "mnuSepar1";
			this.mnuSepar1.Text = "-";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 2;
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcess.Text = "Save and Continue";
			this.mnuProcess.Click += new System.EventHandler(this.mnuProcess_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 3;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 4;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdProcess
			// 
			this.cmdProcess.AppearanceKey = "acceptButton";
			this.cmdProcess.Location = new System.Drawing.Point(207, 25);
			this.cmdProcess.Name = "cmdProcess";
			this.cmdProcess.Size = new System.Drawing.Size(120, 48);
			this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcess.TabIndex = 0;
			this.cmdProcess.Text = "Continue";
			this.cmdProcess.Click += new System.EventHandler(this.mnuProcess_Click);
			// 
			// cmdAccountSearch
			// 
			this.cmdAccountSearch.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAccountSearch.AppearanceKey = "toolbarButton";
			this.cmdAccountSearch.Location = new System.Drawing.Point(461, 29);
			this.cmdAccountSearch.Name = "cmdAccountSearch";
			this.cmdAccountSearch.Size = new System.Drawing.Size(117, 24);
			this.cmdAccountSearch.TabIndex = 1;
			this.cmdAccountSearch.Text = "Account Search";
			this.cmdAccountSearch.Click += new System.EventHandler(this.mnuAccountSearch_Click);
			// 
			// frmMFSConsumptionUpdate
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(619, 660);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmMFSConsumptionUpdate";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Update Readings";
			this.Load += new System.EventHandler(this.frmMFSConsumptionUpdate_Load);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMFSConsumptionUpdate_KeyPress);
			this.Resize += new System.EventHandler(this.frmMFSConsumptionUpdate_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraConsumptionAmounts)).EndInit();
			this.fraConsumptionAmounts.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraAccountSearch)).EndInit();
			this.fraAccountSearch.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsAccountSearch)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAccountSearch)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcess;
		private FCButton cmdAccountSearch;
	}
}
