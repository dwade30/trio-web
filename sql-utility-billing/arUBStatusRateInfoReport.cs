﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for arUTDInformationScreen.
	/// </summary>
	public partial class arUBStatusRateInfoReport : BaseSectionReport
	{
		// nObj = 1
		//   0	arUTInformationScreen	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/22/2005              *
		// ********************************************************
		int lngRow;
		bool boolFirstTime;
		int lngMaxRow;
		int lngMaxCol;
		bool boolWater;
		private frmUBPaymentStatus paymentForm = null;

		public arUBStatusRateInfoReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Information";
		}

		public static arUBStatusRateInfoReport InstancePtr
		{
			get
			{
				return (arUBStatusRateInfoReport)Sys.GetInstance(typeof(arUBStatusRateInfoReport));
			}
		}

		protected arUBStatusRateInfoReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void Init(frmUBPaymentStatus callingForm, int lngPassRW, int lngPassCL, bool boolPassWater)
		{
			paymentForm = callingForm;
			lngMaxRow = lngPassRW;
			lngMaxCol = lngPassCL;
			boolWater = boolPassWater;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolFirstTime)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
				boolFirstTime = true;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			SetHeaderString();
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// this will print the row from the grid
			if (paymentForm.vsRateInfo.Cols > 0)
			{
				fldOut10.Text = paymentForm.vsRateInfo.TextMatrix(0, 0);
				fldOut11.Text = paymentForm.vsRateInfo.TextMatrix(1, 0);
				fldOut12.Text = paymentForm.vsRateInfo.TextMatrix(2, 0);
				fldOut13.Text = paymentForm.vsRateInfo.TextMatrix(3, 0);
				fldOut14.Text = paymentForm.vsRateInfo.TextMatrix(4, 0);
				fldOut15.Text = paymentForm.vsRateInfo.TextMatrix(5, 0);
				fldOut16.Text = paymentForm.vsRateInfo.TextMatrix(6, 0);
				fldOut17.Text = paymentForm.vsRateInfo.TextMatrix(7, 0);
				fldOut18.Text = paymentForm.vsRateInfo.TextMatrix(8, 0);
				fldOut19.Text = paymentForm.vsRateInfo.TextMatrix(9, 0);
				fldOut110.Text = paymentForm.vsRateInfo.TextMatrix(10, 0);
				fldOut111.Text = paymentForm.vsRateInfo.TextMatrix(11, 0);
				fldOut112.Text = paymentForm.vsRateInfo.TextMatrix(12, 0);
				fldOut113.Text = paymentForm.vsRateInfo.TextMatrix(13, 0);
				fldOut114.Text = paymentForm.vsRateInfo.TextMatrix(14, 0);
				fldOut115.Text = paymentForm.vsRateInfo.TextMatrix(15, 0);
				fldOut116.Text = paymentForm.vsRateInfo.TextMatrix(16, 0);
				fldOut117.Text = paymentForm.vsRateInfo.TextMatrix(17, 0);
				fldOut118.Text = paymentForm.vsRateInfo.TextMatrix(18, 0);
				if (paymentForm.vsRateInfo.Rows > 19)
				{
					fldOut119.Text = paymentForm.vsRateInfo.TextMatrix(19, 0);
				}
				// fldOut120.Text = .TextMatrix(20, 0)
				// fldOut121.Text = .TextMatrix(21, 0)
			}
			if (paymentForm.vsRateInfo.Cols > 1)
			{
				fldOut20.Text = paymentForm.vsRateInfo.TextMatrix(0, 1);
				fldOut21.Text = paymentForm.vsRateInfo.TextMatrix(1, 1);
				fldOut22.Text = paymentForm.vsRateInfo.TextMatrix(2, 1);
				fldOut23.Text = paymentForm.vsRateInfo.TextMatrix(3, 1);
				fldOut24.Text = paymentForm.vsRateInfo.TextMatrix(4, 1);
				fldOut25.Text = paymentForm.vsRateInfo.TextMatrix(5, 1);
				fldOut26.Text = paymentForm.vsRateInfo.TextMatrix(6, 1);
				fldOut27.Text = paymentForm.vsRateInfo.TextMatrix(7, 1);
				fldOut28.Text = paymentForm.vsRateInfo.TextMatrix(8, 1);
				fldOut29.Text = paymentForm.vsRateInfo.TextMatrix(9, 1);
				fldOut210.Text = paymentForm.vsRateInfo.TextMatrix(10, 1);
				fldOut211.Text = paymentForm.vsRateInfo.TextMatrix(11, 1);
				fldOut212.Text = paymentForm.vsRateInfo.TextMatrix(12, 1);
				fldOut213.Text = paymentForm.vsRateInfo.TextMatrix(13, 1);
				fldOut214.Text = paymentForm.vsRateInfo.TextMatrix(14, 1);
				fldOut215.Text = paymentForm.vsRateInfo.TextMatrix(15, 1);
				fldOut216.Text = paymentForm.vsRateInfo.TextMatrix(16, 1);
				fldOut217.Text = paymentForm.vsRateInfo.TextMatrix(17, 1);
				fldOut218.Text = paymentForm.vsRateInfo.TextMatrix(18, 1);
				if (paymentForm.vsRateInfo.Rows > 19)
				{
					fldOut219.Text = paymentForm.vsRateInfo.TextMatrix(19, 1);
				}
				// fldOut220.Text = .TextMatrix(20, 1)
				// fldOut221.Text = .TextMatrix(21, 1)
			}
			if (paymentForm.vsRateInfo.Cols > 3)
			{
				fldOut30.Text = paymentForm.vsRateInfo.TextMatrix(0, 3);
				fldOut31.Text = paymentForm.vsRateInfo.TextMatrix(1, 3);
				fldOut32.Text = paymentForm.vsRateInfo.TextMatrix(2, 3);
				fldOut33.Text = paymentForm.vsRateInfo.TextMatrix(3, 3);
				fldOut34.Text = paymentForm.vsRateInfo.TextMatrix(4, 3);
				fldOut35.Text = paymentForm.vsRateInfo.TextMatrix(5, 3);
				fldOut36.Text = paymentForm.vsRateInfo.TextMatrix(6, 3);
				fldOut37.Text = paymentForm.vsRateInfo.TextMatrix(7, 3);
				fldOut38.Text = paymentForm.vsRateInfo.TextMatrix(8, 3);
				fldOut39.Text = paymentForm.vsRateInfo.TextMatrix(9, 3);
				fldOut310.Text = paymentForm.vsRateInfo.TextMatrix(10, 3);
				fldOut311.Text = paymentForm.vsRateInfo.TextMatrix(11, 3);
				fldOut312.Text = paymentForm.vsRateInfo.TextMatrix(12, 3);
				fldOut313.Text = paymentForm.vsRateInfo.TextMatrix(13, 3);
				fldOut314.Text = paymentForm.vsRateInfo.TextMatrix(14, 3);
				fldOut315.Text = paymentForm.vsRateInfo.TextMatrix(15, 3);
				fldOut316.Text = paymentForm.vsRateInfo.TextMatrix(16, 3);
				fldOut317.Text = paymentForm.vsRateInfo.TextMatrix(17, 3);
				fldOut318.Text = paymentForm.vsRateInfo.TextMatrix(18, 3);
				if (paymentForm.vsRateInfo.Rows > 19)
				{
					fldOut319.Text = paymentForm.vsRateInfo.TextMatrix(19, 3);
				}
				// fldOut320.Text = .TextMatrix(20, 3)
				// fldOut321.Text = .TextMatrix(21, 3)
			}
			if (paymentForm.vsRateInfo.Cols > 4)
			{
				fldOut40.Text = paymentForm.vsRateInfo.TextMatrix(0, 4);
				fldOut41.Text = paymentForm.vsRateInfo.TextMatrix(1, 4);
				fldOut42.Text = paymentForm.vsRateInfo.TextMatrix(2, 4);
				fldOut43.Text = paymentForm.vsRateInfo.TextMatrix(3, 4);
				fldOut44.Text = paymentForm.vsRateInfo.TextMatrix(4, 4);
				fldOut45.Text = paymentForm.vsRateInfo.TextMatrix(5, 4);
				fldOut46.Text = paymentForm.vsRateInfo.TextMatrix(6, 4);
				fldOut47.Text = paymentForm.vsRateInfo.TextMatrix(7, 4);
				fldOut48.Text = paymentForm.vsRateInfo.TextMatrix(8, 4);
				fldOut49.Text = paymentForm.vsRateInfo.TextMatrix(9, 4);
				fldOut410.Text = paymentForm.vsRateInfo.TextMatrix(10, 4);
				fldOut411.Text = paymentForm.vsRateInfo.TextMatrix(11, 4);
				fldOut412.Text = paymentForm.vsRateInfo.TextMatrix(12, 4);
				fldOut413.Text = paymentForm.vsRateInfo.TextMatrix(13, 4);
				fldOut414.Text = paymentForm.vsRateInfo.TextMatrix(14, 4);
				fldOut415.Text = paymentForm.vsRateInfo.TextMatrix(15, 4);
				fldOut416.Text = paymentForm.vsRateInfo.TextMatrix(16, 4);
				fldOut417.Text = paymentForm.vsRateInfo.TextMatrix(17, 4);
				fldOut418.Text = paymentForm.vsRateInfo.TextMatrix(18, 4);
				if (paymentForm.vsRateInfo.Rows > 19)
				{
					fldOut419.Text = paymentForm.vsRateInfo.TextMatrix(19, 4);
				}
				// fldOut420.Text = .TextMatrix(20, 4)
				// fldOut421.Text = .TextMatrix(21, 4)
			}
		}

		private int FindNextVisibleRow(ref int lngR)
		{
			int FindNextVisibleRow = 0;
			// this function will start at the row number passed in and return the next row to print
			// this function will return -1 if there are no more rows to show
			int lngNext;
			int intLvl;
			bool boolFound;
			int lngMax = 0;
			boolFound = false;
			lngNext = lngR + 1;
			if (boolWater)
			{
				lngMax = paymentForm.WGRID.Rows;
				if (lngNext < lngMax)
				{
					boolFound = false;
					while (!(boolFound || paymentForm.WGRID.RowOutlineLevel(lngNext) < 2))
					{
						if (paymentForm.WGRID.IsCollapsed(LastParentRow(lngNext)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							lngNext += 1;
							boolFound = false;
							if (lngNext >= lngMax)
							{
								return FindNextVisibleRow;
							}
						}
						else
						{
							boolFound = true;
						}
					}
					FindNextVisibleRow = lngNext;
				}
			}
			else
			{
				lngMax = paymentForm.SGRID.Rows;
				if (lngNext < lngMax)
				{
					boolFound = false;
					while (!(boolFound || paymentForm.SGRID.RowOutlineLevel(lngNext) < 2))
					{
						if (paymentForm.SGRID.IsCollapsed(LastParentRow(lngNext)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							lngNext += 1;
							boolFound = false;
							if (lngNext >= lngMax)
							{
								return FindNextVisibleRow;
							}
						}
						else
						{
							boolFound = true;
						}
					}
					FindNextVisibleRow = lngNext;
				}
			}
			if (FindNextVisibleRow == 0)
				FindNextVisibleRow = -1;
			return FindNextVisibleRow;
		}
		// vbPorter upgrade warning: CurRow As Variant --> As int
		public int LastParentRow(int CurRow)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			if (boolWater)
			{
				curLvl = paymentForm.WGRID.RowOutlineLevel(l);
				l -= 1;
				while (paymentForm.WGRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
				LastParentRow = l;
			}
			else
			{
				curLvl = paymentForm.SGRID.RowOutlineLevel(l);
				l -= 1;
				while (paymentForm.SGRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
				LastParentRow = l;
			}
			return LastParentRow;
		}

		private void SetHeaderString()
		{
			string strTemp = "";
			int lngAcctNum;

			lngAcctNum = paymentForm.ViewModel.Customer.AccountNumber;
			strTemp += " Account " + FCConvert.ToString(lngAcctNum) + " " + paymentForm.fraRateInfo.Text;
			strTemp += "\r\n" + "as of " + Strings.Format(modUTFunctions.Statics.UTEffectiveDate, "MM/dd/yyyy");
			lblHeader.Text = strTemp;
			lblName.Text = "Name: " + paymentForm.lblOwnersName.Text;
			lblLocation.Text = "Location: " + paymentForm.lblLocation.Text;
		}
	}
}
