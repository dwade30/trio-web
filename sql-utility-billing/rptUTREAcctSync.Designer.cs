﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTREAcctSync.
	/// </summary>
	partial class rptUTREAcctSync
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUTREAcctSync));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblCaption = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUserID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldUTAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldREAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOldOwn1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNewOwn1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOldOwn2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNewOwn2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldProcCnt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblCaption)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUserID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUTAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldREAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldOwn1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNewOwn1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldOwn2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNewOwn2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldProcCnt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldUTAcct,
				this.fldREAcct,
				this.fldOldOwn1,
				this.fldNewOwn1,
				this.fldOldOwn2,
				this.fldNewOwn2,
				this.fldLocation
			});
			this.Detail.Height = 0.2291667F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
			this.ReportHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblCaption,
				this.fldDate,
				this.fldUserID
			});
			this.ReportHeader.Height = 0.4270833F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label8,
				this.fldProcCnt
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.Label10
			});
			this.PageHeader.Height = 0.2395833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblCaption
			// 
			this.lblCaption.Height = 0.1875F;
			this.lblCaption.HyperLink = null;
			this.lblCaption.Left = 1F;
			this.lblCaption.Name = "lblCaption";
			this.lblCaption.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.lblCaption.Text = "UT/RE Account Owner Sync";
			this.lblCaption.Top = 0.0625F;
			this.lblCaption.Width = 5.5F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Text = "Date";
			this.fldDate.Top = 0F;
			this.fldDate.Width = 0.9375F;
			// 
			// fldUserID
			// 
			this.fldUserID.Height = 0.1875F;
			this.fldUserID.Left = 7.0625F;
			this.fldUserID.Name = "fldUserID";
			this.fldUserID.Style = "text-align: right";
			this.fldUserID.Text = "UserId";
			this.fldUserID.Top = 0F;
			this.fldUserID.Width = 0.9375F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 8pt; font-weight: bold";
			this.Label1.Text = "Account";
			this.Label1.Top = 0F;
			this.Label1.Width = 0.625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 0.6875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-size: 8pt; font-weight: bold";
			this.Label4.Text = "Old Owner";
			this.Label4.Top = 0F;
			this.Label4.Width = 0.875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-size: 8pt; font-weight: bold";
			this.Label5.Text = "New Owner";
			this.Label5.Top = 0F;
			this.Label5.Width = 1F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 3.4375F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-size: 8pt; font-weight: bold";
			this.Label6.Text = "Old 2nd Owner";
			this.Label6.Top = 0F;
			this.Label6.Width = 1F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 4.8125F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-size: 8pt; font-weight: bold";
			this.Label7.Text = "New 2nd Owner";
			this.Label7.Top = 0F;
			this.Label7.Width = 1F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 7.1875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-size: 8pt; font-weight: bold";
			this.Label10.Text = "RE Account";
			this.Label10.Top = 0F;
			this.Label10.Width = 0.8125F;
			// 
			// fldUTAcct
			// 
			this.fldUTAcct.Height = 0.1875F;
			this.fldUTAcct.Left = 0F;
			this.fldUTAcct.Name = "fldUTAcct";
			this.fldUTAcct.Style = "font-size: 8pt";
			this.fldUTAcct.Text = "UT Acct";
			this.fldUTAcct.Top = 0F;
			this.fldUTAcct.Width = 0.625F;
			// 
			// fldREAcct
			// 
			this.fldREAcct.Height = 0.1875F;
			this.fldREAcct.Left = 7.1875F;
			this.fldREAcct.Name = "fldREAcct";
			this.fldREAcct.Style = "font-size: 8pt";
			this.fldREAcct.Text = "RE Acct";
			this.fldREAcct.Top = 0F;
			this.fldREAcct.Width = 0.625F;
			// 
			// fldOldOwn1
			// 
			this.fldOldOwn1.Height = 0.1875F;
			this.fldOldOwn1.Left = 0.6875F;
			this.fldOldOwn1.Name = "fldOldOwn1";
			this.fldOldOwn1.Style = "font-size: 8pt";
			this.fldOldOwn1.Text = "Old Owner";
			this.fldOldOwn1.Top = 0F;
			this.fldOldOwn1.Width = 1.3125F;
			// 
			// fldNewOwn1
			// 
			this.fldNewOwn1.Height = 0.1875F;
			this.fldNewOwn1.Left = 2.0625F;
			this.fldNewOwn1.Name = "fldNewOwn1";
			this.fldNewOwn1.Style = "font-size: 8pt";
			this.fldNewOwn1.Text = "New Owner";
			this.fldNewOwn1.Top = 0F;
			this.fldNewOwn1.Width = 1.3125F;
			// 
			// fldOldOwn2
			// 
			this.fldOldOwn2.Height = 0.1875F;
			this.fldOldOwn2.Left = 3.4375F;
			this.fldOldOwn2.Name = "fldOldOwn2";
			this.fldOldOwn2.Style = "font-size: 8pt";
			this.fldOldOwn2.Text = "Old 2nd Owner";
			this.fldOldOwn2.Top = 0F;
			this.fldOldOwn2.Width = 1.3125F;
			// 
			// fldNewOwn2
			// 
			this.fldNewOwn2.Height = 0.1875F;
			this.fldNewOwn2.Left = 4.8125F;
			this.fldNewOwn2.Name = "fldNewOwn2";
			this.fldNewOwn2.Style = "font-size: 8pt";
			this.fldNewOwn2.Text = "New 2nd Owner";
			this.fldNewOwn2.Top = 0F;
			this.fldNewOwn2.Width = 1.3125F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 6.1875F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-size: 8pt";
			this.fldLocation.Text = "Location";
			this.fldLocation.Top = 0F;
			this.fldLocation.Width = 0.9375F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 2.25F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "";
			this.Label8.Text = "Total Accounts Affected:";
			this.Label8.Top = 0F;
			this.Label8.Width = 1.625F;
			// 
			// fldProcCnt
			// 
			this.fldProcCnt.Height = 0.1875F;
			this.fldProcCnt.Left = 4.0625F;
			this.fldProcCnt.Name = "fldProcCnt";
			this.fldProcCnt.Text = "Count";
			this.fldProcCnt.Top = 0F;
			this.fldProcCnt.Width = 0.9375F;
			// 
			// rptUTREAcctSync
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblCaption)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUserID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUTAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldREAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldOwn1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNewOwn1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOldOwn2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNewOwn2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldProcCnt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUTAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldREAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOldOwn1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNewOwn1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOldOwn2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNewOwn2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCaption;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUserID;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProcCnt;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
