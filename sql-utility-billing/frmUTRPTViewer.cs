﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTRPTViewer.
	/// </summary>
	public partial class frmUTRPTViewer : BaseForm
	{
		public frmUTRPTViewer()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmUTRPTViewer InstancePtr
		{
			get
			{
				return (frmUTRPTViewer)Sys.GetInstance(typeof(frmUTRPTViewer));
			}
		}

		protected frmUTRPTViewer _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/22/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/22/2005              *
		// ********************************************************
		string strRPTName;
		int intType;
		string strEmailAttachmentName = "";

		public void Init(string strReportName, string strFormCaption, short intPassType)
		{
			strRPTName = strReportName;
			//FC:FINAL:RPU:#i987 - Call the load as in original if the form is not loaded 
			if (!this.IsLoaded)
			{
				this.LoadForm();
			}
			this.Text = strFormCaption;
            //FC:FINAL:CHN - issue #1591: Missing set header text.
            this.HeaderText.Text = strFormCaption;
			//arView.ReportSource.Top = 0;
			//arView.ReportSource.Width = this.Width-100;
			//arView.ReportSource.Left = 0;
			//arView.ReportSource.Height = this.Height-400;
			//arView.Zoom = -1;
			LoadRecreateCombo();
			LoadCombo();
			intType = intPassType;
			switch (intType)
			{
				case 1:
				case 2:
					{
						// this is the audits
						// ShowReport
						mnuFileChange_Click();
						break;
					}
				case 10:
					{
						// this is the purge list
						ShowReport();
						break;
					}
				case 11:
					{
						// bankruptcy report
						cmdFileRecreate.Visible = false;
						break;
					}
				case 12:
					{
						// Reprint of CMF Report
						cmdFileRecreate.Visible = false;
						break;
					}
				case 13:
					{
						// reprint of fee lists
						cmdFileRecreate.Visible = false;
						break;
					}
			}
			//end switch
			// If arView.Visible Then
			// arView.SetFocus
			// End If
			//FC:FINAL:DDU:#i926 - moved activated after variable is seted
			this.Show(App.MainForm);
		}

		private void mnuFileChange_Click()
		{
			mnuFileChange_Click(mnuFileChange, new System.EventArgs());
		}

		private void frmUTRPTViewer_Activated(object sender, System.EventArgs e)
		{
			if (this.WindowState != FormWindowState.Maximized && this.WindowState != FormWindowState.Minimized)
			{
				this.Height = this.Height - 15;
			}
			//arView.ReportSource.Top = 0;
			//arView.ReportSource.Width = this.Width-100;
			//arView.ReportSource.Left = 0;
			//arView.ReportSource.Height = this.Height-400;
			//arView.Zoom = -1;
			LoadRecreateCombo();
			LoadCombo();
			ShowReport();
		}

		private void frmUTRPTViewer_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTRPTViewer properties;
			//frmUTRPTViewer.FillStyle	= 0;
			//frmUTRPTViewer.ScaleWidth	= 9045;
			//frmUTRPTViewer.ScaleHeight	= 7395;
			//frmUTRPTViewer.LinkTopic	= "Form2";
			//frmUTRPTViewer.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Redisplay Daily Audit Report";
			this.WindowState = FormWindowState.Normal;
		}

		private void frmUTRPTViewer_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches escape keys
			if (KeyAscii == Keys.Escape)
			{
				mnuFileExport.Enabled = false;
				mnuFileEmail.Enabled = false;
				KeyAscii = (Keys)0;
				this.Unload();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
		// vbPorter upgrade warning: intNum As short	OnWrite(string)
		private void ShowReport_2(short intNum)
		{
			ShowReport(intNum);
		}

		private void ShowReport(short intNum = 1)
		{
			if (modGlobalFunctions.ReportDirectoryStatus())
			{
				switch (intType)
				{
					case 1:
					case 2:
						{
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF"))
							{
								//arView.Pages.Load("RPT\\"+strRPTName+FCConvert.ToString(intNum)+".RDF");
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF";
								this.Refresh();
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									MessageBox.Show("To view this report, you must run the report first.", "Run Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									MessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								if (Strings.UCase(strRPTName) != Strings.UCase("LastUTDailyAudit"))
								{
									this.Unload();
								}
							}
							break;
						}
					case 10:
						{
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName))
							{
								//arView.Pages.Load("RPT\\"+strRPTName);
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName;
								this.Refresh();
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									MessageBox.Show("To view this report, you must run the report first.", "Run Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									MessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								this.Unload();
							}
							break;
						}
					case 11:
						{
							// bankruptcy report
							//FC:FINAL:MSH - issue #1216: replace wrong path
							//if (File.Exists("RPT\\" + strRPTName + "1.RDF"))
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF"))
							{
								//arView.Pages.Load("RPT\\"+strRPTName+"1.RDF");
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF";
								this.Refresh();
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									// MsgBox "To view this report, you must run the report first.", vbInformation, "Run Report"
									MessageBox.Show("This saved report has not been created.", "Run Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									MessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								this.Unload();
							}
							break;
						}
					case 12:
						{
							// reprint of the last CMF report
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + ".RDF"))
							{
								//arView.Pages.Load("RPT\\"+strRPTName+".RDF");
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + ".RDF";
								this.Refresh();
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									MessageBox.Show("To view this report, you must run the report first.", "Run Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									MessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								this.Unload();
							}
							break;
						}
					case 13:
						{
							// reprint of fees lists
							if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF"))
							{
								//arView.Pages.Load("RPT\\"+strRPTName+FCConvert.ToString(intNum)+".RDF");
								arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intNum) + ".RDF";
								this.Refresh();
								if (arView.Visible)
								{
									arView.Focus();
								}
							}
							else
							{
								frmWait.InstancePtr.Unload();
								if (intNum == 1)
								{
									MessageBox.Show("To view this report, you must run the report first.", "Run Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									MessageBox.Show("There is not a " + strRPTName + " saved report with the number " + FCConvert.ToString(intNum) + " have been created.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
							}
							break;
						}
				}
				//end switch
			}
		}

		private void frmUTRPTViewer_Resize(object sender, System.EventArgs e)
		{
			//arView.ReportSource.Top = 0;
			//arView.ReportSource.Width = this.Width-100;
			//arView.ReportSource.Left = 0;
			//arView.ReportSource.Height = this.Height-400;
			// MAL@20070907: Added option so control does not drop to the bottom of the screen
			if (fraRecreateReport.Visible)
			{
				//fraRecreateReport.Top = FCConvert.ToInt32((arView.Height / 6.0));
			}
			if (fraNumber.Visible)
			{
				//fraNumber.Top = FCConvert.ToInt32((arView.Height / 6.0));
			}
		}

		private void mnuEmailPDF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
				string strList = "";
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
                //FC:FINAL:DSE Exception when ReportSource is null
                //string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + arView.ReportSource.Name + ".pdf";
                //a.Export(arView.ReportSource.Document, fileName);
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".pdf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".pdf";
                }
                a.Export(report.Document, fileName);
                if (!File.Exists(fileName))
				{
					MessageBox.Show("Could not create report file to send as attachment", "Cannot E-mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (Strings.Trim(strEmailAttachmentName) != string.Empty)
					{
						strList = fileName + ";" + strEmailAttachmentName + ".pdf";
					}
					else
					{
						strList = fileName;
					}
					frmEMail.InstancePtr.Init(strList, "", "", "", false, false, true, showAsModalForm: true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailPDF_Click");
			}
		}

		private void mnuEMailRTF_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
				string strList = "";
				if (!Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
				{
					Directory.CreateDirectory(TWSharedLibrary.Variables.Statics.ReportPath);
				}
                //FC:FINAL:DSE Exception when ReportSource is null
                //string fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + arView.ReportSource.Name + ".rtf";
                //a.Export(arView.ReportSource.Document, fileName);
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(arView.ReportSource.Name) + ".rtf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
					//FC:FINAL:DSE Exception if report name contains sensitive characters
					fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + FCUtils.FixFileName(frmReportViewer.InstancePtr.Text) + ".rtf";
                }
                a.Export(report.Document, fileName);
                if (!File.Exists(fileName))
				{
					MessageBox.Show("Could not create report file to send as attachment", "Cannot E-mail", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (Strings.Trim(strEmailAttachmentName) != string.Empty)
					{
						strList = fileName + ";" + strEmailAttachmentName + ".rtf";
					}
					else
					{
						strList = fileName;
					}
					frmEMail.InstancePtr.Init(strList, "", "", "", false, false, true, showAsModalForm: true);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In mnuEmailRTF_Click");
			}
		}

		private void mnuFile_Click(object sender, System.EventArgs e)
		{
			mnuFileSwitch.Text = "Show Report";
		}

		private void mnuFileChange_Click(object sender, System.EventArgs e)
		{
			//FC:FINAL:CHN - issue #990: Overlays test.
			lblInstruction.Visible = true;
			lblRecreateInstructions.Visible = false;
            cmbRecreate.Visible = false;
            TopWrapper.Visible = true;
			// this will let the user switch between the numbered reports
			arView.Visible = false;
            arView.Parent = null;
            mnuFileExport.Enabled = false;
			mnuFileEmail.Enabled = false;
			fraNumber.Visible = true;
			//fraNumber.Left = FCConvert.ToInt32((this.Width-fraNumber.Width) / 40.0);
			//fraNumber.Top = FCConvert.ToInt32((this.Height-fraNumber.Height) / 3.0);
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFileExportExcel_Click(object sender, System.EventArgs e)
		{
			ExportExcel();
		}

		private void mnuFileExportHTML_Click(object sender, System.EventArgs e)
		{
			ExportHTML();
		}

		private void mnuFileExportPDF_Click(object sender, System.EventArgs e)
		{
			ExportPDF();
		}

		private void mnuFileExportRTF_Click(object sender, System.EventArgs e)
		{
			ExportRTF();
		}

		private void mnuFileRecreate_Click(object sender, System.EventArgs e)
		{
			// this will recreate the audit report
			//FC:FINAL:CHN - issue #990: Overlays test.
			lblInstruction.Visible = false;
			lblRecreateInstructions.Visible = true;
            cmbRecreate.Visible = true;
			lblRecreateInstructions.Text = "Select the report to recreate";
			// show the recreate frame
			fraRecreateReport.Visible = true;
            TopWrapper.Visible = true;
			// hide the rest of the frames
			fraNumber.Visible = false;
			arView.Visible = false;
            arView.Parent = null;
            mnuFileExport.Enabled = false;
			mnuFileEmail.Enabled = false;
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			if (fraRecreateReport.Visible)
			{
				if (cmbRecreate.SelectedIndex != -1)
				{
					modGlobalConstants.Statics.gblnRecreate = true;
					// load the report
					ReCreateReport_2(FCConvert.ToInt32(Conversion.Val(Strings.Left(cmbRecreate.Items[cmbRecreate.SelectedIndex].ToString(), 5))));
					this.Unload();
				}
			}
			else if (fraNumber.Visible)
			{
				if (cmbNumber.SelectedIndex != -1)
				{
					ShowReport_2(FCConvert.ToInt16(cmbNumber.Items[cmbNumber.SelectedIndex].ToString()));
					fraNumber.Visible = false;
					arView.Visible = true;
                    arView.Parent = this.ClientArea;
                    this.ClientArea.Controls.SetChildIndex(arView, 0);
                    TopWrapper.Visible = false;
					mnuFileExport.Enabled = true;
					mnuFileEmail.Enabled = true;
					modGlobalConstants.Statics.gblnRecreate = false;
				}
			}
		}

		private void mnuFileSwitch_Click(object sender, System.EventArgs e)
		{
			if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF"))
			{
				//arView.Pages.Load("RPT\\"+strRPTName+"1.RDF");
				arView.ReportName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + "1.RDF";
			}
			else
			{
				MessageBox.Show("The file (" + strRPTName + "1.RDF) being loaded does not exist.  Please create it first.", "Load Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			mnuFileExport.Enabled = false;
			mnuFileEmail.Enabled = false;
			LoadCombo();
		}

		private void LoadCombo()
		{
			// this will check to see which reports are saved and will show the list of numbers
			int intCT;
			cmbNumber.Clear();
			if (Directory.Exists(TWSharedLibrary.Variables.Statics.ReportPath))
			{
				for (intCT = 1; intCT <= 9; intCT++)
				{
					if (File.Exists(TWSharedLibrary.Variables.Statics.ReportPath + "\\" + strRPTName + FCConvert.ToString(intCT) + ".RDF"))
					{
						cmbNumber.AddItem(intCT.ToString());
					}
				}
			}
			else
			{
				MessageBox.Show("You must run a report before viewing it.", "No Report", MessageBoxButtons.OK, MessageBoxIcon.Information);
				modGlobalFunctions.ReportDirectoryStatus();
				// Unload Me
			}
		}
		// vbPorter upgrade warning: lngCloseOut As int	OnWriteFCConvert.ToDouble(
		private void ReCreateReport_2(int lngCloseOut)
		{
			ReCreateReport(ref lngCloseOut);
		}

		private void ReCreateReport(ref int lngCloseOut)
		{
			modMain.Statics.glngCurrentCloseOut = lngCloseOut;
			rptUTDailyAuditMaster.InstancePtr.StartUp(true, FCConvert.CBool(modRegistry.GetRegistryKey("UTAuditLandscape", "CR") == "True"), 2, false, false);
			frmReportViewer.InstancePtr.Init(rptUTDailyAuditMaster.InstancePtr);
			mnuFileExport.Enabled = true;
			mnuFileEmail.Enabled = true;
			frmReportViewer.InstancePtr.Focus();
		}

		private void LoadRecreateCombo()
		{
			// this will fill the recreation combo with all of the closeouts done in the CloseOut table
			clsDRWrapper rsCloseOut = new clsDRWrapper();
			cmbRecreate.Clear();
			rsCloseOut.OpenRecordset("SELECT * FROM CloseOut ORDER BY CloseOutDate desc", modExtraModules.strUTDatabase);
			while (!rsCloseOut.EndOfFile())
			{
				cmbRecreate.AddItem(modGlobalFunctions.PadStringWithSpaces(rsCloseOut.Get_Fields_Int32("ID").ToString(), 5) + " - " + rsCloseOut.Get_Fields_DateTime("CloseOutDate"));
				cmbRecreate.ItemData(cmbRecreate.NewIndex, FCConvert.ToInt32(rsCloseOut.Get_Fields_Int32("ID")));
				rsCloseOut.MoveNext();
			}
		}

		private void ExportHTML()
		{
			try
			{
				//string strFileName = "";
				//// vbPorter upgrade warning: intResp As short, int --> As DialogResult
				//DialogResult intResp;
				//string strOldDir;
				//string strFullName = "";
				//strOldDir = Environment.CurrentDirectory;
				//Information.Err(ex).Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  *///MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.htm";
				//MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "htm";
				//MDIParent.InstancePtr.CommonDialog1_Save.InitialDirectory = Application.StartupPath;
				//MDIParent.InstancePtr.CommonDialog1_Save.ShowDialog();
				//if (Information.Err(ex).Number == 0)
				//{
				GrapeCity.ActiveReports.Export.Html.Section.HtmlExport a = new GrapeCity.ActiveReports.Export.Html.Section.HtmlExport();
                //FC:FINAL:DSE:#1754 Exception when ReportSource is null
                //string fileName = arView.ReportSource.Name + ".html";
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + arView.ReportSource.Name + ".html";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + frmReportViewer.InstancePtr.Text + ".html";
                }
                using (MemoryStream stream = new MemoryStream())
				{
                    //FC:FINAL:DSE:#1754 Exception when ReportSource is null
                    //a.Export(arView.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				//ErrorHandler: ;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HTML export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ExportRTF()
		{
			//const int curOnErrorGoToLabel_Default = 0;
			//const int curOnErrorGoToLabel_ErrorHandler = 1;
			//int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				//	string strOldDir;
				//	strOldDir = Environment.CurrentDirectory;
				//	Information.Err(ex).Clear();
				//	// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//	//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				//	 /*? On Error Resume Next  */
				//	MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//	MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.rtf";
				//	MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "rtf";
				//	MDIParent.InstancePtr.CommonDialog1_Save.InitialDirectory = Application.StartupPath;
				//	MDIParent.InstancePtr.CommonDialog1_Save.ShowDialog();
				//	if (Information.Err(ex).Number==0) {
				GrapeCity.ActiveReports.Export.Word.Section.RtfExport a = new GrapeCity.ActiveReports.Export.Word.Section.RtfExport();
                //FC:FINAL:DSE:#1752 Exception when ReportSource is null
                //string fileName = arView.ReportSource.Name + ".xls";
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + arView.ReportSource.Name + ".rtf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + frmReportViewer.InstancePtr.Text + ".rtf";
                }
                using (MemoryStream stream = new MemoryStream())
				{
                    //FC:FINAL:DSE:#1752 Exception when ReportSource is null
                    //a.Export(arView.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				//ErrorHandler: ;
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In RTF export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ExportPDF()
		{
			//const int curOnErrorGoToLabel_Default = 0;
			//const int curOnErrorGoToLabel_ErrorHandler = 1;
			//int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				//	string strOldDir;
				//	strOldDir = Environment.CurrentDirectory;
				//	Information.Err(ex).Clear();
				//	 MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//	- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				//	 /*? On Error Resume Next  */
				//	MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//	MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.pdf";
				//	MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "pdf";
				//	MDIParent.InstancePtr.CommonDialog1_Save.InitialDirectory = Application.StartupPath;
				//	MDIParent.InstancePtr.CommonDialog1_Save.ShowDialog();
				//	if (Information.Err(ex).Number==0) {
				GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport a = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
                //FC:FINAL:DSE:#1753 Exception when ReportSource is null
                //string fileName = arView.ReportSource.Name + ".pdf";
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + arView.ReportSource.Name + ".pdf";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + frmReportViewer.InstancePtr.Text + ".pdf";
                }
                //a.Export(ARViewer21.ReportSource.Document, Path.Combine(Application.StartupPath,fileName));
                using (MemoryStream stream = new MemoryStream())
				{
                    //FC:FINAL:DSE:#1753 Exception when ReportSource is null
                    //a.Export(arView.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In PDF export", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ExportExcel()
		{
			//const int curOnErrorGoToLabel_Default = 0;
			//const int curOnErrorGoToLabel_ErrorHandler = 1;
			//int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				string strOldDir;
				string strFileName = "";
				string strFullName = "";
				string strPathName = "";
				strOldDir = FCFileSystem.Statics.UserDataFolder;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  *///MDIParent.InstancePtr.CommonDialog1_Save.FileName = "";
				//MDIParent.InstancePtr.CommonDialog1_Save.Filter = "*.xls";
				//MDIParent.InstancePtr.CommonDialog1_Save.DefaultExt = "xls";
				//MDIParent.InstancePtr.CommonDialog1_Save.InitialDirectory = Application.StartupPath;
				//MDIParent.InstancePtr.CommonDialog1_Save.ShowDialog();
				//if (Information.Err(ex).Number==0) {
				//	strFileName = fso.GetBaseName(MDIParent.InstancePtr.CommonDialog1_Save.FileName); // name without extension
				//	strFullName = fso.GetFileName(MDIParent.InstancePtr.CommonDialog1_Save.FileName); // name with extension
				//	strPathName = fso.GetParentFolderName(MDIParent.InstancePtr.CommonDialog1_Save.FileName);
				//	if (Strings.Right(strPathName, 1)!="\\") strPathName += "\\";
				//	ActiveReportsExcelExport.ARExportExcel a = new ActiveReportsExcelExport.ARExportExcel();
				//	a = new ActiveReportsExcelExport.ARExportExcel();
				//	if (!Directory.Exists(strPathName+strFileName)) {
				//		Directory.CreateDirectory(strPathName+strFileName);
				//		strPathName += strFileName+"\\";
				//	}
				//	// a.FileName = .FileName
				//	a.FileName = strPathName+strFullName;
				//	a.Version = 8;
				//	a.Export(arView.Pages);
				//	MessageBox.Show("Export to file  "+strPathName+strFullName+"  was completed successfully."+"\r\n"+"Please note that supporting files may also have been created in the same directory.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//} else {
				//	MessageBox.Show("There was an error saving the file.", null, MessageBoxButtons.OK, MessageBoxIcon.Hand);
				//}
				//vOnErrorGoToLabel = curOnErrorGoToLabel_ErrorHandler; /* On Error GoTo ErrorHandler */
				//// ChDrive strOldDir
				//// ChDir strOldDir
				//modAPIsConst.SetCurrentDirectoryWrp(ref strOldDir);
				GrapeCity.ActiveReports.Export.Excel.Section.XlsExport a = new GrapeCity.ActiveReports.Export.Excel.Section.XlsExport();
                //FC:FINAL:DSE:#1755 Exception when ReportSource is null
                //string fileName = arView.ReportSource.Name + ".xls";
                string fileName = "";
                FCSectionReport report = null;
                if (!(arView.ReportSource == null))
                {
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + arView.ReportSource.Name + ".xls";
                    report = arView.ReportSource;
                }
                else
                {
                    report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    fileName = TWSharedLibrary.Variables.Statics.ReportPath + "\\" + frmReportViewer.InstancePtr.Text + ".xls";
                }
                using (MemoryStream stream = new MemoryStream())
				{
                    //FC:FINAL:DSE:#1755 Exception when ReportSource is null
                    //a.Export(arView.ReportSource.Document, stream);
                    a.Export(report.Document, stream);
					stream.Position = 0;
					FCUtils.DownloadAndOpen("_blank", stream, fileName);
				}
				return;
			}
			catch (Exception ex)
			{
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Excel export.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

        private void toolBar1_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if (e.Button == toolBarButtonEmailPDF)
            {
                this.mnuEmailPDF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonEmailRTF)
            {
                this.mnuEMailRTF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportPDF)
            {
                this.mnuFileExportPDF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportRTF)
            {
                this.mnuFileExportRTF_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportHTML)
            {
                this.mnuFileExportHTML_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == toolBarButtonExportExcel)
            {
                this.mnuFileExportExcel_Click(e.Button, EventArgs.Empty);
            }
            else if (e.Button == this.toolBarButtonPrint)
            {
                if (this.arView.ReportSource != null)
                {
                    this.arView.ReportSource?.PrintReport();
                }
                else if(!String.IsNullOrEmpty(arView.ReportName))
                {
                    FCSectionReport report = new FCSectionReport();
                    report.Document.Load(arView.ReportName);
                    report.ExportToPDFAndPrint();
                }
            }
        }
    }
}
