﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmSetupDisconnectNotices.
	/// </summary>
	public partial class frmSetupDisconnectNotices : BaseForm
	{
		public frmSetupDisconnectNotices()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.chkSendTo = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
			this.chkSendTo.AddControlArrayElement(chkSendTo_0, 0);
			this.chkSendTo.AddControlArrayElement(chkSendTo_1, 1);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupDisconnectNotices InstancePtr
		{
			get
			{
				return (frmSetupDisconnectNotices)Sys.GetInstance(typeof(frmSetupDisconnectNotices));
			}
		}

		protected frmSetupDisconnectNotices _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               07/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/23/2007              *
		// ********************************************************
		public int lngColQuestion;
		public int lngColHidden;
		public int lngColData;
		public int lngRowMuniName;
		public int lngRowPhone;
		public int lngRowHours;
		public int lngRowRateKey;
		public int lngRowGovtContact;
		public int lngRowGovtPhone;
		public string strRateKeyList = string.Empty;
		public string strDefaultRateKeyList = string.Empty;
		public string strOldPrinter = "";

		private void FormatGrid_2(bool boolResize)
		{
			FormatGrid(boolResize);
		}

		private void FormatGrid(bool boolResize = false)
		{
			int lngWid = 0;
			// This will size and format the grid
			vsDefaults.FixedRows = 0;
			vsDefaults.Rows = 6;
			// 4 kgk 11-18-2011 trout-758
			vsDefaults.Cols = 3;
			lngWid = vsDefaults.WidthOriginal;
			vsDefaults.ColWidth(lngColQuestion, FCConvert.ToInt32(lngWid * 0.48));
			vsDefaults.ColWidth(lngColHidden, 0);
			vsDefaults.ColWidth(lngColData, FCConvert.ToInt32(lngWid * 0.47));
			if (boolResize)
			{
				return;
			}
			vsDefaults.TextMatrix(lngRowMuniName, lngColQuestion, "Town or City Name");
			// MAL@20070831
			vsDefaults.TextMatrix(lngRowMuniName, lngColData, modGlobalConstants.Statics.MuniName);
			vsDefaults.TextMatrix(lngRowPhone, lngColQuestion, "Phone");
			vsDefaults.TextMatrix(lngRowHours, lngColQuestion, "Hours");
			vsDefaults.TextMatrix(lngRowRateKey, lngColQuestion, "Outstanding Rate Key");
			vsDefaults.TextMatrix(lngRowGovtContact, lngColQuestion, "Local Gov't Contact");
			// kgk 11-18-2011
			vsDefaults.TextMatrix(lngRowGovtPhone, lngColQuestion, "Local Gov't Phone");
			// ", between the hours of 8 a.m. and 3 p.m.
			SetDefaults();
			strRateKeyList = BuildRateKeyList();
		}

		private void chkIncludeSewer_CheckedChanged(object sender, System.EventArgs e)
		{
			if (chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked)
			{
				fraIncludeSewer.Enabled = true;
			}
			else
			{
				fraIncludeSewer.Enabled = false;
				chkSeperateSewer.CheckState = Wisej.Web.CheckState.Unchecked;
			}
		}

		private void chkSendTo_CheckedChanged(short Index, object sender, System.EventArgs e)
		{
			// Select Case Index
			// Case 0
			// If chkSendTo(0).Value = vbChecked Then
			// chkSendTo(1).Caption = "Owner (if different)"
			// Else
			// chkSendTo(1).Caption = "Owner "
			// End If
			// Case 1
			// End Select
		}

		private void chkSendTo_CheckedChanged(object sender, System.EventArgs e)
		{
			short index = chkSendTo.GetIndex((FCCheckBox)sender);
			chkSendTo_CheckedChanged(index, sender, e);
		}

		private void frmSetupDisconnectNotices_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupDisconnectNotices_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupDisconnectNotices properties;
			//frmSetupDisconnectNotices.FillStyle	= 0;
			//frmSetupDisconnectNotices.ScaleWidth	= 9045;
			//frmSetupDisconnectNotices.ScaleHeight	= 8085;
			//frmSetupDisconnectNotices.LinkTopic	= "Form2";
			//frmSetupDisconnectNotices.LockControls	= true;
			//frmSetupDisconnectNotices.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				lngColQuestion = 0;
				lngColHidden = 1;
				lngColData = 2;
				lngRowMuniName = 0;
				lngRowHours = 3;
				lngRowPhone = 2;
				lngRowRateKey = 1;
				lngRowGovtContact = 4;
				lngRowGovtPhone = 5;
				FormatGrid();
				rsInfo.OpenRecordset("SELECT * FROM DisconnectionInfo");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					if (Information.IsDate(rsInfo.Get_Fields("NoticeDate")))
					{
						txtNoticeDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("NoticeDate"), "MM/dd/yyyy");
					}
					if (Information.IsDate(rsInfo.Get_Fields("ShutOffDate")))
					{
						txtShutOffDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("ShutOffDate"), "MM/dd/yyyy");
					}
					txtName.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
					txtAddress1.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Address1")));
					txtAddress2.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Address2")));
					txtAddress3.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Address3")));
					txtAddress4.Text = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Address4")));
					// kgk 11-18-2011 trout-758
					// TODO Get_Fields: Field [ReconnectFeeReg] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsInfo.Get_Fields("ReconnectFeeReg")) != 0)
					{
						// TODO Get_Fields: Field [ReconnectFeeReg] not found!! (maybe it is an alias?)
						txtReconFeeReg.Text = Strings.Format(rsInfo.Get_Fields("ReconnectFeeReg"), "#.00");
					}
					else
					{
						txtReconFeeReg.Text = "";
					}
					// TODO Get_Fields: Field [ReconnectFeeAH] not found!! (maybe it is an alias?)
					if (Conversion.Val(rsInfo.Get_Fields("ReconnectFeeAH")) != 0)
					{
						// TODO Get_Fields: Field [ReconnectFeeAH] not found!! (maybe it is an alias?)
						txtReconFeeAH.Text = Strings.Format(rsInfo.Get_Fields("ReconnectFeeAH"), "#.00");
					}
					else
					{
						txtReconFeeAH.Text = "";
					}
					// kgk 12-19-2011 trout-770  Add option to show amount due for selected rate key or total due
					cmbTotalDue.Text = "Total Due for Selected Rate Key";
					//optTotalDue[0].Checked = true;
					//optTotalDue[1].Checked = false;
					//FC:FINAL:DDU:#i1031 - get rid of this one, giving error as in original
					//if (FCConvert.ToBoolean(rsInfo.Get_Fields("ShowDepositReqd")))
					//{
					//    chkIncludeDepositText.CheckState = Wisej.Web.CheckState.Checked;
					//}
					//else
					//{
					chkIncludeDepositText.CheckState = Wisej.Web.CheckState.Unchecked;
					//}
					// TODO Get_Fields: Field [LocalGovtContact] not found!! (maybe it is an alias?)
					vsDefaults.TextMatrix(lngRowGovtContact, lngColData, Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("LocalGovtContact"))));
					// TODO Get_Fields: Field [LocalGovtPhone] not found!! (maybe it is an alias?)
					vsDefaults.TextMatrix(lngRowGovtPhone, lngColData, Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("LocalGovtPhone"))));
				}
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);
				// MAL@20070831: Setup exception to color scheme to allow Include Sewer title to be blue
				chkIncludeSewer.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
				// FormatGrid
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmSetupDisconnectNotices_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSetupDisconnectNotices_Resize(object sender, System.EventArgs e)
		{
			FormatGrid_2(true);
		}

		private void mnuFileProcess_Click(object sender, System.EventArgs e)
		{
			StartProcess();
		}

		private bool StartProcess()
		{
			bool StartProcess = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int[] lngFake = null;
				string strPrinter = "";
				int NumFonts = 0;
				bool boolUseFont = false;
				int intCPI = 0;
				int X;
				string strFont = "";
				clsDRWrapper rsInfo = new clsDRWrapper();
				vsDefaults.Select(0, 0);
				if (!Information.IsDate(txtNoticeDate.Text))
				{
					MessageBox.Show("You must enter a valid notice date before you may continue.", "Invalid Notice Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtNoticeDate.Focus();
					return StartProcess;
				}
				else if (DateAndTime.DateValue(txtNoticeDate.Text).ToOADate() < DateTime.Today.ToOADate())
				{
					// allow this to occur but warn them
					if (DateAndTime.DateDiff("D", DateAndTime.DateValue(txtNoticeDate.Text), DateTime.Today) > 30)
					{
						MessageBox.Show("You have entered a notice date that is more than a month earlier than todays date, you must enter a later date before you can continue.", "Date Check", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtNoticeDate.Focus();
						return StartProcess;
					}
					else
					{
						MessageBox.Show("You have entered a notice date that is earlier than todays date.", "Date Check", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
					// txtNoticeDate.SetFocus
					// Exit Function
				}
				if (!Information.IsDate(txtShutOffDate.Text))
				{
					MessageBox.Show("You must enter a valid shut off date before you may continue.", "Invalid Shut Off Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtShutOffDate.Focus();
					return StartProcess;
				}
				else if (DateAndTime.DateValue(txtShutOffDate.Text).ToOADate() < DateTime.Today.ToOADate())
				{
					MessageBox.Show("You must enter a shut off date that is not earlier than todays date before you may continue.", "Invalid Shut Off Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtShutOffDate.Focus();
					return StartProcess;
				}
				if (Conversion.Val(txtMinimumAmount.Text) == 0)
				{
					txtMinimumAmount.Text = "0.00";
				}
				// MAL@20070925: Not used
				// Call Reference: 117182
				// If optNewMailer Then       '3 5/8 Mailer
				// If Not IsDate(txtPayByDate.Text) Then
				// MsgBox "You must enter a valid pay by date before you may continue.", vbInformation, "Invalid Pay By Date"
				// txtPayByDate.SetFocus
				// Exit Function
				// ElseIf CDate(txtPayByDate.Text) < Date Then
				// MsgBox "You must enter a pay by date that is not earlier than todays date before you may continue.", vbInformation, "Invalid Pay By Date"
				// txtPayByDate.SetFocus
				// Exit Function
				// End If
				// End If
				if (cmbAll.SelectedIndex == 2)
				{
					if (Conversion.Val(txtAccountNumber.Text) == 0)
					{
						MessageBox.Show("You must enter an account number before you may continue.", "Invalid Account Number", MessageBoxButtons.OK, MessageBoxIcon.Information);
						txtAccountNumber.Focus();
						return StartProcess;
					}
				}
				rsInfo.OpenRecordset("SELECT * FROM DisconnectionInfo");
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					rsInfo.Edit();
				}
				else
				{
					rsInfo.AddNew();
				}
				if (Information.IsDate(txtNoticeDate.Text))
				{
					rsInfo.Set_Fields("NoticeDate", DateAndTime.DateValue(txtNoticeDate.Text));
				}
				if (Information.IsDate(txtShutOffDate.Text))
				{
					rsInfo.Set_Fields("ShutOffDate", DateAndTime.DateValue(txtShutOffDate.Text));
				}
				rsInfo.Set_Fields("Name", Strings.Trim(txtName.Text));
				rsInfo.Set_Fields("Address1", Strings.Trim(txtAddress1.Text));
				rsInfo.Set_Fields("Address2", Strings.Trim(txtAddress2.Text));
				rsInfo.Set_Fields("Address3", Strings.Trim(txtAddress3.Text));
				rsInfo.Set_Fields("Address4", Strings.Trim(txtAddress4.Text));
				// kgk 11-18-2011 trout-758
				rsInfo.Set_Fields("ReconnectFeeReg", FCConvert.ToString(Conversion.Val(txtReconFeeReg.Text)));
				rsInfo.Set_Fields("ReconnectFeeAH", FCConvert.ToString(Conversion.Val(txtReconFeeAH.Text)));
				rsInfo.Set_Fields("LocalGovtContact", Strings.Trim(vsDefaults.TextMatrix(lngRowGovtContact, lngColData)));
				rsInfo.Set_Fields("LocalGovtPhone", Strings.Trim(vsDefaults.TextMatrix(lngRowGovtPhone, lngColData)));
				rsInfo.Set_Fields("ShowDepositReqd", FCConvert.CBool(chkIncludeDepositText.CheckState == Wisej.Web.CheckState.Checked));
				rsInfo.Update();
				SaveDefaults();
				switch (cmbAll.SelectedIndex)
                {
                    case 1:
                        StartProcess = true;
                        frmBookChoice.InstancePtr.Init(18);

                        break;
                    case 0:
                        switch (cmbNewMailer.Text)
                        {
                            case "Full Page Plain Paper":
                                StartProcess = true;
                                rptDisconnectNoticeOriginalPlainPaper2.InstancePtr.Init("A", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, vsDefaults.TextMatrix(lngRowPhone, lngColData), FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))), vsDefaults.TextMatrix(lngRowHours, lngColData), txtDefaultText.Text, vsDefaults.TextMatrix(lngRowMuniName, lngColData), Conversion.Val(txtReconFeeReg.Text), Conversion.Val(txtReconFeeAH.Text), vsDefaults.TextMatrix(lngRowGovtContact, lngColData), vsDefaults.TextMatrix(lngRowGovtPhone, lngColData), FCConvert.CBool(chkIncludeDepositText.CheckState == Wisej.Web.CheckState.Checked));

                                break;
                            case "Original Plain Paper":
                                StartProcess = true;
                                rptDisconnectNoticeOriginalPlainPaper.InstancePtr.Init("A", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));

                                break;
                            case "Original Mailer":
                            {
                                Information.Err().Clear();
                                strOldPrinter = FCGlobal.Printer.DeviceName;
                               
                                NumFonts = FCGlobal.Printer.FontCount;
                                intCPI = 10;
                                for (X = 0; X <= NumFonts - 1; X++)
                                {
                                    strFont = FCGlobal.Printer.Fonts[X];
                                    ;
                                    if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
                                    {
                                        strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
                                        if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
                                        {
                                            boolUseFont = true;
                                            strFont = FCGlobal.Printer.Fonts[X];
                                            break;
                                        }
                                    }
                                }
                                // X
                                // SHOW THE REPORT
                                if (!boolUseFont)
                                    strFont = "";
                                if (DialogResult.Yes == modPrinterFunctions.PrintXsForAlignment("The X should have printed at the top right of your mailer directly above the right line of the FROM box.", 38, 1, strPrinter))
                                {
                                    StartProcess = true;
                                    rptDisconnectNoticeOriginalMailer.InstancePtr.Init("A", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));
                                }

                                break;
                            }
                            case "Optional Mailer 7in":
                            {
                                Information.Err().Clear();
                                strOldPrinter = FCGlobal.Printer.DeviceName;
                                
                                NumFonts = FCGlobal.Printer.FontCount;
                                boolUseFont = false;
                                intCPI = 10;
                                for (X = 0; X <= NumFonts - 1; X++)
                                {
                                    strFont = FCGlobal.Printer.Fonts[X];

                                    if (Strings.UCase(Strings.Right(strFont, 3)) != "CPI") continue;

                                    strFont = Strings.Mid(strFont, 1, strFont.Length - 3);

                                    if (Conversion.Val(Strings.Right(strFont, 2)) != intCPI && Conversion.Val(Strings.Right(strFont, 3)) != intCPI) 
                                        continue;

                                    boolUseFont = true;
                                    strFont = FCGlobal.Printer.Fonts[X];
                                    
                                    break;
                                }
                                // X
                                // SHOW THE REPORT
                                if (!boolUseFont)
                                    strFont = "";
                                if (DialogResult.Yes == modPrinterFunctions.PrintXsForAlignment("X's should have printed at the top of the address box of your mailer.", 1, 46, strPrinter))
                                {
                                    StartProcess = true;
                                    rptDisconnectNoticeMailer7InchMF.InstancePtr.Init("A", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));
                                }

                                break;
                            }
                            default:
                            {
                                Information.Err().Clear();
                                strOldPrinter = FCGlobal.Printer.DeviceName;
                                
                                NumFonts = FCGlobal.Printer.FontCount;
                                intCPI = 10;
                                for (X = 0; X <= NumFonts - 1; X++)
                                {
                                    strFont = FCGlobal.Printer.Fonts[X];

                                    if (Strings.UCase(Strings.Right(strFont, 3)) != "CPI") 
                                        continue;

                                    strFont = Strings.Mid(strFont, 1, strFont.Length - 3);

                                    if (Conversion.Val(Strings.Right(strFont, 2)) != intCPI && Conversion.Val(Strings.Right(strFont, 3)) != intCPI) 
                                        continue;

                                    boolUseFont = true;
                                    strFont = FCGlobal.Printer.Fonts[X];
                                    break;
                                }
                                // X
                                // SHOW THE REPORT
                                if (!boolUseFont)
                                    strFont = "";
                                if (DialogResult.Yes == modPrinterFunctions.PrintXsForAlignment("The X should have printed at the top right of your mailer directly above the right line of the FROM box.", 38, 1, strPrinter))
                                {
                                    StartProcess = true;
                                    rptDisconnectNoticeNewMailer.InstancePtr.Init("A", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));
                                }

                                break;
                            }
                        }

                        break;
                    default:
                        switch (cmbNewMailer.Text)
                        {
                            case "Full Page Plain Paper":
                                StartProcess = true;
                                rptDisconnectNoticeOriginalPlainPaper2.InstancePtr.Init("S", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, vsDefaults.TextMatrix(lngRowPhone, lngColData), FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))), vsDefaults.TextMatrix(lngRowHours, lngColData), txtDefaultText.Text, vsDefaults.TextMatrix(lngRowMuniName, lngColData), Conversion.Val(txtReconFeeReg.Text), Conversion.Val(txtReconFeeAH.Text), vsDefaults.TextMatrix(lngRowGovtContact, lngColData), vsDefaults.TextMatrix(lngRowGovtPhone, lngColData), FCConvert.CBool(chkIncludeDepositText.CheckState == Wisej.Web.CheckState.Checked));

                                break;
                            case "Original Plain Paper":
                                StartProcess = true;
                                rptDisconnectNoticeOriginalPlainPaper.InstancePtr.Init("S", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));

                                break;
                            case "Original Mailer":
                            {
                                Information.Err().Clear();
                                strOldPrinter = FCGlobal.Printer.DeviceName;
                            
                                NumFonts = FCGlobal.Printer.FontCount;
                                intCPI = 10;
                                for (X = 0; X <= NumFonts - 1; X++)
                                {
                                    strFont = FCGlobal.Printer.Fonts[X];
                                    ;
                                    if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
                                    {
                                        strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
                                        if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
                                        {
                                            boolUseFont = true;
                                            strFont = FCGlobal.Printer.Fonts[X];
                                            ;
                                            break;
                                        }
                                    }
                                }
                                // X
                                // SHOW THE REPORT
                                if (!boolUseFont)
                                    strFont = "";
                                if (modPrinterFunctions.PrintXsForAlignment("The X should have printed at the top right of your mailer directly above the right line of the FROM box.", 38, 1, strPrinter) == DialogResult.Yes)
                                {
                                    StartProcess = true;
                                    rptDisconnectNoticeOriginalMailer.InstancePtr.Init("S", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));
                                }

                                break;
                            }
                            case "Outprint File":
                                modUTLien.DisconnectOutPrint(ref lngFake);

                                break;
                            case "Optional Mailer 7in":
                            {
                                Information.Err().Clear();
                                strOldPrinter = FCGlobal.Printer.DeviceName;
                            
                                NumFonts = FCGlobal.Printer.FontCount;
                                boolUseFont = false;
                                intCPI = 10;
                                for (X = 0; X <= NumFonts - 1; X++)
                                {
                                    strFont = FCGlobal.Printer.Fonts[X];

                                    if (Strings.UCase(Strings.Right(strFont, 3)) != "CPI") 
                                        continue;

                                    strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
                                    if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
                                    {
                                        boolUseFont = true;
                                        strFont = FCGlobal.Printer.Fonts[X];
                                        ;
                                        break;
                                    }
                                }
                                // X
                                // SHOW THE REPORT
                                if (!boolUseFont)
                                    strFont = "";
                                if (DialogResult.Yes == modPrinterFunctions.PrintXsForAlignment("X's should have printed above the line 'PLEASE GIVE THIS MATTER YOUR IMMEDIATE ATTENTION' on your mailer.", 1, 46, strPrinter))
                                {
                                    StartProcess = true;
                                    rptDisconnectNoticeMailer7InchMF.InstancePtr.Init("S", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));
                                }

                                break;
                            }
                            default:
                            {
                                Information.Err().Clear();
                                strOldPrinter = FCGlobal.Printer.DeviceName;
                            
                                NumFonts = FCGlobal.Printer.FontCount;
                                boolUseFont = false;
                                intCPI = 10;
                                for (X = 0; X <= NumFonts - 1; X++)
                                {
                                    strFont = FCGlobal.Printer.Fonts[X];

                                    if (Strings.UCase(Strings.Right(strFont, 3)) != "CPI") 
                                        continue;

                                    strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
                                    if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
                                    {
                                        boolUseFont = true;
                                        strFont = FCGlobal.Printer.Fonts[X];
                                        break;
                                    }
                                }
                                // X
                                // SHOW THE REPORT
                                if (!boolUseFont)
                                    strFont = "";
                                if (DialogResult.Yes == modPrinterFunctions.PrintXsForAlignment("The X should have printed at the top right of your mailer directly above the right line of the FROM box.", 38, 1, strPrinter))
                                {
                                    StartProcess = true;
                                    rptDisconnectNoticeNewMailer.InstancePtr.Init("S", DateAndTime.DateValue(txtNoticeDate.Text), DateAndTime.DateValue(txtShutOffDate.Text), FCConvert.ToDecimal(txtMinimumAmount.Text), chkIncludeSewer.CheckState == Wisej.Web.CheckState.Checked, chkSeperateSewer.CheckState == Wisej.Web.CheckState.Checked, Strings.Trim(txtName.Text), Strings.Trim(txtAddress1.Text), Strings.Trim(txtAddress2.Text), Strings.Trim(txtAddress3.Text), Strings.Trim(txtAddress4.Text), 0, ref lngFake, ref strPrinter, ref strFont, FCConvert.ToInt32(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, lngColData))));
                                }

                                break;
                            }
                        }

                        break;
                }
				return StartProcess;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Process", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return StartProcess;
		}

		private void mnuFileReminderNotices_Click(object sender, System.EventArgs e)
		{
			strDefaultRateKeyList = BuildDefaultRateKeyList();
			frmRateRecChoice.InstancePtr.Unload();
			// kk07142015 trout-867 / trout-1154   Force to reload for regular bills only
			frmRateRecChoice.InstancePtr.Init(30, "", true, strDefaultRateKeyList);
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedBooks.Enabled = false;
			txtSequenceNumber.Text = "";
			fraSelectedAccount.Enabled = false;
			txtAccountNumber.Text = "";
		}
		

		private void optSelectedAccount_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedBooks.Enabled = false;
			txtSequenceNumber.Text = "";
			fraSelectedAccount.Enabled = true;
			txtAccountNumber.Focus();
		}

		private void optSelectedBooks_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelectedBooks.Enabled = true;
			txtSequenceNumber.Focus();
			fraSelectedAccount.Enabled = false;
			txtAccountNumber.Text = "";
		}

		private void txtAccountNumber_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// If Val(txtAccountNumber.Text) <> 0 Then
			// Select Case MsgBox("Would you like to print the notice now?", vbQuestion + vbYesNoCancel, "Print Notice")
			// Case vbYes
			// run the report and then return to this screen after
			// If StartProcess Then
			// DoEvents
			// txtAccountNumber.SelStart = 0
			// txtAccountNumber.SelLength = Len(txtAccountNumber.Text)
			// Me.Hide
			// End If
			// Case Else
			// allow the user to leave
			// End Select
			// Else
			// clear this field
			// txtAccountNumber.Text = ""
			// End If
		}

		private string BuildRateKeyList()
		{
			string BuildRateKeyList = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRK = new clsDRWrapper();
				rsRK.OpenRecordset("SELECT ID, BillDate, Description FROM RateKeys WHERE RateType = 'R' ORDER BY ID desc", modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Loading Rate Keys", true, rsRK.RecordCount(), true);
				while (!rsRK.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					BuildRateKeyList = BuildRateKeyList + rsRK.Get_Fields_Int32("ID") + "\t" + Strings.Format(rsRK.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "\t" + rsRK.Get_Fields_String("Description") + "|";
					rsRK.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				return BuildRateKeyList;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building Rate Key", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildRateKeyList;
		}

		private string BuildDefaultRateKeyList()
		{
			string BuildDefaultRateKeyList = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRK = new clsDRWrapper();
				if (Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, 2)) > 0)
				{
					rsRK.OpenRecordset("SELECT TOP 50 ID, BillDate, Description FROM RateKeys WHERE RateType = 'R' AND ID <= " + FCConvert.ToString(Conversion.Val(vsDefaults.TextMatrix(lngRowRateKey, 2))) + " ORDER BY ID desc", modExtraModules.strUTDatabase);
				}
				else
				{
					rsRK.OpenRecordset("SELECT TOP 50 ID, BillDate, Description FROM RateKeys WHERE RateType = 'R' ORDER BY ID desc", modExtraModules.strUTDatabase);
				}
				frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Loading Rate Keys", true, rsRK.RecordCount(), true);
				while (!rsRK.EndOfFile())
				{
					frmWait.InstancePtr.IncrementProgress();
					//Application.DoEvents();
					BuildDefaultRateKeyList = BuildDefaultRateKeyList + rsRK.Get_Fields_Int32("ID") + ",";
					rsRK.MoveNext();
				}
				frmWait.InstancePtr.Unload();
				return BuildDefaultRateKeyList;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building Default Rate Key", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildDefaultRateKeyList;
		}

		private void vsDefaults_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			vsDefaults.ComboList = "";
			if (vsDefaults.Col == lngColData)
			{
				if (vsDefaults.Row == lngRowHours || vsDefaults.Row == lngRowMuniName || vsDefaults.Row == lngRowPhone)
				{
				}
				else if (vsDefaults.Row == lngRowRateKey)
				{
					vsDefaults.ComboList = strRateKeyList;
				}
			}
		}

		private void vsDefaults_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsDefaults[e.ColumnIndex, e.RowIndex];
            int lngMR;
			lngMR = vsDefaults.GetFlexRowIndex(e.RowIndex);
			if (lngMR == lngRowRateKey)
			{
				//ToolTip1.SetToolTip(vsDefaults, "The is the newest rate key to check.");
				cell.ToolTipText = "The is the newest rate key to check.";
			}
			else
			{
                //ToolTip1.SetToolTip(vsDefaults, "");
                cell.ToolTipText = "";
			}
		}

		private void vsDefaults_RowColChange(object sender, System.EventArgs e)
		{
			if (vsDefaults.Col == lngColData)
			{
				vsDefaults.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			}
			else
			{
				vsDefaults.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void SetDefaults()
		{
			string strTemp = "";
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectMuniName", ref strTemp);
			vsDefaults.TextMatrix(lngRowMuniName, lngColData, strTemp);
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectHours", ref strTemp);
			vsDefaults.TextMatrix(lngRowHours, lngColData, strTemp);
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectRateKey", ref strTemp);
			vsDefaults.TextMatrix(lngRowRateKey, lngColData, strTemp);
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectPhone", ref strTemp);
			vsDefaults.TextMatrix(lngRowPhone, lngColData, strTemp);
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectDefaultText", ref strTemp);
			txtDefaultText.Text = strTemp;
		}

		private void SaveDefaults()
		{
			string strTemp = "";
			strTemp = vsDefaults.TextMatrix(lngRowMuniName, lngColData);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectMuniName", strTemp);
			strTemp = vsDefaults.TextMatrix(lngRowHours, lngColData);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectHours", strTemp);
			strTemp = vsDefaults.TextMatrix(lngRowRateKey, lngColData);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectRateKey", strTemp);
			strTemp = vsDefaults.TextMatrix(lngRowPhone, lngColData);
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectPhone", strTemp);
			strTemp = txtDefaultText.Text;
			modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "DisconnectDefaultText", strTemp);
		}

		private void cmbAll_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Selected Books")
			{
				optSelectedBooks_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Selected Account")
			{
				optSelectedAccount_CheckedChanged(sender, e);
			}
		}
	}
}
