﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arMeterCount.
	/// </summary>
	partial class arMeterCount
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arMeterCount));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWaterMeter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSewerMeter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldCat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.sarMissingCategoryOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.fldTotalW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterMeter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewerMeter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCat,
				this.fldDesc,
				this.fldS,
				this.fldW
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.sarMissingCategoryOb,
				this.fldTotalW,
				this.fldTotalS,
				this.Line1,
				this.fldTotalCount
			});
			this.ReportFooter.Height = 0.4375F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label2,
				this.Label3,
				this.lblWaterMeter,
				this.lblSewerMeter,
				this.Label1,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.Line2
			});
			this.PageHeader.Height = 0.7395833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label2.Text = "Category";
			this.Label2.Top = 0.5F;
			this.Label2.Width = 0.875F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 1F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label3.Text = "Description";
			this.Label3.Top = 0.5F;
			this.Label3.Width = 2.5F;
			// 
			// lblWaterMeter
			// 
			this.lblWaterMeter.Height = 0.1875F;
			this.lblWaterMeter.HyperLink = null;
			this.lblWaterMeter.Left = 3.625F;
			this.lblWaterMeter.Name = "lblWaterMeter";
			this.lblWaterMeter.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblWaterMeter.Text = "Water Meter Total";
			this.lblWaterMeter.Top = 0.5F;
			this.lblWaterMeter.Width = 1.1875F;
			// 
			// lblSewerMeter
			// 
			this.lblSewerMeter.Height = 0.1875F;
			this.lblSewerMeter.HyperLink = null;
			this.lblSewerMeter.Left = 4.875F;
			this.lblSewerMeter.Name = "lblSewerMeter";
			this.lblSewerMeter.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblSewerMeter.Text = "Sewer Meter Total";
			this.lblSewerMeter.Top = 0.5F;
			this.lblSewerMeter.Width = 1.1875F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label1.Text = "Meter Count by Category";
			this.Label1.Top = 0F;
			this.Label1.Width = 6.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.2F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.55F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.2F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 5.55F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.2F;
			this.lblPageNumber.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.6875F;
			this.Line2.Width = 6.0625F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 6.0625F;
			this.Line2.Y1 = 0.6875F;
			this.Line2.Y2 = 0.6875F;
			// 
			// fldCat
			// 
			this.fldCat.Height = 0.1875F;
			this.fldCat.Left = 0F;
			this.fldCat.Name = "fldCat";
			this.fldCat.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldCat.Text = null;
			this.fldCat.Top = 0F;
			this.fldCat.Width = 0.875F;
			// 
			// fldDesc
			// 
			this.fldDesc.Height = 0.1875F;
			this.fldDesc.Left = 1F;
			this.fldDesc.Name = "fldDesc";
			this.fldDesc.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldDesc.Text = null;
			this.fldDesc.Top = 0F;
			this.fldDesc.Width = 2.5F;
			// 
			// fldS
			// 
			this.fldS.Height = 0.1875F;
			this.fldS.Left = 5.1875F;
			this.fldS.Name = "fldS";
			this.fldS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldS.Text = null;
			this.fldS.Top = 0F;
			this.fldS.Width = 0.5625F;
			// 
			// fldW
			// 
			this.fldW.Height = 0.1875F;
			this.fldW.Left = 3.9375F;
			this.fldW.Name = "fldW";
			this.fldW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldW.Text = null;
			this.fldW.Top = 0F;
			this.fldW.Width = 0.625F;
			// 
			// sarMissingCategoryOb
			// 
			this.sarMissingCategoryOb.CloseBorder = false;
			this.sarMissingCategoryOb.Height = 0.0625F;
			this.sarMissingCategoryOb.Left = 0F;
			this.sarMissingCategoryOb.Name = "sarMissingCategoryOb";
			this.sarMissingCategoryOb.Report = null;
			this.sarMissingCategoryOb.Top = 0.375F;
			this.sarMissingCategoryOb.Width = 6.5F;
			// 
			// fldTotalW
			// 
			this.fldTotalW.Height = 0.1875F;
			this.fldTotalW.Left = 3.9375F;
			this.fldTotalW.Name = "fldTotalW";
			this.fldTotalW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalW.Text = null;
			this.fldTotalW.Top = 0.125F;
			this.fldTotalW.Width = 0.625F;
			// 
			// fldTotalS
			// 
			this.fldTotalS.Height = 0.1875F;
			this.fldTotalS.Left = 5.1875F;
			this.fldTotalS.Name = "fldTotalS";
			this.fldTotalS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalS.Text = null;
			this.fldTotalS.Top = 0.125F;
			this.fldTotalS.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.9375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 1.875F;
			this.Line1.X1 = 3.9375F;
			this.Line1.X2 = 5.8125F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 1.125F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldTotalCount.Text = "Total Count:";
			this.fldTotalCount.Top = 0.125F;
			this.fldTotalCount.Width = 2.5F;
			// 
			// arMeterCount
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterMeter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewerMeter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldW;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarMissingCategoryOb;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalS;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWaterMeter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSewerMeter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
