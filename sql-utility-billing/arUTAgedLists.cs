﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arUTAgedLists.
	/// </summary>
	public partial class arUTAgedLists : BaseSectionReport
	{
		public arUTAgedLists()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Aged Status Lists";
		}

		public static arUTAgedLists InstancePtr
		{
			get
			{
				return (arUTAgedLists)Sys.GetInstance(typeof(arUTAgedLists));
			}
		}

		protected arUTAgedLists _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsCalLien.Dispose();
				rsCalBill.Dispose();
				rsRK.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arUTAgedLists	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/08/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/04/2006              *
		// ********************************************************
		string strSQL;
		clsDRWrapper rsData = new clsDRWrapper();
		double[] dblTotals = new double[5 + 1];
		// 0 - Current Tax, 1 - 30 Day, 2 - 60 Day, 3 - 90 Day, 4 - Liens, 5 - Total
		int lngCount;
		clsDRWrapper rsCalLien = new clsDRWrapper();
		clsDRWrapper rsCalBill = new clsDRWrapper();
		clsDRWrapper rsRK = new clsDRWrapper();
		bool boolShowWater;
		bool boolShowSewer;
		DateTime dtCurrentDate;
		bool boolSummaryOnly;
		bool boolPrincipal;
		bool boolInterest;
		bool boolTaxes;
		bool boolCosts;
		bool boolPLI;
		// these are for the summaries in the report footer
		double[] dblYearTotals = new double[2000 + 1];
		// billingyear - 19800
		double[,] dblPayments = new double[11 + 1, 6 + 1];
		// 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total, 11 - Current Interest
		// 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs, 4 - Total, 5 - Tax
		private void ActiveReport_FetchData(object sender, FetchEventArgs e)
		{
			if (rsData.EndOfFile())
			{
				e.EOF = true;
			}
			else
			{
				e.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageEnd()
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				lngCount = 0;
				this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
				;
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				boolShowWater = FCConvert.CBool(Strings.UCase(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowWS, 1)) != "SEWER");
				boolShowSewer = FCConvert.CBool(Strings.UCase(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowWS, 1)) != "WATER");
				// this is what the time scale will be
				dtCurrentDate = DateTime.Today;
				// CDate("05/25/2003")
				boolSummaryOnly = FCConvert.CBool(frmUTStatusList.InstancePtr.chkSummaryOnly.CheckState == Wisej.Web.CheckState.Checked);
				boolPLI = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[4].CheckState == Wisej.Web.CheckState.Checked);
				boolPrincipal = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[0].CheckState == Wisej.Web.CheckState.Checked);
				boolTaxes = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[1].CheckState == Wisej.Web.CheckState.Checked);
				boolInterest = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[2].CheckState == Wisej.Web.CheckState.Checked);
				boolCosts = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[3].CheckState == Wisej.Web.CheckState.Checked);
				SetupFields();
				// Moves/shows the correct fields into the right places
				SetReportHeader();
				// Sets the titles and moves labels in the header
				strSQL = BuildSQL();
				// Generates the SQL String
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading", true, rsData.RecordCount());
				// Me.Printer.Orientation = ddOLandscape
				if (rsData.EndOfFile() && rsData.BeginningOfFile())
				{
					frmWait.InstancePtr.Unload();
					NoRecords();
				}
				else
				{
					// open the lien table to be used laters
					rsCalLien.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
					rsRK.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Aged List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void NoRecords()
		{
			// this will terminate the report and show a message to the user
			MessageBox.Show("There are no records matching the criteria selected.", "Status List", MessageBoxButtons.OK, MessageBoxIcon.Information);
			Cancel();
			this.Close();
			return;
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp = "";
			string strWhereClause = "";
			string strREPPBill = "";
			string strREPPPayment = "";
			string strREInnerJoin = "";
			string strAsOfDateString = "";
			string strSelectFields;
			string strBillingType = "";
			// This is an aged report using the Where Criteria to report the correct information
			strSelectFields = "m.ID, AccountNumber, pBill.FullNameLF AS Name, StreetNumber, StreetName, MapLot";
			if (Strings.Trim(frmUTStatusList.InstancePtr.strRSWhere) != "")
			{
				// kgk        strTemp = "SELECT * FROM Master WHERE " & frmUTStatusList.strRSWhere  ' INNER JOIN MeterTable ON Master.Key = Metertable.AccountKey WHERE MeterNumber = 1 AND
				strTemp = "SELECT * FROM ( SELECT m.*,pOwn.FullNameLF AS OwnerName,pOwn2.FullNameLF AS SecondOwnerName,pOwn.Address1 AS OAddress1,pOwn.Address2 AS OAddress2,pOwn.Address3 AS OAddress3,pOwn.City AS OCity,pOwn.State AS OState, pOwn.Zip AS OZip," + "       pBill.FullNameLF AS Name,pBill2.FullNameLF AS Name2,pBill.Address1 AS BAddress1,pBill.Address2 AS BAddress2,pBill.Address3 AS BAddress3,pBill.City AS BCity,pBill.State AS BState, pBill.Zip AS BZip " + "FROM Master m INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = m.OwnerPartyID INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = m.BillingPartyID " + "     LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = m.SecondOwnerPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = m.SecondBillingPartyID ) AS qMst " + "WHERE " + frmUTStatusList.InstancePtr.strRSWhere;
			}
			else
			{
				// kgk        strTemp = "SELECT * FROM Master " ' INNER JOIN MeterTable ON Master.Key = Metertable.AccountKey WHERE MeterNumber = 1
				// strTemp = "SELECT m.*,pOwn.FullNameLF AS OwnerName,pOwn2.FullNameLF AS SecondOwnerName,pOwn.Address1 AS OAddress1,pOwn.Address2 AS OAddress2,pOwn.Address3 AS OAddress3,pOwn.City AS OCity,pOwn.State AS OState, pOwn.Zip AS OZip,"
				// & "       pBill.FullNameLF AS Name,pBill2.FullNameLF AS Name2,pBill.Address1 AS BAddress1,pBill.Address2 AS BAddress2,pBill.Address3 AS BAddress3,pBill.City AS BCity,pBill.State AS BState, pBill.Zip AS BZip "
				// & "FROM Master m INNER JOIN " & strDbCP & "PartyAndAddressView pOwn ON pOwn.ID = m.OwnerPartyID INNER JOIN " & strDbCP & "PartyAndAddressView pBill ON pBill.ID = m.BillingPartyID "
				// & "     LEFT JOIN " & strDbCP & "PartyNameView pOwn2 ON pOwn2.ID = m.SecondOwnerPartyID LEFT JOIN " & strDbCP & "PartyNameView pBill2 ON pBill2.ID = m.SecondBillingPartyID "
				strTemp = "SELECT " + strSelectFields + " FROM Master m INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID ";
			}
			if (Strings.Trim(frmUTStatusList.InstancePtr.strRSOrder) != "")
			{
				strTemp += " ORDER BY " + frmUTStatusList.InstancePtr.strRSOrder;
			}
			else
			{
				strTemp += " ORDER BY Name, AccountNumber";
			}
			BuildSQL = strTemp;
			return BuildSQL;
		}

		private void SetupFields()
		{
			// Set each field and label's visible property to True/False depending on which fields the user
			// has selected from the Custom Report screen then move them accordingly
			int intRow;
			int intRow2;
			float lngHt;
			if (boolSummaryOnly)
			{
				lblLocation.Visible = false;
				fldLocation.Visible = false;
				lblMapLot.Visible = false;
				fldMapLot.Visible = false;
				fldName.Visible = false;
				fldAccount.Visible = false;
				fldCurrent.Visible = false;
				fld30Day.Visible = false;
				fld60Day.Visible = false;
				fld90Day.Visible = false;
				fldLien.Visible = false;
				fldTotal.Visible = false;
				lblAccount.Visible = false;
				lnHeader.Visible = false;
				lnTotals.Visible = false;
				Detail.Height = 0;
				return;
			}
			intRow = 2;
			lngHt = 270;
			// Location
			if (frmUTStatusList.InstancePtr.boolShowLocation)
			{
				lblLocation.Visible = true;
				fldLocation.Visible = true;
			}
			else
			{
				lblLocation.Visible = false;
				fldLocation.Visible = false;
				fldLocation.Top = 0;
			}
			if (frmUTStatusList.InstancePtr.boolShowMapLot)
			{
				lblMapLot.Visible = true;
				fldMapLot.Visible = true;
			}
			else
			{
				lblMapLot.Visible = false;
				fldMapLot.Visible = false;
				lblMapLot.Top = 0;
				fldMapLot.Top = 0;
			}
			if (frmUTStatusList.InstancePtr.boolShowLocation || frmUTStatusList.InstancePtr.boolShowMapLot)
			{
				this.Detail.Height = (2 * lngHt + 100) / 1440f;
			}
			else
			{
				this.Detail.Height = (1 * lngHt + 100) / 1440f;
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the information into the fields
				//clsDRWrapper rsPayment = new clsDRWrapper();
				int lngRK = 0;
				int lngIndex = 0;
				string strTemp = "";
				bool boolMasterInfo;
				double dblCurrentInterest;
				// this is the calculated current interest for each bill
				double[] dblDue = new double[5 + 1];
				// 0 - Current, 1 - 30 Day, 2 - 60 Day, 3 - 90 Day, 4 - Lien, 5 - Total
				string strWS = "";
				bool boolLienRec;
				double dblTotalDue = 0;
				int lngLastLRNW;
				int lngLastLRNS;
				TRYNEXTACCOUNT:
				;
				// reset fields and variables
				ClearFields();
				frmWait.InstancePtr.IncrementProgress();
				dblCurrentInterest = 0;
				dblDue[0] = 0;
				dblDue[1] = 0;
				dblDue[2] = 0;
				dblDue[3] = 0;
				dblDue[4] = 0;
				dblDue[5] = 0;
				if (rsData.EndOfFile())
				{
					return;
				}
				// get all the bills for this account in reverse order so the newest is first
				rsCalBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsData.Get_Fields_Int32("ID") + " ORDER BY BillingRateKey desc", modExtraModules.strUTDatabase);
				// BillStatus = 'B' AND   'I took this out for call #97040 07/03/2006
				if (rsCalBill.EndOfFile())
				{
					// if there are no bills
					rsData.MoveNext();
					goto TRYNEXTACCOUNT;
				}
				// Name
				fldName.Text = FCConvert.ToString(rsData.Get_Fields_String("NAME"));
				// Location
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = Strings.Trim(rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
				}
				else
				{
					fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
				}
				// MapLot
				fldMapLot.Text = Strings.Trim(rsData.Get_Fields_String("MapLot") + " ");
				// Account Number
				//FC:FINAL:MSH - can't implicitly convert from int to string
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
				dblCurrentInterest = 0;
				// If gboolUTSLDateRange Then
				// strSLPayRange = " AND (RecordedTransactionDate >= #" & gdtUTSLPaymentDate1 & "# AND RecordedTransactionDate <= #" & gdtUTSLPaymentDate2 & "#)"
				// End If
				// 
				// If (rsData.Fields(strWS & "LienRecordNumber") = 0 And Not boolShowRegular) Or (rsData.Fields(strWS & "LienRecordNumber") <> 0 And Not boolShowLien) Then
				// this will check to see if this account should be shown
				// rsData.MoveNext
				// GoTo TRYNEXTACCOUNT
				// End If
				while (!rsCalBill.EndOfFile())
				{
					// Water section of the bill
					if (FCConvert.ToString(rsCalBill.Get_Fields_String("Service")) != "S" && boolShowWater)
					{
						strWS = "W";
						dblTotalDue = 0;
						dblCurrentInterest = 0;
						if (FCConvert.ToInt32(rsCalBill.Get_Fields_Int32("BillingRateKey")) == 0)
						{
							lngIndex = 0;
							if (boolPrincipal)
							{
								dblTotalDue = rsCalBill.Get_Fields_Double("WPrinPaid") * -1;
							}
							dblDue[lngIndex] += dblTotalDue;
						}
						else if (FCConvert.ToInt32(rsCalBill.Get_Fields(strWS + "LienRecordNumber")) == 0)
						{
							// not liened
							// find the information about the payments here
							lngRK = FCConvert.ToInt32(rsCalBill.Get_Fields_Int32("BillingRateKey"));
							lngIndex = FindIndexForAgedList(ref lngRK, ref dtCurrentDate);
							if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
							{
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsCalBill, DateTime.Today, ref dblCurrentInterest, true);
							}
							// 10/24/2006
							dblTotalDue = 0;
							if (boolPrincipal)
							{
								dblTotalDue += rsCalBill.Get_Fields_Double("WPrinOwed") - rsCalBill.Get_Fields_Double("WPrinPaid");
							}
							if (boolTaxes)
							{
								dblTotalDue += rsCalBill.Get_Fields_Double("WTaxOwed") - rsCalBill.Get_Fields_Double("WTaxPaid");
							}
							// DJW@01142013 TROUT-909 Was not includin gcharged interest unless current interest was selected
							if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
							{
								if (boolInterest)
								{
									dblTotalDue += rsCalBill.Get_Fields_Double("WIntOwed") - rsCalBill.Get_Fields_Double("WIntAdded") - rsCalBill.Get_Fields_Double("WIntPaid") + dblCurrentInterest;
								}
							}
							else
							{
								if (boolInterest)
								{
									dblTotalDue += rsCalBill.Get_Fields_Double("WIntOwed") - rsCalBill.Get_Fields_Double("WIntAdded") - rsCalBill.Get_Fields_Double("WIntPaid");
									// + dblCurrentInterest
								}
							}
							if (boolCosts)
							{
								dblTotalDue += rsCalBill.Get_Fields_Double("WCostOwed") - rsCalBill.Get_Fields_Double("WCostAdded") - rsCalBill.Get_Fields_Double("WCostPaid");
							}
							dblDue[lngIndex] += dblTotalDue;
						}
						else
						{
							// lien record
							// calculate this lien
							// If lngLastLRNW <> rsCalBill.Fields(strWS & "LienRecordNumber") Then
							if (FCConvert.ToString(rsCalBill.Get_Fields_Int32("ID")) == FCConvert.ToString(rsCalBill.Get_Fields(strWS + "CombinationLienKey")))
							{
								rsCalLien.FindFirstRecord("ID", rsCalBill.Get_Fields(strWS + "LienRecordNumber"));
								if (rsCalLien.NoMatch)
								{
									boolLienRec = false;
								}
								else
								{
									boolLienRec = true;
									if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
									{
										dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsCalLien, DateTime.Today, ref dblCurrentInterest, true);
									}
									// 10/24/2006
									dblTotalDue = 0;
									if (boolPrincipal)
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
										dblTotalDue += rsCalLien.Get_Fields("Principal") - rsCalLien.Get_Fields("PrinPaid");
									}
									if (boolTaxes)
									{
										// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
										dblTotalDue += rsCalLien.Get_Fields("Tax") - rsCalLien.Get_Fields("TaxPaid");
									}
									// DJW@01142013 TROUT-909 Was not includin gcharged interest unless current interest was selected
									if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
									{
										if (boolInterest)
										{
											if (boolPLI)
											{
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												dblTotalDue += rsCalLien.Get_Fields("Interest") - rsCalLien.Get_Fields("PLIPaid") - rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid") + dblCurrentInterest;
											}
											else
											{
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												dblTotalDue += -rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid") + dblCurrentInterest;
											}
										}
										else
										{
											if (boolPLI)
											{
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												dblTotalDue += rsCalLien.Get_Fields("Interest") - rsCalLien.Get_Fields("PLIPaid") + dblCurrentInterest;
											}
											else
											{
												dblTotalDue += dblCurrentInterest;
											}
										}
									}
									else
									{
										if (boolInterest)
										{
											if (boolPLI)
											{
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												dblTotalDue += rsCalLien.Get_Fields("Interest") - rsCalLien.Get_Fields("PLIPaid") - rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid");
											}
											else
											{
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												dblTotalDue += -rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid");
											}
										}
										else
										{
											if (boolPLI)
											{
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												dblTotalDue += rsCalLien.Get_Fields("Interest") - rsCalLien.Get_Fields("PLIPaid");
											}
										}
									}
									if (boolCosts)
									{
										// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
										dblTotalDue += rsCalLien.Get_Fields("Costs") - rsCalLien.Get_Fields_Double("CostPaid") - rsCalLien.Get_Fields("MaturityFee");
									}
									dblDue[4] += dblTotalDue;
								}
								// lngLastLRNW = rsCalBill.Fields(strWS & "LienRecordNumber")
							}
						}
					}
					// Sewer section of the bill
					if (FCConvert.ToString(rsCalBill.Get_Fields_String("Service")) != "W" && boolShowSewer)
					{
						strWS = "S";
						dblTotalDue = 0;
						dblCurrentInterest = 0;
						if (FCConvert.ToInt32(rsCalBill.Get_Fields_Int32("BillingRateKey")) == 0)
						{
							lngIndex = 0;
							if (boolPrincipal)
							{
								dblTotalDue = rsCalBill.Get_Fields_Double("SPrinPaid") * -1;
							}
							dblDue[lngIndex] += dblTotalDue;
						}
						else if (FCConvert.ToInt32(rsCalBill.Get_Fields(strWS + "LienRecordNumber")) == 0)
						{
							// not liened
							// find the information about the payments here
							lngRK = FCConvert.ToInt32(rsCalBill.Get_Fields_Int32("BillingRateKey"));
							lngIndex = FindIndexForAgedList(ref lngRK, ref dtCurrentDate);
							if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
							{
								dblTotalDue = modUTCalculations.CalculateAccountUT(rsCalBill, DateTime.Today, ref dblCurrentInterest, false);
							}
							// 10/24/2006
							dblTotalDue = 0;
							if (boolPrincipal)
							{
								dblTotalDue += rsCalBill.Get_Fields_Double("SPrinOwed") - rsCalBill.Get_Fields_Double("SPrinPaid");
							}
							if (boolTaxes)
							{
								dblTotalDue += rsCalBill.Get_Fields_Double("STaxOwed") - rsCalBill.Get_Fields_Double("STaxPaid");
							}
							// DJW@01142013 TROUT-909 Was not includin gcharged interest unless current interest was selected
							if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
							{
								if (boolInterest)
								{
									dblTotalDue += rsCalBill.Get_Fields_Double("SIntOwed") - rsCalBill.Get_Fields_Double("SIntAdded") - rsCalBill.Get_Fields_Double("SIntPaid") + dblCurrentInterest;
								}
							}
							else
							{
								if (boolInterest)
								{
									dblTotalDue += rsCalBill.Get_Fields_Double("SIntOwed") - rsCalBill.Get_Fields_Double("SIntAdded") - rsCalBill.Get_Fields_Double("SIntPaid");
									// + dblCurrentInterest
								}
							}
							if (boolCosts)
							{
								dblTotalDue += rsCalBill.Get_Fields_Double("SCostOwed") - rsCalBill.Get_Fields_Double("SCostAdded") - rsCalBill.Get_Fields_Double("SCostPaid");
							}
							dblDue[lngIndex] += dblTotalDue;
						}
						else
						{
							// lien record
							// calculate this lien
							// If lngLastLRNS <> rsCalBill.Fields(strWS & "LienRecordNumber") Then
							if (FCConvert.ToString(rsCalBill.Get_Fields_Int32("ID")) == FCConvert.ToString(rsCalBill.Get_Fields(strWS + "CombinationLienKey")))
							{
								rsCalLien.FindFirstRecord("ID", rsCalBill.Get_Fields(strWS + "LienRecordNumber"));
								if (rsCalLien.NoMatch)
								{
									boolLienRec = false;
								}
								else
								{
									boolLienRec = true;
									if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
									{
										dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsCalLien, DateTime.Today, ref dblCurrentInterest, false);
									}
									// 10/24/2006
									dblTotalDue = 0;
									if (boolPrincipal)
									{
										// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
										dblTotalDue += rsCalLien.Get_Fields("Principal") - rsCalLien.Get_Fields("PrinPaid");
									}
									if (boolTaxes)
									{
										// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
										dblTotalDue += rsCalLien.Get_Fields("Tax") - rsCalLien.Get_Fields("TaxPaid");
									}
									// DJW@01142013 TROUT-909 Was not includin gcharged interest unless current interest was selected
									if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
									{
										if (boolInterest)
										{
											if (boolPLI)
											{
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												dblTotalDue += rsCalLien.Get_Fields("Interest") - rsCalLien.Get_Fields("PLIPaid") - rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid") + dblCurrentInterest;
											}
											else
											{
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												dblTotalDue += -rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid") + dblCurrentInterest;
											}
										}
										else
										{
											if (boolPLI)
											{
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												dblTotalDue += rsCalLien.Get_Fields("Interest") - rsCalLien.Get_Fields("PLIPaid") + dblCurrentInterest;
											}
											else
											{
												dblTotalDue += dblCurrentInterest;
											}
										}
									}
									else
									{
										if (boolInterest)
										{
											if (boolPLI)
											{
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												dblTotalDue += rsCalLien.Get_Fields("Interest") - rsCalLien.Get_Fields("PLIPaid") - rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid");
											}
											else
											{
												// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
												dblTotalDue += -rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid");
											}
										}
										else
										{
											if (boolPLI)
											{
												// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
												// TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
												dblTotalDue += rsCalLien.Get_Fields("Interest") - rsCalLien.Get_Fields("PLIPaid");
											}
										}
									}
									if (boolCosts)
									{
										// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
										// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
										dblTotalDue += rsCalLien.Get_Fields("Costs") - rsCalLien.Get_Fields_Double("CostPaid") - rsCalLien.Get_Fields("MaturityFee");
									}
									dblDue[4] += dblTotalDue;
								}
								// lngLastLRNS = rsCalBill.Fields(strWS & "LienRecordNumber")
							}
						}
					}
					rsCalBill.MoveNext();
				}
				// find the total
				dblDue[5] = FCUtils.Round(dblDue[0] + dblDue[1] + dblDue[2] + dblDue[3] + dblDue[4], 2);
				if (!CheckBalanceDue(ref dblDue[5]))
				{
					rsData.MoveNext();
					goto TRYNEXTACCOUNT;
				}
				// now check to make sure that this account matches the balance due criteria, if not go to the next account
				lngCount += 1;
				// fill in all of the fields
				fldCurrent.Text = Strings.Format(dblDue[0], "#,##0.00");
				fld30Day.Text = Strings.Format(dblDue[1], "#,##0.00");
				fld60Day.Text = Strings.Format(dblDue[2], "#,##0.00");
				fld90Day.Text = Strings.Format(dblDue[3], "#,##0.00");
				fldLien.Text = Strings.Format(dblDue[4], "#,##0.00");
				fldTotal.Text = Strings.Format(dblDue[5], "#,##0.00");
				// add the totals for the sum
				dblTotals[0] += dblDue[0];
				// Current
				dblTotals[1] += dblDue[1];
				// 30 Day
				dblTotals[2] += dblDue[2];
				// 60 Day
				dblTotals[3] += dblDue[3];
				// 90 Day
				dblTotals[4] += dblDue[4];
				// Lien
				dblTotals[5] += dblDue[5];
				// Total
				// move to the next record in the query
				rsData.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool CheckBalanceDue(ref double dblAmt)
		{
			bool CheckBalanceDue = false;
			// this function will return true if this amount matches the
			double dblTestAmt = 0;
			if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowBalanceDue, 2)) != "")
			{
				dblTestAmt = FCConvert.ToDouble(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowBalanceDue, 2));
				string vbPorterVar = frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowBalanceDue, 1);
				if (vbPorterVar == ">")
				{
					CheckBalanceDue = dblAmt > dblTestAmt;
				}
				else if (vbPorterVar == "<")
				{
					CheckBalanceDue = dblAmt < dblTestAmt;
				}
				else if (vbPorterVar == "<>")
				{
					CheckBalanceDue = dblAmt != dblTestAmt;
				}
				else if (vbPorterVar == "=")
				{
					CheckBalanceDue = dblAmt == dblTestAmt;
				}
				else if (vbPorterVar == "")
				{
					CheckBalanceDue = true;
				}
				else
				{
					CheckBalanceDue = false;
				}
			}
			else
			{
				CheckBalanceDue = true;
			}
			return CheckBalanceDue;
		}

		private void ClearFields()
		{
			// this routine will clear the fields
			fldAccount.Text = "";
			fldName.Text = "";
			fldCurrent.Text = "";
			fld30Day.Text = "";
			fld60Day.Text = "";
			fld90Day.Text = "";
			fldLien.Text = "";
			fldTotal.Text = "";
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the totals line at the bottom of the report
			fldTotalCurrent.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotal30Day.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotal60Day.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldTotal90Day.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotalLien.Text = Strings.Format(dblTotals[4], "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotals[5], "#,##0.00");
			if (lngCount > 1)
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Accounts:";
			}
			else if (lngCount == 1)
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Account:";
			}
			else
			{
				lblTotals.Text = "No Accounts";
			}
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";
			for (intCT = 0; intCT <= frmUTStatusList.InstancePtr.vsWhere.Rows - 1; intCT++)
			{
				if (frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) != "" || frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2) != "")
				{
					if (intCT == frmUTStatusList.InstancePtr.lngRowAccount)
					{
						// Account Number
						if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
						{
							if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
							{
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
								{
									strTemp += "Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
								}
								else
								{
									strTemp += "Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
								}
							}
							else
							{
								strTemp += "Below Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
							}
						}
						else
						{
							strTemp += "Above Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
						}
					}
					else if (intCT == frmUTStatusList.InstancePtr.lngRowName)
					{
						// Name
						if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
						{
							if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
							{
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
								{
									strTemp += " Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
								}
								else
								{
									strTemp += " Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
								}
							}
							else
							{
								strTemp += " Below Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
							}
						}
						else
						{
							strTemp += " Above Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
						}
					}
					else if (intCT == frmUTStatusList.InstancePtr.lngRowBill)
					{
						// Bill Number
						if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) != "")
						{
							if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
							{
								strTemp += " Tax Year: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
							}
							else
							{
								strTemp += " Tax Year: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
							}
						}
						else
						{
							strTemp += " Tax Year: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
						}
					}
					else if (intCT == frmUTStatusList.InstancePtr.lngRowBalanceDue)
					{
						// Balance Due
						strTemp += " Balance Due " + Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) + Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2));
					}
					else
					{
						// Exit For
					}
				}
			}
			if (Strings.Trim(strTemp) == "")
			{
				lblReportType.Text = "Complete List";
			}
			else
			{
				lblReportType.Text = strTemp;
			}
			lblReportType.Text = lblReportType.Text + " Showing :";
			if (boolPrincipal)
			{
				lblReportType.Text = lblReportType.Text + " Principal";
			}
			if (boolTaxes)
			{
				lblReportType.Text = lblReportType.Text + " Tax";
			}
			if (boolInterest)
			{
				lblReportType.Text = lblReportType.Text + " Interest";
			}
			if (boolCosts)
			{
				lblReportType.Text = lblReportType.Text + " Costs";
			}
			if (Strings.UCase(modUTStatusList.Statics.strReportType) == "ACCOUNT")
			{
				lblHeader.Text = "Collection Account Status List";
				// find the sort order
				strTemp = "";
				for (intCT = 0; intCT <= frmUTStatusList.InstancePtr.lstSort.Items.Count - 1; intCT++)
				{
					if (frmUTStatusList.InstancePtr.lstSort.Selected(intCT))
					{
						strTemp += frmUTStatusList.InstancePtr.lstSort.Items[intCT].Text + ", ";
					}
				}
				if (strTemp == "")
				{
					strTemp = "Order By: Name, Account, Year";
				}
				else
				{
					strTemp = "Order By: " + Strings.Left(strTemp, strTemp.Length - 2);
				}
				lblReportType.Text = lblReportType + "\r\n" + strTemp;
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "ABATE")
			{
				lblHeader.Text = "Abatement Status List";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "DISCOUNT")
			{
				lblHeader.Text = "Discount Status List";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "SUPPLEMENTAL")
			{
				lblHeader.Text = "Supplemental Status List";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "TAXCLUB")
			{
				lblHeader.Text = "Tax Club Status List";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "COSTS")
			{
				lblHeader.Text = "Lien Costs and 30 Day Notice List";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "REFUNDEDABATE")
			{
				lblHeader.Text = "Refunded Abatement Status List";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "PREPAYMENT")
			{
				lblHeader.Text = "Prepayment Status List";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "PAYMENTS")
			{
				lblHeader.Text = "Payment Status List";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "CORRECTIONS")
			{
				lblHeader.Text = "Correction Status List";
			}
			else
			{
				lblHeader.Text = "Status List";
			}
			if (modMain.Statics.gboolUTUseAsOfDate)
			{
				lblReportType.Text = lblReportType.Text + "\r\n" + "As Of Date: " + Strings.Format(modMain.Statics.gdtUTStatusListAsOfDate, "MM/dd/yyyy");
			}
			if (boolShowWater && boolShowSewer)
			{
				// kk trouts-6 02282013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					lblHeader.Text = "Sewer and Stormwater " + lblHeader.Text;
				}
				else
				{
					lblHeader.Text = "Water and Sewer " + lblHeader.Text;
				}
			}
			else if (boolShowWater)
			{
				// kk trouts-6 02282013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					lblHeader.Text = "Stormwater " + lblHeader.Text;
				}
				else
				{
					lblHeader.Text = "Water " + lblHeader.Text;
				}
			}
			else
			{
				lblHeader.Text = "Sewer " + lblHeader.Text;
			}
		}

		private int FindIndexForAgedList(ref int lngRateKey, ref DateTime dtCheckDate)
		{
			int FindIndexForAgedList = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// vbPorter upgrade warning: lngDays As int --> As long
				long lngDays;
				rsRK.FindFirstRecord("ID", lngRateKey);
				if (!rsRK.NoMatch)
				{
					lngDays = DateAndTime.DateDiff("D", (DateTime)rsRK.Get_Fields_DateTime("BillDate"), dtCheckDate);
					if (lngDays < 30)
					{
						// Current Taxes
						FindIndexForAgedList = 0;
					}
					else if (lngDays >= 30 && lngDays <= 59)
					{
						// 30 Days
						FindIndexForAgedList = 1;
					}
					else if (lngDays >= 60 && lngDays <= 89)
					{
						// 60 Days
						FindIndexForAgedList = 2;
					}
					else if (lngDays >= 90)
					{
						// 90 Days
						FindIndexForAgedList = 3;
					}
				}
				else
				{
					// if no RK is found then put the amount in the current category
					FindIndexForAgedList = 0;
				}
				return FindIndexForAgedList;
			}
			catch (Exception ex)
			{
				
				FindIndexForAgedList = 0;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Index", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FindIndexForAgedList;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalCurrent.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotal30Day.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotal60Day.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldTotal90Day.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotalLien.Text = Strings.Format(dblTotals[4], "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotals[5], "#,##0.00");
			if (lngCount == 1)
			{
				lblTotals.Text = "Total for 1 account:";
			}
			else
			{
				lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " accounts:";
			}
		}

		private void arUTAgedLists_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arUTAgedLists properties;
			//arUTAgedLists.Caption	= "Aged Status Lists";
			//arUTAgedLists.Icon	= "arUTAgedList.dsx":0000";
			//arUTAgedLists.Left	= 0;
			//arUTAgedLists.Top	= 0;
			//arUTAgedLists.Width	= 11880;
			//arUTAgedLists.Height	= 8595;
			//arUTAgedLists.SectionData	= "arUTAgedList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
