﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmEditRateRecordTitles.
	/// </summary>
	partial class frmEditRateRecordTitles : BaseForm
	{
		public FCGrid vsRate;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditRateRecordTitles));
			this.vsRate = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 437);
			this.BottomPanel.Size = new System.Drawing.Size(450, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsRate);
			this.ClientArea.Size = new System.Drawing.Size(450, 377);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(450, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(199, 30);
			this.HeaderText.Text = "Edit Rate Record";
			// 
			// vsRate
			// 
			this.vsRate.AllowSelection = false;
			this.vsRate.AllowUserToResizeColumns = false;
			this.vsRate.AllowUserToResizeRows = false;
			this.vsRate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsRate.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsRate.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsRate.BackColorBkg = System.Drawing.Color.Empty;
			this.vsRate.BackColorFixed = System.Drawing.Color.Empty;
			this.vsRate.BackColorSel = System.Drawing.Color.Empty;
			this.vsRate.Cols = 3;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsRate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsRate.ColumnHeadersHeight = 30;
			this.vsRate.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsRate.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsRate.DragIcon = null;
			this.vsRate.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsRate.ExtendLastCol = true;
			this.vsRate.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsRate.FrozenCols = 0;
			this.vsRate.GridColor = System.Drawing.Color.Empty;
			this.vsRate.GridColorFixed = System.Drawing.Color.Empty;
			this.vsRate.Location = new System.Drawing.Point(30, 30);
			this.vsRate.Name = "vsRate";
			this.vsRate.ReadOnly = true;
			this.vsRate.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsRate.RowHeightMin = 0;
			this.vsRate.Rows = 1;
			this.vsRate.ScrollTipText = null;
			this.vsRate.ShowColumnVisibilityMenu = false;
			this.vsRate.Size = new System.Drawing.Size(390, 317);
			this.vsRate.StandardTab = true;
			this.vsRate.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsRate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsRate.TabIndex = 0;
			this.vsRate.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRate_ValidateEdit);
			this.vsRate.CurrentCellChanged += new System.EventHandler(this.vsRate_RowColChange);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSave,
				this.mnuFileSaveExit,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 1;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save and Exit";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "acceptButton";
			this.cmdFileSave.Location = new System.Drawing.Point(139, 37);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSave.Size = new System.Drawing.Size(145, 48);
			this.cmdFileSave.TabIndex = 0;
			this.cmdFileSave.Text = "Save and Exit";
			this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// frmEditRateRecordTitles
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(450, 545);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmEditRateRecordTitles";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Rate Record Titles";
			this.Load += new System.EventHandler(this.frmEditRateRecordTitles_Load);
			this.Activated += new System.EventHandler(this.frmEditRateRecordTitles_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEditRateRecordTitles_KeyPress);
			this.Resize += new System.EventHandler(this.frmEditRateRecordTitles_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSave;
	}
}
