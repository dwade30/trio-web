//Fecher vbPorter - Version 1.0.0.90
using System.Collections.Generic;

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class cUTBillUtility
	{

		//=========================================================

		private string strUtilityType;
		private double dblInterest;
		private double dblPastDueInterest;
		private double dblCurrentDue;
		private double dblRegular;
		private double dblPastDue;
		private double dblTotalDue;
		private double dblLienAmount;
		private double dblPastDueCurrentInterest;
		private double dblPastDueCurrentLInterest;
		private double dblPreLienInterest;
		private double dblTaxOwed;
		private double dblPrincipalPaid;
		private Dictionary<int, double> PeriodDue = new/*AsNew*/ Dictionary<int, double>();
		private double dblUnitCount;
		private double dblConsumption;
		private double dblUnits;
		private double dblFlatAmount;
		private double dblAdjustment;
		private double dblConsumptionAmount;
		private List<cUTBillAdjustment> collAdjustments = new List<cUTBillAdjustment>();
		private int []aryRateTableIds = new int[5 + 1];


		public int GetRateTableID(int intRateNumber)
		{
			int GetRateTableID = 0;
			int intNumberToUse;
			intNumberToUse = intRateNumber-1;
			if (intNumberToUse<=fecherFoundation.Information.UBound(aryRateTableIds, 1) && intNumberToUse>=0) {
				GetRateTableID = aryRateTableIds[intNumberToUse];
			}
			return GetRateTableID;
		}

		public void SetRateTableID(int intRateNumber, int lngRateTableID)
		{
			int intNumberToUse;
			intNumberToUse = intRateNumber-1;
			if (intNumberToUse>=0 && intNumberToUse<=fecherFoundation.Information.UBound(aryRateTableIds, 1)) {
				aryRateTableIds[intNumberToUse] = lngRateTableID;
			}
		}

		public void AddAdjustment(string strDesc, double dblAdjust)
		{
			cUTBillAdjustment Adjust = new/*AsNew*/ cUTBillAdjustment();
			Adjust.Description = strDesc;
			Adjust.Adjustment = dblAdjust;
			collAdjustments.Add(Adjust);
		}

		public cUTBillAdjustment GetItemizedAdjustment(short intIndex)
		{
			cUTBillAdjustment GetItemizedAdjustment = null;
			if (intIndex>=0 && intIndex<=collAdjustments.Count) {
				cUTBillAdjustment itemizedAdjustment;
				itemizedAdjustment = collAdjustments[intIndex];
				GetItemizedAdjustment = itemizedAdjustment.GetCopy();
			} else {
				GetItemizedAdjustment = new cUTBillAdjustment();
			}
			return GetItemizedAdjustment;
		}

		public IEnumerable<cUTBillAdjustment> ItemizedAdjustments
		{
			get
			{
				return collAdjustments;
			}
		}

		public double ConsumptionAmount
		{
			set
			{
				dblConsumptionAmount = value;
			}

			get
			{
					double ConsumptionAmount = 0;
				ConsumptionAmount = dblConsumptionAmount;
				return ConsumptionAmount;
			}
		}



		public double Adjustment
		{
			set
			{
				dblAdjustment = value;
			}

			get
			{
					double Adjustment = 0;
				Adjustment = dblAdjustment;
				return Adjustment;
			}
		}




		public double UnitsAmount
		{
			set
			{
				dblUnits = value;
			}

			get
			{
					double UnitsAmount = 0;
				UnitsAmount = dblUnits;
				return UnitsAmount;
			}
		}




		public double FlatAmount
		{
			set
			{
				dblFlatAmount = value;
			}

			get
			{
					double FlatAmount = 0;
				FlatAmount = dblFlatAmount;
				return FlatAmount;
			}
		}



		public double Consumption
		{
			set
			{
				dblConsumption = value;
			}

			get
			{
					double Consumption = 0;
				Consumption = dblConsumption;
				return Consumption;
			}
		}



		public double UnitCount
		{
			set
			{
				dblUnitCount = value;
			}

			get
			{
					double UnitCount = 0;
				UnitCount = dblUnitCount;
				return UnitCount;
			}
		}




		// vbPorter upgrade warning: intPeriod As object	OnWrite(short)
		// vbPorter upgrade warning: 'Return' As double	OnWrite(object, short)
		public double GetPeriodDueAmount(int intPeriod)
		{
			double GetPeriodDueAmount = 0;
			if (PeriodDue.ContainsKey(intPeriod)) {
				GetPeriodDueAmount = PeriodDue[intPeriod];
			} else {
				GetPeriodDueAmount = 0;
			}
			return GetPeriodDueAmount;
		}

		public void SetPeriodDueAmount(short intPeriod, double dblAmount)
		{
			if (PeriodDue.ContainsKey(intPeriod)) {
				PeriodDue[intPeriod] = dblAmount;
			} else {
				PeriodDue.Add(intPeriod, dblAmount);
			}
		}

		public double PreLienInterest
		{
			set
			{
				dblPreLienInterest = value;
			}

			get
			{
					double PreLienInterest = 0;
				PreLienInterest = dblPreLienInterest;
				return PreLienInterest;
			}
		}



		public double TotalDue
		{
			set
			{
				dblTotalDue = value;
			}

			get
			{
					double TotalDue = 0;
				TotalDue = dblTotalDue;
				return TotalDue;
			}
		}



		public double PrincipalPaid
		{
			set
			{
				dblPrincipalPaid = value;
			}

			get
			{
					double PrincipalPaid = 0;
				PrincipalPaid = dblPrincipalPaid;
				return PrincipalPaid;
			}
		}



		public double TaxOwed
		{
			set
			{
				dblTaxOwed = value;
			}

			get
			{
					double TaxOwed = 0;
				TaxOwed = dblTaxOwed;
				return TaxOwed;
			}
		}



		public string UtilityType
		{
			set
			{
				strUtilityType = value;
			}

			get
			{
					string UtilityType = "";
				UtilityType = strUtilityType;
				return UtilityType;
			}
		}



		public double Interest
		{
			set
			{
				dblInterest = value;
			}

			get
			{
					double Interest = 0;
				Interest = dblInterest;
				return Interest;
			}
		}



		public double PastDueInterest
		{
			set
			{
				dblPastDueInterest = value;
			}

			get
			{
					double PastDueInterest = 0;
				PastDueInterest = dblPastDueInterest;
				return PastDueInterest;
			}
		}



		public double CurrentDue
		{
			set
			{
				dblCurrentDue = value;
			}

			get
			{
					double CurrentDue = 0;
				CurrentDue = dblCurrentDue;
				return CurrentDue;
			}
		}



		public double Regular
		{
			set
			{
				dblRegular = value;
			}

			get
			{
					double Regular = 0;
				Regular = dblRegular;
				return Regular;
			}
		}



		public double PastDue
		{
			set
			{
				dblPastDue = value;
			}

			get
			{
					double PastDue = 0;
				PastDue = dblPastDue;
				return PastDue;
			}
		}



		public double LienAmount
		{
			set
			{
				dblLienAmount = value;
			}

			get
			{
					double LienAmount = 0;
				LienAmount = dblLienAmount;
				return LienAmount;
			}
		}



		public double PastDueCurrentInterest
		{
			set
			{
				dblPastDueCurrentInterest = value;
			}

			get
			{
					double PastDueCurrentInterest = 0;
				PastDueCurrentInterest = dblPastDueCurrentInterest;
				return PastDueCurrentInterest;
			}
		}



		public double PastDueCurrentLienInterest
		{
			set
			{
				dblPastDueCurrentLInterest = value;
			}

			get
			{
					double PastDueCurrentLienInterest = 0;
				PastDueCurrentLienInterest = dblPastDueCurrentLInterest;
				return PastDueCurrentLienInterest;
			}
		}



		public double GetTotalPastDue()
		{
			double GetTotalPastDue = 0;
			GetTotalPastDue = PastDue+PastDueCurrentInterest+LienAmount-PastDueCurrentLienInterest;
			return GetTotalPastDue;
		}



	}
}
