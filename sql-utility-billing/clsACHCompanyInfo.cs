//Fecher vbPorter - Version 1.0.0.90

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class clsACHCompanyInfo
	{

		//=========================================================

		private string strCompanyAccount = "";
		private string strCompanyID = "";
		private string strCompanyName = "";
		private string strCompanyRT = "";
		private string strAccountType;
		private string strCompanyIDPrefix = "";
		private string strCompanyAccountType = "";

		public string AccountType
		{
			set
			{
				strAccountType = value;
			}

			get
			{
					string AccountType = "";
				AccountType = strAccountType;
				return AccountType;
			}
		}



		public string CompanyAccount
		{
			get
			{
					string CompanyAccount = "";
				CompanyAccount = strCompanyAccount;
				return CompanyAccount;
			}

			set
			{
				strCompanyAccount = value;
			}
		}



		public string CompanyID
		{
			get
			{
					string CompanyID = "";
				CompanyID = strCompanyID;
				return CompanyID;
			}

			set
			{
				strCompanyID = value;
			}
		}



		public string CompanyName
		{
			get
			{
					string CompanyName = "";
				CompanyName = strCompanyName;
				return CompanyName;
			}

			set
			{
				strCompanyName = value;
			}
		}



		public string CompanyRT
		{
			get
			{
					string CompanyRT = "";
				CompanyRT = strCompanyRT;
				return CompanyRT;
			}

			set
			{
				strCompanyRT = value;
			}
		}



		public string IDPrefix
		{
			get
			{
					string IDPrefix = "";
				IDPrefix = strCompanyIDPrefix;
				return IDPrefix;
			}

			set
			{
				strCompanyIDPrefix = value;
			}
		}




	}
}
