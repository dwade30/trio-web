﻿using System;
using Wisej.Web;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWUT0000
{
    public partial class frmImportExceptionList : BaseForm
    {
        public frmImportExceptionList()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
            vsGrid.AfterEdit += vsGrid_AfterEdit;
            this.KeyDown += FrmImportExceptionList2_KeyDown;
            this.Load += FrmImportExceptionList2_Load;
            cmdRemoveAccount.Click += CmdRemoveAccount_Click;
            vsGrid.CellBeginEdit += VsGrid_CellBeginEdit;
        }

        private void VsGrid_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            if (e.ColumnIndex == lngColAccount)
            {
                e.Cancel = true;
            }
        }

        private void CmdRemoveAccount_Click(object sender, EventArgs e)
        {
            try
            {   // On Error GoTo ErrorHandler


                // vbPorter upgrade warning: intRow As short	OnWrite(int)
                short intRow = 0;
                int lngCurrent = 0;
                string strCurrent = "";

                var answer = MessageBox.Show("Are you sure you want to delete this account number?", "Verification", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (answer == DialogResult.Yes)
                {
                    strCurrent = vsGrid.TextMatrix(vsGrid.Row, lngColKey);
                    intRow = (short)vsGrid.Row;

                    if (strCurrent != "")
                    {
                        lngCurrent = Convert.ToInt32(double.Parse(strCurrent));

                        rsExcList.Execute("DELETE FROM tblImportExceptions WHERE ID = " + FCConvert.ToString(lngCurrent), modExtraModules.strUTDatabase);

                        vsGrid.RemoveItem(intRow);
                    }
                    else
                    {
                        MessageBox.Show("No record is selected.", "No Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                return;

            }
            catch (Exception ex)
            {   // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\n" + "In delete");
            }
        }

        private void FrmImportExceptionList2_Load(object sender, EventArgs e)
        {
            try
            {   // On Error GoTo ErrorHandler

                modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
                modGlobalFunctions.SetTRIOColors(this);

                this.KeyPreview = true;

                return;

            }
            catch (Exception ex)
            {   // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\n" + "In Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void FrmImportExceptionList2_KeyDown(object sender, KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = ((int)e.KeyData) / 0x10000;

            // Private Sub txtAddACct_KeyDown(KeyCode As Integer, Shift As Integer)
            switch (KeyCode)
            {
                case Keys.Escape:
                {
                    KeyCode = (Keys)0;
                    Unload();

                    break;
                }
                case Keys.Return:
                {
                    KeyCode = (Keys)0;
                    if (!fecherFoundation.FCUtils.IsNull(txtAddAcct.Text) && txtAddAcct.Text != "")
                    {
                        if (fecherFoundation.Conversion.Val(txtAddAcct.Text) > 0)
                        {
                            if (AddAccount(txtAddAcct.Text))
                            {
                                txtAddAcct.Text = "";
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("You must enter an account number!", "No Account Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtAddAcct.Focus();
                    }

                    break;
                }
            } //end switch
        }

        private void vsGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {   // On Error GoTo ERROR_HANDLER

                if (e.RowIndex > 0)
                {

                    rsExcList.OpenRecordset("SELECT * FROM tblImportExceptions WHERE ID = " + vsGrid.TextMatrix(e.RowIndex, lngColKey), modExtraModules.strUTDatabase);
                    rsExcList.Edit();
                    rsExcList.Set_Fields("Location", vsGrid.TextMatrix(e.RowIndex, lngColLocation));
                    rsExcList.Set_Fields("Name", vsGrid.TextMatrix(e.RowIndex, lngColName));
                    rsExcList.Set_Fields("Comment", vsGrid.TextMatrix(e.RowIndex, lngColComment));
                    rsExcList.Update();

                }
            }
            catch (Exception ex)
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public static frmImportExceptionList InstancePtr
        {
            get
            {
                return (frmImportExceptionList)Sys.GetInstance(typeof(frmImportExceptionList));
            }
        }

        protected frmImportExceptionList _InstancePtr = null;

        clsDRWrapper rsExcList = new/*AsNew*/ clsDRWrapper();

        const short lngColKey = 0;
        const short lngColAccount = 1;
        const short lngColLocation = 2;
        const short lngColName = 3;
        const short lngColComment = 4;
        const short lngColDirty = 5;

        private bool AddAccount(string strAcctNum)
        {
            try
            {   // On Error GoTo ErrorHandler

                if (!IsInExceptionList(ref strAcctNum))
                {

                    rsExcList.OpenRecordset("SELECT * FROM tblImportExceptions WHERE ID = -1", modExtraModules.strUTDatabase);
                    rsExcList.AddNew();
                    rsExcList.Set_Fields("AccountNumber", strAcctNum);
                    rsExcList.Update();

                    FillGrid();
                    return true;
                }
                else
                {
                    MessageBox.Show("This number already exists in the table!", "Record Exists", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
            }
            catch (Exception ex)
            {   // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\n" + "In Add New");
                return false;
            }
        }

        public void Init()
        {
            try
            {   // On Error GoTo ErrorHandler
                FillGrid();
                this.Show(App.MainForm);
                return;

            }
            catch (Exception ex)
            {   // ErrorHandler:
                MessageBox.Show("Error Number " + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + "  " + fecherFoundation.Information.Err(ex).Description + "\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private bool FillGrid()
        {
            bool FillGrid = false;
            try
            {   // On Error GoTo ERROR_HANDLER

                // this will fill the grid with the records from the tblBadNumbers table for this account

                FillGrid = true;

                vsGrid.Rows = 1;
                vsGrid.Cols = 6;
                vsGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                vsGrid.TextMatrix(0, lngColKey, "Key");
                vsGrid.TextMatrix(0, lngColAccount, "Account");
                vsGrid.TextMatrix(0, lngColLocation, "Location");
                vsGrid.TextMatrix(0, lngColName, "Name");
                vsGrid.TextMatrix(0, lngColComment, "Comment");

                rsExcList.OpenRecordset("SELECT * FROM tblImportExceptions ORDER BY AccountNumber", modExtraModules.strUTDatabase);

                if (rsExcList.EndOfFile())
                {
                    FillGrid = false;
                    return FillGrid;
                }

                while (!rsExcList.EndOfFile())
                {
                    vsGrid.AddItem("");
                    vsGrid.TextMatrix(vsGrid.Rows - 1, lngColKey, FCConvert.ToString(rsExcList.Get_Fields("ID")));
                    vsGrid.TextMatrix(vsGrid.Rows - 1, lngColAccount, FCConvert.ToString(rsExcList.Get_Fields("AccountNumber")));
                    vsGrid.TextMatrix(vsGrid.Rows - 1, lngColLocation, FCConvert.ToString(rsExcList.Get_Fields("Location")));
                    vsGrid.TextMatrix(vsGrid.Rows - 1, lngColName, FCConvert.ToString(rsExcList.Get_Fields("Name")));
                    vsGrid.TextMatrix(vsGrid.Rows - 1, lngColComment, FCConvert.ToString(rsExcList.Get_Fields("Comment")));
                    vsGrid.TextMatrix(vsGrid.Rows - 1, lngColDirty, "N");
                    rsExcList.MoveNext();
                }

                SetGridHeight();

                return FillGrid;

            }
            catch (Exception ex)
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return FillGrid;
        }

        private void SetGridHeight()
        {
            // set the height
            int lngW;

            lngW = vsGrid.WidthOriginal;
            vsGrid.Cols = 6;

            vsGrid.ColWidth(lngColKey, 0);
            vsGrid.ColWidth(lngColAccount, Convert.ToInt32(lngW * 0.15));
            vsGrid.ColWidth(lngColLocation, Convert.ToInt32(lngW * 0.2));
            vsGrid.ColWidth(lngColName, Convert.ToInt32(lngW * 0.2));
            vsGrid.ColWidth(lngColComment, Convert.ToInt32(lngW * 0.2));
            vsGrid.ColWidth(lngColDirty, 0);

        }

        private bool IsInExceptionList(ref string strAccountNumber)
        {
            try
            {   // On Error GoTo ERROR_HANDLER

                rsExcList.OpenRecordset("SELECT ID FROM tblImportExceptions WHERE AccountNumber = '" + strAccountNumber + "'", modExtraModules.strUTDatabase);
                return rsExcList.RecordCount() > 0;
            }
            catch (Exception ex)
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                return false;
            }
        }
    }
}
