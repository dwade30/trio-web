﻿//Fecher vbPorter - Version 1.0.0.40
using Global;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReceiptInput.
	/// </summary>
	partial class frmReceiptInput : BaseForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
                fecherFoundation.Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(frmReceiptInput));
			this.SuspendLayout();
			//
			// frmReceiptInput
			//
			this.ClientSize = new System.Drawing.Size(316, 208);
			this.Name = "frmReceiptInput";
			this.BackColor = System.Drawing.SystemColors.Control;
			this.MinimizeBox = true;
			this.MaximizeBox = true;
			this.BackgroundImageLayout = Wisej.Web.ImageLayout.None;
			//this.Icon = ((System.Drawing.Icon)(resources.GetObject("frmReceiptInput.Icon")));
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Form1";
		}
		#endregion
	}
}
