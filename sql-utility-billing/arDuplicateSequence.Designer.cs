﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arDuplicateSequence.
	/// </summary>
	partial class arDuplicateSequence
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arDuplicateSequence));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblSequence = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMeterNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeq = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMeterNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldSeq,
				this.fldBook,
				this.fldMeterNumber,
				this.fldAccount,
				this.fldLocation
			});
			this.Detail.Height = 0.1666667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldFooter
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblSequence,
				this.lblBook,
				this.lblAccount,
				this.lblMeterNumber,
				this.lblHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.lblLocation
			});
			this.PageHeader.Height = 0.6875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblSequence
			// 
			this.lblSequence.Height = 0.1875F;
			this.lblSequence.HyperLink = null;
			this.lblSequence.Left = 1.1875F;
			this.lblSequence.Name = "lblSequence";
			this.lblSequence.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblSequence.Text = "Sequence";
			this.lblSequence.Top = 0.5F;
			this.lblSequence.Width = 1F;
			// 
			// lblBook
			// 
			this.lblBook.Height = 0.1875F;
			this.lblBook.HyperLink = null;
			this.lblBook.Left = 0.1875F;
			this.lblBook.Name = "lblBook";
			this.lblBook.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblBook.Text = "Book";
			this.lblBook.Top = 0.5F;
			this.lblBook.Width = 1F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 2.1875F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.5F;
			this.lblAccount.Width = 0.9375F;
			// 
			// lblMeterNumber
			// 
			this.lblMeterNumber.Height = 0.1875F;
			this.lblMeterNumber.HyperLink = null;
			this.lblMeterNumber.Left = 3.125F;
			this.lblMeterNumber.Name = "lblMeterNumber";
			this.lblMeterNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblMeterNumber.Text = "Meter";
			this.lblMeterNumber.Top = 0.5F;
			this.lblMeterNumber.Width = 0.8125F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Duplicate Sequence List";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 6.8125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.3125F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.5F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 5.3125F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.5F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 4.1875F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left; ddo-" + "char-set: 0";
			this.lblLocation.Text = "Location";
			this.lblLocation.Top = 0.5F;
			this.lblLocation.Width = 2.375F;
			// 
			// fldSeq
			// 
			this.fldSeq.Height = 0.1875F;
			this.fldSeq.Left = 1.1875F;
			this.fldSeq.Name = "fldSeq";
			this.fldSeq.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSeq.Text = null;
			this.fldSeq.Top = 0F;
			this.fldSeq.Width = 1F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0.1875F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldBook.Text = null;
			this.fldBook.Top = 0F;
			this.fldBook.Width = 1F;
			// 
			// fldMeterNumber
			// 
			this.fldMeterNumber.Height = 0.1875F;
			this.fldMeterNumber.Left = 3.125F;
			this.fldMeterNumber.Name = "fldMeterNumber";
			this.fldMeterNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldMeterNumber.Text = null;
			this.fldMeterNumber.Top = 0F;
			this.fldMeterNumber.Width = 0.8125F;
			// 
			// fldAccount
			// 
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 2.1875F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.9375F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 4.1875F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0F;
			this.fldLocation.Width = 2.625F;
			// 
			// fldFooter
			// 
			this.fldFooter.Height = 0.1875F;
			this.fldFooter.Left = 0F;
			this.fldFooter.Name = "fldFooter";
			this.fldFooter.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.fldFooter.Text = null;
			this.fldFooter.Top = 0F;
			this.fldFooter.Width = 6.8125F;
			// 
			// arDuplicateSequence
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.822917F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMeterNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeterNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeq;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMeterNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSequence;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMeterNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
