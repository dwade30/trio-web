﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptRateTables.
	/// </summary>
	public partial class rptRateTables : BaseSectionReport
	{
		public rptRateTables()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Rate Table Listing";
		}

		public static rptRateTables InstancePtr
		{
			get
			{
				return (rptRateTables)Sys.GetInstance(typeof(rptRateTables));
			}
		}

		protected rptRateTables _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRateTables	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/10/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/23/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblMuni.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			rsData.OpenRecordset("SELECT * FROM RateTable", modExtraModules.strUTDatabase);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No Rate Tables Found.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
			}
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblServiceW.Text = "Stormwater";
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					fldRateTableNumber.Text = FCConvert.ToString(rsData.Get_Fields_Int32("RateTableNumber"));
					fldRateTableDescription.Text = rsData.Get_Fields_String("RateTableDescription");
					// Water Fields
					fldConsumptionW.Text = FCConvert.ToString(rsData.Get_Fields_Int32("MinConsW"));
					fldChargeW.Text = Strings.Format(rsData.Get_Fields_Double("MinChargeW"), "#,##0.00");
					fldFlatChargeW.Text = Strings.Format(rsData.Get_Fields_Double("FlatRateW"), "#,##0.00");
					// .Fields ("TaxableW1")      'not used
					// .Fields ("TaxableW2")
					lblMiscDesc1W.Text = FCConvert.ToString(rsData.Get_Fields_String("MiscChargeW1"));
					lblMiscDesc2W.Text = FCConvert.ToString(rsData.Get_Fields_String("MiscChargeW2"));
					fldMiscAmt1W.Text = Strings.Format(rsData.Get_Fields_Double("MiscChargeAmountW1"), "#,##0.00");
					fldMiscAmt2W.Text = Strings.Format(rsData.Get_Fields_Double("MiscChargeAmountW2"), "#,##0.00");
					fldMiscAccount1W.Text = FCConvert.ToString(rsData.Get_Fields_String("MiscChargeAccountW1"));
					fldMiscAccount2W.Text = FCConvert.ToString(rsData.Get_Fields_String("MiscChargeAccountW2"));
					fldOver1W.Text = Strings.Format(rsData.Get_Fields_Int32("WOver1"), "#,##0");
					fldOver2W.Text = Strings.Format(rsData.Get_Fields_Int32("WOver2"), "#,##0");
					fldOver3W.Text = Strings.Format(rsData.Get_Fields_Int32("WOver3"), "#,##0");
					fldOver4W.Text = Strings.Format(rsData.Get_Fields_Int32("WOver4"), "#,##0");
					fldOver5W.Text = Strings.Format(rsData.Get_Fields_Int32("WOver5"), "#,##0");
					fldOver6W.Text = Strings.Format(rsData.Get_Fields_Int32("WOver6"), "#,##0");
					fldOver7W.Text = Strings.Format(rsData.Get_Fields_Int32("WOver7"), "#,##0");
					fldOver8W.Text = Strings.Format(rsData.Get_Fields_Int32("WOver8"), "#,##0");
					fldThru1W.Text = Strings.Format(rsData.Get_Fields_Int32("WThru1"), "#,##0");
					fldThru2W.Text = Strings.Format(rsData.Get_Fields_Int32("WThru2"), "#,##0");
					fldThru3W.Text = Strings.Format(rsData.Get_Fields_Int32("WThru3"), "#,##0");
					fldThru4W.Text = Strings.Format(rsData.Get_Fields_Int32("WThru4"), "#,##0");
					fldThru5W.Text = Strings.Format(rsData.Get_Fields_Int32("WThru5"), "#,##0");
					fldThru6W.Text = Strings.Format(rsData.Get_Fields_Int32("WThru6"), "#,##0");
					fldThru7W.Text = Strings.Format(rsData.Get_Fields_Int32("WThru7"), "#,##0");
					fldThru8W.Text = Strings.Format(rsData.Get_Fields_Int32("WThru8"), "#,##0");
					fldStep1W.Text = Strings.Format(rsData.Get_Fields_Int32("WStep1"), "#,##0");
					fldStep2W.Text = Strings.Format(rsData.Get_Fields_Int32("WStep2"), "#,##0");
					fldStep3W.Text = Strings.Format(rsData.Get_Fields_Int32("WStep3"), "#,##0");
					fldStep4W.Text = Strings.Format(rsData.Get_Fields_Int32("WStep4"), "#,##0");
					fldStep5W.Text = Strings.Format(rsData.Get_Fields_Int32("WStep5"), "#,##0");
					fldStep6W.Text = Strings.Format(rsData.Get_Fields_Int32("WStep6"), "#,##0");
					fldStep7W.Text = Strings.Format(rsData.Get_Fields_Int32("WStep7"), "#,##0");
					fldStep8W.Text = Strings.Format(rsData.Get_Fields_Int32("WStep8"), "#,##0");
					fldAmount1W.Text = Strings.Format(rsData.Get_Fields_Double("WRate1"), "0.0000");
					fldAmount2W.Text = Strings.Format(rsData.Get_Fields_Double("WRate2"), "0.0000");
					fldAmount3W.Text = Strings.Format(rsData.Get_Fields_Double("WRate3"), "0.0000");
					fldAmount4W.Text = Strings.Format(rsData.Get_Fields_Double("WRate4"), "0.0000");
					fldAmount5W.Text = Strings.Format(rsData.Get_Fields_Double("WRate5"), "0.0000");
					fldAmount6W.Text = Strings.Format(rsData.Get_Fields_Double("WRate6"), "0.0000");
					fldAmount7W.Text = Strings.Format(rsData.Get_Fields_Double("WRate7"), "0.0000");
					fldAmount8W.Text = Strings.Format(rsData.Get_Fields_Double("WRate8"), "0.0000");
					// Sewer Fields
					fldConsumptionS.Text = FCConvert.ToString(rsData.Get_Fields_Int32("MinConsS"));
					fldChargeS.Text = Strings.Format(rsData.Get_Fields_Double("MinChargeS"), "#,##0.00");
					fldFlatChargeS.Text = Strings.Format(rsData.Get_Fields_Double("FlatRateS"), "#,##0.00");
					// .Fields ("TaxableS1")
					// .Fields ("TaxableS2")
					lblMiscDesc1S.Text = Strings.Format(rsData.Get_Fields_String("MiscChargeS1"), "#,##0.00");
					lblMiscDesc2S.Text = Strings.Format(rsData.Get_Fields_String("MiscChargeS2"), "#,##0.00");
					fldMiscAmt1S.Text = Strings.Format(rsData.Get_Fields_Double("MiscChargeAmountS1"), "#,##0.00");
					fldMiscAmt2S.Text = Strings.Format(rsData.Get_Fields_Double("MiscChargeAmountS2"), "#,##0.00");
					fldMiscAccount1S.Text = FCConvert.ToString(rsData.Get_Fields_String("MiscChargeAccountS1"));
					fldMiscAccount2S.Text = FCConvert.ToString(rsData.Get_Fields_String("MiscChargeAccountS2"));
					fldOver1S.Text = Strings.Format(rsData.Get_Fields_Int32("SOver1"), "#,##0");
					fldOver2S.Text = Strings.Format(rsData.Get_Fields_Int32("SOver2"), "#,##0");
					fldOver3S.Text = Strings.Format(rsData.Get_Fields_Int32("SOver3"), "#,##0");
					fldOver4S.Text = Strings.Format(rsData.Get_Fields_Int32("SOver4"), "#,##0");
					fldOver5S.Text = Strings.Format(rsData.Get_Fields_Int32("SOver5"), "#,##0");
					fldOver6S.Text = Strings.Format(rsData.Get_Fields_Int32("SOver6"), "#,##0");
					fldOver7S.Text = Strings.Format(rsData.Get_Fields_Int32("SOver7"), "#,##0");
					fldOver8S.Text = Strings.Format(rsData.Get_Fields_Int32("SOver8"), "#,##0");
					fldThru1S.Text = Strings.Format(rsData.Get_Fields_Int32("SThru1"), "#,##0");
					fldThru2S.Text = Strings.Format(rsData.Get_Fields_Int32("SThru2"), "#,##0");
					fldThru3S.Text = Strings.Format(rsData.Get_Fields_Int32("SThru3"), "#,##0");
					fldThru4S.Text = Strings.Format(rsData.Get_Fields_Int32("SThru4"), "#,##0");
					fldThru5S.Text = Strings.Format(rsData.Get_Fields_Int32("SThru5"), "#,##0");
					fldThru6S.Text = Strings.Format(rsData.Get_Fields_Int32("SThru6"), "#,##0");
					fldThru7S.Text = Strings.Format(rsData.Get_Fields_Int32("SThru7"), "#,##0");
					fldThru8S.Text = Strings.Format(rsData.Get_Fields_Int32("SThru8"), "#,##0");
					fldStep1S.Text = Strings.Format(rsData.Get_Fields_Int32("SStep1"), "#,##0");
					fldStep2S.Text = Strings.Format(rsData.Get_Fields_Int32("SStep2"), "#,##0");
					fldStep3S.Text = Strings.Format(rsData.Get_Fields_Int32("SStep3"), "#,##0");
					fldStep4S.Text = Strings.Format(rsData.Get_Fields_Int32("SStep4"), "#,##0");
					fldStep5S.Text = Strings.Format(rsData.Get_Fields_Int32("SStep5"), "#,##0");
					fldStep6S.Text = Strings.Format(rsData.Get_Fields_Int32("SStep6"), "#,##0");
					fldStep7S.Text = Strings.Format(rsData.Get_Fields_Int32("SStep7"), "#,##0");
					fldStep8S.Text = Strings.Format(rsData.Get_Fields_Int32("SStep8"), "#,##0");
					fldAmount1S.Text = Strings.Format(rsData.Get_Fields_Double("SRate1"), "0.0000");
					fldAmount2S.Text = Strings.Format(rsData.Get_Fields_Double("SRate2"), "0.0000");
					fldAmount3S.Text = Strings.Format(rsData.Get_Fields_Double("SRate3"), "0.0000");
					fldAmount4S.Text = Strings.Format(rsData.Get_Fields_Double("SRate4"), "0.0000");
					fldAmount5S.Text = Strings.Format(rsData.Get_Fields_Double("SRate5"), "0.0000");
					fldAmount6S.Text = Strings.Format(rsData.Get_Fields_Double("SRate6"), "0.0000");
					fldAmount7S.Text = Strings.Format(rsData.Get_Fields_Double("SRate7"), "0.0000");
					fldAmount8S.Text = Strings.Format(rsData.Get_Fields_Double("SRate8"), "0.0000");
				}
				rsData.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Rate Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void rptRateTables_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptRateTables properties;
			//rptRateTables.Caption	= "Rate Table Listing";
			//rptRateTables.Icon	= "rptRateTables.dsx":0000";
			//rptRateTables.Left	= 0;
			//rptRateTables.Top	= 0;
			//rptRateTables.Width	= 11880;
			//rptRateTables.Height	= 8595;
			//rptRateTables.StartUpPosition	= 3;
			//rptRateTables.SectionData	= "rptRateTables.dsx":058A;
			//End Unmaped Properties
		}
	}
}
