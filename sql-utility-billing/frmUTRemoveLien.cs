﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmRemoveLien.
	/// </summary>
	public partial class frmRemoveLien : BaseForm
	{
		public frmRemoveLien()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmRemoveLien InstancePtr
		{
			get
			{
				return (frmRemoveLien)Sys.GetInstance(typeof(frmRemoveLien));
			}
		}

		protected frmRemoveLien _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/23/2006              *
		// ********************************************************
		int lngAcct;
		int lngLR;
		int lngBK;
		int intAct;
		bool boolRefill;
		double dblPrin;
		string strWS = "";
		int lngColPayRTDate;
		int lngColPayReference;
		int lngColPayPeriod;
		int lngColPayCode;
		int lngColPayPrincipal;
		int lngColPayTax;
		int lngColPayInterest;
		int lngColPayCosts;
		int lngColPayTotal;
		int lngColPayID;

		private void cmbYear_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (!boolRefill)
			{
				lngLR = cmbYear.ItemData(cmbYear.SelectedIndex);
				if (lngLR > 0)
				{
					FillPaymentGrid();
				}
			}
		}

		private void cmbYear_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Space:
					{
						KeyCode = (Keys)0;
						if (modAPIsConst.SendMessageByNum(cmbYear.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
						{
							modAPIsConst.SendMessageByNum(cmbYear.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
						}
						break;
					}
			}
			//end switch
		}

		private void frmRemoveLien_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmRemoveLien_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmRemoveLien properties;
			//frmRemoveLien.FillStyle	= 0;
			//frmRemoveLien.ScaleWidth	= 9045;
			//frmRemoveLien.ScaleHeight	= 7395;
			//frmRemoveLien.LinkTopic	= "Form2";
			//frmRemoveLien.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			lngColPayRTDate = 1;
			lngColPayReference = 2;
			lngColPayPeriod = 3;
			lngColPayCode = 4;
			lngColPayPrincipal = 5;
			lngColPayTax = 6;
			lngColPayInterest = 7;
			lngColPayCosts = 8;
			lngColPayTotal = 9;
			lngColPayID = 10;
			FormatPaymentGrid();
			intAct = 1;
			this.Text = "Remove From Lien Status";
			if (modUTStatusPayments.Statics.TownService == "W")
			{
				cmbWS.Clear();
				cmbWS.Items.Add("Water");
				cmbWS.Text = "Water";
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				cmbWS.Clear();
				cmbWS.Items.Add("Sewer");
				cmbWS.Text = "Sewer";
			}
			else if (modUTStatusPayments.Statics.TownService == "B")
			{
				if (modUTStatusPayments.Statics.gboolPayWaterFirst)
				{
					cmbWS.Text = "Water";
				}
				else
				{
					cmbWS.Text = "Sewer";
				}
			}
			// lblInstructions.Caption = "Please enter the account that you would like to remove from lien status, then press 'Enter' or F10."
			// lblInstructions.Caption = "Account:"
		}

		private void frmRemoveLien_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmRemoveLien_Resize(object sender, System.EventArgs e)
		{
			SetPaymentGridHeight();
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFileRemove_Click(object sender, System.EventArgs e)
		{
			string strText = "";
			int lngRK = 0;
			string strBD = "";
			switch (intAct)
			{
				case 0:
					{
						// do nothing
						break;
					}
				case 1:
					{
						// this is to enter the account number
						txtAccount_Validate(false);
						break;
					}
				case 2:
					{
						// this is to remove the account/year from lien status
						if (lngAcct > 0)
						{
							strBD = "";
							if (modGlobalConstants.Statics.gboolBD)
							{
								if (vsPayments.Rows > 1)
								{
									strBD = "\r\n" + "A manual entry will have to be made to adjust any payments that were made to the lien accounts.";
								}
							}
							lngRK = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cmbYear.Items[cmbYear.SelectedIndex].ToString(), 5))));
							strText = "Are you sure that you want to remove account " + FCConvert.ToString(modUTStatusPayments.GetAccountNumber(ref lngAcct)) + ", ratekey " + FCConvert.ToString(lngRK) + " from Lien Status and delete the post lien activity." + "\r\n" + "You will have to reenter the payments manually." + strBD;
							if (MessageBox.Show(strText, "Confirmation", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
							{
								// run the payment report
								rptLienRemovalPayments.InstancePtr.Init(ref lngLR, FCConvert.CBool(strWS == "W"));
								RemoveAccountFromLienStatus();
								if (modGlobalConstants.Statics.gboolBD)
								{
									modGlobalFunctions.AddCYAEntry_80("UT", "Sent Remove Lien Entry To BD", dblPrin.ToString(), lngRK.ToString());
									modMain.CreateTransferTaxToLienBDEntry_18(dblPrin, lngRK, FCConvert.CBool(strWS == "W"));
								}
							}
						}
						break;
					}
			}
			//end switch
		}

		private void optWS_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			CheckAccount();
		}

		private void optWS_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbWS.SelectedIndex;
			optWS_CheckedChanged(index, sender, e);
		}

		private void txtAccount_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				if (Conversion.Val(txtAccount.Text) > 0)
				{
					cmbYear.Enabled = true;
                    //FC:FINAL:SBE - global fix - validate event should not be triggered twice (issue #4605)
                    //txtAccount_Validate(false);
                    Support.SendKeys("{TAB}");
                }
            }
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtAccount_Validating_2(bool Cancel)
		{
			txtAccount_Validating(new object(), new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private void txtAccount_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			CheckAccount();
		}

		public void txtAccount_Validate(bool Cancel)
		{
			txtAccount_Validating(txtAccount, new System.ComponentModel.CancelEventArgs(Cancel));
		}

		private bool FillYearCombo()
		{
			bool FillYearCombo = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with all of the eligible years for the account entered
				clsDRWrapper rsYear = new clsDRWrapper();
				cmbYear.Clear();
				FillYearCombo = false;
				rsYear.RecordCount();
				rsYear.OpenRecordset("SELECT Lien.RateKey, Lien.DateCreated, BillMast." + strWS + "LienRecordNumber FROM (SELECT Bill.ID, BillingRateKey, Bill." + strWS + "LienRecordNumber, Bill.AccountKey, Bill." + strWS + "CombinationLienKey FROM Bill INNER JOIN Master ON Bill.AccountKey = Master.ID) AS BillMast INNER JOIN Lien ON BillMast." + strWS + "LienRecordNumber = Lien.ID WHERE AccountKey = " + FCConvert.ToString(lngAcct) + " AND " + strWS + "CombinationLienKey = BillMast.ID ORDER BY BillingRateKey desc", modExtraModules.strUTDatabase);
				while (!rsYear.EndOfFile())
				{
					FillYearCombo = true;
					cmbYear.AddItem(Strings.Format(rsYear.Get_Fields_Int32("RateKey"), "00000") + "  " + Strings.Format(rsYear.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy"));
					cmbYear.ItemData(cmbYear.NewIndex, FCConvert.ToInt32(rsYear.Get_Fields(strWS + "LienRecordNumber")));
					rsYear.MoveNext();
				}
				cmbYear.Visible = true;
				lblYearTitle.Visible = true;
				rsYear.Reset();
				rsYear = null;
				return FillYearCombo;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error " + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Year Combo Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				FillYearCombo = false;
			}
			return FillYearCombo;
		}

		private void FormatPaymentGrid()
		{
			// this will format the payment grid and set the widths of rows, header titles ect
			fraPayments.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORBLUE);
			vsPayments.Cols = 11;
			vsPayments.Rows = 1;
			SetColWidths();
			// set the column headers
			vsPayments.TextMatrix(0, lngColPayRTDate, "Date");
			vsPayments.TextMatrix(0, lngColPayReference, "Reference");
			vsPayments.TextMatrix(0, lngColPayPeriod, "Period");
			vsPayments.TextMatrix(0, lngColPayCode, "Code");
			vsPayments.TextMatrix(0, lngColPayPrincipal, "Principal");
			vsPayments.TextMatrix(0, lngColPayTax, "Tax");
			vsPayments.TextMatrix(0, lngColPayInterest, "Interest");
			vsPayments.TextMatrix(0, lngColPayCosts, "Costs");
			vsPayments.TextMatrix(0, lngColPayTotal, "Total");
			// alignment and other formatting
			vsPayments.ColAlignment(lngColPayRTDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngColPayReference, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngColPayPeriod, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngColPayCode, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsPayments.ColAlignment(lngColPayPrincipal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ColAlignment(lngColPayTax, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ColAlignment(lngColPayInterest, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ColAlignment(lngColPayCosts, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ColAlignment(lngColPayTotal, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsPayments.Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, vsPayments.Cols - 1, true);
			vsPayments.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
		}

		private void SetColWidths()
		{
			int lngWid = 0;
			// set the widths
			lngWid = vsPayments.WidthOriginal;
			vsPayments.Cols = 11;
			vsPayments.ColWidth(0, FCConvert.ToInt32(lngWid * 0.0));
			// Check Box
			vsPayments.ColWidth(lngColPayRTDate, FCConvert.ToInt32(lngWid * 0.13));
			// Payment Date
			vsPayments.ColWidth(lngColPayReference, FCConvert.ToInt32(lngWid * 0.14));
			// Reference
			vsPayments.ColWidth(lngColPayPeriod, FCConvert.ToInt32(lngWid * 0.055));
			// Period
			vsPayments.ColWidth(lngColPayCode, FCConvert.ToInt32(lngWid * 0.055));
			// Code
			vsPayments.ColWidth(lngColPayPrincipal, FCConvert.ToInt32(lngWid * 0.12));
			// Principal
			vsPayments.ColWidth(lngColPayTax, FCConvert.ToInt32(lngWid * 0.12));
			// Tax
			vsPayments.ColWidth(lngColPayInterest, FCConvert.ToInt32(lngWid * 0.12));
			// Interest
			vsPayments.ColWidth(lngColPayCosts, FCConvert.ToInt32(lngWid * 0.12));
			// Costs
			vsPayments.ColWidth(lngColPayTotal, FCConvert.ToInt32(lngWid * 0.14));
			// Total
			vsPayments.ColWidth(lngColPayID, 0);
			// Key
		}

		private void FillPaymentGrid()
		{
			// this will fill the payment grid with the lien payments of the account that was entered
			clsDRWrapper rsPay = new clsDRWrapper();
			string strPayment = "";
			string strSQL = "";
			double dblTotal = 0;
			// get the original principal
			strSQL = "SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngLR);
			rsPay.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (!rsPay.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblPrin = rsPay.Get_Fields("Principal") * -1;
			}
			strSQL = "SELECT * FROM PaymentRec WHERE BillKey = " + FCConvert.ToString(lngLR) + " AND Lien = 1 ORDER BY RecordedTransactionDate";
			vsPayments.Rows = 1;
			rsPay.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			while (!rsPay.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
				dblTotal = Conversion.Val(rsPay.Get_Fields("Principal")) + Conversion.Val(rsPay.Get_Fields("Tax")) + Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + Conversion.Val(rsPay.Get_Fields_Decimal("LienCost"));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				strPayment = "\t" + rsPay.Get_Fields_DateTime("RecordedTransactionDate") + "\t" + rsPay.Get_Fields_String("Reference") + "\t" + rsPay.Get_Fields("Period") + "\t" + rsPay.Get_Fields("Code") + "\t" + rsPay.Get_Fields("Principal") + "\t" + FCConvert.ToString(Conversion.Val(rsPay.Get_Fields_Decimal("CurrentInterest"))) + Conversion.Val(rsPay.Get_Fields_Decimal("PreLienInterest")) + "\t" + rsPay.Get_Fields_Decimal("LienCost") + "\t" + FCConvert.ToString(dblTotal) + "\t" + rsPay.Get_Fields_Int32("ID");
				vsPayments.AddItem("");
				// strPayment
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayRTDate, FCConvert.ToString(rsPay.Get_Fields_DateTime("RecordedTransactionDate")));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayReference, FCConvert.ToString(rsPay.Get_Fields_String("Reference")));
				// TODO Get_Fields: Check the table for the column [Period] and replace with corresponding Get_Field method
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayPeriod, FCConvert.ToString(rsPay.Get_Fields("Period")));
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayCode, FCConvert.ToString(rsPay.Get_Fields("Code")));
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayPrincipal, Strings.Format(rsPay.Get_Fields("Principal"), "#,##0.00"));
				// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayTax, Strings.Format(rsPay.Get_Fields("Tax"), "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayInterest, Strings.Format(rsPay.Get_Fields_Decimal("CurrentInterest") + rsPay.Get_Fields_Decimal("PreLienInterest"), "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayCosts, Strings.Format(rsPay.Get_Fields_Decimal("LienCost"), "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayTotal, Strings.Format(dblTotal, "#,##0.00"));
				vsPayments.TextMatrix(vsPayments.Rows - 1, lngColPayID, FCConvert.ToString(rsPay.Get_Fields_Int32("ID")));
				rsPay.MoveNext();
			}
			SetPaymentGridHeight();
			fraPayments.Visible = true;
			rsPay = null;
		}

		private void SetPaymentGridHeight()
		{
			// this will adjust the height of the grid in the frame after a resize or a change of the amount of rows in the grid
			int lngFrameHeight;
			int lngMaxHeight;
			lngMaxHeight = fraPayments.Height - 50;
			//FC:FINAl:SBE - exclude header row, which has a different height (30)
			int rows = vsPayments.Rows - 1;
			//FC:FINAl:SBE - add column header height(30px) and border height(2px)
			if ((rows * 40) + 30 + 2 > lngMaxHeight)
			{
				vsPayments.Height = lngMaxHeight;
				vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			}
			else
			{
				//FC:FINAl:SBE - add column header height(30px) and border height(2px)
				vsPayments.Height = (rows * 40) + 30 + 2;
				vsPayments.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			}
			SetColWidths();
		}

		private void RemoveAccountFromLienStatus()
		{
			// this sub will actually remove the account from lien status
			clsDRWrapper rsDel = new clsDRWrapper();
			clsDRWrapper rsRate = new clsDRWrapper();
			string strSQL;
			try
			{
				rsDel.DefaultDB = modExtraModules.strUTDatabase;
				strSQL = "SELECT * FROM RateKeys";
				rsRate.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				strSQL = "SELECT * FROM Bill WHERE " + strWS + "LienRecordNumber = " + FCConvert.ToString(lngLR);
				rsDel.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!rsDel.EndOfFile())
				{
					lngBK = FCConvert.ToInt32(rsDel.Get_Fields_Int32("ID"));
					// this will save the original bill record
					// write to the CYA table
					modGlobalFunctions.AddCYAEntry_80("UT", "Removed From Lien Status: " + FCConvert.ToString(modUTStatusPayments.GetAccountNumber(ref lngAcct)), "Bill = " + FCConvert.ToString(lngBK), "LRN = " + FCConvert.ToString(lngLR));
					// delete the lien and CHGINT payments
					strSQL = "DELETE FROM PaymentRec WHERE AccountKey = " + FCConvert.ToString(lngAcct) + " AND BillKey = " + FCConvert.ToString(lngLR) + " AND ISNULL(Lien,0) = 1";
					// AND (Code = '3' OR Code = 'L' OR Code = 'I')"
					if (rsDel.Execute(strSQL, modExtraModules.strUTDatabase))
					{
						// delete the lien record
						strSQL = "DELETE FROM Lien WHERE ID = " + FCConvert.ToString(lngLR);
						if (rsDel.Execute(strSQL, modExtraModules.strUTDatabase))
						{
							strSQL = "SELECT * FROM Bill WHERE " + strWS + "LienRecordNumber = " + FCConvert.ToString(lngLR);
							rsDel.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
							while (!rsDel.EndOfFile())
							{
								rsDel.Edit();
								rsDel.Set_Fields(strWS + "LienRecordNumber", 0);
								rsRate.FindFirstRecord("ID", rsDel.Get_Fields_Int32("BillingRateKey"));
								if (rsRate.NoMatch)
								{
									rsDel.Set_Fields(strWS + "IntPaidDate", 0);
									// this should throw an error
								}
								else
								{
									rsDel.Set_Fields(strWS + "IntPaidDate", DateAndTime.DateAdd("D", -1, (DateTime)rsRate.Get_Fields_DateTime("IntStart")));
								}
								rsDel.Set_Fields(strWS + "LienProcessStatus", "3");
								rsDel.Set_Fields(strWS + "LienStatusEligibility", "4");
								rsDel.Update();
								rsDel.MoveNext();
							}
							// reapply the payments made, not the lien charges or the interest charged
							// set the interest paid date back to the beginning
							// and charge the interest as it would be without the lien
						}
						else
						{
							MessageBox.Show("Error deleting the lien record.", "Deletion Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						}
					}
					else
					{
						MessageBox.Show("Error deleting the lien/interest charges." + "\r\n" + "This account has not been removed from lien status.", "Error Removing Lien/Interest Charges", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
				MessageBox.Show("This account has been removed from lien status and the post lien payments have been erased.", "Remove Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Unload();
				return;
			}
			catch (Exception ex)
			{
				//ERROR_HANDLER:;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Remove From Lien Status Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void CheckAccount()
		{
			fraPayments.Visible = false;
			if (Conversion.Val(txtAccount.Text) != 0)
			{
				if (cmbWS.Text == "Water")
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				lngAcct = modUTStatusPayments.GetAccountKeyUT_2(FCConvert.ToInt32(Math.Round(Conversion.Val(txtAccount.Text))));
				boolRefill = true;
				if (FillYearCombo())
				{
					fraPayments.Visible = false;
					if (cmbYear.Visible && cmbYear.Enabled)
					{
						// automatically set the focus to the year field
						cmbYear.Focus();
					}
					intAct = 2;
				}
				else
				{
					intAct = 1;
					MessageBox.Show("No liened bill years were found for this account number.", "No Liened Years", MessageBoxButtons.OK, MessageBoxIcon.Information);
					cmbYear.Enabled = false;
					cmbYear.Visible = false;
					lblYearTitle.Visible = false;
					fraPayments.Visible = false;
				}
				boolRefill = false;
				// Else
				// MsgBox "Please enter an account number and press enter.", vbInformation, "Invalid Account Number"
			}
		}
	}
}
