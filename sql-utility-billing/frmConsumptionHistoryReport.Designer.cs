﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmConsumptionHistoryReport.
	/// </summary>
	partial class frmConsumptionHistoryReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbGroupByCategory;
		public fecherFoundation.FCComboBox cmbSortByConsumption;
		public fecherFoundation.FCLabel lblSortByConsumption;
		public fecherFoundation.FCComboBox cmbCategorySelected;
		public fecherFoundation.FCComboBox cmbFrequencyAll;
		public fecherFoundation.FCComboBox cmbMeterSizeAll;
		public fecherFoundation.FCFrame fraGroupBy;
		public fecherFoundation.FCCheckBox chkHideDetail;
		public fecherFoundation.FCFrame fraSelectionCriteria;
		public fecherFoundation.FCFrame fraCategory;
		public fecherFoundation.FCFrame fraSelectedCategory;
		public FCGrid vsCategory;
		public fecherFoundation.FCFrame fraFrequency;
		public fecherFoundation.FCFrame fraSelectedFrequency;
		public FCGrid vsFrequency;
		public fecherFoundation.FCFrame fraMeterSize;
		public fecherFoundation.FCFrame fraSelectedMeterSize;
		public FCGrid vsMeterSize;
		public fecherFoundation.FCGrid vsGrid;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle5 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle6 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle7 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle8 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsumptionHistoryReport));
			this.cmbGroupByCategory = new fecherFoundation.FCComboBox();
			this.cmbSortByConsumption = new fecherFoundation.FCComboBox();
			this.lblSortByConsumption = new fecherFoundation.FCLabel();
			this.cmbCategorySelected = new fecherFoundation.FCComboBox();
			this.cmbFrequencyAll = new fecherFoundation.FCComboBox();
			this.cmbMeterSizeAll = new fecherFoundation.FCComboBox();
			this.fraGroupBy = new fecherFoundation.FCFrame();
			this.chkHideDetail = new fecherFoundation.FCCheckBox();
			this.fraSelectionCriteria = new fecherFoundation.FCFrame();
			this.fraCategory = new fecherFoundation.FCFrame();
			this.fraSelectedCategory = new fecherFoundation.FCFrame();
			this.vsCategory = new fecherFoundation.FCGrid();
			this.fraFrequency = new fecherFoundation.FCFrame();
			this.fraSelectedFrequency = new fecherFoundation.FCFrame();
			this.vsFrequency = new fecherFoundation.FCGrid();
			this.fraMeterSize = new fecherFoundation.FCFrame();
			this.fraSelectedMeterSize = new fecherFoundation.FCFrame();
			this.vsMeterSize = new fecherFoundation.FCGrid();
			this.vsGrid = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdFilePreview = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGroupBy)).BeginInit();
			this.fraGroupBy.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkHideDetail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectionCriteria)).BeginInit();
			this.fraSelectionCriteria.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCategory)).BeginInit();
			this.fraCategory.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedCategory)).BeginInit();
			this.fraSelectedCategory.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFrequency)).BeginInit();
			this.fraFrequency.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedFrequency)).BeginInit();
			this.fraSelectedFrequency.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsFrequency)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMeterSize)).BeginInit();
			this.fraMeterSize.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedMeterSize)).BeginInit();
			this.fraSelectedMeterSize.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsMeterSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFilePreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(968, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraGroupBy);
			this.ClientArea.Controls.Add(this.cmbSortByConsumption);
			this.ClientArea.Controls.Add(this.lblSortByConsumption);
			this.ClientArea.Controls.Add(this.fraSelectionCriteria);
			this.ClientArea.Size = new System.Drawing.Size(968, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(968, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(322, 30);
			this.HeaderText.Text = "Consumption History Report";
			// 
			// cmbGroupByCategory
			// 
			this.cmbGroupByCategory.AutoSize = false;
			this.cmbGroupByCategory.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbGroupByCategory.FormattingEnabled = true;
			this.cmbGroupByCategory.Items.AddRange(new object[] {
				"Category",
				"Account #",
				"Book",
				"Frequency",
				"Meter Size",
				"None"
			});
			this.cmbGroupByCategory.Location = new System.Drawing.Point(20, 30);
			this.cmbGroupByCategory.Name = "cmbGroupByCategory";
			this.cmbGroupByCategory.Size = new System.Drawing.Size(189, 40);
			this.cmbGroupByCategory.TabIndex = 7;
			this.cmbGroupByCategory.Text = "None";
			this.cmbGroupByCategory.SelectedIndexChanged += new System.EventHandler(this.cmbGroupByCategory_SelectedIndexChanged);
			// 
			// cmbSortByConsumption
			// 
			this.cmbSortByConsumption.AutoSize = false;
			this.cmbSortByConsumption.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbSortByConsumption.FormattingEnabled = true;
			this.cmbSortByConsumption.Items.AddRange(new object[] {
				"Consumption",
				"Bill Amount",
				"Sequence",
				"Account #",
				"Name"
			});
			//FC:FINAL:CHN - issue #982: Layout Adjustment.
			this.cmbSortByConsumption.Location = new System.Drawing.Point(472, 30);
			this.cmbSortByConsumption.Name = "cmbSortByConsumption";
			this.cmbSortByConsumption.Size = new System.Drawing.Size(176, 40);
			this.cmbSortByConsumption.TabIndex = 31;
			this.cmbSortByConsumption.Text = "Name";
			// 
			// lblSortByConsumption
			// 
			this.lblSortByConsumption.AutoSize = true;
			//FC:FINAL:CHN - issue #982: Layout Adjustment.
			this.lblSortByConsumption.Location = new System.Drawing.Point(354, 44);
			// was (30, 44)
			this.lblSortByConsumption.Name = "lblSortByConsumption";
			this.lblSortByConsumption.Size = new System.Drawing.Size(62, 15);
			this.lblSortByConsumption.TabIndex = 32;
			this.lblSortByConsumption.Text = "SORT BY";
			// 
			// cmbCategorySelected
			// 
			this.cmbCategorySelected.AutoSize = false;
			this.cmbCategorySelected.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCategorySelected.FormattingEnabled = true;
			this.cmbCategorySelected.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbCategorySelected.Location = new System.Drawing.Point(20, 30);
			this.cmbCategorySelected.Name = "cmbCategorySelected";
			this.cmbCategorySelected.Size = new System.Drawing.Size(234, 40);
			this.cmbCategorySelected.TabIndex = 29;
			this.cmbCategorySelected.Text = "All";
			this.cmbCategorySelected.SelectedIndexChanged += new System.EventHandler(this.cmbCategorySelected_SelectedIndexChanged);
			// 
			// cmbFrequencyAll
			// 
			this.cmbFrequencyAll.AutoSize = false;
			this.cmbFrequencyAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbFrequencyAll.FormattingEnabled = true;
			this.cmbFrequencyAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbFrequencyAll.Location = new System.Drawing.Point(20, 30);
			this.cmbFrequencyAll.Name = "cmbFrequencyAll";
			this.cmbFrequencyAll.Size = new System.Drawing.Size(234, 40);
			this.cmbFrequencyAll.TabIndex = 27;
			this.cmbFrequencyAll.Text = "All";
			this.cmbFrequencyAll.SelectedIndexChanged += new System.EventHandler(this.cmbFrequencyAll_SelectedIndexChanged);
			// 
			// cmbMeterSizeAll
			// 
			this.cmbMeterSizeAll.AutoSize = false;
			this.cmbMeterSizeAll.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbMeterSizeAll.FormattingEnabled = true;
			this.cmbMeterSizeAll.Items.AddRange(new object[] {
				"All",
				"Selected"
			});
			this.cmbMeterSizeAll.Location = new System.Drawing.Point(20, 30);
			this.cmbMeterSizeAll.Name = "cmbMeterSizeAll";
			this.cmbMeterSizeAll.Size = new System.Drawing.Size(234, 40);
			this.cmbMeterSizeAll.TabIndex = 25;
			this.cmbMeterSizeAll.Text = "All";
			this.cmbMeterSizeAll.SelectedIndexChanged += new System.EventHandler(this.cmbMeterSizeAll_SelectedIndexChanged);
			// 
			// fraGroupBy
			// 
			this.fraGroupBy.Controls.Add(this.chkHideDetail);
			this.fraGroupBy.Controls.Add(this.cmbGroupByCategory);
			//FC:FINAL:CHN - issue #982: Layout Adjustment.
			this.fraGroupBy.Location = new System.Drawing.Point(30, 30);
			// was(354, 30)
			this.fraGroupBy.Name = "fraGroupBy";
			this.fraGroupBy.Size = new System.Drawing.Size(274, 137);
			this.fraGroupBy.TabIndex = 30;
			this.fraGroupBy.Text = "Group By";
			// 
			// chkHideDetail
			// 
			this.chkHideDetail.Enabled = false;
			this.chkHideDetail.Location = new System.Drawing.Point(20, 90);
			this.chkHideDetail.Name = "chkHideDetail";
			this.chkHideDetail.Size = new System.Drawing.Size(108, 27);
			this.chkHideDetail.TabIndex = 6;
			this.chkHideDetail.Text = "Hide Detail";
			// 
			// fraSelectionCriteria
			// 
			this.fraSelectionCriteria.AppearanceKey = "groupBoxLeftBorder";
			this.fraSelectionCriteria.Controls.Add(this.fraCategory);
			this.fraSelectionCriteria.Controls.Add(this.fraFrequency);
			this.fraSelectionCriteria.Controls.Add(this.fraMeterSize);
			this.fraSelectionCriteria.Controls.Add(this.vsGrid);
			this.fraSelectionCriteria.Location = new System.Drawing.Point(30, 184);
			this.fraSelectionCriteria.Name = "fraSelectionCriteria";
			this.fraSelectionCriteria.Size = new System.Drawing.Size(909, 455);
			this.fraSelectionCriteria.TabIndex = 22;
			this.fraSelectionCriteria.Text = "Selection Criteria";
			// 
			// fraCategory
			// 
			this.fraCategory.Controls.Add(this.fraSelectedCategory);
			this.fraCategory.Controls.Add(this.cmbCategorySelected);
			this.fraCategory.Location = new System.Drawing.Point(20, 30);
			this.fraCategory.Name = "fraCategory";
			this.fraCategory.Size = new System.Drawing.Size(274, 220);
			this.fraCategory.TabIndex = 27;
			this.fraCategory.Text = "Category";
			// 
			// fraSelectedCategory
			// 
			this.fraSelectedCategory.AppearanceKey = "groupBoxNoBorders";
			this.fraSelectedCategory.Controls.Add(this.vsCategory);
			this.fraSelectedCategory.Enabled = false;
			this.fraSelectedCategory.Location = new System.Drawing.Point(20, 90);
			this.fraSelectedCategory.Name = "fraSelectedCategory";
			this.fraSelectedCategory.Size = new System.Drawing.Size(244, 120);
			this.fraSelectedCategory.TabIndex = 28;
			// 
			// vsCategory
			// 
			this.vsCategory.AllowSelection = false;
			this.vsCategory.AllowUserToResizeColumns = false;
			this.vsCategory.AllowUserToResizeRows = false;
			this.vsCategory.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsCategory.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsCategory.BackColorBkg = System.Drawing.Color.Empty;
			this.vsCategory.BackColorFixed = System.Drawing.Color.Empty;
			this.vsCategory.BackColorSel = System.Drawing.Color.Empty;
			this.vsCategory.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsCategory.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsCategory.ColumnHeadersHeight = 30;
			this.vsCategory.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsCategory.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsCategory.DragIcon = null;
			this.vsCategory.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsCategory.ExtendLastCol = true;
			this.vsCategory.FixedCols = 0;
			this.vsCategory.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsCategory.FrozenCols = 0;
			this.vsCategory.GridColor = System.Drawing.Color.Empty;
			this.vsCategory.GridColorFixed = System.Drawing.Color.Empty;
			this.vsCategory.Location = new System.Drawing.Point(0, 0);
			this.vsCategory.Name = "vsCategory";
			this.vsCategory.ReadOnly = true;
			this.vsCategory.RowHeadersVisible = false;
			this.vsCategory.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsCategory.RowHeightMin = 0;
			this.vsCategory.Rows = 1;
			this.vsCategory.ScrollTipText = null;
			this.vsCategory.ShowColumnVisibilityMenu = false;
			this.vsCategory.Size = new System.Drawing.Size(234, 110);
			this.vsCategory.StandardTab = true;
			this.vsCategory.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsCategory.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsCategory.TabIndex = 14;
			this.vsCategory.KeyDown += new Wisej.Web.KeyEventHandler(this.vsCategory_KeyDownEvent);
			this.vsCategory.Click += new System.EventHandler(this.vsCategory_ClickEvent);
			// 
			// fraFrequency
			// 
			this.fraFrequency.Controls.Add(this.fraSelectedFrequency);
			this.fraFrequency.Controls.Add(this.cmbFrequencyAll);
			this.fraFrequency.Location = new System.Drawing.Point(324, 30);
			this.fraFrequency.Name = "fraFrequency";
			this.fraFrequency.Size = new System.Drawing.Size(274, 220);
			this.fraFrequency.TabIndex = 25;
			this.fraFrequency.Text = "Frequency";
			// 
			// fraSelectedFrequency
			// 
			this.fraSelectedFrequency.AppearanceKey = "groupBoxNoBorders";
			this.fraSelectedFrequency.Controls.Add(this.vsFrequency);
			this.fraSelectedFrequency.Enabled = false;
			this.fraSelectedFrequency.Location = new System.Drawing.Point(20, 90);
			this.fraSelectedFrequency.Name = "fraSelectedFrequency";
			this.fraSelectedFrequency.Size = new System.Drawing.Size(244, 120);
			this.fraSelectedFrequency.TabIndex = 26;
			// 
			// vsFrequency
			// 
			this.vsFrequency.AllowSelection = false;
			this.vsFrequency.AllowUserToResizeColumns = false;
			this.vsFrequency.AllowUserToResizeRows = false;
			this.vsFrequency.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsFrequency.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsFrequency.BackColorBkg = System.Drawing.Color.Empty;
			this.vsFrequency.BackColorFixed = System.Drawing.Color.Empty;
			this.vsFrequency.BackColorSel = System.Drawing.Color.Empty;
			this.vsFrequency.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsFrequency.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsFrequency.ColumnHeadersHeight = 30;
			this.vsFrequency.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsFrequency.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsFrequency.DragIcon = null;
			this.vsFrequency.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsFrequency.ExtendLastCol = true;
			this.vsFrequency.FixedCols = 0;
			this.vsFrequency.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsFrequency.FrozenCols = 0;
			this.vsFrequency.GridColor = System.Drawing.Color.Empty;
			this.vsFrequency.GridColorFixed = System.Drawing.Color.Empty;
			this.vsFrequency.Location = new System.Drawing.Point(0, 0);
			this.vsFrequency.Name = "vsFrequency";
			this.vsFrequency.ReadOnly = true;
			this.vsFrequency.RowHeadersVisible = false;
			this.vsFrequency.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsFrequency.RowHeightMin = 0;
			this.vsFrequency.Rows = 1;
			this.vsFrequency.ScrollTipText = null;
			this.vsFrequency.ShowColumnVisibilityMenu = false;
			this.vsFrequency.Size = new System.Drawing.Size(234, 110);
			this.vsFrequency.StandardTab = true;
			this.vsFrequency.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsFrequency.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsFrequency.TabIndex = 17;
			this.vsFrequency.KeyDown += new Wisej.Web.KeyEventHandler(this.vsFrequency_KeyDownEvent);
			this.vsFrequency.Click += new System.EventHandler(this.vsFrequency_ClickEvent);
			// 
			// fraMeterSize
			// 
			this.fraMeterSize.Controls.Add(this.fraSelectedMeterSize);
			this.fraMeterSize.Controls.Add(this.cmbMeterSizeAll);
			this.fraMeterSize.Location = new System.Drawing.Point(628, 30);
			this.fraMeterSize.Name = "fraMeterSize";
			this.fraMeterSize.Size = new System.Drawing.Size(274, 220);
			this.fraMeterSize.TabIndex = 23;
			this.fraMeterSize.Text = "Meter Size";
			// 
			// fraSelectedMeterSize
			// 
			this.fraSelectedMeterSize.AppearanceKey = "groupBoxNoBorders";
			this.fraSelectedMeterSize.Controls.Add(this.vsMeterSize);
			this.fraSelectedMeterSize.Enabled = false;
			this.fraSelectedMeterSize.Location = new System.Drawing.Point(20, 90);
			this.fraSelectedMeterSize.Name = "fraSelectedMeterSize";
			this.fraSelectedMeterSize.Size = new System.Drawing.Size(244, 120);
			this.fraSelectedMeterSize.TabIndex = 24;
			// 
			// vsMeterSize
			// 
			this.vsMeterSize.AllowSelection = false;
			this.vsMeterSize.AllowUserToResizeColumns = false;
			this.vsMeterSize.AllowUserToResizeRows = false;
			this.vsMeterSize.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsMeterSize.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsMeterSize.BackColorBkg = System.Drawing.Color.Empty;
			this.vsMeterSize.BackColorFixed = System.Drawing.Color.Empty;
			this.vsMeterSize.BackColorSel = System.Drawing.Color.Empty;
			this.vsMeterSize.Cols = 2;
			dataGridViewCellStyle5.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsMeterSize.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this.vsMeterSize.ColumnHeadersHeight = 30;
			this.vsMeterSize.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle6.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsMeterSize.DefaultCellStyle = dataGridViewCellStyle6;
			this.vsMeterSize.DragIcon = null;
			this.vsMeterSize.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsMeterSize.ExtendLastCol = true;
			this.vsMeterSize.FixedCols = 0;
			this.vsMeterSize.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsMeterSize.FrozenCols = 0;
			this.vsMeterSize.GridColor = System.Drawing.Color.Empty;
			this.vsMeterSize.GridColorFixed = System.Drawing.Color.Empty;
			this.vsMeterSize.Location = new System.Drawing.Point(0, 0);
			this.vsMeterSize.Name = "vsMeterSize";
			this.vsMeterSize.ReadOnly = true;
			this.vsMeterSize.RowHeadersVisible = false;
			this.vsMeterSize.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsMeterSize.RowHeightMin = 0;
			this.vsMeterSize.Rows = 1;
			this.vsMeterSize.ScrollTipText = null;
			this.vsMeterSize.ShowColumnVisibilityMenu = false;
			this.vsMeterSize.Size = new System.Drawing.Size(234, 110);
			this.vsMeterSize.StandardTab = true;
			this.vsMeterSize.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsMeterSize.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsMeterSize.TabIndex = 20;
			this.vsMeterSize.KeyDown += new Wisej.Web.KeyEventHandler(this.vsMeterSize_KeyDownEvent);
			this.vsMeterSize.Click += new System.EventHandler(this.vsMeterSize_ClickEvent);
			// 
			// vsGrid
			// 
			this.vsGrid.AllowSelection = false;
			this.vsGrid.AllowUserToResizeColumns = false;
			this.vsGrid.AllowUserToResizeRows = false;
			this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrid.Cols = 5;
			dataGridViewCellStyle7.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
			this.vsGrid.ColumnHeadersHeight = 30;
			this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle8.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrid.DefaultCellStyle = dataGridViewCellStyle8;
			this.vsGrid.DragIcon = null;
			this.vsGrid.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrid.ExtendLastCol = true;
			this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.FrozenCols = 0;
			this.vsGrid.GridColor = System.Drawing.Color.Empty;
			this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.Location = new System.Drawing.Point(20, 270);
			this.vsGrid.Name = "vsGrid";
			this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrid.RowHeightMin = 0;
			this.vsGrid.Rows = 6;
			this.vsGrid.ScrollTipText = null;
			this.vsGrid.ShowColumnVisibilityMenu = false;
			this.vsGrid.Size = new System.Drawing.Size(882, 165);
			this.vsGrid.StandardTab = true;
			this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsGrid.TabIndex = 21;
			this.vsGrid.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsGrid_KeyDownEdit);
			this.vsGrid.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsGrid_KeyPressEdit);
			this.vsGrid.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsGrid_BeforeEdit);
			this.vsGrid.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsGrid_AfterEdit);
			this.vsGrid.CurrentCellChanged += new System.EventHandler(this.vsGrid_RowColChange);
			this.vsGrid.CellFormatting += new DataGridViewCellFormattingEventHandler(this.vsGrid_MouseMoveEvent);
			this.vsGrid.KeyDown += new Wisej.Web.KeyEventHandler(this.vsGrid_KeyDownEvent);
			this.vsGrid.Leave += new System.EventHandler(this.vsGrid_Leave);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFilePrint.Text = "Print";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 1;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 2;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 3;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdFilePreview
			// 
			this.cmdFilePreview.AppearanceKey = "acceptButton";
			this.cmdFilePreview.Location = new System.Drawing.Point(424, 30);
			this.cmdFilePreview.Name = "cmdFilePreview";
			this.cmdFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePreview.Size = new System.Drawing.Size(124, 48);
			this.cmdFilePreview.TabIndex = 0;
			this.cmdFilePreview.Text = "Print Preview";
			this.cmdFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(896, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
			this.cmdFilePrint.Size = new System.Drawing.Size(44, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmConsumptionHistoryReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(968, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmConsumptionHistoryReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Consumption History Report";
			this.Load += new System.EventHandler(this.frmConsumptionHistoryReport_Load);
			this.Activated += new System.EventHandler(this.frmConsumptionHistoryReport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmConsumptionHistoryReport_KeyPress);
			this.Resize += new System.EventHandler(this.frmConsumptionHistoryReport_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGroupBy)).EndInit();
			this.fraGroupBy.ResumeLayout(false);
			this.fraGroupBy.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkHideDetail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSelectionCriteria)).EndInit();
			this.fraSelectionCriteria.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraCategory)).EndInit();
			this.fraCategory.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedCategory)).EndInit();
			this.fraSelectedCategory.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraFrequency)).EndInit();
			this.fraFrequency.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedFrequency)).EndInit();
			this.fraSelectedFrequency.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsFrequency)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraMeterSize)).EndInit();
			this.fraMeterSize.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraSelectedMeterSize)).EndInit();
			this.fraSelectedMeterSize.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsMeterSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePreview)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdFilePreview;
		private System.ComponentModel.IContainer components;
		private FCButton cmdFilePrint;
	}
}
