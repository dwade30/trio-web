﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.VisualBasicLayer;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCreateDatabaseExtract.
	/// </summary>
	public partial class frmCreateDatabaseExtract : BaseForm
	{
		public frmCreateDatabaseExtract()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCreateDatabaseExtract InstancePtr
		{
			get
			{
				return (frmCreateDatabaseExtract)Sys.GetInstance(typeof(frmCreateDatabaseExtract));
			}
		}

		protected frmCreateDatabaseExtract _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By         Dave Wade
		// Date               7/16/04
		// This screen will be used to create a database
		// extract
		// ********************************************************
		int intItems;
		public string strTitles = string.Empty;
		string strSQL = "";

		private void frmCreateDatabaseExtract_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCreateDatabaseExtract_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreateDatabaseExtract properties;
			//frmCreateDatabaseExtract.FillStyle	= 0;
			//frmCreateDatabaseExtract.ScaleWidth	= 5880;
			//frmCreateDatabaseExtract.ScaleHeight	= 4080;
			//frmCreateDatabaseExtract.LinkTopic	= "Form2";
			//frmCreateDatabaseExtract.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmCreateDatabaseExtract_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			bool blnSelectedItems;
			int intRecords;
			blnSelectedItems = false;
			intItems = 0;
			strTitles = "";
			if (chkAccountNumber.CheckState == Wisej.Web.CheckState.Checked)
			{
				blnSelectedItems = true;
				intItems += 1;
				strTitles += "Account Number, ";
			}
			if (chkName.CheckState == Wisej.Web.CheckState.Checked)
			{
				blnSelectedItems = true;
				intItems += 1;
				strTitles += "Name, ";
			}
			if (chkLocation.CheckState == Wisej.Web.CheckState.Checked)
			{
				blnSelectedItems = true;
				intItems += 1;
				strTitles += "Location Street Number, Location Apt, Location Street, ";
			}
			if (chkMailingAddress.CheckState == Wisej.Web.CheckState.Checked)
			{
				blnSelectedItems = true;
				intItems += 1;
				strTitles += "Mailing Address Line 1, Mailing Address Line 2, Mailing Address Line 3, Mailing Address City, Mailing Address State, Mailing Address Zip, ";
			}
			if (chkCategory.CheckState == Wisej.Web.CheckState.Checked)
			{
				blnSelectedItems = true;
				intItems += 1;
				strTitles += "Water Category, Sewer Category, ";
			}
			if (chkMapLot.CheckState == Wisej.Web.CheckState.Checked)
			{
				blnSelectedItems = true;
				intItems += 1;
				strTitles += "Map Lot, ";
			}
			if (chkOwnerName.CheckState == Wisej.Web.CheckState.Checked)
			{
				blnSelectedItems = true;
				intItems += 1;
				strTitles += "Owner Name, ";
			}
			if (!blnSelectedItems)
			{
				MessageBox.Show("You must select at least one piece of data to extract before you may continue.", "Select Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return;
			}
			else
			{
				strTitles = Strings.Left(strTitles, strTitles.Length - 2);
			}
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Extracting Data", true);
			intRecords = CreateDataExtract_2("UTEXTRCT.txt");
			frmWait.InstancePtr.Unload();
            //Application.DoEvents();
            //MessageBox.Show(FCConvert.ToString(intRecords) + " records have been extracted to " + Application.MapPath("\\") + "\\" + "UTEXTRCT.txt successfully!");
            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\UTEXTRCT.txt");
            frmReportViewer.InstancePtr.Init(rptRecordLayout.InstancePtr);
			//rptRecordLayout.InstancePtr.Show(App.MainForm);
		}

		private short CreateDataExtract_2(string strFileName)
		{
			return CreateDataExtract(ref strFileName);
		}

		private short CreateDataExtract(ref string strFileName)
		{
			short CreateDataExtract = 0;
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			int intItemsWritten = 0;
			clsDRWrapper rsCategoryInfo = new clsDRWrapper();
			string strCategory = "";
			CreateDataExtract = 0;
			if (chkFinalBilled.Checked)
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM Master ORDER BY AccountNumber");
			}
			else
			{
				rsAccountInfo.OpenRecordset("SELECT * FROM Master WHERE FinalBill = false ORDER BY AccountNumber");
			}
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				FCFileSystem.FileClose(1);
				FCFileSystem.FileOpen(1, strFileName, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				do
				{
					frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(CreateDataExtract + 1) / rsAccountInfo.RecordCount()) * 100);
					//Application.DoEvents();
					intItemsWritten = 0;
					if (chkAccountNumber.CheckState == Wisej.Web.CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							FCFileSystem.WriteLine(1, rsAccountInfo.Get_Fields("AccountNumber"));
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							FCFileSystem.Write(1, rsAccountInfo.Get_Fields("AccountNumber"));
						}
					}
					if (chkName.CheckState == Wisej.Web.CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsAccountInfo.Get_Fields_String("Name"));
						}
						else
						{
							FCFileSystem.Write(1, rsAccountInfo.Get_Fields_String("Name"));
						}
					}
					if (chkLocation.CheckState == Wisej.Web.CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							FCFileSystem.WriteLine(1, rsAccountInfo.Get_Fields("StreetNumber"), rsAccountInfo.Get_Fields_String("Apt"), rsAccountInfo.Get_Fields_String("StreetName"));
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							FCFileSystem.Write(1, rsAccountInfo.Get_Fields("StreetNumber"), rsAccountInfo.Get_Fields_String("Apt"), rsAccountInfo.Get_Fields_String("StreetName"));
						}
					}
					if (chkMailingAddress.CheckState == Wisej.Web.CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("BZip4"))) != "")
							{
								FCFileSystem.WriteLine(1, rsAccountInfo.Get_Fields_String("BAddress1"), rsAccountInfo.Get_Fields_String("BAddress2"), rsAccountInfo.Get_Fields_String("BAddress3"), rsAccountInfo.Get_Fields_String("BCity"), rsAccountInfo.Get_Fields_String("BState"), rsAccountInfo.Get_Fields_String("BZip") + "-" + rsAccountInfo.Get_Fields_String("BZip4"));
							}
							else
							{
								FCFileSystem.WriteLine(1, rsAccountInfo.Get_Fields_String("BAddress1"), rsAccountInfo.Get_Fields_String("BAddress2"), rsAccountInfo.Get_Fields_String("BAddress3"), rsAccountInfo.Get_Fields_String("BCity"), rsAccountInfo.Get_Fields_String("BState"), rsAccountInfo.Get_Fields_String("BZip"));
							}
						}
						else
						{
							if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("BZip4"))) != "")
							{
								FCFileSystem.Write(1, rsAccountInfo.Get_Fields_String("BAddress1"), rsAccountInfo.Get_Fields_String("BAddress2"), rsAccountInfo.Get_Fields_String("BAddress3"), rsAccountInfo.Get_Fields_String("BCity"), rsAccountInfo.Get_Fields_String("BState"), rsAccountInfo.Get_Fields_String("BZip") + "-" + rsAccountInfo.Get_Fields_String("BZip4"));
							}
							else
							{
								FCFileSystem.Write(1, rsAccountInfo.Get_Fields_String("BAddress1"), rsAccountInfo.Get_Fields_String("BAddress2"), rsAccountInfo.Get_Fields_String("BAddress3"), rsAccountInfo.Get_Fields_String("BCity"), rsAccountInfo.Get_Fields_String("BState"), rsAccountInfo.Get_Fields_String("BZip"));
							}
						}
					}
					if (chkCategory.CheckState == Wisej.Web.CheckState.Checked)
					{
						intItemsWritten += 1;
						strCategory = "";
						rsCategoryInfo.OpenRecordset("SELECT * FROM Category WHERE Code = " + rsAccountInfo.Get_Fields_Int32("WaterCategory"));
						if (rsCategoryInfo.EndOfFile() != true && rsCategoryInfo.BeginningOfFile() != true)
						{
							strCategory = rsCategoryInfo.Get_Fields_String("LongDescription") + ", ";
						}
						else
						{
							strCategory = " , ";
						}
						rsCategoryInfo.OpenRecordset("SELECT * FROM Category WHERE Code = " + rsAccountInfo.Get_Fields_Int32("SewerCategory"));
						if (rsCategoryInfo.EndOfFile() != true && rsCategoryInfo.BeginningOfFile() != true)
						{
							strCategory += rsCategoryInfo.Get_Fields_String("LongDescription");
						}
						else
						{
							strCategory += " ";
						}
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, strCategory);
						}
						else
						{
							FCFileSystem.Write(1, strCategory);
						}
					}
					if (chkMapLot.CheckState == Wisej.Web.CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							FCFileSystem.WriteLine(1, rsAccountInfo.Get_Fields_String("MapLot"));
						}
						else
						{
							FCFileSystem.Write(1, rsAccountInfo.Get_Fields_String("MapLot"));
						}
					}
					if (chkOwnerName.CheckState == Wisej.Web.CheckState.Checked)
					{
						intItemsWritten += 1;
						if (intItemsWritten == intItems)
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							FCFileSystem.WriteLine(1, rsAccountInfo.Get_Fields("OwnerName"));
						}
						else
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							FCFileSystem.Write(1, rsAccountInfo.Get_Fields("OwnerName"));
						}
					}
					CreateDataExtract += 1;
					rsAccountInfo.MoveNext();
				}
				while (rsAccountInfo.EndOfFile() != true);
			}
			FCFileSystem.FileClose(1);
			return CreateDataExtract;
		}
	}
}
