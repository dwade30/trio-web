﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arCalculationSummary.
	/// </summary>
	public partial class arCalculationSummary : BaseSectionReport
	{
		public arCalculationSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Calculation Summary Screen";
			this.vsSSum = this.CustomVsSSum.Control as FCReportGrid;
			this.vsWSum = this.CustomVsWSum.Control as FCReportGrid;
		}

		public static arCalculationSummary InstancePtr
		{
			get
			{
				return (arCalculationSummary)Sys.GetInstance(typeof(arCalculationSummary));
			}
		}

		protected arCalculationSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			//FC:FINAL:AM:#994 - don't dispose the custom controls (FCReportGrid) because it throws a cross thread exception
			//base.Dispose(disposing);
			foreach (GrapeCity.ActiveReports.SectionReportModel.ARControl control in this.GetAllControls())
			{
				if (!(control is GrapeCity.ActiveReports.SectionReportModel.CustomControl))
				{
					control.Dispose();
				}
			}
		}
		// nObj = 1
		//   0	arCalculationSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/13/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/03/2005              *
		// ********************************************************
		int WRows;
		// this will hold the number of rows in the summary flex grid for each category for water
		int SRows;
		// this will hold the number of rows in the summary flex grid for each category for sewer
		int lngMaxRows;
		FCReportGrid vsWSum;
		FCReportGrid vsSSum;

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// fldBook.Text = Left$(.lblBook.Caption, 8)
			fldWOv.Text = Strings.Format(frmCalculationSummary.InstancePtr.WOverride, "#,##0.00");
			fldWFlat.Text = Strings.Format(frmCalculationSummary.InstancePtr.WFlat, "#,##0.00");
			fldWUnits.Text = Strings.Format(frmCalculationSummary.InstancePtr.WUnits, "#,##0.00");
			fldWCons.Text = Strings.Format(frmCalculationSummary.InstancePtr.WCons, "#,##0.00");
			fldWAdj.Text = Strings.Format(frmCalculationSummary.InstancePtr.WAdjust, "#,##0.00");
			fldWMisc.Text = Strings.Format(frmCalculationSummary.InstancePtr.WMisc, "#,##0.00");
			fldWTax.Text = Strings.Format(frmCalculationSummary.InstancePtr.WTax, "#,##0.00");
			fldWTotal.Text = Strings.Format(frmCalculationSummary.InstancePtr.WSubTotal, "#,##0.00");
			fldSOv.Text = Strings.Format(frmCalculationSummary.InstancePtr.SOverride, "#,##0.00");
			fldSFlat.Text = Strings.Format(frmCalculationSummary.InstancePtr.SFlat, "#,##0.00");
			fldSUnits.Text = Strings.Format(frmCalculationSummary.InstancePtr.SUnits, "#,##0.00");
			fldSCons.Text = Strings.Format(frmCalculationSummary.InstancePtr.SCons, "#,##0.00");
			fldSAdj.Text = Strings.Format(frmCalculationSummary.InstancePtr.SAdjust, "#,##0.00");
			fldSMisc.Text = Strings.Format(frmCalculationSummary.InstancePtr.SMisc, "#,##0.00");
			fldSTax.Text = Strings.Format(frmCalculationSummary.InstancePtr.STax, "#,##0.00");
			fldSTotal.Text = Strings.Format(frmCalculationSummary.InstancePtr.SSubTotal, "#,##0.00");
			WRows = frmCalculationSummary.InstancePtr.vsSumW.Rows - 1;
			SRows = frmCalculationSummary.InstancePtr.vsSumS.Rows - 1;
			if (frmCalculationSummary.InstancePtr.boolCalculate)
			{
				lblHiddenSummaryHeader.Visible = false;
			}
			else
			{
				lblHiddenSummaryHeader.Visible = true;
				lblHiddenSummaryHeader.Width = rptAnalysisReportsMaster.InstancePtr.lngWidth;
			}
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblWaterCalc.Text = "Stormwater";
				lblWaterSum.Text = "Stormwater";
			}
			Format_Grid();
			Fill_ARGrid();
			Adjust_GridHeight();
			FillHeaderLabels();
			// this sets the line length compared to the flex grids...
			if (WRows > SRows)
			{
				lnCat.Y2 = 3690 / 1440f + (WRows * 270) / 1440f;
				this.Detail.Height = lblHiddenSummaryHeader.Height + Line2.Height + CustomVsWSum.Top + CustomVsWSum.Height + 0.2F;
			}
			else
			{
				lnCat.Y2 = 3690 / 1440f + (SRows * 270) / 1440f;
				this.Detail.Height = lblHiddenSummaryHeader.Height + Line2.Height + CustomVsSSum.Top + CustomVsSSum.Height + 0.2F;
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			frmCalculationSummary.InstancePtr.Show();
			frmCalculationSummary.InstancePtr.Unload();
		}

		private void Format_Grid()
		{
			// formats the col widths and formats the last column for currency of both the flex grids
			vsWSum.ColWidth(0, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.09));
			vsWSum.ColWidth(1, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.37));
			vsWSum.ColWidth(2, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.13));
			vsWSum.ColWidth(3, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.18));
			vsWSum.ColWidth(4, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.24));
			vsSSum.ColWidth(0, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.09));
			vsSSum.ColWidth(1, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.37));
			vsSSum.ColWidth(2, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.13));
			vsSSum.ColWidth(3, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.18));
			vsSSum.ColWidth(4, FCConvert.ToInt32(vsWSum.WidthOriginal * 0.24));
		}

		private void Fill_ARGrid()
		{
			// fills the flex grids from the summary screen
			int i;
			vsWSum.Rows = WRows;
			for (i = 0; i <= WRows - 1; i++)
			{
				vsWSum.TextMatrix(i, 0, frmCalculationSummary.InstancePtr.vsSumW.TextMatrix(i + 1, 0));
				vsWSum.TextMatrix(i, 1, frmCalculationSummary.InstancePtr.vsSumW.TextMatrix(i + 1, 1));
				vsWSum.TextMatrix(i, 2, frmCalculationSummary.InstancePtr.vsSumW.TextMatrix(i + 1, 2));
				vsWSum.TextMatrix(i, 3, frmCalculationSummary.InstancePtr.vsSumW.TextMatrix(i + 1, 3));
				vsWSum.TextMatrix(i, 4, Strings.Format(frmCalculationSummary.InstancePtr.vsSumW.TextMatrix(i + 1, 4), "#,##0.00"));
			}
			lngMaxRows = i;
			vsSSum.Rows = SRows;
			for (i = 0; i <= SRows - 1; i++)
			{
				vsSSum.TextMatrix(i, 0, frmCalculationSummary.InstancePtr.vsSumS.TextMatrix(i + 1, 0));
				vsSSum.TextMatrix(i, 1, frmCalculationSummary.InstancePtr.vsSumS.TextMatrix(i + 1, 1));
				vsSSum.TextMatrix(i, 2, frmCalculationSummary.InstancePtr.vsSumS.TextMatrix(i + 1, 2));
				vsSSum.TextMatrix(i, 3, frmCalculationSummary.InstancePtr.vsSumS.TextMatrix(i + 1, 3));
				vsSSum.TextMatrix(i, 4, Strings.Format(frmCalculationSummary.InstancePtr.vsSumS.TextMatrix(i + 1, 4), "#,##0.00"));
			}
			if (i > lngMaxRows)
			{
				lngMaxRows = i;
			}
		}

		private void Adjust_GridHeight()
		{
			// this will adjust the height of both grid
			CustomVsWSum.Height = vsWSum.RowHeight(0) * vsWSum.Rows * 3;
			vsWSum.Height = vsWSum.RowHeight(0) * vsWSum.Rows * 3;
			CustomVsSSum.Height = vsSSum.RowHeight(0) * vsSSum.Rows * 3;
			vsSSum.Height = vsSSum.RowHeight(0) * vsSSum.Rows * 3;
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			lnCat.Y2 = lnCat.Y1 + ((lngMaxRows + 2) * 270) / 1440f;
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void arCalculationSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arCalculationSummary properties;
			//arCalculationSummary.Caption	= "Calculation Summary Screen";
			//arCalculationSummary.Icon	= "arCalculationSummary.dsx":0000";
			//arCalculationSummary.Left	= 0;
			//arCalculationSummary.Top	= 0;
			//arCalculationSummary.Width	= 11880;
			//arCalculationSummary.Height	= 8595;
			//arCalculationSummary.WindowState	= 2;
			//arCalculationSummary.SectionData	= "arCalculationSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
