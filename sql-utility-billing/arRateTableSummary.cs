﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arRateTableSummary.
	/// </summary>
	public partial class arRateTableSummary : BaseSectionReport
	{
		public arRateTableSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			// FC:FINAL:VGE - i912 Name changed
			this.Name = "Meter List w/ Rate Tables";
		}

		public static arRateTableSummary InstancePtr
		{
			get
			{
				return (arRateTableSummary)Sys.GetInstance(typeof(arRateTableSummary));
			}
		}

		protected arRateTableSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arRateTableSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/17/2005              *
		// ********************************************************
		int curAcct;
		clsDRWrapper rsData = new clsDRWrapper();
		double dblSUnitsTotal;
		double dblSFlatTotal;
		double dblSConsumptionTotal;
		double dblWUnitsTotal;
		double dblWFlatTotal;
		double dblWConsumptionTotal;

		public void Init()
		{
			rsData.OpenRecordset("SELECT Master.*,MeterTable.*,pOwn.FullNameLF AS OwnerName FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID INNER JOIN MeterTable ON Master.ID = MeterTable.AccountKey ORDER BY Master.AccountNumber, MeterNumber", modExtraModules.strUTDatabase);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No accounts with associated meters found.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
			}
			else
			{
				frmReportViewer.InstancePtr.Init(this);
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				string strTemp = "";
				int intHighestRow;
				intHighestRow = 1;
				// This will fill all the fields in
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAcct.Text = rsData.Get_Fields_String("AccountNumber");
				// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
				fldName.Text = rsData.Get_Fields_String("OwnerName");
				fldMeter.Text = FCConvert.ToString(rsData.Get_Fields_Int32("MeterNumber"));
				fldService.Text = rsData.Get_Fields_String("Service");
				if (FCConvert.ToString(rsData.Get_Fields_String("Service")) != "S")
				{
					for (intCT = 1; intCT <= 5; intCT++)
					{
						// TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
						if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("UseRate" + FCConvert.ToString(intCT))))
						{
							if (intCT > intHighestRow)
								intHighestRow = intCT;
							// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
							if (rsData.Get_Fields_Int32("WaterType" + FCConvert.ToString(intCT)) == 1)
							{
								strTemp = "Consum - ";
								// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
								dblWConsumptionTotal += rsData.Get_Fields_Double("WaterAmount" + FCConvert.ToString(intCT));
							}
							// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
							else if (rsData.Get_Fields_Int32("WaterType" + FCConvert.ToString(intCT)) == 3)
							{
								strTemp = "Units  - ";
								// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
								dblWUnitsTotal += rsData.Get_Fields_Double("WaterAmount" + FCConvert.ToString(intCT));
							}
								// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
								else if (rsData.Get_Fields_Int32("WaterType" + FCConvert.ToString(intCT)) == 2)
							{
								strTemp = "Flat   - ";
								// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
								dblWFlatTotal += rsData.Get_Fields_Double("WaterAmount" + FCConvert.ToString(intCT));
							}
							switch (intCT)
							{
								case 1:
									{
										fldWKey1.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT)));
										fldW1.Text = strTemp;
										// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
										fldWUnits1.Text = rsData.Get_Fields_String("WaterAmount" + FCConvert.ToString(intCT));
										break;
									}
								case 2:
									{
										fldWKey2.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT)));
										fldW2.Text = strTemp;
										// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
										fldWUnits2.Text = rsData.Get_Fields_String("WaterAmount" + FCConvert.ToString(intCT));
										break;
									}
								case 3:
									{
										fldWKey3.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT)));
										fldW3.Text = strTemp;
										// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
										fldWUnits3.Text = rsData.Get_Fields_String("WaterAmount" + FCConvert.ToString(intCT));
										break;
									}
								case 4:
									{
										fldWKey4.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT)));
										fldW4.Text = strTemp;
										// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
										fldWUnits4.Text = rsData.Get_Fields_String("WaterAmount" + FCConvert.ToString(intCT));
										break;
									}
								case 5:
									{
										fldWKey5.Text = FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT)));
										fldW5.Text = strTemp;
										// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
										fldWUnits5.Text = rsData.Get_Fields_String("WaterAmount" + FCConvert.ToString(intCT));
										break;
									}
							}
							//end switch
						}
						else
						{
							switch (intCT)
							{
								case 1:
									{
										fldWKey1.Text = "";
										fldW1.Text = "";
										fldWUnits1.Text = "";
										break;
									}
								case 2:
									{
										fldWKey2.Text = "";
										fldW2.Text = "";
										fldWUnits2.Text = "";
										break;
									}
								case 3:
									{
										fldWKey3.Text = "";
										fldW3.Text = "";
										fldWUnits3.Text = "";
										break;
									}
								case 4:
									{
										fldWKey4.Text = "";
										fldW4.Text = "";
										fldWUnits4.Text = "";
										break;
									}
								case 5:
									{
										fldWKey5.Text = "";
										fldW5.Text = "";
										fldWUnits5.Text = "";
										break;
									}
							}
							//end switch
						}
					}
				}
				else
				{
					fldWKey1.Text = "";
					fldW1.Text = "";
					fldWUnits1.Text = "";
					fldWKey2.Text = "";
					fldW2.Text = "";
					fldWUnits2.Text = "";
					fldWKey3.Text = "";
					fldW3.Text = "";
					fldWUnits3.Text = "";
					fldWKey4.Text = "";
					fldW4.Text = "";
					fldWUnits4.Text = "";
					fldWKey5.Text = "";
					fldW5.Text = "";
					fldWUnits5.Text = "";
				}
				if (FCConvert.ToString(rsData.Get_Fields_String("Service")) != "W")
				{
					for (intCT = 1; intCT <= 5; intCT++)
					{
						// TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
						if (rsData.Get_Fields_Boolean("UseRate" + FCConvert.ToString(intCT)))
						{
							if (intCT > intHighestRow)
								intHighestRow = intCT;
							// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
							if (rsData.Get_Fields_Int32("SewerType" + FCConvert.ToString(intCT)) == 1)
							{
								strTemp = "Consum - ";
								// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
								dblSConsumptionTotal += rsData.Get_Fields_Double("SewerAmount" + FCConvert.ToString(intCT));
							}
							// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
							else if (rsData.Get_Fields_Int32("SewerType" + FCConvert.ToString(intCT)) == 3)
							{
								strTemp = "Units  - ";
								// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
								dblSUnitsTotal += rsData.Get_Fields_Double("SewerAmount" + FCConvert.ToString(intCT));
							}
								// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
								else if (rsData.Get_Fields_Int32("SewerType" + FCConvert.ToString(intCT)) == 2)
							{
								strTemp = "Flat   - ";
								// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
								dblSFlatTotal += rsData.Get_Fields_Double("SewerAmount" + FCConvert.ToString(intCT));
							}
							switch (intCT)
							{
								case 1:
									{
										fldSKey1.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT)));
										fldS1.Text = strTemp;
										// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
										fldSUnits1.Text = rsData.Get_Fields_String("SewerAmount" + FCConvert.ToString(intCT));
										break;
									}
								case 2:
									{
										fldSKey2.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT)));
										fldS2.Text = strTemp;
										// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
										fldSUnits2.Text = rsData.Get_Fields_String("SewerAmount" + FCConvert.ToString(intCT));
										break;
									}
								case 3:
									{
										fldSKey3.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT)));
										fldS3.Text = strTemp;
										// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
										fldSUnits3.Text = rsData.Get_Fields_String("SewerAmount" + FCConvert.ToString(intCT));
										break;
									}
								case 4:
									{
										fldSKey4.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT)));
										FldS4.Text = strTemp;
										// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
										fldSUnits4.Text = rsData.Get_Fields_String("SewerAmount" + FCConvert.ToString(intCT));
										break;
									}
								case 5:
									{
										fldSKey5.Text = FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT)));
										fldS5.Text = strTemp;
										// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
										fldSUnits5.Text = rsData.Get_Fields_String("SewerAmount" + FCConvert.ToString(intCT));
										break;
									}
							}
							//end switch
						}
						else
						{
							switch (intCT)
							{
								case 1:
									{
										fldSKey1.Text = "";
										fldS1.Text = "";
										fldSUnits1.Text = "";
										break;
									}
								case 2:
									{
										fldSKey2.Text = "";
										fldS2.Text = "";
										fldSUnits2.Text = "";
										break;
									}
								case 3:
									{
										fldSKey3.Text = "";
										fldS3.Text = "";
										fldSUnits3.Text = "";
										break;
									}
								case 4:
									{
										fldSKey4.Text = "";
										FldS4.Text = "";
										fldSUnits4.Text = "";
										break;
									}
								case 5:
									{
										fldSKey5.Text = "";
										fldS5.Text = "";
										fldSUnits5.Text = "";
										break;
									}
							}
							//end switch
						}
					}
				}
				else
				{
					fldSKey1.Text = "";
					fldS1.Text = "";
					fldSUnits1.Text = "";
					fldSKey2.Text = "";
					fldS2.Text = "";
					fldSUnits2.Text = "";
					fldSKey3.Text = "";
					fldS3.Text = "";
					fldSUnits3.Text = "";
					fldSKey4.Text = "";
					FldS4.Text = "";
					fldSUnits4.Text = "";
					fldSKey5.Text = "";
					fldS5.Text = "";
					fldSUnits5.Text = "";
				}
				// set the height of the detail section
				Detail.Height = (intHighestRow * 200) / 1440f;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			FillHeaderLabels();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			curAcct = -1;
			dblWUnitsTotal = 0;
			dblWConsumptionTotal = 0;
			dblWFlatTotal = 0;
			dblSUnitsTotal = 0;
			dblSConsumptionTotal = 0;
			dblSFlatTotal = 0;
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblWater.Text = "Water";
				lblWaterTot.Text = "Water Totals";
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// frmRateTableSummary.Show
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsData.EndOfFile())
			{
				BindFields();
				rsData.MoveNext();
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldWConsumptionTotal.Text = Strings.Format(dblWConsumptionTotal, "#,##0.00");
			fldWUnitsTotal.Text = Strings.Format(dblWUnitsTotal, "#,##0.00");
			fldWFlatTotal.Text = Strings.Format(dblWFlatTotal, "#,##0.00");
			fldSConsumptionTotal.Text = Strings.Format(dblSConsumptionTotal, "#,##0.00");
			fldSUnitsTotal.Text = Strings.Format(dblSUnitsTotal, "#,##0.00");
			fldSFlatTotal.Text = Strings.Format(dblSFlatTotal, "#,##0.00");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void arRateTableSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arRateTableSummary properties;
			//arRateTableSummary.Caption	= "Rate Table Summary";
			//arRateTableSummary.Icon	= "arRateTableSummary.dsx":0000";
			//arRateTableSummary.Left	= 0;
			//arRateTableSummary.Top	= 0;
			//arRateTableSummary.Width	= 11880;
			//arRateTableSummary.Height	= 8475;
			//arRateTableSummary.WindowState	= 2;
			//arRateTableSummary.SectionData	= "arRateTableSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
