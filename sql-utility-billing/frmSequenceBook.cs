﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmSequenceBook.
	/// </summary>
	public partial class frmSequenceBook : BaseForm
	{
		public frmSequenceBook()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            txtStartSeq.AllowOnlyNumericInput();
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSequenceBook InstancePtr
		{
			get
			{
				return (frmSequenceBook)Sys.GetInstance(typeof(frmSequenceBook));
			}
		}

		protected frmSequenceBook _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		int lngBook;
		clsDRWrapper rs = new clsDRWrapper();
		int MeterKeyCol;
		int StreetNameCol;
		int StreetNumberCol;
		int AccountNumberCol;
		int NameCol;
		int LocationCol;
		int CurrentSeqCol;
		int NewSeqCol;
		bool blnSorting;
		//
		public void Init(ref int[] PassBookArray)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngBook = PassBookArray[1];
				rs.OpenRecordset("SELECT MeterTable.ID  AS MeterKey, pBill.FullNameLF AS Name, AccountNumber, StreetNumber, StreetName, Sequence FROM Master INNER JOIN MeterTable ON Master.ID = MeterTable.AccountKey INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID WHERE BookNumber = " + FCConvert.ToString(lngBook) + " ORDER BY Sequence");
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					this.Show(App.MainForm);
				}
				else
				{
					MessageBox.Show("There are no meters set up in this book.", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cmdSetSequence_Click(object sender, System.EventArgs e)
		{
			int counter;
			// vbPorter upgrade warning: lngSeq As int	OnWrite(string)
			int lngSeq;
			vsResequence.Row = 1;
			vsResequence.RowSel = vsResequence.Rows - 1;
            //FC:FINAL:AM:3736 - set the blnSorting flag to void changing the Col in RowColChange
            blnSorting = true;
            vsResequence.Col = CurrentSeqCol;
			vsResequence.Sort = FCGrid.SortSettings.flexSortNumericAscending;
            blnSorting = false;
            lngSeq = FCConvert.ToInt32(txtStartSeq.Text);
			for (counter = 1; counter <= vsResequence.Rows - 1; counter++)
			{
				vsResequence.TextMatrix(counter, NewSeqCol, FCConvert.ToString(lngSeq + (Conversion.Val(cboInterval.Text) * (counter - 1))));
			}
			//FC:FINAL:CHN - issue #1076: incorrect focus after setting the sequence
			vsResequence.TextMatrix(1, NewSeqCol, FCConvert.ToString(lngSeq));
			vsResequence.Editable = FCGrid.EditableSettings.flexEDNone;
			vsResequence.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsResequence.TextMatrix(1, NewSeqCol, FCConvert.ToString(lngSeq));
		}

		private void frmSequenceBook_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSequenceBook_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSequenceBook properties;
			//frmSequenceBook.FillStyle	= 0;
			//frmSequenceBook.ScaleWidth	= 9045;
			//frmSequenceBook.ScaleHeight	= 7380;
			//frmSequenceBook.LinkTopic	= "Form2";
			//frmSequenceBook.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			MeterKeyCol = 0;
			StreetNameCol = 2;
			StreetNumberCol = 1;
			AccountNumberCol = 3;
			NameCol = 4;
			LocationCol = 5;
			CurrentSeqCol = 6;
			NewSeqCol = 7;
			vsResequence.ExtendLastCol = true;
			vsResequence.ColHidden(MeterKeyCol, true);
			vsResequence.ColHidden(StreetNameCol, true);
			vsResequence.ColHidden(StreetNumberCol, true);
			vsResequence.TextMatrix(0, AccountNumberCol, "Account");
			vsResequence.TextMatrix(0, NameCol, "Name");
			vsResequence.TextMatrix(0, LocationCol, "Location");
			vsResequence.TextMatrix(0, CurrentSeqCol, "Curr Seq");
			vsResequence.TextMatrix(0, NewSeqCol, "New Seq");
			//FC:FINAL:MSH - I.Issue #974: set width by using widthOriginal as in VB6, depending on the scale mode of the parent
			//vsResequence.ColWidth(AccountNumberCol, FCConvert.ToInt32(vsResequence.Width * 0.09));
			//vsResequence.ColWidth(NameCol, FCConvert.ToInt32(vsResequence.Width * 0.35));
			//vsResequence.ColWidth(LocationCol, FCConvert.ToInt32(vsResequence.Width * 0.35));
			//vsResequence.ColWidth(CurrentSeqCol, FCConvert.ToInt32(vsResequence.Width * 0.1));
			//vsResequence.ColWidth(NewSeqCol, FCConvert.ToInt32(vsResequence.Width * 0.05));
			vsResequence.ColWidth(AccountNumberCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.09));
			vsResequence.ColWidth(NameCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.35));
			vsResequence.ColWidth(LocationCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.35));
			vsResequence.ColWidth(CurrentSeqCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.1));
			vsResequence.ColWidth(NewSeqCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.05));
			vsResequence.ColDataType(NewSeqCol, FCGrid.DataTypeSettings.flexDTLong);
			vsResequence.ColDataType(StreetNumberCol, FCGrid.DataTypeSettings.flexDTLong);
			FillGrid();
			FillCOmbo();
			blnSorting = false;
			if (vsResequence.Rows > 1)
			{
				vsResequence.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vsResequence.Rows - 1, CurrentSeqCol, 0xC0C0C0);
				vsResequence.Col = NewSeqCol;
			}
		}

		private void FillCOmbo()
		{
			int intCounter;
			for (intCounter = 1; intCounter <= 20; intCounter++)
			{
				cboInterval.AddItem(FCConvert.ToString(intCounter * 5));
			}
		}

		private void FillGrid()
		{
			do
			{
				vsResequence.Rows += 1;
				vsResequence.TextMatrix(vsResequence.Rows - 1, MeterKeyCol, FCConvert.ToString(rs.Get_Fields_Int32("MeterKey")));
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				vsResequence.TextMatrix(vsResequence.Rows - 1, AccountNumberCol, FCConvert.ToString(rs.Get_Fields("AccountNumber")));
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields("StreetNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					vsResequence.TextMatrix(vsResequence.Rows - 1, StreetNumberCol, FCConvert.ToString(rs.Get_Fields("StreetNumber")));
				}
				else
				{
					vsResequence.TextMatrix(vsResequence.Rows - 1, StreetNumberCol, FCConvert.ToString(0));
				}
				vsResequence.TextMatrix(vsResequence.Rows - 1, StreetNameCol, FCConvert.ToString(rs.Get_Fields_String("StreetName")));
				vsResequence.TextMatrix(vsResequence.Rows - 1, NameCol, FCConvert.ToString(rs.Get_Fields_String("Name")));
				vsResequence.TextMatrix(vsResequence.Rows - 1, LocationCol, FCConvert.ToString(rs.Get_Fields_String("Name")));
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rs.Get_Fields("StreetNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					vsResequence.TextMatrix(vsResequence.Rows - 1, LocationCol, rs.Get_Fields("StreetNumber") + " " + Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("StreetName"))));
				}
				else
				{
					vsResequence.TextMatrix(vsResequence.Rows - 1, LocationCol, Strings.Trim(FCConvert.ToString(rs.Get_Fields_String("StreetName"))));
				}
				// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
				vsResequence.TextMatrix(vsResequence.Rows - 1, CurrentSeqCol, FCConvert.ToString(rs.Get_Fields("Sequence")));
				rs.MoveNext();
			}
			while (rs.EndOfFile() != true);
			//FC:FINAL:CHN - issue #1075: allow numbers only
			vsResequence.EditingControlShowing -= VsResequence_EditingControlShowing;
			vsResequence.EditingControlShowing += VsResequence_EditingControlShowing;
		}

		private void frmSequenceBook_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmSequenceBook_Resize(object sender, System.EventArgs e)
		{
			//FC:FINAL:MSH - I.Issue #974: set width by using widthOriginal as in VB6, depending on the scale mode of the parent
			//vsResequence.ColWidth(AccountNumberCol, FCConvert.ToInt32(vsResequence.Width * 0.09));
			//vsResequence.ColWidth(NameCol, FCConvert.ToInt32(vsResequence.Width * 0.35));
			//vsResequence.ColWidth(LocationCol, FCConvert.ToInt32(vsResequence.Width * 0.35));
			//vsResequence.ColWidth(CurrentSeqCol, FCConvert.ToInt32(vsResequence.Width * 0.1));
			//vsResequence.ColWidth(NewSeqCol, FCConvert.ToInt32(vsResequence.Width * 0.05));
			vsResequence.ColWidth(AccountNumberCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.09));
			vsResequence.ColWidth(NameCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.35));
			vsResequence.ColWidth(LocationCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.35));
			vsResequence.ColWidth(CurrentSeqCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.1));
			vsResequence.ColWidth(NewSeqCol, FCConvert.ToInt32(vsResequence.WidthOriginal * 0.05));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			int counter;
			int counter2;
			int lngCheckSeq = 0;
			for (counter = 1; counter <= vsResequence.Rows - 1; counter++)
			{
				if (Strings.Trim(vsResequence.TextMatrix(counter, NewSeqCol)) == "0")
				{
					MessageBox.Show("You may not enter a new sequence of 0 for any account.  You must correct this issue before you may proceed.", "Invalid Sequence", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				lngCheckSeq = FCConvert.ToInt32(Math.Round(Conversion.Val(vsResequence.TextMatrix(counter, NewSeqCol))));
				if (lngCheckSeq != 0)
				{
					for (counter2 = counter + 1; counter2 <= vsResequence.Rows - 1; counter2++)
					{
						if (Conversion.Val(vsResequence.TextMatrix(counter2, NewSeqCol)) != 0)
						{
							if (lngCheckSeq == Conversion.Val(vsResequence.TextMatrix(counter2, NewSeqCol)))
							{
								MessageBox.Show("There is a duplicate of sequence " + FCConvert.ToString(lngCheckSeq) + ".  You must correct this duplicate before you may proceed.", "Duplicate Sequence", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
						}
						else
						{
							if (lngCheckSeq == Conversion.Val(vsResequence.TextMatrix(counter2, CurrentSeqCol)))
							{
								MessageBox.Show("There is a duplicate of sequence " + FCConvert.ToString(lngCheckSeq) + ".  You must correct this duplicate before you may proceed.", "Duplicate Sequence", MessageBoxButtons.OK, MessageBoxIcon.Information);
								return;
							}
						}
					}
				}
			}
			for (counter = 1; counter <= vsResequence.Rows - 1; counter++)
			{
				if (Conversion.Val(vsResequence.TextMatrix(counter, NewSeqCol)) != 0)
				{
					rs.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(Conversion.Val(vsResequence.TextMatrix(counter, MeterKeyCol))), modExtraModules.strUTDatabase);
					if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
					{
						rs.Edit();
						rs.Set_Fields("Sequence", FCConvert.ToString(Conversion.Val(vsResequence.TextMatrix(counter, NewSeqCol))));
						rs.Update(true);
					}
				}
			}
			//FC:FINAL:MSH - I.Issue #973: in VB6 automaticaly will be added project name to the title of message box if nothing is added as title
			MessageBox.Show("Save Completed!", System.Reflection.Assembly.GetExecutingAssembly().GetName().Name);
			this.Unload();
		}

		private void txtStartSeq_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return))
			{
				// do nothing
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsResequence_AfterSort(object sender, EventArgs e)
		{
			if (vsResequence.Col == LocationCol)
			{
				blnSorting = true;
				if (vsResequence.SortOrder == SortOrder.Ascending)
				{
					vsResequence.ColSort(StreetNumberCol, FCGrid.SortSettings.flexSortNumericAscending);
				}
				else
				{
					vsResequence.ColData(StreetNumberCol, FCGrid.SortSettings.flexSortGenericDescending);
				}
				if (vsResequence.SortOrder == SortOrder.Ascending)
				{
					vsResequence.ColSort(StreetNameCol, FCGrid.SortSettings.flexSortGenericAscending);
				}
				else
				{
					vsResequence.ColSort(StreetNameCol, FCGrid.SortSettings.flexSortGenericDescending);
				}
				vsResequence.RowSel = vsResequence.Rows - 1;
				vsResequence.Row = 1;
				vsResequence.ColSel = StreetNumberCol;
				vsResequence.Col = StreetNumberCol;
				vsResequence.Sort = FCGrid.SortSettings.flexSortCustom;
				vsResequence.ColSel = StreetNameCol;
				vsResequence.Col = StreetNameCol;
				vsResequence.Sort = FCGrid.SortSettings.flexSortCustom;
				blnSorting = false;
				vsResequence.Col = NewSeqCol;
			}
		}

		private void vsResequence_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsResequence_RowColChange(object sender, System.EventArgs e)
		{
			if (!blnSorting)
			{
				if (vsResequence.Col != NewSeqCol)
				{
					vsResequence.Col = NewSeqCol;
				}
				if (vsResequence.Row > 0)
				{
					vsResequence.EditCell();
				}
			}
		}
		//FC:FINAL:CHN - issue #1075: allow numbers only
		private void VsResequence_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				if (vsResequence.Col == 7)// "magic" number - editing column number.
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
                        box.RemoveAlphaCharactersOnValueChange();
                        box.AllowOnlyNumericInput();
					}
				}
			}
		}
	}
}
