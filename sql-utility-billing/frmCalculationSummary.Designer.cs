﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCalculationSummary.
	/// </summary>
	partial class frmCalculationSummary : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWS;
		public System.Collections.Generic.List<FCGrid> vsEdit;
		public fecherFoundation.FCPanel fraCalc;
		public FCGrid vsSumW;
		public FCGrid vsSumS;
		//public fecherFoundation.FCLine Line7;
		//public fecherFoundation.FCLine Line4;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label2;
		//public fecherFoundation.FCLine Line3;
		//public fecherFoundation.FCLine Line2;
		public fecherFoundation.FCLabel Label19;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label9;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel Label12;
		public fecherFoundation.FCLabel Label13;
		public fecherFoundation.FCLabel Label14;
		public fecherFoundation.FCLabel Label15;
		public fecherFoundation.FCLabel Label16;
		public fecherFoundation.FCLabel lblWOverride;
		public fecherFoundation.FCLabel lblWFlat;
		public fecherFoundation.FCLabel lblWUnits;
		public fecherFoundation.FCLabel lblWCons;
		public fecherFoundation.FCLabel lblWMisc;
		public fecherFoundation.FCLabel lblWAdjustment;
		public fecherFoundation.FCLabel lblWTax;
		public fecherFoundation.FCLabel lblWSubTotal;
		public fecherFoundation.FCLabel lblSOverride;
		public fecherFoundation.FCLabel lblSFlat;
		public fecherFoundation.FCLabel lblSUnits;
		public fecherFoundation.FCLabel lblSCons;
		public fecherFoundation.FCLabel lblSMisc;
		public fecherFoundation.FCLabel lblSAdjustment;
		public fecherFoundation.FCLabel lblSTax;
		public fecherFoundation.FCLabel lblSSubTotal;
		//public fecherFoundation.FCLine Line5;
		//public fecherFoundation.FCLine Line6;
		//public fecherFoundation.FCLine Line8;
		//public fecherFoundation.FCLine Line9;
		public fecherFoundation.FCLabel Label18;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblWater;
		public fecherFoundation.FCLabel lblWaterSum;
		public fecherFoundation.FCLabel Label20;
		public fecherFoundation.FCPanel fraBillingEdit;
		public FCGrid vsEdit_0;
		public FCGrid vsEdit_1;
		public FCGrid vsEdit_2;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCButton cmdSummary;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle11 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle12 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle13 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle14 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle15 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle16 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle17 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle18 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle19 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle20 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCalculationSummary));
			this.cmbWS = new fecherFoundation.FCComboBox();
			this.fraCalc = new fecherFoundation.FCPanel();
			this.vsSumW = new fecherFoundation.FCGrid();
			this.vsSumS = new fecherFoundation.FCGrid();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label19 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.Label8 = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.Label12 = new fecherFoundation.FCLabel();
			this.Label13 = new fecherFoundation.FCLabel();
			this.Label14 = new fecherFoundation.FCLabel();
			this.Label15 = new fecherFoundation.FCLabel();
			this.Label16 = new fecherFoundation.FCLabel();
			this.lblWOverride = new fecherFoundation.FCLabel();
			this.lblWFlat = new fecherFoundation.FCLabel();
			this.lblWUnits = new fecherFoundation.FCLabel();
			this.lblWCons = new fecherFoundation.FCLabel();
			this.lblWMisc = new fecherFoundation.FCLabel();
			this.lblWAdjustment = new fecherFoundation.FCLabel();
			this.lblWTax = new fecherFoundation.FCLabel();
			this.lblWSubTotal = new fecherFoundation.FCLabel();
			this.lblSOverride = new fecherFoundation.FCLabel();
			this.lblSFlat = new fecherFoundation.FCLabel();
			this.lblSUnits = new fecherFoundation.FCLabel();
			this.lblSCons = new fecherFoundation.FCLabel();
			this.lblSMisc = new fecherFoundation.FCLabel();
			this.lblSAdjustment = new fecherFoundation.FCLabel();
			this.lblSTax = new fecherFoundation.FCLabel();
			this.Label18 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblWater = new fecherFoundation.FCLabel();
			this.lblWaterSum = new fecherFoundation.FCLabel();
			this.Label20 = new fecherFoundation.FCLabel();
			this.lblSSubTotal = new fecherFoundation.FCLabel();
			this.fraBillingEdit = new fecherFoundation.FCPanel();
			this.vsEdit_2 = new fecherFoundation.FCGrid();
			this.vsEdit_0 = new fecherFoundation.FCGrid();
			this.vsEdit_1 = new fecherFoundation.FCGrid();
			this.cmdPrint = new fecherFoundation.FCButton();
			this.cmdSummary = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCalc)).BeginInit();
			this.fraCalc.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsSumW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSumS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBillingEdit)).BeginInit();
			this.fraBillingEdit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsEdit_2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsEdit_0)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsEdit_1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSummary)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPrint);
			this.BottomPanel.Location = new System.Drawing.Point(0, 558);
			this.BottomPanel.Size = new System.Drawing.Size(898, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraBillingEdit);
			this.ClientArea.Controls.Add(this.fraCalc);
			this.ClientArea.Size = new System.Drawing.Size(898, 498);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdSummary);
			this.TopPanel.Size = new System.Drawing.Size(898, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdSummary, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			// 
			// cmbWS
			// 
			this.cmbWS.AutoSize = false;
			this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWS.FormattingEnabled = true;
			this.cmbWS.Items.AddRange(new object[] {
				"Billing Summary",
				"Category Reports",
				"Meter Reports"
			});
			this.cmbWS.Location = new System.Drawing.Point(30, 30);
			this.cmbWS.Name = "cmbWS";
			this.cmbWS.Size = new System.Drawing.Size(179, 40);
			this.cmbWS.TabIndex = 48;
			this.cmbWS.Text = "Billing Summary";
			this.cmbWS.SelectedIndexChanged += new System.EventHandler(this.cmbWS_SelectedIndexChanged);
			// 
			// fraCalc
			// 
			this.fraCalc.BackColor = System.Drawing.Color.White;
			this.fraCalc.Controls.Add(this.vsSumW);
			this.fraCalc.Controls.Add(this.vsSumS);
			this.fraCalc.Controls.Add(this.Label6);
			this.fraCalc.Controls.Add(this.Label5);
			this.fraCalc.Controls.Add(this.Label4);
			this.fraCalc.Controls.Add(this.Label2);
			this.fraCalc.Controls.Add(this.Label19);
			this.fraCalc.Controls.Add(this.Label3);
			this.fraCalc.Controls.Add(this.Label7);
			this.fraCalc.Controls.Add(this.Label8);
			this.fraCalc.Controls.Add(this.Label9);
			this.fraCalc.Controls.Add(this.Label10);
			this.fraCalc.Controls.Add(this.Label11);
			this.fraCalc.Controls.Add(this.Label12);
			this.fraCalc.Controls.Add(this.Label13);
			this.fraCalc.Controls.Add(this.Label14);
			this.fraCalc.Controls.Add(this.Label15);
			this.fraCalc.Controls.Add(this.Label16);
			this.fraCalc.Controls.Add(this.lblWOverride);
			this.fraCalc.Controls.Add(this.lblWFlat);
			this.fraCalc.Controls.Add(this.lblWUnits);
			this.fraCalc.Controls.Add(this.lblWCons);
			this.fraCalc.Controls.Add(this.lblWMisc);
			this.fraCalc.Controls.Add(this.lblWAdjustment);
			this.fraCalc.Controls.Add(this.lblWTax);
			this.fraCalc.Controls.Add(this.lblWSubTotal);
			this.fraCalc.Controls.Add(this.lblSOverride);
			this.fraCalc.Controls.Add(this.lblSFlat);
			this.fraCalc.Controls.Add(this.lblSUnits);
			this.fraCalc.Controls.Add(this.lblSCons);
			this.fraCalc.Controls.Add(this.lblSMisc);
			this.fraCalc.Controls.Add(this.lblSAdjustment);
			this.fraCalc.Controls.Add(this.lblSTax);
			this.fraCalc.Controls.Add(this.Label18);
			this.fraCalc.Controls.Add(this.Label1);
			this.fraCalc.Controls.Add(this.lblWater);
			this.fraCalc.Controls.Add(this.lblWaterSum);
			this.fraCalc.Controls.Add(this.Label20);
			this.fraCalc.Controls.Add(this.lblSSubTotal);
			this.fraCalc.Location = new System.Drawing.Point(0, 0);
			this.fraCalc.Name = "fraCalc";
			this.fraCalc.Size = new System.Drawing.Size(841, 526);
			this.fraCalc.TabIndex = 7;
			this.fraCalc.Visible = false;
			// 
			// vsSumW
			// 
			this.vsSumW.AllowSelection = false;
			this.vsSumW.AllowUserToResizeColumns = false;
			this.vsSumW.AllowUserToResizeRows = false;
			this.vsSumW.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSumW.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSumW.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSumW.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSumW.BackColorSel = System.Drawing.Color.Empty;
			this.vsSumW.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsSumW.Cols = 5;
			dataGridViewCellStyle11.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsSumW.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
			this.vsSumW.ColumnHeadersHeight = 30;
			this.vsSumW.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle12.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSumW.DefaultCellStyle = dataGridViewCellStyle12;
			this.vsSumW.DragIcon = null;
			this.vsSumW.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsSumW.FixedCols = 0;
			this.vsSumW.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsSumW.FrozenCols = 0;
			this.vsSumW.GridColor = System.Drawing.Color.Empty;
			this.vsSumW.GridColorFixed = System.Drawing.Color.Empty;
			this.vsSumW.Location = new System.Drawing.Point(30, 365);
			this.vsSumW.Name = "vsSumW";
			this.vsSumW.ReadOnly = true;
			this.vsSumW.RowHeadersVisible = false;
			this.vsSumW.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSumW.RowHeightMin = 0;
			this.vsSumW.Rows = 2;
			this.vsSumW.ScrollTipText = null;
			this.vsSumW.ShowColumnVisibilityMenu = false;
			this.vsSumW.ShowFocusCell = false;
			this.vsSumW.Size = new System.Drawing.Size(388, 174);
			this.vsSumW.StandardTab = true;
			this.vsSumW.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsSumW.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsSumW.TabIndex = 45;
			// 
			// vsSumS
			// 
			this.vsSumS.AllowSelection = false;
			this.vsSumS.AllowUserToResizeColumns = false;
			this.vsSumS.AllowUserToResizeRows = false;
			this.vsSumS.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsSumS.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsSumS.BackColorBkg = System.Drawing.Color.Empty;
			this.vsSumS.BackColorFixed = System.Drawing.Color.Empty;
			this.vsSumS.BackColorSel = System.Drawing.Color.Empty;
			this.vsSumS.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsSumS.Cols = 5;
			dataGridViewCellStyle13.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsSumS.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
			this.vsSumS.ColumnHeadersHeight = 30;
			this.vsSumS.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle14.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsSumS.DefaultCellStyle = dataGridViewCellStyle14;
			this.vsSumS.DragIcon = null;
			this.vsSumS.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsSumS.FixedCols = 0;
			this.vsSumS.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsSumS.FrozenCols = 0;
			this.vsSumS.GridColor = System.Drawing.Color.Empty;
			this.vsSumS.GridColorFixed = System.Drawing.Color.Empty;
			this.vsSumS.Location = new System.Drawing.Point(448, 365);
			this.vsSumS.Name = "vsSumS";
			this.vsSumS.ReadOnly = true;
			this.vsSumS.RowHeadersVisible = false;
			this.vsSumS.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsSumS.RowHeightMin = 0;
			this.vsSumS.Rows = 2;
			this.vsSumS.ScrollTipText = null;
			this.vsSumS.ShowColumnVisibilityMenu = false;
			this.vsSumS.ShowFocusCell = false;
			this.vsSumS.Size = new System.Drawing.Size(388, 174);
			this.vsSumS.StandardTab = true;
			this.vsSumS.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsSumS.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsSumS.TabIndex = 46;
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(30, 248);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(100, 17);
			this.Label6.TabIndex = 43;
			this.Label6.Text = "SUB TOTAL";
			this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(30, 136);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(100, 17);
			this.Label5.TabIndex = 42;
			this.Label5.Text = "CONSUMPTION";
			this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(30, 108);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(100, 17);
			this.Label4.TabIndex = 41;
			this.Label4.Text = "UNITS";
			this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(30, 52);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(100, 17);
			this.Label2.TabIndex = 40;
			this.Label2.Text = "OVERRIDE";
			this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label19
			// 
			this.Label19.Location = new System.Drawing.Point(30, 164);
			this.Label19.Name = "Label19";
			this.Label19.Size = new System.Drawing.Size(100, 17);
			this.Label19.TabIndex = 37;
			this.Label19.Text = "MISCELLANEOUS";
			this.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(30, 80);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(100, 17);
			this.Label3.TabIndex = 36;
			this.Label3.Text = "FLAT";
			this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(30, 192);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(100, 17);
			this.Label7.TabIndex = 35;
			this.Label7.Text = "ADJUSTMENTS";
			this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label8
			// 
			this.Label8.Location = new System.Drawing.Point(30, 220);
			this.Label8.Name = "Label8";
			this.Label8.Size = new System.Drawing.Size(100, 17);
			this.Label8.TabIndex = 34;
			this.Label8.Text = "TAX";
			this.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(448, 52);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(100, 17);
			this.Label9.TabIndex = 33;
			this.Label9.Text = "OVERRIDE";
			this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(448, 108);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(100, 17);
			this.Label10.TabIndex = 32;
			this.Label10.Text = "UNITS";
			this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(448, 136);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(100, 17);
			this.Label11.TabIndex = 31;
			this.Label11.Text = "CONSUMPTION";
			this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label12
			// 
			this.Label12.Location = new System.Drawing.Point(448, 248);
			this.Label12.Name = "Label12";
			this.Label12.Size = new System.Drawing.Size(100, 17);
			this.Label12.TabIndex = 30;
			this.Label12.Text = "SUB TOTAL";
			this.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label13
			// 
			this.Label13.Location = new System.Drawing.Point(448, 164);
			this.Label13.Name = "Label13";
			this.Label13.Size = new System.Drawing.Size(100, 17);
			this.Label13.TabIndex = 29;
			this.Label13.Text = "MISCELLANEOUS";
			this.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label14
			// 
			this.Label14.Location = new System.Drawing.Point(448, 80);
			this.Label14.Name = "Label14";
			this.Label14.Size = new System.Drawing.Size(100, 17);
			this.Label14.TabIndex = 28;
			this.Label14.Text = "FLAT";
			this.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label15
			// 
			this.Label15.Location = new System.Drawing.Point(448, 192);
			this.Label15.Name = "Label15";
			this.Label15.Size = new System.Drawing.Size(100, 17);
			this.Label15.TabIndex = 27;
			this.Label15.Text = "ADJUSTMENTS";
			this.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// Label16
			// 
			this.Label16.Location = new System.Drawing.Point(448, 220);
			this.Label16.Name = "Label16";
			this.Label16.Size = new System.Drawing.Size(100, 17);
			this.Label16.TabIndex = 26;
			this.Label16.Text = "TAX";
			this.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblWOverride
			// 
			this.lblWOverride.Location = new System.Drawing.Point(278, 52);
			this.lblWOverride.Name = "lblWOverride";
			this.lblWOverride.Size = new System.Drawing.Size(140, 17);
			this.lblWOverride.TabIndex = 25;
			this.lblWOverride.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblWFlat
			// 
			this.lblWFlat.Location = new System.Drawing.Point(278, 80);
			this.lblWFlat.Name = "lblWFlat";
			this.lblWFlat.Size = new System.Drawing.Size(140, 17);
			this.lblWFlat.TabIndex = 24;
			this.lblWFlat.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblWUnits
			// 
			this.lblWUnits.Location = new System.Drawing.Point(278, 108);
			this.lblWUnits.Name = "lblWUnits";
			this.lblWUnits.Size = new System.Drawing.Size(140, 17);
			this.lblWUnits.TabIndex = 23;
			this.lblWUnits.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblWCons
			// 
			this.lblWCons.Location = new System.Drawing.Point(278, 136);
			this.lblWCons.Name = "lblWCons";
			this.lblWCons.Size = new System.Drawing.Size(140, 17);
			this.lblWCons.TabIndex = 22;
			this.lblWCons.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblWMisc
			// 
			this.lblWMisc.Location = new System.Drawing.Point(278, 164);
			this.lblWMisc.Name = "lblWMisc";
			this.lblWMisc.Size = new System.Drawing.Size(140, 17);
			this.lblWMisc.TabIndex = 21;
			this.lblWMisc.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblWAdjustment
			// 
			this.lblWAdjustment.Location = new System.Drawing.Point(278, 192);
			this.lblWAdjustment.Name = "lblWAdjustment";
			this.lblWAdjustment.Size = new System.Drawing.Size(140, 17);
			this.lblWAdjustment.TabIndex = 20;
			this.lblWAdjustment.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblWTax
			// 
			this.lblWTax.Location = new System.Drawing.Point(278, 220);
			this.lblWTax.Name = "lblWTax";
			this.lblWTax.Size = new System.Drawing.Size(140, 17);
			this.lblWTax.TabIndex = 19;
			this.lblWTax.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblWSubTotal
			// 
			this.lblWSubTotal.Location = new System.Drawing.Point(278, 248);
			this.lblWSubTotal.Name = "lblWSubTotal";
			this.lblWSubTotal.Size = new System.Drawing.Size(140, 17);
			this.lblWSubTotal.TabIndex = 18;
			this.lblWSubTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSOverride
			// 
			this.lblSOverride.Location = new System.Drawing.Point(696, 52);
			this.lblSOverride.Name = "lblSOverride";
			this.lblSOverride.Size = new System.Drawing.Size(140, 17);
			this.lblSOverride.TabIndex = 17;
			this.lblSOverride.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSFlat
			// 
			this.lblSFlat.Location = new System.Drawing.Point(696, 80);
			this.lblSFlat.Name = "lblSFlat";
			this.lblSFlat.Size = new System.Drawing.Size(140, 17);
			this.lblSFlat.TabIndex = 16;
			this.lblSFlat.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSUnits
			// 
			this.lblSUnits.Location = new System.Drawing.Point(696, 108);
			this.lblSUnits.Name = "lblSUnits";
			this.lblSUnits.Size = new System.Drawing.Size(140, 17);
			this.lblSUnits.TabIndex = 15;
			this.lblSUnits.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSCons
			// 
			this.lblSCons.Location = new System.Drawing.Point(696, 136);
			this.lblSCons.Name = "lblSCons";
			this.lblSCons.Size = new System.Drawing.Size(140, 17);
			this.lblSCons.TabIndex = 14;
			this.lblSCons.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSMisc
			// 
			this.lblSMisc.Location = new System.Drawing.Point(696, 164);
			this.lblSMisc.Name = "lblSMisc";
			this.lblSMisc.Size = new System.Drawing.Size(140, 17);
			this.lblSMisc.TabIndex = 13;
			this.lblSMisc.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSAdjustment
			// 
			this.lblSAdjustment.Location = new System.Drawing.Point(696, 192);
			this.lblSAdjustment.Name = "lblSAdjustment";
			this.lblSAdjustment.Size = new System.Drawing.Size(140, 17);
			this.lblSAdjustment.TabIndex = 12;
			this.lblSAdjustment.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// lblSTax
			// 
			this.lblSTax.Location = new System.Drawing.Point(696, 220);
			this.lblSTax.Name = "lblSTax";
			this.lblSTax.Size = new System.Drawing.Size(140, 17);
			this.lblSTax.TabIndex = 11;
			this.lblSTax.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// Label18
			// 
			this.Label18.BackColor = System.Drawing.Color.Transparent;
			this.Label18.Location = new System.Drawing.Point(30, 295);
			this.Label18.Name = "Label18";
			this.Label18.Size = new System.Drawing.Size(806, 15);
			this.Label18.TabIndex = 44;
			this.Label18.Text = "USER CATEGORY SUMMARY";
			this.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Label1
			// 
			this.Label1.BackColor = System.Drawing.Color.Transparent;
			this.Label1.Location = new System.Drawing.Point(448, 21);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(388, 25);
			this.Label1.TabIndex = 39;
			this.Label1.Text = "SEWER";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblWater
			// 
			this.lblWater.BackColor = System.Drawing.Color.Transparent;
			this.lblWater.Location = new System.Drawing.Point(30, 21);
			this.lblWater.Name = "lblWater";
			this.lblWater.Size = new System.Drawing.Size(388, 25);
			this.lblWater.TabIndex = 38;
			this.lblWater.Text = "WATER";
			this.lblWater.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblWaterSum
			// 
			this.lblWaterSum.BackColor = System.Drawing.Color.Transparent;
			this.lblWaterSum.Location = new System.Drawing.Point(30, 330);
			this.lblWaterSum.Name = "lblWaterSum";
			this.lblWaterSum.Size = new System.Drawing.Size(388, 15);
			this.lblWaterSum.TabIndex = 9;
			this.lblWaterSum.Text = "WATER";
			this.lblWaterSum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// Label20
			// 
			this.Label20.BackColor = System.Drawing.Color.Transparent;
			this.Label20.Location = new System.Drawing.Point(448, 330);
			this.Label20.Name = "Label20";
			this.Label20.Size = new System.Drawing.Size(388, 15);
			this.Label20.TabIndex = 8;
			this.Label20.Text = "SEWER";
			this.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblSSubTotal
			// 
			this.lblSSubTotal.Location = new System.Drawing.Point(696, 248);
			this.lblSSubTotal.Name = "lblSSubTotal";
			this.lblSSubTotal.Size = new System.Drawing.Size(140, 17);
			this.lblSSubTotal.TabIndex = 10;
			this.lblSSubTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
			// 
			// fraBillingEdit
			// 
			this.fraBillingEdit.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraBillingEdit.BackColor = System.Drawing.Color.White;
			this.fraBillingEdit.Controls.Add(this.cmbWS);
			this.fraBillingEdit.Controls.Add(this.vsEdit_2);
			this.fraBillingEdit.Controls.Add(this.vsEdit_0);
			this.fraBillingEdit.Controls.Add(this.vsEdit_1);
			this.fraBillingEdit.Location = new System.Drawing.Point(0, 0);
			this.fraBillingEdit.Name = "fraBillingEdit";
			this.fraBillingEdit.Size = new System.Drawing.Size(853, 456);
			this.fraBillingEdit.TabIndex = 2;
			this.fraBillingEdit.Visible = false;
			// 
			// vsEdit_2
			// 
			this.vsEdit_2.AllowSelection = false;
			this.vsEdit_2.AllowUserToResizeColumns = false;
			this.vsEdit_2.AllowUserToResizeRows = false;
			this.vsEdit_2.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsEdit_2.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsEdit_2.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsEdit_2.BackColorBkg = System.Drawing.Color.Empty;
			this.vsEdit_2.BackColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_2.BackColorSel = System.Drawing.Color.Empty;
			this.vsEdit_2.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsEdit_2.Cols = 4;
			dataGridViewCellStyle15.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsEdit_2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
			this.vsEdit_2.ColumnHeadersHeight = 30;
			this.vsEdit_2.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle16.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsEdit_2.DefaultCellStyle = dataGridViewCellStyle16;
			this.vsEdit_2.DragIcon = null;
			this.vsEdit_2.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsEdit_2.ExtendLastCol = true;
			this.vsEdit_2.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_2.FrozenCols = 0;
			this.vsEdit_2.GridColor = System.Drawing.Color.Empty;
			this.vsEdit_2.GridColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_2.Location = new System.Drawing.Point(30, 90);
			this.vsEdit_2.Name = "vsEdit_2";
			this.vsEdit_2.ReadOnly = true;
			this.vsEdit_2.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsEdit_2.RowHeightMin = 0;
			this.vsEdit_2.Rows = 1;
			this.vsEdit_2.ScrollTipText = null;
			this.vsEdit_2.ShowColumnVisibilityMenu = false;
			this.vsEdit_2.ShowFocusCell = false;
			this.vsEdit_2.Size = new System.Drawing.Size(823, 267);
			this.vsEdit_2.StandardTab = true;
			this.vsEdit_2.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsEdit_2.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsEdit_2.TabIndex = 49;
			this.vsEdit_2.Visible = false;
			this.vsEdit_2.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vsEdit_AfterCollapse);
			// 
			// vsEdit_0
			// 
			this.vsEdit_0.AllowSelection = false;
			this.vsEdit_0.AllowUserToResizeColumns = false;
			this.vsEdit_0.AllowUserToResizeRows = false;
			this.vsEdit_0.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsEdit_0.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsEdit_0.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsEdit_0.BackColorBkg = System.Drawing.Color.Empty;
			this.vsEdit_0.BackColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_0.BackColorSel = System.Drawing.Color.Empty;
			this.vsEdit_0.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsEdit_0.Cols = 13;
			dataGridViewCellStyle17.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsEdit_0.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
			this.vsEdit_0.ColumnHeadersHeight = 60;
			this.vsEdit_0.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle18.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsEdit_0.DefaultCellStyle = dataGridViewCellStyle18;
			this.vsEdit_0.DragIcon = null;
			this.vsEdit_0.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsEdit_0.ExtendLastCol = true;
			this.vsEdit_0.FixedRows = 2;
			this.vsEdit_0.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_0.FrozenCols = 0;
			this.vsEdit_0.GridColor = System.Drawing.Color.Empty;
			this.vsEdit_0.GridColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_0.Location = new System.Drawing.Point(30, 90);
			this.vsEdit_0.Name = "vsEdit_0";
			this.vsEdit_0.ReadOnly = true;
			this.vsEdit_0.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsEdit_0.RowHeightMin = 0;
			this.vsEdit_0.Rows = 3;
			this.vsEdit_0.ScrollTipText = null;
			this.vsEdit_0.ShowColumnVisibilityMenu = false;
			this.vsEdit_0.ShowFocusCell = false;
			this.vsEdit_0.Size = new System.Drawing.Size(823, 267);
			this.vsEdit_0.StandardTab = true;
			this.vsEdit_0.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsEdit_0.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsEdit_0.TabIndex = 47;
			this.vsEdit_0.Visible = false;
			this.vsEdit_0.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vsEdit_AfterCollapse);
			// 
			// vsEdit_1
			// 
			this.vsEdit_1.AllowSelection = false;
			this.vsEdit_1.AllowUserToResizeColumns = false;
			this.vsEdit_1.AllowUserToResizeRows = false;
			this.vsEdit_1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsEdit_1.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsEdit_1.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsEdit_1.BackColorBkg = System.Drawing.Color.Empty;
			this.vsEdit_1.BackColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_1.BackColorSel = System.Drawing.Color.Empty;
			this.vsEdit_1.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsEdit_1.Cols = 9;
			dataGridViewCellStyle19.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsEdit_1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle19;
			this.vsEdit_1.ColumnHeadersHeight = 60;
			this.vsEdit_1.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle20.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsEdit_1.DefaultCellStyle = dataGridViewCellStyle20;
			this.vsEdit_1.DragIcon = null;
			this.vsEdit_1.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsEdit_1.ExtendLastCol = true;
			this.vsEdit_1.FixedRows = 2;
			this.vsEdit_1.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_1.FrozenCols = 0;
			this.vsEdit_1.GridColor = System.Drawing.Color.Empty;
			this.vsEdit_1.GridColorFixed = System.Drawing.Color.Empty;
			this.vsEdit_1.Location = new System.Drawing.Point(30, 90);
			this.vsEdit_1.Name = "vsEdit_1";
			this.vsEdit_1.ReadOnly = true;
			this.vsEdit_1.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsEdit_1.RowHeightMin = 0;
			this.vsEdit_1.Rows = 3;
			this.vsEdit_1.ScrollTipText = null;
			this.vsEdit_1.ShowColumnVisibilityMenu = false;
			this.vsEdit_1.ShowFocusCell = false;
			this.vsEdit_1.Size = new System.Drawing.Size(823, 267);
			this.vsEdit_1.StandardTab = true;
			this.vsEdit_1.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsEdit_1.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsEdit_1.TabIndex = 48;
			this.vsEdit_1.Visible = false;
			this.vsEdit_1.RowCollapsed += new Wisej.Web.DataGridViewRowEventHandler(this.vsEdit_AfterCollapse);
			// 
			// cmdPrint
			// 
			this.cmdPrint.AppearanceKey = "acceptButton";
			this.cmdPrint.ForeColor = System.Drawing.Color.White;
			this.cmdPrint.Location = new System.Drawing.Point(220, 30);
			this.cmdPrint.Name = "cmdPrint";
			this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPrint.Size = new System.Drawing.Size(174, 48);
			this.cmdPrint.TabIndex = 1;
			this.cmdPrint.Text = "Preview & Print";
			this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
			// 
			// cmdSummary
			// 
			this.cmdSummary.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdSummary.AppearanceKey = "toolbarButton";
			this.cmdSummary.Location = new System.Drawing.Point(780, 29);
			this.cmdSummary.Name = "cmdSummary";
			this.cmdSummary.Shortcut = Wisej.Web.Shortcut.F7;
			this.cmdSummary.Size = new System.Drawing.Size(90, 24);
			this.cmdSummary.TabIndex = 0;
			this.cmdSummary.Text = "Show Detail";
			this.cmdSummary.Click += new System.EventHandler(this.cmdSummary_Click);
			// 
			// frmCalculationSummary
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(898, 666);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCalculationSummary";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "TRIO Software Corporation  -  Utility Billing";
			this.Load += new System.EventHandler(this.frmCalculationSummary_Load);
			this.Activated += new System.EventHandler(this.frmCalculationSummary_Activated);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCalculationSummary_KeyPress);
			this.Resize += new System.EventHandler(this.frmCalculationSummary_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCalc)).EndInit();
			this.fraCalc.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsSumW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsSumS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraBillingEdit)).EndInit();
			this.fraBillingEdit.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsEdit_2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsEdit_0)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsEdit_1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSummary)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
