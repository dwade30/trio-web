﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptConsumptionHistoryComplete.
	/// </summary>
	public partial class rptConsumptionHistoryComplete : BaseSectionReport
	{
		public rptConsumptionHistoryComplete()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Consumption History";
		}

		public static rptConsumptionHistoryComplete InstancePtr
		{
			get
			{
				return (rptConsumptionHistoryComplete)Sys.GetInstance(typeof(rptConsumptionHistoryComplete));
			}
		}

		protected rptConsumptionHistoryComplete _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rs.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptConsumptionHistoryComplete	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		clsDRWrapper rs = new clsDRWrapper();
		int lngGroupConsumptionTotal;
		int lngFinalConsumptionTotal;
		// vbPorter upgrade warning: curGroupBillAmountTotal As Decimal	OnWrite(short, Decimal)
		Decimal curGroupBillAmountTotal;
		// vbPorter upgrade warning: curFinalBillAmountTotal As Decimal	OnWrite(short, Decimal)
		Decimal curFinalBillAmountTotal;
		int lngGroupCount;
		int lngFinalCount;
		int[] lngBooks = null;
		string strKeys;
		string strDateRanges;
		bool blnWater;
		bool blnHideDetail;

		public void Init(ref int[] PassBookArray, ref string strRateKeys, ref bool blnW, bool blnPrint, string strPassDateRanges = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				lngBooks = PassBookArray;
				strKeys = strRateKeys;
				strDateRanges = strPassDateRanges;
				blnWater = blnW;
				if (blnPrint)
				{
					this.PrintReport(true);
				}
				else
				{
					frmReportViewer.InstancePtr.Init(this);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rs.MoveNext();
				eArgs.EOF = rs.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Account #")
				{
					this.Fields["Binder"].Value = rs.Get_Fields_Int32("ActualAccountNumber");
				}
				else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Book")
				{
					// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
					this.Fields["Binder"].Value = rs.Get_Fields("Book");
				}
				else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Category")
				{
					if (blnWater)
					{
						this.Fields["Binder"].Value = rs.Get_Fields_Int32("WCat");
					}
					else
					{
						this.Fields["Binder"].Value = rs.Get_Fields_Int32("SCat");
					}
				}
				else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Frequency")
				{
					// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
					this.Fields["Binder"].Value = rs.Get_Fields("Frequency");
				}
				else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Meter Size")
				{
					this.Fields["Binder"].Value = rs.Get_Fields_Int32("Size");
				}
				else
				{
					GroupHeader1.Visible = false;
					GroupFooter1.Visible = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As Variant --> As string
			// vbPorter upgrade warning: Label3 As Variant --> As string
			// vbPorter upgrade warning: Label7 As Variant --> As string
			// vbPorter upgrade warning: lblBooksRateKeys As Variant --> As string
			// vbPorter upgrade warning: lblOptions As Variant --> As string
			//string Label2, Label3, Label7, lblBooksRateKeys = "", lblOptions = "";  // - "AutoDim"
			string strBooks = "";
			int counter;
			string strSQL = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			blnFirstRecord = true;
			lngGroupConsumptionTotal = 0;
			lngFinalConsumptionTotal = 0;
			curGroupBillAmountTotal = 0;
			curFinalBillAmountTotal = 0;
			lngGroupCount = 0;
			lngFinalCount = 0;
			// kk11032014 trout-1050  Need the period start and end date from the rate key
			if (Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(5, 1)) != "" || Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(5, 2)) != "")
			{
				strSQL = "SELECT RateKeys.Start,RateKeys.[End],ActualAccountNumber,Book,Bill.SCat,Bill.WCat,Frequency,Size,SPrinOwed,WPrinOwed,Consumption,Bill.BillDate,Sequence,BookNumber,BName " + "FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID INNER JOIN RateKeys ON RateKeys.ID = Bill.BillingRateKey " + "WHERE BillNumber IN (" + Strings.Left(strKeys, strKeys.Length - 1) + ") AND ";
			}
			else
			{
				strSQL = "SELECT ActualAccountNumber,Book,Bill.SCat,Bill.WCat,Frequency,Size,SPrinOwed,WPrinOwed,Consumption,BillDate,Sequence,BookNumber,BName " + "FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE BillNumber IN (" + Strings.Left(strKeys, strKeys.Length - 1) + ") AND ";
			}
			blnHideDetail = frmConsumptionHistoryReport.InstancePtr.chkHideDetail.CheckState == Wisej.Web.CheckState.Checked;
			if (blnWater == true)
			{
				strSQL += "(Bill.Service = 'W' OR Bill.Service = 'B') AND ";
			}
			else
			{
				strSQL += "(Bill.Service = 'S' OR Bill.Service = 'B') AND ";
			}
			strBooks = "";
			strSQL += "(";
			for (counter = 1; counter <= Information.UBound(lngBooks, 1); counter++)
			{
				strBooks += FCConvert.ToString(lngBooks[counter]) + ", ";
				strSQL += "Book = " + FCConvert.ToString(lngBooks[counter]) + " OR ";
			}
			strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ") AND ";
			if (strBooks != "")
			{
				strBooks = Strings.Left(strBooks, strBooks.Length - 2);
			}
			if (!(strDateRanges == ""))
			{
				// trouts-227 5.5.2017 Date Range selection option
				lblBooksRateKeys.Text = strDateRanges + "   Book(s): " + strBooks;
			}
			else
			{
				lblBooksRateKeys.Text = "RateKey(s): " + Strings.Left(strKeys, strKeys.Length - 1) + "   Book(s): " + strBooks;
			}
			strBooks = "";
			if (frmConsumptionHistoryReport.InstancePtr.cmbCategorySelected.Text == "Selected")
			{
				strSQL += "(";
				for (counter = 1; counter <= frmConsumptionHistoryReport.InstancePtr.vsCategory.Rows - 1; counter++)
				{
					if (FCConvert.CBool(frmConsumptionHistoryReport.InstancePtr.vsCategory.TextMatrix(counter, 0)) == true)
					{
						strBooks += frmConsumptionHistoryReport.InstancePtr.vsCategory.TextMatrix(counter, 1) + ", ";
						if (blnWater == true)
						{
							strSQL += "Bill.WCat = " + FCConvert.ToString(Conversion.Val(frmConsumptionHistoryReport.InstancePtr.vsCategory.RowData(counter))) + " OR ";
						}
						else
						{
							strSQL += "Bill.SCat = " + FCConvert.ToString(Conversion.Val(frmConsumptionHistoryReport.InstancePtr.vsCategory.RowData(counter))) + " OR ";
						}
					}
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ") AND ";
				if (strBooks != "")
				{
					strBooks = Strings.Left(strBooks, strBooks.Length - 2);
				}
				lblOptions.Text = "Category: " + strBooks;
			}
			else
			{
				lblOptions.Text = "Category: All";
			}
			strBooks = "";
			if (frmConsumptionHistoryReport.InstancePtr.cmbFrequencyAll.Text == "Selected")
			{
				strSQL += "(";
				for (counter = 1; counter <= frmConsumptionHistoryReport.InstancePtr.vsFrequency.Rows - 1; counter++)
				{
					if (FCConvert.CBool(frmConsumptionHistoryReport.InstancePtr.vsFrequency.TextMatrix(counter, 0)) == true)
					{
						strBooks += frmConsumptionHistoryReport.InstancePtr.vsFrequency.TextMatrix(counter, 1) + ", ";
						strSQL += "Frequency = " + FCConvert.ToString(Conversion.Val(frmConsumptionHistoryReport.InstancePtr.vsFrequency.RowData(counter))) + " OR ";
					}
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ") AND ";
				if (strBooks != "")
				{
					strBooks = Strings.Left(strBooks, strBooks.Length - 2);
				}
				lblOptions.Text += "   Frequency: " + strBooks;
			}
			else
			{
				lblOptions.Text += "   Frequency: All";
			}
			strBooks = "";
			if (frmConsumptionHistoryReport.InstancePtr.cmbMeterSizeAll.Text == "Selected")
			{
				strSQL += "(";
				for (counter = 1; counter <= frmConsumptionHistoryReport.InstancePtr.vsMeterSize.Rows - 1; counter++)
				{
					if (FCConvert.CBool(frmConsumptionHistoryReport.InstancePtr.vsMeterSize.TextMatrix(counter, 0)) == true)
					{
						strBooks += frmConsumptionHistoryReport.InstancePtr.vsMeterSize.TextMatrix(counter, 1) + ", ";
						strSQL += "Size = " + FCConvert.ToString(Conversion.Val(frmConsumptionHistoryReport.InstancePtr.vsMeterSize.RowData(counter))) + " OR ";
					}
				}
				strSQL = Strings.Left(strSQL, strSQL.Length - 4) + ") AND ";
				if (strBooks != "")
				{
					strBooks = Strings.Left(strBooks, strBooks.Length - 2);
				}
				lblOptions.Text += "   Meter Size: " + strBooks;
			}
			else
			{
				lblOptions.Text += "   Meter Size: All";
			}
			for (counter = 1; counter <= 5; counter++)
			{
				// kk11032014 trout-1050
				strBooks = "";
				if (Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) != "" && Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) != "")
				{
					strBooks = Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 0)) + ": " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + " To " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2));
					if (counter == 1)
					{
						strSQL += "(BName > '" + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + "     ' AND BName < '" + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + "zzzzz') AND ";
					}
					else if (counter == 2)
					{
						strSQL += "(ActualAccountNumber >= " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + " AND ActualAccountNumber <= " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + ") AND ";
					}
					else if (counter == 3)
					{
						strSQL += "(Consumption >= " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + " AND Consumption <= " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + ") AND ";
					}
					else if (counter == 4)
					{
						if (blnWater == true)
						{
							strSQL += "(WPrinOwed >= " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + " AND WPrinOwed <= " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + ") AND ";
						}
						else
						{
							strSQL += "(SPrinOwed >= " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + " AND SPrinOwed <= " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + ") AND ";
						}
					}
					else if (counter == 5)
					{
						// kk11042014 trout-1050
						strSQL += "(Start >= '" + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + "' AND [End] <= '" + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + "') AND ";
					}
				}
				else if (Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) != "")
				{
					strBooks = Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 0)) + ": " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1));
					if (counter == 1)
					{
						strSQL += "(BName Like '" + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + "%') AND ";
					}
					else if (counter == 2)
					{
						strSQL += "(ActualAccountNumber = " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + ") AND ";
					}
					else if (counter == 3)
					{
						strSQL += "(Consumption = " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + ") AND ";
					}
					else if (counter == 4)
					{
						if (blnWater == true)
						{
							strSQL += "(WPrinOwed = " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + ") AND ";
						}
						else
						{
							strSQL += "(SPrinOwed = " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + ") AND ";
						}
					}
					else if (counter == 5)
					{
						// kk11042014 trout-1050
						strSQL += "(Start >= '" + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 1)) + "') AND ";
					}
				}
				else if (Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) != "")
				{
					strBooks = Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 0)) + ": " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2));
					if (counter == 1)
					{
						strSQL += "(BName Like '" + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + "%') AND ";
					}
					else if (counter == 2)
					{
						strSQL += "(ActualAccountNumber = " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + ") AND ";
					}
					else if (counter == 3)
					{
						strSQL += "(Consumption = " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + ") AND ";
					}
					else if (counter == 4)
					{
						if (blnWater == true)
						{
							strSQL += "(WPrinOwed = " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + ") AND ";
						}
						else
						{
							strSQL += "(SPrinOwed = " + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + ") AND ";
						}
					}
					else if (counter == 5)
					{
						// kk11042014 trout-1050
						strSQL += "([End] <= '" + Strings.Trim(frmConsumptionHistoryReport.InstancePtr.vsGrid.TextMatrix(counter, 2)) + "') AND ";
					}
				}
				if (strBooks != "")
				{
					lblOptions.Text += "   " + strBooks;
				}
			}
			strSQL = Strings.Left(strSQL, strSQL.Length - 5);
			strBooks = "";
			if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Account #")
			{
				strBooks = "ORDER BY ActualAccountNumber, ";
			}
			else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Book")
			{
				strBooks = "ORDER BY Book, ";
			}
			else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Category")
			{
				if (blnWater)
				{
					strBooks = "ORDER BY Bill.WCat, ";
				}
				else
				{
					strBooks = "ORDER BY Bill.SCat, ";
				}
			}
			else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Frequency")
			{
				strBooks = "ORDER BY Frequency, ";
			}
			else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Meter Size")
			{
				strBooks = "ORDER BY Size, ";
			}
			else
			{
				strBooks = "ORDER BY ";
			}
			if (frmConsumptionHistoryReport.InstancePtr.cmbSortByConsumption.Text == "Account #")
			{
				strBooks += "ActualAccountNumber";
			}
			else if (frmConsumptionHistoryReport.InstancePtr.cmbSortByConsumption.Text == "Bill Amount")
			{
				if (blnWater)
				{
					strBooks += "WPrinOwed";
				}
				else
				{
					strBooks += "SPrinOwed";
				}
			}
			else if (frmConsumptionHistoryReport.InstancePtr.cmbSortByConsumption.Text == "Consumption")
			{
				strBooks += "Consumption";
			}
			else if (frmConsumptionHistoryReport.InstancePtr.cmbSortByConsumption.Text == "Name")
			{
				strBooks += "BName";
			}
			else
			{
				strBooks += "Sequence";
			}
			strSQL += " " + strBooks;
			rs.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				if (blnHideDetail)
				{
					lblCountHeading.Visible = true;
					Line2.Visible = false;
					Line3.X2 = 11520 / 1440f;
					fldFinalCount.Left = lblCountHeading.Left;
					fldCount.Left = lblCountHeading.Left;
					lblFinalCountLabel.Visible = false;
					lblGroupCountLabel.Visible = false;
					Detail.Visible = false;
					GroupHeader1.Visible = false;
					lblTitle2.Visible = true;
					lblGroupTotalsLabel.Visible = false;
					for (counter = 0; counter <= GroupFooter1.Controls.Count - 1; counter++)
					{
						if (GroupFooter1.Controls[counter].Visible)
						{
							GroupFooter1.Controls[counter].Top = 0;
                            if (GroupFooter1.Controls[counter] is GrapeCity.ActiveReports.SectionReportModel.TextBox)
                            {
                                (GroupFooter1.Controls[counter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font = new Font((GroupFooter1.Controls[counter] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Font, FontStyle.Bold);
                            }
                            else if (GroupFooter1.Controls[counter] is GrapeCity.ActiveReports.SectionReportModel.Label)
                            {
                                (GroupFooter1.Controls[counter] as GrapeCity.ActiveReports.SectionReportModel.Label).Font = new Font((GroupFooter1.Controls[counter] as GrapeCity.ActiveReports.SectionReportModel.Label).Font, FontStyle.Bold);
                            }
                        }
					}
				}
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Cancel();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldBillAmount As Variant --> As string
			// vbPorter upgrade warning: fldConsumption As Variant --> As string
			// vbPorter upgrade warning: fldBillDate As Variant --> As string
			//object fldAccount; string fldBillAmount = "", fldConsumption, fldBillDate; object fldSequence, fldBook, fldName;    // - "AutoDim"
			fldAccount.Text = FCConvert.ToString(rs.Get_Fields_Int32("ActualAccountNumber"));
			if (blnWater == true)
			{
				fldBillAmount.Text = Strings.Format(rs.Get_Fields_Double("WPrinOwed"), "#,##0.00");
				curGroupBillAmountTotal += FCConvert.ToDecimal(rs.Get_Fields_Double("WPrinOwed"));
				curFinalBillAmountTotal += FCConvert.ToDecimal(rs.Get_Fields_Double("WPrinOwed"));
			}
			else
			{
				fldBillAmount.Text = Strings.Format(rs.Get_Fields_Double("SPrinOwed"), "#,##0.00");
				curGroupBillAmountTotal += FCConvert.ToDecimal(rs.Get_Fields_Double("SPrinOwed"));
				curFinalBillAmountTotal += FCConvert.ToDecimal(rs.Get_Fields_Double("SPrinOwed"));
			}
			fldConsumption.Text = Strings.Format(rs.Get_Fields_Int32("Consumption"), "#,##0");
			lngGroupConsumptionTotal += rs.Get_Fields_Int32("Consumption");
			lngFinalConsumptionTotal += rs.Get_Fields_Int32("Consumption");
			fldBillDate.Text = Strings.Format(rs.Get_Fields_DateTime("BillDate"), "MM/dd/yy");
			// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
			fldSequence.Text = FCConvert.ToString(rs.Get_Fields("Sequence"));
			fldBook.Text = FCConvert.ToString(rs.Get_Fields_Int32("BookNumber"));
			fldName.Text = FCConvert.ToString(rs.Get_Fields_String("BName"));
			lngGroupCount += 1;
			lngFinalCount += 1;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldCount As Variant --> As string
			// vbPorter upgrade warning: fldGroupBillAmountTotal As Variant --> As string
			// vbPorter upgrade warning: fldGroupConsumptionTotal As Variant --> As string
			//object lblTitle2; string fldCount, fldGroupBillAmountTotal, fldGroupConsumptionTotal;   // - "AutoDim"
			if (blnHideDetail)
			{
				lblTitle2.Text = lblTitle.Text;
			}
			fldCount.Text = Strings.Format(lngGroupCount, "#,##0");
			fldGroupBillAmountTotal.Text = Strings.Format(curGroupBillAmountTotal, "#,##0.00");
			fldGroupConsumptionTotal.Text = Strings.Format(lngGroupConsumptionTotal, "#,##0");
			lngGroupCount = 0;
			lngGroupConsumptionTotal = 0;
			curGroupBillAmountTotal = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: fldFinalCount As Variant --> As string
			// vbPorter upgrade warning: fldFinalBillAmountTotal As Variant --> As string
			// vbPorter upgrade warning: fldFinalConsumptionTotal As Variant --> As string
			// string fldFinalCount, fldFinalBillAmountTotal, fldFinalConsumptionTotal;    // - "AutoDim"
			fldFinalCount.Text = Strings.Format(lngFinalCount, "#,##0");
			fldFinalBillAmountTotal.Text = Strings.Format(curFinalBillAmountTotal, "#,##0.00");
			fldFinalConsumptionTotal.Text = Strings.Format(lngFinalConsumptionTotal, "#,##0");
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lblTitle As Variant --> As string
			//string lblTitle = "";   // - "AutoDim"
            using (clsDRWrapper rsDesc = new clsDRWrapper())
            {
                if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Account #")
                {
                    lblTitle.Text = "Account " + rs.Get_Fields_Int32("ActualAccountNumber") + "  " +
                                    rs.Get_Fields_String("BName");
                }
                else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Book")
                {
                    // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                    rsDesc.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + rs.Get_Fields("Book"),
                        modExtraModules.strUTDatabase);
                    if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                        lblTitle.Text = "Book " + rs.Get_Fields("Book") + "  " +
                                        rsDesc.Get_Fields_String("Description");
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                        lblTitle.Text = "Book " + rs.Get_Fields("Book") + "  UNKNOWN";
                    }
                }
                else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Category")
                {
                    if (blnWater)
                    {
                        rsDesc.OpenRecordset("SELECT * FROM Category WHERE Code = " + rs.Get_Fields_Int32("WCat"),
                            modExtraModules.strUTDatabase);
                        if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
                        {
                            lblTitle.Text = "Category " + rs.Get_Fields_Int32("WCat") + "  " +
                                            rsDesc.Get_Fields_String("LongDescription");
                        }
                        else
                        {
                            lblTitle.Text = "Category " + rs.Get_Fields_Int32("WCat") + "  UNKNOWN";
                        }
                    }
                    else
                    {
                        rsDesc.OpenRecordset("SELECT * FROM Category WHERE Code = " + rs.Get_Fields_Int32("SCat"),
                            modExtraModules.strUTDatabase);
                        if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
                        {
                            lblTitle.Text = "Category " + rs.Get_Fields_Int32("SCat") + "  " +
                                            rsDesc.Get_Fields_String("LongDescription");
                        }
                        else
                        {
                            lblTitle.Text = "Category " + rs.Get_Fields_Int32("SCat") + "  UNKNOWN";
                        }
                    }
                }
                else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Frequency")
                {
                    // TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                    rsDesc.OpenRecordset("SELECT * FROM Frequency WHERE Code = " + rs.Get_Fields("Frequency"),
                        modExtraModules.strUTDatabase);
                    if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
                    {
                        // TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                        lblTitle.Text = "Frequency " + rs.Get_Fields("Frequency") + "  " +
                                        rsDesc.Get_Fields_String("LongDescription");
                    }
                    else
                    {
                        // TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                        lblTitle.Text = "Frequency " + rs.Get_Fields("Frequency") + "  UNKNOWN";
                    }
                }
                else if (frmConsumptionHistoryReport.InstancePtr.cmbGroupByCategory.Text == "Meter Size")
                {
                    rsDesc.OpenRecordset("SELECT * FROM MeterSizes WHERE Code = " + rs.Get_Fields_Int32("Size"),
                        modExtraModules.strUTDatabase);
                    if (rsDesc.EndOfFile() != true && rsDesc.BeginningOfFile() != true)
                    {
                        lblTitle.Text = "Meter Size " + rs.Get_Fields_Int32("Size") + "  " +
                                        rsDesc.Get_Fields_String("LongDescription");
                    }
                    else
                    {
                        lblTitle.Text = "Meter Size " + rs.Get_Fields_Int32("Size") + "  UNKNOWN";
                    }
                }
                else
                {
                    lblTitle.Text = "";
                }
            }
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As Variant --> As string
			//string Label4;  // - "AutoDim"
			Label4.Text = "Page " + this.PageNumber;
		}

		private void rptConsumptionHistoryComplete_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptConsumptionHistoryComplete properties;
			//rptConsumptionHistoryComplete.Caption	= "Consumption History";
			//rptConsumptionHistoryComplete.Icon	= "rptConsumptionHistoryComplete.dsx":0000";
			//rptConsumptionHistoryComplete.Left	= 0;
			//rptConsumptionHistoryComplete.Top	= 0;
			//rptConsumptionHistoryComplete.Width	= 15240;
			//rptConsumptionHistoryComplete.Height	= 11115;
			//rptConsumptionHistoryComplete.StartUpPosition	= 3;
			//rptConsumptionHistoryComplete.SectionData	= "rptConsumptionHistoryComplete.dsx":508A;
			//End Unmaped Properties
		}

		private void rptConsumptionHistoryComplete_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
