﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmEditRateRecordTitles.
	/// </summary>
	public partial class frmEditRateRecordTitles : BaseForm
	{
		public frmEditRateRecordTitles()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmEditRateRecordTitles InstancePtr
		{
			get
			{
				return (frmEditRateRecordTitles)Sys.GetInstance(typeof(frmEditRateRecordTitles));
			}
		}

		protected frmEditRateRecordTitles _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/01/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/06/2006              *
		// ********************************************************
		public void Init()
		{
			Format_Grid();
			Fill_Grid();
			this.Show(FCForm.FormShowEnum.Modal);
		}

		private void frmEditRateRecordTitles_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmEditRateRecordTitles_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmEditRateRecordTitles_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmEditRateRecordTitles properties;
			//frmEditRateRecordTitles.FillStyle	= 0;
			//frmEditRateRecordTitles.ScaleWidth	= 5880;
			//frmEditRateRecordTitles.ScaleHeight	= 4215;
			//frmEditRateRecordTitles.LinkTopic	= "Form2";
			//frmEditRateRecordTitles.LockControls	= true;
			//frmEditRateRecordTitles.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
            //FC:FINAL:BSE:#3768 make moving with tab through grid possible 
            vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
        }

		private void frmEditRateRecordTitles_Resize(object sender, System.EventArgs e)
		{
			Format_Grid();
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			SaveRecords();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			SaveRecords();
			this.Unload();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			int lngCT;
			bool boolDirty = false;
			vsRate.Select(0, 0);
			for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
			{
				if (Conversion.Val(vsRate.TextMatrix(lngCT, 1)) != 0)
				{
					boolDirty = true;
					break;
				}
			}
			if (boolDirty)
			{
				switch (MessageBox.Show("Would you like to save your changes?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
				{
					case DialogResult.Yes:
						{
							SaveRecords();
							this.Unload();
							break;
						}
					case DialogResult.No:
						{
							this.Unload();
							break;
						}
					case DialogResult.Cancel:
						{
							// do nothing
							break;
						}
				}
				//end switch
			}
			else
			{
				this.Unload();
			}
		}

		private void Format_Grid()
		{
			vsRate.TextMatrix(0, 0, "Key");
			vsRate.TextMatrix(0, 1, "Dirty");
			vsRate.TextMatrix(0, 2, "Description");
			vsRate.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// .Cell(FCGrid.CellPropertySettings.flexcpFontBold, 0, 0, 0, .Cols - 1) = True
			vsRate.ColWidth(0, FCConvert.ToInt32(vsRate.WidthOriginal * 0.1));
			vsRate.ColWidth(1, 0);
			vsRate.ColWidth(2, FCConvert.ToInt32(vsRate.WidthOriginal * 0.78));
			vsRate.EditMaxLength = 50;
		}

		private void Fill_Grid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRate = new clsDRWrapper();
				// reset the grid
				vsRate.Rows = 1;
				rsRate.OpenRecordset("SELECT * FROM RateTable ORDER BY RateTableNumber asc");
				while (!rsRate.EndOfFile())
				{
					vsRate.AddItem("");
					vsRate.TextMatrix(vsRate.Rows - 1, 0, FCConvert.ToString(rsRate.Get_Fields_Int32("RateTableNumber")));
					vsRate.TextMatrix(vsRate.Rows - 1, 1, FCConvert.ToString(0));
					vsRate.TextMatrix(vsRate.Rows - 1, 2, FCConvert.ToString(rsRate.Get_Fields_String("RateTableDescription")));
					rsRate.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Fill Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveRecords()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngCT;
				clsDRWrapper rsRate = new clsDRWrapper();
				rsRate.OpenRecordset("SELECT * FROM RateTable ORDER BY RateTableNumber asc");
				vsRate.Select(0, 0);
				for (lngCT = 1; lngCT <= vsRate.Rows - 1; lngCT++)
				{
					if (Conversion.Val(vsRate.TextMatrix(lngCT, 1)) != 0)
					{
						// this is a dirty record
						rsRate.FindFirstRecord("RateTableNumber", Conversion.Val(vsRate.TextMatrix(lngCT, 0)));
						if (!rsRate.NoMatch)
						{
							rsRate.Edit();
							rsRate.Set_Fields("RateTableDescription", Strings.Trim(vsRate.TextMatrix(lngCT, 2)));
							rsRate.Update();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Rate Tables", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsRate_RowColChange(object sender, System.EventArgs e)
		{
            vsRate.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			if (vsRate.Row > 0)
			{
				switch (vsRate.Col)
				{
					case 0:
					case 1:
						{
							vsRate.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
					case 2:
						{
							vsRate.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsRate.EditCell();
							break;
						}
				}
				//end switch
			}
		}

		private void vsRate_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			vsRate.TextMatrix(vsRate.GetFlexRowIndex(e.RowIndex), 1, FCConvert.ToString(-1));
		}
	}
}
