﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTStatusListAccountDetail.
	/// </summary>
	public partial class rptUTStatusListAccountDetail : BaseSectionReport
	{
		public rptUTStatusListAccountDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptUTStatusListAccountDetail InstancePtr
		{
			get
			{
				return (rptUTStatusListAccountDetail)Sys.GetInstance(typeof(rptUTStatusListAccountDetail));
			}
		}

		protected rptUTStatusListAccountDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUTStatusListAccountDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/02/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/02/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngRow;
		int lngPDNumber;
		int lngPDTop;
		double dblPDTotal;
		bool boolPerDiem;
		string strLastYearText = "";
		public DateTime dtAsOfDate;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			string strDateString;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			strDateString = frmUTStatusList.InstancePtr.vsWhere.TextMatrix(5, 1);
			if (strDateString == "")
			{
				dtAsOfDate = DateTime.Today;
			}
			else
			{
				dtAsOfDate = DateAndTime.DateValue(strDateString);
			}
			boolPerDiem = false;
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			strSQL = GetSQL();
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (rsData.EndOfFile())
			{
				frmWait.InstancePtr.Unload();
				MessageBox.Show("There are no accounts matching this criteria.", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsData.EndOfFile())
			{
				BindFields();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// Account
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				lblAccount.Text = "Account: " + rsData.Get_Fields("Account");
				// Billing Year
				lblYear.Text = "Year: " + modExtraModules.FormatYear(FCConvert.ToString(rsData.Get_Fields_Int32("BillingYear")));
				// Name
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) != "")
				{
					fldName.Text = Strings.Trim(Strings.Trim(rsData.Get_Fields_String("Name1") + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2")))));
				}
				else
				{
					fldName.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")));
				}
				// Address
				fldAddress.Text = Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1"))) + " " + Strings.Trim(Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")))));
				// Location
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = Strings.Trim(rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
				}
				else
				{
					fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
				}
				// Map Lot
				fldMapLot.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("MapLot")));
				// Ref 1 & 2
				fldRef1.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref1")));
				fldRef2.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Ref2")));
				// Land, Building and Exempt value
				if (modMain.Statics.boolRE)
				{
					fldLand.Text = Strings.Format(rsData.Get_Fields_Int32("LandValue"), "#,##0");
					fldBuilding.Text = Strings.Format(rsData.Get_Fields_Int32("BuildingValue"), "#,##0");
					fldExempt.Text = Strings.Format(rsData.Get_Fields_Int32("ExemptValue"), "#,##0");
				}
				else
				{
					fldLand.Text = "";
					fldBuilding.Text = "";
					fldExempt.Text = "";
					lblLand.Text = "";
					lblBuilding.Text = "";
					lblExempt.Text = "";
				}
				// Tag for the sub report = BillKey
				srptSLAllActivityDetailOB.Report = new srptUTSLAllActivityDetail();
				srptSLAllActivityDetailOB.Report.UserData = rsData.Get_Fields_Int32("BillKey");
				rsData.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Bind Fields (Main)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private string GetSQL()
		{
			string GetSQL = "";
			string strTemp = "";
			if (Strings.Trim(frmUTStatusList.InstancePtr.strRSWhere) != "")
			{
				strTemp = "SELECT * FROM ((" + frmUTStatusList.InstancePtr.strRSFrom + ") AS Bill INNER JOIN BillingMaster ON Bill.BK = BillingMaster.BillKey) INNER JOIN Master ON BillingMaster.Account = Master.RSAccount WHERE " + frmUTStatusList.InstancePtr.strRSWhere + " ORDER BY " + frmUTStatusList.InstancePtr.strRSOrder;
			}
			else
			{
				strTemp = "SELECT * FROM ((" + frmUTStatusList.InstancePtr.strRSFrom + ") AS Bill INNER JOIN BillingMaster ON Bill.BK = BillingMaster.BillKey) INNER JOIN Master ON BillingMaster.Account = Master.RSAccount ORDER BY " + frmUTStatusList.InstancePtr.strRSOrder;
			}
			GetSQL = strTemp;
			return GetSQL;
		}

		private void rptUTStatusListAccountDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUTStatusListAccountDetail properties;
			//rptUTStatusListAccountDetail.Caption	= "Account Detail";
			//rptUTStatusListAccountDetail.Icon	= "rptUTStatusListAccountDetail.dsx":0000";
			//rptUTStatusListAccountDetail.Left	= 0;
			//rptUTStatusListAccountDetail.Top	= 0;
			//rptUTStatusListAccountDetail.Width	= 11880;
			//rptUTStatusListAccountDetail.Height	= 8595;
			//rptUTStatusListAccountDetail.StartUpPosition	= 3;
			//rptUTStatusListAccountDetail.SectionData	= "rptUTStatusListAccountDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
