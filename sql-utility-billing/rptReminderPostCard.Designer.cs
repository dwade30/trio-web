﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptReminderPostCard.
	/// </summary>
	partial class rptReminderPostCard
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReminderPostCard));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblReturnAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReturnAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReturnAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReturnAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMailingAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMailingAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMailingAddress5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBulkMailer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblMailingAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBulkMailer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblReturnAddress1,
				this.lblReturnAddress2,
				this.lblReturnAddress3,
				this.lblReturnAddress4,
				this.lblName,
				this.lblMailingAddress1,
				this.lblMailingAddress2,
				this.lblMailingAddress3,
				this.lblAccount,
				this.lblMailingAddress5,
				this.lblMessage,
				this.fldBulkMailer,
				this.lblMailingAddress4
			});
			this.Detail.Height = 4F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblReturnAddress1
			// 
			this.lblReturnAddress1.Height = 0.1770833F;
			this.lblReturnAddress1.HyperLink = null;
			this.lblReturnAddress1.Left = 0F;
			this.lblReturnAddress1.Name = "lblReturnAddress1";
			this.lblReturnAddress1.Style = "font-family: \'Tahoma\'";
			this.lblReturnAddress1.Text = " ";
			this.lblReturnAddress1.Top = 0F;
			this.lblReturnAddress1.Width = 3.6F;
			// 
			// lblReturnAddress2
			// 
			this.lblReturnAddress2.Height = 0.1770833F;
			this.lblReturnAddress2.HyperLink = null;
			this.lblReturnAddress2.Left = 0F;
			this.lblReturnAddress2.Name = "lblReturnAddress2";
			this.lblReturnAddress2.Style = "font-family: \'Tahoma\'";
			this.lblReturnAddress2.Text = " ";
			this.lblReturnAddress2.Top = 0.1770833F;
			this.lblReturnAddress2.Width = 3.6F;
			// 
			// lblReturnAddress3
			// 
			this.lblReturnAddress3.Height = 0.1770833F;
			this.lblReturnAddress3.HyperLink = null;
			this.lblReturnAddress3.Left = 0F;
			this.lblReturnAddress3.Name = "lblReturnAddress3";
			this.lblReturnAddress3.Style = "font-family: \'Tahoma\'";
			this.lblReturnAddress3.Text = " ";
			this.lblReturnAddress3.Top = 0.3541667F;
			this.lblReturnAddress3.Width = 3.6F;
			// 
			// lblReturnAddress4
			// 
			this.lblReturnAddress4.Height = 0.1770833F;
			this.lblReturnAddress4.HyperLink = null;
			this.lblReturnAddress4.Left = 0F;
			this.lblReturnAddress4.Name = "lblReturnAddress4";
			this.lblReturnAddress4.Style = "font-family: \'Tahoma\'";
			this.lblReturnAddress4.Text = " ";
			this.lblReturnAddress4.Top = 0.53125F;
			this.lblReturnAddress4.Width = 3.6F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1770833F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 1F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'";
			this.lblName.Text = " ";
			this.lblName.Top = 2.177083F;
			this.lblName.Width = 3.6F;
			// 
			// lblMailingAddress1
			// 
			this.lblMailingAddress1.Height = 0.1770833F;
			this.lblMailingAddress1.HyperLink = null;
			this.lblMailingAddress1.Left = 1F;
			this.lblMailingAddress1.Name = "lblMailingAddress1";
			this.lblMailingAddress1.Style = "font-family: \'Tahoma\'";
			this.lblMailingAddress1.Text = " ";
			this.lblMailingAddress1.Top = 2.354167F;
			this.lblMailingAddress1.Width = 3.6F;
			// 
			// lblMailingAddress2
			// 
			this.lblMailingAddress2.Height = 0.1770833F;
			this.lblMailingAddress2.HyperLink = null;
			this.lblMailingAddress2.Left = 1F;
			this.lblMailingAddress2.Name = "lblMailingAddress2";
			this.lblMailingAddress2.Style = "font-family: \'Tahoma\'";
			this.lblMailingAddress2.Text = " ";
			this.lblMailingAddress2.Top = 2.53125F;
			this.lblMailingAddress2.Width = 3.6F;
			// 
			// lblMailingAddress3
			// 
			this.lblMailingAddress3.Height = 0.1770833F;
			this.lblMailingAddress3.HyperLink = null;
			this.lblMailingAddress3.Left = 1F;
			this.lblMailingAddress3.Name = "lblMailingAddress3";
			this.lblMailingAddress3.Style = "font-family: \'Tahoma\'";
			this.lblMailingAddress3.Text = " ";
			this.lblMailingAddress3.Top = 2.708333F;
			this.lblMailingAddress3.Width = 3.6F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1770833F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 1F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'";
			this.lblAccount.Text = " ";
			this.lblAccount.Top = 2F;
			this.lblAccount.Width = 3.6F;
			// 
			// lblMailingAddress5
			// 
			this.lblMailingAddress5.Height = 0.15F;
			this.lblMailingAddress5.HyperLink = null;
			this.lblMailingAddress5.Left = 1F;
			this.lblMailingAddress5.Name = "lblMailingAddress5";
			this.lblMailingAddress5.Style = "font-family: \'Tahoma\'";
			this.lblMailingAddress5.Text = " ";
			this.lblMailingAddress5.Top = 3.85F;
			this.lblMailingAddress5.Width = 3.6F;
			// 
			// lblMessage
			// 
			this.lblMessage.Height = 1F;
			this.lblMessage.HyperLink = null;
			this.lblMessage.Left = 0.5F;
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Style = "font-family: \'Tahoma\'";
			this.lblMessage.Text = " ";
			this.lblMessage.Top = 1F;
			this.lblMessage.Width = 5.5F;
			// 
			// fldBulkMailer
			// 
			this.fldBulkMailer.Height = 1F;
			this.fldBulkMailer.Left = 4F;
			this.fldBulkMailer.Name = "fldBulkMailer";
			this.fldBulkMailer.Style = "font-family: \'Tahoma\'";
			this.fldBulkMailer.Text = null;
			this.fldBulkMailer.Top = 0F;
			this.fldBulkMailer.Width = 2.8125F;
			// 
			// lblMailingAddress4
			// 
			this.lblMailingAddress4.Height = 0.1770833F;
			this.lblMailingAddress4.HyperLink = null;
			this.lblMailingAddress4.Left = 1F;
			this.lblMailingAddress4.Name = "lblMailingAddress4";
			this.lblMailingAddress4.Style = "font-family: \'Tahoma\'";
			this.lblMailingAddress4.Text = " ";
			this.lblMailingAddress4.Top = 2.875F;
			this.lblMailingAddress4.Width = 3.6F;
			// 
			// rptReminderPostCard
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.8125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += ActiveReport_Terminate;
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReturnAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBulkMailer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReturnAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBulkMailer;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
