﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWUT0000
{
	public class modUTUseCR
	{
		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}

		public class StaticVariables
		{
			
			public double gdblFlatInterestAmount;
			public bool gboolShowLastCLAccountInCR;
           
        }

		public static void FillUTInfo()
		{
			// this is a dummy function for this version of the form instead CR
		}

		public static void DeleteUTPaymentFromShoppingList(ref int lngID)
		{
			// this is a dummy function for this version of the form instead CR
		}
		// vbPorter upgrade warning: 'Return' As double	OnWrite(string)
		public static double Rounding(ref float nValue, ref short nPositiveNumberOfDigits)
		{
			double Rounding = 0;
			string strFormat;
			strFormat = "#,###." + Strings.StrDup(nPositiveNumberOfDigits, "0");
			Rounding = FCConvert.ToDouble(Strings.Format(FCConvert.ToDecimal(nValue), strFormat));
			return Rounding;
		}

        public static void CreateUTBatchReceiptEntry(bool blnPrint, bool boolPostingAutoPayBatch = false)
        {

        }

        public static bool ValidateTeller(string strTellerID)
        {
            return false;
        }
	}
}
