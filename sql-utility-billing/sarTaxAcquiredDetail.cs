﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for sarTaxAcquiredDetail.
	/// </summary>
	public partial class sarTaxAcquiredDetail : FCSectionReport
	{
		public sarTaxAcquiredDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "TA Detail";
		}

		public static sarTaxAcquiredDetail InstancePtr
		{
			get
			{
				return (sarTaxAcquiredDetail)Sys.GetInstance(typeof(sarTaxAcquiredDetail));
			}
		}

		protected sarTaxAcquiredDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsLN.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarTaxAcquiredDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/12/2007              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/12/2007              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		int lngAccount;
		double dblTotal;
		int intTotalYears;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			rsData.OpenRecordset("SELECT Lien.IntPaid AS IntPd, Lien.IntAdded AS IntCHG, Lien.RateKey AS RK, Lien.IntPaidDate AS ApplThrDT, Lien.PrinPaid AS PP, * FROM Bill LEFT JOIN Lien ON Bill.SLienRecordNumber = Lien.ID WHERE ActualAccountNumber = " + FCConvert.ToString(lngAccount) + " ORDER BY BillNumber desc", modExtraModules.strUTDatabase);
			if (rsData.EndOfFile())
			{
				HideFields();
				lblLastBillingAmountTotal.Text = "0.00";
			}
			else
			{
				lblLastBillingAmountTotal.Text = Strings.Format(rsData.Get_Fields_Double("WPrinOwed") + rsData.Get_Fields_Double("SPrinOwed") + rsData.Get_Fields_Double("STaxOwed") + rsData.Get_Fields_Double("WTaxOwed"), "#,##0.00");
				rsLN.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
				if (FindOutstaingTaxes())
				{
					rsData.MoveFirst();
				}
				else
				{
					HideFields();
				}
			}
		}

		private void HideFields()
		{
			fldPrevOwner.Visible = false;
			fldTaxes.Visible = false;
			lblOutStandingTax.Visible = false;
			lblPreviousOwners.Visible = false;
			lblTaxes.Visible = false;
			Line1.Visible = false;
			fldTotalTaxes.Visible = false;
			Detail.Height = 0;
			GroupHeader1.Height = 300 / 1440f;
			GroupFooter1.Height = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
			if (!rsData.EndOfFile())
			{
				// rsData.MoveNext
			}
		}

		private void BindFields()
		{
			string strOwner = "";
			double dblBill = 0;
			double dblXtraInt = 0;
			TRYAGAIN:
			;
			if (!rsData.EndOfFile())
			{
				if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SLienRecordNumber")) == 0)
				{
					// calculate the bill total
					dblBill = modUTCalculations.CalculateAccountUT(rsData, DateTime.Today, ref dblXtraInt, true) + modUTCalculations.CalculateAccountUT(rsData, DateTime.Today, ref dblXtraInt, false);
				}
				else
				{
					rsLN.FindFirstRecord("ID", rsData.Get_Fields_Int32("SLienRecordNumber"));
					if (rsLN.NoMatch)
					{
						dblBill = 0;
					}
					else
					{
						dblBill = modUTCalculations.CalculateAccountUTLien(rsLN, DateTime.Today, ref dblXtraInt, false);
					}
				}
				// no outstanding amounts, then find another one
				if (dblBill == 0)
				{
					rsData.MoveNext();
					goto TRYAGAIN;
				}
				dblTotal += dblBill;
				fldTaxes.Text = Strings.Format(dblBill, "#,##0.00");
				fldPrevOwner.Text = FCConvert.ToString(rsData.Get_Fields_String("OName"));
				lblTaxes.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillNumber"));
				rsData.MoveNext();
			}
			else
			{
				fldTaxes.Text = "";
				fldPrevOwner.Text = "";
				lblTaxes.Text = "";
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalTaxes.Text = Strings.Format(dblTotal, "#,##0.00");
		}

		private bool FindOutstaingTaxes()
		{
			bool FindOutstaingTaxes = false;
			double dblXtraInt = 0;
			double dblBill = 0;
			while (!rsData.EndOfFile())
			{
				if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SLienRecordNumber")) == 0)
				{
					// calculate the bill total
					dblBill = modUTCalculations.CalculateAccountUT(rsData, DateTime.Today, ref dblXtraInt, true) + modUTCalculations.CalculateAccountUT(rsData, DateTime.Today, ref dblXtraInt, false);
				}
				else
				{
					rsLN.FindFirstRecord("ID", rsData.Get_Fields_Int32("SLienRecordNumber"));
					if (rsLN.NoMatch)
					{
						dblBill = 0;
					}
					else
					{
						dblBill = modUTCalculations.CalculateAccountUTLien(rsLN, DateTime.Today, ref dblXtraInt, false);
					}
				}
				// no outstanding amounts, then find another one
				if (dblBill == 0)
				{
					rsData.MoveNext();
				}
				else
				{
					FindOutstaingTaxes = true;
					break;
				}
			}
			return FindOutstaingTaxes;
		}

		private void sarTaxAcquiredDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarTaxAcquiredDetail properties;
			//sarTaxAcquiredDetail.Caption	= "TA Detail";
			//sarTaxAcquiredDetail.Icon	= "sarTaxAcquiredDetail.dsx":0000";
			//sarTaxAcquiredDetail.Left	= 0;
			//sarTaxAcquiredDetail.Top	= 0;
			//sarTaxAcquiredDetail.Width	= 11880;
			//sarTaxAcquiredDetail.Height	= 8490;
			//sarTaxAcquiredDetail.StartUpPosition	= 3;
			//sarTaxAcquiredDetail.SectionData	= "sarTaxAcquiredDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
