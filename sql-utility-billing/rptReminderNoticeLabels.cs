﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptReminderNoticeLabels.
	/// </summary>
	public partial class rptReminderNoticeLabels : BaseSectionReport
	{
		public rptReminderNoticeLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
		}

		public static rptReminderNoticeLabels InstancePtr
		{
			get
			{
				return (rptReminderNoticeLabels)Sys.GetInstance(typeof(rptReminderNoticeLabels));
			}
		}

		protected rptReminderNoticeLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsLData.Dispose();
				rsTemp.Dispose();
				rsRate.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderNoticeLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/03/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/31/2006              *
		// ********************************************************
		// THIS REPORT IS TOTOALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLData = new clsDRWrapper();
		clsDRWrapper rsTemp = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		int lngReportType;
		clsDRWrapper rsRate = new clsDRWrapper();
		int intReminderType;
		// 1- 4 is the period that they want a reminder for and 0 is for past due
		string strLastPrinter = "";
		string strLeftAdjustment = "";
		// vbPorter upgrade warning: lngVertAdjust As int	OnWriteFCConvert.ToDouble(
		int lngVertAdjust;
		string strRateKeyList = "";
		bool boolWater;
		bool boolSendTenant;
		bool boolSendOwner;
		bool boolSecondCopy;
		// DJW 11/6/07 Add the following variables to make global labels class work
		clsPrintLabel labLabels = new clsPrintLabel();
		// vbPorter upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		int lngPrintWidth;
		int intTypeOfLabel;
		string strMasterQry = "";
		// -------------------------------------------------------------------------
		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
		}
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intPassType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref string strSQL, int intReportType, ref int intLabelType, ref string strPrinterName, ref string strFonttoUse, ref bool boolPassWater, int intPassType = 0, string strPassRateKeyList = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intReturn;
				int x;
				bool boolUseFont;
				double dblLabelsAdjustment = 0;
				double dblLabelsAdjustmentH = 0;
				string strTemp = "";
				boolWater = boolPassWater;
				strFont = strFonttoUse;
				this.Document.Printer.PrinterName = strPrinterName;
				if (Strings.Trim(strPassRateKeyList) != "")
				{
					if (frmReminderNotices.InstancePtr.boolLienedRecords)
					{
						strRateKeyList = " AND RateKey IN" + strPassRateKeyList;
					}
					else
					{
						strRateKeyList = " AND BillingRateKey IN" + strPassRateKeyList;
					}
				}
				boolSendTenant = FCConvert.CBool(frmReminderNotices.InstancePtr.chkSendTo[0].CheckState == Wisej.Web.CheckState.Checked);
				boolSendOwner = FCConvert.CBool(frmReminderNotices.InstancePtr.chkSendTo[1].CheckState == Wisej.Web.CheckState.Checked);
				intReminderType = intPassType;
				// this will pass the type of reminder in
				boolPP = false;
				boolMort = false;
				// DJW 11/6/07 Add the following variables to make global labels class work
				intTypeOfLabel = intLabelType;
				// --------------------------------
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				rsLData.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
				rsRate.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
				if (rsData.RecordCount() > 0)
				{
					strMasterQry = "SELECT Master.ID, " + "pOwn.FullNameLF AS OwnerName, pOwn2.FullNameLF AS SecondOwnerName, " + "pOwn.Address1 AS [Master.OAddress1], pOwn.Address2 AS [Master.OAddress2], pOwn.Address3 AS [Master.OAddress3], pOwn.City AS [Master.OCity], pOwn.State AS [Master.OState], pOwn.Zip AS [Master.OZip], " + "pBill.FullNameLF AS Name, pBill2.FullNameLF AS Name2, " + "pBill.Address1 AS [Master.BAddress1], pBill.Address2 AS [Master.BAddress2], pBill.Address3 AS [Master.BAddress3], pBill.City AS [Master.BCity], pBill.State AS [Master.BState], pBill.Zip AS [Master.BZip] " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID " + "INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID " + "LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID " + "LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID";
					if (frmReminderNotices.InstancePtr.boolLienedRecords)
					{
						if (boolWater)
						{
							rsTemp.OpenRecordset("SELECT * FROM (Lien INNER JOIN Bill ON Lien.ID = Bill.WLienRecordNumber) INNER JOIN ( " + strMasterQry + " ) AS qMstr ON Bill.AccountKey = qMstr.ID WHERE Service <> 'S' " + strRateKeyList, modExtraModules.strUTDatabase);
						}
						else
						{
							rsTemp.OpenRecordset("SELECT * FROM (Lien INNER JOIN Bill ON Lien.ID = Bill.SLienRecordNumber) INNER JOIN ( " + strMasterQry + " ) AS qMstr ON Bill.AccountKey = qMstr.ID WHERE Service <> 'W' " + strRateKeyList, modExtraModules.strUTDatabase);
						}
					}
					else
					{
						if (boolWater)
						{
							rsTemp.OpenRecordset("SELECT * FROM Bill INNER JOIN ( " + strMasterQry + " ) AS qMstr ON Bill.AccountKey = qMstr.ID WHERE Service <> 'S' " + strRateKeyList, modExtraModules.strUTDatabase);
						}
						else
						{
							rsTemp.OpenRecordset("SELECT * FROM Bill INNER JOIN ( " + strMasterQry + " ) AS qMstr ON Bill.AccountKey = qMstr.ID WHERE Service <> 'W' " + strRateKeyList, modExtraModules.strUTDatabase);
						}
					}
					// kk11242014 troge-241  Use the label adjustment(s)
					if (intLabelType == 0 || intLabelType == 1)
					{
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMV", ref strTemp);
						dblLabelsAdjustment = Conversion.Val(strTemp);
						modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMH", ref strTemp);
						dblLabelsAdjustmentH = Conversion.Val(strTemp);
						if (dblLabelsAdjustmentH > 0 && dblLabelsAdjustmentH < 8)
						{
							dblLabelsAdjustmentH = dblLabelsAdjustmentH;
						}
						else
						{
							dblLabelsAdjustmentH = 0;
						}
					}
					else
					{
						dblLabelsAdjustment = modMain.Statics.gdblLabelsAdjustment;
					}
					switch (intReportType)
					{
						case 0:
							{
								// Labels
								modUTStatusList.Statics.strReportType = "LABELS";
								// make them choose the printer first if you have to use a custom form
								// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
								if (rsData.RecordCount() != 0)
									rsData.MoveFirst();
								boolDifferentPageSize = false;
								// DJW 11/6/07 Add the following variables to make global labels class work
								int intindex = 0;
								int cnt;
								intindex = labLabels.Get_IndexFromID(intTypeOfLabel);
								
								if (labLabels.Get_IsDymoLabel(intindex))
								{
									switch (labLabels.Get_ID(intindex))
									{
										case modLabels.CNSTLBLTYPEDYMO30256:
											{
												// If Not Me.Printer.PrintDialog Then
												// Unload Me
												// Exit Sub
												// End If
												strPrinterName = this.Document.Printer.PrinterName;
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												;
												// kk11212014 troge-241  Change to use global default settings
												this.PageSettings.Margins.Top = FCConvert.ToSingle(labLabels.Get_TopMargin(intindex) + (dblLabelsAdjustment * 270) / 1440f);
												// 0.128 * 1440
												this.PageSettings.Margins.Bottom = labLabels.Get_BottomMargin(intindex);
												// 0
												this.PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
												// 0
												this.PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
												// 0.128 * 1440
												PrintWidth = labLabels.Get_LabelWidth(intindex) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
												// (2.31 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
												intLabelWidth = labLabels.Get_LabelWidth(intindex);
												// (4 * 1440)
												lngPrintWidth = FCConvert.ToInt32(PrintWidth);
												// (4 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
												Detail.Height = labLabels.Get_LabelHeight(intindex) - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
												// (1440 * 2.3125) - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 '2 5/16"
												Detail.ColumnCount = 1;
												PageSettings.PaperHeight = labLabels.Get_LabelWidth(intindex);
												// Landscape                    '4 * 1440
												PageSettings.PaperWidth = labLabels.Get_LabelHeight(intindex);
												// 2.31 * 1440
												Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												;
												break;
											}
										case modLabels.CNSTLBLTYPEDYMO30252:
											{
												strPrinterName = this.Document.Printer.PrinterName;
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												;
												this.PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
												this.PageSettings.Margins.Bottom = 0;
												this.PageSettings.Margins.Right = 0;
												this.PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
												// 
												PrintWidth = (3.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
												intLabelWidth = FCConvert.ToInt32((3.5));
												lngPrintWidth = FCConvert.ToInt32((3.5) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right);
												Detail.Height = (1.1F) - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
												Detail.ColumnCount = 1;
												PageSettings.PaperHeight = FCConvert.ToSingle(3.5);
												PageSettings.PaperWidth = FCConvert.ToSingle(1.1);
												Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
												this.Document.Printer.DefaultPageSettings.Landscape = true;
												PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
												;
												break;
											}
									}
									//end switch
								}
								else if (labLabels.Get_IsLaserLabel(intindex))
								{
									this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
									this.PageSettings.Margins.Top += FCConvert.ToSingle(modMain.Statics.gdblLabelsAdjustment * 270) / 1440f;
									this.PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
									this.PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
									PrintWidth = labLabels.Get_PageWidth(intindex) - labLabels.Get_LeftMargin(intindex) - labLabels.Get_RightMargin(intindex);
									intLabelWidth = labLabels.Get_LabelWidth(intindex);
									Detail.Height = labLabels.Get_LabelHeight(intindex) + labLabels.Get_VerticalSpace(intindex);
									if (labLabels.Get_LabelsWide(intindex) > 0)
									{
										Detail.ColumnCount = labLabels.Get_LabelsWide(intindex);
										Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intindex);
									}
									lngPrintWidth = FCConvert.ToInt32(PrintWidth);
								}
								// Select Case intLabelType
								// Case 0
								// 4013
								// boolDifferentPageSize = True
								// PrintWidth = 3.5 * 1440 - 100
								// this.PageSettings.Margins.Top = 0 '+ (gdblLabelsAdjustment * 270)
								// this.PageSettings.Margins.Left = 0
								// this.PageSettings.Margins.Right = 0
								// PageSettings.PaperSize = 255
								// PageSettings.PaperHeight = 1440
								// PageSettings.PaperWidth = 3.5 * 1440
								// Detail.Height = 1200
								// strLeftAdjustment = String(CLng(gdblLabelsAdjustmentDMH), " ")
								// lngVertAdjust = (240 * gdblLabelsAdjustmentDMV)
								// 
								// Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After
								// lngReportType = 0
								// Case 1
								// 4014
								// boolDifferentPageSize = True
								// PrintWidth = 4 * 1440 - 1
								// this.PageSettings.Margins.Top = 0 '+ (gdblLabelsAdjustment * 270)
								// this.PageSettings.Margins.Left = 0
								// this.PageSettings.Margins.Right = 0
								// PageSettings.PaperSize = 255
								// PageSettings.PaperHeight = 1.5 * 1440
								// PageSettings.PaperWidth = 4 * 1440
								// Detail.Height = 1200
								// PrintWidth = 4 * 1440 - 1
								// strLeftAdjustment = String(CLng(gdblLabelsAdjustmentDMH), " ")
								// lngVertAdjust = (240 * gdblLabelsAdjustmentDMV)
								// 
								// 
								// Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After
								// lngReportType = 1
								// Case 2
								// Avery 5260 (3 X 10)  Height = 1" Width = 2.63"
								// this.PageSettings.Margins.Top = (0.5 * 1440) + (gdblLabelsAdjustment * 270)
								// this.PageSettings.Margins.Left = (0.1875 * 1440)
								// this.PageSettings.Margins.Right = (0.0625 * 1440)
								// PrintWidth = (8.5 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
								// intLabelWidth = (2.625 * 1440)
								// strLeftAdjustment = ""
								// lngVertAdjust = 0
								// Detail.Height = 1440
								// Detail.ColumnCount = 3
								// Detail.ColumnSpacing = (0.0625 * 1440)
								// lngReportType = 2
								// Case 3
								// Avery 5261 (2 X 10)  Height = 1" Width = 4"
								// this.PageSettings.Margins.Top = (0.5 * 1440) + (gdblLabelsAdjustment * 270)
								// this.PageSettings.Margins.Left = (0.1875 * 1440)
								// this.PageSettings.Margins.Right = (0.1875 * 1440)
								// PrintWidth = (8.5 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
								// intLabelWidth = (4 * 1440)
								// strLeftAdjustment = ""
								// lngVertAdjust = 0
								// 
								// Detail.Height = 1440
								// Detail.ColumnCount = 2
								// Detail.ColumnSpacing = (0.1875 * 1440)
								// lngReportType = 3
								// Case 4
								// Avery 5262 (2 X 7)  Height = 1 1/3" Width = 4"
								// this.PageSettings.Margins.Top = (0.8125 * 1440) + (gdblLabelsAdjustment * 270)
								// this.PageSettings.Margins.Left = (0.1875 * 1440)
								// this.PageSettings.Margins.Right = (0.1875 * 1440)
								// PrintWidth = (8.5 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
								// intLabelWidth = (4 * 1440)
								// strLeftAdjustment = ""
								// lngVertAdjust = 0
								// Detail.Height = (1440 * 1.33)
								// Detail.ColumnCount = 2
								// Detail.ColumnSpacing = (0.1875 * 1440)
								// lngReportType = 4
								// Case 5
								// Avery 5163 (2 X 5)  Height = 2" Width = 4"
								// this.PageSettings.Margins.Top = (0.5 * 1440) + (gdblLabelsAdjustment * 270)
								// this.PageSettings.Margins.Left = (0.1875 * 1440)
								// this.PageSettings.Margins.Right = (0.1875 * 1440)
								// PrintWidth = (8.5 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
								// intLabelWidth = (4 * 1440)
								// strLeftAdjustment = ""
								// lngVertAdjust = 0
								// Detail.Height = (1440 * 2)
								// Detail.ColumnCount = 2
								// Detail.ColumnSpacing = (0.15625 * 1440)
								// lngReportType = 5
								// End Select
								// ----------------------------------------------------------------------
								break;
							}
						case 1:
							{
								// Post Cards
								modUTStatusList.Statics.strReportType = "POSTCARDS";
								this.PageSettings.Margins.Top = (0);
								this.PageSettings.Margins.Bottom = 0;
								PrintWidth = 6.5F;
								this.PageSettings.Margins.Left = 1;
								this.PageSettings.Margins.Right = 1;
								Detail.KeepTogether = false;
								Detail.Height = (4);
								Detail.CanShrink = false;
								Detail.ColumnCount = 1;
								lngReportType = 10;
								break;
							}
						case 2:
							{
								// Forms
								modUTStatusList.Statics.strReportType = "FORMS";
								break;
							}
					}
					//end switch
					CreateDataFields(intReportType, intLabelType);
					frmReportViewer.InstancePtr.Init(this, strPrinterName);
				}
				else
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No records meet the criteria selected.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Notices", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
            {
                int cnt;

                for (cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
				{
					switch (intTypeOfLabel)
					{
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
					}
					//end switch
				}
				// cnt
            }
			else
			{
				this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CLCustomLabel1", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
			}
		}

		private void CreateDataFields(int intRepType, int intLabType)
		{
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow = 0;
            float lngLineHeight;
			lngLineHeight = 225 / 1440F;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS for each type
			switch (intRepType)
			{
				case 0:
					{
						// Labels
						var intNumber = 4;
						if (boolMort)
							intNumber = 5;
						for (intRow = 1; intRow <= intNumber; intRow++)
						{
							NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
							NewField.CanGrow = false;
							NewField.Name = "txtData" + FCConvert.ToString(intRow);
							NewField.Top = (intRow - 1) * lngLineHeight;
							NewField.Left = 144 / 1440f;
							// one space
							// DJW 11/6/07 Add the following variables to make global labels class work
							// If intRow = 1 Then
							// NewField.Width = ((PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 870
							// Else
							NewField.Width = (FCConvert.ToInt32(PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
							// End If
							// ----------------------------------------------------
							NewField.Height = 225 / 1440f;
							NewField.Text = string.Empty;
							NewField.MultiLine = false;
							NewField.WordWrap = true;
							if (Strings.Trim(strFont) != string.Empty)
							{
								NewField.Font = new Font(strFont, NewField.Font.Size);
							}
							Detail.Controls.Add(NewField);
						}
						break;
					}
				case 1:
					{
						// Post Cards
						// set up the form
						Detail.Height = 3;
						// 3"
						this.PageSettings.PaperHeight = 4;
						// 4"
						this.PageSettings.Margins.Left = 360 / 1440f;
						// 1/4"
						this.PageSettings.Margins.Top = 720 / 1440f;
						// 1/2"
						this.PageSettings.Margins.Bottom = 1;
						// 1"
						// Return Mailing Address 1
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress1";
						NewField.Top = 255 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 2
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress2";
						NewField.Top = 225 * 2 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 3
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress3";
						NewField.Top = 225 * 3 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 4
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress4";
						NewField.Top = 225 * 4 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Message (multiline)
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMessage";
						NewField.Top = 1.25F;
						// 1 1/4"
						NewField.Left = 0.375F;
						// 3/8"
						NewField.Width = this.PrintWidth - NewField.Left;
						NewField.Height = 1;
						// 1"
						NewField.Text = string.Empty;
						NewField.MultiLine = true;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Account
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtAccount";
						NewField.Top = 2.125F;
						// 2"
						NewField.Left = 0.875F;
						// 3/8"
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Name
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtName";
						NewField.Top = (2.125F) + 255 / 1440F;
						NewField.Left = 0.875F;
						// 7/8"
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 1
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress1";
						NewField.Top = (2.125F) + (255 * 2) / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 2
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress2";
						NewField.Top = (2.125F) + (255 * 3) / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 3
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress3";
						NewField.Top = (2.125F) + (255 * 4) / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 4
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress4";
						NewField.Top = (2.125F) + (255 * 5) / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 5
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress5";
						NewField.Top = (2.125F) + (255 * 6) / 1440f;
						// this will move this label to the bottom of the detail section to make sure that
						// the detail section cannot shrink to smaller than the post card height
						NewField.Top = (4) - 225 / 1440F;
						NewField.Text = " ";
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						break;
					}
				case 2:
					{
						// Forms
						// setup the page size
						switch (intLabType)
						{
							case 0:
								{
									// Plain Paper (1 per using half page)
									PrintWidth = 6.5F;
									this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
									this.PageSettings.Margins.Left = 1;
									this.PageSettings.Margins.Right = 1;
									Detail.Height = (5.5F);
									Detail.ColumnCount = 1;
									lngReportType = 21;
									break;
								}
							case 1:
								{
									// Plain Paper (2 per page)
									PrintWidth = 6.5F;
									this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
									this.PageSettings.Margins.Left = 1;
									this.PageSettings.Margins.Right = 1;
									Detail.Height = (5.5F);
									Detail.ColumnCount = 1;
									lngReportType = 22;
									break;
								}
							case 2:
								{
									// Plain Paper (1 per using whole page)
									PrintWidth = 6.5F;
									this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
									this.PageSettings.Margins.Left = 1;
									this.PageSettings.Margins.Right = 1;
									Detail.Height = (11);
									Detail.ColumnCount = 1;
									lngReportType = 23;
									break;
								}
						}
						//end switch
						// setup the fields
						switch (intLabType)
						{
							case 0:
							case 1:
								{
									// Plain Paper (1 or two per page)
									// Return Mailing Address 1
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtReturnMailingAddress1";
									NewField.Top = 0;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Return Mailing Address 2
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtReturnMailingAddress2";
									NewField.Top = 225 / 1440f;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Return Mailing Address 3
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtReturnMailingAddress3";
									NewField.Top = 2 * 225 / 1440f;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Return Mailing Address 4
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtReturnMailingAddress4";
									NewField.Top = 3 * 225 / 1440f;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Account
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtAccountNumber";
									NewField.Top = (intRow - 1) * 225 / 1440f;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Message (multiline)
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMessage";
									NewField.Top = 1;
									// 1"
									NewField.Left = 0.375F;
									// 3/8"
									NewField.Width = this.PrintWidth - NewField.Left;
									NewField.Height = 1;
									// 1"
									NewField.Text = string.Empty;
									NewField.MultiLine = true;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Amount Due Line
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtAmountDue";
									NewField.Top = (intRow - 1) * 225 / 1440f;
									NewField.Left = 0;
									NewField.Width = this.PrintWidth - NewField.Left;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Date Line
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtDateLine";
									NewField.Top = (2);
									NewField.Left = 0;
									NewField.Width = this.PrintWidth - NewField.Left;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Name
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtName";
									NewField.Top = (2) + 255 / 1440f;
									NewField.Left = 0.875F;
									// 7/8"
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 1
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress1";
									NewField.Top = (2) + (255 * 2) / 1440f;
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 2
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress2";
									NewField.Top = (2) + (255 * 3) / 1440f;
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 3
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress3";
									NewField.Top = (2) + (255 * 4) / 1440f;
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 4
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress4";
									NewField.Top = (2) + (255 * 5) / 1440f;
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									// Mailing Address 5
									NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
									NewField.CanGrow = false;
									NewField.Name = "txtMailingAddress5";
									NewField.Top = (2) + (255 * 6) / 1440f;
									NewField.Left = 0.875F;
									NewField.Width = this.PrintWidth / 2;
									NewField.Height = 225 / 1440f;
									NewField.Text = string.Empty;
									NewField.MultiLine = false;
									NewField.WordWrap = true;
									if (Strings.Trim(strFont) != string.Empty)
									{
										NewField.Font = new Font(strFont, NewField.Font.Size);
									}
									Detail.Controls.Add(NewField);
									break;
								}
							case 2:
								{
									// Full page notice
									break;
								}
						}
						//end switch
						break;
					}
				case 3:
					{
						// Mailer
						// set up the form
						PrintWidth = 6.5F;
						Detail.Height = 3;
						// 3"
						this.PageSettings.PaperHeight = 4;
						// 4"
						this.PageSettings.Margins.Left = 360 / 1440f;
						// 1/4"
						this.PageSettings.Margins.Top = 720 / 1440f;
						// 1/2"
						this.PageSettings.Margins.Bottom = 720 / 1440f;
						// 1/2"
						// Return Mailing Address 1
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress1";
						NewField.Top = 0;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 2
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress2";
						NewField.Top = 225 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 3
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress3";
						NewField.Top = 2 * 225 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Return Mailing Address 4
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtReturnMailingAddress4";
						NewField.Top = 3 * 225 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Account
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtAccountNumber";
						NewField.Top = (intRow - 1) * 225 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Message (multiline)
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMessage";
						NewField.Top = 1;
						// 1"
						NewField.Left = 0.375F;
						// 3/8"
						NewField.Width = this.PrintWidth - NewField.Left;
						NewField.Height = 1;
						// 1"
						NewField.Text = string.Empty;
						NewField.MultiLine = true;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Amount Due Line
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtAmountDue";
						NewField.Top = (intRow - 1) * 225 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Date Line
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtDateLine";
						NewField.Top = (intRow - 1) * 225 / 1440f;
						NewField.Left = 0;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Name
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtName";
						NewField.Top = 2.125F;
						// 2 1/8"
						NewField.Left = 0.875F;
						// 7/8"
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 1
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress1";
						NewField.Top = (2.125F) + 225 / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 2
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress2";
						NewField.Top = (2.125F) + (225 * 2) / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 3
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress3";
						NewField.Top = (2.125F) + (225 * 3) / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 4
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress4";
						NewField.Top = (2.125F) + (225 * 4) / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						// Mailing Address 5
						NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
						NewField.CanGrow = false;
						NewField.Name = "txtMailingAddress5";
						NewField.Top = (2.125F) + (225 * 5) / 1440f;
						NewField.Left = 0.875F;
						NewField.Width = this.PrintWidth / 2;
						NewField.Height = 225 / 1440f;
						NewField.Text = string.Empty;
						NewField.MultiLine = false;
						NewField.WordWrap = true;
						if (Strings.Trim(strFont) != string.Empty)
						{
							NewField.Font = new Font(strFont, NewField.Font.Size);
						}
						Detail.Controls.Add(NewField);
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
		}

		public void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			// Call VerifyPrintToFile(Me, Tool)
			string vbPorterVar = ""/*Tool.Caption*/;
			if (vbPorterVar == "Print...")
			{
				int intNumberOfPages = this.Document.Pages.Count;
				int intPageStart = 1;
				if (frmNumPages.InstancePtr.Init(ref intNumberOfPages, ref intPageStart, this))
				{
					this.PrintReport(false);
				}
			}
		}

		private void PrintForms()
		{
			// kgk  08092012  THIS IS COLLECTIONS CODE SO I ASSUME IT IS NOT USED
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			if (rsTemp.EndOfFile() != true)
			{
				str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1")));
				str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSAddr1")));
				str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSAddr2")));
				str4 = Strings.Trim(rsTemp.Get_Fields_String("RSAddr3") + "  " + rsTemp.Get_Fields_String("RSState"));
				if (boolPP)
				{
					if (Conversion.Val(rsTemp.Get_Fields_String("RSZip")) > 0)
					{
						str4 += " " + Strings.Format(rsTemp.Get_Fields_String("RSZip"), "00000");
						// TODO Get_Fields: Field [MRSZip4] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsTemp.Get_Fields("MRSZip4")) > 0)
						{
							str4 += "-" + Strings.Format(rsTemp.Get_Fields_String("RSZip4"), "0000");
						}
					}
				}
				else if (boolMort)
				{
					str4 = Strings.Trim(rsTemp.Get_Fields_String("RSAddr3") + "  " + rsTemp.Get_Fields_String("RSState"));
					str4 += " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip")));
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip4"))) != string.Empty)
					{
						str4 += "-" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip4")));
					}
				}
				else
				{
					str4 += " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip")));
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip4"))) != string.Empty)
					{
						str4 += "-" + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("RSZip4")));
					}
				}
				// condense the labels if some are blank
				if (boolMort)
				{
					if (Strings.Trim(str4) == string.Empty)
					{
						str4 = str5;
						str5 = "";
					}
				}
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						switch (intRow)
						{
							case 1:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str1 + " ";
									break;
								}
							case 2:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str2 + " ";
									break;
								}
							case 3:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str3 + " ";
									break;
								}
							case 4:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str4 + " ";
									break;
								}
							case 5:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str5 + " ";
									break;
								}
						}
						//end switch
					}
				}
				// intControl
				rsTemp.MoveNext();
			}
		}

		private void PrintLabels()
		{
			// this will fil in the fields for labels
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			TRYAGAIN:
			;
			if (rsTemp.EndOfFile() != true)
			{
				// check to make sure this account would be on this label report
				if (!CheckAccount())
				{
					// if the account shold not be here then
					rsTemp.MoveNext();
					goto TRYAGAIN;
				}
				if (boolSendTenant && !boolSecondCopy)
				{
					boolSecondCopy = false;
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2"))) != "")
					{
						str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name"))) + " and " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2")));
					}
					else
					{
						str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name")));
					}
					// this will fill the address form the master screen for the mail address
					// TODO Get_Fields: Field [Master.BAddress1] not found!! (maybe it is an alias?)
					str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.BAddress1")));
					// TODO Get_Fields: Field [Master.BAddress2] not found!! (maybe it is an alias?)
					str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.BAddress2")));
					// TODO Get_Fields: Field [Master.BAddress3] not found!! (maybe it is an alias?)
					str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.BAddress3")));
					// TODO Get_Fields: Field [Master.BCity] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Master.BState] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Master.BZip] not found!! (maybe it is an alias?)
					str5 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.BCity"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.BState"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.BZip")));
					// If Trim(.Fields("Master.BZip4")) <> "" Then
					// str5 = str5 & "-" & Trim(.Fields("Master.BZip4"))
					// End If
					if (boolSendOwner)
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name"))) != Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("OwnerName"))))
						{
							boolSecondCopy = true;
						}
					}
				}
				else if (boolSendOwner || boolSecondCopy)
				{
					boolSecondCopy = false;
					// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("SecondOwnerName"))) != "")
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
						str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("OwnerName"))) + " and " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("SecondOwnerName")));
					}
					else
					{
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("OwnerName")));
					}
					// this will fill the address form the master screen for the mail address
					// TODO Get_Fields: Field [Master.OAddress1] not found!! (maybe it is an alias?)
					str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.OAddress1")));
					// TODO Get_Fields: Field [Master.OAddress2] not found!! (maybe it is an alias?)
					str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.OAddress2")));
					// TODO Get_Fields: Field [Master.OAddress3] not found!! (maybe it is an alias?)
					str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.OAddress3")));
					// TODO Get_Fields: Field [Master.OCity] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Master.OState] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Master.OZip] not found!! (maybe it is an alias?)
					str5 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.OCity"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.OState"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields("Master.OZip")));
					// If Trim(.Fields("Master.OZip4")) <> "" Then
					// str5 = str5 & "-" & Trim(.Fields("Master.OZip4"))
					// End If
				}
				// str1 = strLeftAdjustment & Trim(.Fields("OName"))
				// get the mailing address
				// str2 = strLeftAdjustment & Trim(.Fields("Bill.OAddress1"))
				// str3 = strLeftAdjustment & Trim(.Fields("Bill.OAddress2"))
				// str4 = strLeftAdjustment & Trim(.Fields("Bill.OAddress3"))
				// str5 = strLeftAdjustment & Trim(.Fields("Bill.OCity")) & " " & Trim(.Fields("Bill.OState")) & " " & Trim(.Fields("Bill.OZip"))
				// 
				// If Trim(str2) = "" Then
				// this will fill the address form the master screen for the mail address
				// str2 = Trim(.Fields("Master.OAddress1"))
				// str3 = Trim(.Fields("Master.OAddress2"))
				// str4 = Trim(.Fields("Master.OAddress3"))
				// str5 = Trim(.Fields("Master.OCity")) & " " & Trim(.Fields("Master.OState")) & " " & Trim(.Fields("Master.OZip"))
				// If Trim(.Fields("Master.OZip4")) <> "" Then
				// str5 = str5 & "-" & Trim(.Fields("Master.OZip4"))
				// End If
				// End If
				// condense the labels if some are blank
				// If boolMort Then
				if (Strings.Trim(str4) == string.Empty)
				{
					str4 = str5;
					str5 = "";
				}
				// End If
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					// If boolMort Then
					str4 = str5;
					str5 = "";
					// Else
					// str4 = ""
					// End If
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					// If boolMort Then
					str4 = str5;
					str5 = "";
					// Else
					// str4 = ""
					// End If
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					// If boolMort Then
					str4 = str5;
					str5 = "";
					// Else
					// str4 = ""
					// End If
				}
				for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						switch (intRow)
						{
							case 1:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str1;
									break;
								}
							case 2:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str2;
									break;
								}
							case 3:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str3;
									break;
								}
							case 4:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str4;
									break;
								}
							case 5:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str5;
									break;
								}
						}
						//end switch
					}
				}
				// intControl
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsData.EndOfFile())
			{
				if (frmReminderNotices.InstancePtr.boolLienedRecords)
				{
					if (boolWater)
					{
						rsTemp.OpenRecordset("SELECT * FROM (Lien INNER JOIN Bill ON Lien.ID = Bill.WLienRecordNumber) INNER JOIN ( " + strMasterQry + " ) AS qMstr ON Bill.AccountKey = qMstr.ID WHERE ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND Service <> 'S' " + strRateKeyList, modExtraModules.strUTDatabase);
					}
					else
					{
						rsTemp.OpenRecordset("SELECT * FROM (Lien INNER JOIN Bill ON Lien.ID = Bill.SLienRecordNumber) INNER JOIN ( " + strMasterQry + " ) AS qMstr ON Bill.AccountKey = qMstr.ID WHERE ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND Service <> 'W' " + strRateKeyList, modExtraModules.strUTDatabase);
					}
				}
				else
				{
					if (boolWater)
					{
						rsTemp.OpenRecordset("SELECT * FROM Bill INNER JOIN ( " + strMasterQry + " ) AS qMstr ON Bill.AccountKey = qMstr.ID WHERE ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND Service <> 'S' " + strRateKeyList, modExtraModules.strUTDatabase);
					}
					else
					{
						rsTemp.OpenRecordset("SELECT * FROM Bill INNER JOIN ( " + strMasterQry + " ) AS qMstr ON Bill.AccountKey = qMstr.ID WHERE ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND Service <> 'W' " + strRateKeyList, modExtraModules.strUTDatabase);
					}
				}
				if (!rsTemp.EndOfFile())
				{
					if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LABELS")
					{
						PrintLabels();
					}
					else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "FORMS")
					{
						PrintForms();
					}
					else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "POSTCARDS")
					{
						PrintPostCards();
					}
					else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "MAILER")
					{
						PrintMailers();
					}
				}
				if (!boolSecondCopy)
				{
					rsData.MoveNext();
				}
			}
		}

		private void PrintPostCards()
		{
			// this will fill in the fields for post card
			int intCTRLIndex;
			string str1;
			string str2;
			string str3;
			string str4;
			string str5 = "";
			str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2")));
			str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
			str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
			str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
			// condense the labels if some are blank
			if (Strings.Trim(str4) == string.Empty)
			{
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str3) == string.Empty)
			{
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str2) == string.Empty)
			{
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str1) == string.Empty)
			{
				str1 = str2;
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			// Return Address 1
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress1");
			if (intCTRLIndex >= 0)
			{
				// row 3
				// Detail.Controls[intCTRLIndex].Text = frmReminderNotices.vsGrid.TextMatrix(3, 1)
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(1, 0);
			}
			// Return Address 2
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress2");
			if (intCTRLIndex >= 0)
			{
				// row 4
				// Detail.Controls[intCTRLIndex].Text = frmReminderNotices.vsGrid.TextMatrix(4, 1)
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(2, 0);
			}
			// Return Address 3
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress3");
			if (intCTRLIndex >= 0)
			{
				// row 5
				// Detail.Controls[intCTRLIndex].Text = frmReminderNotices.vsGrid.TextMatrix(5, 1)
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(3, 0);
			}
			// Return Address 4
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress4");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(4, 0);
			}
			// Account Number
			intCTRLIndex = FindControlIndex_2("txtAccount");
			if (intCTRLIndex >= 0)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Account: " + rsTemp.Get_Fields("Account");
			}
			// Message
			intCTRLIndex = FindControlIndex_2("txtMessage");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(6, 1);
			}
			// Name
			intCTRLIndex = FindControlIndex_2("txtName");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str1;
			}
			// Mailing Address 1
			intCTRLIndex = FindControlIndex_2("txtMailingAddress1");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str2;
			}
			// Mailing Address 2
			intCTRLIndex = FindControlIndex_2("txtMailingAddress2");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str3;
			}
			// Mailing Address 3
			intCTRLIndex = FindControlIndex_2("txtMailingAddress3");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str4;
			}
			// Mailing Address 4
			intCTRLIndex = FindControlIndex_2("txtMailingAddress4");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str5;
			}
		}

		private void PrintMailers()
		{
			// this routine will add the data to the fields
			int intCTRLIndex;
			double dblTotalDue = 0;
			double dblXtraInt = 0;
			string str1;
			string str2;
			string str3;
			string str4;
			string str5 = "";
			str1 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name1"))) + " " + Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Name2")));
			str2 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address1")));
			str3 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address2")));
			str4 = Strings.Trim(FCConvert.ToString(rsTemp.Get_Fields_String("Address3")));
			// condense the labels if some are blank
			if (Strings.Trim(str4) == string.Empty)
			{
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str3) == string.Empty)
			{
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str2) == string.Empty)
			{
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str1) == string.Empty)
			{
				str1 = str2;
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			// Return Address 1
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress1");
			if (intCTRLIndex >= 0)
			{
				// row 3
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(3, 1);
			}
			// Return Address 2
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress2");
			if (intCTRLIndex >= 0)
			{
				// row 4
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(4, 1);
			}
			// Return Address 3
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress3");
			if (intCTRLIndex >= 0)
			{
				// row 5
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(5, 1);
			}
			// Return Address 4
			intCTRLIndex = FindControlIndex_2("txtReturnMailingAddress4");
			if (intCTRLIndex >= 0)
			{
				// none for now
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "";
			}
			// Account Number
			intCTRLIndex = FindControlIndex_2("txtAccount");
			if (intCTRLIndex >= 0)
			{
				// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = "Account: " + rsTemp.Get_Fields("Account");
			}
			// Message
			intCTRLIndex = FindControlIndex_2("txtMessage");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(6, 1);
			}
			// Amount Due
			intCTRLIndex = FindControlIndex_2("txtAmountDue");
			if (intCTRLIndex >= 0)
			{
				if (FCConvert.ToInt32(rsTemp.Get_Fields_Int32("LienRecordNumber")) != 0)
				{
					rsLData.FindFirstRecord("LienRecordNumber", rsTemp.Get_Fields_Int32("LienRecordNumber"));
					if (rsLData.NoMatch)
					{
						dblTotalDue = 0;
					}
					else
					{
						dblXtraInt = 0;
						dblTotalDue = modUTCalculations.CalculateAccountUTLien(rsTemp, DateTime.Today, ref dblXtraInt, boolWater);
					}
				}
				else
				{
					dblXtraInt = 0;
					dblTotalDue = modUTCalculations.CalculateAccountUT(rsTemp, DateTime.Today, ref dblXtraInt, boolWater);
				}
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(dblTotalDue, "#,##0.00");
			}
			// Name
			intCTRLIndex = FindControlIndex_2("txtName");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str1;
			}
			// Mailing Address 1
			intCTRLIndex = FindControlIndex_2("txtMailingAddress1");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str2;
			}
			// Mailing Address 2
			intCTRLIndex = FindControlIndex_2("txtMailingAddress2");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str3;
			}
			// Mailing Address 3
			intCTRLIndex = FindControlIndex_2("txtMailingAddress3");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str4;
			}
			// Mailing Address 4
			intCTRLIndex = FindControlIndex_2("txtMailingAddress4");
			if (intCTRLIndex >= 0)
			{
				(Detail.Controls[intCTRLIndex] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = str5;
			}
		}
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short FindControlIndex_2(string strName)
		{
			return FindControlIndex(ref strName);
		}

		private short FindControlIndex(ref string strName)
		{
			short FindControlIndex = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intindex;
				// this function will accept the name of a field and return the index of
				// field in the control collection in order to access its properties
				FindControlIndex = -1;
				for (intindex = 0; intindex <= Detail.Controls.Count - 1; intindex++)
				{
					// Debug.Print Detail.Controls[intIndex].Name
					if (Detail.Controls[intindex].Name == strName)
					{
						FindControlIndex = FCConvert.ToInt16(intindex);
						break;
					}
				}
				return FindControlIndex;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Control Index Search", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				FindControlIndex = -1;
			}
			return FindControlIndex;
		}

		private bool CheckAccount()
		{
			bool CheckAccount = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngAcctKey = 0;
				// this function will check the current account to see if it needs a reminder notice
				rsRate.FindFirstRecord("ID", rsTemp.Get_Fields_Int32("BillingRateKey"));
				if (rsRate.NoMatch)
				{
					CheckAccount = false;
				}
				else
				{
					// check to see if anything is due
					lngAcctKey = FCConvert.ToInt32(rsTemp.Get_Fields_Int32("AccountKey"));
					if (modUTCalculations.CalculateAccountUTTotal(lngAcctKey, boolWater) != 0)
					{
						CheckAccount = true;
					}
					else
					{
						CheckAccount = false;
					}
				}
				return CheckAccount;
			}
			catch (Exception ex)
			{
				
				CheckAccount = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CheckAccount;
		}

		private void rptReminderNoticeLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderNoticeLabels properties;
			//rptReminderNoticeLabels.Caption	= "Custom Labels";
			//rptReminderNoticeLabels.Icon	= "rptReminderNoticeLabels.dsx":0000";
			//rptReminderNoticeLabels.Left	= 0;
			//rptReminderNoticeLabels.Top	= 0;
			//rptReminderNoticeLabels.Width	= 11880;
			//rptReminderNoticeLabels.Height	= 8595;
			//rptReminderNoticeLabels.StartUpPosition	= 3;
			//rptReminderNoticeLabels.SectionData	= "rptReminderNoticeLabels.dsx":058A;
			//End Unmaped Properties
		}
	}
}
