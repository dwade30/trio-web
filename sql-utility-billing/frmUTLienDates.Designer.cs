﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTLienDates.
	/// </summary>
	partial class frmUTLienDates : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public FCGrid vsRate;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrintDateChart;
		public fecherFoundation.FCToolStripMenuItem mnuSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTLienDates));
			this.cmbWS = new fecherFoundation.FCComboBox();
			this.lblWS = new fecherFoundation.FCLabel();
			this.vsRate = new fecherFoundation.FCGrid();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrintDateChart = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.cmdFilePrintDateChart = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintDateChart)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 502);
			this.BottomPanel.Size = new System.Drawing.Size(697, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbWS);
			this.ClientArea.Controls.Add(this.lblWS);
			this.ClientArea.Controls.Add(this.vsRate);
			this.ClientArea.Size = new System.Drawing.Size(697, 442);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFilePrintDateChart);
			this.TopPanel.Controls.Add(this.cmdFilePrint);
			this.TopPanel.Size = new System.Drawing.Size(697, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrint, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFilePrintDateChart, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(130, 30);
			this.HeaderText.Text = "Lien Dates";
			// 
			// cmbWS
			// 
			this.cmbWS.AutoSize = false;
			this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWS.FormattingEnabled = true;
			this.cmbWS.Items.AddRange(new object[] {
				"Water",
				"Sewer"
			});
			this.cmbWS.Location = new System.Drawing.Point(187, 30);
			this.cmbWS.Name = "cmbWS";
			this.cmbWS.Size = new System.Drawing.Size(171, 40);
			this.cmbWS.TabIndex = 1;
			this.cmbWS.SelectedIndexChanged += new System.EventHandler(this.cmbWS_SelectedIndexChanged);
			// 
			// lblWS
			// 
			this.lblWS.AutoSize = true;
			this.lblWS.Location = new System.Drawing.Point(30, 44);
			this.lblWS.Name = "lblWS";
			this.lblWS.Size = new System.Drawing.Size(107, 15);
			this.lblWS.TabIndex = 0;
			this.lblWS.Text = "WATER / SEWER";
			// 
			// vsRate
			// 
			this.vsRate.AllowSelection = false;
			this.vsRate.AllowUserToResizeColumns = false;
			this.vsRate.AllowUserToResizeRows = false;
			this.vsRate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsRate.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsRate.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsRate.BackColorBkg = System.Drawing.Color.Empty;
			this.vsRate.BackColorFixed = System.Drawing.Color.Empty;
			this.vsRate.BackColorSel = System.Drawing.Color.Empty;
			this.vsRate.Cols = 10;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsRate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsRate.ColumnHeadersHeight = 30;
			this.vsRate.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsRate.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsRate.DragIcon = null;
			this.vsRate.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsRate.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSortShow;
			this.vsRate.ExtendLastCol = false;
			this.vsRate.FixedCols = 0;
			this.vsRate.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsRate.FrozenCols = 0;
			this.vsRate.GridColor = System.Drawing.Color.Empty;
			this.vsRate.GridColorFixed = System.Drawing.Color.Empty;
			this.vsRate.Location = new System.Drawing.Point(30, 90);
			this.vsRate.Name = "vsRate";
			this.vsRate.ReadOnly = true;
			this.vsRate.RowHeadersVisible = false;
			this.vsRate.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsRate.RowHeightMin = 0;
			this.vsRate.Rows = 1;
			this.vsRate.ScrollTipText = null;
			this.vsRate.ShowColumnVisibilityMenu = false;
			this.vsRate.Size = new System.Drawing.Size(637, 334);
			this.vsRate.StandardTab = true;
			this.vsRate.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsRate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsRate.TabIndex = 2;
			this.vsRate.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsRate_BeforeEdit);
			this.vsRate.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsRate_AfterEdit);
			this.vsRate.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsRate_ValidateEdit);
			this.vsRate.CurrentCellChanged += new System.EventHandler(this.vsRate_RowColChange);
			this.vsRate.DoubleClick += new System.EventHandler(this.vsRate_DblClick);
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.mnuFilePrintDateChart,
				this.mnuSeperator,
				this.mnuFileSave,
				this.mnuFileSaveExit,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Text = "Print Timeline(s)";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// mnuFilePrintDateChart
			// 
			this.mnuFilePrintDateChart.Index = 1;
			this.mnuFilePrintDateChart.Name = "mnuFilePrintDateChart";
			this.mnuFilePrintDateChart.Text = "Print Date Chart";
			this.mnuFilePrintDateChart.Click += new System.EventHandler(this.mnuFilePrintDateChart_Click);
			// 
			// mnuSeperator
			// 
			this.mnuSeperator.Index = 2;
			this.mnuSeperator.Name = "mnuSeperator";
			this.mnuSeperator.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 3;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 4;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Exit";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 5;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 6;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(301, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrint.AppearanceKey = "toolbarButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(437, 29);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Size = new System.Drawing.Size(112, 24);
			this.cmdFilePrint.TabIndex = 1;
			this.cmdFilePrint.Text = "Print Timeline(s)";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdFilePrintDateChart
			// 
			this.cmdFilePrintDateChart.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFilePrintDateChart.AppearanceKey = "toolbarButton";
			this.cmdFilePrintDateChart.Location = new System.Drawing.Point(555, 29);
			this.cmdFilePrintDateChart.Name = "cmdFilePrintDateChart";
			this.cmdFilePrintDateChart.Size = new System.Drawing.Size(114, 24);
			this.cmdFilePrintDateChart.TabIndex = 2;
			this.cmdFilePrintDateChart.Text = "Print Date Chart";
			this.cmdFilePrintDateChart.Click += new System.EventHandler(this.mnuFilePrintDateChart_Click);
			// 
			// frmUTLienDates
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(697, 610);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUTLienDates";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Lien Dates";
			this.Load += new System.EventHandler(this.frmUTLienDates_Load);
			this.Activated += new System.EventHandler(this.frmUTLienDates_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTLienDates_KeyPress);
			this.Resize += new System.EventHandler(this.frmUTLienDates_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsRate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrintDateChart)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdFilePrintDateChart;
		private FCButton cmdFilePrint;
	}
}
