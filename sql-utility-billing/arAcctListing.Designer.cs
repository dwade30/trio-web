﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arAcctListing.
	/// </summary>
	partial class arAcctListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arAcctListing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldAccountNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTenantName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.srptAccountListingMDetailOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBillToSameAsOwner = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDataEntryMessage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNoBill = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRealEstateAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerMasterAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterMasterAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPhoneNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldUseREAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSerialNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRemoteNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMXUNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReadType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLatitude = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLongitude = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.tUTBookZeroListing = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lbl1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTenantName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillToSameAsOwner)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDataEntryMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoBill)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRealEstateAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerMasterAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterMasterAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterCategory)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhoneNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUseREAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemoteNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMXUNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReadType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLatitude)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLongitude)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccountNum,
				this.fldTenantName,
				this.fldLocation,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.srptAccountListingMDetailOb,
				this.fldBook,
				this.fldBillMessage,
				this.fldBillToSameAsOwner,
				this.fldDataEntryMessage,
				this.fldFinalBill,
				this.fldNoBill,
				this.fldOwnerName,
				this.fldRealEstateAccount,
				this.fldSewerMasterAccount,
				this.fldWaterMasterAccount,
				this.fldSewerCategory,
				this.fldWaterCategory,
				this.fldMapLot,
				this.fldPhoneNumber,
				this.fldUseREAccount,
				this.fldWaterTax,
				this.fldSewerTax,
				this.fldSerialNumber,
				this.fldRemoteNumber,
				this.fldMXUNumber,
				this.fldReadType,
				this.fldLatitude,
				this.fldLongitude
			});
			this.Detail.Height = 5.125F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldAccountNum
			// 
			this.fldAccountNum.Height = 0.1875F;
			this.fldAccountNum.Left = 0F;
			this.fldAccountNum.Name = "fldAccountNum";
			this.fldAccountNum.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldAccountNum.Text = null;
			this.fldAccountNum.Top = 0F;
			this.fldAccountNum.Width = 0.875F;
			// 
			// fldTenantName
			// 
			this.fldTenantName.CanGrow = false;
			this.fldTenantName.Height = 0.1875F;
			this.fldTenantName.Left = 0.875F;
			this.fldTenantName.MultiLine = false;
			this.fldTenantName.Name = "fldTenantName";
			this.fldTenantName.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 0";
			this.fldTenantName.Text = null;
			this.fldTenantName.Top = 0F;
			this.fldTenantName.Width = 3.375F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 4.25F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0F;
			this.fldLocation.Width = 3.75F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 0.875F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldAddress1.Text = null;
			this.fldAddress1.Top = 0.1875F;
			this.fldAddress1.Visible = false;
			this.fldAddress1.Width = 3.125F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 0.875F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldAddress2.Text = null;
			this.fldAddress2.Top = 0.375F;
			this.fldAddress2.Visible = false;
			this.fldAddress2.Width = 3.15625F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 0.875F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldAddress3.Text = null;
			this.fldAddress3.Top = 0.5625F;
			this.fldAddress3.Visible = false;
			this.fldAddress3.Width = 3.15625F;
			// 
			// srptAccountListingMDetailOb
			// 
			this.srptAccountListingMDetailOb.CloseBorder = false;
			this.srptAccountListingMDetailOb.Height = 0.125F;
			this.srptAccountListingMDetailOb.Left = 4F;
			this.srptAccountListingMDetailOb.Name = "srptAccountListingMDetailOb";
			this.srptAccountListingMDetailOb.Report = null;
			this.srptAccountListingMDetailOb.Top = 0.1875F;
			this.srptAccountListingMDetailOb.Width = 4F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0.875F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldBook.Text = null;
			this.fldBook.Top = 0.75F;
			this.fldBook.Visible = false;
			this.fldBook.Width = 3.15625F;
			// 
			// fldBillMessage
			// 
			this.fldBillMessage.Height = 0.1875F;
			this.fldBillMessage.Left = 0.875F;
			this.fldBillMessage.Name = "fldBillMessage";
			this.fldBillMessage.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldBillMessage.Text = null;
			this.fldBillMessage.Top = 0.9375F;
			this.fldBillMessage.Visible = false;
			this.fldBillMessage.Width = 3.15625F;
			// 
			// fldBillToSameAsOwner
			// 
			this.fldBillToSameAsOwner.Height = 0.1875F;
			this.fldBillToSameAsOwner.Left = 0.875F;
			this.fldBillToSameAsOwner.Name = "fldBillToSameAsOwner";
			this.fldBillToSameAsOwner.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldBillToSameAsOwner.Text = null;
			this.fldBillToSameAsOwner.Top = 1.125F;
			this.fldBillToSameAsOwner.Visible = false;
			this.fldBillToSameAsOwner.Width = 3.15625F;
			// 
			// fldDataEntryMessage
			// 
			this.fldDataEntryMessage.Height = 0.1875F;
			this.fldDataEntryMessage.Left = 0.875F;
			this.fldDataEntryMessage.Name = "fldDataEntryMessage";
			this.fldDataEntryMessage.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldDataEntryMessage.Text = null;
			this.fldDataEntryMessage.Top = 1.3125F;
			this.fldDataEntryMessage.Visible = false;
			this.fldDataEntryMessage.Width = 3.15625F;
			// 
			// fldFinalBill
			// 
			this.fldFinalBill.Height = 0.1875F;
			this.fldFinalBill.Left = 0.875F;
			this.fldFinalBill.Name = "fldFinalBill";
			this.fldFinalBill.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldFinalBill.Text = null;
			this.fldFinalBill.Top = 1.5F;
			this.fldFinalBill.Visible = false;
			this.fldFinalBill.Width = 3.15625F;
			// 
			// fldNoBill
			// 
			this.fldNoBill.Height = 0.1875F;
			this.fldNoBill.Left = 0.875F;
			this.fldNoBill.Name = "fldNoBill";
			this.fldNoBill.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldNoBill.Text = null;
			this.fldNoBill.Top = 1.6875F;
			this.fldNoBill.Visible = false;
			this.fldNoBill.Width = 3.15625F;
			// 
			// fldOwnerName
			// 
			this.fldOwnerName.Height = 0.1875F;
			this.fldOwnerName.Left = 0.875F;
			this.fldOwnerName.Name = "fldOwnerName";
			this.fldOwnerName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldOwnerName.Text = null;
			this.fldOwnerName.Top = 1.875F;
			this.fldOwnerName.Visible = false;
			this.fldOwnerName.Width = 3.15625F;
			// 
			// fldRealEstateAccount
			// 
			this.fldRealEstateAccount.Height = 0.1875F;
			this.fldRealEstateAccount.Left = 0.875F;
			this.fldRealEstateAccount.Name = "fldRealEstateAccount";
			this.fldRealEstateAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldRealEstateAccount.Text = null;
			this.fldRealEstateAccount.Top = 2.0625F;
			this.fldRealEstateAccount.Visible = false;
			this.fldRealEstateAccount.Width = 3.15625F;
			// 
			// fldSewerMasterAccount
			// 
			this.fldSewerMasterAccount.Height = 0.1875F;
			this.fldSewerMasterAccount.Left = 0.875F;
			this.fldSewerMasterAccount.Name = "fldSewerMasterAccount";
			this.fldSewerMasterAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldSewerMasterAccount.Text = null;
			this.fldSewerMasterAccount.Top = 2.25F;
			this.fldSewerMasterAccount.Visible = false;
			this.fldSewerMasterAccount.Width = 3.15625F;
			// 
			// fldWaterMasterAccount
			// 
			this.fldWaterMasterAccount.Height = 0.1875F;
			this.fldWaterMasterAccount.Left = 0.875F;
			this.fldWaterMasterAccount.Name = "fldWaterMasterAccount";
			this.fldWaterMasterAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldWaterMasterAccount.Text = null;
			this.fldWaterMasterAccount.Top = 2.4375F;
			this.fldWaterMasterAccount.Visible = false;
			this.fldWaterMasterAccount.Width = 3.15625F;
			// 
			// fldSewerCategory
			// 
			this.fldSewerCategory.Height = 0.1875F;
			this.fldSewerCategory.Left = 0.875F;
			this.fldSewerCategory.Name = "fldSewerCategory";
			this.fldSewerCategory.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldSewerCategory.Text = null;
			this.fldSewerCategory.Top = 2.625F;
			this.fldSewerCategory.Visible = false;
			this.fldSewerCategory.Width = 3.15625F;
			// 
			// fldWaterCategory
			// 
			this.fldWaterCategory.Height = 0.1875F;
			this.fldWaterCategory.Left = 0.875F;
			this.fldWaterCategory.Name = "fldWaterCategory";
			this.fldWaterCategory.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldWaterCategory.Text = null;
			this.fldWaterCategory.Top = 2.8125F;
			this.fldWaterCategory.Visible = false;
			this.fldWaterCategory.Width = 3.15625F;
			// 
			// fldMapLot
			// 
			this.fldMapLot.Height = 0.1875F;
			this.fldMapLot.Left = 0.875F;
			this.fldMapLot.Name = "fldMapLot";
			this.fldMapLot.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMapLot.Text = null;
			this.fldMapLot.Top = 3F;
			this.fldMapLot.Visible = false;
			this.fldMapLot.Width = 3.15625F;
			// 
			// fldPhoneNumber
			// 
			this.fldPhoneNumber.Height = 0.1875F;
			this.fldPhoneNumber.Left = 0.875F;
			this.fldPhoneNumber.Name = "fldPhoneNumber";
			this.fldPhoneNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldPhoneNumber.Text = null;
			this.fldPhoneNumber.Top = 3.1875F;
			this.fldPhoneNumber.Visible = false;
			this.fldPhoneNumber.Width = 3.15625F;
			// 
			// fldUseREAccount
			// 
			this.fldUseREAccount.Height = 0.1875F;
			this.fldUseREAccount.Left = 0.875F;
			this.fldUseREAccount.Name = "fldUseREAccount";
			this.fldUseREAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldUseREAccount.Text = null;
			this.fldUseREAccount.Top = 3.375F;
			this.fldUseREAccount.Visible = false;
			this.fldUseREAccount.Width = 3.15625F;
			// 
			// fldWaterTax
			// 
			this.fldWaterTax.Height = 0.1875F;
			this.fldWaterTax.Left = 0.875F;
			this.fldWaterTax.Name = "fldWaterTax";
			this.fldWaterTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldWaterTax.Text = null;
			this.fldWaterTax.Top = 3.5625F;
			this.fldWaterTax.Visible = false;
			this.fldWaterTax.Width = 3.15625F;
			// 
			// fldSewerTax
			// 
			this.fldSewerTax.Height = 0.1875F;
			this.fldSewerTax.Left = 0.875F;
			this.fldSewerTax.Name = "fldSewerTax";
			this.fldSewerTax.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldSewerTax.Text = null;
			this.fldSewerTax.Top = 3.75F;
			this.fldSewerTax.Visible = false;
			this.fldSewerTax.Width = 3.15625F;
			// 
			// fldSerialNumber
			// 
			this.fldSerialNumber.Height = 0.1875F;
			this.fldSerialNumber.Left = 0.875F;
			this.fldSerialNumber.Name = "fldSerialNumber";
			this.fldSerialNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldSerialNumber.Text = null;
			this.fldSerialNumber.Top = 3.9375F;
			this.fldSerialNumber.Visible = false;
			this.fldSerialNumber.Width = 3.15625F;
			// 
			// fldRemoteNumber
			// 
			this.fldRemoteNumber.Height = 0.1875F;
			this.fldRemoteNumber.Left = 0.875F;
			this.fldRemoteNumber.Name = "fldRemoteNumber";
			this.fldRemoteNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldRemoteNumber.Text = null;
			this.fldRemoteNumber.Top = 4.125F;
			this.fldRemoteNumber.Visible = false;
			this.fldRemoteNumber.Width = 3.15625F;
			// 
			// fldMXUNumber
			// 
			this.fldMXUNumber.Height = 0.1875F;
			this.fldMXUNumber.Left = 0.875F;
			this.fldMXUNumber.Name = "fldMXUNumber";
			this.fldMXUNumber.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldMXUNumber.Text = null;
			this.fldMXUNumber.Top = 4.3125F;
			this.fldMXUNumber.Visible = false;
			this.fldMXUNumber.Width = 3.15625F;
			// 
			// fldReadType
			// 
			this.fldReadType.Height = 0.1875F;
			this.fldReadType.Left = 0.875F;
			this.fldReadType.Name = "fldReadType";
			this.fldReadType.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldReadType.Text = null;
			this.fldReadType.Top = 4.5F;
			this.fldReadType.Visible = false;
			this.fldReadType.Width = 3.15625F;
			// 
			// fldLatitude
			// 
			this.fldLatitude.Height = 0.1875F;
			this.fldLatitude.Left = 0.875F;
			this.fldLatitude.Name = "fldLatitude";
			this.fldLatitude.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldLatitude.Text = null;
			this.fldLatitude.Top = 4.6875F;
			this.fldLatitude.Visible = false;
			this.fldLatitude.Width = 3.15625F;
			// 
			// fldLongitude
			// 
			this.fldLongitude.Height = 0.1875F;
			this.fldLongitude.Left = 0.875F;
			this.fldLongitude.Name = "fldLongitude";
			this.fldLongitude.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldLongitude.Text = null;
			this.fldLongitude.Top = 4.875F;
			this.fldLongitude.Visible = false;
			this.fldLongitude.Width = 3.15625F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooter,
				this.tUTBookZeroListing
			});
			this.ReportFooter.Height = 0.3958333F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.1875F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 0.5625F;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblFooter.Text = null;
			this.lblFooter.Top = 0.0625F;
			this.lblFooter.Width = 6.4375F;
			// 
			// tUTBookZeroListing
			// 
			this.tUTBookZeroListing.CloseBorder = false;
			this.tUTBookZeroListing.Height = 0.125F;
			this.tUTBookZeroListing.Left = 0F;
			this.tUTBookZeroListing.Name = "tUTBookZeroListing";
			this.tUTBookZeroListing.Report = null;
			this.tUTBookZeroListing.Top = 0.25F;
			this.tUTBookZeroListing.Visible = false;
			this.tUTBookZeroListing.Width = 8F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lbl1,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.Label1,
				this.Label2,
				this.Label4,
				this.Line1
			});
			this.PageHeader.Height = 0.6458333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lbl1
			// 
			this.lbl1.Height = 0.25F;
			this.lbl1.HyperLink = null;
			this.lbl1.Left = 0F;
			this.lbl1.Name = "lbl1";
			this.lbl1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.lbl1.Text = "Account Listing";
			this.lbl1.Top = 0F;
			this.lbl1.Width = 8F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.5625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.75F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.25F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6.75F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label1.Text = "Acct";
			this.Label1.Top = 0.4375F;
			this.Label1.Width = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.4375F;
			this.Label2.Width = 3.0625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.25F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label4.Text = "Location";
			this.Label4.Top = 0.4375F;
			this.Label4.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.625F;
			this.Line1.Width = 8F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 8F;
			this.Line1.Y1 = 0.625F;
			this.Line1.Y2 = 0.625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// arAcctListing
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTenantName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBillToSameAsOwner)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDataEntryMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoBill)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRealEstateAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerMasterAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterMasterAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterCategory)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPhoneNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUseREAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemoteNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMXUNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReadType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLatitude)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLongitude)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lbl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTenantName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptAccountListingMDetailOb;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBillToSameAsOwner;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDataEntryMessage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoBill;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRealEstateAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerMasterAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterMasterAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPhoneNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUseREAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSerialNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRemoteNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMXUNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReadType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLatitude;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLongitude;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport tUTBookZeroListing;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
