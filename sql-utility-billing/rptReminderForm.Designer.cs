﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptReminderForm.
	/// </summary>
	partial class rptReminderForm
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptReminderForm));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMailingAddress1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMailingAddress2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMailingAddress3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPreMessage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMailingAddress4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.srptReminderFormDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.lblRtnAddr1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRtnAddr2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRtnAddr3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRtnAddr4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRtnAddr5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblNoticeDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPreMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoticeDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblName,
				this.lblMailingAddress1,
				this.lblMailingAddress2,
				this.lblMailingAddress3,
				this.lblAccount,
				this.lblMessage,
				this.lblHeader,
				this.lblPreMessage,
				this.lblMailingAddress4,
				this.srptReminderFormDetail,
				this.lblRtnAddr1,
				this.lblRtnAddr2,
				this.lblRtnAddr3,
				this.lblRtnAddr4,
				this.lblRtnAddr5,
				this.lblNoticeDate
			});
			this.Detail.Height = 7.041667F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblName
			// 
			this.lblName.Height = 0.16F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Courier New\'";
			this.lblName.Tag = "TEXT";
			this.lblName.Text = " ";
			this.lblName.Top = 1.625F;
			this.lblName.Width = 4F;
			// 
			// lblMailingAddress1
			// 
			this.lblMailingAddress1.Height = 0.16F;
			this.lblMailingAddress1.HyperLink = null;
			this.lblMailingAddress1.Left = 0F;
			this.lblMailingAddress1.Name = "lblMailingAddress1";
			this.lblMailingAddress1.Style = "font-family: \'Courier New\'";
			this.lblMailingAddress1.Tag = "TEXT";
			this.lblMailingAddress1.Text = " ";
			this.lblMailingAddress1.Top = 1.75F;
			this.lblMailingAddress1.Width = 4F;
			// 
			// lblMailingAddress2
			// 
			this.lblMailingAddress2.Height = 0.16F;
			this.lblMailingAddress2.HyperLink = null;
			this.lblMailingAddress2.Left = 0F;
			this.lblMailingAddress2.Name = "lblMailingAddress2";
			this.lblMailingAddress2.Style = "font-family: \'Courier New\'";
			this.lblMailingAddress2.Tag = "TEXT";
			this.lblMailingAddress2.Text = " ";
			this.lblMailingAddress2.Top = 1.875F;
			this.lblMailingAddress2.Width = 4F;
			// 
			// lblMailingAddress3
			// 
			this.lblMailingAddress3.Height = 0.16F;
			this.lblMailingAddress3.HyperLink = null;
			this.lblMailingAddress3.Left = 0F;
			this.lblMailingAddress3.Name = "lblMailingAddress3";
			this.lblMailingAddress3.Style = "font-family: \'Courier New\'";
			this.lblMailingAddress3.Tag = "TEXT";
			this.lblMailingAddress3.Text = " ";
			this.lblMailingAddress3.Top = 2F;
			this.lblMailingAddress3.Width = 4F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.16F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Courier New\'";
			this.lblAccount.Tag = "TEXT";
			this.lblAccount.Text = " ";
			this.lblAccount.Top = 0.5625F;
			this.lblAccount.Width = 4.75F;
			// 
			// lblMessage
			// 
			this.lblMessage.Height = 3.5F;
			this.lblMessage.HyperLink = null;
			this.lblMessage.Left = 0F;
			this.lblMessage.Name = "lblMessage";
			this.lblMessage.Style = "font-family: \'Courier New\'";
			this.lblMessage.Tag = "TEXT";
			this.lblMessage.Text = " ";
			this.lblMessage.Top = 3.5F;
			this.lblMessage.Width = 6.25F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Courier New\'; text-align: center";
			this.lblHeader.Tag = "TEXT";
			this.lblHeader.Text = " ";
			this.lblHeader.Top = 2.5F;
			this.lblHeader.Width = 6.25F;
			// 
			// lblPreMessage
			// 
			this.lblPreMessage.Height = 0.375F;
			this.lblPreMessage.HyperLink = null;
			this.lblPreMessage.Left = 0F;
			this.lblPreMessage.Name = "lblPreMessage";
			this.lblPreMessage.Style = "font-family: \'Courier New\'";
			this.lblPreMessage.Tag = "TEXT";
			this.lblPreMessage.Text = " ";
			this.lblPreMessage.Top = 2.875F;
			this.lblPreMessage.Width = 6.25F;
			// 
			// lblMailingAddress4
			// 
			this.lblMailingAddress4.Height = 0.16F;
			this.lblMailingAddress4.HyperLink = null;
			this.lblMailingAddress4.Left = 0F;
			this.lblMailingAddress4.Name = "lblMailingAddress4";
			this.lblMailingAddress4.Style = "font-family: \'Courier New\'";
			this.lblMailingAddress4.Tag = "TEXT";
			this.lblMailingAddress4.Text = "  ";
			this.lblMailingAddress4.Top = 2.125F;
			this.lblMailingAddress4.Width = 4F;
			// 
			// srptReminderFormDetail
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.srptReminderFormDetail.CloseBorder = false;
			this.srptReminderFormDetail.Height = 0.16F;
			this.srptReminderFormDetail.Left = 0F;
			this.srptReminderFormDetail.Name = "srptReminderFormDetail";
			this.srptReminderFormDetail.Report = null;
			this.srptReminderFormDetail.Top = 3.3125F;
			this.srptReminderFormDetail.Width = 6.25F;
			// 
			// lblRtnAddr1
			// 
			this.lblRtnAddr1.Height = 0.16F;
			this.lblRtnAddr1.HyperLink = null;
			this.lblRtnAddr1.Left = 0F;
			this.lblRtnAddr1.Name = "lblRtnAddr1";
			this.lblRtnAddr1.Style = "font-family: \'Courier New\'";
			this.lblRtnAddr1.Tag = "TEXT";
			this.lblRtnAddr1.Text = " ";
			this.lblRtnAddr1.Top = 0F;
			this.lblRtnAddr1.Width = 4F;
			// 
			// lblRtnAddr2
			// 
			this.lblRtnAddr2.Height = 0.16F;
			this.lblRtnAddr2.HyperLink = null;
			this.lblRtnAddr2.Left = 0F;
			this.lblRtnAddr2.Name = "lblRtnAddr2";
			this.lblRtnAddr2.Style = "font-family: \'Courier New\'";
			this.lblRtnAddr2.Tag = "TEXT";
			this.lblRtnAddr2.Text = " ";
			this.lblRtnAddr2.Top = 0.125F;
			this.lblRtnAddr2.Width = 4F;
			// 
			// lblRtnAddr3
			// 
			this.lblRtnAddr3.Height = 0.16F;
			this.lblRtnAddr3.HyperLink = null;
			this.lblRtnAddr3.Left = 0F;
			this.lblRtnAddr3.Name = "lblRtnAddr3";
			this.lblRtnAddr3.Style = "font-family: \'Courier New\'";
			this.lblRtnAddr3.Tag = "TEXT";
			this.lblRtnAddr3.Text = " ";
			this.lblRtnAddr3.Top = 0.25F;
			this.lblRtnAddr3.Width = 4F;
			// 
			// lblRtnAddr4
			// 
			this.lblRtnAddr4.Height = 0.16F;
			this.lblRtnAddr4.HyperLink = null;
			this.lblRtnAddr4.Left = 0F;
			this.lblRtnAddr4.Name = "lblRtnAddr4";
			this.lblRtnAddr4.Style = "font-family: \'Courier New\'";
			this.lblRtnAddr4.Tag = "TEXT";
			this.lblRtnAddr4.Text = " ";
			this.lblRtnAddr4.Top = 0.375F;
			this.lblRtnAddr4.Width = 4F;
			// 
			// lblRtnAddr5
			// 
			this.lblRtnAddr5.Height = 0.16F;
			this.lblRtnAddr5.HyperLink = null;
			this.lblRtnAddr5.Left = 0F;
			this.lblRtnAddr5.Name = "lblRtnAddr5";
			this.lblRtnAddr5.Style = "font-family: \'Courier New\'";
			this.lblRtnAddr5.Tag = "TEXT";
			this.lblRtnAddr5.Text = " ";
			this.lblRtnAddr5.Top = 0.5F;
			this.lblRtnAddr5.Width = 4F;
			// 
			// lblNoticeDate
			// 
			this.lblNoticeDate.Height = 0.16F;
			this.lblNoticeDate.HyperLink = null;
			this.lblNoticeDate.Left = 0F;
			this.lblNoticeDate.Name = "lblNoticeDate";
			this.lblNoticeDate.Style = "font-family: \'Courier New\'";
			this.lblNoticeDate.Tag = "TEXT";
			this.lblNoticeDate.Text = " ";
			this.lblNoticeDate.Top = 0.8125F;
			this.lblNoticeDate.Width = 4F;
			// 
			// rptReminderForm
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.364583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += this.ActiveReport_Terminate;
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPreMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMailingAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRtnAddr5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblNoticeDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPreMessage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMailingAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptReminderFormDetail;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRtnAddr1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRtnAddr2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRtnAddr3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRtnAddr4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRtnAddr5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblNoticeDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
