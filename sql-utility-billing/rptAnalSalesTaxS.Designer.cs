﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptAnalSalesTaxS.
	/// </summary>
	partial class rptAnalSalesTaxS
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAnalSalesTaxS));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblCat1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCat9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldCat3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCat1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCat2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCat6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCat4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCat5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCat7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCat8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCat9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooterTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotal2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldCat3,
				this.fldCat1,
				this.fldCat2,
				this.fldCat6,
				this.fldTotal,
				this.fldBook,
				this.fldCat4,
				this.fldCat5,
				this.fldCat7,
				this.fldCat8,
				this.fldCat9
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblBook,
				this.lnHeader,
				this.lblCat1,
				this.lblCat2,
				this.lblCat3,
				this.lblTotal,
				this.lblCat6,
				this.lblCat4,
				this.lblCat5,
				this.lblCat7,
				this.lblCat8,
				this.lblCat9,
				this.lblReportType
			});
			this.GroupHeader1.Height = 0.625F;
			this.GroupHeader1.KeepTogether = true;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotal5,
				this.lblFooterTitle,
				this.fldGrandTotal,
				this.lnTotals,
				this.fldTotal2,
				this.fldTotal3,
				this.fldTotal4,
				this.fldTotal6,
				this.fldTotal7,
				this.fldTotal8,
				this.fldTotal9,
				this.fldTotal1
			});
			this.GroupFooter1.Height = 0.4479167F;
			this.GroupFooter1.KeepTogether = true;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Sales Tax Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblBook
			// 
			this.lblBook.Height = 0.1875F;
			this.lblBook.HyperLink = null;
			this.lblBook.Left = 0F;
			this.lblBook.Name = "lblBook";
			this.lblBook.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 0";
			this.lblBook.Text = "Book";
			this.lblBook.Top = 0.4375F;
			this.lblBook.Width = 0.5F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.625F;
			this.lnHeader.Width = 7.5F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 7.5F;
			this.lnHeader.Y1 = 0.625F;
			this.lnHeader.Y2 = 0.625F;
			// 
			// lblCat1
			// 
			this.lblCat1.Height = 0.1875F;
			this.lblCat1.HyperLink = null;
			this.lblCat1.Left = 0.5F;
			this.lblCat1.Name = "lblCat1";
			this.lblCat1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat1.Text = "1";
			this.lblCat1.Top = 0.4375F;
			this.lblCat1.Width = 0.6875F;
			// 
			// lblCat2
			// 
			this.lblCat2.Height = 0.1875F;
			this.lblCat2.HyperLink = null;
			this.lblCat2.Left = 1.1875F;
			this.lblCat2.Name = "lblCat2";
			this.lblCat2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat2.Text = "2";
			this.lblCat2.Top = 0.4375F;
			this.lblCat2.Width = 0.6875F;
			// 
			// lblCat3
			// 
			this.lblCat3.Height = 0.1875F;
			this.lblCat3.HyperLink = null;
			this.lblCat3.Left = 1.875F;
			this.lblCat3.Name = "lblCat3";
			this.lblCat3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat3.Text = "3";
			this.lblCat3.Top = 0.4375F;
			this.lblCat3.Width = 0.6875F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 6.6875F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.4375F;
			this.lblTotal.Width = 0.8125F;
			// 
			// lblCat6
			// 
			this.lblCat6.Height = 0.1875F;
			this.lblCat6.HyperLink = null;
			this.lblCat6.Left = 3.9375F;
			this.lblCat6.Name = "lblCat6";
			this.lblCat6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat6.Text = "6";
			this.lblCat6.Top = 0.4375F;
			this.lblCat6.Width = 0.6875F;
			// 
			// lblCat4
			// 
			this.lblCat4.Height = 0.1875F;
			this.lblCat4.HyperLink = null;
			this.lblCat4.Left = 2.5625F;
			this.lblCat4.Name = "lblCat4";
			this.lblCat4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat4.Text = "4";
			this.lblCat4.Top = 0.4375F;
			this.lblCat4.Width = 0.6875F;
			// 
			// lblCat5
			// 
			this.lblCat5.Height = 0.1875F;
			this.lblCat5.HyperLink = null;
			this.lblCat5.Left = 3.25F;
			this.lblCat5.Name = "lblCat5";
			this.lblCat5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat5.Text = "5";
			this.lblCat5.Top = 0.4375F;
			this.lblCat5.Width = 0.6875F;
			// 
			// lblCat7
			// 
			this.lblCat7.Height = 0.1875F;
			this.lblCat7.HyperLink = null;
			this.lblCat7.Left = 4.625F;
			this.lblCat7.Name = "lblCat7";
			this.lblCat7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat7.Text = "7";
			this.lblCat7.Top = 0.4375F;
			this.lblCat7.Width = 0.6875F;
			// 
			// lblCat8
			// 
			this.lblCat8.Height = 0.1875F;
			this.lblCat8.HyperLink = null;
			this.lblCat8.Left = 5.3125F;
			this.lblCat8.Name = "lblCat8";
			this.lblCat8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat8.Text = "8";
			this.lblCat8.Top = 0.4375F;
			this.lblCat8.Width = 0.6875F;
			// 
			// lblCat9
			// 
			this.lblCat9.Height = 0.1875F;
			this.lblCat9.HyperLink = null;
			this.lblCat9.Left = 6F;
			this.lblCat9.Name = "lblCat9";
			this.lblCat9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCat9.Text = "9";
			this.lblCat9.Top = 0.4375F;
			this.lblCat9.Width = 0.6875F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.1875F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.lblReportType.Text = "-  -  -  -  Sewer  -  -  -  -";
			this.lblReportType.Top = 0.25F;
			this.lblReportType.Width = 7.5F;
			// 
			// fldCat3
			// 
			this.fldCat3.Height = 0.1875F;
			this.fldCat3.Left = 1.875F;
			this.fldCat3.Name = "fldCat3";
			this.fldCat3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat3.Text = null;
			this.fldCat3.Top = 0F;
			this.fldCat3.Width = 0.6875F;
			// 
			// fldCat1
			// 
			this.fldCat1.Height = 0.1875F;
			this.fldCat1.Left = 0.5F;
			this.fldCat1.Name = "fldCat1";
			this.fldCat1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat1.Text = null;
			this.fldCat1.Top = 0F;
			this.fldCat1.Width = 0.6875F;
			// 
			// fldCat2
			// 
			this.fldCat2.Height = 0.1875F;
			this.fldCat2.Left = 1.1875F;
			this.fldCat2.Name = "fldCat2";
			this.fldCat2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat2.Text = null;
			this.fldCat2.Top = 0F;
			this.fldCat2.Width = 0.6875F;
			// 
			// fldCat6
			// 
			this.fldCat6.Height = 0.1875F;
			this.fldCat6.Left = 3.9375F;
			this.fldCat6.Name = "fldCat6";
			this.fldCat6.OutputFormat = resources.GetString("fldCat6.OutputFormat");
			this.fldCat6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat6.Text = null;
			this.fldCat6.Top = 0F;
			this.fldCat6.Width = 0.6875F;
			// 
			// fldTotal
			// 
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.6875F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.OutputFormat = resources.GetString("fldTotal.OutputFormat");
			this.fldTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldTotal.Text = null;
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 0.8125F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldBook.Text = null;
			this.fldBook.Top = 0F;
			this.fldBook.Width = 0.5F;
			// 
			// fldCat4
			// 
			this.fldCat4.Height = 0.1875F;
			this.fldCat4.Left = 2.5625F;
			this.fldCat4.Name = "fldCat4";
			this.fldCat4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat4.Text = null;
			this.fldCat4.Top = 0F;
			this.fldCat4.Width = 0.6875F;
			// 
			// fldCat5
			// 
			this.fldCat5.Height = 0.1875F;
			this.fldCat5.Left = 3.25F;
			this.fldCat5.Name = "fldCat5";
			this.fldCat5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat5.Text = null;
			this.fldCat5.Top = 0F;
			this.fldCat5.Width = 0.6875F;
			// 
			// fldCat7
			// 
			this.fldCat7.Height = 0.1875F;
			this.fldCat7.Left = 4.625F;
			this.fldCat7.Name = "fldCat7";
			this.fldCat7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat7.Text = null;
			this.fldCat7.Top = 0F;
			this.fldCat7.Width = 0.6875F;
			// 
			// fldCat8
			// 
			this.fldCat8.Height = 0.1875F;
			this.fldCat8.Left = 5.3125F;
			this.fldCat8.Name = "fldCat8";
			this.fldCat8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat8.Text = null;
			this.fldCat8.Top = 0F;
			this.fldCat8.Width = 0.6875F;
			// 
			// fldCat9
			// 
			this.fldCat9.Height = 0.1875F;
			this.fldCat9.Left = 6F;
			this.fldCat9.Name = "fldCat9";
			this.fldCat9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCat9.Text = null;
			this.fldCat9.Top = 0F;
			this.fldCat9.Width = 0.6875F;
			// 
			// fldTotal5
			// 
			this.fldTotal5.Height = 0.1875F;
			this.fldTotal5.Left = 3.25F;
			this.fldTotal5.Name = "fldTotal5";
			this.fldTotal5.OutputFormat = resources.GetString("fldTotal5.OutputFormat");
			this.fldTotal5.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal5.Text = null;
			this.fldTotal5.Top = 0.125F;
			this.fldTotal5.Width = 0.6875F;
			// 
			// lblFooterTitle
			// 
			this.lblFooterTitle.Height = 0.1875F;
			this.lblFooterTitle.HyperLink = null;
			this.lblFooterTitle.Left = 0.0625F;
			this.lblFooterTitle.Name = "lblFooterTitle";
			this.lblFooterTitle.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblFooterTitle.Text = "Total:";
			this.lblFooterTitle.Top = 0.125F;
			this.lblFooterTitle.Width = 0.4375F;
			// 
			// fldGrandTotal
			// 
			this.fldGrandTotal.Height = 0.1875F;
			this.fldGrandTotal.Left = 6.5625F;
			this.fldGrandTotal.Name = "fldGrandTotal";
			this.fldGrandTotal.OutputFormat = resources.GetString("fldGrandTotal.OutputFormat");
			this.fldGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldGrandTotal.Text = null;
			this.fldGrandTotal.Top = 0.125F;
			this.fldGrandTotal.Width = 0.9375F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 0.5F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Width = 7F;
			this.lnTotals.X1 = 0.5F;
			this.lnTotals.X2 = 7.5F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// fldTotal2
			// 
			this.fldTotal2.Height = 0.1875F;
			this.fldTotal2.Left = 1.1875F;
			this.fldTotal2.Name = "fldTotal2";
			this.fldTotal2.OutputFormat = resources.GetString("fldTotal2.OutputFormat");
			this.fldTotal2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal2.Text = null;
			this.fldTotal2.Top = 0.125F;
			this.fldTotal2.Width = 0.6875F;
			// 
			// fldTotal3
			// 
			this.fldTotal3.Height = 0.1875F;
			this.fldTotal3.Left = 1.875F;
			this.fldTotal3.Name = "fldTotal3";
			this.fldTotal3.OutputFormat = resources.GetString("fldTotal3.OutputFormat");
			this.fldTotal3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal3.Text = null;
			this.fldTotal3.Top = 0.125F;
			this.fldTotal3.Width = 0.6875F;
			// 
			// fldTotal4
			// 
			this.fldTotal4.Height = 0.1875F;
			this.fldTotal4.Left = 2.5625F;
			this.fldTotal4.Name = "fldTotal4";
			this.fldTotal4.OutputFormat = resources.GetString("fldTotal4.OutputFormat");
			this.fldTotal4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal4.Text = null;
			this.fldTotal4.Top = 0.125F;
			this.fldTotal4.Width = 0.6875F;
			// 
			// fldTotal6
			// 
			this.fldTotal6.Height = 0.1875F;
			this.fldTotal6.Left = 3.9375F;
			this.fldTotal6.Name = "fldTotal6";
			this.fldTotal6.OutputFormat = resources.GetString("fldTotal6.OutputFormat");
			this.fldTotal6.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal6.Text = null;
			this.fldTotal6.Top = 0.125F;
			this.fldTotal6.Width = 0.6875F;
			// 
			// fldTotal7
			// 
			this.fldTotal7.Height = 0.1875F;
			this.fldTotal7.Left = 4.625F;
			this.fldTotal7.Name = "fldTotal7";
			this.fldTotal7.OutputFormat = resources.GetString("fldTotal7.OutputFormat");
			this.fldTotal7.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal7.Text = null;
			this.fldTotal7.Top = 0.125F;
			this.fldTotal7.Width = 0.6875F;
			// 
			// fldTotal8
			// 
			this.fldTotal8.Height = 0.1875F;
			this.fldTotal8.Left = 5.3125F;
			this.fldTotal8.Name = "fldTotal8";
			this.fldTotal8.OutputFormat = resources.GetString("fldTotal8.OutputFormat");
			this.fldTotal8.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal8.Text = null;
			this.fldTotal8.Top = 0.125F;
			this.fldTotal8.Width = 0.6875F;
			// 
			// fldTotal9
			// 
			this.fldTotal9.Height = 0.1875F;
			this.fldTotal9.Left = 6F;
			this.fldTotal9.Name = "fldTotal9";
			this.fldTotal9.OutputFormat = resources.GetString("fldTotal9.OutputFormat");
			this.fldTotal9.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal9.Text = null;
			this.fldTotal9.Top = 0.125F;
			this.fldTotal9.Width = 0.6875F;
			// 
			// fldTotal1
			// 
			this.fldTotal1.Height = 0.1875F;
			this.fldTotal1.Left = 0.5F;
			this.fldTotal1.Name = "fldTotal1";
			this.fldTotal1.OutputFormat = resources.GetString("fldTotal1.OutputFormat");
			this.fldTotal1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldTotal1.Text = null;
			this.fldTotal1.Top = 0.125F;
			this.fldTotal1.Width = 0.6875F;
			// 
			// rptAnalSalesTaxS
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 9.989583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCat9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCat9;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat6;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat4;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat8;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCat9;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal1;
	}
}
