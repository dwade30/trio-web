﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptRateKeyListing.
	/// </summary>
	public partial class rptRateKeyListing : BaseSectionReport
	{
		public rptRateKeyListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Rate Key Listing";
		}

		public static rptRateKeyListing InstancePtr
		{
			get
			{
				return (rptRateKeyListing)Sys.GetInstance(typeof(rptRateKeyListing));
			}
		}

		protected rptRateKeyListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRateKeyListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/15/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/18/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();

		public void Init(string strRateKeys)
		{
			if (strRateKeys == "")
			{
				rsData.OpenRecordset("SELECT * FROM RateKeys ORDER BY BillDate", modExtraModules.strUTDatabase);
			}
			else
			{
				rsData.OpenRecordset("SELECT * FROM RateKeys WHERE RateKey IN (" + strRateKeys + ") ORDER BY BillDate", modExtraModules.strUTDatabase);
			}
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No rate keys found with that criteria.", "No Rate Keys Available", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				this.Close();
				return;
			}
			frmReportViewer.InstancePtr.Init(this);
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			FillHeaderLabels();
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblHeaderW.Text = "Stormwater";
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsData.EndOfFile())
			{
				BindFields();
				rsData.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fldRateKeyNumber.Text = FCConvert.ToString(rsData.Get_Fields("RateKey"));
				fldDescription.Text = FCConvert.ToString(rsData.Get_Fields_String("Description"));
				string vbPorterVar = FCConvert.ToString(rsData.Get_Fields_String("RateType"));
				if (vbPorterVar == "R")
				{
					fldRateType.Text = "Regular";
				}
				else if (vbPorterVar == "L")
				{
					fldRateType.Text = "Lien";
				}
				if (Conversion.Val(rsData.Get_Fields_DateTime("BilLDate")) == 0 || !Information.IsDate(rsData.Get_Fields("BilLDate")))
				{
					fldBillDate.Text = "";
				}
				else
				{
					fldBillDate.Text = Strings.Format(rsData.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
				}
				if (Conversion.Val(rsData.Get_Fields_DateTime("Start")) == 0 || !Information.IsDate(rsData.Get_Fields("Start")))
				{
					fldStart.Text = "";
				}
				else
				{
					fldStart.Text = Strings.Format(rsData.Get_Fields_DateTime("Start"), "MM/dd/yyyy");
				}
				// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
				if (Conversion.Val(rsData.Get_Fields("End")) == 0 || !Information.IsDate(rsData.Get_Fields("End")))
				{
					fldEnd.Text = "";
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
					fldEnd.Text = Strings.Format(rsData.Get_Fields("End"), "MM/dd/yyyy");
				}
				if (Conversion.Val(rsData.Get_Fields_DateTime("IntStart")) == 0 || !Information.IsDate(rsData.Get_Fields("IntStart")))
				{
					fldInt.Text = "";
				}
				else
				{
					fldInt.Text = Strings.Format(rsData.Get_Fields_DateTime("IntStart"), "MM/dd/yyyy");
				}
				fldInterestRate.Text = "W - " + rsData.Get_Fields_Double("WIntRate") + "   S - " + rsData.Get_Fields_Double("SIntRate");
				if (Conversion.Val(rsData.Get_Fields_DateTime("30DNDateW")) == 0 || !Information.IsDate(rsData.Get_Fields("30DNDateW")))
				{
					fld30DNW.Text = "";
				}
				else
				{
					fld30DNW.Text = Strings.Format(rsData.Get_Fields_DateTime("30DNDateW"), "MM/dd/yyyy");
				}
				if (Conversion.Val(rsData.Get_Fields_DateTime("LienDateW")) == 0 || !Information.IsDate(rsData.Get_Fields("LienDateW")))
				{
					fldLienW.Text = "";
				}
				else
				{
					fldLienW.Text = Strings.Format(rsData.Get_Fields_DateTime("LienDateW"), "MM/dd/yyyy");
				}
				if (Conversion.Val(rsData.Get_Fields_DateTime("MaturityDateW")) == 0 || !Information.IsDate(rsData.Get_Fields("MaturityDateW")))
				{
					fldMatW.Text = "";
				}
				else
				{
					fldMatW.Text = Strings.Format(rsData.Get_Fields_DateTime("MaturityDateW"), "MM/dd/yyyy");
				}
				if (Conversion.Val(rsData.Get_Fields_DateTime("30DNDateS")) == 0 || !Information.IsDate(rsData.Get_Fields("30DNDateS")))
				{
					fld30DNS.Text = "";
				}
				else
				{
					fld30DNS.Text = Strings.Format(rsData.Get_Fields_DateTime("30DNDateS"), "MM/dd/yyyy");
				}
				if (Conversion.Val(rsData.Get_Fields_DateTime("LienDateS")) == 0 || !Information.IsDate(rsData.Get_Fields("LienDateS")))
				{
					fldLienS.Text = "";
				}
				else
				{
					fldLienS.Text = Strings.Format(rsData.Get_Fields_DateTime("LienDateS"), "MM/dd/yyyy");
				}
				if (Conversion.Val(rsData.Get_Fields_DateTime("MaturityDateS")) == 0 || !Information.IsDate(rsData.Get_Fields("MaturityDateS")))
				{
					fldMatS.Text = "";
				}
				else
				{
					fldMatS.Text = Strings.Format(rsData.Get_Fields_DateTime("MaturityDateS"), "MM/dd/yyyy");
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Field", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void rptRateKeyListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptRateKeyListing properties;
			//rptRateKeyListing.Caption	= "Rate Key Listing";
			//rptRateKeyListing.Icon	= "rptRateKeyListing.dsx":0000";
			//rptRateKeyListing.Left	= 0;
			//rptRateKeyListing.Top	= 0;
			//rptRateKeyListing.Width	= 11880;
			//rptRateKeyListing.Height	= 8595;
			//rptRateKeyListing.WindowState	= 2;
			//rptRateKeyListing.SectionData	= "rptRateKeyListing.dsx":058A;
			//End Unmaped Properties
		}
	}
}
