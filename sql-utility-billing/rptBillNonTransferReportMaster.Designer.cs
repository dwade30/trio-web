﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptBillNonTransferReportMaster.
	/// </summary>
	partial class rptBillNonTransferReportMaster
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillNonTransferReportMaster));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.srptBookDetail = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.fldFooter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalMisc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblOverride = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverride)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.srptBookDetail});
			this.Detail.Height = 0.063F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// srptBookDetail
			// 
			this.srptBookDetail.CloseBorder = false;
			this.srptBookDetail.Height = 0.0625F;
			this.srptBookDetail.Left = 0F;
			this.srptBookDetail.Name = "srptBookDetail";
			this.srptBookDetail.Report = null;
			this.srptBookDetail.Top = 0F;
			this.srptBookDetail.Width = 10.125F;
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldFooter,
            this.fldTotalAmount,
            this.Line2,
            this.fldTotalCons,
            this.fldTotalRegular,
            this.fldTotalMisc,
            this.fldTotalTax,
            this.fldTotalPastDue,
            this.fldTotalInterest});
			this.ReportFooter.Height = 0.5520833F;
			this.ReportFooter.Name = "ReportFooter";
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			// 
			// fldFooter
			// 
			this.fldFooter.Height = 0.1875F;
			this.fldFooter.Left = 0F;
			this.fldFooter.Name = "fldFooter";
			this.fldFooter.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.fldFooter.Text = null;
			this.fldFooter.Top = 0.25F;
			this.fldFooter.Width = 1F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 6.125F;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalAmount.Text = null;
			this.fldTotalAmount.Top = 0.25F;
			this.fldTotalAmount.Width = 1.3125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.9375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.125F;
			this.Line2.Width = 6.5F;
			this.Line2.X1 = 0.9375F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.125F;
			this.Line2.Y2 = 0.125F;
			// 
			// fldTotalCons
			// 
			this.fldTotalCons.Height = 0.1875F;
			this.fldTotalCons.Left = 0.9375F;
			this.fldTotalCons.Name = "fldTotalCons";
			this.fldTotalCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalCons.Text = null;
			this.fldTotalCons.Top = 0.25F;
			this.fldTotalCons.Width = 0.75F;
			// 
			// fldTotalRegular
			// 
			this.fldTotalRegular.Height = 0.1875F;
			this.fldTotalRegular.Left = 1.6875F;
			this.fldTotalRegular.Name = "fldTotalRegular";
			this.fldTotalRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalRegular.Text = null;
			this.fldTotalRegular.Top = 0.25F;
			this.fldTotalRegular.Width = 0.9375F;
			// 
			// fldTotalMisc
			// 
			this.fldTotalMisc.Height = 0.1875F;
			this.fldTotalMisc.Left = 2.625F;
			this.fldTotalMisc.Name = "fldTotalMisc";
			this.fldTotalMisc.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalMisc.Text = null;
			this.fldTotalMisc.Top = 0.25F;
			this.fldTotalMisc.Width = 0.9375F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 3.5625F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalTax.Text = null;
			this.fldTotalTax.Top = 0.25F;
			this.fldTotalTax.Width = 0.8125F;
			// 
			// fldTotalPastDue
			// 
			this.fldTotalPastDue.Height = 0.1875F;
			this.fldTotalPastDue.Left = 4.375F;
			this.fldTotalPastDue.Name = "fldTotalPastDue";
			this.fldTotalPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalPastDue.Text = null;
			this.fldTotalPastDue.Top = 0.25F;
			this.fldTotalPastDue.Width = 1F;
			// 
			// fldTotalInterest
			// 
			this.fldTotalInterest.Height = 0.1875F;
			this.fldTotalInterest.Left = 5.375F;
			this.fldTotalInterest.Name = "fldTotalInterest";
			this.fldTotalInterest.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalInterest.Text = null;
			this.fldTotalInterest.Top = 0.25F;
			this.fldTotalInterest.Width = 0.875F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblHeader,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPageNumber,
            this.lblReportType});
			this.PageHeader.Height = 0.5625F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
			this.lblHeader.Text = "Accounts Not Billed Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 10F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.5625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 8.0625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.9375F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 8.0625F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.9375F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.3125F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblReportType.Text = null;
			this.lblReportType.Top = 0.25F;
			this.lblReportType.Width = 10F;
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblOverride});
			this.PageFooter.Height = 0.1875F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblOverride
			// 
			this.lblOverride.Height = 0.1875F;
			this.lblOverride.HyperLink = null;
			this.lblOverride.Left = 8.875F;
			this.lblOverride.Name = "lblOverride";
			this.lblOverride.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.lblOverride.Text = "* = Override";
			this.lblOverride.Top = 0F;
			this.lblOverride.Width = 1.125F;
			// 
			// rptBillNonTransferReportMaster
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.125F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverride)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptBookDetail;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMisc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOverride;
	}
}
