﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmLoadPreviousReadingFigures.
	/// </summary>
	partial class frmLoadPreviousReadingFigures : BaseForm
	{
		public fecherFoundation.FCGrid vsMeters;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLoadPreviousReadingFigures));
			this.components = new System.ComponentModel.Container();
			this.vsMeters = new fecherFoundation.FCGrid();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsMeters)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 538);
			this.BottomPanel.Size = new System.Drawing.Size(755, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.vsMeters);
			this.ClientArea.Size = new System.Drawing.Size(755, 478);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(755, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(357, 30);
			this.HeaderText.Text = "Load Previous Reading Figures";
			// 
			// vsMeters
			// 
			this.vsMeters.AllowSelection = false;
			this.vsMeters.AllowUserToResizeColumns = false;
			this.vsMeters.AllowUserToResizeRows = false;
			this.vsMeters.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsMeters.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsMeters.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsMeters.BackColorBkg = System.Drawing.Color.Empty;
			this.vsMeters.BackColorFixed = System.Drawing.Color.Empty;
			this.vsMeters.BackColorSel = System.Drawing.Color.Empty;
			this.vsMeters.Cols = 7;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsMeters.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsMeters.ColumnHeadersHeight = 30;
			this.vsMeters.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsMeters.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsMeters.DragIcon = null;
			this.vsMeters.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsMeters.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsMeters.ExtendLastCol = true;
			this.vsMeters.FixedCols = 0;
			this.vsMeters.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsMeters.FrozenCols = 0;
			this.vsMeters.GridColor = System.Drawing.Color.Empty;
			this.vsMeters.GridColorFixed = System.Drawing.Color.Empty;
			this.vsMeters.Location = new System.Drawing.Point(30, 30);
			this.vsMeters.Name = "vsMeters";
			this.vsMeters.ReadOnly = true;
			this.vsMeters.RowHeadersVisible = false;
			this.vsMeters.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsMeters.RowHeightMin = 0;
			this.vsMeters.Rows = 1;
			this.vsMeters.ScrollTipText = null;
			this.vsMeters.ShowColumnVisibilityMenu = false;
			this.vsMeters.Size = new System.Drawing.Size(695, 434);
			this.vsMeters.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsMeters.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.vsMeters.TabIndex = 0;
			this.vsMeters.KeyPressEdit += new Wisej.Web.KeyPressEventHandler(this.vsMeters_KeyPressEdit);
			this.vsMeters.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsMeters_ValidateEdit);
			this.vsMeters.CurrentCellChanged += new System.EventHandler(this.vsMeters_RowColChange);
			this.vsMeters.Enter += new System.EventHandler(this.vsMeters_Enter);
			this.vsMeters.Click += new System.EventHandler(this.vsMeters_ClickEvent);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(268, 30);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(90, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Save";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmLoadPreviousReadingFigures
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(755, 646);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLoadPreviousReadingFigures";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Load Previous Reading Figures";
			this.Load += new System.EventHandler(this.frmLoadPreviousReadingFigures_Load);
			this.Activated += new System.EventHandler(this.frmLoadPreviousReadingFigures_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLoadPreviousReadingFigures_KeyPress);
			this.Resize += new System.EventHandler(this.frmLoadPreviousReadingFigures_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsMeters)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}
