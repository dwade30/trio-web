﻿//Fecher vbPorter - Version 1.0.0.40
namespace TWUT0000
{
	public class modRhymalReporting
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// Date           :               06/08/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// Last Updated   :               09/13/2004              *
		// ********************************************************
		public struct FreeReportFormat
		{
			// vbPorter upgrade warning: DatabaseFieldName As string	OnWrite(string, int)
			public string DatabaseFieldName;
			// Dynamic Variable - this will be the field name inside the database
			// Question - Not Used
			// Calculated Field - this is the variable name in the scope of the report
			// or blank if just a text field...this will not be processed automatically in any way
			// it is just a way for you to get more info from the user
			public string NameToShowUser;
			// Variable - this will be how the user sees the tag ex. "Owner Name"
			// Question - it is the actual question
			public string Tag;
			// Variable - all UPPERCASE, this is the actual word(s) that will be used as the tag ex.<NAME>
			// Question - Not Used
			public bool Required;
			// Variable - if this is true for any variable, the report will not run until that variable is used
			// Question - if it is true for and question then the user cannot move to the next screen until the question is answered
			public int VariableType;
			// 0 - Static Variable, 1 - Dynamic Variable, 2 - Questions, 3 - Calculated
			public string ComboList;
			// Variable - it is the list of answers in the combo box
			// Question - it is the list of answers in the combo box
			public short Type;
			// This is the type of variable. ex(Date, Text, Number) using the Custom Report
			// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
			public int RowNumber;
			// this will be set when loaded into the grid, all questions and Static Variables will be assigned a row number, this is the row of the answer in the grid
			public string ToolTip;
			// this is what the user will see when they hover over the row
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public FreeReportFormat(int unusedParam)
			{
				this.DatabaseFieldName = string.Empty;
				this.NameToShowUser = string.Empty;
				this.Tag = string.Empty;
				this.Required = false;
				this.VariableType = 0;
				this.ComboList = string.Empty;
				this.Type = 0;
				this.RowNumber = 0;
				this.ToolTip = string.Empty;
			}
		};

		public const int MAXFREEVARIABLES = 40;
		// this will be used to find out which report is being processed and use this in multiple modules
		public class StaticVariables
		{
			// this should be the same or less then the max element number in frfFreeReport array
			public FreeReportFormat[] frfFreeReport = new FreeReportFormat[MAXFREEVARIABLES + 1];
			// this is the array to store them
			public string strFreeReportType = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
