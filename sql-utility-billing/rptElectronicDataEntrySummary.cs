﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptElectronicDataEntrySummary.
	/// </summary>
	public partial class rptElectronicDataEntrySummary : BaseSectionReport
	{
		public rptElectronicDataEntrySummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Electronic Data Entry Summary";
		}

		public static rptElectronicDataEntrySummary InstancePtr
		{
			get
			{
				return (rptElectronicDataEntrySummary)Sys.GetInstance(typeof(rptElectronicDataEntrySummary));
			}
		}

		protected rptElectronicDataEntrySummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptElectronicDataEntrySummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               10/19/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/12/2005              *
		// ********************************************************
		bool blnFirstRecord;
		bool blnNoMatchCompleted;
		bool blnNotUpdatedCompleted;
		bool blnDuplicatesCompleted;
		bool blnShowNoMatch;
		bool blnShowNotUpdated;
		bool blnShowDuplicates;
		int lngRowCounter;

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				blnShowNoMatch = true;
				eArgs.EOF = false;
				this.Fields["Binder"].Value = "1";
			}
			else
			{
				lngRowCounter += 1;
				if (blnNoMatchCompleted == false)
				{
					if (lngRowCounter <= frmElectronicDataEntrySummary.InstancePtr.vsNoMatch.Rows - 1)
					{
						eArgs.EOF = false;
						blnShowNoMatch = true;
					}
					else
					{
						blnNoMatchCompleted = true;
						blnShowNoMatch = false;
						lngRowCounter = 1;
						blnShowNotUpdated = true;
						eArgs.EOF = false;
						this.Fields["Binder"].Value = "2";
					}
				}
				else if (blnNotUpdatedCompleted == false)
				{
					if (lngRowCounter <= frmElectronicDataEntrySummary.InstancePtr.vsNotUpdated.Rows - 1)
					{
						eArgs.EOF = false;
						blnShowNotUpdated = true;
					}
					else
					{
						blnNotUpdatedCompleted = true;
						blnShowNotUpdated = false;
						lngRowCounter = 1;
						blnShowDuplicates = true;
						eArgs.EOF = false;
						this.Fields["Binder"].Value = "3";
					}
				}
				else
				{
					if (lngRowCounter <= frmElectronicDataEntrySummary.InstancePtr.vsDuplicates.Rows - 1)
					{
						eArgs.EOF = false;
						blnShowDuplicates = true;
					}
					else
					{
						blnDuplicatesCompleted = true;
						blnShowDuplicates = false;
						lngRowCounter = 1;
						eArgs.EOF = true;
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			blnNoMatchCompleted = false;
			blnNotUpdatedCompleted = false;
			blnDuplicatesCompleted = false;
			blnShowDuplicates = false;
			blnShowNoMatch = false;
			blnShowNotUpdated = false;
			lngRowCounter = 1;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			int lngAcct = 0;
			if (blnShowNoMatch)
			{
				if (frmElectronicDataEntrySummary.InstancePtr.vsNoMatch.Rows == 1)
				{
					lblNoInformation.Visible = true;
					fldBook.Visible = false;
					fldSequence.Visible = false;
					fldCurrent.Visible = false;
					fldName.Visible = false;
					fldAccount.Visible = false;
				}
				else
				{
					lblNoInformation.Visible = false;
					fldBook.Visible = true;
					fldSequence.Visible = true;
					fldCurrent.Visible = true;
					fldName.Visible = true;
					fldAccount.Visible = true;
					fldBook.Text = frmElectronicDataEntrySummary.InstancePtr.vsNoMatch.TextMatrix(lngRowCounter, 0);
					fldSequence.Text = frmElectronicDataEntrySummary.InstancePtr.vsNoMatch.TextMatrix(lngRowCounter, 1);
					fldCurrent.Text = Strings.Format(frmElectronicDataEntrySummary.InstancePtr.vsNoMatch.TextMatrix(lngRowCounter, 2), "#,##0");
					fldName.Text = GetNameFromBookAndSeq_8(FCConvert.ToInt32(Conversion.Val(fldBook.Text)), FCConvert.ToInt32(Conversion.Val(fldSequence.Text)), ref lngAcct);
					fldAccount.Text = lngAcct.ToString();
				}
			}
			else if (blnShowNotUpdated)
			{
				if (frmElectronicDataEntrySummary.InstancePtr.vsNotUpdated.Rows == 1)
				{
					lblNoInformation.Visible = true;
					fldBook.Visible = false;
					fldSequence.Visible = false;
					fldCurrent.Visible = false;
					fldName.Visible = false;
					fldAccount.Visible = false;
				}
				else
				{
					lblNoInformation.Visible = false;
					fldBook.Visible = true;
					fldSequence.Visible = true;
					fldCurrent.Visible = true;
					fldName.Visible = true;
					fldAccount.Visible = true;
					fldBook.Text = frmElectronicDataEntrySummary.InstancePtr.vsNotUpdated.TextMatrix(lngRowCounter, 0);
					fldSequence.Text = frmElectronicDataEntrySummary.InstancePtr.vsNotUpdated.TextMatrix(lngRowCounter, 1);
					fldCurrent.Text = "";
					fldName.Text = GetNameFromBookAndSeq_8(FCConvert.ToInt32(Conversion.Val(fldBook.Text)), FCConvert.ToInt32(Conversion.Val(fldSequence.Text)), ref lngAcct);
					fldAccount.Text = lngAcct.ToString();
				}
			}
			else
			{
				if (frmElectronicDataEntrySummary.InstancePtr.vsDuplicates.Rows == 1)
				{
					lblNoInformation.Visible = true;
					fldBook.Visible = false;
					fldSequence.Visible = false;
					fldCurrent.Visible = false;
					fldName.Visible = false;
					fldAccount.Visible = false;
				}
				else
				{
					lblNoInformation.Visible = false;
					fldBook.Visible = true;
					fldSequence.Visible = true;
					fldCurrent.Visible = true;
					fldName.Visible = true;
					fldAccount.Visible = true;
					fldBook.Text = frmElectronicDataEntrySummary.InstancePtr.vsDuplicates.TextMatrix(lngRowCounter, 0);
					fldSequence.Text = frmElectronicDataEntrySummary.InstancePtr.vsDuplicates.TextMatrix(lngRowCounter, 1);
					fldCurrent.Text = Strings.Format(frmElectronicDataEntrySummary.InstancePtr.vsDuplicates.TextMatrix(lngRowCounter, 2), "#,##0");
					fldName.Text = frmElectronicDataEntrySummary.InstancePtr.vsDuplicates.TextMatrix(lngRowCounter, 3);
					GetNameFromBookAndSeq_8(FCConvert.ToInt32(Conversion.Val(fldBook.Text)), FCConvert.ToInt32(Conversion.Val(fldSequence.Text)), ref lngAcct);
					fldAccount.Text = lngAcct.ToString();
				}
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			fldTotalRecords.Text = frmElectronicDataEntrySummary.InstancePtr.txtTotalRecords.Text;
			fldNoMatchRecords.Text = frmElectronicDataEntrySummary.InstancePtr.txtNoMatchRecords.Text;
			fldNotUpdatedRecords.Text = frmElectronicDataEntrySummary.InstancePtr.txtRecordsWithDupMeters.Text;
			fldDuplicateRecords.Text = frmElectronicDataEntrySummary.InstancePtr.txtDuplicateReadings.Text;
			fldTotalMeters.Text = frmElectronicDataEntrySummary.InstancePtr.txtTotalMeters.Text;
			fldMetersUpdated.Text = frmElectronicDataEntrySummary.InstancePtr.txtMetersUpdated.Text;
			fldNotUpdatedMeters.Text = frmElectronicDataEntrySummary.InstancePtr.txtNotUpdatedMeters.Text;
			fldDuplicateMeters.Text = frmElectronicDataEntrySummary.InstancePtr.txtDuplicateMeters.Text;
		}

		private void GroupHeader2_Format(object sender, EventArgs e)
		{
			if (this.Fields["Binder"].Value == "1")
			{
				lblType.Text = "No Matches Found";
			}
			else if (this.Fields["Binder"].Value == "2")
			{
				lblType.Text = "Not Updated";
			}
			else
			{
				lblType.Text = "Duplicates";
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}
		// vbPorter upgrade warning: lngBook As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: lngSeq As int	OnWriteFCConvert.ToDouble(
		private string GetNameFromBookAndSeq_8(int lngBook, int lngSeq, ref int lngAcct/* = 0 */)
		{
			return GetNameFromBookAndSeq(ref lngBook, ref lngSeq, ref lngAcct);
		}

		private string GetNameFromBookAndSeq(ref int lngBook, ref int lngSeq, ref int lngAcct/* = 0 */)
		{
			string GetNameFromBookAndSeq = "";
            var rsName = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                
                rsName.OpenRecordset(
                    "SELECT pBill.FullNameLF AS Name, AccountNumber FROM Master INNER JOIN MeterTable ON Master.ID = MeterTable.AccountKey INNER JOIN " +
                    modMain.Statics.strDbCP +
                    "PartyNameView pBill ON pBill.ID = Master.BillingPartyID WHERE BookNumber = " +
                    FCConvert.ToString(lngBook) + " AND Sequence = " + FCConvert.ToString(lngSeq),
                    modExtraModules.strUTDatabase);
                if (!rsName.EndOfFile())
                {
                    GetNameFromBookAndSeq = FCConvert.ToString(rsName.Get_Fields_String("Name"));
                    // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                    lngAcct = FCConvert.ToInt32(rsName.Get_Fields("AccountNumber"));
                }

                return GetNameFromBookAndSeq;
            }
            catch (Exception ex)
            {

                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Finding Name", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				rsName.Dispose();
            }
			return GetNameFromBookAndSeq;
		}

		
	}
}
