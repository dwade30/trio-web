﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for sarUTLienDateDetail.
	/// </summary>
	public partial class sarUTLienDateDetail : FCSectionReport
	{
		public sarUTLienDateDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "sarUTLienDateDetail";
		}

		public static sarUTLienDateDetail InstancePtr
		{
			get
			{
				return (sarUTLienDateDetail)Sys.GetInstance(typeof(sarUTLienDateDetail));
			}
		}

		protected sarUTLienDateDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsRK.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	sarUTLienDateDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/15/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/15/2005              *
		// ********************************************************
		int lngRK;
		int intMaxMonths;
		clsDRWrapper rsRK = new clsDRWrapper();
		DateTime dtFirstMonthDate;
		string strWS;

		private void CreateTimeLine(ref DateTime dtStartDate)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will create fields with the first letter of each month in the field
				string strTemp = "";
				string strM;
				int lngNum;
				// vbPorter upgrade warning: strYear As string	OnWriteFCConvert.ToInt32(
				string strYear;
				int intYearIndex;
				// take the first letter of the month in the start date and put it into the first box
				strM = Strings.Left(Strings.Format(dtStartDate, "MMMM"), 1);
				strYear = FCConvert.ToString(dtStartDate.Year);
				fldYear1.Text = strYear;
				intYearIndex = 2;
				// show the first month field
				fldMonth1.Text = strM;
				fldMonth1.Visible = true;
				// create and show the others
				for (lngNum = 2; lngNum <= intMaxMonths; lngNum++)
				{
					// add a field for a month
					GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldMonth" + FCConvert.ToString(lngNum);
					obNew.Top = fldMonth1.Top;
					obNew.Left = fldMonth1.Left + (lngNum - 1) * fldMonth1.Width;
					obNew.Width = fldMonth1.Width;
					obNew.Height = fldMonth1.Height;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
					strTemp = obNew.Font.ToString();
					obNew.Font = fldMonth1.Font;
					// this sets the font to the same as the field that is already created
					obNew.Border.BottomColor = fldMonth1.Border.BottomColor;
					obNew.Border.BottomStyle = fldMonth1.Border.BottomStyle;
					obNew.Border.LeftColor = fldMonth1.Border.LeftColor;
					obNew.Border.LeftStyle = fldMonth1.Border.LeftStyle;
					obNew.Border.RightColor = fldMonth1.Border.RightColor;
					obNew.Border.RightStyle = fldMonth1.Border.RightStyle;
					obNew.Border.Shadow = fldMonth1.Border.Shadow;
					obNew.Border.TopColor = fldMonth1.Border.TopColor;
					obNew.Border.TopStyle = fldMonth1.Border.TopStyle;
					strM = Strings.Left(Strings.Format(DateAndTime.DateAdd("M", lngNum - 1, dtStartDate), "MMMM"), 1);
					if (FCConvert.ToInt16(FCConvert.ToDouble(strYear)) < DateAndTime.DateAdd("M", lngNum - 1, dtStartDate).Year)
					{
						strYear = FCConvert.ToString(DateAndTime.DateAdd("M", lngNum - 1, dtStartDate).Year);
						switch (intYearIndex)
						{
							case 2:
								{
									fldYear2.Text = strYear;
									if (obNew.Left < fldYear1.Left + fldYear1.Width)
									{
										// fldYear2.Left = obNew.Left * 2
										fldYear1.Visible = false;
									}
									fldYear2.Left = obNew.Left;
									break;
								}
							case 3:
								{
									fldYear3.Text = strYear;
									fldYear3.Left = obNew.Left;
									break;
								}
							case 4:
								{
									fldYear4.Text = strYear;
									fldYear4.Left = obNew.Left;
									break;
								}
						}
						//end switch
						intYearIndex += 1;
					}
					obNew.Text = strM;
					Detail.Controls.Add(obNew);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Months", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intMaxMonths = 36;
			lngRK = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
			strWS = rptUTLienDateLine.InstancePtr.strWS;
			if (rsRK.EndOfFile())
			{
				HideFields();
			}
			else
			{
				//FC:FINAL:MSH - if rsRK.Get_Fields("BillDate") will be equal to an empty string will be throwed an exception (same with i.issue #1002)
				if (FCConvert.ToDateTime(rsRK.Get_Fields_DateTime("BillDate") as object).ToOADate() != DateTime.MinValue.ToOADate())
				{
					// start the month before commitment
					dtFirstMonthDate = DateAndTime.DateAdd("M", -1, (DateTime)rsRK.Get_Fields_DateTime("BillDate"));
				}
				CreateTimeLine(ref dtFirstMonthDate);
				FillDates();
				lblYearHeader.Text = Strings.Format(rsRK.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
				lblDescription.Text = rsRK.Get_Fields_Int32("ID") + " - " + rsRK.Get_Fields_String("Description");
			}
		}

		private void FillDates()
		{
			DateTime dtDate1;
			DateTime dtDate2;
			// vbPorter upgrade warning: dt30DN As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dt30DN;
			// vbPorter upgrade warning: dtLien As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dtLien;
			// vbPorter upgrade warning: dtMat As DateTime	OnWriteFCConvert.ToInt16(
			DateTime dtMat;
			// this routine will fill the 3-4 dates that are relevant to this rate record (if any)
			// and move the dates to match the corresponding time line
			if (!rsRK.IsFieldNull("30DNDate" + strWS))
			{
				dt30DN = (DateTime)rsRK.Get_Fields_DateTime("30DNDate" + strWS);
			}
			else
			{
				dt30DN = DateTime.FromOADate(0);
			}
			if (FCConvert.ToString(rsRK.Get_Fields_DateTime("LienDate" + strWS)) != "")
			{
				dtLien = (DateTime)rsRK.Get_Fields_DateTime("LienDate" + strWS);
			}
			else
			{
				dtLien = DateTime.FromOADate(0);
			}
			if (!rsRK.IsFieldNull("MaturityDate" + strWS))
			{
				dtMat = (DateTime)rsRK.Get_Fields_DateTime("MaturityDate" + strWS);
			}
			else
			{
				dtMat = DateTime.FromOADate(0);
			}
			if (dt30DN.ToOADate() != 0)
			{
				if (dtLien.ToOADate() != 0)
				{
					if (dtMat.ToOADate() != 0)
					{
						// all three dates are filled...show all three in the fields with the dates in them
						ShowDates(dt30DN, "30 Day Notice", dtLien, "Lien Date", dtMat, "Maturity Date");
					}
					else
					{
						// beginning of mat notice
						dtDate1 = DateAndTime.DateAdd("M", 18, dtLien);
						dtDate1 = DateAndTime.DateAdd("D", -45, dtDate1);
						// end of mat notice
						dtDate2 = DateAndTime.DateAdd("D", 15, dtDate1);
						// all but the maturity date is supplied so show
						ShowDates(dt30DN, "30 Day Notice", dtLien, "Lien Date", dtDate1, "Begin Maturity Notice", dtDate2, "End Maturity Notice");
					}
				}
				else
				{
					// Begin Lien Window  30 Days after notice date
					dtDate1 = DateAndTime.DateAdd("d", 30, dt30DN);
					// End Lien Window (10 Days)
					dtDate2 = DateAndTime.DateAdd("yyyy", 1, dt30DN);
					// show the 30 DN and the dates to lien in between
					ShowDates(dt30DN, "30 Day Notice", dtDate1, "Begin Lien Process", dtDate2, "End Lien");
				}
			}
			else
			{
				if (dtLien.ToOADate() != 0)
				{
					// this is a lien record since it is missing the 30DN date
					if (dtMat.ToOADate() != 0)
					{
						// show the lien date and the maturity date
						ShowDates(dtLien, "Lien Date", dtMat, "Maturity Date");
					}
					else
					{
						// beginning of mat notice
						dtDate1 = DateAndTime.DateAdd("M", 18, dtLien);
						dtDate1 = DateAndTime.DateAdd("D", -45, dtDate1);
						// end of mat notice
						dtDate2 = DateAndTime.DateAdd("D", 15, dtDate1);
						// show the lien date and the range of the mat notices
						ShowDates(dtLien, "Lien Date", dtDate1, "Begin Maturity Notice", dtDate2, "End Maturity Notice");
					}
				}
				else
				{
					if (dtMat.ToOADate() != 0)
					{
						if (FCConvert.ToDateTime(rsRK.Get_Fields_DateTime("BillDate") as object).ToOADate() != 0)
						{
							// show the commitment date and the maturity date
							ShowDates(rsRK.Get_Fields_DateTime("BillDate"), "Bill Date", dtMat, "Maturity Date");
						}
						else
						{
							// show just the Maturity Date
							ShowDates(dtMat, "Maturity Date");
						}
					}
					else
					{
						//FC:FINAL:MSH - if rsRK.Get_Fields("BillDate") will be equal to an empty string will be throwed an exception (same with i.issue #1002)
						if (FCConvert.ToDateTime(rsRK.Get_Fields_DateTime("BillDate") as object).ToOADate() != DateTime.MinValue.ToOADate())
						{
							// 30 Day Notice should be send 1 month after billdate
							// dtDate1 = DateAdd("M", 1, rsRK.Fields("BillDate"))
							// dtDate1 = DateAdd("D", 1, dtDate1)
							// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
							dtDate1 = DateAndTime.DateAdd("M", 3, (DateTime)rsRK.Get_Fields("DueDate"));
							dtDate1 = DateAndTime.DateAdd("D", 1, dtDate1);
							// End 30 DN time 1 year after the commitment
							// dtDate2 = DateAdd("yyyy", 1, rsRK.Fields("BillDate"))
							// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
							dtDate2 = DateAndTime.DateAdd("yyyy", 1, (DateTime)rsRK.Get_Fields("DueDate"));
							// show the commitment date and the range of 30DN
							ShowDates(rsRK.Get_Fields_DateTime("BillDate"), "Bill Date", dtDate1, "Begin 30 Day Notice", dtDate2, "End 30 Day Notice");
						}
						else
						{
							// no dates...then hide the fields
							HideFields();
						}
					}
				}
			}
		}

		private void ShowDates(DateTime dtDate1, string strTitle1, DateTime? dtDate2Temp = null, string strTitle2 = "", DateTime? dtDate3Temp = null, string strTitle3 = "", DateTime? dtDate4Temp = null, string strTitle4 = "")
		{
			DateTime dtDate2 = dtDate2Temp ?? DateTime.Now;
			DateTime dtDate3 = dtDate3Temp ?? DateTime.Now;
			DateTime dtDate4 = dtDate4Temp ?? DateTime.Now;
			// this routine will actually show the dates and
			// vbPorter upgrade warning: lngOffset As int	OnWriteFCConvert.ToDouble(
			//int lngOffset = 0;
            float lngOffset = 0;
            // vbPorter upgrade warning: intMonDiff As short, int --> As long
            long intMonDiff;
			if (strTitle1 != "")
			{
				lblDate1.Text = strTitle1 + "\r\n" + Strings.Format(dtDate1, "MM/dd/yyyy");
				intMonDiff = DateAndTime.DateDiff("M", dtFirstMonthDate, dtDate1);
				//lngOffset = FCConvert.ToInt32((intMonDiff * fldMonth1.Width) + fldMonth1.Left + (dtDate1.Day * (fldMonth1.Width / 31)));
                lngOffset = (intMonDiff * fldMonth1.Width) + fldMonth1.Left + (dtDate1.Day * (fldMonth1.Width / 31f));
                ln1.X1 = lngOffset;
				ln1.X2 = lngOffset;
                ln1.BringToFront();
				if (lngOffset - (lblDate1.Width / 2f) < 0)
				{
					lblDate1.Left = 0;
				}
				else if (lngOffset + (lblDate1.Width / 2f) > this.PrintWidth)
				{
					lblDate1.Left = this.PrintWidth - lblDate1.Width;
				}
				else
				{
					lblDate1.Left = lngOffset - (lblDate1.Width / 2f);
				}
			}
			else
			{
				lblDate1.Visible = false;
				ln1.Visible = false;
			}
			if (strTitle2 != "")
			{
				lblDate2.Text = strTitle2 + "\r\n" + Strings.Format(dtDate2, "MM/dd/yyyy");
				intMonDiff = DateAndTime.DateDiff("M", dtFirstMonthDate, dtDate2);
				//lngOffset = FCConvert.ToInt32((intMonDiff * fldMonth1.Width) + fldMonth1.Left + (dtDate2.Day * (fldMonth1.Width / 31)));
                lngOffset = (intMonDiff * fldMonth1.Width) + fldMonth1.Left + (dtDate2.Day * (fldMonth1.Width / 31f));
                ln2.X1 = lngOffset;
				ln2.X2 = lngOffset;
                ln2.BringToFront();
                if (lngOffset - (lblDate2.Width / 2f) < 0)
				{
					lblDate2.Left = 0;
				}
				else if (lngOffset + (lblDate2.Width / 2f) > this.PrintWidth)
				{
					lblDate2.Left = this.PrintWidth - lblDate2.Width;
				}
				else
				{
					lblDate2.Left = lngOffset - (lblDate2.Width / 2f);
				}
			}
			else
			{
				lblDate2.Visible = false;
				ln2.Visible = false;
			}
			if (strTitle3 != "")
			{
				lblDate3.Text = strTitle3 + "\r\n" + Strings.Format(dtDate3, "MM/dd/yyyy");
				intMonDiff = DateAndTime.DateDiff("M", dtFirstMonthDate, dtDate3);
				//lngOffset = FCConvert.ToInt32((intMonDiff * fldMonth1.Width) + fldMonth1.Left + (dtDate3.Day * (fldMonth1.Width / 31)));
                lngOffset = (intMonDiff * fldMonth1.Width) + fldMonth1.Left + (dtDate3.Day * (fldMonth1.Width / 31f));
                ln3.X1 = lngOffset;
				ln3.X2 = lngOffset;
                ln3.BringToFront();
                if (lngOffset - (lblDate3.Width / 2f) < 0)
				{
					lblDate3.Left = 0;
				}
				else if (lngOffset + (lblDate3.Width / 2f) > this.PrintWidth)
				{
					lblDate3.Left = this.PrintWidth - lblDate3.Width;
				}
				else
				{
					lblDate3.Left = lngOffset - (lblDate3.Width / 2f);
				}
			}
			else
			{
				lblDate3.Visible = false;
				ln3.Visible = false;
			}
			if (strTitle4 != "")
			{
				lblDate4.Text = strTitle4 + "\r\n" + Strings.Format(dtDate4, "MM/dd/yyyy");
				intMonDiff = DateAndTime.DateDiff("M", dtFirstMonthDate, dtDate4);
				//lngOffset = FCConvert.ToInt32((intMonDiff * fldMonth1.Width) + fldMonth1.Left + (dtDate4.Day * (fldMonth1.Width / 31)));
                lngOffset = (intMonDiff * fldMonth1.Width) + fldMonth1.Left + (dtDate4.Day * (fldMonth1.Width / 31f));
                ln4.X1 = lngOffset;
				ln4.X2 = lngOffset;
                ln4.BringToFront();
                if (lngOffset - (lblDate4.Width / 2f) < 0)
				{
					lblDate4.Left = 0;
				}
				else if (lngOffset + (lblDate4.Width / 2f) > this.PrintWidth)
				{
					lblDate4.Left = this.PrintWidth - lblDate4.Width;
				}
				else
				{
					lblDate4.Left = lngOffset - (lblDate4.Width / 2f);
				}
			}
			else
			{
				lblDate4.Visible = false;
				ln4.Visible = false;
			}
		}

		private void HideFields()
		{
			// this routine will hide all the fields and close the detail section
			fldMonth1.Visible = false;
			ln1.Visible = false;
			ln2.Visible = false;
			ln3.Visible = false;
			ln4.Visible = false;
			lblDate1.Visible = false;
			lblDate2.Visible = false;
			lblDate3.Visible = false;
			lblDate4.Visible = false;
			lblYearHeader.Visible = false;
			lblDescription.Visible = false;
			Detail.Height = 0;
			ReportHeader.Height = 0;
		}

		private void sarUTLienDateDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//sarUTLienDateDetail properties;
			//sarUTLienDateDetail.Icon	= "sarUTLienDateDetail.dsx":0000";
			//sarUTLienDateDetail.Left	= 0;
			//sarUTLienDateDetail.Top	= 0;
			//sarUTLienDateDetail.Width	= 11880;
			//sarUTLienDateDetail.Height	= 8100;
			//sarUTLienDateDetail.StartUpPosition	= 3;
			//sarUTLienDateDetail.SectionData	= "sarUTLienDateDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
