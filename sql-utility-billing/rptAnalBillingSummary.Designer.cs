﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptAnalBillingSummary.
	/// </summary>
	partial class rptAnalBillingSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptAnalBillingSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblOverride = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBills = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWRegular = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWaterHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWMisc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSRegular = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSewerHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSMisc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOverride = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBills = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcctTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWMisc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSMisc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooterTitle = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblBillsTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWRegularTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWMiscTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWTaxTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSRegularTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSMiscTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTaxTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverride)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewerHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOverride)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBills)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWRegularTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWMiscTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTaxTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSRegularTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSMiscTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTaxTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldWRegular,
				this.fldOverride,
				this.fldBills,
				this.fldWTotal,
				this.fldSTotal,
				this.fldAcctTotal,
				this.fldBook,
				this.fldWMisc,
				this.fldWTax,
				this.fldSRegular,
				this.fldSMisc,
				this.fldSTax
			});
			this.Detail.Height = 0.19F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblBook,
				this.lnHeader,
				this.lblOverride,
				this.lblBills,
				this.lblWRegular,
				this.lblTotal,
				this.lblSTotal,
				this.lblWTotal,
				this.lblWaterHeader,
				this.lblWMisc,
				this.lblWTax,
				this.lblSRegular,
				this.lblSewerHeader,
				this.lblSMisc,
				this.lblSTax
			});
			this.GroupHeader1.Height = 0.625F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldGrandWTotal,
				this.fldGrandSTotal,
				this.lblFooterTitle,
				this.fldGrandTotal,
				this.lnTotals,
				this.lblBillsTotal,
				this.fldWRegularTotal,
				this.fldWMiscTotal,
				this.fldWTaxTotal,
				this.fldSRegularTotal,
				this.fldSMiscTotal,
				this.fldSTaxTotal
			});
			this.GroupFooter1.Height = 0.3125F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Billing Edit Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblBook
			// 
			this.lblBook.Height = 0.1875F;
			this.lblBook.HyperLink = null;
			this.lblBook.Left = 0F;
			this.lblBook.Name = "lblBook";
			this.lblBook.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 0";
			this.lblBook.Text = "Book";
			this.lblBook.Top = 0.4375F;
			this.lblBook.Width = 0.5F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.625F;
			this.lnHeader.Width = 7.5F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 7.5F;
			this.lnHeader.Y1 = 0.625F;
			this.lnHeader.Y2 = 0.625F;
			// 
			// lblOverride
			// 
			this.lblOverride.Height = 0.1875F;
			this.lblOverride.HyperLink = null;
			this.lblOverride.Left = 0.5F;
			this.lblOverride.Name = "lblOverride";
			this.lblOverride.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 0";
			this.lblOverride.Text = "Override";
			this.lblOverride.Top = 0.4375F;
			this.lblOverride.Width = 0.6875F;
			// 
			// lblBills
			// 
			this.lblBills.Height = 0.1875F;
			this.lblBills.HyperLink = null;
			this.lblBills.Left = 1.1875F;
			this.lblBills.Name = "lblBills";
			this.lblBills.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblBills.Text = "Bills";
			this.lblBills.Top = 0.4375F;
			this.lblBills.Width = 0.5F;
			// 
			// lblWRegular
			// 
			this.lblWRegular.Height = 0.1875F;
			this.lblWRegular.HyperLink = null;
			this.lblWRegular.Left = 1.6875F;
			this.lblWRegular.Name = "lblWRegular";
			this.lblWRegular.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblWRegular.Text = "Regular";
			this.lblWRegular.Top = 0.4375F;
			this.lblWRegular.Width = 1F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 8.875F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.4375F;
			this.lblTotal.Width = 1.125F;
			// 
			// lblSTotal
			// 
			this.lblSTotal.Height = 0.1875F;
			this.lblSTotal.HyperLink = null;
			this.lblSTotal.Left = 7.8125F;
			this.lblSTotal.Name = "lblSTotal";
			this.lblSTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblSTotal.Text = "Sewer Total";
			this.lblSTotal.Top = 0.4375F;
			this.lblSTotal.Width = 1.0625F;
			// 
			// lblWTotal
			// 
			this.lblWTotal.Height = 0.1875F;
			this.lblWTotal.HyperLink = null;
			this.lblWTotal.Left = 4.1875F;
			this.lblWTotal.Name = "lblWTotal";
			this.lblWTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblWTotal.Text = "Water Total";
			this.lblWTotal.Top = 0.4375F;
			this.lblWTotal.Width = 1.0625F;
			// 
			// lblWaterHeader
			// 
			this.lblWaterHeader.Height = 0.1875F;
			this.lblWaterHeader.HyperLink = null;
			this.lblWaterHeader.Left = 1.6875F;
			this.lblWaterHeader.Name = "lblWaterHeader";
			this.lblWaterHeader.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.lblWaterHeader.Text = "-  -  -  -  Water  -  -  -  -";
			this.lblWaterHeader.Top = 0.25F;
			this.lblWaterHeader.Width = 3.5625F;
			// 
			// lblWMisc
			// 
			this.lblWMisc.Height = 0.1875F;
			this.lblWMisc.HyperLink = null;
			this.lblWMisc.Left = 2.6875F;
			this.lblWMisc.Name = "lblWMisc";
			this.lblWMisc.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblWMisc.Text = "Misc/Adj";
			this.lblWMisc.Top = 0.4375F;
			this.lblWMisc.Width = 0.75F;
			// 
			// lblWTax
			// 
			this.lblWTax.Height = 0.1875F;
			this.lblWTax.HyperLink = null;
			this.lblWTax.Left = 3.4375F;
			this.lblWTax.Name = "lblWTax";
			this.lblWTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblWTax.Text = "Tax";
			this.lblWTax.Top = 0.4375F;
			this.lblWTax.Width = 0.75F;
			// 
			// lblSRegular
			// 
			this.lblSRegular.Height = 0.1875F;
			this.lblSRegular.HyperLink = null;
			this.lblSRegular.Left = 5.3125F;
			this.lblSRegular.Name = "lblSRegular";
			this.lblSRegular.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblSRegular.Text = "Regular";
			this.lblSRegular.Top = 0.4375F;
			this.lblSRegular.Width = 1F;
			// 
			// lblSewerHeader
			// 
			this.lblSewerHeader.Height = 0.1875F;
			this.lblSewerHeader.HyperLink = null;
			this.lblSewerHeader.Left = 5.3125F;
			this.lblSewerHeader.Name = "lblSewerHeader";
			this.lblSewerHeader.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center";
			this.lblSewerHeader.Text = "-  -  -  -  Sewer  -  -  -  -";
			this.lblSewerHeader.Top = 0.25F;
			this.lblSewerHeader.Width = 3.5625F;
			// 
			// lblSMisc
			// 
			this.lblSMisc.Height = 0.1875F;
			this.lblSMisc.HyperLink = null;
			this.lblSMisc.Left = 6.3125F;
			this.lblSMisc.Name = "lblSMisc";
			this.lblSMisc.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblSMisc.Text = "Misc/Adj";
			this.lblSMisc.Top = 0.4375F;
			this.lblSMisc.Width = 0.75F;
			// 
			// lblSTax
			// 
			this.lblSTax.Height = 0.1875F;
			this.lblSTax.HyperLink = null;
			this.lblSTax.Left = 7.0625F;
			this.lblSTax.Name = "lblSTax";
			this.lblSTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: center; d" + "do-char-set: 0";
			this.lblSTax.Text = "Tax";
			this.lblSTax.Top = 0.4375F;
			this.lblSTax.Width = 0.75F;
			// 
			// fldWRegular
			// 
			this.fldWRegular.Height = 0.1875F;
			this.fldWRegular.Left = 1.4375F;
			this.fldWRegular.Name = "fldWRegular";
			this.fldWRegular.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldWRegular.Text = null;
			this.fldWRegular.Top = 0F;
			this.fldWRegular.Width = 0.6875F;
			// 
			// fldOverride
			// 
			this.fldOverride.Height = 0.1875F;
			this.fldOverride.Left = 0.5F;
			this.fldOverride.Name = "fldOverride";
			this.fldOverride.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldOverride.Text = null;
			this.fldOverride.Top = 0F;
			this.fldOverride.Width = 0.5625F;
			// 
			// fldBills
			// 
			this.fldBills.Height = 0.1875F;
			this.fldBills.Left = 1.0625F;
			this.fldBills.Name = "fldBills";
			this.fldBills.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldBills.Text = null;
			this.fldBills.Top = 0F;
			this.fldBills.Width = 0.375F;
			// 
			// fldWTotal
			// 
			this.fldWTotal.Height = 0.1875F;
			this.fldWTotal.Left = 3.25F;
			this.fldWTotal.Name = "fldWTotal";
			this.fldWTotal.OutputFormat = resources.GetString("fldWTotal.OutputFormat");
			this.fldWTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldWTotal.Text = null;
			this.fldWTotal.Top = 0F;
			this.fldWTotal.Width = 0.8125F;
			// 
			// fldSTotal
			// 
			this.fldSTotal.Height = 0.1875F;
			this.fldSTotal.Left = 5.875F;
			this.fldSTotal.Name = "fldSTotal";
			this.fldSTotal.OutputFormat = resources.GetString("fldSTotal.OutputFormat");
			this.fldSTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldSTotal.Text = null;
			this.fldSTotal.Top = 0F;
			this.fldSTotal.Width = 0.8125F;
			// 
			// fldAcctTotal
			// 
			this.fldAcctTotal.Height = 0.1875F;
			this.fldAcctTotal.Left = 6.6875F;
			this.fldAcctTotal.Name = "fldAcctTotal";
			this.fldAcctTotal.OutputFormat = resources.GetString("fldAcctTotal.OutputFormat");
			this.fldAcctTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldAcctTotal.Text = null;
			this.fldAcctTotal.Top = 0F;
			this.fldAcctTotal.Width = 0.8125F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldBook.Text = null;
			this.fldBook.Top = 0F;
			this.fldBook.Width = 0.5F;
			// 
			// fldWMisc
			// 
			this.fldWMisc.Height = 0.1875F;
			this.fldWMisc.Left = 2.125F;
			this.fldWMisc.Name = "fldWMisc";
			this.fldWMisc.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldWMisc.Text = null;
			this.fldWMisc.Top = 0F;
			this.fldWMisc.Width = 0.5625F;
			// 
			// fldWTax
			// 
			this.fldWTax.Height = 0.1875F;
			this.fldWTax.Left = 2.6875F;
			this.fldWTax.Name = "fldWTax";
			this.fldWTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldWTax.Text = null;
			this.fldWTax.Top = 0F;
			this.fldWTax.Width = 0.5625F;
			// 
			// fldSRegular
			// 
			this.fldSRegular.Height = 0.1875F;
			this.fldSRegular.Left = 4.0625F;
			this.fldSRegular.Name = "fldSRegular";
			this.fldSRegular.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldSRegular.Text = null;
			this.fldSRegular.Top = 0F;
			this.fldSRegular.Width = 0.6875F;
			// 
			// fldSMisc
			// 
			this.fldSMisc.Height = 0.1875F;
			this.fldSMisc.Left = 4.75F;
			this.fldSMisc.Name = "fldSMisc";
			this.fldSMisc.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldSMisc.Text = null;
			this.fldSMisc.Top = 0F;
			this.fldSMisc.Width = 0.5625F;
			// 
			// fldSTax
			// 
			this.fldSTax.Height = 0.1875F;
			this.fldSTax.Left = 5.3125F;
			this.fldSTax.Name = "fldSTax";
			this.fldSTax.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldSTax.Text = null;
			this.fldSTax.Top = 0F;
			this.fldSTax.Width = 0.5625F;
			// 
			// fldGrandWTotal
			// 
			this.fldGrandWTotal.Height = 0.1875F;
			this.fldGrandWTotal.Left = 3.25F;
			this.fldGrandWTotal.Name = "fldGrandWTotal";
			this.fldGrandWTotal.OutputFormat = resources.GetString("fldGrandWTotal.OutputFormat");
			this.fldGrandWTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldGrandWTotal.Text = null;
			this.fldGrandWTotal.Top = 0.125F;
			this.fldGrandWTotal.Width = 0.8125F;
			// 
			// fldGrandSTotal
			// 
			this.fldGrandSTotal.Height = 0.1875F;
			this.fldGrandSTotal.Left = 5.875F;
			this.fldGrandSTotal.Name = "fldGrandSTotal";
			this.fldGrandSTotal.OutputFormat = resources.GetString("fldGrandSTotal.OutputFormat");
			this.fldGrandSTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldGrandSTotal.Text = null;
			this.fldGrandSTotal.Top = 0.125F;
			this.fldGrandSTotal.Width = 0.8125F;
			// 
			// lblFooterTitle
			// 
			this.lblFooterTitle.Height = 0.1875F;
			this.lblFooterTitle.HyperLink = null;
			this.lblFooterTitle.Left = 0.0625F;
			this.lblFooterTitle.Name = "lblFooterTitle";
			this.lblFooterTitle.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblFooterTitle.Text = "Total:";
			this.lblFooterTitle.Top = 0.125F;
			this.lblFooterTitle.Width = 0.875F;
			// 
			// fldGrandTotal
			// 
			this.fldGrandTotal.Height = 0.1875F;
			this.fldGrandTotal.Left = 6.5625F;
			this.fldGrandTotal.Name = "fldGrandTotal";
			this.fldGrandTotal.OutputFormat = resources.GetString("fldGrandTotal.OutputFormat");
			this.fldGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldGrandTotal.Text = null;
			this.fldGrandTotal.Top = 0.125F;
			this.fldGrandTotal.Width = 0.9375F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 1.125F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Width = 6.375F;
			this.lnTotals.X1 = 1.125F;
			this.lnTotals.X2 = 7.5F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// lblBillsTotal
			// 
			this.lblBillsTotal.Height = 0.1875F;
			this.lblBillsTotal.HyperLink = null;
			this.lblBillsTotal.Left = 1.0625F;
			this.lblBillsTotal.Name = "lblBillsTotal";
			this.lblBillsTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblBillsTotal.Text = null;
			this.lblBillsTotal.Top = 0.125F;
			this.lblBillsTotal.Width = 0.375F;
			// 
			// fldWRegularTotal
			// 
			this.fldWRegularTotal.Height = 0.1875F;
			this.fldWRegularTotal.Left = 1.4375F;
			this.fldWRegularTotal.Name = "fldWRegularTotal";
			this.fldWRegularTotal.OutputFormat = resources.GetString("fldWRegularTotal.OutputFormat");
			this.fldWRegularTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldWRegularTotal.Text = null;
			this.fldWRegularTotal.Top = 0.125F;
			this.fldWRegularTotal.Width = 0.6875F;
			// 
			// fldWMiscTotal
			// 
			this.fldWMiscTotal.Height = 0.1875F;
			this.fldWMiscTotal.Left = 2.125F;
			this.fldWMiscTotal.Name = "fldWMiscTotal";
			this.fldWMiscTotal.OutputFormat = resources.GetString("fldWMiscTotal.OutputFormat");
			this.fldWMiscTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldWMiscTotal.Text = null;
			this.fldWMiscTotal.Top = 0.125F;
			this.fldWMiscTotal.Width = 0.5625F;
			// 
			// fldWTaxTotal
			// 
			this.fldWTaxTotal.Height = 0.1875F;
			this.fldWTaxTotal.Left = 2.6875F;
			this.fldWTaxTotal.Name = "fldWTaxTotal";
			this.fldWTaxTotal.OutputFormat = resources.GetString("fldWTaxTotal.OutputFormat");
			this.fldWTaxTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldWTaxTotal.Text = null;
			this.fldWTaxTotal.Top = 0.125F;
			this.fldWTaxTotal.Width = 0.5625F;
			// 
			// fldSRegularTotal
			// 
			this.fldSRegularTotal.Height = 0.1875F;
			this.fldSRegularTotal.Left = 4.0625F;
			this.fldSRegularTotal.Name = "fldSRegularTotal";
			this.fldSRegularTotal.OutputFormat = resources.GetString("fldSRegularTotal.OutputFormat");
			this.fldSRegularTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldSRegularTotal.Text = null;
			this.fldSRegularTotal.Top = 0.125F;
			this.fldSRegularTotal.Width = 0.6875F;
			// 
			// fldSMiscTotal
			// 
			this.fldSMiscTotal.Height = 0.1875F;
			this.fldSMiscTotal.Left = 4.75F;
			this.fldSMiscTotal.Name = "fldSMiscTotal";
			this.fldSMiscTotal.OutputFormat = resources.GetString("fldSMiscTotal.OutputFormat");
			this.fldSMiscTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldSMiscTotal.Text = null;
			this.fldSMiscTotal.Top = 0.125F;
			this.fldSMiscTotal.Width = 0.5625F;
			// 
			// fldSTaxTotal
			// 
			this.fldSTaxTotal.Height = 0.1875F;
			this.fldSTaxTotal.Left = 5.3125F;
			this.fldSTaxTotal.Name = "fldSTaxTotal";
			this.fldSTaxTotal.OutputFormat = resources.GetString("fldSTaxTotal.OutputFormat");
			this.fldSTaxTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldSTaxTotal.Text = null;
			this.fldSTaxTotal.Top = 0.125F;
			this.fldSTaxTotal.Width = 0.5625F;
			// 
			// rptAnalBillingSummary
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverride)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewerHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOverride)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBills)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooterTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBillsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWRegularTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWMiscTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTaxTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSRegularTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSMiscTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTaxTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOverride;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBills;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcctTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWMisc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSMisc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTax;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOverride;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBills;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWRegular;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWaterHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWMisc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSRegular;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSewerHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSMisc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSTax;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooterTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBillsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWRegularTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWMiscTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTaxTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSRegularTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSMiscTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTaxTotal;
	}
}
