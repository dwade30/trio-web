//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmPreviousOwner.
	/// </summary>
	partial class frmPreviousOwner : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCLabel> Label8;
		public fecherFoundation.FCFrame fraPreviousOwners;
		public FCGrid vsGrid;
		public fecherFoundation.FCFrame fraEdit;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtSecOwner;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtCity;
		public fecherFoundation.FCTextBox txtState;
		public fecherFoundation.FCTextBox txtZip;
		public fecherFoundation.FCTextBox txtZip4;
		public fecherFoundation.FCTextBox txtAddress2;
		public Global.T2KDateBox T2KSaleDate;
		public fecherFoundation.FCLabel Label8_0;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCLabel Label9;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPreviousOwner));
			this.fraPreviousOwners = new fecherFoundation.FCFrame();
			this.vsGrid = new fecherFoundation.FCGrid();
			this.fraEdit = new fecherFoundation.FCFrame();
			this.txtName = new fecherFoundation.FCTextBox();
			this.txtSecOwner = new fecherFoundation.FCTextBox();
			this.txtAddress1 = new fecherFoundation.FCTextBox();
			this.txtCity = new fecherFoundation.FCTextBox();
			this.txtState = new fecherFoundation.FCTextBox();
			this.txtZip = new fecherFoundation.FCTextBox();
			this.txtZip4 = new fecherFoundation.FCTextBox();
			this.txtAddress2 = new fecherFoundation.FCTextBox();
			this.T2KSaleDate = new Global.T2KDateBox();
			this.Label8_0 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.Label7 = new fecherFoundation.FCLabel();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.Label9 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSaveExit = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.cmdNew = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPreviousOwners)).BeginInit();
			this.fraPreviousOwners.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraEdit)).BeginInit();
			this.fraEdit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.T2KSaleDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveExit);
			this.BottomPanel.Location = new System.Drawing.Point(0, 540);
			this.BottomPanel.Size = new System.Drawing.Size(836, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraEdit);
			this.ClientArea.Controls.Add(this.lblAccount);
			this.ClientArea.Controls.Add(this.Label9);
			this.ClientArea.Controls.Add(this.fraPreviousOwners);
			this.ClientArea.Size = new System.Drawing.Size(836, 480);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdNew);
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Size = new System.Drawing.Size(836, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(187, 30);
			this.HeaderText.Text = "Previous Owner";
			// 
			// fraPreviousOwners
			// 
			this.fraPreviousOwners.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraPreviousOwners.AppearanceKey = "groupBoxNoBorders";
			this.fraPreviousOwners.Controls.Add(this.vsGrid);
			this.fraPreviousOwners.Location = new System.Drawing.Point(30, 70);
			this.fraPreviousOwners.Name = "fraPreviousOwners";
			this.fraPreviousOwners.Size = new System.Drawing.Size(778, 254);
			this.fraPreviousOwners.TabIndex = 3;
			this.fraPreviousOwners.Text = "Previous Owners";
			this.fraPreviousOwners.Visible = false;
			// 
			// vsGrid
			// 
			this.vsGrid.AllowSelection = false;
			this.vsGrid.AllowUserToResizeColumns = false;
			this.vsGrid.AllowUserToResizeRows = false;
			this.vsGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsGrid.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsGrid.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsGrid.BackColorBkg = System.Drawing.Color.Empty;
			this.vsGrid.BackColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.BackColorSel = System.Drawing.Color.Empty;
			this.vsGrid.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsGrid.Cols = 4;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsGrid.ColumnHeadersHeight = 30;
			this.vsGrid.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsGrid.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsGrid.DragIcon = null;
			this.vsGrid.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsGrid.ExtendLastCol = true;
			this.vsGrid.FixedCols = 0;
			this.vsGrid.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.FrozenCols = 0;
			this.vsGrid.GridColor = System.Drawing.Color.Empty;
			this.vsGrid.GridColorFixed = System.Drawing.Color.Empty;
			this.vsGrid.Location = new System.Drawing.Point(0, 30);
			this.vsGrid.Name = "vsGrid";
			this.vsGrid.ReadOnly = true;
			this.vsGrid.RowHeadersVisible = false;
			this.vsGrid.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsGrid.RowHeightMin = 0;
			this.vsGrid.Rows = 1;
			this.vsGrid.ScrollTipText = null;
			this.vsGrid.ShowColumnVisibilityMenu = false;
			this.vsGrid.Size = new System.Drawing.Size(778, 224);
			this.vsGrid.StandardTab = true;
			this.vsGrid.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsGrid.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsGrid.TabIndex = 0;
			this.vsGrid.DoubleClick += new System.EventHandler(this.vsGrid_DblClick);
			// 
			// fraEdit
			// 
			this.fraEdit.Controls.Add(this.txtName);
			this.fraEdit.Controls.Add(this.txtSecOwner);
			this.fraEdit.Controls.Add(this.txtAddress1);
			this.fraEdit.Controls.Add(this.txtCity);
			this.fraEdit.Controls.Add(this.txtState);
			this.fraEdit.Controls.Add(this.txtZip);
			this.fraEdit.Controls.Add(this.txtZip4);
			this.fraEdit.Controls.Add(this.txtAddress2);
			this.fraEdit.Controls.Add(this.T2KSaleDate);
			this.fraEdit.Controls.Add(this.Label8_0);
			this.fraEdit.Controls.Add(this.Label1);
			this.fraEdit.Controls.Add(this.Label2);
			this.fraEdit.Controls.Add(this.Label3);
			this.fraEdit.Controls.Add(this.Label4);
			this.fraEdit.Controls.Add(this.Label5);
			this.fraEdit.Controls.Add(this.Label6);
			this.fraEdit.Controls.Add(this.Label7);
			this.fraEdit.Location = new System.Drawing.Point(30, 70);
			this.fraEdit.Name = "fraEdit";
			this.fraEdit.Size = new System.Drawing.Size(778, 392);
			this.fraEdit.TabIndex = 2;
			this.fraEdit.Text = "Previous Owner Information";
			// 
			// txtName
			// 
			this.txtName.AutoSize = false;
			this.txtName.BackColor = System.Drawing.SystemColors.Window;
			this.txtName.LinkItem = null;
			this.txtName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName.LinkTopic = null;
			this.txtName.Location = new System.Drawing.Point(163, 30);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(595, 40);
			this.txtName.TabIndex = 1;
			// 
			// txtSecOwner
			// 
			this.txtSecOwner.AutoSize = false;
			this.txtSecOwner.BackColor = System.Drawing.SystemColors.Window;
			this.txtSecOwner.LinkItem = null;
			this.txtSecOwner.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtSecOwner.LinkTopic = null;
			this.txtSecOwner.Location = new System.Drawing.Point(163, 90);
			this.txtSecOwner.Name = "txtSecOwner";
			this.txtSecOwner.Size = new System.Drawing.Size(595, 40);
			this.txtSecOwner.TabIndex = 3;
			// 
			// txtAddress1
			// 
			this.txtAddress1.AutoSize = false;
			this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress1.LinkItem = null;
			this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress1.LinkTopic = null;
			this.txtAddress1.Location = new System.Drawing.Point(163, 150);
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Size = new System.Drawing.Size(595, 40);
			this.txtAddress1.TabIndex = 5;
			// 
			// txtCity
			// 
			this.txtCity.AutoSize = false;
			this.txtCity.BackColor = System.Drawing.SystemColors.Window;
			this.txtCity.LinkItem = null;
			this.txtCity.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtCity.LinkTopic = null;
			this.txtCity.Location = new System.Drawing.Point(163, 270);
			this.txtCity.Name = "txtCity";
			this.txtCity.Size = new System.Drawing.Size(196, 40);
			this.txtCity.TabIndex = 9;
			// 
			// txtState
			// 
			this.txtState.AutoSize = false;
			this.txtState.BackColor = System.Drawing.SystemColors.Window;
			this.txtState.LinkItem = null;
			this.txtState.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtState.LinkTopic = null;
			this.txtState.Location = new System.Drawing.Point(456, 270);
			this.txtState.MaxLength = 2;
			this.txtState.Name = "txtState";
			this.txtState.Size = new System.Drawing.Size(67, 40);
			this.txtState.TabIndex = 11;
			// 
			// txtZip
			// 
			this.txtZip.AutoSize = false;
			this.txtZip.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip.LinkItem = null;
			this.txtZip.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip.LinkTopic = null;
			this.txtZip.Location = new System.Drawing.Point(606, 270);
			this.txtZip.MaxLength = 5;
			this.txtZip.Name = "txtZip";
			this.txtZip.Size = new System.Drawing.Size(77, 40);
			this.txtZip.TabIndex = 13;
			// 
			// txtZip4
			// 
			this.txtZip4.AutoSize = false;
			this.txtZip4.BackColor = System.Drawing.SystemColors.Window;
			this.txtZip4.LinkItem = null;
			this.txtZip4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtZip4.LinkTopic = null;
			this.txtZip4.Location = new System.Drawing.Point(693, 270);
			this.txtZip4.MaxLength = 4;
			this.txtZip4.Name = "txtZip4";
			this.txtZip4.Size = new System.Drawing.Size(65, 40);
			this.txtZip4.TabIndex = 14;
			// 
			// txtAddress2
			// 
			this.txtAddress2.AutoSize = false;
			this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress2.LinkItem = null;
			this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress2.LinkTopic = null;
			this.txtAddress2.Location = new System.Drawing.Point(163, 210);
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Size = new System.Drawing.Size(595, 40);
			this.txtAddress2.TabIndex = 7;
			// 
			// T2KSaleDate
			// 
			this.T2KSaleDate.Location = new System.Drawing.Point(163, 330);
			this.T2KSaleDate.Mask = "##/##/####";
			this.T2KSaleDate.MaxLength = 10;
			this.T2KSaleDate.Name = "T2KSaleDate";
			this.T2KSaleDate.Size = new System.Drawing.Size(115, 40);
			this.T2KSaleDate.TabIndex = 16;
			this.T2KSaleDate.Text = "  /  /";
			// 
			// Label8_0
			// 
			this.Label8_0.Location = new System.Drawing.Point(20, 344);
			this.Label8_0.Name = "Label8_0";
			this.Label8_0.Size = new System.Drawing.Size(76, 17);
			this.Label8_0.TabIndex = 15;
			this.Label8_0.Text = "SALE DATE";
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(76, 17);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "NAME";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(76, 17);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "2ND OWNER";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 164);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(76, 17);
			this.Label3.TabIndex = 4;
			this.Label3.Text = "ADDRESS 1";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 224);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(76, 17);
			this.Label4.TabIndex = 6;
			this.Label4.Text = "ADDRESS 2";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 284);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(76, 17);
			this.Label5.TabIndex = 8;
			this.Label5.Text = "CITY";
			// 
			// Label6
			// 
			this.Label6.Location = new System.Drawing.Point(389, 284);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(37, 17);
			this.Label6.TabIndex = 10;
			this.Label6.Text = "STATE";
			// 
			// Label7
			// 
			this.Label7.Location = new System.Drawing.Point(553, 284);
			this.Label7.Name = "Label7";
			this.Label7.Size = new System.Drawing.Size(23, 17);
			this.Label7.TabIndex = 12;
			this.Label7.Text = "ZIP";
			// 
			// lblAccount
			// 
			this.lblAccount.Location = new System.Drawing.Point(128, 30);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(106, 21);
			this.lblAccount.TabIndex = 1;
			// 
			// Label9
			// 
			this.Label9.Location = new System.Drawing.Point(30, 30);
			this.Label9.Name = "Label9";
			this.Label9.Size = new System.Drawing.Size(67, 21);
			this.Label9.TabIndex = 0;
			this.Label9.Text = "ACCOUNT";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuDelete,
				this.mnuNew,
				this.mnuSepar,
				this.mnuSaveExit,
				this.mnuSepar2,
				this.mnuExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 0;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete this Previous Owner";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuNew
			// 
			this.mnuNew.Index = 1;
			this.mnuNew.Name = "mnuNew";
			this.mnuNew.Text = "New";
			this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// mnuSepar
			// 
			this.mnuSepar.Index = 2;
			this.mnuSepar.Name = "mnuSepar";
			this.mnuSepar.Text = "-";
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 3;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 4;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 5;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSaveExit
			// 
			this.cmdSaveExit.AppearanceKey = "acceptButton";
			this.cmdSaveExit.Location = new System.Drawing.Point(355, 30);
			this.cmdSaveExit.Name = "cmdSaveExit";
			this.cmdSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveExit.Size = new System.Drawing.Size(128, 48);
			this.cmdSaveExit.TabIndex = 0;
			this.cmdSaveExit.Text = "Save & Exit";
			this.cmdSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(610, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(184, 24);
			this.cmdDelete.TabIndex = 1;
			this.cmdDelete.Text = "Delete this previous owner";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// cmdNew
			// 
			this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdNew.AppearanceKey = "toolbarButton";
			this.cmdNew.Location = new System.Drawing.Point(553, 29);
			this.cmdNew.Name = "cmdNew";
			this.cmdNew.Size = new System.Drawing.Size(49, 24);
			this.cmdNew.TabIndex = 2;
			this.cmdNew.Text = "New";
			this.cmdNew.Click += new System.EventHandler(this.mnuNew_Click);
			// 
			// frmPreviousOwner
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(836, 648);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmPreviousOwner";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Previous Owner";
			this.Load += new System.EventHandler(this.frmPreviousOwner_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmPreviousOwner_KeyDown);
			this.Resize += new System.EventHandler(this.frmPreviousOwner_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPreviousOwners)).EndInit();
			this.fraPreviousOwners.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsGrid)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraEdit)).EndInit();
			this.fraEdit.ResumeLayout(false);
			this.fraEdit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.T2KSaleDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveExit)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSaveExit;
		private FCButton cmdDelete;
		private FCButton cmdNew;
	}
}