﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptShortMeterReadingSlip.
	/// </summary>
	public partial class rptShortMeterReadingSlip : BaseSectionReport
	{
		public rptShortMeterReadingSlip()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Meter Reading Slips";
		}

		public static rptShortMeterReadingSlip InstancePtr
		{
			get
			{
				return (rptShortMeterReadingSlip)Sys.GetInstance(typeof(rptShortMeterReadingSlip));
			}
		}

		protected rptShortMeterReadingSlip _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptShortMeterReadingSlip	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               07/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/17/2005              *
		// ********************************************************
		bool blnFirstRecord;
		// is this the first record to be shown in report
		clsDRWrapper rsAccountInfo = new clsDRWrapper();
		// Recordset to hold our report information
		string strOrderBy;
		// What the report should be ordered by
		int intArrayMax;
		// How many books there are in the array
		int[] BookArray = null;
		// Array holding book numbers of selected books
		string strBooksSelection;
		// A - All Books   S - Selected Books
		string strSQL = "";
		// SQL statement to get information we need for report
		string strPrinterToUse;
		// Name of the printer that was chosen
		// vbPorter upgrade warning: intCT As short	OnWriteFCConvert.ToInt32(
		public void Init(string strBooks, ref string strOrder, ref string strPrinter, short intCT, ref int[] PassBookArray)
		{
			int counter;
			string strOrderBySQL = "";
			// Initialize variables to use for report
			strBooksSelection = strBooks;
			strOrderBy = strOrder;
			strPrinterToUse = strPrinter;
			intArrayMax = intCT;
			BookArray = PassBookArray;
			// if all books are to be shown
			if (strBooksSelection == "A")
			{
				// set order by sql statement based on selection from previous screen
				if (strOrderBy == "N")
				{
					strOrderBySQL = " ORDER BY Name";
					// kgk  OwnerName is not in the result set  " ORDER BY OwnerName"
				}
				else if (strOrderBy == "L")
				{
					strOrderBySQL = " ORDER BY StreetName, StreetNumber";
				}
				else
				{
					strOrderBySQL = " ORDER BY AccountNumber";
				}
				// MAL@20080506: Changed to include Previous Reading Date and Amounts
				// Tracker Reference: 13555
				// rsAccountInfo.OpenRecordset "SELECT AccountNumber, Name, StreetName, StreetNumber, Apt, SerialNumber, Remote, Size, Backflow, SetDate, BAddress1, BAddress2, BAddress3, Sequence, BookNumber, CurrentReading, CurrentReadingDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key)" & strOrderBySQL
				// rsAccountInfo.OpenRecordset "SELECT AccountNumber, Name, StreetName, StreetNumber, Apt, SerialNumber, Remote, Size, Backflow, SetDate, BAddress1, BAddress2, BAddress3, Sequence, BookNumber, CurrentReading, CurrentReadingDate, PreviousReading, PreviousReadingDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key)" & strOrderBySQL
				rsAccountInfo.OpenRecordset("SELECT AccountNumber, pBill.FullNameLF AS Name, StreetName, StreetNumber, Apt, SerialNumber, Remote, Size, Backflow, SetDate, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, Sequence, BookNumber, CurrentReading, CurrentReadingDate, PreviousReading, PreviousReadingDate " + "FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID)" + strOrderBySQL);
			}
			else
			{
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					strSQL = " = " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strSQL += FCConvert.ToString(BookArray[counter]) + ",";
					}
					strSQL = "IN (" + Strings.Left(strSQL, strSQL.Length - 1) + ")";
				}
				// set order by clause based on selections from previous screen
				if (strOrderBy == "N")
				{
					strOrderBySQL = " ORDER BY Name";
				}
				else if (strOrderBy == "L")
				{
					strOrderBySQL = " ORDER BY StreetName, StreetNumber";
				}
				else
				{
					strOrderBySQL = " ORDER BY Sequence";
				}
				// MAL@20080506: Changed to include Previous Reading Date and Amounts
				// Tracker Reference: 13555
				// rsAccountInfo.OpenRecordset "SELECT AccountNumber, Name, StreetName, StreetNumber, Apt, SerialNumber, Remote, Size, Backflow, SetDate, BAddress1, BAddress2, BAddress3, Sequence, BookNumber, CurrentReading, CurrentReadingDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL & strOrderBySQL
				// rsAccountInfo.OpenRecordset "SELECT AccountNumber, Name, StreetName, StreetNumber, Apt, SerialNumber, Remote, Size, Backflow, SetDate, BAddress1, BAddress2, BAddress3, Sequence, BookNumber, CurrentReading, CurrentReadingDate, PreviousReading, PreviousReadingDate FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL & strOrderBySQL
				rsAccountInfo.OpenRecordset("SELECT AccountNumber, pBill.FullNameLF AS Name, StreetName, StreetNumber, Apt, SerialNumber, Remote, Size, Backflow, SetDate, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, Sequence, BookNumber, CurrentReading, CurrentReadingDate, PreviousReading, PreviousReadingDate " + "FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID ) WHERE BookNumber " + strSQL + strOrderBySQL);
			}
			// Me.FCGlobal.Printer.DeviceName = strPrinterToUse
			// if we found some records show report otherwise pop up message and end report
			if (rsAccountInfo.EndOfFile() != true && rsAccountInfo.BeginningOfFile() != true)
			{
				// Me.PrintReport False
				frmReportViewer.InstancePtr.Init(this, strPrinterToUse);
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsAccountInfo.MoveNext();
				eArgs.EOF = rsAccountInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intReturn = 0;
			//clsDRWrapper rsAdj = new clsDRWrapper();
			int lngHAdj = 0;
			int lngVAdj;
			string strTemp = "";
			
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat", FCConvert.ToInt32(12240/1440f * 100), FCConvert.ToInt32(12240 / 1440f * 100));

			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "MeterReadingAdjustmentV", ref strTemp);
			lngVAdj = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "MeterReadingAdjustmentH", ref strTemp);
			lngHAdj = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
			this.PageSettings.Margins.Bottom = 0;
			this.PageSettings.Margins.Top = lngVAdj;
			this.PageSettings.Margins.Left = lngHAdj;
			this.PageSettings.Margins.Right = 0;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// MAL@20080506: Add check for book status which determines which date and reading amount to print
			// Tracker Reference: 13555
			// kk 110812 trout-884  Add check for No Read (-1)
			string strStatus = "";
			if (IsBillBookCleared(rsAccountInfo.Get_Fields_Int32("BookNumber"), ref strStatus) || FCConvert.ToInt32(rsAccountInfo.Get_Fields_Int32("CurrentReading")) == -1)
			{
				//FC:FINAL:MSH - incorrect date format (same with internal issue #914). In VB6 we can set value to textBox without calling property "Text", but here we must use it.
				fldLastReadingDate.Text = Strings.Format(rsAccountInfo.Get_Fields_DateTime("PreviousReadingDate"), "MMddyy");
                var previousReading = Strings.Trim(rsAccountInfo.Get_Fields_Int32("PreviousReading").ToString());

                fldDigitOne.Text = previousReading.Length > 0 ? Strings.Right(previousReading, 1) : "";
				fldDigitTwo.Text = previousReading.Length > 1 ? Strings.Mid(previousReading, previousReading.Length - 1, 1) : "";
				fldDigitThree.Text = previousReading.Length > 2 ? Strings.Mid(previousReading, previousReading.Length - 2, 1) : "";
				fldDigitFour.Text = previousReading.Length > 3 ? Strings.Mid(previousReading, previousReading.Length - 3, 1) : "";
				fldDigitFive.Text = previousReading.Length > 4 ? Strings.Mid(previousReading, previousReading.Length - 4, 1) : "";
				fldDigitSix.Text = previousReading.Length > 5 ? Strings.Mid(previousReading, previousReading.Length - 5, 1) : "";
			}
			else
			{
				//FC:FINAL:MSH - incorrect date format (same with internal issue #914). In VB6 we can set value to textBox without calling property "Text", but here we must use it.
				fldLastReadingDate.Text = Strings.Format(rsAccountInfo.Get_Fields_DateTime("CurrentReadingDate"), "MMddyy");
                var currentReading = Strings.Trim(rsAccountInfo.Get_Fields_Int32("CurrentReading").ToString());

                fldDigitOne.Text = currentReading.Length > 0 ? Strings.Right(currentReading, 1) : "";
				fldDigitTwo.Text = currentReading.Length > 1 ? Strings.Mid(currentReading, currentReading.Length - 1, 1) : "";
				fldDigitThree.Text = currentReading.Length > 2 ? Strings.Mid(currentReading, currentReading.Length - 2, 1) : "";
				fldDigitFour.Text = currentReading.Length > 3 ? Strings.Mid(currentReading, currentReading.Length - 3, 1) : "";
				fldDigitFive.Text = currentReading.Length > 4 ? Strings.Mid(currentReading, currentReading.Length - 4, 1) : "";
				fldDigitSix.Text = currentReading.Length > 5 ? Strings.Mid(currentReading, currentReading.Length - 5, 1) : "";
			}
			fldDateSet.Text = rsAccountInfo.Get_Fields_DateTime("SetDate").ToOADate() != 0 ? Strings.Format(rsAccountInfo.Get_Fields_DateTime("SetDate"), "MMddyy") : "";
			fldSerialNumber.Text = rsAccountInfo.Get_Fields_String("SerialNumber");
			fldSize.Text = FCConvert.ToString(rsAccountInfo.Get_Fields_Int32("Size"));
			fldRemoteNumber.Text = rsAccountInfo.Get_Fields_String("Remote");
			fldBackflow.Text = FCConvert.ToBoolean(rsAccountInfo.Get_Fields_Boolean("Backflow")) ? "Y" : "N";
			//FC:FINAL:MSH - in VB6 we can set value to textBox without calling property "Text", but here we must use it.
			fldName.Text = rsAccountInfo.Get_Fields_String("Name");
			fldAddress1.Text = rsAccountInfo.Get_Fields_String("BAddress1");
			fldAddress2.Text = rsAccountInfo.Get_Fields_String("BAddress2");
			fldAddress3.Text = rsAccountInfo.Get_Fields_String("BAddress3");
			fldAccountNumber.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("AccountNumber"));
			fldBookSequence.Text = rsAccountInfo.Get_Fields_Int32("BookNumber") + " / " + rsAccountInfo.Get_Fields("Sequence");
			if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt"))) != "")
			{
				fldLocation.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("StreetNumber")) + " " + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName")));
			}
			else
			{
				fldLocation.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("StreetNumber")) + " " + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName")));
			}
		}

		private bool IsBillBookCleared(int lngBook, ref string strStatus)
		{
			bool IsBillBookCleared = false;
			var rsBook = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                bool blnResult = false;

                rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(lngBook),
                    modExtraModules.strUTDatabase);
                if (rsBook.RecordCount() > 0)
                {
                    blnResult = FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")) == "CE";
                    var currentStatusFirstChar =
                        Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1);

                    switch (currentStatusFirstChar)
                    {
                        case "C":
                            strStatus = "Cleared";

                            break;
                        case "D":
                            strStatus = "Data Entry";

                            break;
                        case "X":
                            strStatus = "Calculated";
                            break;
                        case "E":
                            strStatus = "Calc and Edit";

                            break;
                        case "W":
                            strStatus = "Water Billed";

                            break;
                        case "S":
                            strStatus = "Sewer Billed";

                            break;
                        case "B":
                            strStatus = "Billed";

                            break;
                    }
                }

                IsBillBookCleared = blnResult;
                return IsBillBookCleared;
            }
            catch (Exception ex)
            {

                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Determining Book Status", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				rsBook.Dispose();
            }

			return IsBillBookCleared;
		}

		private void rptShortMeterReadingSlip_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptShortMeterReadingSlip properties;
			//rptShortMeterReadingSlip.Caption	= "Meter Reading Slips";
			//rptShortMeterReadingSlip.Icon	= "rptShortMeterReadingSlip.dsx":0000";
			//rptShortMeterReadingSlip.Left	= 0;
			//rptShortMeterReadingSlip.Top	= 0;
			//rptShortMeterReadingSlip.Width	= 11880;
			//rptShortMeterReadingSlip.Height	= 8595;
			//rptShortMeterReadingSlip.StartUpPosition	= 3;
			//rptShortMeterReadingSlip.SectionData	= "rptShortMeterReadingSlip.dsx":058A;
			//End Unmaped Properties
		}
	}
}
