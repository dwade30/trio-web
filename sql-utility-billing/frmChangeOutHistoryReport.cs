﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmChangeOutHistoryReport.
	/// </summary>
	public partial class frmChangeOutHistoryReport : BaseForm
	{
		public frmChangeOutHistoryReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmChangeOutHistoryReport InstancePtr
		{
			get
			{
				return (frmChangeOutHistoryReport)Sys.GetInstance(typeof(frmChangeOutHistoryReport));
			}
		}

		protected frmChangeOutHistoryReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/17/2006              *
		// ********************************************************
		public DateTime dtStartDate;
		public DateTime dtEndDate;
		public int lngStartAccount;
		public int lngEndAccount;
		public string strStartName = "";
		public string strEndName = "";
		public string strSQL = "";

		private void cmdPreview_Click(object sender, System.EventArgs e)
		{
			strSQL = "";
			// Set Date Range
			if (cmbDate.Text == "Range")
			{
				if (Information.IsDate(txtStartDate.Text) && txtStartDate.Text != "")
				{
					dtStartDate = DateAndTime.DateValue(txtStartDate.Text);
				}
				else
				{
					MessageBox.Show("Please enter a valid Start Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtStartDate.Focus();
					return;
				}
				if (Information.IsDate(txtEndDate.Text) && txtEndDate.Text != "")
				{
					dtEndDate = DateAndTime.DateValue(txtEndDate.Text);
				}
				else
				{
					MessageBox.Show("Please enter a valid End Date.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEndDate.Focus();
					return;
				}
				strSQL = " WHERE SetDate >= '" + FCConvert.ToString(dtStartDate) + "' AND SetDate <= '" + FCConvert.ToString(dtEndDate) + "'";
			}
			// Name Range
			if (cmbName.Text == "Range")
			{
				if (txtStartName.Text != "")
				{
					strStartName = txtStartName.Text;
				}
				else
				{
					MessageBox.Show("Please enter a valid Start Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtStartName.Focus();
					return;
				}
				if (txtEndName.Text != "")
				{
					strEndName = txtEndName.Text;
				}
				else
				{
					MessageBox.Show("Please enter a valid Start Name.", "Invalid Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEndName.Focus();
					return;
				}
				if (strSQL != "")
				{
					strSQL += " AND Name >= '" + strStartName + "    ' AND Name < '" + strEndName + "zzzz'";
				}
				else
				{
					strSQL = " WHERE Name >= '" + strStartName + "    ' AND Name < '" + strEndName + "zzzz'";
				}
			}
			// Account Range
			if (cmbAccount.Text == "Range")
			{
				if (txtStartAccount.Text != "")
				{
					lngStartAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(txtStartAccount.Text)));
				}
				else
				{
					MessageBox.Show("Please enter a valid Start Account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtStartAccount.Focus();
					return;
				}
				if (txtEndAccount.Text != "")
				{
					lngEndAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(txtEndAccount.Text)));
				}
				else
				{
					MessageBox.Show("Please enter a valid End Account.", "Invalid Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEndAccount.Focus();
					return;
				}
				if (strSQL != "")
				{
					strSQL += " AND AccountNumber >= " + FCConvert.ToString(lngStartAccount) + " AND AccountNumber <= " + FCConvert.ToString(lngEndAccount);
				}
				else
				{
					strSQL = " WHERE AccountNumber >= " + FCConvert.ToString(lngStartAccount) + " AND AccountNumber <= " + FCConvert.ToString(lngEndAccount);
				}
			}
			//FC:FINAL:DDU:#i933 - add value before unloading form
			rptMeterChangeOutHistory.InstancePtr.strWhere = strSQL;
			frmReportViewer.InstancePtr.Init(rptMeterChangeOutHistory.InstancePtr);
			mnuProcessQuit_Click();
		}

		public void cmdPreview_Click()
		{
			cmdPreview_Click(cmdPreview, new System.EventArgs());
		}

		private void frmChangeOutHistoryReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						this.Unload();
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmChangeOutHistoryReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmChangeOutHistoryReport properties;
			//frmChangeOutHistoryReport.ScaleWidth	= 10800;
			//frmChangeOutHistoryReport.ScaleHeight	= 8010;
			//frmChangeOutHistoryReport.ClipControls	= false;
			//frmChangeOutHistoryReport.LinkTopic	= "Form1";
			//End Unmaped Properties
			// this will size the form to the MDIParent
			modGlobalFunctions.SetFixedSize(this);
			//FC:FINAL:RPU: Fixed these in designer
			//fraReport.Left = FCConvert.ToInt32((this.Width - fraReport.Width) / 2.0);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmChangeOutHistoryReport_Resize(object sender, System.EventArgs e)
		{
			//FC:FINAL:RPU: Fixed these in designer
			//fraReport.Left = FCConvert.ToInt32((this.Width - fraReport.Width) / 2.0);
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			cmdPreview_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void optAccount_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (cmbAccount.Text == "Range")
			{
				txtStartAccount.Enabled = true;
				txtEndAccount.Enabled = true;
				txtStartAccount.BackColor = Color.White;
				txtEndAccount.BackColor = Color.White;
			}
			else
			{
				txtStartAccount.Enabled = false;
				txtEndAccount.Enabled = false;
				txtStartAccount.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				txtEndAccount.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
		}

		private void optDate_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (cmbDate.Text == "Range")
			{
				txtStartDate.Enabled = true;
				txtEndDate.Enabled = true;
				txtStartDate.BackColor = Color.White;
				txtEndDate.BackColor = Color.White;
			}
			else
			{
				txtStartDate.Enabled = false;
				txtEndDate.Enabled = false;
				txtStartDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				txtEndDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
		}

		private void optName_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			if (cmbName.Text == "Range")
			{
				txtStartName.Enabled = true;
				txtEndName.Enabled = true;
				txtStartName.BackColor = Color.White;
				txtEndName.BackColor = Color.White;
			}
			else
			{
				txtStartName.Enabled = false;
				txtEndName.Enabled = false;
				txtStartName.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				txtEndName.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
		}

		private void cmbDate_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int index = cmbDate.SelectedIndex;
			optDate_CheckedChanged(index, sender, e);
		}

		private void cmbName_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int index = cmbName.SelectedIndex;
			optName_CheckedChanged(index, sender, e);
		}

		private void cmbAccount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int index = cmbAccount.SelectedIndex;
			optAccount_CheckedChanged(index, sender, e);
		}
	}
}
