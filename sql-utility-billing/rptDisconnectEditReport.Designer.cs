﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDisconnectEditReport.
	/// </summary>
	partial class rptDisconnectEditReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDisconnectEditReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSeq = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAcct = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblWater = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSewer = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBooks = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSrvcLoc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSequenceNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWater = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSrvcNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSrvcStreet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookWater = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookSewer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.LineGroup1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label23 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label25 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBookWaterNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookSewerNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookTotalNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBookCountNC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBookCountNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblBookNC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalWater = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalSewer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label24 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.LineGroup2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label26 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalWaterNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalSewerNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFinalTotalNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFinalCountNC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldFinalCountNC = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFinalNC = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBooks)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSrvcLoc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequenceNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSrvcNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSrvcStreet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookWater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookWaterNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSewerNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookTotalNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookCountNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookCountNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalWater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalWaterNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalSewerNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotalNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalCountNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCountNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalNC)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldSequenceNumber,
				this.fldAccountNumber,
				this.fldName,
				this.fldWater,
				this.fldSewer,
				this.fldTotal,
				this.fldSrvcNumber,
				this.fldSrvcStreet
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblSeq,
				this.lblAcct,
				this.lblName,
				this.Line1,
				this.lblWater,
				this.lblSewer,
				this.lblTotal,
				this.lblBooks,
				this.lblDate,
				this.lblSrvcLoc
			});
			this.PageHeader.Height = 0.9375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.Height = 0F;
			this.GroupHeader2.Name = "GroupHeader2";
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldFinalWater,
				this.fldFinalSewer,
				this.fldFinalTotal,
				this.Label24,
				this.fldFinalCount,
				this.LineGroup2,
				this.Label26,
				this.fldFinalWaterNC,
				this.fldFinalSewerNC,
				this.fldFinalTotalNC,
				this.lblFinalCountNC,
				this.fldFinalCountNC,
				this.lblFinalNC
			});
			this.GroupFooter2.Height = 0.3125F;
			this.GroupFooter2.Name = "GroupFooter2";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBook,
				this.Binder
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.3020833F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldBookWater,
				this.fldBookSewer,
				this.fldBookTotal,
				this.LineGroup1,
				this.Label23,
				this.fldTotalCount,
				this.Label25,
				this.fldBookWaterNC,
				this.fldBookSewerNC,
				this.fldBookTotalNC,
				this.lblBookCountNC,
				this.fldBookCountNC,
				this.lblBookNC
			});
			this.GroupFooter1.Height = 0.3229167F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Disconnect Edit Report";
			this.Label1.Top = 0F;
			this.Label1.Width = 4.6875F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblSeq
			// 
			this.lblSeq.Height = 0.1875F;
			this.lblSeq.HyperLink = null;
			this.lblSeq.Left = 0.125F;
			this.lblSeq.Name = "lblSeq";
			this.lblSeq.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.lblSeq.Text = "Seq#";
			this.lblSeq.Top = 0.75F;
			this.lblSeq.Width = 0.4375F;
			// 
			// lblAcct
			// 
			this.lblAcct.Height = 0.1875F;
			this.lblAcct.HyperLink = null;
			this.lblAcct.Left = 0.84375F;
			this.lblAcct.Name = "lblAcct";
			this.lblAcct.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 1";
			this.lblAcct.Text = "Acct #";
			this.lblAcct.Top = 0.75F;
			this.lblAcct.Width = 0.5F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 1.4375F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.75F;
			this.lblName.Width = 0.625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.9375F;
			this.Line1.Width = 7.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.9375F;
			this.Line1.Y2 = 0.9375F;
			// 
			// lblWater
			// 
			this.lblWater.Height = 0.1875F;
			this.lblWater.HyperLink = null;
			this.lblWater.Left = 4.875F;
			this.lblWater.Name = "lblWater";
			this.lblWater.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblWater.Text = "Water";
			this.lblWater.Top = 0.75F;
			this.lblWater.Width = 0.8125F;
			// 
			// lblSewer
			// 
			this.lblSewer.Height = 0.1875F;
			this.lblSewer.HyperLink = null;
			this.lblSewer.Left = 5.75F;
			this.lblSewer.Name = "lblSewer";
			this.lblSewer.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblSewer.Text = "Sewer";
			this.lblSewer.Top = 0.75F;
			this.lblSewer.Width = 0.8125F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 6.625F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 1";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0.75F;
			this.lblTotal.Width = 0.8125F;
			// 
			// lblBooks
			// 
			this.lblBooks.Height = 0.1875F;
			this.lblBooks.HyperLink = null;
			this.lblBooks.Left = 1.5F;
			this.lblBooks.Name = "lblBooks";
			this.lblBooks.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblBooks.Text = "Meter Exchange List";
			this.lblBooks.Top = 0.21875F;
			this.lblBooks.Width = 4.6875F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 1.5F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 1";
			this.lblDate.Text = "Meter Exchange List";
			this.lblDate.Top = 0.46875F;
			this.lblDate.Width = 4.6875F;
			// 
			// lblSrvcLoc
			// 
			this.lblSrvcLoc.Height = 0.1875F;
			this.lblSrvcLoc.HyperLink = null;
			this.lblSrvcLoc.Left = 3.4375F;
			this.lblSrvcLoc.Name = "lblSrvcLoc";
			this.lblSrvcLoc.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 1";
			this.lblSrvcLoc.Text = "Location";
			this.lblSrvcLoc.Top = 0.75F;
			this.lblSrvcLoc.Width = 1.0625F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-weight: bold";
			this.fldBook.Text = "Field1";
			this.fldBook.Top = 0.09375F;
			this.fldBook.Width = 1.34375F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.09375F;
			this.Binder.Left = 2.4375F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.0625F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.71875F;
			// 
			// fldSequenceNumber
			// 
			this.fldSequenceNumber.CanGrow = false;
			this.fldSequenceNumber.Height = 0.1875F;
			this.fldSequenceNumber.Left = 0F;
			this.fldSequenceNumber.Name = "fldSequenceNumber";
			this.fldSequenceNumber.Style = "text-align: center";
			this.fldSequenceNumber.Text = "Field1";
			this.fldSequenceNumber.Top = 0F;
			this.fldSequenceNumber.Width = 0.5625F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.CanGrow = false;
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 0.5625F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "text-align: center";
			this.fldAccountNumber.Text = "Field1";
			this.fldAccountNumber.Top = 0F;
			this.fldAccountNumber.Width = 0.8125F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 1.4375F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "text-align: left";
			this.fldName.Text = "Field1";
			this.fldName.Top = 0F;
			this.fldName.Width = 3.3125F;
			// 
			// fldWater
			// 
			this.fldWater.CanGrow = false;
			this.fldWater.Height = 0.1875F;
			this.fldWater.Left = 4.875F;
			this.fldWater.Name = "fldWater";
			this.fldWater.Style = "text-align: right";
			this.fldWater.Text = "Field1";
			this.fldWater.Top = 0F;
			this.fldWater.Width = 0.8125F;
			// 
			// fldSewer
			// 
			this.fldSewer.CanGrow = false;
			this.fldSewer.Height = 0.1875F;
			this.fldSewer.Left = 5.75F;
			this.fldSewer.Name = "fldSewer";
			this.fldSewer.Style = "text-align: right";
			this.fldSewer.Text = "Field1";
			this.fldSewer.Top = 0F;
			this.fldSewer.Width = 0.8125F;
			// 
			// fldTotal
			// 
			this.fldTotal.CanGrow = false;
			this.fldTotal.Height = 0.1875F;
			this.fldTotal.Left = 6.625F;
			this.fldTotal.Name = "fldTotal";
			this.fldTotal.Style = "text-align: right";
			this.fldTotal.Text = "Field1";
			this.fldTotal.Top = 0F;
			this.fldTotal.Width = 0.8125F;
			// 
			// fldSrvcNumber
			// 
			this.fldSrvcNumber.CanGrow = false;
			this.fldSrvcNumber.Height = 0.1875F;
			this.fldSrvcNumber.Left = 3.125F;
			this.fldSrvcNumber.MultiLine = false;
			this.fldSrvcNumber.Name = "fldSrvcNumber";
			this.fldSrvcNumber.Style = "text-align: left; white-space: nowrap";
			this.fldSrvcNumber.Text = "SrvcNumber";
			this.fldSrvcNumber.Top = 0F;
			this.fldSrvcNumber.Width = 0.5F;
			// 
			// fldSrvcStreet
			// 
			this.fldSrvcStreet.CanGrow = false;
			this.fldSrvcStreet.Height = 0.1875F;
			this.fldSrvcStreet.Left = 3.625F;
			this.fldSrvcStreet.MultiLine = false;
			this.fldSrvcStreet.Name = "fldSrvcStreet";
			this.fldSrvcStreet.Style = "text-align: left; white-space: nowrap";
			this.fldSrvcStreet.Text = "SrvcStreet";
			this.fldSrvcStreet.Top = 0F;
			this.fldSrvcStreet.Width = 1.25F;
			// 
			// fldBookWater
			// 
			this.fldBookWater.Height = 0.1875F;
			this.fldBookWater.Left = 4.875F;
			this.fldBookWater.Name = "fldBookWater";
			this.fldBookWater.Style = "font-weight: bold; text-align: right";
			this.fldBookWater.Text = "Field1";
			this.fldBookWater.Top = 0.0625F;
			this.fldBookWater.Width = 0.8125F;
			// 
			// fldBookSewer
			// 
			this.fldBookSewer.Height = 0.1875F;
			this.fldBookSewer.Left = 5.75F;
			this.fldBookSewer.Name = "fldBookSewer";
			this.fldBookSewer.Style = "font-weight: bold; text-align: right";
			this.fldBookSewer.Text = "Field1";
			this.fldBookSewer.Top = 0.0625F;
			this.fldBookSewer.Width = 0.8125F;
			// 
			// fldBookTotal
			// 
			this.fldBookTotal.Height = 0.1875F;
			this.fldBookTotal.Left = 6.625F;
			this.fldBookTotal.Name = "fldBookTotal";
			this.fldBookTotal.Style = "font-weight: bold; text-align: right";
			this.fldBookTotal.Text = "Field1";
			this.fldBookTotal.Top = 0.0625F;
			this.fldBookTotal.Width = 0.8125F;
			// 
			// LineGroup1
			// 
			this.LineGroup1.Height = 0F;
			this.LineGroup1.Left = 3.375F;
			this.LineGroup1.LineWeight = 1F;
			this.LineGroup1.Name = "LineGroup1";
			this.LineGroup1.Top = 0F;
			this.LineGroup1.Width = 3.6875F;
			this.LineGroup1.X1 = 3.375F;
			this.LineGroup1.X2 = 7.0625F;
			this.LineGroup1.Y1 = 0F;
			this.LineGroup1.Y2 = 0F;
			// 
			// Label23
			// 
			this.Label23.Height = 0.1875F;
			this.Label23.HyperLink = null;
			this.Label23.Left = 2.1875F;
			this.Label23.Name = "Label23";
			this.Label23.Style = "font-weight: bold; text-align: right";
			this.Label23.Text = "Count:";
			this.Label23.Top = 0.0625F;
			this.Label23.Width = 0.5F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 2.75F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-weight: bold";
			this.fldTotalCount.Text = "Field4";
			this.fldTotalCount.Top = 0.0625F;
			this.fldTotalCount.Width = 0.75F;
			// 
			// Label25
			// 
			this.Label25.Height = 0.1875F;
			this.Label25.HyperLink = null;
			this.Label25.Left = 4.3125F;
			this.Label25.Name = "Label25";
			this.Label25.Style = "font-weight: bold; text-align: right";
			this.Label25.Text = "Total:";
			this.Label25.Top = 0.0625F;
			this.Label25.Width = 0.5F;
			// 
			// fldBookWaterNC
			// 
			this.fldBookWaterNC.Height = 0.1875F;
			this.fldBookWaterNC.Left = 4.875F;
			this.fldBookWaterNC.Name = "fldBookWaterNC";
			this.fldBookWaterNC.Style = "font-weight: bold; text-align: right";
			this.fldBookWaterNC.Text = "Field1";
			this.fldBookWaterNC.Top = 0.125F;
			this.fldBookWaterNC.Visible = false;
			this.fldBookWaterNC.Width = 0.8125F;
			// 
			// fldBookSewerNC
			// 
			this.fldBookSewerNC.Height = 0.1875F;
			this.fldBookSewerNC.Left = 5.75F;
			this.fldBookSewerNC.Name = "fldBookSewerNC";
			this.fldBookSewerNC.Style = "font-weight: bold; text-align: right";
			this.fldBookSewerNC.Text = "Field1";
			this.fldBookSewerNC.Top = 0.125F;
			this.fldBookSewerNC.Visible = false;
			this.fldBookSewerNC.Width = 0.8125F;
			// 
			// fldBookTotalNC
			// 
			this.fldBookTotalNC.Height = 0.1875F;
			this.fldBookTotalNC.Left = 6.625F;
			this.fldBookTotalNC.Name = "fldBookTotalNC";
			this.fldBookTotalNC.Style = "font-weight: bold; text-align: right";
			this.fldBookTotalNC.Text = "Field1";
			this.fldBookTotalNC.Top = 0.125F;
			this.fldBookTotalNC.Visible = false;
			this.fldBookTotalNC.Width = 0.8125F;
			// 
			// lblBookCountNC
			// 
			this.lblBookCountNC.Height = 0.1875F;
			this.lblBookCountNC.HyperLink = null;
			this.lblBookCountNC.Left = 0.8125F;
			this.lblBookCountNC.Name = "lblBookCountNC";
			this.lblBookCountNC.Style = "font-weight: bold; text-align: right";
			this.lblBookCountNC.Text = "Excluded Accounts (NC):";
			this.lblBookCountNC.Top = 0.125F;
			this.lblBookCountNC.Visible = false;
			this.lblBookCountNC.Width = 1.875F;
			// 
			// fldBookCountNC
			// 
			this.fldBookCountNC.Height = 0.1875F;
			this.fldBookCountNC.Left = 2.75F;
			this.fldBookCountNC.Name = "fldBookCountNC";
			this.fldBookCountNC.Style = "font-weight: bold";
			this.fldBookCountNC.Text = "Field4";
			this.fldBookCountNC.Top = 0.125F;
			this.fldBookCountNC.Visible = false;
			this.fldBookCountNC.Width = 0.75F;
			// 
			// lblBookNC
			// 
			this.lblBookNC.Height = 0.1875F;
			this.lblBookNC.HyperLink = null;
			this.lblBookNC.Left = 4.3125F;
			this.lblBookNC.Name = "lblBookNC";
			this.lblBookNC.Style = "font-weight: bold; text-align: right";
			this.lblBookNC.Text = null;
			this.lblBookNC.Top = 0.125F;
			this.lblBookNC.Visible = false;
			this.lblBookNC.Width = 0.5F;
			// 
			// fldFinalWater
			// 
			this.fldFinalWater.Height = 0.1875F;
			this.fldFinalWater.Left = 4.875F;
			this.fldFinalWater.Name = "fldFinalWater";
			this.fldFinalWater.Style = "font-weight: bold; text-align: right";
			this.fldFinalWater.Text = "Field1";
			this.fldFinalWater.Top = 0.0625F;
			this.fldFinalWater.Width = 0.8125F;
			// 
			// fldFinalSewer
			// 
			this.fldFinalSewer.Height = 0.1875F;
			this.fldFinalSewer.Left = 5.75F;
			this.fldFinalSewer.Name = "fldFinalSewer";
			this.fldFinalSewer.Style = "font-weight: bold; text-align: right";
			this.fldFinalSewer.Text = "Field1";
			this.fldFinalSewer.Top = 0.0625F;
			this.fldFinalSewer.Width = 0.8125F;
			// 
			// fldFinalTotal
			// 
			this.fldFinalTotal.Height = 0.1875F;
			this.fldFinalTotal.Left = 6.625F;
			this.fldFinalTotal.Name = "fldFinalTotal";
			this.fldFinalTotal.Style = "font-weight: bold; text-align: right";
			this.fldFinalTotal.Text = "Field1";
			this.fldFinalTotal.Top = 0.0625F;
			this.fldFinalTotal.Width = 0.8125F;
			// 
			// Label24
			// 
			this.Label24.Height = 0.1875F;
			this.Label24.HyperLink = null;
			this.Label24.Left = 1.75F;
			this.Label24.Name = "Label24";
			this.Label24.Style = "font-weight: bold; text-align: right";
			this.Label24.Text = "Final Count:";
			this.Label24.Top = 0.0625F;
			this.Label24.Width = 0.9375F;
			// 
			// fldFinalCount
			// 
			this.fldFinalCount.Height = 0.1875F;
			this.fldFinalCount.Left = 2.75F;
			this.fldFinalCount.Name = "fldFinalCount";
			this.fldFinalCount.Style = "font-weight: bold";
			this.fldFinalCount.Text = "Field4";
			this.fldFinalCount.Top = 0.0625F;
			this.fldFinalCount.Width = 0.75F;
			// 
			// LineGroup2
			// 
			this.LineGroup2.Height = 0F;
			this.LineGroup2.Left = 3.375F;
			this.LineGroup2.LineWeight = 1F;
			this.LineGroup2.Name = "LineGroup2";
			this.LineGroup2.Top = 0F;
			this.LineGroup2.Width = 3.6875F;
			this.LineGroup2.X1 = 3.375F;
			this.LineGroup2.X2 = 7.0625F;
			this.LineGroup2.Y1 = 0F;
			this.LineGroup2.Y2 = 0F;
			// 
			// Label26
			// 
			this.Label26.Height = 0.1875F;
			this.Label26.HyperLink = null;
			this.Label26.Left = 3.9375F;
			this.Label26.Name = "Label26";
			this.Label26.Style = "font-weight: bold; text-align: right";
			this.Label26.Text = "Final Total:";
			this.Label26.Top = 0.0625F;
			this.Label26.Width = 0.875F;
			// 
			// fldFinalWaterNC
			// 
			this.fldFinalWaterNC.Height = 0.1875F;
			this.fldFinalWaterNC.Left = 4.875F;
			this.fldFinalWaterNC.Name = "fldFinalWaterNC";
			this.fldFinalWaterNC.Style = "font-weight: bold; text-align: right";
			this.fldFinalWaterNC.Text = "Field1";
			this.fldFinalWaterNC.Top = 0.125F;
			this.fldFinalWaterNC.Visible = false;
			this.fldFinalWaterNC.Width = 0.8125F;
			// 
			// fldFinalSewerNC
			// 
			this.fldFinalSewerNC.Height = 0.1875F;
			this.fldFinalSewerNC.Left = 5.75F;
			this.fldFinalSewerNC.Name = "fldFinalSewerNC";
			this.fldFinalSewerNC.Style = "font-weight: bold; text-align: right";
			this.fldFinalSewerNC.Text = "Field1";
			this.fldFinalSewerNC.Top = 0.125F;
			this.fldFinalSewerNC.Visible = false;
			this.fldFinalSewerNC.Width = 0.8125F;
			// 
			// fldFinalTotalNC
			// 
			this.fldFinalTotalNC.Height = 0.1875F;
			this.fldFinalTotalNC.Left = 6.625F;
			this.fldFinalTotalNC.Name = "fldFinalTotalNC";
			this.fldFinalTotalNC.Style = "font-weight: bold; text-align: right";
			this.fldFinalTotalNC.Text = "Field1";
			this.fldFinalTotalNC.Top = 0.125F;
			this.fldFinalTotalNC.Visible = false;
			this.fldFinalTotalNC.Width = 0.8125F;
			// 
			// lblFinalCountNC
			// 
			this.lblFinalCountNC.Height = 0.1875F;
			this.lblFinalCountNC.HyperLink = null;
			this.lblFinalCountNC.Left = 0.5F;
			this.lblFinalCountNC.Name = "lblFinalCountNC";
			this.lblFinalCountNC.Style = "font-weight: bold; text-align: right";
			this.lblFinalCountNC.Text = "Excluded Accounts (NC):";
			this.lblFinalCountNC.Top = 0.125F;
			this.lblFinalCountNC.Visible = false;
			this.lblFinalCountNC.Width = 2.1875F;
			// 
			// fldFinalCountNC
			// 
			this.fldFinalCountNC.Height = 0.1875F;
			this.fldFinalCountNC.Left = 2.75F;
			this.fldFinalCountNC.Name = "fldFinalCountNC";
			this.fldFinalCountNC.Style = "font-weight: bold";
			this.fldFinalCountNC.Text = "Field4";
			this.fldFinalCountNC.Top = 0.125F;
			this.fldFinalCountNC.Visible = false;
			this.fldFinalCountNC.Width = 0.75F;
			// 
			// lblFinalNC
			// 
			this.lblFinalNC.Height = 0.1875F;
			this.lblFinalNC.HyperLink = null;
			this.lblFinalNC.Left = 3.9375F;
			this.lblFinalNC.Name = "lblFinalNC";
			this.lblFinalNC.Style = "font-weight: bold; text-align: right";
			this.lblFinalNC.Text = null;
			this.lblFinalNC.Top = 0.125F;
			this.lblFinalNC.Visible = false;
			this.lblFinalNC.Width = 0.875F;
			// 
			// rptDisconnectEditReport
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBooks)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSrvcLoc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequenceNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSrvcNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSrvcStreet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookWater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookWaterNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSewerNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookTotalNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookCountNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookCountNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalWater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalWaterNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalSewerNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalTotalNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalCountNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFinalCountNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFinalNC)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSequenceNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWater;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSrvcNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSrvcStreet;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSeq;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAcct;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWater;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSewer;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBooks;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSrvcLoc;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalWater;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalSewer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Line LineGroup2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalWaterNC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalSewerNC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalTotalNC;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFinalCountNC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFinalCountNC;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFinalNC;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookWater;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSewer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line LineGroup1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookWaterNC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSewerNC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookTotalNC;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBookCountNC;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookCountNC;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBookNC;
	}
}
