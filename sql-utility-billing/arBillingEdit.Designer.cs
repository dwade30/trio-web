﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arBillingEdit.
	/// </summary>
	partial class arBillingEdit
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arBillingEdit));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblSeq = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnBookHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblPrev = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCons = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeq = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLoc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPrev = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCur = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAcctTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.srptCalculationDetailOb = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.lblWarning = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldMapLot = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnWarning = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lnBookTotal = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblBookTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldBookWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblOverride = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandWTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldGrandSTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblReportTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldGrandTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.srptSummary = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.srptSummary2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrev)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLoc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrev)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCur)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarning)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverride)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandWTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandSTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcct,
				this.fldName,
				this.fldSeq,
				this.fldCons,
				this.fldLoc,
				this.fldPrev,
				this.fldCur,
				this.fldWTotal,
				this.fldSTotal,
				this.fldAcctTotal,
				this.srptCalculationDetailOb,
				this.lblWarning,
				this.fldMapLot,
				this.lnWarning
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldGrandWTotal,
				this.fldGrandSTotal,
				this.lblReportTotal,
				this.fldGrandTotal,
				this.lnTotals,
				this.srptSummary,
				this.srptSummary2
			});
			this.ReportFooter.Height = 0.625F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber
			});
			this.PageHeader.Height = 0.375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblOverride
			});
			this.PageFooter.Height = 0.21875F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Format += new System.EventHandler(this.GroupHeader1_Format);
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblBook,
				this.fldBook,
				this.lblSeq,
				this.lblAccountNumber,
				this.lblName,
				this.lblLocation,
				this.lnBookHeader,
				this.lblPrev,
				this.lblCurrent,
				this.lblCons,
				this.lblTotal,
				this.lblSTotal,
				this.lblWTotal
			});
			this.GroupHeader1.DataField = "Binder";
			this.GroupHeader1.Height = 0.3541667F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lnBookTotal,
				this.lblBookTotal,
				this.fldBookWTotal,
				this.fldBookSTotal,
				this.fldBookTotal
			});
			this.GroupFooter1.Height = 0.3541667F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.25F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Billing Edit Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.5F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.5F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.0625F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.4375F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6.0625F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.4375F;
			// 
			// lblBook
			// 
			this.lblBook.Height = 0.1875F;
			this.lblBook.HyperLink = null;
			this.lblBook.Left = 0F;
			this.lblBook.Name = "lblBook";
			this.lblBook.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 0";
			this.lblBook.Text = "Book #";
			this.lblBook.Top = 0.1875F;
			this.lblBook.Width = 0.5625F;
			// 
			// fldBook
			// 
			this.fldBook.DataField = "Binder";
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0.5625F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.fldBook.Text = null;
			this.fldBook.Top = 0.1875F;
			this.fldBook.Width = 0.5625F;
			// 
			// lblSeq
			// 
			this.lblSeq.Height = 0.1875F;
			this.lblSeq.HyperLink = null;
			this.lblSeq.Left = 0F;
			this.lblSeq.Name = "lblSeq";
			this.lblSeq.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblSeq.Text = "Seq";
			this.lblSeq.Top = 0F;
			this.lblSeq.Width = 0.375F;
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.Height = 0.1875F;
			this.lblAccountNumber.HyperLink = null;
			this.lblAccountNumber.Left = 4.625F;
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblAccountNumber.Text = "Acct";
			this.lblAccountNumber.Top = 0F;
			this.lblAccountNumber.Width = 0.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 5.3125F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0F;
			this.lblName.Width = 0.875F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 6.4375F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; ddo-char-set: 0";
			this.lblLocation.Text = "Location";
			this.lblLocation.Top = 0F;
			this.lblLocation.Width = 1F;
			// 
			// lnBookHeader
			// 
			this.lnBookHeader.Height = 0F;
			this.lnBookHeader.Left = 0F;
			this.lnBookHeader.LineWeight = 1F;
			this.lnBookHeader.Name = "lnBookHeader";
			this.lnBookHeader.Top = 0.1875F;
			this.lnBookHeader.Width = 7.5F;
			this.lnBookHeader.X1 = 0F;
			this.lnBookHeader.X2 = 7.5F;
			this.lnBookHeader.Y1 = 0.1875F;
			this.lnBookHeader.Y2 = 0.1875F;
			// 
			// lblPrev
			// 
			this.lblPrev.Height = 0.1875F;
			this.lblPrev.HyperLink = null;
			this.lblPrev.Left = 0.375F;
			this.lblPrev.Name = "lblPrev";
			this.lblPrev.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblPrev.Text = "Previous";
			this.lblPrev.Top = 0F;
			this.lblPrev.Width = 0.6875F;
			// 
			// lblCurrent
			// 
			this.lblCurrent.Height = 0.1875F;
			this.lblCurrent.HyperLink = null;
			this.lblCurrent.Left = 1.0625F;
			this.lblCurrent.Name = "lblCurrent";
			this.lblCurrent.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCurrent.Text = "Current";
			this.lblCurrent.Top = 0F;
			this.lblCurrent.Width = 0.625F;
			// 
			// lblCons
			// 
			this.lblCons.Height = 0.1875F;
			this.lblCons.HyperLink = null;
			this.lblCons.Left = 1.6875F;
			this.lblCons.Name = "lblCons";
			this.lblCons.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblCons.Text = "Cons";
			this.lblCons.Top = 0F;
			this.lblCons.Width = 0.5F;
			// 
			// lblTotal
			// 
			this.lblTotal.Height = 0.1875F;
			this.lblTotal.HyperLink = null;
			this.lblTotal.Left = 3.6875F;
			this.lblTotal.Name = "lblTotal";
			this.lblTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblTotal.Text = "Total";
			this.lblTotal.Top = 0F;
			this.lblTotal.Width = 0.9375F;
			// 
			// lblSTotal
			// 
			this.lblSTotal.Height = 0.1875F;
			this.lblSTotal.HyperLink = null;
			this.lblSTotal.Left = 2.9375F;
			this.lblSTotal.Name = "lblSTotal";
			this.lblSTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblSTotal.Text = "Sewer";
			this.lblSTotal.Top = 0F;
			this.lblSTotal.Width = 0.75F;
			// 
			// lblWTotal
			// 
			this.lblWTotal.Height = 0.1875F;
			this.lblWTotal.HyperLink = null;
			this.lblWTotal.Left = 2.1875F;
			this.lblWTotal.Name = "lblWTotal";
			this.lblWTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblWTotal.Text = "Water";
			this.lblWTotal.Top = 0F;
			this.lblWTotal.Width = 0.75F;
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 4.625F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.625F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.125F;
			this.fldName.Left = 5.3125F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 1.125F;
			// 
			// fldSeq
			// 
			this.fldSeq.Height = 0.1875F;
			this.fldSeq.Left = 0F;
			this.fldSeq.Name = "fldSeq";
			this.fldSeq.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldSeq.Text = null;
			this.fldSeq.Top = 0F;
			this.fldSeq.Width = 0.375F;
			// 
			// fldCons
			// 
			this.fldCons.Height = 0.1875F;
			this.fldCons.Left = 1.6875F;
			this.fldCons.Name = "fldCons";
			this.fldCons.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCons.Text = null;
			this.fldCons.Top = 0F;
			this.fldCons.Width = 0.5F;
			// 
			// fldLoc
			// 
			this.fldLoc.Height = 0.1875F;
			this.fldLoc.Left = 6.4375F;
			this.fldLoc.Name = "fldLoc";
			this.fldLoc.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.fldLoc.Text = null;
			this.fldLoc.Top = 0F;
			this.fldLoc.Width = 1.0625F;
			// 
			// fldPrev
			// 
			this.fldPrev.Height = 0.1875F;
			this.fldPrev.Left = 0.375F;
			this.fldPrev.Name = "fldPrev";
			this.fldPrev.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldPrev.Text = null;
			this.fldPrev.Top = 0F;
			this.fldPrev.Width = 0.6875F;
			// 
			// fldCur
			// 
			this.fldCur.Height = 0.1875F;
			this.fldCur.Left = 1.0625F;
			this.fldCur.Name = "fldCur";
			this.fldCur.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldCur.Text = null;
			this.fldCur.Top = 0F;
			this.fldCur.Width = 0.625F;
			// 
			// fldWTotal
			// 
			this.fldWTotal.Height = 0.1875F;
			this.fldWTotal.Left = 2.1875F;
			this.fldWTotal.Name = "fldWTotal";
			this.fldWTotal.OutputFormat = resources.GetString("fldWTotal.OutputFormat");
			this.fldWTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldWTotal.Text = null;
			this.fldWTotal.Top = 0F;
			this.fldWTotal.Width = 0.75F;
			// 
			// fldSTotal
			// 
			this.fldSTotal.Height = 0.1875F;
			this.fldSTotal.Left = 2.9375F;
			this.fldSTotal.Name = "fldSTotal";
			this.fldSTotal.OutputFormat = resources.GetString("fldSTotal.OutputFormat");
			this.fldSTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldSTotal.Text = null;
			this.fldSTotal.Top = 0F;
			this.fldSTotal.Width = 0.75F;
			// 
			// fldAcctTotal
			// 
			this.fldAcctTotal.Height = 0.1875F;
			this.fldAcctTotal.Left = 3.6875F;
			this.fldAcctTotal.Name = "fldAcctTotal";
			this.fldAcctTotal.OutputFormat = resources.GetString("fldAcctTotal.OutputFormat");
			this.fldAcctTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.fldAcctTotal.Text = null;
			this.fldAcctTotal.Top = 0F;
			this.fldAcctTotal.Width = 0.9375F;
			// 
			// srptCalculationDetailOb
			// 
			this.srptCalculationDetailOb.CloseBorder = false;
			this.srptCalculationDetailOb.Height = 0.0625F;
			this.srptCalculationDetailOb.Left = 0F;
			this.srptCalculationDetailOb.Name = "srptCalculationDetailOb";
			this.srptCalculationDetailOb.Report = null;
			this.srptCalculationDetailOb.Top = 0.125F;
			this.srptCalculationDetailOb.Visible = false;
			this.srptCalculationDetailOb.Width = 7.5F;
			// 
			// lblWarning
			// 
			this.lblWarning.Height = 0.1875F;
			this.lblWarning.HyperLink = null;
			this.lblWarning.Left = 0.4375F;
			this.lblWarning.Name = "lblWarning";
			this.lblWarning.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold";
			this.lblWarning.Text = null;
			this.lblWarning.Top = 0.125F;
			this.lblWarning.Visible = false;
			this.lblWarning.Width = 6F;
			// 
			// fldMapLot
			// 
			this.fldMapLot.Height = 0.1875F;
			this.fldMapLot.Left = 5.25F;
			this.fldMapLot.Name = "fldMapLot";
			this.fldMapLot.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; ddo-char-set: 0";
			this.fldMapLot.Text = null;
			this.fldMapLot.Top = 0F;
			this.fldMapLot.Visible = false;
			this.fldMapLot.Width = 1.1875F;
			// 
			// lnWarning
			// 
			this.lnWarning.Height = 0F;
			this.lnWarning.Left = 0.3125F;
			this.lnWarning.LineWeight = 1F;
			this.lnWarning.Name = "lnWarning";
			this.lnWarning.Top = 0.3125F;
			this.lnWarning.Visible = false;
			this.lnWarning.Width = 6.75F;
			this.lnWarning.X1 = 0.3125F;
			this.lnWarning.X2 = 7.0625F;
			this.lnWarning.Y1 = 0.3125F;
			this.lnWarning.Y2 = 0.3125F;
			// 
			// lnBookTotal
			// 
			this.lnBookTotal.Height = 0F;
			this.lnBookTotal.Left = 2.25F;
			this.lnBookTotal.LineWeight = 1F;
			this.lnBookTotal.Name = "lnBookTotal";
			this.lnBookTotal.Top = 0.0625F;
			this.lnBookTotal.Width = 2.375F;
			this.lnBookTotal.X1 = 2.25F;
			this.lnBookTotal.X2 = 4.625F;
			this.lnBookTotal.Y1 = 0.0625F;
			this.lnBookTotal.Y2 = 0.0625F;
			// 
			// lblBookTotal
			// 
			this.lblBookTotal.Height = 0.1875F;
			this.lblBookTotal.HyperLink = null;
			this.lblBookTotal.Left = 0.75F;
			this.lblBookTotal.Name = "lblBookTotal";
			this.lblBookTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblBookTotal.Text = "Book Total:";
			this.lblBookTotal.Top = 0.125F;
			this.lblBookTotal.Width = 1.25F;
			// 
			// fldBookWTotal
			// 
			this.fldBookWTotal.Height = 0.1875F;
			this.fldBookWTotal.Left = 2F;
			this.fldBookWTotal.Name = "fldBookWTotal";
			this.fldBookWTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.fldBookWTotal.Text = null;
			this.fldBookWTotal.Top = 0.125F;
			this.fldBookWTotal.Width = 0.9375F;
			// 
			// fldBookSTotal
			// 
			this.fldBookSTotal.Height = 0.1875F;
			this.fldBookSTotal.Left = 2.9375F;
			this.fldBookSTotal.Name = "fldBookSTotal";
			this.fldBookSTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.fldBookSTotal.Text = null;
			this.fldBookSTotal.Top = 0.125F;
			this.fldBookSTotal.Width = 0.9375F;
			// 
			// fldBookTotal
			// 
			this.fldBookTotal.Height = 0.1875F;
			this.fldBookTotal.Left = 3.8125F;
			this.fldBookTotal.Name = "fldBookTotal";
			this.fldBookTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right";
			this.fldBookTotal.Text = null;
			this.fldBookTotal.Top = 0.125F;
			this.fldBookTotal.Width = 1F;
			// 
			// lblOverride
			// 
			this.lblOverride.Height = 0.1875F;
			this.lblOverride.HyperLink = null;
			this.lblOverride.Left = 6.3125F;
			this.lblOverride.Name = "lblOverride";
			this.lblOverride.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right; ddo-char-set: 0";
			this.lblOverride.Text = "* = Override";
			this.lblOverride.Top = 0F;
			this.lblOverride.Width = 1.125F;
			// 
			// fldGrandWTotal
			// 
			this.fldGrandWTotal.Height = 0.1875F;
			this.fldGrandWTotal.Left = 2F;
			this.fldGrandWTotal.Name = "fldGrandWTotal";
			this.fldGrandWTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldGrandWTotal.Text = null;
			this.fldGrandWTotal.Top = 0.125F;
			this.fldGrandWTotal.Width = 0.9375F;
			// 
			// fldGrandSTotal
			// 
			this.fldGrandSTotal.Height = 0.1875F;
			this.fldGrandSTotal.Left = 2.9375F;
			this.fldGrandSTotal.Name = "fldGrandSTotal";
			this.fldGrandSTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldGrandSTotal.Text = null;
			this.fldGrandSTotal.Top = 0.125F;
			this.fldGrandSTotal.Width = 0.9375F;
			// 
			// lblReportTotal
			// 
			this.lblReportTotal.Height = 0.1875F;
			this.lblReportTotal.HyperLink = null;
			this.lblReportTotal.Left = 1F;
			this.lblReportTotal.Name = "lblReportTotal";
			this.lblReportTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.lblReportTotal.Text = "Total:";
			this.lblReportTotal.Top = 0.125F;
			this.lblReportTotal.Width = 1F;
			// 
			// fldGrandTotal
			// 
			this.fldGrandTotal.Height = 0.1875F;
			this.fldGrandTotal.Left = 3.8125F;
			this.fldGrandTotal.Name = "fldGrandTotal";
			this.fldGrandTotal.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; font-weight: bold; text-align: right; dd" + "o-char-set: 0";
			this.fldGrandTotal.Text = null;
			this.fldGrandTotal.Top = 0.125F;
			this.fldGrandTotal.Width = 1F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 2.3125F;
			this.lnTotals.LineWeight = 1F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0.0625F;
			this.lnTotals.Width = 2.3125F;
			this.lnTotals.X1 = 2.3125F;
			this.lnTotals.X2 = 4.625F;
			this.lnTotals.Y1 = 0.0625F;
			this.lnTotals.Y2 = 0.0625F;
			// 
			// srptSummary
			// 
			this.srptSummary.CloseBorder = false;
			this.srptSummary.Height = 0.0625F;
			this.srptSummary.Left = 0F;
			this.srptSummary.Name = "srptSummary";
			this.srptSummary.Report = null;
			this.srptSummary.Top = 0.375F;
			this.srptSummary.Width = 7.5F;
			// 
			// srptSummary2
			// 
			this.srptSummary2.CloseBorder = false;
			this.srptSummary2.Height = 0.0625F;
			this.srptSummary2.Left = 0F;
			this.srptSummary2.Name = "srptSummary2";
			this.srptSummary2.Report = null;
			this.srptSummary2.Top = 0.5F;
			this.srptSummary2.Width = 7.5F;
			// 
			// arBillingEdit
			// 
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPrev)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLoc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPrev)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCur)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWarning)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMapLot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBookTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblOverride)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandWTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandSTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldGrandTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeq;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLoc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPrev;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCur;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcctTotal;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptCalculationDetailOb;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWarning;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMapLot;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnWarning;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldGrandTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptSummary;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport srptSummary2;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblOverride;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSeq;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnBookHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrev;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCons;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnBookTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBookTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookWTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookTotal;
	}
}
