﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptCustomLabels.
	/// </summary>
	public partial class rptCustomLabels : BaseSectionReport
	{
		public rptCustomLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
		}

		public static rptCustomLabels InstancePtr
		{
			get
			{
				return (rptCustomLabels)Sys.GetInstance(typeof(rptCustomLabels));
			}
		}

		protected rptCustomLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsMort.Dispose();
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/05/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/13/2004              *
		// ********************************************************
		// THIS REPORT IS TOTALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsMort = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		bool boolEmpty;
		string strLeftAdjustment = "";
		// vbPorter upgrade warning: lngVertAdjust As int	OnWriteFCConvert.ToDouble(
		float lngVertAdjust;
		int intTypeOfLabel;
		bool boolCondensed;
		int intPrinterOption;
		string strPrintOption = "";
		bool boolWater;
		int lngLastAccountKey;
		bool blnFirstRecord;
		clsPrintLabel labLabels = new clsPrintLabel();
		// vbPorter upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		float lngPrintWidth;
		public int intRateType;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				lngLastAccountKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey"));
				// k  ("Key")
				eArgs.EOF = false;
			}
			else
			{
				rsData.MoveNext();
				TryAgain:
				;
				if (rsData.EndOfFile() != true)
				{
					lngLastAccountKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey"));
						// k   ("Key")				
				}
			}
			eArgs.EOF = rsData.EndOfFile();
		}
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: intRateType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref string strSQL, ref string strReportType, int intLabelType, ref string strPrinterName, ref string strFonttoUse, ref string strPassSortOrder, short intRateType, ref bool boolPassWater, string strPassDefaultPrinter = "", bool boolPassCondensed = false)
		{
			string strDBName = "";
			int intReturn;
			int x;
			bool boolUseFont;
			//clsDRWrapper rsAdjust = new clsDRWrapper();
			double dblLabelsAdjustment = 0;
			double dblLabelsAdjustmentH = 0;
			string strTemp = "";
			// *****
			if (intLabelType == 0 || intLabelType == 1)
			{
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMV", ref strTemp);
				dblLabelsAdjustment = Conversion.Val(strTemp);
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMH", ref strTemp);
				dblLabelsAdjustmentH = Conversion.Val(strTemp);
			}
			else
			{
				dblLabelsAdjustment = modMain.Statics.gdblLabelsAdjustment;
			}
			boolWater = boolPassWater;
			boolCondensed = boolPassCondensed;
			strFont = strFonttoUse;
			this.Document.Printer.PrinterName = strPrinterName;
			
			
			boolPP = false;
			boolMort = false;
			intPrinterOption = 7;
			// find out what types should be printed   ie.  Owners, Mortgage Holders and/or New Owners
			if (FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[0].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState != Wisej.Web.CheckState.Checked) || FCConvert.CBool(frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState != Wisej.Web.CheckState.Checked))
			{
				if (frmCustomLabels.InstancePtr.chkFormPrintOptions[0].CheckState == Wisej.Web.CheckState.Checked)
				{
					// Owner
					if (frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState == Wisej.Web.CheckState.Checked)
					{
						// MH
						if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
						{
							// New Owner
							// this should not happen
						}
						else
						{
							// Owner and MH
							strPrintOption = "AND NOT NewOwner = -1";
							intPrinterOption = 3;
						}
					}
					else
					{
						if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
						{
							// New Owner
							// Owner and New Owner
							strPrintOption = "AND MortgageHolder = 0";
							intPrinterOption = 5;
						}
						else
						{
							// Owner
							strPrintOption = "AND (NOT NewOwner = -1 AND MortgageHolder = 0)";
							intPrinterOption = 1;
						}
					}
				}
				else
				{
					if (frmCustomLabels.InstancePtr.chkFormPrintOptions[1].CheckState == Wisej.Web.CheckState.Checked)
					{
						// MH
						if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
						{
							// New Owner
							// MH and New Owner
							strPrintOption = "AND (NewOwner = -1 OR MortgageHolder <> 0)";
							intPrinterOption = 6;
						}
						else
						{
							strPrintOption = "AND MortgageHolder <> 0";
							intPrinterOption = 2;
						}
					}
					else
					{
						if (frmCustomLabels.InstancePtr.chkFormPrintOptions[2].CheckState == Wisej.Web.CheckState.Checked)
						{
							// New Owner
							// New Owner
							strPrintOption = "AND NewOwner = -1";
							intPrinterOption = 4;
						}
						else
						{
							// None
							strPrintOption = "AND Account = 0";
							intPrinterOption = 0;
						}
					}
				}
			}
			else
			{
				// get all of the copies
			}
			if (strReportType == "MORTGAGEHOLDER")
			{
				strDBName = "CentralData";
				boolMort = true;
			}
			else if ((strReportType == "RECL") || (strReportType == "LABELS"))
			{
				strDBName = modExtraModules.strUTDatabase;
			}
			if (boolWater)
			{
				rsData.OpenRecordset("SELECT *, CMFNumbers.Name AS [CMFNumbers.Name] FROM ((" + strSQL + ") AS New INNER JOIN CMFNumbers ON New.Bill = CMFNumbers.BillKey) WHERE BillStatus = 'B' AND ISNULL(boolWater,0) = 1 AND Type = " + FCConvert.ToString(intRateType) + strPrintOption + " ORDER BY " + strPassSortOrder + ", NewOwner desc, MortgageHolder, BillingRateKey", modExtraModules.strUTDatabase);
			}
			else
			{
				rsData.OpenRecordset("SELECT *, CMFNumbers.Name AS [CMFNumbers.Name] FROM ((" + strSQL + ") AS New INNER JOIN CMFNumbers ON New.Bill = CMFNumbers.BillKey) WHERE BillStatus = 'B' AND ISNULL(boolWater,0) = 0 AND Type = " + FCConvert.ToString(intRateType) + strPrintOption + " ORDER BY " + strPassSortOrder + ", NewOwner desc, MortgageHolder, BillingRateKey", modExtraModules.strUTDatabase);
			}
			rsMort.OpenRecordset("SELECT * FROM MortgageHolders", "CentralData");
			intTypeOfLabel = intLabelType;
			// make them choose the printer first if you have to use a custom form
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			boolDifferentPageSize = false;
			int intindex;
			int cnt;
			intindex = labLabels.Get_IndexFromID(intTypeOfLabel);
			
			if (labLabels.Get_IsDymoLabel(intindex))
			{
				switch (labLabels.Get_ID(intindex))
				{
					case modLabels.CNSTLBLTYPEDYMO30256:
						{
							// If Not Me.Printer.PrintDialog Then
							// Unload Me
							// Exit Sub
							// End If
							strPrinterName = this.Document.Printer.PrinterName;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							// kk11242014 troge-241  Change to use global default settings
							this.PageSettings.Margins.Top = FCConvert.ToSingle(labLabels.Get_TopMargin(intindex) + (dblLabelsAdjustment * 270) / 1440f);
							// 0.128 * 1440
							this.PageSettings.Margins.Bottom = labLabels.Get_BottomMargin(intindex);
							// 0
							this.PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
							// 0
							this.PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
							// 0.128 * 1440
							PrintWidth = labLabels.Get_LabelWidth(intindex) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							// (2.31 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
							intLabelWidth = labLabels.Get_LabelWidth(intindex);
							// (4 * 1440)
							lngPrintWidth = PrintWidth;
							// (4 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
							Detail.Height = labLabels.Get_LabelHeight(intindex) - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
							// (1440 * 2.3125) - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 '2 5/16"
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = labLabels.Get_LabelWidth(intindex);
							// Landscape                    '4 * 1440
							PageSettings.PaperWidth = labLabels.Get_LabelHeight(intindex);
							// 2.31 * 1440
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							break;
						}
					case modLabels.CNSTLBLTYPEDYMO30252:
						{
							// If Not Me.Printer.PrintDialog Then
							// Unload Me
							// Exit Sub
							// End If
							strPrinterName = this.Document.Printer.PrinterName;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							this.PageSettings.Margins.Top = 0.128F;
							this.PageSettings.Margins.Bottom = 0;
							this.PageSettings.Margins.Right = 0;
							this.PageSettings.Margins.Left = 0.128F;
							// 
							PrintWidth = 3.5F - (this.PageSettings.Margins.Left - this.PageSettings.Margins.Right) / 1440F;
							intLabelWidth = 3.5F;
							lngPrintWidth = 3.5F - (this.PageSettings.Margins.Left - this.PageSettings.Margins.Right) / 1440F;
							Detail.Height = 1.1F - (this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10) / 1440F;
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = 3.5F;
							PageSettings.PaperWidth = 1.1F;
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							break;
						}
				}
				//end switch
			}
			else if (labLabels.Get_IsLaserLabel(intindex))
			{
				this.PageSettings.Margins.Top = 0.5F;
				this.PageSettings.Margins.Top += FCConvert.ToSingle(dblLabelsAdjustment * 270) / 1440F;
				this.PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
				this.PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
				PrintWidth = labLabels.Get_PageWidth(intindex) - labLabels.Get_LeftMargin(intindex) - labLabels.Get_RightMargin(intindex);
				intLabelWidth = labLabels.Get_LabelWidth(intindex);
				Detail.Height = labLabels.Get_LabelHeight(intindex) + labLabels.Get_VerticalSpace(intindex);
				if (labLabels.Get_LabelsWide(intindex) > 0)
				{
					Detail.ColumnCount = labLabels.Get_LabelsWide(intindex);
					Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intindex);
				}
				lngPrintWidth = PrintWidth;
			}
			CreateDataFields();
			frmReportViewer.InstancePtr.Init(this, strPrinterName);
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (rsData.RecordCount() > 0)
			{
				if (boolWater)
				{
					//FC:FINAL:DDU:#1134 - fixed intRateType value after frmRateRecChoice is unloadded
					//switch (frmRateRecChoice.InstancePtr.intRateType)
					switch (intRateType)
					{
						case 10:
							{
								modGlobalFunctions.IncrementSavedReports("LastWUT30DayLabels");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUT30DayLabels1.RDF"));
								break;
							}
						case 11:
							{
								modGlobalFunctions.IncrementSavedReports("LastWUTTransferToLienLabels");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTTransferToLienLabels1.RDF"));
								break;
							}
						case 12:
							{
								modGlobalFunctions.IncrementSavedReports("LastWUTLienMaturityLabels");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTLienMaturityLabels1.RDF"));
								break;
							}
					}
					//end switch
				}
				else
				{
					//FC:FINAL:DDU:#1134 - fixed intRateType value after frmRateRecChoice is unloadded
					//switch (frmRateRecChoice.InstancePtr.intRateType)
					switch (intRateType)
					{
						case 10:
							{
								modGlobalFunctions.IncrementSavedReports("LastSUT30DayLabels");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUT30DayLabels1.RDF"));
								break;
							}
						case 11:
							{
								modGlobalFunctions.IncrementSavedReports("LastSUTTransferToLienLabels");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTTransferToLienLabels1.RDF"));
								break;
							}
						case 12:
							{
								modGlobalFunctions.IncrementSavedReports("LastSUTLienMaturityLabels");
								this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTLienMaturityLabels1.RDF"));
								break;
							}
					}
					//end switch
				}
			}
			else
			{
				//FC:FINAl:SBE - #i1056 - MessageBox is not visible in the reports, if it's shown as modal
				//MessageBox.Show("There are no eligible accounts.", "No Labels", MessageBoxButtons.OK, MessageBoxIcon.Information, modal:false);
				MessageBox.Show("There are no eligible accounts.", "No Labels", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			int intReturn = 0;
			int const_PrintToolID;
			int cnt;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// override the print button so the print dialog doesn't come up again
			const_PrintToolID = 9950;
			//for(cnt=0; cnt<=this.Toolbar.Tools.Count-1; cnt++) {
			//	if ("Print..."==this.Toolbar.Tools(cnt).Caption) {
			//		this.Toolbar.Tools(cnt).ID = const_PrintToolID;
			//		this.Toolbar.Tools(cnt).Enabled = true;
			//	}
			//} // cnt
			if (intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30256 || intTypeOfLabel == modLabels.CNSTLBLTYPEDYMO30252)
			{
				for (cnt = 0; cnt <= this.Document.Printer.PaperSizes.Count - 1; cnt++)
				{
					switch (intTypeOfLabel)
					{
						case modLabels.CNSTLBLTYPEDYMO30252:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30252 ADDRESS")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
						case modLabels.CNSTLBLTYPEDYMO30256:
							{
								if (Strings.UCase(this.Document.Printer.PaperSizes[cnt].PaperName) == "30256 SHIPPING")
								{
									this.Document.Printer.PaperSize = this.Document.Printer.PaperSizes[cnt];
									this.Document.Printer.DefaultPageSettings.Landscape = true;
									break;
								}
								break;
							}
					}
					//end switch
				}
				// cnt
			}
			else
			{
                this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
			}
			blnFirstRecord = true;
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intCol;
			int intNumber;
			int intRowH;
			intRowH = 200;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 5;
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
				if (intRow == 1)
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.CanGrow = false;
					NewField.Name = "txtAcct";
					NewField.Top = (((intRow - 1) * intRowH) + lngVertAdjust) / 1440F;
					// MAL@20080312: Change so account line is left justified
					// NewField.Left = ((PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 800
					NewField.Left = 144 / 1440F;
					// NewField.Width = 999
					NewField.Width = (PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing);
					NewField.Height = intRowH / 1440F;
					NewField.Text = string.Empty;
					NewField.MultiLine = false;
					NewField.WordWrap = false;
					// MAL@20080312
					if (Strings.Trim(strFont) != string.Empty)
					{
						NewField.Font = new Font(strFont, NewField.Font.Size);
					}
					else
					{
						NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
					}
					if (boolCondensed)
					{
						NewField.Left = ((FCConvert.ToInt32(PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 800 / 1440F) / 2.0F;
						NewField.Font = FCUtils.FontChangeSize(NewField.Font, FCConvert.ToInt32(NewField.Font.Size - 5));
						//.Size -= 5;
					}
				}
				else
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.CanGrow = false;
					NewField.Name = "txtData" + FCConvert.ToString(intRow);
					NewField.Top = (((intRow - 1) * intRowH) + lngVertAdjust) / 1440F;
					NewField.Left = 144 / 1440F;
					// one space
					NewField.Width = ((PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing));
					// - 145
					NewField.Height = intRowH / 1440F;
					NewField.CanGrow = false;
					NewField.Text = string.Empty;
					NewField.MultiLine = false;
					NewField.WordWrap = false;
					// MAL@20080312
					if (Strings.Trim(strFont) != string.Empty)
					{
						NewField.Font = new Font(strFont, NewField.Font.Size);
					}
					else
					{
						NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
					}
					if (boolCondensed)
					{
						NewField.Font = FCUtils.FontChangeSize(NewField.Font, FCConvert.ToInt32(NewField.Font.Size - 5));
						//.Size -= 5;
					}
				}
				Detail.Controls.Add(NewField);				
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			
		}

		public void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			// Call VerifyPrintToFile(Me, Tool)
			string vbPorterVar = ""/*Tool.Caption*/;
			if (vbPorterVar == "Print...")
			{
				int intNumberOfPages = this.Document.Pages.Count;
				int intPageStart = 1;
				if (frmNumPages.InstancePtr.Init(ref intNumberOfPages, ref intPageStart, this))
				{
					this.PrintReport(false);
				}
			}
		}

		private void PrintMortgageHolders()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intControl;
				int intRow;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string str5 = "";
				int lngAcctNumber;
				if (rsData.EndOfFile() != true)
				{
					if (boolPP)
					{
						str1 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name")));
						// TODO Get_Fields: Field [PPAddr1] not found!! (maybe it is an alias?)
						str2 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("PPAddr1")));
						// TODO Get_Fields: Field [PPAddr2] not found!! (maybe it is an alias?)
						str3 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("PPAddr2")));
						// TODO Get_Fields: Field [PPCity] not found!! (maybe it is an alias?)
						str4 = strLeftAdjustment + Strings.Trim(rsData.Get_Fields("PPCity") + "  " + rsData.Get_Fields_String("RSState"));
					}
					else
					{
						str1 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name")));
						// TODO Get_Fields: Field [PPAddr1] not found!! (maybe it is an alias?)
						str2 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("PPAddr1")));
						// TODO Get_Fields: Field [PPAddr2] not found!! (maybe it is an alias?)
						str3 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields("PPAddr2")));
						// TODO Get_Fields: Field [PPCity] not found!! (maybe it is an alias?)
						str4 = strLeftAdjustment + Strings.Trim(rsData.Get_Fields("PPCity") + "  " + rsData.Get_Fields_String("RSState"));
					}
					if (boolPP)
					{
						if (Conversion.Val(rsData.Get_Fields_String("Zip")) > 0)
						{
							str4 += " " + Strings.Format(rsData.Get_Fields_String("Zip"), "00000");
							if (Conversion.Val(rsData.Get_Fields_String("Zip4")) > 0)
							{
								str4 += "-" + Strings.Format(rsData.Get_Fields_String("Zip4"), "0000");
							}
						}
					}
					else if (boolMort)
					{
						str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
						str5 = Strings.Trim(rsData.Get_Fields_String("City") + "  " + rsData.Get_Fields_String("State"));
						str5 += " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip")));
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4"))) != string.Empty)
						{
							str5 += "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4")));
						}
					}
					else
					{
						str4 += " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip")));
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4"))) != string.Empty)
						{
							str4 += "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4")));
						}
					}
					// condense the labels if some are blank
					if (boolMort)
					{
						if (Strings.Trim(str4) == string.Empty)
						{
							str4 = str5;
							str5 = "";
						}
					}
					if (Strings.Trim(str3) == string.Empty)
					{
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						str2 = str3;
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						str1 = str2;
						str2 = str3;
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
					{
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
						{
							intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
							switch (intRow)
							{
								case 2:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str1;
										break;
									}
								case 3:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str2;
										break;
									}
								case 4:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str3;
										break;
									}
								case 5:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str4;
										break;
									}
								case 6:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str5;
										break;
									}
							}
							//end switch
						}
						// this will fill the account number in
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtAcct")
						{
							// MAL@20080304: Add left adjustment to account number
							if (boolWater)
							{
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + "UW" + rsData.Get_Fields("AccountNumber");
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + "US" + rsData.Get_Fields("AccountNumber");
							}
						}
						if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
						{
							// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
							(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields("MortgageHolderNumber"), "00000");
						}
					}
					// intControl
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				//FC:FINAl:SBE - #i1056 - MessageBox is not visible in the reports, if it's shown as modal
				//MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Printing Mortgage Holders", MessageBoxButtons.OK, MessageBoxIcon.Hand, modal:false);
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Printing Mortgage Holders", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PrintLabels()
		{
			// this will fill in the fields for labels
			int intControl;
			int intRow;
			string str1;
			string str2;
			string str3;
			string str4;
			string str5;
			string strAccount = "";
			int intLen = 0;
			bool blnLeft = false;
			// TODO Get_Fields: Field [CMFNumbers.Name] not found!! (maybe it is an alias?)
			str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields("CMFNumbers.Name")));
			str2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
			str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
			str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
			str5 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address4")));
			if (intTypeOfLabel == 2)
			{
				// small labels
				if (str1.Length > 20)
				{
					str1 = Strings.Left(str1, 20);
				}
			}
			else
			{
				if (str1.Length > 34)
				{
					str1 = Strings.Left(str1, 34);
				}
			}
			// condense the labels if some are blank
			if (Strings.Trim(str4) == string.Empty)
			{
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str3) == string.Empty)
			{
				str3 = str4;
				if (boolMort)
				{
					str4 = str5;
					str5 = "";
				}
				else
				{
					str4 = "";
				}
			}
			if (Strings.Trim(str2) == string.Empty)
			{
				str2 = str3;
				str3 = str4;
				if (boolMort)
				{
					str4 = str5;
					str5 = "";
				}
				else
				{
					str4 = "";
				}
			}
			if (Strings.Trim(str1) == string.Empty)
			{
				str1 = str2;
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str5) != string.Empty)
			{
				if (boolWater)
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					intLen = ("UW" + rsData.Get_Fields("AccountNumber")).Length;
					strAccount = Strings.Left(str1, 25 - intLen);
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					strAccount += "UW" + rsData.Get_Fields("AccountNumber");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					intLen = (" US" + rsData.Get_Fields("AccountNumber")).Length;
					strAccount = Strings.Left(str1, 25 - intLen);
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					strAccount += " US" + rsData.Get_Fields("AccountNumber");
				}
				str1 = str2;
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
				blnLeft = true;
			}
			else
			{
				if (boolWater)
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					strAccount = "UW" + rsData.Get_Fields("AccountNumber");
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					strAccount = "US" + rsData.Get_Fields("AccountNumber");
				}
				blnLeft = false;
			}
			for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
			{
				if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
				{
					intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
					switch (intRow)
					{
					// MAL@20080304: Change to have the account number be row 1
					// Tracker Reference: 12445
						case 2:
							{
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str1;
								break;
							}
						case 3:
							{
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str2;
								break;
							}
						case 4:
							{
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str3;
								break;
							}
						case 5:
							{
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str4;
								break;
							}
						case 6:
							{
								(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str5;
								break;
							}
					}
					//end switch
				}
				// this will fill the account number in
				if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtAcct")
				{
					(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strAccount;
					if (blnLeft)
					{
						Detail.Controls[1].Left = 144 / 1440F;
					}
					else
					{
						Detail.Controls[1].Left = ((PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - (800 / 1440F);
					}
				}
				if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
				{
					// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
					(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields("MortgageHolderNumber"), "00000");
				}
			}
			// intControl
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LABELS")
			{
				PrintLabels();
			}
			else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "RECL") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "MORTGAGEHOLDER"))
			{
				PrintMortgageHolders();
			}
		}

		private void rptCustomLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCustomLabels properties;
			//rptCustomLabels.Caption	= "Custom Labels";
			//rptCustomLabels.Icon	= "rptCustomLabels.dsx":0000";
			//rptCustomLabels.Left	= 0;
			//rptCustomLabels.Top	= 0;
			//rptCustomLabels.Width	= 11880;
			//rptCustomLabels.Height	= 8595;
			//rptCustomLabels.StartUpPosition	= 3;
			//rptCustomLabels.SectionData	= "rptCustomLabels.dsx":058A;
			//End Unmaped Properties
		}
	}
}
