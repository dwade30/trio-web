﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDisconnectNoticeOriginalPlainPaper.
	/// </summary>
	public partial class rptDisconnectNoticeOriginalPlainPaper : BaseSectionReport
	{
		public rptDisconnectNoticeOriginalPlainPaper()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Disconnection Notices";
		}

		public static rptDisconnectNoticeOriginalPlainPaper InstancePtr
		{
			get
			{
				return (rptDisconnectNoticeOriginalPlainPaper)Sys.GetInstance(typeof(rptDisconnectNoticeOriginalPlainPaper));
			}
		}

		protected rptDisconnectNoticeOriginalPlainPaper _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDisconnectNoticeOriginalPlainPaper	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/02/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/28/2006              *
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int intArrayMax;
		// How many books there are in the array
		int[] BookArray = null;
		// Array holding book numbers of selected books
		string strBooksSelection;
		// A - All Books   S - Selected Books
		string strSQL = "";
		// SQL statement to get information we need for report
		bool blnIncSew;
		bool blnSepSew;
		Decimal curMinAmt;
		DateTime datNotice;
		DateTime datShutoff;
		bool boolPrintForOwner;
		int lngNewestRateKey;
		bool boolExcludeNCAccounts;
		// trouts-147
		double dblWTot;
		double dblSTot;
		double dblXInt;
		// vbPorter upgrade warning: intCT As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngPassNewestRateKey As int	OnWriteFCConvert.ToDouble(
		public void Init(string strBooks, DateTime datNoticeDate, DateTime datShutOffDate, Decimal curMinimumAmount, bool blnIncludeSewer, bool blnSeperateSewer, string strName, string strAddress1, string strAddress2, string strAddress3, string strAddress4, short intCT, ref int[] PassBookArray, int lngPassNewestRateKey)
		{
			int counter;
			strBooksSelection = strBooks;
			BookArray = PassBookArray;
			intArrayMax = intCT;
			blnIncSew = blnIncludeSewer;
			blnSepSew = blnSeperateSewer;
			curMinAmt = curMinimumAmount;
			datNotice = datNoticeDate;
			datShutoff = datShutOffDate;
			fldReturnName.Text = strName;
			fldReturnAddress1.Text = strAddress1;
			fldReturnAddress2.Text = strAddress2;
			fldReturnAddress3.Text = strAddress3;
			fldReturnAddress4.Text = strAddress4;
			lngNewestRateKey = lngPassNewestRateKey;
			if (strBooksSelection == "A")
			{
				strSQL = "";
			}
			else if (strBooksSelection == "B")
			{
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					strSQL = " = " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strSQL += FCConvert.ToString(BookArray[counter]) + ",";
					}
					strSQL = "IN (" + Strings.Left(strSQL, strSQL.Length - 1) + ")";
				}
			}
			else
			{
				strSQL = "= " + frmSetupDisconnectNotices.InstancePtr.txtAccountNumber.Text;
			}
			if (strBooksSelection == "A")
			{
				if (blnIncSew)
				{
					rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed <> Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed <> Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE (WaterTotal + SewerTotal) >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed <> Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed <> Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE WaterTotal >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
				}
			}
			else if (strBooksSelection == "B")
			{
				if (blnIncSew)
				{
					rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed <> Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed <> Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 AND Sequence > " + FCConvert.ToString(Conversion.Val(frmSetupDisconnectEditReport.InstancePtr.txtSequenceNumber.Text)) + " AND MeterTable.BookNumber " + strSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE (WaterTotal + SewerTotal) >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed <> Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed <> Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 AND Sequence > " + FCConvert.ToString(Conversion.Val(frmSetupDisconnectEditReport.InstancePtr.txtSequenceNumber.Text)) + " AND MeterTable.BookNumber " + strSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE WaterTotal >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
				}
			}
			else
			{
				if (blnIncSew)
				{
					rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM ((MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID) " + "WHERE ((Bill.WPrinOwed <> Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed <> Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 AND Master.AccountNumber " + strSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE (WaterTotal + SewerTotal) >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT Master.NoBill, qTmp.* " + "FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM ((MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID) INNER JOIN Master ON Bill.AccountKey = Master.ID) " + "WHERE ((Bill.WPrinOwed <> Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed <> Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 AND Master.AccountNumber " + strSQL + " " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) AS qTmp " + "INNER JOIN Master ON qTmp.AccountKey = Master.ID " + "WHERE WaterTotal >= " + FCConvert.ToString(curMinAmt) + " " + "ORDER BY BookNumber, Sequence, AccountKey");
				}
			}
			boolExcludeNCAccounts = FCConvert.CBool(frmSetupDisconnectNotices.InstancePtr.chkExcludeNC.CheckState == Wisej.Web.CheckState.Checked);
			// kk trouts-147  Add option to not send to NC accounts
			// if we found some records show report otherwise pop up message and end report
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					dblWTot = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), true, ref datNotice, lngNewestRateKey), 2);
					dblSTot = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), false, ref datNotice, lngNewestRateKey), 2);
					if (FCConvert.ToDecimal(dblWTot) >= curMinAmt && dblWTot > 0)
					{
						if (boolExcludeNCAccounts)
						{
							// kk09032015 trouts-147
							if (!FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("NoBill")))
							{
								break;
							}
						}
						else
						{
							break;
						}
					}
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
				if (rsInfo.EndOfFile() != true)
				{
					frmReportViewer.InstancePtr.Init(this);
				}
				else
				{
					MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
					this.Close();
				}
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile())
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				else
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				rsInfo.MovePrevious();
			}
			else
			{
				if (!boolPrintForOwner)
				{
					rsInfo.MoveNext();
					if (rsInfo.EndOfFile() != true)
					{
						do
						{
							dblWTot = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), true, ref datNotice, lngNewestRateKey), 2);
							dblSTot = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), false, ref datNotice, lngNewestRateKey), 2);
							if (FCConvert.ToDecimal(dblWTot) >= curMinAmt && dblWTot > 0)
							{
								if (boolExcludeNCAccounts)
								{
									// kk09032015 trouts-147
									if (!FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("NoBill")))
									{
										break;
									}
								}
								else
								{
									break;
								}
							}
							rsInfo.MoveNext();
						}
						while (rsInfo.EndOfFile() != true);
					}
				}
				eArgs.EOF = rsInfo.EndOfFile();
				rsInfo.MoveNext();
				if (rsInfo.EndOfFile())
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
				}
				else
				{
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
				}
				rsInfo.MovePrevious();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (strBooksSelection == "S")
			{
				frmSetupDisconnectNotices.InstancePtr.Show(App.MainForm);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			string strText = "";
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			//clsDRWrapper rsBill = new clsDRWrapper();
			// MAL@20080715: Move check for minimum amount to be first
			// Tracker Reference: 14569
			rsAccountInfo.OpenRecordset(modUTStatusPayments.UTMasterQuery(rsInfo.Get_Fields_Int32("AccountKey")), modExtraModules.strUTDatabase);
			if (modUTStatusPayments.Statics.TownService == "W" || !blnIncSew)
			{
				lblAsterisk.Visible = false;
				lblAsterisk2.Visible = false;
				fldSeperateSewer.Visible = false;
				fldSeperateSewerLabel.Visible = false;
				fldSewerTotalLabel.Visible = false;
				fldSewerTotal.Visible = false;
				linTotalLine.Visible = false;
				fldTotalTotalLabel.Visible = false;
				fldTotalTotal.Visible = false;
			}
			else
			{
				// TODO Get_Fields: Field [SewerTotal] not found!! (maybe it is an alias?)
				if (blnSepSew && FCConvert.ToInt32(rsInfo.Get_Fields("SewerTotal")) != 0)
				{
					lblAsterisk.Visible = true;
					lblAsterisk2.Visible = true;
					fldSeperateSewer.Visible = true;
					fldSeperateSewerLabel.Visible = true;
					fldSewerTotalLabel.Visible = false;
					fldSewerTotal.Visible = false;
					linTotalLine.Visible = false;
					fldTotalTotalLabel.Visible = false;
					fldTotalTotal.Visible = false;
				}
				else if (blnIncSew)
				{
					lblAsterisk.Visible = false;
					lblAsterisk2.Visible = false;
					fldSeperateSewer.Visible = false;
					fldSeperateSewerLabel.Visible = false;
					fldSewerTotalLabel.Visible = true;
					fldSewerTotal.Visible = true;
					linTotalLine.Visible = true;
					fldTotalTotalLabel.Visible = true;
					fldTotalTotal.Visible = true;
				}
			}
			// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
			fldBookSequence.Text = rsInfo.Get_Fields_Int32("BookNumber") + " / " + rsInfo.Get_Fields("Sequence");
			if (rsAccountInfo.EndOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccountNumber.Text = Strings.Format(rsAccountInfo.Get_Fields("AccountNumber"), "00000");
				if (boolPrintForOwner)
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					fldName.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("OwnerName"));
					// fldBName.Text = rsAccountInfo.Fields("OwnerName")
					if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("OState"))) == "")
					{
						strText = rsAccountInfo.Get_Fields_String("BCity") + ", " + rsAccountInfo.Get_Fields_String("BState") + " " + rsAccountInfo.Get_Fields_String("BZip");
					}
					else
					{
						strText = rsAccountInfo.Get_Fields_String("OCity") + ", " + rsAccountInfo.Get_Fields_String("OState") + " " + rsAccountInfo.Get_Fields_String("OZip");
					}
					// If Trim(rsAccountInfo.Fields("BZip4")) <> "" Then
					// strText = strText & "-" & Trim(rsAccountInfo.Fields("BZip4"))
					// End If
					// MAL@20071212: Add second owner name
					// Reference: 116734
					// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					if (FCConvert.ToString(rsAccountInfo.Get_Fields("SecondOwnerName")) != "")
					{
						// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
						fldAddress1.Text = FCConvert.ToString(rsAccountInfo.Get_Fields("SecondOwnerName"));
						fldAddress2.Text = rsAccountInfo.Get_Fields_String("OAddress1");
						fldAddress3.Text = rsAccountInfo.Get_Fields_String("OAddress2");
						fldAddress4.Text = rsAccountInfo.Get_Fields_String("OAddress3");
						fldAddress5.Text = strText;
						fldAddress5.Visible = true;
					}
					else
					{
						fldAddress1.Text = rsAccountInfo.Get_Fields_String("OAddress1");
						fldAddress2.Text = rsAccountInfo.Get_Fields_String("OAddress2");
						fldAddress3.Text = rsAccountInfo.Get_Fields_String("OAddress3");
						fldAddress4.Text = strText;
						fldAddress5.Text = "";
						fldAddress5.Visible = false;
					}
				}
				else
				{
					fldName.Text = rsAccountInfo.Get_Fields_String("Name");
					// fldBName.Text = rsAccountInfo.Fields("Name")
					if (Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("BState"))) == "")
					{
						strText = rsAccountInfo.Get_Fields_String("BCity") + ", " + rsAccountInfo.Get_Fields_String("OState") + " " + rsAccountInfo.Get_Fields_String("OZip");
					}
					else
					{
						strText = rsAccountInfo.Get_Fields_String("BCity") + ", " + rsAccountInfo.Get_Fields_String("BState") + " " + rsAccountInfo.Get_Fields_String("BZip");
					}
					// If Trim(rsAccountInfo.Fields("BZip4")) <> "" Then
					// strText = strText & "-" & Trim(rsAccountInfo.Fields("BZip4"))
					// End If
					// MAL@20071212
					if (FCConvert.ToString(rsAccountInfo.Get_Fields_String("Name2")) != "")
					{
						fldAddress1.Text = rsAccountInfo.Get_Fields_String("Name2");
						fldAddress2.Text = rsAccountInfo.Get_Fields_String("BAddress1");
						fldAddress3.Text = rsAccountInfo.Get_Fields_String("BAddress2");
						fldAddress4.Text = rsAccountInfo.Get_Fields_String("BAddress3");
						fldAddress5.Text = strText;
						// fldAddress5.Visible = True
					}
					else
					{
						fldAddress1.Text = rsAccountInfo.Get_Fields_String("BAddress1");
						fldAddress2.Text = rsAccountInfo.Get_Fields_String("BAddress2");
						fldAddress3.Text = rsAccountInfo.Get_Fields_String("BAddress3");
						fldAddress4.Text = strText;
						fldAddress5.Text = "";
						// fldAddress5.Visible = False
					}
				}
				if (frmSetupDisconnectNotices.InstancePtr.chkSendTo[1].CheckState == Wisej.Web.CheckState.Checked && !boolPrintForOwner)
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					if (rsAccountInfo.Get_Fields("OwnerName") != rsAccountInfo.Get_Fields_String("Name") && !FCConvert.ToBoolean(rsAccountInfo.Get_Fields_Boolean("SameBillOwner")))
					{
						boolPrintForOwner = true;
					}
					else
					{
						boolPrintForOwner = false;
					}
				}
				else
				{
					boolPrintForOwner = false;
				}
			}
			else
			{
				boolPrintForOwner = false;
				fldAccountNumber.Text = "00000";
				fldName.Text = "UNKNOWN";
			}
			dblWTot = modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), true, ref datNotice, lngNewestRateKey);
			dblSTot = modUTCalculations.CalculateAccountUTTotal_1(rsInfo.Get_Fields_Int32("AccountKey"), false, ref datNotice, lngNewestRateKey);
			fldNoticeDate.Text = Strings.Format(datNotice, "MM/dd/yyyy");
			fldShutOffDate.Text = Strings.Format(datShutoff, "MM/dd/yyyy");
			fldWaterTotal.Text = Strings.Format(dblWTot, "#,##0.00");
			fldSewerTotal.Text = Strings.Format(dblSTot, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblWTot + dblSTot, "#,##0.00");
			fldSeperateSewer.Text = Strings.Format(dblSTot, "#,##0.00");
			// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
			lblText.Text = "Water services will be discontinued for the premises at " + rsAccountInfo.Get_Fields("StreetNumber") + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt"))) + " " + rsAccountInfo.Get_Fields_String("StreetName") + ", on " + Strings.Format(datShutoff, "MM/dd/yyyy");
			if (Strings.Trim(fldAddress1.Text) == "")
			{
				if (Strings.Trim(fldAddress2.Text) == "")
				{
					if (Strings.Trim(fldAddress3.Text) == "")
					{
						if (Strings.Trim(fldAddress4.Text) == "")
						{
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								// Do Nothing - All Empty
							}
							else
							{
								fldAddress1.Text = fldAddress5.Text;
								fldAddress2.Text = "";
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
						else
						{
							fldAddress1.Text = fldAddress4.Text;
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress2.Text = "";
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress2.Text = fldAddress5.Text;
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
					}
					else
					{
						fldAddress1.Text = fldAddress3.Text;
						if (Strings.Trim(fldAddress4.Text) == "")
						{
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress2.Text = "";
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress2.Text = fldAddress5.Text;
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
						else
						{
							fldAddress2.Text = fldAddress4.Text;
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress2.Text = "";
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress3.Text = fldAddress5.Text;
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
					}
				}
				else
				{
					fldAddress1.Text = fldAddress2.Text;
					if (Strings.Trim(fldAddress3.Text) == "")
					{
						if (Strings.Trim(fldAddress4.Text) == "")
						{
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress2.Text = "";
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress2.Text = fldAddress5.Text;
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
						else
						{
							fldAddress2.Text = fldAddress4.Text;
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress3.Text = fldAddress5.Text;
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
					}
					else
					{
						fldAddress2.Text = fldAddress3.Text;
						if (Strings.Trim(fldAddress4.Text) == "")
						{
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress3.Text = fldAddress5.Text;
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
						else
						{
							fldAddress3.Text = fldAddress4.Text;
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress4.Text = fldAddress5.Text;
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
					}
				}
			}
			else
			{
				if (Strings.Trim(fldAddress2.Text) == "")
				{
					if (Strings.Trim(fldAddress3.Text) == "")
					{
						if (Strings.Trim(fldAddress4.Text) == "")
						{
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress2.Text = "";
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress2.Text = fldAddress5.Text;
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
						else
						{
							fldAddress2.Text = fldAddress4.Text;
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress3.Text = fldAddress5.Text;
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
					}
					else
					{
						fldAddress2.Text = fldAddress3.Text;
						if (Strings.Trim(fldAddress4.Text) == "")
						{
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress3.Text = fldAddress5.Text;
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
						else
						{
							fldAddress3.Text = fldAddress4.Text;
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress4.Text = fldAddress5.Text;
								fldAddress5.Text = "";
							}
						}
					}
				}
				else
				{
					fldAddress2.Text = fldAddress2.Text;
					if (Strings.Trim(fldAddress3.Text) == "")
					{
						if (Strings.Trim(fldAddress4.Text) == "")
						{
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress3.Text = "";
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress3.Text = fldAddress5.Text;
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
						}
						else
						{
							fldAddress3.Text = fldAddress4.Text;
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress4.Text = fldAddress5.Text;
								fldAddress5.Text = "";
							}
						}
					}
					else
					{
						fldAddress3.Text = fldAddress3.Text;
						if (Strings.Trim(fldAddress4.Text) == "")
						{
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress4.Text = "";
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress4.Text = fldAddress5.Text;
								fldAddress5.Text = "";
							}
						}
						else
						{
							fldAddress4.Text = fldAddress4.Text;
							if (Strings.Trim(fldAddress5.Text) == "")
							{
								fldAddress5.Text = "";
							}
							else
							{
								fldAddress5.Text = fldAddress5.Text;
							}
						}
					}
				}
			}
			rsAccountInfo.Dispose();
        }

	}
}
