﻿using System;
using Wisej.Web;
using fecherFoundation;
using Global;

namespace TWUT0000
{
    public partial class frmACHBankInformation : BaseForm
    {
        public frmACHBankInformation()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            SetupHandlers();
        }
        private void SetupHandlers()
        {
            cmdSave.Click += CmdSave_Click;
            this.Load += new System.EventHandler(this.frmACHBankInformation_Load);
            this.KeyDown += frmACHBankInformation_KeyDown;
        }

        private void frmACHBankInformation_Load(object sender, EventArgs e)
        {
            FillAccountCombo();
            LoadInfo();
        }

        private void frmACHBankInformation_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Escape:
                {
                    KeyCode = (Keys)0;
                    Close();
                    break;
                }
            }
            //end switch
        }

        private void FillAccountCombo()
        {
            cmbAccountType.Clear();
            cmbAccountType.AddItem("Checking");
            cmbAccountType.ItemData(0, 27);
            cmbAccountType.AddItem("Savings");
            cmbAccountType.ItemData(1, 37);
        }

        private void CmdSave_Click(object sender, EventArgs e)
        {
            SaveInfo();
        }

        private void SaveAndExit()
        {
            if (SaveInfo())
            {
                Close();
            }
        }

        private bool SaveInfo()
        {
            try
            {
                if (String.IsNullOrWhiteSpace(txtDestName.Text))
                {
                    MessageBox.Show("You must provide a destination name.", "Missing Name", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    txtDestName.Focus();
                    return false;
                }

                if (txtDestRT.Text.Trim().Length != 9)
                {
                    MessageBox.Show("You must provide a 9 digit destination routing transit number.", "Bad RT Number",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtDestRT.Focus();
                    return false;
                }

                if (String.IsNullOrWhiteSpace(txtCompanyName.Text))
                {
                    MessageBox.Show("You must provide a company name.", "Missing Company Name", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    txtCompanyName.Focus();
                    return false;
                }

                if (txtCompanyRT.Text.Trim().Length != 9)
                {
                    MessageBox.Show("You must provide a 9 didgit company routing transit number.", "Bad RT Number",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCompanyRT.Focus();
                    return false;
                }

                if (txtCompanyID.Text.Trim().Length != 9)
                {
                    MessageBox.Show("You must provide a 9 digit company ID.", "Bad Company ID", MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    txtCompanyID.Focus();
                    return false;
                }

                if (string.IsNullOrWhiteSpace(txtCompanyAccount.Text))
                {
                    MessageBox.Show("You must provide a company account number", "Bad Account Number",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtCompanyAccount.Focus();
                    return false;
                }

                var rsSave = new clsDRWrapper();
                rsSave.OpenRecordset("select * from tblACHInformation", "UtilityBilling");
                if (rsSave.EndOfFile())
                {
                    rsSave.AddNew();
                }
                else
                {
                    rsSave.Edit();
                }

                rsSave.Set_Fields("DestinationName", txtDestName.Text.Trim());
                rsSave.Set_Fields("DestinationRT", txtDestRT.Text.Trim());
                rsSave.Set_Fields("CompanyName", txtCompanyName.Text.Trim());
                if (String.IsNullOrWhiteSpace(txtImmediateOriginName.Text))
                {
                    rsSave.Set_Fields("OriginName", txtCompanyName.Text.Trim());
                }
                else
                {
                    rsSave.Set_Fields("OriginName", txtImmediateOriginName.Text.Trim());
                }
                rsSave.Set_Fields("CompanyRT",txtCompanyRT.Text.Trim());
                rsSave.Set_Fields("CompanyID", txtCompanyID.Text.Trim());
                if (string.IsNullOrWhiteSpace(txtImmediateOriginRT.Text))
                {
                    rsSave.Set_Fields("OriginRT",txtCompanyRT.Text.Trim());
                }
                else
                {
                    rsSave.Set_Fields("OriginRT", txtImmediateOriginRT.Text.Trim());
                }

                if (string.IsNullOrWhiteSpace(txtODFI.Text))
                {
                    rsSave.Set_Fields("ODFINum",txtCompanyRT.Text.Trim());
                }
                else
                {
                    rsSave.Set_Fields("ODFINum", txtODFI.Text.Trim());
                }
                rsSave.Set_Fields("CompanyAccount", txtCompanyAccount.Text.Trim());
                rsSave.Set_Fields("CompanyAccountType", cmbAccountType.ItemData(cmbAccountType.SelectedIndex));
                rsSave.Update();
                MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void LoadInfo()
        {
            try
            {
                var rsLoad = new clsDRWrapper();
                rsLoad.OpenRecordset("Select * from tblachinformation", "UtilityBilling");
                if (!rsLoad.EndOfFile())
                {
                    txtDestName.Text = rsLoad.Get_Fields_String("DestinationName").Trim();
                    txtDestRT.Text = rsLoad.Get_Fields_String("DestinationRT").Trim();
                    txtCompanyName.Text = rsLoad.Get_Fields_String("CompanyName").Trim();
                    txtCompanyRT.Text = rsLoad.Get_Fields_String("CompanyRT").Trim();
                    txtCompanyAccount.Text = rsLoad.Get_Fields_String("CompanyAccount").Trim();
                    txtCompanyID.Text = rsLoad.Get_Fields_String("CompanyID").Trim();
                    if (!string.IsNullOrWhiteSpace(rsLoad.Get_Fields_String("OriginName")))
                    {
                        txtImmediateOriginName.Text = rsLoad.Get_Fields_String("OriginName").Trim();
                    }
                    else
                    {
                        txtImmediateOriginName.Text = rsLoad.Get_Fields_String("CompanyName").Trim();
                    }

                    if (!string.IsNullOrWhiteSpace("OriginRT"))
                    {
                        txtImmediateOriginRT.Text = rsLoad.Get_Fields_String("OriginRT").Trim();
                    }
                    else
                    {
                        txtImmediateOriginRT.Text = rsLoad.Get_Fields_String("CompanyRT").Trim();
                    }

                    if (!string.IsNullOrWhiteSpace("ODFINum"))
                    {
                        txtODFI.Text = rsLoad.Get_Fields_String("ODFINUM").Trim();
                    }
                    else
                    {
                        txtODFI.Text = txtImmediateOriginRT.Text;
                    }

                    var intAccountType = 27;
                    if (!string.IsNullOrWhiteSpace(rsLoad.Get_Fields_String("CompanyAccountType")))
                    {
                        intAccountType = Convert.ToInt32(rsLoad.Get_Fields_String("CompanyAccountType"));
                        if (intAccountType < 20)
                        {
                            intAccountType = 27;
                        }
                    }
                    if (intAccountType == 37)
                    {
                        cmbAccountType.SelectedIndex = 1;
                    }
                    else
                    {
                        cmbAccountType.SelectedIndex = 0;
                    }

                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
        }

        private void mnuExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void mnuSaveExit_Click(object sender, EventArgs e)
        {
            SaveAndExit();
        }
    }
}
