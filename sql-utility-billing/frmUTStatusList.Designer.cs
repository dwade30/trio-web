﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTStatusList.
	/// </summary>
	partial class frmUTStatusList : BaseForm
	{
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkInclude;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraSave;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCButton cmdPrint;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCDraggableListBox lstFields;
		public fecherFoundation.FCFrame fraQuestions;
		public fecherFoundation.FCFrame fraInclude;
		public fecherFoundation.FCCheckBox chkInclude_0;
		public fecherFoundation.FCCheckBox chkInclude_1;
		public fecherFoundation.FCCheckBox chkInclude_2;
		public fecherFoundation.FCCheckBox chkInclude_3;
		public fecherFoundation.FCCheckBox chkInclude_4;
		public fecherFoundation.FCCheckBox chkPrincipalOnly;
		public fecherFoundation.FCCheckBox chkShowPayments;
		public fecherFoundation.FCCheckBox chkSummaryOnly;
		public fecherFoundation.FCComboBox cmbNameOption;
		public fecherFoundation.FCCheckBox chkCurrentInterest;
		public fecherFoundation.FCCheckBox chkUseFullStatus;
		public fecherFoundation.FCCheckBox chkHardCode;
		public fecherFoundation.FCFrame fraHardCode;
		public fecherFoundation.FCComboBox cmbHardCode;
		public fecherFoundation.FCLabel lblShowFields;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTStatusList));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.cmdClear = new fecherFoundation.FCButton();
            this.fraSave = new fecherFoundation.FCFrame();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCDraggableListBox();
            this.fraQuestions = new fecherFoundation.FCFrame();
            this.fraHardCode = new fecherFoundation.FCFrame();
            this.cmbHardCode = new fecherFoundation.FCComboBox();
            this.fraInclude = new fecherFoundation.FCFrame();
            this.chkInclude_0 = new fecherFoundation.FCCheckBox();
            this.chkInclude_1 = new fecherFoundation.FCCheckBox();
            this.chkInclude_2 = new fecherFoundation.FCCheckBox();
            this.chkInclude_3 = new fecherFoundation.FCCheckBox();
            this.chkInclude_4 = new fecherFoundation.FCCheckBox();
            this.chkPrincipalOnly = new fecherFoundation.FCCheckBox();
            this.chkShowPayments = new fecherFoundation.FCCheckBox();
            this.chkSummaryOnly = new fecherFoundation.FCCheckBox();
            this.cmbNameOption = new fecherFoundation.FCComboBox();
            this.chkCurrentInterest = new fecherFoundation.FCCheckBox();
            this.chkUseFullStatus = new fecherFoundation.FCCheckBox();
            this.chkHardCode = new fecherFoundation.FCCheckBox();
            this.lblShowFields = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
            this.fraQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraHardCode)).BeginInit();
            this.fraHardCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraInclude)).BeginInit();
            this.fraInclude.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrincipalOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPayments)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrentInterest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseFullStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHardCode)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 589);
            this.BottomPanel.Size = new System.Drawing.Size(1051, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.fraSave);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.fraQuestions);
            this.ClientArea.Size = new System.Drawing.Size(1051, 529);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(1051, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(127, 30);
            this.HeaderText.Text = "Status List";
            // 
            // fraWhere
            // 
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 559);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(584, 238);
            this.fraWhere.TabIndex = 4;
            this.fraWhere.Text = "Select Search Criteria";
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Cols = 3;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.ExtendLastCol = true;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(20, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.Size = new System.Drawing.Size(544, 188);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEdit);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
            this.vsWhere.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsWhere_MouseMoveEvent);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            this.vsWhere.Leave += new System.EventHandler(this.vsWhere_Leave);
            this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.cmdClear.Location = new System.Drawing.Point(883, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(140, 24);
            this.cmdClear.TabIndex = 9;
            this.cmdClear.Text = "Clear Search Criteria";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // fraSave
            // 
            this.fraSave.Enabled = false;
            this.fraSave.Location = new System.Drawing.Point(696, 308);
            this.fraSave.Name = "fraSave";
            this.fraSave.Size = new System.Drawing.Size(379, 232);
            this.fraSave.TabIndex = 3;
            this.fraSave.Text = "Report";
            this.fraSave.Visible = false;
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Location = new System.Drawing.Point(344, 308);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(336, 232);
            this.fraSort.TabIndex = 2;
            this.fraSort.Text = "Fields To Sort By";
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(296, 182);
            this.lstSort.Style = 1;
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.ForeColor = System.Drawing.Color.White;
            this.cmdPrint.Location = new System.Drawing.Point(510, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(80, 48);
            this.cmdPrint.TabIndex = 2;
            this.cmdPrint.Text = "Print";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Enabled = false;
            this.fraFields.Location = new System.Drawing.Point(30, 308);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(297, 232);
            this.fraFields.TabIndex = 1;
            this.fraFields.Text = "Fields To Display On Report";
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.CheckBoxes = true;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(257, 182);
            this.lstFields.Style = 1;
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            // 
            // fraQuestions
            // 
            this.fraQuestions.AppearanceKey = "groupBoxNoBorders";
            this.fraQuestions.Controls.Add(this.fraHardCode);
            this.fraQuestions.Controls.Add(this.fraInclude);
            this.fraQuestions.Controls.Add(this.chkPrincipalOnly);
            this.fraQuestions.Controls.Add(this.chkShowPayments);
            this.fraQuestions.Controls.Add(this.chkSummaryOnly);
            this.fraQuestions.Controls.Add(this.cmbNameOption);
            this.fraQuestions.Controls.Add(this.chkCurrentInterest);
            this.fraQuestions.Controls.Add(this.chkUseFullStatus);
            this.fraQuestions.Controls.Add(this.chkHardCode);
            this.fraQuestions.Controls.Add(this.lblShowFields);
            this.fraQuestions.Name = "fraQuestions";
            this.fraQuestions.Size = new System.Drawing.Size(1089, 290);
            this.fraQuestions.TabIndex = 5;
            // 
            // fraHardCode
            // 
            this.fraHardCode.Controls.Add(this.cmbHardCode);
            this.fraHardCode.Enabled = false;
            this.fraHardCode.Location = new System.Drawing.Point(415, 194);
            this.fraHardCode.Name = "fraHardCode";
            this.fraHardCode.Size = new System.Drawing.Size(336, 90);
            this.fraHardCode.TabIndex = 2;
            this.fraHardCode.Text = "Choose A Default Report";
            this.fraHardCode.Visible = false;
            // 
            // cmbHardCode
            // 
            this.cmbHardCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbHardCode.Enabled = false;
            this.cmbHardCode.Location = new System.Drawing.Point(20, 30);
            this.cmbHardCode.Name = "cmbHardCode";
            this.cmbHardCode.Size = new System.Drawing.Size(296, 40);
            this.cmbHardCode.SelectedIndexChanged += new System.EventHandler(this.cmbHardCode_SelectedIndexChanged);
            // 
            // fraInclude
            // 
            this.fraInclude.Controls.Add(this.chkInclude_0);
            this.fraInclude.Controls.Add(this.chkInclude_1);
            this.fraInclude.Controls.Add(this.chkInclude_2);
            this.fraInclude.Controls.Add(this.chkInclude_3);
            this.fraInclude.Controls.Add(this.chkInclude_4);
            this.fraInclude.Location = new System.Drawing.Point(30, 196);
            this.fraInclude.Name = "fraInclude";
            this.fraInclude.Size = new System.Drawing.Size(379, 70);
            this.fraInclude.TabIndex = 9;
            this.fraInclude.Text = "Include";
            // 
            // chkInclude_0
            // 
            this.chkInclude_0.Checked = true;
            this.chkInclude_0.CheckState = Wisej.Web.CheckState.Checked;
            this.chkInclude_0.Location = new System.Drawing.Point(20, 30);
            this.chkInclude_0.Name = "chkInclude_0";
            this.chkInclude_0.Size = new System.Drawing.Size(38, 27);
            this.chkInclude_0.Text = "P";
            this.ToolTip1.SetToolTip(this.chkInclude_0, "Show Principal on the report.");
            // 
            // chkInclude_1
            // 
            this.chkInclude_1.Checked = true;
            this.chkInclude_1.CheckState = Wisej.Web.CheckState.Checked;
            this.chkInclude_1.Location = new System.Drawing.Point(88, 30);
            this.chkInclude_1.Name = "chkInclude_1";
            this.chkInclude_1.Size = new System.Drawing.Size(37, 27);
            this.chkInclude_1.TabIndex = 1;
            this.chkInclude_1.Text = "T";
            this.ToolTip1.SetToolTip(this.chkInclude_1, "Show Tax on the report.");
            // 
            // chkInclude_2
            // 
            this.chkInclude_2.Checked = true;
            this.chkInclude_2.CheckState = Wisej.Web.CheckState.Checked;
            this.chkInclude_2.Location = new System.Drawing.Point(159, 30);
            this.chkInclude_2.Name = "chkInclude_2";
            this.chkInclude_2.Size = new System.Drawing.Size(31, 27);
            this.chkInclude_2.TabIndex = 2;
            this.chkInclude_2.Text = "I";
            this.ToolTip1.SetToolTip(this.chkInclude_2, "Show Interest on the report.");
            // 
            // chkInclude_3
            // 
            this.chkInclude_3.BackColor = System.Drawing.Color.FromName("@window");
            this.chkInclude_3.Checked = true;
            this.chkInclude_3.CheckState = Wisej.Web.CheckState.Checked;
            this.chkInclude_3.Location = new System.Drawing.Point(230, 30);
            this.chkInclude_3.Name = "chkInclude_3";
            this.chkInclude_3.Size = new System.Drawing.Size(39, 27);
            this.chkInclude_3.TabIndex = 3;
            this.chkInclude_3.Text = "C";
            this.ToolTip1.SetToolTip(this.chkInclude_3, "Show Costs on the report.");
            // 
            // chkInclude_4
            // 
            this.chkInclude_4.Checked = true;
            this.chkInclude_4.CheckState = Wisej.Web.CheckState.Checked;
            this.chkInclude_4.Enabled = false;
            this.chkInclude_4.Location = new System.Drawing.Point(303, 30);
            this.chkInclude_4.Name = "chkInclude_4";
            this.chkInclude_4.Size = new System.Drawing.Size(52, 27);
            this.chkInclude_4.TabIndex = 4;
            this.chkInclude_4.Text = "PLI";
            this.ToolTip1.SetToolTip(this.chkInclude_4, "Show Costs on the report.");
            // 
            // chkPrincipalOnly
            // 
            this.chkPrincipalOnly.Enabled = false;
            this.chkPrincipalOnly.Location = new System.Drawing.Point(696, 171);
            this.chkPrincipalOnly.Name = "chkPrincipalOnly";
            this.chkPrincipalOnly.Size = new System.Drawing.Size(174, 27);
            this.chkPrincipalOnly.TabIndex = 8;
            this.chkPrincipalOnly.Text = "Show Principal Only";
            this.chkPrincipalOnly.Visible = false;
            // 
            // chkShowPayments
            // 
            this.chkShowPayments.Location = new System.Drawing.Point(696, 77);
            this.chkShowPayments.Name = "chkShowPayments";
            this.chkShowPayments.Size = new System.Drawing.Size(146, 27);
            this.chkShowPayments.TabIndex = 6;
            this.chkShowPayments.Text = "Show Payments";
            this.ToolTip1.SetToolTip(this.chkShowPayments, "This will show all of the payments for this account.");
            // 
            // chkSummaryOnly
            // 
            this.chkSummaryOnly.Location = new System.Drawing.Point(696, 124);
            this.chkSummaryOnly.Name = "chkSummaryOnly";
            this.chkSummaryOnly.Size = new System.Drawing.Size(181, 27);
            this.chkSummaryOnly.TabIndex = 7;
            this.chkSummaryOnly.Text = "Show Summary Only";
            // 
            // cmbNameOption
            // 
            this.cmbNameOption.BackColor = System.Drawing.SystemColors.Window;
            this.cmbNameOption.Location = new System.Drawing.Point(344, 30);
            this.cmbNameOption.Name = "cmbNameOption";
            this.cmbNameOption.Size = new System.Drawing.Size(336, 40);
            this.cmbNameOption.TabIndex = 1;
            this.cmbNameOption.DropDown += new System.EventHandler(this.cmbNameOption_DropDown);
            this.cmbNameOption.KeyDown += new Wisej.Web.KeyEventHandler(this.cmbNameOption_KeyDown);
            // 
            // chkCurrentInterest
            // 
            this.chkCurrentInterest.Enabled = false;
            this.chkCurrentInterest.Location = new System.Drawing.Point(344, 90);
            this.chkCurrentInterest.Name = "chkCurrentInterest";
            this.chkCurrentInterest.Size = new System.Drawing.Size(187, 27);
            this.chkCurrentInterest.TabIndex = 3;
            this.chkCurrentInterest.Text = "Show Current Interest";
            this.chkCurrentInterest.Visible = false;
            // 
            // chkUseFullStatus
            // 
            this.chkUseFullStatus.Location = new System.Drawing.Point(696, 30);
            this.chkUseFullStatus.Name = "chkUseFullStatus";
            this.chkUseFullStatus.Size = new System.Drawing.Size(187, 27);
            this.chkUseFullStatus.TabIndex = 5;
            this.chkUseFullStatus.Text = "Show Current Interest";
            this.chkUseFullStatus.CheckedChanged += new System.EventHandler(this.chkUseFullStatus_CheckedChanged);
            // 
            // chkHardCode
            // 
            this.chkHardCode.Enabled = false;
            this.chkHardCode.Location = new System.Drawing.Point(344, 137);
            this.chkHardCode.Name = "chkHardCode";
            this.chkHardCode.Size = new System.Drawing.Size(180, 27);
            this.chkHardCode.TabIndex = 4;
            this.chkHardCode.Text = "Use a Default Report";
            this.chkHardCode.Visible = false;
            this.chkHardCode.CheckedChanged += new System.EventHandler(this.chkHardCode_CheckedChanged);
            this.chkHardCode.KeyDown += new Wisej.Web.KeyEventHandler(this.chkHardCode_KeyDown);
            // 
            // lblShowFields
            // 
            this.lblShowFields.Location = new System.Drawing.Point(30, 30);
            this.lblShowFields.Name = "lblShowFields";
            this.lblShowFields.Size = new System.Drawing.Size(284, 196);
            this.lblShowFields.TabIndex = 10;
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuFileSeperator2,
            this.mnuPrint,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 1;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 2;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Process";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 3;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuLayout
            // 
            this.mnuLayout.Enabled = false;
            this.mnuLayout.Index = -1;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Layout";
            this.mnuLayout.Visible = false;
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 0;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row";
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column";
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 2;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 3;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            // 
            // frmUTStatusList
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1051, 697);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmUTStatusList";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Status Lists";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmUTStatusList_Load);
            this.Activated += new System.EventHandler(this.frmUTStatusList_Activated);
            this.Resize += new System.EventHandler(this.frmUTStatusList_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTStatusList_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
            this.fraQuestions.ResumeLayout(false);
            this.fraQuestions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraHardCode)).EndInit();
            this.fraHardCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraInclude)).EndInit();
            this.fraInclude.ResumeLayout(false);
            this.fraInclude.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkInclude_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkPrincipalOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkShowPayments)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSummaryOnly)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkCurrentInterest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseFullStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkHardCode)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
