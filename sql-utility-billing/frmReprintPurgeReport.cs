﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReprintPurgeReport.
	/// </summary>
	public partial class frmReprintPurgeReport : BaseForm
	{
		public frmReprintPurgeReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReprintPurgeReport InstancePtr
		{
			get
			{
				return (frmReprintPurgeReport)Sys.GetInstance(typeof(frmReprintPurgeReport));
			}
		}

		protected frmReprintPurgeReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		private void frmReprintPurgeReport_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			if (cboReport.Items.Count == 0)
			{
				MessageBox.Show("No reports exist", "No Reports", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Unload();
			}
			else
			{
				cboReport.SelectedIndex = 0;
				cboReport.Focus();
			}
			this.Refresh();
		}

		private void frmReprintPurgeReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReprintPurgeReport properties;
			//frmReprintPurgeReport.FillStyle	= 0;
			//frmReprintPurgeReport.ScaleWidth	= 3885;
			//frmReprintPurgeReport.ScaleHeight	= 1995;
			//frmReprintPurgeReport.LinkTopic	= "Form2";
			//frmReprintPurgeReport.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM PurgeReports ORDER BY ReportOrder");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				cboReport.Clear();
				do
				{
					cboReport.AddItem(rsInfo.Get_Fields_Int32("ReportOrder") + " - " + "Purge Date: " + Strings.Format(rsInfo.Get_Fields_DateTime("PurgeDate"), "MM/dd/yy"));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmReprintPurgeReport_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessPreview_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM PurgeReports WHERE ReportOrder = " + Strings.Left(cboReport.Text, 1));
			frmReportViewer.InstancePtr.Init(null, "", 0, false, false, "Pages", false, TWSharedLibrary.Variables.Statics.ReportPath + "\\" + rsInfo.Get_Fields_String("FileName"));
			// frmReportViewer.ARViewer21.ReportSource.Document.Pages.Load
			// frmReportViewer.Show , MDIParent
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}
	}
}
