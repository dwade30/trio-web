﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arTextDump.
	/// </summary>
	public partial class arTextDump : BaseSectionReport
	{
		public arTextDump()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static arTextDump InstancePtr
		{
			get
			{
				return (arTextDump)Sys.GetInstance(typeof(arTextDump));
			}
		}

		protected arTextDump _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arTextDump	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/21/2004              *
		// ********************************************************
		public string strText = string.Empty;
		bool boolFirstTime;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolFirstTime;
			boolFirstTime = true;
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			if (Strings.UCase(modUTStatusList.Statics.strReportType) == "30DAYNOTICE")
			{
				lblHeader.Text = "30 DayNotice";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENPROCESS")
			{
				lblHeader.Text = "Transfer Tax To Lien";
			}
			else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LIENMATURITY")
			{
				lblHeader.Text = "Lien Maturity";
			}
			fldOut.Text = strText;
		}

		private void arTextDump_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arTextDump properties;
			//arTextDump.Icon	= "arTextDump.dsx":0000";
			//arTextDump.Left	= 0;
			//arTextDump.Top	= 0;
			//arTextDump.Width	= 11880;
			//arTextDump.Height	= 8010;
			//arTextDump.StartUpPosition	= 3;
			//arTextDump.SectionData	= "arTextDump.dsx":058A;
			//End Unmaped Properties
		}
	}
}
