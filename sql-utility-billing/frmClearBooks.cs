﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using Wisej.Web;

namespace TWUT0000
{
    /// <summary>
    /// Summary description for frmClearBooks.
    /// </summary>
    public partial class frmClearBooks : BaseForm
    {
        public frmClearBooks()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmClearBooks InstancePtr
        {
            get
            {
                return (frmClearBooks)Sys.GetInstance(typeof(frmClearBooks));
            }
        }

        protected frmClearBooks _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               03/19/2003              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               05/30/2005              *
        // ********************************************************
        string strSQL;
        int i;
        clsDRWrapper rsCMeter;
        clsDRWrapper rsClear;
        int FGWidth;

        private void cmdClear_Click(object sender, System.EventArgs e)
        {
            // this sub will attempt to clear the books selected by the user and
            // also create an output box that informs the user which books have been cleared and
            // which have not
            string strCLear;
            string strNotClear;
            strCLear = "Books Cleared: " + "\r\n";
            strNotClear = "Books Not Cleared: " + "\r\n";
            for (i = 1; i <= vsBook.Rows - 1; i++)
            {
                if (vsBook.TextMatrix(i, 0) != "")
                {
                    if (FCConvert.ToDouble(vsBook.TextMatrix(i, 0)) == -1)
                    {
                        if (Clear_Book_2(FCConvert.ToInt32(Conversion.Val(vsBook.TextMatrix(i, 1)))))
                        {
                            strCLear += vsBook.TextMatrix(i, 1) + "\r\n";
                        }
                        else
                        {
                            strNotClear += vsBook.TextMatrix(i, 1) + "\r\n";
                        }
                    }
                }
            }
            // displays a message box with the results of the attempted clear
            MessageBox.Show(strCLear + "\r\n" + strNotClear, "Cleared", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Unload();
            //Application.DoEvents();
            //MDIParent.InstancePtr.Focus();
        }

        public void cmdClear_Click()
        {
            cmdClear_Click(cmdClear, new System.EventArgs());
        }

        private void frmClearBooks_Activated(object sender, System.EventArgs e)
        {
            if (modMain.FormExist(this))
                return;
            // sets label captions
            FGWidth = vsBook.WidthOriginal;
            Format_Grid();
            // formats the grid's col width and headers ect
            Fill_Grid();
            // populates flex grid
        }

        private void frmClearBooks_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            if (KeyAscii == Keys.Escape)
            {
                // checks for the return key (ASCII 27)
                KeyAscii = (Keys)0;
                Unload();
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void frmClearBooks_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmClearBooks properties;
            //frmClearBooks.FillStyle	= 0;
            //frmClearBooks.ScaleWidth	= 9300;
            //frmClearBooks.ScaleHeight	= 7740;
            //frmClearBooks.LinkTopic	= "Form2";
            //frmClearBooks.LockControls	= true;
            //frmClearBooks.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            Text = "Clear Books";
        }

        private void frmClearBooks_Resize(object sender, System.EventArgs e)
        {
            FGWidth = vsBook.WidthOriginal;
            Format_Grid();
        }

        private void Fill_Grid()
        {
            int X;
            // temporary variable
            int LastIndex = 0;
            rsClear = new clsDRWrapper();
            strSQL = "SELECT * FROM BOOK";
            // builds the SQL statement to select all records
            rsClear.OpenRecordset(strSQL);
            // selects all the records
            if (rsClear.EndOfFile() != true && rsClear.BeginningOfFile() != true)
            {
                // if not at the beginning or the end of the recordset
                // If .RecordCount Mod 2 = 1 Then
                // LastIndex = (.RecordCount \ 2) + 1
                // Else
                // LastIndex = .RecordCount \ 2
                // End If
                LastIndex = rsClear.RecordCount();
                vsBook.Rows = LastIndex + 1;
                // calculates the number of rows used and displays extra
                for (X = 1; X <= LastIndex; X++)
                {
                    // designate the first half of the recordset
                    vsBook.TextMatrix(X, 1, Strings.Format(rsClear.Get_Fields_Int32("BookNumber"), "0000"));
                    // fills the grid
                    vsBook.TextMatrix(X, 2, FCConvert.ToString(rsClear.Get_Fields_String("Description")));
                    if (fecherFoundation.FCUtils.IsNull(rsClear.Get_Fields_String("CurrentStatus")) == false)
                    {
                        if (Strings.Left(FCConvert.ToString(rsClear.Get_Fields_String("CurrentStatus")), 1) == "C")
                        {
                            vsBook.TextMatrix(X, 3, "C - Cleared");
                            vsBook.Cell(FCGrid.CellPropertySettings.flexcpBackColor, X, 0, X, vsBook.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                        }
                        else if (Strings.Left(FCConvert.ToString(rsClear.Get_Fields_String("CurrentStatus")), 1) == "D")
                        {
                            vsBook.TextMatrix(X, 3, "D - Data Entry");
                            vsBook.Cell(FCGrid.CellPropertySettings.flexcpBackColor, X, 0, X, vsBook.Cols - 1, Color.White);
                        }
                        else if (Strings.Left(FCConvert.ToString(rsClear.Get_Fields_String("CurrentStatus")), 1) == "X")
                        {
                            vsBook.TextMatrix(X, 3, "X - Calculated");
                            vsBook.Cell(FCGrid.CellPropertySettings.flexcpBackColor, X, 0, X, vsBook.Cols - 1, Color.White);
                            // kk03302015 trouts-11  Change Billed Edit to Calc and Edit
                        }
                        else if (Strings.Left(FCConvert.ToString(rsClear.Get_Fields_String("CurrentStatus")), 1) == "E")
                        {
                            vsBook.TextMatrix(X, 3, "E - Calc and Edit");
                            vsBook.Cell(FCGrid.CellPropertySettings.flexcpBackColor, X, 0, X, vsBook.Cols - 1, Color.White);
                        }
                        else if (Strings.Left(FCConvert.ToString(rsClear.Get_Fields_String("CurrentStatus")), 1) == "W")
                        {
                            vsBook.TextMatrix(X, 3, "W - Water Billed");
                            vsBook.Cell(FCGrid.CellPropertySettings.flexcpBackColor, X, 0, X, vsBook.Cols - 1, Color.White);
                        }
                        else if (Strings.Left(FCConvert.ToString(rsClear.Get_Fields_String("CurrentStatus")), 1) == "S")
                        {
                            vsBook.TextMatrix(X, 3, "S - Sewer Billed");
                            vsBook.Cell(FCGrid.CellPropertySettings.flexcpBackColor, X, 0, X, vsBook.Cols - 1, Color.White);
                        }
                        else if (Strings.Left(FCConvert.ToString(rsClear.Get_Fields_String("CurrentStatus")), 1) == "B")
                        {
                            vsBook.TextMatrix(X, 3, "B - Billed");
                            vsBook.Cell(FCGrid.CellPropertySettings.flexcpBackColor, X, 0, X, vsBook.Cols - 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                        }
                    }
                    if (!rsClear.EndOfFile())
                        rsClear.MoveNext();
                    // if not the end of the recordset, go to the next
                }
                // For x = 1 To .RecordCount \ 2   'second half of the recordset
                // vsBook.TextMatrix(x, 5) = Format(.Fields("BookNumber"), "0000") 'fills the grid
                // vsBook.TextMatrix(x, 6) = .Fields("Description")
                // If IsNull(.Fields("CurrentStatus")) = False Then
                // Select Case Left(.Fields("CurrentStatus"), 1)
                // Case "C"
                // vsBook.TextMatrix(x, 7) = "C - Cleared"
                // Case "D"
                // vsBook.TextMatrix(x, 7) = "D - Data Entry"
                // Case "X"
                // vsBook.TextMatrix(x, 7) = "X - Calculated"
                // Case "E"
                // vsBook.TextMatrix(x, 7) = "E - Billed Edit"
                // Case "W"
                // vsBook.TextMatrix(x, 7) = "W - Water Billed"
                // Case "S"
                // vsBook.TextMatrix(x, 7) = "S - Sewer Billed"
                // Case "B"
                // vsBook.TextMatrix(x, 7) = "B - Billed"
                // End Select
                // End If
                // If Not .EndOfFile Then .MoveNext  'if not the end of the recordset, go to the next
                // Next
                //X = vsBook.RowHeight(0) * vsBook.Rows; // calculates the total height of the flexgrid
                //if (X < this.Height * 0.8)
                //{
                //    vsBook.Height = X + 75; // if area used is less then start, decrease the unused grey area
                //    vsBook.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
                //}
                //else
                //{
                //    vsBook.Height = FCConvert.ToInt32(this.Height * 0.8);
                //    vsBook.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                //}
            }
            // this will create a vertical line to seperate the two lists of books
            // vsBook.Select 0, 4, vsBook.rows - 1, 4
            // vsBook.CellBorder 1, 2, 0, 0, 0, 1, 0
            vsBook.Select(0, 0);
        }

        private void mnuClearBooks_Click(object sender, System.EventArgs e)
        {
            cmdClear_Click();
        }

        private void mnuProcessQuit_Click(object sender, System.EventArgs e)
        {
            Unload();
        }
        // vbPorter upgrade warning: Num As int	OnWriteFCConvert.ToDouble(
        private bool Clear_Book_2(int Num)
        {
            return Clear_Book(ref Num);
        }

        private bool Clear_Book(ref int Num)
        {
            // this function will clear the individual books and return true if completed, otherwise false
            bool IsClear = true;
            bool Clear_Book = false;
            rsCMeter = new clsDRWrapper();

            try
            {
                rsCMeter.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(Num) + " ORDER BY Sequence");
                if (rsCMeter.RecordCount() > 0)
                {
                    // records are returned, then fill the populate the recordset
                    while (rsCMeter.EndOfFile() == false)
                    {
                        // loop through each meter
                        var billingStatus = rsCMeter.Get_Fields_String("BillingStatus");

                        if (billingStatus != null)
                        {
                            if (billingStatus == "B" || billingStatus == "C" || billingStatus == "")
                            {
                                // meters can only be cleared if they are status 'B'
                                var currentReading = rsCMeter.Get_Fields_Int32("CurrentReading");

                                if (fecherFoundation.FCUtils.IsNull(currentReading) == false)
                                {
                                    rsCMeter.Edit();

                                    // kk 110812 trout-884 Change so -1 is NoReading since 0 could be a valid reading
                                    if (currentReading != -1)
                                    {
                                        // If .Fields("CurrentReading") <> 0 Then
                                        // to clear the book, the current reading is moved to the previous reading if it has a current reading of something other than 0
                                        if (FCConvert.ToString(billingStatus) != "C")
                                        {
                                            // kk04242014 trouts-64   Don't move the readings if the book is already cleared
                                            rsCMeter.Set_Fields("PreviousReading", currentReading);
                                            rsCMeter.Set_Fields("PreviousReadingDate", rsCMeter.Get_Fields_DateTime("CurrentReadingDate"));
                                        }
                                    }

                                    rsCMeter.Set_Fields("BillingStatus", "C");

                                    // change status to Cleared
                                    rsCMeter.Set_Fields("CurrentReading", -1);

                                    // reset current reading
                                    rsCMeter.Set_Fields("CurrentReadingDate", 0);
                                    rsCMeter.Update();
                                }
                            }
                            else
                            {
                                if (billingStatus != "C") IsClear = false;
                            }
                        }

                        rsCMeter.MoveNext();
                    }
                }
                else
                {
                    MessageBox.Show("There are no meters in book #" + FCConvert.ToString(Num) + " to be cleared.  Book status set to cleared.", "No Meters", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            finally
            {
                rsCMeter.DisposeOf();
            }

            if (IsClear)
            {
                // if all meters are cleared, then clear the book
                rsClear = new clsDRWrapper();

                try
                {
                    rsClear.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(Num));
                    rsClear.MoveFirst();
                    rsClear.Edit();
                    rsClear.Set_Fields("CurrentStatus", "CE");
                    // update the status of the book
                    rsClear.Set_Fields("CDate", DateTime.Now);
                    rsClear.Update();
                }
                finally
                {
                    rsClear.DisposeOf();
                }
            }
            Clear_Book = IsClear;

            // return true if all meters and the book are cleared
            return Clear_Book;
        }

        private void vsBook_ClickEvent(object sender, System.EventArgs e)
        {
            int lngMR;
            int lngMC;
            int lngCurrent = 0;
            // Store Current State of Checkbox
            lngMR = vsBook.MouseRow;
            lngMC = vsBook.MouseCol;
            // so the user can only change the descriptions of the book
            if (vsBook.Col == 0)
            {
                // Or .Col = 4 Or .Col = 6  Then  '.Col = 2 Or
                lngCurrent = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBook.TextMatrix(vsBook.Row, vsBook.Col))));
                //FC:FINAL:CHN - issue #955: Incorrect selection
                //vsBook.EditCell();
            }
            else
            {
                vsBook.Editable = FCGrid.EditableSettings.flexEDNone;
            }
            if (vsBook.Col == 0)
            {
                // Or .Col = 4 Then
                if (Strings.Left(vsBook.TextMatrix(vsBook.Row, vsBook.Col + 3), 1) == "B" || Strings.Left(vsBook.TextMatrix(vsBook.Row, vsBook.Col + 3), 1) == "C")
                {
                    // MAL@20070904: Added check for already selected to toggle selection
                    if (lngCurrent == -1)
                    {
                        vsBook.TextMatrix(vsBook.Row, vsBook.Col, FCConvert.ToString(0));
                    }
                    else
                    {
                        lblError.Visible = false;
                        vsBook.TextMatrix(vsBook.Row, vsBook.Col, FCConvert.ToString(-1));
                        return;
                    }
                }
                else
                {
                    lblError.Visible = true;
                    vsBook.TextMatrix(vsBook.Row, vsBook.Col, FCConvert.ToString(0));
                    return;
                }
            }
            lblError.Visible = false;
        }

        private void Format_Grid()
        {
            vsBook.Cols = 4;
            vsBook.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
            // sets col 0 and 4 to checkboxes
            vsBook.ColWidth(0, FCConvert.ToInt32(FGWidth * 0.05 * 2));
            // check boxes
            vsBook.ColWidth(1, FCConvert.ToInt32(FGWidth * 0.07 * 2));
            // book number
            vsBook.ColWidth(2, FCConvert.ToInt32(FGWidth * 0.19 * 2));
            // description
            vsBook.ColWidth(3, FCConvert.ToInt32(FGWidth * 0.18 * 2));
            // status
            vsBook.TextMatrix(0, 1, "Book");
            // fills the column titles
            vsBook.TextMatrix(0, 2, "Description");
            vsBook.TextMatrix(0, 3, "Status");
            // .TextMatrix(0, 5) = "Book"
            // .TextMatrix(0, 6) = "Description"
            // .TextMatrix(0, 7) = "Status"
            //vsBook.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 1, 0, vsBook.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
        }

        private void SelectAllBooks()
        {
            int lngMR;
            int lngMC;
            int lngCT;
            lngMR = vsBook.MouseRow;
            lngMC = vsBook.MouseCol;
            if (lngMR == 0 && lngMC == 0)
            {
                // this is a double click on the top row and the first col which will select all of the rows
                for (lngCT = 1; lngCT <= vsBook.Rows - 1; lngCT++)
                {
                    if (vsBook.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngCT, 0) != ColorTranslator.ToOle(Color.White))
                    {
                        vsBook.TextMatrix(lngCT, 0, FCConvert.ToString(-1));
                    }
                }
            }
        }

        private void vsBook_DblClick(object sender, System.EventArgs e)
        {
            SelectAllBooks();
        }
    }
}
