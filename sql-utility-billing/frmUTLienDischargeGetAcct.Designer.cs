﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTLienDischargeGetAcct.
	/// </summary>
	partial class frmUTLienDischargeGetAcct : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public fecherFoundation.FCCheckBox chkReprint;
		public fecherFoundation.FCCheckBox chkAllNotices;
		public fecherFoundation.FCFrame fraSingle;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtAcct;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTLienDischargeGetAcct));
			this.cmbWS = new fecherFoundation.FCComboBox();
			this.lblWS = new fecherFoundation.FCLabel();
			this.chkReprint = new fecherFoundation.FCCheckBox();
			this.chkAllNotices = new fecherFoundation.FCCheckBox();
			this.fraSingle = new fecherFoundation.FCFrame();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.txtAcct = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkReprint)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAllNotices)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSingle)).BeginInit();
			this.fraSingle.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 414);
			this.BottomPanel.Size = new System.Drawing.Size(393, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cmbWS);
			this.ClientArea.Controls.Add(this.lblWS);
			this.ClientArea.Controls.Add(this.chkReprint);
			this.ClientArea.Controls.Add(this.chkAllNotices);
			this.ClientArea.Controls.Add(this.fraSingle);
			this.ClientArea.Size = new System.Drawing.Size(393, 354);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(393, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(234, 30);
			this.HeaderText.Text = "Print Lien Discharge";
			// 
			// cmbWS
			// 
			this.cmbWS.AutoSize = false;
			this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWS.FormattingEnabled = true;
			this.cmbWS.Items.AddRange(new object[] {
				"Water",
				"Sewer"
			});
			this.cmbWS.Location = new System.Drawing.Point(187, 30);
			this.cmbWS.Name = "cmbWS";
			this.cmbWS.Size = new System.Drawing.Size(175, 40);
			this.cmbWS.TabIndex = 1;
			this.cmbWS.Text = "Water";
			this.cmbWS.SelectedIndexChanged += new System.EventHandler(this.optWS_CheckedChanged);
			// 
			// lblWS
			// 
			this.lblWS.AutoSize = true;
			this.lblWS.Location = new System.Drawing.Point(30, 44);
			this.lblWS.Name = "lblWS";
			this.lblWS.Size = new System.Drawing.Size(107, 15);
			this.lblWS.TabIndex = 0;
			this.lblWS.Text = "WATER / SEWER";
			// 
			// chkReprint
			// 
			this.chkReprint.Location = new System.Drawing.Point(30, 140);
			this.chkReprint.Name = "chkReprint";
			this.chkReprint.Size = new System.Drawing.Size(162, 27);
			this.chkReprint.TabIndex = 3;
			this.chkReprint.Text = "Reprint Last Batch";
			this.chkReprint.CheckedChanged += new System.EventHandler(this.chkReprint_CheckedChanged);
			// 
			// chkAllNotices
			// 
			this.chkAllNotices.Checked = true;
			this.chkAllNotices.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
			this.chkAllNotices.Location = new System.Drawing.Point(30, 90);
			this.chkAllNotices.Name = "chkAllNotices";
			this.chkAllNotices.Size = new System.Drawing.Size(194, 27);
			this.chkAllNotices.TabIndex = 2;
			this.chkAllNotices.Text = "Print All Saved Notices";
			this.chkAllNotices.CheckedChanged += new System.EventHandler(this.chkAllNotices_CheckedChanged);
			// 
			// fraSingle
			// 
			this.fraSingle.Controls.Add(this.cmbYear);
			this.fraSingle.Controls.Add(this.txtAcct);
			this.fraSingle.Controls.Add(this.Label1);
			this.fraSingle.Controls.Add(this.Label2);
			this.fraSingle.Enabled = false;
			this.fraSingle.Location = new System.Drawing.Point(30, 190);
			this.fraSingle.Name = "fraSingle";
			this.fraSingle.Size = new System.Drawing.Size(332, 150);
			this.fraSingle.TabIndex = 4;
			this.fraSingle.Text = "Single Account";
			this.fraSingle.Click += new System.EventHandler(this.fraSingle_Click);
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.Enabled = false;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(146, 90);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(166, 40);
			this.cmbYear.TabIndex = 3;
			this.cmbYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.cmbYear_KeyPress);
			this.cmbYear.Validating += new System.ComponentModel.CancelEventHandler(this.cmbYear_Validating);
			// 
			// txtAcct
			// 
			this.txtAcct.AutoSize = false;
			this.txtAcct.BackColor = System.Drawing.SystemColors.Window;
			this.txtAcct.LinkItem = null;
			this.txtAcct.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAcct.LinkTopic = null;
			this.txtAcct.Location = new System.Drawing.Point(146, 30);
			this.txtAcct.Name = "txtAcct";
			this.txtAcct.Size = new System.Drawing.Size(166, 40);
			this.txtAcct.TabIndex = 1;
			this.txtAcct.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtAcct_KeyPress);
			this.txtAcct.Validating += new System.ComponentModel.CancelEventHandler(this.txtAcct_Validating);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(20, 44);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(70, 18);
			this.Label1.TabIndex = 0;
			this.Label1.Text = "ACCOUNT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 104);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(54, 18);
			this.Label2.TabIndex = 2;
			this.Label2.Text = "BILL";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileSave,
				this.mnuFileSeperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 0;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSave.Text = "Save && Continue";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 1;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "acceptButton";
			this.cmdFileSave.Location = new System.Drawing.Point(120, 30);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSave.Size = new System.Drawing.Size(175, 48);
			this.cmdFileSave.TabIndex = 0;
			this.cmdFileSave.Text = "Save & Continue";
			this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// frmUTLienDischargeGetAcct
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(393, 522);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmUTLienDischargeGetAcct";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Print Lien Discharge";
			this.Load += new System.EventHandler(this.frmUTLienDischargeGetAcct_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTLienDischargeGetAcct_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkReprint)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkAllNotices)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraSingle)).EndInit();
			this.fraSingle.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSave;
	}
}
