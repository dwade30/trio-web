﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmLienMaturityFees.
	/// </summary>
	partial class frmLienMaturityFees : BaseForm
	{
		public fecherFoundation.FCPanel fraGrid;
		public Global.T2KDateBox txtIntDate;
		public fecherFoundation.FCCheckBox chkIntApplied;
		public fecherFoundation.FCGrid vsDemand;
		//public fecherFoundation.FCLine lnDemand;
		public fecherFoundation.FCLabel lblInstruction;
		public fecherFoundation.FCPanel fraValidate;
		public FCGrid vsValidate;
		//public fecherFoundation.FCLine lnValidate;
		public fecherFoundation.FCLabel lblValidateInstruction;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelectAll;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLienMaturityFees));
			this.fraGrid = new fecherFoundation.FCPanel();
			this.txtIntDate = new Global.T2KDateBox();
			this.chkIntApplied = new fecherFoundation.FCCheckBox();
			this.vsDemand = new fecherFoundation.FCGrid();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.fraValidate = new fecherFoundation.FCPanel();
			this.vsValidate = new fecherFoundation.FCGrid();
			this.lblValidateInstruction = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSelectAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.cmdFileClear = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).BeginInit();
			this.fraGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtIntDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIntApplied)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraValidate)).BeginInit();
			this.fraValidate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsValidate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 578);
			this.BottomPanel.Size = new System.Drawing.Size(689, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraValidate);
			this.ClientArea.Controls.Add(this.fraGrid);
			this.ClientArea.Size = new System.Drawing.Size(689, 518);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileClear);
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Size = new System.Drawing.Size(689, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(265, 30);
			this.HeaderText.Text = "Add Lien Maturity Fees";
			// 
			// fraGrid
			// 
			this.fraGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraGrid.AppearanceKey = "groupBoxNoBorders";
			this.fraGrid.Controls.Add(this.txtIntDate);
			this.fraGrid.Controls.Add(this.chkIntApplied);
			this.fraGrid.Controls.Add(this.vsDemand);
			this.fraGrid.Controls.Add(this.lblInstruction);
			this.fraGrid.Location = new System.Drawing.Point(0, 0);
			this.fraGrid.Name = "fraGrid";
			this.fraGrid.Size = new System.Drawing.Size(659, 479);
			this.fraGrid.TabIndex = 1;
			this.fraGrid.Visible = false;
			// 
			// txtIntDate
			// 
			this.txtIntDate.Enabled = false;
			this.txtIntDate.Location = new System.Drawing.Point(433, 90);
			this.txtIntDate.Mask = "##/##/####";
			this.txtIntDate.Name = "txtIntDate";
			this.txtIntDate.Size = new System.Drawing.Size(115, 40);
			this.txtIntDate.TabIndex = 2;
			this.txtIntDate.Text = "  /  /";
			this.txtIntDate.Visible = false;
			// 
			// chkIntApplied
			// 
			this.chkIntApplied.Location = new System.Drawing.Point(30, 98);
			this.chkIntApplied.Name = "chkIntApplied";
			this.chkIntApplied.Size = new System.Drawing.Size(386, 27);
			this.chkIntApplied.TabIndex = 1;
			this.chkIntApplied.Text = "Interest Applied Through Date:  (for grace period)";
			this.chkIntApplied.Visible = false;
			this.chkIntApplied.CheckedChanged += new System.EventHandler(this.chkIntApplied_CheckedChanged);
			// 
			// vsDemand
			// 
			this.vsDemand.AllowSelection = false;
			this.vsDemand.AllowUserToResizeColumns = false;
			this.vsDemand.AllowUserToResizeRows = false;
			this.vsDemand.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsDemand.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDemand.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDemand.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDemand.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.BackColorSel = System.Drawing.Color.Empty;
			this.vsDemand.Cols = 5;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDemand.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsDemand.ColumnHeadersHeight = 30;
			this.vsDemand.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDemand.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsDemand.DragIcon = null;
			this.vsDemand.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDemand.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsDemand.ExtendLastCol = true;
			this.vsDemand.FixedCols = 0;
			this.vsDemand.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.FrozenCols = 0;
			this.vsDemand.GridColor = System.Drawing.Color.Empty;
			this.vsDemand.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.Location = new System.Drawing.Point(30, 145);
			this.vsDemand.Name = "vsDemand";
			this.vsDemand.ReadOnly = true;
			this.vsDemand.RowHeadersVisible = false;
			this.vsDemand.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDemand.RowHeightMin = 0;
			this.vsDemand.Rows = 1;
			this.vsDemand.ScrollTipText = null;
			this.vsDemand.ShowColumnVisibilityMenu = false;
			this.vsDemand.Size = new System.Drawing.Size(599, 313);
			this.vsDemand.StandardTab = true;
			this.vsDemand.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDemand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDemand.TabIndex = 3;
			this.vsDemand.CurrentCellChanged += new System.EventHandler(this.vsDemand_RowColChange);
			this.vsDemand.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDemand_MouseMoveEvent);
			// 
			// lblInstruction
			// 
			this.lblInstruction.Location = new System.Drawing.Point(30, 30);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(596, 41);
			this.lblInstruction.TabIndex = 0;
			this.lblInstruction.Visible = false;
			// 
			// fraValidate
			// 
			this.fraValidate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraValidate.AppearanceKey = "groupBoxNoBorders";
			this.fraValidate.Controls.Add(this.vsValidate);
			this.fraValidate.Controls.Add(this.lblValidateInstruction);
			this.fraValidate.Location = new System.Drawing.Point(0, 0);
			this.fraValidate.Name = "fraValidate";
			this.fraValidate.Size = new System.Drawing.Size(474, 398);
			this.fraValidate.TabIndex = 0;
			this.fraValidate.Visible = false;
			// 
			// vsValidate
			// 
			this.vsValidate.AllowSelection = false;
			this.vsValidate.AllowUserToResizeColumns = false;
			this.vsValidate.AllowUserToResizeRows = false;
			this.vsValidate.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsValidate.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsValidate.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsValidate.BackColorBkg = System.Drawing.Color.Empty;
			this.vsValidate.BackColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.BackColorSel = System.Drawing.Color.Empty;
			this.vsValidate.Cols = 2;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsValidate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsValidate.ColumnHeadersHeight = 30;
			this.vsValidate.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsValidate.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsValidate.DragIcon = null;
			this.vsValidate.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsValidate.ExtendLastCol = true;
			this.vsValidate.FixedCols = 0;
			this.vsValidate.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.FrozenCols = 0;
			this.vsValidate.GridColor = System.Drawing.Color.Empty;
			this.vsValidate.GridColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.Location = new System.Drawing.Point(30, 90);
			this.vsValidate.Name = "vsValidate";
			this.vsValidate.ReadOnly = true;
			this.vsValidate.RowHeadersVisible = false;
			this.vsValidate.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsValidate.RowHeightMin = 0;
			this.vsValidate.Rows = 1;
			this.vsValidate.ScrollTipText = null;
			this.vsValidate.ShowColumnVisibilityMenu = false;
			this.vsValidate.Size = new System.Drawing.Size(414, 288);
			this.vsValidate.StandardTab = true;
			this.vsValidate.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsValidate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsValidate.TabIndex = 0;
			// 
			// lblValidateInstruction
			// 
			this.lblValidateInstruction.Location = new System.Drawing.Point(30, 30);
			this.lblValidateInstruction.Name = "lblValidateInstruction";
			this.lblValidateInstruction.Size = new System.Drawing.Size(414, 41);
			this.lblValidateInstruction.TabIndex = 2;
			this.lblValidateInstruction.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileClear,
				this.mnuFileSelectAll,
				this.mnuFileSeperator,
				this.mnuFileSave,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileClear
			// 
			this.mnuFileClear.Index = 0;
			this.mnuFileClear.Name = "mnuFileClear";
			this.mnuFileClear.Text = "Clear All";
			this.mnuFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// mnuFileSelectAll
			// 
			this.mnuFileSelectAll.Index = 1;
			this.mnuFileSelectAll.Name = "mnuFileSelectAll";
			this.mnuFileSelectAll.Text = "Select All";
			this.mnuFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 2;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 3;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSave.Text = "Process";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 5;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "acceptButton";
			this.cmdFileSave.Location = new System.Drawing.Point(288, 30);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSave.Size = new System.Drawing.Size(100, 48);
			this.cmdFileSave.TabIndex = 0;
			this.cmdFileSave.Text = "Process";
			this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(511, 29);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(76, 24);
			this.cmdFileSelectAll.TabIndex = 1;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// cmdFileClear
			// 
			this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClear.AppearanceKey = "toolbarButton";
			this.cmdFileClear.Location = new System.Drawing.Point(593, 29);
			this.cmdFileClear.Name = "cmdFileClear";
			this.cmdFileClear.Size = new System.Drawing.Size(68, 24);
			this.cmdFileClear.TabIndex = 2;
			this.cmdFileClear.Text = "Clear All";
			this.cmdFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// frmLienMaturityFees
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(689, 686);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLienMaturityFees";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Add Lien Maturity Fees";
			this.Load += new System.EventHandler(this.frmLienMaturityFees_Load);
			this.Activated += new System.EventHandler(this.frmLienMaturityFees_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLienMaturityFees_KeyPress);
			this.Resize += new System.EventHandler(this.frmLienMaturityFees_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).EndInit();
			this.fraGrid.ResumeLayout(false);
			this.fraGrid.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtIntDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkIntApplied)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraValidate)).EndInit();
			this.fraValidate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsValidate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSave;
		private FCButton cmdFileClear;
		private FCButton cmdFileSelectAll;
	}
}
