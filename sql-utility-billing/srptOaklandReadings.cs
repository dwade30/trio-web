﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using System;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptOaklandReadings.
	/// </summary>
	public partial class srptOaklandReadings : FCSectionReport
	{
		public srptOaklandReadings()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Daily Audit Detail";
		}

		public static srptOaklandReadings InstancePtr
		{
			get
			{
				return (srptOaklandReadings)Sys.GetInstance(typeof(srptOaklandReadings));
			}
		}

		protected srptOaklandReadings _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptOaklandReadings	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               10/10/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/15/2004              *
		// ********************************************************
		bool blnFirstRecord;
		int lngCounter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Consumption > 0)
				{
					eArgs.EOF = false;
				}
				else
				{
					//TODO
					//goto MoveNext;
				}
			}
			else
			{
				MoveNext:
				;
				lngCounter += 1;
				if (lngCounter <= Information.UBound(modTSCONSUM.Statics.OaklandReportInfo, 1))
				{
					if (modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Consumption > 0)
					{
						eArgs.EOF = false;
					}
					else
					{
						goto MoveNext;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lngCounter = 0;
			blnFirstRecord = true;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (lngCounter > Information.UBound(modTSCONSUM.Statics.OaklandReportInfo, 1))
			{
				fldAccountNumber.Text = "";
				fldName.Text = "";
				fldBook.Text = "";
				fldSequence.Text = "";
				fldReading.Text = "";
				fldDate.Text = "";
			}
			else
			{
				fldAccountNumber.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].AccountNumber.ToString();
				fldName.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].OwnerName;
				fldBook.Text = Strings.Format(modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Book, "0000");
				fldSequence.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Sequence.ToString();
				fldReading.Text = modTSCONSUM.Statics.OaklandReportInfo[lngCounter].Consumption.ToString();
				fldDate.Text = Strings.Format(modTSCONSUM.Statics.OaklandReportInfo[lngCounter].ReadingDate, "MM/dd/yyyy");
			}
		}

		private void srptOaklandReadings_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptOaklandReadings properties;
			//srptOaklandReadings.Caption	= "Daily Audit Detail";
			//srptOaklandReadings.Icon	= "srptOaklandReadings.dsx":0000";
			//srptOaklandReadings.Left	= 0;
			//srptOaklandReadings.Top	= 0;
			//srptOaklandReadings.Width	= 11880;
			//srptOaklandReadings.Height	= 8175;
			//srptOaklandReadings.StartUpPosition	= 3;
			//srptOaklandReadings.SectionData	= "srptOaklandReadings.dsx":058A;
			//End Unmaped Properties
		}
	}
}
