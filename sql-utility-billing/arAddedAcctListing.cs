﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arAddedAcctListing.
	/// </summary>
	public partial class arAddedAcctListing : BaseSectionReport
	{
		public arAddedAcctListing()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Added Account Listing";
		}

		public static arAddedAcctListing InstancePtr
		{
			get
			{
				return (arAddedAcctListing)Sys.GetInstance(typeof(arAddedAcctListing));
			}
		}

		protected arAddedAcctListing _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arAddedAcctListing	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :                                       *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :                                       *
		// LAST UPDATED   :                                       *
		// ********************************************************
		// Display the accounts added by the RE/UT Sync routine for Bangor Stormwater
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsAddress = new clsDRWrapper();
		int lngAcctCount;
		int lngCurrentAcct;
		bool blnFirstRecord;
		int lngBook;

		public void Init(ref int lngPassBook)
		{
			lngBook = lngPassBook;
			frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal));
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				if (rsData.EndOfFile())
				{
					eArgs.EOF = true;
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					lngCurrentAcct = FCConvert.ToInt32(rsData.Get_Fields("AccountNumber"));
					eArgs.EOF = false;
				}
			}
			else
			{
				CheckAgain:
				;
				rsData.MoveNext();
				if (rsData.EndOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					if (lngCurrentAcct != FCConvert.ToInt32(rsData.Get_Fields("AccountNumber")))
					{
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						lngCurrentAcct = FCConvert.ToInt32(rsData.Get_Fields("AccountNumber"));
						eArgs.EOF = false;
					}
					else
					{
						goto CheckAgain;
					}
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int ct;
				string strMSQL = "";
				bool boolDone;
				// keep a running total
				lngAcctCount += 1;
				// primary line
				//FC:FINAL:MSH - can't implicitly convert from int to string
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccountNum.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) != "")
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					fldLocation.Text = Strings.Trim(rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
				}
				else
				{
					fldLocation.Text = Strings.Trim(" " + rsData.Get_Fields_String("StreetName"));
				}
				// RE Account
				fldRealEstateAccount.Text = FCConvert.ToString(rsData.Get_Fields_Int32("REAccount"));
				// Owner Name
				// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("SecondOwnerName"))) != "")
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					fldOwnerName.Text = FCConvert.ToString(rsData.Get_Fields("OwnerName")) + " and " + FCConvert.ToString(rsData.Get_Fields("SecondOwnerName"));
				}
				else
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					fldOwnerName.Text = FCConvert.ToString(rsData.Get_Fields("OwnerName"));
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			// Data.RecordSource = strPrintSQL
			strSQL = BuildSQL();
			blnFirstRecord = true;
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			// fill the page header labels
			FillHeaderLabels();
			SetupDetailFormat();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// This function will create the string for this report
			BuildSQL = "SELECT AccountNumber, p1.FullNameLF AS OwnerName, p2.FullNameLF AS SecondOwnerName, StreetName, StreetNumber,REAccount,MapLot " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p1 ON Master.OwnerPartyID = p1.ID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView p2 ON Master.SecondOwnerPartyID = p2.ID " + "WHERE Master.UTBook = " + FCConvert.ToString(lngBook) + " ORDER BY StreetName, StreetNumber";
			return BuildSQL;
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void SetupDetailFormat()
		{
			// this sub will get all of the users options from the Account Listing Form adjust the fields accordingly
			// As it keeps track of how many fields/lines the report will need for each account, the height
			// of each group will be adjusted
			// Dim lngLine             As Long
			// Dim lngHeight           As Long
			// Dim lngStarting         As Long
			// Dim lngCurrentTop       As Long
			// Dim intCT               As Integer
			// Dim Xtra                As Integer  'extra lines
			// 
			// lngStarting = 0
			// lngLine = 1
			// lngHeight = 240
			// 
			// boolShowMeterInformation = False
			// 
			// HideAllFields
			// 
			// gets info from the last form that is hidden, not unloaded...
			// With frmAccountListing
			// For intCT = 0 To .lstFields.ListCount - 1
			// If .lstFields.Selected(intCT) Then
			// lngCurrentTop = (lngLine * lngHeight) + lngStarting
			// Select Case .lstFields.ItemData(intCT)
			// Case 1  'Address                'Master
			// fldAddress1.Visible = True
			// fldAddress2.Visible = True
			// fldAddress3.Visible = True
			// 
			// fldAddress1.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// lngCurrentTop = (lngLine * lngHeight) + lngStarting
			// fldAddress2.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// lngCurrentTop = (lngLine * lngHeight) + lngStarting
			// fldAddress3.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 2  'Book                   'Master
			// fldBook.Visible = True
			// 
			// fldBook.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 3  'Billing Type           'Meter
			// boolShowMeterInformation = True
			// Case 4  'Bill Message           'Master
			// fldBillMessage.Visible = True
			// 
			// fldBillMessage.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 5  'Bill To Same AS Owner  'Master
			// fldBillToSameAsOwner.Visible = True
			// 
			// fldBillToSameAsOwner.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 6  'Combination Type       'Meter
			// boolShowMeterInformation = True
			// Case 7  'Current Reading        'Meter
			// boolShowMeterInformation = True
			// Case 8  'Data Entry Message     'Master
			// fldDataEntryMessage.Visible = True
			// 
			// fldDataEntryMessage.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 9  'Meter Final Bill       'Meter
			// boolShowMeterInformation = True
			// Case 10 'Meter Size             'Meter
			// boolShowMeterInformation = True
			// Case 11 'Meter No Bill          'Meter
			// boolShowMeterInformation = True
			// Case 12 'Owner Name             'Master
			// fldOwnerName.Visible = True
			// 
			// fldOwnerName.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 13 'Previous Reading       'Meter
			// boolShowMeterInformation = True
			// Case 14 'RE Account             'Master
			// fldRealEstateAccount.Visible = True
			// 
			// fldRealEstateAccount.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 15 'Sequence               'Meter
			// boolShowMeterInformation = True
			// Case 16 'Service                'Meter
			// boolShowMeterInformation = True
			// Case 17 'Sewer Master Account   'Master
			// fldSewerMasterAccount.Visible = True
			// 
			// fldSewerMasterAccount.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 18 'Sewer RT               'Meter
			// boolShowMeterInformation = True
			// Case 19 'Water Master Account   'Master
			// fldWaterMasterAccount.Visible = True
			// 
			// fldWaterMasterAccount.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 20 'Water RT               'Meter
			// boolShowMeterInformation = True
			// Case 21 'Account Final Bill     'Master
			// fldFinalBill.Visible = True
			// 
			// fldFinalBill.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 22 'Account No Bill        'Master
			// fldNoBill.Visible = True
			// 
			// fldNoBill.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 23
			// fldWaterCategory.Visible = True
			// 
			// fldWaterCategory.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 24
			// fldSewerCategory.Visible = True
			// 
			// fldSewerCategory.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 25
			// fldMapLot.Visible = True
			// fldMapLot.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 26
			// fldPhoneNumber.Visible = True
			// fldPhoneNumber.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 27
			// fldUseREAccount.Visible = True
			// fldUseREAccount.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// MAL@20080527: 13713
			// Case 28
			// fldWaterTax.Visible = True
			// fldWaterTax.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 29
			// fldSewerTax.Visible = True
			// fldSewerTax.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 30
			// fldSerialNumber.Visible = True
			// fldSerialNumber.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 31
			// fldRemoteNumber.Visible = True
			// fldRemoteNumber.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 32
			// fldMXUNumber.Visible = True
			// fldMXUNumber.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 33
			// fldReadType.Visible = True
			// fldReadType.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 34
			// fldLatitude.Visible = True
			// fldLatitude.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// 
			// Case 35
			// fldLongitude.Visible = True
			// fldLongitude.Top = lngCurrentTop
			// lngLine = lngLine + 1
			// Case Else
			// do nothing
			// End Select
			// End If
			// Next
			// 
			// Xtra = 1 'Val(.cmbSpace.List(.cmbSpace.ListIndex))
			// End With
			// 
			// this sets the heights of the detail section of the report to one line past the last entry
			// 
			// If boolShowMeterInformation Then
			// Detail.CanGrow = True
			// Else
			// IncreaseFieldWidth
			// Detail.CanGrow = True
			// End If
			// Detail.Height = (lngLine + Xtra) * lngHeight
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// MAL@20080227: Add check for empty recordset
			// Call Reference: 101984
			if (!rsData.EndOfFile())
			{
				BindFields();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			lblFooter.Text = FCConvert.ToString(lngAcctCount) + " accounts processed.";
		}

		private void arAddedAcctListing_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arAddedAcctListing properties;
			//arAddedAcctListing.Caption	= "Added Account Listing";
			//arAddedAcctListing.Icon	= "arAddedAcctListing.dsx":0000";
			//arAddedAcctListing.Left	= 0;
			//arAddedAcctListing.Top	= 0;
			//arAddedAcctListing.Width	= 15240;
			//arAddedAcctListing.Height	= 11115;
			//arAddedAcctListing.WindowState	= 2;
			//arAddedAcctListing.SectionData	= "arAddedAcctListing.dsx":058A;
			//End Unmaped Properties
		}
	}
}
