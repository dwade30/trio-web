﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptMeterChangeOutHistory.
	/// </summary>
	public partial class rptMeterChangeOutHistory : BaseSectionReport
	{
		public rptMeterChangeOutHistory()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Change Out History";
		}

		public static rptMeterChangeOutHistory InstancePtr
		{
			get
			{
				return (rptMeterChangeOutHistory)Sys.GetInstance(typeof(rptMeterChangeOutHistory));
			}
		}

		protected rptMeterChangeOutHistory _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMeterChangeOutHistory	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/07/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/01/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		//clsDRWrapper rsTemp = new clsDRWrapper();
		int lngCount;
		string strAccount = "";
		DateTime dtStartDate;
		DateTime dtEndDate;
		int lngTop;
		int lngMax;
		int lngAccount;
		int lngSum;
		int intCount;
		int intTotalRecords;
		public string strWhere = string.Empty;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			if (eArgs.EOF)
			{
				return;
			}
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intRecCount;
				//FC:FINAL:DDU:#i933 - add value before unloading form
				//strWhere = frmChangeOutHistoryReport.InstancePtr.strSQL;
				lblCriteria.Text = "Report showing meter change out history.";
				// rsData.OpenRecordset "SELECT AccountNumber, Name, Name2, SetDate, SerialNumber, Remote, Size, Digits, Multiplier, LastReading, ChangeOutReason "
				// & "FROM tblPreviousMeter INNER JOIN "
				// & "(SELECT Master.ID, AccountNumber, pBill.FullNameLF AS Name, pBill2.FullNameLF AS Name2 FROM Master INNER JOIN " & strDbCP & "PartyNameView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN " & strDbCP & "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID) AS qTmp "
				// & "ON tblPreviousMeter.AccountKey = qTmp.ID " & strWhere, strUTDatabase
				rsData.OpenRecordset("SELECT Mtr.*, Name, Name2, AccountNumber FROM " + "(SELECT AccountKey, SerialNumber, Size, Digits, Multiplier, Remote, SetDate, LastReading FROM tblPreviousMeter " + "UNION SELECT MeterTable.AccountKey, MeterTable.SerialNumber, MeterTable.Size, MeterTable.Digits, MeterTable.Multiplier, MeterTable.Remote, MeterTable.SetDate, NULL AS LastReading FROM MeterTable INNER JOIN tblPreviousMeter ON MeterTable.AccountKey = tblPreviousMeter.AccountKey AND tblPreviousMeter.PreviousMeterKey = MeterTable.ID) AS Mtr " + "LEFT JOIN (SELECT Master.ID, AccountNumber, pBill.FullNameLF AS Name, pBill2.FullNameLF AS Name2 FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID) AS qTmp ON Mtr.AccountKey = qTmp.ID" + strWhere + " ORDER BY AccountNumber, SetDate", modExtraModules.strUTDatabase);
				if (rsData.EndOfFile())
				{
					MessageBox.Show("No meter change out history found.", "No History", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					return;
				}
				rsData.MoveFirst();
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				FillHeaderLabels();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			string strName = "";
			string strSetDate = "";
			clsDRWrapper rsTemp = new clsDRWrapper();
			if (!rsData.EndOfFile())
			{
				if (rsData.Get_Fields_String("Name2") != "")
				{
					strName = rsData.Get_Fields_String("Name") + " & " + rsData.Get_Fields_String("Name2");
				}
				else
				{
					strName = rsData.Get_Fields_String("Name");
				}
				//FC:FINAL:MSH - can't implicitly convert from int to string
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
				fldName.Text = strName;
				//FC:FINAL:DSE Use correct string for empty date
				//if (FCConvert.ToString(rsData.Get_Fields("SetDate")) == "12:00:00 AM")
				if (FCConvert.ToString(rsData.Get_Fields_DateTime("SetDate")) == FCConvert.ToString(DateTime.FromOADate(0)))
				{
					fldSetDate.Text = "";
					strSetDate = Strings.Format(DateAndTime.DateValue(FCConvert.ToString(DateTime.FromOADate(0))), "MM/dd/yyyy");
				}
				else
				{
					fldSetDate.Text = Strings.Format(rsData.Get_Fields_DateTime("SetDate"), "MM/dd/yyyy");
					strSetDate = Strings.Format(rsData.Get_Fields_DateTime("SetDate"), "MM/dd/yyyy");
				}
				// kk10012014 trout-759
				rsTemp.OpenRecordset("SELECT TOP 1 * FROM tblPreviousMeter WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND SetDate < '" + strSetDate + "' ORDER BY SetDate DESC", modExtraModules.strUTDatabase);
				if (!rsTemp.EndOfFile())
				{
					fldReason.Text = rsTemp.Get_Fields_String("ChangeOutReason");
				}
				else
				{
					fldReason.Text = "";
				}
				fldSerialNum.Text = rsData.Get_Fields_String("SerialNumber");
				fldRemoteNum.Text = rsData.Get_Fields_String("Remote");
				//FC:FINAL:MSH - can't implicitly convert from int to string (same with internal issue #914)
				fldSize.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Size"));
				fldDigits.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Digits"));
				fldMultiplier.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Multiplier"));
				fldLastReading.Text = FCConvert.ToString(rsData.Get_Fields_Int32("LastReading"));
				// kk010012014 trout-759   fldReason.Text = .Fields("ChangeOutReason")
				rsData.MoveNext();
			}
			rsTemp.Dispose();
        }

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void rptMeterChangeOutHistory_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMeterChangeOutHistory properties;
			//rptMeterChangeOutHistory.Caption	= "Change Out History";
			//rptMeterChangeOutHistory.Icon	= "rptMeterChangeOutHistory.dsx":0000";
			//rptMeterChangeOutHistory.Left	= 0;
			//rptMeterChangeOutHistory.Top	= 0;
			//rptMeterChangeOutHistory.Width	= 18465;
			//rptMeterChangeOutHistory.Height	= 9930;
			//rptMeterChangeOutHistory.WindowState	= 2;
			//rptMeterChangeOutHistory.SectionData	= "rptMeterChangeOutHistory.dsx":058A;
			//End Unmaped Properties
		}
	}
}
