﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptLienInfo.
	/// </summary>
	public partial class srptLienInfo : FCSectionReport
	{
		public srptLienInfo()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptLienInfo InstancePtr
		{
			get
			{
				return (srptLienInfo)Sys.GetInstance(typeof(srptLienInfo));
			}
		}

		protected srptLienInfo _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
				rsPaymentInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptLienInfo	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		public int lngBillKey;
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		clsDRWrapper rsPaymentInfo = new clsDRWrapper();

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsPaymentInfo.MoveNext();
				eArgs.EOF = rsPaymentInfo.EndOfFile();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			blnFirstRecord = true;
			rsInfo.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngBillKey));
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
			}
			else
			{
				this.Cancel();
				return;
			}
			rsPaymentInfo.OpenRecordset("SELECT * FROM PaymentRec WHERE ISNULL(Lien,0) = 1 AND BillKey = " + FCConvert.ToString(lngBillKey));
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			fldPaymentDate.Text = Strings.Format(rsPaymentInfo.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy");
			// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
			fldCode.Text = FCConvert.ToString(rsPaymentInfo.Get_Fields("Code"));
			fldReceipt.Text = Strings.Format(rsPaymentInfo.Get_Fields_Int32("ReceiptNumber"), "00000");
			if (FCConvert.ToString(rsPaymentInfo.Get_Fields_String("Service")) == "W")
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				fldWaterPrincipalPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields("Principal"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
				fldWaterTaxPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields("Tax"), "#,##0.00");
				fldWaterInterestPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields_Decimal("PreLienInterest") + rsPaymentInfo.Get_Fields_Decimal("CurrentInterest"), "#,##0.00");
				fldWaterCostsPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields_Decimal("LienCost"), "#,##0.00");
				fldSewerPrincipalPayment.Text = "";
				fldSewerTaxPayment.Text = "";
				fldSewerInterestPayment.Text = "";
				fldSewerCostsPayment.Text = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				fldSewerPrincipalPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields("Principal"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
				fldSewerTaxPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields("Tax"), "#,##0.00");
				fldSewerInterestPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields_Decimal("PreLienInterest") + rsPaymentInfo.Get_Fields_Decimal("CurrentInterest"), "#,##0.00");
				fldSewerCostsPayment.Text = Strings.Format(rsPaymentInfo.Get_Fields_Decimal("LienCost"), "#,##0.00");
				fldWaterPrincipalPayment.Text = "";
				fldWaterTaxPayment.Text = "";
				fldWaterInterestPayment.Text = "";
				fldWaterCostsPayment.Text = "";
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldDate.Text = Strings.Format(rsInfo.Get_Fields_DateTime("DateCreated"), "MM/dd/yyyy");
			fldReference.Text = "Original Lien";
			if (FCConvert.ToBoolean(rsInfo.Get_Fields_Boolean("Water")))
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				fldWaterPrincipal.Text = Strings.Format(rsInfo.Get_Fields("Principal"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
				fldWaterTax.Text = Strings.Format(rsInfo.Get_Fields("Tax"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
				fldWaterInterest.Text = Strings.Format(rsInfo.Get_Fields("Interest"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
				fldWaterCosts.Text = Strings.Format(rsInfo.Get_Fields("Costs"), "#,##0.00");
				fldSewerPrincipal.Text = "";
				fldSewerTax.Text = "";
				fldSewerInterest.Text = "";
				fldSewerCosts.Text = "";
			}
			else
			{
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				fldSewerPrincipal.Text = Strings.Format(rsInfo.Get_Fields("Principal"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
				fldSewerTax.Text = Strings.Format(rsInfo.Get_Fields("Tax"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
				fldSewerInterest.Text = Strings.Format(rsInfo.Get_Fields("Interest"), "#,##0.00");
				// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
				fldSewerCosts.Text = Strings.Format(rsInfo.Get_Fields("Costs"), "#,##0.00");
				fldWaterPrincipal.Text = "";
				fldWaterTax.Text = "";
				fldWaterInterest.Text = "";
				fldWaterCosts.Text = "";
			}
		}

		private void srptLienInfo_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptLienInfo properties;
			//srptLienInfo.Icon	= "srptLienInfo.dsx":0000";
			//srptLienInfo.Left	= 0;
			//srptLienInfo.Top	= 0;
			//srptLienInfo.Width	= 11880;
			//srptLienInfo.Height	= 8595;
			//srptLienInfo.StartUpPosition	= 3;
			//srptLienInfo.SectionData	= "srptLienInfo.dsx":058A;
			//End Unmaped Properties
		}

		private void srptLienInfo_ReportEndedAndCanceled(object sender, EventArgs e)
		{
			this.Unload();
		}
	}
}
