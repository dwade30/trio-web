﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptLienDateChart.
	/// </summary>
	partial class rptLienDateChart
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLienDateChart));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldKey = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldType = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCommitment = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDueDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStart30DN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEnd30DN = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fld30DNDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStartLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEndLien = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLienDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldStartMat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldEndMat = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMatDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSeq = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCurrent = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			((System.ComponentModel.ISupportInitialize)(this.fldKey)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCommitment)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStart30DN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEnd30DN)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fld30DNDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStartLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEndLien)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStartMat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEndMat)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMatDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldKey,
				this.fldType,
				this.fldDescription,
				this.fldCommitment,
				this.fldDueDate,
				this.fldStart30DN,
				this.fldEnd30DN,
				this.fld30DNDate,
				this.fldStartLien,
				this.fldEndLien,
				this.fldLienDate,
				this.fldStartMat,
				this.fldEndMat,
				this.fldMatDate
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldKey
			// 
			this.fldKey.Height = 0.19F;
			this.fldKey.Left = 0F;
			this.fldKey.Name = "fldKey";
			this.fldKey.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldKey.Text = "Field1";
			this.fldKey.Top = 0.03125F;
			this.fldKey.Width = 0.46875F;
			// 
			// fldType
			// 
			this.fldType.Height = 0.19F;
			this.fldType.Left = 0.59375F;
			this.fldType.Name = "fldType";
			this.fldType.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldType.Text = "Field3";
			this.fldType.Top = 0.03125F;
			this.fldType.Width = 0.4375F;
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.19F;
			this.fldDescription.Left = 1.09375F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDescription.Text = "Field4";
			this.fldDescription.Top = 0.03125F;
			this.fldDescription.Width = 1.625F;
			// 
			// fldCommitment
			// 
			this.fldCommitment.Height = 0.19F;
			this.fldCommitment.Left = 2.763F;
			this.fldCommitment.Name = "fldCommitment";
			this.fldCommitment.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldCommitment.Text = "20/20/0000";
			this.fldCommitment.Top = 0.031F;
			this.fldCommitment.Width = 0.625F;
			// 
			// fldDueDate
			// 
			this.fldDueDate.Height = 0.19F;
			this.fldDueDate.Left = 3.461F;
			this.fldDueDate.Name = "fldDueDate";
			this.fldDueDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldDueDate.Text = "Field6";
			this.fldDueDate.Top = 0.031F;
			this.fldDueDate.Width = 0.625F;
			// 
			// fldStart30DN
			// 
			this.fldStart30DN.Height = 0.156F;
			this.fldStart30DN.Left = 4.149F;
			this.fldStart30DN.Name = "fldStart30DN";
			this.fldStart30DN.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldStart30DN.Text = "Field7";
			this.fldStart30DN.Top = 0.031F;
			this.fldStart30DN.Width = 0.675F;
			// 
			// fldEnd30DN
			// 
			this.fldEnd30DN.Height = 0.19F;
			this.fldEnd30DN.Left = 4.90625F;
			this.fldEnd30DN.Name = "fldEnd30DN";
			this.fldEnd30DN.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldEnd30DN.Text = "Field8";
			this.fldEnd30DN.Top = 0.03125F;
			this.fldEnd30DN.Width = 0.625F;
			// 
			// fld30DNDate
			// 
			this.fld30DNDate.Height = 0.19F;
			this.fld30DNDate.Left = 5.59375F;
			this.fld30DNDate.Name = "fld30DNDate";
			this.fld30DNDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fld30DNDate.Text = "Field9";
			this.fld30DNDate.Top = 0.03125F;
			this.fld30DNDate.Width = 0.625F;
			// 
			// fldStartLien
			// 
			this.fldStartLien.Height = 0.19F;
			this.fldStartLien.Left = 6.3125F;
			this.fldStartLien.Name = "fldStartLien";
			this.fldStartLien.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldStartLien.Text = "Field10";
			this.fldStartLien.Top = 0.03125F;
			this.fldStartLien.Width = 0.625F;
			// 
			// fldEndLien
			// 
			this.fldEndLien.Height = 0.19F;
			this.fldEndLien.Left = 7.03125F;
			this.fldEndLien.Name = "fldEndLien";
			this.fldEndLien.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldEndLien.Text = "Field11";
			this.fldEndLien.Top = 0.03125F;
			this.fldEndLien.Width = 0.625F;
			// 
			// fldLienDate
			// 
			this.fldLienDate.Height = 0.19F;
			this.fldLienDate.Left = 7.75F;
			this.fldLienDate.Name = "fldLienDate";
			this.fldLienDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldLienDate.Text = "Field12";
			this.fldLienDate.Top = 0.03125F;
			this.fldLienDate.Width = 0.625F;
			// 
			// fldStartMat
			// 
			this.fldStartMat.Height = 0.19F;
			this.fldStartMat.Left = 8.46875F;
			this.fldStartMat.Name = "fldStartMat";
			this.fldStartMat.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldStartMat.Text = "Field13";
			this.fldStartMat.Top = 0.03125F;
			this.fldStartMat.Width = 0.625F;
			// 
			// fldEndMat
			// 
			this.fldEndMat.Height = 0.19F;
			this.fldEndMat.Left = 9.1875F;
			this.fldEndMat.Name = "fldEndMat";
			this.fldEndMat.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldEndMat.Text = "Field14";
			this.fldEndMat.Top = 0.03125F;
			this.fldEndMat.Width = 0.625F;
			// 
			// fldMatDate
			// 
			this.fldMatDate.Height = 0.19F;
			this.fldMatDate.Left = 9.875F;
			this.fldMatDate.Name = "fldMatDate";
			this.fldMatDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldMatDate.Text = "Field15";
			this.fldMatDate.Top = 0.03125F;
			this.fldMatDate.Width = 0.625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblSeq,
				this.lblCurrent,
				this.lblName,
				this.Line1,
				this.lblAccount,
				this.Label8,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Label15,
				this.Label16,
				this.Label17
			});
			this.PageHeader.Height = 0.6979167F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Lien Date Chart";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.65625F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 9.1875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 9.1875F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblSeq
			// 
			this.lblSeq.Height = 0.1875F;
			this.lblSeq.HyperLink = null;
			this.lblSeq.Left = 0.59375F;
			this.lblSeq.Name = "lblSeq";
			this.lblSeq.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.lblSeq.Text = "Type";
			this.lblSeq.Top = 0.5F;
			this.lblSeq.Width = 0.4375F;
			// 
			// lblCurrent
			// 
			this.lblCurrent.Height = 0.1875F;
			this.lblCurrent.HyperLink = null;
			this.lblCurrent.Left = 1.09375F;
			this.lblCurrent.Name = "lblCurrent";
			this.lblCurrent.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.lblCurrent.Text = "Description";
			this.lblCurrent.Top = 0.5F;
			this.lblCurrent.Width = 1.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 2.763F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.lblName.Text = "Commit";
			this.lblName.Top = 0.5F;
			this.lblName.Width = 0.625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.6875F;
			this.Line1.Width = 10.46875F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10.46875F;
			this.Line1.Y1 = 0.6875F;
			this.Line1.Y2 = 0.6875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.03125F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.lblAccount.Text = "Key";
			this.lblAccount.Top = 0.5F;
			this.lblAccount.Width = 0.46875F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.188F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 4.149F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label8.Text = "Start 30DN";
			this.Label8.Top = 0.5F;
			this.Label8.Width = 0.675F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 4.90625F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label9.Text = "End 30DN";
			this.Label9.Top = 0.5F;
			this.Label9.Width = 0.625F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.188F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 5.59375F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label10.Text = "30DN Date";
			this.Label10.Top = 0.5F;
			this.Label10.Width = 0.655F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 6.3125F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label11.Text = "Start Lien";
			this.Label11.Top = 0.5F;
			this.Label11.Width = 0.625F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 3.461F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label12.Text = "Due Date";
			this.Label12.Top = 0.5F;
			this.Label12.Width = 0.625F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 7.75F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label13.Text = "Lien Date";
			this.Label13.Top = 0.5F;
			this.Label13.Width = 0.625F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 8.46875F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label14.Text = "Start Mat";
			this.Label14.Top = 0.5F;
			this.Label14.Width = 0.625F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 9.1875F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label15.Text = "End Mat";
			this.Label15.Top = 0.5F;
			this.Label15.Width = 0.625F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.1875F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 9.875F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label16.Text = "Mat Date";
			this.Label16.Top = 0.5F;
			this.Label16.Width = 0.625F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 7.03125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.Label17.Text = "End Lien";
			this.Label17.Top = 0.5F;
			this.Label17.Width = 0.625F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// rptLienDateChart
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.51042F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			((System.ComponentModel.ISupportInitialize)(this.fldKey)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCommitment)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDueDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStart30DN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEnd30DN)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fld30DNDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStartLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEndLien)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLienDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldStartMat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldEndMat)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMatDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCurrent)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldKey;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldType;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCommitment;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDueDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStart30DN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEnd30DN;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fld30DNDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStartLien;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEndLien;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLienDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStartMat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldEndMat;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMatDate;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSeq;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCurrent;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
