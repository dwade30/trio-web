﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using System.Windows.Forms;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmChangeOutHistoryReport.
	/// </summary>
	partial class frmChangeOutHistoryReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAccount;
		public fecherFoundation.FCLabel lblAccount;
		public fecherFoundation.FCComboBox cmbName;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCComboBox cmbDate;
		public fecherFoundation.FCLabel lblDate;
		public fecherFoundation.FCFrame fraReport;
		public fecherFoundation.FCButton cmdPreview;
		public fecherFoundation.FCFrame fraAccount;
		public fecherFoundation.FCTextBox txtEndAccount;
		public fecherFoundation.FCTextBox txtStartAccount;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraName;
		public fecherFoundation.FCTextBox txtStartName;
		public fecherFoundation.FCTextBox txtEndName;
		public fecherFoundation.FCLabel lblStartName;
		public fecherFoundation.FCLabel lblEndName;
		public fecherFoundation.FCFrame fraDate;
		public Wisej.Web.MaskedTextBox txtStartDate;
		public Wisej.Web.MaskedTextBox txtEndDate;
		public fecherFoundation.FCLabel lblEndDate;
		public fecherFoundation.FCLabel lblStartDate;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmChangeOutHistoryReport));
			this.cmbAccount = new fecherFoundation.FCComboBox();
			this.lblAccount = new fecherFoundation.FCLabel();
			this.cmbName = new fecherFoundation.FCComboBox();
			this.lblName = new fecherFoundation.FCLabel();
			this.cmbDate = new fecherFoundation.FCComboBox();
			this.lblDate = new fecherFoundation.FCLabel();
			this.fraReport = new fecherFoundation.FCFrame();
			this.fraAccount = new fecherFoundation.FCFrame();
			this.txtEndAccount = new fecherFoundation.FCTextBox();
			this.txtStartAccount = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.fraName = new fecherFoundation.FCFrame();
			this.txtStartName = new fecherFoundation.FCTextBox();
			this.txtEndName = new fecherFoundation.FCTextBox();
			this.lblStartName = new fecherFoundation.FCLabel();
			this.lblEndName = new fecherFoundation.FCLabel();
			this.fraDate = new fecherFoundation.FCFrame();
			this.txtStartDate = new Wisej.Web.MaskedTextBox();
			this.txtEndDate = new Wisej.Web.MaskedTextBox();
			this.lblEndDate = new fecherFoundation.FCLabel();
			this.lblStartDate = new fecherFoundation.FCLabel();
			this.cmdPreview = new fecherFoundation.FCButton();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReport)).BeginInit();
			this.fraReport.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraAccount)).BeginInit();
			this.fraAccount.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraName)).BeginInit();
			this.fraName.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).BeginInit();
			this.fraDate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdPreview);
			this.BottomPanel.Location = new System.Drawing.Point(0, 452);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraReport);
			this.ClientArea.Size = new System.Drawing.Size(610, 392);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(309, 30);
			this.HeaderText.Text = "Change Out History Report";
			// 
			// cmbAccount
			// 
			this.cmbAccount.AutoSize = false;
			this.cmbAccount.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAccount.FormattingEnabled = true;
			this.cmbAccount.Items.AddRange(new object[] {
				"All Accounts",
				"Range"
			});
			this.cmbAccount.Location = new System.Drawing.Point(133, 0);
			this.cmbAccount.Name = "cmbAccount";
			this.cmbAccount.Size = new System.Drawing.Size(149, 40);
			this.cmbAccount.TabIndex = 23;
			this.cmbAccount.Text = "All Accounts";
			this.cmbAccount.SelectedIndexChanged += new System.EventHandler(this.cmbAccount_SelectedIndexChanged);
			// 
			// lblAccount
			// 
			this.lblAccount.AutoSize = true;
			this.lblAccount.Location = new System.Drawing.Point(0, 14);
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Size = new System.Drawing.Size(104, 15);
			this.lblAccount.TabIndex = 24;
			this.lblAccount.Text = "ACCOUNT TYPE";
			// 
			// cmbName
			// 
			this.cmbName.AutoSize = false;
			this.cmbName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbName.FormattingEnabled = true;
			this.cmbName.Items.AddRange(new object[] {
				"All Names",
				"Range"
			});
			this.cmbName.Location = new System.Drawing.Point(133, 0);
			this.cmbName.Name = "cmbName";
			this.cmbName.Size = new System.Drawing.Size(149, 40);
			this.cmbName.TabIndex = 17;
			this.cmbName.Text = "All Names";
			this.cmbName.SelectedIndexChanged += new System.EventHandler(this.cmbName_SelectedIndexChanged);
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(0, 14);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(79, 15);
			this.lblName.TabIndex = 18;
			this.lblName.Text = "NAME TYPE";
			// 
			// cmbDate
			// 
			this.cmbDate.AutoSize = false;
			this.cmbDate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbDate.FormattingEnabled = true;
			this.cmbDate.Items.AddRange(new object[] {
				"All Set Dates",
				"Range"
			});
			this.cmbDate.Location = new System.Drawing.Point(133, 0);
			this.cmbDate.Name = "cmbDate";
			this.cmbDate.Size = new System.Drawing.Size(149, 40);
			this.cmbDate.TabIndex = 13;
			this.cmbDate.Text = "All Set Dates";
			this.cmbDate.SelectedIndexChanged += new System.EventHandler(this.cmbDate_SelectedIndexChanged);
			// 
			// lblDate
			// 
			this.lblDate.AutoSize = true;
			this.lblDate.Location = new System.Drawing.Point(0, 14);
			this.lblDate.Name = "lblDate";
			this.lblDate.Size = new System.Drawing.Size(76, 15);
			this.lblDate.TabIndex = 14;
			this.lblDate.Text = "DATE TYPE";
			this.lblDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// fraReport
			// 
			this.fraReport.AppearanceKey = "groupBoxNoBorders";
			this.fraReport.Controls.Add(this.fraAccount);
			this.fraReport.Controls.Add(this.fraName);
			this.fraReport.Controls.Add(this.fraDate);
			this.fraReport.Location = new System.Drawing.Point(30, 30);
			this.fraReport.Name = "fraReport";
			this.fraReport.Size = new System.Drawing.Size(581, 340);
			this.fraReport.TabIndex = 0;
			// 
			// fraAccount
			// 
			this.fraAccount.AppearanceKey = "groupBoxNoBorders";
			this.fraAccount.Controls.Add(this.txtEndAccount);
			this.fraAccount.Controls.Add(this.cmbAccount);
			this.fraAccount.Controls.Add(this.lblAccount);
			this.fraAccount.Controls.Add(this.txtStartAccount);
			this.fraAccount.Controls.Add(this.Label2);
			this.fraAccount.Controls.Add(this.Label1);
			this.fraAccount.Location = new System.Drawing.Point(0, 240);
			this.fraAccount.Name = "fraAccount";
			this.fraAccount.Size = new System.Drawing.Size(577, 100);
			this.fraAccount.TabIndex = 7;
			// 
			// txtEndAccount
			// 
			this.txtEndAccount.AutoSize = false;
			this.txtEndAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtEndAccount.Enabled = false;
			this.txtEndAccount.LinkItem = null;
			this.txtEndAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEndAccount.LinkTopic = null;
			this.txtEndAccount.Location = new System.Drawing.Point(392, 60);
			this.txtEndAccount.Name = "txtEndAccount";
			this.txtEndAccount.Size = new System.Drawing.Size(149, 40);
			this.txtEndAccount.TabIndex = 22;
			// 
			// txtStartAccount
			// 
			this.txtStartAccount.AutoSize = false;
			this.txtStartAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartAccount.Enabled = false;
			this.txtStartAccount.LinkItem = null;
			this.txtStartAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStartAccount.LinkTopic = null;
			this.txtStartAccount.Location = new System.Drawing.Point(133, 60);
			this.txtStartAccount.Name = "txtStartAccount";
			this.txtStartAccount.Size = new System.Drawing.Size(144, 40);
			this.txtStartAccount.TabIndex = 21;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(300, 74);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(34, 15);
			this.Label2.TabIndex = 10;
			this.Label2.Text = "END";
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(0, 74);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(48, 15);
			this.Label1.TabIndex = 8;
			this.Label1.Text = "START";
			// 
			// fraName
			// 
			this.fraName.AppearanceKey = "groupBoxNoBorders";
			this.fraName.Controls.Add(this.txtStartName);
			this.fraName.Controls.Add(this.cmbName);
			this.fraName.Controls.Add(this.lblName);
			this.fraName.Controls.Add(this.txtEndName);
			this.fraName.Controls.Add(this.lblStartName);
			this.fraName.Controls.Add(this.lblEndName);
			this.fraName.Location = new System.Drawing.Point(0, 120);
			this.fraName.Name = "fraName";
			this.fraName.Size = new System.Drawing.Size(580, 100);
			this.fraName.TabIndex = 4;
			// 
			// txtStartName
			// 
			this.txtStartName.AutoSize = false;
			this.txtStartName.BackColor = System.Drawing.SystemColors.Window;
			this.txtStartName.Enabled = false;
			this.txtStartName.LinkItem = null;
			this.txtStartName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStartName.LinkTopic = null;
			this.txtStartName.Location = new System.Drawing.Point(133, 60);
			this.txtStartName.Name = "txtStartName";
			this.txtStartName.Size = new System.Drawing.Size(149, 40);
			this.txtStartName.TabIndex = 16;
			// 
			// txtEndName
			// 
			this.txtEndName.AutoSize = false;
			this.txtEndName.BackColor = System.Drawing.SystemColors.Window;
			this.txtEndName.Enabled = false;
			this.txtEndName.LinkItem = null;
			this.txtEndName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEndName.LinkTopic = null;
			this.txtEndName.Location = new System.Drawing.Point(392, 60);
			this.txtEndName.Name = "txtEndName";
			this.txtEndName.Size = new System.Drawing.Size(149, 40);
			this.txtEndName.TabIndex = 17;
			// 
			// lblStartName
			// 
			this.lblStartName.AutoSize = true;
			this.lblStartName.Location = new System.Drawing.Point(0, 74);
			this.lblStartName.Name = "lblStartName";
			this.lblStartName.Size = new System.Drawing.Size(48, 15);
			this.lblStartName.TabIndex = 6;
			this.lblStartName.Text = "START";
			// 
			// lblEndName
			// 
			this.lblEndName.AutoSize = true;
			this.lblEndName.Location = new System.Drawing.Point(300, 74);
			this.lblEndName.Name = "lblEndName";
			this.lblEndName.Size = new System.Drawing.Size(34, 15);
			this.lblEndName.TabIndex = 5;
			this.lblEndName.Text = "END";
			// 
			// fraDate
			// 
			this.fraDate.AppearanceKey = "groupBoxNoBorders";
			this.fraDate.Controls.Add(this.txtStartDate);
			this.fraDate.Controls.Add(this.cmbDate);
			this.fraDate.Controls.Add(this.lblDate);
			this.fraDate.Controls.Add(this.txtEndDate);
			this.fraDate.Controls.Add(this.lblEndDate);
			this.fraDate.Controls.Add(this.lblStartDate);
			this.fraDate.Location = new System.Drawing.Point(0, 0);
			this.fraDate.Name = "fraDate";
			this.fraDate.Size = new System.Drawing.Size(577, 100);
			this.fraDate.TabIndex = 1;
			// 
			// txtStartDate
			// 
			this.txtStartDate.MaxLength = 10;
			this.txtStartDate.Enabled = false;
			this.txtStartDate.Location = new System.Drawing.Point(133, 60);
			this.txtStartDate.Mask = "##/##/####";
			this.txtStartDate.Name = "txtStartDate";
			this.txtStartDate.Size = new System.Drawing.Size(149, 40);
			this.txtStartDate.TabIndex = 12;
			this.txtStartDate.Text = "  /  /";
			// 
			// txtEndDate
			// 
			this.txtEndDate.MaxLength = 10;
			this.txtEndDate.Enabled = false;
			this.txtEndDate.Location = new System.Drawing.Point(392, 60);
			this.txtEndDate.Mask = "##/##/####";
			this.txtEndDate.Name = "txtEndDate";
			this.txtEndDate.Size = new System.Drawing.Size(149, 40);
			this.txtEndDate.TabIndex = 13;
			this.txtEndDate.Text = "  /  /";
			// 
			// lblEndDate
			// 
			this.lblEndDate.AutoSize = true;
			this.lblEndDate.Location = new System.Drawing.Point(300, 74);
			this.lblEndDate.Name = "lblEndDate";
			this.lblEndDate.Size = new System.Drawing.Size(34, 15);
			this.lblEndDate.TabIndex = 3;
			this.lblEndDate.Text = "END";
			this.lblEndDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblStartDate
			// 
			this.lblStartDate.AutoSize = true;
			this.lblStartDate.Location = new System.Drawing.Point(0, 74);
			this.lblStartDate.Name = "lblStartDate";
			this.lblStartDate.Size = new System.Drawing.Size(48, 15);
			this.lblStartDate.TabIndex = 2;
			this.lblStartDate.Text = "START";
			this.lblStartDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmdPreview
			// 
			this.cmdPreview.AppearanceKey = "acceptButton";
			this.cmdPreview.ForeColor = System.Drawing.Color.White;
			this.cmdPreview.Location = new System.Drawing.Point(225, 30);
			this.cmdPreview.Name = "cmdPreview";
			this.cmdPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdPreview.Size = new System.Drawing.Size(108, 48);
			this.cmdPreview.TabIndex = 19;
			this.cmdPreview.Text = "Preview";
			this.cmdPreview.Click += new System.EventHandler(this.cmdPreview_Click);
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePreview,
				this.mnuFileSeperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuFilePreview
			// 
			this.mnuFilePreview.Index = 0;
			this.mnuFilePreview.Name = "mnuFilePreview";
			this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePreview.Text = "Print Preview";
			this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 1;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// frmChangeOutHistoryReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 560);
			this.FillColor = 0;
			this.KeyPreview = true;
			this.Name = "frmChangeOutHistoryReport";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
			this.Text = "Change Out History Report";
			this.Load += new System.EventHandler(this.frmChangeOutHistoryReport_Load);
			this.FormUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_Unload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmChangeOutHistoryReport_KeyDown);
			this.Resize += new System.EventHandler(this.frmChangeOutHistoryReport_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraReport)).EndInit();
			this.fraReport.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.fraAccount)).EndInit();
			this.fraAccount.ResumeLayout(false);
			this.fraAccount.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraName)).EndInit();
			this.fraName.ResumeLayout(false);
			this.fraName.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraDate)).EndInit();
			this.fraDate.ResumeLayout(false);
			this.fraDate.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdPreview)).EndInit();
			this.ResumeLayout(false);
			//FC:FINAL:CHN - issue #978: incorrect tab order
			this.cmbDate.TabIndex = 0;
			this.txtStartDate.TabIndex = 1;
			this.txtEndDate.TabIndex = 2;
			this.cmbName.TabIndex = 3;
			this.txtStartName.TabIndex = 4;
			this.txtEndName.TabIndex = 5;
			this.cmbAccount.TabIndex = 6;
			this.txtStartAccount.TabIndex = 7;
			this.txtEndAccount.TabIndex = 8;
			this.cmbDate.KeyPress += new Wisej.Web.KeyPressEventHandler(OnCmbDateTabClick);
			this.txtEndDate.KeyPress += new Wisej.Web.KeyPressEventHandler(OnTxtEndDateTabClick);
			this.txtEndName.KeyPress += new Wisej.Web.KeyPressEventHandler(OnTxtEndNameTabClick);
			this.cmbAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(OnCmbAccountTabClick);
			this.txtEndAccount.KeyPress += new Wisej.Web.KeyPressEventHandler(OnTxtAccountEndTabClick);
		}
		#endregion

		//FC:FINAL:CHN - issue #978: incorrect tab order
		private void OnCmbDateTabClick(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			if (e.KeyChar != '\t')
				return;
			if (this.cmbDate.Text == "Range")
			{
				this.txtStartDate.Focus();
			}
			else
			{
				this.cmbName.Focus();
			}
		}

		private void OnTxtStartDateTabClick(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			if (e.KeyChar != '\t')
				return;
			this.txtEndDate.Focus();
		}

		private void OnTxtEndDateTabClick(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			if (e.KeyChar != '\t')
				return;
			this.cmbName.Focus();
		}

		private void OnTxtEndNameTabClick(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			if (e.KeyChar != '\t')
				return;
			this.cmbAccount.Focus();
		}

		private void OnCmbAccountTabClick(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			if (e.KeyChar != '\t')
				return;
			if (cmbAccount.Text != "Range")
				this.cmbDate.Focus();
			else
				this.txtStartAccount.Focus();
		}

		private void OnTxtAccountEndTabClick(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			if (e.KeyChar != '\t')
				return;
			this.cmbDate.Focus();
		}

		private System.ComponentModel.IContainer components;
	}
}
