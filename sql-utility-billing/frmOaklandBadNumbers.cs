﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmOaklandBadNumbers.
	/// </summary>
	public partial class frmOaklandBadNumbers : BaseForm
	{
		public frmOaklandBadNumbers()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmOaklandBadNumbers InstancePtr
		{
			get
			{
				return (frmOaklandBadNumbers)Sys.GetInstance(typeof(frmOaklandBadNumbers));
			}
		}

		protected frmOaklandBadNumbers _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Melissa Lamprecht       *
		// DATE           :               09/24/2007              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               09/24/2007              *
		// ********************************************************
		int lngColKey;
		int lngColAccount;

		private void frmOaklandBadNumbers_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						if (this.ActiveControl.GetName() == "vsGrid")
						{
							// Do Nothing
						}
						else
						{
							if (!fecherFoundation.FCUtils.IsNull(txtAccount.Text) && txtAccount.Text != "")
							{
								if (Conversion.Val(txtAccount.Text) > 0)
								{
									mnuNew_Click();
								}
							}
							else
							{
								MessageBox.Show("You must enter a serial number!", "No Serial Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								txtAccount.Focus();
							}
						}
						break;
					}
				default:
					{
						// Do Nothing
						break;
					}
			}
			//end switch
		}

		private void frmOaklandBadNumbers_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmOaklandBadNumbers properties;
			//frmOaklandBadNumbers.ScaleWidth	= 6555;
			//frmOaklandBadNumbers.ScaleHeight	= 5145;
			//frmOaklandBadNumbers.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo ErrorHandler
				lngColKey = 0;
				lngColAccount = 1;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmOaklandBadNumbers_Resize(object sender, System.EventArgs e)
		{
			SetGridHeight();
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsDelete = new clsDRWrapper();
				// vbPorter upgrade warning: intAnswer As short, int --> As DialogResult
				DialogResult intAnswer;
				int intRow = 0;
				int lngCurrent = 0;
				string strCurrent = "";
				intAnswer = MessageBox.Show("Are you sure you want to delete this number?", "Verification", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (intAnswer == DialogResult.Yes)
				{
					strCurrent = vsGrid.TextMatrix(vsGrid.Row, lngColKey);
					intRow = vsGrid.Row;
					if (strCurrent != "")
					{
						lngCurrent = FCConvert.ToInt32(FCConvert.ToDouble(strCurrent));
						clsDelete.Execute("DELETE FROM tblBadNumbers WHERE BadAccountID = " + FCConvert.ToString(lngCurrent), modExtraModules.strUTDatabase);
						vsGrid.RemoveItem(intRow);
					}
					else
					{
						MessageBox.Show("No record is selected.", "No Record", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In delete");
			}
		}

		private void mnuNew_Click()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsNew = new clsDRWrapper();
				if (!IsInBadTable(FCConvert.ToInt32(FCConvert.ToDouble(txtAccount.Text))))
				{
					clsNew.Execute("INSERT INTO tblBadNumbers (BadAccountNumber) VALUES(" + txtAccount.Text + ")", modExtraModules.strUTDatabase, true);
					FillGrid();
					txtAccount.Text = "";
				}
				else
				{
					MessageBox.Show("This number already exists in the table!", "Record Exists", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtAccount.Text = "";
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Add New");
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public void Init()
		{
			try
			{
				// On Error GoTo ErrorHandler
				if (FillGrid())
				{
					fraBadAccounts.Visible = true;
				}
				else
				{
					fraBadAccounts.Visible = false;
				}
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool FillGrid()
		{
			bool FillGrid = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the grid with the records from the tblBadNumbers table for this account
				clsDRWrapper rsGrid = new clsDRWrapper();
				FillGrid = true;
				vsGrid.Rows = 1;
				vsGrid.Cols = 2;
				vsGrid.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
				vsGrid.TextMatrix(0, lngColKey, "Key");
				vsGrid.TextMatrix(0, lngColAccount, "Serial Number");
				rsGrid.OpenRecordset("SELECT * FROM tblBadNumbers ORDER BY BadAccountNumber", modExtraModules.strUTDatabase);
				if (rsGrid.EndOfFile())
				{
					FillGrid = false;
					return FillGrid;
				}
				while (!rsGrid.EndOfFile())
				{
					vsGrid.AddItem("");
					// TODO Get_Fields: Field [BadAccountID] not found!! (maybe it is an alias?)
					vsGrid.TextMatrix(vsGrid.Rows - 1, lngColKey, FCConvert.ToString(rsGrid.Get_Fields("BadAccountID")));
					vsGrid.TextMatrix(vsGrid.Rows - 1, lngColAccount, FCConvert.ToString(rsGrid.Get_Fields_Int32("BadAccountNumber")));
					rsGrid.MoveNext();
				}
				SetGridHeight();
				return FillGrid;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return FillGrid;
		}

		private void SetGridHeight()
		{
			// set the height
			int lngHt;
			int lngW;
			lngW = vsGrid.Width;
			lngHt = fraBadAccounts.Height - (2 * vsGrid.Top);
			if (lngHt <= (vsGrid.RowHeight(0) * vsGrid.Rows) + 70)
			{
				vsGrid.Height = lngHt;
			}
			else
			{
				vsGrid.Height = (vsGrid.RowHeight(0) * vsGrid.Rows) + 70;
			}
			vsGrid.ColWidth(lngColKey, 0);
			vsGrid.ColWidth(lngColAccount, FCConvert.ToInt32(lngW * 0.05));
		}
		// vbPorter upgrade warning: lngSerialNumber As Variant --> As int
		private bool IsInBadTable(int lngSerialNumber)
		{
			bool IsInBadTable = false;
			// MAL@20070924: Added function to check for serial number in table
			bool blnResult;
			clsDRWrapper rsBadNum = new clsDRWrapper();
			rsBadNum.OpenRecordset("SELECT * FROM tblBadNumbers", modExtraModules.strUTDatabase);
			rsBadNum.FindFirstRecord("BadAccountNumber", lngSerialNumber);
			blnResult = !rsBadNum.NoMatch;
			IsInBadTable = blnResult;
			return IsInBadTable;
		}
	}
}
