﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for ar30DayNotice.
	/// </summary>
	public partial class ar30DayNotice : BaseSectionReport
	{
		public ar30DayNotice()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static ar30DayNotice InstancePtr
		{
			get
			{
				return (ar30DayNotice)Sys.GetInstance(typeof(ar30DayNotice));
			}
		}

		protected ar30DayNotice _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
                if (rsData != null)
					rsData.Dispose();
                if (rsCMFNumbers != null)
					rsCMFNumbers.Dispose();
                if (rsUT != null)
					rsUT.Dispose();
                if (rsRate != null)
					rsRate.Dispose();
                if (rsMort != null)
					rsMort.Dispose();
                if (rsAccount != null)
					rsAccount.Dispose();
                if (rsBills != null)
					rsBills.Dispose();
                if (rsCert != null)
					rsCert.Dispose();
                if (rsPayments != null)
					rsPayments.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	ar30DayNotice	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/27/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsCMFNumbers = new clsDRWrapper();
		string strSQL;
		string strText = "";
		// vbPorter upgrade warning: intYear As short --> As int	OnRead(string)
		short intYear;
		bool boolPayCert;
		bool boolMort;
		bool boolPayMortCert;
		bool boolPayNewOwner;
		bool boolAddressCO;
		clsDRWrapper rsCert;
		int lngCopies;
		// this is to keep track of the number of mortgage holder copies left
		bool boolCopy;
		// is this a copy or the original
		int intNewOwnerChoice;
		// this is if the user wants to send to any new owners 0 = Do not send any, 1 =
		clsDRWrapper rsMort = new clsDRWrapper();
		clsDRWrapper rsUT = new clsDRWrapper();
		clsDRWrapper rsRate = new clsDRWrapper();
		clsDRWrapper rsPayments = new clsDRWrapper();
		bool boolNewOwner;
		// this is true if there is a new owner
		bool boolAtLeastOneRecord;
		// this is true if there is at least one record to see
		string strAcctList = "";
		int lngMaxNumber;
		int lngCount;
		string strReportDesc = "";
		// this will be the string that will describe the parameters the user set for the report ie. Account 1 to 100
		string strAccountNumber = "";
		bool boolRemoveLastPage;
		// vbPorter upgrade warning: strDateCheck As string	OnWrite(string, DateTime)
		DateTime strDateCheck;
		int lngArrayIndex;
		bool boolWater;
		string strWS = "";
		int lngLastAccountKey;
		bool boolCombinedBills;
		string strRKList = "";
		double dblPayments;
		string str30DNQuery = "";
		bool blnFirstAccount;
		clsDRWrapper rsAccount = new clsDRWrapper();
		bool blnFirstRun;
		// these variables are for counting the accounts
		public int lngBalanceZero;
		public int lngBalanceNeg;
		public int lngBalancePos;
		public int lngDemandFeesApplied;
		string strBalanceZero = "";
		string strBalanceNeg = "";
		string strBalancePos = "";
		string strDemandFeesApplied = "";
		int lngCMFBillKey;
		clsDRWrapper rsBills = new clsDRWrapper();
		string strAddressPrefix = "";
		// MAL@20080423
		int intCurrentIndex;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			int lngAccountKey = 0;
			frmWait.InstancePtr.IncrementProgress();
			if (intCurrentIndex <= Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
			{
				lngAccountKey = modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey;
				rsData.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND " + strWS + "LienRecordNumber = 0 AND ISNULL(" + strWS + "DemandGroupID,0) = 0 AND (Service = 'B' OR Service = '" + strWS + "')", modExtraModules.strUTDatabase);
				if (!boolCopy)
				{
					// MAL@20071107
					blnFirstRun = true;
					// calculate fields
					CalculateVariableTotals();
					eArgs.EOF = intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1);
					if (eArgs.EOF)
					{
						frmWait.InstancePtr.Unload();
						FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
						EndReport();
					}
				}
				else
				{
					CalculateVariableTotals();
					blnFirstRun = false;
					eArgs.EOF = false;
				}
			}
			else
			{
				eArgs.EOF = true;
				frmWait.InstancePtr.Unload();
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
				EndReport();
			}
			if (lngCopies > 1)
			{
				// this is to produce multiple copies for certain parties (mortgage holders)
				lngCopies -= 1;
				boolCopy = true;
			}
			else
			{
				intCurrentIndex += 1;
				boolCopy = false;
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void EndReport()
		{
			// this will set all of the bill records to status of 1
			int lngCT;
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 1, FCConvert.ToString(lngBalanceZero));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 1, FCConvert.ToString(lngBalanceNeg));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 1, FCConvert.ToString(lngBalancePos));
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 1, FCConvert.ToString(lngBalanceZero + lngBalanceNeg + lngBalancePos));
			// this will take the trailing comma off
			if (strBalanceZero.Length > 0)
			{
				strBalanceZero = Strings.Left(strBalanceZero, strBalanceZero.Length - 1);
			}
			if (strBalanceNeg.Length > 0)
			{
				strBalanceNeg = Strings.Left(strBalanceNeg, strBalanceNeg.Length - 1);
			}
			if (strBalancePos.Length > 0)
			{
				strBalancePos = Strings.Left(strBalancePos, strBalancePos.Length - 1);
			}
			if (strDemandFeesApplied.Length > 0)
			{
				strDemandFeesApplied = Strings.Left(strDemandFeesApplied, strDemandFeesApplied.Length - 1);
			}
			// set the strings of accounts for the return grid to show
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(1, 2, strBalanceZero);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(2, 2, strBalanceNeg);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(3, 2, strBalancePos);
			frmFreeReport.InstancePtr.vsSummary.TextMatrix(4, 2, strDemandFeesApplied);
			if (lngDemandFeesApplied > 0)
			{
				// show a special label that is highlighted to show this information
				if ((((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70) < (frmFreeReport.InstancePtr.fraSummary.Height - 200))
				{
					frmFreeReport.InstancePtr.vsSummary.HeightOriginal = ((frmFreeReport.InstancePtr.vsSummary.Rows - 1) * frmFreeReport.InstancePtr.vsSummary.RowHeight(1)) + 70;
				}
				else
				{
					frmFreeReport.InstancePtr.vsSummary.HeightOriginal = frmFreeReport.InstancePtr.fraSummary.HeightOriginal - 200;
				}
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strOrderBy = "";
			int intCounter = 0;
			boolWater = frmFreeReport.InstancePtr.boolWater;
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			intCurrentIndex = 0;
			lblPageNumber.Left = 0;
			lblPageNumber.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
			this.Hide();
			//.Visible = false; // this will have to be set back to true to be shown
			// blnFirstAccount = True
			this.Name = "30 Day Notice";
			lngBalanceNeg = 0;
			lngBalancePos = 0;
			lngBalanceZero = 0;
			lngDemandFeesApplied = 0;
			lngCount = 0;
			boolPayNewOwner = false;
			boolPayCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[19].RowNumber, 1), 2) == "Ye");
			boolMort = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1), 2) == "Ye");
			boolAddressCO = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[18].RowNumber, 1), 8) == "Owner at");
			// MAL@20080423: Check which address will need to print if user adds the Address fields to the notice
			// Tracker Reference:
			if (boolAddressCO)
			{
				strAddressPrefix = "O";
			}
			else
			{
				strAddressPrefix = "B";
			}
			if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 2) == "Ye")
			{
				if (Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[35].RowNumber, 1), 7) == "Yes, ch")
				{
					intNewOwnerChoice = 2;
					boolPayNewOwner = true;
				}
				else
				{
					intNewOwnerChoice = 1;
				}
			}
			else
			{
				intNewOwnerChoice = 0;
			}
			boolNewOwner = false;
			if (boolMort)
			{
				boolPayMortCert = FCConvert.CBool(Strings.Left(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[32].RowNumber, 1), 9) == "Yes, char");
			}
			else
			{
				boolPayMortCert = false;
			}
			if (frmFreeReport.InstancePtr.chkRecommitment.CheckState == Wisej.Web.CheckState.Checked)
			{
				// make sure that the recommitment variables are set with whatever the user typed in
				modRhymalReporting.Statics.frfFreeReport[33].DatabaseFieldName = frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(0, 1);
				if (Information.IsDate(frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1)))
				{
					//FC:FINAL:MSH - I.Issue #1005: incorrect date format
					modRhymalReporting.Statics.frfFreeReport[34].DatabaseFieldName = Strings.Format(frmFreeReport.InstancePtr.vsRecommitment.TextMatrix(1, 1), "MMMM d yyyy");
				}
				else
				{
					//FC:FINAL:MSH - I.Issue #1005: incorrect date format
					modRhymalReporting.Statics.frfFreeReport[34].DatabaseFieldName = Strings.Format(DateTime.Today, "MMMM d, yyyy");
				}
			}
			// this will add a picture of the signature of the treasurer to the 30 Day Notice
			if (modMain.Statics.gboolUseSigFile)
			{
				imgSig.Visible = true;
				//imgSig.ZOrder(0);
				//Line2.ZOrder(0);
				imgSig.Image = FCUtils.LoadPicture(modSignatureFile.Statics.gstrUtilitySigPath);
				imgSig.PictureAlignment = GrapeCity.ActiveReports.SectionReportModel.PictureAlignment.BottomLeft;
			}
			else
			{
				imgSig.Visible = false;
			}
			fldTitle.Text = GetVariableValue_2("COLLECTORTITLE");

			rsUT.OpenRecordset(
                "SELECT Master.ID, Apt, StreetNumber, StreetName, MapLot, UseREAccount, REAccount, OwnerPartyID, " +
                "DeedName1 AS OwnerName, pOwn.Address1 AS OAddress1, pOwn.Address2 AS OAddress2, pOwn.Address3 AS OAddress3, pOwn.City AS OCity, pOwn.State AS OState, pOwn.Zip AS OZip, " +
                "DeedName2 AS SecondOwnerName " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP +
                "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID " + "LEFT JOIN " + modMain.Statics.strDbCP +
                "PartyNameView pOwn2 ON pOwn2.ID = Master.SecondOwnerPartyID", modExtraModules.strUTDatabase);
			rsRate.OpenRecordset("SELECT * FROM RateKeys", modExtraModules.strUTDatabase);
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			strSQL = SetupSQL();
			rsAccount.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			// MAL@20070911: Added additional steps to recordset being empty. Moved the frmWait.Init step to proper step
			if (!rsAccount.EndOfFile())
			{
				lngMaxNumber = rsAccount.RecordCount();
				rsCMFNumbers.OpenRecordset("SELECT * FROM CMFNumbers", modExtraModules.strUTDatabase);

				strOrderBy = " ORDER BY ActualAccountNumber, OName";

				rsData.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE " + strWS + "LienRecordNumber = 0 AND ISNULL(" + strWS + "DemandGroupID,0) = 0 AND (Service = 'B' OR Service = '" + strWS + "')" + strOrderBy, modExtraModules.strUTDatabase);
				//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
				modUTBilling.Statics.typAccountInfo = new modUTBilling.AcctInfo[0 + 1];
				modUTBilling.Statics.typAccountInfo[0].AccountKey = rsData.Get_Fields_Int32("AccountKey");
				if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "30DN"))
				{
					//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
					modMain.GetLatestOwnerInformation_59022(rsData.Get_Fields_Int32("AccountKey"), "30DN", ref modUTBilling.Statics.typAccountInfo[0].AccountName, "", "", "", "", "", "", "");
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
					modUTBilling.Statics.typAccountInfo[0].AccountName = rsData.Get_Fields_String("OName");
				}
				intCounter = 1;
				rsData.MoveNext();
				while (rsData.EndOfFile() != true)
				{
					//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
					if (modUTBilling.Statics.typAccountInfo[intCounter - 1].AccountKey != rsData.Get_Fields_Int32("AccountKey"))
					{
						//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
						Array.Resize(ref modUTBilling.Statics.typAccountInfo, intCounter + 1);
						modUTBilling.Statics.typAccountInfo[intCounter].AccountKey = rsData.Get_Fields_Int32("AccountKey");
						if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "30DN"))
						{
							//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
							modMain.GetLatestOwnerInformation_59022(rsData.Get_Fields_Int32("AccountKey"), "30DN", ref modUTBilling.Statics.typAccountInfo[intCounter].AccountName, "", "", "", "", "", "", "");
						}
						else
						{
							//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
							modUTBilling.Statics.typAccountInfo[intCounter].AccountName = rsData.Get_Fields_String("OName");
						}
						intCounter += 1;
					}
					rsData.MoveNext();
				}
				rsData.MoveFirst();
				if (frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account")
				{
					// do nothing the data is already in account number order
				}
				else
				{
					//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
					modUTBilling.strSort_24(modUTBilling.Statics.typAccountInfo, true, true);
				}
				//FC:FINAL:MSH - i.issue #1005: incorrect assignment after converting from VB6
				rsData.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE AccountKey = " + modUTBilling.Statics.typAccountInfo[0].AccountKey + " AND " + strWS + "LienRecordNumber = 0 AND ISNULL(" + strWS + "DemandGroupID,0) = 0 AND (Service = 'B' OR Service = '" + strWS + "')", modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, lngMaxNumber);
				modGlobalConstants.Statics.blnHasRecords = true;
			}
			else
			{
				lngMaxNumber = 0;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("There are no notices to print.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				modGlobalConstants.Statics.blnHasRecords = false;
				ActiveReport_Terminate(this, EventArgs.Empty);
				this.Cancel();
				this.Close();
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			DateTime dtDate;
			// this will create another active report with the summary information on it so the user can print if the feel like it
			//FC:FINAL:AM:#1167 - frmReportViewer is already closed at this point
			frmFreeReport.InstancePtr.Unload();
			{
				if (Information.IsDate(strDateCheck))
				{
					dtDate = strDateCheck;
				}
				else
				{
					dtDate = DateTime.Today;
				}
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                Int32 tempRepType = 1;

                rptLienNoticeSummary.InstancePtr.Init(ref strAcctList, "30 Day Notice Summary", strReportDesc, ref intYear, ref dtDate, ref boolWater, FCConvert.CBool(frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account"),1, ref str30DNQuery, ref boolAddressCO);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			string strTemp = "";
			// this sub will put all of the information into the string
			if (blnFirstRun)
			{
				strTemp = "NOTICE AND DEMAND FOR PAYMENT";
			}
			else
			{
				strTemp = " * * * COPY * * *  NOTICE AND DEMAND FOR PAYMENT  * * * COPY * * * ";
			}
			lblReportHeader.Text = "STATE OF MAINE" + "\r\n" + GetVariableValue_2("UTILITYTITLE") + "\r\n" + strTemp + "\r\n" + GetVariableValue_2("LEGALDESCRIPTION");
			// this will set the main string
			rtbText.Text = SetupVariablesInString(frmFreeReport.InstancePtr.rtbData.Text);
			rtbText.Font = new Font("Courier New", rtbText.Font.Size);

			// this will set the bottom of the report
			SetupFooterVariables();
		}

		private string SetupSQL()
		{
			string SetupSQL = "";
			// this will return the SQL statement for this batch of reports
			string strWhereClause = "";
			int intCT;
			string strOrderBy = "";
			if (frmFreeReport.InstancePtr.cmbSortOrder.Text == "Account")
			{
				strOrderBy = " ORDER BY ActualAccountNumber, OName";
			}
			else
			{
				strOrderBy = " ORDER BY OName, ActualAccountNumber";
			}
			if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Account")
			{
				// range of accounts
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						strReportDesc = "Account Number " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND ActualAccountNumber >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
						strReportDesc = "Account Number Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND ActualAccountNumber <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
						strReportDesc = "Account Number Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
						strReportDesc = "";
					}
				}
			}
			else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
			{
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'";
						strReportDesc = "Name " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " To " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND OName >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "'";
						strReportDesc = "Name Above " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND OName <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "'";
						strReportDesc = "Name Below " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
						strReportDesc = "";
					}
				}
			}
			else
			{
				strWhereClause = "";
				strReportDesc = "";
			}
			if (boolWater)
			{
				modMain.Statics.gstr30DayNoticeQuery = "SELECT * FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND NOT (LienProcessExclusion = 1) AND (WLienProcessStatus = 0 OR WLienProcessStatus = 1) AND WLienStatusEligibility = 1 AND WLienRecordNumber = 0 AND ISNULL(WDemandGroupID, 0) = 0 " + strWhereClause + strRKList;
				str30DNQuery = "SELECT * FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND NOT (LienProcessExclusion = 1) AND (WLienProcessStatus = 0 OR WLienProcessStatus = 1) AND WLienStatusEligibility = 2 AND WLienRecordNumber = 0 AND ISNULL(WDemandGroupID, 0) = 0 " + strWhereClause + strRKList;
			}
			else
			{
				modMain.Statics.gstr30DayNoticeQuery = "SELECT * FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND NOT (LienProcessExclusion = 1) AND (SLienProcessStatus = 0 OR SLienProcessStatus = 1) AND SLienStatusEligibility = 1 AND SLienRecordNumber = 0 AND ISNULL(SDemandGroupID, 0) = 0 " + strWhereClause + strRKList;
				str30DNQuery = "SELECT * FROM (" + modMain.Statics.gstrBillingMasterYear + ") AS qTmpY WHERE BillStatus = 'B' AND NOT (LienProcessExclusion = 1) AND (SLienProcessStatus = 0 OR SLienProcessStatus = 1) AND SLienStatusEligibility = 2 AND SLienRecordNumber = 0 AND ISNULL(SDemandGroupID, 0) = 0 " + strWhereClause + strRKList;
			}

			SetupSQL = "SELECT DISTINCT AccountKey, OName, ActualAccountNumber FROM (" + modMain.Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE " + strWS + "LienRecordNumber = 0 AND ISNULL(" + strWS + "DemandGroupID, 0) = 0 AND (Service = 'B' OR Service = '" + strWS + "')" + strOrderBy;
			return SetupSQL;
		}

		private string SetupVariablesInString(string strOriginal)
		{
			string SetupVariablesInString = "";
			// this will replace all of the variables with the information needed
			string strBuildString;
			// this is the string that will be built and returned at the end
			string strTemp = "";
			// this is a temporary sting that will be used to store and transfer string segments
			int lngNextVariable;
			// this is the position of the beginning of the next variable (0 = no more variables)
			int lngEndOfLastVariable;
			// this is the position of the end of the last variable
			lngEndOfLastVariable = 0;
			lngNextVariable = 1;
			strBuildString = "";
			// priming read
			if (Strings.InStr(1, strOriginal, "<", CompareConstants.vbBinaryCompare) > 0)
			{
				lngNextVariable = Strings.InStr(lngEndOfLastVariable + 1, strOriginal, "<", CompareConstants.vbBinaryCompare);
				// do until there are no more variables left
				do
				{
					// add the string from lngEndOfLastVariable - lngNextVariable to the BuildString
					strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1, lngNextVariable - lngEndOfLastVariable - 1);
					// set the end pointer
					lngEndOfLastVariable = Strings.InStr(lngNextVariable, strOriginal, ">", CompareConstants.vbBinaryCompare);
					// replace the variable
					strBuildString += GetVariableValue_2(Strings.Mid(strOriginal, lngNextVariable + 1, lngEndOfLastVariable - lngNextVariable - 1));
					// check for another variable
					lngNextVariable = Strings.InStr(lngEndOfLastVariable, strOriginal, "<", CompareConstants.vbBinaryCompare);
				}
				while (!(lngNextVariable == 0));
			}
			// take the last of the string and add it to the end
			strBuildString += Strings.Mid(strOriginal, lngEndOfLastVariable + 1);
			// check for variables
			if (Strings.InStr(1, "<", strOriginal, CompareConstants.vbBinaryCompare) > 0)
			{
				// strBuildString = SetupVariablesInString(strBuildString)     'setup recursion
				MessageBox.Show("ERROR: There are still variables in the report string.", "SetupVariablesInString Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			else
			{
				SetupVariablesInString = strBuildString;
			}
			return SetupVariablesInString;
		}

		private void SetupFooterVariables()
		{
			// this will setup the bottom part of the detail section
			fldCollector.Text = GetVariableValue_2("COLLECTOR");
			fldMuni.Text = GetVariableValue_2("MUNI");
			fldPrincipal.Text = Strings.Format(FCConvert.ToDouble(GetVariableValue_2("PRINCIPAL")) - dblPayments, "#,##0.00");
			// rsData.Fields(strWS & "PrinPaid")
			fldTax.Text = Strings.Format(FCConvert.ToDecimal(GetVariableValue_2("TAX")), "#,##0.00");
			fldInterest.Text = Strings.Format(GetVariableValue_2("INTEREST"), "#,##0.00");
			fldDemand.Text = Strings.Format(GetVariableValue_2("DEMAND"), "#,##0.00");
			fldCertMailFee.Text = Strings.Format(GetVariableValue_2("CERTTOTAL"), "#,##0.00");
			if (fldPrincipal.Text == "")
			{
				fldPrincipal.Text = "0.00";
			}
			// MAL@20080204: 12107
			if (fldTax.Text == "")
			{
				fldTax.Text = "0.00";
			}
			if (fldInterest.Text == "" || !Information.IsNumeric(fldInterest.Text))
			{
				fldInterest.Text = "0.00";
			}
			if (fldDemand.Text == "")
			{
				fldDemand.Text = "0.00";
			}
			if (fldCertMailFee.Text == "")
			{
				fldCertMailFee.Text = "0.00";
			}
			fldTotal.Text = Strings.Format(FCConvert.ToDecimal(fldPrincipal.Text) + FCConvert.ToDecimal(fldTax.Text) + FCConvert.ToDecimal(fldInterest.Text) + FCConvert.ToDecimal(fldDemand.Text) + FCConvert.ToDecimal(fldCertMailFee.Text), "#,##0.00");
		}

		private string GetVariableValue_2(string strVarName)
		{
			return GetVariableValue(ref strVarName);
		}

		private string GetVariableValue(ref string strVarName)
		{
			string GetVariableValue = "";
			// this function will take a variable name and find it in the array, then get the value and return it as a string
			int intCT;
			string strValue = "";
			// this is a dummy code to show the user where the hard codes are
			if (strVarName == "CRLF")
			{
				GetVariableValue = "";
				return GetVariableValue;
			}
			for (intCT = 0; intCT <= modRhymalReporting.MAXFREEVARIABLES; intCT++)
			{
				if (Strings.UCase(strVarName) == Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag))
				{
					switch (modRhymalReporting.Statics.frfFreeReport[intCT].VariableType)
					{
						case 0:
							{
								// value from the static variables in the grid
								switch (modRhymalReporting.Statics.frfFreeReport[intCT].Type)
								{
								// 0 = Text, 1 = Number, 2 = Date, 3 = Do not ask for default (comes from DB), 4 = Formatted Year
									case 0:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 1:
										{
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "#,##0.00");
											break;
										}
									case 2:
										{
											//FC:FINAL:MSH - I.Issue #1005: incorrect date format
											strValue = Strings.Format(frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1), "MMMM d, yyyy");
											break;
										}
									case 3:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
									case 4:
										{
											strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
											break;
										}
								}
								//end switch
								break;
								break;
							}
						case 1:
							{
								// value from the dynamic variables in the database
								// MAL@20080423: Test for address variables and add appropriate prefix
								// Tracker Reference:
								if (Strings.UCase(Strings.Left(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName, 4)) == "ADDR")
								{
									strValue = FCConvert.ToString(rsData.Get_Fields(strAddressPrefix + modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
								}
								else
								{
									strValue = FCConvert.ToString(rsData.Get_Fields(modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName));
								}
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								break;
								break;
							}
						case 2:
							{
								// questions at the bottom of the grid
								strValue = frmFreeReport.InstancePtr.vsVariableSetup.TextMatrix(modRhymalReporting.Statics.frfFreeReport[intCT].RowNumber, 1);
								break;
								break;
							}
						case 3:
							{
								// calculated values that are stored in the DatabaseFieldName field
								strValue = modRhymalReporting.Statics.frfFreeReport[intCT].DatabaseFieldName;
								if (Strings.Trim(strValue) == "")
								{
									if (Strings.UCase(modRhymalReporting.Statics.frfFreeReport[intCT].Tag) != "LESSPAYMENTS")
									{
										// maybe put a blank line there
										strValue = "__________";
									}
								}
								break;
								break;
							}
					}
					//end switch
				}
			}
			GetVariableValue = strValue;
			return GetVariableValue;
		}

		private void CalculateVariableTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will calculate all of the fields that need it and store
				// the value in the DatabaseFieldName string in the frfFreeReport
				// struct that these fields include principal, interest, costs,
				// total due and certified mail fee
				//clsDRWrapper rsSetCert;
				clsDRWrapper rsCombineBills = new clsDRWrapper();
				clsDRWrapper rsTmp = new clsDRWrapper();
				double dblTotalDue = 0;
				double dblInt = 0;
				double dblPrin = 0;
				double dblCost = 0;
				double dblTax = 0;
				double dblDemand;
				double dblCertMailFee = 0;
				int lngMortHolder = 0;
				string strAddressBar = "";
				string strBillDate = "";
				string strLessPaymentsLine = "";
				string strLocation = "";
				string strTemp = "";
				string strTemp2 = "";
				string strTemp3 = "";
				string strTemp4 = "";
				string strTemp5 = "";
				string strOwnerName = "";
				string strMapLot = "";
				string strBookPage = "";
				string strTrueFalse = "";
				double dblBillDue = 0;
				double dblBillInt = 0;
				double dblBillInt2 = 0;
				// Latest Tenant/Owner Information
				string strLastOwner;
				string strLastOwner2;
				string strLastAddress;
				string strLastAddress2;
				string strLastAddress3;
				string strLastCity;
				string strLastState;
				string strLastZip;
				// for CMFNumbers
				string strCMFName = "";
				int lngCMFMHNumber = 0;
				bool boolCMFNewOwner;
				bool boolPrintCMF;
				// true if a CMF should be printed for that person
				string strCMFAddr1;
				string strCMFAddr2;
				string strCMFAddr3;
				string strCMFAddr4;
				int lngBillCount = 0;
				string strBillList = "";
				TRYAGAIN:
				;
				strCMFAddr1 = "";
				strCMFAddr2 = "";
				strCMFAddr3 = "";
				strCMFAddr4 = "";
				strLastOwner = "";
				strLastOwner2 = "";
				strLastAddress = "";
				strLastAddress2 = "";
				strLastAddress3 = "";
				strLastCity = "";
				strLastState = "";
				strLastZip = "";
				// MAL@20070924: Check for multiple owners and get the latest
				// Changed code from this point on to refer to new string variables rather then rsData.Fields
				if (modMain.HasMoreThanOneOwner(rsData.Get_Fields_Int32("AccountKey"), "30DN"))
				{
					modMain.GetLatestOwnerInformation(rsData.Get_Fields_Int32("AccountKey"), "30DN", ref strLastOwner, ref strLastOwner2, ref strLastAddress, ref strLastAddress2, ref strLastAddress3, ref strLastCity, ref strLastState, ref strLastZip);
				}
				else
				{
					strLastOwner = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName")));
					strLastOwner2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName2")));
					strLastAddress = FCConvert.ToString(rsData.Get_Fields_String("OAddress1"));
					strLastAddress2 = FCConvert.ToString(rsData.Get_Fields_String("OAddress2"));
					strLastAddress3 = FCConvert.ToString(rsData.Get_Fields_String("OAddress3"));
					strLastCity = FCConvert.ToString(rsData.Get_Fields_String("OCity"));
					strLastState = FCConvert.ToString(rsData.Get_Fields_String("OState"));
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OZip4"))) != "")
					{
						strLastZip = rsData.Get_Fields_String("OZip") + "-" + rsData.Get_Fields_String("OZip4");
					}
					else
					{
						strLastZip = FCConvert.ToString(rsData.Get_Fields_String("OZip"));
					}
				}
				if (!boolCopy)
				{
					dblPayments = 0;
					dblTotalDue = 0;
					dblInt = 0;
					boolNewOwner = false;
					strDateCheck = Convert.ToDateTime(GetVariableValue_2("MAILDATE"));
					if (Strings.Trim(FCConvert.ToString(strDateCheck)) == "")
					{
						strDateCheck = DateTime.Today;
					}
					if (!modUTLien.Statics.gboolUseMailDateForMaturity)
					{
						// this will calculate the interest ahead 30 days from the mail date
						//FC:FINAL:MSH - i.issue #1005: incorrect date format
						strDateCheck = Convert.ToDateTime(Strings.Format(DateAndTime.DateAdd("D", 30, strDateCheck), "MMMM dd, yyyy"));
					}
					lngLastAccountKey = FCConvert.ToInt32(rsData.Get_Fields_Int32("AccountKey"));
					lngBillCount = 0;
					rsCombineBills.OpenRecordset(rsData.Name(), modExtraModules.strUTDatabase);
					if (rsCombineBills.RecordCount() > 1)
					{
						boolCombinedBills = true;
						strBillList = "";
						while (!rsCombineBills.EndOfFile())
						{
							dblBillInt2 = 0;

							dblBillDue = FCUtils.Round(modUTCalculations.CalculateAccountUT(rsCombineBills, ref strDateCheck, ref dblBillInt, boolWater, ref dblBillInt2) - dblBillInt, 2);
							// End If
							if (dblBillDue > 0)
							{
								// count this bill
								lngBillCount += 1;
								dblInt += dblBillInt2;
								dblTotalDue += dblBillDue;
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								lngCMFBillKey = FCConvert.ToInt32(rsCombineBills.Get_Fields("Bill"));
								// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
								strBillList += rsCombineBills.Get_Fields("Bill") + ",";
							}
							rsCombineBills.MoveNext();
						}
						// If lngBillCount > 1 Then
						boolCombinedBills = true;
						// Else
						// boolCombinedBills = False
						// End If
					}
					else
					{
						lngBillCount = 1;
						boolCombinedBills = false;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						lngCMFBillKey = FCConvert.ToInt32(rsData.Get_Fields("Bill"));
					}
					if (!boolCombinedBills)
					{
						// this will calculate the principal, interest and total due on a single bill
						if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) != 0)
						{
							dblTotalDue = FCUtils.Round(modUTCalculations.CalculateAccountUTLien(rsData, strDateCheck, ref dblInt, boolWater) - dblInt, 2);
						}
						else
						{
							// if a blank date is returned, then this line will crash
							dblTotalDue = FCUtils.Round(modUTCalculations.CalculateAccountUT(rsData, strDateCheck, ref dblInt, boolWater) - dblInt, 2);
						}
					}
					if (FCUtils.Round(dblTotalDue, 2) == 0 || (frmFreeReport.InstancePtr.dblMinimumAmount > 0 && FCUtils.Round(dblTotalDue, 2) < frmFreeReport.InstancePtr.dblMinimumAmount))
					{
						lngBalanceZero += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strBalanceZero += rsData.Get_Fields("Bill") + ", ";
						// kk07302015 trout-1030 Prevent Type Mismatch error in SetupFooterVariables when getting variable value for Principal, Tax, Interest, Demand and CertTotal
						modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = "0.00";
						// Principal (20)
						modRhymalReporting.Statics.frfFreeReport[36].DatabaseFieldName = "0.00";
						// Tax (36)
						modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = "0.00";
						// Interest (21)
						modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = "0.00";
						// Total Certified Mail Fee (31)
						modRhymalReporting.Statics.frfFreeReport[12].DatabaseFieldName = "0.00";
						// Demand Fee (12)
						intCurrentIndex += 1;
						if (intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
						{
							return;
						}
						else
						{
							rsData.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey) + " AND " + strWS + "LienRecordNumber = 0 AND ISNULL(" + strWS + "DemandGroupID,0) = 0 AND (Service = 'B' OR Service = '" + strWS + "')", modExtraModules.strUTDatabase);
						}
						goto TRYAGAIN;
					}
					// this will check to see if demand fees are already in place on this acocunt
					if (rsData.Get_Fields(strWS + "LienProcessStatus") > 1)
					{
						if (lngDemandFeesApplied == 0)
						{
							frmFreeReport.InstancePtr.vsSummary.AddItem("");
							frmFreeReport.InstancePtr.vsSummary.AddItem("Accounts that already have demand fees applied:");
							frmFreeReport.InstancePtr.vsSummary.MergeCells = FCGrid.MergeCellsSettings.flexMergeFree;
							frmFreeReport.InstancePtr.vsSummary.IsSubtotal(frmFreeReport.InstancePtr.vsSummary.Rows - 1, true);
							frmFreeReport.InstancePtr.vsSummary.RowOutlineLevel(frmFreeReport.InstancePtr.vsSummary.Rows - 1, 1);
							frmFreeReport.InstancePtr.vsSummary.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
							frmFreeReport.InstancePtr.vsSummary.HeightOriginal += 540;
						}
						lngDemandFeesApplied += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strDemandFeesApplied += rsData.Get_Fields("Bill") + ", ";
						frmFreeReport.InstancePtr.vsSummary.AddItem("\t" + rsData.Get_Fields_Int32("AccountKey"));
						frmFreeReport.InstancePtr.vsSummary.IsSubtotal(frmFreeReport.InstancePtr.vsSummary.Rows - 1, true);
						frmFreeReport.InstancePtr.vsSummary.RowOutlineLevel(frmFreeReport.InstancePtr.vsSummary.Rows - 1, 2);
						frmFreeReport.InstancePtr.vsSummary.HeightOriginal += 270;
						// kk07302015 trout-1030 Prevent Type Mismatch error in SetupFooterVariables when getting variable value for Principal, Tax, Interest, Demand and CertTotal
						modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = "0.00";
						// Principal (20)
						modRhymalReporting.Statics.frfFreeReport[36].DatabaseFieldName = "0.00";
						// Tax (36)
						modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = "0.00";
						// Interest (21)
						modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = "0.00";
						// Total Certified Mail Fee (31)
						modRhymalReporting.Statics.frfFreeReport[12].DatabaseFieldName = "0.00";
						// Demand Fee (12)
						intCurrentIndex += 1;
						if (intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
						{
							return;
						}
						else
						{
							rsData.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey) + " AND " + strWS + "LienRecordNumber = 0 AND ISNULL(" + strWS + "DemandGroupID,0) = 0 AND (Service = 'B' OR Service = '" + strWS + "')", modExtraModules.strUTDatabase);
						}
						goto TRYAGAIN;
					}
					if (dblTotalDue > 0)
					{
						// these are the account that will be demanded
						lngBalancePos += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strAcctList += rsData.Get_Fields("Bill") + ",";
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strBalancePos += rsData.Get_Fields("Bill") + ", ";
					}
					else if (dblTotalDue < 0)
					{
						lngBalanceNeg += 1;
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strBalanceNeg += rsData.Get_Fields("Bill") + ", ";
						// kk07302015 trout-1030 Prevent Type Mismatch error in SetupFooterVariables when getting variable value for Principal, Tax, Interest, Demand and CertTotal
						modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = "0.00";
						// Principal (20)
						modRhymalReporting.Statics.frfFreeReport[36].DatabaseFieldName = "0.00";
						// Tax (36)
						modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = "0.00";
						// Interest (21)
						modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = "0.00";
						// Total Certified Mail Fee (31)
						modRhymalReporting.Statics.frfFreeReport[12].DatabaseFieldName = "0.00";
						// Demand Fee (12)
						intCurrentIndex += 1;
						if (intCurrentIndex > Information.UBound(modUTBilling.Statics.typAccountInfo, 1))
						{
							return;
						}
						else
						{
							rsData.OpenRecordset("SELECT * FROM (" + modMain.Statics.gstr30DayNoticeQuery + ") AS qTmpY WHERE AccountKey = " + FCConvert.ToString(modUTBilling.Statics.typAccountInfo[intCurrentIndex].AccountKey) + " AND " + strWS + "LienRecordNumber = 0 AND ISNULL(" + strWS + "DemandGroupID,0) = 0 AND (Service = 'B' OR Service = '" + strWS + "')", modExtraModules.strUTDatabase);
						}
						goto TRYAGAIN;
					}
					// this will find out how many mortgage holders this account has and how many copies to print
					if (boolMort)
					{
						lngMortHolder = CalculateMortgageHolders();
						lngCopies = lngMortHolder + 1;
					}
					else
					{
						lngMortHolder = 0;
						lngCopies = 1;
					}
					// this will find out the certified mail fee
					if (boolPayCert)
					{
						// MAL@20081222: Fixed to leave the Mortgage Holder copy value alone
						// Tracker Reference: 16402
						// lngMortHolder = lngMortHolder + 1
						if (boolPayMortCert)
						{
							dblCertMailFee = (lngMortHolder + 1) * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
						else
						{
							dblCertMailFee = FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
					}
					else
					{
						if (boolPayMortCert)
						{
							dblCertMailFee = lngMortHolder * FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
						}
						else
						{
							dblCertMailFee = 0;
						}
					}
					// check to see if one is going to be sent to the next owner
					bool boolUTMatch = false;
                    //FC:FINAL:MSH - issue #1627: use '&' operator instead of '&&' for updating data in 'rsUT' in any case (as in original)
                    //if (intNewOwnerChoice != 0 && modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
                    if (intNewOwnerChoice != 0 & modMain.NewOwnerUT(ref rsData, ref rsUT, ref boolUTMatch))
					{
						// this should set the RE account as well
						// if there has been a new owner, then make a copy for the new owner as well
						if (!boolNewOwner)
						{
							if (intNewOwnerChoice == 2)
							{
								// charge for the new owner's copy
								dblCertMailFee += FCConvert.ToDouble(GetVariableValue_2("MAILFEE"));
							}
							lngCopies += 1;
							boolNewOwner = true;
						}
					}
					strAccountNumber = modMain.PadToString_8(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey")), 6);
					lblPageNumber.Text = strAccountNumber;
					if (Strings.Trim(strLastOwner2) != "")
					{
						strOwnerName = Strings.Trim(strLastOwner) + " and " + Strings.Trim(strLastOwner2);
					}
					else
					{
						strOwnerName = Strings.Trim(strLastOwner);
					}

					dblBillInt = 0;
					if (boolCombinedBills)
					{
						rsCombineBills.MoveFirst();
						while (!rsCombineBills.EndOfFile())
						{
							// this will cycle through the bills and find out how much is owed for all of them and roll it into the lien
							dblPrin += rsCombineBills.Get_Fields(strWS + "PrinOwed");
							dblPayments += rsCombineBills.Get_Fields(strWS + "PrinPaid");
							dblBillInt += rsCombineBills.Get_Fields(strWS + "IntOwed") + (rsCombineBills.Get_Fields(strWS + "IntAdded") * -1) - rsCombineBills.Get_Fields(strWS + "IntPaid");
							dblCost += rsCombineBills.Get_Fields(strWS + "CostOwed") - rsCombineBills.Get_Fields(strWS + "CostAdded") - rsCombineBills.Get_Fields(strWS + "CostPaid");
							dblTax += rsCombineBills.Get_Fields(strWS + "TaxOwed") - rsCombineBills.Get_Fields(strWS + "TaxPaid");
							rsCombineBills.MoveNext();
						}
						// allow dblInt to stay the same (ie calculated from above)
					}
					else
					{
						dblPrin = rsData.Get_Fields(strWS + "PrinOwed");
						// + rsData.Fields(strWS & "TaxOwed") ; 'MAL@20080130: Removed because amount was being duplicated ; Tracker Reference: 12107
						dblPayments = rsData.Get_Fields(strWS + "PrinPaid");
						dblInt = rsData.Get_Fields(strWS + "IntOwed") + (rsData.Get_Fields(strWS + "IntAdded") * -1) - rsData.Get_Fields(strWS + "IntPaid") + dblInt;
						dblCost = rsData.Get_Fields(strWS + "CostOwed") - rsData.Get_Fields(strWS + "CostAdded") - rsData.Get_Fields(strWS + "CostPaid");
						dblTax = rsData.Get_Fields(strWS + "TaxOwed") - rsData.Get_Fields(strWS + "TaxPaid");
					}
					// less payments line
					if (FCUtils.Round(dblPayments, 2) != 0)
					{
						// only show this when there are payments
						if (dblPayments < 0)
						{
							// if the principal paid is a negative number then some adjustment has happened (supplemental) this should not happen later in the code life
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsData.Get_Fields("Bill") + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
							if (rsPayments.EndOfFile())
							{
								strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
							else
							{
								strLessPaymentsLine = "plus adjustment of " + Strings.Format(Math.Abs(dblPayments), "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
						}
						else
						{
							// principal paid is greater then zero
							// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
							rsPayments.OpenRecordset("SELECT ID FROM PaymentRec WHERE BillKey = " + rsData.Get_Fields("Bill") + " AND (Code = 'A' OR Code = 'R' OR Code = 'S' OR Code = 'D')");
							if (rsPayments.EndOfFile())
							{
								strLessPaymentsLine = "less payment and adjustment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
							else
							{
								strLessPaymentsLine = "less payment of " + Strings.Format(dblPayments, "#,##0.00") + " for the net sum of " + Strings.Format(dblPrin - dblPayments, "#,##0.00") + ",";
							}
						}
					}
					else
					{
						strLessPaymentsLine = "";
					}
					// Location
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					strLocation = Strings.Trim(Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("StreetNumber")))) + " " + rsUT.Get_Fields_String("StreetName");
					// MapLot
					strMapLot = Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("MapLot")));
					// BookPage
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BookPage"))) == "")
					{
						strBookPage = "";
					}
					else
					{
						strBookPage = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BookPage")));
					}
					if (Strings.Trim(strLocation) == "")
					{
						strLocation = "________________";
					}
					if (Strings.Trim(strMapLot) == "")
					{
						strMapLot = "________________";
					}
					if (Strings.Trim(strBookPage) == "")
					{
						strBookPage = "________________";
					}
					// Commitment Date
					rsRate.FindFirstRecord("ID", rsData.Get_Fields_Int32("BillingRateKey"));
					if (!rsRate.NoMatch)
					{
						//FC:FINAL:MSH - i.issue #1005: incorrect converting from date to int
						//if (FCConvert.ToInt32(rsRate.Get_Fields("BillDate")) != 0)
						if (FCConvert.ToDateTime(rsRate.Get_Fields_DateTime("BillDate") as object).ToOADate() != 0)
						{
							strBillDate = Strings.Format(rsRate.Get_Fields_DateTime("BillDate"), "MMMM d, yyyy");
						}
						else
						{
							strBillDate = "__________";
						}
					}
					else
					{
						strBillDate = "__________";
					}
					// Address Bar
					// MAL@20070926
					strAddressBar = "    " + Strings.Trim(strLastOwner);
					if (Strings.Trim(strLastOwner2) != "")
					{
						strAddressBar += " and " + Strings.Trim(strLastOwner2);
					}
					if (boolAddressCO || (strLastAddress == "" && strLastAddress2 == "" && strLastAddress3 == ""))
					{
						// this is the owner at time of billing C/O the new owner and the new owner address
						// or if there is no information in the CL fields (Should only happen the first year after conversion)
						// Dave 5/20/07 And boolNewOwner
						if (boolAddressCO)
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							if (FCConvert.ToString(rsUT.Get_Fields("OwnerName")) != strLastOwner)
							{
								// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
								strAddressBar += "\r\n" + "    C/O " + rsUT.Get_Fields("OwnerName");
								// name on the current RE account
								// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName"))) != "")
								{
									// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
									strAddressBar += " and " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName")));
								}
							}
						}
						// if the row needs to be added, then do it
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
						}
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strAddressBar += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strAddressBar += " " + rsUT.Get_Fields_String("OZip");
							}
						}
						else
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strAddressBar += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strAddressBar += rsUT.Get_Fields_String("OZip");
							}
						}
						// Dave 5/20/07 And boolNewOwner
						if (boolAddressCO)
						{
							// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
							if (FCConvert.ToString(rsUT.Get_Fields("OwnerName")) != strLastOwner)
							{
								// this is information for the Certified Mailing List
								// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
								strCMFAddr1 = "C/O " + rsUT.Get_Fields("OwnerName");
								strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
								strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
								// MAL@20080325: Tracker Reference: 12766
								if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
									if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
									{
										strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
									}
									else
									{
										strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
									}
								}
								else
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
									if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
									{
										strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
									}
									else
									{
										strCMFAddr4 += rsUT.Get_Fields_String("OZip");
									}
								}
							}
							else
							{
								// this is information for the Certified Mailing List
								strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
								strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
								strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
								// MAL@20080325: Tracker Reference: 12766
								if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
									if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
									{
										strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
									}
									else
									{
										strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
									}
								}
								else
								{
									strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState") + rsUT.Get_Fields_String("OZip");
									if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
									{
										strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
									}
									else
									{
										strCMFAddr4 += rsUT.Get_Fields_String("OZip");
									}
								}
							}
						}
						else
						{
							// this is information for the Certified Mailing List
							strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
							strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
							strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
								}
							}
							else
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strCMFAddr4 += rsUT.Get_Fields_String("OZip");
								}
							}
						}
					}
					else
					{
						// this is the owner name at the time of billing and the address at the time of billing
						if (strLastAddress == "" && strLastAddress2 == "" && strLastAddress3 == "")
						{
							// if the row needs to be added, then do it
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
							}
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
							}
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
							}
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strAddressBar += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strAddressBar += " " + rsUT.Get_Fields_String("OZip");
								}
							}
							else
							{
								strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strAddressBar += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strAddressBar += rsUT.Get_Fields_String("OZip");
								}
							}
							// this is information for the Certified Mailing List
							strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
							strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
							strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
							// MAL@20080325: Tracker Reference: 12766
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
								}
							}
							else
							{
								strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strCMFAddr4 += rsUT.Get_Fields_String("OZip");
								}
							}
						}
						else
						{
							// if the row needs to be added, then do it
							if (Strings.Trim(strLastAddress) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress;
							}
							if (Strings.Trim(strLastAddress2) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress2;
							}
							if (Strings.Trim(strLastAddress3) != "")
							{
								strAddressBar += "\r\n" + "    " + strLastAddress3;
								// & ", " & rsRE.Fields("RSState") & " " & rsRE.Fields("RSZip")
							}
							// strAddressBar = strAddressBar & vbCrLf & "    " & rsUT.Fields("OCity") & ", " & rsUT.Fields("OState") & " " & rsUT.Fields("OZip")
							// MAL@20080320: Change to avoid empty "," in string
							// Tracker Reference: 12766
							if (strLastCity != "")
							{
								strLastCity += ", ";
							}
							else
							{
								strLastCity = strLastCity;
							}
							if (strLastState != "")
							{
								strLastState += " ";
							}
							else
							{
								strLastState = strLastState;
							}
							// strAddressBar = strAddressBar & vbCrLf & "    " & strLastCity & ", " & strLastState & " " & strLastZip
							strAddressBar += "\r\n" + "    " + strLastCity + strLastState + strLastZip;
							// this is information for the Certified Mailing List
							strCMFAddr1 = strLastAddress;
							strCMFAddr2 = strLastAddress2;
							strCMFAddr3 = strLastAddress3;
                            strCMFAddr4 = strLastCity + strLastState + strLastZip;
                        }
					}
					// info for CMFNumbers table
					strCMFName = strOwnerName;
					lngCMFMHNumber = 0;
					// boolNewOwner = False
					// Billing Year (1)
					modRhymalReporting.Statics.frfFreeReport[1].DatabaseFieldName = FCConvert.ToString(intYear);
					// MapLot (2)
					modRhymalReporting.Statics.frfFreeReport[2].DatabaseFieldName = strMapLot;
					// BookPage (3)
					modRhymalReporting.Statics.frfFreeReport[3].DatabaseFieldName = strBookPage;
					// Commitment Date (11)
					modRhymalReporting.Statics.frfFreeReport[11].DatabaseFieldName = strBillDate;
					modRhymalReporting.Statics.frfFreeReport[20].DatabaseFieldName = Strings.Format(dblPrin, "#,##0.00");
					// Tax (36)
					modRhymalReporting.Statics.frfFreeReport[36].DatabaseFieldName = Strings.Format(dblTax, "#,##0.00");
					// Interest (21)
					modRhymalReporting.Statics.frfFreeReport[21].DatabaseFieldName = Strings.Format(dblInt, "#,##0.00");
					// Less Payments (22)
					modRhymalReporting.Statics.frfFreeReport[22].DatabaseFieldName = strLessPaymentsLine;
					// City/Town (23)
					if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
					{
						modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = modGlobalConstants.Statics.gstrCityTown + " of";
					}
					else
					{
						modRhymalReporting.Statics.frfFreeReport[23].DatabaseFieldName = "";
					}
					// Owner Name (24)
					modRhymalReporting.Statics.frfFreeReport[24].DatabaseFieldName = strOwnerName;
					// Total Certified Mail Fee (31)
					modRhymalReporting.Statics.frfFreeReport[31].DatabaseFieldName = Strings.Format(dblCertMailFee, "#,##0.00");
					// Total Due (28)
					modRhymalReporting.Statics.frfFreeReport[28].DatabaseFieldName = Strings.Format(dblTotalDue - dblBillInt, "#,##0.00");
					// + dblCertMailFee + CDbl(GetVariableValue("DEMAND") 'dblTotalDue changed for AMSD 07/14/2006
					// Location (29)
					modRhymalReporting.Statics.frfFreeReport[29].DatabaseFieldName = strLocation;
					// Name And Address Bar (30)
					modRhymalReporting.Statics.frfFreeReport[30].DatabaseFieldName = strAddressBar;
					Array.Resize(ref modUTLien.Statics.arrDemand, lngArrayIndex + 1);
					// this will make sure that there are enough elements to use
					modUTLien.Statics.arrDemand[lngArrayIndex].Used = true;
					modUTLien.Statics.arrDemand[lngArrayIndex].Processed = false;
					modUTLien.Statics.arrDemand[lngArrayIndex].Account = modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"));
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					modUTLien.Statics.arrDemand[lngArrayIndex].BillKey = FCConvert.ToInt32(rsData.Get_Fields("Bill"));
					modUTLien.Statics.arrDemand[lngArrayIndex].Name = strLastOwner;
					// arrDemand(lngArrayIndex).Name = rsData.Fields("OName")
					modUTLien.Statics.arrDemand[lngArrayIndex].Fee = FCConvert.ToDouble(GetVariableValue_2("DEMAND"));
					modUTLien.Statics.arrDemand[lngArrayIndex].CertifiedMailFee = dblCertMailFee;
					lngArrayIndex += 1;
					// MAL@20080602: Add update routine to update copies for all effected bills
					// Tracker Reference: 13749
					if (boolCombinedBills)
					{
						if (Strings.Right(strBillList, 1) == ",")
						{
							strBillList = Strings.Left(strBillList, strBillList.Length - 1);
						}
						UpdateCopies(strBillList);
					}
					else
					{
						// k           rsData.Edit
						// rsData.Fields("Copies") = lngCopies
						// rsData.Update
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsTmp.Execute("UPDATE Bill SET Copies = " + FCConvert.ToString(lngCopies) + " WHERE ID = " + rsData.Get_Fields("Bill"), modExtraModules.strUTDatabase);
					}
				}
				else
				{
					// this is the difference when it is a copy
					if (boolPayMortCert)
					{
						// if the user is getting charged for certified mail, then save thier info to print on Cert Mail Forms
						boolPrintCMF = true;
					}
					else
					{
						boolPrintCMF = false;
					}
					// address bar
					if (rsMort.EndOfFile() != true)
					{
						// info for CMFNumbers table
						strCMFName = FCConvert.ToString(rsMort.Get_Fields_String("Name"));
						lngCMFMHNumber = FCConvert.ToInt32(rsMort.Get_Fields_Int32("MortgageHolderID"));
						if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1"))) != "")
						{
							strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address1"));
							if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
							{
								strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address2"));
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr3 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr4 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr4 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
						}
						else
						{
							if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2"))) != "")
							{
								strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address2"));
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr2 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr3 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
							else
							{
								if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3"))) != "")
								{
									strCMFAddr1 = FCConvert.ToString(rsMort.Get_Fields_String("Address3"));
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr2 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
								else
								{
									if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
									{
										strCMFAddr1 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP") + "-" + rsMort.Get_Fields_String("ZIP4");
									}
									else
									{
										strCMFAddr1 = rsMort.Get_Fields_String("CITY") + ", " + rsMort.Get_Fields_String("STATE") + " " + rsMort.Get_Fields_String("ZIP");
									}
								}
							}
						}
						strAddressBar = "    " + modGlobalFunctions.PadStringWithSpaces(rsMort.Get_Fields_String("Name"), 36, false) + modGlobalFunctions.PadStringWithSpaces(" ", 11, false) + rsData.Get_Fields_String("OName");
						// this gets the user info
						if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OAddress1"))) == "" && Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OAddress2"))) == "" && Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OAddress3"))) == "")
						{
							strTemp = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
							strTemp2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
							strTemp3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
							if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
							{
								strTemp4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strTemp4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strTemp4 += " " + rsUT.Get_Fields_String("OZip");
								}
							}
							else
							{
								strTemp4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
								if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
								{
									strTemp4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
								}
								else
								{
									strTemp4 += rsUT.Get_Fields_String("OZip");
								}
							}
						}
						else if (Strings.Trim(strLastAddress) != "")
						{
							strTemp = strLastAddress;
							strTemp2 = strLastAddress2;
							strTemp3 = strLastAddress3;
							// MAL@20080320: Tracker Reference: 12766
							if (strLastCity != "")
							{
								strLastCity += ", ";
							}
							else
							{
								strLastCity = strLastCity;
							}
							if (strLastState != "")
							{
								strLastState += " ";
							}
							else
							{
								strLastState = strLastState;
							}
							// strTemp4 = strLastCity & ", " & strLastState & " " & strLastZip
							strTemp4 = strLastCity + strLastState + strLastZip;
						}
						else
						{
							strTemp = strLastAddress2;
							strTemp2 = strLastAddress3;
							// MAL@20080320: Tracker Reference: 12766
							if (strLastCity != "")
							{
								strLastCity += ", ";
							}
							else
							{
								strLastCity = strLastCity;
							}
							if (strLastState != "")
							{
								strLastState += " ";
							}
							else
							{
								strLastState = strLastState;
							}
							strTemp3 = strLastCity + strLastState + strLastZip;
							strTemp4 = "";
						}

						if (Strings.Trim(strLastOwner2) != "")
						{
							// move all of the fields down a level
							strTemp5 = strTemp4;
							strTemp4 = strTemp3;
							strTemp3 = strTemp2;
							strTemp2 = strTemp;
							strTemp = Strings.Trim(strLastOwner2);
						}

						if (Strings.Trim(strTemp4) == "")
						{
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp3) == "")
						{
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp2) == "")
						{
							strTemp2 = strTemp3;
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						if (Strings.Trim(strTemp) == "")
						{
							strTemp = strTemp2;
							strTemp2 = strTemp3;
							strTemp3 = strTemp4;
							strTemp4 = strTemp5;
							strTemp5 = "";
						}
						strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr1, 47, false) + strTemp;
						strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr2, 47, false) + strTemp2;
						if (Strings.Trim(strCMFAddr3) != "" || Strings.Trim(strTemp3) != "")
						{
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr3, 47, false) + strTemp3;
						}
						if (Strings.Trim(strCMFAddr4) != "" || Strings.Trim(strTemp4) != "")
						{
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces(strCMFAddr4, 47, false) + strTemp4;
						}
						if (Strings.Trim(strTemp5) != "")
						{
							strAddressBar += "\r\n" + "    " + modGlobalFunctions.PadStringWithSpaces("", 47, false) + strTemp5;
						}
						// set name and address bar for a mortgage holder
						modRhymalReporting.Statics.frfFreeReport[30].DatabaseFieldName = strAddressBar;
						rsMort.MoveNext();
					}
					else if (boolNewOwner)
					{
						// this is the last copy to be made, it is for the new owner
						if (intNewOwnerChoice > 0)
						{
							// if the user is getting charged for certified mail, then save thier info to print on Cert Mail Forms
							boolPrintCMF = true;
						}
						else
						{
							boolPrintCMF = false;
						}
						// info for CMFNumbers table
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						strCMFName = FCConvert.ToString(rsUT.Get_Fields("OwnerName"));
						lngCMFMHNumber = 0;
						// lngCMFBillKey = 0
						// lngCMFBillKey = rsData.Fields("Bill")
						// this is for a copy for the new owner
						// If boolAddressCO Then   'this is the owner at time of billing C/O the new owner and the new owner address
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						strAddressBar = "    " + rsUT.Get_Fields("OwnerName");
						// name at time of billing
						// if the row needs to be added, then do it
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress1");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress2");
						}
						if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"))) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OAddress3");
						}
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strAddressBar += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strAddressBar += " " + rsUT.Get_Fields_String("OZip");
							}
						}
						else
						{
							strAddressBar += "\r\n" + "    " + rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strAddressBar += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strAddressBar += rsUT.Get_Fields_String("OZip");
							}
						}
						// this is information for the Certified Mailing List
						strCMFAddr1 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress1"));
						strCMFAddr2 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress2"));
						strCMFAddr3 = FCConvert.ToString(rsUT.Get_Fields_String("OAddress3"));
						// MAL@20080325: Tracker Reference: 12766
						if (FCConvert.ToString(rsUT.Get_Fields_String("OCity")) != "")
						{
							strCMFAddr4 = rsUT.Get_Fields_String("OCity") + ", " + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strCMFAddr4 += " " + rsUT.Get_Fields_String("OZip");
							}
						}
						else
						{
							strCMFAddr4 = rsUT.Get_Fields_String("OCity") + rsUT.Get_Fields_String("OState");
							if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("OZip4"))) != "")
							{
								strCMFAddr4 += rsUT.Get_Fields_String("OZip") + "-" + rsUT.Get_Fields_String("OZip4");
							}
							else
							{
								strCMFAddr4 += rsUT.Get_Fields_String("OZip");
							}
						}
						// set name and address bar for a New Owner
						modRhymalReporting.Statics.frfFreeReport[30].DatabaseFieldName = strAddressBar;
						rsUT.MoveNext();
					}
					else
					{
						strAddressBar = "    " + strLastOwner;
						if (Strings.Trim(strLastAddress) != "")
						{
							strAddressBar += "\r\n" + "    " + strLastAddress;
						}
						// strAddressBar = "    " & rsData.Fields("OwnerName")
						// If Trim(rsData.Fields("OAddress1")) <> "" Then
						// strAddressBar = strAddressBar & vbCrLf & "    " & rsData.Fields("OAddress1")
						// End If
						strAddressBar += "\r\n" + "    " + GetVariableValue_2("MUNI") + ", " + GetVariableValue_2("STATE") + " " + GetVariableValue_2("ZIP");
						// set name and address bar for a mortgage holder
						modRhymalReporting.Statics.frfFreeReport[30].DatabaseFieldName = strAddressBar;
					}
				}
				if (boolWater)
				{
					strTrueFalse = "1";
				}
				else
				{
					strTrueFalse = "0";
				}
				// this will add the information to the CMFNumber table so that it will be
				// easier to figure out which accounts need certified mail forms to be printed for them
				// MAL@20071016: Added support for new Process Type
				// Call Reference: 114824
				if (lngCMFMHNumber != 0)
				{
					// rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 20 AND MortgageHolder = " & lngCMFMHNumber & " AND MortgageHolder <> 0 AND boolWater = " & strTrueFalse
					// kgk 07312012  rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 20 AND MortgageHolder = " & lngCMFMHNumber & " AND MortgageHolder <> 0 AND boolWater = " & strTrueFalse & " AND ProcessType = '30DN'"
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					rsCMFNumbers.FindFirstRecord2("BillKey,Type,MortgageHolder,boolWater,ProcessType", rsData.Get_Fields("Bill") + ",20," + FCConvert.ToString(lngCMFMHNumber) + "," + strTrueFalse + ",30DN", ",");
				}
				else
				{
					if (boolCopy && boolNewOwner)
					{
						// rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 20 AND NewOwner = TRUE AND boolWater = " & strTrueFalse
						// kgk            rsCMFNumbers.FindFirstRecord2 , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 20 AND NewOwner = TRUE AND boolWater = " & strTrueFalse & " AND ProcessType = '30DN'"
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsCMFNumbers.FindFirstRecord2("BillKey,Type,NewOwner,boolWater,ProcessType", rsData.Get_Fields("Bill") + ",20,1," + strTrueFalse + ",30DN", ",");
						// kk08122014 trocls-49  ' ",'30DN'"
					}
					else
					{
						// rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 20 AND NewOwner = FALSE AND boolWater = " & strTrueFalse
						// kgk            rsCMFNumbers.FindFirstRecord , , "BillKey = " & rsData.Fields("Bill") & " AND Type = 20 AND NewOwner = FALSE AND boolWater = " & strTrueFalse & " AND ProcessType = '30DN'"
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						rsCMFNumbers.FindFirstRecord2("BillKey,Type,NewOwner,boolWater,ProcessType", rsData.Get_Fields("Bill") + ",20,0," + strTrueFalse + ",30DN", ",");
						// kk08122014 trocls-49  ' ",'30DN'"
					}
				}
				if (rsCMFNumbers.NoMatch)
				{
					rsCMFNumbers.AddNew();
				}
				else
				{
					rsCMFNumbers.Edit();
				}
				rsCMFNumbers.Set_Fields("boolWater", boolWater);
				rsCMFNumbers.Set_Fields("Name", strCMFName);
				rsCMFNumbers.Set_Fields("Address1", strCMFAddr1);
				rsCMFNumbers.Set_Fields("Address2", strCMFAddr2);
				rsCMFNumbers.Set_Fields("Address3", strCMFAddr3);
				rsCMFNumbers.Set_Fields("Address4", strCMFAddr4);
				rsCMFNumbers.Set_Fields("Billkey", lngCMFBillKey);
				rsCMFNumbers.Set_Fields("MortgageHolder", lngCMFMHNumber);
				rsCMFNumbers.Set_Fields("Account", rsData.Get_Fields_Int32("AccountKey"));
				// DJW@11192009 Added in section to put in BName, Service, and Actual Account Number to the table becuase we need the BName to order Certified Mail Notices
				rsCMFNumbers.Set_Fields("Service", rsData.Get_Fields_String("Service"));
				if (fecherFoundation.FCUtils.IsNull(rsData.Get_Fields_String("BName")) || FCConvert.ToString(rsData.Get_Fields_String("BName")) == "")
				{
					rsCMFNumbers.Set_Fields("BName", strCMFName);
				}
				else
				{
					rsCMFNumbers.Set_Fields("BName", rsData.Get_Fields_String("BName"));
				}
				rsCMFNumbers.Set_Fields("ActualAccountNumber", rsData.Get_Fields_Int32("ActualAccountNumber"));
				if (lngCMFMHNumber != 0)
				{
					// mort holder
					// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not boolPayMortCert
				}
				else
				{
					rsCMFNumbers.Set_Fields("NewOwner", false);
					if (boolCopy && boolNewOwner)
					{
						rsCMFNumbers.Set_Fields("NewOwner", true);
						// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not (intNewOwnerChoice = 2 Or intNewOwnerChoice = 3)    'Not boolPayNewOwner
						boolNewOwner = false;
					}
					else
					{
						// kgk trout-416    rsCMFNumbers.Fields("LabelOnly") = Not boolPayCert
					}
				}
				rsCMFNumbers.Set_Fields("Type", 20);
				rsCMFNumbers.Set_Fields("ProcessType", "30DN");
				// MAL@20071016
				rsCMFNumbers.Update(true);
				rsCombineBills.Dispose();
				rsTmp.Dispose();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Calculate Variable Error - " + rsData.Get_Fields_Int32("BillKey"), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int CalculateMortgageHolders()
		{
			int CalculateMortgageHolders = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the number of mortgage holders this account has
				bool boolUseRE = false;
				clsDRWrapper rsM = new clsDRWrapper();
				rsM.OpenRecordset("SELECT UseREAccount, UseMortgageHolder, REAccount, ID FROM Master WHERE ID = " + rsData.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
				if (!rsM.EndOfFile())
				{
					// MAL@20071106: Changed to take into account that Use Mortgage Holders can be selected without Use RE Account
					// Tracker Reference: 11257
					// boolUseRE = rsM.Fields("UseREAccount") And rsM.Fields("UseMortgageHolder") And rsM.Fields("REAccount") <> 0
					boolUseRE = rsM.Get_Fields_Boolean("UseMortgageHolder") && FCConvert.ToInt32(rsM.Get_Fields_Int32("REAccount")) != 0;
				}
				// MAL@20081106: Change to take the Receive Copies option into account
				// Tracker Reference: 16047
				if (boolUseRE)
				{
					// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & rsM.Fields("REAccount") & " AND Module = 'RE'", strGNDatabase
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + rsM.Get_Fields_Int32("REAccount") + " AND Module = 'RE' AND ISNULL(ReceiveCopies,0) = 1", "CentralData");
				}
				else
				{
					// rsMort.OpenRecordset "SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.MortgageHolderID WHERE Account = " & GetUTAccountNumber(rsData.Fields("AccountKey")) & " AND Module = 'UT'", strGNDatabase
					rsMort.OpenRecordset("SELECT * FROM MortgageAssociation INNER JOIN MortgageHolders ON MortgageAssociation.MortgageHolderID = MortgageHolders.ID WHERE Account = " + FCConvert.ToString(modUTStatusPayments.GetUTAccountNumber_2(rsData.Get_Fields_Int32("AccountKey"))) + " AND Module = 'UT' AND ISNULL(ReceiveCopies,0) = 1", "CentralData");
				}
				if (rsMort.EndOfFile() != true)
				{
					CalculateMortgageHolders = rsMort.RecordCount();
				}
				else
				{
					CalculateMortgageHolders = 0;
				}
				rsM.Dispose();
				return CalculateMortgageHolders;
			}
			catch
			{
				
				CalculateMortgageHolders = 0;
			}
			return CalculateMortgageHolders;
		}

		public void SaveLienStatus()
		{
			string strTemp;
			string strAcct = "";
			int lngCommaPosition = 0;
			if (Strings.Right(strAcctList, 1) == ",")
			{
				strAcctList = Strings.Left(strAcctList, strAcctList.Length - 1);
			}
			strTemp = strAcctList;
			// this statement will change the status of the accounts that have been process
			if (Strings.Trim(strAcctList) != "")
			{
				while (!(strTemp == ""))
				{
					lngCommaPosition = Strings.InStr(1, strTemp, ",", CompareConstants.vbBinaryCompare);
					if (lngCommaPosition > 0)
					{
						strAcct = Strings.Left(strTemp, lngCommaPosition - 1);
					}
					else
					{
						strAcct = strTemp;
						strTemp = "";
					}
					strTemp = Strings.Right(strTemp, strTemp.Length - lngCommaPosition);
					if (Strings.Trim(strAcct) != "")
					{
						rsData.OpenRecordset("SELECT * FROM Bill WHERE ID = " + strAcct, modExtraModules.strUTDatabase);
						if (!rsData.EndOfFile())
						{
							// rsData.OpenRecordset "SELECT * FROM Bill WHERE Bill = " & strAcct, strUTDatabase
							rsData.Execute("UPDATE Bill SET " + strWS + "LienProcessStatus = 1, " + strWS + "LienStatusEligibility = 2 WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND BillStatus = 'B' AND " + strWS + "LienStatusEligibility = 1", modExtraModules.strUTDatabase);
						}
					}
				}
			}
		}

		private void UpdateCopies(string strBillList)
		{
			// Tracker Reference: 13749
			clsDRWrapper rsCopy = new clsDRWrapper();
			rsCopy.OpenRecordset("SELECT * FROM Bill WHERE ID IN(" + strBillList + ")", modExtraModules.strUTDatabase);
			if (rsCopy.RecordCount() > 0)
			{
				rsCopy.MoveFirst();
				while (!rsCopy.EndOfFile())
				{
					rsCopy.Edit();
					rsCopy.Set_Fields("Copies", lngCopies);
					rsCopy.Update();
					rsCopy.MoveNext();
				}
			}
			rsCopy.Dispose();
		}

		private void ar30DayNotice_Load(object sender, System.EventArgs e)
		{

		}
	}
}
