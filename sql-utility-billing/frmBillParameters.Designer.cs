﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBillParameters.
	/// </summary>
	partial class frmBillParameters : BaseForm
	{
		public Wisej.Web.ImageList BillImages;
		public fecherFoundation.FCFrame Frame3;
		public FCGrid GridMessage;
		public fecherFoundation.FCFrame framRTB;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOky;
		public fecherFoundation.FCRichTextBox RichTextBox1;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress3;
		public fecherFoundation.FCTextBox txtAddress4;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label6;
		public FCGrid GridDelete;
		public fecherFoundation.FCComboBox cmbBill;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCPictureBox BillPic;
		public fecherFoundation.FCLabel Label1;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuAddMessage;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSepar2;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillParameters));
			Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images"))));
			Wisej.Web.ImageListEntry imageListEntry2 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images1"))));
			Wisej.Web.ImageListEntry imageListEntry3 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images2"))));
			Wisej.Web.ImageListEntry imageListEntry4 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images3"))));
			Wisej.Web.ImageListEntry imageListEntry5 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images4"))));
			Wisej.Web.ImageListEntry imageListEntry6 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images5"))));
			Wisej.Web.ImageListEntry imageListEntry7 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images6"))));
			Wisej.Web.ImageListEntry imageListEntry8 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images7"))));
			Wisej.Web.ImageListEntry imageListEntry9 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images8"))));
			Wisej.Web.ImageListEntry imageListEntry10 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images9"))));
			Wisej.Web.ImageListEntry imageListEntry11 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images10"))));
			Wisej.Web.ImageListEntry imageListEntry12 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("BillImages.Images11"))));
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			this.BillImages = new Wisej.Web.ImageList(this.components);
			this.Frame3 = new fecherFoundation.FCFrame();
			this.GridMessage = new fecherFoundation.FCGrid();
			this.framRTB = new fecherFoundation.FCFrame();
			this.cmdCancel = new fecherFoundation.FCButton();
			this.cmdOky = new fecherFoundation.FCButton();
			this.RichTextBox1 = new fecherFoundation.FCRichTextBox();
			this.Frame2 = new fecherFoundation.FCFrame();
			this.txtName = new fecherFoundation.FCTextBox();
			this.txtAddress2 = new fecherFoundation.FCTextBox();
			this.txtAddress3 = new fecherFoundation.FCTextBox();
			this.txtAddress4 = new fecherFoundation.FCTextBox();
			this.txtAddress1 = new fecherFoundation.FCTextBox();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label6 = new fecherFoundation.FCLabel();
			this.GridDelete = new fecherFoundation.FCGrid();
			this.cmbBill = new fecherFoundation.FCComboBox();
			this.Frame1 = new fecherFoundation.FCFrame();
			this.BillPic = new fecherFoundation.FCPictureBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuAddMessage = new fecherFoundation.FCToolStripMenuItem();
			this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSepar2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdAddMessage = new fecherFoundation.FCButton();
			this.cmdDelete = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
			this.Frame3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framRTB)).BeginInit();
			this.framRTB.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOky)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
			this.Frame2.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
			this.Frame1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.BillPic)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddMessage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 580);
			this.BottomPanel.Size = new System.Drawing.Size(840, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.Frame3);
			this.ClientArea.Controls.Add(this.Frame2);
			this.ClientArea.Controls.Add(this.GridDelete);
			this.ClientArea.Controls.Add(this.cmbBill);
			this.ClientArea.Controls.Add(this.Frame1);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Size = new System.Drawing.Size(840, 520);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdDelete);
			this.TopPanel.Controls.Add(this.cmdAddMessage);
			this.TopPanel.Size = new System.Drawing.Size(840, 60);
			this.TopPanel.TabIndex = 0;
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdAddMessage, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(137, 30);
			//FC:FINAL:MSH - issue #1089: remove header text
			//this.HeaderText.Text = "Bill Options";
			// 
			// BillImages
			// 
			this.BillImages.Images.AddRange(new Wisej.Web.ImageListEntry[] {
				imageListEntry1,
				imageListEntry2,
				imageListEntry3,
				imageListEntry4,
				imageListEntry5,
				imageListEntry6,
				imageListEntry7,
				imageListEntry8,
				imageListEntry9,
				imageListEntry10,
				imageListEntry11,
				imageListEntry12
			});
			this.BillImages.ImageSize = new System.Drawing.Size(256, 256);
			this.BillImages.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
			// 
			// Frame3
			// 
			this.Frame3.Controls.Add(this.GridMessage);
			this.Frame3.Controls.Add(this.framRTB);
			this.Frame3.Location = new System.Drawing.Point(460, 496);
			this.Frame3.Name = "Frame3";
			this.Frame3.Size = new System.Drawing.Size(332, 330);
			this.Frame3.TabIndex = 3;
			this.Frame3.Text = "Bill Messages";
			// 
			// GridMessage
			// 
			this.GridMessage.AllowSelection = false;
			this.GridMessage.AllowUserToResizeColumns = false;
			this.GridMessage.AllowUserToResizeRows = false;
			this.GridMessage.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridMessage.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridMessage.BackColorBkg = System.Drawing.Color.Empty;
			this.GridMessage.BackColorFixed = System.Drawing.Color.Empty;
			this.GridMessage.BackColorSel = System.Drawing.Color.Empty;
			this.GridMessage.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridMessage.Cols = 4;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridMessage.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.GridMessage.ColumnHeadersHeight = 30;
			this.GridMessage.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridMessage.DefaultCellStyle = dataGridViewCellStyle2;
			this.GridMessage.DragIcon = null;
			this.GridMessage.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
			this.GridMessage.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridMessage.ExtendLastCol = true;
			this.GridMessage.FixedCols = 0;
			this.GridMessage.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridMessage.FrozenCols = 0;
			this.GridMessage.GridColor = System.Drawing.Color.Empty;
			this.GridMessage.GridColorFixed = System.Drawing.Color.Empty;
			this.GridMessage.Location = new System.Drawing.Point(20, 30);
			this.GridMessage.Name = "GridMessage";
			this.GridMessage.RowHeadersVisible = false;
			this.GridMessage.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridMessage.RowHeightMin = 0;
			this.GridMessage.Rows = 1;
			this.GridMessage.ScrollTipText = null;
			this.GridMessage.ShowColumnVisibilityMenu = false;
			this.GridMessage.ShowFocusCell = false;
			this.GridMessage.Size = new System.Drawing.Size(292, 280);
			this.GridMessage.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridMessage.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
			this.GridMessage.TabIndex = 6;
			this.GridMessage.CellButtonClick += new System.EventHandler(this.GridMessage_CellButtonClick);
			this.GridMessage.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridMessage_ValidateEdit);
			this.GridMessage.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.GridMessage_MouseMoveEvent);
			// 
			// framRTB
			// 
			this.framRTB.AppearanceKey = "groupboxLeftBorder";
			this.framRTB.BackColor = System.Drawing.SystemColors.Window;
			this.framRTB.Controls.Add(this.cmdCancel);
			this.framRTB.Controls.Add(this.cmdOky);
			this.framRTB.Controls.Add(this.RichTextBox1);
			this.framRTB.Location = new System.Drawing.Point(20, 30);
			this.framRTB.Name = "framRTB";
			this.framRTB.Size = new System.Drawing.Size(292, 280);
			this.framRTB.TabIndex = 17;
			this.framRTB.Visible = false;
			// 
			// cmdCancel
			// 
			this.cmdCancel.AppearanceKey = "actionButton";
			this.cmdCancel.ForeColor = System.Drawing.Color.White;
			this.cmdCancel.Location = new System.Drawing.Point(96, 240);
			this.cmdCancel.Name = "cmdCancel";
			this.cmdCancel.Size = new System.Drawing.Size(66, 40);
			this.cmdCancel.TabIndex = 20;
			this.cmdCancel.Text = "Cancel";
			this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
			// 
			// cmdOky
			// 
			this.cmdOky.AppearanceKey = "actionButton";
			this.cmdOky.ForeColor = System.Drawing.Color.White;
			this.cmdOky.Location = new System.Drawing.Point(0, 240);
			this.cmdOky.Name = "cmdOky";
			this.cmdOky.Size = new System.Drawing.Size(66, 40);
			this.cmdOky.TabIndex = 19;
			this.cmdOky.Text = "Ok";
			this.cmdOky.Click += new System.EventHandler(this.cmdOky_Click);
			// 
			// RichTextBox1
			// 
			this.RichTextBox1.Location = new System.Drawing.Point(0, 30);
			this.RichTextBox1.Multiline = true;
			this.RichTextBox1.Name = "RichTextBox1";
			this.RichTextBox1.OLEDragMode = fecherFoundation.FCRichTextBox.OLEDragConstants.rtfOLEDragManual;
			this.RichTextBox1.OLEDropMode = fecherFoundation.FCRichTextBox.OLEDropConstants.rtfOLEDropNone;
			this.RichTextBox1.SelTabCount = null;
			this.RichTextBox1.Size = new System.Drawing.Size(292, 190);
			this.RichTextBox1.TabIndex = 18;
			// 
			// Frame2
			// 
			this.Frame2.BackColor = System.Drawing.SystemColors.Window;
			this.Frame2.Controls.Add(this.txtName);
			this.Frame2.Controls.Add(this.txtAddress2);
			this.Frame2.Controls.Add(this.txtAddress3);
			this.Frame2.Controls.Add(this.txtAddress4);
			this.Frame2.Controls.Add(this.txtAddress1);
			this.Frame2.Controls.Add(this.Label2);
			this.Frame2.Controls.Add(this.Label3);
			this.Frame2.Controls.Add(this.Label4);
			this.Frame2.Controls.Add(this.Label5);
			this.Frame2.Controls.Add(this.Label6);
			this.Frame2.Location = new System.Drawing.Point(30, 496);
			this.Frame2.Name = "Frame2";
			this.Frame2.Size = new System.Drawing.Size(400, 330);
			this.Frame2.TabIndex = 2;
			this.Frame2.Text = "Return Address";
			// 
			// txtName
			// 
			this.txtName.AutoSize = false;
			this.txtName.BackColor = System.Drawing.SystemColors.Window;
			this.txtName.LinkItem = null;
			this.txtName.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtName.LinkTopic = null;
			this.txtName.Location = new System.Drawing.Point(152, 30);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(228, 40);
			this.txtName.TabIndex = 1;
			// 
			// txtAddress2
			// 
			this.txtAddress2.AutoSize = false;
			this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress2.LinkItem = null;
			this.txtAddress2.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress2.LinkTopic = null;
			this.txtAddress2.Location = new System.Drawing.Point(152, 150);
			this.txtAddress2.Name = "txtAddress2";
			this.txtAddress2.Size = new System.Drawing.Size(228, 40);
			this.txtAddress2.TabIndex = 5;
			// 
			// txtAddress3
			// 
			this.txtAddress3.AutoSize = false;
			this.txtAddress3.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress3.LinkItem = null;
			this.txtAddress3.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress3.LinkTopic = null;
			this.txtAddress3.Location = new System.Drawing.Point(152, 210);
			this.txtAddress3.Name = "txtAddress3";
			this.txtAddress3.Size = new System.Drawing.Size(228, 40);
			this.txtAddress3.TabIndex = 7;
			// 
			// txtAddress4
			// 
			this.txtAddress4.AutoSize = false;
			this.txtAddress4.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress4.LinkItem = null;
			this.txtAddress4.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress4.LinkTopic = null;
			this.txtAddress4.Location = new System.Drawing.Point(152, 270);
			this.txtAddress4.Name = "txtAddress4";
			this.txtAddress4.Size = new System.Drawing.Size(228, 40);
			this.txtAddress4.TabIndex = 9;
			// 
			// txtAddress1
			// 
			this.txtAddress1.AutoSize = false;
			this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
			this.txtAddress1.LinkItem = null;
			this.txtAddress1.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAddress1.LinkTopic = null;
			this.txtAddress1.Location = new System.Drawing.Point(152, 90);
			this.txtAddress1.Name = "txtAddress1";
			this.txtAddress1.Size = new System.Drawing.Size(228, 40);
			this.txtAddress1.TabIndex = 3;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(20, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(43, 15);
			this.Label2.TabIndex = 0;
			this.Label2.Text = "NAME";
			// 
			// Label3
			// 
			this.Label3.AutoSize = true;
			this.Label3.Location = new System.Drawing.Point(20, 104);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(77, 15);
			this.Label3.TabIndex = 2;
			this.Label3.Text = "ADDRESS 1";
			// 
			// Label4
			// 
			this.Label4.AutoSize = true;
			this.Label4.Location = new System.Drawing.Point(20, 164);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(77, 15);
			this.Label4.TabIndex = 4;
			this.Label4.Text = "ADDRESS 2";
			// 
			// Label5
			// 
			this.Label5.AutoSize = true;
			this.Label5.Location = new System.Drawing.Point(20, 224);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(77, 15);
			this.Label5.TabIndex = 6;
			this.Label5.Text = "ADDRESS 3";
			// 
			// Label6
			// 
			this.Label6.AutoSize = true;
			this.Label6.Location = new System.Drawing.Point(20, 284);
			this.Label6.Name = "Label6";
			this.Label6.Size = new System.Drawing.Size(77, 15);
			this.Label6.TabIndex = 8;
			this.Label6.Text = "ADDRESS 4";
			// 
			// GridDelete
			// 
			this.GridDelete.AllowSelection = false;
			this.GridDelete.AllowUserToResizeColumns = false;
			this.GridDelete.AllowUserToResizeRows = false;
			this.GridDelete.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridDelete.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridDelete.BackColorBkg = System.Drawing.Color.Empty;
			this.GridDelete.BackColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.BackColorSel = System.Drawing.Color.Empty;
			this.GridDelete.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.GridDelete.Cols = 1;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridDelete.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridDelete.ColumnHeadersHeight = 30;
			this.GridDelete.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			this.GridDelete.ColumnHeadersVisible = false;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridDelete.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridDelete.DragIcon = null;
			this.GridDelete.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridDelete.FixedCols = 0;
			this.GridDelete.FixedRows = 0;
			this.GridDelete.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.FrozenCols = 0;
			this.GridDelete.GridColor = System.Drawing.Color.Empty;
			this.GridDelete.GridColorFixed = System.Drawing.Color.Empty;
			this.GridDelete.Location = new System.Drawing.Point(176, 535);
			this.GridDelete.Name = "GridDelete";
			this.GridDelete.ReadOnly = true;
			this.GridDelete.RowHeadersVisible = false;
			this.GridDelete.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridDelete.RowHeightMin = 0;
			this.GridDelete.Rows = 0;
			this.GridDelete.ScrollTipText = null;
			this.GridDelete.ShowColumnVisibilityMenu = false;
			this.GridDelete.ShowFocusCell = false;
			this.GridDelete.Size = new System.Drawing.Size(25, 10);
			this.GridDelete.StandardTab = true;
			this.GridDelete.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridDelete.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridDelete.TabIndex = 5;
			this.GridDelete.Visible = false;
			// 
			// cmbBill
			// 
			this.cmbBill.AutoSize = false;
			this.cmbBill.BackColor = System.Drawing.SystemColors.Window;
			this.cmbBill.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbBill.FormattingEnabled = true;
			this.cmbBill.Location = new System.Drawing.Point(30, 436);
			this.cmbBill.Name = "cmbBill";
			this.cmbBill.Size = new System.Drawing.Size(203, 40);
			this.cmbBill.TabIndex = 1;
			this.cmbBill.SelectedIndexChanged += new System.EventHandler(this.cmbBill_SelectedIndexChanged);
			// 
			// Frame1
			// 
			this.Frame1.AppearanceKey = "groupBoxNoBorders";
			this.Frame1.Controls.Add(this.BillPic);
			this.Frame1.Location = new System.Drawing.Point(30, 28);
			this.Frame1.Name = "Frame1";
			this.Frame1.Size = new System.Drawing.Size(782, 388);
			this.Frame1.TabIndex = 0;
			// 
			// BillPic
			// 
			this.BillPic.AllowDrop = true;
			this.BillPic.BorderStyle = Wisej.Web.BorderStyle.None;
			this.BillPic.DrawStyle = ((short)(0));
			this.BillPic.DrawWidth = ((short)(1));
			this.BillPic.FillStyle = ((short)(1));
			this.BillPic.FontTransparent = true;
			this.BillPic.Image = ((System.Drawing.Image)(resources.GetObject("BillPic.Image")));
			this.BillPic.Location = new System.Drawing.Point(0, 0);
			this.BillPic.Name = "BillPic";
			this.BillPic.Picture = ((System.Drawing.Image)(resources.GetObject("BillPic.Picture")));
			this.BillPic.Size = new System.Drawing.Size(762, 388);
			this.BillPic.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
			this.BillPic.TabIndex = 0;
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(33, 534);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(126, 18);
			this.Label1.TabIndex = 4;
			this.Label1.Text = "RETURN ADDRESS";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuAddMessage,
				this.mnuDelete,
				this.mnuSepar2,
				this.mnuSaveExit,
				this.Seperator,
				this.mnuExit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuAddMessage
			// 
			this.mnuAddMessage.Index = 0;
			this.mnuAddMessage.Name = "mnuAddMessage";
			this.mnuAddMessage.Text = "Add Message";
			this.mnuAddMessage.Click += new System.EventHandler(this.mnuAddMessage_Click);
			// 
			// mnuDelete
			// 
			this.mnuDelete.Index = 1;
			this.mnuDelete.Name = "mnuDelete";
			this.mnuDelete.Text = "Delete Message";
			this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// mnuSepar2
			// 
			this.mnuSepar2.Index = 2;
			this.mnuSepar2.Name = "mnuSepar2";
			this.mnuSepar2.Text = "-";
			// 
			// mnuSaveExit
			// 
			this.mnuSaveExit.Index = 3;
			this.mnuSaveExit.Name = "mnuSaveExit";
			this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuSaveExit.Text = "Save & Exit";
			this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuExit
			// 
			this.mnuExit.Index = 5;
			this.mnuExit.Name = "mnuExit";
			this.mnuExit.Text = "Exit";
			this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(369, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuSaveExit_Click);
			// 
			// cmdAddMessage
			// 
			this.cmdAddMessage.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdAddMessage.AppearanceKey = "toolbarButton";
			this.cmdAddMessage.Location = new System.Drawing.Point(714, 29);
			this.cmdAddMessage.Name = "cmdAddMessage";
			this.cmdAddMessage.Size = new System.Drawing.Size(98, 24);
			this.cmdAddMessage.TabIndex = 2;
			this.cmdAddMessage.Text = "Add Message";
			this.cmdAddMessage.Click += new System.EventHandler(this.mnuAddMessage_Click);
			// 
			// cmdDelete
			// 
			this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdDelete.AppearanceKey = "toolbarButton";
			this.cmdDelete.Location = new System.Drawing.Point(595, 29);
			this.cmdDelete.Name = "cmdDelete";
			this.cmdDelete.Size = new System.Drawing.Size(113, 24);
			this.cmdDelete.TabIndex = 1;
			this.cmdDelete.Text = "Delete Message";
			this.cmdDelete.Click += new System.EventHandler(this.mnuDelete_Click);
			// 
			// frmBillParameters
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(840, 688);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBillParameters";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Bill Options";
			this.Load += new System.EventHandler(this.frmBillParameters_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBillParameters_KeyDown);
			this.Resize += new System.EventHandler(this.frmBillParameters_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
			this.Frame3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framRTB)).EndInit();
			this.framRTB.ResumeLayout(false);
			this.framRTB.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdOky)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.RichTextBox1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
			this.Frame2.ResumeLayout(false);
			this.Frame2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridDelete)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
			this.Frame1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.BillPic)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdAddMessage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton cmdSave;
		private System.ComponentModel.IContainer components;
		private FCButton cmdDelete;
		private FCButton cmdAddMessage;
	}
}
