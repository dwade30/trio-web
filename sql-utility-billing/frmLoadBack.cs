﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using SharedApplication.Extensions;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmLoadBack.
	/// </summary>
	public partial class frmLoadBack : BaseForm
	{
		public frmLoadBack()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLoadBack InstancePtr
		{
			get
			{
				return (frmLoadBack)Sys.GetInstance(typeof(frmLoadBack));
			}
		}

		protected frmLoadBack _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/29/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/10/2006              *
		// ********************************************************
		clsDRWrapper rsUT = new clsDRWrapper();
		bool boolLoaded;
		bool boolForceAdd;
		bool boolLien;
		int intAction;
		int lngRateKey;
		DateTime dtDefaultInterestPTD;
		string strMod = "";
		bool boolPer2;
		bool boolPer3;
		bool boolPer4;
		clsDRWrapper rsRK = new clsDRWrapper();
		int lngType;
		bool boolBillStormwater;
		// kk03112015 trouts-106  Final Bill stormwater
		// Grid Columns
		int lngColAccount;
		int lngColName;
		int lngColName2;
		int lngColWInterestPTD;
		int lngColWIntOwed;
		int lngColWPrinOwed;
		int lngColWTaxOwed;
		int lngColWOrigIntOwed;
		int lngColWCostOwed;
		int lngColSInterestPTD;
		int lngColSIntOwed;
		int lngColSPrinOwed;
		int lngColSTaxOwed;
		int lngColSOrigIntOwed;
		int lngColSCostOwed;
		int lngColOverwrite;
		// if the value is 0 then do not overwrite, if it is -1 then overwrite
		int lngHiddenCol;
		int lngTypeCol;
		int lngColLocation;
		int lngColBook;
		int lngColSequence;
		int lngColStartDate;
		int lngColEndDate;
		int lngColBillDate;
		int lngColPrevious;
		int lngColCurrent;
		int lngColWAdj;
		int lngColSAdj;

		public void Init(short intType)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsCfg = new clsDRWrapper();

				switch (intType)
				{
					case 0:
						{
							// Load Backs
							lngType = 0;
							this.Text = "Load of Back Information";
							txtDefaultDate.Visible = true;
							lblInterestPTD.Visible = true;
							break;
						}
					case 1:
						{
							// Final Bills
							lngType = 1;
							this.Text = "Final Billing";

							// kk03132015 trouts-106  Add stormwater final billing
							boolBillStormwater = false;
							if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
							{
								rsCfg.OpenRecordset("SELECT BillStormwaterFee FROM UtilityBilling", modExtraModules.strUTDatabase);
								if (!rsCfg.EndOfFile())
								{
									boolBillStormwater = FCConvert.ToBoolean(rsCfg.Get_Fields_Boolean("BillStormwaterFee"));
								}
								rsCfg.DisposeOf();
							}
							txtDefaultDate.Visible = false;
							lblInterestPTD.Visible = false;
							if (modUTBilling.CheckBDAccountsSetup())
							{
								this.Show(App.MainForm);
							}
							else
							{
								MessageBox.Show("Please enter the Commitment Account in Budgetary.", "Missing Commitment Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								this.Unload();
								return;
							}
							break;
						}
				}
				//end switch
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void cboRateKeys_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			lngRateKey = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboRateKeys.Text, 5))));
			if (vsAccounts.Rows > 1)
			{
				ans = MessageBox.Show("If you change the rate key without first saving you will lose all of your entered data.  Do you wish to continue?", "Continue?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					vsAccounts.Rows = 1;
					SetGridHeight();
				}
				else
				{
					SetRateKeyCombo(ref lngRateKey);
				}
			}
			if (Strings.Mid(cboRateKeys.Text, 8, 1) == "L")
			{
				boolLien = true;
			}
			else
			{
				boolLien = false;
			}
			// MAL@20070928: Disable the owed interest column if RateKey = "R"
			// Call Reference: 117213
			if (vsAccounts.Visible == true && lngType == 0)
			{
				// kgk 06-15-2011 trout-730  Add check for loadback, not for final bill
				if (Strings.Mid(cboRateKeys.Text, 8, 1) == "R")
				{
					vsAccounts.ColHidden(lngColWOrigIntOwed, true);
					vsAccounts.ColHidden(lngColSOrigIntOwed, true);
				}
				else
				{
					vsAccounts.ColHidden(lngColWOrigIntOwed, false);
					vsAccounts.ColHidden(lngColSOrigIntOwed, false);
				}
			}
		}

		private void frmLoadBack_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				boolLoaded = true;
				if (boolForceAdd)
				{
					mnuFileAdd_Click();
					boolForceAdd = false;
				}
			}
			else
			{
				// FormatGrid
			}
			vsAccounts.EditingControlShowing -= vsAccounts_EditingControlShowing;
			vsAccounts.EditingControlShowing += vsAccounts_EditingControlShowing;
		}

		private void vsAccounts_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//FC:FINAL:MSH - issue #1004: assign control when EditControl is appeared
				e.Control.KeyPress -= vsAccounts_KeyPressEdit;
				e.Control.KeyPress += vsAccounts_KeyPressEdit;
			}
		}

		private void frmLoadBack_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				KeyCode = (Keys)0;
				if (mnuFileNew.Visible && mnuFileNew.Enabled)
				{
					mnuFileNew_Click();
				}
			}
		}

		private void frmLoadBack_Load(object sender, System.EventArgs e)
		{

			try
			{
				// On Error GoTo ERROR_HANDLER
				// these will keep track of the columns even if I move them
				lngColAccount = 0;
				lngColName = 1;
				lngColName2 = 2;
				lngTypeCol = 4;
				lngHiddenCol = 3;
				lngColOverwrite = 5;
				lngColWInterestPTD = 6;
				lngColWIntOwed = 7;
				lngColWPrinOwed = 8;
				lngColWTaxOwed = 9;
				lngColWOrigIntOwed = 10;
				lngColWCostOwed = 11;
				lngColSInterestPTD = 12;
				lngColSIntOwed = 13;
				lngColSPrinOwed = 14;
				lngColSTaxOwed = 15;
				lngColSOrigIntOwed = 16;
				lngColSCostOwed = 17;
				lngColLocation = 2;
				lngColCurrent = 4;
				lngColPrevious = 3;
				lngColStartDate = 5;
				lngColEndDate = 6;
				lngColBillDate = 7;
				lngColBook = 8;
				lngColSequence = 9;
				lngColWAdj = 10;
				lngColSAdj = 11;
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
				modGlobalFunctions.SetTRIOColors(this);

				rsUT.OpenRecordset("SELECT Master.*, " + "Master.DeedName1 AS OwnerName, Master.DeedName2 AS SecondOwnerName, pBill.FullNameLF AS Name ,pBill2.FullNameLF AS Name2, " + "StreetNumber, Apt, StreetName " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID " + "INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID " + "LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pOwn2 ON pOwn2.ID = Master.secondOwnerpartyid " + "LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID", modExtraModules.strUTDatabase);
				if (rsUT.EndOfFile())
				{
					MessageBox.Show("No Utility Billing accounts have been found.", "Missing Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
				LoadRateKeys();
				if (cboRateKeys.Items.Count > 0)
				{
					cboRateKeys.SelectedIndex = 0;
					lngRateKey = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboRateKeys.Text, 5))));
				}
				else
				{
					boolForceAdd = true;
					// MsgBox "No rate keys found", vbInformation, "No Info"
				}
				switch (lngType)
				{
					case 0:
						{
							txtDefaultDate.ToolTipText = "Effective interest date to start calculating interest for this bill.";
							break;
						}
					case 1:
						{
							txtDefaultDate.ToolTipText = "Bill date for final bill.";
							break;
						}
				}
				//end switch
				ToolTip1.SetToolTip(lblInterestPTD, txtDefaultDate.ToolTipText);
				// "Effective interest date to start calculating interest for this bill."
				FormatGrid();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmLoadBack_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				if (fraAccountSearch.Visible)
				{
					ReturnFromSearch_2(0);
				}
				else
				{
					this.Unload();
				}
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void frmLoadBack_Resize(object sender, System.EventArgs e)
		{
			if (fraAccountSearch.Visible)
			{
				SetSearchGridHeight();
			}
			else
			{
				SetGridHeight();
				FormatGrid();
			}
		}

		private void mnuFileAdd_Click(object sender, System.EventArgs e)
		{
			int lngPassRateKey = 0;
			frmUTAuditInfo.InstancePtr.Init(FCConvert.ToInt16(lngType + 1), DateTime.Now, DateTime.Now, DateTime.Now, ref lngPassRateKey);
			this.Hide();
		}

		public void mnuFileAdd_Click()
		{
			mnuFileAdd_Click(mnuFileAdd, new System.EventArgs());
		}

		private void mnuFileDelete_Click(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: ans As short, int --> As DialogResult
			DialogResult ans;
			string strText = "";
			switch (lngType)
			{
				case 0:
					{
						strText = "Are you sure you want to delete this load back information?";
						break;
					}
				case 1:
					{
						strText = "Are you sure you want to delete this bill information?";
						break;
					}
			}
			//end switch
			if (vsAccounts.Row > 0)
			{
				ans = MessageBox.Show(strText, "Delete Info?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (ans == DialogResult.Yes)
				{
					vsAccounts.RemoveItem(vsAccounts.Row);
					SetGridHeight();
				}
			}
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFileNew_Click(object sender, System.EventArgs e)
		{
			// add a line to the grid and put the focus there
			// If TownService = "W" Then
			// vsAccounts.AddItem "" & vbTab & "" & vbTab & "W" & vbTab & txtDefaultDate.Text & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00"
			// ElseIf TownService = "S" Then
			// vsAccounts.AddItem "" & vbTab & "" & vbTab & "S" & vbTab & txtDefaultDate.Text & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00"
			// Else
			// vsAccounts.AddItem "" & vbTab & "" & vbTab & "W" & vbTab & txtDefaultDate.Text & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00"
			// vsAccounts.AddItem "" & vbTab & "" & vbTab & "S" & vbTab & txtDefaultDate.Text & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00" & vbTab & "0.00"
			// vsAccounts.Select vsAccounts.rows - 1, 0, vsAccounts.rows - 1, vsAccounts.Cols - 1
			// Call vsAccounts.CellBorder(RGB(1, 1, 1), -1, -1, -1, 1, -1, -1)
			// End If
			vsAccounts.AddItem("");
			if (lngType == 0)
			{
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColWInterestPTD, txtDefaultDate.Text);
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColWIntOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColWPrinOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColWTaxOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColWOrigIntOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColWCostOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColSInterestPTD, txtDefaultDate.Text);
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColSIntOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColSPrinOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColSTaxOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColSOrigIntOwed, "0.00");
				vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngColSCostOwed, "0.00");
			}
			SetGridHeight();
			if (vsAccounts.Visible && vsAccounts.Enabled)
			{
				vsAccounts.Focus();
				if (lngType == 0)
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngTypeCol, modUTStatusPayments.Statics.TownService);
				}
				else
				{
					vsAccounts.TextMatrix(vsAccounts.Rows - 1, lngTypeCol, FCConvert.ToString(0));
				}
				vsAccounts.Select(vsAccounts.Rows - 1, lngColAccount);
			}
		}

		public void mnuFileNew_Click()
		{
			mnuFileNew_Click(mnuFileNew, new System.EventArgs());
		}

		private void mnuFileSave_Click(object sender, System.EventArgs e)
		{
			// save all of the information
			if (SaveInformation())
			{
				// this will clear the grid so that the user can keep going
				vsAccounts.Rows = 1;
			}
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			int lngRW = 0;
			int lngAccount = 0;
			if (fraAccountSearch.Visible)
			{
				// search for accounts
				// SearchForAccounts
				// load the selected account
				if (vsAccountSearch.Rows > 1 && vsAccountSearch.Row > 0)
				{
					lngRW = vsAccountSearch.Row;
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(lngRW, 1))));
					ReturnFromSearch(ref lngAccount);
				}
			}
			else
			{
				// save the information
				if (SaveInformation())
				{
					this.Unload();
				}
			}
		}

		private bool LoadBackInformation()
		{
			bool LoadBackInformation = false;
            clsDRWrapper rsAccountInfo = new clsDRWrapper();
            clsDRWrapper rsMeterInfo = new clsDRWrapper();
            clsDRWrapper rsInfo = new clsDRWrapper();
            clsDRWrapper rsDeleteInfo = new clsDRWrapper();
            clsDRWrapper rsRK = new clsDRWrapper();
            clsDRWrapper rsNewRK = new clsDRWrapper();
            clsDRWrapper rsUtilityBilling = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER

                int counter;
                int lngRegRK = 0;
                int lngComboRK;
                int lngMK = 0;
                int lngBook = 0;
                int lngBK;
                // rsBillInfo = new clsDRWrapper();

                lngComboRK = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(cboRateKeys.Text, 5))));
                rsUtilityBilling.OpenRecordset("SELECT * FROM UtilityBilling");
                if (boolLien)
                {
                    // add a different rate key to use for the bills
                    // rsNewRK.OpenRecordset "SELECT * FROM RateKeys WHERE RateKey = 0"
                    rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey),
                        modExtraModules.strUTDatabase);
                    // TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
                    rsNewRK.OpenRecordset(
                        "SELECT * FROM RateKeys WHERE RateType = 'R' AND Description = 'LoadBack Regular' AND BillDate = '" +
                        rsRK.Get_Fields_DateTime("BillDate") + "' AND IntStart = '" +
                        rsRK.Get_Fields_DateTime("IntStart") + "' AND Start = '" + rsRK.Get_Fields_DateTime("Start") +
                        "' AND [End] = '" + rsRK.Get_Fields("End") + "'", modExtraModules.strUTDatabase);
                    if (rsNewRK.EndOfFile() != true && rsNewRK.BeginningOfFile() != true)
                    {
                        lngRegRK = FCConvert.ToInt32(rsNewRK.Get_Fields_Int32("ID"));
                    }
                    else
                    {
                        rsNewRK.AddNew();
                        rsNewRK.Set_Fields("RateType", "R");
                        rsNewRK.Set_Fields("Description", "LoadBack Regular");
                        if (!rsRK.EndOfFile())
                        {
                            // copy from the lien record
                            rsNewRK.Set_Fields("BillDate", rsRK.Get_Fields_DateTime("BillDate"));
                            rsNewRK.Set_Fields("IntStart", rsRK.Get_Fields_DateTime("IntStart"));
                            rsNewRK.Set_Fields("Start", rsRK.Get_Fields_DateTime("Start"));
                            // TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
                            rsNewRK.Set_Fields("End", rsRK.Get_Fields("End"));
                        }

                        rsNewRK.Update();
                        lngRegRK = FCConvert.ToInt32(rsNewRK.Get_Fields_Int32("ID"));
                    }
                }

                LoadBackInformation = true;
                rsInfo.OpenRecordset("SELECT * FROM Bill WHERE ID = 0");
                for (counter = 1; counter <= vsAccounts.Rows - 1; counter++)
                {
                    int lngAccountNumber = FCConvert.ToInt32(vsAccounts.TextMatrix(counter, lngColAccount));
                    if (FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColOverwrite)) == 1)
                    {
                        // if this is an overrite then
                        // find the existing bill
                        if (boolLien)
                        {
                            rsInfo.OpenRecordset(
                                "SELECT * FROM Bill WHERE AccountKey = " +
                                FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAccountNumber)) +
                                " AND BillingRateKey = " + FCConvert.ToString(lngRegRK), modExtraModules.strUTDatabase);
                        }
                        else
                        {
                            rsInfo.OpenRecordset(
                                "SELECT * FROM Bill WHERE AccountKey = " +
                                FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAccountNumber)) +
                                " AND BillingRateKey = " + FCConvert.ToString(lngRateKey),
                                modExtraModules.strUTDatabase);
                        }

                        // and remove any liens against it
                        if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("WLienRecordNumber")) != 0)
                        {
                            rsDeleteInfo.Execute(
                                "DELETE FROM Lien WHERE ID = " + rsInfo.Get_Fields_Int32("WLienRecordNumber"),
                                modExtraModules.strUTDatabase);
                        }

                        if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("SLienRecordNumber")) != 0)
                        {
                            rsDeleteInfo.Execute(
                                "DELETE FROM Lien WHERE ID = " + rsInfo.Get_Fields_Int32("SLienRecordNumber"),
                                modExtraModules.strUTDatabase);
                        }

                        // also remove any charged interest payment lines that were added with the previous bill
                        if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("WLienRecordNumber")) != 0)
                        {
                            rsDeleteInfo.Execute(
                                "DELETE FROM PaymentRec WHERE BillKey = " +
                                rsInfo.Get_Fields_Int32("WLienRecordNumber") + " AND ISNULL(Lien,0) = 1",
                                modExtraModules.strUTDatabase);
                        }
                        else
                        {
                            rsDeleteInfo.Execute(
                                "DELETE FROM PaymentRec WHERE BillKey = " + rsInfo.Get_Fields_Int32("ID") +
                                " AND ISNULL(Lien,0) = 0", modExtraModules.strUTDatabase);
                        }

                        if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("SLienRecordNumber")) != 0)
                        {
                            rsDeleteInfo.Execute(
                                "DELETE FROM PaymentRec WHERE BillKey = " +
                                rsInfo.Get_Fields_Int32("SLienRecordNumber") + " AND ISNULL(Lien,0) = 1",
                                modExtraModules.strUTDatabase);
                        }
                        else
                        {
                            rsDeleteInfo.Execute(
                                "DELETE FROM PaymentRec WHERE BillKey = " + rsInfo.Get_Fields_Int32("ID") +
                                " AND ISNULL(Lien,0) = 0", modExtraModules.strUTDatabase);
                        }

                        rsInfo.Edit();
                    }
                    else
                    {
                        rsInfo.AddNew();
                    }

                    rsInfo.Update();
                    lngBK = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID"));
                    lngAccountNumber = FCConvert.ToInt32(vsAccounts.TextMatrix(counter, lngColAccount));
                    rsMeterInfo.OpenRecordset(
                        "SELECT * FROM MeterTable WHERE AccountKey = " +
                        FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAccountNumber)) +
                        " AND MeterNumber = 1", modExtraModules.strUTDatabase);
                    if (!rsMeterInfo.EndOfFile())
                    {
                        lngMK = FCConvert.ToInt32(rsMeterInfo.Get_Fields_Int32("ID"));
                        lngBook = FCConvert.ToInt32(rsMeterInfo.Get_Fields_Int32("BookNumber"));
                    }
                    else
                    {
                        lngMK = 0;
                        lngBook = 0;
                    }

                    int lngAcctKey = modUTStatusPayments.GetAccountKeyUT_2(
                        FCConvert.ToInt32(Conversion.Val(vsAccounts.TextMatrix(counter, lngColAccount))));
                    rsAccountInfo.OpenRecordset(modUTStatusPayments.UTMasterQuery(lngAcctKey),
                        modExtraModules.strUTDatabase);
                    // k  "SELECT * FROM Master WHERE AccountNumber = " & vsAccounts.TextMatrix(counter, lngColAccount), strUTDatabase
                    rsInfo.Set_Fields("AccountKey",
                        modUTStatusPayments.GetAccountKeyUT_2(FCConvert.ToInt32(
                            FCConvert.ToString(Conversion.Val(vsAccounts.TextMatrix(counter, lngColAccount))))));
                    rsInfo.Set_Fields("ActualAccountNumber",
                        FCConvert.ToString(Conversion.Val(vsAccounts.TextMatrix(counter, lngColAccount))));
                    if (boolLien)
                    {
                        rsInfo.Set_Fields("BillNumber", lngRegRK);
                        rsInfo.Set_Fields("BillingRateKey", lngRegRK);
                    }
                    else
                    {
                        rsInfo.Set_Fields("BillNumber", lngComboRK);
                        rsInfo.Set_Fields("BillingRateKey", lngComboRK);
                        rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngComboRK),
                            modExtraModules.strUTDatabase);
                    }

                    rsInfo.Set_Fields("BillingYear", 0);
                    rsInfo.Set_Fields("MeterKey", lngMK);
                    rsInfo.Set_Fields("Book", lngBook);
                    rsInfo.Set_Fields("Service", modUTStatusPayments.Statics.TownService);
                    rsInfo.Set_Fields("BillStatus", "B");
                    rsInfo.Set_Fields("LoadBack", true);
                    if (!boolLien)
                    {
                        rsInfo.Set_Fields("WLienStatusEligibility", 0);
                        rsInfo.Set_Fields("WLienProcessStatus", 0);
                        rsInfo.Set_Fields("WLienRecordNumber", 0);
                        rsInfo.Set_Fields("WCombinationLienKey", 0);
                        rsInfo.Set_Fields("SLienStatusEligibility", 0);
                        rsInfo.Set_Fields("SLienProcessStatus", 0);
                        rsInfo.Set_Fields("SLienRecordNumber", 0);
                        rsInfo.Set_Fields("SCombinationLienKey", 0);
                    }
                    else
                    {
                        if (modUTStatusPayments.Statics.TownService != "S" &&
                            (FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColWPrinOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColWTaxOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColWIntOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColWCostOwed)) != 0))
                        {
                            rsInfo.Set_Fields("WLienStatusEligibility", 5);
                            rsInfo.Set_Fields("WLienProcessStatus", 4);
                            rsInfo.Set_Fields("WLienRecordNumber",
                                CreateLienRecord_240(counter, 
                                    rsInfo.Get_Fields_Int32("AccountKey"),
                                    FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColWIntOwed)), true));
                            rsInfo.Set_Fields("WCombinationLienKey", rsInfo.Get_Fields_Int32("ID"));
                            rsInfo.Set_Fields("BillNumber", lngRegRK);
                            rsInfo.Set_Fields("BillingRateKey", lngRegRK);
                        }
                        else
                        {
                            rsInfo.Set_Fields("WLienStatusEligibility", 0);
                            rsInfo.Set_Fields("WLienProcessStatus", 0);
                            rsInfo.Set_Fields("WLienRecordNumber", 0);
                            rsInfo.Set_Fields("WCombinationLienKey", 0);
                        }

                        if (modUTStatusPayments.Statics.TownService != "W" &&
                            (FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColSPrinOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColSTaxOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColSIntOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColSCostOwed)) != 0))
                        {
                            rsInfo.Set_Fields("SLienStatusEligibility", 5);
                            rsInfo.Set_Fields("SLienProcessStatus", 4);
                            rsInfo.Set_Fields("SLienRecordNumber",
                                CreateLienRecord_240(counter, 
                                    rsInfo.Get_Fields_Int32("AccountKey"),
                                    FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColSIntOwed)), false));
                            rsInfo.Set_Fields("SCombinationLienKey", rsInfo.Get_Fields_Int32("ID"));
                            rsInfo.Set_Fields("BillingRateKey", lngRegRK);
                        }
                        else
                        {
                            rsInfo.Set_Fields("SLienStatusEligibility", 0);
                            rsInfo.Set_Fields("SLienProcessStatus", 0);
                            rsInfo.Set_Fields("SLienRecordNumber", 0);
                            rsInfo.Set_Fields("SCombinationLienKey", 0);
                        }
                    }

                    if (vsAccounts.TextMatrix(counter, lngTypeCol) != "S")
                    {
                        rsInfo.Set_Fields("WIntPaidDate",
                            DateAndTime.DateValue(vsAccounts.TextMatrix(counter, lngColWInterestPTD)));
                        if (Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColWPrinOwed)))
                        {
                            rsInfo.Set_Fields("WPrinOwed",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColWPrinOwed)));
                        }
                        else
                        {
                            rsInfo.Set_Fields("WPrinOwed", 0);
                            vsAccounts.TextMatrix(counter, lngColWPrinOwed, "0.00");
                        }

                        if (Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColWTaxOwed)))
                        {
                            rsInfo.Set_Fields("WTaxOwed",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColWTaxOwed)));
                        }
                        else
                        {
                            rsInfo.Set_Fields("WTaxOwed", 0);
                            vsAccounts.TextMatrix(counter, lngColWTaxOwed, "0.00");
                        }

                        if (Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColWOrigIntOwed)))
                        {
                            rsInfo.Set_Fields("WIntOwed",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColWOrigIntOwed)));
                        }
                        else
                        {
                            rsInfo.Set_Fields("WIntOwed", 0);
                            vsAccounts.TextMatrix(counter, lngColWOrigIntOwed, "0.00");
                        }

                        if (Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColWCostOwed)))
                        {
                            rsInfo.Set_Fields("WCostOwed",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColWCostOwed)));
                        }
                        else
                        {
                            rsInfo.Set_Fields("WCostOwed", 0);
                            vsAccounts.TextMatrix(counter, lngColWCostOwed, "0.00");
                        }

                        if (!Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColWIntOwed)))
                        {
                            vsAccounts.TextMatrix(counter, lngColWIntOwed, "0.00");
                        }

                        if (FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColWIntOwed)) != 0 &&
                            FCConvert.ToInt32(rsInfo.Get_Fields_Int32("WLienRecordNumber")) == 0)
                        {
                            rsInfo.Set_Fields("WIntAdded",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColWIntOwed)) * -1);
                            //if (rsInfo.Get_Fields_Double("WIntAdded") != 0 && !boolLien)
                            //{
                            //    // Add a CHGINT Line for this bill
                            //    CreateCHGINTForBill_242(true, rsInfo.Get_Fields_Int32("ID"),
                            //        rsInfo.Get_Fields_Int32("AccountKey"), false,
                            //        vsAccounts.TextMatrix(counter, lngColWIntOwed).ToDecimalValue());
                            //}
                        }
                        else
                        {
                            rsInfo.Set_Fields("WIntAdded", 0);
                        }

                        rsInfo.Set_Fields("WPrinPaid", 0);
                        rsInfo.Set_Fields("WTaxPaid", 0);
                        rsInfo.Set_Fields("WIntPaid", 0);
                        rsInfo.Set_Fields("WCostPaid", 0);
                    }

                    if (vsAccounts.TextMatrix(counter, lngTypeCol) != "W")
                    {
                        rsInfo.Set_Fields("SIntPaidDate",
                            DateAndTime.DateValue(vsAccounts.TextMatrix(counter, lngColSInterestPTD)));
                        if (Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColSPrinOwed)))
                        {
                            rsInfo.Set_Fields("SPrinOwed",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColSPrinOwed)));
                        }
                        else
                        {
                            rsInfo.Set_Fields("SPrinOwed", 0);
                            vsAccounts.TextMatrix(counter, lngColSPrinOwed, "0.00");
                        }

                        if (Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColSTaxOwed)))
                        {
                            rsInfo.Set_Fields("STaxOwed",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColSTaxOwed)));
                        }
                        else
                        {
                            rsInfo.Set_Fields("STaxOwed", 0);
                            vsAccounts.TextMatrix(counter, lngColSTaxOwed, "0.00");
                        }

                        if (Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColSOrigIntOwed)))
                        {
                            rsInfo.Set_Fields("SIntOwed",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColSOrigIntOwed)));
                        }
                        else
                        {
                            rsInfo.Set_Fields("SIntOwed", 0);
                            vsAccounts.TextMatrix(counter, lngColSOrigIntOwed, "0.00");
                        }

                        if (Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColSCostOwed)))
                        {
                            rsInfo.Set_Fields("SCostOwed",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColSCostOwed)));
                        }
                        else
                        {
                            rsInfo.Set_Fields("SCostOwed", 0);
                            vsAccounts.TextMatrix(counter, lngColSCostOwed, "0.00");
                        }

                        if (Strings.Trim(vsAccounts.TextMatrix(counter, lngColSIntOwed)) == "")
                        {
                            vsAccounts.TextMatrix(counter, lngColSIntOwed, FCConvert.ToString(0));
                        }

                        if (!Information.IsNumeric(vsAccounts.TextMatrix(counter, lngColSIntOwed)))
                        {
                            vsAccounts.TextMatrix(counter, lngColSIntOwed, "0.00");
                        }

                        if (FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColSIntOwed)) != 0 &&
                            FCConvert.ToInt32(rsInfo.Get_Fields_Int32("SLienRecordNumber")) == 0)
                        {
                            rsInfo.Set_Fields("SIntAdded",
                                FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColSIntOwed)) * -1);
                            //if (rsInfo.Get_Fields_Double("SIntAdded") != 0 && !boolLien)
                            //{
                            //    // Add a CHGINT Line for this bill
                            //    CreateCHGINTForBill_242(false, rsInfo.Get_Fields_Int32("ID"),
                            //        rsInfo.Get_Fields_Int32("AccountKey"), false,
                            //        vsAccounts.TextMatrix(counter, lngColSIntOwed).ToDecimalValue());
                            //}
                        }
                        else
                        {
                            rsInfo.Set_Fields("SIntAdded", 0);
                        }

                        rsInfo.Set_Fields("SPrinPaid", 0);
                        rsInfo.Set_Fields("STaxPaid", 0);
                        rsInfo.Set_Fields("SIntPaid", 0);
                        rsInfo.Set_Fields("SCostPaid", 0);
                    }

                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    rsInfo.Set_Fields("Location",
                        rsAccountInfo.Get_Fields("StreetNumber") +
                        Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt"))) + " " +
                        Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName"))));
                    rsInfo.Set_Fields("CreationDate", DateTime.Today);
                    rsInfo.Set_Fields("LastUpdatedDate", DateTime.Today);
                    string vbPorterVar = vsAccounts.TextMatrix(counter, lngTypeCol);
                    if (vbPorterVar == "W")
                    {
                        if (FCConvert.ToBoolean(rsUtilityBilling.Get_Fields_Boolean("WBillToOwner")))
                        {
                            // TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
                            rsInfo.Set_Fields("BName", rsAccountInfo.Get_Fields("OwnerName"));
                            // TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
                            rsInfo.Set_Fields("BName2", rsAccountInfo.Get_Fields("SecondOwnerName"));
                            rsInfo.Set_Fields("BAddress1", rsAccountInfo.Get_Fields_String("OAddress1"));
                            rsInfo.Set_Fields("BAddress2", rsAccountInfo.Get_Fields_String("OAddress2"));
                            rsInfo.Set_Fields("BAddress3", rsAccountInfo.Get_Fields_String("OAddress3"));
                            rsInfo.Set_Fields("BCity", rsAccountInfo.Get_Fields_String("OCity"));
                            rsInfo.Set_Fields("BState", rsAccountInfo.Get_Fields_String("OState"));
                            rsInfo.Set_Fields("BZip", rsAccountInfo.Get_Fields_String("OZip"));
                            // rsInfo.Fields("BZip4") = rsAccountInfo.Fields("OZip4")
                        }
                        else
                        {
                            rsInfo.Set_Fields("BName", rsAccountInfo.Get_Fields_String("Name"));
                            rsInfo.Set_Fields("BName2", rsAccountInfo.Get_Fields_String("Name2"));
                            rsInfo.Set_Fields("BAddress1", rsAccountInfo.Get_Fields_String("BAddress1"));
                            rsInfo.Set_Fields("BAddress2", rsAccountInfo.Get_Fields_String("BAddress2"));
                            rsInfo.Set_Fields("BAddress3", rsAccountInfo.Get_Fields_String("BAddress3"));
                            rsInfo.Set_Fields("BCity", rsAccountInfo.Get_Fields_String("BCity"));
                            rsInfo.Set_Fields("BState", rsAccountInfo.Get_Fields_String("BState"));
                            rsInfo.Set_Fields("BZip", rsAccountInfo.Get_Fields_String("BZip"));
                            // rsInfo.Fields("BZip4") = rsAccountInfo.Fields("BZip4")
                        }
                    }
                    else if (vbPorterVar == "S")
                    {
                        if (FCConvert.ToBoolean(rsUtilityBilling.Get_Fields_Boolean("SBillToOwner")))
                        {
                            // TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
                            rsInfo.Set_Fields("BName", rsAccountInfo.Get_Fields("OwnerName"));
                            // TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
                            rsInfo.Set_Fields("BName2", rsAccountInfo.Get_Fields("SecondOwnerName"));
                            rsInfo.Set_Fields("BAddress1", rsAccountInfo.Get_Fields_String("OAddress1"));
                            rsInfo.Set_Fields("BAddress2", rsAccountInfo.Get_Fields_String("OAddress2"));
                            rsInfo.Set_Fields("BAddress3", rsAccountInfo.Get_Fields_String("OAddress3"));
                            rsInfo.Set_Fields("BCity", rsAccountInfo.Get_Fields_String("OCity"));
                            rsInfo.Set_Fields("BState", rsAccountInfo.Get_Fields_String("OState"));
                            rsInfo.Set_Fields("BZip", rsAccountInfo.Get_Fields_String("OZip"));
                            // rsInfo.Fields("BZip4") = rsAccountInfo.Fields("OZip4")
                        }
                        else
                        {
                            rsInfo.Set_Fields("BName", rsAccountInfo.Get_Fields_String("Name"));
                            rsInfo.Set_Fields("BName2", rsAccountInfo.Get_Fields_String("Name2"));
                            rsInfo.Set_Fields("BAddress1", rsAccountInfo.Get_Fields_String("BAddress1"));
                            rsInfo.Set_Fields("BAddress2", rsAccountInfo.Get_Fields_String("BAddress2"));
                            rsInfo.Set_Fields("BAddress3", rsAccountInfo.Get_Fields_String("BAddress3"));
                            rsInfo.Set_Fields("BCity", rsAccountInfo.Get_Fields_String("BCity"));
                            rsInfo.Set_Fields("BState", rsAccountInfo.Get_Fields_String("BState"));
                            rsInfo.Set_Fields("BZip", rsAccountInfo.Get_Fields_String("BZip"));
                            // rsInfo.Fields("BZip4") = rsAccountInfo.Fields("BZip4")
                        }
                    }
                    else
                    {
                        rsInfo.Set_Fields("BName", rsAccountInfo.Get_Fields_String("Name"));
                        rsInfo.Set_Fields("BName2", rsAccountInfo.Get_Fields_String("Name2"));
                        rsInfo.Set_Fields("BAddress1", rsAccountInfo.Get_Fields_String("BAddress1"));
                        rsInfo.Set_Fields("BAddress2", rsAccountInfo.Get_Fields_String("BAddress2"));
                        rsInfo.Set_Fields("BAddress3", rsAccountInfo.Get_Fields_String("BAddress3"));
                        rsInfo.Set_Fields("BCity", rsAccountInfo.Get_Fields_String("BCity"));
                        rsInfo.Set_Fields("BState", rsAccountInfo.Get_Fields_String("BState"));
                        rsInfo.Set_Fields("BZip", rsAccountInfo.Get_Fields_String("BZip"));
                        // rsInfo.Fields("BZip4") = rsAccountInfo.Fields("BZip4")
                    }

                    // owner name the same as the bill to name
                    rsInfo.Set_Fields("OName", rsInfo.Get_Fields_String("BName"));
                    rsInfo.Set_Fields("OName2", rsInfo.Get_Fields_String("BName2"));
                    rsInfo.Set_Fields("OAddress1", rsAccountInfo.Get_Fields_String("OAddress1"));
                    rsInfo.Set_Fields("OAddress2", rsAccountInfo.Get_Fields_String("OAddress2"));
                    rsInfo.Set_Fields("OAddress3", rsAccountInfo.Get_Fields_String("OAddress3"));
                    rsInfo.Set_Fields("OCity", rsAccountInfo.Get_Fields_String("OCity"));
                    rsInfo.Set_Fields("OState", rsAccountInfo.Get_Fields_String("OState"));
                    rsInfo.Set_Fields("OZip", rsAccountInfo.Get_Fields_String("OZip"));
                    rsInfo.Set_Fields("OZip4", rsAccountInfo.Get_Fields_String("OZip4"));
                    rsInfo.Set_Fields("MapLot", rsAccountInfo.Get_Fields_String("MapLot"));
                    rsInfo.Set_Fields("BookPage", rsAccountInfo.Get_Fields_String("BookPage"));
                    rsInfo.Set_Fields("Telephone", rsAccountInfo.Get_Fields_String("Telephone"));
                    rsInfo.Set_Fields("Email", rsAccountInfo.Get_Fields_String("Email"));
                    rsInfo.Set_Fields("CombinationCode", "N");
                    if (!boolLien)
                    {
                        rsInfo.Set_Fields("BillDate", rsRK.Get_Fields_DateTime("BillDate"));
                    }

                    rsInfo.Update();
                    
                    if (vsAccounts.TextMatrix(counter, lngTypeCol) != "S")
                    {
                        if (FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColWIntOwed)) != 0 &&
                            FCConvert.ToInt32(rsInfo.Get_Fields_Int32("WLienRecordNumber")) == 0)
                        {
                            if (rsInfo.Get_Fields_Double("WIntAdded") != 0 && !boolLien)
                            {
                                // Add a CHGINT Line for this bill
                                CreateCHGINTForBill_242(true, rsInfo.Get_Fields_Int32("ID"),
                                    rsInfo.Get_Fields_Int32("AccountKey"), false,
                                    vsAccounts.TextMatrix(counter, lngColWIntOwed).ToDecimalValue());
                            }
                        }
					}
                    if (vsAccounts.TextMatrix(counter, lngTypeCol) != "W")
                    {
                        if (FCConvert.ToDecimal(vsAccounts.TextMatrix(counter, lngColSIntOwed)) != 0 &&
                            FCConvert.ToInt32(rsInfo.Get_Fields_Int32("SLienRecordNumber")) == 0)
                        {

                            if (rsInfo.Get_Fields_Double("SIntAdded") != 0 && !boolLien)
                            {
                                // Add a CHGINT Line for this bill
                                CreateCHGINTForBill_242(false, rsInfo.Get_Fields_Int32("ID"),
                                    rsInfo.Get_Fields_Int32("AccountKey"), false,
                                    vsAccounts.TextMatrix(counter, lngColSIntOwed).ToDecimalValue());
                            }
                        }
                    }

                    if (boolLien)
                    {
                        if (modUTStatusPayments.Statics.TownService != "S" &&
                            (FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColWPrinOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColWTaxOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColWIntOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColWCostOwed)) != 0))
                        {
                            rsInfo.Set_Fields("WCombinationLienKey", rsInfo.Get_Fields_Int32("ID"));
                        }
                        if (modUTStatusPayments.Statics.TownService != "W" &&
                            (FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColSPrinOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColSTaxOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColSIntOwed)) != 0 ||
                             FCConvert.ToDouble(vsAccounts.TextMatrix(counter, lngColSCostOwed)) != 0))
                        {
                            rsInfo.Set_Fields("SCombinationLienKey", rsInfo.Get_Fields_Int32("ID"));
                        }

                        rsInfo.Update();
                    }
				}

                return LoadBackInformation;
            }
            catch (Exception ex)
            {

                LoadBackInformation = false;
                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Load Back Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
                rsAccountInfo?.Dispose();
                rsMeterInfo?.Dispose();
                rsInfo?.Dispose();
                rsDeleteInfo?.Dispose();
                rsRK?.Dispose();
                rsNewRK?.Dispose();
                rsUtilityBilling?.Dispose();
			}
			return LoadBackInformation;
		}

		private bool SaveInformation()
		{
			bool SaveInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int[] BookArray = null;
				// holds the books to be billed
				clsDRWrapper rsB = new clsDRWrapper();
				int intCT = 0;
				SaveInformation = true;
				if (ValidateInformation())
				{
					switch (lngType)
					{
						case 0:
							{
								if (LoadBackInformation())
								{
									MessageBox.Show("The load back was successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								else
								{
									SaveInformation = false;
									MessageBox.Show("The load back was not successful.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
								}
								break;
							}
						case 1:
							{
								rsB.OpenRecordset("SELECT * FROM Book ORDER BY BookNumber", modExtraModules.strUTDatabase);
								while (!rsB.EndOfFile())
								{
									intCT += 1;
									Array.Resize(ref BookArray, intCT + 1);
									BookArray[intCT] = FCConvert.ToInt32(rsB.Get_Fields_Int32("BookNumber"));
									rsB.MoveNext();
								}
								for (intCT = 1; intCT <= vsAccounts.Rows - 1; intCT++)
								{
									// Load the amounts of calculated principal and tax
									CalculateNewAccountTotal_8(modUTStatusPayments.GetAccountKeyUT_2(FCConvert.ToInt32(Conversion.Val(vsAccounts.TextMatrix(intCT, lngColAccount)))), intCT);
								}
								frmCalculation.InstancePtr.Init(false, FCConvert.ToInt16(vsAccounts.Rows - 1), ref BookArray, true, FCConvert.ToInt32(Conversion.Val(Strings.Left(cboRateKeys.Items[cboRateKeys.SelectedIndex].ToString(), 5))));
								break;
							}
					}
					//end switch
				}
				else
				{
					SaveInformation = false;
				}
				return SaveInformation;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInformation;
		}

		private bool ValidateInformation()
		{
			bool ValidateInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				int lngRow;
				string strText;
				strText = "an interest paid through";
				ValidateInformation = true;
				vsAccounts.Select(0, 0);
				// this will make the edittext be put into the grid
				// check to make sure that there are accounts loaded
				if (vsAccounts.Rows <= 1)
				{
					if (MessageBox.Show("There are no accounts added.  Would you like to continue?", "Continue", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						modGlobalFunctions.AddCYAEntry_26("UT", "Final Bill - No Accounts Added", "Bypassing entry screen.");
					}
					else
					{
						ValidateInformation = false;
						// MsgBox "Please add an account to be loaded.", vbInformation, "No Accounts"
						return ValidateInformation;
					}
				}
				for (lngRow = 1; lngRow <= vsAccounts.Rows - 1; lngRow++)
				{
					if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColAccount)) == 0)
					{
						ValidateInformation = false;
						MessageBox.Show("You must enter an account number before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
						if (vsAccounts.Visible && vsAccounts.Enabled)
						{
							vsAccounts.Focus();
							vsAccounts.Select(lngRow, lngColAccount);
						}
						return ValidateInformation;
					}
					if (lngType == 0)
					{
						if (Strings.Trim(vsAccounts.TextMatrix(lngRow, lngColName)) == "")
						{
							ValidateInformation = false;
							MessageBox.Show("You must enter a name before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							if (vsAccounts.Visible && vsAccounts.Enabled)
							{
								vsAccounts.Focus();
								vsAccounts.Select(lngRow, lngColName);
							}
							return ValidateInformation;
						}
						if (!Information.IsDate(vsAccounts.TextMatrix(lngRow, lngColWInterestPTD)))
						{
							ValidateInformation = false;
							MessageBox.Show("You must enter " + strText + " date before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							if (vsAccounts.Visible && vsAccounts.Enabled)
							{
								vsAccounts.Focus();
								vsAccounts.Select(lngRow, lngColWInterestPTD);
							}
							return ValidateInformation;
						}
						if (!Information.IsDate(vsAccounts.TextMatrix(lngRow, lngColSInterestPTD)))
						{
							ValidateInformation = false;
							MessageBox.Show("You must enter " + strText + " date before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							if (vsAccounts.Visible && vsAccounts.Enabled)
							{
								vsAccounts.Focus();
								vsAccounts.Select(lngRow, lngColSInterestPTD);
							}
							return ValidateInformation;
						}
						if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColWPrinOwed)) == 0)
						{
							if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColWTaxOwed)) == 0)
							{
								if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColWOrigIntOwed)) == 0)
								{
									if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColWCostOwed)) == 0)
									{
										if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColWIntOwed)) == 0)
										{
											if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColSPrinOwed)) == 0)
											{
												if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColSTaxOwed)) == 0)
												{
													if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColSOrigIntOwed)) == 0)
													{
														if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColSCostOwed)) == 0)
														{
															if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColSIntOwed)) == 0)
															{
																ValidateInformation = false;
																MessageBox.Show("All accounts shown must have an amount due to create a bill.", "Missing Amount", MessageBoxButtons.OK, MessageBoxIcon.Hand);
																if (vsAccounts.Visible && vsAccounts.Enabled)
																{
																	vsAccounts.Focus();
																	if (modUTStatusPayments.Statics.TownService != "S")
																	{
																		vsAccounts.Select(lngRow, lngColWIntOwed);
																	}
																	else
																	{
																		vsAccounts.Select(lngRow, lngColSIntOwed);
																	}
																}
																return ValidateInformation;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
					else
					{
						// the only things that need to be filled in is the consumption and the account number and the dates
						if (vsAccounts.TextMatrix(lngRow, lngColCurrent) == "" || Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColCurrent)) == -1)
						{
							// 04152015 xxxxx    Val(vsAccounts.TextMatrix(lngRow, lngColCurrent)) = 0
							ValidateInformation = false;
							MessageBox.Show("You must enter a consumption before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							if (vsAccounts.Visible && vsAccounts.Enabled)
							{
								vsAccounts.Focus();
								vsAccounts.Select(lngRow, lngTypeCol);
							}
							return ValidateInformation;
						}
						if (!Information.IsDate(vsAccounts.TextMatrix(lngRow, lngColBillDate)))
						{
							ValidateInformation = false;
							MessageBox.Show("You must enter a bill date before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							if (vsAccounts.Visible && vsAccounts.Enabled)
							{
								vsAccounts.Focus();
								vsAccounts.Select(lngRow, lngColStartDate);
							}
							return ValidateInformation;
						}
						if (!Information.IsDate(vsAccounts.TextMatrix(lngRow, lngColStartDate)))
						{
							ValidateInformation = false;
							MessageBox.Show("You must enter a start date before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							if (vsAccounts.Visible && vsAccounts.Enabled)
							{
								vsAccounts.Focus();
								vsAccounts.Select(lngRow, lngColStartDate);
							}
							return ValidateInformation;
						}
						if (!Information.IsDate(vsAccounts.TextMatrix(lngRow, lngColEndDate)))
						{
							ValidateInformation = false;
							MessageBox.Show("You must enter an end date before you may proceed.", "Missing Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							if (vsAccounts.Visible && vsAccounts.Enabled)
							{
								vsAccounts.Focus();
								vsAccounts.Select(lngRow, lngColEndDate);
							}
							return ValidateInformation;
						}
					}
				}
				return ValidateInformation;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return ValidateInformation;
		}

		private void SetSearchGridHeight()
		{
			//if ((vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70 > fraAccountSearch.Height * 0.7)
			//{
			// too many rows to be shown
			//vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			//vsAccountSearch.Height = FCConvert.ToInt32(fraAccountSearch.Height * 0.7);
			//}
			//else
			//{
			//vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			//vsAccountSearch.Height = (vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70;
			//}
		}

		private void FormatSearchGrid()
		{
			vsAccountSearch.Cols = 4;
			vsAccountSearch.ColWidth(0, 0);
			vsAccountSearch.ColWidth(1, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.12));
			// account number
			vsAccountSearch.ColWidth(2, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.5));
			// name
			vsAccountSearch.ColWidth(3, FCConvert.ToInt32(vsAccountSearch.WidthOriginal * 0.36));
			// maplot
			vsAccountSearch.ExtendLastCol = true;
			vsAccountSearch.TextMatrix(0, 1, "Account");
			vsAccountSearch.TextMatrix(0, 2, "Name");
			vsAccountSearch.TextMatrix(0, 3, "Location");
			vsAccountSearch.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSort;
			//FC:FINAL:CHN - issue #1133: incorrect ordering.
			vsAccountSearch.ColDataType(1, FCGrid.DataTypeSettings.flexDTLong);
		}

		private void SetGridHeight()
		{
			//FC:FINAL:CHN - issue #1327: Set grid height based on rows number. 
			var height = (vsAccounts.Rows + 1) * 40 - 25;
			//if (((vsAccounts.Rows + 1) * vsAccounts.RowHeight(0)) > (fraAccounts.Height * 0.9))
			if (height > (fraAccounts.Height * 0.9))
			{
				// too many rows to be shown
				vsAccounts.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollBoth;
				//vsAccounts.Height = FCConvert.ToInt32(fraAccounts.Height * 0.9);
			}
			else
			{
				vsAccounts.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
				//vsAccounts.Height = ((vsAccounts.Rows + 1) * vsAccounts.RowHeight(0) + 20);
				vsAccounts.Height = height;
			}
		}

		private void FormatGrid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intCT;
				vsAccounts.Cols = 18;
				for (intCT = 0; intCT <= vsAccounts.Cols - 1; intCT++)
				{
					vsAccounts.ColWidth(intCT, 0);
				}
				switch (lngType)
				{
					case 0:
						{
							// Loadback
							vsAccounts.ColWidth(lngColAccount, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.06));
							vsAccounts.ColWidth(lngColName, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
							vsAccounts.ColWidth(lngColName2, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
							vsAccounts.ColWidth(lngTypeCol, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.06));
							vsAccounts.ColWidth(lngHiddenCol, 0);
							vsAccounts.ColWidth(lngColOverwrite, 0);
							if (modUTStatusPayments.Statics.TownService != "S")
							{
								vsAccounts.ColWidth(lngColWInterestPTD, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColWIntOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColWPrinOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColWTaxOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColWOrigIntOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColWCostOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
							}
							else
							{
								vsAccounts.ColWidth(lngColWInterestPTD, 0);
								vsAccounts.ColWidth(lngColWIntOwed, 0);
								vsAccounts.ColWidth(lngColWPrinOwed, 0);
								vsAccounts.ColWidth(lngColWTaxOwed, 0);
								vsAccounts.ColWidth(lngColWOrigIntOwed, 0);
								vsAccounts.ColWidth(lngColWCostOwed, 0);
							}
							if (modUTStatusPayments.Statics.TownService != "W")
							{
								vsAccounts.ColWidth(lngColSInterestPTD, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColSIntOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColSPrinOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColSTaxOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColSOrigIntOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
								vsAccounts.ColWidth(lngColSCostOwed, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.17));
							}
							else
							{
								vsAccounts.ColWidth(lngColSInterestPTD, 0);
								vsAccounts.ColWidth(lngColSIntOwed, 0);
								vsAccounts.ColWidth(lngColSPrinOwed, 0);
								vsAccounts.ColWidth(lngColSTaxOwed, 0);
								vsAccounts.ColWidth(lngColSOrigIntOwed, 0);
								vsAccounts.ColWidth(lngColSCostOwed, 0);
							}
							//FC:FINAL:AM:#1006 - don't extend the last column
							//vsAccounts.ExtendLastCol = true;
							vsAccounts.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
							// set the alignment
							vsAccounts.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							vsAccounts.ColAlignment(lngTypeCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							vsAccounts.ColAlignment(lngColWInterestPTD, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							vsAccounts.ColAlignment(lngColWIntOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColWPrinOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColWTaxOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColWOrigIntOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColWCostOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColSInterestPTD, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							vsAccounts.ColAlignment(lngColSIntOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColSPrinOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColSTaxOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColSOrigIntOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColSCostOwed, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColFormat(lngColWIntOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColWPrinOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColWTaxOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColWOrigIntOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColWCostOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColWInterestPTD, "MM/dd/yyyy");
							vsAccounts.ColFormat(lngColSIntOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColSPrinOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColSTaxOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColSOrigIntOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColSCostOwed, "#,##0.00");
							vsAccounts.ColFormat(lngColSInterestPTD, "MM/dd/yyyy");
							vsAccounts.TextMatrix(0, lngColAccount, "Acct");
							vsAccounts.TextMatrix(0, lngColName, "Name");
							vsAccounts.TextMatrix(0, lngColName2, "Name 2");
							vsAccounts.TextMatrix(0, lngTypeCol, "Type");
							vsAccounts.TextMatrix(0, lngColWInterestPTD, "Water Int Pd Date");
							vsAccounts.TextMatrix(0, lngColSInterestPTD, "Sewer Int Pd Date");
							vsAccounts.TextMatrix(0, lngColWPrinOwed, "Water Prin Owed");
							vsAccounts.TextMatrix(0, lngColWTaxOwed, "Water Tax Owed");
							vsAccounts.TextMatrix(0, lngColWOrigIntOwed, "Water Int Owed");
							vsAccounts.TextMatrix(0, lngColWCostOwed, "Water Costs Owed");
							vsAccounts.TextMatrix(0, lngColWIntOwed, "Water Int After Bill");
							vsAccounts.TextMatrix(0, lngColSPrinOwed, "Sewer Prin Owed");
							vsAccounts.TextMatrix(0, lngColSTaxOwed, "Sewer Tax Owed");
							vsAccounts.TextMatrix(0, lngColSOrigIntOwed, "Sewer Int Owed");
							vsAccounts.TextMatrix(0, lngColSCostOwed, "Sewer Costs Owed");
							vsAccounts.TextMatrix(0, lngColSIntOwed, "Sewer Int After Bill");
							// kk trouts-6 03012013  Change Water to Stormwater for Bangor
							if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
							{
								vsAccounts.TextMatrix(0, lngColWInterestPTD, "Stormwater Int Pd Date");
								vsAccounts.TextMatrix(0, lngColWPrinOwed, "Stormwater Prin Owed");
								vsAccounts.TextMatrix(0, lngColWTaxOwed, "Stormwater Tax Owed");
								vsAccounts.TextMatrix(0, lngColWOrigIntOwed, "Stormwater Int Owed");
								vsAccounts.TextMatrix(0, lngColWCostOwed, "Stormwater Costs Owed");
								vsAccounts.TextMatrix(0, lngColWIntOwed, "Stormwater Int After Bill");
							}
							// *******************************************************************************************************
							// *******************************************************************************************************
							break;
						}
					case 1:
						{
							// MAL@20081104: Temporary message to see if we can track down a problem
							// Tracker Reference: 15874
							if (modGlobalConstants.Statics.MuniName == "Fairfield")
							{
								MessageBox.Show("Formatting Grid", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
							}
							// Final Billed
							vsAccounts.ColWidth(lngColAccount, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.09));
							vsAccounts.ColWidth(lngColName, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.19));
							// FC:FINAL:VGE - #i877 'Scrollable' Columns sizes are slightly adjusted.
							vsAccounts.ColWidth(lngColLocation, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.175));
							vsAccounts.ColWidth(lngColCurrent, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.105));
							vsAccounts.ColWidth(lngColPrevious, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
							vsAccounts.ColWidth(lngColBillDate, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.105));
							vsAccounts.ColWidth(lngColStartDate, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.105));
							vsAccounts.ColWidth(lngColEndDate, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.105));
							vsAccounts.ColWidth(lngColBook, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
							vsAccounts.ColWidth(lngColSequence, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
							vsAccounts.ColWidth(lngColSAdj, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
							vsAccounts.ColWidth(lngColWAdj, FCConvert.ToInt32(vsAccounts.WidthOriginal * 0.1));
							vsAccounts.FrozenCols = 2;
							//FC:FINAL:RPU:#i877 - don't extend the last col because the width becomes too small
							//vsAccounts.ExtendLastCol = true;
							vsAccounts.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollHorizontal;
							// set the alignment
							vsAccounts.ColAlignment(lngColAccount, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							vsAccounts.ColAlignment(lngColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							vsAccounts.ColAlignment(lngColCurrent, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColPrevious, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColBillDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							// MAL@20070830: Changed to be Centered ; flexAlignRightCenter
							vsAccounts.ColAlignment(lngColStartDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							// flexAlignRightCenter
							vsAccounts.ColAlignment(lngColEndDate, FCGrid.AlignmentSettings.flexAlignLeftCenter);
							// flexAlignRightCenter
							vsAccounts.ColAlignment(lngColBook, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColSAdj, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColAlignment(lngColWAdj, FCGrid.AlignmentSettings.flexAlignRightCenter);
							vsAccounts.ColFormat(lngColAccount, "");
							vsAccounts.ColFormat(lngColName, "");
							vsAccounts.ColFormat(lngColLocation, "");
							vsAccounts.ColFormat(lngColCurrent, "");
							vsAccounts.ColFormat(lngColPrevious, "");
							vsAccounts.ColFormat(lngColBillDate, "");
							vsAccounts.ColFormat(lngColStartDate, "");
							vsAccounts.ColFormat(lngColEndDate, "");
							vsAccounts.ColFormat(lngColBook, "");
							vsAccounts.ColFormat(lngColSAdj, "");
							vsAccounts.ColFormat(lngColWAdj, "");
							vsAccounts.ColDataType(lngColBillDate, FCGrid.DataTypeSettings.flexDTDate);
							vsAccounts.ColDataType(lngColStartDate, FCGrid.DataTypeSettings.flexDTDate);
							vsAccounts.ColDataType(lngColEndDate, FCGrid.DataTypeSettings.flexDTDate);
							vsAccounts.ColFormat(lngColBillDate, "MM/dd/yyyy");
							vsAccounts.ColFormat(lngColEndDate, "MM/dd/yyyy");
							vsAccounts.ColFormat(lngColStartDate, "MM/dd/yyyy");
							vsAccounts.ColFormat(lngColSAdj, "#,##0.00");
							vsAccounts.ColFormat(lngColSAdj, "#,##0.00");
							vsAccounts.TextMatrix(0, lngColAccount, "Acct");
							vsAccounts.TextMatrix(0, lngColName, "Name");
							vsAccounts.TextMatrix(0, lngColLocation, "Location");
							vsAccounts.TextMatrix(0, lngColCurrent, "Current");
							vsAccounts.TextMatrix(0, lngColPrevious, "Previous");
							vsAccounts.TextMatrix(0, lngColBillDate, "Bill Date");
							vsAccounts.TextMatrix(0, lngColStartDate, "Start Date");
							vsAccounts.TextMatrix(0, lngColEndDate, "End Date");
							vsAccounts.TextMatrix(0, lngColBook, "Book");
							vsAccounts.TextMatrix(0, lngColSequence, "Seq");
							vsAccounts.TextMatrix(0, lngColWAdj, "W Adj");
							vsAccounts.TextMatrix(0, lngColSAdj, "S Adj");
							break;
						}
				}
				//end switch
				vsAccounts.AllowUserResizing = FCGrid.AllowUserResizeSettings.flexResizeColumns;
				// .ExplorerBar = flexExSortShow
				// set the default information
				txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				fraAccounts.Enabled = true;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Formatting Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngAcct As int	OnWrite(double, int)
		private bool LoadAccountInformation_2(int lngAcct, ref int lngRow)
		{
			return LoadAccountInformation(ref lngAcct, ref lngRow);
		}

		private bool LoadAccountInformation_6(int lngAcct, int lngRow)
		{
			return LoadAccountInformation(ref lngAcct, ref lngRow);
		}

		private bool LoadAccountInformation(ref int lngAcct, ref int lngRow)
		{
			bool LoadAccountInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept an account number and will fill the grid with the name from that account
				clsDRWrapper rsBillCheck = new clsDRWrapper();
				clsDRWrapper rsPayments = new clsDRWrapper();
				clsDRWrapper rsMeter = new clsDRWrapper();
				int lngBillingYearNumber;
				bool blnNewRecord;
				bool boolFirstRow = false;
				DateTime dtEndDate = default(DateTime);
				DateTime dtStartDate = default(DateTime);
				int lngCheck;
				string strStatus = "";
				clsDRWrapper rsLienCheck = new clsDRWrapper();
				bool blnNotAllowed = false;
				clsDRWrapper rsUtilityBilling = new clsDRWrapper();
				clsDRWrapper rsRateKey = new clsDRWrapper();
				if (lngRow == 0)
				{
					// this will add a row so that the search does not write over any information
					vsAccounts.AddItem("");
					blnNewRecord = true;
					lngRow = vsAccounts.Rows - 1;
				}
				else
				{
					blnNewRecord = false;
				}
				// check to make sure that this account has not already been added to the grid
				for (lngCheck = 1; lngCheck <= vsAccounts.Rows - 1; lngCheck++)
				{
					if (Conversion.Val(vsAccounts.TextMatrix(lngCheck, lngColAccount)) == lngAcct && lngCheck != lngRow)
					{
						MessageBox.Show("Account #" + FCConvert.ToString(lngAcct) + " has already been added to this list.", "Duplicate Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						LoadAccountInformation = false;
						return LoadAccountInformation;
					}
				}
				lngBillingYearNumber = 1;
				// check the billingmaster to find out if any other bills have been created for this year
				if (boolLien)
				{
					rsLienCheck.OpenRecordset("SELECT * FROM Lien WHERE RateKey = " + FCConvert.ToString(lngRateKey), "TWUT0000.vb1");
					if (rsLienCheck.EndOfFile() != true && rsLienCheck.BeginningOfFile() != true)
					{
						if (rsLienCheck.Get_Fields_Boolean("Water") == true)
						{
							rsBillCheck.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND WLienRecordNumber IN (SELECT ID FROM Lien WHERE RateKey = " + FCConvert.ToString(lngRateKey) + ")", modExtraModules.strUTDatabase);
						}
						else
						{
							rsBillCheck.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND SLienRecordNumber IN (SELECT ID FROM Lien WHERE RateKey = " + FCConvert.ToString(lngRateKey) + ")", modExtraModules.strUTDatabase);
						}
					}
				}
				else
				{
					rsBillCheck.OpenRecordset("SELECT * FROM Bill WHERE ActualAccountNumber = " + FCConvert.ToString(lngAcct) + " AND BillingRateKey = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
				}
				if (rsBillCheck.EndOfFile())
				{
					// none created...keep going
					vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
				}
				else
				{
					if (lngType == 0)
					{
						// ask the user if they still want to create a bill for this account even though it has been created before
						blnNotAllowed = false;
						do
						{
							if (rsBillCheck.Get_Fields_Boolean("LoadBack") == false)
							{
								blnNotAllowed = true;
								break;
							}
							rsBillCheck.MoveNext();
						}
						while (rsBillCheck.EndOfFile() != true);
						if (blnNotAllowed)
						{
							MessageBox.Show("You may not overwrite an actual bill only loadbacks may be overwritten.", "Cannot Overwrite", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
							vsAccounts.TextMatrix(lngRow, lngColAccount, "");
							vsAccounts.EditText = "";
							vsAccounts.Select(lngRow, lngColAccount);
							return LoadAccountInformation;
						}
						switch (MessageBox.Show("There is a bill already created for account " + FCConvert.ToString(lngAcct) + ".  Do you want to continue the load back process for this account and year?", "Existing Bill", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
						{
							case DialogResult.Yes:
								{
									rsBillCheck.MoveFirst();
									// check to see if there are already payments.  If so, then do not allow the overwrite
									rsPayments.OpenRecordset("SELECT * FROM PaymentRec WHERE Reference <> 'CHGINT' AND (BillKey = " + rsBillCheck.Get_Fields_Int32("ID") + " OR ((BillKey = " + rsBillCheck.Get_Fields_Int32("WLienRecordNumber") + " OR BillKey = " + rsBillCheck.Get_Fields_Int32("SLienRecordNumber") + ") AND ISNULL(Lien,0) = 1))", modExtraModules.strUTDatabase);
									if (rsPayments.EndOfFile())
									{
										vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(1));
									}
									else
									{
										// the user has payments on this bill
										MessageBox.Show("This bill already has payments or adjustments made against it.  Please write off this bill manually for audit purposes.", "Cannot Overwrite", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
										vsAccounts.TextMatrix(lngRow, lngColAccount, "");
										vsAccounts.EditText = "";
										vsAccounts.Select(lngRow, lngColAccount);
										return LoadAccountInformation;
									}
									break;
								}
							case DialogResult.No:
							case DialogResult.Cancel:
								{
									MessageBox.Show("This meter already has a bill with this rate key made against it.  Please select a different rate key to use.", "Rate Key", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									vsAccounts.TextMatrix(lngRow, lngColOverwrite, FCConvert.ToString(0));
									vsAccounts.TextMatrix(lngRow, lngColAccount, "");
									vsAccounts.EditText = "";
									vsAccounts.Select(lngRow, lngColAccount);
									return LoadAccountInformation;
								}
						}
						//end switch
					}
					else
					{
						// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
						if (!IsBillBookCleared(rsBillCheck.Get_Fields("Book"), ref strStatus) && lngType == 1)
						{
							// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
							MessageBox.Show("Book " + rsBillCheck.Get_Fields("Book") + "'s status is currently " + strStatus + " and final bills cannot be processed for any status except Cleared.", "Cannot Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							return LoadAccountInformation;
						}
						else
						{
                            if (rsBillCheck.Get_Fields_Boolean("Final") &&
                                FCConvert.ToString(rsBillCheck.Get_Fields_String("BillStatus")) != "B")
                            {
                                rsPayments.OpenRecordset(
                                    "select * from paymentrec where code <> 'Y' and Reference <> 'CHGINT' and (BillKey = " +
                                    rsBillCheck.Get_Fields_Int32("ID") + " OR ((BillKey = " +
                                    rsBillCheck.Get_Fields_Int32("WLienRecordNumber") + " OR BillKey = " +
                                    rsBillCheck.Get_Fields_Int32("SLienRecordNumber") + ") AND ISNULL(Lien,0) = 1))",
                                    "UtilityBilling");
                                if (rsPayments.EndOfFile())
                                {                                
                                    if (MessageBox.Show(
                                            "This account already has a Final bill that is associated with this rate key but has not been billed.  Would you like to remove the previous bill?",
                                            "Overwrite Final Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                                        DialogResult.Yes)
                                    {
                                        modUTBilling.ResetFinalBilling_2(
                                            "AND AccountKey = " + rsBillCheck.Get_Fields_Int32("AccountKey"));

                                    }
                                    else
                                    {
                                        vsAccounts.TextMatrix(lngRow, lngColAccount, "");
                                        vsAccounts.Select(lngRow, lngColAccount);
                                        return LoadAccountInformation;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(
                                        "This account already has a Final bill that is associated with this rate key but has not been billed. It also has payments and cannot be removed.",
                                        "Duplicate", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    vsAccounts.TextMatrix(lngRow, lngColAccount, "");
                                    vsAccounts.Select(lngRow, lngColAccount);
                                    return false;
                                }
                        }
							else
							{
								MessageBox.Show("This account already has a bill that is associated with this rate key.", "Duplicate Rate Key", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								vsAccounts.TextMatrix(lngRow, lngColAccount, "");
								vsAccounts.Select(lngRow, lngColAccount);
								return LoadAccountInformation;
							}
						}
					}
				}
				rsUT.FindFirstRecord("AccountNumber", lngAcct);
				rsUtilityBilling.OpenRecordset("SELECT * FROM UtilityBilling");
				if (rsUT.NoMatch)
				{
					MessageBox.Show("There is no Utility Billing account with that number.  Please try again.", "Incorrect Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
					vsAccounts.Select(vsAccounts.Row, lngColAccount);
					return LoadAccountInformation;
				}
				else
				{
					if (!IsBillBookCleared(rsUT.Get_Fields_Int32("UTBook"), ref strStatus) && lngType == 1)
					{
						MessageBox.Show("Book " + rsUT.Get_Fields_Int32("UTBook") + "'s status is currently " + strStatus + " and final bills cannot be processed for any status except Cleared.", "Cannot Process", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return LoadAccountInformation;
					}
					else
					{
                        if (RateKeyWasUsedForBillInBook(rsUT.Get_Fields_Int32("UTBook"), lngRateKey) && lngType == 1)
                        {
                            MessageBox.Show(
                                "There is already a final bill from this book that is associated with this rate key. /n Please pick another Book or another Rate",
                                "Already Used", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            vsAccounts.TextMatrix(lngRow, lngColAccount, "");
                            vsAccounts.Select(lngRow, lngColAccount);
                            return false;
                        }
						vsAccounts.ColSel = lngColAccount;

						if (lngType == 0)
						{
							// matching account number is the current record
							vsAccounts.TextMatrix(lngRow, lngColAccount, FCConvert.ToString(lngAcct));
                            string tempBillType = vsAccounts.TextMatrix(lngRow, lngTypeCol);
                            var useOwner = rsUT.Get_Fields_Boolean(tempBillType == "W" ? "WBillToOwner" : "SBillToOwner");
                            if (!useOwner && rsUT.Get_Fields("BillingPartyID") > 0 &&
                                rsUT.Get_Fields_Int32("BillingPartyID") != rsUT.Get_Fields_Int32("OwnerPartyID"))
                            {
                                vsAccounts.TextMatrix(lngRow, lngColName, FCConvert.ToString(rsUT.Get_Fields("Name")));
                                vsAccounts.TextMatrix(lngRow, lngColName2, FCConvert.ToString(rsUT.Get_Fields("Name2")));
                            }
                            else
                            {
                                vsAccounts.TextMatrix(lngRow, lngColName, FCConvert.ToString(rsUT.Get_Fields("OwnerName")));
                                vsAccounts.TextMatrix(lngRow, lngColName2, FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName")));
                            }
                            vsAccounts.TextMatrix(lngRow, lngHiddenCol, "");
							vsAccounts.TextMatrix(lngRow, lngTypeCol, Strings.Left(modUTStatusPayments.Statics.TownService, 1));
							vsAccounts.TextMatrix(lngRow, lngColWInterestPTD, txtDefaultDate.Text);
							vsAccounts.TextMatrix(lngRow, lngColWIntOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColWPrinOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColWTaxOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColWOrigIntOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColWCostOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColSInterestPTD, txtDefaultDate.Text);
							vsAccounts.TextMatrix(lngRow, lngColSIntOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColSPrinOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColSTaxOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColSOrigIntOwed, "0.00");
							vsAccounts.TextMatrix(lngRow, lngColSCostOwed, "0.00");
							SetGridHeight();
							//FC:FINAL:MSH - issue #1004: changing current cell on validating execute validating for current cell again and first validating will finish work with incorrect data
							// (in original new validating won't be executed from validating and data will be correct)
							//if (vsAccounts.Visible && vsAccounts.Enabled)
							//{
							//    vsAccounts.Focus();
							//    vsAccounts.Select(lngRow, lngColName);
							//}
						}
						else
						{
							rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAcct)), modExtraModules.strUTDatabase);
							boolFirstRow = true;
							while (!rsMeter.EndOfFile())
							{
								// kk04152015 trouts-106  Change for Water only - If UCase(MuniName) <> "BANGOR" Or rsMeter.Fields("Service") <> "W" Then      'kk07242014 trouts-104  Don't process the meter if Bangor AND (Storm)Water
								if (Strings.UCase(modGlobalConstants.Statics.MuniName) != "BANGOR" || FCConvert.ToInt32(rsMeter.Get_Fields_Int32("MeterNumber")) == 1 || FCConvert.ToString(rsMeter.Get_Fields_String("Service")) != "W")
								{
									if (FCConvert.ToString(rsMeter.Get_Fields_String("BillingStatus")) == "D" || FCConvert.ToString(rsMeter.Get_Fields_String("BillingStatus")) == "C")
									{
										if (!boolFirstRow)
										{
											vsAccounts.AddItem("");
											lngRow = vsAccounts.Rows - 1;
										}
										boolFirstRow = false;
										vsAccounts.TextMatrix(lngRow, lngColAccount, FCConvert.ToString(lngAcct));
										string tempBillType = vsAccounts.TextMatrix(lngRow, lngTypeCol);
                                        var useOwner = rsUT.Get_Fields_Boolean(tempBillType == "W" ? "WBillToOwner" : "SBillToOwner");
                                        if (!useOwner && rsUT.Get_Fields("BillingPartyID") > 0 &&
                                            rsUT.Get_Fields_Int32("BillingPartyID") != rsUT.Get_Fields_Int32("OwnerPartyID"))
                                        {
                                            vsAccounts.TextMatrix(lngRow, lngColName, FCConvert.ToString(rsUT.Get_Fields("Name")));
                                            vsAccounts.TextMatrix(lngRow, lngColName2, FCConvert.ToString(rsUT.Get_Fields("Name2")));
                                        }
                                        else
                                        {
                                            vsAccounts.TextMatrix(lngRow, lngColName, FCConvert.ToString(rsUT.Get_Fields("OwnerName")));
                                            vsAccounts.TextMatrix(lngRow, lngColName2, FCConvert.ToString(rsUT.Get_Fields("SecondOwnerName")));
                                        }          
										
										// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
										vsAccounts.TextMatrix(lngRow, lngColLocation, rsUT.Get_Fields("StreetNumber") + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsUT.Get_Fields_String("StreetName"))));
										// kk04212015 trouts-137
										vsAccounts.TextMatrix(lngRow, lngColPrevious, FCConvert.ToString(rsMeter.Get_Fields_Int32("PreviousReading")));
										// kk04152015 trouts-106       .TextMatrix(lngRow, lngColCurrent) = rsMeter.Fields("CurrentReading")
										if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading")) == -1)
										{
											vsAccounts.TextMatrix(lngRow, lngColCurrent, "");
										}
										else
										{
											vsAccounts.TextMatrix(lngRow, lngColCurrent, FCConvert.ToString(rsMeter.Get_Fields_Int32("CurrentReading")));
										}
										// .TextMatrix(lngRow, lngColSAdj) = rsMeter.Fields("SDEAdjustAmount")
										// .TextMatrix(lngRow, lngColWAdj) = rsMeter.Fields("WDEAdjustAmount")
										// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
										FindPeriodDates_2(rsMeter.Get_Fields("Frequency"), ref dtStartDate, ref dtEndDate);
										vsAccounts.TextMatrix(lngRow, lngColStartDate, FCConvert.ToString(dtStartDate));
										vsAccounts.TextMatrix(lngRow, lngColEndDate, FCConvert.ToString(dtEndDate));
										rsRateKey.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(Conversion.Val(Strings.Left(cboRateKeys.Text, 5))));
										if (rsRateKey.EndOfFile() != true && rsRateKey.BeginningOfFile() != true)
										{
											vsAccounts.TextMatrix(lngRow, lngColBillDate, Strings.Format(rsRateKey.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy"));
										}
										else
										{
											vsAccounts.TextMatrix(lngRow, lngColBillDate, Strings.Format(DateTime.Today, "MM/dd/yyyy"));
										}
										vsAccounts.TextMatrix(lngRow, lngColBook, FCConvert.ToString(rsMeter.Get_Fields_Int32("BookNumber")));
										// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
										vsAccounts.TextMatrix(lngRow, lngColSequence, FCConvert.ToString(rsMeter.Get_Fields("Sequence")));
									}
									else
									{
										MessageBox.Show("Meter number " + rsMeter.Get_Fields_Int32("MeterNumber") + " does not have a status of Cleared or Data Entered.", "Wrong Status", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										LoadAccountInformation = false;
										return LoadAccountInformation;
									}
								}
								rsMeter.MoveNext();
							}
							SetGridHeight();
						}
					}
				}
				LoadAccountInformation = true;
				return LoadAccountInformation;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Account Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadAccountInformation;
		}

        private bool RateKeyWasUsedForBillInBook(int lngBook, int lngRateKey)
        {
            var rsLoad = new clsDRWrapper();
            rsLoad.OpenRecordset("Select top 1 [id] from Bill where Book = " + lngBook + " and BillingRateKey = " + lngRateKey, "UtilityBilling");
            if (!rsLoad.EndOfFile())
            {
                return true;
            }
                return false;
        }

        private void mnuFileSearch_Click(object sender, System.EventArgs e)
		{
			// this will show the search frame
			mnuFileSearch.Enabled = false;
			mnuFileSave.Enabled = false;
			mnuFileNew.Enabled = false;
			txtSearchMapLot.Visible = false;
			lblSearchMapLot.Visible = false;
			//fraAccountSearch.Top = fraAccounts.Top;
			//fraAccountSearch.Left = FCConvert.ToInt32((this.Width - fraAccountSearch.Width) / 2.0);
			fraAccounts.Visible = false;
			fraAccountSearch.Visible = true;
			if (txtSearchName.Enabled && txtSearchName.Visible)
			{
				txtSearchName.Focus();
			}
			txtSearchName.Text = "";
			vsAccountSearch.Visible = false;
			vsAccountSearch.Rows = 1;
		}

		private void txtDefaultDate_Validate(object sender, System.ComponentModel.CancelEventArgs e)
		{
			// this should check to make sure that the interest date is after
			// or the same as the due date of the rate rec that has been selected
			DateTime dttemp;
			// check this against the rate key information
			if (Strings.Trim(txtDefaultDate.Text) != "")
			{
				if (Information.IsDate(txtDefaultDate.Text))
				{
					dttemp = DateAndTime.DateValue(txtDefaultDate.Text);
					rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), modExtraModules.strUTDatabase);
					if (!(rsRK.EndOfFile()))
					{
						if (DateAndTime.DateDiff("d", dttemp, (DateTime)rsRK.Get_Fields_DateTime("IntStart")) <= 1)
						{
							// this is ok
						}
						else
						{
							txtDefaultDate.Text = Strings.Format(DateAndTime.DateAdd("D", -1, (DateTime)rsRK.Get_Fields_DateTime("IntStart")), "MM/dd/yyyy");
							MessageBox.Show("The date can only be one day before the interest start date for this rate record.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							// MsgBox "Please enter a date that is the same or later than the interest start date for this rate record.", vbExclamation, "Invalid Date"
							e.Cancel = true;
						}
					}
					else
					{
						txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
					}
				}
				else
				{
					txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				}
			}
			else
			{
				txtDefaultDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			}
		}

		private void txtSearchName_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						SearchForAccounts();
						break;
					}
			}
			//end switch
		}

		private void vsAccounts_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (lngType == 0)
			{
				if (vsAccounts.Col == lngColWInterestPTD || vsAccounts.Col == lngColSInterestPTD)
				{
					vsAccounts.EditMask = "0#/0#/####";
				}
				else
				{
					vsAccounts.EditMask = "";
				}
			}
			else
			{
				if (vsAccounts.Col == lngColStartDate || vsAccounts.Col == lngColEndDate || vsAccounts.Col == lngColBillDate)
				{
					vsAccounts.EditMask = "0#/0#/####";
				}
				else
				{
					vsAccounts.EditMask = "";
				}
			}
		}

		private void vsAccounts_DblClick(object sender, System.EventArgs e)
		{
			int lngRW = 0;
			int lngCL = 0;
			lngRW = vsAccounts.MouseRow;
			lngCL = vsAccounts.MouseCol;
			if (lngType == 0)
			{
				if (modUTStatusPayments.Statics.TownService == "B")
				{
					if (lngRW > 1)
					{
						if (lngCL == lngTypeCol)
						{
							if (Strings.Trim(vsAccounts.TextMatrix(lngRW, lngCL)) == "B")
							{
								vsAccounts.TextMatrix(lngRW, lngCL, "S");
							}
							else if (Strings.Trim(vsAccounts.TextMatrix(lngRW, lngCL)) == "S")
							{
								vsAccounts.TextMatrix(lngRW, lngCL, "W");
							}
							else if (Strings.Trim(vsAccounts.TextMatrix(lngRW, lngCL)) == "W")
							{
								vsAccounts.TextMatrix(lngRW, lngCL, "B");
							}
							else
							{
								vsAccounts.TextMatrix(lngRW, lngCL, "B");
							}
						}
					}
				}
			}
		}

		private void vsAccounts_Enter(object sender, System.EventArgs e)
		{
			vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsAccounts_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			switch (KeyCode)
			{
				case Keys.Delete:
					{
						KeyCode = 0;
						switch (MessageBox.Show("Are you sure that you would like to delete this row?", "Delete Row", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
						{
							case DialogResult.Yes:
								{
									RemoveFinalBilled(vsAccounts.Row);
									vsAccounts.RemoveItem(vsAccounts.Row);
									SetGridHeight();
									break;
								}
							case DialogResult.No:
							case DialogResult.Cancel:
								{
									break;
								}
						}
						//end switch
						return;
					}
				case Keys.Escape:
					{
						this.Unload();
						break;
					}
			}
			//end switch
			if (lngType == 1)
			{
				if (vsAccounts.Col == lngColCurrent || vsAccounts.Col == lngColStartDate || vsAccounts.Col == lngColEndDate || vsAccounts.Col == lngColBillDate || vsAccounts.Col == lngColWAdj || vsAccounts.Col == lngColSAdj)
				{
					if ((e.KeyCode >= Keys.D0 && e.KeyCode <= Keys.D9) || (FCConvert.ToInt32(e.KeyCode) == 45) || (FCConvert.ToInt32(e.KeyCode) == 8) || (FCConvert.ToInt32(e.KeyCode) == 37) || (FCConvert.ToInt32(e.KeyCode) == 38) || (FCConvert.ToInt32(e.KeyCode) == 39))
					{
						// allow this to pass
					}
					else
					{
						// stop
						KeyCode = 0;
					}
				}
				else
				{
					switch (FCConvert.ToInt32(e.KeyCode))
					{
						case 9:
						case 17:
						case 78:
						case 37:
						case 38:
						case 39:
							{
								// allow this to pass
								break;
							}
						default:
							{
								// stop
								KeyCode = 0;
								break;
							}
					}
					//end switch
				}
			}
		}

		private void vsAccounts_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
			{
				this.Unload();
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsAccounts_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 27)
			{
				this.Unload();
			}
			//FC:FINAL:MSH - issue #1004: go to next cell if 'Enter' pressed in first column
			if (KeyAscii == 13)
			{
				if (vsAccounts.Visible && vsAccounts.Enabled && vsAccounts.Col == lngColAccount)
				{
					vsAccounts.Focus();
					vsAccounts.Col += 1;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsAccounts_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
			int lngMR;
			int lngMC;
			lngMR = e.RowIndex;
			lngMC = e.ColumnIndex;
			//FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
			if (lngMR > -1 && lngMC > -1)
			{
				DataGridViewCell cell = vsAccounts[lngMC, lngMR];
				if (lngMR > 0)
				{
					if (lngType == 0)
					{
						if (lngMC == lngColName || lngMC == lngColWInterestPTD || lngMC == lngColSInterestPTD)
						{
							cell.ToolTipText = vsAccounts.TextMatrix(lngMR, lngMC);
						}
						else
						{
							cell.ToolTipText = "";
						}
					}
					else
					{
						if (lngMC == lngColEndDate)
						{
							cell.ToolTipText = "Please enter the billing period end date.";
						}
						else if (lngMC == lngColStartDate)
						{
							cell.ToolTipText = "Please enter the billing period start date.";
						}
						else if (lngMC == lngColBillDate)
						{
							cell.ToolTipText = "Please enter the billing date.";
						}
						else if (lngMC == lngColName || lngMC == lngColWInterestPTD || lngMC == lngColSInterestPTD)
						{
							cell.ToolTipText = vsAccounts.TextMatrix(lngMR, lngMC);
						}
						else
						{
							cell.ToolTipText = "";
						}
					}
				}
			}
		}

		private void vsAccounts_RowColChange(object sender, System.EventArgs e)
		{
			if (vsAccounts.Row > 0)
			{
				if (lngType == 0)
				{
					if (vsAccounts.Col == lngColAccount)
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else if (vsAccounts.Col == lngColWInterestPTD || vsAccounts.Col == lngColSInterestPTD)
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else if (vsAccounts.Col == lngColWIntOwed || vsAccounts.Col == lngColSIntOwed)
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else if (vsAccounts.Col == lngColName || vsAccounts.Col == lngColName2)
					{
						// allow the name to be edited
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else if (vsAccounts.Col == lngColWPrinOwed || vsAccounts.Col == lngColSPrinOwed || vsAccounts.Col == lngColWTaxOwed || vsAccounts.Col == lngColSTaxOwed || vsAccounts.Col == lngColWOrigIntOwed || vsAccounts.Col == lngColSOrigIntOwed || vsAccounts.Col == lngColWCostOwed || vsAccounts.Col == lngColSCostOwed)
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else if (vsAccounts.Col == lngTypeCol)
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
					}
					else if (vsAccounts.Col == lngHiddenCol || vsAccounts.Col == lngColOverwrite)
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				else
				{
					if (vsAccounts.Col == lngColAccount || vsAccounts.Col == lngColCurrent || vsAccounts.Col == lngColStartDate || vsAccounts.Col == lngColEndDate || vsAccounts.Col == lngColBillDate || vsAccounts.Col == lngColWAdj || vsAccounts.Col == lngColSAdj)
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
					}
					else
					{
						vsAccounts.Editable = FCGrid.EditableSettings.flexEDNone;
					}
				}
				if (vsAccounts.Editable == FCGrid.EditableSettings.flexEDKbdMouse)
				{
					vsAccounts.EditCell();
				}
			}
			if (vsAccounts.Row == vsAccounts.Rows - 1 && vsAccounts.Col == vsAccounts.Cols - 1)
			{
				vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsAccounts.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsAccounts_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - issue #1004: save and use correct indexes of the cell
			int row = vsAccounts.GetFlexRowIndex(e.RowIndex);
			int col = vsAccounts.GetFlexColIndex(e.ColumnIndex);
			if (lngType == 0)
			{
				if (col == lngColAccount)
				{
					// if this is validating an account
					if (vsAccounts.EditText != "")
					{
						if (Strings.Trim(vsAccounts.EditText) != vsAccounts.TextMatrix(row, col))
						{
							int lngRow = row;
							if (LoadAccountInformation_2(FCConvert.ToInt32(Conversion.Val(vsAccounts.EditText)), ref lngRow))
							{
							}
						}
					}
				}
				else if (col == lngColWInterestPTD || col == lngColSInterestPTD)
				{
					// validate the paid through date
					if (Information.IsDate(vsAccounts.EditText))
					{
						vsAccounts.TextMatrix(row, col, Strings.Format(vsAccounts.EditText, "MM/dd/yyyy"));
					}
					else
					{
						vsAccounts.TextMatrix(row, col, "");
						e.Cancel = true;
					}
				}
			}
			else
			{
				if (col == lngColAccount)
				{
					// if this is validating an account
					if (vsAccounts.EditText != "")
					{
						if (Strings.Trim(vsAccounts.EditText) != vsAccounts.TextMatrix(row, col))
						{
							int lngRow = row;
							if (LoadAccountInformation_2(FCConvert.ToInt32(Conversion.Val(vsAccounts.EditText)), ref lngRow))
							{
							}
							else
							{
								vsAccounts.EditText = "";
								vsAccounts.EditCell();
							}
						}
					}
				}
				else if (col == lngColStartDate || col == lngColEndDate || col == lngColBillDate)
				{
					// validate the paid through date
					if (Information.IsDate(vsAccounts.EditText))
					{
						vsAccounts.TextMatrix(row, col, Strings.Format(vsAccounts.EditText, "MM/dd/yyyy"));
					}
					else
					{
						vsAccounts.TextMatrix(row, col, "");
						e.Cancel = true;
					}
				}
			}
		}

		private void SearchForAccounts()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsSearch = new clsDRWrapper();
				string strSQL = "";
				FormatSearchGrid();
				// kk04212015 trouts-137  Add AccountNumber and Location fields to query
				rsSearch.OpenRecordset("SELECT * FROM (SELECT AccountNumber, pBill.FullNameLF AS Name, StreetNumber, Apt, StreetName FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID) AS qMstr WHERE Name > '" + Strings.Trim(txtSearchName.Text) + "     ' AND Name < '" + Strings.Trim(txtSearchName.Text) + "ZZZZZ' ORDER BY Name");
				if (rsSearch.EndOfFile())
				{
					// no accounts
					MessageBox.Show("No accounts match the search criteria.", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					vsAccountSearch.Rows = 1;
					while (!rsSearch.EndOfFile())
					{
                        if (rsSearch.Get_Fields_Boolean("SameBillOwner"))
                        {
                            vsAccountSearch.AddItem("\t" + rsSearch.Get_Fields("AccountNumber") + "\t" + rsSearch.Get_Fields_String("DeedName1") + "\t" + rsSearch.Get_Fields("StreetNumber") + Strings.Trim(FCConvert.ToString(rsSearch.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsSearch.Get_Fields_String("StreetName"))));
                        }
                        else
                        {
                            vsAccountSearch.AddItem("\t" + rsSearch.Get_Fields("AccountNumber") + "\t" + rsSearch.Get_Fields_String("Name") + "\t" + rsSearch.Get_Fields("StreetNumber") + Strings.Trim(FCConvert.ToString(rsSearch.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsSearch.Get_Fields_String("StreetName"))));
                        }
                        
						rsSearch.MoveNext();
					}
				}
				vsAccountSearch.Visible = true;
				// resize the grid
				SetSearchGridHeight();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Searching For Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void vsAccountSearch_DblClick(object sender, System.EventArgs e)
		{
			// selecting an account to use
			int lngRW;
			int lngAccount;
			lngRW = vsAccountSearch.MouseRow;
			//Application.DoEvents();
			lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(lngRW, 1))));
			ReturnFromSearch(ref lngAccount);
		}

		private void ReturnFromSearch_2(int lngAcct)
		{
			ReturnFromSearch(ref lngAcct);
		}

		private void ReturnFromSearch(ref int lngAcct)
		{
			fraAccountSearch.Visible = false;
			fraAccounts.Visible = true;
			mnuFileSave.Enabled = true;
			mnuFileSearch.Enabled = true;
			mnuFileNew.Enabled = true;
			if (lngAcct == 0)
			{
				if (vsAccounts.Visible && vsAccounts.Enabled)
				{
					vsAccounts.Focus();
				}
				else
				{
				}
			}
			else
			{
				LoadAccountInformation_6(lngAcct, vsAccounts.Rows - 1);
				SetGridHeight();
			}
		}

		private void vsAccountSearch_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int lngAccount = 0;
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (KeyAscii == 13)
			{
				// return key
				KeyAscii = 0;
				if (vsAccountSearch.Row > 0)
				{
					// selecting an account to use
					lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(vsAccountSearch.Row, 1))));
					ReturnFromSearch(ref lngAccount);
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		public void LoadRateKeys()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				cboRateKeys.Clear();
				if (lngType == 0)
				{
					rsInfo.OpenRecordset("SELECT * FROM RateKeys ORDER BY BillDate DESC");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT * FROM RateKeys WHERE RateType <> 'L' ORDER BY BillDate DESC");
				}
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						cboRateKeys.AddItem(Strings.Format(rsInfo.Get_Fields_Int32("ID"), "00000") + "  " + rsInfo.Get_Fields_String("RateType") + "   " + Strings.Format(rsInfo.Get_Fields("BillDate"), "MM/dd/yyyy") + "   " + rsInfo.Get_Fields_String("Description"));
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Rate Keys", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetRateKeyCombo(ref int lngKey)
		{
			int counter;
			for (counter = 0; counter <= cboRateKeys.Items.Count - 1; counter++)
			{
				if (Conversion.Val(Strings.Left(cboRateKeys.Text, 5)) == lngRateKey)
				{
					cboRateKeys.SelectedIndex = counter;
					break;
				}
			}
		}

		private int CreateLienRecord_240(int lngRow,  int lngAccountKey, Decimal curIntAdded, bool boolWater)
		{
			return CreateLienRecord(ref lngRow,  ref lngAccountKey, ref curIntAdded, ref boolWater);
		}

		private int CreateLienRecord(ref int lngRow,  ref int lngAccountKey, ref Decimal curIntAdded, ref bool boolWater)
		{
			int CreateLienRecord = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				rsInfo.OpenRecordset("SELECT * FROM Lien WHERE ID = 0", modExtraModules.strUTDatabase);
				rsInfo.AddNew();
				rsInfo.Update();
				if (boolWater)
				{
					rsInfo.Set_Fields("Water", true);
					rsInfo.Set_Fields("IntPaidDate", vsAccounts.TextMatrix(lngRow, lngColWInterestPTD));
					rsInfo.Set_Fields("Principal", vsAccounts.TextMatrix(lngRow, lngColWPrinOwed));
					rsInfo.Set_Fields("Tax", vsAccounts.TextMatrix(lngRow, lngColWTaxOwed));
					rsInfo.Set_Fields("Interest", vsAccounts.TextMatrix(lngRow, lngColWOrigIntOwed));
					rsInfo.Set_Fields("Costs", vsAccounts.TextMatrix(lngRow, lngColWCostOwed));
					if (FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColWIntOwed)) != 0)
					{
						rsInfo.Set_Fields("IntAdded", FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColWIntOwed)) * -1);
						// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						//if (rsInfo.Get_Fields_Double("IntAdded") != 0)
						//{
						//	// Add a CHGINT Line for this bill
						//	CreateCHGINTForBill_222(boolWater, rsInfo.Get_Fields_Int32("ID"), lngAccountKey, true, curIntAdded);
						//}
					}
					else
					{
						rsInfo.Set_Fields("IntAdded", 0);
					}
				}
				else
				{
					rsInfo.Set_Fields("Water", false);
					rsInfo.Set_Fields("IntPaidDate", vsAccounts.TextMatrix(lngRow, lngColSInterestPTD));
					rsInfo.Set_Fields("Principal", vsAccounts.TextMatrix(lngRow, lngColSPrinOwed));
					rsInfo.Set_Fields("Tax", vsAccounts.TextMatrix(lngRow, lngColSTaxOwed));
					rsInfo.Set_Fields("Interest", vsAccounts.TextMatrix(lngRow, lngColSOrigIntOwed));
					rsInfo.Set_Fields("Costs", vsAccounts.TextMatrix(lngRow, lngColSCostOwed));
					if (FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColSIntOwed)) != 0)
					{
						rsInfo.Set_Fields("IntAdded", FCConvert.ToDecimal(vsAccounts.TextMatrix(lngRow, lngColSIntOwed)) * -1);
						// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						//if (FCConvert.ToInt32(rsInfo.Get_Fields("IntAdded")) != 0)
						//{
						//	// Add a CHGINT Line for this bill
						//	CreateCHGINTForBill_222(boolWater, rsInfo.Get_Fields_Int32("ID"), lngAccountKey, true, curIntAdded);
						//}
					}
					else
					{
						rsInfo.Set_Fields("IntAdded", 0);
					}
				}
				rsInfo.Set_Fields("PrinPaid", 0);
				rsInfo.Set_Fields("TaxPaid", 0);
				rsInfo.Set_Fields("PLIPaid", 0);
				rsInfo.Set_Fields("IntPaid", 0);
				rsInfo.Set_Fields("CostPaid", 0);
				rsInfo.Set_Fields("MaturityFee", 0);
				rsInfo.Set_Fields("DateCreated", DateTime.Today);
				rsInfo.Set_Fields("Status", "A");
				rsInfo.Set_Fields("RateKey", FCConvert.ToString(Conversion.Val(Strings.Left(cboRateKeys.Text, 5))));
				rsInfo.Set_Fields("Book", 0);
				rsInfo.Set_Fields("Page", 0);
				rsInfo.Set_Fields("PrintedLDN", false);
				
				rsInfo.Update();
                CreateLienRecord = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID"));
				if (boolWater)
                {
                    if (vsAccounts.TextMatrix(lngRow, lngColWIntOwed).ToDecimalValue() != 0)
                    {
                        if (curIntAdded != 0)
                        {
                            // Add a CHGINT Line for this bill
                            CreateCHGINTForBill_222(boolWater, rsInfo.Get_Fields_Int32("ID"), lngAccountKey, true, curIntAdded);
                        }
                    }
				}
                else
                {
                    if (vsAccounts.TextMatrix(lngRow, lngColSIntOwed).ToDecimalValue() != 0)
                    {
                        if (curIntAdded != 0)
                        {
                            // Add a CHGINT Line for this bill
                            CreateCHGINTForBill_222(boolWater, rsInfo.Get_Fields_Int32("ID"), lngAccountKey, true, curIntAdded);
                        }
                    }
				}
				return CreateLienRecord;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Lien Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CreateLienRecord;
		}
		// vbPorter upgrade warning: dblAmt As double	OnWrite(Decimal, double)
		private void CreateCHGINTForBill_222(bool boolWater, int lngBill, int lngAcct, bool boolLienRec, decimal dblAmt, DateTime? dtRTDTemp = null/*DateTime dtRTD= DateTime.Now*/)
		{
			DateTime dtRTD = dtRTDTemp ?? DateTime.Now;
			CreateCHGINTForBill( boolWater,  lngBill,  lngAcct,  boolLienRec,  dblAmt, dtRTD);
		}

		private void CreateCHGINTForBill_242(bool boolWater, int lngBill, int lngAcct, bool boolLienRec, decimal dblAmt, DateTime? dtRTDTemp = null/*DateTime dtRTD= DateTime.Now*/)
		{
			DateTime dtRTD = dtRTDTemp ?? DateTime.Now;
			CreateCHGINTForBill(boolWater,  lngBill, lngAcct,  boolLienRec,  dblAmt, dtRTD);
		}

		private void CreateCHGINTForBill( bool boolWater,  int lngBill,  int lngAcct,  bool boolLienRec,  decimal dblAmt, DateTime? dtRTDTemp = null/*DateTime dtRTD = DateTime.Now*/)
		{
			DateTime dtRTD = dtRTDTemp ?? DateTime.Now;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will create a charged interest line for the bill passed in
				// and will not update the lien or bill record
				clsDRWrapper rsP = new clsDRWrapper();
				rsP.OpenRecordset("SELECT * FROM PaymentRec WHERE ID = 0", modExtraModules.strUTDatabase);
				rsP.AddNew();
				rsP.Set_Fields("AccountKey", lngAcct);
				rsP.Set_Fields("BillKey", lngBill);
                if (!boolLienRec)
                {
					rsP.Set_Fields("BillId",lngBill);
                }
                else
                {
					rsP.Set_Fields("LienId",lngBill);
                }
				rsP.Set_Fields("ActualSystemDate", DateTime.Today);
				rsP.Set_Fields("EffectiveInterestDate", DateTime.Today);
				if (dtRTD.ToOADate() == 0)
				{
					// if nothing gets passed in then use the current date
					rsP.Set_Fields("RecordedTransactionDate", DateTime.Today);
				}
				else
				{
					rsP.Set_Fields("RecordedTransactionDate", dtRTD);
				}
				rsP.Set_Fields("Reference", "CHGINT");
				rsP.Set_Fields("Period", "A");
				rsP.Set_Fields("Code", "I");
				rsP.Set_Fields("CurrentInterest", dblAmt * -1);
				rsP.Set_Fields("CashDrawer", "Y");
				rsP.Set_Fields("GeneralLedger", "Y");
				if (boolWater)
				{
					rsP.Set_Fields("Service", "W");
				}
				else
				{
					rsP.Set_Fields("Service", "S");
				}
				rsP.Set_Fields("Lien", boolLienRec);
				rsP.Update();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + FCConvert.ToString(Information.Err(ex).Number) + ".", "Error Creating Charged Interest", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private double CalculateNewAccountTotal_8(int lngAccountKey, int lngRow)
		{
			return CalculateNewAccountTotal(ref lngAccountKey, ref lngRow);
		}

		private double CalculateNewAccountTotal(ref int lngAccountKey, ref int lngRow)
		{
			double CalculateNewAccountTotal = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This funciton cycles through the meters that are not combined to find the billing amount due
				// The combined meters will be calculated when it processes the main meter
				clsDRWrapper rsM = new clsDRWrapper();
				double dblWPrin = 0;
				double dblWTax = 0;
				double dblSPrin = 0;
				double dblSTax = 0;
				// kk04152015 trouts-106  Add BookNumber to query
				rsM.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BookNumber = " + vsAccounts.TextMatrix(lngRow, lngColBook) + " AND Sequence = " + vsAccounts.TextMatrix(lngRow, lngColSequence), modExtraModules.strUTDatabase);
				if (!rsM.EndOfFile())
				{
					rsM.Edit();
					rsM.Set_Fields("CurrentReadingDate", vsAccounts.TextMatrix(lngRow, lngColBillDate));
					rsM.Set_Fields("CurrentReading", vsAccounts.TextMatrix(lngRow, lngColCurrent));
					rsM.Update();
					if (FCConvert.ToInt32(rsM.Get_Fields_Int32("MeterNumber")) == 1)
					{
						CalculateNewAccountTotal = CalculateBillAmounts_18960(lngAccountKey, rsM.Get_Fields_Int32("ID"), ref dblWPrin, ref dblWTax, ref dblSPrin, ref dblSTax, DateAndTime.DateValue(vsAccounts.TextMatrix(lngRow, lngColBillDate)), DateAndTime.DateValue(vsAccounts.TextMatrix(lngRow, lngColStartDate)), DateAndTime.DateValue(vsAccounts.TextMatrix(lngRow, lngColEndDate)), lngRow);
					}
				}
				// .TextMatrix(lngRow, lngColWPrinOwed) = dblWPrin
				// .TextMatrix(lngRow, lngColWTaxOwed) = dblWTax
				// .TextMatrix(lngRow, lngColSPrinOwed) = dblSPrin
				// .TextMatrix(lngRow, lngColSTaxOwed) = dblSTax
				return CalculateNewAccountTotal;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateNewAccountTotal;
		}

		private double CalculateBillAmounts_18960(int lngAccountKey, int lngMeterKey, ref double dblWPrinSum, ref double dblWTaxSum, ref double dblSPrinSum, ref double dblSTaxSum, DateTime dtBill, DateTime dtStart, DateTime dtEnd, int lngRow = 0)
		{
			return CalculateBillAmounts(ref lngAccountKey, ref lngMeterKey, ref dblWPrinSum, ref dblWTaxSum, ref dblSPrinSum, ref dblSTaxSum, ref dtBill, ref dtStart, ref dtEnd, lngRow);
		}

		private double CalculateBillAmounts(ref int lngAccountKey, ref int lngMeterKey, ref double dblWPrinSum, ref double dblWTaxSum, ref double dblSPrinSum, ref double dblSTaxSum, ref DateTime dtBill, ref DateTime dtStart, ref DateTime dtEnd, int lngRow = 0)
		{
			double CalculateBillAmounts = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// This will default the totals
				// this sub will calculate the current bill/meter information in order to
				// fill the rest of the bill record
				string Service = "";
				int intCT;
				double[,] dblBillAmount = new double[5 + 1, 1 + 1];
				// (1 - Cons, 2 - Flat, 3 - Units, 4 - Misc1, 5 - Misc2, 0 - Water, 1 - Sewer)
				// vbPorter upgrade warning: lngCons As int	OnWrite(int, double)
				int lngCons = 0;
				double dblUnits = 0;
				int lngRT = 0;
				string strMisc1Desc = "";
				string strMisc2Desc = "";
				double dblMisc1 = 0;
				double dblMisc2 = 0;
				double dblMiscSum1 = 0;
				double dblMiscSum2 = 0;
				double dblSAdjustAmount = 0;
				double dblWAdjustAmount = 0;
				double dblCons = 0;
				double dblFlat = 0;
				double dblUnit = 0;
				double dblSumCons = 0;
				double dblSumFlat = 0;
				double dblSumUnit = 0;
				double dblWTax = 0;
				double dblSTax = 0;
				// vbPorter upgrade warning: dblWDEAdj As double	OnWrite(short, string)
				double dblWDEAdj = 0;
				// vbPorter upgrade warning: dblSDEAdj As double	OnWrite(short, string)
				double dblSDEAdj = 0;
				double dblTaxableAmount;
				// vbPorter upgrade warning: dblOverrideAmount As double	OnRead(int, double)
				double dblOverrideAmount = 0;
				clsDRWrapper rsM = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				int lngBill;
				string strSQL;
				int lngBook = 0;
				bool boolAddBreakdownRecords;
				bool boolFinal;
				double dblImpervSurf = 0;
				bool boolStormwaterOnly = false;
				boolFinal = FCConvert.CBool(lngType == 1);
				// this array will only hold the taxable amount
				// 0 - Consumption, 1- Flat, 2- Units, 3 - Misc, 4 - Data Entry Adjustments, 5 - Meter Adjustments
				double[] dblTaxes = new double[5 + 1];
				dblTaxableAmount = 0;
				boolAddBreakdownRecords = true;
				strSQL = "SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND ID = " + FCConvert.ToString(lngMeterKey);
				rsM.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!rsM.EndOfFile())
				{
					// kk03132015 trouts-106  Add final bills for stormwater billing
					dblImpervSurf = 0;
					boolStormwaterOnly = false;
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
						if ((FCConvert.ToInt32(rsM.Get_Fields("Frequency")) == 1 || FCConvert.ToInt32(rsM.Get_Fields("Frequency")) == 2) && boolBillStormwater)
						{
							strSQL = "SELECT ImpervSurfArea, NoBill FROM Master WHERE ID = " + FCConvert.ToString(lngAccountKey);
							rsBill.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
							if (!rsBill.EndOfFile())
							{
								if (rsBill.Get_Fields_Decimal("ImpervSurfArea") >= 0)
								{
									// kk07082015 trouts-149  Indicate account is not billed for SW by setting the Value to -1
									dblImpervSurf = FCConvert.ToDouble(rsBill.Get_Fields_Decimal("ImpervSurfArea"));
								}
								else
								{
									dblImpervSurf = 0;
								}
								boolStormwaterOnly = FCConvert.ToBoolean(rsBill.Get_Fields_Boolean("NoBill"));
							}
						}
					}
					// add new bill
					strSQL = "SELECT * FROM Bill WHERE MeterKey = " + FCConvert.ToString(lngMeterKey) + " AND (BillStatus <> 'B')";
					rsBill.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
					if (rsBill.EndOfFile())
					{
						rsBill.AddNew();
						// kk 06242013 trouts-19  Bill Fields with Nulls messing up prepays
						rsBill.Set_Fields("WLienStatusEligibility", 0);
						rsBill.Set_Fields("WLienProcessStatus", 0);
						rsBill.Set_Fields("WLienRecordNumber", 0);
						rsBill.Set_Fields("WCombinationLienKey", 0);
						rsBill.Set_Fields("SLienStatusEligibility", 0);
						rsBill.Set_Fields("SLienProcessStatus", 0);
						rsBill.Set_Fields("SLienRecordNumber", 0);
						rsBill.Set_Fields("SCombinationLienKey", 0);
						//rsBill.Set_Fields("WIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
						// FC:FINAL:VGE - #i877 Using OADate to bypass database validation errors
						rsBill.Set_Fields("WIntPaidDate", DateTime.FromOADate(0));
						rsBill.Set_Fields("WIntOwed", 0);
						rsBill.Set_Fields("WIntAdded", 0);
						rsBill.Set_Fields("WCostOwed", 0);
						rsBill.Set_Fields("WCostAdded", 0);
						rsBill.Set_Fields("WPrinPaid", 0);
						rsBill.Set_Fields("WTaxPaid", 0);
						rsBill.Set_Fields("WIntPaid", 0);
						rsBill.Set_Fields("WCostPaid", 0);
						//rsBill.Set_Fields("SIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
						// FC:FINAL:VGE - #i877 Using OADate to bypass database validation errors
						rsBill.Set_Fields("SIntPaidDate", DateTime.FromOADate(0));
						rsBill.Set_Fields("SIntOwed", 0);
						rsBill.Set_Fields("SIntAdded", 0);
						rsBill.Set_Fields("SCostOwed", 0);
						rsBill.Set_Fields("SCostAdded", 0);
						rsBill.Set_Fields("SPrinPaid", 0);
						rsBill.Set_Fields("STaxPaid", 0);
						rsBill.Set_Fields("SIntPaid", 0);
						rsBill.Set_Fields("SCostPaid", 0);
						rsBill.Set_Fields("Copies", 0);
						rsBill.Set_Fields("Loadback", 0);
						rsBill.Set_Fields("Final", 0);
						rsBill.Set_Fields("SHasOverride", 0);
						rsBill.Set_Fields("WHasOverride", 0);
						rsBill.Set_Fields("SDemandGroupID", 0);
						rsBill.Set_Fields("WDemandGroupID", 0);
						rsBill.Set_Fields("SendEBill", 0);
						rsBill.Set_Fields("WinterBill", 0);
						rsBill.Set_Fields("SUploaded", 0);
						rsBill.Set_Fields("WUploaded", 0);
						rsBill.Update();
					}
					else
					{
						rsBill.Edit();
					}
					lngBook = FCConvert.ToInt32(rsM.Get_Fields_Int32("BookNumber"));
					rsBill.Set_Fields("Book", lngBook);
					rsBill.Set_Fields("Final", true);
					rsBill.Set_Fields("BillStatus", "X");
					rsBill.Set_Fields("Service", rsM.Get_Fields_String("Service"));
					rsBill.Set_Fields("ActualAccountNumber", modUTStatusPayments.GetAccountNumber(ref lngAccountKey));
					rsBill.Set_Fields("AccountKey", lngAccountKey);
					rsBill.Set_Fields("MeterKey", lngMeterKey);
					rsBill.Set_Fields("CurReading", rsM.Get_Fields_Int32("CurrentReading"));
					rsBill.Set_Fields("PrevReading", rsM.Get_Fields_Int32("PreviousReading"));
					if (FCConvert.ToInt32(rsBill.Get_Fields_Int32("CurReading")) == -1)
					{
						// kk trout-884 111312  Add check for No Read
						rsBill.Set_Fields("Consumption", rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading"));
					}
					else
					{
						rsBill.Set_Fields("Consumption", 0);
					}
					// kk05192016 trouts-180  Add the Reading Dates to the bill
					if (Information.IsDate(rsM.Get_Fields("CurrentReadingDate")))
					{
						rsBill.Set_Fields("CurDate", rsM.Get_Fields_DateTime("CurrentReadingDate"));
					}
					if (Information.IsDate(rsM.Get_Fields("PreviousReadingDate")))
					{
						rsBill.Set_Fields("PrevDate", rsM.Get_Fields_DateTime("PreviousReadingDate"));
					}
					if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColSAdj)) == 0)
					{
						dblSDEAdj = 0;
					}
					else
					{
						dblSDEAdj = FCConvert.ToDouble(vsAccounts.TextMatrix(lngRow, lngColSAdj));
					}
					if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColWAdj)) == 0)
					{
						dblWDEAdj = 0;
					}
					else
					{
						dblWDEAdj = FCConvert.ToDouble(vsAccounts.TextMatrix(lngRow, lngColWAdj));
					}
					rsBill.Set_Fields("SDEAdjustAmount", dblSDEAdj);
					rsBill.Set_Fields("WDEAdjustAmount", dblWDEAdj);
					rsBill.Set_Fields("FinalBillDate", dtBill);
					rsBill.Set_Fields("FinalStartDate", dtStart);
					rsBill.Set_Fields("FinalEndDate", dtEnd);
					rsM.Edit();
					rsM.Set_Fields("Final", true);
					rsM.Set_Fields("FinalBillDate", dtBill);
					rsM.Set_Fields("FinalStartDate", dtStart);
					rsM.Set_Fields("FinalEndDate", dtEnd);
					rsM.Update();
					rsBill.Set_Fields("BillingRateKey", FCConvert.ToString(Conversion.Val(Strings.Left(cboRateKeys.Items[cboRateKeys.SelectedIndex].ToString(), 5))));
					rsBill.Set_Fields("BillNumber", rsBill.Get_Fields_Int32("BillingRateKey"));
					rsBill.Set_Fields("NoBill", boolStormwaterOnly);
					// kk04152015 trouts-106  Only bill stormwater if account is marked for NoBill
					if (!boolStormwaterOnly)
					{
						// check to see if this meter is combined by consumption
						// the combination by consumption will be taken care
						// of when calculating the main meter
						if (FCConvert.ToString(rsM.Get_Fields_String("Combine")) != "C")
						{
							// Water
							if (FCConvert.ToString(rsM.Get_Fields_String("Service")) != "S")
							{
								for (intCT = 1; intCT <= 5; intCT++)
								{
									// TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
									if (FCConvert.ToBoolean(rsM.Get_Fields("UseRate" + FCConvert.ToString(intCT))))
									{
										dblMisc1 = 0;
										dblMisc2 = 0;
										strMisc1Desc = "";
										strMisc2Desc = "";
										lngRT = FCConvert.ToInt32(rsM.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT)));
										// get the rate table
										// .Fields("WRT" & intCT) = lngRT
										// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
										if (Conversion.Val(rsM.Get_Fields("WaterAmount" + FCConvert.ToString(intCT))) != 0)
										{
											// TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
											dblOverrideAmount = FCConvert.ToDouble(rsM.Get_Fields("WaterAmount" + FCConvert.ToString(intCT)));
											// get the override amount
										}
										else
										{
											dblOverrideAmount = 0;
										}
										// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
										if (rsM.Get_Fields("WaterType" + FCConvert.ToString(intCT)) == 1)
										{
											// consumption
											if (FCConvert.ToInt32(rsM.Get_Fields_Int32("MeterNumber")) == 1)
											{
												lngCons = modUTBilling.FindTotalMeterConsumption_6(lngAccountKey, true);
											}
											else if (FCConvert.ToInt32(dblOverrideAmount) != 0)
											{
												lngCons = FCConvert.ToInt32(dblOverrideAmount);
											}
											else
											{
												lngCons = FCConvert.ToInt32(rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading"));
											}
											bool boolMin = false;
											dblCons = FCUtils.Round(modUTBilling.CalculateConsumptionBased_13140(ref lngCons, lngRT, true, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), ref boolMin), 2);
											dblSumCons += dblCons;
											if (dblCons != 0 && boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_487700(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Consumption", 0, "", dblCons, "C", lngRT, FCConvert.ToDouble(lngCons), rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
										}
										// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
										else if (rsM.Get_Fields("WaterType" + FCConvert.ToString(intCT)) == 2)
										{
											// flat
											dblFlat = FCUtils.Round((modUTBilling.CalculateFlatBased_6(lngRT, true, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, dblOverrideAmount, ref strMisc1Desc, ref strMisc2Desc) * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
											dblSumFlat += dblFlat;
											if (dblFlat != 0 && boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_433025(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Flat", 0, "", dblFlat, "F", lngRT, rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
										}
											// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
											else if (rsM.Get_Fields("WaterType" + FCConvert.ToString(intCT)) == 3)
										{
											// units
											lngCons = FCConvert.ToInt32(dblOverrideAmount);
											// set the amount of units
											dblUnits = dblOverrideAmount;
											dblUnit = FCUtils.Round(modUTBilling.CalculateUnitBased_13140(dblUnits, lngRT, true, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
											dblSumUnit += dblUnit;
											if (dblUnit != 0 && boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_487700(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Unit", 0, "", dblUnit, "U", lngRT, FCConvert.ToDouble(lngCons), rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
										}
										// add up all of the misc amounts
										if (dblMisc1 != 0)
										{
											if (boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_432863(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", strMisc1Desc, 0, "", dblMisc1 * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), "M", lngRT, rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
											dblMiscSum1 += dblMisc1;
										}
										if (dblMisc2 != 0)
										{
											if (boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_432863(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", strMisc2Desc, 0, "", dblMisc2 * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), "M", lngRT, rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
											dblMiscSum2 += dblMisc2;
										}
									}
								}
								if (rsM.Get_Fields_Boolean("UseAdjustRate") && rsM.Get_Fields_Double("WaterAdjustAmount") != 0)
								{
									dblWAdjustAmount = rsM.Get_Fields_Double("WaterAdjustAmount");
									if (boolAddBreakdownRecords)
									{
										modUTBilling.AddBreakDownRecord_448334(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Adjustment", 0, "", dblWAdjustAmount, "A", rsBill.Get_Fields_Int32("ActualAccountNumber"));
									}
								}
								else
								{
									dblWAdjustAmount = 0;
								}
								if (dblWDEAdj != 0)
								{
									if (boolAddBreakdownRecords)
									{
										modUTBilling.AddBreakDownRecord_448334(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Adjustment", 0, "", dblWDEAdj, "A", rsBill.Get_Fields_Int32("ActualAccountNumber"));
									}
								}
								// fill in the water category for this bill
								rsBill.Set_Fields("WCat", modMain.GetCategory_8(true, rsBill.Get_Fields_Int32("AccountKey")));
							}
							else
							{
								rsBill.Set_Fields("WCat", 0);
							}
							// add taxes
							dblWTax = CalculateTaxes_8(true, rsM.Get_Fields_Int32("ID"), dblTaxableAmount);
							if (boolAddBreakdownRecords && FCUtils.Round(dblWTax, 2) != 0)
							{
								modUTBilling.AddBreakDownRecord_448334(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Tax", 0, "", dblWTax, "T", rsBill.Get_Fields_Int32("ActualAccountNumber"));
							}
							dblTaxableAmount = 0;
							dblOverrideAmount = 0;
							if (FCConvert.ToString(rsM.Get_Fields_String("Combine")) == "B")
							{
								// WDEAdjustAmount is filled in the DE screen
								rsBill.Set_Fields("WConsumptionAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WConsumptionAmount")) + dblSumCons);
								rsBill.Set_Fields("WFlatAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WFlatAmount")) + dblSumFlat);
								rsBill.Set_Fields("WUnitsAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WUnitsAmount")) + dblSumUnit);
								rsBill.Set_Fields("WMiscAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WMiscAmount")) + dblMiscSum1 + dblMiscSum2);
								// TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
								rsBill.Set_Fields("WTax", rsBill.Get_Fields("WTax") + dblWTax);
								rsBill.Set_Fields("WAdjustAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WAdjustAmount")) + dblWAdjustAmount);
							}
							else
							{
								// WDEAdjustAmount is filled in the DE screen
								rsBill.Set_Fields("WConsumptionAmount", dblSumCons);
								rsBill.Set_Fields("WFlatAmount", dblSumFlat);
								rsBill.Set_Fields("WUnitsAmount", dblSumUnit);
								rsBill.Set_Fields("WMiscAmount", dblMiscSum1 + dblMiscSum2);
								rsBill.Set_Fields("WTax", dblWTax);
								rsBill.Set_Fields("WAdjustAmount", dblWAdjustAmount);
							}
							CalculateBillAmounts = dblSumCons + dblSumFlat + dblSumUnit + dblMiscSum1 + dblMiscSum2 + dblWTax + dblWAdjustAmount;
							dblWPrinSum += dblSumCons + dblSumFlat + dblSumUnit + dblMiscSum1 + dblMiscSum2 + dblWAdjustAmount;
							dblWTaxSum += dblWTax;
							dblWTax = 0;
							dblMiscSum1 = 0;
							dblMiscSum2 = 0;
							dblSumCons = 0;
							dblSumFlat = 0;
							dblSumUnit = 0;
							dblCons = 0;
							dblFlat = 0;
							dblUnit = 0;
							// Sewer
							if (FCConvert.ToString(rsM.Get_Fields_String("Service")) != "W")
							{
								for (intCT = 1; intCT <= 5; intCT++)
								{
									// TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
									if (FCConvert.ToBoolean(rsM.Get_Fields("UseRate" + FCConvert.ToString(intCT))))
									{
										dblMisc1 = 0;
										dblMisc2 = 0;
										strMisc1Desc = "";
										strMisc2Desc = "";
										lngRT = FCConvert.ToInt32(rsM.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT)));
										// get the rate table
										rsBill.Set_Fields("SRT" + intCT, lngRT);
										// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
										if (Conversion.Val(rsM.Get_Fields("SewerAmount" + FCConvert.ToString(intCT))) != 0)
										{
											// TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
											dblOverrideAmount = FCConvert.ToDouble(rsM.Get_Fields("SewerAmount" + FCConvert.ToString(intCT)));
											// get the override amount
										}
										else
										{
											dblOverrideAmount = 0;
										}
										// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
										if (rsM.Get_Fields("SewerType" + FCConvert.ToString(intCT)) == 1)
										{
											// consumption
											if (FCConvert.ToInt32(rsM.Get_Fields_Int32("MeterNumber")) == 1)
											{
												lngCons = modUTBilling.FindTotalMeterConsumption_8(rsBill.Get_Fields_Int32("AccountKey"), false);
											}
											else if (FCConvert.ToInt32(dblOverrideAmount) != 0)
											{
												lngCons = FCConvert.ToInt32(dblOverrideAmount);
											}
											else
											{
												lngCons = FCConvert.ToInt32(rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading"));
											}
											bool boolMin = false;
											dblCons = FCUtils.Round(modUTBilling.CalculateConsumptionBased_13140(ref lngCons, lngRT, false, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), ref boolMin), 2);
											dblSumCons += dblCons;
											if (dblCons != 0 && boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_487700(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Consumption", 0, "", dblCons, "C", lngRT, FCConvert.ToDouble(lngCons), rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
										}
										// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
										else if (rsM.Get_Fields("SewerType" + FCConvert.ToString(intCT)) == 2)
										{
											// flat
											dblFlat = FCUtils.Round((modUTBilling.CalculateFlatBased_6(lngRT, false, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, dblOverrideAmount, ref strMisc1Desc, ref strMisc2Desc) * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
											dblSumFlat += dblFlat;
											if (dblFlat != 0 && boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_433025(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Flat", 0, "", dblFlat, "F", lngRT, rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
										}
											// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
											else if (rsM.Get_Fields("SewerType" + FCConvert.ToString(intCT)) == 3)
										{
											// units
											lngCons = FCConvert.ToInt32(dblOverrideAmount);
											// set the amount of units
											dblUnits = dblOverrideAmount;
											dblUnit = FCUtils.Round(modUTBilling.CalculateUnitBased_13140(dblUnits, lngRT, false, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
											dblSumUnit += dblUnit;
											if (dblUnit != 0 && boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_487700(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Unit", 0, "", dblUnit, "U", lngRT, FCConvert.ToDouble(lngCons), rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
										}
										// add up all of the misc amounts
										if (dblMisc1 != 0)
										{
											if (boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_432863(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", strMisc1Desc, 0, "", dblMisc1 * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), "M", lngRT, rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
											dblMiscSum1 += dblMisc1;
										}
										if (dblMisc2 != 0)
										{
											if (boolAddBreakdownRecords)
											{
												modUTBilling.AddBreakDownRecord_432863(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", strMisc2Desc, 0, "", dblMisc2 * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), "M", lngRT, rsBill.Get_Fields_Int32("ActualAccountNumber"));
											}
											dblMiscSum2 += dblMisc2;
										}
									}
								}
								if (rsM.Get_Fields_Boolean("UseAdjustRate") && rsM.Get_Fields_Double("SewerAdjustAmount") != 0)
								{
									dblSAdjustAmount = rsM.Get_Fields_Double("SewerAdjustAmount");
									if (boolAddBreakdownRecords)
									{
										modUTBilling.AddBreakDownRecord_448334(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Adjustment", 0, "", dblSAdjustAmount, "A", rsBill.Get_Fields_Int32("ActualAccountNumber"));
									}
								}
								else
								{
									dblSAdjustAmount = 0;
								}
								if (dblSDEAdj != 0)
								{
									if (boolAddBreakdownRecords)
									{
										modUTBilling.AddBreakDownRecord_448334(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Adjustment", 0, "", dblSDEAdj, "A", rsBill.Get_Fields_Int32("ActualAccountNumber"));
									}
								}
								// fill in the sewer category for this bill
								rsBill.Set_Fields("SCat", modMain.GetCategory_8(false, rsBill.Get_Fields_Int32("AccountKey")));
							}
							else
							{
								rsBill.Set_Fields("SCat", 0);
							}
							dblSTax = CalculateTaxes_8(false, rsM.Get_Fields_Int32("ID"), dblTaxableAmount);
							if (boolAddBreakdownRecords && FCUtils.Round(dblSTax, 2) != 0)
							{
								modUTBilling.AddBreakDownRecord_448334(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Tax", 0, "", dblSTax, "T", rsBill.Get_Fields_Int32("ActualAccountNumber"));
							}
							dblTaxableAmount = 0;
							dblOverrideAmount = 0;
							if (FCConvert.ToString(rsM.Get_Fields_String("Combine")) == "B")
							{
								// SDEAdjustAmount is filled in the DE screen
								rsBill.Set_Fields("SConsumptionAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SConsumptionAmount")) + dblSumCons);
								rsBill.Set_Fields("SFlatAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SFlatAmount")) + dblSumFlat);
								rsBill.Set_Fields("SUnitsAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SUnitsAmount")) + dblSumUnit);
								rsBill.Set_Fields("SMiscAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SMiscAmount")) + dblMiscSum1 + dblMiscSum2);
								// TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
								rsBill.Set_Fields("STax", rsBill.Get_Fields("STax") + dblSTax);
								rsBill.Set_Fields("SAdjustAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SAdjustAmount")) + dblSAdjustAmount);
							}
							else
							{
								// SDEAdjustAmount is filled in the DE screen
								rsBill.Set_Fields("SConsumptionAmount", dblSumCons);
								rsBill.Set_Fields("SFlatAmount", dblSumFlat);
								rsBill.Set_Fields("SUnitsAmount", dblSumUnit);
								rsBill.Set_Fields("SMiscAmount", dblMiscSum1 + dblMiscSum2);
								rsBill.Set_Fields("STax", dblSTax);
								rsBill.Set_Fields("SAdjustAmount", dblSAdjustAmount);
							}
						}
					}
					// kk03112015 trouts-106  Bangor - Bill Stormwater Fee in place of water bill
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
						if ((FCConvert.ToInt32(rsM.Get_Fields("Frequency")) == 1 || FCConvert.ToInt32(rsM.Get_Fields("Frequency")) == 2) && boolBillStormwater)
						{
							// TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
							dblFlat = FCUtils.Round((modUTBilling.GetStormwaterFee_6(dblImpervSurf, FCConvert.ToInt32(rsM.Get_Fields("Frequency")) == 2) * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
							if (dblFlat != 0 && boolAddBreakdownRecords)
							{
								modUTBilling.AddBreakDownRecord_19682(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Stormwater Fee", 0, "", dblFlat, "F");
								rsBill.Set_Fields("WConsumptionAmount", 0);
								rsBill.Set_Fields("WFlatAmount", dblFlat);
								rsBill.Set_Fields("WUnitsAmount", 0);
								rsBill.Set_Fields("WMiscAmount", 0);
								rsBill.Set_Fields("WTax", 0);
								rsBill.Set_Fields("WAdjustAmount", 0);
								CalculateBillAmounts = dblFlat;
								dblWPrinSum = dblFlat;
							}
						}
					}
					rsBill.Update();
				}
				else
				{
					MessageBox.Show("The account does not have a meter associated with it.", "No Meter", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				CalculateBillAmounts += dblSumCons + dblSumFlat + dblSumUnit + dblMiscSum1 + dblMiscSum2 + dblSTax + dblSAdjustAmount;
				dblSPrinSum += dblSumCons + dblSumFlat + dblSumUnit + dblMiscSum1 + dblMiscSum2 + dblSAdjustAmount;
				dblSTaxSum += dblSTax;
				return CalculateBillAmounts;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Bill Amounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateBillAmounts;
		}

		private void RemoveFinalBilled(int lngRow)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will remove all the bills created for this account through the final bill function
				clsDRWrapper rsDel = new clsDRWrapper();
				if (Conversion.Val(vsAccounts.TextMatrix(lngRow, lngColAccount)) != 0)
				{
					int lngAccountNumber = FCConvert.ToInt32(vsAccounts.TextMatrix(lngRow, lngColAccount));
					rsDel.OpenRecordset("SELECT * FROM Bill WHERE ISNULL(Final,0) = 1 AND BillStatus <> 'B' AND AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAccountNumber)), modExtraModules.strUTDatabase);
					while (!rsDel.EndOfFile())
					{
						rsDel.Delete();
						rsDel.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Removing Final Bills", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private double CalculateTaxes_8(bool boolWater, int lngMeterKey, double dblAmount)
		{
			return CalculateTaxes(ref boolWater, ref lngMeterKey, ref dblAmount);
		}

		private double CalculateTaxes(ref bool boolWater, ref int lngMeterKey, ref double dblAmount)
		{
			double CalculateTaxes = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the tax for each meter and combine them if needed
				// then return the total tax for the Water or Sewer for that bill
				clsDRWrapper rsM = new clsDRWrapper();
				string strWaterSewer = "";
				string strWS = "";
				double dblMeterTax = 0;
				double dblTaxableAmount = 0;
				double dblTaxablePercent = 0;
				if (boolWater)
				{
					strWaterSewer = "Water";
					strWS = "W";
				}
				else
				{
					strWaterSewer = "Sewer";
					strWS = "S";
				}
				rsM.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(lngMeterKey), modExtraModules.strUTDatabase);
				if (!rsM.EndOfFile())
				{
					// get the taxable amount
					dblTaxablePercent = rsM.Get_Fields(strWaterSewer + "TaxPercent");
					if (dblTaxablePercent == 0)
						return CalculateTaxes;
					// this meter is not taxable (ie customer)
					dblTaxableAmount = dblAmount * (dblTaxablePercent / 100);
					// find the percentage to be taxes
					dblMeterTax = dblTaxableAmount * modUTStatusPayments.Statics.gdblUTMuniTaxRate;
				}
				CalculateTaxes = FCUtils.Round(dblMeterTax, 2);
				return CalculateTaxes;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Calculating Tax", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return CalculateTaxes;
		}

		private void FindPeriodDates_2(int lngFreq, ref DateTime dtStart, ref DateTime dtEnd)
		{
			FindPeriodDates(ref lngFreq, ref dtStart, ref dtEnd);
		}

		private void FindPeriodDates(ref int lngFreq, ref DateTime dtStart, ref DateTime dtEnd)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRK = new clsDRWrapper();
				rsRK.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + Strings.Left(cboRateKeys.Items[cboRateKeys.SelectedIndex].ToString(), 6), modExtraModules.strUTDatabase);
				if (!rsRK.EndOfFile())
				{
					dtStart = (DateTime)rsRK.Get_Fields_DateTime("Start");
					// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
					dtEnd = (DateTime)rsRK.Get_Fields("End");
				}
				else
				{
					DateTime dtCurDate;
					switch (FCConvert.ToInt32(lngFreq))
					{
						case 1:
							{
								// Quarterly
								switch (FCConvert.ToInt32(Strings.Format(DateTime.Today, "MM")))
								{
									case 1:
									case 2:
									case 3:
										{
											dtStart = DateAndTime.DateValue("01/01/" + FCConvert.ToString(DateTime.Today.Year));
											dtEnd = DateAndTime.DateValue("03/31/" + FCConvert.ToString(DateTime.Today.Year));
											break;
										}
									case 4:
									case 5:
									case 6:
										{
											dtStart = DateAndTime.DateValue("04/01/" + FCConvert.ToString(DateTime.Today.Year));
											dtEnd = DateAndTime.DateValue("06/30/" + FCConvert.ToString(DateTime.Today.Year));
											break;
										}
									case 7:
									case 8:
									case 9:
										{
											dtStart = DateAndTime.DateValue("07/01/" + FCConvert.ToString(DateTime.Today.Year));
											dtEnd = DateAndTime.DateValue("09/30/" + FCConvert.ToString(DateTime.Today.Year));
											break;
										}
									case 10:
									case 11:
									case 12:
										{
											dtStart = DateAndTime.DateValue("10/01/" + FCConvert.ToString(DateTime.Today.Year));
											dtEnd = DateAndTime.DateValue("12/31/" + FCConvert.ToString(DateTime.Today.Year));
											break;
										}
								}
								//end switch
								break;
							}
						case 2:
							{
								// Monthly
								dtStart = DateAndTime.DateAdd("D", 1, DateAndTime.DateAdd("M", -1, DateTime.Today));
								dtEnd = DateAndTime.DateAdd("D", -1, DateAndTime.DateAdd("M", 1, DateTime.Today));
								break;
							}
						case 3:
							{
								// Yearly
								dtStart = DateAndTime.DateAdd("D", 1, DateAndTime.DateAdd("Y", -1, DateTime.Today));
								dtEnd = DateAndTime.DateAdd("D", -1, DateAndTime.DateAdd("Y", 1, DateTime.Today));
								break;
							}
					}
					//end switch
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Period Dates", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool IsBillBookCleared(int lngBook, ref string strStatus)
		{
			bool IsBillBookCleared = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool blnResult = false;
				clsDRWrapper rsBook = new clsDRWrapper();
				rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(lngBook), modExtraModules.strUTDatabase);
				if (rsBook.RecordCount() > 0)
				{
					blnResult = FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")) == "CE";
					if (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "C")
					{
						strStatus = "Cleared";
					}
					else if (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "D")
					{
						strStatus = "Data Entry";
					}
					else if (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "X")
					{
						strStatus = "Calculated";
						// kk03302015 trouts-11  Change Billed Edit to Calc and Edit
					}
					else if (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "E")
					{
						strStatus = "Calc and Edit";
					}
					else if (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "W")
					{
						// kk trouts-6 03012013  Change Water to Stormwater for Bangor
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							strStatus = "Stormwater Billed";
						}
						else
						{
							strStatus = "Water Billed";
						}
					}
					else if (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "S")
					{
						strStatus = "Sewer Billed";
					}
					else if (Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1) == "B")
					{
						strStatus = "Billed";
					}
				}
				else
				{
					blnResult = false;
				}
				IsBillBookCleared = blnResult;
				return IsBillBookCleared;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Determining Book Status", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return IsBillBookCleared;
		}
	}
}
