﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptHighestConsumption.
	/// </summary>
	public partial class rptHighestConsumption : BaseSectionReport
	{
		public rptHighestConsumption()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Highest Consumption";
		}

		public static rptHighestConsumption InstancePtr
		{
			get
			{
				return (rptHighestConsumption)Sys.GetInstance(typeof(rptHighestConsumption));
			}
		}

		protected rptHighestConsumption _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptHighestConsumption	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/07/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/01/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsTemp = new clsDRWrapper();
		int lngCount;
		string strAccount = "";
		public DateTime dtStartDate;
		public DateTime dtEndDate;
		public int lngTop;
		int lngMax;
		int lngAccount;
		int lngSum;
		int intCount;
		int intTotalRecords;
		Dictionary<object, object> objDictionary = new Dictionary<object, object>();
		Dictionary<object, object> objTop = new Dictionary<object, object>();
		public bool blnUseRange;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = (intCount == intTotalRecords);
			if (eArgs.EOF)
			{
				return;
			}
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intRecCount;
				if (blnUseRange)
				{
					if (dtStartDate.ToOADate() == dtEndDate.ToOADate())
					{
						lblCriteria.Text = "Bill Date : " + Strings.Format(dtStartDate, "MM/dd/yyyy");
					}
					else
					{
						lblCriteria.Text = "Bill Date : " + Strings.Format(dtStartDate, "MM/dd/yyyy") + " to " + Strings.Format(dtEndDate, "MM/dd/yyyy");
					}
				}
				else
				{
					lblCriteria.Text = "Bill Date : All Dates";
				}
				lblCriteria.Text = lblCriteria.Text + "\r\n" + "Report showing highest consumption for the Top " + FCConvert.ToString(lngTop) + " accounts.";
				if (blnUseRange)
				{
					rsData.OpenRecordset("SELECT * FROM MeterConsumption WHERE (BillDate >= '" + FCConvert.ToString(dtStartDate) + "' AND BillDate <= '" + FCConvert.ToString(dtEndDate) + "') ORDER BY BillDate", modExtraModules.strUTDatabase);
				}
				else
				{
					rsData.OpenRecordset("SELECT * FROM MeterConsumption ORDER BY BillDate", modExtraModules.strUTDatabase);
				}
				if (rsData.EndOfFile())
				{
					MessageBox.Show("No consumption history found.", "No Consumption History", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
					return;
				}
				if (blnUseRange)
				{
					rsTemp.OpenRecordset("SELECT Max(SumofConsumption) as HighestConsumption FROM (SELECT MeterConsumption.AccountKey, Sum(MeterConsumption.Consumption) AS SumOfConsumption " + "From MeterConsumption GROUP BY MeterConsumption.AccountKey, MeterConsumption.BillDate " + "HAVING (((MeterConsumption.BillDate) Between '" + FCConvert.ToString(dtStartDate) + "' And '" + FCConvert.ToString(dtEndDate) + "'))) AS qTmp");
					// TODO Get_Fields: Field [HighestConsumption] not found!! (maybe it is an alias?)
					lngMax = FCConvert.ToInt32(rsTemp.Get_Fields("HighestConsumption"));
				}
				else
				{
					rsTemp.OpenRecordset("SELECT Max(SumofConsumption) as HighestConsumption FROM (SELECT MeterConsumption.AccountKey, Sum(MeterConsumption.Consumption) AS SumOfConsumption " + "From MeterConsumption GROUP BY MeterConsumption.AccountKey, MeterConsumption.BillDate) AS qTmp");
					// TODO Get_Fields: Field [HighestConsumption] not found!! (maybe it is an alias?)
					lngMax = FCConvert.ToInt32(rsTemp.Get_Fields("HighestConsumption"));
				}
				// Loop through all records and add records to the Top Dictionary
				if (blnUseRange)
				{
					rsTemp.OpenRecordset("SELECT MeterConsumption.AccountKey, Sum(MeterConsumption.Consumption) AS SumOfConsumption " + "FROM MeterConsumption WHERE MeterConsumption.BillDate Between '" + FCConvert.ToString(dtStartDate) + "' And '" + FCConvert.ToString(dtEndDate) + "' " + "GROUP BY MeterConsumption.AccountKey " + "ORDER BY Sum(MeterConsumption.Consumption) DESC", modExtraModules.strUTDatabase);
				}
				else
				{
					rsTemp.OpenRecordset("SELECT MeterConsumption.AccountKey, Sum(MeterConsumption.Consumption) AS SumOfConsumption " + "FROM MeterConsumption GROUP BY MeterConsumption.AccountKey " + "ORDER BY Sum(MeterConsumption.Consumption) DESC", modExtraModules.strUTDatabase);
				}
				rsTemp.MoveFirst();
				for (intRecCount = 1; intRecCount <= lngTop; intRecCount++)
				{
					if (!rsTemp.EndOfFile())
					{
						// TODO Get_Fields: Field [SumOfConsumption] not found!! (maybe it is an alias?)
						objTop.Add(intRecCount, rsTemp.Get_Fields_Int32("AccountKey") + "|" + rsTemp.Get_Fields("SumOfConsumption"));
						rsTemp.MoveNext();
					}
				}
				// intRecCount
				intTotalRecords = objTop.Count;
				intCount = 1;
				if (blnUseRange)
				{
					rsData.OpenRecordset("SELECT MeterConsumption.*, Master.StreetNumber, Master.StreetName, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip, pBill2.FullNameLF AS Name2 FROM MeterConsumption INNER JOIN Master ON MeterConsumption.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID WHERE (BillDate >= '" + FCConvert.ToString(dtStartDate) + "' AND BillDate <= '" + FCConvert.ToString(dtEndDate) + "') ORDER BY MeterConsumption.AccountKey desc", modExtraModules.strUTDatabase);
				}
				else
				{
					rsData.OpenRecordset("SELECT MeterConsumption.*, Master.StreetNumber, Master.StreetName, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip, pBill2.FullNameLF AS Name2 FROM MeterConsumption INNER JOIN Master ON MeterConsumption.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID ORDER BY MeterConsumption.AccountKey desc", modExtraModules.strUTDatabase);
				}
				rsData.MoveFirst();
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				FillHeaderLabels();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lngAcct As int	OnWrite(string)
			int lngAcct = 0;
			// vbPorter upgrade warning: lngSum As int	OnWrite(string)
			int lngSum = 0;
			// vbPorter upgrade warning: arySplit As Variant --> As string()	OnReadFCConvert.ToInt32(
			string[] arySplit = null;
			int varKey;
			if (objTop.ContainsKey(intCount))
			{
				arySplit = Strings.Split(FCConvert.ToString(objTop[intCount]), "|", -1, CompareConstants.vbBinaryCompare);
				lngAcct = FCConvert.ToInt32(arySplit[0]);
				lngSum = FCConvert.ToInt32(arySplit[1]);
				BindFields(lngAcct, lngSum);
			}
		}
		// vbPorter upgrade warning: varKey As Variant --> As int
		private void BindFields(int varKey, int lngSum)
		{
			string strAddress = "";
			string strAddress2 = "";
			string strAddress3 = "";
			string strAddress4 = "";
			string strLocation = "";
			string strName = "";
			rsData.FindFirstRecord("AccountKey", varKey);
			if (!rsData.EndOfFile())
			{
				if (FCConvert.ToString(rsData.Get_Fields_String("Name2")) != "")
				{
					strName = rsData.Get_Fields_String("Name") + " & " + rsData.Get_Fields_String("Name2");
				}
				else
				{
					strName = FCConvert.ToString(rsData.Get_Fields_String("Name"));
				}
				strAddress = FCConvert.ToString(rsData.Get_Fields_String("BAddress1"));
				strAddress2 = FCConvert.ToString(rsData.Get_Fields_String("BAddress2"));
				strAddress3 = FCConvert.ToString(rsData.Get_Fields_String("BAddress3"));
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BCity"))) != "")
				{
					strAddress4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BCity"))) + ", " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BState"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BZip")));
				}
				else
				{
					strAddress4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BState"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("BZip")));
				}
				// If Trim(.Fields("BZip4")) <> "" Then
				// strAddress4 = strAddress4 & " " & Trim(.Fields("BZip4"))
				// End If
				if (strAddress == "")
				{
					if (strAddress2 == "")
					{
						if (strAddress3 == "")
						{
							if (strAddress4 == "")
							{
								// Nothing to Do - All Empty
							}
							else
							{
								strAddress = strAddress4;
								strAddress4 = "";
							}
						}
						else
						{
							strAddress = strAddress3;
							strAddress3 = "";
							if (strAddress4 == "")
							{
								// Nothing
							}
							else
							{
								strAddress2 = strAddress4;
								strAddress4 = "";
							}
						}
					}
					else
					{
						strAddress = strAddress2;
						if (strAddress3 == "")
						{
							if (strAddress4 == "")
							{
								strAddress2 = "";
							}
							else
							{
								strAddress2 = strAddress4;
								strAddress4 = "";
							}
						}
						else
						{
							if (strAddress4 == "")
							{
								strAddress2 = strAddress3;
								strAddress3 = "";
							}
							else
							{
								strAddress2 = strAddress3;
								strAddress3 = strAddress4;
								strAddress4 = "";
							}
						}
					}
				}
				else
				{
					if (strAddress2 == "")
					{
						if (strAddress3 == "")
						{
							if (strAddress4 == "")
							{
								// Nothing to Do - All Empty
							}
							else
							{
								strAddress2 = strAddress4;
								strAddress4 = "";
							}
						}
						else
						{
							strAddress2 = strAddress3;
							if (strAddress4 == "")
							{
								// Nothing
							}
							else
							{
								strAddress3 = strAddress4;
								strAddress4 = "";
							}
						}
					}
					else
					{
						strAddress2 = strAddress2;
						if (strAddress3 == "")
						{
							if (strAddress4 == "")
							{
								strAddress3 = "";
							}
							else
							{
								strAddress3 = strAddress4;
								strAddress4 = "";
							}
						}
						else
						{
							if (strAddress4 == "")
							{
								strAddress3 = strAddress3;
								strAddress4 = "";
							}
							else
							{
								strAddress3 = strAddress3;
								strAddress4 = strAddress4;
							}
						}
					}
				}
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) == "")
				{
					strLocation = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
					strLocation = Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) + " " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
				}
				fldAccount.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey")));
				fldName.Text = strName;
				fldAddress.Text = strAddress;
				fldAddress2.Text = strAddress2;
				fldAddress3.Text = strAddress3;
				fldAddress4.Text = strAddress4;
				fldLocation.Text = strLocation;
				fldConsumption.Text = lngSum.ToString();
				lngCount += lngSum;
				intCount += 1;
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotal.Text = lngCount.ToString();
		}

		private void rptHighestConsumption_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptHighestConsumption properties;
			//rptHighestConsumption.Caption	= "Highest Consumption";
			//rptHighestConsumption.Icon	= "rptHighestConsumption.dsx":0000";
			//rptHighestConsumption.Left	= 0;
			//rptHighestConsumption.Top	= 0;
			//rptHighestConsumption.Width	= 13515;
			//rptHighestConsumption.Height	= 8460;
			//rptHighestConsumption.WindowState	= 2;
			//rptHighestConsumption.SectionData	= "rptHighestConsumption.dsx":058A;
			//End Unmaped Properties
		}
	}
}
