﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for sarLienEditMortHolders.
	/// </summary>
	partial class sarLienEditMortHolders
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sarLienEditMortHolders));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldMHName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMHAddr1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMHAddr2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMHAddr3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMHAddr4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblExtraLines = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.fldMHName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMHAddr1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMHAddr2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMHAddr3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMHAddr4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExtraLines)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.ColumnCount = 2;
			this.Detail.ColumnDirection = GrapeCity.ActiveReports.SectionReportModel.ColumnDirection.AcrossDown;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldMHName,
				this.fldMHAddr1,
				this.fldMHAddr2,
				this.fldMHAddr3,
				this.fldMHAddr4,
				this.fldBookPage,
				this.lblExtraLines
			});
			this.Detail.Height = 0.65625F;
			this.Detail.Name = "Detail";
			// 
			// fldMHName
			// 
			this.fldMHName.Height = 0.1875F;
			this.fldMHName.Left = 0F;
			this.fldMHName.MultiLine = false;
			this.fldMHName.Name = "fldMHName";
			this.fldMHName.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fldMHName.Text = null;
			this.fldMHName.Top = 0F;
			this.fldMHName.Width = 3.125F;
			// 
			// fldMHAddr1
			// 
			this.fldMHAddr1.Height = 0.1875F;
			this.fldMHAddr1.Left = 0F;
			this.fldMHAddr1.MultiLine = false;
			this.fldMHAddr1.Name = "fldMHAddr1";
			this.fldMHAddr1.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fldMHAddr1.Text = null;
			this.fldMHAddr1.Top = 0.125F;
			this.fldMHAddr1.Width = 3.125F;
			// 
			// fldMHAddr2
			// 
			this.fldMHAddr2.Height = 0.1875F;
			this.fldMHAddr2.Left = 0F;
			this.fldMHAddr2.MultiLine = false;
			this.fldMHAddr2.Name = "fldMHAddr2";
			this.fldMHAddr2.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fldMHAddr2.Text = null;
			this.fldMHAddr2.Top = 0.25F;
			this.fldMHAddr2.Width = 3.125F;
			// 
			// fldMHAddr3
			// 
			this.fldMHAddr3.Height = 0.1875F;
			this.fldMHAddr3.Left = 0F;
			this.fldMHAddr3.MultiLine = false;
			this.fldMHAddr3.Name = "fldMHAddr3";
			this.fldMHAddr3.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fldMHAddr3.Text = null;
			this.fldMHAddr3.Top = 0.375F;
			this.fldMHAddr3.Width = 3.125F;
			// 
			// fldMHAddr4
			// 
			this.fldMHAddr4.Height = 0.1875F;
			this.fldMHAddr4.Left = 0F;
			this.fldMHAddr4.MultiLine = false;
			this.fldMHAddr4.Name = "fldMHAddr4";
			this.fldMHAddr4.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fldMHAddr4.Text = null;
			this.fldMHAddr4.Top = 0.5F;
			this.fldMHAddr4.Width = 3.125F;
			// 
			// fldBookPage
			// 
			this.fldBookPage.Height = 0.1875F;
			this.fldBookPage.Left = 0F;
			this.fldBookPage.MultiLine = false;
			this.fldBookPage.Name = "fldBookPage";
			this.fldBookPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt";
			this.fldBookPage.Text = null;
			this.fldBookPage.Top = 0.625F;
			this.fldBookPage.Width = 3.125F;
			// 
			// lblExtraLines
			// 
			this.lblExtraLines.Height = 0.125F;
			this.lblExtraLines.HyperLink = null;
			this.lblExtraLines.Left = 0.625F;
			this.lblExtraLines.Name = "lblExtraLines";
			this.lblExtraLines.Style = "";
			this.lblExtraLines.Text = "              ";
			this.lblExtraLines.Top = 0.8125F;
			this.lblExtraLines.Width = 1.25F;
			// 
			// sarLienEditMortHolders
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 6.25F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.Detail);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldMHName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMHAddr1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMHAddr2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMHAddr3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMHAddr4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblExtraLines)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMHName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMHAddr1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMHAddr2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMHAddr3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMHAddr4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblExtraLines;
	}
}
