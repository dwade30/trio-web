﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Collections.Generic;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDisconnectEditReport.
	/// </summary>
	public partial class rptDisconnectEditReport : BaseSectionReport
	{
		public rptDisconnectEditReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Disconnect Edit Report";
		}

		public static rptDisconnectEditReport InstancePtr
		{
			get
			{
				return (rptDisconnectEditReport)Sys.GetInstance(typeof(rptDisconnectEditReport));
			}
		}

		protected rptDisconnectEditReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsInfo.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptDisconnectEditReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               11/26/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/09/2006              *
		// ********************************************************
		clsDRWrapper rsInfo = new clsDRWrapper();
		bool blnFirstRecord;
		int intArrayMax;
		// How many books there are in the array
		int[] BookArray = null;
		// Array holding book numbers of selected books
		string strBooksSelection;
		// A - All Books   S - Selected Books
		string strSQL = "";
		// SQL statement to get information we need for report
		int intTotal;
		// Total of how many meters were reported on
		// vbPorter upgrade warning: curBookWater As Decimal	OnWrite(short, Decimal)
		Decimal curBookWater;
		// vbPorter upgrade warning: curBookSewer As Decimal	OnWrite(short, Decimal)
		Decimal curBookSewer;
		int intTotalNC;
		// Total of how many meters were reported on No Charge accounts
		// vbPorter upgrade warning: curBookWaterNC As Decimal	OnWrite(short, Decimal)
		Decimal curBookWaterNC;
		// vbPorter upgrade warning: curBookSewerNC As Decimal	OnWrite(short, Decimal)
		Decimal curBookSewerNC;
		int intFinalTotal;
		// Grand Total of how many meters were reported on
		// vbPorter upgrade warning: curFinalWater As Decimal	OnWrite(short, Decimal)
		Decimal curFinalWater;
		// vbPorter upgrade warning: curFinalSewer As Decimal	OnWrite(short, Decimal)
		Decimal curFinalSewer;
		int intFinalTotalNC;
		// Grand Total of how many meters were reported on No Charge accounts
		// vbPorter upgrade warning: curFinalWaterNC As Decimal	OnWrite(short, Decimal)
		Decimal curFinalWaterNC;
		// vbPorter upgrade warning: curFinalSewerNC As Decimal	OnWrite(short, Decimal)
		Decimal curFinalSewerNC;
		int lngNewestRateKey;
		DateTime dtDate;
		double dblMinimum;
		bool boolShowSrvcLoc;
		bool boolExcludeNoBill;
		Dictionary<object, object> objDict = new Dictionary<object, object>();

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("Binder");
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				rsInfo.MoveNext();
				eArgs.EOF = rsInfo.EndOfFile();
			}
			if (!eArgs.EOF)
			{
				this.Fields["Binder"].Value = rsInfo.Get_Fields_Int32("BookNumber");
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			objDict = null;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			blnFirstRecord = true;
			intTotal = 0;
			curBookSewer = 0;
			curBookWater = 0;
			intTotalNC = 0;
			curBookSewerNC = 0;
			curBookWaterNC = 0;
			intFinalTotal = 0;
			curFinalSewer = 0;
			curFinalWater = 0;
			intFinalTotalNC = 0;
			curFinalSewerNC = 0;
			curFinalWaterNC = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsAccountInfo = new clsDRWrapper();
			double dblSewerTotal = 0;
			double dblWaterTotal = 0;
			rsAccountInfo.OpenRecordset("SELECT Master.ID, AccountNumber, pBill.FullNameLF AS Name, pBill2.FullNameLF AS Name2, Master.StreetName, Master.StreetNumber, Master.Apt, Master.NoBill " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID LEFT JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill2 ON pBill2.ID = Master.SecondBillingPartyID WHERE Master.ID = " + rsInfo.Get_Fields_Int32("AccountKey"));
			
			if (objDict.ContainsKey(rsInfo.Get_Fields_Int32("AccountKey")))
			{
				// Do Nothing - It Has Already Been Processed
			}
			else
			{
				objDict.Add(rsInfo.Get_Fields_Int32("AccountKey"), rsInfo.Get_Fields_Int32("AccountKey"));
				dblSewerTotal = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsAccountInfo.Get_Fields_Int32("ID"), false, ref dtDate, lngNewestRateKey), 2);
				dblWaterTotal = FCUtils.Round(modUTCalculations.CalculateAccountUTTotal_1(rsAccountInfo.Get_Fields_Int32("ID"), true, ref dtDate, lngNewestRateKey), 2);
				// MAL@20080715: Add check for minimum amount in water balance
				// Tracker Reference: 14569
				if (dblWaterTotal >= dblMinimum && dblWaterTotal > 0)
				{
					// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
					fldSequenceNumber.Text = Strings.Format(rsInfo.Get_Fields("Sequence"), "0000");
					if (rsAccountInfo.EndOfFile() != true)
					{
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						fldAccountNumber.Text = Strings.Format(rsAccountInfo.Get_Fields("AccountNumber"), "00000");
						fldName.Text = rsAccountInfo.Get_Fields_String("Name");
						if (FCConvert.ToString(rsAccountInfo.Get_Fields_String("Name2")) != "")
						{
							fldName.Text = fldName.Text + "\r\n" + rsAccountInfo.Get_Fields_String("Name2");
						}
						else
						{
							fldName.Text = fldName.Text;
						}
					}
					else
					{
						fldAccountNumber.Text = "00000";
						fldName.Text = "UNKNOWN";
					}
					// kk 12-18-2012 trout-804  Option to show service location on disconnect edit report
					if (boolShowSrvcLoc)
					{
						// kk09262014 trout-804  Reformat to get better export to excel
						// fldSequenceNumber.Left = 0
						// fldAccountNumber.Left = 1000
						// fldName.Left = 2000
						// fldSewer.Left = 6980
						// fldWater.Left = 8240
						// fldTotal.Left = 9500
						fldSrvcStreet.Text = Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("StreetName")));
						fldSrvcStreet.Height = 270 / 1440f;
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						fldSrvcNumber.Text = rsAccountInfo.Get_Fields("StreetNumber") + Strings.Trim(FCConvert.ToString(rsAccountInfo.Get_Fields_String("Apt")));
						fldSrvcNumber.Height = 270 / 1440f;
					}
					else
					{
						fldSrvcStreet.Text = "";
						fldSrvcStreet.Height = 0;
						fldSrvcStreet.Visible = false;
						fldSrvcNumber.Text = "";
						fldSrvcNumber.Height = 0;
						fldSrvcNumber.Visible = false;
					}
					fldWater.Text = Strings.Format(dblWaterTotal, "#,##0.00");
					// rsInfo.Fields("WaterTotal")
					fldSewer.Text = Strings.Format(dblSewerTotal, "#,##0.00");
					// rsInfo.Fields("SewerTotal")
					fldTotal.Text = Strings.Format(dblWaterTotal + dblSewerTotal, "#,##0.00");
					// kk09022015 moved down
					// curBookWater = curBookWater + dblWaterTotal 'rsInfo.Fields("WaterTotal")
					// curBookSewer = curBookSewer + dblSewerTotal 'rsInfo.Fields("SewerTotal")
					fldSequenceNumber.Height = 270 / 1440f;
					fldAccountNumber.Height = 270 / 1440f;
					fldName.Height = 270 / 1440f;
					fldWater.Height = 270 / 1440f;
					fldSewer.Height = 270 / 1440f;
					fldTotal.Height = 270 / 1440f;
					// kk09022015 trouts-147
					if (boolExcludeNoBill && rsAccountInfo.Get_Fields_Boolean("NoBill"))
					{
						// If the account is marked for NoBill flag it on the report
						// Keep track of the excluded accounts separate from the running total
						fldAccountNumber.Text = fldAccountNumber.Text + " (NC)";
						curBookWaterNC += FCConvert.ToDecimal(dblWaterTotal);
						curBookSewerNC += FCConvert.ToDecimal(dblSewerTotal);
						intTotalNC += 1;
					}
					else
					{
						curBookWater += FCConvert.ToDecimal(dblWaterTotal);
						// rsInfo.Fields("WaterTotal")
						curBookSewer += FCConvert.ToDecimal(dblSewerTotal);
						// rsInfo.Fields("SewerTotal")
						intTotal += 1;
					}
				}
				else
				{
					// Skip This Record
					fldSequenceNumber.Text = "";
					fldAccountNumber.Text = "";
					fldName.Text = "";
					fldWater.Text = "";
					fldSewer.Text = "";
					fldTotal.Text = "";
					fldSequenceNumber.Height = 0;
					fldAccountNumber.Height = 0;
					fldName.Height = 0;
					fldWater.Height = 0;
					fldSewer.Height = 0;
					fldTotal.Height = 0;
					fldSrvcStreet.Text = "";
					fldSrvcStreet.Height = 0;
					fldSrvcNumber.Text = "";
					fldSrvcNumber.Height = 0;
				}
			}
			rsAccountInfo.Dispose();
        }

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// kk 12-18-2012 trout-804  Option to show service location on disconnect edit report
			if (boolShowSrvcLoc)
			{
				LineGroup1.X2 = 10755 / 1440f;
				fldBookSewer.Left = 6980 / 1440f;
				fldBookWater.Left = 8240 / 1440f;
				fldBookTotal.Left = 9500 / 1440f;
			}
			fldTotalCount.Text = intTotal.ToString();
			fldBookSewer.Text = Strings.Format(curBookSewer, "#,##0.00");
			fldBookWater.Text = Strings.Format(curBookWater, "#,##0.00");
			fldBookTotal.Text = Strings.Format(curBookSewer + curBookWater, "#,##0.00");
			fldBookCountNC.Text = intTotalNC.ToString();
			fldBookSewerNC.Text = Strings.Format(curBookSewerNC, "#,##0.00");
			fldBookWaterNC.Text = Strings.Format(curBookWaterNC, "#,##0.00");
			fldBookTotalNC.Text = Strings.Format(curBookSewerNC + curBookWaterNC, "#,##0.00");
			if (boolExcludeNoBill && intTotalNC > 0)
			{
				lblBookCountNC.Visible = true;
				lblBookCountNC.Top = 425 / 1440f;
				fldBookCountNC.Visible = true;
				fldBookCountNC.Top = 425 / 1440f;
				lblBookNC.Visible = true;
				lblBookNC.Top = 425 / 1440f;
				fldBookSewerNC.Visible = true;
				fldBookSewerNC.Top = 425 / 1440f;
				fldBookWaterNC.Visible = true;
				fldBookWaterNC.Top = 425 / 1440f;
				fldBookTotalNC.Visible = true;
				fldBookTotalNC.Top = 425 / 1440f;
			}
			else
			{
				lblBookCountNC.Visible = false;
				fldBookCountNC.Visible = false;
				lblBookNC.Visible = false;
				fldBookSewerNC.Visible = false;
				fldBookWaterNC.Visible = false;
				fldBookTotalNC.Visible = false;
			}
			curFinalSewer += curBookSewer;
			curFinalWater += curBookWater;
			intFinalTotal += intTotal;
			curFinalSewerNC += curBookSewerNC;
			curFinalWaterNC += curBookWaterNC;
			intFinalTotalNC += intTotalNC;
			intTotal = 0;
			curBookSewer = 0;
			curBookWater = 0;
			intTotalNC = 0;
			curBookSewerNC = 0;
			curBookWaterNC = 0;
		}

		private void GroupFooter2_Format(object sender, EventArgs e)
		{
			// kk 12-18-2012 trout-804  Option to show service location on disconnect edit report
			if (boolShowSrvcLoc)
			{
				// kk09262014 trout-804  Reformat for better export to excel
				// LineGroup2.X1 = 10755
				// fldFinalSewer.Left = 6980
				// fldFinalWater.Left = 8240
				// fldFinalTotal.Left = 9500
			}
			fldFinalCount.Text = intFinalTotal.ToString();
			fldFinalSewer.Text = Strings.Format(curFinalSewer, "#,##0.00");
			fldFinalWater.Text = Strings.Format(curFinalWater, "#,##0.00");
			fldFinalTotal.Text = Strings.Format(curFinalSewer + curFinalWater, "#,##0.00");
			fldFinalCountNC.Text = intFinalTotalNC.ToString();
			fldFinalSewerNC.Text = Strings.Format(curFinalSewerNC, "#,##0.00");
			fldFinalWaterNC.Text = Strings.Format(curFinalWaterNC, "#,##0.00");
			fldFinalTotalNC.Text = Strings.Format(curFinalSewerNC + curFinalWaterNC, "#,##0.00");
			if (boolExcludeNoBill && intFinalTotalNC > 0)
			{
				lblFinalCountNC.Visible = true;
				lblFinalCountNC.Top = 425 / 1440f;
				fldFinalCountNC.Visible = true;
				fldFinalCountNC.Top = 425 / 1440f;
				lblFinalNC.Visible = true;
				lblFinalNC.Top = 425 / 1440f;
				fldFinalSewerNC.Visible = true;
				fldFinalSewerNC.Top = 425 / 1440f;
				fldFinalWaterNC.Visible = true;
				fldFinalWaterNC.Top = 425 / 1440f;
				fldFinalTotalNC.Visible = true;
				fldFinalTotalNC.Top = 425 / 1440f;
			}
			else
			{
				lblFinalCountNC.Visible = false;
				fldFinalCountNC.Visible = false;
				lblFinalNC.Visible = false;
				fldFinalSewerNC.Visible = false;
				fldFinalWaterNC.Visible = false;
				fldFinalTotalNC.Visible = false;
			}
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			fldBook.Text = "Book " + rsInfo.Get_Fields_Int32("BookNumber");
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// kk 12-18-2012 trout-804  Option to show service location on disconnect edit report
			if (boolShowSrvcLoc)
			{
				// kk09262014 trout-804  Reformat for better export to excel
				// lblSeq.Left = 0
				// lblAcct.Left = 1000
				// lblName.Left = 2000
				// lblSewer.Left = 6980
				// lblWater.Left = 8240
				// lblTotal.Left = 9500
				lblSrvcLoc.Visible = true;
			}
			else
			{
				lblSrvcLoc.Text = "";
				lblSrvcLoc.Height = 0;
			}
			Label4.Text = "Page " + this.PageNumber;
		}
		// vbPorter upgrade warning: intCT As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: lngPassNewestRateKey As int	OnWriteFCConvert.ToDouble(
		public void Init(string strBooks, DateTime datNoticeDate, DateTime datShutOffDate, short intCT, ref int[] PassBookArray, int lngPassNewestRateKey, double dblPassMinimum)
		{
			int counter;
			string strRKSQL = "";
			dtDate = datNoticeDate;
			strBooksSelection = strBooks;
			BookArray = PassBookArray;
			intArrayMax = intCT;
			lngNewestRateKey = lngPassNewestRateKey;
			// MAL@20080715: Add minimum amount
			// Tracker Reference: 14569
			dblMinimum = dblPassMinimum;
			lblDate.Text = "Notice Date:  " + Strings.Format(datNoticeDate, "MM/dd/yyyy") + "          Shutoff Date:  " + Strings.Format(datShutOffDate, "MM/dd/yyyy");
			if (strBooksSelection == "A")
			{
				// set label saying so
				lblBooks.Text = "All Books";
			}
			else
			{
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					lblBooks.Text = "Book " + FCConvert.ToString(BookArray[1]);
					strSQL = " = " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					lblBooks.Text = "Books ";
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strSQL += FCConvert.ToString(BookArray[counter]) + ",";
						lblBooks.Text = lblBooks.Text + FCConvert.ToString(BookArray[counter]) + ",";
					}
					strSQL = "IN (" + Strings.Left(strSQL, strSQL.Length - 1) + ")";
					lblBooks.Text = Strings.Left(lblBooks.Text, lblBooks.Text.Length - 1);
				}
				lblBooks.Text = lblBooks.Text + "    Starting Seq# " + FCConvert.ToString(Conversion.Val(frmSetupDisconnectEditReport.InstancePtr.txtSequenceNumber.Text));
			}
			// kk 12182012 trout-804  Add option to show service location on the edit list
			boolShowSrvcLoc = FCConvert.CBool(frmSetupDisconnectEditReport.InstancePtr.chkShowSrvcLoc.CheckState == Wisej.Web.CheckState.Checked);
			// kk09022015 trouts-147  Add option to exclude No Charge accounts
			boolExcludeNoBill = FCConvert.CBool(frmSetupDisconnectEditReport.InstancePtr.chkExcludeNoBill.CheckState == Wisej.Web.CheckState.Checked);
			if (lngNewestRateKey > 0)
			{
				strRKSQL = " AND BillingRateKey <= " + FCConvert.ToString(lngNewestRateKey);
			}
			if (strBooksSelection == "A")
			{
				// rsInfo.OpenRecordset "SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed > Bill.WTaxPaid) OR (Bill.WIntOwed > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded > Bill.WCostPaid)) AND WLienRecordNumber = 0 " & strRKSQL & " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey ORDER BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey"
				// MAL@20070912: Call Reference:312488 ; Corrected SQL statement to better grab balances greater than zero (same as Disconnect Notices)
				// rsInfo.OpenRecordset "SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey WHERE ((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) > 0 AND WLienRecordNumber = 0 " & strRKSQL & " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey ORDER BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey"
				rsInfo.OpenRecordset("SELECT MeterTable.Sequence as Sequence " + ", MeterTable.BookNumber as BookNumber " + ", Bill.AccountKey as AccountKey " + ", SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal " + ", SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable " + "INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) Or (Bill.WTaxOwed <> Bill.WTaxPaid) Or (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) Or (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) " + "AND WLienRecordNumber = 0 " + strRKSQL + " AND ((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) > 0 " + "GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey " + "ORDER BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey");
			}
			else
			{
				// rsInfo.OpenRecordset "SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed > Bill.WTaxPaid) OR (Bill.WIntOwed > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded > Bill.WCostPaid)) AND WLienRecordNumber = 0  " & strRKSQL
				// & " AND Sequence > " & Val(frmSetupDisconnectEditReport.txtSequenceNumber.Text) & " AND MeterTable.BookNumber " & strSQL & " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey ORDER BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey"
				// MAL@20070831: Added the Sequence # back into the WHERE clause
				rsInfo.OpenRecordset("SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, " + "SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, " + "SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal " + "FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.ID " + "WHERE ((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) > 0 " + "AND WLienRecordNumber = 0 " + " AND MeterTable.BookNumber " + strSQL + strRKSQL + " AND MeterTable.Sequence >= " + FCConvert.ToString(Conversion.Val(frmSetupDisconnectEditReport.InstancePtr.txtSequenceNumber.Text)) + " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey ORDER BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey");
				// rsInfo.OpenRecordset "SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey WHERE ((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) > 0 AND WLienRecordNumber = 0  " & strRKSQL
				// & " AND Sequence > " & Val(frmSetupDisconnectEditReport.txtSequenceNumber.Text) & " AND MeterTable.BookNumber " & strSQL & " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey ORDER BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey"
			}
			// if we found some records show report otherwise pop up message and end report
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				frmReportViewer.InstancePtr.Init(this);
			}
			else
			{
				MessageBox.Show("No Information Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
			}
		}

		
	}
}
