﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptLienFeeIncr.
	/// </summary>
	partial class rptLienFeeIncr
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptLienFeeIncr));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldUserID = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldYear = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldProcCnt = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalIncrease = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUserID)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldProcCnt)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalIncrease)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccount,
				this.fldName,
				this.fldYear
			});
			this.Detail.Height = 0.2708333F;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Format += new System.EventHandler(this.ReportHeader_Format);
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label3,
				this.fldProcCnt,
				this.Label4,
				this.fldTotalIncrease
			});
			this.ReportFooter.Height = 0.5520833F;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.fldDate,
				this.Label5,
				this.fldUserID
			});
			this.PageHeader.Height = 0.6770833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.875F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-size: 12pt; font-weight: bold; text-align: center";
			this.Label1.Text = "Lien Discharge Filing Fee Increase";
			this.Label1.Top = 0.0625F;
			this.Label1.Width = 2.875F;
			// 
			// fldDate
			// 
			this.fldDate.Height = 0.1875F;
			this.fldDate.Left = 0F;
			this.fldDate.Name = "fldDate";
			this.fldDate.Style = "text-align: left";
			this.fldDate.Text = "Date";
			this.fldDate.Top = 0.0625F;
			this.fldDate.Width = 0.875F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 0.625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "text-align: center";
			this.Label5.Text = "A $6.00 filing fee increase effective October 8, 2013 was added to the following " + "liens.";
			this.Label5.Top = 0.375F;
			this.Label5.Width = 5.375F;
			// 
			// fldUserID
			// 
			this.fldUserID.Height = 0.1875F;
			this.fldUserID.Left = 6.375F;
			this.fldUserID.Name = "fldUserID";
			this.fldUserID.Style = "text-align: right";
			this.fldUserID.Text = "UserID";
			this.fldUserID.Top = 0.0625F;
			this.fldUserID.Width = 1.125F;
			// 
			// fldAccount
			// 
			this.fldAccount.CanShrink = true;
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0F;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Text = "Account";
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 1F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 2.1875F;
			this.fldName.Name = "fldName";
			this.fldName.Text = "Name";
			this.fldName.Top = 0F;
			this.fldName.Width = 4.8125F;
			// 
			// fldYear
			// 
			this.fldYear.Height = 0.1875F;
			this.fldYear.Left = 1.125F;
			this.fldYear.Name = "fldYear";
			this.fldYear.Text = "Year";
			this.fldYear.Top = 0F;
			this.fldYear.Width = 0.9375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 0.4375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "";
			this.Label3.Text = "Liens Affected:";
			this.Label3.Top = 0.1875F;
			this.Label3.Width = 1.125F;
			// 
			// fldProcCnt
			// 
			this.fldProcCnt.Height = 0.1875F;
			this.fldProcCnt.Left = 1.625F;
			this.fldProcCnt.Name = "fldProcCnt";
			this.fldProcCnt.Text = "ProcCnt";
			this.fldProcCnt.Top = 0.1875F;
			this.fldProcCnt.Width = 0.75F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "";
			this.Label4.Text = "Total Cost Increase:";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.375F;
			// 
			// fldTotalIncrease
			// 
			this.fldTotalIncrease.Height = 0.1875F;
			this.fldTotalIncrease.Left = 4.4375F;
			this.fldTotalIncrease.Name = "fldTotalIncrease";
			this.fldTotalIncrease.Text = "TotalIncrease";
			this.fldTotalIncrease.Top = 0.1875F;
			this.fldTotalIncrease.Width = 1.0625F;
			// 
			// rptLienFeeIncr
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldUserID)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldProcCnt)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalIncrease)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldYear;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldProcCnt;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalIncrease;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUserID;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
