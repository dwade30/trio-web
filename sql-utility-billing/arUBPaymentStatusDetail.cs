﻿using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using SharedApplication.Extensions;
using SharedApplication.UtilityBilling.Receipting;
using TWSharedLibrary;
using Wisej.Web;

namespace Global
{
	/// <summary>
	/// Summary description for arUTAccountDetail.
	/// </summary>
	public partial class arUBPaymentStatusDetail : BaseSectionReport
	{
		// nObj = 1
		//   0	arUTAccountDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/11/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/25/2006              *
		// ********************************************************
		int lngRow;
		int lngPDNumber;
		int lngPDTop;
		double dblPDTotal;
		bool boolPerDiem;
		string strBillText = "";
		bool boolSecondGrid;
		bool boolBothGrids;
		bool boolFirstLineBoth;
		private frmUBPaymentStatus paymentForm = null;

		public arUBPaymentStatusDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static arUBPaymentStatusDetail InstancePtr
		{
			get
			{
				return (arUBPaymentStatusDetail)Sys.GetInstance(typeof(arUBPaymentStatusDetail));
			}
		}

		protected arUBPaymentStatusDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public void SetPaymentForm(frmUBPaymentStatus callingForm)
		{
			paymentForm = callingForm;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (lngRow < 0)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}
		//private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		//{
		//	switch (KeyCode)
		//	{
		//		case Keys.Escape:
		//			{
		//				Close();
		//				break;
		//			}
		//	} //end switch
		//}
		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (paymentForm.WGRID.Visible && paymentForm.WGRID.Rows > 1 && paymentForm.SGRID.Visible && paymentForm.SGRID.Rows > 1)
			{
				boolBothGrids = true;
				boolFirstLineBoth = true;
			}
			else
			{
				boolBothGrids = false;
			}
			SetHeaderString();
			boolPerDiem = false;
			modUTFunctions.Statics.intPerdiemCounter = 0;
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			if (boolBothGrids)
			{
				// kk12222014 trouts-121   If both grids then set up the sewer per diem table
				CreatePerDiemTable_2(false);
			}
			else
			{
				if (paymentForm.ViewModel.VisibleService == UtilityType.Water || paymentForm.ViewModel.VisibleService == UtilityType.All)
				{
					CreatePerDiemTable_2(true);
				}
				else
				{
					CreatePerDiemTable_2(false);
				}
			}
			lngRow = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (boolBothGrids)
			{
				// show both
				if (boolSecondGrid)
				{
					lngRow = FindNextVisibleRow_6(lngRow, true);
					BindFields(ref paymentForm.WGRID);
				}
				else
				{
					lngRow = FindNextVisibleRow_6(lngRow, false);
					BindFields(ref paymentForm.SGRID);
				}
			}
			else if (paymentForm.ViewModel.VisibleService == UtilityType.Water || paymentForm.ViewModel.VisibleService == UtilityType.All)
			{
				lngRow = FindNextVisibleRow_6(lngRow, true);
				BindFields(ref paymentForm.WGRID);
			}
			else
			{
				lngRow = FindNextVisibleRow_6(lngRow, false);
				BindFields(ref paymentForm.SGRID);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}
		// vbPorter upgrade warning: Grid As object	OnWrite(VSFlex7LCtl.VSFlexGrid)
		private void BindFields(ref FCGrid Grid)
		{
			// this will print the row from the grid
			lnTotals.Visible = false;
			// reset the visible property to false so that only a subtotal line will have a horizontal line above it
			lnTotals.LineWeight = 0;
			//subPerDiem = null; // kk01152015 trout-1134  Make sure the per diem stuff is hidden first
			subPerDiem.Report = null;
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.None;
			if (boolBothGrids && boolFirstLineBoth)
			{
				fldYear.Text = "Sewer";
				fldDate.Text = "";
				fldRef.Text = "";
				fldCode.Text = "";
				fldPrincipal.Text = "";
				fldTax.Text = "";
				fldInterest.Text = "";
				fldCosts.Text = "";
				fldTotal.Text = "";
				fldName.Text = "";
				lngRow = 0;
				boolFirstLineBoth = false;
			}
			else if (lngRow >= 0)
			{
				// check for the signal to exit the loop
				// figure out what type of line it is
				if (Grid.RowOutlineLevel(lngRow) == 1)
				{
					// is it a header line
					HideFields_2(false);
					// Year
					fldYear.Text = Grid.TextMatrix(lngRow, paymentForm.lngGRIDColBillNumber);
					// Date
					fldDate.Text = Grid.TextMatrix(lngRow, paymentForm.lngGRIDColDate);
					// Ref
					fldRef.Text = Grid.TextMatrix(lngRow, paymentForm.lngGRIDColRef);
					// fldRef.Text = ""
					// Code
					fldCode.Text = "";
					// Principal
					fldPrincipal.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColPrincipal), "#,##0.00");
					// Tax
					fldTax.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColTax), "#,##0.00");
					// Interest
					fldInterest.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColInterest), "#,##0.00");
					// Costs
					fldCosts.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColCosts), "#,##0.00");
					// Total
					fldTotal.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColTotal), "#,##0.00");
				}
				else if (Strings.Left(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColRef), 10) == "Billed To:")
				{
					// is it a name line
					HideFields_2(true);
					// Name
					fldName.Text = Grid.TextMatrix(lngRow, paymentForm.lngGRIDColRef) + " " + Grid.TextMatrix(lngRow, paymentForm.lngGRIDColPaymentCode);
				}
				else
				{
					if (Strings.Left(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColBillNumber), 14) == "Account Totals")
					{
						// is it the last line?
						HideFields_2(false);
						fldName.Visible = true;
						fldYear.Visible = false;
						fldDate.Visible = false;
						fldRef.Visible = false;
						fldCode.Visible = false;
						fldName.Left = 0;
						fldName.Width = fldPrincipal.Left;
						// show the line above this row and make it darker
						lnTotals.Visible = true;
						lnTotals.LineWeight = 2;
						// total line
						fldName.Text = Grid.TextMatrix(lngRow, paymentForm.lngGRIDColBillNumber);
						// Principal
						fldPrincipal.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColPrincipal), "#,##0.00");
						// Tax
						fldTax.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColTax), "#,##0.00");
						// Interest
						fldInterest.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColInterest), "#,##0.00");
						// Costs
						fldCosts.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColCosts), "#,##0.00");
						// Total
						fldTotal.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColTotal), "#,##0.00");
						// kk12222014 trouts-121  Move this out of Report footer so it shows up for both services
						// But in testing this condition was never run
						if (boolPerDiem)
						{
							subPerDiem.Report = new srptUTPerDiemList();
						}
						else
						{
							subPerDiem.Report = null;
						}
					}
					else if (Grid.TextMatrix(lngRow - 1, paymentForm.lngGRIDColLineCode) == "=")
					{
						// is this line a hidden totals line?
						HideFields_2(true);
						fldName.Text = "";
					}
					else
					{
						HideFields_2(false);
						fldYear.Text = "";
						// Date
						fldDate.Text = Grid.TextMatrix(lngRow, paymentForm.lngGRIDColDate);
						// Ref
						if (Strings.Left(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColRef), 4) == "Lien" && Grid.TextMatrix(lngRow, paymentForm.lngGRIDColRef) == Grid.TextMatrix(lngRow, paymentForm.lngGRIDColDate))
						{
							fldRef.Text = "";
						}
						else
						{
							fldRef.Text = Grid.TextMatrix(lngRow, paymentForm.lngGRIDColRef);
						}
						if (Strings.Left(fldRef.Text, 5) == "Total")
						{
							lnTotals.Visible = true;
							lnTotals.LineWeight = 1;
						}
						else
						{
							lnTotals.Visible = false;
						}
						// Code
						fldCode.Text = Grid.TextMatrix(lngRow, paymentForm.lngGRIDColPaymentCode);
						if (fldCode.Text.Length > 1)
						{
							fldRef.Text = "";
							fldCode.Text = "";
						}
						// kk01152015 trout-1134 Only do this on the last line
						// show the line above this row and make it darker
						// lnTotals.Visible = True
						// lnTotals.LineWeight = 2
						// Principal
						fldPrincipal.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColPrincipal), "#,##0.00");
						// Tax
						fldTax.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColTax), "#,##0.00");
						// Interest
						fldInterest.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColInterest), "#,##0.00");
						// Costs
						fldCosts.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColCosts), "#,##0.00");
						// Total
						fldTotal.Text = Strings.Format(Grid.TextMatrix(lngRow, paymentForm.lngGRIDColTotal), "#,##0.00");
						// kk01152015 trout-1134  This is the totals line and payment details, only show the per diem stuff after the totals line
						if (Grid.RowOutlineLevel(lngRow) == 0)
						{
							// show the line above this row and make it darker
							lnTotals.Visible = true;
							lnTotals.LineWeight = 2;
							lnTotals.X1 = 0;
							// kk12222014 trouts-121  Move this out of Report footer so it shows up for both services
							if (boolPerDiem)
							{
								subPerDiem.Report = new srptUTPerDiemList();
								subPerDiem.Visible = true;
							}
						}
					}
				}
			}
			else
			{
				fldYear.Text = "";
				fldDate.Text = "";
				fldRef.Text = "";
				fldCode.Text = "";
				fldPrincipal.Text = "";
				fldTax.Text = "";
				fldInterest.Text = "";
				fldCosts.Text = "";
				fldTotal.Text = "";
				fldName.Text = "";
				subPerDiem.Report = null;
				// kk12222014 trouts-121   Hide the per diem subreport after the sewer grid is printed
				//subPerDiem.Visible = false;
				boolPerDiem = false;
				if (boolBothGrids && boolSecondGrid == false)
				{
					// if the second grid has not been printed then reset the counter and allow it to count until reset at a negative number
					this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.Before;
					boolSecondGrid = true;
					lngRow = 0;
					fldYear.Text = "Water";
					if (boolBothGrids)
					{
						// kk12222014 trouts-121  Reset the per diem table for water
						CreatePerDiemTable_2(true);
					}
					// kk trouts-6 03012013  Change Water to Stormwater for Bangor
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
					{
						fldYear.Text = "Stormwater";
						fldYear.Width = 1100 / 1440F;
						// kk12222014  Change the width so the line isn't split
					}
				}
			}
		}

		private int FindNextVisibleRow_6(int lngR, bool boolWater)
		{
			return FindNextVisibleRow(ref lngR, ref boolWater);
		}

		private int FindNextVisibleRow(ref int lngR, ref bool boolWater)
		{
			int FindNextVisibleRow = -1;
			// this function will start at the row number passed in and return the next row to print
			// this function will return -1 if there are no more rows to show
			int lngNext = 0;
			int intLvl;
			bool boolFound = false;
			int lngMax = 0;
			if (boolWater)
			{
				boolFound = false;
				lngNext = lngR + 1;
				lngMax = paymentForm.WGRID.Rows;
				if (lngNext < lngMax)
				{
					boolFound = false;
					while (!(boolFound || paymentForm.WGRID.RowOutlineLevel(lngNext) < 2))
					{
						if (paymentForm.WGRID.IsCollapsed(LastParentRow(lngNext, ref boolWater)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							lngNext += 1;
							boolFound = false;
							if (lngNext >= lngMax)
							{
								return FindNextVisibleRow;
							}
						}
						else
						{
							boolFound = true;
						}
					}
					FindNextVisibleRow = lngNext;
				}
			}
			else
			{
				boolFound = false;
				lngNext = lngR + 1;
				lngMax = paymentForm.SGRID.Rows;
				if (lngNext < lngMax)
				{
					boolFound = false;
					while (!(boolFound || paymentForm.SGRID.RowOutlineLevel(lngNext) < 2))
					{
						if (paymentForm.SGRID.IsCollapsed(LastParentRow(lngNext, ref boolWater)) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
						{
							lngNext += 1;
							boolFound = false;
							if (lngNext >= lngMax)
							{
								return FindNextVisibleRow;
							}
						}
						else
						{
							boolFound = true;
						}
					}
					FindNextVisibleRow = lngNext;
				}
			}
			if (FindNextVisibleRow == 0)
				FindNextVisibleRow = -1;
			return FindNextVisibleRow;
		}
		// vbPorter upgrade warning: CurRow As Variant --> As int
		public int LastParentRow(int CurRow, ref bool boolWater)
		{
			int LastParentRow = 0;
			int l;
			int curLvl;
			l = CurRow;
			if (boolWater)
			{
				curLvl = paymentForm.WGRID.RowOutlineLevel(l);
				l -= 1;
				while (paymentForm.WGRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
			}
			else
			{
				curLvl = paymentForm.SGRID.RowOutlineLevel(l);
				l -= 1;
				while (paymentForm.SGRID.RowOutlineLevel(l) > 1)
				{
					l -= 1;
				}
			}
			LastParentRow = l;
			return LastParentRow;
		}

		private void HideFields_2(bool boolHide)
		{
			HideFields(ref boolHide);
		}

		private void HideFields(ref bool boolHide)
		{
			// this sub will either hide or show the fields depending on which variable is shown
			fldYear.Visible = !boolHide;
			fldDate.Visible = !boolHide;
			fldRef.Visible = !boolHide;
			fldCode.Visible = !boolHide;
			fldPrincipal.Visible = !boolHide;
			fldTax.Visible = !boolHide;
			fldInterest.Visible = !boolHide;
			fldCosts.Visible = !boolHide;
			fldTotal.Visible = !boolHide;
			fldName.Visible = boolHide;
			if (boolHide)
			{
				// makes sure that the name field is in the proper position
				fldName.Left = 1F;
				fldName.Width = this.PrintWidth - 2F;
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			string strTemp;

			string strAddr1 = paymentForm.ViewModel.Customer.OwnerAddress1;
			string strAddr2 = paymentForm.ViewModel.Customer.OwnerAddress2;
			string strAddr3 = paymentForm.ViewModel.Customer.OwnerAddress3;
			string strAddr4 = paymentForm.ViewModel.Customer.OwnerCity + ", " + paymentForm.ViewModel.Customer.OwnerState + " " + paymentForm.ViewModel.Customer.OwnerZip;
			strTemp = "UT";
			
			lblAcreage.Text = "RE Acct: " + paymentForm.ViewModel.Master.REAccount;
			if (Strings.Trim(strAddr3) == "")
			{
				strAddr3 = strAddr4;
				strAddr4 = "";
			}
			if (Strings.Trim(strAddr2) == "")
			{
				strAddr2 = strAddr3;
				strAddr3 = strAddr4;
				strAddr4 = "";
			}
			if (Strings.Trim(strAddr1) == "")
			{
				strAddr1 = strAddr2;
				strAddr2 = strAddr3;
				strAddr3 = strAddr4;
				strAddr4 = "";
			}

			lblAddress1.Text = strAddr1;
			lblAddress2.Text = strAddr2;
			lblAddress3.Text = strAddr3;
			lblAddress4.Text = strAddr4;
			strTemp += " Account " + paymentForm.ViewModel.Master.AccountNumber + " Detail";
			strTemp += "\r\n" + "as of " + paymentForm.ViewModel.EffectiveDate.FormatAndPadShortDate();
			if (boolBothGrids)
			{
				lblHeader.Text = strTemp;
			}
			else if (paymentForm.ViewModel.VisibleService == UtilityType.Water || paymentForm.ViewModel.VisibleService == UtilityType.All)
			{
				lblHeader.Text = strTemp + " - Water";
				// kk trouts-6 02282013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					lblHeader.Text = strTemp + " - Stormwater";
				}
			}
			else
			{
				lblHeader.Text = strTemp + " - Sewer";
			}
			lblName.Text = "Name: " + paymentForm.lblOwnersName.Text;
			lblLocation.Text = "Location: " + paymentForm.lblLocation.Text;
			lblMapLot.Text = "Map/Lot: " + paymentForm.lblMapLot.Text;
		}

		private void CreatePerDiemTable_2(bool boolWater)
		{
			CreatePerDiemTable(ref boolWater);
		}

		private void CreatePerDiemTable(ref bool boolWater)
		{
			int lngRW;
			FCUtils.EraseSafe(modUTFunctions.Statics.pdiInfo);
			// kk12222014 trouts-121   Reset pdiInfo array and count for 2nd pass
			modUTFunctions.Statics.intPerdiemCounter = 0;
			if (boolWater)
			{
				for (lngRW = 1; lngRW <= paymentForm.WGRID.Rows - 1; lngRW++)
				{
					if (Strings.Trim(paymentForm.WGRID.TextMatrix(lngRW, paymentForm.lngGRIDColBillNumber)) != "")
					{
						strBillText = Strings.Trim(paymentForm.WGRID.TextMatrix(lngRW, paymentForm.lngGRIDColBillNumber));
					}
					if (Conversion.Val(paymentForm.WGRID.TextMatrix(lngRW, paymentForm.lngGRIDColPerDiem)) != 0)
					{
						AddPerDiem(ref lngRW, ref boolWater);
					}
				}
			}
			else
			{
				for (lngRW = 1; lngRW <= paymentForm.SGRID.Rows - 1; lngRW++)
				{
					if (Strings.Trim(paymentForm.SGRID.TextMatrix(lngRW, paymentForm.lngGRIDColBillNumber)) != "")
					{
						strBillText = Strings.Trim(paymentForm.SGRID.TextMatrix(lngRW, paymentForm.lngGRIDColBillNumber));
					}
					if (Conversion.Val(paymentForm.SGRID.TextMatrix(lngRW, paymentForm.lngGRIDColPerDiem)) != 0)
					{
						AddPerDiem(ref lngRW, ref boolWater);
					}
				}
			}
		}

		private void AddPerDiem(ref int lngRNum, ref bool boolWater)
		{
			object obNew;
			string strTemp = "";
			if (boolWater)
			{
				Array.Resize(ref modUTFunctions.Statics.pdiInfo, modUTFunctions.Statics.intPerdiemCounter + 1);
				if (Strings.Trim(paymentForm.WGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColBillNumber)) == "")
				{
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(paymentForm.WGRID.TextMatrix(paymentForm.LastParentRow(lngRNum, paymentForm.WGRID), paymentForm.lngGRIDColBillNumber));
					if (boolWater)
					{
						Array.Resize(ref modUTFunctions.Statics.pdiInfo, modUTFunctions.Statics.intPerdiemCounter + 1);
						if (Strings.Trim(paymentForm.WGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColBillNumber)) == "")
						{
							modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(paymentForm.WGRID.TextMatrix(paymentForm.LastParentRow(lngRNum, paymentForm.WGRID), paymentForm.lngGRIDColBillNumber));
						}
						else
						{
							modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(paymentForm.WGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColBillNumber));
						}
					}
					else
					{
						modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(paymentForm.WGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColBillNumber));
						modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].dblPerDiem = Conversion.Val(paymentForm.WGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColPerDiem));
					}
				}
				else
				{
					Array.Resize(ref modUTFunctions.Statics.pdiInfo, modUTFunctions.Statics.intPerdiemCounter + 1);
					if (Strings.Trim(paymentForm.SGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColBillNumber)) == "")
					{
						modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(paymentForm.SGRID.TextMatrix(paymentForm.LastParentRow(lngRNum, paymentForm.SGRID), paymentForm.lngGRIDColBillNumber));
					}
					else
					{
						modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(paymentForm.SGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColBillNumber));
					}
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].dblPerDiem = Conversion.Val(paymentForm.WGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColPerDiem));
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].dblPerDiem = Conversion.Val(paymentForm.SGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColPerDiem));
				}
			}
			else
			{
				Array.Resize(ref modUTFunctions.Statics.pdiInfo, modUTFunctions.Statics.intPerdiemCounter + 1);
				if (Strings.Trim(paymentForm.SGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColBillNumber)) == "")
				{
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(paymentForm.SGRID.TextMatrix(paymentForm.LastParentRow(lngRNum, paymentForm.SGRID), paymentForm.lngGRIDColBillNumber));
				}
				else
				{
					modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].intBill = FCConvert.ToInt16(paymentForm.SGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColBillNumber));
				}
				modUTFunctions.Statics.pdiInfo[modUTFunctions.Statics.intPerdiemCounter].dblPerDiem = Conversion.Val(paymentForm.SGRID.TextMatrix(lngRNum, paymentForm.lngGRIDColPerDiem));
			}
			modUTFunctions.Statics.intPerdiemCounter += 1;
			boolPerDiem = true;
		}
	}
}
