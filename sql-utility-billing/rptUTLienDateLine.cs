﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTLienDateLine.
	/// </summary>
	public partial class rptUTLienDateLine : BaseSectionReport
	{
		public rptUTLienDateLine()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Date Line";
		}

		public static rptUTLienDateLine InstancePtr
		{
			get
			{
				return (rptUTLienDateLine)Sys.GetInstance(typeof(rptUTLienDateLine));
			}
		}

		protected rptUTLienDateLine _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUTLienDateLine	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/15/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/15/2005              *
		// ********************************************************
		bool boolDone;
		// vbPorter upgrade warning: strRK As Variant --> As string()
		string[] strRK = null;
		string strRKList;
		int intNumberOfRK;
		int intindex = 0;
		public string strWS = string.Empty;

		public void Init(ref string strPassList, ref string strPassWS)
		{
			strRKList = strPassList;
			strWS = strPassWS;
			if (strWS == "S")
			{
				lblTitle.Text = "Sewer";
			}
			else
			{
				lblTitle.Text = "Water";
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblHeader.Text = frmUTLienDates.InstancePtr.strReportHeader;
			lblMuni.Text = modGlobalConstants.Statics.MuniName;
			// here I will find out how many and which rate records to print
			// this is a comma delimited string and I will break it down into an array
			strRK = Strings.Split(strRKList, ",", -1, CompareConstants.vbBinaryCompare);
			intNumberOfRK = Information.UBound(strRK, 1);
			if (intNumberOfRK >= 0)
			{
				boolDone = false;
			}
			else
			{
				boolDone = true;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (intindex > intNumberOfRK)
			{
				boolDone = true;
				sarLienDateDetailOb.Report = null;
			}
			else
			{
				boolDone = false;
				sarLienDateDetailOb.Report = new sarUTLienDateDetail();
				sarLienDateDetailOb.Report.UserData = strRK[intindex];
				intindex += 1;
			}
		}

		private void rptUTLienDateLine_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUTLienDateLine properties;
			//rptUTLienDateLine.Caption	= "Lien Date Line";
			//rptUTLienDateLine.Icon	= "rptUTLienDateLine.dsx":0000";
			//rptUTLienDateLine.Left	= 0;
			//rptUTLienDateLine.Top	= 0;
			//rptUTLienDateLine.Width	= 11880;
			//rptUTLienDateLine.Height	= 8595;
			//rptUTLienDateLine.StartUpPosition	= 3;
			//rptUTLienDateLine.SectionData	= "rptUTLienDateLine.dsx":058A;
			//End Unmaped Properties
		}
	}
}
