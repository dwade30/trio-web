﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptOaklandMetersWithNoUpdate.
	/// </summary>
	partial class srptOaklandMetersWithNoUpdate
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(srptOaklandMetersWithNoUpdate));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblPrincipal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSerialNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccountNumber,
				this.fldSequence,
				this.fldSerialNumber,
				this.fldName,
				this.fldBook
			});
			this.Detail.Height = 0.1875F;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblPrincipal,
				this.lblInterest,
				this.lnHeader,
				this.lblType,
				this.lblDesc,
				this.lblHeader,
				this.lblTax
			});
			this.GroupHeader1.Height = 0.5208333F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Height = 0F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblPrincipal
			// 
			this.lblPrincipal.Height = 0.1875F;
			this.lblPrincipal.HyperLink = null;
			this.lblPrincipal.Left = 0F;
			this.lblPrincipal.Name = "lblPrincipal";
			this.lblPrincipal.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblPrincipal.Text = "Account #";
			this.lblPrincipal.Top = 0.3125F;
			this.lblPrincipal.Width = 0.75F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 1.75F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblInterest.Text = "Sequence";
			this.lblInterest.Top = 0.3125F;
			this.lblInterest.Width = 0.75F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 0.5F;
			this.lnHeader.Width = 7.5F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 7.5F;
			this.lnHeader.Y1 = 0.5F;
			this.lnHeader.Y2 = 0.5F;
			// 
			// lblType
			// 
			this.lblType.Height = 0.1875F;
			this.lblType.HyperLink = null;
			this.lblType.Left = 2.59375F;
			this.lblType.Name = "lblType";
			this.lblType.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblType.Text = "Serial #";
			this.lblType.Top = 0.3125F;
			this.lblType.Width = 0.9375F;
			// 
			// lblDesc
			// 
			this.lblDesc.Height = 0.1875F;
			this.lblDesc.HyperLink = null;
			this.lblDesc.Left = 3.65625F;
			this.lblDesc.Name = "lblDesc";
			this.lblDesc.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblDesc.Text = "Name";
			this.lblDesc.Top = 0.3125F;
			this.lblDesc.Width = 3F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3125F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0.40625F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Meter Records Not Updated";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 0.90625F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: left";
			this.lblTax.Text = "Book";
			this.lblTax.Top = 0.3125F;
			this.lblTax.Width = 0.75F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 0F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldAccountNumber.Text = null;
			this.fldAccountNumber.Top = 0F;
			this.fldAccountNumber.Width = 0.75F;
			// 
			// fldSequence
			// 
			this.fldSequence.Height = 0.1875F;
			this.fldSequence.Left = 1.75F;
			this.fldSequence.Name = "fldSequence";
			this.fldSequence.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldSequence.Text = null;
			this.fldSequence.Top = 0F;
			this.fldSequence.Width = 0.75F;
			// 
			// fldSerialNumber
			// 
			this.fldSerialNumber.Height = 0.1875F;
			this.fldSerialNumber.Left = 2.59375F;
			this.fldSerialNumber.Name = "fldSerialNumber";
			this.fldSerialNumber.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldSerialNumber.Text = null;
			this.fldSerialNumber.Top = 0F;
			this.fldSerialNumber.Width = 0.9375F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 3.65625F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 3.8125F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0.90625F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; text-align: left";
			this.fldBook.Text = null;
			this.fldBook.Top = 0F;
			this.fldBook.Width = 0.75F;
			// 
			// srptOaklandMetersWithNoUpdate
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.510417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblPrincipal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSerialNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPrincipal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
	}
}
