﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptReminderSummary.
	/// </summary>
	public partial class rptReminderSummary : BaseSectionReport
	{
		public rptReminderSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptReminderSummary InstancePtr
		{
			get
			{
				return (rptReminderSummary)Sys.GetInstance(typeof(rptReminderSummary));
			}
		}

		protected rptReminderSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/21/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/07/2006              *
		// ********************************************************
		bool boolDone;
		double dblTotal;
		int lngCount;
		int lngAccts;
		int lngRetry;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			dblTotal = 0;
			lngCount = 0;
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			switch (modReminderNoticeSummary.Statics.intRPTReminderSummaryType)
			{
				case 0:
					{
						// Labels
						fldTotal.Visible = false;
						lblTotal.Visible = false;
						fldSumTotal.Visible = false;
						lnFooter.Visible = false;
						break;
					}
				case 1:
					{
						// Post Cards
						fldTotal.Visible = false;
						lblTotal.Visible = false;
						fldSumTotal.Visible = false;
						lnFooter.Visible = false;
						break;
					}
				case 2:
					{
						// Forms
						fldTotal.Visible = true;
						lblTotal.Visible = true;
						// lblYear.Visible = False
						break;
					}
				case 3:
					{
						// Mailers
						fldTotal.Visible = true;
						lblTotal.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will print the row from the grid
				TRYAGAIN:
				;
				if (lngCount <= Information.UBound(modReminderNoticeSummary.Statics.arrReminderSummaryList, 1))
				{
					if (modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Account == 0 && modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Name1 == "" && modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Total == 0)
					{
						lngRetry += 1;
						lngCount += 1;
						goto TRYAGAIN;
					}
					fldAccount.Text = modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Account.ToString();
					// If arrReminderSummaryList(lngCount).Year <> 0 And intRPTReminderSummaryType <> 2 Then
					// fldYear.Text = arrReminderSummaryList(lngCount).Year
					// Else
					// fldYear.Text = ""
					// End If
					fldName.Text = modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Name1;
					fldTotal.Text = Strings.Format(modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Total, "#,##0.00");
					dblTotal += modReminderNoticeSummary.Statics.arrReminderSummaryList[lngCount].Total;
					lngCount += 1;
					boolDone = false;
				}
				else
				{
					boolDone = true;
					fldYear.Text = "";
					fldName.Text = "";
					fldTotal.Text = "";
					fldAccount.Text = "";
					frmWait.InstancePtr.Unload();
				}
				return;
			}
			catch (Exception ex)
			{
				
				boolDone = true;
				if (Information.Err(ex).Number != 9)
				{
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			switch (modReminderNoticeSummary.Statics.intRPTReminderSummaryType)
			{
				case 0:
				case 1:
					{
						// Labels & Post Cards
						if (lngCount - lngRetry == 1)
						{
							fldTotalCount.Text = "There was " + FCConvert.ToString(lngCount - lngRetry) + " account processed.";
						}
						else
						{
							fldTotalCount.Text = "There were " + FCConvert.ToString(lngCount - lngRetry) + " notices processed.";
						}
						fldSumTotal.Text = "";
						fldTotalCount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Left;
						fldTotalCount.Left = 0;
						break;
					}
				case 2:
				case 3:
					{
						// Forms & Mailers
						fldTotalCount.Text = "Total for " + FCConvert.ToString(lngCount - lngRetry) + " notices:";
						fldSumTotal.Text = Strings.Format(dblTotal, "$#,##0.00");
						break;
					}
			}
			//end switch
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void rptReminderSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderSummary properties;
			//rptReminderSummary.Caption	= "Account Detail";
			//rptReminderSummary.Icon	= "rptReminderSummary.dsx":0000";
			//rptReminderSummary.Left	= 0;
			//rptReminderSummary.Top	= 0;
			//rptReminderSummary.Width	= 11880;
			//rptReminderSummary.Height	= 8595;
			//rptReminderSummary.StartUpPosition	= 3;
			//rptReminderSummary.SectionData	= "rptReminderSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
