﻿namespace TWUT0000
{
	public class modCustomPrograms
	{
		//=========================================================
		public struct CustomConsCalcInfo
		{
			public int lngAccountKey;
			public int lngMeterKey;
			public int lngConsumptionOverride;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CustomConsCalcInfo(int unusedParam)
			{
				this.lngAccountKey = 0;
				this.lngMeterKey = 0;
				this.lngConsumptionOverride = 0;
			}
		};

		public class StaticVariables
		{
			//Fecher vbPorter - Version 1.0.0.40
			public CustomConsCalcInfo[] cciInfo = null;
			public int lngAcctCounter;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
