﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptBillTransferReportMaster.
	/// </summary>
	public partial class rptBillTransferReportMaster : BaseSectionReport
	{
		public rptBillTransferReportMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Created Bills Report";
		}

		public static rptBillTransferReportMaster InstancePtr
		{
			get
			{
				return (rptBillTransferReportMaster)Sys.GetInstance(typeof(rptBillTransferReportMaster));
			}
		}

		protected rptBillTransferReportMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {

            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptBillTransferReportMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/07/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/12/2006              *
		// ********************************************************
		int[] BookArray = null;
		public int lngRateKey;
		//clsDRWrapper rsData = new clsDRWrapper();
		public double dblTotalSumAmount0;
		public double dblTotalSumAmount1;
		public double dblTotalSumAmount2;
		public double dblTotalSumAmount3;
		public double dblTotalSumAmount4;
		public double dblTotalSumAmount5;
		public double dblTotalSumAmount6;
		public double dblTotalSumAmount0W;
		public double dblTotalSumAmount1W;
		public double dblTotalSumAmount2W;
		public double dblTotalSumAmount3W;
		public double dblTotalSumAmount0S;
		public double dblTotalSumAmount1S;
		public double dblTotalSumAmount2S;
		public double dblTotalSumAmount3S;
		public int lngBillCount;
		int lngCT;
		public DateTime dtBillingDate;
		public int lngOrder;
		string strBooks = "";
		string strDateRanges;
		bool boolDone;

		public void Init(ref int[] BA, ref int lngRK, ref DateTime dtBillDate, string strPassDateRanges = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				BookArray = BA;
				lngRateKey = lngRK;
				dtBillingDate = dtBillDate;
				strDateRanges = strPassDateRanges;
				// trouts-227 kjr RK select by date range
				frmReportViewer.InstancePtr.Init(this);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = FCConvert.CBool(lngCT > Information.UBound(BookArray, 1));
				if (lngOrder == 0)
				{
					boolDone = eArgs.EOF;
				}
				else
				{
					boolDone = true;
				}
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			var rsOrder = new clsDRWrapper();
            try
            {
                rsOrder.OpenRecordset("SELECT BillingRepSeq FROM UtilityBilling", modExtraModules.strUTDatabase);
                if (!rsOrder.EndOfFile())
                {
                    lngOrder = FCConvert.ToInt32(Math.Round(Conversion.Val(rsOrder.Get_Fields_Int32("BillingRepSeq"))));
                }

                // force this report to be landscape
                this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
                ;
                lngCT = 1;
                // this loads the data
                if (Information.UBound(BookArray, 1) > 0)
                {
                    // rock on
                    FillHeaderLabels();
                    if (!(strDateRanges == ""))
                    {
                        // trouts-227 5.5.2017 Date Range selection option
                        lblReportType.Text = strDateRanges;
                    }
                    else
                    {
                        lblReportType.Text = "Rate Key : " + FCConvert.ToString(lngRateKey);
                    }

                    lblReportType.Text = lblReportType.Text + "    Interest As Of: " +
                                         Strings.Format(dtBillingDate, "MM/dd/yyyy");
                }
                else
                {
                    MessageBox.Show("No eligible accounts found for rate key " + FCConvert.ToString(lngRateKey) + ".",
                        "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    this.Close();
                }

                // kk trouts-6 03012013  Change Water to Stormwater for Bangor
                if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
                {
                    lblWater.Text = "Stormwater";
                }

                return;
            }
            catch (Exception ex)
            {

                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				rsOrder.Dispose();
            }
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (lngOrder == 0)
				{
					if (lngCT <= Information.UBound(BookArray, 1))
					{
						srptBookDetail.Report = new rptBillTransferReport();
						srptBookDetail.Report.UserData = BookArray[lngCT];
						lngCT += 1;
					}
				}
				else
				{
					SetBookString();
					srptBookDetail.Report = new rptBillTransferReport();
					srptBookDetail.Report.UserData = strBooks;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillTotals()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				fldTotalCons.Text = Strings.Format(dblTotalSumAmount0, "#,##0");
				fldTotalRegular.Text = Strings.Format(dblTotalSumAmount1, "#,##0.00");
				fldTotalMisc.Text = Strings.Format(dblTotalSumAmount2, "#,##0.00");
				fldTotalTax.Text = Strings.Format(dblTotalSumAmount3, "#,##0.00");
				fldTotalPastDue.Text = Strings.Format(dblTotalSumAmount4, "#,##0.00");
				fldTotalInterest.Text = Strings.Format(dblTotalSumAmount5, "#,##0.00");
				fldTotalAmount.Text = Strings.Format(dblTotalSumAmount6, "#,##0.00");
				if (lngBillCount == 1)
				{
					fldFooter.Text = "Total: " + "\r\n" + "1 bill";
				}
				else
				{
					fldFooter.Text = "Total: " + "\r\n" + FCConvert.ToString(lngBillCount) + " bills";
				}
				if (modUTStatusPayments.Statics.TownService == "B")
				{
					fldRegularWater.Text = Strings.Format(dblTotalSumAmount0W, "#,##0.00");
					fldMiscWater.Text = Strings.Format(dblTotalSumAmount1W, "#,##0.00");
					fldTaxWater.Text = Strings.Format(dblTotalSumAmount2W, "#,##0.00");
					fldInterestWater.Text = Strings.Format(dblTotalSumAmount3W, "#,##0.00");
					fldRegularSewer.Text = Strings.Format(dblTotalSumAmount0S, "#,##0.00");
					fldMiscSewer.Text = Strings.Format(dblTotalSumAmount1S, "#,##0.00");
					fldTaxSewer.Text = Strings.Format(dblTotalSumAmount2S, "#,##0.00");
					fldInterestSewer.Text = Strings.Format(dblTotalSumAmount3S, "#,##0.00");
				}
				else
				{
					lblWater.Visible = false;
					lblSewer.Visible = false;
					linServiceSummary.Visible = false;
					lblRegular.Visible = false;
					lblInterest.Visible = false;
					lblMisc.Visible = false;
					lblTax.Visible = false;
					fldRegularWater.Visible = false;
					fldMiscWater.Visible = false;
					fldTaxWater.Visible = false;
					fldInterestWater.Visible = false;
					fldRegularSewer.Visible = false;
					fldMiscSewer.Visible = false;
					fldTaxSewer.Visible = false;
					fldInterestSewer.Visible = false;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Totals", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			FillTotals();
		}

		private void SetBookString()
		{
			int intCT;
			strBooks = "";
			for (intCT = 1; intCT <= Information.UBound(BookArray, 1); intCT++)
			{
				strBooks += FCConvert.ToString(BookArray[intCT]) + ",";
			}
			strBooks = " IN (" + Strings.Left(strBooks, strBooks.Length - 1) + ")";
		}

		
	}
}
