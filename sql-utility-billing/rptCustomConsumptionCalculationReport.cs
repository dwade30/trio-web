﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptCustomConsumptionCalculationReport.
	/// </summary>
	public partial class rptCustomConsumptionCalculationReport : BaseSectionReport
	{
		public rptCustomConsumptionCalculationReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Consumption Calculation Report";
		}

		public static rptCustomConsumptionCalculationReport InstancePtr
		{
			get
			{
				return (rptCustomConsumptionCalculationReport)Sys.GetInstance(typeof(rptCustomConsumptionCalculationReport));
			}
		}

		protected rptCustomConsumptionCalculationReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomConsumptionCalculationReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		bool blnFirstRecord;
		int counter;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				counter += 1;
				if (counter < modCustomPrograms.Statics.lngAcctCounter)
				{
					eArgs.EOF = false;
				}
				else
				{
					eArgs.EOF = true;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			blnFirstRecord = true;
			counter = 0;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			clsDRWrapper rsMeterInfo = new clsDRWrapper();
			rsInfo.OpenRecordset(
                "SELECT Master.ID, AccountNumber, OwnerPartyID, pOwn.ID, pOwn.FullName FROM Master INNER JOIN " +
                modMain.Statics.strDbCP + "PartyNameView pOwn ON pOwn.ID = Master.OwnerPartyID WHERE Master.ID = " +
                modCustomPrograms.Statics.cciInfo[counter].lngAccountKey, "UtilityBilling");
            rsMeterInfo.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + modCustomPrograms.Statics.cciInfo[counter].lngMeterKey,
                "UtilityBilling");
            if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccount.Text = modGlobalFunctions.PadStringWithSpaces(Strings.Trim(rsInfo.Get_Fields("AccountNumber").ToString()), 8) + "  " + rsInfo.Get_Fields_String("FullName");
			}
			else
			{
				fldAccount.Text = "UNKNOWN";
			}
			if (rsMeterInfo.EndOfFile() != true && rsMeterInfo.BeginningOfFile() != true)
			{
				fldMeterNumber.Text = rsMeterInfo.Get_Fields_String("MeterNumber");
			}
			else
			{
				fldMeterNumber.Text = "UNKNOWN";
			}
			if (modCustomPrograms.Statics.cciInfo[counter].lngConsumptionOverride >= 0)
			{
				fldOverride.Text = Strings.Format(modCustomPrograms.Statics.cciInfo[counter].lngConsumptionOverride, "#,##0");
			}
			else
			{
				fldOverride.Text = "NOT SET";
			}
			rsInfo.Dispose();
			rsMeterInfo.Dispose();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			Label4.Text = "Page " + this.PageNumber;
		}

		
	}
}
