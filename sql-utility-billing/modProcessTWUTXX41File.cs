﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using modUTStatusPayments = Global.modUTFunctions;
using fecherFoundation.VisualBasicLayer;
using System.Linq;

namespace TWUT0000
{
	public class modProcessTWUTXX41File
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               11/13/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/27/2006              *
		// ********************************************************
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct CONSUMPTIONEXTRACT
		{
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] MeterKeyCharArray;
			public string MeterKey
			{
				get
				{
					return FCUtils.FixedStringFromArray(MeterKeyCharArray);
				}
				set
				{
					MeterKeyCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public char[] BookCharArray;
			public string Book
			{
				get
				{
					return FCUtils.FixedStringFromArray(BookCharArray);
				}
				set
				{
					BookCharArray = FCUtils.FixedStringToArray(value, 4);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public char[] SequenceCharArray;
			public string Sequence
			{
				get
				{
					return FCUtils.FixedStringFromArray(SequenceCharArray);
				}
				set
				{
					SequenceCharArray = FCUtils.FixedStringToArray(value, 4);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] CurrentReadingCharArray;
			public string CurrentReading
			{
				get
				{
					return FCUtils.FixedStringFromArray(CurrentReadingCharArray);
				}
				set
				{
					CurrentReadingCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] ReadingDateCharArray;
			public string ReadingDate
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingDateCharArray);
				}
				set
				{
					ReadingDateCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CONSUMPTIONEXTRACT(int unusedParam)
			{
				this.MeterKeyCharArray = new string(' ', 6).ToArray();
				this.BookCharArray = new string(' ', 4).ToArray();
				this.SequenceCharArray = new string(' ', 4).ToArray();
				this.CurrentReadingCharArray = new string(' ', 6).ToArray();
				this.ReadingDateCharArray = new string(' ', 6).ToArray();
				this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		public struct MeterData
		{
			public int MeterKey;
			public int BookNumber;
			public int Sequence;
			public int CurrentReading;
			public DateTime ReadingDate;
			public bool MatchFound;
			public bool Updated;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public MeterData(int unusedParam)
			{
				this.MeterKey = 0;
				this.BookNumber = 0;
				this.Sequence = 0;
				this.CurrentReading = 0;
				this.ReadingDate = default(DateTime);
				this.MatchFound = false;
				this.Updated = false;
			}
		};
		// holds book information
		// vbPorter upgrade warning: intCT As short	OnWriteFCConvert.ToInt32(
		public static void ProcessTWUTXX41File(ref short intCT, ref int[] PassBookArray)
		{
			int counter = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// vbPorter upgrade warning: counter As short --> As int	OnWrite(int, double)
				string strBookSeqSQL;
				clsDRWrapper rsTemp = new clsDRWrapper();
				bool boolForceReadDate;
				// kk01282014 trout-1009
				bool boolBillStormwater;
				Statics.intArrayMax = intCT;
				Statics.BookArray = PassBookArray;
				strBookSeqSQL = "";
				frmElectronicDataEntrySummary.InstancePtr.Unload();
				// kk01282014 trouts-136  Need to reset the form
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(Statics.BookArray, 1) == 1)
				{
					Statics.strSQL = " = " + FCConvert.ToString(Statics.BookArray[1]);
				}
				else
				{
					Statics.strSQL = "";
					for (counter = 1; counter <= Information.UBound(Statics.BookArray, 1); counter++)
					{
						Statics.strSQL += FCConvert.ToString(Statics.BookArray[counter]) + ",";
					}
					Statics.strSQL = "IN (" + Strings.Left(Statics.strSQL, Statics.strSQL.Length - 1) + ")";
				}
				// kk 02252013 trout-896  Add check for Badger Extract/Import
				rsTemp.OpenRecordset("SELECT ExtractFileType, ForceReadingDate, BillStormwaterFee FROM UtilityBilling");
				// kk01282014 trout-1009  Option to force Electronic Data Entry to use the default reading date
				boolForceReadDate = FCConvert.CBool(rsTemp.Get_Fields_Boolean("ForceReadingDate"));
				// kk01292014 trouts-68  Force all accounts to receive stormwater bill, even no bill
				boolBillStormwater = FCConvert.ToBoolean(rsTemp.Get_Fields_Boolean("BillStormwaterFee"));
				// MAL@20080121: Changed to exclude deleted accounts from the list
				// Tracker Reference: 12023
				// rsAccountInfo.OpenRecordset "SELECT * FROM MeterTable WHERE BookNumber " & strSQL
				Statics.rsAccountInfo.OpenRecordset("SELECT *, MeterTable.ID AS MeterKey FROM MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID WHERE ISNULL(Master.Deleted,0) = 0 AND BookNumber " + Statics.strSQL);
				if (FCFileSystem.FileExists("TSUTXX41.DAT") == false)
				{
					MessageBox.Show("Could not find the TSUTXX41.DAT file in the data directory.  Please copy the file into your data directory then try again.", "No File Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				Statics.datDefaultReading = DateTime.Today;
				object strInput = Statics.datDefaultReading;
				if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this process", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					//FC:FINAL:DDU:converted back to correct variable
					Statics.datDefaultReading = FCConvert.ToDateTime(strInput);
					FCFileSystem.FileOpen(1, "TSUTXX41.DAT", OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.CE));
					Statics.mtdMeters = new MeterData[FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.CE)) + 1];
					for (counter = 1; counter <= FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.CE)); counter++)
					{
						FCFileSystem.FileGet(1, ref Statics.CE, counter);
						// kk 01222013 trout-896
						// If Val(Trim(CE.CurrentReading)) <> 0 Then
						Statics.mtdMeters[counter - 1].MeterKey = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Statics.CE.MeterKey))));
						Statics.mtdMeters[counter - 1].BookNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Statics.CE.Book))));
						Statics.mtdMeters[counter - 1].CurrentReading = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Statics.CE.CurrentReading))));
						Statics.mtdMeters[counter - 1].MatchFound = false;
						if (!boolForceReadDate && Information.IsDate(Strings.Left(Statics.CE.ReadingDate, 2) + "/" + Strings.Mid(Statics.CE.ReadingDate, 3, 2) + "/" + Strings.Right(Statics.CE.ReadingDate, 2)))
						{
							Statics.mtdMeters[counter - 1].ReadingDate = DateAndTime.DateValue(Strings.Left(Statics.CE.ReadingDate, 2) + "/" + Strings.Mid(Statics.CE.ReadingDate, 3, 2) + "/" + Strings.Right(Statics.CE.ReadingDate, 2));
						}
						else
						{
							Statics.mtdMeters[counter - 1].ReadingDate = Statics.datDefaultReading;
						}
						Statics.mtdMeters[counter - 1].Sequence = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(Statics.CE.Sequence))));
						Statics.mtdMeters[counter - 1].Updated = false;
						// kk01282015 trouts-136  Initialize/Reset the updated flag
						// End If
					}
					FCFileSystem.FileClose(1);
					frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true, Information.UBound(Statics.mtdMeters, 1), true);
					for (Statics.lngMeterCounter = 0; Statics.lngMeterCounter <= Information.UBound(Statics.mtdMeters, 1) - 1; Statics.lngMeterCounter++)
					{
						//Application.DoEvents();
						frmWait.InstancePtr.IncrementProgress();

						// kk 02252013 trout-896  Add check for Badger Extract/Import
						// kk 09022015  Add Veazie Extract Here for Orono - Type 8
						if (FCConvert.ToString(rsTemp.Get_Fields_String("ExtractFileType")) == "B" || FCConvert.ToString(rsTemp.Get_Fields_String("ExtractFileType")) == "8" || FCConvert.ToString(rsTemp.Get_Fields_String("ExtractFileType")) == "7" || (FCConvert.ToString(rsTemp.Get_Fields_String("ExtractFileType")) == "T" && modGlobalConstants.Statics.MuniName.ToLower().StartsWith("winthrop util")) )
						{
							if (Statics.rsAccountInfo.FindFirstRecord("MeterKey", Statics.mtdMeters[Statics.lngMeterCounter].MeterKey))
							{
								Statics.mtdMeters[Statics.lngMeterCounter].MatchFound = true;
								// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
								Statics.mtdMeters[Statics.lngMeterCounter].Sequence = FCConvert.ToInt32(Statics.rsAccountInfo.Get_Fields("Sequence"));
								// kk 10182013  trouts-42  Always update the meter - even if there's no readings
								// If mtdMeters(lngMeterCounter).CurrentReading <> 0 Then
								clsDRWrapper temp = Statics.rsAccountInfo;
								SaveInfo(ref temp);
								Statics.rsAccountInfo = temp;
								// End If
							}
							else
							{
								Statics.mtdMeters[Statics.lngMeterCounter].MatchFound = false;
							}
						}
						else
						{
							if (Statics.rsAccountInfo.FindFirstRecord2("BookNumber,Sequence", FCConvert.ToString(Statics.mtdMeters[Statics.lngMeterCounter].BookNumber) + "," + FCConvert.ToString(Statics.mtdMeters[Statics.lngMeterCounter].Sequence), ","))
							{
								Statics.mtdMeters[Statics.lngMeterCounter].MatchFound = true;
								// strBookSeqSQL = strBookSeqSQL & "(Book = " & mtdMeters(lngMeterCounter).BookNumber & " AND Sequence = " & mtdMeters(lngMeterCounter).Sequence & ") OR "
								clsDRWrapper temp = Statics.rsAccountInfo;
								if (Statics.mtdMeters[Statics.lngMeterCounter].CurrentReading != 0)
								{
									SaveInfo(ref temp);
								}
								else if (rsTemp.Get_Fields_String("ExtractFileType") == "G")
								{
									// kk 11042013 trouts-55  For Bangor, save 0 readings
									SaveInfo(ref temp);
								}
								Statics.rsAccountInfo = temp;
							}
							else
							{
								Statics.mtdMeters[Statics.lngMeterCounter].MatchFound = false;
							}
						}
					}
					// kk01292014 trouts-68  Force all accounts in the selected books to get a stormwater bill
					if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR" && boolBillStormwater)
					{
						modStormwater.ForceStormwaterBills(ref Statics.BookArray);
					}
					frmWait.InstancePtr.Unload();
					frmElectronicDataEntrySummary.InstancePtr.strBookSQL = Statics.strSQL;
					frmElectronicDataEntrySummary.InstancePtr.Show(App.MainForm);
				}
				return;
			}
			catch (Exception ex)
			{
				
				FCFileSystem.FileClose(1);
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Processing TWUTXX41.Dat " + FCConvert.ToString(counter), MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static bool IsDeletedAccount(ref clsDRWrapper rsMeter)
		{
			bool IsDeletedAccount = false;
			// MAL@20080116
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool blnResult = false;
				clsDRWrapper rsAcct = new clsDRWrapper();
				rsAcct.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsMeter.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
				if (rsAcct.RecordCount() > 0)
				{
					blnResult = FCConvert.ToBoolean(rsAcct.Get_Fields_Boolean("Deleted"));
				}
				else
				{
					blnResult = true;
				}
				IsDeletedAccount = blnResult;
			}
			catch
			{
				
				IsDeletedAccount = true;
			}
			return IsDeletedAccount;
		}

		private static bool SaveInfo(ref clsDRWrapper rsMeter)
		{
			bool SaveInfo = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this updates the record in the meter table and updates the information including the status
				// after billing status is set to 'D' then the user must calculate next
				bool NeedBill = false;
				int lngMeterReadingUnits = 0;
				bool blnCreatePrePay = false;
				double dblCredit = 0;
				clsDRWrapper rsTemp = new clsDRWrapper();
				// MAL@20080117: Create Pre-Payments with Credits
				// Call Reference: 113597
				if (modGlobalConstants.Statics.gblnAutoPrepayments)
				{
					blnCreatePrePay = modUTBilling.CalculatePrePayments(rsMeter.Get_Fields_Int32("AccountKey"), rsMeter.Get_Fields_Int32("MeterKey"), ref dblCredit);
					if (!blnCreatePrePay && dblCredit < 0)
					{
						modUTBilling.AddAccounttoPrePayTable(rsMeter.Get_Fields_Int32("AccountKey"), dblCredit, false);
					}
					else if (blnCreatePrePay && dblCredit < 0)
					{
						modUTBilling.AddAccounttoPrePayTable(rsMeter.Get_Fields_Int32("AccountKey"), dblCredit, true);
					}
				}
				if (rsMeter.Get_Fields_String("BillingStatus") == "C" || rsMeter.Get_Fields_String("BillingStatus") == "")
				{
					// if the meter status is cleared then no bill has been created yet
					UpdateBillRecord_6(rsMeter, true);
					// create bill record
					// CreateBillRecord
				}
				else
				{
					// if a bill already exists and something has been changed,
					NeedBill = UpdateBillRecord_21(rsMeter, true);
					// update the current bill record
				}
				// If .EditMode = 0 Then           'checks the mode of the recordset
				// .Edit
				// End If
				if (NeedBill == true)
				{
					// checks the status of the bill record
					rsMeter.Set_Fields("BillingStatus", "C");
					// if there is no bill (somehow) this will set the status back to "C"
					SaveInfo = SaveInfo;
					// a bill will be created when this sub is called again (slight recursion)
					return SaveInfo;
					// exit to avoid infinite loop
				}
				else
				{
					rsMeter.Set_Fields("BillingStatus", "D");
					// all is good, move status to Data Entered
					Statics.mtdMeters[Statics.lngMeterCounter].Updated = true;
				}
				lngMeterReadingUnits = rsMeter.Get_Fields_Int32("Multiplier");
				// input values
				rsMeter.Set_Fields("CurrentReading", (Statics.mtdMeters[Statics.lngMeterCounter].CurrentReading) * (FCConvert.ToDouble(lngMeterReadingUnits) / modExtraModules.Statics.glngTownReadingUnits));
				// kk 06262013 Use the mtdMeters actual read date     .Fields("CurrentReadingDate") = datDefaultReading
				rsMeter.Set_Fields("CurrentReadingDate", Statics.mtdMeters[Statics.lngMeterCounter].ReadingDate);
				rsMeter.Set_Fields("CurrentCode", "A");
				// kk 03132013  Can't update a Join
				// update the recordset
				// .Update
				rsTemp.Execute("UPDATE MeterTable SET CurrentReading = " + rsMeter.Get_Fields_Int32("CurrentReading") + ", " + "CurrentReadingDate = '" + Strings.Format(rsMeter.Get_Fields_DateTime("CurrentReadingDate"), "MM/dd/yyyy") + "', " + "CurrentCode = '" + rsMeter.Get_Fields_String("CurrentCode") + "', " + "BillingStatus = '" + rsMeter.Get_Fields_String("BillingStatus") + "' " + "WHERE ID = " + rsMeter.Get_Fields_Int32("MeterKey"), modExtraModules.strUTDatabase);
				Statics.rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + rsMeter.Get_Fields_Int32("BookNumber"));
				if (Statics.rsBook.EndOfFile() != true && Statics.rsBook.BeginningOfFile() != true)
				{
					// set book status
					// k            If .EditMode = 0 Then
					// .Edit
					Statics.rsBook.Set_Fields("CurrentStatus", "DE");
					Statics.rsBook.Set_Fields("DDate", DateTime.Now);
					Statics.rsBook.Update();
					// Else
					// .Fields("CurrentStatus") = "DE"
					// .Fields("DDate") = Now
					// .Update
					// End If
				}
				SaveInfo = true;
				return SaveInfo;
			}
			catch (Exception ex)
			{
				
				SaveInfo = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveInfo;
		}

		private static bool UpdateBillRecord_6(clsDRWrapper rsMeter, bool boolCreateNew = false, bool boolAddBillAsNeeded = false)
		{
			return UpdateBillRecord(ref rsMeter, boolCreateNew, boolAddBillAsNeeded);
		}

		private static bool UpdateBillRecord_21(clsDRWrapper rsMeter, bool boolAddBillAsNeeded = false)
		{
			return UpdateBillRecord(ref rsMeter, false, boolAddBillAsNeeded);
		}

		private static bool UpdateBillRecord(ref clsDRWrapper rsMeter, bool boolCreateNew = false, bool boolAddBillAsNeeded = false)
		{
			bool UpdateBillRecord = false;
			// this function will create/edit the bill record for this meter
			// editing the major info like meter key, account key, service
			// and also editing the DE Adjustment amounts and rate keys
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intTemp;
				string Service = "";
				string BillingCode = "";
				clsDRWrapper rsRate = new clsDRWrapper();
				clsDRWrapper rsBill = new clsDRWrapper();
				double dblWAdjust;
				double dblSAdjust;

                rsBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND BillStatus <> 'B'", modExtraModules.strUTDatabase);

                if (rsBill.RecordCount() > 0 || boolCreateNew || boolAddBillAsNeeded)
                {
                    // added a RC check for the AMSD problem 04/27/2006
                    if ((boolCreateNew && rsBill.RecordCount() == 0) ||
                        (boolAddBillAsNeeded && rsBill.RecordCount() <= 0))
                    {
                        rsBill.AddNew();
                        // kk 06172013 trouts-19  Bill Fields with Nulls messing up prepays
                        rsBill.Set_Fields("BillNumber", 0);
                        rsBill.Set_Fields("BillingRateKey", 0);
                        rsBill.Set_Fields("WLienStatusEligibility", 0);
                        rsBill.Set_Fields("WLienProcessStatus", 0);
                        rsBill.Set_Fields("WLienRecordNumber", 0);
                        rsBill.Set_Fields("WCombinationLienKey", 0);
                        rsBill.Set_Fields("SLienStatusEligibility", 0);
                        rsBill.Set_Fields("SLienProcessStatus", 0);
                        rsBill.Set_Fields("SLienRecordNumber", 0);
                        rsBill.Set_Fields("SCombinationLienKey", 0);
                        rsBill.Set_Fields("WIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
                        rsBill.Set_Fields("WIntOwed", 0);
                        rsBill.Set_Fields("WIntAdded", 0);
                        rsBill.Set_Fields("WCostOwed", 0);
                        rsBill.Set_Fields("WCostAdded", 0);
                        rsBill.Set_Fields("WPrinPaid", 0);
                        rsBill.Set_Fields("WTaxPaid", 0);
                        rsBill.Set_Fields("WIntPaid", 0);
                        rsBill.Set_Fields("WCostPaid", 0);
                        rsBill.Set_Fields("SIntPaidDate", DateAndTime.DateValue(FCConvert.ToString(0)));
                        rsBill.Set_Fields("SIntOwed", 0);
                        rsBill.Set_Fields("SIntAdded", 0);
                        rsBill.Set_Fields("SCostOwed", 0);
                        rsBill.Set_Fields("SCostAdded", 0);
                        rsBill.Set_Fields("SPrinPaid", 0);
                        rsBill.Set_Fields("STaxPaid", 0);
                        rsBill.Set_Fields("SIntPaid", 0);
                        rsBill.Set_Fields("SCostPaid", 0);
                        rsBill.Set_Fields("Copies", 0);
                        rsBill.Set_Fields("Loadback", 0);
                        rsBill.Set_Fields("Final", 0);
                        rsBill.Set_Fields("SHasOverride", 0);
                        rsBill.Set_Fields("WHasOverride", 0);
                        rsBill.Set_Fields("SDemandGroupID", 0);
                        rsBill.Set_Fields("WDemandGroupID", 0);
                        rsBill.Set_Fields("SendEBill", 0);
                        rsBill.Set_Fields("WinterBill", 0);
                        rsBill.Set_Fields("SUploaded", 0);
                        rsBill.Set_Fields("WUploaded", 0);
                        rsBill.Update();
                        // get next bill number
                        // .Fields("BillNumber") = GetNextBillNumber(rsMeter.Fields("AccountKey"))
                    }
                    else
                    {
                        rsBill.Edit();
                    }

                    // add the final bill dates        'kk03132015 trouts-106  Copy from manual data entry so bill after final is correct
                    if (rsMeter.Get_Fields_Boolean("Final"))
                    {
                        rsBill.Set_Fields("FinalEndDate", rsMeter.Get_Fields_DateTime("FinalEndDate"));
                        rsBill.Set_Fields("FinalStartDate", rsMeter.Get_Fields_DateTime("FinalStartDate"));
                        rsBill.Set_Fields("FinalBillDate", rsMeter.Get_Fields_DateTime("FinalBillDate"));
                    }

                    rsBill.Set_Fields("AccountKey", rsMeter.Get_Fields_Int32("AccountKey"));
                    int lngAccountKey = rsMeter.Get_Fields_Int32("AccountKey");
                    rsBill.Set_Fields("ActualAccountNumber", modUTStatusPayments.GetAccountNumber(ref lngAccountKey));
                    rsBill.Set_Fields("Book", rsMeter.Get_Fields_Int32("BookNumber"));

                    // .Fields("MeterKey") = rsMeter.Fields("MeterKey")

                    // .fields("CollectionKey = Wait until it is created for this bill record, then fill this field in
                    if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_String("Service")) == false)
                    {
                        rsBill.Set_Fields("Service", rsMeter.Get_Fields_String("Service"));
                    }
                    else
                    {
                        rsBill.Set_Fields("Service", "B");
                    }

                    Service = FCConvert.ToString(rsBill.Get_Fields_String("Service"));
                    rsBill.Set_Fields("BillStatus", "D");
                    if (rsMeter.Get_Fields_Int32("MeterNumber") == 1)
                    {
                        rsBill.Set_Fields("MeterKey", rsMeter.Get_Fields_Int32("MeterKey"));
                        modUTBilling.ClearBreakDownRecords(true, rsBill.Get_Fields_Int32("Id"), true,
                            rsMeter.Get_Fields_Int32("MeterKey"));
                   

                        if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_String("BillingCode")) == false)
                        {
                            rsBill.Set_Fields("CombinationCode", rsMeter.Get_Fields_String("BillingCode"));
                        }
                        else
                        {
                            rsBill.Set_Fields("CombinationCode", "2");
                        }

                        if (fecherFoundation.FCUtils.IsNull(rsBill.Get_Fields_String("CombinationCode")) == false)
                        {
                            BillingCode = FCConvert.ToString(rsBill.Get_Fields_String("CombinationCode"));
                        }
                        else
                        {
                            BillingCode = "";
                        }

                        rsBill.Set_Fields("CurDate", Statics.mtdMeters[Statics.lngMeterCounter].ReadingDate);
                        // kk 09052013 trout-986   datDefaultReading
                        rsBill.Set_Fields("CurReading",
                            Statics.mtdMeters[Statics.lngMeterCounter].CurrentReading *
                            (rsMeter.Get_Fields_Int32("Multiplier") / modExtraModules.Statics.glngTownReadingUnits));
                        rsBill.Set_Fields("CurCode", "A");
                        if (Information.IsDate(rsMeter.Get_Fields("PreviousReadingDate")))
                        {
                            rsBill.Set_Fields("PrevDate", rsMeter.Get_Fields_DateTime("PreviousReadingDate"));
                        }

                        rsBill.Set_Fields("PrevReading",
                            FCConvert.ToString(Conversion.Val(rsMeter.Get_Fields_Int32("PreviousReading"))));
                        rsBill.Set_Fields("PrevCode", Strings.Trim(rsMeter.Get_Fields_String("PreviousCode") + " "));
                        short nTemp = FCConvert.ToInt16(rsMeter.Get_Fields_Int32("Digits"));
                        rsBill.Set_Fields("Consumption",
                            CalculateMeterConsumption_8(
                                FCConvert.ToInt32(
                                    FCConvert.ToString(Conversion.Val(rsBill.Get_Fields_Int32("PrevReading")))),
                                FCConvert.ToInt32(
                                    FCConvert.ToString(Conversion.Val(rsBill.Get_Fields_Int32("CurReading")))), ref nTemp));
                        rsBill.Set_Fields("BillingCode", rsMeter.Get_Fields_String("BillingCode"));
                        rsBill.Set_Fields("WDEAdjustAmount", 0);
                        rsBill.Set_Fields("SDEAdjustAmount", 0);
                    }

                for (intTemp = 1; intTemp <= 5; intTemp++)
					{
						// rate table information
						if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intTemp))) == false)
						{
							rsBill.Set_Fields("WRT" + intTemp, rsMeter.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intTemp)));
						}
						else
						{
							rsBill.Set_Fields("WRT" + intTemp, 0);
						}
						if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intTemp))) == false)
						{
							rsBill.Set_Fields("SRT" + intTemp, rsMeter.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intTemp)));
						}
						else
						{
							rsBill.Set_Fields("SRT" + intTemp, 0);
						}
					}
					rsBill.Update();
					UpdateBillRecord = false;
				}
				else
				{
					UpdateBillRecord = true;
				}
				return UpdateBillRecord;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateBillRecord;
		}
		// vbPorter upgrade warning: lngPrevious As int	OnWrite(string, int)
		// vbPorter upgrade warning: lngCurrent As int	OnWrite(string, int)
		public static int CalculateMeterConsumption_8(int lngPrevious, int lngCurrent, ref short intMeterDigits)
		{
			return CalculateMeterConsumption(ref lngPrevious, ref lngCurrent, ref intMeterDigits);
		}

		public static int CalculateMeterConsumption_26(int lngPrevious, int lngCurrent, short intMeterDigits)
		{
			return CalculateMeterConsumption(ref lngPrevious, ref lngCurrent, ref intMeterDigits);
		}

		public static int CalculateMeterConsumption(ref int lngPrevious, ref int lngCurrent, ref short intMeterDigits)
		{
			int CalculateMeterConsumption = 0;
			// vbPorter upgrade warning: lngMax As int	OnWrite(string)
			int lngMax = 0;
			if (intMeterDigits == 0)
			{
				intMeterDigits = 6;
			}
			if (lngPrevious <= lngCurrent)
			{
				CalculateMeterConsumption = lngCurrent - lngPrevious;
			}
			else
			{
				lngMax = FCConvert.ToInt32(Strings.StrDup(intMeterDigits, "9"));
				CalculateMeterConsumption = (lngMax - lngPrevious) + lngCurrent;
			}
			return CalculateMeterConsumption;
		}

		public class StaticVariables
		{
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public CONSUMPTIONEXTRACT CE = new CONSUMPTIONEXTRACT(0);
			public MeterData[] mtdMeters = null;
			public int intArrayMax;
			// How many books there are in the array
			public int[] BookArray = null;
			// Array holding book numbers of selected books
			public string strSQL = "";
			// SQL statement to get information we need for report
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsAccountInfo = new clsDRWrapper();
			public clsDRWrapper rsAccountInfo_AutoInitialized = null;
			public clsDRWrapper rsAccountInfo
			{
				get
				{
					if ( rsAccountInfo_AutoInitialized == null)
					{
						 rsAccountInfo_AutoInitialized = new clsDRWrapper();
					}
					return rsAccountInfo_AutoInitialized;
				}
				set
				{
					 rsAccountInfo_AutoInitialized = value;
				}
			}
			public int lngMeterCounter;
			public DateTime datDefaultReading;
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsBook = new clsDRWrapper();
			public clsDRWrapper rsBook_AutoInitialized = null;
			public clsDRWrapper rsBook
			{
				get
				{
					if ( rsBook_AutoInitialized == null)
					{
						 rsBook_AutoInitialized = new clsDRWrapper();
					}
					return rsBook_AutoInitialized;
				}
				set
				{
					 rsBook_AutoInitialized = value;
				}
			}
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
