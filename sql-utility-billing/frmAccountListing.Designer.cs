﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmAccountListing.
	/// </summary>
	partial class frmAccountListing : BaseForm
	{
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraSave;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCDraggableListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCListBox lstFields;
		public fecherFoundation.FCButton cmdExit;
		public fecherFoundation.FCFrame fraQuestions;
		public fecherFoundation.FCCheckBox chkHardCode;
		public fecherFoundation.FCFrame fraHardCode;
		public fecherFoundation.FCComboBox cmbHardCode;
		public fecherFoundation.FCLabel lblShowFields;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		public fecherFoundation.FCToolStripMenuItem mnuLayout;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAccountListing));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.javaScriptNumbersOnlyWithoutBackspace = new Wisej.Web.JavaScript(this.components);
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.cmdClear = new fecherFoundation.FCButton();
            this.fraSave = new fecherFoundation.FCFrame();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCDraggableListBox();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCListBox();
            this.cmdExit = new fecherFoundation.FCButton();
            this.fraQuestions = new fecherFoundation.FCFrame();
            this.chkHardCode = new fecherFoundation.FCCheckBox();
            this.fraHardCode = new fecherFoundation.FCFrame();
            this.cmbHardCode = new fecherFoundation.FCComboBox();
            this.lblShowFields = new fecherFoundation.FCLabel();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLayout = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdProcess = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).BeginInit();
            this.fraQuestions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHardCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraHardCode)).BeginInit();
            this.fraHardCode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 558);
            this.BottomPanel.Size = new System.Drawing.Size(908, 108);
            this.ToolTip1.SetToolTip(this.BottomPanel, null);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.fraSave);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.cmdExit);
            this.ClientArea.Controls.Add(this.fraQuestions);
            this.ClientArea.Size = new System.Drawing.Size(908, 498);
            this.ToolTip1.SetToolTip(this.ClientArea, null);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(908, 60);
            this.ToolTip1.SetToolTip(this.TopPanel, null);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(179, 30);
            this.HeaderText.Text = "Account Listing";
            this.ToolTip1.SetToolTip(this.HeaderText, null);
            // 
            // fraWhere
            // 
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 481);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(831, 247);
            this.fraWhere.TabIndex = 6;
            this.fraWhere.Text = "Select Search Criteria";
            this.ToolTip1.SetToolTip(this.fraWhere, null);
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Cols = 3;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.ExtendLastCol = true;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(20, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.Size = new System.Drawing.Size(791, 197);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 7;
            this.ToolTip1.SetToolTip(this.vsWhere, null);
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_ChangeEdit);
            this.vsWhere.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsWhere_MouseMoveEvent);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            this.vsWhere.EditingControlShowing += new Wisej.Web.DataGridViewEditingControlShowingEventHandler(this.vsWhere_EditingControlShowing);
            this.vsWhere.Enter += new System.EventHandler(this.vsWhere_Enter);
            this.vsWhere.Leave += new System.EventHandler(this.vsWhere_Leave);
            this.vsWhere.KeyDown += new Wisej.Web.KeyEventHandler(this.vsWhere_KeyDownEvent);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(143)))), ((int)(((byte)(164)))));
            this.cmdClear.Location = new System.Drawing.Point(740, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(140, 24);
            this.cmdClear.TabIndex = 8;
            this.cmdClear.Text = "Clear Search Criteria";
            this.ToolTip1.SetToolTip(this.cmdClear, null);
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // fraSave
            // 
            this.fraSave.Enabled = false;
            this.fraSave.Location = new System.Drawing.Point(604, 247);
            this.fraSave.Name = "fraSave";
            this.fraSave.Size = new System.Drawing.Size(257, 211);
            this.fraSave.TabIndex = 5;
            this.fraSave.Text = "Report";
            this.ToolTip1.SetToolTip(this.fraSave, null);
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Location = new System.Drawing.Point(317, 247);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(257, 211);
            this.fraSort.TabIndex = 3;
            this.fraSort.Text = "Fields To Sort By";
            this.ToolTip1.SetToolTip(this.fraSort, null);
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(217, 161);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 4;
            this.ToolTip1.SetToolTip(this.lstSort, null);
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Enabled = false;
            this.fraFields.Location = new System.Drawing.Point(30, 247);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(257, 211);
            this.fraFields.TabIndex = 1;
            this.fraFields.Text = "Fields To Display On Report";
            this.ToolTip1.SetToolTip(this.fraFields, null);
            this.fraFields.DoubleClick += new System.EventHandler(this.fraFields_DoubleClick);
            // 
            // lstFields
            // 
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.CheckBoxes = true;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(217, 161);
            this.lstFields.Style = 1;
            this.lstFields.TabIndex = 2;
            this.ToolTip1.SetToolTip(this.lstFields, null);
            // 
            // cmdExit
            // 
            this.cmdExit.Location = new System.Drawing.Point(42, 544);
            this.cmdExit.Name = "cmdExit";
            this.cmdExit.Size = new System.Drawing.Size(96, 26);
            this.cmdExit.TabIndex = 9;
            this.cmdExit.Text = "Exit";
            this.ToolTip1.SetToolTip(this.cmdExit, null);
            this.cmdExit.Visible = false;
            this.cmdExit.Click += new System.EventHandler(this.cmdExit_Click);
            // 
            // fraQuestions
            // 
            this.fraQuestions.AppearanceKey = "groupBoxNoBorders";
            this.fraQuestions.Controls.Add(this.chkHardCode);
            this.fraQuestions.Controls.Add(this.fraHardCode);
            this.fraQuestions.Controls.Add(this.lblShowFields);
            this.fraQuestions.Location = new System.Drawing.Point(30, 30);
            this.fraQuestions.Name = "fraQuestions";
            this.fraQuestions.Size = new System.Drawing.Size(831, 197);
            this.fraQuestions.TabIndex = 11;
            this.ToolTip1.SetToolTip(this.fraQuestions, null);
            // 
            // chkHardCode
            // 
            this.chkHardCode.Enabled = false;
            this.chkHardCode.Location = new System.Drawing.Point(425, 60);
            this.chkHardCode.Name = "chkHardCode";
            this.chkHardCode.Size = new System.Drawing.Size(151, 23);
            this.chkHardCode.TabIndex = 14;
            this.chkHardCode.Text = "Use a Default Report";
            this.ToolTip1.SetToolTip(this.chkHardCode, null);
            this.chkHardCode.CheckedChanged += new System.EventHandler(this.chkHardCode_CheckedChanged);
            this.chkHardCode.KeyDown += new Wisej.Web.KeyEventHandler(this.chkHardCode_KeyDown);
            // 
            // fraHardCode
            // 
            this.fraHardCode.Controls.Add(this.cmbHardCode);
            this.fraHardCode.Enabled = false;
            this.fraHardCode.Location = new System.Drawing.Point(425, 107);
            this.fraHardCode.Name = "fraHardCode";
            this.fraHardCode.Size = new System.Drawing.Size(406, 90);
            this.fraHardCode.TabIndex = 13;
            this.fraHardCode.Text = "Choose A Default Report";
            this.ToolTip1.SetToolTip(this.fraHardCode, null);
            // 
            // cmbHardCode
            // 
            this.cmbHardCode.BackColor = System.Drawing.SystemColors.Window;
            this.cmbHardCode.Enabled = false;
            this.cmbHardCode.Location = new System.Drawing.Point(20, 30);
            this.cmbHardCode.Name = "cmbHardCode";
            this.cmbHardCode.Size = new System.Drawing.Size(356, 40);
            this.ToolTip1.SetToolTip(this.cmbHardCode, null);
            this.cmbHardCode.SelectedIndexChanged += new System.EventHandler(this.cmbHardCode_SelectedIndexChanged);
            // 
            // lblShowFields
            // 
            this.lblShowFields.Name = "lblShowFields";
            this.lblShowFields.Size = new System.Drawing.Size(395, 197);
            this.lblShowFields.TabIndex = 15;
            this.ToolTip1.SetToolTip(this.lblShowFields, null);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 18);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuFileSeperator2,
            this.mnuPrint,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear Search Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 1;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 2;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrint.Text = "Process";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 3;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuLayout
            // 
            this.mnuLayout.Enabled = false;
            this.mnuLayout.Index = -1;
            this.mnuLayout.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn});
            this.mnuLayout.Name = "mnuLayout";
            this.mnuLayout.Text = "Layout";
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 0;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row";
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 1;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column";
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 2;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 3;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(403, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(100, 48);
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // frmAccountListing
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(908, 666);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmAccountListing";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Account Listing";
            this.ToolTip1.SetToolTip(this, null);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmAccountListing_Load);
            this.Activated += new System.EventHandler(this.frmAccountListing_Activated);
            this.Resize += new System.EventHandler(this.frmAccountListing_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmAccountListing_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdExit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraQuestions)).EndInit();
            this.fraQuestions.ResumeLayout(false);
            this.fraQuestions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkHardCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraHardCode)).EndInit();
            this.fraHardCode.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcess;
		//FC:FINAL:CHN - issue #930: allow numbers only to enter the account
		private JavaScript javaScriptNumbersOnlyWithoutBackspace;
		Wisej.Web.JavaScript.ClientEvent clientEventKeyPressNumbersOnlyWithoutBackspace;
	}
}
