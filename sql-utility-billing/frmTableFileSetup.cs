﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Core;
using Wisej.Web;
using Wisej.Web.Ext.CustomProperties;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmTableFileSetup.
	/// </summary>
	public partial class frmTableFileSetup : BaseForm
	{
		public frmTableFileSetup()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			this.fraRateTable = new System.Collections.Generic.List<fecherFoundation.FCPanel>();
			this.txtMinCons = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtRateS = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.txtRateW = new System.Collections.Generic.List<fecherFoundation.FCTextBox>();
			this.fraRateTable.AddControlArrayElement(fraRateTable_0, 0);
			this.txtMinCons.AddControlArrayElement(txtMinCons_1, 1);
			this.txtMinCons.AddControlArrayElement(txtMinCons_0, 0);
			this.txtRateS.AddControlArrayElement(txtRateS_0, 0);
			this.txtRateS.AddControlArrayElement(txtRateS_1, 1);
			this.txtRateW.AddControlArrayElement(txtRateW_1, 1);
			this.txtRateW.AddControlArrayElement(txtRateW_0, 0);
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
            txtMinCons_0.AllowOnlyNumericInput();
            txtMinCons_1.AllowOnlyNumericInput();
            txtRateS_0.AllowOnlyNumericInput(true);
            txtRateS_1.AllowOnlyNumericInput(true);
            txtRateW_0.AllowOnlyNumericInput(true);
            txtRateW_1.AllowOnlyNumericInput(true);
        }
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmTableFileSetup InstancePtr
		{
			get
			{
				return (frmTableFileSetup)Sys.GetInstance(typeof(frmTableFileSetup));
			}
		}

		protected frmTableFileSetup _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/19/2007              *
		// ********************************************************
		clsDRWrapper rs = new clsDRWrapper();
		string strSQL;
		bool Dirty;
		bool Cancelled;
		int maxTable;
		clsDRWrapper rsCat;
		clsDRWrapper rsRate;
		// holds rate table information
		int CurrentRT;
		// holds the current rate table number
		int AdjustRow;
		int FGWidth;
		bool boolCheckBookedit;
		bool boolReloading;
		clsGridAccount clsWCatAcct = new clsGridAccount();
		clsGridAccount clsSCatAcct = new clsGridAccount();
		clsGridAccount clsWAdj = new clsGridAccount();
		clsGridAccount clsSAdj = new clsGridAccount();
		clsGridAccount clsAdjustment = new clsGridAccount();
		int lngLastcmbRTIndex;
		bool boolRateTableUpdated;
		int lngCatColCode;
		int lngCatColDescription;
		int lngCatColShort;
		int lngCatColWAccount;
		int lngCatColSAccount;
		int lngCatColWTaxable;
		int lngCatColSTaxable;
        bool inValidateEdit = false;

		private void chkCombineToMisc_CheckedChanged(object sender, System.EventArgs e)
		{
			Dirty = true;
			// kk08142015 trout-1185
		}

		private void cmbRT_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			// vbPorter upgrade warning: temp As short, int --> As DialogResult
			DialogResult temp;
			if (!boolReloading)
			{
				if (Dirty == true)
				{
					temp = MessageBox.Show("Would you like to save your changes?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					// yes = 6 no = 7 cancel = 2
					if (temp == DialogResult.Yes)
					{
						// yes
						Save_RateTable();
					}
					else if (temp == DialogResult.No)
					{
						// no
					}
					else
					{
						// cancel
						boolReloading = true;
						cmbRT.SelectedIndex = lngLastcmbRTIndex;
						// kk08142015 trout-1185  Combobox has already been changed, must reset the index
						boolReloading = false;
						return;
					}
				}
				if (cmbRT.Text != "")
				{
					rsRate.FindFirstRecord("RateTableNumber", Strings.Left(cmbRT.Text, 2));
					Reset_RateTables();
				}
				lngLastcmbRTIndex = cmbRT.SelectedIndex;
				// kk08142015 trout-1185  Save the last index
			}
		}

		private void frmTableFileSetup_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
				return;
			// sets all of the general information  ie. date/time/title
			Dirty = false;
			// initializes Dirty to False (No change to data has occured)
			Cancelled = false;
			// cancel was not pressed (for tab click)
			boolRateTableUpdated = false;
		}

		private void frmTableFileSetup_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.F10)
			{
				KeyCode = (Keys)0;
				mnuProcessSave_Click();
			}
			// If UCase(Me.ActiveControl.Name) = UCase("vsAdjust") Then
			// If vsAdjust.Col = 5 And (KeyCode < vbKeyF1 Or KeyCode > vbKeyF12) Then
			// CheckFormKeyDown vsAdjust, vsAdjust.Row, vsAdjust.Col, KeyCode, Shift, vsAdjust.EditSelStart, vsAdjust.EditText, vsAdjust.EditSelLength
			// End If
			// End If
			// 
			// If UCase(Me.ActiveControl.Name) = UCase("vsCategory") Then
			// If (vsCategory.Col = lngCatColWAccount Or vsCategory.Col = lngCatColSAccount) And (KeyCode < vbKeyF1 Or KeyCode > vbKeyF12) Then
			// CheckFormKeyDown vsCategory, vsCategory.Row, vsCategory.Col, KeyCode, Shift, vsCategory.EditSelStart, vsCategory.EditText, vsCategory.EditSelLength
			// End If
			// End If
			// 
			// If UCase(Me.ActiveControl.Name) = UCase("vsMisc") Then
			// With vsMisc(Me.ActiveControl.Index)
			// If (.Col = 3) And (KeyCode < vbKeyF1 Or KeyCode > vbKeyF12) Then
			// CheckFormKeyDown vsMisc(Me.ActiveControl.Index), .Row, .Col, KeyCode, Shift, .EditSelStart, .EditText, .EditSelLength
			// End If
			// End With
			// End If
			// If TypeOf ActiveControl Is txtAcct Then  'traps the M key if BD is found
			// If gboolBD = True Then
			// If KeyCode = vbKeyM Then
			// KeyCode = 0
			// txtDAccount(acctIndex).Text = ""
			// txtDAccount(acctIndex).Visible = False
			// End If
			// Else
			// If KeyCode = vbKeyE Or KeyCode = vbKeyG Or KeyCode = vbKeyR Or KeyCode = vbKeyA Then KeyCode = 0
			// End If
			// End If
		}

		private void frmTableFileSetup_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				// traps the escape key
				KeyAscii = (Keys)0;
				this.Unload();
				return;
			}
			// traps the enter key for two reasons, if the active control is a flexgrid then
			// let the KeyAcsii code pass, otherwise send a tab instead
			if (KeyAscii == Keys.Return && fecherFoundation.FCUtils.IsNull(frmTableFileSetup.InstancePtr.ActiveControl.GetName()) == false)
			{
				// If Not TypeOf frmTableFileSetup.ActiveControl Is VSFlexGrid Then
				KeyAscii = (Keys)0;
				Support.SendKeys("{tab}", false);
				// End If
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmTableFileSetup_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmTableFileSetup properties;
			//frmTableFileSetup.FillStyle	= 0;
			//frmTableFileSetup.ScaleWidth	= 9300;
			//frmTableFileSetup.ScaleHeight	= 7830;
			//frmTableFileSetup.LinkTopic	= "Form2";
			//frmTableFileSetup.LockControls	= true;
			//frmTableFileSetup.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			this.Text = "Table File Setup";
			// for grid account box
			if (modGlobalConstants.Statics.gboolBD)
			{
				modValidateAccount.SetBDFormats();
			}
			Frame1.BackColor = tabSetup.BackColor;
			Frame2.BackColor = tabSetup.BackColor;
			Frame3.BackColor = tabSetup.BackColor;
			Frame4.BackColor = tabSetup.BackColor;
			Frame5.BackColor = tabSetup.BackColor;
			vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsWMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsSMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			Meter_Sizes();
			SetGridVariables();
			clsWCatAcct.GRID7Light = vsCategory;
			clsWCatAcct.AccountCol = FCConvert.ToInt16(lngCatColWAccount);
			clsSCatAcct.GRID7Light = vsCategory;
			clsSCatAcct.AccountCol = FCConvert.ToInt16(lngCatColSAccount);
			clsWAdj.GRID7Light = vsWMisc;
			clsWAdj.AccountCol = 3;
			clsSAdj.GRID7Light = vsSMisc;
			clsSAdj.AccountCol = 3;
			clsAdjustment.GRID7Light = vsAdjust;
			clsAdjustment.AccountCol = 5;
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblRateWater.Text = "Stormwater";
			}
			this.Menu = null;
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: temp As int --> As DialogResult
			DialogResult temp;
			if (Dirty == true)
			{
				temp = MessageBox.Show("Would you like to save your changes?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				// yes = 6 no = 7 cancel = 2
				if (temp == DialogResult.Yes)
				{
					// yes
					Save_RateTable();
				}
				else if (temp == DialogResult.No)
				{
					// no
				}
				else
				{
					// cancel
					e.Cancel = true;
				}
			}
			if (boolRateTableUpdated && !e.Cancel)
			{
				// kk08172015 trout-1186  Prompt to print listing if rates changed
				temp = MessageBox.Show("Would you like to print the Rate Table Listing?", "Print Listing", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
				if (temp == DialogResult.Yes)
				{
					boolRateTableUpdated = false;
					frmReportViewer.InstancePtr.Init(rptRateTablesCond.InstancePtr);
				}
			}
		}

		private void frmTableFileSetup_Resize(object sender, System.EventArgs e)
		{
			switch (tabSetup.SelectedIndex)
			{
				case 0:
					{
						// Meter Sizes
						Format_MeterSizes_Grid();
						MeterSizesHeightAdjust();
						break;
					}
				case 1:
					{
						// Frequency Codes
						Format_Frequency_Grid();
						AdjustFrequencyGridHeight();
						break;
					}
				case 2:
					{
						// User Category Codes
						Format_Category_Grid();
						CategoryHeightAdjust();
						break;
					}
				case 3:
					{
						// Account Adjustment
						Format_Adjust_Grid();
						AdjustHeightAdjust();
						break;
					}
				case 4:
					{
						// Rate Tables
						Format_Rate_tables();
						FlexGridSizeCheck();
						break;
					}
				case 5:
					{
						// Book Descriptions
						Format_BookDescriptions();
						AdjustBookGridHeight();
						break;
					}
			}
			//end switch
		}

		private void mnuAddCategory_Click(object sender, System.EventArgs e)
		{
			// this will add a user category to the list
			vsCategory.Rows += 1;
			// add a row to the end of the grid
			CategoryHeightAdjust();
			// adjust the height accordingly
			vsCategory.Col = 0;
			vsCategory.Row = vsCategory.Rows - 1;
			// select the code cell
			vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsCategory.EditCell();
			// let the user type a number
			//mnuDeleteCategory.Enabled = true;
			cmdDeleteCategory.Enabled = true;
		}

		private void mnuAdjustAdd_Click(object sender, System.EventArgs e)
		{
			vsAdjust.Rows += 1;
			AdjustHeightAdjust();
			vsAdjust.Select(vsAdjust.Rows - 1, 0);
			vsAdjust.Col = 0;
			vsAdjust.EditCell();
			//mnuAdjustDelete.Enabled = true;
			cmdAdjustDelete.Enabled = true;
		}

		private void mnuAdjustDelete_Click(object sender, System.EventArgs e)
		{
			short intCode = 0;
			short i = 0;
			// vbPorter upgrade warning: Ans As short, int --> As DialogResult
			DialogResult Ans;
			if (vsAdjust.Row >= 0)
			{
				intCode = FCConvert.ToInt16(Math.Round(Conversion.Val(vsAdjust.TextMatrix(vsAdjust.Row, 0))));
				Ans = MessageBox.Show("Are you sure that you would like to delete Adjustment #" + FCConvert.ToString(intCode) + "?", "Delete Adjustment", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				if (Ans == DialogResult.Yes)
				{
					DeleteAdjustment(ref intCode, ref i);
				}
			}
			if (vsAdjust.Rows <= 1)
			{
				//mnuAdjustDelete.Enabled = false;
				cmdAdjustDelete.Enabled = false;
			}
			else
			{
				//mnuAdjustDelete.Enabled = true;
				cmdAdjustDelete.Enabled = true;
			}
		}
		// vbPorter upgrade warning: intCode As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: DeleteRow As short	OnWriteFCConvert.ToInt32(
		private void DeleteAdjustment(ref short intCode, ref short DeleteRow)
		{
			// this will delete one record from the adjustment recordset
			clsDRWrapper rsAdj = new clsDRWrapper();
			rsAdj.Execute("DELETE FROM Adjust WHERE Code = " + FCConvert.ToString(intCode), modExtraModules.strUTDatabase);
			Fill_Adjust_Grid();
			AdjustHeightAdjust();
		}

		private void mnuAdjustEdit_Click()
		{
			// vsAdjust.Editable = True
		}

		private void mnuDeleteCategory_Click(object sender, System.EventArgs e)
		{
			// this will remove the row from the grid, delete is not final here
			// vbPorter upgrade warning: temp As short, int --> As DialogResult
			DialogResult temp;
			if (vsCategory.Rows > 1 && vsCategory.Row > 0)
			{
				// have to have another row other than the titles
				temp = MessageBox.Show("Are you sure you want to delete this category?", "Delete", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				// 6 = yes, 7 = no, 2 = cancel
				if (temp == DialogResult.Yes)
				{
					vsCategory.RemoveItem(vsCategory.Row);
					CategoryHeightAdjust();
				}
			}
			if (vsCategory.Rows <= 1)
			{
				//mnuDeleteCategory.Enabled = false;
				cmdDeleteCategory.Enabled = false;
			}
			else
			{
				//mnuDeleteCategory.Enabled = true;
				cmdDeleteCategory.Enabled = true;
			}
		}

		private void mnuFileBookAdd_Click(object sender, System.EventArgs e)
		{
			boolCheckBookedit = true;
			vsBookDescriptions.AddItem("");
			AdjustBookGridHeight();
			vsBookDescriptions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsBookDescriptions.Select(vsBookDescriptions.Rows - 1, 0);
			vsBookDescriptions.EditCell();
			boolCheckBookedit = false;
			// when a book is added, then the user can delete one
			//mnuFileBookDelete.Enabled = true;
			cmdFileBookDelete.Enabled = true;
		}

		private void mnuFileBookDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsDelete = new clsDRWrapper();
			int lngBookNumber = 0;
			clsDRWrapper rsCount = new clsDRWrapper();
			bool blnDelete = false;
			string strSQL = "";
			if (vsBookDescriptions.Row > 0)
			{
				lngBookNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBookDescriptions.TextMatrix(vsBookDescriptions.Row, 0))));
				if (lngBookNumber > 0)
				{
					// MAL@20070904: Check for any open accounts within this book before proceeding
					if (MessageBox.Show("Are you sure that you would like to delete book " + FCConvert.ToString(lngBookNumber) + "?", "Delete Book", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						blnDelete = true;
						// Optimistic
						strSQL = "SELECT Master.ID, Master.Deleted, MeterTable.BookNumber " + "FROM Master INNER JOIN MeterTable ON Master.ID = MeterTable.AccountKey " + "WHERE ISNULL(Master.Deleted,0) = 0 AND MeterTable.BookNumber = " + FCConvert.ToString(lngBookNumber);
						rsCount.OpenRecordset(strSQL);
						if (rsCount.RecordCount() > 0)
						{
							blnDelete = false;
							MessageBox.Show("This book has open accounts associated with it and cannot be deleted.", "Unable to Delete Book", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						if (blnDelete)
						{
							rsDelete.Execute("DELETE FROM Book WHERE BookNumber = " + FCConvert.ToString(lngBookNumber), modExtraModules.strUTDatabase);
							// remove the row from the grid
							vsBookDescriptions.RemoveItem(vsBookDescriptions.Row);
							if (vsBookDescriptions.Rows <= 1)
							{
								//mnuFileBookDelete.Enabled = false;
								cmdFileBookDelete.Enabled = false;
							}
						}
					}
					else
					{
						// Exit Sub
						// Do Nothing
					}
				}
				else
				{
					// there is no code to delete from the database
				}
			}
		}

		private void mnuFileRateTableListing_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptRateTables.InstancePtr);
		}

		private void mnuFileRateTableListingCond_Click(object sender, System.EventArgs e)
		{
			frmReportViewer.InstancePtr.Init(rptRateTablesCond.InstancePtr);
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			if (Save_TabData_2(tabSetup.SelectedIndex))
			{
				// save the correct tab information
				this.Unload();
				//Application.DoEvents();
				//MDIParent.InstancePtr.Focus();
			}
		}

		private void mnuProcessAddMeterSize_Click(object sender, System.EventArgs e)
		{
			int intCode;
			int ct;
			bool Found;
			int lngRow = 0;
			Found = false;
			intCode = FCConvert.ToInt32(Math.Round(Conversion.Val(Interaction.InputBox("Enter the code that you would like to associate with the new Meter Size (1-30)", "Add New Meter Size", null/*, -1, -1*/))));
			if (intCode > 0 && intCode <= 30)
			{
				for (ct = 1; ct <= vsMeterSizeDescList.Rows - 1; ct++)
				{
					// MAL@20070904: Added check (lngRow=0) to stop the lngRow from being changed once it is set
					if (Conversion.Val(vsMeterSizeDescList.TextMatrix(ct, 0)) > intCode && lngRow == 0)
					{
						// this will find the row that the new row should preceed
						lngRow = ct;
					}
					else if (intCode == 30 && lngRow == 0)
					{
						// Last possible code
						lngRow = vsMeterSizeDescList.Rows;
					}
					if (Conversion.Val(vsMeterSizeDescList.TextMatrix(ct, 0)) == intCode)
					{
						Found = true;
						break;
					}
				}
				if (Found == false)
				{
					if (lngRow == 0)
					{
						lngRow = vsMeterSizeDescList.Rows;
					}
					vsMeterSizeDescList.AddItem(FCConvert.ToString(intCode), lngRow);
					vsMeterSizeDescList.Select(lngRow, 1);
					//mnuProcessDeleteMeterSize.Enabled = true;
					cmdProcessDeleteMeterSize.Enabled = true;
					// resize grid
					MeterSizesHeightAdjust();
					Dirty = true;
				}
				else
				{
					MessageBox.Show("You have to choose a number that is between 1-30 and not used already.", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				MessageBox.Show("You must choose a number that is between 1-30.", "Invalid Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
			// Dirty = True
		}

		private void mnuProcessDeleteMeterSize_Click(object sender, System.EventArgs e)
		{
			// this will remove the row from the grid, delete is not final here
			// vbPorter upgrade warning: temp As short, int --> As DialogResult
			DialogResult temp;
			temp = MessageBox.Show("Are you sure you want to delete this category code " + FCConvert.ToString(FCConvert.ToInt32(Math.Round(Conversion.Val(vsMeterSizeDescList.TextMatrix(vsMeterSizeDescList.Row, 0))))) + "?", "Delete", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			// 6 = yes, 7 = no, 2 = cancel
			if (temp == DialogResult.Yes)
			{
				rs.Execute("DELETE FROM MeterSizes WHERE Code = " + FCConvert.ToString(Conversion.Val(vsMeterSizeDescList.TextMatrix(vsMeterSizeDescList.Row, 0))), modExtraModules.strUTDatabase);
				vsMeterSizeDescList.RemoveItem(vsMeterSizeDescList.Row);
				if (vsMeterSizeDescList.Rows <= 1)
				{
					//mnuProcessDeleteMeterSize.Enabled = false;
					cmdProcessDeleteMeterSize.Enabled = false;
				}
				MeterSizesHeightAdjust();
			}
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			Save_TabData_2(tabSetup.SelectedIndex);
			// save the correct tab information
		}

		public void mnuProcessSave_Click()
		{
			mnuProcessSave_Click(mnuProcessSave, new System.EventArgs());
		}

		private void mnuRateTableDelete_Click(object sender, System.EventArgs e)
		{
			// By pressing delete, all the fields will be cleared the record number
			// will be saved for use later and no other records will be disrupted
			// vbPorter upgrade warning: i As short, int --> As DialogResult
			DialogResult result;
			int j;
			result = MessageBox.Show("Are you sure you would like to delete this information?", "Delete Table Information", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
			// yes = 6 no = 7 cancel = 2
			if (result == DialogResult.Yes)
			{
				for (int i = 1; i <= 3; i++)
				{
					for (j = 1; j <= 8; j++)
					{
						vsRateTableS.TextMatrix(j, i, "");
						vsRateTableW.TextMatrix(j, i, "");
					}
					// j
				}
				// i
				rsRate.FindFirstRecord("RateTableNumber", Conversion.Val(cmbRT.Items[cmbRT.SelectedIndex].ToString()));
				if (!rsRate.NoMatch)
				{
					rsRate.Delete();
					maxTable -= 1;
					if (maxTable > 0)
					{
						cmbRT.SelectedIndex = 0;
						//mnuRateTableDelete.Enabled = true;
						cmdRateTableDelete.Enabled = true;
					}
					else
					{
						//mnuRateTableDelete.Enabled = false;
						cmdRateTableDelete.Enabled = false;
					}
					boolRateTableUpdated = true;
					// kk08172015 trout-1186  Prompt to print listing if rates changed
				}
				else
				{
					MessageBox.Show("Rate Table not found.", "Not Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			FillRTCombo();
			cmbRT.SelectedIndex = 0;
		}

		private void mnuRateTableEditDesc_Click(object sender, System.EventArgs e)
		{
			int i;
			// Dim temp As String
			// Dim strTemp As String
			// Dim UserPrompt As String
			// UserPrompt = "You are about to edit the rate table description.  Please enter the new description."
			// 
			// prompt the user for a new rate table number
			// strTemp = InputBox(UserPrompt, "Edit Description", Right(cmbRT.Text, Len(cmbRT.Text) - 5))
			// strTemp = RemoveApostrophe(strTemp)
			// 
			// If strTemp = "" Then
			// Exit Sub
			// Else
			// i = cmbRT.ListIndex                             'save the index
			// temp = Left$(cmbRT.Text, 2) & " - " & strTemp   'create a new string
			// cmbRT.RemoveItem i                              'delete the old
			// cmbRT.AddItem temp                              'add the new string
			// cmbRT.ListIndex = cmbRT.NewIndex
			// End If
			// Dirty = True
			// this will give the user the ability to change the names
			frmEditRateRecordTitles.InstancePtr.Init();
			// this will reload the combo
			boolReloading = true;
			i = cmbRT.SelectedIndex;
			FillRTCombo();
			cmbRT.SelectedIndex = i;
			boolReloading = false;
		}

		private void mnuRateTablesAdd_Click(object sender, System.EventArgs e)
		{
			int i;
			int temp = 0;
			string strTemp = "";
			string UserPrompt;
			clsDRWrapper rsTest = new clsDRWrapper();
			UserPrompt = "You are about to add a new rate table.  Please enter the number that you would like to add";
			do
			{
				// prompt the user for a new rate table number
				strTemp = Interaction.InputBox(UserPrompt, "", null/*, -1, -1*/);
				if (strTemp == "")
				{
					return;
				}
				temp = FCConvert.ToInt32(Math.Round(Conversion.Val(strTemp)));
				UserPrompt = "You are about to add a new rate table.  Please enter the Rate Table Number.";
				if (temp > 0)
				{
					rsTest.OpenRecordset("SELECT * FROM RateTable WHERE RateTableNumber = " + FCConvert.ToString(temp));
					if (rsTest.RecordCount() > 0)
					{
						// if temp <> 0 then test to see if there is a rate table with that number already
						temp = 0;
						UserPrompt = "No duplicate Rate Table numbers.  Please try again.";
					}
				}
			}
			while (!(temp != 0));
			Clear_RateTable();
			maxTable += 1;
			rsRate.AddNew();
			rsRate.Set_Fields("RateTableNumber", temp);
			rsRate.Update();
			rsRate.FindFirstRecord("RateTableNumber", temp);
			//mnuRateTables.Enabled = true;
			cmdRateTablesNext.Enabled = true;
			cmdRateTablesPrevious.Enabled = true;
			cmdRateTablesAdd.Enabled = true;
			cmdRateTableDelete.Enabled = true;
			mnuFileRateTableListing.Enabled = true;
			mnuFileRateTableListingCond.Enabled = true;
			mnuRateTableDelete.Enabled = true;
			mnuRateTableEditDesc.Enabled = true;
			mnuRateTablesNext.Enabled = true;
			mnuRateTablesPrevious.Enabled = true;
			FillRTCombo();
			for (i = 0; i <= cmbRT.Items.Count - 1; i++)
			{
				// If Val(Left$(cmbRT.Text, 2)) = Val(temp) Then
				// Exit For
				// End If
				if (Conversion.Val(Strings.Left(cmbRT.Items[i].ToString(), 2)) == FCConvert.ToInt16(temp))
				{
					break;
				}
			}
			if (i < cmbRT.Items.Count)
			{
				cmbRT.SelectedIndex = i;
			}
			else
			{
				cmbRT.SelectedIndex = cmbRT.NewIndex;
			}
			Reset_RateTables();
		}

		private void Clear_RateTable()
		{
			// clears all the ratetable information so the user can add a new table
			txtRateS[0].Text = "";
			txtRateS[1].Text = "";
			txtRateW[0].Text = "";
			txtRateW[1].Text = "";
			txtMinCons[0].Text = "";
			txtMinCons[1].Text = "";
		}

		private void mnuRateTablesNext_Click(object sender, System.EventArgs e)
		{
			// moves to the next rate table in the list (if any)
			// vbPorter upgrade warning: temp As short, int --> As DialogResult
			DialogResult temp;
			// kk08142015 trout-1185  Make consistant with Previous
			if (Dirty == true)
			{
				temp = MessageBox.Show("Would you like to save your changes?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				// yes = 6 no = 7 cancel = 2
				if (temp == DialogResult.Yes)
				{
					// yes
					Save_RateTable();
				}
				else if (temp == DialogResult.No)
				{
					// no          'kk08142015 trout-1185  If No then don't prompt to save when cmbRT changes
					Dirty = false;
				}
				else if (temp == DialogResult.Cancel)
				{
					// cancel
					return;
				}
			}
			if (cmbRT.SelectedIndex == cmbRT.Items.Count - 1)
			{
				cmbRT.SelectedIndex = 0;
			}
			else
			{
				cmbRT.SelectedIndex = cmbRT.SelectedIndex + 1;
			}
		}

		private void mnuRateTablesPrevious_Click(object sender, System.EventArgs e)
		{
			// moves to the previous rate table in the list (if any)
			// vbPorter upgrade warning: temp As short, int --> As DialogResult
			DialogResult temp;
			if (Dirty == true)
			{
				temp = MessageBox.Show("Would you like to save your changes?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				// yes = 6 no = 7 cancel = 2
				if (temp == DialogResult.Yes)
				{
					// yes
					Save_RateTable();
				}
				else if (temp == DialogResult.No)
				{
					// no          'kk08142015 trout-1185  If No then don't prompt to save when cmbRT changes
					Dirty = false;
				}
				else if (temp == DialogResult.Cancel)
				{
					// cancel
					return;
				}
			}
			if (cmbRT.SelectedIndex == 0)
			{
				cmbRT.SelectedIndex = cmbRT.Items.Count - 1;
			}
			else
			{
				cmbRT.SelectedIndex = cmbRT.SelectedIndex - 1;
			}
		}

		private void tabSetup_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			bool boolReload = false;
			// If PreviousTab = 0 Then txtDAccount(acctIndex).Visible = False
			if (Cancelled == false)
			{
				boolReload = true;
				if (Dirty == true)
				{
					// checks to see if the last data accessed has been updated
					// vbPorter upgrade warning: temp As short, int --> As DialogResult
					DialogResult temp;
					temp = MessageBox.Show("Would you like to save your changes?", "Save Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
					// yes = 6 no = 7 cancel = 2
					if (temp == DialogResult.Yes)
					{
						Save_TabData_2(tabSetup.PreviousTab);
						// prompts user to save data if desired
					}
					else if (temp == DialogResult.No)
					{
						Dirty = false;
					}
					else if (temp == DialogResult.Cancel)
					{
						Dirty = true;
						// still need to have changes saved
						Cancelled = true;
						tabSetup.SelectedIndex = tabSetup.PreviousTab;
						// goes back to the previous tab to keep making changes
						return;
					}
				}
			}
			else
			{
				Cancelled = false;
				boolReload = false;
				// kk08142015 trout-1185  Don't wipe out changes after prompt to save
			}
			//mnuCategory.Visible = false;
			cmdAddCategory.Visible = false;
			cmdDeleteCategory.Visible = false;
			this.Menu = null;
			//mnuRateTables.Visible = false;
			mnuRateTableEditDesc.Visible = false;
			mnuFileRateTableListing.Visible = false;
			mnuFileRateTableListingCond.Visible = false;
			cmdRateTablesNext.Visible = false;
			cmdRateTablesPrevious.Visible = false;
			cmdRateTablesAdd.Visible = false;
			cmdRateTableDelete.Visible = false;
			//mnuAdjust.Visible = false;
			cmdAdjustAdd.Visible = false;
			cmdAdjustDelete.Visible = false;
			//mnuFileMeterSize.Visible = false;
			cmdProcessAddMeterSize.Visible = false;
			cmdProcessDeleteMeterSize.Visible = false;
			//mnuFileBook.Visible = false;
			cmdFileBookAdd.Visible = false;
			cmdFileBookDelete.Visible = false;
			switch (tabSetup.SelectedIndex)
			{
				case 0:
					{
						// Meter Sizes
						if (boolReload)
						{
							// kk08142015 trout-1185  Don't wipe out changes after prompt to save
							Meter_Sizes();
						}
						//mnuFileMeterSize.Visible = true;
						cmdProcessAddMeterSize.Visible = true;
						cmdProcessDeleteMeterSize.Visible = true;
						break;
					}
				case 1:
					{
						// Frequency Codes
						if (boolReload)
						{
							// kk08142015 trout-1185  Don't wipe out changes after prompt to save
							Frequency();
						}
						break;
					}
				case 2:
					{
						// User Category Codes
						if (boolReload)
						{
							// kk08142015 trout-1185  Don't wipe out changes after prompt to save
							Category();
						}
						//mnuCategory.Visible = true;
						cmdAddCategory.Visible = true;
						cmdDeleteCategory.Visible = true;
						break;
					}
				case 3:
					{
						// Account Adjustment
						if (boolReload)
						{
							// kk08142015 trout-1185  Don't wipe out changes after prompt to save
							Account_Adjust();
						}
						//mnuAdjust.Visible = true;
						cmdAdjustAdd.Visible = true;
						cmdAdjustDelete.Visible = true;
						break;
					}
				case 4:
					{
						// Rate Tables
						if (boolReload)
						{
							// kk08142015 trout-1185  Don't wipe out changes after prompt to save
							Rate_Table();
						}
						Menu = MainMenu1;
						//mnuRateTables.Visible = true;
						mnuRateTableEditDesc.Visible = true;
						mnuFileRateTableListing.Visible = true;
						mnuFileRateTableListingCond.Visible = true;
						cmdRateTablesNext.Visible = true;
						cmdRateTablesPrevious.Visible = true;
						cmdRateTablesAdd.Visible = true;
						cmdRateTableDelete.Visible = true;
						break;
					}
				case 5:
					{
						// Book Descriptions
						if (boolReload)
						{
							// kk08142015 trout-1185  Don't wipe out changes after prompt to save
							Book_Descriptions();
						}
						//mnuFileBook.Visible = true;
						cmdFileBookAdd.Visible = true;
						cmdFileBookDelete.Visible = true;
						break;
					}
			}
			//end switch
		}

		private void Meter_Sizes()
		{
			// let the user add and delete meter sizes
			Fill_MeterSizes_Grid();
			MeterSizesHeightAdjust();
		}

		private void Frequency()
		{
			Fill_Frequency_Grid();
			AdjustFrequencyGridHeight();
		}

		private void Category()
		{
			//mnuCategory.Visible = true;          
			//mnuCategory.Enabled = true;
			cmdAddCategory.Enabled = true;
			cmdDeleteCategory.Enabled = true;
			Fill_Category_Grid();
			CategoryHeightAdjust();
		}

		private void Account_Adjust()
		{
			//mnuAdjust.Visible = true;
			cmdAdjustAdd.Visible = true;
			cmdAdjustDelete.Visible = true;
			Fill_Adjust_Grid();
			AdjustHeightAdjust();
		}

		private void Rate_Table()
		{
			FillRTCombo();
			// fills the combo box with eligible rate tables
			Fill_Rate_Tables();
			// fill the flexgrids with information from the tables
			mnuRateTables.Visible = true;
			// let the user move through the different rate tables
			mnuRateTableEditDesc.Visible = true;
			mnuFileRateTableListing.Visible = true;
			mnuFileRateTableListingCond.Visible = true;
			cmdRateTablesNext.Visible = true;
			cmdRateTablesPrevious.Visible = true;
			cmdRateTablesAdd.Visible = true;
			cmdRateTableDelete.Visible = true;
			txtMinCons[0].Focus();
		}

		private void Book_Descriptions()
		{
			Fill_BookDescriptions_Grid();
			AdjustBookGridHeight();
		}

		private void Format_MeterSizes_Grid()
		{
            //FC:FINAL:BSE #3818 remove anchors
            vsMeterSizeDescList.Width = vsMeterSizeDescList.Parent.Width - 100;
            vsMeterSizeDescList.Height = vsMeterSizeDescList.Parent.Height - 100;
			int x;
			// sets width for datatype of col
			vsMeterSizeDescList.Cols = 3;
			vsMeterSizeDescList.ColWidth(0, FCConvert.ToInt32(vsMeterSizeDescList.WidthOriginal * 0.12));
			// Code
			vsMeterSizeDescList.ColWidth(1, FCConvert.ToInt32(vsMeterSizeDescList.WidthOriginal * 0.5));
			// Long Description
			vsMeterSizeDescList.ColWidth(2, FCConvert.ToInt32(vsMeterSizeDescList.WidthOriginal * 0.35));
			// Short Description
			//vsMeterSizeDescList.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// MAL@20070830: Added column alignments
			vsMeterSizeDescList.ColAlignment(0, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsMeterSizeDescList.ColAlignment(1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsMeterSizeDescList.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsMeterSizeDescList.ExtendLastCol = true;
			vsMeterSizeDescList.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsMeterSizeDescList.TextMatrix(0, 0, "Code");
			vsMeterSizeDescList.TextMatrix(0, 1, "Long Description");
			vsMeterSizeDescList.TextMatrix(0, 2, "Short Description");
			vsMeterSizeDescList.EditMaxLength = 25;
		}

		private void Fill_MeterSizes_Grid()
		{
			int x;
			int rc = 0;
			clsDRWrapper rsTemp = new clsDRWrapper();
			strSQL = "SELECT * FROM MeterSizes ORDER BY Code";
			// builds the SQL statement to select all records
			rsTemp.OpenRecordset(strSQL);
			// selects all the records
			Format_MeterSizes_Grid();
			vsMeterSizeDescList.Rows = 1;
			if (rsTemp.EndOfFile() != true && rsTemp.BeginningOfFile() != true)
			{
				// if not at the beginning or the end of the recordset
				rc = rsTemp.RecordCount();
				for (x = 1; x <= rc; x++)
				{
					vsMeterSizeDescList.Rows += 1;
					vsMeterSizeDescList.TextMatrix(x, 0, FCConvert.ToString(x));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					if (!fecherFoundation.FCUtils.IsNull(rsTemp.Get_Fields("Code")))
						vsMeterSizeDescList.TextMatrix(x, 0, FCConvert.ToString(rsTemp.Get_Fields("Code")));
					// fills the grid
					if (!fecherFoundation.FCUtils.IsNull(rsTemp.Get_Fields_String("LongDescription")))
						vsMeterSizeDescList.TextMatrix(x, 1, FCConvert.ToString(rsTemp.Get_Fields_String("LongDescription")));
					if (!fecherFoundation.FCUtils.IsNull(rsTemp.Get_Fields_String("ShortDescription")))
						vsMeterSizeDescList.TextMatrix(x, 2, FCConvert.ToString(rsTemp.Get_Fields_String("ShortDescription")));
					// kgk If Not .EditMode Then .MoveNext       'if not the end of the recordset, go to the next
					rsTemp.MoveNext();
					// if not the end of the recordset, go to the next
				}
				//mnuProcessDeleteMeterSize.Enabled = true;
				cmdProcessDeleteMeterSize.Enabled = true;
			}
			else
			{
				//mnuProcessDeleteMeterSize.Enabled = false;
				cmdProcessDeleteMeterSize.Enabled = false;
			}
		}

		private void MeterSizesHeightAdjust()
		{
			if (((vsMeterSizeDescList.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * vsMeterSizeDescList.Rows)) < (frmTableFileSetup.InstancePtr.Height * 0.55))
			{
				//vsMeterSizeDescList.Height = vsMeterSizeDescList.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * vsMeterSizeDescList.Rows + 75;
			}
			else
			{
				//vsMeterSizeDescList.Height = FCConvert.ToInt32((frmTableFileSetup.InstancePtr.Height * 0.55));
			}
		}

		private bool ValidateMeterSizes()
		{
			bool ValidateMeterSizes = false;
			// Check the grid for mistakes
			// Duplicate codes
			int lngCT;
			int lngRW;
			int lngCode = 0;
			int lngFoundRow = 0;
			ValidateMeterSizes = true;
			if (vsMeterSizeDescList.Rows > 2)
			{
				for (lngRW = 1; lngRW <= vsMeterSizeDescList.Rows - 2; lngRW++)
				{
					lngCode = FCConvert.ToInt32(Math.Round(Conversion.Val(vsMeterSizeDescList.TextMatrix(lngRW, 0))));
					if (lngCode > 0)
					{
						lngFoundRow = vsMeterSizeDescList.FindRow(lngCode, lngRW + 1, 0);
						if (lngFoundRow > 0)
						{
							ValidateMeterSizes = false;
							MessageBox.Show("Duplicate code found, please enter a unique code.", "Duplicate Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							vsMeterSizeDescList.Row = lngFoundRow;
							vsMeterSizeDescList.Col = 0;
							if (vsMeterSizeDescList.Visible)
							{
								vsMeterSizeDescList.Focus();
							}
						}
					}
					else
					{
						ValidateMeterSizes = false;
						MessageBox.Show("Please enter a valid code.", "Not A Valid Code", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsMeterSizeDescList.Row = lngRW;
						vsMeterSizeDescList.Col = 0;
						if (vsMeterSizeDescList.Visible)
						{
							vsMeterSizeDescList.Focus();
						}
						break;
					}
				}
			}
			return ValidateMeterSizes;
		}

		private void Format_Frequency_Grid()
		{
            //FC:FINAL:BSE #3818 remove anchors
            vsFrequency.Width = vsFrequency.Parent.Width - 100;
            vsFrequency.Height = vsFrequency.Parent.Height - 100;
			int x;
			// counter variable
			// sets width for datatype of col
			vsFrequency.Cols = 3;
			vsFrequency.ColWidth(0, FCConvert.ToInt32(vsFrequency.WidthOriginal * 0.1));
			// Code
			vsFrequency.ColWidth(1, FCConvert.ToInt32(vsFrequency.WidthOriginal * 0.5));
			// Long Description
			vsFrequency.ColWidth(2, FCConvert.ToInt32(vsFrequency.WidthOriginal * 0.4));
			// Short Description
			//vsFrequency.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsFrequency.ExtendLastCol = true;
			vsFrequency.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsFrequency.TextMatrix(0, 0, "Code");
			vsFrequency.TextMatrix(0, 1, "Long Description");
			vsFrequency.TextMatrix(0, 2, "Short Description");
			vsFrequency.EditMaxLength = 25;
			// For x = 1 To 10
			// .TextMatrix(x, 0) = x
			// Next
		}

		private void Fill_Frequency_Grid()
		{
			int x;
			int rc;
			clsDRWrapper rsTemp = new clsDRWrapper();
			strSQL = "SELECT * FROM Frequency ORDER BY Code";
			// builds the SQL statement to select all records
			rsTemp.OpenRecordset(strSQL);
			// selects all the records
			vsFrequency.Rows = 1;
			Format_Frequency_Grid();
			while (!rsTemp.EndOfFile())
			{
				vsFrequency.AddItem("");
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				if (!fecherFoundation.FCUtils.IsNull(rsTemp.Get_Fields("Code")))
					vsFrequency.TextMatrix(vsFrequency.Rows - 1, 0, FCConvert.ToString(rsTemp.Get_Fields("Code")));
				// fills the grid
				if (!fecherFoundation.FCUtils.IsNull(rsTemp.Get_Fields_String("LongDescription")))
					vsFrequency.TextMatrix(vsFrequency.Rows - 1, 1, FCConvert.ToString(rsTemp.Get_Fields_String("LongDescription")));
				if (!fecherFoundation.FCUtils.IsNull(rsTemp.Get_Fields_String("ShortDescription")))
					vsFrequency.TextMatrix(vsFrequency.Rows - 1, 2, FCConvert.ToString(rsTemp.Get_Fields_String("ShortDescription")));
				rsTemp.MoveNext();
				// if not the end of the recordset, go to the next
			}
		}

		private void AdjustFrequencyGridHeight()
		{
			if (((vsFrequency.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * vsFrequency.Rows)) + 75 < (frmTableFileSetup.InstancePtr.Height * 0.55))
			{
				//vsFrequency.Height = (vsFrequency.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * vsFrequency.Rows) + 75;
			}
			else
			{
				//vsFrequency.Height = FCConvert.ToInt32((frmTableFileSetup.InstancePtr.Height * 0.55));
			}
		}

		private void Format_Category_Grid()
		{
            //FC:FINAL:BSE #3818 remove anchors
            vsCategory.Width = vsCategory.Parent.Width - 100;
            vsCategory.Height = vsCategory.Parent.Height - 100;
            // sets width for datatype of col
            vsCategory.FixedCols = 0;
            vsCategory.Cols = 7;
			vsCategory.EditMaxLength = 20;
			// MAL@20070831: Adjusted the column widths to allow the WT and ST columns to be seen
			vsCategory.ColWidth(lngCatColCode, FCConvert.ToInt32(vsCategory.WidthOriginal * 0.08));
			// Code
			// .ColWidth(lngCatColDescription) = .Width * 0.3              'Description
			vsCategory.ColWidth(lngCatColDescription, FCConvert.ToInt32(vsCategory.WidthOriginal * 0.27));
			// Description
			vsCategory.ColWidth(lngCatColShort, FCConvert.ToInt32(vsCategory.WidthOriginal * 0.2));
			// Short Description
			vsCategory.ColWidth(lngCatColWAccount, FCConvert.ToInt32(vsCategory.WidthOriginal * 0.17));
			// Water Account
			vsCategory.ColWidth(lngCatColSAccount, FCConvert.ToInt32(vsCategory.WidthOriginal * 0.17));
			// Sewer Account
			// .ColWidth(lngCatColWTaxable) = .Width * 0.04                'Water Taxable
			// .ColWidth(lngCatColSTaxable) = .Width * 0.04                'Sewer Taxable
			vsCategory.ColWidth(lngCatColWTaxable, FCConvert.ToInt32(vsCategory.WidthOriginal * 0.05));
			// Water Taxable
			vsCategory.ColWidth(lngCatColSTaxable, FCConvert.ToInt32(vsCategory.WidthOriginal * 0.05));
			// Sewer Taxable
			vsCategory.ExtendLastCol = true;
			vsCategory.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsCategory.ColDataType(lngCatColWTaxable, FCGrid.DataTypeSettings.flexDTBoolean);
			vsCategory.ColDataType(lngCatColSTaxable, FCGrid.DataTypeSettings.flexDTBoolean);
			vsCategory.TextMatrix(0, lngCatColCode, "Code");
			vsCategory.TextMatrix(0, lngCatColDescription, "Long Description");
			vsCategory.TextMatrix(0, lngCatColShort, "Short Description");
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				vsCategory.TextMatrix(0, lngCatColWAccount, "Stormwater Account");
			}
			else
			{
				vsCategory.TextMatrix(0, lngCatColWAccount, "Water Account");
			}
			vsCategory.TextMatrix(0, lngCatColSAccount, "Sewer Account");
			vsCategory.TextMatrix(0, lngCatColWTaxable, "WT");
			vsCategory.TextMatrix(0, lngCatColSTaxable, "ST");
			vsCategory.EditMaxLength = 25;
			//FC:FINAL:DDU:#1016 - aligned headers with columns
			//vsCategory.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsCategory.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsCategory.ColAlignment(lngCatColCode, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCategory.ColAlignment(lngCatColDescription, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCategory.ColAlignment(lngCatColShort, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCategory.ColAlignment(lngCatColWAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCategory.ColAlignment(lngCatColSAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCategory.ColAlignment(lngCatColWTaxable, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsCategory.ColAlignment(lngCatColSTaxable, FCGrid.AlignmentSettings.flexAlignLeftCenter);
		}

		private void Fill_Category_Grid()
		{
			int x;
			// temporary variable
			strSQL = "SELECT * FROM Category ORDER BY Code Asc";
			// builds the SQL statement to select all records
			rs.OpenRecordset(strSQL);
			// selects all the records
			Format_Category_Grid();
			vsCategory.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if not at the beginning or the end of the recordset
				for (x = 1; x <= rs.RecordCount(); x++)
				{
					vsCategory.Rows = x + 2;
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("Code")))
						vsCategory.TextMatrix(x, lngCatColCode, FCConvert.ToString(rs.Get_Fields("Code")));
					// fills the grid
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("LongDescription")))
						vsCategory.TextMatrix(x, lngCatColDescription, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ShortDescription")))
						vsCategory.TextMatrix(x, lngCatColShort, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("WaterAccount")))
						vsCategory.TextMatrix(x, lngCatColWAccount, FCConvert.ToString(rs.Get_Fields_String("WaterAccount")));
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("SewerAccount")))
						vsCategory.TextMatrix(x, lngCatColSAccount, FCConvert.ToString(rs.Get_Fields_String("SewerAccount")));
					// add taxable amount
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("WTaxable")))
					{
						vsCategory.TextMatrix(x, lngCatColWTaxable, FCConvert.ToString(true));
					}
					else
					{
						vsCategory.TextMatrix(x, lngCatColWTaxable, FCConvert.ToString(false));
					}
					if (FCConvert.ToBoolean(rs.Get_Fields_Boolean("STaxable")))
					{
						vsCategory.TextMatrix(x, lngCatColSTaxable, FCConvert.ToString(true));
					}
					else
					{
						vsCategory.TextMatrix(x, lngCatColSTaxable, FCConvert.ToString(false));
					}
					if (!rs.EndOfFile())
						rs.MoveNext();
					// if not the end of the recordset, go to the next
				}
				vsCategory.Rows -= 1;
				CategoryHeightAdjust();
			}
		}

		private void CategoryHeightAdjust()
		{
			// this will calculate the height of the flex grid depending on how many rows are open with a max height of the 25% of the form height
			int ExposedRows = 0;
			double HPercent;
			HPercent = 0.5729;
			// all rows are shown, no subtotals
			ExposedRows = vsCategory.Rows;
			if (((vsCategory.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows) + 75) < (frmTableFileSetup.InstancePtr.Height * HPercent))
			{
				//vsCategory.Height = vsCategory.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows + 75;
			}
			else
			{
				//vsCategory.Height = FCConvert.ToInt32((frmTableFileSetup.InstancePtr.Height * HPercent));
			}
		}

		private void AdjustHeightAdjust()
		{
			// this will calculate the height of the flex grid depending on how many rows are open with a max height of the hpercent% of the form height
			//int ExposedRows = 0;
			//double HPercent;
			//HPercent = 0.5729;
			// percentage of the screen height, used to calculate the max height of the flex grid
			// all rows are shown, no subtotals
			//ExposedRows = vsAdjust.Rows;
			//if (((vsAdjust.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows) + 75) < (frmTableFileSetup.InstancePtr.Height * HPercent))
			//{
				//vsAdjust.Height = vsAdjust.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * ExposedRows + 75;
			//}
			//else
			//{
				//vsAdjust.Height = FCConvert.ToInt32((frmTableFileSetup.InstancePtr.Height * HPercent));
			//}
		}

		private void Format_Adjust_Grid()
		{
            //FC:FINAL:BSE #3818 remove anchors
            vsAdjust.Width = vsAdjust.Parent.Width - 100;
            vsAdjust.Height = vsAdjust.Parent.Height - 100;
			// sets width for datatype of col
			vsAdjust.Cols = 6;
			vsAdjust.ColWidth(0, FCConvert.ToInt32(vsAdjust.WidthOriginal * 0.08));
			// Code
			vsAdjust.ColWidth(1, FCConvert.ToInt32(vsAdjust.WidthOriginal * 0.29));
			// LongDescription
			vsAdjust.ColWidth(2, FCConvert.ToInt32(vsAdjust.WidthOriginal * 0.2));
			// ShortDescription
			vsAdjust.ColWidth(3, FCConvert.ToInt32(vsAdjust.WidthOriginal * 0.1));
			// Service
			vsAdjust.ColWidth(4, FCConvert.ToInt32(vsAdjust.WidthOriginal * 0.15));
			// Amount
			vsAdjust.ColFormat(4, "#,##0.00");
            vsAdjust.ColAlignment(4, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsAdjust.ColWidth(5, FCConvert.ToInt32(vsAdjust.WidthOriginal * 0.15));
			// Account
			//vsAdjust.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 5, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsAdjust.ExtendLastCol = true;
			vsAdjust.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsAdjust.TextMatrix(0, 0, "Code");
			vsAdjust.TextMatrix(0, 1, "Long Description");
			vsAdjust.TextMatrix(0, 2, "Short Description");
			vsAdjust.TextMatrix(0, 3, "Service");
			vsAdjust.TextMatrix(0, 4, "Amount");
			vsAdjust.TextMatrix(0, 5, "Account");
			vsAdjust.EditMaxLength = 25;
		}

		private void Fill_Adjust_Grid()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int x;
				// temporary variable
				int rc = 0;
				strSQL = "SELECT * FROM Adjust ORDER BY Code";
				// builds the SQL statement to select all records
				rs.OpenRecordset(strSQL);
				// selects all the records
				vsAdjust.Rows = 1;
				Format_Adjust_Grid();
				if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
				{
					// if not at the beginning or the end of the recordset
					rc = rs.RecordCount();
					vsAdjust.Rows = rc + 1;
					// set the amount of rows needed
					for (x = 1; x <= rc; x++)
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("Code")))
							vsAdjust.TextMatrix(x, 0, FCConvert.ToString(rs.Get_Fields("Code")));
						// fills the grid
						if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("LongDescription")))
							vsAdjust.TextMatrix(x, 1, FCConvert.ToString(rs.Get_Fields_String("LongDescription")));
						if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("ShortDescription")))
							vsAdjust.TextMatrix(x, 2, FCConvert.ToString(rs.Get_Fields_String("ShortDescription")));
						if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Service")), 1) == "W")
						{
							// kk trouts-6 03012013  Change Water to Stormwater for Bangor
							if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
							{
								vsAdjust.TextMatrix(x, 3, "Stmwater");
							}
							else
							{
								vsAdjust.TextMatrix(x, 3, "Water");
							}
						}
						else if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Service")), 1) == "S")
						{
							vsAdjust.TextMatrix(x, 3, "Sewer");
						}
						else if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("Service")), 1) == "B")
						{
							vsAdjust.TextMatrix(x, 3, "Both");
						}
						else
						{
							vsAdjust.TextMatrix(x, 3, "");
						}
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("Amount")))
							vsAdjust.TextMatrix(x, 4, FCConvert.ToString(rs.Get_Fields("Amount")));
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
						if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields("Account")))
							vsAdjust.TextMatrix(x, 5, FCConvert.ToString(rs.Get_Fields("Account")));
						if (!rs.EndOfFile())
							rs.MoveNext();
						// if not the end of the recordset, go to the next
					}
				}
				if (vsAdjust.Rows <= 1)
				{
					//mnuAdjustDelete.Enabled = false;
					cmdAdjustDelete.Enabled = false;
				}
				else
				{
					//mnuAdjustDelete.Enabled = true;
					cmdAdjustDelete.Enabled = true;
				}
				AdjustHeightAdjust();
				// this function adjusts the height of the flex grid
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Adjustments", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Format_Rate_tables()
		{
			int x;
			// table 1
			// sets width for datatype of col
			vsRateTableW.Cols = 5;
			vsRateTableW.Rows = 9;
			vsRateTableW.ColWidth(0, FCConvert.ToInt32(vsRateTableW.WidthOriginal * 0.07));
			// Number
			vsRateTableW.ColWidth(1, FCConvert.ToInt32(vsRateTableW.WidthOriginal * 0.26));
			// Over what consumption
			vsRateTableW.ColWidth(2, FCConvert.ToInt32(vsRateTableW.WidthOriginal * 0.26));
			// Thru what consumption
			vsRateTableW.ColWidth(3, FCConvert.ToInt32(vsRateTableW.WidthOriginal * 0.26));
			// Rate at that consumption
			vsRateTableW.ColWidth(4, FCConvert.ToInt32(vsRateTableW.WidthOriginal * 0.15));
			// Step Size
			//vsRateTableW.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsRateTableW.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsRateTableW.ColFormat(3, "0.000000");
			vsRateTableW.EditMaxLength = 9;
			vsRateTableW.ExtendLastCol = true;
			vsRateTableW.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			vsRateTableW.TextMatrix(0, 0, "");
			vsRateTableW.TextMatrix(0, 1, "Over");
			vsRateTableW.TextMatrix(0, 2, "Thru");
			vsRateTableW.TextMatrix(0, 3, "Rate");
			vsRateTableW.TextMatrix(0, 4, "Step");
			for (x = 1; x <= 8; x++)
			{
				vsRateTableW.TextMatrix(x, 0, FCConvert.ToString(x));
			}
			// adjust the height of the grid to match the rows...
			//vsRateTableW.Height = vsRateTableW.RowHeight(0) * 9 + 75;
			// table 2
			// sets width for datatype of col
			vsRateTableS.Cols = 5;
			vsRateTableS.Rows = 9;
			vsRateTableS.ColWidth(0, FCConvert.ToInt32(vsRateTableS.WidthOriginal * 0.07));
			// Number
			vsRateTableS.ColWidth(1, FCConvert.ToInt32(vsRateTableS.WidthOriginal * 0.26));
			// Over what consumption
			vsRateTableS.ColWidth(2, FCConvert.ToInt32(vsRateTableS.WidthOriginal * 0.26));
			// Thru what consumption
			vsRateTableS.ColWidth(3, FCConvert.ToInt32(vsRateTableS.WidthOriginal * 0.26));
			// Rate at that consumption
			vsRateTableS.ColWidth(4, FCConvert.ToInt32(vsRateTableS.WidthOriginal * 0.15));
			// Step Size
			//vsRateTableS.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsRateTableS.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsRateTableS.ColFormat(3, "0.000000");
			vsRateTableS.EditMaxLength = 9;
			vsRateTableS.ExtendLastCol = true;
			vsRateTableS.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			vsRateTableS.TextMatrix(0, 0, "");
			vsRateTableS.TextMatrix(0, 1, "Over");
			vsRateTableS.TextMatrix(0, 2, "Thru");
			vsRateTableS.TextMatrix(0, 3, "Rate");
			vsRateTableS.TextMatrix(0, 4, "Step");
			for (x = 1; x <= 8; x++)
			{
				vsRateTableS.TextMatrix(x, 0, FCConvert.ToString(x));
			}
			//vsRateTableS.Height = vsRateTableS.RowHeight(0) * 9 + 75;
			// adj tables
			// sets width for datatype of col
			vsWMisc.Cols = 4;
			vsWMisc.Rows = 3;
			vsWMisc.ColWidth(0, FCConvert.ToInt32(vsWMisc.WidthOriginal * 0.0));
			// Check Box Taxable
			vsWMisc.ColWidth(1, FCConvert.ToInt32(vsWMisc.WidthOriginal * 0.42));
			// Description
			vsWMisc.ColWidth(2, FCConvert.ToInt32(vsWMisc.WidthOriginal * 0.22));
			// Amount
			vsWMisc.ColWidth(3, FCConvert.ToInt32(vsWMisc.WidthOriginal * 0.33));
			// Account
			vsWMisc.ColFormat(2, "0.00");
            vsWMisc.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
			//vsWMisc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsWMisc.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsWMisc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsWMisc.Rows - 1, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsWMisc.EditMaxLength = 10;
			vsWMisc.ExtendLastCol = true;
			vsWMisc.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			vsWMisc.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsWMisc.TextMatrix(0, 0, "");
			vsWMisc.TextMatrix(0, 1, "Description");
			vsWMisc.TextMatrix(0, 2, "Amount");
			vsWMisc.TextMatrix(0, 3, "Account");
			//vsWMisc.Height = (vsWMisc.RowHeight(0) * 3) + 75;
			// sets width for datatype of col
			vsSMisc.Cols = 4;
			vsSMisc.Rows = 3;
			vsSMisc.ColWidth(0, FCConvert.ToInt32(vsSMisc.WidthOriginal * 0.0));
			// Check Box Taxable
			vsSMisc.ColWidth(1, FCConvert.ToInt32(vsSMisc.WidthOriginal * 0.42));
			// Description
			vsSMisc.ColWidth(2, FCConvert.ToInt32(vsSMisc.WidthOriginal * 0.22));
			// Amount
			vsSMisc.ColWidth(3, FCConvert.ToInt32(vsSMisc.WidthOriginal * 0.33));
			// Account
			vsSMisc.ColFormat(2, "0.00");
            vsSMisc.ColAlignment(2, FCGrid.AlignmentSettings.flexAlignRightCenter);
            //vsSMisc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, vsSMisc.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			//vsSMisc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, vsSMisc.Rows - 1, 0, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			vsSMisc.EditMaxLength = 10;
			vsSMisc.ExtendLastCol = true;
			vsSMisc.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
			vsSMisc.ColDataType(0, FCGrid.DataTypeSettings.flexDTBoolean);
			vsSMisc.TextMatrix(0, 0, "");
			vsSMisc.TextMatrix(0, 1, "Description");
			vsSMisc.TextMatrix(0, 2, "Amount");
			vsSMisc.TextMatrix(0, 3, "Account");
			//vsSMisc.Height = (vsSMisc.RowHeight(0) * 3) + 75;
			//FC:FINAL:CHN - issue #1025: fix allow to enter numbers.
			vsRateTableS.EditingControlShowing -= this.VsRateTableS_EditingControlShowing;
			vsRateTableS.EditingControlShowing += this.VsRateTableS_EditingControlShowing;
			vsRateTableW.EditingControlShowing -= this.VsRateTableW_EditingControlShowing;
			vsRateTableW.EditingControlShowing += this.VsRateTableW_EditingControlShowing;
			vsWMisc.EditingControlShowing -= this.VsWMisc_EditingControlShowing;
			vsWMisc.EditingControlShowing += this.VsWMisc_EditingControlShowing;
			vsSMisc.EditingControlShowing -= this.VsSMisc_EditingControlShowing;
			vsSMisc.EditingControlShowing += this.VsSMisc_EditingControlShowing;
		}

		private void Fill_Rate_Tables()
		{
			rsRate = new clsDRWrapper();
			strSQL = "SELECT * FROM RateTable ORDER BY RateTableNumber Asc";
			// builds the SQL statement to select all records
			rsRate.OpenRecordset(strSQL);
			// selects all the records
			if (rsRate.RecordCount() != 0)
			{
				// stores the rate tables in rsRate
				//mnuRateTableDelete.Enabled = true;
				cmdRateTableDelete.Enabled = true;
				mnuRateTableEditDesc.Enabled = true;
				//mnuRateTablesNext.Enabled = true;
				cmdRateTablesNext.Enabled = true;
				//mnuRateTablesPrevious.Enabled = true;
				cmdRateTablesPrevious.Enabled = true;
				maxTable = rsRate.RecordCount();
				// finds number of rate tables in recordset
				CurrentRT = FCConvert.ToInt32(rsRate.Get_Fields_Int32("RateTableNumber"));
				Format_Rate_tables();
				if (maxTable != 0)
				{
					cmbRT.SelectedIndex = 0;
				}
				else
				{
					MessageBox.Show("No Rate Tables found.", "Missing Rate Tables", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
			}
			else
			{
				//mnuRateTableDelete.Enabled = false;
				cmdRateTableDelete.Enabled = false;
				mnuRateTableEditDesc.Enabled = false;
				//mnuRateTablesNext.Enabled = false;
				cmdRateTablesNext.Enabled = false;
				//mnuRateTablesPrevious.Enabled = false;
				cmdRateTablesPrevious.Enabled = false;
			}
		}

		private void Reset_RateTables()
		{
			int x;
			// insert values into the grids
			for (x = 1; x <= 8; x++)
			{
				if (x == 1)
				{
					vsRateTableW.TextMatrix(x, 1, FCConvert.ToString(0));
				}
				else
				{
					// TODO Get_Fields: Field [WThru] not found!! (maybe it is an alias?)
					vsRateTableW.TextMatrix(x, 1, FCConvert.ToString(rsRate.Get_Fields("WThru" + FCConvert.ToString(x - 1))));
				}
				// TODO Get_Fields: Field [WThru] not found!! (maybe it is an alias?)
				vsRateTableW.TextMatrix(x, 2, FCConvert.ToString(rsRate.Get_Fields("WThru" + FCConvert.ToString(x))));
				// TODO Get_Fields: Field [WRate] not found!! (maybe it is an alias?)
				if (FCConvert.ToInt32(rsRate.Get_Fields("WRate" + FCConvert.ToString(x))) != 0)
				{
					// TODO Get_Fields: Field [WRate] not found!! (maybe it is an alias?)
					vsRateTableW.TextMatrix(x, 3, Strings.Format(rsRate.Get_Fields("WRate" + FCConvert.ToString(x)), "#0.000000"));
				}
				else
				{
					vsRateTableW.TextMatrix(x, 3, FCConvert.ToString(0));
				}
				// TODO Get_Fields: Field [WStep] not found!! (maybe it is an alias?)
				if (rsRate.Get_Fields("WStep" + FCConvert.ToString(x)) > 0)
				{
					// TODO Get_Fields: Field [WStep] not found!! (maybe it is an alias?)
					vsRateTableW.TextMatrix(x, 4, FCConvert.ToString(rsRate.Get_Fields("WStep" + FCConvert.ToString(x))));
				}
				else
				{
					vsRateTableW.TextMatrix(x, 4, FCConvert.ToString(1));
				}
			}
			if (vsRateTableS.Rows > 1)
			{
				for (x = 1; x <= 8; x++)
				{
					if (x == 1)
					{
						vsRateTableS.TextMatrix(x, 1, FCConvert.ToString(0));
					}
					else
					{
						// TODO Get_Fields: Field [SThru] not found!! (maybe it is an alias?)
						vsRateTableS.TextMatrix(x, 1, FCConvert.ToString(rsRate.Get_Fields("SThru" + FCConvert.ToString(x - 1))));
					}
					// TODO Get_Fields: Field [SThru] not found!! (maybe it is an alias?)
					vsRateTableS.TextMatrix(x, 2, FCConvert.ToString(rsRate.Get_Fields("SThru" + FCConvert.ToString(x))));
					// TODO Get_Fields: Field [SRate] not found!! (maybe it is an alias?)
					if (FCConvert.ToInt32(rsRate.Get_Fields("SRate" + FCConvert.ToString(x))) != 0)
					{
						// TODO Get_Fields: Field [SRate] not found!! (maybe it is an alias?)
						vsRateTableS.TextMatrix(x, 3, Strings.Format(rsRate.Get_Fields("SRate" + FCConvert.ToString(x)), "#0.000000"));
					}
					else
					{
						vsRateTableS.TextMatrix(x, 3, FCConvert.ToString(0));
					}
					// TODO Get_Fields: Field [SStep] not found!! (maybe it is an alias?)
					if (rsRate.Get_Fields("SStep" + FCConvert.ToString(x)) > 0)
					{
						// TODO Get_Fields: Field [SStep] not found!! (maybe it is an alias?)
						vsRateTableS.TextMatrix(x, 4, FCConvert.ToString(rsRate.Get_Fields("SStep" + FCConvert.ToString(x))));
					}
					else
					{
						vsRateTableS.TextMatrix(x, 4, FCConvert.ToString(1));
					}
				}
			}
			// minimun consumtion/charge/flat rate at top of form
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Int32("MinConsW")))
			{
				txtMinCons[0].Text = FCConvert.ToString(rsRate.Get_Fields_Int32("MinConsW"));
			}
			else
			{
				txtMinCons[0].Text = "0";
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Int32("MinConsS")))
			{
				txtMinCons[1].Text = FCConvert.ToString(rsRate.Get_Fields_Int32("MinConsS"));
			}
			else
			{
				txtMinCons[1].Text = "0";
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Double("MinChargeW")))
			{
				txtRateW[0].Text = Strings.Format(rsRate.Get_Fields_Double("MinChargeW"), "#,##0.00");
			}
			else
			{
				txtRateW[0].Text = "0";
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Double("MinChargeS")))
			{
				txtRateS[0].Text = Strings.Format(rsRate.Get_Fields_Double("MinChargeS"), "#,##0.00");
			}
			else
			{
				txtRateS[0].Text = "0";
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Double("FlatRateW")))
			{
				txtRateW[1].Text = Strings.Format(rsRate.Get_Fields_Double("FlatRateW"), "#,##0.00");
			}
			else
			{
				txtRateW[1].Text = "0";
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Double("FlatRateS")))
			{
				txtRateS[1].Text = Strings.Format(rsRate.Get_Fields_Double("FlatRateS"), "#,##0.00");
			}
			else
			{
				txtRateS[1].Text = "0";
			}
			// find out if the misc amounts are taxable
			if (FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("TaxableW1")))
			{
				vsWMisc.TextMatrix(1, 0, FCConvert.ToString(0));
				// -1
			}
			else
			{
				vsWMisc.TextMatrix(1, 0, FCConvert.ToString(0));
			}
			if (FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("TaxableW2")))
			{
				vsWMisc.TextMatrix(2, 0, FCConvert.ToString(0));
				// -1
			}
			else
			{
				vsWMisc.TextMatrix(2, 0, FCConvert.ToString(0));
			}
			if (FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("TaxableS1")))
			{
				vsSMisc.TextMatrix(1, 0, FCConvert.ToString(0));
				// -1
			}
			else
			{
				vsSMisc.TextMatrix(1, 0, FCConvert.ToString(0));
			}
			if (FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("TaxableS2")))
			{
				vsSMisc.TextMatrix(2, 0, FCConvert.ToString(0));
				// -1
			}
			else
			{
				vsSMisc.TextMatrix(2, 0, FCConvert.ToString(0));
			}
			// misc charges for this type of account
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_String("MiscChargeW1")))
			{
				vsWMisc.TextMatrix(1, 1, FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeW1")));
			}
			else
			{
				vsWMisc.TextMatrix(1, 1, "");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_String("MiscChargeW2")))
			{
				vsWMisc.TextMatrix(2, 1, FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeW2")));
			}
			else
			{
				vsWMisc.TextMatrix(2, 1, "");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_String("MiscChargeS1")))
			{
				vsSMisc.TextMatrix(1, 1, FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeS1")));
			}
			else
			{
				vsSMisc.TextMatrix(1, 1, "");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_String("MiscChargeS2")))
			{
				vsSMisc.TextMatrix(2, 1, FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeS2")));
			}
			else
			{
				vsSMisc.TextMatrix(2, 1, "");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Double("MiscChargeAmountW1")))
			{
				vsWMisc.TextMatrix(1, 2, Strings.Format(rsRate.Get_Fields_Double("MiscChargeAmountW1"), "#,##0.00"));
			}
			else
			{
				vsWMisc.TextMatrix(1, 2, "0");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Double("MiscChargeAmountW2")))
			{
				vsWMisc.TextMatrix(2, 2, Strings.Format(rsRate.Get_Fields_Double("MiscChargeAmountW2"), "#,##0.00"));
			}
			else
			{
				vsWMisc.TextMatrix(2, 2, "0");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Double("MiscChargeAmountS1")))
			{
				vsSMisc.TextMatrix(1, 2, Strings.Format(rsRate.Get_Fields_Double("MiscChargeAmountS1"), "#,##0.00"));
			}
			else
			{
				vsSMisc.TextMatrix(1, 2, "0");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_Double("MiscChargeAmountS2")))
			{
				vsSMisc.TextMatrix(2, 2, Strings.Format(rsRate.Get_Fields_Double("MiscChargeAmountS2"), "#,##0.00"));
			}
			else
			{
				vsSMisc.TextMatrix(2, 2, "0");
			}
			// account boxes
			// water
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_String("MiscChargeAccountW1")))
			{
				vsWMisc.TextMatrix(1, 3, FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeAccountW1")));
			}
			else
			{
				vsWMisc.TextMatrix(1, 3, "");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_String("MiscChargeAccountW2")))
			{
				vsWMisc.TextMatrix(2, 3, FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeAccountW2")));
			}
			else
			{
				vsWMisc.TextMatrix(2, 3, "");
			}
			// sewer
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_String("MiscChargeAccountS1")))
			{
				vsSMisc.TextMatrix(1, 3, FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeAccountS1")));
			}
			else
			{
				vsSMisc.TextMatrix(1, 3, "");
			}
			if (!fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields_String("MiscChargeAccountS2")))
			{
				vsSMisc.TextMatrix(2, 3, FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeAccountS2")));
			}
			else
			{
				vsSMisc.TextMatrix(2, 3, "");
			}
			if (FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("CombineToMisc1")))
			{
				chkCombineToMisc.CheckState = Wisej.Web.CheckState.Checked;
			}
			else
			{
				chkCombineToMisc.CheckState = Wisej.Web.CheckState.Unchecked;
			}
			Dirty = false;
		}

		private void Format_BookDescriptions()
		{
            //FC:FINAL:BSE #3818 remove anchors 
            vsBookDescriptions.Width = vsBookDescriptions.Parent.Width - 100;
            vsBookDescriptions.Height = vsBookDescriptions.Parent.Height - 100;
			// sets width for datatype of col
			vsBookDescriptions.Cols = 8;
			vsBookDescriptions.ColWidth(0, FCConvert.ToInt32(vsBookDescriptions.WidthOriginal * 0.05));
			// Number
			vsBookDescriptions.ColWidth(1, FCConvert.ToInt32(vsBookDescriptions.WidthOriginal * 0.22));
			// Description
			vsBookDescriptions.ColWidth(2, FCConvert.ToInt32(vsBookDescriptions.WidthOriginal * 0.15));
			// Status
			vsBookDescriptions.ColWidth(3, FCConvert.ToInt32(vsBookDescriptions.WidthOriginal * 0.11));
			// Cleared Date
			vsBookDescriptions.ColWidth(4, FCConvert.ToInt32(vsBookDescriptions.WidthOriginal * 0.12));
			// Data Entered Date
			vsBookDescriptions.ColWidth(5, FCConvert.ToInt32(vsBookDescriptions.WidthOriginal * 0.11));
			// Calculated Date
			vsBookDescriptions.ColWidth(6, FCConvert.ToInt32(vsBookDescriptions.WidthOriginal * 0.11));
			// Edited Date
			vsBookDescriptions.ColWidth(7, FCConvert.ToInt32(vsBookDescriptions.WidthOriginal * 0.11));
			// Billed Date
			vsBookDescriptions.ExtendLastCol = true;
			vsBookDescriptions.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
			vsBookDescriptions.TextMatrix(0, 0, "Book");
			vsBookDescriptions.TextMatrix(0, 1, "Description");
			vsBookDescriptions.TextMatrix(0, 2, "Status");
			vsBookDescriptions.TextMatrix(0, 3, "Cleared");
			vsBookDescriptions.TextMatrix(0, 4, "Data Entered");
			vsBookDescriptions.TextMatrix(0, 5, "Calculated");
			vsBookDescriptions.TextMatrix(0, 6, "Edited");
			vsBookDescriptions.TextMatrix(0, 7, "Billed");
			vsBookDescriptions.EditMaxLength = 20;
		}

		private void Fill_BookDescriptions_Grid()
		{
			int x;
			// temporary variable
			strSQL = "SELECT * FROM Book ORDER BY BookNumber";
			// builds the SQL statement to select all records
			rs.OpenRecordset(strSQL);
			// selects all the records
			Format_BookDescriptions();
			vsBookDescriptions.Rows = 1;
			if (rs.EndOfFile() != true && rs.BeginningOfFile() != true)
			{
				// if not at the beginning or the end of the recordset
				vsBookDescriptions.Rows = rs.RecordCount() + 1;
				for (x = 1; x <= vsBookDescriptions.Rows - 1; x++)
				{
					vsBookDescriptions.TextMatrix(x, 0, FCConvert.ToString(x));
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_Int32("BookNumber")))
						vsBookDescriptions.TextMatrix(x, 0, FCConvert.ToString(rs.Get_Fields_Int32("BookNumber")));
					// fills the grid
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("Description")))
						vsBookDescriptions.TextMatrix(x, 1, FCConvert.ToString(rs.Get_Fields_String("Description")));
					if (!fecherFoundation.FCUtils.IsNull(rs.Get_Fields_String("CurrentStatus")))
					{
						if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("CurrentStatus")), 1) == "C")
						{
							vsBookDescriptions.TextMatrix(x, 2, "Cleared");
						}
						else if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("CurrentStatus")), 1) == "D")
						{
							vsBookDescriptions.TextMatrix(x, 2, "Data Entry");
						}
						else if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("CurrentStatus")), 1) == "X")
						{
							vsBookDescriptions.TextMatrix(x, 2, "Calculated");
							// kk03302015 trouts-11  Change Billed Edit to Calc and Edit
						}
						else if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("CurrentStatus")), 1) == "E")
						{
							vsBookDescriptions.TextMatrix(x, 2, "Calc and Edit");
						}
						else if (Strings.Left(FCConvert.ToString(rs.Get_Fields_String("CurrentStatus")), 1) == "B")
						{
							vsBookDescriptions.TextMatrix(x, 2, "Billed");
						}
					}
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("CDate")))
						vsBookDescriptions.TextMatrix(x, 3, Strings.Format(rs.Get_Fields_DateTime("CDate"), "MM/dd/yyyy"));
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("DDate")))
						vsBookDescriptions.TextMatrix(x, 4, Strings.Format(rs.Get_Fields_DateTime("DDate"), "MM/dd/yyyy"));
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("XDate")))
						vsBookDescriptions.TextMatrix(x, 5, Strings.Format(rs.Get_Fields_DateTime("XDate"), "MM/dd/yyyy"));
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("EDate")))
						vsBookDescriptions.TextMatrix(x, 6, Strings.Format(rs.Get_Fields_DateTime("EDate"), "MM/dd/yyyy"));
					if (!fecherFoundation.FCUtils.IsEmptyDateTime(rs.Get_Fields_DateTime("BDate")))
						vsBookDescriptions.TextMatrix(x, 7, Strings.Format(rs.Get_Fields_DateTime("BDate"), "MM/dd/yyyy"));
					if (!rs.EndOfFile())
						rs.MoveNext();
					// if not the end of the recordset, go to the next
				}
				//mnuFileBookDelete.Enabled = true;
				cmdFileBookDelete.Enabled = true;
			}
			else
			{
				//mnuFileBookDelete.Enabled = false;
				cmdFileBookDelete.Enabled = false;
			}
			int ExposedRows;
			// keeps track of rows that are not collapsed
			ExposedRows = 1;
			for (x = 1; x <= vsBookDescriptions.Rows - 1; x++)
			{
				// check each row
				if (vsBookDescriptions.RowOutlineLevel(x) == 0)
				{
					// if it is the upper level row
					// if row is collapsed
					if (vsBookDescriptions.IsCollapsed(x) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
					{
						ExposedRows += 1;
						// count the exposed row (Upper level)
						x += 1;
						// move to the next row
						// check each row for subordinate rows and do
						// not count the subordinate rows because they are collapsed
						while (vsBookDescriptions.RowOutlineLevel(x) != 0)
						{
							if (x < vsBookDescriptions.Rows - 1)
							{
								x += 1;
							}
							else
							{
								break;
							}
						}
						x -= 1;
					}
					else
					{
						// if the row is not collapsed then
						ExposedRows += 1;
						// count a row
					}
				}
				else
				{
					ExposedRows += 1;
					// if the row is a subordinate row, then count it
				}
			}
		}

		private void AdjustBookGridHeight()
		{
			// this will calculate the height of the flex grid depending on how many rows are open with a max height of the 25% of the form height
			if (((vsBookDescriptions.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * vsBookDescriptions.Rows) + 75) < (frmTableFileSetup.InstancePtr.Height * 0.637))
			{
				//vsBookDescriptions.Height = (vsBookDescriptions.Cell(FCGrid.CellPropertySettings.flexcpHeight, 0, 0, 0, 0) * vsBookDescriptions.Rows) + 75;
			}
			else
			{
				//vsBookDescriptions.Height = FCConvert.ToInt32((frmTableFileSetup.InstancePtr.Height * 0.637));
			}
		}

		private bool Save_TabData_2(int Index)
		{
			return Save_TabData(ref Index);
		}

		private bool Save_TabData(ref int Index)
		{
			bool Save_TabData = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// saves the information on tab index Index
				int i;
				bool boolUpdate = false;
				tabSetup.Focus();
				i = 1;
				Save_TabData = true;
				switch (Index)
				{
					case 0:
						{
							if (ValidateMeterSizes())
							{
								// Meter Sizes
								rs.OpenRecordset("SELECT * FROM MeterSizes");
								vsMeterSizeDescList.Select(0, 0);
								for (i = 1; i <= vsMeterSizeDescList.Rows - 1; i++)
								{
									if (vsMeterSizeDescList.TextMatrix(i, 0) != "")
									{
										rs.FindFirstRecord("Code", Conversion.Val(vsMeterSizeDescList.TextMatrix(i, 0)));
										if (rs.NoMatch)
										{
											rs.AddNew();
											// if no record, create one
										}
										else
										{
											rs.Edit();
										}
										if (fecherFoundation.FCUtils.IsNull(vsMeterSizeDescList.TextMatrix(i, 0)) == false)
											rs.Set_Fields("Code", FCConvert.ToString(Conversion.Val(Strings.Trim(vsMeterSizeDescList.TextMatrix(i, 0)))));
										if (fecherFoundation.FCUtils.IsNull(vsMeterSizeDescList.TextMatrix(i, 1)) == false)
											rs.Set_Fields("LongDescription", Strings.Trim(vsMeterSizeDescList.TextMatrix(i, 1)));
										if (fecherFoundation.FCUtils.IsNull(vsMeterSizeDescList.TextMatrix(i, 2)) == false)
											rs.Set_Fields("ShortDescription", Strings.Trim(vsMeterSizeDescList.TextMatrix(i, 2)));
										rs.Update();
									}
								}
								Dirty = false;
							}
							else
							{
								Save_TabData = false;
							}
							break;
						}
					case 1:
						{
							// Frequency Codes
							rs.OpenRecordset("SELECT * FROM Frequency", modExtraModules.strUTDatabase);
							for (i = 1; i <= vsFrequency.Rows - 1; i++)
							{
								boolUpdate = false;
								if (vsFrequency.TextMatrix(i, 0) != "")
								{
									rs.FindFirstRecord("Code", Conversion.Val(vsFrequency.TextMatrix(i, 0)));
									if (rs.NoMatch)
									{
										rs.AddNew();
										// if no record, create one
									}
									else
									{
										rs.Edit();
									}
									// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
									if (rs.Get_Fields("Code") != Conversion.Val(Strings.Trim(vsFrequency.TextMatrix(i, 0))))
									{
										boolUpdate = true;
										rs.Set_Fields("Code", FCConvert.ToString(Conversion.Val(Strings.Trim(vsFrequency.TextMatrix(i, 0)))));
									}
									if (FCConvert.ToString(rs.Get_Fields_String("LongDescription")) != Strings.Trim(vsFrequency.TextMatrix(i, 1)))
									{
										boolUpdate = true;
										rs.Set_Fields("LongDescription", Strings.Trim(vsFrequency.TextMatrix(i, 1)));
									}
									if (FCConvert.ToString(rs.Get_Fields_String("ShortDescription")) != Strings.Trim(vsFrequency.TextMatrix(i, 2)))
									{
										boolUpdate = true;
										rs.Set_Fields("ShortDescription", Strings.Trim(vsFrequency.TextMatrix(i, 2)));
									}
									if (boolUpdate)
									{
										rs.Set_Fields("DateUpdated", DateTime.Now);
									}
									rs.Update();
								}
							}
							Dirty = false;
							break;
						}
					case 2:
						{
							// User Category Codes
							// txtDAccount(0).Visible = False
							// txtDAccount(1).Visible = False
							rs.Execute("DELETE FROM Category", modExtraModules.strUTDatabase);
							// this will delete all the records and rebuild them all
							vsCategory.Select(0, 0);
							for (i = 1; i <= vsCategory.Rows - 1; i++)
							{
								if (vsCategory.TextMatrix(i, lngCatColCode) != "")
								{
									rs.AddNew();
									// if no record, create one
									if (fecherFoundation.FCUtils.IsNull(vsCategory.TextMatrix(i, lngCatColCode)) == false)
										rs.Set_Fields("Code", FCConvert.ToString(Conversion.Val(Strings.Trim(vsCategory.TextMatrix(i, lngCatColCode)))));
									if (fecherFoundation.FCUtils.IsNull(vsCategory.TextMatrix(i, lngCatColDescription)) == false)
										rs.Set_Fields("LongDescription", Strings.Trim(vsCategory.TextMatrix(i, lngCatColDescription)));
									if (fecherFoundation.FCUtils.IsNull(vsCategory.TextMatrix(i, lngCatColShort)) == false)
										rs.Set_Fields("ShortDescription", Strings.Trim(vsCategory.TextMatrix(i, lngCatColShort)));
									if (fecherFoundation.FCUtils.IsNull(vsCategory.TextMatrix(i, lngCatColWAccount)) == false)
										rs.Set_Fields("WaterAccount", Strings.Trim(vsCategory.TextMatrix(i, lngCatColWAccount)).Replace("_", ""));
									if (fecherFoundation.FCUtils.IsNull(vsCategory.TextMatrix(i, lngCatColSAccount)) == false)
										rs.Set_Fields("SewerAccount", Strings.Trim(vsCategory.TextMatrix(i, lngCatColSAccount)).Replace("_", ""));
									if (Conversion.Val(vsCategory.TextMatrix(i, lngCatColWTaxable)) == -1)
									{
										rs.Set_Fields("WTaxable", true);
									}
									else
									{
										rs.Set_Fields("WTaxable", false);
									}
									if (Conversion.Val(vsCategory.TextMatrix(i, lngCatColSTaxable)) == -1)
									{
										rs.Set_Fields("STaxable", true);
									}
									else
									{
										rs.Set_Fields("STaxable", false);
									}
									rs.Update();
								}
							}
							Dirty = false;
							Category();
							break;
						}
					case 3:
						{
							// Account Adjustment
							rs.OpenRecordset("SELECT * FROM Adjust");
							vsAdjust.Select(0, 0);
							for (i = 1; i <= vsAdjust.Rows - 1; i++)
							{
								if (vsAdjust.TextMatrix(i, 0) != "")
								{
									rs.FindFirstRecord("Code", Conversion.Val(vsAdjust.TextMatrix(i, 0)));
									if (rs.NoMatch)
									{
										rs.AddNew();
										// if no record, create one
									}
									else
									{
										rs.Edit();
									}
									if (fecherFoundation.FCUtils.IsNull(vsAdjust.TextMatrix(i, 0)) == false)
										rs.Set_Fields("Code", FCConvert.ToString(Conversion.Val(Strings.Trim(vsAdjust.TextMatrix(i, 0)))));
									if (fecherFoundation.FCUtils.IsNull(vsAdjust.TextMatrix(i, 1)) == false)
										rs.Set_Fields("LongDescription", Strings.Trim(vsAdjust.TextMatrix(i, 1)));
									if (fecherFoundation.FCUtils.IsNull(vsAdjust.TextMatrix(i, 2)) == false)
										rs.Set_Fields("ShortDescription", Strings.Trim(vsAdjust.TextMatrix(i, 2)));
									if (fecherFoundation.FCUtils.IsNull(vsAdjust.TextMatrix(i, 3)) == false)
										rs.Set_Fields("Service", Strings.Left(Strings.Trim(vsAdjust.TextMatrix(i, 3)), 1));
									if (Conversion.Val(Strings.Trim(vsAdjust.TextMatrix(i, 4))) != 0)
									{
										rs.Set_Fields("Amount", FCConvert.ToDouble(vsAdjust.TextMatrix(i, 4)));
									}
									else
									{
										rs.Set_Fields("Amount", 0);
									}
									if (fecherFoundation.FCUtils.IsNull(vsAdjust.TextMatrix(i, 5)) == false)
										rs.Set_Fields("Account", Strings.Trim(vsAdjust.TextMatrix(i, 5)).Replace("_", ""));
									rs.Update();
								}
							}
							vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
							Dirty = false;
							break;
						}
					case 4:
						{
							// Rate Tables
							if (!Save_RateTable())
							{
								return Save_TabData;
							}
							break;
						}
					case 5:
						{
							// Book Descriptions
							rs.OpenRecordset("SELECT * FROM Book");
							vsBookDescriptions.Select(0, 0);
							for (i = 1; i <= vsBookDescriptions.Rows - 1; i++)
							{
								if (Conversion.Val(vsBookDescriptions.TextMatrix(i, 0)) > 0)
								{
									rs.FindFirstRecord("BookNumber", Conversion.Val(vsBookDescriptions.TextMatrix(i, 0)));
									if (rs.NoMatch)
									{
										rs.AddNew();
										// if there is no record, create one
									}
									else
									{
										rs.Edit();
									}
									if (Conversion.Val(vsBookDescriptions.TextMatrix(i, 0)) > 0)
									{
										rs.Set_Fields("BookNumber", FCConvert.ToString(Conversion.Val(Strings.Trim(vsBookDescriptions.TextMatrix(i, 0)))));
									}
									if (fecherFoundation.FCUtils.IsNull(vsBookDescriptions.TextMatrix(i, 1)) == false)
									{
										rs.Set_Fields("Description", Strings.Trim(vsBookDescriptions.TextMatrix(i, 1)));
									}
									if (fecherFoundation.FCUtils.IsNull(vsBookDescriptions.TextMatrix(i, 2)) == false)
									{
										if (Strings.UCase(Strings.Trim(vsBookDescriptions.TextMatrix(i, 2))) == Strings.UCase("CLEARED"))
										{
											rs.Set_Fields("CurrentStatus", "CE");
										}
										else if (Strings.UCase(Strings.Trim(vsBookDescriptions.TextMatrix(i, 2))) == Strings.UCase("Data Entry"))
										{
											rs.Set_Fields("CurrentStatus", "DE");
										}
										else if (Strings.UCase(Strings.Trim(vsBookDescriptions.TextMatrix(i, 2))) == Strings.UCase("Calculated"))
										{
											rs.Set_Fields("CurrentStatus", "XE");
										}
										else if (Strings.UCase(Strings.Trim(vsBookDescriptions.TextMatrix(i, 2))) == Strings.UCase("Billed Edit"))
										{
											rs.Set_Fields("CurrentStatus", "EE");
											// kk03302015 trouts-11  Change Billed Edit to Calc and Edit
										}
										else if (Strings.UCase(Strings.Trim(vsBookDescriptions.TextMatrix(i, 2))) == Strings.UCase("Calc and Edit"))
										{
											rs.Set_Fields("CurrentStatus", "EE");
										}
										else if (Strings.UCase(Strings.Trim(vsBookDescriptions.TextMatrix(i, 2))) == Strings.UCase("Billed"))
										{
											rs.Set_Fields("CurrentStatus", "BE");
										}
										else
										{
											rs.Set_Fields("CurrentStatus", "CE");
										}
									}
									else
									{
										rs.Set_Fields("CurrentStatus", "BE");
									}
									// do no allow changing of the dates
									// If IsNull(.TextMatrix(i, 3)) = False Then
									// If IsDate(.TextMatrix(i, 3)) Then
									// rs.Fields("CDate") = CDate(.TextMatrix(i, 3))
									// End If
									// End If
									// If IsNull(.TextMatrix(i, 4)) = False Then
									// If IsDate(.TextMatrix(i, 4)) Then
									// rs.Fields("DDate") = CDate(.TextMatrix(i, 4))
									// End If
									// End If
									// If IsNull(.TextMatrix(i, 5)) = False Then
									// If IsDate(.TextMatrix(i, 5)) Then
									// rs.Fields("XDate") = CDate(.TextMatrix(i, 5))
									// End If
									// End If
									// If IsNull(.TextMatrix(i, 6)) = False Then
									// If IsDate(.TextMatrix(i, 6)) Then
									// rs.Fields("EDate") = CDate(.TextMatrix(i, 6))
									// End If
									// End If
									// If IsNull(.TextMatrix(i, 7)) = False Then
									// If IsDate(.TextMatrix(i, 7)) Then
									// rs.Fields("BDate") = CDate(.TextMatrix(i, 7))
									// End If
									// End If
									rs.Update();
								}
							}
							Dirty = false;
							break;
						}
				}
				//end switch
				if (Save_TabData == true)
				{
					MessageBox.Show("Save Successful.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
				return Save_TabData;
			}
			catch (Exception ex)
			{
				
				Save_TabData = false;
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Save_TabData;
		}

		private bool Save_RateTable()
		{
			bool Save_RateTable = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				// saves the current rate table to the database
				int x;
				int lngRT;
				vsWMisc.Select(0, 0);
				vsSMisc.Select(0, 0);
				vsRateTableS.Select(0, 0);
				vsRateTableW.Select(0, 0);
				lngRT = FCConvert.ToInt32(rsRate.Get_Fields_Int32("RateTableNumber"));
				rsRate.Edit();
				// saves the grid information
				for (x = 1; x <= 8; x++)
				{
					rsRate.Set_Fields("WThru" + x, FCConvert.ToString(Conversion.Val(vsRateTableW.TextMatrix(x, 2))));
					rsRate.Set_Fields("WOver" + x, FCConvert.ToString(Conversion.Val(vsRateTableW.TextMatrix(x, 1))));
					if (Conversion.Val(vsRateTableW.TextMatrix(x, 3)) != 0)
					{
						rsRate.Set_Fields("WRate" + x, FCConvert.ToDouble(vsRateTableW.TextMatrix(x, 3)));
					}
					else
					{
						rsRate.Set_Fields("WRate" + x, 0);
					}
					rsRate.Set_Fields("WStep" + x, FCConvert.ToString(Conversion.Val(vsRateTableW.TextMatrix(x, 4))));
				}
				for (x = 1; x <= 8; x++)
				{
					rsRate.Set_Fields("SThru" + x, FCConvert.ToString(Conversion.Val(vsRateTableS.TextMatrix(x, 2))));
					rsRate.Set_Fields("SOver" + x, FCConvert.ToString(Conversion.Val(vsRateTableS.TextMatrix(x, 1))));
					if (Conversion.Val(vsRateTableS.TextMatrix(x, 3)) != 0)
					{
						rsRate.Set_Fields("SRate" + x, FCConvert.ToDouble(vsRateTableS.TextMatrix(x, 3)));
					}
					else
					{
						rsRate.Set_Fields("SRate" + x, 0);
					}
					rsRate.Set_Fields("SStep" + x, FCConvert.ToString(Conversion.Val(vsRateTableS.TextMatrix(x, 4))));
				}
				// Minimum consumption or Water/Sewer
				if (txtMinCons[0].Text != "")
				{
					rsRate.Set_Fields("MinConsW", FCConvert.ToString(Conversion.Val(txtMinCons[0].Text.Replace(",", ""))));
				}
				else
				{
					rsRate.Set_Fields("MinConsW", 0);
				}
				if (txtMinCons[1].Text != "")
				{
					rsRate.Set_Fields("MinConsS", FCConvert.ToString(Conversion.Val(txtMinCons[1].Text.Replace(",", ""))));
				}
				else
				{
					rsRate.Set_Fields("MinConsS", 0);
				}
				// Minimum consumption charge Water/Sewer
				if (txtRateW[0].Text != "")
				{
					rsRate.Set_Fields("MinChargeW", FCConvert.ToString(Conversion.Val(txtRateW[0].Text.Replace(",", ""))));
				}
				else
				{
					rsRate.Set_Fields("MinChargeW", 0);
				}
				if (txtRateS[0].Text != "")
				{
					rsRate.Set_Fields("MinChargeS", FCConvert.ToString(Conversion.Val(txtRateS[0].Text.Replace(",", ""))));
				}
				else
				{
					rsRate.Set_Fields("MinChargeS", 0);
				}
				// Flat Rate for Water/Sewer
				if (txtRateW[1].Text != "")
				{
					rsRate.Set_Fields("FlatRateW", FCConvert.ToString(Conversion.Val(txtRateW[1].Text.Replace(",", ""))));
				}
				else
				{
					rsRate.Set_Fields("FlatRateW", 0);
				}
				if (txtRateS[1].Text != "")
				{
					rsRate.Set_Fields("FlatRateS", FCConvert.ToString(Conversion.Val(txtRateS[1].Text.Replace(",", ""))));
				}
				else
				{
					rsRate.Set_Fields("FlatRateS", 0);
				}
				// Misc charge descriptions Water/Sewer
				if (vsWMisc.TextMatrix(1, 1) != "")
				{
					rsRate.Set_Fields("MiscChargeW1", (vsWMisc.TextMatrix(1, 1).Replace(",", "")));
				}
				else
				{
					rsRate.Set_Fields("MiscChargeW1", "");
				}
				if (vsSMisc.TextMatrix(1, 1) != "")
				{
					rsRate.Set_Fields("MiscChargeS1", (vsSMisc.TextMatrix(1, 1).Replace(",", "")));
				}
				else
				{
					rsRate.Set_Fields("MiscChargeS1", "");
				}
				if (vsWMisc.TextMatrix(2, 1) != "")
				{
					rsRate.Set_Fields("MiscChargeW2", (vsWMisc.TextMatrix(2, 1).Replace(",", "")));
				}
				else
				{
					rsRate.Set_Fields("MiscChargeW2", "");
				}
				if (vsSMisc.TextMatrix(2, 1) != "")
				{
					rsRate.Set_Fields("MiscChargeS2", (vsSMisc.TextMatrix(2, 1).Replace(",", "")));
				}
				else
				{
					rsRate.Set_Fields("MiscChargeS2", "");
				}
				rsRate.Set_Fields("CombineToMisc1", FCConvert.CBool(chkCombineToMisc.CheckState == Wisej.Web.CheckState.Checked));
				rsRate.Set_Fields("TaxableW1", true);
				rsRate.Set_Fields("TaxableW2", true);
				rsRate.Set_Fields("TaxableS1", true);
				rsRate.Set_Fields("TaxableS2", true);
				// Misc charge amount Water/Sewer
				if (Conversion.Val(vsWMisc.TextMatrix(1, 2)) != 0)
				{
					rsRate.Set_Fields("MiscChargeAmountW1", FCConvert.ToString(Conversion.Val(vsWMisc.TextMatrix(1, 2).Replace(",", ""))));
					if (Strings.Trim(FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeW1"))) == "")
					{
						MessageBox.Show("Please add a name for this charge.", "Rate Table Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsWMisc.Focus();
						vsWMisc.Select(1, 1);
						vsWMisc.EditCell();
						return Save_RateTable;
					}
				}
				else
				{
					rsRate.Set_Fields("MiscChargeAmountW1", 0);
				}
				if (Conversion.Val(vsSMisc.TextMatrix(1, 2)) != 0)
				{
					rsRate.Set_Fields("MiscChargeAmountS1", FCConvert.ToString(Conversion.Val(vsSMisc.TextMatrix(1, 2).Replace(",", ""))));
					if (Strings.Trim(FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeS1"))) == "")
					{
						MessageBox.Show("Please add a name for this charge.", "Rate Table Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsSMisc.Focus();
						vsSMisc.Select(1, 1);
						vsSMisc.EditCell();
						return Save_RateTable;
					}
				}
				else
				{
					rsRate.Set_Fields("MiscChargeAmountS1", 0);
				}
				if (Conversion.Val(vsWMisc.TextMatrix(2, 2)) != 0)
				{
					rsRate.Set_Fields("MiscChargeAmountW2", FCConvert.ToString(Conversion.Val(vsWMisc.TextMatrix(2, 2).Replace(",", ""))));
					if (Strings.Trim(FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeW2"))) == "")
					{
						MessageBox.Show("Please add a name for this charge.", "Rate Table Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsWMisc.Focus();
						vsWMisc.Select(2, 1);
						vsWMisc.EditCell();
						return Save_RateTable;
					}
				}
				else
				{
					rsRate.Set_Fields("MiscChargeAmountW2", 0);
				}
				if (Conversion.Val(vsSMisc.TextMatrix(2, 2)) != 0)
				{
					rsRate.Set_Fields("MiscChargeAmountS2", FCConvert.ToString(Conversion.Val(vsSMisc.TextMatrix(2, 2).Replace(",", ""))));
					if (Strings.Trim(FCConvert.ToString(rsRate.Get_Fields_String("MiscChargeS2"))) == "")
					{
						MessageBox.Show("Please add a name for this charge.", "Rate Table Not Saved", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						vsSMisc.Focus();
						vsSMisc.Select(2, 1);
						vsSMisc.EditCell();
						return Save_RateTable;
					}
				}
				else
				{
					rsRate.Set_Fields("MiscChargeAmountS2", 0);
				}
				// Misc Charge Account Numbers Water/Sewer
				if (vsWMisc.TextMatrix(1, 3) != "")
				{
					rsRate.Set_Fields("MiscChargeAccountW1", vsWMisc.TextMatrix(1, 3).Replace(",", "").Replace("_", ""));
				}
				else
				{
					rsRate.Set_Fields("MiscChargeAccountW1", "");
				}
				if (vsSMisc.TextMatrix(1, 3) != "")
				{
					rsRate.Set_Fields("MiscChargeAccountS1", vsSMisc.TextMatrix(1, 3).Replace(",", "").Replace("_", ""));
				}
				else
				{
					rsRate.Set_Fields("MiscChargeAccountS1", "");
				}
				if (vsWMisc.TextMatrix(2, 3) != "")
				{
					rsRate.Set_Fields("MiscChargeAccountW2", vsWMisc.TextMatrix(2, 3).Replace(",", "").Replace("_", ""));
				}
				else
				{
					rsRate.Set_Fields("MiscChargeAccountW2", "");
				}
				if (vsSMisc.TextMatrix(2, 3) != "")
				{
					rsRate.Set_Fields("MiscChargeAccountS2", vsSMisc.TextMatrix(2, 3).Replace(",", "").Replace("_", ""));
				}
				else
				{
					rsRate.Set_Fields("MiscChargeAccountS2", "");
				}
				// rate table description
				// kk08152015 trout-1185   rsRate.Fields("RateTableDescription") = Right$(cmbRT.Text, Len(cmbRT.Text) - 5)    'cmbRT.Text already has the next rate table description if prompted to save
				rsRate.Update();
				Save_RateTable = true;
				ResetBooksByRT(ref lngRT);
				boolRateTableUpdated = true;
				// kk08172015 trout-1186  Prompt to print listing if rates changed
				Dirty = false;
				// clear file
				return Save_RateTable;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Rate Table Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return Save_RateTable;
		}

		private void FlexGridSizeCheck()
		{
			int i = 0;
			switch (tabSetup.SelectedIndex)
			{
				case 0:
					{
						break;
					}
				case 1:
					{
						break;
					}
				case 2:
					{
						break;
					}
				case 3:
					{
						break;
					}
				case 4:
					{
						i = vsRateTableW.RowHeight(0) * vsRateTableW.Rows;
						// calculates the total height of the flexgrid
						if (i < frmTableFileSetup.InstancePtr.Height * 0.388)
						{
							vsRateTableW.Height = i + 75;
							// if area used is less then start, decrease the unused grey area
							vsRateTableS.Height = i + 75;
						}
						else
						{
							vsRateTableW.Height = FCConvert.ToInt32(frmTableFileSetup.InstancePtr.Height * 0.388);
							vsRateTableS.Height = FCConvert.ToInt32(frmTableFileSetup.InstancePtr.Height * 0.388);
						}
						break;
					}
				case 5:
					{
						break;
					}
			}
			//end switch
		}

		private void txtMinCons_TextChanged(short Index, object sender, System.EventArgs e)
		{
			Dirty = true;
			// kk08142015 trout-1185
		}

		private void txtMinCons_TextChanged(object sender, System.EventArgs e)
		{
			short index = txtMinCons.GetIndex((FCTextBox)sender);
			txtMinCons_TextChanged(index, sender, e);
		}

		private void txtMinCons_Enter(short Index, object sender, System.EventArgs e)
		{
			txtMinCons[Index].SelectionStart = 0;
			txtMinCons[Index].SelectionLength = txtMinCons[Index].Text.Length;
		}

		private void txtMinCons_Enter(object sender, System.EventArgs e)
		{
			short index = txtMinCons.GetIndex((FCTextBox)sender);
			txtMinCons_Enter(index, sender, e);
		}

		private void txtMinCons_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if ((KeyAscii < Keys.D0 || KeyAscii > Keys.D9) && KeyAscii != Keys.Back)
			{
				KeyAscii = (Keys)0;
				// any key other than backspace or number keys are not allowed
				// kk08142015 trout-1185   ElseIf Dirty = False Then Dirty = True
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtMinCons_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			short index = txtMinCons.GetIndex((FCTextBox)sender);
			txtMinCons_KeyPress(index, sender, e);
		}

		private void txtRateAccount_KeyPress(ref short Index, ref short KeyAscii)
		{
			if (Dirty == false)
			{
				Dirty = true;
			}
		}

		private void txtRateMiscW_Change(ref short Index)
		{
			if (Dirty == false)
				Dirty = true;
		}

		private void txtRateS_TextChanged(short Index, object sender, System.EventArgs e)
		{
			Dirty = true;
			// kk08142015 trout-1185
		}

		private void txtRateS_TextChanged(object sender, System.EventArgs e)
		{
			short index = txtRateS.GetIndex((FCTextBox)sender);
			txtRateS_TextChanged(index, sender, e);
		}

		private void txtRateS_Enter(short Index, object sender, System.EventArgs e)
		{
			txtRateS[Index].SelectionStart = 0;
			txtRateS[Index].SelectionLength = txtRateS[Index].Text.Length;
		}

		private void txtRateS_Enter(object sender, System.EventArgs e)
		{
			short index = txtRateS.GetIndex((FCTextBox)sender);
			txtRateS_Enter(index, sender, e);
		}

		private void txtRateS_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return) || (KeyAscii == Keys.Insert))
			{
			}
			else if (KeyAscii == Keys.Delete)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtRateS[Index].Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtRateS[Index].SelectionStart < lngDecPlace && txtRateS[Index].SelectionLength + txtRateS[Index].SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtRateS_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			short index = txtRateS.GetIndex((FCTextBox)sender);
			txtRateS_KeyPress(index, sender, e);
		}

		private void txtRateS_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtRateS[Index].Text) != 0)
			{
				txtRateS[Index].Text = Strings.Format(txtRateS[Index].Text, "#,##0.00");
			}
			else
			{
				txtRateS[Index].Text = "0.00";
			}
		}

		private void txtRateS_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txtRateS.GetIndex((FCTextBox)sender);
			txtRateS_Validating(index, sender, e);
		}

		private void txtRateW_TextChanged(short Index, object sender, System.EventArgs e)
		{
			Dirty = true;
			// kk08142015 trout-1185
		}

		private void txtRateW_TextChanged(object sender, System.EventArgs e)
		{
			short index = txtRateW.GetIndex((FCTextBox)sender);
			txtRateW_TextChanged(index, sender, e);
		}

		private void txtRateW_Enter(short Index, object sender, System.EventArgs e)
		{
			txtRateW[Index].SelectionStart = 0;
			txtRateW[Index].SelectionLength = txtRateW[Index].Text.Length;
		}

		private void txtRateW_Enter(object sender, System.EventArgs e)
		{
			short index = txtRateW.GetIndex((FCTextBox)sender);
			txtRateW_Enter(index, sender, e);
		}

		private void txtRateW_KeyPress(short Index, object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			int lngDecPlace = 0;
			if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == Keys.Back) || (KeyAscii == Keys.Return) || (KeyAscii == Keys.Insert))
			{
			}
			else if (KeyAscii == Keys.Delete)
			{
				// decimal point
				lngDecPlace = Strings.InStr(1, txtRateW[Index].Text, ".", CompareConstants.vbBinaryCompare);
				if (lngDecPlace != 0)
				{
					if (txtRateW[Index].SelectionStart < lngDecPlace && txtRateW[Index].SelectionLength + txtRateW[Index].SelectionStart >= lngDecPlace)
					{
						// if the decimal place is being highlighted and will be written over, then allow it
					}
					else
					{
						// if there is already a decimal point then stop others
						KeyAscii = (Keys)0;
					}
				}
			}
			else
			{
				KeyAscii = (Keys)0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtRateW_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			short index = txtRateW.GetIndex((FCTextBox)sender);
			txtRateW_KeyPress(index, sender, e);
		}

		private void txtRateW_Validating(short Index, object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (Conversion.Val(txtRateW[Index].Text) != 0)
			{
				txtRateW[Index].Text = Strings.Format(txtRateW[Index].Text, "#,##0.00");
			}
			else
			{
				txtRateW[Index].Text = "0.00";
			}
		}

		private void txtRateW_Validating(object sender, System.ComponentModel.CancelEventArgs e)
		{
			short index = txtRateW.GetIndex((FCTextBox)sender);
			txtRateW_Validating(index, sender, e);
		}

		private void vsAdjust_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (vsAdjust.Row > 0)
			{
				switch (vsAdjust.Col)
				{
					case 3:
						{
							// kgk 11-11-2011 trout-781  Only allow Town Services
							if (modUTStatusPayments.Statics.TownService == "B")
							{
								// kk trouts-6 03012013  Change Water to Stormwater for Bangor
								if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
								{
									vsAdjust.ComboList = "#0;Stmwater|#1;Sewer|#2;Both";
									// Leave in per Ben  ' also take out Both option  |#2;Both"
								}
								else
								{
									vsAdjust.ComboList = "#0;Water";
								}
							}
							else if (modUTStatusPayments.Statics.TownService == "W")
							{
								// kk trouts-6 03012013  Change Water to Stormwater for Bangor
								if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
								{
									vsAdjust.ComboList = "#0;Stmwater";
								}
								else
								{
									vsAdjust.ComboList = "#0;Water";
								}
							}
							else if (modUTStatusPayments.Statics.TownService == "S")
							{
								vsAdjust.ComboList = "#0;Sewer";
							}
							break;
						}
					case 5:
						{
							vsAdjust.EditMaxLength = 17;
							break;
						}
					default:
						{
							vsAdjust.ComboList = "";
							break;
						}
				}
				//end switch
			}
		}

		private void vsAdjust_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsAdjust.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsAdjust_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsAdjust.Row > 0)
			{
				switch (vsAdjust.Col)
				{
					case 0:
						{
							vsAdjust.EditMaxLength = 2;
							// If vsAdjust.Visible = True Then
							// txtAdjAcct.Text = "R"
							// End If
							// txtAdjAcct.Visible = False
							break;
						}
					case 1:
						{
							vsAdjust.EditMaxLength = 25;
							// If vsAdjust.Visible = True Then
							// txtAdjAcct.Text = "R"
							// End If
							// txtAdjAcct.Visible = False
							break;
						}
					case 2:
						{
							vsAdjust.EditMaxLength = 10;
							// If vsAdjust.Visible = True Then
							// txtAdjAcct.Text = "R"
							// End If
							// txtAdjAcct.Visible = False
							break;
						}
					case 3:
						{
							vsAdjust.EditMaxLength = 1;
							// If vsAdjust.Visible = True Then
							// txtAdjAcct.Text = "R"
							// End If
							// txtAdjAcct.Visible = False
							break;
						}
					case 4:
						{
							vsAdjust.EditMaxLength = 8;
							// If vsAdjust.Visible = True Then
							// txtAdjAcct.Text = "R"
							// End If
							// txtAdjAcct.Visible = False
							break;
						}
					case 5:
						{
							// txtAdjAcct.Left = .Left + .Cell(FCGrid.CellPropertySettings.flexcpLeft, .Row, 5) + 30
							// txtAdjAcct.Top = .Top + .Cell(FCGrid.CellPropertySettings.flexcpTop, .Row, 5) + 30
							// txtAdjAcct.Text = .Cell(FCGrid.CellPropertySettings.flexcpText, .Row, 5)
							// txtAdjAcct.Visible = True
							// AdjustRow = .Row
							// txtAdjAcct.SetFocus
							// txtAdjAcct.SelStart = 0
							// txtAdjAcct.SelLength = 1
							break;
						}
				}
				//end switch
				vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsAdjust.EditCell();
				vsAdjust.EditSelStart = 0;
			}
		}

		private void vsAdjust_Enter(object sender, System.EventArgs e)
		{
			if (vsAdjust.Col == 5 && vsAdjust.Row > 0)
			{
				modNewAccountBox.SetGridFormat(vsAdjust, vsAdjust.Row, vsAdjust.Col, false);
			}
            //FC:FINAL:AM:#4344 - don't set the Editable property here; it is causing issues and it is set in RowColChange again
			//else
			//{
			//	vsAdjust.Editable = FCGrid.EditableSettings.flexEDNone;
			//}
		}

		private void vsAdjust_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				//e.Control.KeyDown -= vsAdjust_KeyDownEdit;
				//e.Control.KeyPress -= vsAdjust_KeyPressEdit;
				//e.Control.KeyUp -= vsAdjust_KeyUpEdit;
				//e.Control.KeyDown += vsAdjust_KeyDownEdit;
				//e.Control.KeyPress += vsAdjust_KeyPressEdit;
				//e.Control.KeyUp += vsAdjust_KeyUpEdit;
				JavaScript javaScript = ControlExtension.Statics.GlobalJavaScript;
				CustomProperties customProperties = ControlExtension.Statics.GlobalCustomProperties;
				int col = (sender as FCGrid).Col;
                if (col == 5)
                {
                    if (e.Control is TextBox)
                    {
                        (e.Control as TextBox).CharacterCasing = CharacterCasing.Upper;
                    }
                    DynamicObject customClientData = new DynamicObject();
                    customClientData["GRID7Light_Col"] = col;
                    customClientData["intAcctCol"] = 5;
                    customClientData["AllowFormats"] = modNewAccountBox.Statics.AllowFormats;
                    customClientData["gboolTownAccounts"] = true;
                    customClientData["gboolSchoolAccounts"] = false;
                    customClientData["DefaultAccountType"] = modNewAccountBox.DefaultAccountType;
                    customClientData["DefaultInfochoolAccountType"] = modNewAccountBox.DefaultInfochoolAccountType;
                    customClientData["ESegmentSize"] = modNewAccountBox.Statics.ESegmentSize;
                    customClientData["RSegmentSize"] = modNewAccountBox.Statics.RSegmentSize;
                    customClientData["GSegmentSize"] = modNewAccountBox.Statics.GSegmentSize;
                    customClientData["PSegmentSize"] = modNewAccountBox.Statics.PSegmentSize;
                    customClientData["VSegmentSize"] = modNewAccountBox.Statics.VSegmentSize;
                    customClientData["LSegmentSize"] = modNewAccountBox.Statics.LSegmentSize;
                    customClientData["AllowedKeys"] = "0;1;2;3;4;5;6;7;8;9";
                    customProperties.SetCustomPropertiesValue(e.Control, customClientData);
                    if (e.Control.UserData.GRID7LightClientSideKeys == null)
                    {
                        e.Control.UserData.GRID7LightClientSideKeys = "GRID7LightClientSideKeys";
                        JavaScript.ClientEventCollection clientEvents = javaScript.GetJavaScriptEvents(e.Control);
                        JavaScript.ClientEvent keyDownEvent = new JavaScript.ClientEvent();
                        keyDownEvent.Event = "keydown";
                        keyDownEvent.JavaScript = "AccountInput_KeyDown(this, e);";
                        JavaScript.ClientEvent keyUpEvent = new JavaScript.ClientEvent();
                        keyUpEvent.Event = "keyup";
                        keyUpEvent.JavaScript = "AccountInput_KeyUp(this, e);";
                        clientEvents.Add(keyDownEvent);
                        clientEvents.Add(keyUpEvent);
                    }
                }
                else if (col == 4)
                {
                    if (e.Control is TextBox textBox)
                    {
                        textBox.AllowOnlyNumericInput(true);
                    }
                }
			}
		}

		private void vsAdjust_KeyDownEdit(object sender, KeyEventArgs e)
		{
			//FC:FINAL:MSH - issue #1021: remove extra functionality to avoid multi showing accounts form and prevent exceptions on table data initializing
			//if (vsAdjust.Row > 0)
			//{
			//    switch (vsAdjust.Col)
			//    {
			//        case 5:
			//            {
			//                // this is for the left and right arrow key
			//                string vsAdjustEditText = vsAdjust.EditText;
			//                modNewAccountBox.CheckKeyDownEditF2(vsAdjust, vsAdjust.Row, vsAdjust.Col, e.KeyCode, FCConvert.ToInt32(e.Shift), vsAdjust.EditSelStart, ref vsAdjustEditText, vsAdjust.EditSelLength);
			//                vsAdjust.EditText = vsAdjustEditText;
			//                break;
			//            }
			//    } //end switch
			//}
			// If KeyCode = vbKeyReturn Then
			// KeyCode = 0
			// If .Col < (.Cols - 1) Then
			// .Col = .Col + 1
			// Else
			// If .Row < .rows - 1 Then
			// .Row = .Row + 1
			// .Col = 1
			// End If
			// End If
			// End If
		}

		private void vsAdjust_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int lngDecPlace = 0;
			int KeyAscii = Strings.Asc(e.KeyChar);
			switch (vsAdjust.Col)
			{
				case 5:
					{
						//modNewAccountBox.CheckAccountKeyPress(vsAdjust, vsAdjust.Row, vsAdjust.Col, KeyAscii, vsAdjust.EditSelStart, vsAdjust.EditText);
						break;
					}
				case 4:
					{
						if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii == 45))
						{
						}
						else if (KeyAscii == 46)
						{
							// decimal point
							lngDecPlace = Strings.InStr(1, vsAdjust.EditText, ".", CompareConstants.vbBinaryCompare);
							if (lngDecPlace != 0)
							{
								if (vsAdjust.EditSelStart < lngDecPlace && vsAdjust.EditSelLength + vsAdjust.EditSelStart >= lngDecPlace)
								{
									// if the decimal place is being highlighted and will be written over, then allow it
								}
								else
								{
									// if there is already a decimal point then stop others
									KeyAscii = 0;
								}
							}
						}
						else
						{
							KeyAscii = 0;
						}
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsAdjust_KeyUpEdit(object sender, KeyEventArgs e)
		{
			int KeyCode = FCConvert.ToInt32(e.KeyCode);
			if (vsAdjust.Col == 5 && vsAdjust.Row > 0)
			{
				if (KeyCode != 40 && KeyCode != 38 && KeyCode != 123 && KeyCode != 122)
				{
					// up and down arrows
					//modNewAccountBox.CheckAccountKeyCode(vsAdjust, vsAdjust.Row, vsAdjust.Col, KeyCode, 0, vsAdjust.EditSelStart, vsAdjust.EditText, vsAdjust.EditSelLength);
				}
			}
		}

		private void vsAdjust_RowColChange(object sender, System.EventArgs e)
		{
			vsAdjust.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			if (vsAdjust.Row > 0)
			{
				switch (vsAdjust.Col)
				{
					case 0:
						{
							vsAdjust.EditMaxLength = 2;
							vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsAdjust.EditCell();
							vsAdjust.EditSelStart = 0;
							vsAdjust.EditSelLength = vsAdjust.EditText.Length;
							break;
						}
					case 1:
						{
							vsAdjust.EditMaxLength = 25;
							vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsAdjust.EditCell();
							vsAdjust.EditSelStart = 0;
							vsAdjust.EditSelLength = vsAdjust.EditText.Length;
							break;
						}
					case 2:
						{
							vsAdjust.EditMaxLength = 10;
							vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsAdjust.EditCell();
							vsAdjust.EditSelStart = 0;
							vsAdjust.EditSelLength = vsAdjust.EditText.Length;
							break;
						}
					case 3:
						{
							vsAdjust.EditMaxLength = 1;
							vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsAdjust.EditCell();
							vsAdjust.EditSelStart = 0;
							vsAdjust.EditSelLength = vsAdjust.EditText.Length;
							break;
						}
					case 4:
						{
							vsAdjust.EditMaxLength = 8;
							vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsAdjust.EditCell();
							vsAdjust.EditSelStart = 0;
							vsAdjust.EditSelLength = vsAdjust.EditText.Length;
							break;
						}
					case 5:
						{
							vsAdjust.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
							vsAdjust.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							modNewAccountBox.SetGridFormat(vsAdjust, vsAdjust.Row, vsAdjust.Col, true);
							// txtAdjAcct.Left = .Left + .Cell(FCGrid.CellPropertySettings.flexcpLeft, .Row, 5) + 30
							// txtAdjAcct.Top = .Top + .Cell(FCGrid.CellPropertySettings.flexcpTop, .Row, 5) + 30
							// txtAdjAcct.Text = .Cell(FCGrid.CellPropertySettings.flexcpText, .Row, 5)
							// txtAdjAcct.Visible = True
							// AdjustRow = .Row
							// txtAdjAcct.SetFocus
							// txtAdjAcct.SelStart = 0
							// txtAdjAcct.SelLength = 1
							// Exit Sub
							break;
						}
				}
				//end switch
			}
		}

		private void vsAdjust_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
            //FC:FINAL:AM:#4347 - avoid entering validate multiple times
            if (inValidateEdit)
            {
                return;
            }
            else
            {
                inValidateEdit = true;
            }
            // validates the code for adjustments
			int i;
			//FC:FINAL:MSH - issue #1023: saving correct indexes of the cell
			int row = vsAdjust.GetFlexRowIndex(e.RowIndex);
			int col = vsAdjust.GetFlexColIndex(e.ColumnIndex);
			switch (col)
			{
				case 0:
					{
						if (Conversion.Val(vsAdjust.EditText) == 0)
						{
							e.Cancel = true;
						}
						else
						{
							for (i = 1; i <= vsAdjust.Rows - 1; i++)
							{
								if (i != row)
								{
									if (vsAdjust.EditText == vsAdjust.TextMatrix(i, 0))
									{
										e.Cancel = true;
										break;
									}
								}
							}
						}
						break;
					}
				case 5:
					{
						e.Cancel = modNewAccountBox.CheckAccountValidate(vsAdjust, row, col, e.Cancel);
						if (modUTStatusPayments.Statics.TownService == "W")
						{
							if (!modMain.CheckDefaultFundForAccount_2(true, vsAdjust.EditText))
							{
								MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
							}
						}
						else if (modUTStatusPayments.Statics.TownService == "S")
						{
							if (!modMain.CheckDefaultFundForAccount_2(false, vsAdjust.EditText))
							{
								MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
							}
						}
						else
						{
							if (!modMain.CheckDefaultFundForAccount_2(true, vsAdjust.EditText))
							{
								MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
							}
							if (!modMain.CheckDefaultFundForAccount_2(false, vsAdjust.EditText))
							{
								MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								e.Cancel = true;
							}
						}
						break;
					}
			}
            inValidateEdit = false;
			//end switch
		}

		private void vsBookDescriptions_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsBookDescriptions.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsBookDescriptions_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				if (vsBookDescriptions.Col < (vsBookDescriptions.Cols - 1))
				{
					vsBookDescriptions.Col += 1;
				}
				else
				{
					vsBookDescriptions.Row += 1;
					vsBookDescriptions.Col = 1;
				}
			}
		}

		private void vsBookDescriptions_RowColChange(object sender, System.EventArgs e)
		{
			if (vsBookDescriptions.Col == vsBookDescriptions.Cols - 1 && vsBookDescriptions.Row == vsBookDescriptions.Rows - 1)
			{
				vsBookDescriptions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsBookDescriptions.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			if (vsBookDescriptions.Row > 0)
			{
				switch (vsBookDescriptions.Col)
				{
					case 0:
						{
							vsBookDescriptions.EditMaxLength = 5;
							vsBookDescriptions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsBookDescriptions.EditCell();
							vsBookDescriptions.EditSelStart = 0;
							vsBookDescriptions.EditSelLength = vsBookDescriptions.EditText.Length;
							break;
						}
					case 1:
						{
							vsBookDescriptions.EditMaxLength = 30;
							vsBookDescriptions.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsBookDescriptions.EditCell();
							vsBookDescriptions.EditSelStart = 0;
							vsBookDescriptions.EditSelLength = vsBookDescriptions.EditText.Length;
							break;
						}
					case 2:
						{
							vsBookDescriptions.EditMaxLength = 14;
							vsBookDescriptions.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
					default:
						{
							vsBookDescriptions.EditMaxLength = 10;
							vsBookDescriptions.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
				}
				//end switch
			}
			else
			{
				vsBookDescriptions.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsBookDescriptions_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			int lngNumber = 0;
			int lngRW;
			//FC:FINAL:MSH - issue #1023: saving correct indexes of the cell
			int row = vsBookDescriptions.GetFlexRowIndex(e.RowIndex);
			int col = vsBookDescriptions.GetFlexColIndex(e.ColumnIndex);
			switch (col)
			{
				case 0:
					{
						// make sure that there is not another instance of the same booknumber
						lngNumber = FCConvert.ToInt32(Math.Round(Conversion.Val(vsBookDescriptions.EditText)));
						if (lngNumber > 0)
						{
							for (lngRW = 1; lngRW <= vsBookDescriptions.Rows - 1; lngRW++)
							{
								if (lngNumber == Conversion.Val(vsBookDescriptions.TextMatrix(lngRW, 0)) && lngRW != row)
								{
									MessageBox.Show("This book number has already been used.  Please select another.", "Duplicate Book Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
									e.Cancel = true;
									break;
								}
							}
						}
						else
						{
							if (Strings.Trim(vsBookDescriptions.EditText) == "")
							{
								// do nothing
							}
							else
							{
								MessageBox.Show("Please enter a valid book number.", "Invalid Book Number", MessageBoxButtons.OK, MessageBoxIcon.Warning);
							}
							e.Cancel = true;
						}
						break;
					}
				default:
					{
						break;
					}
			}
			//end switch
		}

		private void vsCategory_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			int temp = 0;
			int i;
			if (vsCategory.Row > 0)
			{
				switch (vsCategory.Col)
				{
					case 0:
						{
							temp = FCConvert.ToInt32(Math.Round(Conversion.Val(vsCategory.TextMatrix(vsCategory.Row, vsCategory.Col))));
							if (temp == 0 && !(Strings.Trim(vsCategory.TextMatrix(vsCategory.Row, lngCatColCode)) == "" && Strings.Trim(vsCategory.TextMatrix(vsCategory.Row, lngCatColDescription)) == "" && Strings.Trim(vsCategory.TextMatrix(vsCategory.Row, lngCatColShort)) == ""))
							{
								MessageBox.Show("Please enter a non zero number in the Code field.", "Data Entry Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
								return;
							}
							for (i = 1; i <= vsCategory.Rows - 1; i++)
							{
								if (i != vsCategory.Row)
								{
									if (vsCategory.TextMatrix(i, vsCategory.Col) == "")
									{
										return;
									}
									if (temp == FCConvert.ToInt16(FCConvert.ToDouble(vsCategory.TextMatrix(i, vsCategory.Col))))
									{
										MessageBox.Show("Redundent category code number.", "Invalid Data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										vsCategory.TextMatrix(vsCategory.Row, vsCategory.Col, "");
										vsCategory.Row = vsCategory.Row;
										vsCategory.Col = 0;
										return;
									}
								}
							}
							break;
						}
				}
				//end switch
			}
		}

		private void vsCategory_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			// Select Case Col
			// Case lngCatColWAccount, lngCatColSAccount
			// vsCategory.EditMaxLength = 25
			// End Select
		}

		private void vsCategory_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsCategory.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsCategory_ClickEvent(object sender, System.EventArgs e)
		{
			// TODO:IPI - Waiting for FC-9151
			//FC:TEMP:IPI - #1016, #1364, Waiting for FC-9151 - replace vsCategory.Col with vsCategory.MouseCol because the CurrentCell is not set correctly
			if (vsCategory.MouseCol == lngCatColCode)
			{
				vsCategory.EditMaxLength = 2;
				vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsCategory.EditCell();
				vsCategory.EditSelStart = 0;
			}
			else if (vsCategory.MouseCol == lngCatColDescription)
			{
				vsCategory.EditMaxLength = 25;
				vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsCategory.EditCell();
				vsCategory.EditSelStart = 0;
			}
			else if (vsCategory.MouseCol == lngCatColShort)
			{
				vsCategory.EditMaxLength = 10;
				vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsCategory.EditCell();
				vsCategory.EditSelStart = 0;
			}
			else if (vsCategory.MouseCol == lngCatColWTaxable || vsCategory.MouseCol == lngCatColSTaxable)
			{
				vsCategory.Editable = FCGrid.EditableSettings.flexEDNone;
				if (Conversion.Val(vsCategory.TextMatrix(vsCategory.MouseRow, vsCategory.MouseCol)) == -1)
				{
					vsCategory.TextMatrix(vsCategory.MouseRow, vsCategory.MouseCol, FCConvert.ToString(0));
				}
				else
				{
					vsCategory.TextMatrix(vsCategory.MouseRow, vsCategory.MouseCol, FCConvert.ToString(-1));
				}
			}
			else
			{
				vsCategory.EditMaxLength = 16;
				vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsCategory.EditCell();
				vsCategory.EditSelStart = 0;
			}
		}

		private void vsCategory_Enter(object sender, System.EventArgs e)
		{
			// If vsCategory.Row > 0 Then
			// If vsCategory.Col = lngCatColWAccount Or vsCategory.Col = lngCatColSAccount Then
			// SetGridFormat vsCategory, vsCategory.Row, vsCategory.Col, False
			// make the account box look like it is part of the grid
			// txtDAccount(acctIndex).Left = vsCategory.Left + vsCategory.CellLeft + 30
			// txtDAccount(acctIndex).Top = vsCategory.Top + vsCategory.CellTop + 35
			// txtDAccount(acctIndex).Height = vsCategory.CellHeight - 50
			// txtDAccount(acctIndex).Width = vsCategory.CellWidth - 20
			// If vsCategory.TextMatrix(vsCategory.Row, vsCategory.Col) <> "" Then   'if the field is not empty
			// txtdaccount(acctindex).MaxLength = Len(vscategory.TextMatrix(vscategory.Row, vscategory.Col))
			// txtDAccount(acctIndex).Text = vsCategory.TextMatrix(vsCategory.Row, vsCategory.Col)   'show what is in the field
			// Else
			// txtDAccount(acctIndex).Text = "R"       'otherwise initialize it
			// End If
			// txtDAccount(acctIndex).ZOrder 0             'set it on top
			// txtDAccount(acctIndex).Visible = True       'make it visible
			// txtDAccount(acctIndex).SetFocus             'give it focus
			// End If
			// End If
		}

		private void vsCategory_KeyDownEdit(object sender, KeyEventArgs e)
		{
			// With vsCategory
			// .TabBehavior = flexTabCells
			// If Row > 0 Then
			// Select Case Col
			// Case lngCatColWAccount, lngCatColSAccount
			// CheckKeyDownEdit vsCategory, Row, Col, KeyCode, Shift, .EditSelStart, .EditText, .EditSelLength
			// If Row = .rows - 1 Then
			// .TabBehavior = flexTabControls
			// End If
			// End Select
			// 
			// If KeyCode = vbKeyReturn Then
			// .Row = (.Row + 1) Mod .rows - 1
			// End If
			// End If
			// End With
		}

		private void vsCategory_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int KeyAscii = Strings.Asc(e.KeyChar);
			// only allow users to type numbers in the code field
			if (vsCategory.Col == 0)
			{
				if (KeyAscii < 0 || KeyAscii > 9)
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsCategory_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			// If Row > 0 Then
			// Select Case Col
			// Case lngCatColWAccount, lngCatColSAccount
			// CheckAccountKeyPress vsCategory, Row, Col, KeyAscii, vsCategory.EditSelStart, vsCategory.EditText
			// End Select
			// End If
		}

		private void vsCategory_KeyUpEdit(object sender, KeyEventArgs e)
		{
			// If Row > 0 Then
			// Select Case Col
			// Case lngCatColWAccount, lngCatColSAccount
			// If KeyCode <> 40 And KeyCode <> 38 And KeyCode <> 123 And KeyCode <> 122 Then     'up and down arrows
			// CheckAccountKeyCode vsCategory, Row, Col, KeyCode, 0, vsCategory.EditSelStart, vsCategory.EditText, vsCategory.EditSelLength
			// End If
			// End Select
			// End If
		}

		private void vsCategory_Leave(object sender, System.EventArgs e)
		{
			// txtDAccount(acctIndex).Visible = False            'make the text box invisible
		}

		private void vsCategory_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = vsCategory[e.ColumnIndex, e.RowIndex];
            int lngMC;
			lngMC = vsCategory.GetFlexColIndex(e.ColumnIndex);
			if (lngMC == lngCatColCode)
			{
                //ToolTip1.SetToolTip(vsCategory, "");
                cell.ToolTipText = "";
			}
			else if (lngMC == lngCatColDescription || lngMC == lngCatColShort)
			{
                //ToolTip1.SetToolTip(vsCategory, "");
                cell.ToolTipText = "";
			}
			else if (lngMC == lngCatColWAccount || lngMC == lngCatColSAccount)
			{
                //ToolTip1.SetToolTip(vsCategory, "");
                cell.ToolTipText = "";
			}
			else if (lngMC == lngCatColWTaxable || lngMC == lngCatColSTaxable)
			{
				// vsCategory.ToolTipText = "If this is checked then when changing the category on meters to this category, you will be prompted if the taxable amount is zero."
				// MAL@20070831
				// kk trouts-6 03012013  Change Water to Stormwater for Bangor
				if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
				{
					//ToolTip1.SetToolTip(vsCategory, "Select to default the Storm(W)ater Taxable or Sewer Taxable % to 100 for this category.");
					cell.ToolTipText = "Select to default the Storm(W)ater Taxable or Sewer Taxable % to 100 for this category.";
				}
				else
				{
					//ToolTip1.SetToolTip(vsCategory, "Select to default the Water Taxable or Sewer Taxable % to 100 for this category.");
					cell.ToolTipText = "Select to default the Water Taxable or Sewer Taxable % to 100 for this category.";
				}
			}
			else
			{
                //ToolTip1.SetToolTip(vsCategory, "");
                cell.ToolTipText = "";
			}
		}

		private void vsCategory_RowColChange(object sender, System.EventArgs e)
		{
			if (vsCategory.Row == vsCategory.Rows - 1 && vsCategory.Col == vsCategory.Cols - 1)
			{
				vsCategory.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsCategory.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
			if (vsCategory.Col == lngCatColWAccount || vsCategory.Col == lngCatColSAccount)
			{
				// if we are in the first column
				vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				modNewAccountBox.SetGridFormat(vsCategory, vsCategory.Row, vsCategory.Col, true);
			}
			if (vsCategory.Col == lngCatColCode)
			{
				vsCategory.EditMaxLength = 2;
				vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsCategory.EditCell();
				vsCategory.EditSelStart = 0;
				vsCategory.EditSelLength = vsCategory.EditText.Length;
			}
			else if (vsCategory.Col == lngCatColDescription)
			{
				vsCategory.EditMaxLength = 25;
				vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsCategory.EditCell();
				vsCategory.EditSelStart = 0;
				vsCategory.EditSelLength = vsCategory.EditText.Length;
			}
			else if (vsCategory.Col == lngCatColShort)
			{
				vsCategory.EditMaxLength = 10;
				vsCategory.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				vsCategory.EditCell();
				vsCategory.EditSelStart = 0;
				vsCategory.EditSelLength = vsCategory.EditText.Length;
			}
		}

		private void vsCategory_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			// If Row > 0 Then
			// Select Case Col
			// Case lngCatColWAccount
			// If InStr(1, vsCategory.EditText, "_") = 0 And Left(vsCategory.EditText, 1) <> "M" Then
			// Cancel = CheckAccountValidate(vsCategory, Row, Col, Cancel)
			// If Not CheckDefaultFundForAccount(True, vsCategory.EditText) Then
			// MsgBox "Please enter an account that matches the fund of the default account.", vbExclamation, "Correct Fund"
			// Cancel = True
			// End If
			// Else
			// vsCategory.EditText = ""
			// End If
			// Case lngCatColSAccount
			// If InStr(1, vsCategory.EditText, "_") = 0 And Left(vsCategory.EditText, 1) <> "M" Then
			// Cancel = CheckAccountValidate(vsCategory, Row, Col, Cancel)
			// If Not CheckDefaultFundForAccount(False, vsCategory.EditText) Then
			// MsgBox "Please enter an account that matches the fund of the default account.", vbExclamation, "Correct Fund"
			// Cancel = True
			// End If
			// Else
			// vsCategory.EditText = ""
			// End If
			// End Select
			// End If
		}

		private void vsFrequency_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsFrequency.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsFrequency_ClickEvent(object sender, System.EventArgs e)
		{
			switch (vsFrequency.Col)
			{
				case 1:
					{
						vsFrequency.EditMaxLength = 25;
						break;
					}
				case 2:
					{
						vsFrequency.EditMaxLength = 10;
						break;
					}
			}
			//end switch
			vsFrequency.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsFrequency.EditCell();
			vsFrequency.EditSelStart = 0;
		}

		private void vsFrequency_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				if (vsFrequency.Col < (vsFrequency.Cols - 1))
				{
					vsFrequency.Col += 1;
				}
				else
				{
					vsFrequency.Row = (vsFrequency.Row + 1) % (vsFrequency.Rows - 1);
					vsFrequency.Col = 1;
				}
			}
		}

		private void vsFrequency_RowColChange(object sender, System.EventArgs e)
		{
			switch (vsFrequency.Col)
			{
				case 1:
					{
						vsFrequency.EditMaxLength = 25;
						break;
					}
				case 2:
					{
						vsFrequency.EditMaxLength = 10;
						break;
					}
			}
			//end switch
			vsFrequency.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsFrequency.EditCell();
			vsFrequency.EditSelStart = 0;
			vsFrequency.EditSelLength = vsFrequency.EditText.Length;
			vsFrequency.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
		}

		private void vsMeterSizeDescList_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsMeterSizeDescList.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsMeterSizeDescList_ClickEvent(object sender, System.EventArgs e)
		{
			switch (vsMeterSizeDescList.Col)
			{
				case 1:
					{
						vsMeterSizeDescList.EditMaxLength = 25;
						break;
					}
				case 2:
					{
						vsMeterSizeDescList.EditMaxLength = 10;
						break;
					}
			}
			//end switch
			vsMeterSizeDescList.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsMeterSizeDescList.EditCell();
			vsMeterSizeDescList.EditSelStart = 0;
		}

		private void vsMeterSizeDescList_RowColChange(object sender, System.EventArgs e)
		{
			switch (vsMeterSizeDescList.Col)
			{
				case 1:
					{
						vsMeterSizeDescList.EditMaxLength = 25;
						break;
					}
				case 2:
					{
						vsMeterSizeDescList.EditMaxLength = 10;
						break;
					}
			}
			//end switch
			vsMeterSizeDescList.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			vsMeterSizeDescList.EditCell();
			vsMeterSizeDescList.EditSelStart = 0;
			vsMeterSizeDescList.EditSelLength = vsMeterSizeDescList.EditText.Length;
			if (vsMeterSizeDescList.Col == vsMeterSizeDescList.Cols - 1 && vsMeterSizeDescList.Row == vsMeterSizeDescList.Rows - 1)
			{
				vsMeterSizeDescList.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
			}
			else
			{
				vsMeterSizeDescList.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsWMisc_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int lngDecPlace = 0;
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (vsWMisc.Col == 3)
			{
				modNewAccountBox.CheckAccountKeyPress(vsWMisc, vsWMisc.Row, vsWMisc.Col, KeyAscii, vsWMisc.EditSelStart, vsWMisc.EditText);
			}
			else if (vsWMisc.Col == 2)
			{
				if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii == 45))
				{
				}
				else if (KeyAscii == 46)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, vsWMisc.EditText, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (vsWMisc.EditSelStart < lngDecPlace && vsWMisc.EditSelLength + vsWMisc.EditSelStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = 0;
						}
					}
				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsSMisc_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int lngDecPlace = 0;
			int KeyAscii = Strings.Asc(e.KeyChar);
			if (vsSMisc.Col == 3)
			{
				modNewAccountBox.CheckAccountKeyPress(vsSMisc, vsSMisc.Row, vsSMisc.Col, KeyAscii, vsSMisc.EditSelStart, vsSMisc.EditText);
			}
			else if (vsSMisc.Col == 2)
			{
				if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii == 45))
				{
				}
				else if (KeyAscii == 46)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, vsSMisc.EditText, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (vsSMisc.EditSelStart < lngDecPlace && vsSMisc.EditSelLength + vsSMisc.EditSelStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = 0;
						}
					}
				}
				else
				{
					KeyAscii = 0;
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsWMisc_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsWMisc.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsSMisc_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsSMisc.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsWMisc_RowColChange(object sender, System.EventArgs e)
		{
			if (vsWMisc.Row > 0)
			{
				vsWMisc.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				switch (vsWMisc.Col)
				{
					case 0:
						{
							// Taxable
							vsWMisc.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
					case 1:
						{
							// Description
							vsWMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsWMisc.EditCell();
							break;
						}
					case 2:
						{
							// Amount
							vsWMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsWMisc.EditCell();
							break;
						}
					case 3:
						{
							// Account
							vsWMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							modNewAccountBox.SetGridFormat(vsWMisc, vsWMisc.Row, vsWMisc.Col, true, "", "R");
							if (vsWMisc.Row == vsWMisc.Rows - 1)
							{
								vsWMisc.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
							}
							break;
						}
				}
				//end switch
			}
			else
			{
				vsWMisc.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsSMisc_RowColChange(object sender, System.EventArgs e)
		{
			if (vsSMisc.Row > 0)
			{
				vsSMisc.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
				switch (vsSMisc.Col)
				{
					case 0:
						{
							// Taxable
							vsSMisc.Editable = FCGrid.EditableSettings.flexEDNone;
							break;
						}
					case 1:
						{
							// Description
							vsSMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsSMisc.EditCell();
							break;
						}
					case 2:
						{
							// Amount
							vsSMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							vsSMisc.EditCell();
							break;
						}
					case 3:
						{
							// Account
							vsSMisc.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
							modNewAccountBox.SetGridFormat(vsSMisc, vsSMisc.Row, vsSMisc.Col, true, "", "R");
							if (vsSMisc.Row == vsSMisc.Rows - 1)
							{
								vsSMisc.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
							}
							break;
						}
				}
				//end switch
			}
			else
			{
				vsSMisc.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}

		private void vsWMisc_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - issue #1023: saving correct indexes of the cell
			int row = vsWMisc.GetFlexRowIndex(e.RowIndex);
			int col = vsWMisc.GetFlexColIndex(e.ColumnIndex);
			if (col == 3)
			{
				//FC:FINAL:MSH - i.issue #1023: got indexes from wrong table
				//e.Cancel = modNewAccountBox.CheckAccountValidate(vsWMisc, vsSMisc.Row, vsSMisc.Col, e.Cancel);
				e.Cancel = modNewAccountBox.CheckAccountValidate(vsWMisc, row, col, e.Cancel);
				if (modUTStatusPayments.Statics.TownService == "W")
				{
					if (!modMain.CheckDefaultFundForAccount_2(true, vsWMisc.EditText))
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
				}
				else if (modUTStatusPayments.Statics.TownService == "S")
				{
					if (!modMain.CheckDefaultFundForAccount_2(false, vsWMisc.EditText))
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
				}
				else
				{
					if (!modMain.CheckDefaultFundForAccount_2(true, vsWMisc.EditText))
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
					if (!modMain.CheckDefaultFundForAccount_2(false, vsWMisc.EditText))
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
				}
			}
		}

		private void vsSMisc_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - issue #1023: saving correct indexes of the cell
			int row = vsSMisc.GetFlexRowIndex(e.RowIndex);
			int col = vsSMisc.GetFlexColIndex(e.ColumnIndex);
			if (col == 3)
			{
				e.Cancel = modNewAccountBox.CheckAccountValidate(vsSMisc, row, col, e.Cancel);
				if (modUTStatusPayments.Statics.TownService == "W")
				{
					if (!modMain.CheckDefaultFundForAccount_2(true, vsSMisc.EditText))
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
				}
				else if (modUTStatusPayments.Statics.TownService == "S")
				{
					if (!modMain.CheckDefaultFundForAccount_2(false, vsSMisc.EditText))
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
				}
				else
				{
					if (!modMain.CheckDefaultFundForAccount_2(true, vsSMisc.EditText))
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
					if (!modMain.CheckDefaultFundForAccount_2(false, vsSMisc.EditText))
					{
						MessageBox.Show("Please enter an account that matches the fund of the default account.", "Correct Fund", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						e.Cancel = true;
					}
				}
			}
		}

		private void vsRateTableS_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// this makes sure that the over and thru cols are not the same value and sets row+1's over col
			int i;
			int MaxValue;
			MaxValue = 999999999;
			if (vsRateTableS.Col == 2)
			{
				if (Conversion.Val(vsRateTableS.TextMatrix(vsRateTableS.Row, 2)) < MaxValue && !(Conversion.Val(vsRateTableS.TextMatrix(vsRateTableS.Row, 1)) == 0 && vsRateTableS.Row != 1))
				{
					if (Conversion.Val(vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col)) <= Conversion.Val(vsRateTableS.TextMatrix(vsRateTableS.Row, 1)))
					{
						// greater than or equal to the "Over" Amount in the same row
						vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col, FCConvert.ToString(Conversion.Val(vsRateTableS.TextMatrix(vsRateTableS.Row, 1)) + 1));
						// add one to the
						for (i = vsRateTableS.Row; i <= 8; i++)
						{
							vsRateTableS.TextMatrix(i, 1, FCConvert.ToString(0));
							vsRateTableS.TextMatrix(i, 2, FCConvert.ToString(0));
						}
					}
					if (vsRateTableS.Row < 8 && Conversion.Val(vsRateTableS.TextMatrix(vsRateTableS.Row, 1)) < MaxValue)
					{
						vsRateTableS.TextMatrix(vsRateTableS.Row + 1, 1, vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col));
						if (Conversion.Val(vsRateTableS.TextMatrix(vsRateTableS.Row + 1, 2)) == 0)
						{
							vsRateTableS.TextMatrix(vsRateTableS.Row + 1, 2, FCConvert.ToString(MaxValue));
							if (vsRateTableS.Row < 7)
							{
								for (i = vsRateTableS.Row + 2; i <= 8; i++)
								{
									vsRateTableS.TextMatrix(i, 1, FCConvert.ToString(0));
									vsRateTableS.TextMatrix(i, 2, FCConvert.ToString(0));
								}
							}
						}
					}
				}
			}
			if (vsRateTableS.Col == 3)
			{
				if (Conversion.Val(vsRateTableS.TextMatrix(vsRateTableS.Row, 2)) == MaxValue)
				{
					for (i = vsRateTableS.Row + 1; i <= 8; i++)
					{
						vsRateTableS.TextMatrix(i, 1, FCConvert.ToString(0));
						vsRateTableS.TextMatrix(i, 2, FCConvert.ToString(0));
					}
				}
			}
		}

		private void vsRateTableS_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsRateTableS.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsRateTableS_ClickEvent(object sender, System.EventArgs e)
		{
			switch (vsRateTableS.Col)
			{
				case 1:
					{
						vsRateTableS.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case 2:
					{
						vsRateTableS.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableS.EditCell();
						vsRateTableS.EditSelStart = 0;
						vsRateTableS.EditSelLength = vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col).Length;
						vsRateTableS.EditMaxLength = 9;
						break;
					}
				case 3:
					{
						vsRateTableS.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableS.EditCell();
						vsRateTableS.EditSelStart = 0;
						vsRateTableS.EditSelLength = vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col).Length;
						vsRateTableS.EditMaxLength = 9;
						break;
					}
				case 4:
					{
						vsRateTableS.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableS.EditCell();
						vsRateTableS.EditSelStart = 0;
						vsRateTableS.EditSelLength = vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col).Length;
						vsRateTableS.EditMaxLength = 5;
						break;
					}
			}
			//end switch
		}

		private void vsRateTableS_Enter(object sender, System.EventArgs e)
		{
			if (vsRateTableS.Col == vsRateTableS.Cols - 1 && vsRateTableS.Row == vsRateTableS.Rows - 1)
			{
				vsRateTableS.Select(1, 0);
			}
			else
			{
				vsRateTableS.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsRateTableS_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				if (vsRateTableS.Col < (vsRateTableS.Cols - 1))
				{
					vsRateTableS.Col += 1;
				}
				else
				{
					if (vsRateTableS.Row < 8)
					{
						vsRateTableS.Row += 1;
					}
					vsRateTableS.Col = 2;
				}
			}
		}

		private void vsRateTableS_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int lngDecPlace = 0;
			int KeyAscii = Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if (vsRateTableS.Col == 3)
			{
				if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii == 45))
				{
				}
				else if (KeyAscii == 46)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, vsRateTableS.EditText, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (vsRateTableS.EditSelStart < lngDecPlace && vsRateTableS.EditSelLength + vsRateTableS.EditSelStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = 0;
						}
					}
				}
				else
				{
					KeyAscii = 0;
				}
				// If (KeyAscii < 46 Or KeyAscii > 57 Or KeyAscii = 47) And KeyAscii <> 8 Then
				// KeyAscii = 0                'any key other than backspace or number keys are not allowed
				// ElseIf KeyAscii = 45 Then
				// this is the negative
				// If vsRateTableS.EditText <> "" Then
				// vsRateTableS.EditText = Abs(vsRateTableS.EditText) * -1
				// Else
				// vsRateTableS.TextMatrix(Row, Col) = Abs(vsRateTableS.TextMatrix(Row, Col)) * -1
				// End If
				// KeyAscii = 0
				// End If
			}
			else
			{
				if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
				{
					KeyAscii = 0;
					// any key other than backspace or number keys are not allowed
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsRateTableS_RowColChange(object sender, System.EventArgs e)
		{
			vsRateTableS.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			switch (vsRateTableS.Col)
			{
				case 1:
					{
						vsRateTableS.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case 2:
					{
						vsRateTableS.EditMaxLength = 9;
						vsRateTableS.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableS.EditCell();
						vsRateTableS.EditSelStart = 0;
						vsRateTableS.EditSelLength = vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col).Length;
						break;
					}
				case 3:
					{
						vsRateTableS.EditMaxLength = 9;
						vsRateTableS.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableS.EditCell();
						vsRateTableS.EditSelStart = 0;
						vsRateTableS.EditSelLength = vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col).Length;
						break;
					}
				case 4:
					{
						vsRateTableS.EditMaxLength = 5;
						vsRateTableS.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableS.EditCell();
						vsRateTableS.EditSelStart = 0;
						vsRateTableS.EditSelLength = vsRateTableS.TextMatrix(vsRateTableS.Row, vsRateTableS.Col).Length;
						if (vsRateTableS.Row == vsRateTableS.Rows - 1)
						{
							vsRateTableS.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
						}
						break;
					}
			}
			//end switch
		}

		private void vsRateTableW_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// this makes sure that the over and thru cols are not the same value and sets row+1's over col
			int i;
			int MaxValue;
			MaxValue = 999999999;
			if (vsRateTableW.Col == 2)
			{
				if (Conversion.Val(vsRateTableW.TextMatrix(vsRateTableW.Row, 2)) < MaxValue && !(Conversion.Val(vsRateTableW.TextMatrix(vsRateTableW.Row, 1)) == 0 && vsRateTableW.Row != 1))
				{
					if (Conversion.Val(vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col)) <= Conversion.Val(vsRateTableW.TextMatrix(vsRateTableW.Row, 1)))
					{
						// greater than or equal to the "Over" Amount in the same row
						vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col, FCConvert.ToString(Conversion.Val(vsRateTableW.TextMatrix(vsRateTableW.Row, 1)) + 1));
						// add one to the
						for (i = vsRateTableW.Row; i <= 8; i++)
						{
							vsRateTableW.TextMatrix(i, 1, FCConvert.ToString(0));
							vsRateTableW.TextMatrix(i, 2, FCConvert.ToString(0));
						}
					}
					if (vsRateTableW.Row < 8 && Conversion.Val(vsRateTableW.TextMatrix(vsRateTableW.Row, 1)) < MaxValue)
					{
						vsRateTableW.TextMatrix(vsRateTableW.Row + 1, 1, vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col));
						if (Conversion.Val(vsRateTableW.TextMatrix(vsRateTableW.Row + 1, 2)) == 0)
						{
							vsRateTableW.TextMatrix(vsRateTableW.Row + 1, 2, FCConvert.ToString(MaxValue));
							if (vsRateTableW.Row < 7)
							{
								for (i = vsRateTableW.Row + 2; i <= 8; i++)
								{
									vsRateTableW.TextMatrix(i, 1, FCConvert.ToString(0));
									vsRateTableW.TextMatrix(i, 2, FCConvert.ToString(0));
								}
							}
						}
					}
				}
			}
			if (vsRateTableW.Col == 3)
			{
				if (Conversion.Val(vsRateTableW.TextMatrix(vsRateTableW.Row, 2)) == MaxValue)
				{
					for (i = vsRateTableW.Row + 1; i <= 8; i++)
					{
						vsRateTableW.TextMatrix(i, 1, FCConvert.ToString(0));
						vsRateTableW.TextMatrix(i, 2, FCConvert.ToString(0));
					}
				}
			}
		}

		private void vsRateTableW_ChangeEdit(object sender, System.EventArgs e)
		{
			if (this.vsRateTableW.IsCurrentCellInEditMode)
			{
				Dirty = true;
			}
		}

		private void vsRateTableW_ClickEvent(object sender, System.EventArgs e)
		{
			switch (vsRateTableW.Col)
			{
				case 1:
					{
						vsRateTableW.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case 2:
					{
						vsRateTableW.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableW.EditCell();
						vsRateTableW.EditSelStart = 0;
						vsRateTableW.EditSelLength = vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col).Length;
						vsRateTableW.EditMaxLength = 9;
						break;
					}
				case 3:
					{
						vsRateTableW.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableW.EditCell();
						vsRateTableW.EditSelStart = 0;
						vsRateTableW.EditSelLength = vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col).Length;
						vsRateTableW.EditMaxLength = 9;
						break;
					}
				case 4:
					{
						vsRateTableW.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableW.EditCell();
						vsRateTableW.EditSelStart = 0;
						vsRateTableW.EditSelLength = vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col).Length;
						vsRateTableW.EditMaxLength = 5;
						break;
					}
			}
			//end switch
		}

		private void vsRateTableW_Enter(object sender, System.EventArgs e)
		{
			if (vsRateTableW.Col == vsRateTableW.Cols - 1 && vsRateTableW.Row == vsRateTableW.Rows - 1)
			{
				vsRateTableW.Select(1, 0);
			}
			else
			{
				vsRateTableW.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			}
		}

		private void vsRateTableW_KeyDownEdit(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Return)
			{
				if (vsRateTableW.Col < (vsRateTableW.Cols - 1))
				{
					vsRateTableW.Col += 1;
				}
				else
				{
					if (vsRateTableW.Row < 8)
					{
						vsRateTableW.Row += 1;
					}
					vsRateTableW.Col = 2;
				}
			}
		}

		private void vsRateTableW_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			int lngDecPlace = 0;
			int KeyAscii = Strings.Asc(e.KeyChar);
			// traps the backspace key and all keys that are non numeric
			if (vsRateTableW.Col == 3)
			{
				if ((KeyAscii >= 48 && KeyAscii <= 57) || (KeyAscii == 8) || (KeyAscii == 13) || (KeyAscii == 45))
				{
				}
				else if (KeyAscii == 46)
				{
					// decimal point
					lngDecPlace = Strings.InStr(1, vsRateTableW.EditText, ".", CompareConstants.vbBinaryCompare);
					if (lngDecPlace != 0)
					{
						if (vsRateTableW.EditSelStart < lngDecPlace && vsRateTableW.EditSelLength + vsRateTableW.EditSelStart >= lngDecPlace)
						{
							// if the decimal place is being highlighted and will be written over, then allow it
						}
						else
						{
							// if there is already a decimal point then stop others
							KeyAscii = 0;
						}
					}
				}
				else
				{
					KeyAscii = 0;
				}
				// If (KeyAscii < 46 Or KeyAscii > 57 Or KeyAscii = 47) And KeyAscii <> 8 Then
				// KeyAscii = 0                'any key other than backspace or number keys are not allowed
				// ElseIf KeyAscii = 45 Then
				// this is the negative
				// If vsRateTableW.EditText <> "" Then
				// vsRateTableW.EditText = Abs(vsRateTableW.EditText) * -1
				// Else
				// vsRateTableW.TextMatrix(Row, Col) = Abs(vsRateTableW.TextMatrix(Row, Col)) * -1
				// End If
				// KeyAscii = 0
				// End If
			}
			else
			{
				if ((KeyAscii < 48 || KeyAscii > 57) && KeyAscii != 8)
				{
					KeyAscii = 0;
					// any key other than backspace or number keys are not allowed
				}
			}
			e.KeyChar = Strings.Chr(KeyAscii);
		}

		private void vsRateTableW_RowColChange(object sender, System.EventArgs e)
		{
			vsRateTableW.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
			switch (vsRateTableW.Col)
			{
				case 1:
					{
						vsRateTableW.Editable = FCGrid.EditableSettings.flexEDNone;
						break;
					}
				case 2:
					{
						vsRateTableW.EditMaxLength = 9;
						vsRateTableW.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableW.EditCell();
						vsRateTableW.EditSelStart = 0;
						vsRateTableW.EditSelLength = vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col).Length;
						break;
					}
				case 3:
					{
						vsRateTableW.EditMaxLength = 9;
						vsRateTableW.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableW.EditCell();
						vsRateTableW.EditSelStart = 0;
						vsRateTableW.EditSelLength = vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col).Length;
						break;
					}
				case 4:
					{
						vsRateTableW.EditMaxLength = 5;
						vsRateTableW.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
						vsRateTableW.EditCell();
						vsRateTableW.EditSelStart = 0;
						vsRateTableW.EditSelLength = vsRateTableW.TextMatrix(vsRateTableW.Row, vsRateTableW.Col).Length;
						if (vsRateTableW.Row == vsRateTableW.Rows - 1)
						{
							vsRateTableW.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
						}
						break;
					}
			}
			//end switch
		}

		private bool ValidateAccount(ref string F1, ref string F2, ref string F3, ref string F4, ref string F5)
		{
			bool ValidateAccount = false;
			// this will validate each account number by searcing for each piece or the account string
			clsDRWrapper rsValid = new clsDRWrapper();
			if (F1 == "G")
			{
				rsValid.OpenRecordset("SELECT * FROM LedgerTitles WHERE Fund = '" + F2 + "' AND Account = '" + F3 + "' AND ([Year] = 'AL' OR [Year] = '" + F4 + "')");
				if (rsValid.RecordCount() == 0)
				{
					ValidateAccount = false;
				}
				else
				{
					ValidateAccount = true;
				}
			}
			else if (F1 == "R")
			{
				rsValid.OpenRecordset("SELECT * FROM RevTitles WHERE Department = '" + F2 + "' AND Division = '" + F3 + "' AND Revenue = '" + F4 + "'");
				if (rsValid.RecordCount() == 0)
				{
					ValidateAccount = false;
				}
				else
				{
					ValidateAccount = true;
				}
			}
			else if (F1 == "E")
			{
				rsValid.OpenRecordset("SELECT * FROM DeptDivTitles WHERE Department = '" + F2 + "' AND Division = '" + F3 + "'");
				if (rsValid.RecordCount() == 0)
				{
					ValidateAccount = false;
				}
				else
				{
					rsValid.OpenRecordset("SELECT * FROM ExpObjTitles WHERE Expense = '" + F4 + "' AND Object = '" + F5 + "'");
					if (rsValid.RecordCount() == 0)
					{
						ValidateAccount = false;
					}
					else
					{
						ValidateAccount = true;
					}
				}
			}
			return ValidateAccount;
		}

		private void FillRTCombo()
		{
			// this sub will fill the Book combo box with the available book numbers
			clsDRWrapper rsTemp = new clsDRWrapper();
			int i;
			string temp = "";
			cmbRT.Clear();
			rsTemp.OpenRecordset("SELECT * FROM RateTable ORDER BY RateTableNumber Asc");
			if (rsTemp.RecordCount() > 0)
			{
				for (i = 1; i <= rsTemp.RecordCount(); i++)
				{
					temp = modMain.PadToString_8(rsTemp.Get_Fields_Int32("RateTableNumber"), 2) + " - " + rsTemp.Get_Fields_String("RateTableDescription");
					cmbRT.AddItem(temp);
					// enters the book number and description into the combo box
					rsTemp.MoveNext();
				}
			}
		}

		private void ResetBooksByRT(ref int lngRT)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsRT = new clsDRWrapper();
				string strSQL;
				string strBookString = "";
				strSQL = "SELECT DISTINCT BookNumber FROM MeterTable WHERE WaterKey1 = " + FCConvert.ToString(lngRT) + " OR WaterKey2 = " + FCConvert.ToString(lngRT) + " OR WaterKey3 = " + FCConvert.ToString(lngRT) + " OR WaterKey4 = " + FCConvert.ToString(lngRT) + " OR WaterKey5 = " + FCConvert.ToString(lngRT) + " OR ";
				strSQL += "SewerKey1 = " + FCConvert.ToString(lngRT) + " Or SewerKey2 = " + FCConvert.ToString(lngRT) + " Or SewerKey3 = " + FCConvert.ToString(lngRT) + " Or SewerKey4 = " + FCConvert.ToString(lngRT) + " Or SewerKey5 = " + FCConvert.ToString(lngRT);
				rsRT.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!rsRT.EndOfFile())
				{
					while (!rsRT.EndOfFile())
					{
						strBookString += rsRT.Get_Fields_Int32("BookNumber") + ",";
						rsRT.MoveNext();
					}
					// this will reset the book status of all of the books associated with the meters associated with this rate table
					if (strBookString.Length > 0)
					{
						// remove the last comma
						strBookString = Strings.Left(strBookString, strBookString.Length - 1);
						rsRT.OpenRecordset("SELECT * FROM Book WHERE BookNumber IN (" + strBookString + ")", modExtraModules.strUTDatabase);
						while (!rsRT.EndOfFile())
						{
							modMain.ResetBookStatus_2(rsRT.Get_Fields_Int32("BookNumber"));
							rsRT.MoveNext();
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Resetting Book Status By Rate Table", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetGridVariables()
		{
			// Category Grid
			lngCatColCode = 0;
			lngCatColDescription = 1;
			lngCatColShort = 2;
			lngCatColWAccount = 3;
			lngCatColSAccount = 4;
			lngCatColWTaxable = 5;
			lngCatColSTaxable = 6;
		}
		// MAL@20070904: No longer used
		// Private Sub vsSMisc_Click()
		// Dim lngMR           As Long
		// Dim lngMC           As Long
		//
		// With vsSMisc
		// lngMR = .MouseRow
		// lngMC = .MouseCol
		//
		// Select Case .Col
		// Case 0      'Taxable
		// If .TextMatrix(lngMR, lngMC) = -1 Then  'change what is in the grid rather than letting an edit happen
		// .TextMatrix(lngMR, lngMC) = 0
		// Else
		// .TextMatrix(lngMR, lngMC) = -1
		// End If
		// End Select
		// End With
		// End Sub
		// MAL@20070904: No longer used
		// Private Sub vsWMisc_Click()
		// Dim lngMR           As Long
		// Dim lngMC           As Long
		//
		// With vsWMisc
		// lngMR = .MouseRow
		// lngMC = .MouseCol
		//
		// Select Case .Col
		// Case 0      'Taxable
		// If .TextMatrix(lngMR, lngMC) = -1 Then 'change what is in the grid rather than letting an edit happen
		// .TextMatrix(lngMR, lngMC) = 0
		// Else
		// .TextMatrix(lngMR, lngMC) = -1
		// End If
		// End Select
		// End With
		// End Sub
		//FC:FINAL:CHN - issue #1025: fix allow to enter numbers.
		private void VsWMisc_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				if (vsWMisc.Col == 2)
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
					}
				}
			}
		}

		private void VsSMisc_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				if (vsSMisc.Col == 2)
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
					}
				}
			}
		}

		private void VsRateTableS_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				var box = e.Control as TextBox;
				if (box != null)
				{
					if (vsRateTableS.Col == 2 || vsRateTableS.Col == 4)
					{
                        box.AllowOnlyNumericInput();
                        box.RemoveAlphaCharactersOnValueChange();
					}
					if (vsRateTableS.Col == 3)
					{
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
					}
				}
			}
		}

		private void VsRateTableW_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				var box = e.Control as TextBox;
				if (box != null)
				{
					if (vsRateTableW.Col == 2 || vsRateTableW.Col == 4)
					{
                        box.AllowOnlyNumericInput();
                        box.RemoveAlphaCharactersOnValueChange();
					}
					if (vsRateTableW.Col == 3)
					{
                        box.AllowOnlyNumericInput(true);
                        box.RemoveAlphaCharactersOnValueChange();
					}
				}
			}
		}
	}
}
