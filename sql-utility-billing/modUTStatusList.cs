﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;
using System.Diagnostics;

namespace TWUT0000
{
	public class modUTStatusList
	{
		public const string CUSTOMREPORTDATABASE = "TWUT0000.VB1";
		// for frmCustomLabels
		// These constants are for frmCustomLabels
		public const int GRIDNONE = 0;
		public const int GRIDTEXT = 1;
		public const int GRIDDATE = 2;
		public const int GRIDCOMBOIDTEXT = 3;
		public const int GRIDNUMRANGE = 4;
		public const int GRIDCOMBOIDNUM = 5;
		public const int GRIDCOMBOTEXT = 6;
		public const int GRIDTEXTRANGE = 7;
		public const int GRIDCOMBOANDNUM = 8;
		// THIS HAS A COMBO BOX AND NUMERIC FIELD
		public const int GRIDCOMBORANGE = 9;
		public const int GRIDDATERANGE = 10;
		// vbPorter upgrade warning: strValue As string	OnWrite(VB.Label)
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string FixQuotes(string strValue)
		{
			string FixQuotes = "";
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= Information.UBound(Statics.strComboList, 1); intCount++)
			{
				Statics.strComboList[intCount, 0] = string.Empty;
				Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool IsValidDate(string DateToCheck)
		{
			bool IsValidDate = false;
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As short --> As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As short --> As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							MessageBox.Show("Invalid day on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							MessageBox.Show("Invalid day on Date", "ErTRIO Softwarevror", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}
		// vbPorter upgrade warning: intShowOwnerType As short	OnWriteFCConvert.ToInt32(
		public static string GetStatusName_6(clsDRWrapper rsD, short intShowOwnerType)
		{
			return GetStatusName(ref rsD, ref intShowOwnerType);
		}

		public static string GetStatusName(ref clsDRWrapper rsD, ref short intShowOwnerType)
		{
			string GetStatusName = "";
			// this sub will figure out what name needs to be on this report
			clsDRWrapper rsRE = new clsDRWrapper();
			clsDRWrapper rsSR = new clsDRWrapper();
			rsRE.OpenRecordset("SELECT p.FullNameLF AS Name FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE ID = " + rsD.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);
			Debug.Assert(true);
			// XXXX kgk  This is more CL code, does it get run?
			Debug.Assert(false);
			if (!rsRE.EndOfFile())
			{
				// if this is true then get the info from the RE Master, else get it from the bill record
				switch (intShowOwnerType)
				{
					case 0:
						{
							// show the owners name
							GetStatusName = FCConvert.ToString(rsRE.Get_Fields_String("Name"));
							break;
						}
					case 1:
						{
							if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(FCConvert.ToString(rsRE.Get_Fields_String("Name"))))
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsSR.OpenRecordset("SELECT * FROM SRMaster WHERE RSAccount = " + rsD.Get_Fields("Account") + " AND RSCard = 1 AND SaleDate > '" + rsD.Get_Fields_DateTime("TransferFromBillingDateLast") + "'", modExtraModules.strREDatabase);
								if (rsSR.EndOfFile())
								{
									GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
								}
								else
								{
									GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2") + " C\\O " + rsRE.Get_Fields_String("Name");
								}
							}
							else
							{
								GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
							}
							break;
						}
					case 2:
						{
							if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(FCConvert.ToString(rsRE.Get_Fields_String("Name"))))
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsSR.OpenRecordset("SELECT * FROM SRMaster WHERE RSAccount = " + rsD.Get_Fields("Account") + " AND RSCard = 1 AND SaleDate > #" + rsD.Get_Fields_DateTime("TransferFromBillingDateLast") + "#", modExtraModules.strREDatabase);
								if (rsSR.EndOfFile())
								{
									GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
								}
								else
								{
									GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2") + " C\\O " + rsRE.Get_Fields_String("Name");
								}
							}
							else
							{
								GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
							}
							break;
						}
					case 3:
						{
							if (Strings.UCase(rsD.Get_Fields_String("Name1")) != Strings.UCase(FCConvert.ToString(rsRE.Get_Fields_String("Name"))))
							{
								// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
								rsSR.OpenRecordset("SELECT * FROM SRMaster WHERE RSAccount = " + rsD.Get_Fields("Account") + " AND RSCard = 1 AND SaleDate > #" + rsD.Get_Fields_DateTime("TransferFromBillingDateLast") + "#", modExtraModules.strREDatabase);
								if (rsSR.EndOfFile())
								{
									GetStatusName = FCConvert.ToString(rsRE.Get_Fields_String("Name"));
								}
								else
								{
									GetStatusName = rsRE.Get_Fields_String("Name") + " Bill = " + rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
								}
							}
							else
							{
								GetStatusName = FCConvert.ToString(rsRE.Get_Fields_String("Name"));
							}
							break;
						}
				}
				//end switch
			}
			else
			{
				GetStatusName = rsD.Get_Fields_String("Name1") + " " + rsD.Get_Fields_String("Name2");
			}
			return GetStatusName;
		}

		public static void SetupStatusListCombos_2(bool boolRegular)
		{
			SetupStatusListCombos(boolRegular);
		}

		public static void SetupStatusListCombos(bool boolRegular = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strRK = "";
				string strBook = "";
				// this will reset the arrays
				ClearComboListArray();
				// fill the book string
				rsTemp.OpenRecordset("SELECT * FROM Book ORDER BY BookNumber asc", modExtraModules.strUTDatabase);
				while (!rsTemp.EndOfFile())
				{
					strBook += "#" + rsTemp.Get_Fields_Int32("BookNumber") + ";" + modMain.PadToString_8(rsTemp.Get_Fields_Int32("BookNumber"), 5) + "\t" + rsTemp.Get_Fields_String("Description") + "|";
					rsTemp.MoveNext();
				}
				// fill the book string
				rsTemp.OpenRecordset("SELECT * FROM RateKeys WHERE RateType <> 'L' ORDER BY ID desc", modExtraModules.strUTDatabase);
				while (!rsTemp.EndOfFile())
				{
					strRK += "#" + rsTemp.Get_Fields_Int32("ID") + ";" + modMain.PadToString_8(rsTemp.Get_Fields_Int32("ID"), 5) + "\t" + "  " + Strings.Format(rsTemp.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "|";
					rsTemp.MoveNext();
				}
				Statics.strComboList[frmUTStatusList.InstancePtr.lngRowTaxAcquired, 0] = "#0;Non Tax Acquired|#1;Tax Acquired|2;Both";
				// fill them correctly
				if (boolRegular)
				{
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowReportType, 0] = "#0;Regular|#1;Aged Report";
					if (modUTStatusPayments.Statics.TownService == "B")
					{
						// kk trouts-6 03012013  Change Water to Stormwater for Bangor
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							Statics.strComboList[frmUTStatusList.InstancePtr.lngRowWS, 0] = "#0;Stormwater|#1;Sewer";
						}
						else
						{
							Statics.strComboList[frmUTStatusList.InstancePtr.lngRowWS, 0] = "#0;Water|#1;Sewer";
						}
					}
					else if (modUTStatusPayments.Statics.TownService == "W")
					{
						Statics.strComboList[frmUTStatusList.InstancePtr.lngRowWS, 0] = "#0;Water";
					}
					else if (modUTStatusPayments.Statics.TownService == "S")
					{
						Statics.strComboList[frmUTStatusList.InstancePtr.lngRowWS, 0] = "#1;Sewer";
					}
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowShowLien, 0] = "#0;Regular|#1;Lien|#2;Both";
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBook, 0] = strBook;
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBook, 1] = strBook;
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBill, 0] = strRK;
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBill, 1] = strRK;
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBalanceDue, 0] = "#0;<" + "\t" + "Less Than|#1;>" + "\t" + "Greater Than|#2;=" + "\t" + "Equals|#3;<>" + "\t" + "Not Equal";
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowGroupBy, 0] = "#0;Account|#1;Book";
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowPaymentType, 0] = "#0;Payments|#1;Corrections|#3;Lien / 30 Day Costs|#4;Prepayments|#5;Abatements";
				}
				else
				{
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowReportType, 0] = "#0;Regular|#1;Aged Report";
					if (modUTStatusPayments.Statics.TownService == "B")
					{
						// kk trouts-6 03012013  Change Water to Stormwater for Bangor
						if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
						{
							Statics.strComboList[frmUTStatusList.InstancePtr.lngRowWS, 0] = "#0;Stormwater|#1;Sewer|#2;Both";
						}
						else
						{
							Statics.strComboList[frmUTStatusList.InstancePtr.lngRowWS, 0] = "#0;Water|#1;Sewer|#2;Both";
						}
					}
					else if (modUTStatusPayments.Statics.TownService == "W")
					{
						Statics.strComboList[frmUTStatusList.InstancePtr.lngRowWS, 0] = "#0;Water";
					}
					else if (modUTStatusPayments.Statics.TownService == "S")
					{
						Statics.strComboList[frmUTStatusList.InstancePtr.lngRowWS, 0] = "#1;Sewer";
					}
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowShowLien, 0] = "#0;Regular|#1;Lien|#2;Both";
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBook, 0] = strBook;
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBook, 1] = strBook;
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBill, 0] = strRK;
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBill, 1] = strRK;
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowBalanceDue, 0] = "#0;<" + "\t" + "Less Than|#1;>" + "\t" + "Greater Than|#2;=" + "\t" + "Equals|#3;<>" + "\t" + "Not Equal";
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowGroupBy, 0] = "";
					Statics.strComboList[frmUTStatusList.InstancePtr.lngRowPaymentType, 0] = "";
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Combos", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public class StaticVariables
		{
			// ********************************************************
			// LAST UPDATED   :               01/23/2007              *
			// MODIFIED BY    :               Jim Bertolino           *
			// *
			// DATE           :               12/03/2004              *
			// WRITTEN BY     :               Jim Bertolino           *
			// *
			// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
			// ********************************************************
			//=========================================================
			public int intNumberOfSQLFields;
			public string[] strFields = new string[20 + 1];
			public string[] strFieldNames = new string[50 + 1];
			// this is for the mortgage labels
			public string[,] strComboList = new string[30 + 1, 25 + 1];
			public string strCustomTitle = string.Empty;
			public string strReportType = "";
			// for frmFreeReport
			public string[] strWhereType = new string[20 + 1];
			// for frmCustomLabels
			public string[] strCaptions = new string[20 + 1];
			// for frmCustomLabels
			public string[] strFieldCaptions = new string[20 + 1];
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
