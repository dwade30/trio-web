﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arMeterDataExtract.
	/// </summary>
	partial class arMeterDataExtract
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arMeterDataExtract));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWaterHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSewerHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWType1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWType2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWType3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWType4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWType5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field7 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field8 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field9 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field10 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field11 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field12 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field13 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field14 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field15 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTypeW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmountW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRateW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccountW = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field16 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field17 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field18 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field19 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field20 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field21 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field22 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field23 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field24 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field25 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field26 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field27 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field28 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field29 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field30 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field31 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field32 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field33 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field34 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field35 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWater = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSewer = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldTotalW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewerHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTypeW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmountW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcct,
				this.fldDesc,
				this.fldS,
				this.fldW,
				this.fldWType1,
				this.fldWType2,
				this.fldWType3,
				this.fldWType4,
				this.fldWType5,
				this.Field1,
				this.Field2,
				this.Field3,
				this.Field4,
				this.Field5,
				this.Field6,
				this.Field7,
				this.Field8,
				this.Field9,
				this.Field10,
				this.Field11,
				this.Field12,
				this.Field13,
				this.Field14,
				this.Field15,
				this.lblTypeW,
				this.lblAmountW,
				this.lblRateW,
				this.lblAccountW,
				this.Field16,
				this.Field17,
				this.Field18,
				this.Field19,
				this.Field20,
				this.Field21,
				this.Field22,
				this.Field23,
				this.Field24,
				this.Field25,
				this.Field26,
				this.Field27,
				this.Field28,
				this.Field29,
				this.Field30,
				this.Field31,
				this.Field32,
				this.Field33,
				this.Field34,
				this.Field35,
				this.Label2,
				this.Label3,
				this.Label4,
				this.Label5,
				this.lblWater,
				this.lblSewer
			});
			this.Detail.Height = 1.614583F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccount,
				this.lblDesc,
				this.lblWaterHeader,
				this.lblSewerHeader,
				this.Line2,
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName
			});
			this.GroupHeader1.Height = 0.7604167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalW,
				this.fldTotalCount,
				this.fldTotalS,
				this.Line1
			});
			this.GroupFooter1.Height = 0.3333333F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.5625F;
			this.lblAccount.Width = 0.875F;
			// 
			// lblDesc
			// 
			this.lblDesc.Height = 0.1875F;
			this.lblDesc.HyperLink = null;
			this.lblDesc.Left = 1F;
			this.lblDesc.Name = "lblDesc";
			this.lblDesc.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblDesc.Text = "Description";
			this.lblDesc.Top = 0.5625F;
			this.lblDesc.Width = 2.5F;
			// 
			// lblWaterHeader
			// 
			this.lblWaterHeader.Height = 0.1875F;
			this.lblWaterHeader.HyperLink = null;
			this.lblWaterHeader.Left = 3.625F;
			this.lblWaterHeader.Name = "lblWaterHeader";
			this.lblWaterHeader.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblWaterHeader.Text = "W Cat";
			this.lblWaterHeader.Top = 0.5625F;
			this.lblWaterHeader.Width = 0.5F;
			// 
			// lblSewerHeader
			// 
			this.lblSewerHeader.Height = 0.1875F;
			this.lblSewerHeader.HyperLink = null;
			this.lblSewerHeader.Left = 4.25F;
			this.lblSewerHeader.Name = "lblSewerHeader";
			this.lblSewerHeader.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblSewerHeader.Text = "S Cat";
			this.lblSewerHeader.Top = 0.5625F;
			this.lblSewerHeader.Width = 0.4375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.75F;
			this.Line2.Width = 7.5F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 7.5F;
			this.Line2.Y1 = 0.75F;
			this.Line2.Y2 = 0.75F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.3125F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Meter Information Report";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7.625F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.5F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.125F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 6.5F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.875F;
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.875F;
			// 
			// fldDesc
			// 
			this.fldDesc.Height = 0.1875F;
			this.fldDesc.Left = 1F;
			this.fldDesc.Name = "fldDesc";
			this.fldDesc.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldDesc.Text = null;
			this.fldDesc.Top = 0F;
			this.fldDesc.Width = 2.5F;
			// 
			// fldS
			// 
			this.fldS.Height = 0.1875F;
			this.fldS.Left = 4.3125F;
			this.fldS.Name = "fldS";
			this.fldS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldS.Text = null;
			this.fldS.Top = 0F;
			this.fldS.Width = 0.375F;
			// 
			// fldW
			// 
			this.fldW.Height = 0.1875F;
			this.fldW.Left = 3.6875F;
			this.fldW.Name = "fldW";
			this.fldW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldW.Text = null;
			this.fldW.Top = 0F;
			this.fldW.Width = 0.4375F;
			// 
			// fldWType1
			// 
			this.fldWType1.Height = 0.1875F;
			this.fldWType1.Left = 0.1875F;
			this.fldWType1.Name = "fldWType1";
			this.fldWType1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldWType1.Text = null;
			this.fldWType1.Top = 0.5625F;
			this.fldWType1.Width = 0.375F;
			// 
			// fldWType2
			// 
			this.fldWType2.Height = 0.1875F;
			this.fldWType2.Left = 0.1875F;
			this.fldWType2.Name = "fldWType2";
			this.fldWType2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldWType2.Text = null;
			this.fldWType2.Top = 0.75F;
			this.fldWType2.Width = 0.375F;
			// 
			// fldWType3
			// 
			this.fldWType3.Height = 0.1875F;
			this.fldWType3.Left = 0.1875F;
			this.fldWType3.Name = "fldWType3";
			this.fldWType3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldWType3.Text = null;
			this.fldWType3.Top = 0.9375F;
			this.fldWType3.Width = 0.375F;
			// 
			// fldWType4
			// 
			this.fldWType4.Height = 0.1875F;
			this.fldWType4.Left = 0.1875F;
			this.fldWType4.Name = "fldWType4";
			this.fldWType4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldWType4.Text = null;
			this.fldWType4.Top = 1.125F;
			this.fldWType4.Width = 0.375F;
			// 
			// fldWType5
			// 
			this.fldWType5.Height = 0.1875F;
			this.fldWType5.Left = 0.1875F;
			this.fldWType5.Name = "fldWType5";
			this.fldWType5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldWType5.Text = null;
			this.fldWType5.Top = 1.3125F;
			this.fldWType5.Width = 0.375F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 0.625F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field1.Text = null;
			this.Field1.Top = 0.5625F;
			this.Field1.Width = 1.1875F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 0.625F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field2.Text = null;
			this.Field2.Top = 0.75F;
			this.Field2.Width = 1.1875F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 0.625F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field3.Text = null;
			this.Field3.Top = 0.9375F;
			this.Field3.Width = 1.1875F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 0.625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field4.Text = null;
			this.Field4.Top = 1.125F;
			this.Field4.Width = 1.1875F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 0.625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field5.Text = null;
			this.Field5.Top = 1.3125F;
			this.Field5.Width = 1.1875F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 1.875F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field6.Text = null;
			this.Field6.Top = 0.5625F;
			this.Field6.Width = 0.375F;
			// 
			// Field7
			// 
			this.Field7.Height = 0.1875F;
			this.Field7.Left = 1.875F;
			this.Field7.Name = "Field7";
			this.Field7.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field7.Text = null;
			this.Field7.Top = 0.75F;
			this.Field7.Width = 0.375F;
			// 
			// Field8
			// 
			this.Field8.Height = 0.1875F;
			this.Field8.Left = 1.875F;
			this.Field8.Name = "Field8";
			this.Field8.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field8.Text = null;
			this.Field8.Top = 0.9375F;
			this.Field8.Width = 0.375F;
			// 
			// Field9
			// 
			this.Field9.Height = 0.1875F;
			this.Field9.Left = 1.875F;
			this.Field9.Name = "Field9";
			this.Field9.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field9.Text = null;
			this.Field9.Top = 1.125F;
			this.Field9.Width = 0.375F;
			// 
			// Field10
			// 
			this.Field10.Height = 0.1875F;
			this.Field10.Left = 1.875F;
			this.Field10.Name = "Field10";
			this.Field10.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field10.Text = null;
			this.Field10.Top = 1.3125F;
			this.Field10.Width = 0.375F;
			// 
			// Field11
			// 
			this.Field11.Height = 0.1875F;
			this.Field11.Left = 2.3125F;
			this.Field11.Name = "Field11";
			this.Field11.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field11.Text = null;
			this.Field11.Top = 0.5625F;
			this.Field11.Width = 1.1875F;
			// 
			// Field12
			// 
			this.Field12.Height = 0.1875F;
			this.Field12.Left = 2.3125F;
			this.Field12.Name = "Field12";
			this.Field12.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field12.Text = null;
			this.Field12.Top = 0.75F;
			this.Field12.Width = 1.1875F;
			// 
			// Field13
			// 
			this.Field13.Height = 0.1875F;
			this.Field13.Left = 2.3125F;
			this.Field13.Name = "Field13";
			this.Field13.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field13.Text = null;
			this.Field13.Top = 0.9375F;
			this.Field13.Width = 1.1875F;
			// 
			// Field14
			// 
			this.Field14.Height = 0.1875F;
			this.Field14.Left = 2.3125F;
			this.Field14.Name = "Field14";
			this.Field14.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field14.Text = null;
			this.Field14.Top = 1.125F;
			this.Field14.Width = 1.1875F;
			// 
			// Field15
			// 
			this.Field15.Height = 0.1875F;
			this.Field15.Left = 2.3125F;
			this.Field15.Name = "Field15";
			this.Field15.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field15.Text = null;
			this.Field15.Top = 1.3125F;
			this.Field15.Width = 1.1875F;
			// 
			// lblTypeW
			// 
			this.lblTypeW.Height = 0.1875F;
			this.lblTypeW.HyperLink = null;
			this.lblTypeW.Left = 0.1875F;
			this.lblTypeW.Name = "lblTypeW";
			this.lblTypeW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblTypeW.Text = "Type";
			this.lblTypeW.Top = 0.375F;
			this.lblTypeW.Width = 0.375F;
			// 
			// lblAmountW
			// 
			this.lblAmountW.Height = 0.1875F;
			this.lblAmountW.HyperLink = null;
			this.lblAmountW.Left = 0.625F;
			this.lblAmountW.Name = "lblAmountW";
			this.lblAmountW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAmountW.Text = "Amount";
			this.lblAmountW.Top = 0.375F;
			this.lblAmountW.Width = 1.1875F;
			// 
			// lblRateW
			// 
			this.lblRateW.Height = 0.1875F;
			this.lblRateW.HyperLink = null;
			this.lblRateW.Left = 1.875F;
			this.lblRateW.Name = "lblRateW";
			this.lblRateW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblRateW.Text = "Rate";
			this.lblRateW.Top = 0.375F;
			this.lblRateW.Width = 0.375F;
			// 
			// lblAccountW
			// 
			this.lblAccountW.Height = 0.1875F;
			this.lblAccountW.HyperLink = null;
			this.lblAccountW.Left = 2.3125F;
			this.lblAccountW.Name = "lblAccountW";
			this.lblAccountW.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccountW.Text = "Account";
			this.lblAccountW.Top = 0.375F;
			this.lblAccountW.Width = 1.1875F;
			// 
			// Field16
			// 
			this.Field16.Height = 0.1875F;
			this.Field16.Left = 3.9375F;
			this.Field16.Name = "Field16";
			this.Field16.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field16.Text = null;
			this.Field16.Top = 0.5625F;
			this.Field16.Width = 0.375F;
			// 
			// Field17
			// 
			this.Field17.Height = 0.1875F;
			this.Field17.Left = 3.9375F;
			this.Field17.Name = "Field17";
			this.Field17.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field17.Text = null;
			this.Field17.Top = 0.75F;
			this.Field17.Width = 0.375F;
			// 
			// Field18
			// 
			this.Field18.Height = 0.1875F;
			this.Field18.Left = 3.9375F;
			this.Field18.Name = "Field18";
			this.Field18.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field18.Text = null;
			this.Field18.Top = 0.9375F;
			this.Field18.Width = 0.375F;
			// 
			// Field19
			// 
			this.Field19.Height = 0.1875F;
			this.Field19.Left = 3.9375F;
			this.Field19.Name = "Field19";
			this.Field19.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field19.Text = null;
			this.Field19.Top = 1.125F;
			this.Field19.Width = 0.375F;
			// 
			// Field20
			// 
			this.Field20.Height = 0.1875F;
			this.Field20.Left = 3.9375F;
			this.Field20.Name = "Field20";
			this.Field20.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field20.Text = null;
			this.Field20.Top = 1.3125F;
			this.Field20.Width = 0.375F;
			// 
			// Field21
			// 
			this.Field21.Height = 0.1875F;
			this.Field21.Left = 4.375F;
			this.Field21.Name = "Field21";
			this.Field21.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field21.Text = null;
			this.Field21.Top = 0.5625F;
			this.Field21.Width = 1.1875F;
			// 
			// Field22
			// 
			this.Field22.Height = 0.1875F;
			this.Field22.Left = 4.375F;
			this.Field22.Name = "Field22";
			this.Field22.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field22.Text = null;
			this.Field22.Top = 0.75F;
			this.Field22.Width = 1.1875F;
			// 
			// Field23
			// 
			this.Field23.Height = 0.1875F;
			this.Field23.Left = 4.375F;
			this.Field23.Name = "Field23";
			this.Field23.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field23.Text = null;
			this.Field23.Top = 0.9375F;
			this.Field23.Width = 1.1875F;
			// 
			// Field24
			// 
			this.Field24.Height = 0.1875F;
			this.Field24.Left = 4.375F;
			this.Field24.Name = "Field24";
			this.Field24.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field24.Text = null;
			this.Field24.Top = 1.125F;
			this.Field24.Width = 1.1875F;
			// 
			// Field25
			// 
			this.Field25.Height = 0.1875F;
			this.Field25.Left = 4.375F;
			this.Field25.Name = "Field25";
			this.Field25.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field25.Text = null;
			this.Field25.Top = 1.3125F;
			this.Field25.Width = 1.1875F;
			// 
			// Field26
			// 
			this.Field26.Height = 0.1875F;
			this.Field26.Left = 5.625F;
			this.Field26.Name = "Field26";
			this.Field26.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field26.Text = null;
			this.Field26.Top = 0.5625F;
			this.Field26.Width = 0.375F;
			// 
			// Field27
			// 
			this.Field27.Height = 0.1875F;
			this.Field27.Left = 5.625F;
			this.Field27.Name = "Field27";
			this.Field27.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field27.Text = null;
			this.Field27.Top = 0.75F;
			this.Field27.Width = 0.375F;
			// 
			// Field28
			// 
			this.Field28.Height = 0.1875F;
			this.Field28.Left = 5.625F;
			this.Field28.Name = "Field28";
			this.Field28.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field28.Text = null;
			this.Field28.Top = 0.9375F;
			this.Field28.Width = 0.375F;
			// 
			// Field29
			// 
			this.Field29.Height = 0.1875F;
			this.Field29.Left = 5.625F;
			this.Field29.Name = "Field29";
			this.Field29.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field29.Text = null;
			this.Field29.Top = 1.125F;
			this.Field29.Width = 0.375F;
			// 
			// Field30
			// 
			this.Field30.Height = 0.1875F;
			this.Field30.Left = 5.625F;
			this.Field30.Name = "Field30";
			this.Field30.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field30.Text = null;
			this.Field30.Top = 1.3125F;
			this.Field30.Width = 0.375F;
			// 
			// Field31
			// 
			this.Field31.Height = 0.1875F;
			this.Field31.Left = 6.0625F;
			this.Field31.Name = "Field31";
			this.Field31.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field31.Text = null;
			this.Field31.Top = 0.5625F;
			this.Field31.Width = 1.1875F;
			// 
			// Field32
			// 
			this.Field32.Height = 0.1875F;
			this.Field32.Left = 6.0625F;
			this.Field32.Name = "Field32";
			this.Field32.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field32.Text = null;
			this.Field32.Top = 0.75F;
			this.Field32.Width = 1.1875F;
			// 
			// Field33
			// 
			this.Field33.Height = 0.1875F;
			this.Field33.Left = 6.0625F;
			this.Field33.Name = "Field33";
			this.Field33.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field33.Text = null;
			this.Field33.Top = 0.9375F;
			this.Field33.Width = 1.1875F;
			// 
			// Field34
			// 
			this.Field34.Height = 0.1875F;
			this.Field34.Left = 6.0625F;
			this.Field34.Name = "Field34";
			this.Field34.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field34.Text = null;
			this.Field34.Top = 1.125F;
			this.Field34.Width = 1.1875F;
			// 
			// Field35
			// 
			this.Field35.Height = 0.1875F;
			this.Field35.Left = 6.0625F;
			this.Field35.Name = "Field35";
			this.Field35.Style = "font-family: \'Tahoma\'; text-align: right";
			this.Field35.Text = null;
			this.Field35.Top = 1.3125F;
			this.Field35.Width = 1.1875F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 3.9375F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label2.Text = "Type";
			this.Label2.Top = 0.375F;
			this.Label2.Width = 0.375F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 4.375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label3.Text = "Amount";
			this.Label3.Top = 0.375F;
			this.Label3.Width = 1.1875F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 5.625F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label4.Text = "Rate";
			this.Label4.Top = 0.375F;
			this.Label4.Width = 0.375F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 6.0625F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.Label5.Text = "Account";
			this.Label5.Top = 0.375F;
			this.Label5.Width = 1.1875F;
			// 
			// lblWater
			// 
			this.lblWater.Height = 0.1875F;
			this.lblWater.HyperLink = null;
			this.lblWater.Left = 0.1875F;
			this.lblWater.Name = "lblWater";
			this.lblWater.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblWater.Text = "Water";
			this.lblWater.Top = 0.1875F;
			this.lblWater.Width = 3.3125F;
			// 
			// lblSewer
			// 
			this.lblSewer.Height = 0.1875F;
			this.lblSewer.HyperLink = null;
			this.lblSewer.Left = 3.9375F;
			this.lblSewer.Name = "lblSewer";
			this.lblSewer.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblSewer.Text = "Sewer";
			this.lblSewer.Top = 0.1875F;
			this.lblSewer.Width = 3.3125F;
			// 
			// fldTotalW
			// 
			this.fldTotalW.Height = 0.1875F;
			this.fldTotalW.Left = 3.9375F;
			this.fldTotalW.Name = "fldTotalW";
			this.fldTotalW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalW.Text = null;
			this.fldTotalW.Top = 0.125F;
			this.fldTotalW.Width = 0.625F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 1.125F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldTotalCount.Text = "Total Count:";
			this.fldTotalCount.Top = 0.125F;
			this.fldTotalCount.Width = 2.5F;
			// 
			// fldTotalS
			// 
			this.fldTotalS.Height = 0.1875F;
			this.fldTotalS.Left = 5.1875F;
			this.fldTotalS.Name = "fldTotalS";
			this.fldTotalS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalS.Text = null;
			this.fldTotalS.Top = 0.125F;
			this.fldTotalS.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.9375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 1.875F;
			this.Line1.X1 = 3.9375F;
			this.Line1.X2 = 5.8125F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// arMeterDataExtract
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewerHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWType5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTypeW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmountW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRateW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field23)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field24)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field25)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field26)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field27)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field28)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field29)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field30)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field31)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field32)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field33)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field34)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field35)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWType1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWType2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWType3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWType4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWType5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field7;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field8;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field9;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field10;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field11;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field12;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field13;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field14;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTypeW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmountW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRateW;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field16;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field17;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field19;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field20;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field21;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field23;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field24;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field25;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field26;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field27;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field28;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field29;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field30;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field31;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field32;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field33;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field34;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field35;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWater;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSewer;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWaterHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSewerHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalS;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
	}
}
