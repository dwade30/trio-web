﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptBillTransferReport.
	/// </summary>
	partial class rptBillTransferReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptBillTransferReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblRegular = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAmount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSeq = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCons = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMisc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTax = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblInterest = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPastDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldAcctNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSeq = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMisc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldFooter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalAmount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCons = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalRegular = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalMisc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTax = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPastDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalInterest = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeq)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCons)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegular)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMisc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPastDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcctNum,
				this.fldName,
				this.fldSeq,
				this.fldAmount,
				this.fldBook,
				this.fldCons,
				this.fldRegular,
				this.fldMisc,
				this.fldTax,
				this.fldPastDue,
				this.fldInterest
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccount,
				this.lblName,
				this.lblRegular,
				this.lblAmount,
				this.lblReportType,
				this.lblBook,
				this.lblSeq,
				this.lblCons,
				this.lblMisc,
				this.lblTax,
				this.lblInterest,
				this.lblPastDue,
				this.Line1
			});
			this.GroupHeader1.Height = 0.5625F;
			this.GroupHeader1.Name = "GroupHeader1";
			this.GroupHeader1.RepeatStyle = GrapeCity.ActiveReports.SectionReportModel.RepeatStyle.OnPage;
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldFooter,
				this.fldTotalAmount,
				this.Line2,
				this.fldTotalCons,
				this.fldTotalRegular,
				this.fldTotalMisc,
				this.fldTotalTax,
				this.fldTotalPastDue,
				this.fldTotalInterest
			});
			this.GroupFooter1.Height = 0.5F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 7.4375F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.375F;
			this.lblAccount.Width = 0.75F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 8.1875F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.375F;
			this.lblName.Width = 1.5625F;
			// 
			// lblRegular
			// 
			this.lblRegular.Height = 0.1875F;
			this.lblRegular.HyperLink = null;
			this.lblRegular.Left = 1.6875F;
			this.lblRegular.Name = "lblRegular";
			this.lblRegular.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblRegular.Text = "Regular";
			this.lblRegular.Top = 0.375F;
			this.lblRegular.Width = 0.9375F;
			// 
			// lblAmount
			// 
			this.lblAmount.Height = 0.1875F;
			this.lblAmount.HyperLink = null;
			this.lblAmount.Left = 6.25F;
			this.lblAmount.Name = "lblAmount";
			this.lblAmount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblAmount.Text = "Amount";
			this.lblAmount.Top = 0.375F;
			this.lblAmount.Width = 1.1875F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.375F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblReportType.Text = null;
			this.lblReportType.Top = 0F;
			this.lblReportType.Width = 10F;
			// 
			// lblBook
			// 
			this.lblBook.Height = 0.1875F;
			this.lblBook.HyperLink = null;
			this.lblBook.Left = 0F;
			this.lblBook.Name = "lblBook";
			this.lblBook.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblBook.Text = "Book";
			this.lblBook.Top = 0.375F;
			this.lblBook.Width = 0.5F;
			// 
			// lblSeq
			// 
			this.lblSeq.Height = 0.1875F;
			this.lblSeq.HyperLink = null;
			this.lblSeq.Left = 0.5F;
			this.lblSeq.Name = "lblSeq";
			this.lblSeq.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblSeq.Text = "Seq";
			this.lblSeq.Top = 0.375F;
			this.lblSeq.Width = 0.4375F;
			// 
			// lblCons
			// 
			this.lblCons.Height = 0.1875F;
			this.lblCons.HyperLink = null;
			this.lblCons.Left = 0.9375F;
			this.lblCons.Name = "lblCons";
			this.lblCons.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblCons.Text = "Cons";
			this.lblCons.Top = 0.375F;
			this.lblCons.Width = 0.75F;
			// 
			// lblMisc
			// 
			this.lblMisc.Height = 0.1875F;
			this.lblMisc.HyperLink = null;
			this.lblMisc.Left = 2.625F;
			this.lblMisc.Name = "lblMisc";
			this.lblMisc.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblMisc.Text = "Misc";
			this.lblMisc.Top = 0.375F;
			this.lblMisc.Width = 0.9375F;
			// 
			// lblTax
			// 
			this.lblTax.Height = 0.1875F;
			this.lblTax.HyperLink = null;
			this.lblTax.Left = 3.5625F;
			this.lblTax.Name = "lblTax";
			this.lblTax.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblTax.Text = "Tax";
			this.lblTax.Top = 0.375F;
			this.lblTax.Width = 0.8125F;
			// 
			// lblInterest
			// 
			this.lblInterest.Height = 0.1875F;
			this.lblInterest.HyperLink = null;
			this.lblInterest.Left = 5.375F;
			this.lblInterest.Name = "lblInterest";
			this.lblInterest.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblInterest.Text = "Interest";
			this.lblInterest.Top = 0.375F;
			this.lblInterest.Width = 0.875F;
			// 
			// lblPastDue
			// 
			this.lblPastDue.Height = 0.1875F;
			this.lblPastDue.HyperLink = null;
			this.lblPastDue.Left = 4.375F;
			this.lblPastDue.Name = "lblPastDue";
			this.lblPastDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 0";
			this.lblPastDue.Text = "Past Due";
			this.lblPastDue.Top = 0.375F;
			this.lblPastDue.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.5625F;
			this.Line1.Width = 10F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 10F;
			this.Line1.Y1 = 0.5625F;
			this.Line1.Y2 = 0.5625F;
			// 
			// fldAcctNum
			// 
			this.fldAcctNum.Height = 0.1875F;
			this.fldAcctNum.Left = 7.4375F;
			this.fldAcctNum.Name = "fldAcctNum";
			this.fldAcctNum.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAcctNum.Text = null;
			this.fldAcctNum.Top = 0F;
			this.fldAcctNum.Width = 0.75F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 8.1875F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 1.8125F;
			// 
			// fldSeq
			// 
			this.fldSeq.Height = 0.1875F;
			this.fldSeq.Left = 0.5F;
			this.fldSeq.Name = "fldSeq";
			this.fldSeq.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldSeq.Text = null;
			this.fldSeq.Top = 0F;
			this.fldSeq.Width = 0.4375F;
			// 
			// fldAmount
			// 
			this.fldAmount.Height = 0.1875F;
			this.fldAmount.Left = 6.25F;
			this.fldAmount.Name = "fldAmount";
			this.fldAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldAmount.Text = null;
			this.fldAmount.Top = 0F;
			this.fldAmount.Width = 1.1875F;
			// 
			// fldBook
			// 
			this.fldBook.Height = 0.1875F;
			this.fldBook.Left = 0F;
			this.fldBook.Name = "fldBook";
			this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldBook.Text = null;
			this.fldBook.Top = 0F;
			this.fldBook.Width = 0.5F;
			// 
			// fldCons
			// 
			this.fldCons.Height = 0.1875F;
			this.fldCons.Left = 0.9375F;
			this.fldCons.Name = "fldCons";
			this.fldCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldCons.Text = null;
			this.fldCons.Top = 0F;
			this.fldCons.Width = 0.75F;
			// 
			// fldRegular
			// 
			this.fldRegular.Height = 0.1875F;
			this.fldRegular.Left = 1.6875F;
			this.fldRegular.Name = "fldRegular";
			this.fldRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldRegular.Text = null;
			this.fldRegular.Top = 0F;
			this.fldRegular.Width = 0.9375F;
			// 
			// fldMisc
			// 
			this.fldMisc.Height = 0.1875F;
			this.fldMisc.Left = 2.625F;
			this.fldMisc.Name = "fldMisc";
			this.fldMisc.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldMisc.Text = null;
			this.fldMisc.Top = 0F;
			this.fldMisc.Width = 0.9375F;
			// 
			// fldTax
			// 
			this.fldTax.Height = 0.1875F;
			this.fldTax.Left = 3.5625F;
			this.fldTax.Name = "fldTax";
			this.fldTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTax.Text = null;
			this.fldTax.Top = 0F;
			this.fldTax.Width = 0.8125F;
			// 
			// fldPastDue
			// 
			this.fldPastDue.Height = 0.1875F;
			this.fldPastDue.Left = 4.375F;
			this.fldPastDue.Name = "fldPastDue";
			this.fldPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldPastDue.Text = null;
			this.fldPastDue.Top = 0F;
			this.fldPastDue.Width = 1F;
			// 
			// fldInterest
			// 
			this.fldInterest.Height = 0.1875F;
			this.fldInterest.Left = 5.375F;
			this.fldInterest.Name = "fldInterest";
			this.fldInterest.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldInterest.Text = null;
			this.fldInterest.Top = 0F;
			this.fldInterest.Width = 0.875F;
			// 
			// fldFooter
			// 
			this.fldFooter.Height = 0.375F;
			this.fldFooter.Left = 0F;
			this.fldFooter.Name = "fldFooter";
			this.fldFooter.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.fldFooter.Text = null;
			this.fldFooter.Top = 0.125F;
			this.fldFooter.Width = 0.9375F;
			// 
			// fldTotalAmount
			// 
			this.fldTotalAmount.Height = 0.1875F;
			this.fldTotalAmount.Left = 6.125F;
			this.fldTotalAmount.Name = "fldTotalAmount";
			this.fldTotalAmount.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalAmount.Text = null;
			this.fldTotalAmount.Top = 0.25F;
			this.fldTotalAmount.Width = 1.3125F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.9375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.125F;
			this.Line2.Width = 6.5F;
			this.Line2.X1 = 0.9375F;
			this.Line2.X2 = 7.4375F;
			this.Line2.Y1 = 0.125F;
			this.Line2.Y2 = 0.125F;
			// 
			// fldTotalCons
			// 
			this.fldTotalCons.Height = 0.1875F;
			this.fldTotalCons.Left = 0.9375F;
			this.fldTotalCons.Name = "fldTotalCons";
			this.fldTotalCons.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalCons.Text = null;
			this.fldTotalCons.Top = 0.25F;
			this.fldTotalCons.Width = 0.75F;
			// 
			// fldTotalRegular
			// 
			this.fldTotalRegular.Height = 0.1875F;
			this.fldTotalRegular.Left = 1.6875F;
			this.fldTotalRegular.Name = "fldTotalRegular";
			this.fldTotalRegular.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalRegular.Text = null;
			this.fldTotalRegular.Top = 0.25F;
			this.fldTotalRegular.Width = 0.9375F;
			// 
			// fldTotalMisc
			// 
			this.fldTotalMisc.Height = 0.1875F;
			this.fldTotalMisc.Left = 2.625F;
			this.fldTotalMisc.Name = "fldTotalMisc";
			this.fldTotalMisc.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalMisc.Text = null;
			this.fldTotalMisc.Top = 0.25F;
			this.fldTotalMisc.Width = 0.9375F;
			// 
			// fldTotalTax
			// 
			this.fldTotalTax.Height = 0.1875F;
			this.fldTotalTax.Left = 3.5625F;
			this.fldTotalTax.Name = "fldTotalTax";
			this.fldTotalTax.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalTax.Text = null;
			this.fldTotalTax.Top = 0.25F;
			this.fldTotalTax.Width = 0.8125F;
			// 
			// fldTotalPastDue
			// 
			this.fldTotalPastDue.Height = 0.1875F;
			this.fldTotalPastDue.Left = 4.375F;
			this.fldTotalPastDue.Name = "fldTotalPastDue";
			this.fldTotalPastDue.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalPastDue.Text = null;
			this.fldTotalPastDue.Top = 0.25F;
			this.fldTotalPastDue.Width = 1F;
			// 
			// fldTotalInterest
			// 
			this.fldTotalInterest.Height = 0.1875F;
			this.fldTotalInterest.Left = 5.375F;
			this.fldTotalInterest.Name = "fldTotalInterest";
			this.fldTotalInterest.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalInterest.Text = null;
			this.fldTotalInterest.Top = 0.25F;
			this.fldTotalInterest.Width = 0.875F;
			// 
			// rptBillTransferReport
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10.01042F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcctNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeq)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCons)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalRegular)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalMisc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTax)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPastDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalInterest)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcctNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeq;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAmount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMisc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldInterest;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblRegular;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSeq;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCons;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMisc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTax;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblInterest;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalAmount;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCons;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalRegular;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalMisc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTax;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPastDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalInterest;
	}
}
