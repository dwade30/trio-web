﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmTaxAcquiredRemoval.
	/// </summary>
	partial class frmTaxAcquiredRemoval : BaseForm
	{
		public fecherFoundation.FCPanel fraGrid;
		public fecherFoundation.FCCheckBox chkUpdateRE;
		public fecherFoundation.FCGrid vsDemand;
		public fecherFoundation.FCLabel lblInstructionHeader;
		public fecherFoundation.FCLabel lblInstruction;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelectAll;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTaxAcquiredRemoval));
			this.fraGrid = new fecherFoundation.FCPanel();
			this.chkUpdateRE = new fecherFoundation.FCCheckBox();
			this.vsDemand = new fecherFoundation.FCGrid();
			this.lblInstructionHeader = new fecherFoundation.FCLabel();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSelectAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdFileClear = new fecherFoundation.FCButton();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).BeginInit();
			this.fraGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkUpdateRE)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 577);
			this.BottomPanel.Size = new System.Drawing.Size(673, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraGrid);
			this.ClientArea.Size = new System.Drawing.Size(673, 517);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Controls.Add(this.cmdFileClear);
			this.TopPanel.Size = new System.Drawing.Size(673, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(349, 30);
			// 
			// fraGrid
			// 
			this.fraGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraGrid.AppearanceKey = "groupBoxNoBorders";
			this.fraGrid.Controls.Add(this.chkUpdateRE);
			this.fraGrid.Controls.Add(this.vsDemand);
			this.fraGrid.Controls.Add(this.lblInstructionHeader);
			this.fraGrid.Controls.Add(this.lblInstruction);
			this.fraGrid.Location = new System.Drawing.Point(30, 30);
			this.fraGrid.Name = "fraGrid";
			this.fraGrid.Size = new System.Drawing.Size(613, 482);
			this.fraGrid.TabIndex = 0;
			this.fraGrid.Visible = false;
			// 
			// chkUpdateRE
			// 
			this.chkUpdateRE.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
			this.chkUpdateRE.Location = new System.Drawing.Point(372, 48);
			this.chkUpdateRE.Name = "chkUpdateRE";
			this.chkUpdateRE.Size = new System.Drawing.Size(223, 27);
			this.chkUpdateRE.TabIndex = 2;
			this.chkUpdateRE.Text = "Update RE Tax Acq Status";
			this.chkUpdateRE.CheckedChanged += new System.EventHandler(this.chkUpdateRE_CheckedChanged);
			// 
			// vsDemand
			// 
			this.vsDemand.AllowSelection = false;
			this.vsDemand.AllowUserToResizeColumns = false;
			this.vsDemand.AllowUserToResizeRows = false;
			this.vsDemand.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsDemand.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDemand.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDemand.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDemand.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.BackColorSel = System.Drawing.Color.Empty;
			this.vsDemand.CellBorderStyle = Wisej.Web.DataGridViewCellBorderStyle.Horizontal;
			this.vsDemand.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDemand.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsDemand.ColumnHeadersHeight = 30;
			this.vsDemand.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDemand.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsDemand.DragIcon = null;
			this.vsDemand.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDemand.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsDemand.ExtendLastCol = true;
			this.vsDemand.FixedCols = 0;
			this.vsDemand.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.FrozenCols = 0;
			this.vsDemand.GridColor = System.Drawing.Color.Empty;
			this.vsDemand.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.Location = new System.Drawing.Point(0, 146);
			this.vsDemand.Name = "vsDemand";
			this.vsDemand.ReadOnly = true;
			this.vsDemand.RowHeadersVisible = false;
			this.vsDemand.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDemand.RowHeightMin = 0;
			this.vsDemand.Rows = 1;
			this.vsDemand.ScrollTipText = null;
			this.vsDemand.ShowColumnVisibilityMenu = false;
			this.vsDemand.ShowFocusCell = false;
			this.vsDemand.Size = new System.Drawing.Size(613, 334);
			this.vsDemand.StandardTab = true;
			this.vsDemand.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDemand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDemand.TabIndex = 3;
			this.vsDemand.CurrentCellChanged += new System.EventHandler(this.vsDemand_RowColChange);
			this.vsDemand.DoubleClick += new System.EventHandler(this.vsDemand_DblClick);
			// 
			// lblInstructionHeader
			// 
			this.lblInstructionHeader.Location = new System.Drawing.Point(0, 0);
			this.lblInstructionHeader.Name = "lblInstructionHeader";
			this.lblInstructionHeader.Size = new System.Drawing.Size(596, 18);
			this.lblInstructionHeader.TabIndex = 0;
			// 
			// lblInstruction
			// 
			this.lblInstruction.Location = new System.Drawing.Point(0, 48);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(332, 68);
			this.lblInstruction.TabIndex = 1;
			this.lblInstruction.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileClear,
				this.mnuFileSelectAll,
				this.mnuFileSeperator2,
				this.mnuFilePrint,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileClear
			// 
			this.mnuFileClear.Index = 0;
			this.mnuFileClear.Name = "mnuFileClear";
			this.mnuFileClear.Text = "Clear Selection";
			this.mnuFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// mnuFileSelectAll
			// 
			this.mnuFileSelectAll.Index = 1;
			this.mnuFileSelectAll.Name = "mnuFileSelectAll";
			this.mnuFileSelectAll.Text = "Select All";
			this.mnuFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 2;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 3;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePrint.Text = "Save and Exit";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 5;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(279, 30);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// cmdFileClear
			// 
			this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClear.AppearanceKey = "toolbarButton";
			this.cmdFileClear.Location = new System.Drawing.Point(537, 29);
			this.cmdFileClear.Name = "cmdFileClear";
			this.cmdFileClear.Size = new System.Drawing.Size(108, 24);
			this.cmdFileClear.TabIndex = 1;
			this.cmdFileClear.Text = "Clear Selection";
			this.cmdFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(457, 29);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(74, 24);
			this.cmdFileSelectAll.TabIndex = 2;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// frmTaxAcquiredRemoval
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(673, 685);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmTaxAcquiredRemoval";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Manual Tax Acquired Removal";
			this.Load += new System.EventHandler(this.frmTaxAcquiredRemoval_Load);
			this.Activated += new System.EventHandler(this.frmTaxAcquiredRemoval_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmTaxAcquiredRemoval_KeyPress);
			this.Resize += new System.EventHandler(this.frmTaxAcquiredRemoval_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).EndInit();
			this.fraGrid.ResumeLayout(false);
			this.fraGrid.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkUpdateRE)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdFileClear;
		private FCButton cmdFileSelectAll;
	}
}
