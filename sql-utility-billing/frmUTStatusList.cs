﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using System.Drawing;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
    /// <summary>
    /// Summary description for frmUTStatusList.
    /// </summary>
    public partial class frmUTStatusList : BaseForm
    {
        public frmUTStatusList()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            chkInclude = new System.Collections.Generic.List<fecherFoundation.FCCheckBox>();
            chkInclude.AddControlArrayElement(chkInclude_0, 0);
            chkInclude.AddControlArrayElement(chkInclude_1, 1);
            chkInclude.AddControlArrayElement(chkInclude_2, 2);
            chkInclude.AddControlArrayElement(chkInclude_3, 3);
            chkInclude.AddControlArrayElement(chkInclude_4, 4);
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmUTStatusList InstancePtr
        {
            get
            {
                return (frmUTStatusList)Sys.GetInstance(typeof(frmUTStatusList));
            }
        }

        protected frmUTStatusList _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               12/02/2004              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               01/23/2007              *
        // ********************************************************
        int intCounter;
        int intStart;
        int intEnd;
        int intID;
        string strTemp = "";
        bool boolSaveReport;
        string strYearChoice = "";
        bool boolLoaded;
        string strGridToolTipText = "";
        // these will be to pass the string on to the reports when needed
        public string strRSWhere = string.Empty;
        public string strRSFrom = "";
        public string strRSOrder = string.Empty;
        public string strBinaryWhere = "";
        public int lngMax;
        public bool boolFullStatusAmounts;
        public bool boolShowCurrentInterest;
        public bool boolShowLocation;
        public bool boolShowMapLot;
        public int intShowOwnerType;
        public int intMasterReportType;
        public string strTAStatus = "";
        public int lngRowHeader;
        public int lngRowReportType;
        public int lngRowWS;
        public int lngRowAccount;
        public int lngRowName;
        public int lngRowBill;
        public int lngRowBook;
        public int lngRowShowLien;
        public int lngRowBalanceDue;
        public int lngRowGroupBy;
        public int lngRowPaymentType;
        public int lngRowAsOfDate;
        public int lngRowShowPaymentFrom;
        public int lngRowTownCode;
        public int lngRowTaxAcquired;

        private bool _isBangor = Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR";

        private void chkHardCode_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
            {
                // this turns the hard coded report on
                fraHardCode.Enabled = true;
                cmbHardCode.Enabled = true;
                fraSave.Enabled = false;
                cmbNameOption.Enabled = true;
                if (cmbHardCode.Items.Count > 0)
                {
                    // this should select the first item in the combo box
                    cmbHardCode.SelectedIndex = 0;
                }
                fraSort.Enabled = false;
                lstSort.Enabled = false;
                fraFields.Enabled = false;
                lstFields.Enabled = false;
            }
            else
            {
                // this turns the hard coded reports off
                fraHardCode.Enabled = false;
                cmbHardCode.Enabled = false;
                cmbHardCode.SelectedIndex = -1;
                // fraSave.Enabled = True
                fraSort.Enabled = true;
                lstSort.Enabled = true;
                fraFields.Enabled = true;
                lstFields.Enabled = true;
                cmbNameOption.Enabled = true;
            }
            ShowAutomaticFields();
        }

        private void chkHardCode_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Space:
                    {
                        KeyCode = (Keys)0;
                        if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
                        {
                            chkHardCode.CheckState = Wisej.Web.CheckState.Unchecked;
                        }
                        else
                        {
                            chkHardCode.CheckState = Wisej.Web.CheckState.Checked;
                        }
                        break;
                    }
            }
            //end switch
        }

        private void chkUseFullStatus_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkUseFullStatus.CheckState == Wisej.Web.CheckState.Unchecked)
            {
                // And Left(vsWhere.TextMatrix(lngRowReportType, 1), 4) = "Aged" Then
                chkInclude[2].Enabled = true;
                // chkPrincipalOnly.Enabled = True
            }
            else
            {
                chkInclude[2].CheckState = Wisej.Web.CheckState.Checked;
                chkInclude[2].Enabled = false;
            }
        }

        private void cmbHardCode_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            ShowAutomaticFields();
        }

        private void cmbNameOption_DropDown(object sender, System.EventArgs e)
        {
            modAPIsConst.SendMessageByNum(cmbNameOption.Handle.ToInt32(), modAPIsConst.CB_SETDROPPEDWIDTH, 400, 0);
        }

        private void cmbNameOption_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Space:
                    {
                        if (modAPIsConst.SendMessageByNum(cmbNameOption.Handle.ToInt32(), modAPIsConst.CB_GETDROPPEDSTATE, 2, 0) == 0)
                        {
                            modAPIsConst.SendMessageByNum(cmbNameOption.Handle.ToInt32(), modAPIsConst.CB_SHOWDROPDOWN, 2, 0);
                            KeyCode = (Keys)0;
                        }
                        break;
                    }
            }
            //end switch
        }

        private void cmdClear_Click(object sender, System.EventArgs e)
        {
            // CLEAR THE WHERE GRID SO THE USER CAN START FROM SCRATCH
            int intCounter;
            for (intCounter = 0; intCounter <= vsWhere.Rows - 1; intCounter++)
            {
                vsWhere.TextMatrix(intCounter, 1, string.Empty);
                vsWhere.TextMatrix(intCounter, 2, string.Empty);
                vsWhere.TextMatrix(intCounter, 3, string.Empty);
            }
            vsWhere.EditText = "";
        }

        public void cmdClear_Click()
        {
            cmdClear_Click(cmdClear, new System.EventArgs());
        }

        private void cmdExit_Click(object sender, System.EventArgs e)
        {
            Unload();
        }

        public void cmdExit_Click()
        {
            //cmdExit_Click(cmdExit, new System.EventArgs());
        }

        private void cmdPrint_Click(object sender, System.EventArgs e)
        {
            // THIS ROUTINE WORKS TO PRINT OUT THE REPORT
            try
            {
                // On Error GoTo ERROR_HANDLER
                int intCT;
                // set the cursor to an hourglass
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
                // check to see if the full amounts should be shown
                boolFullStatusAmounts = FCConvert.CBool(chkUseFullStatus.CheckState == Wisej.Web.CheckState.Checked);
                chkCurrentInterest.CheckState = chkUseFullStatus.CheckState;
                // make them the same
                // boolShowCurrentOwner = CBool(optShowName(1).Value)
                intShowOwnerType = cmbNameOption.ItemData(cmbNameOption.SelectedIndex);
                if (boolFullStatusAmounts)
                {
                    boolShowCurrentInterest = FCConvert.CBool(chkCurrentInterest.CheckState == Wisej.Web.CheckState.Checked);
                }
                else
                {
                    boolShowCurrentInterest = false;
                }
                BuildSQL();
                // Dim rsSQL As New clsDRWrapper
                // set focus to another object in order to validate the where grid info
                vsWhere.Select(0, 0);
                if (!ValidateWhereGrid())
                {
                    return;
                }
                SetExtraFields();
                // SHOW THE REPORT
                if (chkHardCode.CheckState == Wisej.Web.CheckState.Checked)
                {
                    if (cmbHardCode.SelectedIndex > -1)
                    {
                        ShowHardCodedReport_2(FCConvert.ToInt16(cmbHardCode.ItemData(cmbHardCode.SelectedIndex)));
                    }
                    else
                    {
                        MessageBox.Show("Please select a report to show.", "No Selection", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbHardCode.Focus();
                    }
                }
                else
                {
                    if (Strings.UCase(vsWhere.TextMatrix(lngRowReportType, 1)) == "REGULAR")
                    {
                        // Regular Status List
                        frmReportViewer.InstancePtr.Init(arUTStatusLists.InstancePtr, allowCancel: true);
                        // , , , , , , , , , , , , , True
                    }
                    else
                    {
                        // Aged Account Listing
                        frmReportViewer.InstancePtr.Init(arUTAgedLists.InstancePtr, allowCancel: true);
                    }
                }
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                return;
            }
            catch (Exception ex)
            {
                
                FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
                MessageBox.Show("ERROR #:" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Print Status Lists ERROR", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        public void cmdPrint_Click()
        {
            cmdPrint_Click(cmdPrint, new System.EventArgs());
        }
        // vbPorter upgrade warning: 'Return' As bool	OnWrite(string)
        public bool BuildSQL()
        {
            bool BuildSQL = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // BUILD THE SQL STATEMENT FROM THE CRITERIA THAT THE USER HAS CHOSEN
                int intCounter;
                string strPaymentDateRage = "";
                string strSort;
                string strWhere;
                bool boolNoLien;
                bool boolAgedList;
                vsWhere.Select(0, 0);
                strSort = " ";
                strWhere = " ";
                boolAgedList = FCConvert.CBool(Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRowReportType, 1), 3)) == "AGE");
                if (modMain.Statics.gboolUTSLDateRange)
                {
                    strPaymentDateRage = " AND (RecordedTransactionDate >= '" + FCConvert.ToString(modMain.Statics.gdtUTSLPaymentDate1) + "' AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTSLPaymentDate2) + "')";
                }
                else
                {
                    strPaymentDateRage = "";
                }
                // Build the SQL string from the grid
                // this will find out which tables to get the data from
                if (Strings.UCase(vsWhere.TextMatrix(lngRowReportType, 1)) == "REGULAR")
                {
                    // Regular Report
                    if ((Strings.UCase(vsWhere.TextMatrix(lngRowWS, 1)) == "WATER") || (Strings.UCase(vsWhere.TextMatrix(lngRowWS, 1)) == "STORMWATER"))
                    {
                        strRSFrom = "SELECT Distinct Service AS SRV, ID AS BK FROM Bill WHERE Service <> 'S'";
                    }
                    else if (Strings.UCase(vsWhere.TextMatrix(lngRowWS, 1)) == "SEWER")
                    {
                        strRSFrom = "SELECT Distinct Service AS SRV, ID AS BK FROM Bill WHERE Service <> 'W'";
                    }
                    else if (Strings.UCase(vsWhere.TextMatrix(lngRowWS, 1)) == "BOTH")
                    {
                        strRSFrom = "SELECT Distinct Service AS SRV, ID AS BK FROM Bill";
                    }
                }
                else
                {
                    // Aged Report
                }
                // GET THE FIELDS TO SORT BY
                for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
                {
                    if (lstSort.Selected(intCounter))
                    {
                        // it is checked
                        if (strSort != " ")
                            strSort += ", ";
                        // lstSort.ItemData(intCounter)
                        var prefix = Strings.Left(lstSort.Items[intCounter].Text, 2);

                        switch (prefix)
                        {
                            // 0
                            case "Ac" when boolAgedList:
                                strSort += "AccountNumber";

                                break;
                            case "Ac":
                                strSort += "ActualAccountNumber";

                                break;

                            // 1
                            case "Na" when intShowOwnerType == 0 || intShowOwnerType == 2 || boolAgedList:
                                strSort += "Name";

                                break;
                            case "Na":
                                strSort += "BName";

                                break;

                            // 2
                            case "Bi" when !boolAgedList:
                                strSort += "BillingRateKey";

                                break;
                            case "Bi":
                            {
                                if (strSort != " ")
                                    strSort = Strings.Left(strSort, strSort.Length - 2);

                                break;
                            }

                            // 3
                            case "Bo" when !boolAgedList:
                                strSort += "UTBook";

                                break;
                            case "Bo":
                            {
                                if (strSort != " ")
                                    strSort = Strings.Left(strSort, strSort.Length - 2);

                                break;
                            }

                            // 4           'kgk trout-743  Add Map/Lot to sort
                            case "Ma" when !boolAgedList:
                                strSort += "Bill.MapLot";
                                // "Master.MapLot"    'kgk looks like these are backwards
                                break;
                            case "Ma":
                                strSort += "Master.MapLot";
                                // "Bill.MapLot"
                                break;
                        }
                    }
                }
                if (Strings.Trim(strSort) == "")
                {
                    if (!boolAgedList)
                    {
                        if (intShowOwnerType == 0 || intShowOwnerType == 3)
                        {
                            strSort = "Name, ActualAccountNumber, BillingRateKey";
                            // default sort order
                        }
                        else
                        {
                            strSort = "BName, ActualAccountNumber, BillingRateKey";
                            // default sort order
                        }
                    }
                    else
                    {
                        strSort = "";
                    }
                }
                // create the where string here
                if (!boolAgedList)
                {
                    if (Strings.Trim(vsWhere.TextMatrix(lngRowAccount, 1)) != "")
                    {
                        // Account Number
                        if (Strings.Trim(vsWhere.TextMatrix(lngRowAccount, 2)) != "")
                        {
                            strWhere += "ActualAccountNumber <= " + vsWhere.TextMatrix(lngRowAccount, 2) + " AND ActualAccountNumber  >= " + vsWhere.TextMatrix(lngRowAccount, 1);
                        }
                        else
                        {
                            strWhere += "ActualAccountNumber  >= " + vsWhere.TextMatrix(lngRowAccount, 1);
                        }
                    }
                    else
                    {
                        if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
                        {
                            strWhere += "ActualAccountNumber  <= " + vsWhere.TextMatrix(lngRowAccount, 2);
                        }
                    }
                    if (Strings.Trim(vsWhere.TextMatrix(lngRowName, 1)) != "")
                    {
                        // Name
                        if (Strings.Trim(vsWhere.TextMatrix(lngRowName, 1)) == "")
                        {
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "pBill.FullNameLF >= '" + vsWhere.TextMatrix(lngRowName, 1) + "    ' AND pBill.FullNameLF < '" + vsWhere.TextMatrix(lngRowName, 1) + "zzzz'";
                        }
                        else
                        {
                            // this is when they both have values in the fields and will be a range by name
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "pBill.FullNameLF >= '" + vsWhere.TextMatrix(lngRowName, 1) + "    ' AND pBill.FullNameLF < '" + vsWhere.TextMatrix(lngRowName, 2) + "zzzz'";
                        }
                    }
                    if (Strings.Trim(vsWhere.TextMatrix(lngRowBook, 1)) != "")
                    {
                        // Book
                        if (Strings.Trim(vsWhere.TextMatrix(lngRowBook, 2)) != "")
                        {
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "Bill.Book >= " + vsWhere.TextMatrix(lngRowBook, 1) + " AND Bill.Book <= " + vsWhere.TextMatrix(lngRowBook, 2);
                        }
                        else
                        {
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "Bill.Book = " + vsWhere.TextMatrix(lngRowBook, 1);
                        }
                    }
                    if (Strings.Trim(vsWhere.TextMatrix(lngRowBill, 1)) != "")
                    {
                        // Bill Number
                        if (Strings.Trim(vsWhere.TextMatrix(lngRowBill, 2)) != "")
                        {
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "BillingRateKey >= " + vsWhere.TextMatrix(lngRowBill, 1) + " AND BillingRateKey <= " + vsWhere.TextMatrix(lngRowBill, 2);
                        }
                        else
                        {
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "BillingRateKey = " + vsWhere.TextMatrix(lngRowBill, 1);
                        }
                    }
                    if (Strings.Trim(vsWhere.TextMatrix(lngRowTaxAcquired, 1)) != "")
                    {
                        // Tax Acquired
                        if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRowTaxAcquired, 1), 2)) == "NO")
                        {
                            // If strWhere <> " " Then strWhere = strWhere & " AND "
                            // strWhere = strWhere & "NOT TaxAcquired "
                            strTAStatus = "N";
                        }
                        else if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRowTaxAcquired, 1), 2)) == "TA")
                        {
                            // If strWhere <> " " Then strWhere = strWhere & " AND "
                            // strWhere = strWhere & "TaxAcquired "
                            strTAStatus = "Y";
                        }
                    }
                }
                else
                {
                    if (Strings.Trim(vsWhere.TextMatrix(lngRowAccount, 1)) != "")
                    {
                        // Account Number
                        if (Strings.Trim(vsWhere.TextMatrix(lngRowAccount, 2)) != "")
                        {
                            strWhere += "AccountNumber <= " + vsWhere.TextMatrix(lngRowAccount, 2) + " AND AccountNumber  >= " + vsWhere.TextMatrix(lngRowAccount, 1);
                        }
                        else
                        {
                            strWhere += "AccountNumber >= " + vsWhere.TextMatrix(lngRowAccount, 1);
                        }
                    }
                    else
                    {
                        if (Strings.Trim(vsWhere.TextMatrix(0, 2)) != "")
                        {
                            strWhere += "AccountNumber  <= " + vsWhere.TextMatrix(lngRowAccount, 2);
                        }
                    }
                    if (Strings.Trim(vsWhere.TextMatrix(lngRowName, 1)) != "")
                    {
                        // Name
                        if (Strings.Trim(vsWhere.TextMatrix(lngRowName, 1)) == "")
                        {
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "Name >= '" + vsWhere.TextMatrix(lngRowName, 1) + "    ' AND Name < '" + vsWhere.TextMatrix(lngRowName, 1) + "zzzz'";
                        }
                        else
                        {
                            // this is when they both have values in the fields and will be a range by name
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "Name >= '" + vsWhere.TextMatrix(lngRowName, 1) + "    ' AND Name < '" + vsWhere.TextMatrix(lngRowName, 2) + "zzzz'";
                        }
                    }
                    if (!boolAgedList)
                    {
                        if (Strings.Trim(vsWhere.TextMatrix(lngRowBook, 1)) != "")
                        {
                            // Book
                            if (Strings.Trim(vsWhere.TextMatrix(lngRowBook, 2)) != "")
                            {
                                if (strWhere != " ")
                                    strWhere += " AND ";
                                strWhere += "BookNumber >= " + vsWhere.TextMatrix(lngRowBook, 1) + " AND BookNumber <= " + vsWhere.TextMatrix(lngRowBook, 2);
                            }
                            else
                            {
                                if (strWhere != " ")
                                    strWhere += " AND ";
                                strWhere += "BookNumber = " + vsWhere.TextMatrix(lngRowBook, 1);
                            }
                        }
                    }
                    else
                    {
                        if (Strings.Trim(vsWhere.TextMatrix(lngRowBook, 1)) != "")
                        {
                            // Book
                            if (Strings.Trim(vsWhere.TextMatrix(lngRowBook, 2)) != "")
                            {
                                if (strWhere != " ")
                                    strWhere += " AND ";
                                strWhere += "UTBook >= " + vsWhere.TextMatrix(lngRowBook, 1) + " AND UTBook <= " + vsWhere.TextMatrix(lngRowBook, 2);
                            }
                            else
                            {
                                if (strWhere != " ")
                                    strWhere += " AND ";
                                strWhere += "UTBook = " + vsWhere.TextMatrix(lngRowBook, 1);
                            }
                        }
                    }
                    if (Strings.Trim(vsWhere.TextMatrix(lngRowTaxAcquired, 1)) != "")
                    {
                        // Bill Number
                        if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRowTaxAcquired, 1), 2)) == "NO")
                        {
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "ISNULL(TaxAcquired,0) = 0 ";
                        }
                        else if (Strings.UCase(Strings.Left(vsWhere.TextMatrix(lngRowTaxAcquired, 1), 2)) == "TA")
                        {
                            if (strWhere != " ")
                                strWhere += " AND ";
                            strWhere += "ISNULL(TaxAcquired,0) = 1 ";
                        }
                    }
                }
                if (Strings.Trim(vsWhere.TextMatrix(lngRowAsOfDate, 1)) != "")
                {
                    if (Information.IsDate(vsWhere.TextMatrix(lngRowAsOfDate, 1)))
                    {
                        modMain.Statics.gdtUTStatusListAsOfDate = DateAndTime.DateValue(vsWhere.TextMatrix(lngRowAsOfDate, 1));
                        modMain.Statics.gboolUTUseAsOfDate = true;
                        // If strWhere <> " " Then strWhere = strWhere & " AND "
                        // strWhere = strWhere & " CreationDate <= #" & gdtUTStatusListAsOfDate & "#"
                    }
                    else
                    {
                        modMain.Statics.gdtUTStatusListAsOfDate = DateTime.Today;
                        // set the default as of date
                        modMain.Statics.gboolUTUseAsOfDate = false;
                    }
                }
                else
                {
                    modMain.Statics.gdtUTStatusListAsOfDate = DateTime.Today;
                    // set the default as of date
                    modMain.Statics.gboolUTUseAsOfDate = false;
                }
                if (Strings.Trim(strWhere) == "")
                {
                    // make something up
                }
                strRSWhere = strWhere;
                strRSOrder = strSort;
                return BuildSQL;
            }
            catch (Exception ex)
            {
                
                BuildSQL = FCConvert.CBool("");
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building SQL", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return BuildSQL;
        }

        private void frmUTStatusList_Activated(object sender, System.EventArgs e)
        {
            if (boolLoaded)
            {
            }
            else
            {
                lngMax = 7;
                EnableFrames_2(true);
                //Application.DoEvents();
                Format_WhereGrid_2(true);
                ShowAutomaticFields();
                modUTStatusList.SetupStatusListCombos_2(true);
                Fill_Lists();
                boolLoaded = true;
            }
        }

        private void frmUTStatusList_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
        {
            Keys KeyCode = e.KeyCode;
            int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        Unload();
                        break;
                    }
            }
            //end switch
        }

        private void frmUTStatusList_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmUTStatusList properties;
            //frmUTStatusList.ScaleWidth	= 9045;
            //frmUTStatusList.ScaleHeight	= 7335;
            //frmUTStatusList.LinkTopic	= "Form1";
            //frmUTStatusList.LockControls	= true;
            //End Unmaped Properties
            // LOAD THE FORM AND FILL THE CONTROLS WITH DATA IF THERE IS ANY
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            Text = "Status Lists";
            lngRowReportType = 0;
            lngRowWS = 1;
            lngRowAccount = 2;
            lngRowName = 3;
            lngRowBill = 4;
            lngRowBook = 5;
            lngRowBalanceDue = 6;
            lngRowShowLien = 7;
            lngRowGroupBy = 8;
            lngRowPaymentType = 9;
            lngRowAsOfDate = 10;
            lngRowShowPaymentFrom = 11;
            // lngRowTownCode = 12
            lngRowTaxAcquired = 12;
            CheckReportTable();
            // fill the hard coded combo list with the completed reports
            FillHardCodeCombo();
            modGlobalFunctions.SetTRIOColors(this);
            Set_Note_Text();
            modMain.Statics.gdtUTStatusListAsOfDate = DateTime.Today;
            // set the default as of date
        }

        private void frmUTStatusList_Resize(object sender, System.EventArgs e)
        {
            Format_WhereGrid_2(false);
        }

        private void Form_Unload(object sender, FCFormClosingEventArgs e)
        {
            boolLoaded = false;
        }

        private void fraFields_DoubleClick(object sender, System.EventArgs e)
        {
            int intCounter;
            for (intCounter = 0; intCounter <= lstFields.Items.Count - 1; intCounter++)
            {
                lstFields.SetSelected(intCounter, true);
            }
        }

        private void fraSort_DoubleClick(object sender, System.EventArgs e)
        {
            int intCounter;
            for (intCounter = 0; intCounter <= lstSort.Items.Count - 1; intCounter++)
            {
                lstSort.SetSelected(intCounter, true);
            }
        }

        private void fraWhere_DoubleClick(object sender, System.EventArgs e)
        {
            cmdClear_Click();
        }

        private void lstFields_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            // If lstFields.ListIndex < 0 Then Exit Sub
            // If vsLayout.Row < 1 Then vsLayout.Row = 1
            // 
            // vsLayout.TextMatrix(vsLayout.Row, vsLayout.Col) = lstFields.List(lstFields.ListIndex)
            // vsLayout.Cell(FCGrid.CellPropertySettings.flexcpData, vsLayout.Row, vsLayout.Col) = lstFields.ItemData(lstFields.ListIndex)
        }

        //private void lstFields_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
        //	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
        //	// WHERE TO SWAP THE TWO ITEMS.
        //	// 
        //	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //	// WILL BE DISPLAYED ON THE REPORT ITSELF
        //	// intStart = lstFields.ListIndex
        //}

        //private void lstFields_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
        //	// ITEMS THAT ARE TO BE SWAPED
        //	// 
        //	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //	// WILL BE DISPLAYED ON THE REPORT ITSELF
        //	// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
        //	// If intStart <> lstFields.ListIndex And lstFields.ListIndex > 0 Then
        //	// SAVE THE CAPTION AND ID FOR THE NEW ITEM
        //	// strtemp = lstFields.List(lstFields.ListIndex)
        //	// intID = lstFields.ItemData(lstFields.ListIndex)
        //	// 
        //	// CHANGE THE NEW ITEM
        //	// lstFields.List(lstFields.ListIndex) = lstFields.List(intStart)
        //	// lstFields.ItemData(lstFields.ListIndex) = lstFields.ItemData(intStart)
        //	// 
        //	// SAVE THE OLD ITEM
        //	// lstFields.List(intStart) = strtemp
        //	// lstFields.ItemData(intStart) = intID
        //	// 
        //	// SET BOTH ITEMS TO BE SELECTED
        //	// lstFields.Selected(lstFields.ListIndex) = True
        //	// lstFields.Selected(intStart) = True
        //	// End If
        //}

        //private void lstSort_MouseDown(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //	// THIS IS USED SO THAT WHEN THE USER CLICKS ON AN ITEM IN THIS
        //	// CONTROL AND DRAGS IT TO A DIFFERENT LOCATION THEN WE WILL KNOW
        //	// WHERE TO SWAP THE TWO ITEMS.
        //	// 
        //	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //	// WILL BE DISPLAYED ON THE REPORT ITSELF
        //	intStart = lstSort.SelectedIndex;
        //}

        //private void lstSort_MouseUp(object sender, Wisej.Web.MouseEventArgs e)
        //{
        //	MouseButtonConstants Button = (MouseButtonConstants)(FCConvert.ToInt32(e.Button) / 0x100000);
        //	int Shift = (FCConvert.ToInt32(Control.ModifierKeys) / 0x10000);
        //	float X = FCConvert.ToSingle(FCUtils.PixelsToTwipsX(e.X);
        //	float Y = FCConvert.ToSingle(FCUtils.PixelsToTwipsY(e.Y);
        //	// THIS WILL CHANGE THE CAPTION AND THE ITEM DATA NUMBER FOR THE TWO
        //	// ITEMS THAT ARE TO BE SWAPED
        //	// 
        //	// THE ORDER THE ITEMS SHOW IN THIS LIST BOX IS THE ORDER THAT THEY
        //	// WILL BE DISPLAYED ON THE REPORT ITSELF
        //	//FC:FINAL:DDU:#i948 - no need for drag and drop here
        //	//bool boolSecondSelected = false;
        //	//// IF THIS IS A CHANGE (DRAG AND DROP) AND NOT JUST A CLICK THEN...
        //	//if (intStart != lstSort.SelectedIndex)
        //	//{
        //	//    // SAVE THE CAPTION AND ID FOR THE NEW ITEM
        //	//    strTemp = lstSort.Items[lstSort.SelectedIndex].Text;
        //	//    intID = lstSort.ItemData(lstSort.SelectedIndex);
        //	//    boolSecondSelected = lstSort.Selected(lstSort.SelectedIndex);
        //	//    // CHANGE THE NEW ITEM
        //	//    lstSort.Items[lstSort.ListIndex] = lstSort.Items[intStart];
        //	//    lstSort.ItemData(lstSort.ListIndex, lstSort.ItemData(intStart));
        //	//    // SAVE THE OLD ITEM
        //	//    lstSort.Items[intStart].Text = strTemp;
        //	//    lstSort.ItemData(intStart, intID);
        //	//    // SET BOTH ITEMS TO BE SELECTED
        //	//    lstSort.SetSelected(lstSort.ListIndex, true);
        //	//    lstSort.SetSelected(intStart, boolSecondSelected);
        //	//}
        //}

        private void mnuClear_Click(object sender, System.EventArgs e)
        {
            cmdClear_Click();
        }

        private void mnuExit_Click(object sender, System.EventArgs e)
        {
            cmdExit_Click();
        }

        private void mnuPrint_Click(object sender, System.EventArgs e)
        {
            cmdPrint_Click();
        }

        private void vsWhere_AfterEdit(object sender, DataGridViewCellEventArgs e)
        {
            // AFTER THE USER SELECTS AN ITEM IN COMBO FOR A FIELD IN THE WHERE
            // GIRD THEN WE NEED TO SAVE THE ***ID*** IN A FIELD FOR THAT SELECTION
            // 
            // THIS NEEDS TO BE SAVED AS THE COMBOITEMLIST IS PER FIELD AND BUILT
            // AND USED DYNAMICALLY INT HE BEFOREEDIT ROUTINE
            // 
            // THIS ID WILL BE USED TO PULL OUT THE CORRECT DATA IN THE WHERE CLAUSE
            // vsWhere.TextMatrix(Row, 2) = vsWhere.ComboData
            if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 1)) == "__/__/____")
            {
                vsWhere.TextMatrix(vsWhere.Row, 1, string.Empty);
            }
            if (Strings.Trim(vsWhere.TextMatrix(vsWhere.Row, 2)) == "__/__/____")
            {
                vsWhere.TextMatrix(vsWhere.Row, 2, string.Empty);
            }
        }

        private void vsWhere_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            vsWhere.EditMask = string.Empty;
            vsWhere.ComboList = string.Empty;
            if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, vsWhere.Row, vsWhere.Col) != modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX)
            {
                if (vsWhere.Row == lngRowAsOfDate || vsWhere.Row == lngRowShowPaymentFrom)
                {
                    vsWhere.EditMask = "##/##/####";
                }
                else if (vsWhere.Row == lngRowReportType || vsWhere.Row == lngRowWS || vsWhere.Row == lngRowShowLien || vsWhere.Row == lngRowGroupBy || vsWhere.Row == lngRowPaymentType || vsWhere.Row == lngRowTaxAcquired)
                {
                    if (vsWhere.Col == 1)
                    {
                        vsWhere.ComboList = modUTStatusList.Statics.strComboList[vsWhere.Row, 0];
                    }
                }
                else if (vsWhere.Row == lngRowAccount || vsWhere.Row == lngRowName || vsWhere.Row == lngRowBill || vsWhere.Row == lngRowBook)
                {
                    if (vsWhere.Col == 1 || vsWhere.Col == 2)
                    {
                        vsWhere.ComboList = modUTStatusList.Statics.strComboList[vsWhere.Row, 0];
                    }
                }
                else if (vsWhere.Row == lngRowBalanceDue)
                {
                    if (vsWhere.Col == 1)
                    {
                        // only show the list if the user is in the first col
                        vsWhere.ComboList = modUTStatusList.Statics.strComboList[vsWhere.Row, 0];
                    }
                }
                else
                {
                }
            }
        }

        private void vsWhere_ChangeEdit(object sender, System.EventArgs e)
        {
            ShowAutomaticFields();
        }

        private void vsWhere_KeyDownEvent(object sender, KeyEventArgs e)
        {
            if (vsWhere.Col > 0)
            {
                switch (e.KeyCode)
                {
                    case Keys.Delete:
                        {
                            // MAL@20070830:Added to not allow Report Type and W/S To Be Cleared
                            if (vsWhere.Row == lngRowReportType)
                            {
                                // Do Nothing
                            }
                            else if (vsWhere.Row == lngRowWS)
                            {
                                // Do Nothing
                            }
                            else
                            {
                                vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
                            }
                            // vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col) = ""
                            break;
                        }
                }
                //end switch
            }
        }

        private void vsWhere_KeyDownEdit(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Delete:
                    {
                        // MAL@20070830:Added to not allow Report Type and W/S To Be Cleared
                        if (vsWhere.Row == lngRowReportType)
                        {
                            // Do Nothing
                        }
                        else if (vsWhere.Row == lngRowWS)
                        {
                            // Do Nothing
                        }
                        else
                        {
                            vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
                        }
                        break;
                    }
                case Keys.Escape:
                    {
                        vsWhere.TextMatrix(vsWhere.Row, vsWhere.Col, "");
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            //end switch
        }

        private void vsWhere_Leave(object sender, System.EventArgs e)
        {
            vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
            vsWhere.Select(0, 1);
        }

        private void vsWhere_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int lngMR;
            int lngMC;
            lngMR = e.RowIndex;
            lngMC = e.ColumnIndex;
            //FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
            if (lngMR > -1 && lngMC > -1)
            {
                DataGridViewCell cell = vsWhere[lngMC, lngMR];
                cell.ToolTipText = "";
                switch (lngMC)
                {
                    case 0 when lngMR == lngRowReportType:
                        cell.ToolTipText = "This will determine whether to show a regular status list of bills or an aged list.";

                        break;
                    case 0 when lngMR == lngRowWS:
                    {
                        // kk trouts-6 03012013  Change Water to Stormwater for Bangor
                        cell.ToolTipText = Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR" 
                            ? "This will show the Stormwater or Sewer bills." 
                            : "This will show the Water or Sewer bills.";

                        break;
                    }
                    case 0 when lngMR == lngRowAccount:
                        cell.ToolTipText = "Show a range of accounts.";

                        break;
                    case 0 when lngMR == lngRowName:
                        cell.ToolTipText = "Show a range of names.";

                        break;
                    case 0 when lngMR == lngRowBill:
                        cell.ToolTipText = "Show a range by rate key.";

                        break;
                    case 0 when lngMR == lngRowBook:
                        cell.ToolTipText = "Show a range by book number.";

                        break;
                    case 0 when lngMR == lngRowShowLien:
                        cell.ToolTipText = "Show regular or liened accounts.";

                        break;
                    case 0 when lngMR == lngRowGroupBy:
                        cell.ToolTipText = "Show subtotals for the bills grouped on this selection.";

                        break;
                    case 0 when lngMR == lngRowPaymentType:
                        cell.ToolTipText = "Only show bills with this payment type.";

                        break;
                    case 0 when lngMR == lngRowAsOfDate:
                        cell.ToolTipText = "Show amounts affect by payments recorded before this date.";

                        break;
                    case 0 when lngMR == lngRowTownCode:
                        cell.ToolTipText = "Show accounts that are associated to this town.";

                        break;
                    case 0 when lngMR == lngRowPaymentType:
                        cell.ToolTipText = "Show bills that have this type of payment.";

                        break;
                    case 0 when lngMR == lngRowAsOfDate:
                        cell.ToolTipText = "Show bills with the payments that would have been recorded at this date.";

                        break;
                    case 0 when lngMR == lngRowShowPaymentFrom:
                        cell.ToolTipText = "Accounts with payments withing this range of dates.";

                        break;
                    case 0:
                    {
                        if (lngMR == lngRowTaxAcquired)
                        {
                            cell.ToolTipText = "Accounts that are either Tax Acquired or not.";
                            // Case lngRowTownCode
                            // vsWhere.ToolTipText = "Residence code."
                            // Case 5
                            // vsWhere.ToolTipText = "This will show accounts with payment amounts recorded prior to the 'As Of' Date."
                            // Case 6
                            // vsWhere.ToolTipText = "This will only show accounts with payment amounts recorded during the date range supplied."
                        }

                        break;
                    }
                }
            }
        }

        private void vsWhere_RowColChange(object sender, System.EventArgs e)
        {
            // SET THE FORMAT FOR THE TYPE OF FIELD THAT THIS IS
            // IF COMBO...ADD THE LIST OF OPTIONS
            int lngRow;
            int lngCol;
            lngRow = vsWhere.Row;
            lngCol = vsWhere.Col;
            //FC:FINAL:MSH - issue #942: Incorrect comparing. States of the cells in the 1 row aren't changed
            //if (lngRow > 0)
            if (lngRow >= 0)
            {
                if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngCol) != modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX)
                {
                    if (lngRow == vsWhere.Rows - 1)
                    {
                        vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabControls;
                    }
                    else
                    {
                        vsWhere.TabBehavior = FCGrid.TabBehaviorSettings.flexTabCells;
                    }
                    if (modUTStatusList.Statics.strComboList[lngRow, 0] != string.Empty)
                    {
                        vsWhere.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
                        vsWhere.ComboList = modUTStatusList.Statics.strComboList[lngRow, 0];
                    }
                    if (lngCol == 2)
                    {
                        if (vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngCol) == modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND)
                        {
                            Support.SendKeys("{Tab}", false);
                        }
                    }
                }
                else
                {
                    vsWhere.Editable = FCGrid.EditableSettings.flexEDNone;
                }
            }
        }

        private void vsWhere_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                //FC:FINAL:MSH - issue #942: save and use correct indexes of the cell
                int row = vsWhere.GetFlexRowIndex(e.RowIndex);
                int col = vsWhere.GetFlexColIndex(e.ColumnIndex);
                if (row == lngRowReportType)
                {
                    if (vsWhere.EditText == "Regular")
                    {
                        modUTStatusList.SetupStatusListCombos_2(true);
                        if (vsWhere.TextMatrix(lngRowWS, 1) == "Both")
                        {
                            if (modUTStatusPayments.Statics.gboolPayWaterFirst)
                            {
                                vsWhere.TextMatrix(lngRowWS, 1, "Water");
                            }
                            else
                            {
                                vsWhere.TextMatrix(lngRowWS, 1, "Sewer");
                            }
                        }
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowShowLien, 1, Color.White);
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowGroupBy, 1, Color.White);
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowBill, 1, Color.White);
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowAsOfDate, 1, Color.White);
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowPaymentType, 1, Color.White);
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowShowPaymentFrom, 1, lngRowShowPaymentFrom, 2, Color.White);
                        chkShowPayments.Enabled = true;
                        cmbNameOption.Enabled = true;
                        chkInclude[4].Enabled = false;
                        // chkPrincipalOnly.Value = vbUnchecked
                        // chkPrincipalOnly.Enabled = False
                    }
                    else
                    {
                        modUTStatusList.SetupStatusListCombos_2(false);
                        vsWhere.TextMatrix(lngRowShowLien, 1, "");
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowShowLien, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                        vsWhere.TextMatrix(lngRowGroupBy, 1, "");
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowGroupBy, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                        vsWhere.TextMatrix(lngRowBill, 1, "");
                        vsWhere.TextMatrix(lngRowBill, 2, "");
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowAsOfDate, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                        vsWhere.TextMatrix(lngRowAsOfDate, 1, "");
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowPaymentType, 1, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                        vsWhere.TextMatrix(lngRowPaymentType, 1, "");
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowShowPaymentFrom, 1, lngRowShowPaymentFrom, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                        vsWhere.TextMatrix(lngRowShowPaymentFrom, 1, "");
                        vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowBill, 1, lngRowBill, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                        chkShowPayments.CheckState = Wisej.Web.CheckState.Unchecked;
                        chkShowPayments.Enabled = false;
                        cmbNameOption.Enabled = false;
                        chkInclude[4].Enabled = true;
                        if (chkUseFullStatus.CheckState == Wisej.Web.CheckState.Unchecked)
                        {
                            chkInclude[2].Enabled = true;
                            // chkPrincipalOnly.Enabled = True
                        }
                        else
                        {
                            chkInclude[2].Enabled = false;
                            chkInclude[2].CheckState = Wisej.Web.CheckState.Checked;
                            // chkPrincipalOnly.Value = vbUnchecked
                            // chkPrincipalOnly.Enabled = False
                        }
                    }
                }
                else if (row == lngRowAsOfDate || row == lngRowShowPaymentFrom)
                {
                    if (Strings.Trim(vsWhere.EditText) == "/  /")
                    {
                        vsWhere.EditMask = string.Empty;
                        vsWhere.EditText = string.Empty;
                        vsWhere.TextMatrix(row, col, string.Empty);
                        vsWhere.Refresh();
                        return;
                    }
                }
                ShowAutomaticFields();
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validate Edit", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void EnableFrames_2(bool boolEnable)
        {
            EnableFrames(ref boolEnable);
        }

        private void EnableFrames(ref bool boolEnable)
        {
            fraFields.Enabled = boolEnable;
            fraSort.Enabled = boolEnable;
            fraWhere.Enabled = boolEnable;
            vsWhere.Enabled = boolEnable;
            lstFields.Enabled = boolEnable;
            lstSort.Enabled = boolEnable;
        }

        private void Set_Note_Text()
        {
            string strTemp;
            strTemp = "1. The Fields To Sort By section provides a way to choose the order of the report.  If you select Account, then the report will have accounts listed by their account number." + "\r\n" + "\r\n";
            strTemp += "2. Set any criteria needed in the Select Search Criteria list." + "\r\n" + "\r\n";
            strTemp += "Other Notes:" + "\r\n" + "Some fields will be printed automatically." + "\r\n" + "\r\n";
            strTemp += "\r\n" + "If you choose a default report, only the account number and the tax year criteria will affect the report.  Any changes in the other fields will not be used in the creation of the report." + "\r\n";
        }

        private void FindAllYears()
        {
            // this will fill the variable strYearChoice with all of the possible years and an All Years Option
            clsDRWrapper rsYear = new clsDRWrapper();

            try
            {
                rsYear.OpenRecordset("SELECT * FROM PaymentYears", modExtraModules.strUTDatabase);
                if (rsYear.EndOfFile() != true && rsYear.BeginningOfFile() != true)
                {
                    var intNum = 1;
                    strYearChoice = "#0;All Years|";
                    while (!rsYear.EndOfFile())
                    {
                        // TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
                        strYearChoice += "#" + FCConvert.ToString(intNum) + ";" + modExtraModules.FormatYear(rsYear.Get_Fields("Year")) + "|";
                        intNum += 1;
                        rsYear.MoveNext();
                    }
                    // take off the last pipe character '|'
                    strYearChoice = Strings.Left(strYearChoice, strYearChoice.Length - 1);
                }
                else
                {
                    strYearChoice = "#0;All Years";
                }
            }
            finally
            {
                rsYear.DisposeOf();
            }
        }

        private void CheckReportTable()
        {
            clsDRWrapper rsCreateTable = new clsDRWrapper();
            // XXXXX kgk 07-30-2=12
            // With rsCreateTable
            // CREATE A NEW TABLE IF IT DOESN'T EXIST
            // If .CreateNewDatabaseTable("SavedStatusReports", DEFAULTDATABASE) Then
            // CREATE THE NEW FIELDS
            // .CreateTableField "ID", dbLong
            // .CreateTableField "ReportName", dbText
            // .CreateTableField "Type", dbText
            // .CreateTableField "SQL", dbMemo
            // .CreateTableField "LastUpdated", dbDate
            // 
            // .CreateTableField "WhereSelection", dbText
            // .CreateTableField "SortSelection", dbText
            // .CreateTableField "FieldConstraint0", dbText
            // .CreateTableField "FieldConstraint1", dbText
            // .CreateTableField "FieldConstraint2", dbText
            // .CreateTableField "FieldConstraint3", dbText
            // .CreateTableField "FieldConstraint4", dbText
            // .CreateTableField "FieldConstraint5", dbText
            // .CreateTableField "FieldConstraint6", dbText
            // .CreateTableField "FieldConstraint7", dbText
            // .CreateTableField "FieldConstraint8", dbText
            // .CreateTableField "FieldConstraint9", dbText
            // 
            // SET THE PROPERTIES OF THE NEW FIELDS
            // .SetFieldAttribute "ID", dbAutoIncrField
            // .SetFieldDefaultValue "LastUpdated", "NOW()"
            // .SetFieldAllowZeroLength "ReportName", True
            // .SetFieldAllowZeroLength "Type", True
            // .SetFieldAllowZeroLength "SQL", True
            // 
            // .SetFieldAllowZeroLength "WhereSelection", True
            // .SetFieldAllowZeroLength "SortSelection", True
            // .SetFieldAllowZeroLength "FieldConstraint0", True
            // .SetFieldAllowZeroLength "FieldConstraint1", True
            // .SetFieldAllowZeroLength "FieldConstraint2", True
            // .SetFieldAllowZeroLength "FieldConstraint3", True
            // .SetFieldAllowZeroLength "FieldConstraint4", True
            // .SetFieldAllowZeroLength "FieldConstraint5", True
            // .SetFieldAllowZeroLength "FieldConstraint6", True
            // .SetFieldAllowZeroLength "FieldConstraint7", True
            // .SetFieldAllowZeroLength "FieldConstraint8", True
            // .SetFieldAllowZeroLength "FieldConstraint9", True
            // 
            // DO THE ACTUAL CREATION OF THE TABLE
            // .UpdateTableCreation
            // End If
            // End With
            // 
            // With rsCreateTable
            // CREATE A NEW TABLE IF IT DOESN'T EXIST
            // If .CreateNewDatabaseTable("tblReportLayout", DEFAULTDATABASE) Then
            // CREATE THE NEW FIELDS
            // .CreateTableField "AutoID", dbLong
            // .CreateTableField "ReportID", dbInteger
            // .CreateTableField "RowID", dbInteger
            // .CreateTableField "ColumnID", dbInteger
            // .CreateTableField "FieldID", dbInteger
            // .CreateTableField "Width", dbInteger
            // .CreateTableField "DisplayText", dbText
            // .CreateTableField "LastUpdated", dbDate
            // 
            // SET THE PROPERTIES OF THE NEW FIELDS
            // .SetFieldAttribute "AutoID", dbAutoIncrField
            // .SetFieldDefaultValue "LastUpdated", "NOW()"
            // .SetFieldAllowZeroLength "DisplayText", True
            // 
            // DO THE ACTUAL CREATION OF THE TABLE
            // .UpdateTableCreation
            // End If
            // End With
        }

        private string GetWhereConstraint(ref short intRow)
        {
            string GetWhereConstraint = "";
            // this will go to the grid and get the constraint for that row and return it
            // if there is none, then it will return a nullstring
            int lngType;
            string strTemp;
            strTemp = vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 1) + "|||" + vsWhere.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intRow, 2);
            GetWhereConstraint = strTemp;
            return GetWhereConstraint;
        }

        private bool ItemInSortList(ref short intCounter)
        {
            bool ItemInSortList = false;
            // this will return true if the item has already been added to the Sort List
            int intTemp;
            ItemInSortList = false;
            for (intTemp = 0; intTemp <= lstSort.Items.Count - 1; intTemp++)
            {
                if (lstSort.ItemData(intTemp) == intCounter)
                {
                    ItemInSortList = true;
                    break;
                }
            }
            return ItemInSortList;
        }

        public void LoadSortList()
        {
            // LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
            lstSort.Clear();
            lstSort.AddItem("Account Number");
            lstSort.ItemData(lstSort.NewIndex, 0);
            lstSort.AddItem("Name");
            lstSort.ItemData(lstSort.NewIndex, 1);
            lstSort.AddItem("Bill Number");
            lstSort.ItemData(lstSort.NewIndex, 2);
        }

        private void FillHardCodeCombo()
        {
            // this will fill the hard coded combo list with the list of reports that the user can choose
            cmbHardCode.Clear();
            cmbHardCode.AddItem("Non Zero Balance on All Accounts");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 2);
            cmbHardCode.AddItem("Non Zero Balance on Non Lien Accounts");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 0);
            cmbHardCode.AddItem("Non Zero Balance on Lien Accounts");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 1);
            cmbHardCode.AddItem("Lien Breakdown");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 3);
            cmbHardCode.AddItem("Zero Balance Report");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 4);
            cmbHardCode.AddItem("Negative Balance Report");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 5);
            cmbHardCode.AddItem("Supplemental Outstanding Balance Report");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 6);
            cmbHardCode.AddItem("Supplemental Negative Balance Report");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 7);
            cmbHardCode.AddItem("Supplemental Zero Balance Report");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 8);
            cmbHardCode.AddItem("Outstanding Balance By Period");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 9);
            cmbHardCode.AddItem("Account Detail Report");
            cmbHardCode.ItemData(cmbHardCode.NewIndex, 10);
            // this will also fill the name options combo
            cmbNameOption.Clear();
            cmbNameOption.AddItem("Show Current Owner");
            cmbNameOption.ItemData(cmbNameOption.NewIndex, 0);
            cmbNameOption.AddItem("Show Billed Owner");
            cmbNameOption.ItemData(cmbNameOption.NewIndex, 1);
            cmbNameOption.AddItem("Show Billed Owner C\\O Current Owner");
            cmbNameOption.ItemData(cmbNameOption.NewIndex, 2);
            cmbNameOption.AddItem("Show Current Owner Bill= Billed Owner");
            cmbNameOption.ItemData(cmbNameOption.NewIndex, 3);
            cmbNameOption.SelectedIndex = 0;
        }
        // vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
        private void ShowHardCodedReport_2(short intIndex)
        {
            ShowHardCodedReport(ref intIndex);
        }

        private void ShowHardCodedReport(ref short intIndex)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                switch (intIndex)
                {
                    case 0:
                        {
                            // Non Zero Balance on Non Lien Accounts
                            frmReportViewer.InstancePtr.Init(rptUTOutstandingBalances.InstancePtr, allowCancel: true);
                            break;
                        }
                    case 1:
                        {
                            // Non Zero Balance on Lien Accounts
                            frmReportViewer.InstancePtr.Init(rptUTOutstandingLienBalances.InstancePtr, allowCancel: true);
                            break;
                        }
                    case 2:
                        {
                            // Non Zero Balance on Lien Accounts
                            frmReportViewer.InstancePtr.Init(rptUTOutstandingBalancesAll.InstancePtr, allowCancel: true);
                            intMasterReportType = 0;
                            break;
                        }
                    case 3:
                        {
                            // Lien Breakdown Report
                            frmReportViewer.InstancePtr.Init(arUTLienStatusReport.InstancePtr, allowCancel: true);
                            break;
                        }
                    case 4:
                        {
                            // Zero Balance Report
                            intMasterReportType = 4;
                            frmReportViewer.InstancePtr.Init(rptUTOutstandingBalancesAll.InstancePtr, allowCancel: true);
                            break;
                        }
                    case 5:
                        {
                            // Negative Balance Report
                            intMasterReportType = 5;
                            frmReportViewer.InstancePtr.Init(rptUTOutstandingBalancesAll.InstancePtr, allowCancel: true);
                            break;
                        }
                    case 6:
                        {
                            // Supplemental Outstanding Balance Report
                            intMasterReportType = 6;
                            frmReportViewer.InstancePtr.Init(rptUTOutstandingBalancesAll.InstancePtr, allowCancel: true);
                            break;
                        }
                    case 7:
                        {
                            // Supplemental Negative Balance Report
                            intMasterReportType = 7;
                            frmReportViewer.InstancePtr.Init(rptUTOutstandingBalancesAll.InstancePtr, allowCancel: true);
                            break;
                        }
                    case 8:
                        {
                            // Supplemental Zero Balance Report
                            intMasterReportType = 8;
                            frmReportViewer.InstancePtr.Init(rptUTOutstandingBalancesAll.InstancePtr, allowCancel: true);
                            break;
                        }
                    case 9:
                        {
                            // Outstanding Balance By Period
                            frmRateRecChoice.InstancePtr.intRateType = 50;
                            frmRateRecChoice.InstancePtr.Show();
                            break;
                        }
                    case 10:
                        {
                            intMasterReportType = 10;
                            frmReportViewer.InstancePtr.Init(rptUTStatusListAccountDetail.InstancePtr, allowCancel: true);
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
                //end switch
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Hard Coded Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void ShowAutomaticFields()
        {
            string strRptType = "";
            chkUseFullStatus.Enabled = true;
            if (chkHardCode.CheckState != Wisej.Web.CheckState.Checked)
            {
                // this is not a hard coded report
                if (vsWhere.TextMatrix(lngRowReportType, 1) != "Regular")
                {
                    // this is an Aged Report
                    lblShowFields.Text = "Fields automatically included on report:" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Type" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Amount Due" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Payment Received" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Abatements and Adjustments" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Balance Due" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Refunded Abatements" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
                }
                else
                {
                    // this is a Regular Report
                    lblShowFields.Text = "Fields automatically included on the Status List:" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Bill Date" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Original Amount" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Payments / Adjustments" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Total Due" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Breakdown Of Amount Owed" + "\r\n";
                }
            }
            else
            {
                // this is a hardcoded report
                if (cmbHardCode.SelectedIndex == -1)
                {
                    lblShowFields.Text = "Please select a report from the 'Default Report' list." + "\r\n";
                }
                else
                {
                    if (cmbHardCode.ItemData(cmbHardCode.SelectedIndex) == 3)
                    {
                        chkUseFullStatus.CheckState = Wisej.Web.CheckState.Checked;
                        chkUseFullStatus.Enabled = false;
                    }
                    lblShowFields.Text = "Fields automatically included on report:" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Account Number" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Type" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Name" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Rate Key" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Original Principal" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Payments and Adjustments" + "\r\n";
                    lblShowFields.Text = lblShowFields.Text + "    Amount Due" + "\r\n";
                }
            }
        }

        private bool ValidateWhereGrid()
        {
            bool ValidateWhereGrid = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                string strSwap = "";
                // lngRowReportType
                // lngRowWS
                // lngRowAccount
                if (Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 2)) > 0)
                {
                    if (Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowAccount, 2)))
                    {
                        strSwap = vsWhere.TextMatrix(0, 1);
                        vsWhere.TextMatrix(lngRowAccount, 1, vsWhere.TextMatrix(lngRowAccount, 2));
                        vsWhere.TextMatrix(lngRowAccount, 2, strSwap);
                    }
                }
                // lngRowName
                // lngRowBill
                if (Conversion.Val(vsWhere.TextMatrix(lngRowBill, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowBill, 2)) > 0)
                {
                    if (Conversion.Val(vsWhere.TextMatrix(lngRowBill, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowBill, 2)))
                    {
                        strSwap = vsWhere.TextMatrix(lngRowBill, 1);
                        vsWhere.TextMatrix(lngRowBill, 1, vsWhere.TextMatrix(lngRowBill, 2));
                        vsWhere.TextMatrix(lngRowBill, 2, strSwap);
                    }
                }
                // lngRowBook
                if (Conversion.Val(vsWhere.TextMatrix(lngRowBook, 1)) > 0 && Conversion.Val(vsWhere.TextMatrix(lngRowBook, 2)) > 0)
                {
                    if (Conversion.Val(vsWhere.TextMatrix(lngRowBook, 1)) > Conversion.Val(vsWhere.TextMatrix(lngRowBook, 2)))
                    {
                        strSwap = vsWhere.TextMatrix(lngRowBook, 1);
                        vsWhere.TextMatrix(lngRowBook, 1, vsWhere.TextMatrix(lngRowBook, 2));
                        vsWhere.TextMatrix(lngRowBook, 2, strSwap);
                    }
                }
                // lngRowShowLien
                // 3 - Balance Due
                // 4 - Only Accounts with Type
                // 5 - As Of Date:
                // If IsDate(vsWhere.TextMatrix(5, 1)) Then
                // gdtUTStatusListAsOfDate = CDate(vsWhere.TextMatrix(5, 1))
                // Else
                // gdtUTStatusListAsOfDate = Date
                // End If
                // 
                // If gdtUTStatusListAsOfDate <> Date Then
                // gboolUTUseAsOfDate = True
                // Else
                // gboolUTUseAsOfDate = False
                // End If
                // 
                // Show Payments from Date range
                modMain.Statics.gboolUTSLDateRange = false;
                if (Strings.Trim(vsWhere.TextMatrix(11, 1)) != "")
                {
                    // make sure that there is something in the field
                    if (Information.IsDate(vsWhere.TextMatrix(11, 1)))
                    {
                        // make sure that it is a date
                        modMain.Statics.gdtUTSLPaymentDate1 = DateAndTime.DateValue(vsWhere.TextMatrix(11, 1));
                        modMain.Statics.gboolUTSLDateRange = true;
                        if (Strings.Trim(vsWhere.TextMatrix(11, 2)) != "")
                        {
                            if (Information.IsDate(vsWhere.TextMatrix(11, 2)))
                            {
                                modMain.Statics.gdtUTSLPaymentDate2 = DateAndTime.DateValue(vsWhere.TextMatrix(11, 2));
                            }
                            else
                            {
                                modMain.Statics.gdtUTSLPaymentDate2 = DateTime.Today;
                            }
                        }
                        else
                        {
                            modMain.Statics.gdtUTSLPaymentDate2 = DateTime.Today;
                        }
                    }
                }
                ValidateWhereGrid = true;
                return ValidateWhereGrid;
            }
            catch (Exception ex)
            {
                
                ValidateWhereGrid = false;
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Validating Where Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return ValidateWhereGrid;
        }

        private void SetExtraFields()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will check the fields list to see if any other fields should be added
                if (lstFields.Selected(0))
                {
                    // check the location
                    boolShowLocation = true;
                }
                else
                {
                    boolShowLocation = false;
                }
                if (lstFields.Selected(1))
                {
                    // check the map lot
                    boolShowMapLot = true;
                }
                else
                {
                    boolShowMapLot = false;
                }
                return;
            }
            catch
            {
                
                boolShowLocation = false;
                boolShowMapLot = false;
            }
        }

        public void Format_WhereGrid_2(bool boolReset)
        {
            Format_WhereGrid(boolReset);
        }

        public void Format_WhereGrid(bool boolReset = false)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int intCounter;
                int lngWid = 0;
                if (boolReset)
                {
                    vsWhere.Rows = 0;
                }
                vsWhere.Rows = 13;
                vsWhere.Cols = 4;
                lngWid = vsWhere.WidthOriginal;
                vsWhere.ColWidth(0, FCConvert.ToInt32(lngWid * 0.38));
                vsWhere.ColWidth(1, FCConvert.ToInt32(lngWid * 0.285));
                vsWhere.ColWidth(2, FCConvert.ToInt32(lngWid * 0.285));
                vsWhere.ColWidth(3, 0);
                // Report Type (ie Regular or Aged Listing)
                vsWhere.TextMatrix(lngRowReportType, 0, "Report Type");
                vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowReportType, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                // Water/Sewer/Both
                // kk trouts-6 03012013  Change Water to Stormwater for Bangor
                vsWhere.TextMatrix(lngRowWS, 0, _isBangor ? "Stormwater or Sewer" : "Water or Sewer");
                vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowWS, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                // Account Range
                vsWhere.TextMatrix(lngRowAccount, 0, "Account");
                // Name Range
                vsWhere.TextMatrix(lngRowName, 0, "Name");
                // Bill Range
                vsWhere.TextMatrix(lngRowBill, 0, "Bill Number");
                // Book Range
                vsWhere.TextMatrix(lngRowBook, 0, "Book");
                // Balance Due
                vsWhere.TextMatrix(lngRowBalanceDue, 0, "Balance Due");
                // Bill Types (ie Regular, Lien, Both)
                vsWhere.TextMatrix(lngRowShowLien, 0, "Show Account Types");
                vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowShowLien, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                vsWhere.TextMatrix(lngRowGroupBy, 0, "Group By");
                vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowGroupBy, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                // Payment Type
                vsWhere.TextMatrix(lngRowPaymentType, 0, "Only Accounts With Type");
                vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowPaymentType, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                // As Of Date
                vsWhere.TextMatrix(lngRowAsOfDate, 0, "As Of Date");
                vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowAsOfDate, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                // Show Payment From
                vsWhere.TextMatrix(lngRowShowPaymentFrom, 0, "Show Payments From");
                // Tax Acquired
                vsWhere.TextMatrix(lngRowTaxAcquired, 0, "Tax Acquired");
                vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRowTaxAcquired, 2, modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
                // Town Code
                // .TextMatrix(lngRowTownCode, 0) = "Town Code"
                // .Height = (.rows * .RowHeight(0)) + 70
                vsWhere.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
                // set the defaults
                if (boolReset)
                {
                    vsWhere.TextMatrix(lngRowReportType, 1, "Regular");

                    switch (modUTStatusPayments.Statics.TownService)
                    {
                        // kk trouts-6 03012013  Change Water to Stormwater for Bangor
                        case "W" when _isBangor:
                            vsWhere.TextMatrix(lngRowWS, 1, "Stormwater");

                            break;
                        case "W":
                            vsWhere.TextMatrix(lngRowWS, 1, "Water");

                            break;
                        case "S":
                            vsWhere.TextMatrix(lngRowWS, 1, "Sewer");

                            break;
                        case "B" when modUTStatusPayments.Statics.gboolPayWaterFirst:
                        {
                            // kk trouts-6 03012013  Change Water to Stormwater for Bangor
                            vsWhere.TextMatrix(lngRowWS, 1, _isBangor ? "Stormwater" : "Water");

                            break;
                        }
                        case "B":
                            vsWhere.TextMatrix(lngRowWS, 1, "Sewer");

                            break;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Where Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void Fill_Lists()
        {
            // this routine will fill the extra fields and the sort lists
            lstFields.Clear();
            lstFields.AddItem("Location");
            lstFields.AddItem("Map / Lot");
            lstSort.Clear();
            lstSort.AddItem("Account Number");
            lstSort.AddItem("Name");
            lstSort.AddItem("Bill Number");
            lstSort.AddItem("Book Number");
            lstSort.AddItem("Map / Lot");
            // kgk trout-743  Add Map/Lot to sort
            lstSort.SetSelected(1, true);
            // automatically select name
        }
    }
}
