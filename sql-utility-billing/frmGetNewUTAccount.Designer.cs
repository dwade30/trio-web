﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmNewUTAccountNumber.
	/// </summary>
	partial class frmNewUTAccountNumber : BaseForm
	{
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCLabel lblText;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmNewUTAccountNumber));
            this.txtAccount = new fecherFoundation.FCTextBox();
            this.lblText = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 125);
            this.BottomPanel.Size = new System.Drawing.Size(325, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtAccount);
            this.ClientArea.Controls.Add(this.lblText);
            this.ClientArea.Size = new System.Drawing.Size(345, 284);
            this.ClientArea.Controls.SetChildIndex(this.lblText, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAccount, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(345, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            // 
            // txtAccount
            // 
            this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccount.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtAccount.Location = new System.Drawing.Point(30, 85);
            this.txtAccount.MaxLength = 9;
            this.txtAccount.Name = "txtAccount";
            this.txtAccount.Size = new System.Drawing.Size(110, 40);
            this.txtAccount.TabIndex = 1;
            this.txtAccount.Text = "0";
            this.txtAccount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtAccount.Enter += new System.EventHandler(this.txtAccount_Enter);
            // 
            // lblText
            // 
            this.lblText.Location = new System.Drawing.Point(30, 44);
            this.lblText.Name = "lblText";
            this.lblText.Size = new System.Drawing.Size(313, 20);
            this.lblText.TabIndex = 0;
            this.lblText.Text = "PLEASE ENTER THE NEW ACCOUNT NUMBER";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSave,
            this.mnuFileSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 0;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 1;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 2;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(116, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.TabIndex = 2;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmNewUTAccountNumber
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(345, 344);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmNewUTAccountNumber";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = " ";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmNewUTAccountNumber_Load);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmNewUTAccountNumber_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmNewUTAccountNumber_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
