﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBillEmailOptions.
	/// </summary>
	partial class frmBillEmailOptions : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWaterSewer;
		public fecherFoundation.FCLabel lblWaterSewer;
		public fecherFoundation.FCFrame fraEmailMessage;
		public fecherFoundation.FCRichTextBox txtEmailMessage;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCButton cmdClearAll;
		public fecherFoundation.FCButton cmdSelectAll;
		public FCGrid fgrdAccounts;
		public fecherFoundation.FCButton cmdProcessSave;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillEmailOptions));
            this.cmbWaterSewer = new fecherFoundation.FCComboBox();
            this.lblWaterSewer = new fecherFoundation.FCLabel();
            this.fraEmailMessage = new fecherFoundation.FCFrame();
            this.txtEmailMessage = new fecherFoundation.FCRichTextBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.cmdClearAll = new fecherFoundation.FCButton();
            this.cmdSelectAll = new fecherFoundation.FCButton();
            this.fgrdAccounts = new fecherFoundation.FCGrid();
            this.cmdProcessSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraEmailMessage)).BeginInit();
            this.fraEmailMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailMessage)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fgrdAccounts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcessSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 389);
            this.BottomPanel.Size = new System.Drawing.Size(757, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraEmailMessage);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.cmbWaterSewer);
            this.ClientArea.Controls.Add(this.lblWaterSewer);
            this.ClientArea.Size = new System.Drawing.Size(757, 329);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(757, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(134, 30);
            this.HeaderText.Text = "E-Mail Bills";
            // 
            // cmbWaterSewer
            // 
            this.cmbWaterSewer.Items.AddRange(new object[] {
            "Water",
            "Sewer",
            "Both"});
            this.cmbWaterSewer.Location = new System.Drawing.Point(148, 30);
            this.cmbWaterSewer.Name = "cmbWaterSewer";
            this.cmbWaterSewer.Size = new System.Drawing.Size(152, 40);
            this.cmbWaterSewer.TabIndex = 9;
            this.cmbWaterSewer.Text = "Both";
            // 
            // lblWaterSewer
            // 
            this.lblWaterSewer.AutoSize = true;
            this.lblWaterSewer.Location = new System.Drawing.Point(30, 44);
            this.lblWaterSewer.Name = "lblWaterSewer";
            this.lblWaterSewer.Size = new System.Drawing.Size(62, 15);
            this.lblWaterSewer.TabIndex = 10;
            this.lblWaterSewer.Text = "INCLUDE";
            this.lblWaterSewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraEmailMessage
            // 
            this.fraEmailMessage.Controls.Add(this.txtEmailMessage);
            this.fraEmailMessage.Location = new System.Drawing.Point(320, 30);
            this.fraEmailMessage.Name = "fraEmailMessage";
            this.fraEmailMessage.Size = new System.Drawing.Size(409, 123);
            this.fraEmailMessage.TabIndex = 8;
            this.fraEmailMessage.Text = "E-Mail Message";
            // 
            // txtEmailMessage
            // 
            this.txtEmailMessage.Location = new System.Drawing.Point(20, 30);
            this.txtEmailMessage.Name = "txtEmailMessage";
            this.txtEmailMessage.Size = new System.Drawing.Size(371, 73);
            this.txtEmailMessage.TabIndex = 9;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.cmdClearAll);
            this.Frame1.Controls.Add(this.cmdSelectAll);
            this.Frame1.Controls.Add(this.fgrdAccounts);
            this.Frame1.Location = new System.Drawing.Point(30, 173);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(699, 384);
            this.Frame1.TabIndex = 4;
            this.Frame1.Text = "Select Bills To Send";
            // 
            // cmdClearAll
            // 
            this.cmdClearAll.AppearanceKey = "actionButton";
            this.cmdClearAll.ForeColor = System.Drawing.Color.White;
            this.cmdClearAll.Location = new System.Drawing.Point(341, 324);
            this.cmdClearAll.Name = "cmdClearAll";
            this.cmdClearAll.Size = new System.Drawing.Size(108, 40);
            this.cmdClearAll.TabIndex = 7;
            this.cmdClearAll.Text = "Clear All";
            this.cmdClearAll.Click += new System.EventHandler(this.cmdClearAll_Click);
            // 
            // cmdSelectAll
            // 
            this.cmdSelectAll.AppearanceKey = "actionButton";
            this.cmdSelectAll.ForeColor = System.Drawing.Color.White;
            this.cmdSelectAll.Location = new System.Drawing.Point(209, 324);
            this.cmdSelectAll.Name = "cmdSelectAll";
            this.cmdSelectAll.Size = new System.Drawing.Size(112, 40);
            this.cmdSelectAll.TabIndex = 6;
            this.cmdSelectAll.Text = "Select All";
            this.cmdSelectAll.Click += new System.EventHandler(this.cmdSelectAll_Click);
            // 
            // fgrdAccounts
            // 
            this.fgrdAccounts.Cols = 5;
            this.fgrdAccounts.ExtendLastCol = true;
            this.fgrdAccounts.FixedCols = 0;
            this.fgrdAccounts.Location = new System.Drawing.Point(20, 30);
            this.fgrdAccounts.Name = "fgrdAccounts";
            this.fgrdAccounts.RowHeadersVisible = false;
            this.fgrdAccounts.Rows = 1;
            this.fgrdAccounts.Size = new System.Drawing.Size(659, 274);
            this.fgrdAccounts.TabIndex = 5;
            this.fgrdAccounts.Click += new System.EventHandler(this.fgrdAccounts_ClickEvent);
            this.fgrdAccounts.KeyDown += new Wisej.Web.KeyEventHandler(this.fgrdAccounts_KeyDownEvent);
            // 
            // cmdProcessSave
            // 
            this.cmdProcessSave.AppearanceKey = "acceptButton";
            this.cmdProcessSave.Location = new System.Drawing.Point(220, 30);
            this.cmdProcessSave.Name = "cmdProcessSave";
            this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcessSave.Size = new System.Drawing.Size(185, 48);
            this.cmdProcessSave.Text = "Save & Continue";
            this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
            // 
            // frmBillEmailOptions
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(757, 497);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmBillEmailOptions";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "E-Mail Bills";
            this.Load += new System.EventHandler(this.frmBillEmailOptions_Load);
            this.Activated += new System.EventHandler(this.frmBillEmailOptions_Activated);
            this.Resize += new System.EventHandler(this.frmBillEmailOptions_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmBillEmailOptions_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraEmailMessage)).EndInit();
            this.fraEmailMessage.ResumeLayout(false);
            this.fraEmailMessage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtEmailMessage)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdClearAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSelectAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fgrdAccounts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
