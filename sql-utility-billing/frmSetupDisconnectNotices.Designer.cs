﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmSetupDisconnectNotices.
	/// </summary>
	partial class frmSetupDisconnectNotices : BaseForm
	{
		public fecherFoundation.FCComboBox cmbTotalDue;
		public fecherFoundation.FCLabel lblTotalDue;
		public fecherFoundation.FCComboBox cmbNewMailer;
		public fecherFoundation.FCLabel lblNewMailer;
		public fecherFoundation.FCComboBox cmbAll;
		public fecherFoundation.FCLabel lblAll;
		public System.Collections.Generic.List<fecherFoundation.FCCheckBox> chkSendTo;
		public fecherFoundation.FCCheckBox chkExcludeNC;
		public fecherFoundation.FCFrame Frame5;
		public fecherFoundation.FCCheckBox chkIncludeDepositText;
		public fecherFoundation.FCTextBox txtReconFeeAH;
		public fecherFoundation.FCTextBox txtReconFeeReg;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame fraSendTo;
		public fecherFoundation.FCCheckBox chkSendTo_0;
		public fecherFoundation.FCCheckBox chkSendTo_1;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCFrame fraNewMailer;
		public Global.T2KDateBox txtPayByDate;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame Frame2;
		public Global.T2KDateBox txtNoticeDate;
		public fecherFoundation.FCFrame fraText;
		public fecherFoundation.FCTextBox txtDefaultText;
		public fecherFoundation.FCFrame fraDefaultInfo;
		public FCGrid vsDefaults;
		public fecherFoundation.FCFrame Frame7;
		public fecherFoundation.FCTextBox txtMinimumAmount;
		public fecherFoundation.FCFrame Frame6;
		public fecherFoundation.FCTextBox txtAddress4;
		public fecherFoundation.FCTextBox txtAddress3;
		public fecherFoundation.FCTextBox txtAddress2;
		public fecherFoundation.FCTextBox txtAddress1;
		public fecherFoundation.FCTextBox txtName;
		public fecherFoundation.FCCheckBox chkIncludeSewer;
		public fecherFoundation.FCFrame fraIncludeSewer;
		public fecherFoundation.FCCheckBox chkSeperateSewer;
		public fecherFoundation.FCFrame Frame4;
		public fecherFoundation.FCFrame fraSelectedBooks;
		public fecherFoundation.FCTextBox txtSequenceNumber;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCFrame fraSelectedAccount;
		public fecherFoundation.FCTextBox txtAccountNumber;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCFrame Frame3;
		public Global.T2KDateBox txtShutOffDate;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFileReminderNotices;
		public fecherFoundation.FCToolStripMenuItem mnuFileProcess;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSetupDisconnectNotices));
            this.cmbTotalDue = new fecherFoundation.FCComboBox();
            this.lblTotalDue = new fecherFoundation.FCLabel();
            this.cmbNewMailer = new fecherFoundation.FCComboBox();
            this.lblNewMailer = new fecherFoundation.FCLabel();
            this.cmbAll = new fecherFoundation.FCComboBox();
            this.lblAll = new fecherFoundation.FCLabel();
            this.chkExcludeNC = new fecherFoundation.FCCheckBox();
            this.Frame5 = new fecherFoundation.FCFrame();
            this.chkIncludeDepositText = new fecherFoundation.FCCheckBox();
            this.txtReconFeeAH = new fecherFoundation.FCTextBox();
            this.txtReconFeeReg = new fecherFoundation.FCTextBox();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.fraSendTo = new fecherFoundation.FCFrame();
            this.chkSendTo_0 = new fecherFoundation.FCCheckBox();
            this.chkSendTo_1 = new fecherFoundation.FCCheckBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.fraNewMailer = new fecherFoundation.FCFrame();
            this.txtPayByDate = new Global.T2KDateBox();
            this.Label1 = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.txtNoticeDate = new Global.T2KDateBox();
            this.fraText = new fecherFoundation.FCFrame();
            this.txtDefaultText = new fecherFoundation.FCTextBox();
            this.fraDefaultInfo = new fecherFoundation.FCFrame();
            this.vsDefaults = new fecherFoundation.FCGrid();
            this.Frame7 = new fecherFoundation.FCFrame();
            this.txtMinimumAmount = new fecherFoundation.FCTextBox();
            this.Frame6 = new fecherFoundation.FCFrame();
            this.txtAddress4 = new fecherFoundation.FCTextBox();
            this.txtAddress3 = new fecherFoundation.FCTextBox();
            this.txtAddress2 = new fecherFoundation.FCTextBox();
            this.txtAddress1 = new fecherFoundation.FCTextBox();
            this.txtName = new fecherFoundation.FCTextBox();
            this.chkIncludeSewer = new fecherFoundation.FCCheckBox();
            this.fraIncludeSewer = new fecherFoundation.FCFrame();
            this.chkSeperateSewer = new fecherFoundation.FCCheckBox();
            this.Frame4 = new fecherFoundation.FCFrame();
            this.fraSelectedBooks = new fecherFoundation.FCFrame();
            this.txtSequenceNumber = new fecherFoundation.FCTextBox();
            this.Label2 = new fecherFoundation.FCLabel();
            this.fraSelectedAccount = new fecherFoundation.FCFrame();
            this.txtAccountNumber = new fecherFoundation.FCTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Frame3 = new fecherFoundation.FCFrame();
            this.txtShutOffDate = new Global.T2KDateBox();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileReminderNotices = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileProcess = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdFileProcess = new fecherFoundation.FCButton();
            this.cmdFileReminderNotices = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludeNC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).BeginInit();
            this.Frame5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeDepositText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSendTo)).BeginInit();
            this.fraSendTo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSendTo_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSendTo_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraNewMailer)).BeginInit();
            this.fraNewMailer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayByDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoticeDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraText)).BeginInit();
            this.fraText.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultInfo)).BeginInit();
            this.fraDefaultInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDefaults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).BeginInit();
            this.Frame7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).BeginInit();
            this.Frame6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraIncludeSewer)).BeginInit();
            this.fraIncludeSewer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeperateSewer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).BeginInit();
            this.Frame4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedBooks)).BeginInit();
            this.fraSelectedBooks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedAccount)).BeginInit();
            this.fraSelectedAccount.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtShutOffDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileReminderNotices)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdFileProcess);
            this.BottomPanel.Location = new System.Drawing.Point(0, 721);
            this.BottomPanel.Size = new System.Drawing.Size(1004, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.chkExcludeNC);
            this.ClientArea.Controls.Add(this.chkIncludeSewer);
            this.ClientArea.Controls.Add(this.Frame5);
            this.ClientArea.Controls.Add(this.fraSendTo);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraText);
            this.ClientArea.Controls.Add(this.fraDefaultInfo);
            this.ClientArea.Controls.Add(this.Frame7);
            this.ClientArea.Controls.Add(this.Frame6);
            this.ClientArea.Controls.Add(this.fraIncludeSewer);
            this.ClientArea.Controls.Add(this.Frame4);
            this.ClientArea.Size = new System.Drawing.Size(1004, 661);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileReminderNotices);
            this.TopPanel.Size = new System.Drawing.Size(1004, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileReminderNotices, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(223, 30);
            this.HeaderText.Text = "Disconnect Notices";
            // 
            // cmbTotalDue
            // 
            this.cmbTotalDue.Items.AddRange(new object[] {
            "Total Due for Selected Rate Key",
            "Total Due on Account as of Notice Date"});
            this.cmbTotalDue.Location = new System.Drawing.Point(126, 142);
            this.cmbTotalDue.Name = "cmbTotalDue";
            this.cmbTotalDue.Size = new System.Drawing.Size(411, 40);
            this.cmbTotalDue.TabIndex = 1;
            // 
            // lblTotalDue
            // 
            this.lblTotalDue.AutoSize = true;
            this.lblTotalDue.Location = new System.Drawing.Point(22, 156);
            this.lblTotalDue.Name = "lblTotalDue";
            this.lblTotalDue.Size = new System.Drawing.Size(83, 16);
            this.lblTotalDue.Text = "TOTAL DUE";
            // 
            // cmbNewMailer
            // 
            this.cmbNewMailer.Items.AddRange(new object[] {
            "Full Page Plain Paper",
            "Original Mailer",
            "Original Plain Paper",
            "Optional Mailer 7in"});
            this.cmbNewMailer.Location = new System.Drawing.Point(135, 30);
            this.cmbNewMailer.Name = "cmbNewMailer";
            this.cmbNewMailer.Size = new System.Drawing.Size(313, 40);
            this.cmbNewMailer.TabIndex = 3;
            this.cmbNewMailer.Text = "Full Page Plain Paper";
            // 
            // lblNewMailer
            // 
            this.lblNewMailer.AutoSize = true;
            this.lblNewMailer.Location = new System.Drawing.Point(20, 44);
            this.lblNewMailer.Name = "lblNewMailer";
            this.lblNewMailer.Size = new System.Drawing.Size(58, 16);
            this.lblNewMailer.TabIndex = 2;
            this.lblNewMailer.Text = "MAILER";
            // 
            // cmbAll
            // 
            this.cmbAll.Items.AddRange(new object[] {
            "All",
            "Selected Books",
            "Selected Account"});
            this.cmbAll.Location = new System.Drawing.Point(96, 30);
            this.cmbAll.Name = "cmbAll";
            this.cmbAll.Size = new System.Drawing.Size(210, 40);
            this.cmbAll.TabIndex = 1;
            this.cmbAll.Text = "All";
            this.cmbAll.SelectedIndexChanged += new System.EventHandler(this.cmbAll_SelectedIndexChanged);
            // 
            // lblAll
            // 
            this.lblAll.AutoSize = true;
            this.lblAll.Location = new System.Drawing.Point(20, 44);
            this.lblAll.Name = "lblAll";
            this.lblAll.Size = new System.Drawing.Size(55, 16);
            this.lblAll.TabIndex = 3;
            this.lblAll.Text = "RANGE";
            // 
            // chkExcludeNC
            // 
            this.chkExcludeNC.Location = new System.Drawing.Point(375, 389);
            this.chkExcludeNC.Name = "chkExcludeNC";
            this.chkExcludeNC.Size = new System.Drawing.Size(201, 23);
            this.chkExcludeNC.TabIndex = 3;
            this.chkExcludeNC.Text = "Exclude No Charge Accounts";
            // 
            // Frame5
            // 
            this.Frame5.Controls.Add(this.lblTotalDue);
            this.Frame5.Controls.Add(this.chkIncludeDepositText);
            this.Frame5.Controls.Add(this.cmbTotalDue);
            this.Frame5.Controls.Add(this.txtReconFeeAH);
            this.Frame5.Controls.Add(this.txtReconFeeReg);
            this.Frame5.Controls.Add(this.Label5);
            this.Frame5.Controls.Add(this.Label4);
            this.Frame5.Location = new System.Drawing.Point(30, 549);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(572, 201);
            this.Frame5.TabIndex = 11;
            this.Frame5.Text = "Reconnect Fees";
            // 
            // chkIncludeDepositText
            // 
            this.chkIncludeDepositText.Location = new System.Drawing.Point(20, 93);
            this.chkIncludeDepositText.Name = "chkIncludeDepositText";
            this.chkIncludeDepositText.Size = new System.Drawing.Size(178, 23);
            this.chkIncludeDepositText.TabIndex = 4;
            this.chkIncludeDepositText.Text = "Deposit May Be Required";
            // 
            // txtReconFeeAH
            // 
            this.txtReconFeeAH.BackColor = System.Drawing.SystemColors.Window;
            this.txtReconFeeAH.Location = new System.Drawing.Point(449, 30);
            this.txtReconFeeAH.Name = "txtReconFeeAH";
            this.txtReconFeeAH.Size = new System.Drawing.Size(100, 40);
            this.txtReconFeeAH.TabIndex = 3;
            // 
            // txtReconFeeReg
            // 
            this.txtReconFeeReg.BackColor = System.Drawing.SystemColors.Window;
            this.txtReconFeeReg.Location = new System.Drawing.Point(143, 30);
            this.txtReconFeeReg.Name = "txtReconFeeReg";
            this.txtReconFeeReg.Size = new System.Drawing.Size(100, 40);
            this.txtReconFeeReg.TabIndex = 1;
            // 
            // Label5
            // 
            this.Label5.Location = new System.Drawing.Point(20, 44);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(74, 18);
            this.Label5.TabIndex = 5;
            this.Label5.Text = "REGULAR";
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(300, 44);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(96, 18);
            this.Label4.TabIndex = 2;
            this.Label4.Text = "AFTER HOURS";
            // 
            // fraSendTo
            // 
            this.fraSendTo.Controls.Add(this.chkSendTo_0);
            this.fraSendTo.Controls.Add(this.chkSendTo_1);
            this.fraSendTo.Location = new System.Drawing.Point(380, 201);
            this.fraSendTo.Name = "fraSendTo";
            this.fraSendTo.Size = new System.Drawing.Size(216, 104);
            this.fraSendTo.TabIndex = 4;
            this.fraSendTo.Text = "Send To";
            // 
            // chkSendTo_0
            // 
            this.chkSendTo_0.Checked = true;
            this.chkSendTo_0.CheckState = ((Wisej.Web.CheckState)(Wisej.Web.CheckState.Checked));
            this.chkSendTo_0.Enabled = false;
            this.chkSendTo_0.Location = new System.Drawing.Point(20, 67);
            this.chkSendTo_0.Name = "chkSendTo_0";
            this.chkSendTo_0.Size = new System.Drawing.Size(68, 23);
            this.chkSendTo_0.TabIndex = 1;
            this.chkSendTo_0.Text = "Tenant";
            this.chkSendTo_0.Visible = false;
            this.chkSendTo_0.CheckedChanged += new System.EventHandler(this.chkSendTo_CheckedChanged);
            // 
            // chkSendTo_1
            // 
            this.chkSendTo_1.Location = new System.Drawing.Point(20, 30);
            this.chkSendTo_1.Name = "chkSendTo_1";
            this.chkSendTo_1.Size = new System.Drawing.Size(137, 23);
            this.chkSendTo_1.TabIndex = 2;
            this.chkSendTo_1.Text = "Owner (if different)";
            this.chkSendTo_1.CheckedChanged += new System.EventHandler(this.chkSendTo_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.fraNewMailer);
            this.Frame1.Controls.Add(this.cmbNewMailer);
            this.Frame1.Controls.Add(this.lblNewMailer);
            this.Frame1.Location = new System.Drawing.Point(30, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(566, 149);
            this.Frame1.TabIndex = 12;
            this.Frame1.Text = "Notice Type";
            // 
            // fraNewMailer
            // 
            this.fraNewMailer.AppearanceKey = "groupBoxNoBorders";
            this.fraNewMailer.Controls.Add(this.txtPayByDate);
            this.fraNewMailer.Controls.Add(this.Label1);
            this.fraNewMailer.Enabled = false;
            this.fraNewMailer.Location = new System.Drawing.Point(1, 73);
            this.fraNewMailer.Name = "fraNewMailer";
            this.fraNewMailer.Size = new System.Drawing.Size(305, 70);
            this.fraNewMailer.TabIndex = 4;
            this.fraNewMailer.Visible = false;
            // 
            // txtPayByDate
            // 
            this.txtPayByDate.Location = new System.Drawing.Point(135, 17);
            this.txtPayByDate.Mask = "##/##/####";
            this.txtPayByDate.MaxLength = 10;
            this.txtPayByDate.Name = "txtPayByDate";
            this.txtPayByDate.Size = new System.Drawing.Size(130, 22);
            this.txtPayByDate.TabIndex = 1;
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 31);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(96, 18);
            this.Label1.TabIndex = 2;
            this.Label1.Text = "PAID BY DATE";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.txtNoticeDate);
            this.Frame2.Location = new System.Drawing.Point(30, 438);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(189, 90);
            this.Frame2.TabIndex = 7;
            this.Frame2.Text = "Notice Date";
            // 
            // txtNoticeDate
            // 
            this.txtNoticeDate.Location = new System.Drawing.Point(20, 30);
            this.txtNoticeDate.Mask = "##/##/####";
            this.txtNoticeDate.MaxLength = 10;
            this.txtNoticeDate.Name = "txtNoticeDate";
            this.txtNoticeDate.Size = new System.Drawing.Size(147, 22);
            // 
            // fraText
            // 
            this.fraText.Controls.Add(this.txtDefaultText);
            this.fraText.Location = new System.Drawing.Point(625, 484);
            this.fraText.Name = "fraText";
            this.fraText.Size = new System.Drawing.Size(396, 90);
            this.fraText.TabIndex = 10;
            this.fraText.Text = "Default Text";
            // 
            // txtDefaultText
            // 
            this.txtDefaultText.BackColor = System.Drawing.SystemColors.Window;
            this.txtDefaultText.Location = new System.Drawing.Point(20, 30);
            this.txtDefaultText.Name = "txtDefaultText";
            this.txtDefaultText.Size = new System.Drawing.Size(356, 40);
            // 
            // fraDefaultInfo
            // 
            this.fraDefaultInfo.Controls.Add(this.vsDefaults);
            this.fraDefaultInfo.Location = new System.Drawing.Point(625, 320);
            this.fraDefaultInfo.Name = "fraDefaultInfo";
            this.fraDefaultInfo.Size = new System.Drawing.Size(396, 144);
            this.fraDefaultInfo.TabIndex = 6;
            this.fraDefaultInfo.Text = "Default Information";
            // 
            // vsDefaults
            // 
            this.vsDefaults.Cols = 3;
            this.vsDefaults.ColumnHeadersVisible = false;
            this.vsDefaults.FixedRows = 0;
            this.vsDefaults.Location = new System.Drawing.Point(20, 30);
            this.vsDefaults.Name = "vsDefaults";
            this.vsDefaults.Rows = 1;
            this.vsDefaults.ShowFocusCell = false;
            this.vsDefaults.Size = new System.Drawing.Size(356, 94);
            this.vsDefaults.StandardTab = false;
            this.vsDefaults.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsDefaults.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsDefaults_BeforeEdit);
            this.vsDefaults.CurrentCellChanged += new System.EventHandler(this.vsDefaults_RowColChange);
            this.vsDefaults.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDefaults_MouseMoveEvent);
            // 
            // Frame7
            // 
            this.Frame7.Controls.Add(this.txtMinimumAmount);
            this.Frame7.Location = new System.Drawing.Point(452, 438);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(150, 90);
            this.Frame7.TabIndex = 9;
            this.Frame7.Text = "Min Amount";
            // 
            // txtMinimumAmount
            // 
            this.txtMinimumAmount.BackColor = System.Drawing.SystemColors.Window;
            this.txtMinimumAmount.Location = new System.Drawing.Point(20, 30);
            this.txtMinimumAmount.Name = "txtMinimumAmount";
            this.txtMinimumAmount.Size = new System.Drawing.Size(110, 40);
            // 
            // Frame6
            // 
            this.Frame6.Controls.Add(this.txtAddress4);
            this.Frame6.Controls.Add(this.txtAddress3);
            this.Frame6.Controls.Add(this.txtAddress2);
            this.Frame6.Controls.Add(this.txtAddress1);
            this.Frame6.Controls.Add(this.txtName);
            this.Frame6.Location = new System.Drawing.Point(625, 30);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(396, 270);
            this.Frame6.TabIndex = 1;
            this.Frame6.Text = "Return Address";
            // 
            // txtAddress4
            // 
            this.txtAddress4.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress4.Location = new System.Drawing.Point(20, 210);
            this.txtAddress4.MaxLength = 50;
            this.txtAddress4.Name = "txtAddress4";
            this.txtAddress4.Size = new System.Drawing.Size(356, 40);
            this.txtAddress4.TabIndex = 4;
            // 
            // txtAddress3
            // 
            this.txtAddress3.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress3.Location = new System.Drawing.Point(20, 165);
            this.txtAddress3.MaxLength = 50;
            this.txtAddress3.Name = "txtAddress3";
            this.txtAddress3.Size = new System.Drawing.Size(356, 40);
            this.txtAddress3.TabIndex = 3;
            // 
            // txtAddress2
            // 
            this.txtAddress2.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress2.Location = new System.Drawing.Point(20, 120);
            this.txtAddress2.MaxLength = 50;
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(356, 40);
            this.txtAddress2.TabIndex = 2;
            // 
            // txtAddress1
            // 
            this.txtAddress1.BackColor = System.Drawing.SystemColors.Window;
            this.txtAddress1.Location = new System.Drawing.Point(20, 75);
            this.txtAddress1.MaxLength = 50;
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(356, 40);
            this.txtAddress1.TabIndex = 1;
            // 
            // txtName
            // 
            this.txtName.BackColor = System.Drawing.SystemColors.Window;
            this.txtName.Location = new System.Drawing.Point(20, 30);
            this.txtName.MaxLength = 50;
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(356, 40);
            this.txtName.TabIndex = 5;
            // 
            // chkIncludeSewer
            // 
            this.chkIncludeSewer.Location = new System.Drawing.Point(400, 311);
            this.chkIncludeSewer.Name = "chkIncludeSewer";
            this.chkIncludeSewer.Size = new System.Drawing.Size(111, 23);
            this.chkIncludeSewer.TabIndex = 4;
            this.chkIncludeSewer.Text = "Include Sewer";
            this.chkIncludeSewer.CheckedChanged += new System.EventHandler(this.chkIncludeSewer_CheckedChanged);
            // 
            // fraIncludeSewer
            // 
            this.fraIncludeSewer.AppearanceKey = "groupBoxNoBorders";
            this.fraIncludeSewer.Controls.Add(this.chkSeperateSewer);
            this.fraIncludeSewer.Enabled = false;
            this.fraIncludeSewer.Location = new System.Drawing.Point(375, 337);
            this.fraIncludeSewer.Name = "fraIncludeSewer";
            this.fraIncludeSewer.Size = new System.Drawing.Size(240, 40);
            this.fraIncludeSewer.TabIndex = 5;
            // 
            // chkSeperateSewer
            // 
            this.chkSeperateSewer.Location = new System.Drawing.Point(25, 7);
            this.chkSeperateSewer.Name = "chkSeperateSewer";
            this.chkSeperateSewer.Size = new System.Drawing.Size(169, 23);
            this.chkSeperateSewer.TabIndex = 1;
            this.chkSeperateSewer.Text = "Show Sewer Separately";
            // 
            // Frame4
            // 
            this.Frame4.Controls.Add(this.fraSelectedBooks);
            this.Frame4.Controls.Add(this.cmbAll);
            this.Frame4.Controls.Add(this.lblAll);
            this.Frame4.Controls.Add(this.fraSelectedAccount);
            this.Frame4.Location = new System.Drawing.Point(30, 201);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(325, 214);
            this.Frame4.TabIndex = 2;
            this.Frame4.Text = "Account Range";
            // 
            // fraSelectedBooks
            // 
            this.fraSelectedBooks.AppearanceKey = "groupBoxNoBorders";
            this.fraSelectedBooks.Controls.Add(this.txtSequenceNumber);
            this.fraSelectedBooks.Controls.Add(this.Label2);
            this.fraSelectedBooks.Enabled = false;
            this.fraSelectedBooks.Location = new System.Drawing.Point(20, 90);
            this.fraSelectedBooks.Name = "fraSelectedBooks";
            this.fraSelectedBooks.Size = new System.Drawing.Size(299, 58);
            this.fraSelectedBooks.TabIndex = 2;
            // 
            // txtSequenceNumber
            // 
            this.txtSequenceNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtSequenceNumber.Location = new System.Drawing.Point(169, 0);
            this.txtSequenceNumber.Name = "txtSequenceNumber";
            this.txtSequenceNumber.Size = new System.Drawing.Size(117, 40);
            this.txtSequenceNumber.TabIndex = 1;
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(0, 14);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(152, 18);
            this.Label2.TabIndex = 2;
            this.Label2.Text = "STARTING SEQUENCE #";
            // 
            // fraSelectedAccount
            // 
            this.fraSelectedAccount.AppearanceKey = "groupBoxNoBorders";
            this.fraSelectedAccount.Controls.Add(this.txtAccountNumber);
            this.fraSelectedAccount.Controls.Add(this.Label3);
            this.fraSelectedAccount.Enabled = false;
            this.fraSelectedAccount.Location = new System.Drawing.Point(16, 150);
            this.fraSelectedAccount.Name = "fraSelectedAccount";
            this.fraSelectedAccount.Size = new System.Drawing.Size(303, 43);
            this.fraSelectedAccount.TabIndex = 3;
            // 
            // txtAccountNumber
            // 
            this.txtAccountNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtAccountNumber.Location = new System.Drawing.Point(172, 0);
            this.txtAccountNumber.Name = "txtAccountNumber";
            this.txtAccountNumber.Size = new System.Drawing.Size(117, 40);
            this.txtAccountNumber.TabIndex = 1;
            this.txtAccountNumber.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccountNumber_Validating);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(0, 14);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(81, 18);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "ACCOUNT #";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.txtShutOffDate);
            this.Frame3.Location = new System.Drawing.Point(240, 438);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(188, 90);
            this.Frame3.TabIndex = 8;
            this.Frame3.Text = "Shut Off Date";
            // 
            // txtShutOffDate
            // 
            this.txtShutOffDate.Location = new System.Drawing.Point(20, 30);
            this.txtShutOffDate.Mask = "##/##/####";
            this.txtShutOffDate.MaxLength = 10;
            this.txtShutOffDate.Name = "txtShutOffDate";
            this.txtShutOffDate.Size = new System.Drawing.Size(147, 22);
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileReminderNotices,
            this.mnuFileProcess,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFileReminderNotices
            // 
            this.mnuFileReminderNotices.Index = 0;
            this.mnuFileReminderNotices.Name = "mnuFileReminderNotices";
            this.mnuFileReminderNotices.Text = "Print Labels";
            this.mnuFileReminderNotices.Click += new System.EventHandler(this.mnuFileReminderNotices_Click);
            // 
            // mnuFileProcess
            // 
            this.mnuFileProcess.Index = 1;
            this.mnuFileProcess.Name = "mnuFileProcess";
            this.mnuFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileProcess.Text = "Process";
            this.mnuFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdFileProcess
            // 
            this.cmdFileProcess.AppearanceKey = "acceptButton";
            this.cmdFileProcess.Location = new System.Drawing.Point(425, 18);
            this.cmdFileProcess.Name = "cmdFileProcess";
            this.cmdFileProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdFileProcess.Size = new System.Drawing.Size(113, 48);
            this.cmdFileProcess.Text = "Process";
            this.cmdFileProcess.Click += new System.EventHandler(this.mnuFileProcess_Click);
            // 
            // cmdFileReminderNotices
            // 
            this.cmdFileReminderNotices.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdFileReminderNotices.Location = new System.Drawing.Point(874, 29);
            this.cmdFileReminderNotices.Name = "cmdFileReminderNotices";
            this.cmdFileReminderNotices.Size = new System.Drawing.Size(90, 24);
            this.cmdFileReminderNotices.TabIndex = 1;
            this.cmdFileReminderNotices.Text = "Print Labels";
            this.cmdFileReminderNotices.Click += new System.EventHandler(this.mnuFileReminderNotices_Click);
            // 
            // frmSetupDisconnectNotices
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1004, 829);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSetupDisconnectNotices";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Disconnect Notices";
            this.Load += new System.EventHandler(this.frmSetupDisconnectNotices_Load);
            this.Activated += new System.EventHandler(this.frmSetupDisconnectNotices_Activated);
            this.Resize += new System.EventHandler(this.frmSetupDisconnectNotices_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupDisconnectNotices_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkExcludeNC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame5)).EndInit();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeDepositText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSendTo)).EndInit();
            this.fraSendTo.ResumeLayout(false);
            this.fraSendTo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSendTo_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkSendTo_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraNewMailer)).EndInit();
            this.fraNewMailer.ResumeLayout(false);
            this.fraNewMailer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtPayByDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtNoticeDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraText)).EndInit();
            this.fraText.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraDefaultInfo)).EndInit();
            this.fraDefaultInfo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsDefaults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame7)).EndInit();
            this.Frame7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame6)).EndInit();
            this.Frame6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeSewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraIncludeSewer)).EndInit();
            this.fraIncludeSewer.ResumeLayout(false);
            this.fraIncludeSewer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSeperateSewer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame4)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.Frame4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedBooks)).EndInit();
            this.fraSelectedBooks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraSelectedAccount)).EndInit();
            this.fraSelectedAccount.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtShutOffDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileReminderNotices)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileProcess;
		private FCButton cmdFileReminderNotices;
	}
}
