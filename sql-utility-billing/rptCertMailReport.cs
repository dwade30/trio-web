﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptCertMailReport.
	/// </summary>
	public partial class rptCertMailReport : BaseSectionReport
	{
		public rptCertMailReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptCertMailReport InstancePtr
		{
			get
			{
				return (rptCertMailReport)Sys.GetInstance(typeof(rptCertMailReport));
			}
		}

		protected rptCertMailReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCertMailReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/05/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/05/2004              *
		// This will reflect the changes that were made in the    *
		// Last printing of CMF but it will show the whole list   *
		// ********************************************************
		int lngTotalAccounts;
		clsDRWrapper rsData = new clsDRWrapper();
		// vbPorter upgrade warning: intPrintOption As short	OnWriteFCConvert.ToInt32(
		public void Init(short intType, ref int intPrintOption)
		{
			string strSQL;
			string strPrintOption = "";
			// inttype will be 20 for 30 day notice, 21 for transfer tax to lien and 22 for lien maturity
			switch (intPrintOption)
			{
				case 0:
					{
						// print nothing
						strPrintOption = " AND Account = 0";
						break;
					}
				case 1:
					{
						// kgk strPrintOption = " AND (NOT NewOwner = -1 AND MortgageHolder = 0)"
						strPrintOption = " AND (ISNULL(NewOwner,0) = 0 AND MortgageHolder = 0)";
						break;
					}
				case 2:
					{
						strPrintOption = " AND MortgageHolder <> 0";
						break;
					}
				case 3:
					{
						// kgk strPrintOption = " AND NOT NewOwner = -1"
						strPrintOption = " AND ISNULL(NewOwner,0) = 0";
						break;
					}
				case 4:
					{
						// kgk strPrintOption = " AND NewOwner = -1"
						strPrintOption = " AND ISNULL(NewOwner,0) = 1";
						break;
					}
				case 5:
					{
						strPrintOption = " AND MortgageHolder = 0";
						break;
					}
				case 6:
					{
						// kgk strPrintOption = " AND (NewOwner = -1 OR MortgageHolder <> 0)"
						strPrintOption = " AND (ISNULL(NewOwner,0) = 1 OR MortgageHolder <> 0)";
						break;
					}
				case 7:
					{
						// do nothing...this is all options
						break;
					}
			}
			//end switch
			strSQL = "SELECT * FROM CMFNumbers WHERE not isnull(BARCODE,'') = '' AND Type = " + FCConvert.ToString(intType) + strPrintOption + " ORDER BY BarCode";
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (rsData.EndOfFile() != true)
			{
				SetHeaderString();
				lngTotalAccounts = rsData.RecordCount();
				// Me.Show
			}
			else
			{
				MessageBox.Show("There are no eligible accounts to print.", "No Eligible Accounts", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (this.Document.Pages.Count > 0)
			{
				modGlobalFunctions.IncrementSavedReports("UTCMFList");
				this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "UTCMFList1.RDF"));
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			// save this report in case of a printer jam
			// Me.Pages.Save "RPT\LastUTCMFReport.RDF"
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will print the row from the grid
				if (rsData.EndOfFile() != true && rsData.BeginningOfFile() != true)
				{
					// TODO Get_Fields: Check the table for the column [Account] and replace with corresponding Get_Field method
					fldAccount.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields("Account")));
					fldCMFNumber.Text = Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 13, 4) + " " + Strings.Right(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 4);
					// kk09232015 trouts-157  Add IMPB Tracking Number to report
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber"))) != "")
					{
						fldIMPBnum.Text = Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 15, 2) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 17, 4) + " " + Strings.Right(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 2);
					}
					else
					{
						fldIMPBnum.Text = "";
					}
					fldName.Text = rsData.Get_Fields_String("Name");
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Bind Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetHeaderString()
		{
			// this will set the correct header for this account
			string strTemp = "";
			strTemp += "Certified Mail Report";
			lblHeader.Text = strTemp;
			if (Strings.Trim(modGlobalConstants.Statics.gstrCityTown) != "")
			{
				strTemp = modGlobalConstants.Statics.gstrCityTown + " of " + modGlobalConstants.Statics.MuniName;
			}
			else
			{
				strTemp = modGlobalConstants.Statics.MuniName;
			}
			strTemp += "\r\n" + Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 5, 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("BarCode")), 9, 4);
			// kk09232015 trouts-157  Add IMPB Tracking info
			if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber"))) != "")
			{
				strTemp += "\r\n" + "IMPB " + Strings.Left(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 5, 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 9, 4) + " " + Strings.Mid(FCConvert.ToString(rsData.Get_Fields_String("IMPBTrackingNumber")), 13, 2);
			}
			lblHeader2.Text = strTemp;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			if (lngTotalAccounts == 1)
			{
				fldTotalProcessed.Text = "Processed " + FCConvert.ToString(lngTotalAccounts) + " forms.";
			}
			else
			{
				fldTotalProcessed.Text = "Processed " + FCConvert.ToString(lngTotalAccounts) + " forms.";
			}
		}

		private void rptCertMailReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCertMailReport properties;
			//rptCertMailReport.Caption	= "Account Detail";
			//rptCertMailReport.Icon	= "rptCertMailReport.dsx":0000";
			//rptCertMailReport.Left	= 0;
			//rptCertMailReport.Top	= 0;
			//rptCertMailReport.Width	= 14505;
			//rptCertMailReport.Height	= 8595;
			//rptCertMailReport.StartUpPosition	= 3;
			//rptCertMailReport.SectionData	= "rptCertMailReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
