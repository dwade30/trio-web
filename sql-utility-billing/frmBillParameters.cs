﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBillParameters.
	/// </summary>
	public partial class frmBillParameters : BaseForm
	{
		public frmBillParameters()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillParameters InstancePtr
		{
			get
			{
				return (frmBillParameters)Sys.GetInstance(typeof(frmBillParameters));
			}
		}

		protected frmBillParameters _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// DATE           :               02/09/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/17/2005              *
		// ********************************************************
		const int CNSTGRIDMESSAGECOLAUTOID = 0;
		const int CNSTGRIDMESSAGECOLAUTOPOP = 2;
		const int CNSTGRIDMESSAGECOLMESSAGENUM = 1;
		const int CNSTGRIDMESSAGECOLDESCRIPTION = 3;
		const int CNSTGRIDMESSAGECOLMESSAGE = 4;
		private int lngBillIDToReturn;
		bool boolStillLoading;
		bool boolCombinedOnly;

		public int Init(bool boolCombined)
		{
			int Init = 0;
			// returns the chosen bill id
			boolCombinedOnly = boolCombined;
			lngBillIDToReturn = 0;
			boolStillLoading = true;
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngBillIDToReturn;
			return Init;
		}

		private void cmbBill_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			SizeThePic_2(FCConvert.ToInt16(cmbBill.SelectedIndex));
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			framRTB.Visible = false;
		}

		private void cmdOky_Click(object sender, System.EventArgs e)
		{
			framRTB.Visible = false;
			GridMessage.TextMatrix(GridMessage.Row, CNSTGRIDMESSAGECOLMESSAGE, RichTextBox1.Text);
			GridMessage.RowData(GridMessage.Row, true);
			mnuAddMessage.Enabled = true;
			mnuDelete.Enabled = true;
		}

		private void frmBillParameters_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBillParameters_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBillParameters properties;
			//frmBillParameters.FillStyle	= 0;
			//frmBillParameters.ScaleWidth	= 9300;
			//frmBillParameters.ScaleHeight	= 7845;
			//frmBillParameters.LinkTopic	= "Form2";
			//frmBillParameters.LockControls	= true;
			//frmBillParameters.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridMessage();
			FillControls();
			boolStillLoading = false;
		}

		private void SetupGridMessage()
		{
			GridMessage.Rows = 1;
			GridMessage.Cols = 5;
			GridMessage.ColHidden(CNSTGRIDMESSAGECOLAUTOID, true);
			GridMessage.ColHidden(CNSTGRIDMESSAGECOLAUTOPOP, true);
			GridMessage.TextMatrix(0, CNSTGRIDMESSAGECOLMESSAGENUM, "Num");
			GridMessage.TextMatrix(0, CNSTGRIDMESSAGECOLAUTOPOP, "Prompt");
			GridMessage.TextMatrix(0, CNSTGRIDMESSAGECOLDESCRIPTION, "Description");
			GridMessage.TextMatrix(0, CNSTGRIDMESSAGECOLMESSAGE, "Msg");
			GridMessage.ColDataType(CNSTGRIDMESSAGECOLAUTOPOP, FCGrid.DataTypeSettings.flexDTBoolean);
			GridMessage.ColComboList(CNSTGRIDMESSAGECOLMESSAGE, "...");
			GridMessage.CellButtonPicture = null;
		}

		private void frmBillParameters_Resize(object sender, System.EventArgs e)
		{
			ResizeGridMessage();
            //FC:FINAL:SBE - not needed anymore because the form was redesigned
            //ResizeThePic();
		}

		private void GridMessage_CellButtonClick(object sender, EventArgs e)
		{
			switch (GridMessage.Col)
			{
				case CNSTGRIDMESSAGECOLMESSAGE:
					{
						RichTextBox1.Text = GridMessage.TextMatrix(GridMessage.Row, CNSTGRIDMESSAGECOLMESSAGE);
						mnuAddMessage.Enabled = false;
						mnuDelete.Enabled = false;
						framRTB.Visible = true;
						framRTB.BringToFront();
						framRTB.Text = "Message " + FCConvert.ToString(Conversion.Val(GridMessage.TextMatrix(GridMessage.Row, CNSTGRIDMESSAGECOLMESSAGENUM)));
						//Application.DoEvents();
						RichTextBox1.Focus();
						break;
					}
			}
			//end switch
		}

		private void GridMessage_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
		{
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
            {
                return;
            }
            DataGridViewCell cell = GridMessage[e.ColumnIndex, e.RowIndex];
            int lngCol;
			lngCol = GridMessage.GetFlexColIndex(e.ColumnIndex);
			switch (lngCol)
			{
				case CNSTGRIDMESSAGECOLAUTOPOP:
					{
						//ToolTip1.SetToolTip(GridMessage, "Mark this as true if you want to be automatically prompted to view/edit the message each time you print bills");
						cell.ToolTipText = "Mark this as true if you want to be automatically prompted to view/edit the message each time you print bills";
						break;
					}
				default:
					{
                        //ToolTip1.SetToolTip(GridMessage, "");
                        cell.ToolTipText = "";
						break;
					}
			}
			//end switch
		}

		private void GridMessage_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			GridMessage.RowData(GridMessage.GetFlexRowIndex(e.RowIndex), true);
		}

		private void mnuAddMessage_Click(object sender, System.EventArgs e)
		{
			int X;
			int intMax;
			intMax = 0;
			for (X = 1; X <= GridMessage.Rows - 1; X++)
			{
				if (Conversion.Val(GridMessage.TextMatrix(X, CNSTGRIDMESSAGECOLMESSAGENUM)) > intMax)
				{
					intMax = FCConvert.ToInt32(Math.Round(Conversion.Val(GridMessage.TextMatrix(X, CNSTGRIDMESSAGECOLMESSAGENUM))));
				}
			}
			// X
			intMax += 1;
			GridMessage.Rows += 1;
			GridMessage.TextMatrix(GridMessage.Rows - 1, CNSTGRIDMESSAGECOLAUTOID, FCConvert.ToString(0));
			GridMessage.TextMatrix(GridMessage.Rows - 1, CNSTGRIDMESSAGECOLMESSAGENUM, FCConvert.ToString(intMax));
			GridMessage.TextMatrix(GridMessage.Rows - 1, CNSTGRIDMESSAGECOLAUTOPOP, FCConvert.ToString(false));
			GridMessage.RowData(GridMessage.Rows - 1, true);
			GridMessage.Row = GridMessage.Rows - 1;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			if (GridMessage.Row < 1)
				return;
			GridDelete.Rows += 1;
			GridDelete.TextMatrix(GridDelete.Rows - 1, 0, GridMessage.TextMatrix(GridMessage.Row, CNSTGRIDMESSAGECOLAUTOID));
			GridMessage.RemoveItem(GridMessage.Row);
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void ResizeGridMessage()
		{
			int GridWidth = 0;
			GridWidth = GridMessage.WidthOriginal;
			GridMessage.ColWidth(CNSTGRIDMESSAGECOLMESSAGENUM, FCConvert.ToInt32(0.11 * GridWidth));
			// .ColWidth(CNSTGRIDMESSAGECOLAUTOPOP) = 0.17 * GridWidth
			GridMessage.ColWidth(CNSTGRIDMESSAGECOLDESCRIPTION, FCConvert.ToInt32(0.5 * GridWidth));
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			if (SaveData())
			{
				mnuExit_Click();
			}
		}

		private bool SaveData()
		{
			bool SaveData = false;
			clsDRWrapper clsSave = new clsDRWrapper();
			int X;
			int lngRow;
			int lngID = 0;
			try
			{
				// On Error GoTo ErrorHandler
				SaveData = false;
				// save return address info in DISCOnnectioninfo
				clsSave.OpenRecordset("select * from disconnectioninfo", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					clsSave.AddNew();
				}
				else
				{
					clsSave.Edit();
				}
				clsSave.Set_Fields("name", txtName.Text);
				clsSave.Set_Fields("address1", txtAddress1.Text);
				clsSave.Set_Fields("address2", txtAddress2.Text);
				clsSave.Set_Fields("address3", txtAddress3.Text);
				clsSave.Set_Fields("address4", txtAddress4.Text);
				clsSave.Update();
				// save default billformat
				clsSave.OpenRecordset("SELECT * from utilitybilling", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					clsSave.AddNew();
				}
				else
				{
					clsSave.Edit();
				}
				clsSave.Set_Fields("defaultbillformat", cmbBill.ItemData(cmbBill.SelectedIndex));
				clsSave.Update();
				lngBillIDToReturn = cmbBill.ItemData(cmbBill.SelectedIndex);
				// save messages
				// take care of deleted ones first
				for (X = 0; X <= GridDelete.Rows - 1; X++)
				{
					clsSave.Execute("delete from custombillmessages where ID = " + FCConvert.ToString(Conversion.Val(GridDelete.TextMatrix(X, 0))), modExtraModules.strUTDatabase);
				}
				// x
				GridDelete.Rows = 0;
				// now save changes and add new messages
				lngRow = GridMessage.FindRow(true);
				while (lngRow > 0)
				{
					lngID = FCConvert.ToInt32(Math.Round(Conversion.Val(GridMessage.TextMatrix(lngRow, CNSTGRIDMESSAGECOLAUTOID))));
					clsSave.OpenRecordset("select * from custombillmessages where ID = " + FCConvert.ToString(lngID), modExtraModules.strUTDatabase);
					if (!clsSave.EndOfFile())
					{
						clsSave.Edit();
					}
					else
					{
						clsSave.AddNew();
						GridMessage.TextMatrix(lngRow, CNSTGRIDMESSAGECOLAUTOID, FCConvert.ToString(clsSave.Get_Fields_Int32("ID")));
					}
					clsSave.Set_Fields("messagenum", FCConvert.ToString(Conversion.Val(GridMessage.TextMatrix(lngRow, CNSTGRIDMESSAGECOLMESSAGENUM))));
					if (GridMessage.TextMatrix(lngRow, CNSTGRIDMESSAGECOLAUTOPOP) == string.Empty)
					{
						clsSave.Set_Fields("autoprompt", false);
					}
					else
					{
						clsSave.Set_Fields("autoprompt", FCConvert.CBool(GridMessage.TextMatrix(lngRow, CNSTGRIDMESSAGECOLAUTOPOP)));
					}
					clsSave.Set_Fields("description", GridMessage.TextMatrix(lngRow, CNSTGRIDMESSAGECOLDESCRIPTION));
					clsSave.Set_Fields("MessageText", GridMessage.TextMatrix(lngRow, CNSTGRIDMESSAGECOLMESSAGE));
					clsSave.Update();
					if (lngRow < GridMessage.Rows - 1)
					{
						lngRow = GridMessage.FindRow(true, lngRow + 1);
					}
					else
					{
						lngRow = -1;
					}
				}
				MessageBox.Show("Save Complete", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
				SaveData = true;
				return SaveData;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In SaveData", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return SaveData;
		}

		private void FillControls()
		{
			clsDRWrapper clsLoad = new clsDRWrapper();
			int X;
			bool boolAddIt = false;
			cmbBill.Clear();
			clsLoad.OpenRecordset("SELECT * FROM CustomBills WHERE BillFormat > 0 ORDER BY BillFormat, FormatName", modExtraModules.strUTDatabase);
			// MAL@20070914: Added FormatName to Order by
			while (!clsLoad.EndOfFile())
			{
				boolAddIt = false;
				if (boolCombinedOnly)
				{
					// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					if (Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 1 || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 5// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					    || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 8 || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 12)
					{
						boolAddIt = true;
					}
				}
				else
				{
					// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					if (Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 2 || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 3// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					    || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 4 || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 6// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					    || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 7 || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 9// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					    || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 10 || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 11// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
					    || Conversion.Val(clsLoad.Get_Fields("BillFormat")) == 13)
					{
						boolAddIt = true;
					}
				}
				if (boolAddIt)
				{
					cmbBill.AddItem(clsLoad.Get_Fields_String("FormatName"));
					cmbBill.ItemData(cmbBill.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID")));
				}
				clsLoad.MoveNext();
			}
			clsLoad.OpenRecordset("SELECT * FROM CustomBills WHERE BillFormat = 0 ORDER BY ID, FormatName", modExtraModules.strUTDatabase);
			// MAL@20070914: Added FormatName to Order by
			while (!clsLoad.EndOfFile())
			{
				cmbBill.AddItem(clsLoad.Get_Fields_String("FormatName"));
				cmbBill.ItemData(cmbBill.NewIndex, FCConvert.ToInt32(clsLoad.Get_Fields_Int32("ID")));
				clsLoad.MoveNext();
			}
			// cmbBill.AddItem "Outprint"
			// cmbBill.ItemData(cmbBill.NewIndex) = CNSTCUSTOMBILLTYPEOUTPRINT
			clsLoad.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
			if (!clsLoad.EndOfFile())
			{
				for (X = 0; X <= cmbBill.Items.Count - 1; X++)
				{
					if (cmbBill.ItemData(X) == FCConvert.ToInt32(clsLoad.Get_Fields_String("DefaultBillFormat")))
					{
						cmbBill.SelectedIndex = X;
					}
				}
				// X
			}
			if (cmbBill.SelectedIndex < 0)
			{
				cmbBill.SelectedIndex = 0;
			}
			clsLoad.OpenRecordset("SELECT * FROM DisconnectionInfo", modExtraModules.strUTDatabase);
			if (!clsLoad.EndOfFile())
			{
				txtName.Text = FCConvert.ToString(clsLoad.Get_Fields_String("name"));
				txtAddress1.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address1"));
				txtAddress2.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address2"));
				txtAddress3.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address3"));
				txtAddress4.Text = FCConvert.ToString(clsLoad.Get_Fields_String("address4"));
			}
			clsLoad.OpenRecordset("SELECT * FROM CustomBillMessages ORDER BY MessageNum", modExtraModules.strUTDatabase);
			while (!clsLoad.EndOfFile())
			{
				GridMessage.Rows += 1;
				GridMessage.TextMatrix(GridMessage.Rows - 1, CNSTGRIDMESSAGECOLAUTOID, FCConvert.ToString(clsLoad.Get_Fields_Int32("id")));
				GridMessage.TextMatrix(GridMessage.Rows - 1, CNSTGRIDMESSAGECOLAUTOPOP, FCConvert.ToString(clsLoad.Get_Fields_Boolean("autoprompt")));
				GridMessage.TextMatrix(GridMessage.Rows - 1, CNSTGRIDMESSAGECOLMESSAGENUM, FCConvert.ToString(clsLoad.Get_Fields_Int16("messagenum")));
				GridMessage.TextMatrix(GridMessage.Rows - 1, CNSTGRIDMESSAGECOLDESCRIPTION, FCConvert.ToString(clsLoad.Get_Fields_String("description")));
				GridMessage.TextMatrix(GridMessage.Rows - 1, CNSTGRIDMESSAGECOLMESSAGE, FCConvert.ToString(clsLoad.Get_Fields_String("messagetext")));
				clsLoad.MoveNext();
			}
		}

		private void ResizeThePic()
		{
			double dblRatio = 0;
			short intpic;
			if (boolStillLoading)
				return;
			// intpic = cmbBill.ListIndex + 1
			intpic = GetItemData_2(FCConvert.ToInt16(cmbBill.SelectedIndex));
			// MAL@20070914
			intpic = GetRealPicNum(ref intpic);
			if (intpic > 11)
				intpic = 12;
			// show the custom ? image        'MAL@20070914:Changed from If intpic > 10 Then intpic = 11
			//FC:FINAL:RPU:#1088 - These were set in designer
			///BillPic.Width = Frame1.Width - 120;
			//BillPic.Height = Frame1.Height - 180;
			if (intpic == 0)
			{
			}
			else
			{
				dblRatio = (double)BillImages.Images[intpic - 1].Image.Width / BillImages.Images[intpic - 1].Image.Height;
				if (BillImages.Images[intpic - 1].Image.Height > BillImages.Images[intpic - 1].Image.Width)
				{
					//FC:FINAL:RPU:#1088 - These were set in designer
					//BillPic.Width = FCConvert.ToInt32((BillPic.Height * dblRatio));
				}
				else
				{
					//FC:FINAL:RPU:#1088 - These were set in designer
					//BillPic.Height = FCConvert.ToInt32((1 / dblRatio) * BillPic.Width);
				}
			}
		}
		// vbPorter upgrade warning: intpic As short	OnWriteFCConvert.ToInt32(
		private void SizeThePic_2(short intpic)
		{
			SizeThePic(ref intpic);
		}

		private void SizeThePic(ref short intpic)
		{
			double dblRatio;
			// intpic = intpic + 1
			intpic = GetItemData(ref intpic);
			// MAL@20070914
			intpic = GetRealPicNum(ref intpic);
			if (intpic > 11)
				intpic = 12;
			// show the custom ? image
			dblRatio = (double)BillImages.Images[intpic - 1].Image.Width / BillImages.Images[intpic - 1].Image.Height;
			if (BillImages.Images[intpic - 1].Image.Height > BillImages.Images[intpic - 1].Image.Width)
			{
				//FC:FINAL:RPU:#1088 - These were set in designer
				//BillPic.HeightOriginal = Frame1.HeightOriginal - 180;
				//BillPic.WidthOriginal = FCConvert.ToInt32((BillPic.HeightOriginal * dblRatio));
				BillPic.Image = BillImages.Images[intpic - 1];
			}
			else
			{
				//FC:FINAL:RPU:#1088 - These were set in designer
				//BillPic.WidthOriginal = Frame1.WidthOriginal - 120;
				BillPic.Image = BillImages.Images[intpic - 1];
				// BillPic.Width = dblRatio * BillPic.Height
				//BillPic.HeightOriginal = FCConvert.ToInt32((1 / dblRatio) * BillPic.WidthOriginal);
				if (BillPic.HeightOriginal > Frame1.HeightOriginal - 180)
				{
					//BillPic.HeightOriginal = Frame1.HeightOriginal - 180;
					//BillPic.WidthOriginal = FCConvert.ToInt32(BillPic.HeightOriginal * dblRatio);
				}
			}
		}
		// vbPorter upgrade warning: intPicNum As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetRealPicNum(ref short intPicNum)
		{
			short GetRealPicNum = 0;
			// MAL@20070914: Changed the way picture index is retrieved
			// Call Reference: 117154
			int intReturn = 0;
			int intTemp = 0;
			clsDRWrapper rsCustom = new clsDRWrapper();
			rsCustom.OpenRecordset("SELECT BillFormat FROM CustomBills WHERE ID = " + FCConvert.ToString(intPicNum), modExtraModules.strUTDatabase);
			if (rsCustom.RecordCount() > 0)
			{
				// TODO Get_Fields: Check the table for the column [BillFormat] and replace with corresponding Get_Field method
				intTemp = FCConvert.ToInt32(rsCustom.Get_Fields("BillFormat"));
			}
			else
			{
				intTemp = 0;
			}
			switch (intTemp)
			{
				case 0:
					{
						intReturn = 12;
						break;
					}
				default:
					{
						intReturn = intTemp;
						break;
					}
			}
			//end switch
			// 12 is the ? picture
			// If boolCombinedOnly Then
			// Select Case intPicNum
			// Select Case intTemp
			// Case 1
			// intReturn = 1
			// Case 2
			// intReturn = 5
			// Case 3
			// intReturn = 8
			// Case 4
			// intReturn = 12
			// Case Else
			// intReturn = 12
			// End Select
			// Else
			// Select Case intPicNum
			// Select Case intTemp
			// Case 1
			// intReturn = 2
			// Case 2
			// intReturn = 3
			// Case 3
			// intReturn = 4
			// Case 4
			// intReturn = 6
			// Case 5
			// intReturn = 7
			// Case 6
			// intReturn = 9
			// Case 7
			// intReturn = 10
			// Case 8
			// intReturn = 11
			// Case Else
			// intReturn = 12
			// End Select
			// End If
			GetRealPicNum = FCConvert.ToInt16(intReturn);
			return GetRealPicNum;
		}
		// vbPorter upgrade warning: intIndex As short	OnWriteFCConvert.ToInt32(
		// vbPorter upgrade warning: 'Return' As short	OnWriteFCConvert.ToInt32(
		private short GetItemData_2(short intIndex)
		{
			return GetItemData(ref intIndex);
		}

		private short GetItemData(ref short intIndex)
		{
			short GetItemData = 0;
			// MAL@20070914: Returns AutoID for Currently Selected List Item
			int intReturn;
			intReturn = cmbBill.ItemData(intIndex);
			GetItemData = FCConvert.ToInt16(intReturn);
			return GetItemData;
		}
	}
}
