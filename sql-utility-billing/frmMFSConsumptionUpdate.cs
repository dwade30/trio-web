﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using fecherFoundation.DataBaseLayer;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmMFSConsumptionUpdate.
	/// </summary>
	public partial class frmMFSConsumptionUpdate : BaseForm
	{
		public frmMFSConsumptionUpdate()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmMFSConsumptionUpdate InstancePtr
		{
			get
			{
				return (frmMFSConsumptionUpdate)Sys.GetInstance(typeof(frmMFSConsumptionUpdate));
			}
		}

		protected frmMFSConsumptionUpdate _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Melissa Lamprecht       *
		// DATE           :               10/16/2007              *
		// *
		// MODIFIED BY    :               Melissa Lamprecht       *
		// LAST UPDATED   :               10/16/2007              *
		// ********************************************************
		int lngColKey;
		int lngColAccount;
		int lngColDate;
		int lngColReading;
		int lngColConsumption;
		int lngColPrevReading;
		int lngRowCurrent;
		int lngRowPrevious;
		int lngRowPrevious2;
		int lngRowPrevious3;
		int lngRowPrevious4;
		bool blnHasCurrent;
		DateTime dtCurrReadBillDate;
		DateTime dtLastReadBillDate;

		private void cboAccount_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			string strAccount = "";
			if (cboAccount.SelectedIndex != -1)
			{
				LoadMeters();
			}
			else
			{
				cboMeters.Clear();
			}
		}

		private void cboMeters_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cboMeters.SelectedIndex != -1)
			{
				FormatGrid();
				fraConsumptionAmounts.Visible = true;
				FillConsumptionGrid();
				SetGridHeight();
			}
		}

		private void frmMFSConsumptionUpdate_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			switch (KeyAscii)
			{
				case Keys.Escape:
					{
						KeyAscii = (Keys)0;
						mnuExit_Click();
						break;
					}
				default:
					{
						// Do Nothing
						break;
					}
			}
			//end switch
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmMFSConsumptionUpdate_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmMFSConsumptionUpdate properties;
			//frmMFSConsumptionUpdate.ScaleWidth	= 6600;
			//frmMFSConsumptionUpdate.ScaleHeight	= 5370;
			//frmMFSConsumptionUpdate.LinkTopic	= "Form1";
			//End Unmaped Properties
			try
			{
				// On Error GoTo ErrorHandler
				lngColKey = 0;
				lngColDate = 1;
				lngColReading = 2;
				lngColPrevReading = 3;
				lngColConsumption = 4;
				lngRowCurrent = 1;
				lngRowPrevious = 2;
				lngRowPrevious2 = 3;
				lngRowPrevious3 = 4;
				lngRowPrevious4 = 5;
				LoadAccounts();
				cboMeters.Clear();
				modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
				modGlobalFunctions.SetTRIOColors(this);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Form_Load", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void frmMFSConsumptionUpdate_Resize(object sender, System.EventArgs e)
		{
			SetGridHeight();
			FormatGrid();
		}

		private void FormatGrid()
		{
			vsGrid.Cols = 5;
			vsGrid.ColHidden(lngColKey, true);
			vsGrid.ColHidden(lngColPrevReading, true);
			vsGrid.TextMatrix(0, lngColKey, "Type");
			vsGrid.TextMatrix(0, lngColDate, "Read/Bill Date");
			vsGrid.TextMatrix(0, lngColReading, "Reading");
			vsGrid.TextMatrix(0, lngColPrevReading, "Prev Reading");
			vsGrid.TextMatrix(0, lngColConsumption, "Consumption");
			vsGrid.ColAlignment(lngColReading, FCGrid.AlignmentSettings.flexAlignRightCenter);
			vsGrid.ColWidth(lngColKey, 0);
			vsGrid.ColWidth(lngColDate, FCConvert.ToInt32(vsGrid.Width * 0.33));
			vsGrid.ColWidth(lngColReading, FCConvert.ToInt32(vsGrid.Width * 0.33));
			vsGrid.ColWidth(lngColPrevReading, 0);
			vsGrid.ColWidth(lngColConsumption, FCConvert.ToInt32(vsGrid.Width * 0.33));
		}

		private void LoadAccounts()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			rsInfo.OpenRecordset("SELECT * FROM (SELECT Master.ID, AccountNumber, pBill.FullNameLF AS Name FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID) AS qMstr ORDER By AccountNumber");
			cboAccount.Clear();
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
					cboAccount.AddItem(Strings.Format(rsInfo.Get_Fields("AccountNumber"), "000000") + " - " + rsInfo.Get_Fields_String("Name"));
					cboAccount.ItemData(cboAccount.NewIndex, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
		}

		private void LoadMeters()
		{
			clsDRWrapper rsInfo = new clsDRWrapper();
			int lngIndex = 0;
			int lngMeter = 0;
			rsInfo.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(cboAccount.ItemData(cboAccount.SelectedIndex)) + " ORDER BY AccountKey");
			cboMeters.Clear();
			if (rsInfo.RecordCount() > 0)
			{
				while (!rsInfo.EndOfFile())
				{
					cboMeters.AddItem(FCConvert.ToString(rsInfo.Get_Fields_Int32("MeterNumber")));
					cboMeters.ItemData(cboMeters.NewIndex, FCConvert.ToInt32(rsInfo.Get_Fields_Int32("ID")));
					lngMeter = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("MeterNumber"));
					rsInfo.MoveNext();
				}
			}
			if (cboMeters.Items.Count == 1)
			{
				lngIndex = GetListIndex(lngMeter);
				if (lngIndex != -1)
				{
					cboMeters.SelectedIndex = lngIndex;
				}
			}
		}

		private void mnuAccountSearch_Click(object sender, System.EventArgs e)
		{
			cmdAccountSearch.Enabled = false;
			mnuProcess.Enabled = false;
			fraAccountSearch.Top = fraConsumptionAmounts.Top;
			fraAccountSearch.Left = FCConvert.ToInt32((this.Width - fraAccountSearch.Width) / 2.0);
			fraConsumptionAmounts.Visible = false;
			fraAccountSearch.Visible = true;
			if (txtSearchName.Enabled && txtSearchName.Visible)
			{
				txtSearchName.Focus();
			}
			txtSearchName.Text = "";
			txtSearchAccount.Text = "";
			vsAccountSearch.Visible = false;
			vsAccountSearch.Rows = 1;
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		public void Init()
		{
			try
			{
				// On Error GoTo ErrorHandler
				this.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In Init", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private object FillConsumptionGrid()
		{
			object FillConsumptionGrid = null;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsGrid = new clsDRWrapper();
				bool blnHasData = false;
				int intCount = 0;
				int intReadings = 0;
				int[] lngReading = new int[5 + 1];
				int[] lngPrevRead = new int[5 + 1];
				int[] lngConsumption = new int[5 + 1];
				DateTime[] dtDate = new DateTime[5 + 1];
				int lngPrev;
				int lngCurr;
				int lngAdj;
				int lngDigits;
				vsGrid.Rows = 6;
				rsGrid.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(cboMeters.ItemData(cboMeters.SelectedIndex)), modExtraModules.strUTDatabase);
				if (rsGrid.RecordCount() > 0)
				{
					blnHasData = true;
					if (FCConvert.ToInt32(rsGrid.Get_Fields_Int32("CurrentReading")) != -1)
					{
						// kk 110812 trout-884  Change from 0 to -1 for No Read
						blnHasCurrent = true;
						dtDate[intCount] = (DateTime)rsGrid.Get_Fields_DateTime("CurrentReadingDate");
						lngReading[intCount] = FCConvert.ToInt32(rsGrid.Get_Fields_Int32("CurrentReading"));
						lngPrevRead[intCount] = FCConvert.ToInt32(rsGrid.Get_Fields_Int32("PreviousReading"));
						lngConsumption[intCount] = (lngReading[intCount] - lngPrevRead[intCount]);
						intCount += 1;
						intReadings += 1;
					}
					else
					{
						blnHasCurrent = false;
					}
				}
				rsGrid.OpenRecordset("SELECT * FROM MeterConsumption WHERE AccountKey = " + FCConvert.ToString(cboAccount.ItemData(cboAccount.SelectedIndex)) + " AND MeterKey = " + FCConvert.ToString(cboMeters.ItemData(cboMeters.SelectedIndex)) + " ORDER BY BillDate DESC");
				if (rsGrid.RecordCount() > 0)
				{
					blnHasData = true;
					while (!rsGrid.EndOfFile())
					{
						if (intReadings < 5)
						{
							if (blnHasCurrent && rsGrid.Get_Fields_DateTime("BillDate").ToOADate() > dtDate[0].ToOADate())
							{
								// Current Record is the same as the Current Reading Record
								// Store Bill Date for Use Later
								dtCurrReadBillDate = (DateTime)rsGrid.Get_Fields_DateTime("BillDate");
							}
							else
							{
								dtDate[intCount] = (DateTime)rsGrid.Get_Fields_DateTime("BillDate");
								// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
								lngReading[intCount] = FCConvert.ToInt32(rsGrid.Get_Fields("End"));
								lngPrevRead[intCount] = FCConvert.ToInt32(rsGrid.Get_Fields_Int32("Begin"));
								lngConsumption[intCount] = FCConvert.ToInt32(rsGrid.Get_Fields_Int32("Consumption"));
								intCount += 1;
								intReadings += 1;
							}
						}
						else if (intReadings == 5)
						{
							dtLastReadBillDate = (DateTime)rsGrid.Get_Fields_DateTime("BillDate");
							intReadings += 1;
						}
						else
						{
							break;
						}
						rsGrid.MoveNext();
					}
				}
				if (blnHasData)
				{
					if (blnHasCurrent)
					{
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowCurrent, lngColKey, "C");
						vsGrid.TextMatrix(lngRowCurrent, lngColDate, FCConvert.ToString(dtDate[lngRowCurrent - 1]));
						vsGrid.TextMatrix(lngRowCurrent, lngColReading, FCConvert.ToString(lngReading[lngRowCurrent - 1]));
						vsGrid.TextMatrix(lngRowCurrent, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowCurrent - 1]));
						vsGrid.TextMatrix(lngRowCurrent, lngColConsumption, FCConvert.ToString(lngConsumption[lngRowCurrent - 1]));
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowPrevious, lngColKey, "P");
						vsGrid.TextMatrix(lngRowPrevious, lngColDate, FCConvert.ToString(dtDate[lngRowPrevious - 1]));
						vsGrid.TextMatrix(lngRowPrevious, lngColReading, FCConvert.ToString(lngReading[lngRowPrevious - 1]));
						vsGrid.TextMatrix(lngRowPrevious, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowPrevious - 1]));
						vsGrid.TextMatrix(lngRowPrevious, lngColConsumption, FCConvert.ToString(lngConsumption[lngRowPrevious - 1]));
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowPrevious2, lngColKey, "P2");
						vsGrid.TextMatrix(lngRowPrevious2, lngColDate, FCConvert.ToString(dtDate[lngRowPrevious2 - 1]));
						vsGrid.TextMatrix(lngRowPrevious2, lngColReading, FCConvert.ToString(lngReading[lngRowPrevious2 - 1]));
						vsGrid.TextMatrix(lngRowPrevious2, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowPrevious2 - 1]));
						vsGrid.TextMatrix(lngRowPrevious2, lngColConsumption, FCConvert.ToString(lngConsumption[lngRowPrevious2 - 1]));
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowPrevious3, lngColKey, "P3");
						vsGrid.TextMatrix(lngRowPrevious3, lngColDate, FCConvert.ToString(dtDate[lngRowPrevious3 - 1]));
						vsGrid.TextMatrix(lngRowPrevious3, lngColReading, FCConvert.ToString(lngReading[lngRowPrevious3 - 1]));
						vsGrid.TextMatrix(lngRowPrevious3, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowPrevious3 - 1]));
						vsGrid.TextMatrix(lngRowPrevious3, lngColConsumption, FCConvert.ToString(lngConsumption[lngRowPrevious3 - 1]));
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowPrevious4, lngColKey, "P4");
						vsGrid.TextMatrix(lngRowPrevious4, lngColDate, FCConvert.ToString(dtDate[lngRowPrevious4 - 1]));
						vsGrid.TextMatrix(lngRowPrevious4, lngColReading, FCConvert.ToString(lngReading[lngRowPrevious4 - 1]));
						vsGrid.TextMatrix(lngRowPrevious4, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowPrevious4 - 1]));
						vsGrid.TextMatrix(lngRowPrevious4, lngColConsumption, "");
					}
					else
					{
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowCurrent, lngColKey, "P");
						vsGrid.TextMatrix(lngRowCurrent, lngColDate, FCConvert.ToString(dtDate[lngRowCurrent - 1]));
						vsGrid.TextMatrix(lngRowCurrent, lngColReading, FCConvert.ToString(lngReading[lngRowCurrent - 1]));
						vsGrid.TextMatrix(lngRowCurrent, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowCurrent - 1]));
						vsGrid.TextMatrix(lngRowCurrent, lngColConsumption, FCConvert.ToString(lngConsumption[lngRowCurrent - 1]));
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowPrevious, lngColKey, "P2");
						vsGrid.TextMatrix(lngRowPrevious, lngColDate, FCConvert.ToString(dtDate[lngRowPrevious - 1]));
						vsGrid.TextMatrix(lngRowPrevious, lngColReading, FCConvert.ToString(lngReading[lngRowPrevious - 1]));
						vsGrid.TextMatrix(lngRowPrevious, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowPrevious - 1]));
						vsGrid.TextMatrix(lngRowPrevious, lngColConsumption, FCConvert.ToString(lngConsumption[lngRowPrevious - 1]));
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowPrevious2, lngColKey, "P3");
						vsGrid.TextMatrix(lngRowPrevious2, lngColDate, FCConvert.ToString(dtDate[lngRowPrevious2 - 1]));
						vsGrid.TextMatrix(lngRowPrevious2, lngColReading, FCConvert.ToString(lngReading[lngRowPrevious2 - 1]));
						vsGrid.TextMatrix(lngRowPrevious2, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowPrevious2 - 1]));
						vsGrid.TextMatrix(lngRowPrevious2, lngColConsumption, FCConvert.ToString(lngConsumption[lngRowPrevious2 - 1]));
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowPrevious3, lngColKey, "P4");
						vsGrid.TextMatrix(lngRowPrevious3, lngColDate, FCConvert.ToString(dtDate[lngRowPrevious3 - 1]));
						vsGrid.TextMatrix(lngRowPrevious3, lngColReading, FCConvert.ToString(lngReading[lngRowPrevious3 - 1]));
						vsGrid.TextMatrix(lngRowPrevious3, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowPrevious3 - 1]));
						vsGrid.TextMatrix(lngRowPrevious3, lngColConsumption, FCConvert.ToString(lngConsumption[lngRowPrevious3 - 1]));
						vsGrid.AddItem("");
						vsGrid.TextMatrix(lngRowPrevious4, lngColKey, "P5");
						vsGrid.TextMatrix(lngRowPrevious4, lngColDate, FCConvert.ToString(dtDate[lngRowPrevious4 - 1]));
						vsGrid.TextMatrix(lngRowPrevious4, lngColReading, FCConvert.ToString(lngReading[lngRowPrevious4 - 1]));
						vsGrid.TextMatrix(lngRowPrevious4, lngColPrevReading, FCConvert.ToString(lngPrevRead[lngRowPrevious4 - 1]));
						vsGrid.TextMatrix(lngRowPrevious4, lngColConsumption, "");
					}
				}
				else
				{
					MessageBox.Show("There is no consumption data for this meter", "No Data", MessageBoxButtons.OK, MessageBoxIcon.Information);
					fraConsumptionAmounts.Visible = false;
				}
				return FillConsumptionGrid;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
			return FillConsumptionGrid;
		}

		private void SetGridHeight()
		{
			// set the height
			int lngHt;
			int lngW;
			lngW = vsGrid.Width;
			lngHt = fraConsumptionAmounts.Height - (2 * vsGrid.Top);
			if (lngHt <= (vsGrid.RowHeight(0) * vsGrid.Rows) + 70)
			{
				vsGrid.Height = lngHt;
			}
			else
			{
				vsGrid.Height = (vsGrid.RowHeight(0) * vsGrid.Rows) + 70;
			}
			vsGrid.ColWidth(lngColKey, 0);
			vsGrid.ColWidth(lngColAccount, FCConvert.ToInt32(lngW * 0.05));
		}

		private void mnuProcess_Click(object sender, System.EventArgs e)
		{
			int intRow;
			int lngCT;
			int lngAmount;
			int lngKey;
			// vbPorter upgrade warning: lngMNum As int	OnWrite(string)
			int lngMNum;
			int lngMeter;
			string strCurrAmt = "";
			string strPAmt = "";
			string strP2Amt = "";
			string strP3Amt = "";
			string strP4Amt = "";
			string strP5Amt = "";
			int lngCKey = 0;
			int lngPKey = 0;
			int lngP2Key = 0;
			int lngP3Key = 0;
			int lngP4Key = 0;
			int lngP5Key;
			clsDRWrapper rsCurrent = new clsDRWrapper();
			clsDRWrapper rsHistory = new clsDRWrapper();
			lngKey = cboAccount.ItemData(cboAccount.SelectedIndex);
			lngMeter = cboMeters.ItemData(cboMeters.SelectedIndex);
			lngMNum = FCConvert.ToInt32(cboMeters.Items[cboMeters.SelectedIndex].ToString());
			rsCurrent.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(lngKey) + " AND MeterNumber = " + FCConvert.ToString(lngMNum), modExtraModules.strUTDatabase);
			rsHistory.OpenRecordset("SELECT * FROM MeterConsumption WHERE AccountKey = " + FCConvert.ToString(lngKey) + " AND MeterNumber = " + FCConvert.ToString(lngMNum) + " ORDER BY BillDate Desc", modExtraModules.strUTDatabase);
			for (lngCT = 1; lngCT <= vsGrid.Rows - 1; lngCT++)
			{
				string vbPorterVar = vsGrid.TextMatrix(lngCT, lngColKey);
				if (vbPorterVar == "C")
				{
					strCurrAmt = vsGrid.TextMatrix(lngCT, lngColReading);
					rsCurrent.Edit();
					rsCurrent.Set_Fields("CurrentReading", vsGrid.TextMatrix(lngCT, lngColReading));
					rsCurrent.Update();
					rsHistory.FindFirstRecord("BillDate", dtCurrReadBillDate);
					if (!rsHistory.NoMatch)
					{
						lngCKey = FCConvert.ToInt32(rsHistory.Get_Fields_Int32("ID"));
						rsHistory.Edit();
						rsHistory.Set_Fields("End", vsGrid.TextMatrix(lngCT, lngColReading));
						// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
						rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
						rsHistory.Update();
					}
				}
				else if (vbPorterVar == "P")
				{
					strPAmt = vsGrid.TextMatrix(lngCT, lngColReading);
					rsCurrent.Edit();
					rsCurrent.Set_Fields("PreviousReading", vsGrid.TextMatrix(lngCT, lngColReading));
					rsCurrent.Update();
					rsHistory.FindFirstRecord("BillDate", vsGrid.TextMatrix(lngCT, lngColDate));
					if (!rsHistory.NoMatch)
					{
						lngPKey = FCConvert.ToInt32(rsHistory.Get_Fields_Int32("ID"));
						rsHistory.Edit();
						rsHistory.Set_Fields("End", vsGrid.TextMatrix(lngCT, lngColReading));
						// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
						rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
						rsHistory.Update();
					}
					if (lngCKey != 0)
					{
						rsHistory.FindFirstRecord("ID", lngCKey);
						if (!rsHistory.NoMatch)
						{
							rsHistory.Edit();
							rsHistory.Set_Fields("Begin", strPAmt);
							// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
							rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
							rsHistory.Update();
						}
					}
				}
				else if (vbPorterVar == "P2")
				{
					strP2Amt = vsGrid.TextMatrix(lngCT, lngColReading);
					rsHistory.FindFirstRecord("BillDate", vsGrid.TextMatrix(lngCT, lngColDate));
					if (!rsHistory.NoMatch)
					{
						lngP2Key = FCConvert.ToInt32(rsHistory.Get_Fields_Int32("ID"));
						rsHistory.Edit();
						rsHistory.Set_Fields("End", vsGrid.TextMatrix(lngCT, lngColReading));
						// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
						rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
						rsHistory.Update();
					}
					if (lngPKey != 0)
					{
						rsHistory.FindFirstRecord("ID", lngPKey);
						if (!rsHistory.NoMatch)
						{
							rsHistory.Edit();
							rsHistory.Set_Fields("Begin", strP2Amt);
							// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
							rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
							rsHistory.Update();
						}
					}
				}
				else if (vbPorterVar == "P3")
				{
					strP3Amt = vsGrid.TextMatrix(lngCT, lngColReading);
					rsHistory.FindFirstRecord("BillDate", vsGrid.TextMatrix(lngCT, lngColDate));
					if (!rsHistory.NoMatch)
					{
						lngP3Key = FCConvert.ToInt32(rsHistory.Get_Fields_Int32("ID"));
						rsHistory.Edit();
						rsHistory.Set_Fields("End", vsGrid.TextMatrix(lngCT, lngColReading));
						// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
						rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
						rsHistory.Update();
					}
					if (lngP2Key != 0)
					{
						rsHistory.FindFirstRecord("ID", lngP2Key);
						if (!rsHistory.NoMatch)
						{
							rsHistory.Edit();
							rsHistory.Set_Fields("Begin", strP3Amt);
							// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
							rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
							rsHistory.Update();
						}
					}
					// If blnHasCurrent = True Then        'This is the last record
					// If lngP3Key <> 0 Then
					// rsHistory.FindFirstRecord , , "BillDate = #" & dtLastReadBillDate & "#"
					// If Not rsHistory.NoMatch Then
					// rsHistory.Edit
					// rsHistory.Fields("Begin") = strP3Amt
					// rsHistory.Fields("Consumption") = (rsHistory.Fields("End") - rsHistory.Fields("Begin"))
					// rsHistory.Update
					// End If
					// End If
					// End If
				}
				else if (vbPorterVar == "P4")
				{
					strP4Amt = vsGrid.TextMatrix(lngCT, lngColReading);
					rsHistory.FindFirstRecord("BillDate", vsGrid.TextMatrix(lngCT, lngColDate));
					if (!rsHistory.NoMatch)
					{
						lngP4Key = FCConvert.ToInt32(rsHistory.Get_Fields_Int32("ID"));
						rsHistory.Edit();
						rsHistory.Set_Fields("End", vsGrid.TextMatrix(lngCT, lngColReading));
						// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
						rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
						rsHistory.Update();
					}
					if (lngP3Key != 0)
					{
						rsHistory.FindFirstRecord("ID", lngP3Key);
						if (!rsHistory.NoMatch)
						{
							rsHistory.Edit();
							rsHistory.Set_Fields("Begin", strP4Amt);
							// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
							rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
							rsHistory.Update();
						}
					}
				}
				else if (vbPorterVar == "P5")
				{
					strP5Amt = vsGrid.TextMatrix(lngCT, lngColReading);
					rsHistory.FindFirstRecord("BillDate", vsGrid.TextMatrix(lngCT, lngColDate));
					if (!rsHistory.NoMatch)
					{
						lngP5Key = FCConvert.ToInt32(rsHistory.Get_Fields_Int32("ID"));
						rsHistory.Edit();
						rsHistory.Set_Fields("End", vsGrid.TextMatrix(lngCT, lngColReading));
						// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
						rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
						rsHistory.Update();
					}
					if (lngP4Key != 0)
					{
						rsHistory.FindFirstRecord("ID", lngP4Key);
						if (!rsHistory.NoMatch)
						{
							rsHistory.Edit();
							rsHistory.Set_Fields("Begin", strP5Amt);
							// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
							rsHistory.Set_Fields("Consumption", (rsHistory.Get_Fields("End") - rsHistory.Get_Fields_Int32("Begin")));
							rsHistory.Update();
						}
					}
				}
				else
				{
					// Do Nothing
				}
			}
			// lngCT
			MessageBox.Show("Save Successful", "Saved", MessageBoxButtons.OK, MessageBoxIcon.Information);
			this.Unload();
		}

		private void vsAccountSearch_DblClick(object sender, System.EventArgs e)
		{
			// selecting an account to use
			int lngRW;
			int lngAccount;
			int lngIndex;
			lngRW = vsAccountSearch.MouseRow;
			//Application.DoEvents();
			lngAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(vsAccountSearch.TextMatrix(lngRW, 0))));
			lngIndex = GetListIndex(lngAccount);
			ReturnFromSearch(ref lngIndex);
		}

		private void SearchForAccounts()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsSearch = new clsDRWrapper();
				string strSQL = "";
				string strWhere = "";
				string strName = "";
				string strAccount = "";
				int lngIndex = 0;
				int lngMousePos;
				FormatSearchGrid();
				if (Strings.Trim(txtSearchName.Text) != "")
				{
					strName = "Name > '" + Strings.Trim(txtSearchName.Text) + "     ' AND Name < '" + Strings.Trim(txtSearchName.Text) + "ZZZZZ'";
				}
				else
				{
					strName = "";
				}
				if (Strings.Trim(txtSearchAccount.Text) != "")
				{
					strAccount = "AccountNumber LIKE '%" + Strings.Trim(txtSearchAccount.Text) + "%'";
				}
				else
				{
					strAccount = "";
				}
				if (strName != "")
				{
					strWhere = strName;
				}
				if (strAccount != "")
				{
					if (strWhere != "")
					{
						strWhere += " AND " + strAccount;
					}
					else
					{
						strWhere = strAccount;
					}
				}
				rsSearch.OpenRecordset("SELECT * FROM (SELECT Master.ID, AccountNumber, pBill.FullNameLF AS Name FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID) AS qMstr WHERE " + strWhere + " ORDER BY AccountNumber");
				if (rsSearch.EndOfFile())
				{
					// no accounts
					MessageBox.Show("No accounts match the search criteria.", "No Matches", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
				else
				{
					if (rsSearch.RecordCount() == 1)
					{
						// Automatically populate with a single find
						lngIndex = GetListIndex(rsSearch.Get_Fields_Int32("ID"));
						ReturnFromSearch(ref lngIndex);
					}
					else
					{
						vsAccountSearch.Rows = 1;
						while (!rsSearch.EndOfFile())
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							vsAccountSearch.AddItem(rsSearch.Get_Fields_Int32("ID") + "\t" + rsSearch.Get_Fields("AccountNumber") + "\t" + rsSearch.Get_Fields_String("Name"));
							rsSearch.MoveNext();
						}
						vsAccountSearch.Visible = true;
						// resize the grid
						SetSearchGridHeight();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Searching For Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int MakeDWord(ref short LoWord, ref short HiWord)
		{
			int MakeDWord = 0;
			MakeDWord = (HiWord * 0x10000) | (LoWord & 0xFFFF);
			return MakeDWord;
		}

		private void FormatSearchGrid()
		{
			vsAccountSearch.Cols = 3;
			vsAccountSearch.ColWidth(0, 0);
			// Account Key
			vsAccountSearch.ColWidth(1, FCConvert.ToInt32(vsAccountSearch.Width * 0.25));
			// account number
			vsAccountSearch.ColWidth(2, FCConvert.ToInt32(vsAccountSearch.Width * 0.5));
			// name
			vsAccountSearch.ExtendLastCol = true;
			vsAccountSearch.TextMatrix(0, 1, "Account");
			vsAccountSearch.TextMatrix(0, 2, "Name");
			vsAccountSearch.ExplorerBar = FCGrid.ExplorerBarSettings.flexExSort;
		}

		private void SetSearchGridHeight()
		{
			if ((vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70 > fraAccountSearch.Height * 0.7)
			{
				// too many rows to be shown
				vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
				vsAccountSearch.Height = FCConvert.ToInt32(fraAccountSearch.Height * 0.7);
			}
			else
			{
				vsAccountSearch.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollNone;
				vsAccountSearch.Height = (vsAccountSearch.Rows * vsAccountSearch.RowHeight(0)) + 70;
			}
		}

		private void txtSearchName_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						SearchForAccounts();
						break;
					}
			}
			//end switch
		}

		private void txtSearchAccount_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Return:
					{
						KeyCode = (Keys)0;
						SearchForAccounts();
						break;
					}
			}
			//end switch
		}

		private void ReturnFromSearch(ref int lngIndex)
		{
			fraAccountSearch.Visible = false;
			fraConsumptionAmounts.Visible = true;
			mnuProcess.Enabled = true;
			cmdAccountSearch.Enabled = true;
			if (lngIndex == -1)
			{
				// No Account Selected
				cboAccount.Focus();
			}
			else
			{
				cboAccount.SelectedIndex = lngIndex;
			}
		}

		private int GetListIndex(int lngKey)
		{
			int GetListIndex = 0;
			// MAL@20070907: Gets List Index Based on Account Key
			FCRecordset rsTemp = null;
			int lngResult = 0;
			int intCount;
			int i;
			intCount = cboAccount.Items.Count;
			for (i = 0; i <= intCount - 1; i++)
			{
				if (cboAccount.ItemData(i) == lngKey)
				{
					lngResult = i;
					break;
				}
			}
			// i
			GetListIndex = lngResult;
			return GetListIndex;
		}

		private void vsGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			// Update Consumption Amount when Reading Changes
			vsGrid.TextMatrix(lngRowCurrent, lngColConsumption, FCConvert.ToString((Conversion.Val(vsGrid.TextMatrix(lngRowCurrent, lngColReading)) - Conversion.Val(vsGrid.TextMatrix(lngRowPrevious, lngColReading)))));
			vsGrid.TextMatrix(lngRowPrevious, lngColConsumption, FCConvert.ToString((Conversion.Val(vsGrid.TextMatrix(lngRowPrevious, lngColReading)) - Conversion.Val(vsGrid.TextMatrix(lngRowPrevious2, lngColReading)))));
			vsGrid.TextMatrix(lngRowPrevious2, lngColConsumption, FCConvert.ToString((Conversion.Val(vsGrid.TextMatrix(lngRowPrevious2, lngColReading)) - Conversion.Val(vsGrid.TextMatrix(lngRowPrevious3, lngColReading)))));
			vsGrid.TextMatrix(lngRowPrevious3, lngColConsumption, FCConvert.ToString((Conversion.Val(vsGrid.TextMatrix(lngRowPrevious3, lngColReading)) - Conversion.Val(vsGrid.TextMatrix(lngRowPrevious4, lngColReading)))));
			// vsGrid.TextMatrix(lngRowPrevious4, lngColConsumption) = (Val(vsGrid.TextMatrix(lngRowPrevious4, lngColReading)) - Val(vsGrid.TextMatrix(lngRowPrevious4, lngColPrevReading)))
		}

		private void vsGrid_RowColChange(object sender, System.EventArgs e)
		{
			if (vsGrid.Col == lngColReading)
			{
				vsGrid.Editable = FCGrid.EditableSettings.flexEDKbd;
			}
			else
			{
				vsGrid.Editable = FCGrid.EditableSettings.flexEDNone;
			}
		}
	}
}
