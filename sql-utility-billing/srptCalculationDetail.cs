﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptCalculationDetail.
	/// </summary>
	public partial class srptCalculationDetail : FCSectionReport
	{
		public srptCalculationDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
		}

		public static srptCalculationDetail InstancePtr
		{
			get
			{
				return (srptCalculationDetail)Sys.GetInstance(typeof(srptCalculationDetail));
			}
		}

		protected srptCalculationDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsM.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptCalculationDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/19/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               07/27/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsM = new clsDRWrapper();
		double dblSTotal;
		double dblWTotal;
		int intType;
		int lngBK;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			intType = rptAnalysisReportsMaster.InstancePtr.intType;
			dblSTotal = 0;
			dblWTotal = 0;
			lngBK = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
			rsData.OpenRecordset(SetupSQL(), modExtraModules.strUTDatabase);
			if (rsData.EndOfFile())
			{
				this.Detail.Height = 0;
				this.GroupFooter1.Height = 0;
			}

            //FC:FINAL:AM:#3822 - moved code from group footer format (cannot create control there)
            if (!arBillingEdit.InstancePtr.boolShowSubtotals)
            {
                lnSubtotals.Visible = false;
                fldWTotal.Visible = false;
                fldSTotal.Visible = false;
                lblTotals.Visible = false;
                lblWarning.Top = 0;
            }
            rsM.OpenRecordset("SELECT AccountKey FROM Bill WHERE ID = " + FCConvert.ToString(lngBK), modExtraModules.strUTDatabase);
            if (!rsM.EndOfFile())
            {
                rsM.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsM.Get_Fields_Int32("AccountKey") + " AND MeterNumber > 1", modExtraModules.strUTDatabase);
                if (!rsM.EndOfFile())
                {
                    int lngCT = AddMeterConsumption();
                }
            }
        }

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void BindFields()
		{
			// this sub will put all of the information into the string
			if (rsData.EndOfFile() != true)
			{
				// TODO Get_Fields: Field [WAmt] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsData.Get_Fields("WAmt")) != 0)
				{
					// TODO Get_Fields: Field [WAmt] not found!! (maybe it is an alias?)
					fldWAmt.Text = Strings.Format(rsData.Get_Fields("WAmt"), "#,##0.00");
					// TODO Get_Fields: Field [WAmt] not found!! (maybe it is an alias?)
					dblWTotal += FCConvert.ToDouble(rsData.Get_Fields("WAmt"));
				}
				else
				{
					fldWAmt.Text = "0.00";
				}
				// TODO Get_Fields: Field [SAmt] not found!! (maybe it is an alias?)
				if (Conversion.Val(rsData.Get_Fields("SAmt")) != 0)
				{
					// TODO Get_Fields: Field [SAmt] not found!! (maybe it is an alias?)
					fldSAmt.Text = Strings.Format(rsData.Get_Fields("SAmt"), "#,##0.00");
					// TODO Get_Fields: Field [SAmt] not found!! (maybe it is an alias?)
					dblSTotal += FCConvert.ToDouble(rsData.Get_Fields("SAmt"));
				}
				else
				{
					fldSAmt.Text = "0.00";
				}
				// TODO Get_Fields: Field [WDescription] not found!! (maybe it is an alias?)
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("WDescription"))) != "")
				{
					// TODO Get_Fields: Field [WDescription] not found!! (maybe it is an alias?)
					lblDescription.Text = FCConvert.ToString(rsData.Get_Fields("WDescription"));
				}
				else
				{
					// TODO Get_Fields: Field [SDescription] not found!! (maybe it is an alias?)
					lblDescription.Text = FCConvert.ToString(rsData.Get_Fields("SDescription"));
				}
				rsData.MoveNext();
			}
		}

		private string SetupSQL()
		{
			string SetupSQL = "";
			// this will return the SQL statement for this batch of reports
			string strTempString;
			// this SQL statement will take the right join and union the left join so there are no duplicates
			// This is essentially a way to create an outer join which access does not allow as a function
			strTempString = "Temp";
			SetupSQL = "SELECT * FROM (";
			SetupSQL = SetupSQL + "SELECT * FROM (SELECT SUM(Amount) AS WAmt, Description AS WDescription, MeterKey AS WMeterKey FROM " + strTempString + "Breakdown WHERE BillKey = " + FCConvert.ToString(lngBK) + " AND Service = 'W' GROUP BY MeterKey, Description) AS Water";
			SetupSQL = SetupSQL + " LEFT JOIN (SELECT SUM(Amount) AS SAmt, Description AS SDescription, MeterKey AS SMeterKey FROM " + strTempString + "Breakdown WHERE BillKey = " + FCConvert.ToString(lngBK) + " AND Service = 'S' GROUP BY MeterKey, Description) AS Sewer";
			SetupSQL = SetupSQL + " ON WMeterKey = SMeterKey AND RTRIM(WDescription) = RTRIM(SDescription)";
			SetupSQL = SetupSQL + " UNION ";
			SetupSQL = SetupSQL + "SELECT * FROM (SELECT SUM(Amount) AS WAmt, Description AS WDescription, MeterKey AS WMeterKey FROM " + strTempString + "Breakdown WHERE BillKey = " + FCConvert.ToString(lngBK) + " AND Service = 'W' GROUP BY MeterKey, Description) AS Water";
			SetupSQL = SetupSQL + " RIGHT JOIN (SELECT SUM(Amount) AS SAmt, Description AS SDescription, MeterKey AS SMeterKey FROM " + strTempString + "Breakdown WHERE BillKey = " + FCConvert.ToString(lngBK) + " AND Service = 'S' GROUP BY MeterKey, Description) AS Sewer";
			SetupSQL = SetupSQL + " ON WMeterKey = SMeterKey AND RTRIM(WDescription) = RTRIM(SDescription)";
			SetupSQL = SetupSQL + ") AS qTmp";
			return SetupSQL;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//int lngCT = 0;
				fldSTotal.Text = Strings.Format(dblSTotal, "#,##0.00");
				fldWTotal.Text = Strings.Format(dblWTotal, "#,##0.00");
				lblWarning.Text = arBillingEdit.InstancePtr.GetWarningMessage(ref lngBK);
                if (!arBillingEdit.InstancePtr.boolShowSubtotals)
                {
                    lnSubtotals.Visible = false;
                    fldWTotal.Visible = false;
                    fldSTotal.Visible = false;
                    lblTotals.Visible = false;
                    lblWarning.Top = 0;
                }
                rsM.OpenRecordset("SELECT AccountKey FROM Bill WHERE ID = " + FCConvert.ToString(lngBK), modExtraModules.strUTDatabase);
                if (!rsM.EndOfFile())
                {
                    rsM.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsM.Get_Fields_Int32("AccountKey") + " AND MeterNumber > 1", modExtraModules.strUTDatabase);
                    if (!rsM.EndOfFile())
                    {
                        //FC:FINAL:SBE - #3822 - move only control creation to ReportStart event
                        //lngCT = AddMeterConsumption();
                        int lngCT = rsM.RecordCount();
                        if (Strings.Trim(lblWarning.Text) == "")
                        {
                            lblWarning.Visible = false;
                            lblWarning.Top = 0;
                        }
                        else
                        {
                            lblWarning.Visible = true;
                        }
                        this.GroupFooter1.Height = lblWarning.Top + lblWarning.Height + ((lngCT + 2) * 250) / 1440;
                    }
                    else
                    {
                        if (Strings.Trim(lblWarning.Text) == "")
                        {
                            lblWarning.Visible = false;
                            lblWarning.Top = 0;
                        }
                        else
                        {
                            lblWarning.Visible = true;
                            this.GroupFooter1.Height = lblWarning.Top + lblWarning.Height;
                        }
                    }
                }
                else
                {
                    if (Strings.Trim(lblWarning.Text) == "")
                    {
                        lblWarning.Visible = false;
                        lblWarning.Top = 0;
                    }
                    else
                    {
                        lblWarning.Visible = true;
                        this.GroupFooter1.Height = lblWarning.Top + lblWarning.Height;
                    }
                }
                return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Group Footer", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private int AddMeterConsumption()
		{
			int AddMeterConsumption = 0;
			try
			{
				// On Error GoTo ERROR_HANDLER
				float lngCurH = 0;
				GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
				while (!rsM.EndOfFile())
				{
					lngCurH = lblWarning.Top + lblWarning.Height + ((AddMeterConsumption) * 250) / 1440f;
					AddMeterConsumption += 1;
					// sets the Meter Number
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldMeterNum" + FCConvert.ToString(AddMeterConsumption);
					obNew.Top = lngCurH;
					obNew.Left = 0;
					obNew.Width = 540 / 1440f;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSTotal.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = "";
					// "  #" & rsM.Fields("MeterNumber")
					GroupFooter1.Controls.Add(obNew);
					// Previous
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldPrev" + FCConvert.ToString(AddMeterConsumption);
					obNew.Top = lngCurH;
					obNew.Left = 540 / 1440f;
					obNew.Width = 990 / 1440f;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSTotal.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = FCConvert.ToString(rsM.Get_Fields_Int32("PreviousReading"));
					GroupFooter1.Controls.Add(obNew);
					// Current
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldCurr" + FCConvert.ToString(AddMeterConsumption);
					obNew.Top = lngCurH;
					obNew.Left = 1530 / 1440f;
					obNew.Width = 990 / 1440f;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSTotal.Font;
					// this sets the font to the same as the field that is already created
					if (FCConvert.ToInt32(rsM.Get_Fields_Int32("CurrentReading")) != -1)
					{
						// kk 110812 trout-884  Make -1 = No Read
						obNew.Text = FCConvert.ToString(rsM.Get_Fields_Int32("CurrentReading"));
					}
					else
					{
						obNew.Text = "";
					}
					GroupFooter1.Controls.Add(obNew);
					// Consumption
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldCons" + FCConvert.ToString(AddMeterConsumption);
					obNew.Top = lngCurH;
					obNew.Left = 2430 / 1440f;
					obNew.Width = 720 / 1440f;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSTotal.Font;
					// this sets the font to the same as the field that is already created
					if (FCConvert.ToInt32(rsM.Get_Fields_Int32("CurrentReading")) != -1)
					{
						// kk 110812 trout-884  Make -1 = No Read
						obNew.Text = FCConvert.ToString(rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading"));
					}
					GroupFooter1.Controls.Add(obNew);
					// Note
					obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					obNew.Name = "fldNote" + FCConvert.ToString(AddMeterConsumption);
					obNew.Top = lngCurH;
					obNew.Left = 3330 / 1440f;
					obNew.Width = 3000 / 1440f;
					obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
					obNew.Font = fldSTotal.Font;
					// this sets the font to the same as the field that is already created
					obNew.Text = "Meter #" + rsM.Get_Fields_Int32("MeterNumber") + " is a Combined Meter.";
					GroupFooter1.Controls.Add(obNew);
					rsM.MoveNext();
				}
				return AddMeterConsumption;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Add Meter Consumption", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddMeterConsumption;
		}

		private void srptCalculationDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptCalculationDetail properties;
			//srptCalculationDetail.Icon	= "srptCalculationDetail.dsx":0000";
			//srptCalculationDetail.Left	= 0;
			//srptCalculationDetail.Top	= 0;
			//srptCalculationDetail.Width	= 11880;
			//srptCalculationDetail.Height	= 8595;
			//srptCalculationDetail.StartUpPosition	= 3;
			//srptCalculationDetail.SectionData	= "srptCalculationDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
