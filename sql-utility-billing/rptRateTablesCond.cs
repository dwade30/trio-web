﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptRateTablesCond.
	/// </summary>
	public partial class rptRateTablesCond : BaseSectionReport
	{
		public static rptRateTablesCond InstancePtr
		{
			get
			{
				return (rptRateTablesCond)Sys.GetInstance(typeof(rptRateTablesCond));
			}
		}

		protected rptRateTablesCond _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}

		public rptRateTablesCond()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Rate Table Listing";
		}
		// nObj = 1
		//   0	rptRateTablesCond	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Kevin Kelly             *
		// DATE           :               08/15/2015              *
		// *
		// MODIFIED BY    :                                       *
		// LAST UPDATED   :                                       *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			Fields.Add("Service");
			// Add field for grouping
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			if (!eArgs.EOF)
			{
				Fields["Service"].Value = rsData.Get_Fields_String("Service");
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSewerQuery;
			string strWaterQuery;
			string strSQL = "";
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lblMuni.Text = modGlobalConstants.Statics.MuniName;
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			strSewerQuery = "SELECT RateTableNumber,RateTableDescription,'Sewer' AS Service," + "MinConsS AS MinCons,MinChargeS AS MinCharge,FlatRateS AS FlatRate," + "MiscChargeS1 AS MiscCharge1,MiscChargeAmountS1 AS MiscChargeAmount1,MiscChargeAccountS1 AS MiscChargeAccount1," + "MiscChargeS2 AS MiscCharge2,MiscChargeAmountS2 AS MiscChargeAmount2,MiscChargeAccountS2 AS MiscChargeAccount2," + "SOver1 AS Over1, SThru1 AS Thru1, SRate1 AS Rate1, SStep1 AS Step1," + "SOver2 AS Over2, SThru2 AS Thru2, SRate2 AS Rate2, SStep2 AS Step2," + "SOver3 AS Over3, SThru3 AS Thru3, SRate3 AS Rate3, SStep3 AS Step3," + "SOver4 AS Over4, SThru4 AS Thru4, SRate4 AS Rate4, SStep4 AS Step4," + "SOver5 AS Over5, SThru5 AS Thru5, SRate5 AS Rate5, SStep5 AS Step5," + "SOver6 AS Over6, SThru6 AS Thru6, SRate6 AS Rate6, SStep6 AS Step6," + "SOver7 AS Over7, SThru7 AS Thru7, SRate7 AS Rate7, SStep7 AS Step7," + "SOver8 AS Over8, SThru8 AS Thru8, SRate8 AS Rate8, SStep8 AS Step8 " + "FROM RateTable";
			strWaterQuery = "SELECT RateTableNumber,RateTableDescription,'Water' AS Service," + "MinConsW AS MinCons,MinChargeW AS MinCharge,FlatRateW AS FlatRate," + "MiscChargeW1 AS MiscCharge1,MiscChargeAmountW1 AS MiscChargeAmount1,MiscChargeAccountW1 AS MiscChargeAccount1," + "MiscChargeW2 AS MiscCharge2,MiscChargeAmountW2 AS MiscChargeAmount2,MiscChargeAccountW2 AS MiscChargeAccount2," + "WOver1 AS Over1, WThru1 AS Thru1, WRate1 AS Rate1, WStep1 AS Step1," + "WOver2 AS Over2, WThru2 AS Thru2, WRate2 AS Rate2, WStep2 AS Step2," + "WOver3 AS Over3, WThru3 AS Thru3, WRate3 AS Rate3, WStep3 AS Step3," + "WOver4 AS Over4, WThru4 AS Thru4, WRate4 AS Rate4, WStep4 AS Step4," + "WOver5 AS Over5, WThru5 AS Thru5, WRate5 AS Rate5, WStep5 AS Step5," + "WOver6 AS Over6, WThru6 AS Thru6, WRate6 AS Rate6, WStep6 AS Step6," + "WOver7 AS Over7, WThru7 AS Thru7, WRate7 AS Rate7, WStep7 AS Step7," + "WOver8 AS Over8, WThru8 AS Thru8, WRate8 AS Rate8, WStep8 AS Step8 " + "FROM RateTable";
			// Now we need to decide what we're pulling and based on what
			if (modUTStatusPayments.Statics.TownService == "S")
			{
				strSQL = strSewerQuery + " ORDER BY RateTableNumber";
			}
			else if (modUTStatusPayments.Statics.TownService == "W")
			{
				strSQL = strWaterQuery + " ORDER BY RateTableNumber";
			}
			else
			{
				strSQL = strSewerQuery + " UNION ALL " + strWaterQuery + " ORDER BY Service, RateTableNumber";
			}
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("No Rate Tables Found.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Information);
				Cancel();
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			if (!rsData.EndOfFile())
			{
				fService.Text = rsData.Get_Fields_String("Service");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// vbPorter upgrade warning: fDesc As Variant --> As string
			//string fDesc = "";  // - "AutoDim"
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					fDesc.Text = rsData.Get_Fields_Int32("RateTableNumber") + " - " + rsData.Get_Fields_String("RateTableDescription");
					// Minimum/Flat
					// TODO Get_Fields: Field [MinCons] not found!! (maybe it is an alias?)
					fMinCons.Text = Strings.Format(rsData.Get_Fields("MinCons"), "#,##0");
					// TODO Get_Fields: Field [MinCharge] not found!! (maybe it is an alias?)
					fMinCharge.Text = Strings.Format(rsData.Get_Fields("MinCharge"), "#,##0.00");
					fFlat.Text = Strings.Format(rsData.Get_Fields_Decimal("FlatRate"), "#,##0.00");
					// Misc Charges
					// TODO Get_Fields: Field [MiscCharge1] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [MiscChargeAmount1] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [MiscCharge2] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [MiscChargeAmount2] not found!! (maybe it is an alias?)
					if ((FCConvert.ToString(rsData.Get_Fields("MiscCharge1")) == "" || Conversion.Val(rsData.Get_Fields("MiscChargeAmount1")) == 0) && (FCConvert.ToString(rsData.Get_Fields("MiscCharge2")) == "" || Conversion.Val(rsData.Get_Fields("MiscChargeAmount2")) == 0))
					{
						fMisc1Desc.Text = "None";
						fMisc2Desc.Text = "";
						fMisc1Amount.Text = "";
						fMisc1Account.Text = "";
						fMisc2Amount.Text = "";
						fMisc2Account.Text = "";
						fMisc2Desc.Visible = false;
						fMisc1Amount.Visible = false;
						fMisc1Account.Visible = false;
						fMisc2Amount.Visible = false;
						fMisc2Account.Visible = false;
					}
					else
					{
						// TODO Get_Fields: Field [MiscCharge1] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [MiscChargeAmount1] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsData.Get_Fields("MiscCharge1")) != "" || Conversion.Val(rsData.Get_Fields("MiscChargeAmount1")) != 0)
						{
							// TODO Get_Fields: Field [MiscCharge1] not found!! (maybe it is an alias?)
							fMisc1Desc.Text = FCConvert.ToString(rsData.Get_Fields("MiscCharge1"));
							// TODO Get_Fields: Field [MiscChargeAmount1] not found!! (maybe it is an alias?)
							fMisc1Amount.Text = Strings.Format(rsData.Get_Fields("MiscChargeAmount1"), "#,##0.00");
							// TODO Get_Fields: Field [MiscChargeAccount1] not found!! (maybe it is an alias?)
							fMisc1Account.Text = FCConvert.ToString(rsData.Get_Fields("MiscChargeAccount1"));
							// .Fields ("Taxable1")      'not used
						}
						// TODO Get_Fields: Field [MiscCharge2] not found!! (maybe it is an alias?)
						// TODO Get_Fields: Field [MiscChargeAmount2] not found!! (maybe it is an alias?)
						if (FCConvert.ToString(rsData.Get_Fields("MiscCharge2")) != "" || Conversion.Val(rsData.Get_Fields("MiscChargeAmount2")) != 0)
						{
							// TODO Get_Fields: Field [MiscCharge2] not found!! (maybe it is an alias?)
							fMisc2Desc.Text = FCConvert.ToString(rsData.Get_Fields("MiscCharge2"));
							// TODO Get_Fields: Field [MiscChargeAmount2] not found!! (maybe it is an alias?)
							fMisc2Amount.Text = Strings.Format(rsData.Get_Fields("MiscChargeAmount2"), "#,##0.00");
							// TODO Get_Fields: Field [MiscChargeAccount2] not found!! (maybe it is an alias?)
							fMisc2Account.Text = FCConvert.ToString(rsData.Get_Fields("MiscChargeAccount2"));
							// .Fields ("Taxable2")
						}
					}
					// Rate Details
					// TODO Get_Fields: Field [Over1] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [OVer1] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Thru1] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsData.Get_Fields("Over1")) != 0 && Conversion.Val(rsData.Get_Fields("OVer1")) != 999999999.0) || Conversion.Val(rsData.Get_Fields("Thru1")) != 0)
					{
						// TODO Get_Fields: Field [Over1] not found!! (maybe it is an alias?)
						fFrom1.Text = Strings.Format(rsData.Get_Fields("Over1"), "#,##0");
						// TODO Get_Fields: Field [Thru1] not found!! (maybe it is an alias?)
						fThru1.Text = Strings.Format(rsData.Get_Fields("Thru1"), "#,##0");
						// TODO Get_Fields: Field [Rate1] not found!! (maybe it is an alias?)
						fRate1.Text = Strings.Format(rsData.Get_Fields("Rate1"), "#,##0.00");
						// TODO Get_Fields: Field [Step1] not found!! (maybe it is an alias?)
						fStep1.Text = FCConvert.ToString(rsData.Get_Fields("Step1"));
						lblRate1.Visible = true;
						fFrom1.Visible = true;
						fThru1.Visible = true;
						fRate1.Visible = true;
						fStep1.Visible = true;
					}
					else
					{
						lblRate1.Visible = false;
						fFrom1.Visible = false;
						fThru1.Visible = false;
						fRate1.Visible = false;
						fStep1.Visible = false;
					}
					// TODO Get_Fields: Field [Over2] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [OVer2] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Thru2] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsData.Get_Fields("Over2")) != 0 && Conversion.Val(rsData.Get_Fields("OVer2")) != 999999999.0) || Conversion.Val(rsData.Get_Fields("Thru2")) != 0)
					{
						// TODO Get_Fields: Field [Over2] not found!! (maybe it is an alias?)
						fFrom2.Text = Strings.Format(rsData.Get_Fields("Over2"), "#,##0");
						// TODO Get_Fields: Field [Thru2] not found!! (maybe it is an alias?)
						fThru2.Text = Strings.Format(rsData.Get_Fields("Thru2"), "#,##0");
						// TODO Get_Fields: Field [Rate2] not found!! (maybe it is an alias?)
						fRate2.Text = Strings.Format(rsData.Get_Fields("Rate2"), "#,##0.00");
						// TODO Get_Fields: Field [Step2] not found!! (maybe it is an alias?)
						fStep2.Text = FCConvert.ToString(rsData.Get_Fields("Step2"));
						lblRate2.Visible = true;
						fFrom2.Visible = true;
						fThru2.Visible = true;
						fRate2.Visible = true;
						fStep2.Visible = true;
					}
					else
					{
						lblRate2.Visible = false;
						fFrom2.Visible = false;
						fThru2.Visible = false;
						fRate2.Visible = false;
						fStep2.Visible = false;
					}
					// TODO Get_Fields: Field [Over3] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [OVer3] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Thru3] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsData.Get_Fields("Over3")) != 0 && Conversion.Val(rsData.Get_Fields("OVer3")) != 999999999.0) || Conversion.Val(rsData.Get_Fields("Thru3")) != 0)
					{
						// TODO Get_Fields: Field [Over3] not found!! (maybe it is an alias?)
						fFrom3.Text = Strings.Format(rsData.Get_Fields("Over3"), "#,##0");
						// TODO Get_Fields: Field [Thru3] not found!! (maybe it is an alias?)
						fThru3.Text = Strings.Format(rsData.Get_Fields("Thru3"), "#,##0");
						// TODO Get_Fields: Field [Rate3] not found!! (maybe it is an alias?)
						fRate3.Text = Strings.Format(rsData.Get_Fields("Rate3"), "#,##0.00");
						// TODO Get_Fields: Field [Step3] not found!! (maybe it is an alias?)
						fStep3.Text = FCConvert.ToString(rsData.Get_Fields("Step3"));
						lblRate3.Visible = true;
						fFrom3.Visible = true;
						fThru3.Visible = true;
						fRate3.Visible = true;
						fStep3.Visible = true;
					}
					else
					{
						lblRate3.Visible = false;
						fFrom3.Visible = false;
						fThru3.Visible = false;
						fRate3.Visible = false;
						fStep3.Visible = false;
					}
					// TODO Get_Fields: Field [Over4] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [OVer4] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Thru4] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsData.Get_Fields("Over4")) != 0 && Conversion.Val(rsData.Get_Fields("OVer4")) != 999999999.0) || Conversion.Val(rsData.Get_Fields("Thru4")) != 0)
					{
						// TODO Get_Fields: Field [Over4] not found!! (maybe it is an alias?)
						fFrom4.Text = Strings.Format(rsData.Get_Fields("Over4"), "#,##0");
						// TODO Get_Fields: Field [Thru4] not found!! (maybe it is an alias?)
						fThru4.Text = Strings.Format(rsData.Get_Fields("Thru4"), "#,##0");
						// TODO Get_Fields: Field [Rate4] not found!! (maybe it is an alias?)
						fRate4.Text = Strings.Format(rsData.Get_Fields("Rate4"), "#,##0.00");
						// TODO Get_Fields: Field [Step4] not found!! (maybe it is an alias?)
						fStep4.Text = FCConvert.ToString(rsData.Get_Fields("Step4"));
						lblRate4.Visible = true;
						fFrom4.Visible = true;
						fThru4.Visible = true;
						fRate4.Visible = true;
						fStep4.Visible = true;
					}
					else
					{
						lblRate4.Visible = false;
						fFrom4.Visible = false;
						fThru4.Visible = false;
						fRate4.Visible = false;
						fStep4.Visible = false;
					}
					// TODO Get_Fields: Field [Over5] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [OVer5] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Thru5] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsData.Get_Fields("Over5")) != 0 && Conversion.Val(rsData.Get_Fields("OVer5")) != 999999999.0) || Conversion.Val(rsData.Get_Fields("Thru5")) != 0)
					{
						// TODO Get_Fields: Field [Over5] not found!! (maybe it is an alias?)
						fFrom5.Text = Strings.Format(rsData.Get_Fields("Over5"), "#,##0");
						// TODO Get_Fields: Field [Thru5] not found!! (maybe it is an alias?)
						fThru5.Text = Strings.Format(rsData.Get_Fields("Thru5"), "#,##0");
						fRate5.Text = Strings.Format(Fields["Rate5"], "#,##0.00");
						// TODO Get_Fields: Field [Step5] not found!! (maybe it is an alias?)
						fStep5.Text = FCConvert.ToString(rsData.Get_Fields("Step5"));
						lblRate5.Visible = true;
						fFrom5.Visible = true;
						fThru5.Visible = true;
						fRate5.Visible = true;
						fStep5.Visible = true;
					}
					else
					{
						lblRate5.Visible = false;
						fFrom5.Visible = false;
						fThru5.Visible = false;
						fRate5.Visible = false;
						fStep5.Visible = false;
					}
					// TODO Get_Fields: Field [Over6] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [OVer6] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Thru6] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsData.Get_Fields("Over6")) != 0 && Conversion.Val(rsData.Get_Fields("OVer6")) != 999999999.0) || Conversion.Val(rsData.Get_Fields("Thru6")) != 0)
					{
						// TODO Get_Fields: Field [Over6] not found!! (maybe it is an alias?)
						fFrom6.Text = Strings.Format(rsData.Get_Fields("Over6"), "#,##0");
						// TODO Get_Fields: Field [Thru6] not found!! (maybe it is an alias?)
						fThru6.Text = Strings.Format(rsData.Get_Fields("Thru6"), "#,##0");
						// TODO Get_Fields: Field [Rate6] not found!! (maybe it is an alias?)
						fRate6.Text = Strings.Format(rsData.Get_Fields("Rate6"), "#,##0.00");
						// TODO Get_Fields: Field [Step6] not found!! (maybe it is an alias?)
						fStep6.Text = FCConvert.ToString(rsData.Get_Fields("Step6"));
						lblRate6.Visible = true;
						fFrom6.Visible = true;
						fThru6.Visible = true;
						fRate6.Visible = true;
						fStep6.Visible = true;
					}
					else
					{
						lblRate6.Visible = false;
						fFrom6.Visible = false;
						fThru6.Visible = false;
						fRate6.Visible = false;
						fStep6.Visible = false;
					}
					// TODO Get_Fields: Field [Over7] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [OVer7] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Thru7] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsData.Get_Fields("Over7")) != 0 && Conversion.Val(rsData.Get_Fields("OVer7")) != 999999999.0) || Conversion.Val(rsData.Get_Fields("Thru7")) != 0)
					{
						// TODO Get_Fields: Field [Over7] not found!! (maybe it is an alias?)
						fFrom7.Text = Strings.Format(rsData.Get_Fields("Over7"), "#,##0");
						// TODO Get_Fields: Field [Thru7] not found!! (maybe it is an alias?)
						fThru7.Text = Strings.Format(rsData.Get_Fields("Thru7"), "#,##0");
						// TODO Get_Fields: Field [Rate7] not found!! (maybe it is an alias?)
						fRate7.Text = Strings.Format(rsData.Get_Fields("Rate7"), "#,##0.00");
						// TODO Get_Fields: Field [Step7] not found!! (maybe it is an alias?)
						fStep7.Text = FCConvert.ToString(rsData.Get_Fields("Step7"));
						lblRate7.Visible = true;
						fFrom7.Visible = true;
						fThru7.Visible = true;
						fRate7.Visible = true;
						fStep7.Visible = true;
					}
					else
					{
						lblRate7.Visible = false;
						fFrom7.Visible = false;
						fThru7.Visible = false;
						fRate7.Visible = false;
						fStep7.Visible = false;
					}
					// TODO Get_Fields: Field [Over8] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [OVer8] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [Thru8] not found!! (maybe it is an alias?)
					if ((Conversion.Val(rsData.Get_Fields("Over8")) != 0 && Conversion.Val(rsData.Get_Fields("OVer8")) != 999999999.0) || Conversion.Val(rsData.Get_Fields("Thru8")) != 0)
					{
						// TODO Get_Fields: Field [Over8] not found!! (maybe it is an alias?)
						fFrom8.Text = Strings.Format(rsData.Get_Fields("Over8"), "#,##0");
						// TODO Get_Fields: Field [Thru8] not found!! (maybe it is an alias?)
						fThru8.Text = Strings.Format(rsData.Get_Fields("Thru8"), "#,##0");
						// TODO Get_Fields: Field [Rate8] not found!! (maybe it is an alias?)
						fRate8.Text = Strings.Format(rsData.Get_Fields("Rate8"), "#,##0.00");
						// TODO Get_Fields: Field [Step8] not found!! (maybe it is an alias?)
						fStep8.Text = FCConvert.ToString(rsData.Get_Fields("Step8"));
						lblRate8.Visible = true;
						fFrom8.Visible = true;
						fThru8.Visible = true;
						fRate8.Visible = true;
						fStep8.Visible = true;
					}
					else
					{
						lblRate8.Visible = false;
						fFrom8.Visible = false;
						fThru8.Visible = false;
						fRate8.Visible = false;
						fStep8.Visible = false;
					}
					// Set srptRateDetails1 = New srptRateDetail
					// srptRateDetails1.Object.Init rsData
					// srptRateDetails1.Visible = True
				}
				rsData.MoveNext();
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Showing Rate Information", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void rptRateTablesCond_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptRateTablesCond properties;
			//rptRateTablesCond.Caption	= "Rate Table Listing";
			//rptRateTablesCond.Icon	= "rptRateTablesCondensed.dsx":0000";
			//rptRateTablesCond.Left	= 0;
			//rptRateTablesCond.Top	= 0;
			//rptRateTablesCond.Width	= 20280;
			//rptRateTablesCond.Height	= 10230;
			//rptRateTablesCond.StartUpPosition	= 3;
			//rptRateTablesCond.SectionData	= "rptRateTablesCondensed.dsx":058A;
			//End Unmaped Properties
		}
	}
}
