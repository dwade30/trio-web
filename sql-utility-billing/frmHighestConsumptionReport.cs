﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmHighestConsumptionReport.
	/// </summary>
	public partial class frmHighestConsumptionReport : BaseForm
	{
		public frmHighestConsumptionReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmHighestConsumptionReport InstancePtr
		{
			get
			{
				return (frmHighestConsumptionReport)Sys.GetInstance(typeof(frmHighestConsumptionReport));
			}
		}

		protected frmHighestConsumptionReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/19/2002              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/17/2006              *
		// ********************************************************
		public DateTime StartDate;
		public DateTime EndDate;
		// vbPorter upgrade warning: lngTop As int	OnWrite(string)
		public int lngTop;
		public bool blnUseRange;

		private void cmdPreview_Click(object sender, System.EventArgs e)
		{
			if (cboTop.SelectedIndex == -1)
			{
				MessageBox.Show("You must select a valid number of accounts to show.", "Missing Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				return;
			}
			else
			{
				lngTop = FCConvert.ToInt32(cboTop.Items[cboTop.SelectedIndex].ToString());
			}
			if (cmbRange.Text == "Range")
			{
				if (Information.IsDate(txtStartDate.Text))
				{
					StartDate = Convert.ToDateTime(txtStartDate.Text);
				}
				else
				{
					MessageBox.Show("Please enter a valid Start Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtStartDate.Focus();
					return;
				}
				if (Information.IsDate(txtEndDate.Text))
				{
					EndDate = Convert.ToDateTime(txtEndDate.Text);
				}
				else
				{
					MessageBox.Show("Please enter a valid End Date", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					txtEndDate.Focus();
					return;
				}
				blnUseRange = true;
			}
			else
			{
				blnUseRange = false;
			}
			//FC:FINAL:DDU:#i889 - get data from form
			frmReportViewer.InstancePtr.Init(rptHighestConsumption.InstancePtr);
			rptHighestConsumption.InstancePtr.blnUseRange = blnUseRange;
			rptHighestConsumption.InstancePtr.dtStartDate = StartDate;
			rptHighestConsumption.InstancePtr.dtEndDate = EndDate;
			rptHighestConsumption.InstancePtr.lngTop = lngTop;
			mnuProcessQuit_Click();
		}

		public void cmdPreview_Click()
		{
			cmdPreview_Click(cmdPreview, new System.EventArgs());
		}

		private void frmHighestConsumptionReport_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						this.Unload();
						break;
					}
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmHighestConsumptionReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmHighestConsumptionReport properties;
			//frmHighestConsumptionReport.ScaleWidth	= 5250;
			//frmHighestConsumptionReport.ScaleHeight	= 4035;
			//frmHighestConsumptionReport.LinkTopic	= "Form1";
			//End Unmaped Properties
			// this will size the form to the MDIParent
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			//FC:FINAL:RPU: these were fixed in designer
			//fraChoice.Left = FCConvert.ToInt32((this.Width - fraChoice.Width) / 2.0);
			//fraChoice.Top = 0;
			LoadTopCombo();
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmHighestConsumptionReport_Resize(object sender, System.EventArgs e)
		{
			//FC:FINAL:RPU: these were fixed in designer
			//fraChoice.Left = FCConvert.ToInt32((this.Width - fraChoice.Width) / 2.0);
			//fraChoice.Top = 0;
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//MDIParent.InstancePtr.Show();
			//MDIParent.InstancePtr.GRID.Focus();
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			cmdPreview_Click();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuProcessQuit_Click()
		{
			mnuProcessQuit_Click(mnuProcessQuit, new System.EventArgs());
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			if (cmbRange.Text == "Range")
			{
				txtStartDate.Enabled = true;
				txtEndDate.Enabled = true;
				txtStartDate.BackColor = Color.White;
				txtEndDate.BackColor = Color.White;
			}
			else
			{
				txtStartDate.Enabled = false;
				txtEndDate.Enabled = false;
				txtStartDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
				txtEndDate.BackColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORGRAYEDOUTTEXTBOX);
			}
		}

		private void LoadTopCombo()
		{
			cboTop.AddItem("10", 0);
			cboTop.AddItem("100", 1);
			cboTop.AddItem("200", 2);
			cboTop.AddItem("300", 3);
			cboTop.AddItem("400", 4);
			cboTop.AddItem("500", 5);
			cboTop.AddItem("600", 6);
			cboTop.AddItem("700", 7);
			cboTop.AddItem("800", 8);
			cboTop.AddItem("900", 9);
			cboTop.AddItem("1000", 10);
		}

		private void cmbRange_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			optRange_CheckedChanged(sender, e);
		}
	}
}
