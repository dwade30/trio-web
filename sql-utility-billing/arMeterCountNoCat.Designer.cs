﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arMeterCountNoCat.
	/// </summary>
	partial class arMeterCountNoCat
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arMeterCountNoCat));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDesc = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWaterHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSewerHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDesc = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalW = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalS = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalCount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewerHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDesc)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalW)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalS)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcct,
				this.fldDesc,
				this.fldS,
				this.fldW
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccount,
				this.lblDesc,
				this.lblWaterHeader,
				this.lblSewerHeader,
				this.lblHeader,
				this.Line2
			});
			this.GroupHeader1.Height = 0.7604167F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalW,
				this.fldTotalS,
				this.Line1,
				this.fldTotalCount
			});
			this.GroupFooter1.Height = 0.3333333F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.5625F;
			this.lblAccount.Width = 0.875F;
			// 
			// lblDesc
			// 
			this.lblDesc.Height = 0.1875F;
			this.lblDesc.HyperLink = null;
			this.lblDesc.Left = 1F;
			this.lblDesc.Name = "lblDesc";
			this.lblDesc.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblDesc.Text = "Description";
			this.lblDesc.Top = 0.5625F;
			this.lblDesc.Width = 2.5F;
			// 
			// lblWaterHeader
			// 
			this.lblWaterHeader.Height = 0.1875F;
			this.lblWaterHeader.HyperLink = null;
			this.lblWaterHeader.Left = 3.625F;
			this.lblWaterHeader.Name = "lblWaterHeader";
			this.lblWaterHeader.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblWaterHeader.Text = "Water Category";
			this.lblWaterHeader.Top = 0.5625F;
			this.lblWaterHeader.Width = 1.1875F;
			// 
			// lblSewerHeader
			// 
			this.lblSewerHeader.Height = 0.1875F;
			this.lblSewerHeader.HyperLink = null;
			this.lblSewerHeader.Left = 4.875F;
			this.lblSewerHeader.Name = "lblSewerHeader";
			this.lblSewerHeader.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblSewerHeader.Text = "Sewer Category";
			this.lblSewerHeader.Top = 0.5625F;
			this.lblSewerHeader.Width = 1.1875F;
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblHeader.Text = "Meters Not Setup With A Category";
			this.lblHeader.Top = 0.375F;
			this.lblHeader.Width = 6.5F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.75F;
			this.Line2.Width = 6.0625F;
			this.Line2.X1 = 0F;
			this.Line2.X2 = 6.0625F;
			this.Line2.Y1 = 0.75F;
			this.Line2.Y2 = 0.75F;
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.875F;
			// 
			// fldDesc
			// 
			this.fldDesc.Height = 0.1875F;
			this.fldDesc.Left = 1F;
			this.fldDesc.Name = "fldDesc";
			this.fldDesc.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
			this.fldDesc.Text = null;
			this.fldDesc.Top = 0F;
			this.fldDesc.Width = 2.5F;
			// 
			// fldS
			// 
			this.fldS.Height = 0.1875F;
			this.fldS.Left = 5.1875F;
			this.fldS.Name = "fldS";
			this.fldS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldS.Text = null;
			this.fldS.Top = 0F;
			this.fldS.Width = 0.5625F;
			// 
			// fldW
			// 
			this.fldW.Height = 0.1875F;
			this.fldW.Left = 3.9375F;
			this.fldW.Name = "fldW";
			this.fldW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldW.Text = null;
			this.fldW.Top = 0F;
			this.fldW.Width = 0.625F;
			// 
			// fldTotalW
			// 
			this.fldTotalW.Height = 0.1875F;
			this.fldTotalW.Left = 3.9375F;
			this.fldTotalW.Name = "fldTotalW";
			this.fldTotalW.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalW.Text = null;
			this.fldTotalW.Top = 0.125F;
			this.fldTotalW.Width = 0.625F;
			// 
			// fldTotalS
			// 
			this.fldTotalS.Height = 0.1875F;
			this.fldTotalS.Left = 5.1875F;
			this.fldTotalS.Name = "fldTotalS";
			this.fldTotalS.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
			this.fldTotalS.Text = null;
			this.fldTotalS.Top = 0.125F;
			this.fldTotalS.Width = 0.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 3.9375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.0625F;
			this.Line1.Width = 1.875F;
			this.Line1.X1 = 3.9375F;
			this.Line1.X2 = 5.8125F;
			this.Line1.Y1 = 0.0625F;
			this.Line1.Y2 = 0.0625F;
			// 
			// fldTotalCount
			// 
			this.fldTotalCount.Height = 0.1875F;
			this.fldTotalCount.Left = 1.125F;
			this.fldTotalCount.Name = "fldTotalCount";
			this.fldTotalCount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.fldTotalCount.Text = "Total Count:";
			this.fldTotalCount.Top = 0.125F;
			this.fldTotalCount.Width = 2.5F;
			// 
			// arMeterCountNoCat
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSewerHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDesc)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalW)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalS)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDesc;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldS;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldW;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDesc;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWaterHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSewerHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalW;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalS;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCount;
	}
}
