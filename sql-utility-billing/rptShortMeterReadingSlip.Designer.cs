﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptShortMeterReadingSlip.
	/// </summary>
	partial class rptShortMeterReadingSlip
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptShortMeterReadingSlip));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldLastReadingDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDigitSix = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDigitFive = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDigitFour = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDigitThree = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDigitTwo = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDigitOne = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDateSet = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSerialNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSize = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRemote = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRemoteNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBackFlowLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBackflow = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldLastReadingDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitSix)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitFive)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitFour)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitThree)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitTwo)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitOne)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateSet)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemote)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemoteNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBackFlowLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBackflow)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldLastReadingDate,
				this.fldDigitSix,
				this.fldDigitFive,
				this.fldDigitFour,
				this.fldDigitThree,
				this.fldDigitTwo,
				this.fldDigitOne,
				this.fldDateSet,
				this.fldSerialNumber,
				this.fldSize,
				this.fldRemote,
				this.fldRemoteNumber,
				this.fldBackFlowLabel,
				this.fldBackflow,
				this.fldName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldLocation,
				this.fldAccountNumber,
				this.fldBookSequence
			});
			this.Detail.Height = 8.5F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldLastReadingDate
			// 
			this.fldLastReadingDate.Height = 0.1875F;
			this.fldLastReadingDate.Left = 0F;
			this.fldLastReadingDate.Name = "fldLastReadingDate";
			this.fldLastReadingDate.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldLastReadingDate.Text = "Field1";
			this.fldLastReadingDate.Top = 0F;
			this.fldLastReadingDate.Width = 0.5625F;
			// 
			// fldDigitSix
			// 
			this.fldDigitSix.Height = 0.1875F;
			this.fldDigitSix.Left = 0.5625F;
			this.fldDigitSix.Name = "fldDigitSix";
			this.fldDigitSix.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldDigitSix.Text = "Field1";
			this.fldDigitSix.Top = 0F;
			this.fldDigitSix.Width = 0.15625F;
			// 
			// fldDigitFive
			// 
			this.fldDigitFive.Height = 0.1875F;
			this.fldDigitFive.Left = 0.78125F;
			this.fldDigitFive.Name = "fldDigitFive";
			this.fldDigitFive.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldDigitFive.Text = "Field1";
			this.fldDigitFive.Top = 0F;
			this.fldDigitFive.Width = 0.15625F;
			// 
			// fldDigitFour
			// 
			this.fldDigitFour.Height = 0.1875F;
			this.fldDigitFour.Left = 1F;
			this.fldDigitFour.Name = "fldDigitFour";
			this.fldDigitFour.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldDigitFour.Text = "Field1";
			this.fldDigitFour.Top = 0F;
			this.fldDigitFour.Width = 0.15625F;
			// 
			// fldDigitThree
			// 
			this.fldDigitThree.Height = 0.1875F;
			this.fldDigitThree.Left = 1.21875F;
			this.fldDigitThree.Name = "fldDigitThree";
			this.fldDigitThree.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldDigitThree.Text = "Field1";
			this.fldDigitThree.Top = 0F;
			this.fldDigitThree.Width = 0.15625F;
			// 
			// fldDigitTwo
			// 
			this.fldDigitTwo.Height = 0.1875F;
			this.fldDigitTwo.Left = 1.4375F;
			this.fldDigitTwo.Name = "fldDigitTwo";
			this.fldDigitTwo.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldDigitTwo.Text = "Field1";
			this.fldDigitTwo.Top = 0F;
			this.fldDigitTwo.Width = 0.15625F;
			// 
			// fldDigitOne
			// 
			this.fldDigitOne.Height = 0.1875F;
			this.fldDigitOne.Left = 1.65625F;
			this.fldDigitOne.Name = "fldDigitOne";
			this.fldDigitOne.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldDigitOne.Text = "Field1";
			this.fldDigitOne.Top = 0F;
			this.fldDigitOne.Width = 0.15625F;
			// 
			// fldDateSet
			// 
			this.fldDateSet.Height = 0.1875F;
			this.fldDateSet.Left = 0F;
			this.fldDateSet.Name = "fldDateSet";
			this.fldDateSet.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldDateSet.Text = "Field1";
			this.fldDateSet.Top = 0.59375F;
			this.fldDateSet.Width = 0.875F;
			// 
			// fldSerialNumber
			// 
			this.fldSerialNumber.Height = 0.1875F;
			this.fldSerialNumber.Left = 0.96875F;
			this.fldSerialNumber.Name = "fldSerialNumber";
			this.fldSerialNumber.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldSerialNumber.Text = "Field1";
			this.fldSerialNumber.Top = 0.59375F;
			this.fldSerialNumber.Width = 0.90625F;
			// 
			// fldSize
			// 
			this.fldSize.Height = 0.1875F;
			this.fldSize.Left = 1.90625F;
			this.fldSize.Name = "fldSize";
			this.fldSize.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldSize.Text = "Field1";
			this.fldSize.Top = 0.59375F;
			this.fldSize.Width = 0.65625F;
			// 
			// fldRemote
			// 
			this.fldRemote.Height = 0.1875F;
			this.fldRemote.Left = 2.59375F;
			this.fldRemote.Name = "fldRemote";
			this.fldRemote.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldRemote.Text = "Remote";
			this.fldRemote.Top = 0.59375F;
			this.fldRemote.Width = 0.5625F;
			// 
			// fldRemoteNumber
			// 
			this.fldRemoteNumber.Height = 0.1875F;
			this.fldRemoteNumber.Left = 3.25F;
			this.fldRemoteNumber.Name = "fldRemoteNumber";
			this.fldRemoteNumber.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldRemoteNumber.Text = "Remote";
			this.fldRemoteNumber.Top = 0.59375F;
			this.fldRemoteNumber.Width = 0.875F;
			// 
			// fldBackFlowLabel
			// 
			this.fldBackFlowLabel.Height = 0.1875F;
			this.fldBackFlowLabel.Left = 0F;
			this.fldBackFlowLabel.Name = "fldBackFlowLabel";
			this.fldBackFlowLabel.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldBackFlowLabel.Text = "Backflow";
			this.fldBackFlowLabel.Top = 0.84375F;
			this.fldBackFlowLabel.Width = 0.875F;
			// 
			// fldBackflow
			// 
			this.fldBackflow.Height = 0.1875F;
			this.fldBackflow.Left = 0.96875F;
			this.fldBackflow.Name = "fldBackflow";
			this.fldBackflow.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldBackflow.Text = "Field1";
			this.fldBackflow.Top = 0.84375F;
			this.fldBackflow.Width = 0.90625F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.19F;
			this.fldName.Left = 0F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldName.Text = "yyyy";
			this.fldName.Top = 1.53125F;
			this.fldName.Width = 2.3125F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.19F;
			this.fldAddress1.Left = 0F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress1.Text = "Backflow";
			this.fldAddress1.Top = 1.6875F;
			this.fldAddress1.Width = 2.3125F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.19F;
			this.fldAddress2.Left = 0F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress2.Text = "Backflow";
			this.fldAddress2.Top = 1.84375F;
			this.fldAddress2.Width = 2.3125F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.19F;
			this.fldAddress3.Left = 0F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldAddress3.Text = "Backflow";
			this.fldAddress3.Top = 2F;
			this.fldAddress3.Width = 2.3125F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 0F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Roman 12cpi\'; ddo-char-set: 1";
			this.fldLocation.Text = "Backflow";
			this.fldLocation.Top = 2.4375F;
			this.fldLocation.Width = 2.3125F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 3.25F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldAccountNumber.Text = "Backflow";
			this.fldAccountNumber.Top = 2.25F;
			this.fldAccountNumber.Width = 0.90625F;
			// 
			// fldBookSequence
			// 
			this.fldBookSequence.Height = 0.1875F;
			this.fldBookSequence.Left = 3.25F;
			this.fldBookSequence.Name = "fldBookSequence";
			this.fldBookSequence.Style = "font-family: \'Roman 12cpi\'; text-align: center; ddo-char-set: 1";
			this.fldBookSequence.Text = "Backflow";
			this.fldBookSequence.Top = 2.4375F;
			this.fldBookSequence.Width = 0.90625F;
			// 
			// rptShortMeterReadingSlip
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0F;
			this.PageSettings.Margins.Left = 0F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 4.385417F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.fldLastReadingDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitSix)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitFive)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitFour)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitThree)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitTwo)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigitOne)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDateSet)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemote)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemoteNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBackFlowLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBackflow)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLastReadingDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDigitSix;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDigitFive;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDigitFour;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDigitThree;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDigitTwo;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDigitOne;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDateSet;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSerialNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSize;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRemote;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRemoteNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBackFlowLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBackflow;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSequence;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
