﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;
using fecherFoundation.VisualBasicLayer;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptOutprintingFile.
	/// </summary>
	public partial class rptOutprintingFile : BaseSectionReport
	{
		public rptOutprintingFile()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Calculation Report";
		}

		public static rptOutprintingFile InstancePtr
		{
			get
			{
				return (rptOutprintingFile)Sys.GetInstance(typeof(rptOutprintingFile));
			}
		}

		protected rptOutprintingFile _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptOutprintingFile	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/03/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/12/2006              *
		// ********************************************************
		double BookWTotal;
		double BookSTotal;
		double AcctTotal;
		bool boolDone;
		StreamReader stream;
		bool boolFullDetail;

		public void Init(bool boolPassFullDetail, bool modalDialog, string strPassFilename = "")
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				boolFullDetail = boolPassFullDetail;
				if (strPassFilename == "")
				{
					strPassFilename = "TWUTBillExport.CSV";
				}
				// check to make sure the file is there
				if (FCFileSystem.FileExists(strPassFilename))
				{
					// open the file
					stream = FCFileSystem.OpenText(strPassFilename);
					frmReportViewer.InstancePtr.Init(this, "", FCConvert.ToInt32(FCForm.FormShowEnum.Modal), showModal: modalDialog);
				}
				else
				{
					MessageBox.Show("File " + strPassFilename + " does not exist", "Missing File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = boolDone;
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp;
				int lngCT;
				// read the first two header lines out
				strTemp = stream.ReadLine();
				strTemp = stream.ReadLine();
				FillHeaderLabels();
				SetupFields();
				return;
			}
			catch (Exception ex)
			{
				
				boolDone = true;
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!stream.EndOfStream)
			{
				BindFields();
			}
			else
			{
				boolDone = true;
				HideFields();
				//FC:FINAL:AM: close the stream
				stream.Close();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// this adds the totals at the end of the report
			fldSTotal.Text = Strings.Format(BookSTotal, "$#,##0.00");
			fldWTotal.Text = Strings.Format(BookWTotal, "$#,##0.00");
			fldOverallTotal.Text = Strings.Format(BookWTotal + BookSTotal, "$#,##0.00");
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strRecord;
				string[] arrValues = null;
				double dblWTot;
				double dblSTot;
				string str1;
				string str2;
				string str3;
				string str4 = "";
				// Short Version
				strRecord = stream.ReadLine();
				strRecord = strRecord.Replace(FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)), "|");
				arrValues = Strings.Split(strRecord, "|", -1, CompareConstants.vbBinaryCompare);
				fldAcct.Text = FCConvert.ToString(Conversion.Val(Strings.Right(arrValues[0], arrValues[0].Length - 1)));
				fldName.Text = Strings.Trim(arrValues[1]);
				fldLocation.Text = Strings.Trim(arrValues[12]);
				fldBookSeq.Text = "B" + Strings.Trim(arrValues[23]) + "  S " + Strings.Trim(arrValues[24]);
				dblWTot = FCConvert.ToDouble(arrValues[39]) / 100;
				dblSTot = FCConvert.ToDouble(arrValues[55]) / 100;
				fldSewer.Text = Strings.Format(dblSTot, "#,##0.00");
				fldWater.Text = Strings.Format(dblWTot, "#,##0.00");
				fldTotal.Text = Strings.Format(dblWTot + dblSTot, "#,##0.00");
				BookWTotal += dblWTot;
				BookSTotal += dblSTot;
				// Extended Version
				str1 = Strings.Trim(arrValues[3]);
				str2 = Strings.Trim(arrValues[4]);
				str3 = Strings.Trim(arrValues[5]);
				if (Strings.Trim(arrValues[9]) != "")
				{
					str4 = Strings.Trim(arrValues[6]) + ", " + Strings.Trim(arrValues[7]) + " " + Strings.Trim(arrValues[8]) + "-" + Strings.Trim(arrValues[9]);
				}
				else
				{
					str4 = Strings.Trim(arrValues[6]) + ", " + Strings.Trim(arrValues[7]) + " " + Strings.Trim(arrValues[8]);
				}
				if (str1 == "")
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					str4 = "";
				}
				if (str2 == "")
				{
					str2 = str3;
					str3 = str4;
					str4 = "";
				}
				if (str3 == "")
				{
					str3 = str4;
					str4 = "";
				}
				if (str2 == "")
				{
					str2 = str3;
					str3 = str4;
					str4 = "";
				}
				if (Strings.Trim(str1) != "")
				{
					fldAddr1.Text = str1 + "\r\n";
				}
				if (Strings.Trim(str2) != "")
				{
					fldAddr1.Text = fldAddr1.Text + str2 + "\r\n";
				}
				if (Strings.Trim(str3) != "")
				{
					fldAddr1.Text = fldAddr1.Text + str3 + "\r\n";
				}
				if (Strings.Trim(str4) != "")
				{
					fldAddr1.Text = fldAddr1.Text + str4;
				}
				fldCategory.Text = Strings.Trim(arrValues[10]);
				fldMapLot.Text = Strings.Trim(arrValues[11]);
				fldStatementDate.Text = Strings.Format(arrValues[14], "##/##/####");
				fldBillingPeriod.Text = Strings.Trim(arrValues[15]);
				fldReadingDate.Text = Strings.Format(arrValues[16], "##/##/####");
				// kk07272016 trouts-169  Use previous reading date for start date
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BANGOR")
				{
					lblStartDate.Text = "Prev Reading Date:";
					fldStartDate.Text = Strings.Format(Strings.Left(arrValues[72], 8), "##/##/####");
					lblEndDate.Visible = false;
					fldEndDate.Text = "";
				}
				else
				{
					fldStartDate.Text = Strings.Format(arrValues[17], "##/##/####");
					fldEndDate.Text = Strings.Format(arrValues[18], "##/##/####");
				}
				fldInterestDate.Text = Strings.Format(arrValues[19], "##/##/####");
				fldWUnitAmount.Text = Strings.Format(FCConvert.ToDouble(arrValues[28]) / 100, "#,##0.00");
				fldSUnitAmount.Text = Strings.Format(FCConvert.ToDouble(arrValues[44]) / 100, "#,##0.00");
				fldWConsAmount.Text = Strings.Format(FCConvert.ToDouble(arrValues[27]) / 100, "#,##0.00");
				fldSConsAmount.Text = Strings.Format(FCConvert.ToDouble(arrValues[43]) / 100, "#,##0.00");
				fldWFlatAmount.Text = Strings.Format(FCConvert.ToDouble(arrValues[29]) / 100, "#,##0.00");
				fldSFlatAmount.Text = Strings.Format(FCConvert.ToDouble(arrValues[45]) / 100, "#,##0.00");
				fldWAdjAmount.Text = Strings.Format(FCConvert.ToDouble(arrValues[30]) / 100, "#,##0.00");
				fldSAdjAmount.Text = Strings.Format(FCConvert.ToDouble(arrValues[46]) / 100, "#,##0.00");
				fldWRegular.Text = Strings.Format(FCConvert.ToDouble(arrValues[32]) / 100, "#,##0.00");
				fldSRegular.Text = Strings.Format(FCConvert.ToDouble(arrValues[48]) / 100, "#,##0.00");
				fldWTax.Text = Strings.Format(FCConvert.ToDouble(arrValues[33]) / 100, "#,##0.00");
				fldSTax.Text = Strings.Format(FCConvert.ToDouble(arrValues[49]) / 100, "#,##0.00");
				fldWNonLienPastDue.Text = Strings.Format(FCConvert.ToDouble(arrValues[34]) / 100, "#,##0.00");
				fldSNonLienPastDue.Text = Strings.Format(FCConvert.ToDouble(arrValues[50]) / 100, "#,##0.00");
				fldWCredits.Text = Strings.Format(FCConvert.ToDouble(arrValues[35]) / 100, "#,##0.00");
				fldSCredits.Text = Strings.Format(FCConvert.ToDouble(arrValues[51]) / 100, "#,##0.00");
				fldWLien.Text = Strings.Format(FCConvert.ToDouble(arrValues[38]) / 100, "#,##0.00");
				fldSLien.Text = Strings.Format(FCConvert.ToDouble(arrValues[54]) / 100, "#,##0.00");
				fldWInterest.Text = Strings.Format(FCConvert.ToDouble(arrValues[37]) / 100, "#,##0.00");
				fldSInterest.Text = Strings.Format(FCConvert.ToDouble(arrValues[53]) / 100, "#,##0.00");
				fldWTotalPastDue.Text = Strings.Format(FCConvert.ToDouble(arrValues[40]) / 100, "#,##0.00");
				fldSTotalPastDue.Text = Strings.Format(FCConvert.ToDouble(arrValues[56]) / 100, "#,##0.00");
				fldWTotalDue.Text = Strings.Format(FCConvert.ToDouble(arrValues[36]) / 100, "#,##0.00");
				fldSTotalDue.Text = Strings.Format(FCConvert.ToDouble(arrValues[52]) / 100, "#,##0.00");
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Bind Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupFields()
		{
			if (boolFullDetail)
			{
				fldAddr1.Visible = true;
				// fldAddr2.Visible = True
				// fldAddr3.Visible = True
				// fldAddr4.Visible = True
				fldCategory.Visible = true;
				lblCategory.Visible = true;
				lblMapLot.Visible = true;
				fldMapLot.Visible = true;
				lblStatementDate.Visible = true;
				fldStatementDate.Visible = true;
				lblBillingPeriod.Visible = true;
				fldBillingPeriod.Visible = true;
				lblReadingDate.Visible = true;
				fldReadingDate.Visible = true;
				lblStartDate.Visible = true;
				fldStartDate.Visible = true;
				lblInterestDate.Visible = true;
				fldInterestDate.Visible = true;
				lblEndDate.Visible = true;
				fldEndDate.Visible = true;
				lblUnitAmount.Visible = true;
				fldWUnitAmount.Visible = true;
				fldSUnitAmount.Visible = true;
				lblConsAmount.Visible = true;
				fldWConsAmount.Visible = true;
				fldSConsAmount.Visible = true;
				lblFlatAmount.Visible = true;
				fldWFlatAmount.Visible = true;
				fldSFlatAmount.Visible = true;
				lblAdjAmount.Visible = true;
				fldWAdjAmount.Visible = true;
				fldSAdjAmount.Visible = true;
				lblRegular.Visible = true;
				fldWRegular.Visible = true;
				fldSRegular.Visible = true;
				lblTax.Visible = true;
				fldWTax.Visible = true;
				fldSTax.Visible = true;
				lblNonLienPastDue.Visible = true;
				fldWNonLienPastDue.Visible = true;
				fldSNonLienPastDue.Visible = true;
				lblCredit.Visible = true;
				fldWCredits.Visible = true;
				fldSCredits.Visible = true;
				lblLienAmount.Visible = true;
				fldWLien.Visible = true;
				fldSLien.Visible = true;
				lblInterest.Visible = true;
				fldWInterest.Visible = true;
				fldSInterest.Visible = true;
				lblTotalPastDue.Visible = true;
				fldWTotalPastDue.Visible = true;
				fldSTotalPastDue.Visible = true;
				lblTotalAmountDue.Visible = true;
				fldWTotalDue.Visible = true;
				fldSTotalDue.Visible = true;
				Detail.Height = lblTotalAmountDue.Top + lblTotalAmountDue.Height + 300 / 1440f;
			}
			else
			{
				fldAddr1.Visible = false;
				// fldAddr2.Visible = False
				// fldAddr3.Visible = False
				// fldAddr4.Visible = False
				fldCategory.Visible = false;
				lblCategory.Visible = false;
				lblMapLot.Visible = false;
				fldMapLot.Visible = false;
				lblStatementDate.Visible = false;
				fldStatementDate.Visible = false;
				lblBillingPeriod.Visible = false;
				fldBillingPeriod.Visible = false;
				lblReadingDate.Visible = false;
				fldReadingDate.Visible = false;
				lblStartDate.Visible = false;
				fldStartDate.Visible = false;
				lblInterestDate.Visible = false;
				fldInterestDate.Visible = false;
				lblEndDate.Visible = false;
				fldEndDate.Visible = false;
				lblUnitAmount.Visible = false;
				fldWUnitAmount.Visible = false;
				fldSUnitAmount.Visible = false;
				lblConsAmount.Visible = false;
				fldWConsAmount.Visible = false;
				fldSConsAmount.Visible = false;
				lblFlatAmount.Visible = false;
				fldWFlatAmount.Visible = false;
				fldSFlatAmount.Visible = false;
				lblAdjAmount.Visible = false;
				fldWAdjAmount.Visible = false;
				fldSAdjAmount.Visible = false;
				lblRegular.Visible = false;
				fldWRegular.Visible = false;
				fldSRegular.Visible = false;
				lblTax.Visible = false;
				fldWTax.Visible = false;
				fldSTax.Visible = false;
				lblNonLienPastDue.Visible = false;
				fldWNonLienPastDue.Visible = false;
				fldSNonLienPastDue.Visible = false;
				lblCredit.Visible = false;
				fldWCredits.Visible = false;
				fldSCredits.Visible = false;
				lblLienAmount.Visible = false;
				fldWLien.Visible = false;
				fldSLien.Visible = false;
				lblTotalPastDue.Visible = false;
				lblInterest.Visible = false;
				fldWInterest.Visible = false;
				fldSInterest.Visible = false;
				fldWTotalPastDue.Visible = false;
				fldSTotalPastDue.Visible = false;
				lblTotalAmountDue.Visible = false;
				fldWTotalDue.Visible = false;
				fldSTotalDue.Visible = false;
				Detail.Height = fldLocation.Top + fldLocation.Height + 100 / 1440f;
			}
		}

		private void HideFields()
		{
			fldName.Visible = false;
			fldAcct.Visible = false;
			fldLocation.Visible = false;
			fldBookSeq.Visible = false;
			fldWater.Visible = false;
			fldSewer.Visible = false;
			fldTotal.Visible = false;
			fldAddr1.Visible = false;
			fldCategory.Visible = false;
			lblCategory.Visible = false;
			lblMapLot.Visible = false;
			fldMapLot.Visible = false;
			lblStatementDate.Visible = false;
			fldStatementDate.Visible = false;
			lblBillingPeriod.Visible = false;
			fldBillingPeriod.Visible = false;
			lblReadingDate.Visible = false;
			fldReadingDate.Visible = false;
			lblStartDate.Visible = false;
			fldStartDate.Visible = false;
			lblInterestDate.Visible = false;
			fldInterestDate.Visible = false;
			lblEndDate.Visible = false;
			fldEndDate.Visible = false;
			lblUnitAmount.Visible = false;
			fldWUnitAmount.Visible = false;
			fldSUnitAmount.Visible = false;
			lblConsAmount.Visible = false;
			fldWConsAmount.Visible = false;
			fldSConsAmount.Visible = false;
			lblFlatAmount.Visible = false;
			fldWFlatAmount.Visible = false;
			fldSFlatAmount.Visible = false;
			lblAdjAmount.Visible = false;
			fldWAdjAmount.Visible = false;
			fldSAdjAmount.Visible = false;
			lblRegular.Visible = false;
			fldWRegular.Visible = false;
			fldSRegular.Visible = false;
			lblTax.Visible = false;
			fldWTax.Visible = false;
			fldSTax.Visible = false;
			lblNonLienPastDue.Visible = false;
			fldWNonLienPastDue.Visible = false;
			fldSNonLienPastDue.Visible = false;
			lblCredit.Visible = false;
			fldWCredits.Visible = false;
			fldSCredits.Visible = false;
			lblLienAmount.Visible = false;
			fldWLien.Visible = false;
			fldSLien.Visible = false;
			lblTotalPastDue.Visible = false;
			lblInterest.Visible = false;
			fldWInterest.Visible = false;
			fldSInterest.Visible = false;
			fldWTotalPastDue.Visible = false;
			fldSTotalPastDue.Visible = false;
			lblTotalAmountDue.Visible = false;
			fldWTotalDue.Visible = false;
			fldSTotalDue.Visible = false;
			Detail.Height = 0;
		}

		private void rptOutprintingFile_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptOutprintingFile properties;
			//rptOutprintingFile.Caption	= "Calculation Report";
			//rptOutprintingFile.Icon	= "rptOutprintingFile.dsx":0000";
			//rptOutprintingFile.Left	= 0;
			//rptOutprintingFile.Top	= 0;
			//rptOutprintingFile.Width	= 15240;
			//rptOutprintingFile.Height	= 11115;
			//rptOutprintingFile.WindowState	= 2;
			//rptOutprintingFile.SectionData	= "rptOutprintingFile.dsx":058A;
			//End Unmaped Properties
		}
	}
}
