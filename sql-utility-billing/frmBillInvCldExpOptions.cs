﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBillInvCldExpOptions.
	/// </summary>
	public partial class frmBillInvCldExpOptions : BaseForm
	{
		public frmBillInvCldExpOptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillInvCldExpOptions InstancePtr
		{
			get
			{
				return (frmBillInvCldExpOptions)Sys.GetInstance(typeof(frmBillInvCldExpOptions));
			}
		}

		protected frmBillInvCldExpOptions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation                  *
		// *
		// WRITTEN BY     :   Corey Gray                          *
		// MODIFIED BY    :   Kevin Kelly                         *
		// DATE MODIFIED  :   10-14-2011                          *
		// *
		// Allows the user to pick bill paramters like ranges     *
		// and order then prints the bills                        *
		// *
		// Copied frmBillPrintOptions to allow user to export     *
		// bills to PDF for uploading to Invoice Cloud            *
		// ********************************************************
		int lngRateKeyToPrint;
		string strListOfBooks;
		clsDRWrapper rsSettings = new clsDRWrapper();
		FCTimer oTimer;

		public void Init(ref int lngRateKey, ref string strBookList)
		{
			lngRateKeyToPrint = lngRateKey;
			strListOfBooks = strBookList;
			this.Show(App.MainForm);
		}

		private void frmBillInvCldExpOptions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBillInvCldExpOptions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmBillInvCldExpOptions properties;
			//frmBillInvCldExpOptions.FillStyle	= 0;
			//frmBillInvCldExpOptions.ScaleWidth	= 5880;
			//frmBillInvCldExpOptions.ScaleHeight	= 4425;
			//frmBillInvCldExpOptions.LinkTopic	= "Form2";
			//frmBillInvCldExpOptions.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			string strTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridIndividual();
			strTemp = modRegistry.GetRegistryKey("UTPrintBillOrder");
			if (Strings.UCase(strTemp) == "ACCOUNT")
			{
				//optOrder[1].Checked = true;
				cmbOrder.Text = "Account";
			}
			else if (Strings.UCase(strTemp) == "SEQUENCE")
			{
				//optOrder[2].Checked = true;
				cmbOrder.Text = "Sequence";
			}
			else
			{
			}
			if (modUTStatusPayments.Statics.TownService == "B")
			{
				//optWaterSewer[2].Checked = true;
				cmbWaterSewer.Text = "Sequence";
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				//optWaterSewer[1].Checked = true;
				//optWaterSewer[2].Enabled = false;
				//optWaterSewer[0].Enabled = false;
				cmbWaterSewer.Clear();
				cmbWaterSewer.Items.Add("Sewer");
				cmbWaterSewer.Text = "Sewer";
			}
			else if (modUTStatusPayments.Statics.TownService == "W")
			{
				//optWaterSewer[0].Checked = true;
				//optWaterSewer[2].Enabled = false;
				//optWaterSewer[1].Enabled = false;
				cmbWaterSewer.Clear();
				cmbWaterSewer.Items.Add("Water");
				cmbWaterSewer.Text = "Water";
			}
			clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select zerobilloption from utilitybilling", modExtraModules.strUTDatabase);
			if (!rsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("zerobilloption")))
				{
					chkExcludeZero.CheckState = Wisej.Web.CheckState.Checked;
				}
				else
				{
					chkExcludeZero.CheckState = Wisej.Web.CheckState.Unchecked;
				}
			}
		}

		private void GridIndividual_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			//Application.DoEvents();
			CheckGridIndividual();
		}

		private void GridIndividual_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						// remove this row
						if (GridIndividual.Row > 0)
						{
							GridIndividual.RemoveItem(GridIndividual.Row);
							CheckGridIndividual();
						}
						break;
					}
			}
			//end switch
		}

		private void GridIndividual_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						// force a validate edit
						GridIndividual.Row = 0;
						break;
					}
			}
			//end switch
		}

		private void GridIndividual_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = GridIndividual.GetFlexRowIndex(e.RowIndex);
			int col = GridIndividual.GetFlexColIndex(e.ColumnIndex);
			clsDRWrapper clsLoad = new clsDRWrapper();
			if (Conversion.Val(GridIndividual.EditText) > 0)
			{
				// check to see if it is a legit account number
				clsLoad.OpenRecordset("select key from master where accountnumber = " + FCConvert.ToString(Conversion.Val(GridIndividual.EditText)), modExtraModules.strUTDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [key] not found!! (maybe it is an alias?)
					GridIndividual.TextMatrix(row, 1, FCConvert.ToString(clsLoad.Get_Fields("key")));
					GridIndividual.TextMatrix(row, 0, GridIndividual.EditText);
				}
				else
				{
					GridIndividual.EditText = "";
					GridIndividual.TextMatrix(row, 0, "");
					GridIndividual.TextMatrix(row, 1, FCConvert.ToString(0));
				}
			}
			else
			{
				GridIndividual.EditText = "";
				GridIndividual.TextMatrix(row, 0, "");
				GridIndividual.TextMatrix(row, 1, FCConvert.ToString(0));
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		public void mnuExit_Click()
		{
			//FC:FINAL:RPU: mnuExit does not exist 
			//mnuExit_Click(mnuExit, new System.EventArgs());
			mnuExit_Click(new Button(), new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			string strSQL;
			string strOPSQL = "";
			clsCustomUTBill CBill = new clsCustomUTBill();
			string strWhere = "";
			string strList = "";
			int X;
			string strOrderBy = "";
			clsDRWrapper clsTemp = new clsDRWrapper();
			int intType;
			DateTime dtStatementDate;
			string strSQLTemp = "";
			string strBillFields;
			GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport oExpPDF = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
			// MAL@20080806: Specify which fields get pulled from the Bill table
			// Tracker Reference: 12862
			strBillFields = "Bill, Bill.AccountKey, ActualAccountNumber, BillNumber, Bill.MeterKey, Bill.Book, Bill.Service, Bill.BillStatus, Bill.BillingRateKey, Bill.NoBill";
			strBillFields += ", Bill.CombinationCode, Bill.CurDate, Bill.CurReading, Bill.CurCode, Bill.PrevDate, Bill.PrevReading, Bill.PrevCode, Bill.Consumption";
			strBillFields += ", WaterOverrideCons, WaterOverrideAmount, SewerOverrideCons, SewerOverrideAmount, WMiscAmount";
			strBillFields += ", WAdjustAmount, WDEAdjustAmount, WFlatAmount, WUnitsAmount, WConsumptionAmount, SMiscAmount, SAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount";
			strBillFields += ", WTax, STax, TotalWBillAmount, TotalSBillAmount, WIntPaidDate, WPrinOwed, WTaxOwed, WIntOwed, WIntAdded, WCostOwed, WCostAdded, WPrinPaid, WTaxPaid, WIntPaid, WCostPaid";
			strBillFields += ", SIntPaidDate, SPrinOwed, STaxOwed, SIntOwed, SIntAdded, SCostOwed, SCostAdded, SPrinPaid, STaxPaid, SIntPaid, SCostPaid";
			strBillFields += ", WRT1, WRT2, WRT3, WRT4, WRT5, SRT1, SRT2, SRT3, SRT4, SRT5, Bill.Location";
			strBillFields += ", Bill.BName, Bill.BName2, Bill.BAddress1, Bill.BAddress2, Bill.BAddress3, Bill.BCity, Bill.BState, Bill.BZip, Bill.BZip4, Bill.OName, Bill.OName2, Bill.OAddress1, Bill.OAddress2, Bill.OAddress3, Bill.OCity, Bill.OState, Bill.OZip, Bill.OZip4";
			strBillFields += ", Bill.Note, Bill.MapLot, Bill.BookPage, Bill.Telephone, Bill.Email, Bill.WCat, Bill.SCat, Bill.BillMessage, Bill.FinalEndDate, Bill.FinalStartDate, Bill.FinalBillDate, Bill.Final";
			strBillFields += ", WBillOwner, SBillOwner, BillDate, ReadingUnits, WOrigBillAmount, SOrigBillAmount, SHasOverride, WHasOverride, SDemandGroupID, WDemandGroupID, SendEBill";
			try
			{
				// On Error GoTo ErrorHandler
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
				//Application.DoEvents();
				if (!ValidateChoices())
				{
					return;
				}
				if (cmbOrder.Text == "Name")
				{
					strOrderBy = " ORDER BY BName,Bill";
					modRegistry.SaveRegistryKey("UTPrintBillOrder", "Name");
				}
				else if (cmbOrder.Text == "Account")
				{
					// MAL@20080318
					// strOrderBy = " ORDER BY accountnumber"
					strOrderBy = " ORDER BY ActualAccountNumber";
					modRegistry.SaveRegistryKey("UTPrintBillOrder", "Account");
				}
				else if (cmbOrder.Text == "Sequence")
				{
					strOrderBy = " ORDER BY Sequence";
					modRegistry.SaveRegistryKey("UTPrintBillOrder", "Sequence");
				}
				if (chkExcludeZero.CheckState == Wisej.Web.CheckState.Checked)
				{
					clsTemp.Execute("update utilitybilling set zerobilloption = true", modExtraModules.strUTDatabase);
				}
				else
				{
					clsTemp.Execute("update utilitybilling set zerobilloption = false", modExtraModules.strUTDatabase);
				}
				if (cmbRange.Text == "All")
				{
					strWhere = "";
				}
				else if (cmbRange.Text == "Range")
				{
					if (cmbRangeOf.Text == "Name")
					{
						strWhere = " AND BName BETWEEN '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "' ";
					}
					else if (cmbRangeOf.Text == "Account")
					{
						// MAL@20080318
						// strWhere = " and accountnumber between " & Val(txtStart.Text) & " and " & Val(txtEnd.Text) & " "
						strWhere = " AND ActualAccountNumber BETWEEN " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " AND " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";
					}
				}
				else if (cmbRange.Text == "Individual")
				{
					for (X = 1; X <= GridIndividual.Rows - 1; X++)
					{
						if (Conversion.Val(GridIndividual.TextMatrix(X, 0)) > 0)
						{
							strList += FCConvert.ToString(Conversion.Val(GridIndividual.TextMatrix(X, 0))) + ",";
						}
					}
					// X
					if (strList != string.Empty)
					{
						strList = Strings.Mid(strList, 1, strList.Length - 1);
					}
					// MAL@20080318
					// strWhere = " and master.accountnumber in (" & strList & ") "
					strWhere = " AND ActualAccountNumber in (" + strList + ") ";
				}
				// trout-624 08-08-2011 kgk  add an option to exclude bills for accounts set to email
				if (chkExcludeEmail.CheckState == Wisej.Web.CheckState.Checked)
				{
					strList = "";
					clsTemp.OpenRecordset("SELECT Master.Key, Master.EmailBill, Master.AccountNumber, Bill.AccountKey, Bill.Book FROM (Bill INNER JOIN Master on Bill.AccountKey = Master.Key) WHERE EmailBill AND BillingRateKey = " + FCConvert.ToString(lngRateKeyToPrint) + " AND (" + strListOfBooks + ")", "TWUT0000.vb1");
					if (clsTemp.EndOfFile() != true && clsTemp.BeginningOfFile() != true)
					{
						do
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							strList += clsTemp.Get_Fields("AccountNumber") + ",";
							clsTemp.MoveNext();
						}
						while (clsTemp.EndOfFile() != true);
						if (strList != string.Empty)
						{
							strList = Strings.Mid(strList, 1, strList.Length - 1);
						}
						strWhere += " AND ActualAccountNumber NOT In (" + strList + ") ";
					}
				}
				strSQL = " (" + strListOfBooks + ") ";
				if (cmbWaterSewer.Text != "Both")
				{
					// MAL@20080806: Add Check for IConnectInfo E-Bill Options
					// Tracker Reference: 10680
					// strSQL = "select bill.*,master.accountnumber,METERtable.sequence from metertable inner join (bill inner join master on (bill.accountkey = master.key)) on (metertable.meterkey = bill.meterkey) where billstatus = 'B' and billingratekey = " & lngRateKeyToPrint & strWhere & " and " & strSQL & strOrderBy
					strSQL = "select " + strBillFields + ",master.accountnumber,METERtable.sequence, tblIConnectInfo.AcceptEbill FROM (metertable INNER JOIN (bill INNER JOIN master ON bill.AccountKey = master.Key) ON metertable.MeterKey = bill.MeterKey) INNER JOIN tblIConnectInfo ON bill.AccountKey = tblIConnectInfo.AccountKey WHERE billstatus = 'B' and billingratekey = " + FCConvert.ToString(lngRateKeyToPrint) + strWhere + " AND " + strSQL + strOrderBy;
					strOPSQL = strSQL;
				}
				else
				{
					// combined.  Might have to split some bills into water and sewer if the addresses are different
					// same as above but uses a union to actually retrieve two entries in the recordset
					// for records that have to be split.  The split is done in the recordset not in code
					// MAL@20080806: Changed all SQL statements to include check for IConnect E-Bill option = Paper or Both
					// Tracker Reference: 10680
					// MAL@20080806: Changed all SQL statements to use the strBillFields to avoid the 255 field limit
					// Tracker Reference: 12862
					// strSQLTemp = "SELECT " & CNSTUTPRINTBILLTYPECOMBINED & " as PrintBillType ,* from Bill WHERE (WBillOwner AND SBillOwner) OR (NOT WBillOwner AND NOT SBillOwner)"
					strSQLTemp = "SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPECOMBINED) + " as PrintBillType, " + strBillFields + " FROM Bill INNER JOIN tblIConnectInfo ON Bill.AccountKey = tblIConnectInfo.AccountKey WHERE ((WBillOwner AND SBillOwner) AND ((tblIConnectInfo.AcceptEbill)='P' Or (tblIConnectInfo.AcceptEbill)='B')) OR ((NOT WBillOwner AND NOT SBillOwner) AND ((tblIConnectInfo.AcceptEbill)='P' Or (tblIConnectInfo.AcceptEbill)='B'))";
					clsTemp.CreateStoredProcedure("qryCombinedBillInfo", strSQLTemp, modExtraModules.strUTDatabase);
					// strSQLTemp = "SELECT " & CNSTUTPRINTBILLTYPESEWER & " as PrintBillType ,* from Bill WHERE (WBillOwner AND NOT SBillOwner) AND Service <> 'W' UNION ALL SELECT " & CNSTUTPRINTBILLTYPEWATER & " AS PrintBillType,* FROM Bill WHERE (WBillOwner AND NOT SBillOwner) AND Service <> 'S'"
					strSQLTemp = "SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPESEWER) + " as PrintBillType, " + strBillFields + " FROM Bill INNER JOIN tblIConnectInfo ON Bill.AccountKey = tblIConnectInfo.AccountKey WHERE (WBillOwner AND NOT SBillOwner) AND Service <> 'W' AND ((tblIConnectInfo.AcceptEbill)='P' Or (tblIConnectInfo.AcceptEbill)='B') UNION ALL SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPEWATER) + " AS PrintBillType, " + strBillFields + " FROM Bill INNER JOIN tblIConnectInfo ON Bill.AccountKey = tblIConnectInfo.AccountKey WHERE (WBillOwner AND NOT SBillOwner) AND Service <> 'S' AND ((tblIConnectInfo.AcceptEbill)='P' Or (tblIConnectInfo.AcceptEbill)='B')";
					clsTemp.CreateStoredProcedure("qryWaterBillOwnerInfo", strSQLTemp, modExtraModules.strUTDatabase);
					// strSQLTemp = "SELECT " & CNSTUTPRINTBILLTYPESEWER & " as PrintBillType, * from Bill WHERE (NOT WBillOwner AND SBillOwner) AND Service <> 'W' UNION ALL SELECT " & CNSTUTPRINTBILLTYPEWATER & " AS PrintBillType, * FROM Bill WHERE (NOT WBillOwner AND SBillOwner) AND Service <> 'S'"
					strSQLTemp = "SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPESEWER) + " as PrintBillType, " + strBillFields + " from Bill INNER JOIN tblIConnectInfo ON Bill.AccountKey = tblIConnectInfo.AccountKey WHERE (NOT WBillOwner AND SBillOwner) AND Service <> 'W' AND ((tblIConnectInfo.AcceptEbill)='P' Or (tblIConnectInfo.AcceptEbill)='B') UNION ALL SELECT " + FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPEWATER) + " AS PrintBillType, " + strBillFields + " FROM Bill INNER JOIN tblIConnectInfo ON Bill.AccountKey = tblIConnectInfo.AccountKey WHERE (NOT WBillOwner AND SBillOwner) AND Service <> 'S' AND ((tblIConnectInfo.AcceptEbill)='P' Or (tblIConnectInfo.AcceptEbill)='B')";
					clsTemp.CreateStoredProcedure("qrySewerBillOwnerInfo", strSQLTemp, modExtraModules.strUTDatabase);
					strSQLTemp = "SELECT * from qrySewerbillOwnerinfo union all select * from qrywaterbillownerinfo ";
					clsTemp.CreateStoredProcedure("qryCombinedSeparateBillInfo", strSQLTemp, modExtraModules.strUTDatabase);
					strSQLTemp = "SELECT * from qrycombinedseparatebillinfo union all select * from qrycombinedbillinfo";
					clsTemp.CreateStoredProcedure("qryPrintBill", strSQLTemp, modExtraModules.strUTDatabase);
					// MAL@20080318: Adjust field selection to avoid 255 field limit
					// Tracker Reference: 12732
					// strOPSQL = "select qryprintbill.*,master.accountnumber,METERtable.sequence from metertable inner join (qryprintbill inner join master on (qryprintbill.accountkey = master.key)) on (metertable.meterkey = qryprintbill.meterkey) where billstatus = 'B' AND Combine = 'N' and billingratekey = " & lngRateKeyToPrint & strWhere & " and PrintBillType <> 2 AND " & strSQL & strOrderBy
					strOPSQL = "SELECT qryPrintBill.*,MeterTable.Sequence from MeterTable INNER JOIN qryPrintBill on MeterTable.MeterKey = qryprintbill.MeterKey WHERE BillStatus = 'B' AND Combine = 'N' and BillingRateKey = " + FCConvert.ToString(lngRateKeyToPrint) + strWhere + " and PrintBillType <> 2 AND " + strSQL + strOrderBy;
					// strSQL = "select qryprintbill.*,master.accountnumber,METERtable.sequence from metertable inner join (qryprintbill inner join master on (qryprintbill.accountkey = master.key)) on (metertable.meterkey = qryprintbill.meterkey) where billstatus = 'B' AND Combine = 'N' and billingratekey = " & lngRateKeyToPrint & strWhere & " and " & strSQL & strOrderBy
					strSQL = "SELECT qryPrintBill.*,MeterTable.Sequence from MeterTable INNER JOIN qryPrintBill on MeterTable.MeterKey = qryPrintBill.MeterKey WHERE BillStatus = 'B' AND Combine = 'N' and BillingRateKey = " + FCConvert.ToString(lngRateKeyToPrint) + strWhere + " and " + strSQL + strOrderBy;
				}
				// CBill.FormatID = frmChooseCustomBillType.Init("UT", "TWUT0000.vb1")
				// Check for an outprinting file
				rsSettings.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Unload();
				if (!rsSettings.EndOfFile())
				{
					if (Conversion.Val(rsSettings.Get_Fields_String("DefaultBillFormat")) < 0)
					{
						if (cmbWaterSewer.Text == "Water")
						{
							intType = 0;
						}
						else if (cmbWaterSewer.Text == "Water")
						{
							intType = 1;
						}
						else
						{
							intType = 2;
						}
					}
					else
					{
						CBill.FormatID = frmBillParameters.InstancePtr.Init(cmbWaterSewer.Text == "Both");
						if (CBill.FormatID > 0)
						{
							CBill.PrinterName = "";
							CBill.DBFile = "TWUT0000.vb1";
							CBill.Module = "UT";
							CBill.SQL = strSQL;
							if (cmbWaterSewer.Text == "Water")
							{
								CBill.UseWaterBills = true;
								CBill.UseSewerBills = false;
							}
							else if (cmbWaterSewer.Text == "Sewer")
							{
								CBill.UseWaterBills = false;
								CBill.UseSewerBills = true;
							}
							else
							{
								CBill.UseWaterBills = true;
								CBill.UseSewerBills = true;
							}
							if (chkExcludeZero.CheckState == Wisej.Web.CheckState.Checked)
							{
								CBill.SkipZeroNegativeBills = true;
							}
							else
							{
								CBill.SkipZeroNegativeBills = false;
							}
							rptCustomBill.InstancePtr.Unload();
							// trout-624 kgk 08-05-2011  Email bills creates this also
							SetupInvCldFields_2(CBill.FormatID);
							rptCustomBill.InstancePtr.Init(CBill, true, true);
							if (!CBill.BeginningOfFile && !CBill.EndOfFile)
							{
								frmWait.InstancePtr.Init("Please wait..." + "\r\n" + "Generating bills");
								rptCustomBill.InstancePtr.Run(false);
								HideInvCldFields_2(CBill.FormatID);
								frmWait.InstancePtr.Unload();
								if (rptCustomBill.InstancePtr.State == GrapeCity.ActiveReports.SectionReport.ReportState.Completed && rptCustomBill.InstancePtr.Document.Pages.Count > 0)
								{
									oExpPDF = new GrapeCity.ActiveReports.Export.Pdf.Section.PdfExport();
									string FileName = "UTExp" + Strings.Format(DateTime.Now, "MMddyyyyhhmmss") + ".InvCld.icx";
									oExpPDF.Export(rptCustomBill.InstancePtr.Document, FileName);
									oExpPDF = null;
									MessageBox.Show("The export file was created successfully.");
								}
								else
								{
									MessageBox.Show("The export file was not created.");
								}
							}
							this.Unload();
							return;
						}
					}
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MnuSaveContinue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private bool ValidateChoices()
		{
			bool ValidateChoices = false;
			// MAL@20070830: Added Unload frmWait to all Errors before Exit Function to avoid lock-up
			ValidateChoices = true;
			if (cmbRange.Text == "All")
			{
				// all
				frmWait.InstancePtr.Unload();
				return ValidateChoices;
			}
			else if (cmbRange.Text == "Range")
			{
				// range
				if (Strings.Trim(txtStart.Text) == string.Empty && Strings.Trim(txtEnd.Text) == string.Empty)
				{
					ValidateChoices = false;
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You have chosen to print by range but have not provided a range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateChoices;
				}
				else if (Strings.Trim(txtStart.Text) == string.Empty)
				{
					ValidateChoices = false;
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You must provide a valid start range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateChoices;
				}
				else if (Strings.Trim(txtEnd.Text) == string.Empty)
				{
					ValidateChoices = false;
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You must provide a valid end range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateChoices;
				}
				else if (cmbRangeOf.Text == "Account")
				{
					// range of account
					if (Conversion.Val(txtEnd.Text) < Conversion.Val(txtStart.Text))
					{
						ValidateChoices = false;
						frmWait.InstancePtr.Unload();
						MessageBox.Show("The last account must be equal to or greater than the first account", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return ValidateChoices;
					}
				}
				else if (cmbRangeOf.Text == "Name")
				{
					if (Strings.CompareString(Strings.Trim(txtEnd.Text), "<", Strings.Trim(txtStart.Text)))
					{
						ValidateChoices = false;
						frmWait.InstancePtr.Unload();
						MessageBox.Show("The end range must be equal to or greater than the start range");
						return ValidateChoices;
					}
				}
			}
			else if (cmbRange.Text == "Individual")
			{
				// individual
				GridIndividual.Row = 0;
				//Application.DoEvents();
				if (GridIndividual.Rows <= 2)
				{
					ValidateChoices = false;
					frmWait.InstancePtr.Unload();
					MessageBox.Show("You have selected individual but have not entered any accounts", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return ValidateChoices;
				}
			}
			return ValidateChoices;
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// all
						framIndividual.Visible = false;
						framRange.Visible = false;
						break;
					}
				case 1:
					{
						// range
						framIndividual.Visible = false;
						framRange.Visible = true;
						break;
					}
				case 2:
					{
						// individual
						framIndividual.Visible = true;
						framRange.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void SetupGridIndividual()
		{
			GridIndividual.Rows = 2;
			GridIndividual.Cols = 2;
			GridIndividual.ColHidden(1, true);
			GridIndividual.TextMatrix(0, 0, "Account");
			GridIndividual.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void CheckGridIndividual()
		{
			int X;
			//Application.DoEvents();
			if (GridIndividual.Rows == 1)
			{
				GridIndividual.Rows = 2;
				GridIndividual.Row = 1;
				return;
			}
			for (X = (GridIndividual.Rows - 1); X >= 1; X--)
			{
				if (Conversion.Val(GridIndividual.TextMatrix(X, 0)) <= 0)
				{
					if (X != GridIndividual.Rows - 1)
					{
						GridIndividual.RemoveItem(X);
					}
					else
					{
						GridIndividual.TextMatrix(X, 0, "");
						GridIndividual.TextMatrix(X, 1, FCConvert.ToString(0));
					}
				}
			}
			// X
			if (Conversion.Val(GridIndividual.TextMatrix(GridIndividual.Rows - 1, 0)) > 0)
			{
				GridIndividual.Rows += 1;
				GridIndividual.Row = GridIndividual.Rows - 1;
			}
		}

		private void optRangeOf_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						break;
					}
				case 1:
					{
						if (Conversion.Val(txtStart.Text) <= 0)
						{
							txtStart.Text = "";
						}
						if (Conversion.Val(txtEnd.Text) <= 0)
						{
							txtEnd.Text = "";
						}
						break;
					}
			}
			//end switch
		}

		private void optRangeOf_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRangeOf.SelectedIndex;
			optRangeOf_CheckedChanged(index, sender, e);
		}

		private void txtEnd_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbRangeOf.Text == "Account")
			{
				// account so only numbers allowed
				if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStart_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbRangeOf.Text == "Account")
			{
				// account so only numbers allowed
				if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void SetupInvCldFields_2(int lngFormatID)
		{
			SetupInvCldFields(ref lngFormatID);
		}

		private void SetupInvCldFields(ref int lngFormatID)
		{
			// ADD THE "HIDDEN" INVOICE CLOUD ACCT # AND INV #
			clsDRWrapper rsCust = new clsDRWrapper();
			clsDRWrapper rsTemp = new clsDRWrapper();
			rsCust.OpenRecordset("SELECT * FROM CustomBillFields WHERE FormatID = " + FCConvert.ToString(lngFormatID) + " AND FieldID = 117 AND FontSize = 4", modExtraModules.strUTDatabase);
			if (rsCust.RecordCount() == 0)
			{
				rsTemp.OpenRecordset("SELECT Max(FieldNumber) as LastFld FROM CustomBillFields WHERE FormatID = " + FCConvert.ToString(lngFormatID), modExtraModules.strUTDatabase);
				rsCust.OpenRecordset("SELECT * FROM CustomBillFields WHERE ID = 0", modExtraModules.strUTDatabase);
				rsCust.AddNew();
				// TODO Get_Fields: Field [LastFld] not found!! (maybe it is an alias?)
				rsCust.Set_Fields("FieldNumber", Conversion.Val(rsTemp.Get_Fields("LastFld")) + 1);
				rsCust.Set_Fields("FieldID", 117);
				// the account number
				rsCust.Set_Fields("FormatID", lngFormatID);
			}
			else
			{
				rsCust.Edit();
			}
			rsCust.Set_Fields("Top", 0);
			rsCust.Set_Fields("Left", 0);
			rsCust.Set_Fields("Height", 0.1667);
			rsCust.Set_Fields("Width", 1);
			rsCust.Set_Fields("BillType", 1);
			// XXXXXXXXXXXX ????
			rsCust.Set_Fields("Description", "Reserved - Please Don't Remove");
			rsCust.Set_Fields("Alignment", 0);
			// Left Aligned
			rsCust.Set_Fields("UserText", "Do Not Remove");
			rsCust.Set_Fields("Font", "Courier New");
			rsCust.Set_Fields("FontSize", 4);
			rsCust.Set_Fields("FontStyle", 0);
			rsCust.Set_Fields("ExtraParameters", "");
			rsCust.Update();
			rsCust.OpenRecordset("SELECT * FROM CustomBillFields WHERE FormatID = " + FCConvert.ToString(lngFormatID) + " AND FieldID = 118 AND FontSize = 4", modExtraModules.strUTDatabase);
			if (rsCust.RecordCount() == 0)
			{
				rsTemp.OpenRecordset("SELECT Max(FieldNumber) as LastFld FROM CustomBillFields WHERE FormatID = " + FCConvert.ToString(lngFormatID), modExtraModules.strUTDatabase);
				rsCust.OpenRecordset("SELECT * FROM CustomBillFields WHERE ID = 0", modExtraModules.strUTDatabase);
				rsCust.AddNew();
				// TODO Get_Fields: Field [LastFld] not found!! (maybe it is an alias?)
				rsCust.Set_Fields("FieldNumber", Conversion.Val(rsTemp.Get_Fields("LastFld")) + 1);
				rsCust.Set_Fields("FieldID", 118);
				// the invoice number
				rsCust.Set_Fields("FormatID", lngFormatID);
			}
			else
			{
				rsCust.Edit();
			}
			rsCust.Set_Fields("Top", 0);
			rsCust.Set_Fields("Left", 1);
			rsCust.Set_Fields("Height", 0.1667);
			rsCust.Set_Fields("Width", 1);
			rsCust.Set_Fields("BillType", 1);
			rsCust.Set_Fields("Description", "Reserved - Please Don't Remove");
			rsCust.Set_Fields("Alignment", 0);
			// Left Aligned
			rsCust.Set_Fields("UserText", "Do Not Remove");
			rsCust.Set_Fields("Font", "Courier New");
			rsCust.Set_Fields("FontSize", 4);
			rsCust.Set_Fields("FontStyle", 0);
			rsCust.Set_Fields("ExtraParameters", "");
			rsCust.Update();
		}

		private void HideInvCldFields_2(int lngFormatID)
		{
			HideInvCldFields(ref lngFormatID);
		}

		private void HideInvCldFields(ref int lngFormatID)
		{
			// HIDE THE INVOICE CLOUD ACCT # AND INV #
			clsDRWrapper rsCust = new clsDRWrapper();
			rsCust.OpenRecordset("SELECT * FROM CustomBillFields WHERE FormatID = " + FCConvert.ToString(lngFormatID) + " AND FieldID = 117 AND FontSize = 4", modExtraModules.strUTDatabase);
			if (rsCust.RecordCount() != 0)
			{
				rsCust.Edit();
				rsCust.Set_Fields("Top", 0);
				rsCust.Set_Fields("Left", 0);
				rsCust.Set_Fields("Height", 0);
				rsCust.Set_Fields("Width", 0);
				rsCust.Set_Fields("UserText", "");
				rsCust.Update();
			}
			rsCust.OpenRecordset("SELECT * FROM CustomBillFields WHERE FormatID = " + FCConvert.ToString(lngFormatID) + " AND FieldID = 118 AND FontSize = 4", modExtraModules.strUTDatabase);
			if (rsCust.RecordCount() != 0)
			{
				rsCust.Edit();
				rsCust.Set_Fields("Top", 0);
				rsCust.Set_Fields("Left", 0);
				rsCust.Set_Fields("Height", 0);
				rsCust.Set_Fields("Width", 0);
				rsCust.Set_Fields("UserText", "");
				rsCust.Update();
			}
		}
	}
}
