﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDisconnectNoticeOriginalPlainPaper.
	/// </summary>
	partial class rptDisconnectNoticeOriginalPlainPaper
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDisconnectNoticeOriginalPlainPaper));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.fldReturnName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReturnAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldBookSequence = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblText = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldNoticeDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldShutOffDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Field6 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWaterTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerTotalLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSewerTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.linTotalLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalTotalLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAsterisk = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeperateSewerLabel = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblAsterisk2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSeperateSewer = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAddress5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblText)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoticeDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldShutOffDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTotalLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewerLabel)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewer)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanGrow = false;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldReturnName,
				this.fldReturnAddress1,
				this.fldReturnAddress2,
				this.fldReturnAddress3,
				this.fldReturnAddress4,
				this.fldBookSequence,
				this.Field1,
				this.Field2,
				this.fldAccountNumber,
				this.Field3,
				this.Line1,
				this.Line2,
				this.lblText,
				this.Field4,
				this.fldNoticeDate,
				this.Field5,
				this.fldShutOffDate,
				this.Field6,
				this.fldWaterTotal,
				this.fldSewerTotalLabel,
				this.fldSewerTotal,
				this.linTotalLine,
				this.fldTotalTotalLabel,
				this.fldTotalTotal,
				this.lblAsterisk,
				this.fldSeperateSewerLabel,
				this.lblAsterisk2,
				this.fldSeperateSewer,
				this.fldName,
				this.fldAddress1,
				this.fldAddress2,
				this.fldAddress3,
				this.fldAddress4,
				this.fldAddress5
			});
			this.Detail.Height = 6.03125F;
			this.Detail.Name = "Detail";
			this.Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
			// 
			// PageHeader
			// 
			this.PageHeader.Height = 0F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// fldReturnName
			// 
			this.fldReturnName.Height = 0.1875F;
			this.fldReturnName.Left = 0.96875F;
			this.fldReturnName.Name = "fldReturnName";
			this.fldReturnName.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnName.Text = "Field1";
			this.fldReturnName.Top = 0.34375F;
			this.fldReturnName.Width = 2.40625F;
			// 
			// fldReturnAddress1
			// 
			this.fldReturnAddress1.Height = 0.1875F;
			this.fldReturnAddress1.Left = 0.96875F;
			this.fldReturnAddress1.Name = "fldReturnAddress1";
			this.fldReturnAddress1.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnAddress1.Text = "Field1";
			this.fldReturnAddress1.Top = 0.53125F;
			this.fldReturnAddress1.Width = 2.40625F;
			// 
			// fldReturnAddress2
			// 
			this.fldReturnAddress2.Height = 0.1875F;
			this.fldReturnAddress2.Left = 0.96875F;
			this.fldReturnAddress2.Name = "fldReturnAddress2";
			this.fldReturnAddress2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnAddress2.Text = "Field1";
			this.fldReturnAddress2.Top = 0.71875F;
			this.fldReturnAddress2.Width = 2.40625F;
			// 
			// fldReturnAddress3
			// 
			this.fldReturnAddress3.Height = 0.1875F;
			this.fldReturnAddress3.Left = 0.96875F;
			this.fldReturnAddress3.Name = "fldReturnAddress3";
			this.fldReturnAddress3.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnAddress3.Text = "Field1";
			this.fldReturnAddress3.Top = 0.90625F;
			this.fldReturnAddress3.Width = 2.40625F;
			// 
			// fldReturnAddress4
			// 
			this.fldReturnAddress4.Height = 0.1875F;
			this.fldReturnAddress4.Left = 0.96875F;
			this.fldReturnAddress4.Name = "fldReturnAddress4";
			this.fldReturnAddress4.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldReturnAddress4.Text = "Field1";
			this.fldReturnAddress4.Top = 1.09375F;
			this.fldReturnAddress4.Width = 2.40625F;
			// 
			// fldBookSequence
			// 
			this.fldBookSequence.Height = 0.1875F;
			this.fldBookSequence.Left = 1.03125F;
			this.fldBookSequence.Name = "fldBookSequence";
			this.fldBookSequence.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldBookSequence.Text = "Field1";
			this.fldBookSequence.Top = 1.6875F;
			this.fldBookSequence.Width = 1F;
			// 
			// Field1
			// 
			this.Field1.Height = 0.25F;
			this.Field1.Left = 2.25F;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; font-size: 14.5pt; text-align: center; ddo-char-set: 0";
			this.Field1.Text = "DISCONNECTION NOTICE";
			this.Field1.Top = 1.625F;
			this.Field1.Width = 3.375F;
			// 
			// Field2
			// 
			this.Field2.Height = 0.1875F;
			this.Field2.Left = 5.84375F;
			this.Field2.Name = "Field2";
			this.Field2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.Field2.Text = "Acct#";
			this.Field2.Top = 1.6875F;
			this.Field2.Width = 0.5F;
			// 
			// fldAccountNumber
			// 
			this.fldAccountNumber.Height = 0.1875F;
			this.fldAccountNumber.Left = 6.40625F;
			this.fldAccountNumber.Name = "fldAccountNumber";
			this.fldAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAccountNumber.Text = "Acct#";
			this.fldAccountNumber.Top = 1.6875F;
			this.fldAccountNumber.Width = 0.84375F;
			// 
			// Field3
			// 
			this.Field3.Height = 0.1875F;
			this.Field3.Left = 1.9375F;
			this.Field3.Name = "Field3";
			this.Field3.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: center; ddo-char-set: 0";
			this.Field3.Text = "YOUR BILL IS OVERDUE, PLEASE PAY PROMPTLY ";
			this.Field3.Top = 1.875F;
			this.Field3.Width = 4F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 1.125F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 2F;
			this.Line1.Width = 0.75F;
			this.Line1.X1 = 1.875F;
			this.Line1.X2 = 1.125F;
			this.Line1.Y1 = 2F;
			this.Line1.Y2 = 2F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 5.875F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 2F;
			this.Line2.Width = 0.75F;
			this.Line2.X1 = 6.625F;
			this.Line2.X2 = 5.875F;
			this.Line2.Y1 = 2F;
			this.Line2.Y2 = 2F;
			// 
			// lblText
			// 
			this.lblText.Height = 0.1875F;
			this.lblText.Left = 1.9375F;
			this.lblText.Name = "lblText";
			this.lblText.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.lblText.Text = null;
			this.lblText.Top = 2.21875F;
			this.lblText.Width = 4F;
			// 
			// Field4
			// 
			this.Field4.Height = 0.1875F;
			this.Field4.Left = 2.0625F;
			this.Field4.Name = "Field4";
			this.Field4.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.Field4.Text = "Notice Date:";
			this.Field4.Top = 2.71875F;
			this.Field4.Width = 0.875F;
			// 
			// fldNoticeDate
			// 
			this.fldNoticeDate.Height = 0.1875F;
			this.fldNoticeDate.Left = 3.125F;
			this.fldNoticeDate.Name = "fldNoticeDate";
			this.fldNoticeDate.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldNoticeDate.Text = "Acct#";
			this.fldNoticeDate.Top = 2.71875F;
			this.fldNoticeDate.Width = 0.90625F;
			// 
			// Field5
			// 
			this.Field5.Height = 0.1875F;
			this.Field5.Left = 2.0625F;
			this.Field5.Name = "Field5";
			this.Field5.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.Field5.Text = "Shut Off Date:";
			this.Field5.Top = 3.0625F;
			this.Field5.Width = 1F;
			// 
			// fldShutOffDate
			// 
			this.fldShutOffDate.Height = 0.1875F;
			this.fldShutOffDate.Left = 3.125F;
			this.fldShutOffDate.Name = "fldShutOffDate";
			this.fldShutOffDate.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldShutOffDate.Text = "Acct#";
			this.fldShutOffDate.Top = 3.0625F;
			this.fldShutOffDate.Width = 0.90625F;
			// 
			// Field6
			// 
			this.Field6.Height = 0.1875F;
			this.Field6.Left = 4.28125F;
			this.Field6.Name = "Field6";
			this.Field6.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.Field6.Text = "Water Due:";
			this.Field6.Top = 2.625F;
			this.Field6.Width = 1F;
			// 
			// fldWaterTotal
			// 
			this.fldWaterTotal.Height = 0.1875F;
			this.fldWaterTotal.Left = 5.34375F;
			this.fldWaterTotal.Name = "fldWaterTotal";
			this.fldWaterTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; ddo-char-set: 0";
			this.fldWaterTotal.Text = "Acct#";
			this.fldWaterTotal.Top = 2.625F;
			this.fldWaterTotal.Width = 0.90625F;
			// 
			// fldSewerTotalLabel
			// 
			this.fldSewerTotalLabel.Height = 0.1875F;
			this.fldSewerTotalLabel.Left = 4.28125F;
			this.fldSewerTotalLabel.Name = "fldSewerTotalLabel";
			this.fldSewerTotalLabel.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldSewerTotalLabel.Text = "Sewer Due:";
			this.fldSewerTotalLabel.Top = 2.8125F;
			this.fldSewerTotalLabel.Width = 1F;
			// 
			// fldSewerTotal
			// 
			this.fldSewerTotal.Height = 0.1875F;
			this.fldSewerTotal.Left = 5.34375F;
			this.fldSewerTotal.Name = "fldSewerTotal";
			this.fldSewerTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; ddo-char-set: 0";
			this.fldSewerTotal.Text = "Acct#";
			this.fldSewerTotal.Top = 2.8125F;
			this.fldSewerTotal.Width = 0.90625F;
			// 
			// linTotalLine
			// 
			this.linTotalLine.Height = 0F;
			this.linTotalLine.Left = 4.28125F;
			this.linTotalLine.LineWeight = 1F;
			this.linTotalLine.Name = "linTotalLine";
			this.linTotalLine.Top = 3.03125F;
			this.linTotalLine.Width = 1.96875F;
			this.linTotalLine.X1 = 4.28125F;
			this.linTotalLine.X2 = 6.25F;
			this.linTotalLine.Y1 = 3.03125F;
			this.linTotalLine.Y2 = 3.03125F;
			// 
			// fldTotalTotalLabel
			// 
			this.fldTotalTotalLabel.Height = 0.1875F;
			this.fldTotalTotalLabel.Left = 4.28125F;
			this.fldTotalTotalLabel.Name = "fldTotalTotalLabel";
			this.fldTotalTotalLabel.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldTotalTotalLabel.Text = "Total Due:";
			this.fldTotalTotalLabel.Top = 3.0625F;
			this.fldTotalTotalLabel.Width = 1F;
			// 
			// fldTotalTotal
			// 
			this.fldTotalTotal.Height = 0.1875F;
			this.fldTotalTotal.Left = 5.34375F;
			this.fldTotalTotal.Name = "fldTotalTotal";
			this.fldTotalTotal.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: right; ddo-char-set: 0";
			this.fldTotalTotal.Text = "Acct#";
			this.fldTotalTotal.Top = 3.0625F;
			this.fldTotalTotal.Width = 0.90625F;
			// 
			// lblAsterisk
			// 
			this.lblAsterisk.Height = 0.1875F;
			this.lblAsterisk.HyperLink = null;
			this.lblAsterisk.Left = 6.3125F;
			this.lblAsterisk.Name = "lblAsterisk";
			this.lblAsterisk.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.lblAsterisk.Text = "*";
			this.lblAsterisk.Top = 2.625F;
			this.lblAsterisk.Width = 0.1875F;
			// 
			// fldSeperateSewerLabel
			// 
			this.fldSeperateSewerLabel.Height = 0.1875F;
			this.fldSeperateSewerLabel.Left = 2.5625F;
			this.fldSeperateSewerLabel.Name = "fldSeperateSewerLabel";
			this.fldSeperateSewerLabel.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left; ddo-char-set: 0";
			this.fldSeperateSewerLabel.Text = "DOES NOT INCLUDE SEWER CHARGES OF";
			this.fldSeperateSewerLabel.Top = 3.46875F;
			this.fldSeperateSewerLabel.Width = 2.71875F;
			// 
			// lblAsterisk2
			// 
			this.lblAsterisk2.Height = 0.1875F;
			this.lblAsterisk2.HyperLink = null;
			this.lblAsterisk2.Left = 2.34375F;
			this.lblAsterisk2.Name = "lblAsterisk2";
			this.lblAsterisk2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.lblAsterisk2.Text = "*";
			this.lblAsterisk2.Top = 3.46875F;
			this.lblAsterisk2.Width = 0.1875F;
			// 
			// fldSeperateSewer
			// 
			this.fldSeperateSewer.Height = 0.1875F;
			this.fldSeperateSewer.Left = 5.28125F;
			this.fldSeperateSewer.Name = "fldSeperateSewer";
			this.fldSeperateSewer.Style = "font-family: \'Tahoma\'; font-size: 12pt; text-align: left; ddo-char-set: 0";
			this.fldSeperateSewer.Text = "Acct#";
			this.fldSeperateSewer.Top = 3.46875F;
			this.fldSeperateSewer.Width = 0.90625F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 4.5625F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldName.Text = "Field1";
			this.fldName.Top = 3.875F;
			this.fldName.Width = 2.4375F;
			// 
			// fldAddress1
			// 
			this.fldAddress1.Height = 0.1875F;
			this.fldAddress1.Left = 4.5625F;
			this.fldAddress1.Name = "fldAddress1";
			this.fldAddress1.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress1.Text = "Field1";
			this.fldAddress1.Top = 4.0625F;
			this.fldAddress1.Width = 2.4375F;
			// 
			// fldAddress2
			// 
			this.fldAddress2.Height = 0.1875F;
			this.fldAddress2.Left = 4.5625F;
			this.fldAddress2.Name = "fldAddress2";
			this.fldAddress2.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress2.Text = "Field1";
			this.fldAddress2.Top = 4.25F;
			this.fldAddress2.Width = 2.4375F;
			// 
			// fldAddress3
			// 
			this.fldAddress3.Height = 0.1875F;
			this.fldAddress3.Left = 4.5625F;
			this.fldAddress3.Name = "fldAddress3";
			this.fldAddress3.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress3.Text = "Field1";
			this.fldAddress3.Top = 4.4375F;
			this.fldAddress3.Width = 2.4375F;
			// 
			// fldAddress4
			// 
			this.fldAddress4.Height = 0.1875F;
			this.fldAddress4.Left = 4.5625F;
			this.fldAddress4.Name = "fldAddress4";
			this.fldAddress4.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress4.Text = "Field1";
			this.fldAddress4.Top = 4.625F;
			this.fldAddress4.Width = 2.4375F;
			// 
			// fldAddress5
			// 
			this.fldAddress5.Height = 0.1875F;
			this.fldAddress5.Left = 4.5625F;
			this.fldAddress5.Name = "fldAddress5";
			this.fldAddress5.Style = "font-family: \'Tahoma\'; font-size: 12pt; ddo-char-set: 0";
			this.fldAddress5.Text = "Field1";
			this.fldAddress5.Top = 4.8125F;
			this.fldAddress5.Width = 2.4375F;
			// 
			// rptDisconnectNoticeOriginalPlainPaper
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.489583F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.fldReturnName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReturnAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldBookSequence)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblText)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldNoticeDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldShutOffDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWaterTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTotalLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSewerTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewerLabel)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAsterisk2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSeperateSewer)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAddress5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReturnAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBookSequence;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field3;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox lblText;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldNoticeDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldShutOffDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field6;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWaterTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerTotalLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSewerTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Line linTotalLine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotalLabel;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAsterisk;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeperateSewerLabel;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAsterisk2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeperateSewer;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAddress5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
