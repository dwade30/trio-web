﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arAddedAcctListing.
	/// </summary>
	partial class arAddedAcctListing
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arAddedAcctListing));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lbl1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldAccountNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldOwnerName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldLocation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRealEstateAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblFooter = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lbl1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwnerName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRealEstateAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAccountNum,
				this.fldOwnerName,
				this.fldLocation,
				this.fldRealEstateAccount
			});
			this.Detail.Height = 0.2395833F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblFooter
			});
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lbl1,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.Label1,
				this.Label2,
				this.Label4,
				this.Line1,
				this.Label5
			});
			this.PageHeader.Height = 0.6458333F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lbl1
			// 
			this.lbl1.Height = 0.25F;
			this.lbl1.HyperLink = null;
			this.lbl1.Left = 0F;
			this.lbl1.Name = "lbl1";
			this.lbl1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.lbl1.Text = "Added Account Listing";
			this.lbl1.Top = 0F;
			this.lbl1.Width = 8F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.5625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.75F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.25F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6.75F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1.25F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.1875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Arial\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label1.Text = "Acct";
			this.Label1.Top = 0.4375F;
			this.Label1.Width = 0.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0.875F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label2.Text = "Name";
			this.Label2.Top = 0.4375F;
			this.Label2.Width = 3.0625F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 4.875F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label4.Text = "Location";
			this.Label4.Top = 0.4375F;
			this.Label4.Width = 1F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.625F;
			this.Line1.Width = 8F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 8F;
			this.Line1.Y1 = 0.625F;
			this.Line1.Y2 = 0.625F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 7.125F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Arial\'; font-size: 8pt; font-weight: bold; ddo-char-set: 1";
			this.Label5.Text = "RE Acct";
			this.Label5.Top = 0.4375F;
			this.Label5.Width = 0.625F;
			// 
			// fldAccountNum
			// 
			this.fldAccountNum.Height = 0.1875F;
			this.fldAccountNum.Left = 0F;
			this.fldAccountNum.Name = "fldAccountNum";
			this.fldAccountNum.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldAccountNum.Text = null;
			this.fldAccountNum.Top = 0F;
			this.fldAccountNum.Width = 0.875F;
			// 
			// fldOwnerName
			// 
			this.fldOwnerName.CanGrow = false;
			this.fldOwnerName.Height = 0.1875F;
			this.fldOwnerName.Left = 0.875F;
			this.fldOwnerName.MultiLine = false;
			this.fldOwnerName.Name = "fldOwnerName";
			this.fldOwnerName.Style = "font-family: \'Tahoma\'; font-size: 9pt; white-space: nowrap; ddo-char-set: 0";
			this.fldOwnerName.Text = null;
			this.fldOwnerName.Top = 0F;
			this.fldOwnerName.Width = 3.9375F;
			// 
			// fldLocation
			// 
			this.fldLocation.Height = 0.1875F;
			this.fldLocation.Left = 4.875F;
			this.fldLocation.Name = "fldLocation";
			this.fldLocation.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldLocation.Text = null;
			this.fldLocation.Top = 0F;
			this.fldLocation.Width = 2.1875F;
			// 
			// fldRealEstateAccount
			// 
			this.fldRealEstateAccount.Height = 0.1875F;
			this.fldRealEstateAccount.Left = 7.125F;
			this.fldRealEstateAccount.Name = "fldRealEstateAccount";
			this.fldRealEstateAccount.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldRealEstateAccount.Text = null;
			this.fldRealEstateAccount.Top = 0F;
			this.fldRealEstateAccount.Width = 0.875F;
			// 
			// lblFooter
			// 
			this.lblFooter.Height = 0.1875F;
			this.lblFooter.HyperLink = null;
			this.lblFooter.Left = 0.5625F;
			this.lblFooter.Name = "lblFooter";
			this.lblFooter.Style = "font-family: \'Tahoma\'; text-align: center";
			this.lblFooter.Text = null;
			this.lblFooter.Top = 0.0625F;
			this.lblFooter.Width = 6.4375F;
			// 
			// arAddedAcctListing
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 8F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lbl1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccountNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldOwnerName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRealEstateAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblFooter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccountNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldOwnerName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLocation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRealEstateAccount;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblFooter;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lbl1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
