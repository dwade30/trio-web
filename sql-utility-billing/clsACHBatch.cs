//Fecher vbPorter - Version 1.0.0.90

using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class clsACHBatch
	{

		//=========================================================

		private clsACHBatchHeader tBatchHeader = new/*AsNew*/ clsACHBatchHeader();
		private clsACHBatchControlRecord tBatchControlRecord = new/*AsNew*/ clsACHBatchControlRecord();
        private List<clsACHDetailRecord> tDets = new List<clsACHDetailRecord>();
		private short intBlocks;

		public void AddDetail(ref clsACHDetailRecord tRec)
		{
			tDets.Add(tRec);
			tRec.RecordNumber = tDets.Count;
		}

		public IEnumerable<clsACHDetailRecord> Details()
		{
			return tDets;
		}

        public int BatchNumber { get; set; } = 0;



		public double Debits
		{
			get
			{
					double Debits = 0;
				Debits = tBatchControlRecord.TotalDebit;
				return Debits;
			}
		}

		public double Credits
		{
			get
			{
					double Credits = 0;
				Credits = tBatchControlRecord.TotalCredit;
				return Credits;
			}
		}

		public double Hash
		{
			get
			{
					double Hash = 0;
				Hash = tBatchControlRecord.Hash;
				return Hash;
			}
		}

		public short BlockRecords
		{
			get
			{
					short BlockRecords = 0;
				BlockRecords = intBlocks;
				return BlockRecords;
			}

			set
			{
				intBlocks = value;
			}
		}



		public clsACHBatchHeader BatchHeader
        {
            get { return tBatchHeader; }
        }
		

		public clsACHBatchControlRecord BatchControl
        {
            get { return tBatchControlRecord; }
        }
		

		public double CalcBatchTotalAmount()
		{
			double CalcBatchTotalAmount = 0;
			double dblTotalAmount;
			dblTotalAmount = 0;
			foreach (var tRec in tDets) {
				dblTotalAmount += tRec.TotalAmount;
			} // tRec
			CalcBatchTotalAmount = dblTotalAmount;
			return CalcBatchTotalAmount;
		}

		public void BuildBatchControlRecord()
		{
			short intRecCount = 0;
			double dblHashNumber = 0;
			double dblTotalDebits = 0;
			double dblTotalCredits = 0;

			intBlocks = 2;
			foreach (clsACHDetailRecord tRec in tDets) {
				intRecCount += 1;
				// kk        tRec.RecordNumber = intRecCount
				dblHashNumber += tRec.HashNumber;
				if (tRec.EntryType==clsACHDetailRecord.ACHDetailEntryType.ACHDetailEntryTypeCredit) {
					dblTotalCredits += tRec.TotalAmount;
				} else {
					dblTotalDebits += tRec.TotalAmount;
				}
			} // tRec

			intBlocks += intRecCount;
			tBatchControlRecord.Hash = dblHashNumber;
			tBatchControlRecord.EntryAddendaCount = intRecCount;
			tBatchControlRecord.TotalCredit = dblTotalCredits;
			tBatchControlRecord.TotalDebit = dblTotalDebits;
			tBatchControlRecord.BatchNumber = BatchNumber;
		}

	}
}
