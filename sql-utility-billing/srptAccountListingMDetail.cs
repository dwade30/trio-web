﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptAccountListingMDetail.
	/// </summary>
	public partial class srptAccountListingMDetail : FCSectionReport
	{
		public srptAccountListingMDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Meter Listing Detail";
		}

		public static srptAccountListingMDetail InstancePtr
		{
			get
			{
				return (srptAccountListingMDetail)Sys.GetInstance(typeof(srptAccountListingMDetail));
			}
		}

		protected srptAccountListingMDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptAccountListingMDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/16/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/14/2007              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		string strMeterSQL;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				strMeterSQL = Strings.Trim(FCConvert.ToString(this.UserData));
				// This will show the correct data
				SetupDetailFormat();
				// this loads the data
				if (LoadAccounts())
				{
					// rock on
				}
				else
				{
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Sub Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private bool LoadAccounts()
		{
			bool LoadAccounts = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				rsData.OpenRecordset(strMeterSQL, modExtraModules.strUTDatabase);
				if (!rsData.EndOfFile())
				{
					LoadAccounts = true;
				}
				else
				{
					LoadAccounts = false;
				}
				return LoadAccounts;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Accounts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return LoadAccounts;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strRTList = "";
				int intCT;
				if (!rsData.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
					fldSequence.Text = FCConvert.ToString(rsData.Get_Fields("Sequence"));
					fldCurrent.Text = FCConvert.ToString(rsData.Get_Fields_Int32("CurrentReading"));
					fldPrevious.Text = FCConvert.ToString(rsData.Get_Fields_Int32("PreviousReading"));
					fldCombine.Text = FCConvert.ToString(rsData.Get_Fields_String("Combine"));
					fldService.Text = FCConvert.ToString(rsData.Get_Fields_String("Service"));
					fldSize.Text = FCConvert.ToString(rsData.Get_Fields_Int32("Size"));
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("FinalBilled")))
					{
						fldFinalBill.Text = "Y";
					}
					else
					{
						fldFinalBill.Text = "N";
					}
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("NoBill")))
					{
						fldNoBill.Text = "Y";
					}
					else
					{
						fldNoBill.Text = "N";
					}
					if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Backflow")))
					{
						fldBackflow.Text = "Y";
					}
					else
					{
						fldBackflow.Text = "N";
					}
					// Sewer Rate Tables
					for (intCT = 1; intCT <= 5; intCT++)
					{
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT))) != 0)
						{
							strRTList += FCConvert.ToString(rsData.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT)) + ",");
						}
					}
					if (Strings.Trim(strRTList) != "")
					{
						fldSRT.Text = Strings.Left(strRTList, strRTList.Length - 1);
					}
					else
					{
						fldSRT.Text = "";
					}
					strRTList = "";
					// Water Rate Tables
					for (intCT = 1; intCT <= 5; intCT++)
					{
						if (FCConvert.ToInt32(rsData.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT))) != 0)
						{
							strRTList += FCConvert.ToString(rsData.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT)) + ",");
						}
					}
					if (Strings.Trim(strRTList) != "")
					{
						fldWRT.Text = Strings.Left(strRTList, strRTList.Length - 1);
					}
					else
					{
						fldWRT.Text = "";
					}
					strRTList = "";
					// Sewer Bill Types
					for (intCT = 1; intCT <= 5; intCT++)
					{
						// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsData.Get_Fields("SewerType" + FCConvert.ToString(intCT))) == 1)
						{
							strRTList += "Cons" + ",";
						}
						// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
						else if (Conversion.Val(rsData.Get_Fields("SewerType" + FCConvert.ToString(intCT))) == 2)
						{
							strRTList += "Flat" + ",";
						}
							// TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
							else if (Conversion.Val(rsData.Get_Fields("SewerType" + FCConvert.ToString(intCT))) == 3)
						{
							strRTList += "Unit" + ",";
						}
						else
						{
							// no marked
						}
					}
					if (Strings.Trim(strRTList) != "")
					{
						fldSBT.Text = Strings.Left(strRTList, strRTList.Length - 1);
					}
					else
					{
						fldSBT.Text = "";
					}
					strRTList = "";
					// Water Bill Types
					for (intCT = 1; intCT <= 5; intCT++)
					{
						// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
						if (Conversion.Val(rsData.Get_Fields("WaterType" + FCConvert.ToString(intCT))) == 1)
						{
							strRTList += "Cons" + ",";
						}
						// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
						else if (Conversion.Val(rsData.Get_Fields("WaterType" + FCConvert.ToString(intCT))) == 2)
						{
							strRTList += "Flat" + ",";
						}
							// TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
							else if (Conversion.Val(rsData.Get_Fields("WaterType" + FCConvert.ToString(intCT))) == 3)
						{
							strRTList += "Unit" + ",";
						}
						else
						{
							// no marked
						}
					}
					if (Strings.Trim(strRTList) != "")
					{
						fldWBT.Text = Strings.Left(strRTList, strRTList.Length - 1);
					}
					else
					{
						fldWBT.Text = "";
					}
					// move to the next record
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SetupDetailFormat()
		{
			float lngHeight;
			int intCT;
			bool boolSewerRT = false;
			bool boolSewerCat;
			lngHeight = 240 / 1440F;
			for (intCT = 0; intCT <= frmAccountListing.InstancePtr.lstFields.Items.Count - 1; intCT++)
			{
				if (frmAccountListing.InstancePtr.lstFields.Selected(intCT))
				{
					switch (frmAccountListing.InstancePtr.lstFields.ItemData(intCT))
					{
						case 3:
							{
								// Billing Type
								lblSBT.Visible = true;
								lblWBT.Visible = true;
								fldSBT.Visible = true;
								fldWBT.Visible = true;
								if (frmAccountListing.InstancePtr.lstFields.Selected(18) && frmAccountListing.InstancePtr.lstFields.Selected(19))
								{
									if (!frmAccountListing.InstancePtr.lstFields.Selected(18) || !frmAccountListing.InstancePtr.lstFields.Selected(19))
									{
										// one is selected
										lblSBT.Top = lngHeight * 2;
										fldSBT.Top = lngHeight * 2;
										lblWBT.Top = lngHeight * 3;
										fldWBT.Top = lngHeight * 3;
									}
									else
									{
										// both are selected
										lblSBT.Top = lngHeight * 3;
										fldSBT.Top = lngHeight * 3;
										lblWBT.Top = lngHeight * 4;
										fldWBT.Top = lngHeight * 4;
									}
								}
								else
								{
									// neither are selected
									lblSBT.Top = lngHeight * 1;
									fldSBT.Top = lngHeight * 1;
									lblWBT.Top = lngHeight * 2;
									fldWBT.Top = lngHeight * 2;
								}
								break;
							}
						case 6:
							{
								// Combination Type
								break;
							}
						case 7:
							{
								// Current Reading
								break;
							}
						case 9:
							{
								// Meter Final Bill
								break;
							}
						case 10:
							{
								// Meter Size
								break;
							}
						case 11:
							{
								// Meter No Bill
								break;
							}
						case 13:
							{
								// Previous Reading
								break;
							}
						case 15:
							{
								// Sequence
								break;
							}
						case 16:
							{
								// Service
								break;
							}
						case 18:
							{
								// Sewer RT
								lblSRT.Visible = true;
								fldSRT.Visible = true;
								boolSewerRT = true;
								break;
							}
						case 20:
							{
								// Water RT
								lblWRT.Visible = true;
								fldWRT.Visible = true;
								if (boolSewerRT)
								{
									lblWRT.Top = lngHeight * 2;
									fldWRT.Top = lngHeight * 2;
								}
								else
								{
									lblWRT.Top = lngHeight * 1;
									fldWRT.Top = lngHeight * 1;
								}
								// kk trouts-6 02282013  Change Water to Stormwater for Bangor
								if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
								{
									lblWRT.Text = "SW Rates";
								}
								break;
							}
						default:
							{
								// do nothing
								break;
							}
					}
					//end switch
				}
			}
		}

		private void srptAccountListingMDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptAccountListingMDetail properties;
			//srptAccountListingMDetail.Caption	= "Meter Listing Detail";
			//srptAccountListingMDetail.Icon	= "srptAccountListingMDetail.dsx":0000";
			//srptAccountListingMDetail.Left	= 0;
			//srptAccountListingMDetail.Top	= 0;
			//srptAccountListingMDetail.Width	= 11880;
			//srptAccountListingMDetail.Height	= 8010;
			//srptAccountListingMDetail.WindowState	= 2;
			//srptAccountListingMDetail.SectionData	= "srptAccountListingMDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
