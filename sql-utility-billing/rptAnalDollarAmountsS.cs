﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptAnalDollarAmountsS.
	/// </summary>
	public partial class rptAnalDollarAmountsS : BaseSectionReport
	{
		public rptAnalDollarAmountsS()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Billing Edit Report";
		}

		public static rptAnalDollarAmountsS InstancePtr
		{
			get
			{
				return (rptAnalDollarAmountsS)Sys.GetInstance(typeof(rptAnalDollarAmountsS));
			}
		}

		protected rptAnalDollarAmountsS _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAnalDollarAmountsS	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/19/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/16/2006              *
		// ********************************************************
		double[] dblTotals = new double[10 + 1];
		// holds totals for all books
		int ct;
		int lngBook;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolPreBilling;
		bool boolLandscape;
		float lngWidth;
		string strTemp = "";
		string strRK = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// sets the book number on the Page header
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngWidth = rptAnalysisReportsMaster.InstancePtr.lngWidth;
			boolLandscape = rptAnalysisReportsMaster.InstancePtr.boolLandscape;
			boolPreBilling = rptAnalysisReportsMaster.InstancePtr.boolPreBilling;
			BuildSQL();
			SetupFields();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblTemp;
				int lngBook = 0;
				double[] dblAmount = new double[10 + 1];
				double dblBookTotal = 0;
                using (clsDRWrapper rsCatAmt = new clsDRWrapper())
                {
                    int intCT;
                    string strFinal = "";
                    if (rptAnalysisReportsMaster.InstancePtr.boolFinal)
                    {
                        strRK = " AND ISNULL(Bill.Final,0) = 1 ";
                    }

                    if (!rsData.EndOfFile())
                    {
                        // calculate the amounts
                        // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                        lngBook = FCConvert.ToInt32(rsData.Get_Fields("Book"));
                        // rsCatAmt.OpenRecordset "SELECT SUM(TotalSBillAmount) AS Total, SCat FROM Bill WHERE Book = " & lngBook & " AND Service <> 'W' GROUP BY SCat", strUTDatabase
                        rsCatAmt.OpenRecordset(
                            "SELECT SUM(SMiscAmount + SAdjustAmount + SDEAdjustAmount + SFlatAmount+SUnitsAmount + SConsumptionAmount) AS Total, Bill.SCat FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE Book = " +
                            FCConvert.ToString(lngBook) + " AND Bill.Service <> 'W' AND BillStatus " + strTemp +
                            " 'B'" + strRK + " GROUP BY Bill.SCat", modExtraModules.strUTDatabase);
                        if (!rsCatAmt.EndOfFile())
                        {
                            // fill the amount totals in
                            for (intCT = 1; intCT <= 9; intCT++)
                            {
                                rsCatAmt.FindFirstRecord("SCat", intCT);
                                if (!rsCatAmt.NoMatch)
                                {
                                    dblAmount[intCT] = FCConvert.ToDouble(rsCatAmt.Get_Fields_Decimal("Total"));
                                }
                                else
                                {
                                    dblAmount[intCT] = 0;
                                }
                            }
                        }
                        else
                        {
                            // clear the amounts
                            for (intCT = 1; intCT <= 9; intCT++)
                            {
                                dblAmount[intCT] = 0;
                            }
                        }

                        // keep track of totals for the book to put at the last line of this report
                        dblBookTotal = dblAmount[1] + dblAmount[2] + dblAmount[3] + dblAmount[4] + dblAmount[5] +
                                       dblAmount[6] + dblAmount[7] + dblAmount[8] + dblAmount[9];
                        // fill the fields on the report
                        // fldBook
                        fldBook.Text = lngBook.ToString();
                        // fldCat1
                        fldCat1.Text = Strings.Format(dblAmount[1], "#,##0.00");
                        // fldCat2
                        fldCat2.Text = Strings.Format(dblAmount[2], "#,##0.00");
                        // fldCat3
                        fldCat3.Text = Strings.Format(dblAmount[3], "#,##0.00");
                        // fldCat4
                        fldCat4.Text = Strings.Format(dblAmount[4], "#,##0.00");
                        // fldCat5
                        fldCat5.Text = Strings.Format(dblAmount[5], "#,##0.00");
                        // fldCat6
                        fldCat6.Text = Strings.Format(dblAmount[6], "#,##0.00");
                        // fldCat7
                        fldCat7.Text = Strings.Format(dblAmount[7], "#,##0.00");
                        // fldCat8
                        fldCat8.Text = Strings.Format(dblAmount[8], "#,##0.00");
                        // fldCat9
                        fldCat9.Text = Strings.Format(dblAmount[9], "#,##0.00");
                        // fldTotal
                        fldTotal.Text = Strings.Format(dblBookTotal, "#,##0.00");
                        rsData.MoveNext();
                    }

                    // update the sums of all of the columns
                    dblTotals[1] += dblAmount[1];
                    dblTotals[2] += dblAmount[2];
                    dblTotals[3] += dblAmount[3];
                    dblTotals[4] += dblAmount[4];
                    dblTotals[5] += dblAmount[5];
                    dblTotals[6] += dblAmount[6];
                    dblTotals[7] += dblAmount[7];
                    dblTotals[8] += dblAmount[8];
                    dblTotals[9] += dblAmount[9];
                    dblTotals[10] += dblBookTotal;
                }

                return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields - BS", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (boolPreBilling)
				{
					strTemp = "<>";
					strRK = " AND BillingRateKey = 0 ";
				}
				else
				{
					strTemp = "=";
					strRK = " AND BillingRateKey IN " + rptAnalysisReportsMaster.InstancePtr.strRateKeyList;
				}
				rsData.OpenRecordset("SELECT DISTINCT Book FROM Bill WHERE (" + rptAnalysisReportsMaster.InstancePtr.strBookList + ") AND BillStatus " + strTemp + " 'B'" + strRK + " ORDER BY Book", modExtraModules.strUTDatabase);
				return BuildSQL;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building SQL - BS", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildSQL;
		}

		private void ShowEndTotals()
		{
			// this will fill the totals in
			fldTotal1.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotal2.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldTotal3.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldTotal4.Text = Strings.Format(dblTotals[4], "#,##0.00");
			fldTotal5.Text = Strings.Format(dblTotals[5], "#,##0.00");
			fldTotal6.Text = Strings.Format(dblTotals[6], "#,##0.00");
			fldTotal7.Text = Strings.Format(dblTotals[7], "#,##0.00");
			fldTotal8.Text = Strings.Format(dblTotals[8], "#,##0.00");
			fldTotal9.Text = Strings.Format(dblTotals[9], "#,##0.00");
			fldGrandTotal.Text = Strings.Format(dblTotals[10], "#,##0.00");
		}

		private void SetupFields()
		{
			if (boolLandscape)
			{
				// landscape
				// set all of the header labels, all of the other fields will mimic the settings here
				lblBook.Left = 0;
				lblCat1.Left = 720 / 1440f;
				lblCat2.Left = 2070 / 1440f;
				lblCat3.Left = 3420 / 1440f;
				lblCat4.Left = 4770 / 1440f;
				lblCat5.Left = 6120 / 1440f;
				lblCat6.Left = 7470 / 1440f;
				lblCat7.Left = 8820 / 1440f;
				lblCat8.Left = 10170 / 1440f;
				lblCat9.Left = 11520 / 1440f;
				lblTotal.Left = 12870 / 1440f;
				lblBook.Width = 720 / 1440f;
				lblCat1.Width = 1350 / 1440f;
				lblCat2.Width = 1350 / 1440f;
				lblCat3.Width = 1350 / 1440f;
				lblCat4.Width = 1350 / 1440f;
				lblCat5.Width = 1350 / 1440f;
				lblCat6.Width = 1350 / 1440f;
				lblCat7.Width = 1350 / 1440f;
				lblCat8.Width = 1350 / 1440f;
				lblCat9.Width = 1350 / 1440f;
				lblTotal.Width = 1530 / 1440f;
			}
			else
			{
				// portrait
				// set all of the header labels, all of the other fields will mimic the settings here
				lblBook.Left = 0;
				lblCat1.Left = 720 / 1440f;
				lblCat2.Left = 1710 / 1440f;
				lblCat3.Left = 2700 / 1440f;
				lblCat4.Left = 3690 / 1440f;
				lblCat5.Left = 4680 / 1440f;
				lblCat6.Left = 5670 / 1440f;
				lblCat7.Left = 6660 / 1440f;
				lblCat8.Left = 7650 / 1440f;
				lblCat9.Left = 8640 / 1440f;
				lblTotal.Left = 9630 / 1440f;
				lblBook.Width = 720 / 1440f;
				lblCat1.Width = 990 / 1440f;
				lblCat2.Width = 990 / 1440f;
				lblCat3.Width = 990 / 1440f;
				lblCat4.Width = 990 / 1440f;
				lblCat5.Width = 990 / 1440f;
				lblCat6.Width = 990 / 1440f;
				lblCat7.Width = 990 / 1440f;
				lblCat8.Width = 990 / 1440f;
				lblCat9.Width = 990 / 1440f;
				lblTotal.Width = 990 / 1440f;
			}
			// report header
			lblHeader.Width = lngWidth;
			lblReportType.Width = lngWidth;
			// set the lines
			lnHeader.X1 = 0;
			lnHeader.X2 = lngWidth;
			lnTotals.X1 = lblCat1.Left;
			lnTotals.X2 = lngWidth;
			// set all of the other fields to the settings above
			fldBook.Left = lblBook.Left;
			fldCat1.Left = lblCat1.Left;
			fldCat2.Left = lblCat2.Left;
			fldCat3.Left = lblCat3.Left;
			fldCat4.Left = lblCat4.Left;
			fldCat5.Left = lblCat5.Left;
			fldCat6.Left = lblCat6.Left;
			fldCat7.Left = lblCat7.Left;
			fldCat8.Left = lblCat8.Left;
			fldCat9.Left = lblCat9.Left;
			fldTotal.Left = lblTotal.Left;
			lblFooterTitle.Left = lblBook.Left;
			fldTotal1.Left = lblCat1.Left;
			fldTotal2.Left = lblCat2.Left;
			fldTotal3.Left = lblCat3.Left;
			fldTotal4.Left = lblCat4.Left;
			fldTotal5.Left = lblCat5.Left;
			fldTotal6.Left = lblCat6.Left;
			fldTotal7.Left = lblCat7.Left;
			fldTotal8.Left = lblCat8.Left;
			fldTotal9.Left = lblCat9.Left;
			fldGrandTotal.Left = lblTotal.Left;
			fldBook.Width = lblBook.Width;
			fldCat1.Width = lblCat1.Width;
			fldCat2.Width = lblCat2.Width;
			fldCat3.Width = lblCat3.Width;
			fldCat4.Width = lblCat4.Width;
			fldCat5.Width = lblCat5.Width;
			fldCat6.Width = lblCat6.Width;
			fldCat7.Width = lblCat7.Width;
			fldCat8.Width = lblCat8.Width;
			fldCat9.Width = lblCat9.Width;
			fldTotal.Width = lblTotal.Width;
			lblFooterTitle.Width = lblBook.Width;
			fldTotal1.Width = lblCat1.Width;
			fldTotal2.Width = lblCat2.Width;
			fldTotal3.Width = lblCat3.Width;
			fldTotal4.Width = lblCat4.Width;
			fldTotal5.Width = lblCat5.Width;
			fldTotal6.Width = lblCat6.Width;
			fldTotal7.Width = lblCat7.Width;
			fldTotal8.Width = lblCat8.Width;
			fldTotal9.Width = lblCat9.Width;
			fldGrandTotal.Width = lblTotal.Width;
		}

		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// fill in the report footer
			ShowEndTotals();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			// Setup the headers for this category report
            using (clsDRWrapper rsCat = new clsDRWrapper())
            {
                // vbPorter upgrade warning: intCT As short --> As int	OnRead(string)
                int intCT;
                // vbPorter upgrade warning: strCatTitle As string	OnWrite(int, string)
                string strCatTitle = "";
                rsCat.OpenRecordset("SELECT * FROM Category WHERE Code > 0 AND Code < 10",
                    modExtraModules.strUTDatabase);
                if (!rsCat.EndOfFile())
                {
                    for (intCT = 1; intCT <= 9; intCT++)
                    {
                        strCatTitle = FCConvert.ToString(intCT);
                        // default to the number of the code
                        rsCat.FindFirstRecord("Code", intCT);
                        if (!rsCat.NoMatch)
                        {
                            // Found
                            if (Strings.Trim(rsCat.Get_Fields_String("ShortDescription") + " ") != "")
                            {
                                strCatTitle = Strings.Trim(rsCat.Get_Fields_String("ShortDescription") + " ");
                            }
                        }

                        switch (intCT)
                        {
                            case 1:
                            {
                                lblCat1.Text = strCatTitle;
                                break;
                            }
                            case 2:
                            {
                                lblCat2.Text = strCatTitle;
                                break;
                            }
                            case 3:
                            {
                                lblCat3.Text = strCatTitle;
                                break;
                            }
                            case 4:
                            {
                                lblCat4.Text = strCatTitle;
                                break;
                            }
                            case 5:
                            {
                                lblCat5.Text = strCatTitle;
                                break;
                            }
                            case 6:
                            {
                                lblCat6.Text = strCatTitle;
                                break;
                            }
                            case 7:
                            {
                                lblCat7.Text = strCatTitle;
                                break;
                            }
                            case 8:
                            {
                                lblCat8.Text = strCatTitle;
                                break;
                            }
                            case 9:
                            {
                                lblCat9.Text = strCatTitle;
                                break;
                            }
                        }

                        //end switch
                    }
                }
            }
        }

		private void rptAnalDollarAmountsS_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAnalDollarAmountsS properties;
			//rptAnalDollarAmountsS.Caption	= "Billing Edit Report";
			//rptAnalDollarAmountsS.Icon	= "rptAnalDollarAmountsS.dsx":0000";
			//rptAnalDollarAmountsS.Left	= 0;
			//rptAnalDollarAmountsS.Top	= 0;
			//rptAnalDollarAmountsS.Width	= 11880;
			//rptAnalDollarAmountsS.Height	= 4860;
			//rptAnalDollarAmountsS.WindowState	= 2;
			//rptAnalDollarAmountsS.SectionData	= "rptAnalDollarAmountsS.dsx":058A;
			//End Unmaped Properties
		}
	}
}
