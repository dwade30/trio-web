﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTDailyAudit.
	/// </summary>
	partial class frmUTDailyAudit : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAudit;
		public fecherFoundation.FCComboBox cmbREPP;
		public fecherFoundation.FCLabel lblREPP;
		public fecherFoundation.FCComboBox cmbAlignment;
		public fecherFoundation.FCLabel lblAlignment;
		public fecherFoundation.FCFrame fraPreAudit;
		public fecherFoundation.FCButton cmdDone;
		public fecherFoundation.FCButton cmdPassword;
		public fecherFoundation.FCTextBox txtAuditPassword;
		public fecherFoundation.FCLabel lblAuditPassword;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTDailyAudit));
			this.cmbAudit = new fecherFoundation.FCComboBox();
			this.cmbREPP = new fecherFoundation.FCComboBox();
			this.lblREPP = new fecherFoundation.FCLabel();
			this.cmbAlignment = new fecherFoundation.FCComboBox();
			this.lblAlignment = new fecherFoundation.FCLabel();
			this.fraPreAudit = new fecherFoundation.FCFrame();
			this.label1 = new Wisej.Web.Label();
			this.cmdDone = new fecherFoundation.FCButton();
			this.cmdPassword = new fecherFoundation.FCButton();
			this.txtAuditPassword = new fecherFoundation.FCTextBox();
			this.lblAuditPassword = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPreAudit)).BeginInit();
			this.fraPreAudit.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 396);
			this.BottomPanel.Size = new System.Drawing.Size(617, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraPreAudit);
			this.ClientArea.Size = new System.Drawing.Size(617, 336);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(617, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(211, 30);
			this.HeaderText.Text = "Daily Audit Report";
			// 
			// cmbAudit
			// 
			this.cmbAudit.AutoSize = false;
			this.cmbAudit.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAudit.FormattingEnabled = true;
			this.cmbAudit.Items.AddRange(new object[] {
				"Audit Preview",
				"Audit Report and Close Out"
			});
			this.cmbAudit.Location = new System.Drawing.Point(177, 30);
			this.cmbAudit.Name = "cmbAudit";
			this.cmbAudit.Size = new System.Drawing.Size(367, 40);
			this.cmbAudit.TabIndex = 1;
			this.cmbAudit.Text = "Audit Preview";
			this.cmbAudit.SelectedIndexChanged += new System.EventHandler(this.optAudit_CheckedChanged);
			// 
			// cmbREPP
			// 
			this.cmbREPP.AutoSize = false;
			this.cmbREPP.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbREPP.FormattingEnabled = true;
			this.cmbREPP.Items.AddRange(new object[] {
				"Water",
				"Sewer",
				"Both"
			});
			this.cmbREPP.Location = new System.Drawing.Point(177, 90);
			this.cmbREPP.Name = "cmbREPP";
			this.cmbREPP.Size = new System.Drawing.Size(367, 40);
			this.cmbREPP.TabIndex = 3;
			this.cmbREPP.Text = "Water";
			// 
			// lblREPP
			// 
			this.lblREPP.AutoSize = true;
			this.lblREPP.Location = new System.Drawing.Point(20, 104);
			this.lblREPP.Name = "lblREPP";
			this.lblREPP.Size = new System.Drawing.Size(107, 15);
			this.lblREPP.TabIndex = 2;
			this.lblREPP.Text = "WATER / SEWER";
			// 
			// cmbAlignment
			// 
			this.cmbAlignment.AutoSize = false;
			this.cmbAlignment.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAlignment.FormattingEnabled = true;
			this.cmbAlignment.Items.AddRange(new object[] {
				"Portrait",
				"Landscape"
			});
			this.cmbAlignment.Location = new System.Drawing.Point(177, 150);
			this.cmbAlignment.Name = "cmbAlignment";
			this.cmbAlignment.Size = new System.Drawing.Size(367, 40);
			this.cmbAlignment.TabIndex = 5;
			this.cmbAlignment.Text = "Portrait";
			// 
			// lblAlignment
			// 
			this.lblAlignment.AutoSize = true;
			this.lblAlignment.Location = new System.Drawing.Point(20, 164);
			this.lblAlignment.Name = "lblAlignment";
			this.lblAlignment.Size = new System.Drawing.Size(80, 15);
			this.lblAlignment.TabIndex = 4;
			this.lblAlignment.Text = "ALIGNMENT";
			// 
			// fraPreAudit
			// 
			this.fraPreAudit.Controls.Add(this.label1);
			this.fraPreAudit.Controls.Add(this.cmbREPP);
			this.fraPreAudit.Controls.Add(this.lblREPP);
			this.fraPreAudit.Controls.Add(this.cmbAudit);
			this.fraPreAudit.Controls.Add(this.cmbAlignment);
			this.fraPreAudit.Controls.Add(this.lblAlignment);
			this.fraPreAudit.Controls.Add(this.cmdDone);
			this.fraPreAudit.Location = new System.Drawing.Point(30, 30);
			this.fraPreAudit.Name = "fraPreAudit";
			this.fraPreAudit.Size = new System.Drawing.Size(564, 288);
			this.fraPreAudit.TabIndex = 0;
			this.fraPreAudit.Text = "Select Audit Process";
			this.fraPreAudit.Visible = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(20, 44);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(45, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "AUDIT ";
			// 
			// cmdDone
			// 
			this.cmdDone.AppearanceKey = "acceptButton";
			this.cmdDone.ForeColor = System.Drawing.Color.White;
			this.cmdDone.Location = new System.Drawing.Point(20, 220);
			this.cmdDone.Name = "cmdDone";
			this.cmdDone.Size = new System.Drawing.Size(122, 48);
			//FC:FINAL:CHN - issue #1301: F12 don't work on form. 
			this.cmdDone.Shortcut = Shortcut.F12;
			this.cmdDone.TabIndex = 6;
			this.cmdDone.Text = "Print Preview";
			this.cmdDone.Click += new System.EventHandler(this.cmdDone_Click);
			// 
			// cmdPassword
			// 
			this.cmdPassword.AppearanceKey = "toolbarButton";
			this.cmdPassword.Location = new System.Drawing.Point(51, 83);
			this.cmdPassword.Name = "cmdPassword";
			this.cmdPassword.Size = new System.Drawing.Size(78, 23);
			this.cmdPassword.TabIndex = 8;
			this.cmdPassword.Text = "Process";
			// 
			// txtAuditPassword
			// 
			this.txtAuditPassword.AutoSize = false;
			this.txtAuditPassword.BackColor = System.Drawing.SystemColors.Window;
			this.txtAuditPassword.LinkItem = null;
			this.txtAuditPassword.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAuditPassword.LinkTopic = null;
			this.txtAuditPassword.Location = new System.Drawing.Point(32, 55);
			this.txtAuditPassword.Name = "txtAuditPassword";
			this.txtAuditPassword.Size = new System.Drawing.Size(112, 40);
			this.txtAuditPassword.TabIndex = 7;
			// 
			// lblAuditPassword
			// 
			this.lblAuditPassword.Location = new System.Drawing.Point(38, 16);
			this.lblAuditPassword.Name = "lblAuditPassword";
			this.lblAuditPassword.Size = new System.Drawing.Size(116, 31);
			this.lblAuditPassword.TabIndex = 10;
			this.lblAuditPassword.Text = "PLEASE ENTER YOUR AUDIT PASSWORD";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFilePrint,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFilePrint
			// 
			this.mnuFilePrint.Index = 0;
			this.mnuFilePrint.Name = "mnuFilePrint";
			this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFilePrint.Text = "Print Preview";
			this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 2;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// frmUTDailyAudit
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(617, 504);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmUTDailyAudit";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Daily Audit Report";
			this.Load += new System.EventHandler(this.frmUTDailyAudit_Load);
			this.Activated += new System.EventHandler(this.frmUTDailyAudit_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTDailyAudit_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraPreAudit)).EndInit();
			this.fraPreAudit.ResumeLayout(false);
			this.fraPreAudit.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdDone)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdPassword)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private Label label1;
		private System.ComponentModel.IContainer components;
	}
}
