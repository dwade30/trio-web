﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arLienTransferReport.
	/// </summary>
	public partial class arLienTransferReport : BaseSectionReport
	{
		public arLienTransferReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Lien Process Edit Report";
		}

		public static arLienTransferReport InstancePtr
		{
			get
			{
				return (arLienTransferReport)Sys.GetInstance(typeof(arLienTransferReport));
			}
		}

		protected arLienTransferReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsLN.Dispose();
				rsUT.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arLienTransferReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               07/21/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               09/01/2004              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		clsDRWrapper rsUT = new clsDRWrapper();
		string strSQL;
		double[] dblTotals = new double[5 + 1];
		int intYear;
		double dblMinimumAmount;
		int lngPosBal;
		int lngZeroBal;
		int lngNegBal;
		int lngDemandAlready;
		DateTime dtMailDate;
		int lngCount;
		public int intReportDetail;
		bool boolWater;
		string strWS = "";
		DateTime dtFileDate;
		string strDataSQL = "";

		public void Init(string strAccountList, bool boolPassWater, DateTime dtPassDate)
		{
			// this sub actually lets me pass the data connection class in
			string strTemp = "";
			boolWater = boolPassWater;
			if (boolWater)
			{
				strWS = "W";
			}
			else
			{
				strWS = "S";
			}
			// MAL@20080505: Pass in Filing Date
			// Tracker Reference: 13300
			dtFileDate = dtPassDate;
			if (strAccountList != "")
			{
				strTemp = Strings.Left(strAccountList, strAccountList.Length - 1);
				// kk        rsData.OpenRecordset "SELECT * FROM Bill WHERE ID IN (" & strTemp & ") ORDER BY OName", strUTDatabase
				strDataSQL = "SELECT * FROM Bill WHERE ID IN (" + strTemp + ")";
				rsData.OpenRecordset(strDataSQL + " ORDER BY OName", modExtraModules.strUTDatabase);
				StartLienEditReportQuery();
				if (rsData.RecordCount() > 0)
				{
					lngCount = rsData.RecordCount();
				}
				else
				{
					lngCount = 0;
				}
				// Me.Show
			}
			else
			{
				this.Close();
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void StartLienEditReportQuery()
		{
			try
			{
				if (rsData.EndOfFile())
				{
					lblAccount.Left = 0;
					lblAccount.Width = this.PrintWidth;
					lblAccount.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Center;
					lblAccount.Text = "No lien records for the year " + FCConvert.ToString(intYear) + " were found.";
					HideAllFields();
				}
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data", true, rsData.RecordCount() + 1);
				// dtMailDate = CDate(frmRateRecChoice.txtMailDate.Text)
				dtMailDate = DateAndTime.DateValue(FCConvert.ToString(DateTime.Now));
				if (frmRateRecChoice.InstancePtr.cmbReportDetail.SelectedIndex < 0)
				{
					intReportDetail = 1;
				}
				else
				{
					intReportDetail = frmRateRecChoice.InstancePtr.cmbReportDetail.ItemData(frmRateRecChoice.InstancePtr.cmbReportDetail.SelectedIndex);
				}
				rsLN.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
				frmWait.InstancePtr.IncrementProgress();
				rsUT.OpenRecordset("SELECT Master.ID, DeedName1 as OwnerName FROM Master ", modExtraModules.strUTDatabase);
				frmWait.InstancePtr.Show(App.MainForm);
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description, "Start Lien Edit Report Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
			// save this report
			if (this.Document.Pages.Count > 0)
			{
				if (boolWater)
				{
					modGlobalFunctions.IncrementSavedReports("LastWUTTranfserToLien");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTTranfserToLien1.RDF"));
				}
				else
				{
					modGlobalFunctions.IncrementSavedReports("LastSUTTranfserToLien");
					this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTTranfserToLien1.RDF"));
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// format the report
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			lngPosBal = 0;
			lngZeroBal = 0;
			lngNegBal = 0;
			lngDemandAlready = 0;
			SetupDetailSection();
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
			frmTransferTaxToLien.InstancePtr.Unload();
			frmRateRecChoice.InstancePtr.Unload();
			// frmRateRecChoice.Show , MDIParent
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			// this will return the correct SQL string
			string strFields = "";
			string strWhereClause = "";
			int intCT;
			if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Account")
			{
				// range of accounts
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text)) + " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Account >= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[0].Text));
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Account <= " + FCConvert.ToString(Conversion.Val(frmRateRecChoice.InstancePtr.txtRange[1].Text));
					}
					else
					{
						// both empty
						strWhereClause = "";
					}
				}
			}
			else if (frmRateRecChoice.InstancePtr.cmbRange.Text == "Range by Name")
			{
				// range of names
				if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[0].Text) != "")
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// both full
						// strWhereClause = " AND Name1 BETWEEN '" & .txtRange(0).Text & "' AND '" & .txtRange(1).Text & "'"
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "    ' AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'";
					}
					else
					{
						// first full second empty
						strWhereClause = " AND Name1 >= '" + frmRateRecChoice.InstancePtr.txtRange[0].Text + "   '";
					}
				}
				else
				{
					if (Strings.Trim(frmRateRecChoice.InstancePtr.txtRange[1].Text) != "")
					{
						// first empty second full
						strWhereClause = " AND Name1 <= '" + frmRateRecChoice.InstancePtr.txtRange[1].Text + "ZZZZ'";
					}
					else
					{
						// both empty
						strWhereClause = "";
					}
				}
			}
			else
			{
				strWhereClause = "";
			}
			// strSQL = "SELECT * FROM BillingMaster WHERE BillingYear \ 10 = " & intYear & " AND BillingType = 'RE'" & strWhereClause & " ORDER BY Account"
			strSQL = "SELECT * FROM " + modMain.Statics.gstrBillingMasterYear + " WHERE BillingType = 'RE'" + strWhereClause + " ORDER BY Name1";
			// kgk 07312021    rsData.CreateStoredProcedure "LienEditReportQuery", strSQL
			modMain.Statics.gstrLienEditReportQuery = strSQL;
			// kgk    strSQL = "SELECT * FROM LienEditReportQuery"
			strSQL = "SELECT * FROM " + modMain.Statics.gstrLienEditReportQuery;
			BuildSQL = strSQL;
			return BuildSQL;
		}

		private void BindFields()
		{
			// this will fill the fields in
			double dblCurInt = 0;
			double dblTotal = 0;
			double dblPrin = 0;
			double dblInt = 0;
			double dblTax = 0;
			float lngNextLine = 0;
			double dblDemandFeeAmount = 0;
			clsDRWrapper rsCombineBills = new clsDRWrapper();
			double dblCost = 0;
			double dblBillDue = 0;
			double dblBillInt = 0;
			double dblBillPrin = 0;
			double dblBillTax = 0;
			double dblBillCost = 0;
			int lngIndex;
			double dblChrgInt;
			double dblBillChgInt = 0;
			double dblAddedCosts;
			// MAL@20080505
			clsDRWrapper rsTempInfo = new clsDRWrapper();
			clsDRWrapper rsPayment = new clsDRWrapper();
			TRYAGAIN:
			// this is where the program will start again if the current account is not acceptable
			if (rsData.EndOfFile() != true)
			{
				frmWait.InstancePtr.IncrementProgress();
				rsTempInfo.OpenRecordset("SELECT * FROM (" + strDataSQL + ") AS qTmpY WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey"), "TWUT0000.vb1");
				if (rsTempInfo.EndOfFile() != true && rsTempInfo.BeginningOfFile() != true)
				{
					do
					{
						rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND BillKey = " + rsTempInfo.Get_Fields_Int32("ID") + " AND Reference = 'DEMAND'", modExtraModules.strUTDatabase);
						if (rsPayment.RecordCount() > 0)
						{
							while (!rsPayment.EndOfFile())
							{
								dblDemandFeeAmount += FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost") * -1);
								rsPayment.MoveNext();
							}
						}
						else
						{
							dblDemandFeeAmount = dblDemandFeeAmount;
						}
						rsTempInfo.MoveNext();
					}
					while (rsTempInfo.EndOfFile() != true);
				}
				else
				{
					lngIndex = -1;
				}
				if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) != 0)
				{
					rsLN.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
					if (!rsLN.NoMatch)
					{
						rsCombineBills.OpenRecordset("SELECT * FROM Bill WHERE " + strWS + "LienRecordNumber = " + rsData.Get_Fields(strWS + "LienRecordNumber"), modExtraModules.strUTDatabase);
						if (rsCombineBills.RecordCount() > 1)
						{
							dblCost = 0;
							dblBillPrin = 0;
							dblBillTax = 0;
							dblBillInt = 0;
							dblBillCost = 0;
							rsCombineBills.MoveFirst();
							while (!rsCombineBills.EndOfFile())
							{
								// this will calculate the principal, interest and total due
								dblBillInt = 0;
								double dblPerDiem = 0;
								bool boolForcePerDiem = false;
								bool blnForceBillIntDate = false;
								dblBillDue = modUTCalculations.CalculateAccountUT(rsCombineBills, ref dtFileDate, ref dblBillInt, boolWater, ref dblBillPrin, ref dblBillChgInt, ref dblBillCost, false, false, DateTime.Now, ref dblBillTax, ref dblPerDiem, ref boolForcePerDiem, ref blnForceBillIntDate);
								if (dblBillDue > 0)
								{
									if (dblDemandFeeAmount == 0)
									{
										dblDemandFeeAmount = (rsCombineBills.Get_Fields(strWS + "CostAdded") * -1) - rsCombineBills.Get_Fields(strWS + "CostPaid");
									}
								}
								rsCombineBills.MoveNext();
							}
						}
						else
						{
							dblCost = (rsData.Get_Fields(strWS + "CostOwed") - rsData.Get_Fields(strWS + "CostAdded") - rsData.Get_Fields(strWS + "CostPaid"));
						}
						// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
						dblPrin = FCConvert.ToDouble(rsLN.Get_Fields("Principal"));
						// TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
						// TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
						dblInt = FCConvert.ToDouble(rsLN.Get_Fields("Interest") - rsLN.Get_Fields("IntAdded"));
						// + dblChrgInt      'MAL@20070913: Add already charged interest
						// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
						dblTax = FCConvert.ToDouble(rsLN.Get_Fields("Tax"));
						// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
						dblCost = FCConvert.ToDouble((rsLN.Get_Fields("Costs") - rsLN.Get_Fields_Double("CostPaid")) - dblDemandFeeAmount);
						dblTotal = modUTCalculations.CalculateAccountUTLien(rsLN, dtFileDate, ref dblCurInt, boolWater);
						if (FCUtils.Round(dblTotal, 2) == 0)
						{
							rsData.MoveNext();
							goto TRYAGAIN;
						}
						else
						{
							fldAccount.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey")));
							if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName2"))) != "")
							{
								fldName.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName"))) + " and " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName2")));
							}
							else
							{
								fldName.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("OName")));
							}
							fldPrincipal.Text = Strings.Format(dblPrin, "#,##0.00");
							fldPLInt.Text = Strings.Format(dblInt, "#,##0.00");
							fldTax.Text = Strings.Format(dblTax, "#,##0.00");
							fldCosts.Text = Strings.Format(dblCost, "#,##0.00");
							fldDemand.Text = Strings.Format(dblDemandFeeAmount, "#,##0.00");
							dblTotals[0] += dblPrin;
							dblTotals[1] += dblInt;
							// MAL@20080505
							// dblTotals(2) = dblTotals(2) + rsLN.Fields("Costs") - rsLN.Fields("MaturityFee") - dblDemandFeeAmount
							dblTotals[2] += dblCost;
							dblTotals[3] += dblCurInt;
							dblTotals[4] += dblDemandFeeAmount;
							// this will shwo the demand fee on each line seperately
							dblTotals[5] += dblTax;
							// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
							if (FCUtils.Round(dblTotal, 2) == FCUtils.Round((dblPrin + dblTax + dblInt + rsLN.Get_Fields("Costs") - rsLN.Get_Fields("MaturityFee")) - (rsLN.Get_Fields("PrinPaid") + rsLN.Get_Fields("IntPaid") + rsLN.Get_Fields_Double("CostPaid")), 2))
							{
								// - dblCurInt
								fldTotal.Text = Strings.Format(dblTotal, "#,##0.00");
							}
							else
							{
								// TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
								// TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
								fldTotal.Text = Strings.Format((dblPrin + dblTax + dblInt + rsLN.Get_Fields("Costs") - rsLN.Get_Fields("MaturityFee")) - (rsLN.Get_Fields("PrinPaid") + rsLN.Get_Fields("IntPaid") + rsLN.Get_Fields_Double("CostPaid")), "#,##0.00");
								// - dblCurInt
							}
						}
					}
					else
					{
						fldTotal.Text = "ERROR";
					}
				}
				else
				{
				}
				rsUT.FindFirstRecord("ID", rsData.Get_Fields_Int32("AccountKey"));
				if (!rsUT.NoMatch)
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsUT.Get_Fields("OwnerName"))) == FCConvert.ToString(rsData.Get_Fields_String("OName")))
					{
						fldCO.Top = 0;
						fldCO.Visible = false;
						fldCO.Text = "";
						lngNextLine = 180 / 1440F;
					}
					else
					{
						fldCO.Top = 180 / 1440f;
						fldCO.Visible = true;
						// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
						fldCO.Text = "C/O " + rsUT.Get_Fields("OwnerName");
						lngNextLine = 360 / 1440F;
					}
				}
				else
				{
					fldCO.Top = 0;
					fldCO.Visible = false;
					fldCO.Text = "";
					lngNextLine = 180 / 1440F;
				}
				switch (intReportDetail)
				{
					case 1:
					case 2:
						{
							sarLienEditMortHolders.Top = lngNextLine + 100 / 1440f;
							sarLienEditMortHolders.Report.UserData = modUTStatusPayments.GetAccountNumber_2(rsData.Get_Fields_Int32("AccountKey")) * -1;
							break;
						}
				}
				//end switch
				rsData.MoveNext();
			}
			else
			{
				fldTotal.Text = "";
				fldPrincipal.Text = "";
				fldAccount.Text = "";
				fldCosts.Text = "";
				// fldCurrentInt.Text = ""
				fldPLInt.Text = "";
				fldName.Text = "";
			}
			rsCombineBills.Dispose();
			rsPayment.Dispose();
			rsTempInfo.Dispose();
		}

		private int ReturnAccountIndex(ref int lngBK)
		{
			int ReturnAccountIndex = 0;
			// MAL@20070913: Added to get the demand fees in a similar way to the Notice Summary
			// Call: 307971
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will return the array index for the account passed in...-1 if not found
				int lngCT;
				bool boolFound = false;
				for (lngCT = 0; lngCT <= Information.UBound(modUTLien.Statics.arrDemand, 1); lngCT++)
				{
					if (modUTLien.Statics.arrDemand[lngCT].BillKey == lngBK)
					{
						boolFound = true;
						break;
					}
				}
				if (boolFound)
				{
					ReturnAccountIndex = lngCT;
				}
				else
				{
					ReturnAccountIndex = -1;
				}
				return ReturnAccountIndex;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Array Index", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
			return ReturnAccountIndex;
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			// fill the totals line in
			fldTotalPrincipal.Text = Strings.Format(dblTotals[0], "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTotals[5], "#,##0.00");
			fldTotalPLInt.Text = Strings.Format(dblTotals[1], "#,##0.00");
			fldTotalCosts.Text = Strings.Format(dblTotals[2], "#,##0.00");
			// fldTotalCurrentInt.Text = Format(dblTotals(3), "#,##0.00")
			fldTotalDemand.Text = Strings.Format(dblTotals[4], "#,##0.00");
			lblTotals.Text = "Count " + FCConvert.ToString(lngCount) + "  Total:";
			fldTotalTotal.Text = Strings.Format(dblTotals[0] + dblTotals[1] + dblTotals[2] + dblTotals[4] + dblTotals[5], "#,##0.00");
			// + dblTotals(3)
		}

		private void HideAllFields()
		{
			lblCosts.Visible = false;
			// lblCurrentInt.Visible = False
			lblName.Visible = false;
			lblPLInt.Visible = false;
			lblPrincipal.Visible = false;
			lblReportType.Visible = false;
			lblTotal.Visible = false;
			lblTotals.Visible = false;
			lblDemand.Visible = false;
			Line1.Visible = false;
			Line2.Visible = false;
			fldTotalCosts.Visible = false;
			// fldTotalCurrentInt.Visible = False
			fldTotalPLInt.Visible = false;
			fldTotalPrincipal.Visible = false;
			fldTotalTotal.Visible = false;
			fldTotalDemand.Visible = false;
		}

		private void SetupDetailSection()
		{
			// this will set the height of the detail section and the visibility of the sub report
			switch (intReportDetail)
			{
				case 0:
					{
						sarLienEditMortHolders.Visible = false;
						sarLienEditMortHolders.Top = 0;
						break;
					}
				case 1:
				case 2:
					{
						sarLienEditMortHolders.Visible = true;
						sarLienEditMortHolders.Report = new sarLienEditMortHolders();
						break;
					}
			}
			//end switch
		}

		private void arLienTransferReport_Load(object sender, System.EventArgs e)
		{
		}
	}
}
