﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptReminderPostCard.
	/// </summary>
	public partial class rptReminderPostCard : BaseSectionReport
	{
		public rptReminderPostCard()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Reminder Notice - Post Card";
		}

		public static rptReminderPostCard InstancePtr
		{
			get
			{
				return (rptReminderPostCard)Sys.GetInstance(typeof(rptReminderPostCard));
			}
		}

		protected rptReminderPostCard _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsTemp.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptReminderPostCard	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/02/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               10/31/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsTemp = new clsDRWrapper();
		bool boolDeleteLastPage;
		bool boolReturnAddress;
		string strBulkMailer = "";
		bool boolWater;
		int lngIndex;
		bool boolNoSummary;
		bool boolSendTenant;
		bool boolSendOwner;
		bool boolSecondCopy;
		bool boolLienedRecords;
		string strRateKeyList = "";

		public void Init(ref string strSQL, ref string strPrinter, ref bool boolPassWater, ref bool boolPassLienedRecords, ref string strPassRateKeyList)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will be used to pass variables into the report
				boolReturnAddress = FCConvert.CBool(frmReminderNotices.InstancePtr.vsGrid.TextMatrix(4, 1) == "Yes");
				boolLienedRecords = boolPassLienedRecords;
				if (Strings.Trim(strPassRateKeyList) != "")
				{
					if (frmReminderNotices.InstancePtr.boolLienedRecords)
					{
						strRateKeyList = " AND RateKey IN" + strPassRateKeyList;
					}
					else
					{
						strRateKeyList = " AND BillingRateKey IN" + strPassRateKeyList;
					}
				}
				if (frmReminderNotices.InstancePtr.chkBulkMailing.CheckState == Wisej.Web.CheckState.Checked)
				{
					strBulkMailer = modMain.GetBulkMailerString();
				}
				else
				{
					strBulkMailer = "";
				}
				boolSendTenant = FCConvert.CBool(frmReminderNotices.InstancePtr.chkSendTo[0].CheckState == Wisej.Web.CheckState.Checked);
				boolSendOwner = FCConvert.CBool(frmReminderNotices.InstancePtr.chkSendTo[1].CheckState == Wisej.Web.CheckState.Checked);
				FCUtils.EraseSafe(modReminderNoticeSummary.Statics.arrReminderSummaryList);
				modReminderNoticeSummary.Statics.arrReminderSummaryList = new modReminderNoticeSummary.ReminderSummaryList[0 + 1];
				lngIndex = 0;
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (rsData.EndOfFile())
				{
					frmWait.InstancePtr.Unload();
					MessageBox.Show("No records meet the criteria selected.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					boolNoSummary = true;
					this.Close();
				}
				else
				{
					boolNoSummary = false;
					this.PageSettings.PaperHeight = 4;
					lblMailingAddress5.Top = ((4 * 1440) - 256) / 1440f;
					// this sits on the bottom of the form to make sure that the detail section is still large enough
					this.PageSettings.PaperWidth = ((8.5F * 1440) + 1) / 1440f;
					frmReportViewer.InstancePtr.Init(this);
				}
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Notices", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (boolDeleteLastPage)
			{
				this.Document.Pages.RemoveAt(this.Document.Pages.Count - 1);
				//this.Document.Pages.Commit();
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			//this.Zoom = -1;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			if (!boolNoSummary)
			{
                fecherFoundation.Sys.ClearInstance(frmReportViewer.InstancePtr);
                modReminderNoticeSummary.Statics.intRPTReminderSummaryType = 1;
				frmReportViewer.InstancePtr.Init(rptReminderSummary.InstancePtr);
				//rptReminderSummary.InstancePtr.Show(App.MainForm);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			TRYAGAIN:
			;
			if (!rsData.EndOfFile())
			{
				if (boolWater)
				{
					rsTemp.OpenRecordset("SELECT AccountKey, ActualAccountNumber, SUM(TotalOwed) as TotalPOwed, SUM(TotalPaid) as TotalPPaid FROM (SELECT AccountKey, ActualAccountNumber, SUM(WPrinOwed) as TotalOwed, SUM(WPrinPaid) as TotalPaid FROM Bill WHERE Service <> 'S' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND WLienRecordNumber = 0 GROUP BY AccountKey, ActualAccountNumber UNION SELECT AccountKey, ActualAccountNumber, SUM(Principal) AS TotalOwed, SUM(PrinPaid) as TotalPaid FROM Bill INNER JOIN Lien ON Bill.WLienRecordNumber = Lien.ID WHERE Service <> 'S' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND WCombinationLienKey = Bill.ID GROUP BY AccountKey, ActualAccountNumber) AS qTmp GROUP BY AccountKey, ActualAccountNumber");
				}
				else
				{
					rsTemp.OpenRecordset("SELECT AccountKey, ActualAccountNumber, SUM(TotalOwed) as TotalPOwed, SUM(TotalPaid) as TotalPPaid FROM (SELECT AccountKey, ActualAccountNumber, SUM(SPrinOwed) as TotalOwed, SUM(SPrinPaid) as TotalPaid FROM Bill WHERE Service <> 'W' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND SLienRecordNumber = 0 GROUP BY AccountKey, ActualAccountNumber UNION SELECT AccountKey, ActualAccountNumber, SUM(Principal) AS TotalOwed, SUM(PrinPaid) as TotalPaid FROM Bill INNER JOIN Lien ON Bill.SLienRecordNumber = Lien.ID WHERE Service <> 'W' AND ActualAccountNumber = " + rsData.Get_Fields_Int32("ActualAccountNumber") + " AND SCombinationLienKey = Bill.ID GROUP BY AccountKey, ActualAccountNumber) AS qTmp GROUP BY AccountKey, ActualAccountNumber");
				}
				PrintPostCards();
				if (!boolSecondCopy)
				{
					rsData.MoveNext();
				}
			}
		}

		private void PrintPostCards()
		{
			// this will fill in the fields for post card
			int intCTRLIndex;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			double dblDue;
			double dblPaid;
			int lngAcctKey = 0;
			clsDRWrapper rsMaster = new clsDRWrapper();
			GETNEXTACCOUNT:
			;
			lngAcctKey = modUTStatusPayments.GetAccountKeyUT_2(rsData.Get_Fields_Int32("ActualAccountNumber"));
			rsMaster.OpenRecordset(modUTStatusPayments.UTMasterQuery(lngAcctKey), modExtraModules.strUTDatabase);
			if (boolSendTenant && !boolSecondCopy)
			{
				boolSecondCopy = false;
				if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name2"))) != "")
				{
					str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name"))) + " and " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name2")));
				}
				else
				{
					str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name")));
				}
				// this will fill the address form the master screen for the mail address
				str2 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress1")));
				str3 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress2")));
				str4 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BAddress3")));
				str5 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BCity"))) + " " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BState"))) + " " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("BZip")));
				// If Trim(.Fields("BZip4")) <> "" Then
				// str5 = str5 & "-" & Trim(.Fields("BZip4"))
				// End If
				if (boolSendOwner)
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("Name"))) != Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName"))))
					{
						boolSecondCopy = true;
					}
				}
			}
			else if (boolSendOwner || boolSecondCopy)
			{
				boolSecondCopy = false;
				// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
				if (Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("SecondOwnerName"))) != "")
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
					str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName"))) + " and " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("SecondOwnerName")));
				}
				else
				{
					// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
					str1 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields("OwnerName")));
				}
				// this will fill the address form the master screen for the mail address
				str2 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress1")));
				str3 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress2")));
				str4 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OAddress3")));
				str5 = Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OCity"))) + " " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OState"))) + " " + Strings.Trim(FCConvert.ToString(rsMaster.Get_Fields_String("OZip")));
				// If Trim(.Fields("OZip4")) <> "" Then
				// str5 = str5 & "-" & Trim(.Fields("OZip4"))
				// End If
			}
			// condense the labels if some are blank
			if (Strings.Trim(str4) == string.Empty)
			{
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str3) == string.Empty)
			{
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str2) == string.Empty)
			{
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			if (Strings.Trim(str1) == string.Empty)
			{
				str1 = str2;
				str2 = str3;
				str3 = str4;
				str4 = str5;
				str5 = "";
			}
			fldBulkMailer.Text = strBulkMailer;
			// TODO Get_Fields: Field [TotalPOwed] not found!! (maybe it is an alias?)
			dblDue = rsTemp.Get_Fields("TotalPOwed");
			// TODO Get_Fields: Field [TotalPPaid] not found!! (maybe it is an alias?)
			dblPaid = rsTemp.Get_Fields("TotalPPaid");
			if (boolReturnAddress)
			{
				// Return Address
				lblReturnAddress1.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(1, 0);
				lblReturnAddress2.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(2, 0);
				lblReturnAddress3.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(3, 0);
				lblReturnAddress4.Text = frmReminderNotices.InstancePtr.vsReturnAddress.TextMatrix(4, 0);
			}
			Array.Resize(ref modReminderNoticeSummary.Statics.arrReminderSummaryList, lngIndex + 1);
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Name1 = str1;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str2;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str3;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str4;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Addr1 = str5;
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Total = dblDue - dblPaid;
			// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			modReminderNoticeSummary.Statics.arrReminderSummaryList[lngIndex].Account = FCConvert.ToInt32(rsMaster.Get_Fields("AccountNumber"));
			// Account Number
			// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			lblAccount.Text = "Account: " + rsMaster.Get_Fields("AccountNumber");
			// Message
			lblMessage.Text = frmReminderNotices.InstancePtr.vsGrid.TextMatrix(3, 1);
			// Name
			lblName.Text = str1;
			// Mailing Address
			lblMailingAddress1.Text = str2;
			lblMailingAddress2.Text = str3;
			lblMailingAddress3.Text = str4;
			lblMailingAddress4.Text = str5;
			// increment the index of the array
			lngIndex += 1;
			rsMaster.Dispose();
		}

		private void rptReminderPostCard_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptReminderPostCard properties;
			//rptReminderPostCard.Caption	= "Reminder Notice - Post Card";
			//rptReminderPostCard.Icon	= "rptReminderPostCard.dsx":0000";
			//rptReminderPostCard.Left	= 0;
			//rptReminderPostCard.Top	= 0;
			//rptReminderPostCard.Width	= 11880;
			//rptReminderPostCard.Height	= 8595;
			//rptReminderPostCard.StartUpPosition	= 3;
			//rptReminderPostCard.SectionData	= "rptReminderPostCard.dsx":058A;
			//End Unmaped Properties
		}
	}
}
