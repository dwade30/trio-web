﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptDemandFeeList.
	/// </summary>
	public partial class rptDemandFeeList : BaseSectionReport
	{
		public static rptDemandFeeList InstancePtr
		{
			get
			{
				return (rptDemandFeeList)Sys.GetInstance(typeof(rptDemandFeeList));
			}
		}

		protected rptDemandFeeList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}

		public rptDemandFeeList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Demand Fees List";
		}
		// nObj = 1
		//   0	rptDemandFeeList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/11/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/11/2004              *
		// ********************************************************
		int lngTotalAccounts;
		int lngCount;
		// this will keep track of which row I am on in the grid
		double dblDemandFee;
		bool boolDone;
		double dblTotalDemand;
		double dblTotalCMF;
		double dblTotalPrin;
		bool boolReverse;
		bool boolWater;

		public void Init(ref int lngPassTotalAccounts, ref double dblPassDemand, ref bool boolPassWater, bool boolPassReverse = false)
		{
			lngTotalAccounts = lngPassTotalAccounts;
			dblDemandFee = dblPassDemand;
			boolReverse = boolPassReverse;
			boolWater = boolPassWater;
			// Me.Show
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (!modUTLien.Statics.arrDemand[lngCount].Used && boolDone)
			{
				eArgs.EOF = true;
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			// save this report for future access
			if (this.Document.Pages.Count > 0)
			{
				if (boolWater)
				{
					if (boolReverse)
					{
						modGlobalFunctions.IncrementSavedReports("LastWUTReverseDemand");
						this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTReverseDemand1.RDF"));
					}
					else
					{
						modGlobalFunctions.IncrementSavedReports("LastWUTDemandFeesList");
						this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastWUTDemandFeesList1.RDF"));
					}
				}
				else
				{
					if (boolReverse)
					{
						modGlobalFunctions.IncrementSavedReports("LastSUTReverseDemand");
						this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTReverseDemand1.RDF"));
					}
					else
					{
						modGlobalFunctions.IncrementSavedReports("LastSUTDemandFeesList");
						this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastSUTDemandFeesList1.RDF"));
					}
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			if (boolReverse)
			{
				lblHeader.Text = "Reverse Demand Fees List";
			}
			else
			{
				lblHeader.Text = "Demand Fees List";
			}
			lngCount = 0;
		}

		private void BindFields()
		{
			// this will fill the information into the fields
			if (!boolDone)
			{
				// this will find the next available account that was processed
				while (!(modUTLien.Statics.arrDemand[lngCount].Processed || lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1)))
				{
					lngCount += 1;
				}
				// check to see if we are at the end of the list
				if (lngCount >= Information.UBound(modUTLien.Statics.arrDemand, 1))
				{
					boolDone = true;
					// clear the fields so there are no repeats
					fldAcct.Text = "";
					fldName.Text = "";
					fldDemand.Text = "";
					fldCMF.Text = "";
					fldTotal.Text = "";
					return;
				}
				fldAcct.Text = FCConvert.ToString(modUTStatusPayments.GetAccountNumber(ref modUTLien.Statics.arrDemand[lngCount].Account));
				fldName.Text = modUTLien.Statics.arrDemand[lngCount].Name;
				fldDemand.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].Fee, "#,##0.00");
				if (modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee != 0)
				{
					fldCMF.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee - modUTLien.Statics.arrDemand[lngCount].Fee, "#,##0.00");
					fldTotal.Text = Strings.Format(modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee, "#,##0.00");
					dblTotalCMF += modUTLien.Statics.arrDemand[lngCount].CertifiedMailFee - modUTLien.Statics.arrDemand[lngCount].Fee;
				}
				else
				{
					fldCMF.Text = "0.00";
					fldTotal.Text = fldDemand.Text;
				}
				dblTotalDemand += modUTLien.Statics.arrDemand[lngCount].Fee;
				// move to the next record in the array
				lngCount += 1;
			}
			else
			{
				// clear the fields
				fldAcct.Text = "";
				fldName.Text = "";
				fldDemand.Text = "";
				fldCMF.Text = "";
				fldTotal.Text = "";
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void SetupTotals()
		{
			// this sub will fill in the footer line at the bottom of the report
			lblNumberOfAccounts.Text = "There were " + FCConvert.ToString(lngTotalAccounts) + " accounts processed";
			// lblTotalPrin.Caption = Format(dblTotalPrin, "#,##0.00")
			lblTotalCMF.Text = Strings.Format(dblTotalCMF, "#,##0.00");
			lblTotalDemand.Text = Strings.Format(dblTotalDemand, "#,##0.00");
			lblTotalTotal.Text = Strings.Format(dblTotalCMF + dblTotalDemand, "#,##0.00");
			// + dblTotalPrin
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			SetupTotals();
		}

		private void rptDemandFeeList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptDemandFeeList properties;
			//rptDemandFeeList.Caption	= "Demand Fees List";
			//rptDemandFeeList.Icon	= "rptDemandFeesList.dsx":0000";
			//rptDemandFeeList.Left	= 0;
			//rptDemandFeeList.Top	= 0;
			//rptDemandFeeList.Width	= 11880;
			//rptDemandFeeList.Height	= 8595;
			//rptDemandFeeList.StartUpPosition	= 3;
			//rptDemandFeeList.SectionData	= "rptDemandFeesList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
