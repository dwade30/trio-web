﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Drawing;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmApplyDemandFees.
	/// </summary>
	partial class frmApplyDemandFees : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public fecherFoundation.FCPanel fraValidate;
		public FCGrid vsValidate;
		public fecherFoundation.FCLabel lblValidateInstruction;
		public fecherFoundation.FCPanel fraGrid;
		public fecherFoundation.FCGrid vsDemand;
		public fecherFoundation.FCLabel lblInstructionHeader;
		public fecherFoundation.FCLabel lblInstruction;
		public fecherFoundation.FCButton cmdFileClear;
		public fecherFoundation.FCButton cmdFileSelectAll;
		public fecherFoundation.FCButton cmdFilePrint;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmApplyDemandFees));
			this.cmbWS = new fecherFoundation.FCComboBox();
			this.lblWS = new fecherFoundation.FCLabel();
			this.fraValidate = new fecherFoundation.FCPanel();
			this.vsValidate = new fecherFoundation.FCGrid();
			this.lblValidateInstruction = new fecherFoundation.FCLabel();
			this.fraGrid = new fecherFoundation.FCPanel();
			this.vsDemand = new fecherFoundation.FCGrid();
			this.lblInstructionHeader = new fecherFoundation.FCLabel();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.cmdFileClear = new fecherFoundation.FCButton();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.cmdFilePrint = new fecherFoundation.FCButton();
			this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraValidate)).BeginInit();
			this.fraValidate.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsValidate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).BeginInit();
			this.fraGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 1500);
			this.BottomPanel.Size = new System.Drawing.Size(900, 10);
            // 
            // ClientArea
            // 

            this.ClientArea.Controls.Add(this.cmdFilePrint);
            this.ClientArea.Controls.Add(this.cmbWS);
			this.ClientArea.Controls.Add(this.lblWS);
			this.ClientArea.Controls.Add(this.fraGrid);
			this.ClientArea.Controls.Add(this.fraValidate);
			this.ClientArea.Size = new System.Drawing.Size(900, 623);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileClear);
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Size = new System.Drawing.Size(900, 60);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(235, 30);
			this.HeaderText.Text = "Apply Demand Fees";
			// 
			// cmbWS
			// 
			this.cmbWS.AutoSize = false;
			this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWS.FormattingEnabled = true;
			this.cmbWS.Items.AddRange(new object[] {
				"Water",
				"Sewer"
			});
			this.cmbWS.Location = new System.Drawing.Point(188, 30);
			this.cmbWS.Name = "cmbWS";
			this.cmbWS.Size = new System.Drawing.Size(189, 40);
			this.cmbWS.TabIndex = 0;
			this.cmbWS.Text = "Water";
			// 
			// lblWS
			// 
			this.lblWS.AutoSize = true;
			this.lblWS.Location = new System.Drawing.Point(30, 44);
			this.lblWS.Name = "lblWS";
			this.lblWS.Size = new System.Drawing.Size(107, 15);
			this.lblWS.TabIndex = 1;
			this.lblWS.Text = "WATER / SEWER";
			this.lblWS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblWS.Visible = false;
			// 
			// fraValidate
			// 
			this.fraValidate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraValidate.Controls.Add(this.vsValidate);
			this.fraValidate.Controls.Add(this.lblValidateInstruction);
			this.fraValidate.Location = new System.Drawing.Point(0, 0);
			this.fraValidate.Name = "fraValidate";
			this.fraValidate.Size = new System.Drawing.Size(897, 520);
			this.fraValidate.TabIndex = 3;
			this.fraValidate.Visible = false;
			// 
			// vsValidate
			// 
			this.vsValidate.AllowSelection = false;
			this.vsValidate.AllowUserToResizeColumns = false;
			this.vsValidate.AllowUserToResizeRows = false;
			this.vsValidate.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsValidate.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsValidate.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsValidate.BackColorBkg = System.Drawing.Color.Empty;
			this.vsValidate.BackColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.BackColorSel = System.Drawing.Color.Empty;
			this.vsValidate.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsValidate.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.vsValidate.ColumnHeadersHeight = 30;
			this.vsValidate.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsValidate.DefaultCellStyle = dataGridViewCellStyle4;
			this.vsValidate.DragIcon = null;
			this.vsValidate.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsValidate.ExtendLastCol = true;
			this.vsValidate.FixedCols = 0;
			this.vsValidate.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.FrozenCols = 0;
			this.vsValidate.GridColor = System.Drawing.Color.Empty;
			this.vsValidate.GridColorFixed = System.Drawing.Color.Empty;
			this.vsValidate.Location = new System.Drawing.Point(30, 94);
			this.vsValidate.Name = "vsValidate";
			this.vsValidate.ReadOnly = true;
			this.vsValidate.RowHeadersVisible = false;
			this.vsValidate.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsValidate.RowHeightMin = 0;
			this.vsValidate.Rows = 1;
			this.vsValidate.ScrollTipText = null;
			this.vsValidate.ShowColumnVisibilityMenu = false;
			this.vsValidate.Size = new System.Drawing.Size(837, 390);
			this.vsValidate.StandardTab = true;
			this.vsValidate.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsValidate.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsValidate.TabIndex = 4;
			// 
			// lblValidateInstruction
			// 
			this.lblValidateInstruction.Location = new System.Drawing.Point(30, 30);
			this.lblValidateInstruction.Name = "lblValidateInstruction";
			this.lblValidateInstruction.Size = new System.Drawing.Size(422, 43);
			this.lblValidateInstruction.TabIndex = 5;
			this.lblValidateInstruction.Visible = false;
			// 
			// fraGrid
			// 
			this.fraGrid.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraGrid.AppearanceKey = "groupBoxNoBorders";
			this.fraGrid.Controls.Add(this.vsDemand);
			this.fraGrid.Controls.Add(this.lblInstructionHeader);
			this.fraGrid.Controls.Add(this.lblInstruction);
			this.fraGrid.Location = new System.Drawing.Point(0, 0);
			this.fraGrid.Name = "fraGrid";
			this.fraGrid.Size = new System.Drawing.Size(897, 520);
			this.fraGrid.TabIndex = 0;
			this.fraGrid.Visible = false;
			// 
			// vsDemand
			// 
			this.vsDemand.AllowSelection = false;
			this.vsDemand.AllowUserToResizeColumns = false;
			this.vsDemand.AllowUserToResizeRows = false;
			this.vsDemand.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsDemand.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDemand.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDemand.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDemand.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.BackColorSel = System.Drawing.Color.Empty;
			this.vsDemand.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDemand.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsDemand.ColumnHeadersHeight = 30;
			this.vsDemand.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDemand.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsDemand.DragIcon = null;
			this.vsDemand.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDemand.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsDemand.ExtendLastCol = true;
			this.vsDemand.FixedCols = 0;
			this.vsDemand.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.FrozenCols = 0;
			this.vsDemand.GridColor = System.Drawing.Color.Empty;
			this.vsDemand.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.Location = new System.Drawing.Point(30, 170);
			this.vsDemand.Name = "vsDemand";
			this.vsDemand.ReadOnly = true;
			this.vsDemand.RowHeadersVisible = false;
			this.vsDemand.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDemand.RowHeightMin = 0;
			this.vsDemand.Rows = 1;
			this.vsDemand.ScrollTipText = null;
			this.vsDemand.ShowColumnVisibilityMenu = false;
			this.vsDemand.Size = new System.Drawing.Size(837, 340);
			this.vsDemand.StandardTab = true;
			this.vsDemand.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDemand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDemand.TabIndex = 1;
			this.vsDemand.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDemand_AfterEdit);
			this.vsDemand.CurrentCellChanged += new System.EventHandler(this.vsDemand_RowColChange);
			this.vsDemand.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDemand_MouseMoveEvent);
			this.vsDemand.DoubleClick += new System.EventHandler(this.vsDemand_DblClick);
			// 
			// lblInstructionHeader
			// 
			this.lblInstructionHeader.Location = new System.Drawing.Point(30, 30);
			this.lblInstructionHeader.Name = "lblInstructionHeader";
			this.lblInstructionHeader.Size = new System.Drawing.Size(596, 18);
			this.lblInstructionHeader.TabIndex = 6;
			this.lblInstructionHeader.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// lblInstruction
			// 
			this.lblInstruction.Location = new System.Drawing.Point(30, 90);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(596, 60);
			this.lblInstruction.TabIndex = 2;
			this.lblInstruction.Visible = false;
			// 
			// cmdFileClear
			// 
			this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClear.AppearanceKey = "toolbarButton";
			this.cmdFileClear.Location = new System.Drawing.Point(765, 29);
			this.cmdFileClear.Name = "cmdFileClear";
			this.cmdFileClear.Size = new System.Drawing.Size(108, 24);
			this.cmdFileClear.TabIndex = 1;
			this.cmdFileClear.Text = "Clear Selection";
			this.cmdFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(687, 29);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(75, 24);
			this.cmdFileSelectAll.TabIndex = 2;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// cmdFilePrint
			// 
			this.cmdFilePrint.AppearanceKey = "acceptButton";
			this.cmdFilePrint.Location = new System.Drawing.Point(621, 863);
			this.cmdFilePrint.Name = "cmdFilePrint";
			this.cmdFilePrint.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFilePrint.Size = new System.Drawing.Size(202, 48);
			this.cmdFilePrint.TabIndex = 0;
			this.cmdFilePrint.Text = "Apply Demand Fees";
			this.cmdFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
			// 
			// frmApplyDemandFees
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(900, 791);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmApplyDemandFees";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Apply Demand Fees";
			this.Load += new System.EventHandler(this.frmApplyDemandFees_Load);
			this.Activated += new System.EventHandler(this.frmApplyDemandFees_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmApplyDemandFees_KeyPress);
			this.Resize += new System.EventHandler(this.frmApplyDemandFees_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraValidate)).EndInit();
			this.fraValidate.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsValidate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).EndInit();
			this.fraGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFilePrint)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
