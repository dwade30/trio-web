﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptMeterImportErrorReport.
	/// </summary>
	public partial class rptMeterImportErrorReport : BaseSectionReport
	{
		public rptMeterImportErrorReport()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Meter Import Error Report";
		}

		public static rptMeterImportErrorReport InstancePtr
		{
			get
			{
				return (rptMeterImportErrorReport)Sys.GetInstance(typeof(rptMeterImportErrorReport));
			}
		}

		protected rptMeterImportErrorReport _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMeterImportErrorReport	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               04/04/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               01/06/2014              *
		// ********************************************************
		////clsDRWrapper rsData = new clsDRWrapper();
		////clsDRWrapper rsDataW = new clsDRWrapper();
		////clsDRWrapper rsDataS = new clsDRWrapper();
		int lngIndex;
		int lngMax;
		int intType;

		public void Init(short intPassType)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				intType = intPassType;
				// 0 - Aqua America, 1 - RVS, 2 - Badger, 3 - EZ Reader, 4 - Bangor NDS, 5 - TRIO Windows, 6 - Hampden
				switch (intType)
				{
					case 0:
					case 1:
					case 2:
					case 3:
					case 4:
					case 5:
					case 6:
						{
							frmReportViewer.InstancePtr.Init(this);
							break;
						}
					default:
						{
							this.Close();
							break;
						}
				}
				//end switch
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Error Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = FCConvert.CBool(lngIndex > lngMax);
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				switch (intType)
				{
					case 0:
						{
							// AA
							lngMax = Information.UBound(modTSCONSUM.Statics.MeterImportError, 1);
							break;
						}
					case 1:
						{
							// RVS
							lngMax = Information.UBound(modTSCONSUM.Statics.RVSMeterImportError, 1);
							break;
						}
					case 2:
						{
							// Badger
							lngMax = Information.UBound(modTSCONSUM.Statics.RVSMeterImportError, 1);
							break;
						}
					case 3:
						{
							// EZ Reader
							lngMax = Information.UBound(modTSCONSUM.Statics.EZImportError, 1);
							break;
						}
					case 4:
						{
							// Bangor NDS
							lngMax = Information.UBound(modTSCONSUM.Statics.BangorMeterImportError, 1);
							break;
						}
					case 5:
						{
							// TRIO Windows
							lngMax = Information.UBound(modTSCONSUM.Statics.CRMeterImportError, 1);
							break;
						}
					case 6:
						{
							// Hampden
							lngMax = Information.UBound(modTSCONSUM.Statics.HampdenImportError, 1);
							break;
						}
                    case 7:
                    {
                        lngMax = modTSCONSUM.Statics.TopshamImportError.GetUpperBound(1);
                        break;
                    }
				}
				//end switch
				FillHeaderLabels();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (lngIndex <= lngMax)
				{
					// use the information from the meterimporterror array of structs
					switch (intType)
					{
						case 0:
							{
								// AA
								fldAccountNumber.Text = modTSCONSUM.Statics.MeterImportError[lngIndex].AccountNumber + modTSCONSUM.Statics.MeterImportError[lngIndex].ResidentNumber;
								fldName.Text = modTSCONSUM.Statics.MeterImportError[lngIndex].Name;
								fldAddress1.Text = modTSCONSUM.Statics.MeterImportError[lngIndex].Address;
								fldReading.Text = modTSCONSUM.Statics.MeterImportError[lngIndex].Reading;
								fldReadingDate.Text = Strings.Format(modTSCONSUM.Statics.MeterImportError[lngIndex].ReadingDate, "##/##/##");
								fldReadingType.Text = modTSCONSUM.Statics.MeterImportError[lngIndex].ReadingType;
								lngIndex += 1;
								break;
							}
						case 1:
							{
								// RVS
								fldAccountNumber.Text = modTSCONSUM.Statics.RVSMeterImportError[lngIndex].Account.ToString();
								fldName.Text = modTSCONSUM.Statics.RVSMeterImportError[lngIndex].Name;
								fldAddress1.Text = modTSCONSUM.Statics.RVSMeterImportError[lngIndex].Address1;
								fldReading.Text = modTSCONSUM.Statics.RVSMeterImportError[lngIndex].Current.ToString();
								fldReadingDate.Text = Strings.Format(modTSCONSUM.Statics.RVSMeterImportError[lngIndex].ReadingDate, "MM/dd/yyyy");
								lblReadingType.Text = "Route :";
								fldReadingType.Text = modTSCONSUM.Statics.RVSMeterImportError[lngIndex].Route.ToString();
								lngIndex += 1;
								break;
							}
						case 2:
							{
								// Badger
								fldAccountNumber.Text = modTSCONSUM.Statics.BadgerMeterImportError[lngIndex].AccountNumber;
								fldName.Text = modTSCONSUM.Statics.BadgerMeterImportError[lngIndex].CustomerName;
								fldAddress1.Text = modTSCONSUM.Statics.BadgerMeterImportError[lngIndex].ServiceAddress;
								fldReading.Text = modTSCONSUM.Statics.BadgerMeterImportError[lngIndex].CurrentReading;
								fldReadingDate.Text = Strings.Format(modTSCONSUM.Statics.BadgerMeterImportError[lngIndex].ReadDate, "MM/dd/yyyy");
								// lblReadingType.Caption = "Route :"
								fldReadingType.Text = modTSCONSUM.Statics.BadgerMeterImportError[lngIndex].ReadCode1;
								lngIndex += 1;
								break;
							}
						case 3:
							{
								// EZ Reader
								fldAccountNumber.Text = modTSCONSUM.Statics.EZImportError[lngIndex].Account;
								fldName.Text = modTSCONSUM.Statics.EZImportError[lngIndex].Name;
								fldAddress1.Text = modTSCONSUM.Statics.EZImportError[lngIndex].Location;
								fldReading.Text = modTSCONSUM.Statics.EZImportError[lngIndex].Current;
								fldReadingDate.Text = Strings.Format(modTSCONSUM.Statics.EZImportError[lngIndex].ReadingDate, "MM/dd/yyyy");
								lblReadingType.Text = "Route :";
								fldReadingType.Text = modTSCONSUM.Statics.EZImportError[lngIndex].Route;
								lngIndex += 1;
								break;
							}
						case 4:
							{
								// Bangor NDS
								fldAccountNumber.Text = modTSCONSUM.Statics.BangorMeterImportError[lngIndex].AccountNumber.ToString();
								fldName.Text = modTSCONSUM.Statics.BangorMeterImportError[lngIndex].CustomerName.ToString();
								fldAddress1.Text = modTSCONSUM.Statics.BangorMeterImportError[lngIndex].ServiceAddress.ToString();
								fldReading.Text = modTSCONSUM.Statics.BangorMeterImportError[lngIndex].CurrentReading.ToString();
								fldReadingDate.Text = Strings.Format(modTSCONSUM.Statics.BangorMeterImportError[lngIndex].ReadDate, "MM/dd/yy");
								lblReadingType.Visible = true;
								lblReadingType.Text = "Acct Not Found";
								fldReadingType.Text = "";
								lngIndex += 1;
								break;
							}
						case 5:
							{
								// TRIO Windows
								fldAccountNumber.Text = modTSCONSUM.Statics.CRMeterImportError[lngIndex].AcctNumber;
								fldName.Text = modTSCONSUM.Statics.CRMeterImportError[lngIndex].Name;
								fldAddress1.Text = modTSCONSUM.Statics.CRMeterImportError[lngIndex].Location;
								fldReading.Text = Strings.Format(modTSCONSUM.Statics.CRMeterImportError[lngIndex].Current, "#######0");
								lblReadingDate.Text = "";
								// fldReadingDate.Text = Format(.ReadingDate, "MM/dd/yyyy")
								lblReadingType.Text = "";
								fldReadingType.Text = "";
								lngIndex += 1;
								break;
							}
					}
					//end switch
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields In Error Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalCount.Text = lngIndex.ToString();
		}

		private void rptMeterImportErrorReport_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMeterImportErrorReport properties;
			//rptMeterImportErrorReport.Caption	= "Meter Import Error Report";
			//rptMeterImportErrorReport.Icon	= "rptMeterImportErrorReport.dsx":0000";
			//rptMeterImportErrorReport.Left	= 0;
			//rptMeterImportErrorReport.Top	= 0;
			//rptMeterImportErrorReport.Width	= 11805;
			//rptMeterImportErrorReport.Height	= 8595;
			//rptMeterImportErrorReport.WindowState	= 2;
			//rptMeterImportErrorReport.SectionData	= "rptMeterImportErrorReport.dsx":058A;
			//End Unmaped Properties
		}
	}
}
