﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Diagnostics;
using fecherFoundation.DataBaseLayer;

namespace TWUT0000
{
	public class modCoreysUTCode
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Corey Gray              *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               04/14/2005              *
		// ********************************************************
		public const int CNSTCUSTOMBILLTYPEOUTPRINT = -1;
		public const int CNSTUTPRINTBILLTYPECOMBINED = 0;
		// These are loaded as virtual fields to tell which
		public const int CNSTUTPRINTBILLTYPEWATER = 1;
		// bill we are printing when doing combined bills
		public const int CNSTUTPRINTBILLTYPESEWER = 2;
		// since we need to split them if the water and sewer have separate billing addresses
		public struct BreakDownType
		{
			public int Key;
			public int BillKey;
			public int MeterKey;
			public string Description;
			public int Value;
			public Decimal Amount;
			public string Type;
			public int RateKey;
			public string Service;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public BreakDownType(int unusedParam)
			{
				this.Key = 0;
				this.BillKey = 0;
				this.MeterKey = 0;
				this.Description = string.Empty;
				this.Value = 0;
				this.Amount = 0;
				this.Type = string.Empty;
				this.RateKey = 0;
				this.Service = string.Empty;
			}
		};

		public struct MeterInfoType
		{
			public int MeterKey;
			public int BillKey;
			public int AccountKey;
			// vbPorter upgrade warning: Value As int	OnWrite(int, double)
			public int Value;
			public double Amount;
			public string Type;
			public string Service;
			public string Frequency;
			public string FrequencyShort;
			public string Previous;
			public DateTime PreviousDate;
			public string Current;
			public DateTime CurrentDate;
			public string CurrentCode;
			public double Tax;
			public string PreviousWater;
			public string CurrentWater;
			public string PreviousSewer;
			public string CurrentSewer;
			// vbPorter upgrade warning: Consumption As int	OnWrite(short, double)
			public int Consumption;
			public double TaxWater;
			public double TaxSewer;
			public double AmountWater;
			public double AmountSewer;
			public double Units;
			// kgk 12-07-2011 trout-789  Change to eliminate truncation  ' As Long
			public double UnitsSewer;
			// As Long
			public double UnitsWater;
			// As Long
			public int ValueWater;
			public int ValueSewer;
			public bool ChangedOut;
			// kgk 10-18-2011 trout-765  Flag to indicate meter changed out
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public MeterInfoType(int unusedParam)
			{
				this.MeterKey = 0;
				this.BillKey = 0;
				this.AccountKey = 0;
				this.Value = 0;
				this.Amount = 0;
				this.Type = string.Empty;
				this.Service = string.Empty;
				this.Frequency = string.Empty;
				this.FrequencyShort = string.Empty;
				this.Previous = string.Empty;
				this.PreviousDate = default(DateTime);
				this.Current = string.Empty;
				this.CurrentDate = default(DateTime);
				this.CurrentCode = string.Empty;
				this.Tax = 0;
				this.PreviousWater = string.Empty;
				this.CurrentWater = string.Empty;
				this.PreviousSewer = string.Empty;
				this.CurrentSewer = string.Empty;
				this.Consumption = 0;
				this.TaxWater = 0;
				this.TaxSewer = 0;
				this.AmountWater = 0;
				this.AmountSewer = 0;
				this.Units = 0;
				this.UnitsSewer = 0;
				this.UnitsWater = 0;
				this.ValueWater = 0;
				this.ValueSewer = 0;
				this.ChangedOut = false;
			}
		};

		public struct UTBill
		{
			public string strBillTo;
			public string strName;
			public string strAddress1;
			public string strAddress2;
			public string strCity;
			public string strState;
			public string strZip;
			public string strLocation;
			public int lngAccount;
			public string strUserMessage1;
			public string strUserMessage2;
			public int lngBook;
			public int lngSeq;
			public DateTime dtBillDate;
			public DateTime dtPeriodEnding;
			public string strRateClass;
			public Decimal CurrentWater;
			public Decimal CurrentSewer;
			public Decimal PastDue;
			public int lngConsumption;
			public bool boolCurActual;
			public DateTime dtCurDate;
			public int lngCurReading;
			public DateTime dtPrevDate;
			public int lngPrevReading;
			public bool boolPrevActual;
			public short intBillingCode;
			// 0 Flat 1 units 2 consumption 3 adjustment
			public Decimal WFlatAmount;
			// water flat amount
			public Decimal WUnitsAmount;
			public Decimal WConsumptionAmount;
			public Decimal SFlatAmount;
			public Decimal SUnitsAmount;
			public Decimal SConsumptionAmount;
			public Decimal WaterTax;
			public Decimal SewerTax;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public UTBill(int unusedParam)
			{
				this.strBillTo = string.Empty;
				this.strName = string.Empty;
				this.strAddress1 = string.Empty;
				this.strAddress2 = string.Empty;
				this.strCity = string.Empty;
				this.strState = string.Empty;
				this.strZip = string.Empty;
				this.strLocation = string.Empty;
				this.lngAccount = 0;
				this.strUserMessage1 = string.Empty;
				this.strUserMessage2 = string.Empty;
				this.lngBook = 0;
				this.lngSeq = 0;
				this.dtBillDate = default(DateTime);
				this.dtPeriodEnding = default(DateTime);
				this.strRateClass = string.Empty;
				this.CurrentWater = 0;
				this.CurrentSewer = 0;
				this.PastDue = 0;
				this.lngConsumption = 0;
				this.boolCurActual = false;
				this.dtCurDate = default(DateTime);
				this.lngCurReading = 0;
				this.dtPrevDate = default(DateTime);
				this.lngPrevReading = 0;
				this.boolPrevActual = false;
				this.intBillingCode = 0;
				this.WFlatAmount = 0;
				this.WUnitsAmount = 0;
				this.WConsumptionAmount = 0;
				this.SFlatAmount = 0;
				this.SUnitsAmount = 0;
				this.SConsumptionAmount = 0;
				this.WaterTax = 0;
				this.SewerTax = 0;
			}
		};

		const int CNSTDETAILBREAKDOWNBILLNUM = 1;
		const int CNSTDETAILBREAKDOWNDESCRIPTION = 2;
		const int CNSTDETAILBREAKDOWNAMOUNT = 3;
		// ===============================================================
		// kgk trout-746  Changed the order of detail lines for Oakland
		// Private Const CNSTDETAILBREAKDOWNROWREGULAR = 1
		// Private Const CNSTDETAILBREAKDOWNROWPASTDUE = 3
		// Private Const CNSTDETAILBREAKDOWNROWLIENS = 4
		// Private Const CNSTDETAILBREAKDOWNROWINTEREST = 5
		// Private Const CNSTDETAILBREAKDOWNROWTAX = 2
		// Private Const CNSTDETAILBREAKDOWNROWMISC = 6
		// Private Const CNSTDETAILBREAKDOWNROWADJ = 7
		// ===============================================================
		const int CNSTDETAILBREAKDOWNROWREGULAR = 1;
		const int CNSTDETAILBREAKDOWNROWMISC = 2;
		const int CNSTDETAILBREAKDOWNROWADJ = 3;
		const int CNSTDETAILBREAKDOWNROWTAX = 4;
		const int CNSTDETAILBREAKDOWNROWINTEREST = 5;
		const int CNSTDETAILBREAKDOWNROWPASTDUE = 6;
		const int CNSTDETAILBREAKDOWNROWLIENS = 7;
		const int CNSTDETAILBREAKDOWNROWCOST = 8;
		// kgk 11-11-2011 trout-780
		// kgk 06-25-2012 trout-839  Breakdown of regular to unit, consumption and flat
		const int CNSTDETAILBREAKDOWNROWCONS = 1;
		const int CNSTDETAILBREAKDOWNROWUNIT = 2;
		const int CNSTDETAILBREAKDOWNROWFLAT = 3;

		public static void GetReadingUnitsOnBill()
		{
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				Statics.dblRRatio = 0;
				rsLoad.OpenRecordset("select * from utilitybilling", modExtraModules.strUTDatabase);
				if (!rsLoad.EndOfFile())
				{
					Statics.dblReadingUnitsOnBill = Conversion.Val(rsLoad.Get_Fields_Int32("readingunitsonbill"));
					if (Statics.dblReadingUnitsOnBill == 0)
						Statics.dblReadingUnitsOnBill = 1;
					Statics.dblRRatio = Conversion.Val(rsLoad.Get_Fields_Int32("READINGUNITS")) / Statics.dblReadingUnitsOnBill;
				}
				if (Statics.dblReadingUnitsOnBill == 0)
					Statics.dblReadingUnitsOnBill = 1;
				if (Statics.dblRRatio == 0)
					Statics.dblRRatio = 1;
				Statics.dblRRatio = FCConvert.ToDouble(Strings.Format(Statics.dblRRatio, "0.000"));
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number" + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetReadingUnitsOnBill", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void SetBillTypeForPrinting_8(bool boolWater, bool boolSewer)
		{
			SetBillTypeForPrinting(ref boolWater, ref boolSewer);
		}

		public static void SetBillTypeForPrinting(ref bool boolWater, ref bool boolSewer)
		{
			Statics.boolUseSewerBills = boolSewer;
			Statics.boolUseWaterBills = boolWater;
		}
		// vbPorter upgrade warning: clsRep As object	OnWrite(clsDRWrapper)
		public static string HandleRegularCase(ref clsDRWrapper clsRep, ref modCustomBill.CustomBillCodeType clsCCode)
		{
			string HandleRegularCase = "";
			string strTemp;
			// Takes a code number and looks up the data based on the field name in the database
			try
			{
				// On Error GoTo ErrorHandler
				HandleRegularCase = "";
				strTemp = FCConvert.ToString(clsRep.Get_Fields(clsCCode.FieldName));
				switch ((DataTypeEnum)clsCCode.Datatype)
				{
					case DataTypeEnum.dbDate:
						{
							if (clsRep.Get_Fields(clsCCode.FieldName) == 0)
							{
								strTemp = "";
							}
							else
							{
								if (clsCCode.FormatString != string.Empty)
								{
									strTemp = Strings.Format(strTemp, clsCCode.FormatString);
								}
								else
								{
									strTemp = Strings.Format(strTemp, "MM/dd/yyyy");
								}
							}
							break;
						}
					default:
						{
							if (Strings.Trim(clsCCode.FormatString) != string.Empty)
							{
								strTemp = Strings.Format(strTemp, clsCCode.FormatString);
							}
							break;
						}
				}
				//end switch
				HandleRegularCase = strTemp;
				return HandleRegularCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleRegularCase with code " + FCConvert.ToString(clsCCode.FieldID), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleRegularCase;
		}
		// vbPorter upgrade warning: clsRep As object	OnWrite(clsDRWrapper)
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		public static string HandleSpecialCase(ref clsDRWrapper clsRep, ref modCustomBill.CustomBillCodeType clsCCode, ref string strExtraParameters)
		{
			string HandleSpecialCase = "";
			// always module specific.
			// vbPorter upgrade warning: dblTemp As double	OnWrite(short, double, string)	OnRead(double, string)
			double dblTemp = 0;
			// vbPorter upgrade warning: strTemp As string	OnWrite(int, string, double)
			string strTemp = "";
			// vbPorter upgrade warning: dttemp As DateTime	OnWrite(string, short)
			DateTime dttemp;
			string[] strAry = null;
			DateTime dtTemp2;
			// vbPorter upgrade warning: lngTemp As int	OnWrite(int, double)
			int lngTemp = 0;
			int lngtemp2 = 0;
			int x;
			int Y;
			bool boolTemp = false;
			double[] dblAry = null;
			clsDRWrapper clsTemp = new clsDRWrapper();
			double dblTemp2 = 0;
			double dblInt1 = 0;
			double dblInt2 = 0;
			double dblInt3 = 0;
			double dblInt4 = 0;
			bool boolOV = false;
			try
			{
				// On Error GoTo ErrorHandler
				HandleSpecialCase = "";
				int vbPorterVar = clsCCode.FieldID;
				if (vbPorterVar == modCustomBill.CNSTCUSTOMBILLCUSTOMBARCODE)
				{
					// make it the current account
					strTemp = FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey")));
					strTemp = Strings.Format(FCConvert.ToString(Conversion.Val(strTemp)), "0000000");
					strTemp = "U" + strTemp;
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 1)
				{
					// current reading date
					// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
					strTemp = GetMeterList_6(clsRep.Get_Fields("bill"), "0", clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					if (strTemp != string.Empty)
					{
						strTemp = "";
						if (Information.UBound(Statics.aryMeterBreakdown, 1) > 0)
						{
							if (Statics.aryMeterBreakdown[1].CurrentDate.ToOADate() != 0)
							{
								strTemp = Strings.Format(Statics.aryMeterBreakdown[1].CurrentDate, strExtraParameters);
							}
						}
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 2)
				{
					// current reading for specified meter(s)
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					Debug.WriteLine(clsRep.Get_Fields_Int32("accountkey"));
					if (Information.UBound(strAry, 1) < 0)
					{
						if (clsRep.Get_Fields_Int32("CurReading") != -1)
						{
							// kk trout-884 111312  Handle No Read
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsRep.Get_Fields_Int32("curreading"))));
							lngTemp = FCConvert.ToInt32(lngTemp * Statics.dblRRatio);
							strTemp = Strings.Format(lngTemp, "#,###,##0");
						}
						else
						{
							strTemp = "";
							// kk return "", "-1", or "NR"?
						}
						HandleSpecialCase = strTemp;
						return HandleSpecialCase;
						// strTemp = GetMeterList(clsRep.Fields("Bill"), "0", clsRep.Fields("accountkey"), clsRep.Fields("MeterKey"))
					}
					else
					{
						// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
						strTemp = GetMeterList(clsRep.Get_Fields("Bill"), ref strAry[0], clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					}
					if (strTemp != string.Empty)
					{
						strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
						lngTemp = 0;

						if (strAry[0] == "0")
						{
							HandleSpecialCase = Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(Statics.aryMeterBreakdown[1].Current))) * Statics.dblRRatio, "#,###,##0");
						}
						else
						{
							if (Conversion.Val(strAry[0]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
							{
								HandleSpecialCase = Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[0])].Current))) * Statics.dblRRatio, "#,###,##0");
							}
						}
						// End If
					}
				}
				else if (vbPorterVar == 3)
				{
					// previous reading date
					// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
					strTemp = GetMeterList_6(clsRep.Get_Fields("bill"), "0", clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					if (strTemp != string.Empty)
					{
						strTemp = "";
						if (Information.UBound(Statics.aryMeterBreakdown, 1) > 0)
						{
							if (Statics.aryMeterBreakdown[1].PreviousDate.ToOADate() != 0)
							{
								strTemp = Strings.Format(Statics.aryMeterBreakdown[1].PreviousDate, strExtraParameters);
							}
						}
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 4)
				{
					// previous reading for specified meter(s)
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					if (Information.UBound(strAry, 1) < 0)
					{
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsRep.Get_Fields_Int32("prevreading"))));
						lngTemp = FCConvert.ToInt32(lngTemp * Statics.dblRRatio);
						strTemp = Strings.Format(lngTemp, "#,###,##0");
						HandleSpecialCase = strTemp;
						return HandleSpecialCase;
						// strTemp = GetMeterList(clsRep.Fields("bill"), "0", clsRep.Fields("accountkey"), clsRep.Fields("MeterKey"))
					}
					else
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						strTemp = GetMeterList(clsRep.Get_Fields("bill"), ref strAry[0], clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					}
					if (strTemp != string.Empty)
					{
						strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
						lngTemp = 0;

						if (strAry[0] == "0")
						{
							HandleSpecialCase = Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(Statics.aryMeterBreakdown[1].Previous))) * Statics.dblRRatio, "#,###,##0");
						}
						else
						{
							if (Conversion.Val(strAry[0]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
							{
								HandleSpecialCase = Strings.Format(FCConvert.ToDouble(FCConvert.ToString(Conversion.Val(Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[0])].Previous))) * Statics.dblRRatio, "#,###,##0");
							}
						}
						// End If
					}
				}
				else if (vbPorterVar == 5)
				{
					// consumption
					// changed to read consumption from bill if all meters are specified
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					// kk01232015 trout-1061  Add option to get actual consumption, not the billed consumption which could be override/minimum
					if (Information.UBound(strAry, 1) > 1)
					{
						boolTemp = FCConvert.CBool(strAry[2] == "Actual");
					}
					else
					{
						boolTemp = false;
					}
					if (Information.UBound(strAry, 1) < 0)
					{
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsRep.Get_Fields_Int32("CONSUMPTION"))));
						lngTemp = FCConvert.ToInt32(lngTemp * Statics.dblRRatio);
						strTemp = Strings.Format(lngTemp, "#,###,##0");
						HandleSpecialCase = strTemp;
						return HandleSpecialCase;
						// strTemp = GetMeterList(clsRep.Fields("bill"), "0", clsRep.Fields("accountkey"), clsRep.Fields("MeterKey"))
					}
					else if (strAry[0] == "0")
					{
						lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsRep.Get_Fields_Int32("CONSUMPTION"))));
						lngTemp = FCConvert.ToInt32(lngTemp * Statics.dblRRatio);
						strTemp = Strings.Format(lngTemp, "#,###,##0");
						HandleSpecialCase = strTemp;
						return HandleSpecialCase;
					}
					else
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						strTemp = GetMeterList(clsRep.Get_Fields("bill"), ref strAry[0], clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					}
					if (strTemp != string.Empty)
					{
						strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
						lngTemp = 0;
						dblTemp = 0;
						if (Conversion.Val(strAry[0]) > 0)
						{
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								if (Conversion.Val(strAry[x]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
								{
									if (boolTemp)
									{
										// kk01232015 trout-1061  Add option to show actual consumption, not billed because it could be override/minimum
										lngTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Consumption;
									}
									else
									{
										lngTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Value;
									}
									dblTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Units;
								}
								else
								{
									boolTemp = false;
									// kk01232015 trout-1061  If the selected meter number doesn't exist then force back to Blank consumption
								}
							}
							// x
						}
						else
						{
							for (x = 1; x <= Information.UBound(Statics.aryMeterBreakdown, 1); x++)
							{
								if (boolTemp)
								{
									// kk01232015 trout-1061  Add option to show actual consumption, not billed because it could be override/minimum
									lngTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Consumption;
								}
								else
								{
									lngTemp += Statics.aryMeterBreakdown[x].Value;
								}
								dblTemp += Statics.aryMeterBreakdown[x].Units;
							}
							// x
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(clsRep.Get_Fields_Int32("consumption"))));
						}
						if (!boolTemp)
						{
							// kk01232015 trout-1061  Make sure without the option still works the same
							strTemp = "";
							if (lngTemp != 0)
							{
								lngTemp = FCConvert.ToInt32(lngTemp * Statics.dblRRatio);
								strTemp = Strings.Format(lngTemp, "#,###,##0");
							}
						}
						else
						{
							strTemp = Strings.Format(lngTemp, "#,###,##0");
						}
						if (dblTemp > 0)
						{
							strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
							if (Information.UBound(strAry, 1) > 0)
							{
								// check to see if they chose to show units
								if (Strings.UCase(strAry[1]) == "YES")
								{
									strTemp += "\r\n" + "Units = " + FCConvert.ToString(dblTemp);
								}
							}
						}
						HandleSpecialCase = strTemp;
					}
				}
				else if (vbPorterVar == 6)
				{
					// location
					strTemp = "";
					if (Strings.UCase(strExtraParameters) == "WATER")
					{
						if (Statics.boolUseWaterBills)
						{
							strTemp = clsRep.Get_Fields_String("location");
						}
					}
					else if (Strings.UCase(strExtraParameters) == "SEWER")
					{
						if (Statics.boolUseSewerBills)
						{
							strTemp = clsRep.Get_Fields_String("location");
						}
					}
					else if (Strings.UCase(strExtraParameters) == "ALWAYS")
					{
						strTemp = clsRep.Get_Fields_String("location");
					}
					else
					{
						strTemp = clsRep.Get_Fields_String("location");
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 9)
				{
					// water prin owed
					if (Statics.boolUseWaterBills)
					{
						dblTemp = 0;
						if (Strings.UCase(strExtraParameters) == "DO NOT INCLUDE")
						{
							dblTemp = Conversion.Val(clsRep.Get_Fields_Double("wprinowed")) - Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) - Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount"));
							if (dblTemp < 0)
								dblTemp = 0;
						}
						else
						{
							dblTemp = Conversion.Val(clsRep.Get_Fields_Double("wprinowed"));
						}
						HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 15)
				{
					// Total Current Water owed
					if (Statics.boolUseWaterBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("wprinowed")) + Conversion.Val(clsRep.Get_Fields_Double("WTaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("WPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("WTaxPaid"));
						HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 16)
				{
					// sewer prin owed
					if (Statics.boolUseSewerBills)
					{
						dblTemp = 0;
						if (Strings.UCase(strExtraParameters) == "DO NOT INCLUDE")
						{
							dblTemp = Conversion.Val(clsRep.Get_Fields_Double("sprinowed")) - Conversion.Val(clsRep.Get_Fields_Decimal("sadjustamount")) - Conversion.Val(clsRep.Get_Fields_Decimal("sdeadjustamount"));
							if (dblTemp < 0)
								dblTemp = 0;
						}
						else
						{
							dblTemp = Conversion.Val(clsRep.Get_Fields_Double("sprinowed"));
						}
						HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 20)
				{
					// Total Current Water Paid
					if (Statics.boolUseWaterBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("wprinpaid")) + Conversion.Val(clsRep.Get_Fields_Double("WTaxPaid"));
						HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
					
				}
				else if ((vbPorterVar == 31) || (vbPorterVar == 126))
				{
					// billing name 1 and billing name 1 trunc (trout-836)
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (Strings.UCase(strAry[0]) == "WATER")
					{
						if (Statics.boolUseWaterBills)
						{
							if (boolTemp)
							{
								strTemp = clsRep.Get_Fields_String("oname");
							}
							else
							{
								strTemp = clsRep.Get_Fields_String("bname");
							}
						}
					}
					else if (Strings.UCase(strAry[0]) == "SEWER")
					{
						if (Statics.boolUseSewerBills)
						{
							if (boolTemp)
							{
								strTemp = clsRep.Get_Fields_String("oname");
							}
							else
							{
								strTemp = clsRep.Get_Fields_String("bname");
							}
						}
					}
					else if (Strings.UCase(strAry[0]) == "ALWAYS")
					{
						if (boolTemp)
						{
							strTemp = clsRep.Get_Fields_String("oname");
						}
						else
						{
							strTemp = clsRep.Get_Fields_String("bname");
						}
					}
					else
					{
						if (boolTemp)
						{
							strTemp = clsRep.Get_Fields_String("oname");
						}
						else
						{
							strTemp = clsRep.Get_Fields_String("bname");
						}
					}
					if (clsCCode.FieldID == 31)
					{
						HandleSpecialCase = strTemp;
					}
					else
					{
						// trout-836
						if (Information.UBound(strAry, 1) >= 1)
						{
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(strAry[1]))));
						}
						else
						{
							lngTemp = strTemp.Length;
						}
						if (lngTemp == 0)
						{
							lngTemp = strTemp.Length;
						}
						lngtemp2 = Strings.InStr(1, strTemp, " ", CompareConstants.vbBinaryCompare) - 1;
						if (lngtemp2 > 0 && lngtemp2 < lngTemp)
						{
							lngTemp = lngtemp2;
						}
						lngtemp2 = Strings.InStr(1, strTemp, ",", CompareConstants.vbBinaryCompare) - 1;
						if (lngtemp2 > 0 && lngtemp2 < lngTemp)
						{
							lngTemp = lngtemp2;
						}
						HandleSpecialCase = Strings.Left(strTemp, lngTemp);
					}
				}
				else if (vbPorterVar == 32)
				{
					// billing name 2
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (Strings.UCase(strExtraParameters) == "WATER")
					{
						if (Statics.boolUseWaterBills)
						{
							if (boolTemp)
							{
								strTemp = clsRep.Get_Fields_String("oname2");
							}
							else
							{
								strTemp = clsRep.Get_Fields_String("bname2");
							}
						}
					}
					else if (Strings.UCase(strExtraParameters) == "SEWER")
					{
						if (Statics.boolUseSewerBills)
						{
							if (boolTemp)
							{
								strTemp = clsRep.Get_Fields_String("oname2");
							}
							else
							{
								strTemp = clsRep.Get_Fields_String("bname2");
							}
						}
					}
					else if (Strings.UCase(strExtraParameters) == "ALWAYS")
					{
						if (boolTemp)
						{
							strTemp = clsRep.Get_Fields_String("oname2");
						}
						else
						{
							strTemp = clsRep.Get_Fields_String("bname2");
						}
					}
					else
					{
						if (boolTemp)
						{
							strTemp = clsRep.Get_Fields_String("oname2");
						}
						else
						{
							strTemp = clsRep.Get_Fields_String("bname2");
						}
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 33)
				{
					// billingaddress1
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						strTemp = clsRep.Get_Fields_String("oaddress1");
					}
					else
					{
						strTemp = clsRep.Get_Fields_String("Baddress1");
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 34)
				{
					// billingaddress2
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						strTemp = clsRep.Get_Fields_String("oaddress2");
					}
					else
					{
						strTemp = clsRep.Get_Fields_String("baddress2");
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 35)
				{
					// billingaddress3
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						strTemp = clsRep.Get_Fields_String("oaddress3");
					}
					else
					{
						strTemp = clsRep.Get_Fields_String("baddress3");
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 36)
				{
					// billing city
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						strTemp = clsRep.Get_Fields_String("ocity");
					}
					else
					{
						strTemp = clsRep.Get_Fields_String("bcity");
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 37)
				{
					// billing state
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						strTemp = clsRep.Get_Fields_String("ostate");
					}
					else
					{
						strTemp = clsRep.Get_Fields_String("bstate");
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 38)
				{
					// billing zip
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						strTemp = clsRep.Get_Fields_String("ozip");
					}
					else
					{
						strTemp = clsRep.Get_Fields_String("bzip");
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 39)
				{
					// billing zip4
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						strTemp = clsRep.Get_Fields_String("ozip4");
					}
					else
					{
						strTemp = clsRep.Get_Fields_String("bzip4");
					}
					HandleSpecialCase = strTemp;					
				}
				else if (vbPorterVar == 41)
				{
					// complete billing address. Will auto condense
					strTemp = "";
					boolTemp = true;
					// default to owner
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						if (Strings.Trim(clsRep.Get_Fields_String("OName")) != string.Empty)
						{
							strTemp += Strings.Trim(clsRep.Get_Fields_String("OName"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("OName2")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(clsRep.Get_Fields_String("OName2"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("OAddress1")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(clsRep.Get_Fields_String("OAddress1"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("OAddress2")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(clsRep.Get_Fields_String("OAddress2"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("OAddress3")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(clsRep.Get_Fields_String("OAddress3"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("OCity")) != string.Empty || Strings.Trim(clsRep.Get_Fields_String("OState")) != string.Empty || Strings.Trim(clsRep.Get_Fields_String("OZip")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(Strings.Trim(Strings.Trim(clsRep.Get_Fields_String("OCity")) + "  " + Strings.Trim(clsRep.Get_Fields_String("OState"))) + "  " + Strings.Trim(clsRep.Get_Fields_String("OZip")) + " " + Strings.Trim(clsRep.Get_Fields_String("OZip4")));
						}
					}
					else
					{
						if (Strings.Trim(clsRep.Get_Fields_String("BName")) != string.Empty)
						{
							strTemp += Strings.Trim(clsRep.Get_Fields_String("BName"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("BName2")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(clsRep.Get_Fields_String("BName2"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("BAddress1")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(clsRep.Get_Fields_String("BAddress1"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("BAddress2")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(clsRep.Get_Fields_String("BAddress2"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("BAddress3")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(clsRep.Get_Fields_String("BAddress3"));
						}
						if (Strings.Trim(clsRep.Get_Fields_String("BCity")) != string.Empty || Strings.Trim(clsRep.Get_Fields_String("BState")) != string.Empty || Strings.Trim(clsRep.Get_Fields_String("BZip")) != string.Empty)
						{
							strTemp += "\r\n";
							strTemp += Strings.Trim(Strings.Trim(Strings.Trim(clsRep.Get_Fields_String("BCity")) + "  " + Strings.Trim(clsRep.Get_Fields_String("BState"))) + "  " + Strings.Trim(clsRep.Get_Fields_String("BZip")) + " " + Strings.Trim(clsRep.Get_Fields_String("BZip4")));
						}
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 45)
				{
					// sequence
					HandleSpecialCase = FCConvert.ToString(GetSequenceFromMeterKey_2(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsRep.Get_Fields_Int32("meterkey"))))));
				}
				else if (vbPorterVar == 46)
				{
					// period start
					HandleSpecialCase = Strings.Format(GetPeriodStartFromRate_2(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey"))))), strExtraParameters);
				}
				else if (vbPorterVar == 47)
				{
					// period end
					HandleSpecialCase = Strings.Format(GetPeriodEndFromRate_2(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey"))))), strExtraParameters);
				}
				else if (vbPorterVar == 48)
				{
					// bill date
					HandleSpecialCase = Strings.Format(GetBillingDateFromRate_2(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey"))))), strExtraParameters);
				}
				else if (vbPorterVar == 49)
				{
					// account
					strTemp = "";
					if (Strings.UCase(strExtraParameters) == "WATER")
					{
						if (Statics.boolUseWaterBills)
						{
							strTemp = FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey")));
						}
					}
					else if (Strings.UCase(strExtraParameters) == "SEWER")
					{
						if (Statics.boolUseSewerBills)
						{
							strTemp = FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey")));
						}
					}
					else if (Strings.UCase(strExtraParameters) == "ALWAYS")
					{
						strTemp = FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey")));
					}
					else
					{
						// default to always
						strTemp = FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey")));
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 50)
				{
					// Total Sewer Owed (current)
					if (Statics.boolUseSewerBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("SPRinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("STaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("SPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("STaxPaid"));
						HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 52)
				{
					// total sewer paid (current)
					if (Statics.boolUseSewerBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("Sprinpaid")) + Conversion.Val(clsRep.Get_Fields_Double("staxpaid"));
						HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 54)
				{
					// total paid (current)
					dblTemp = 0;
					if (Statics.boolUseSewerBills)
					{
						dblTemp += Conversion.Val(clsRep.Get_Fields_Double("Sprinpaid")) + Conversion.Val(clsRep.Get_Fields_Double("staxpaid"));
						// dblTemp = dblTemp - Val(clsRep.Fields("SPrinPaid")) - Val(clsRep.Fields("STaxPaid"))
					}
					if (Statics.boolUseWaterBills)
					{
						dblTemp += Conversion.Val(clsRep.Get_Fields_Double("wprinpaid")) + Conversion.Val(clsRep.Get_Fields_Double("Wtaxpaid"));
					}
					HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
				}
				else if (vbPorterVar == 53)
				{
					// Total Owed (water and Sewer)
					dblTemp = 0;
					if (Statics.boolUseSewerBills)
					{
						dblTemp += Conversion.Val(clsRep.Get_Fields_Double("SPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("STaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("SPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("STaxPaid"));
					}
					if (Statics.boolUseWaterBills)
					{
						dblTemp += Conversion.Val(clsRep.Get_Fields_Double("WPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("WTaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("WPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("WTaxPaid"));
					}
					HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
				}
				else if (vbPorterVar == 55)
				{
					// water past due
					// total due - current due
					if (Statics.boolUseWaterBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("WPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("WTaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("WPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("WTaxPaid"));
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblInt1, ref dblInt2, clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
						dblTemp = dblTemp;
						// - dblInt1 - dblInt2
						HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 56)
				{
					// sewer past due
					// total due - current due
					if (Statics.boolUseSewerBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("SPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("STaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("SPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("STaxPaid"));
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), false, false, ref dblInt1, ref dblInt2, clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
						// get rid of interest also
						dblTemp = dblTemp;
						// - dblInt1 - dblInt2
						HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 57)
				{
					// total past due
					// total due - current due
					dblTemp = 0;
					dblInt1 = 0;
					dblInt2 = 0;
					dblInt3 = 0;
					dblInt4 = 0;
					if (Statics.boolUseSewerBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("SPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("STaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("SPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("STaxPaid"));
					}
					if (Statics.boolUseWaterBills)
					{
						dblTemp += Conversion.Val(clsRep.Get_Fields_Double("WPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("WTaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("WPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("WTaxPaid"));
					}
					if (Statics.boolUseWaterBills && Statics.boolUseSewerBills)
					{
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblInt1, ref dblInt2, clsRep.Get_Fields_DateTime("billdate")) + modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), true, false, ref dblInt3, ref dblInt4, clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
					}
					else if (Statics.boolUseWaterBills)
					{
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblInt1, ref dblInt2, clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
					}
					else if (Statics.boolUseSewerBills)
					{
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblInt1, ref dblInt2, clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
					}
					dblTemp += -dblInt1 - dblInt2 - dblInt3 - dblInt4;
					HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
				}
				else if (vbPorterVar == 58)
				{
					// water total owed
					if (Statics.boolUseWaterBills)
					{
						dblTemp = modUTCalculations.CalculateAccountUTTotal_32(clsRep.Get_Fields_Int32("accountkey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, clsRep.Get_Fields_DateTime("billdate"));
						HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 59)
				{
					// sewer total owed
					if (Statics.boolUseSewerBills)
					{
						dblTemp = modUTCalculations.CalculateAccountUTTotal_32(clsRep.Get_Fields_Int32("accountkey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, clsRep.Get_Fields_DateTime("billdate"));
						HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 60)
				{
					// total owed
					dblTemp = 0;
					if (Statics.boolUseWaterBills)
					{
						dblTemp += modUTCalculations.CalculateAccountUTTotal_32(clsRep.Get_Fields_Int32("accountkey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, clsRep.Get_Fields_DateTime("billdate"));
					}
					if (Statics.boolUseSewerBills)
					{
						dblTemp += modUTCalculations.CalculateAccountUTTotal_32(clsRep.Get_Fields_Int32("accountkey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, clsRep.Get_Fields_DateTime("billdate"));
					}
					HandleSpecialCase = Strings.Format(dblTemp, "###,##0.00");
				}
				else if (vbPorterVar == 61)
				{
					// meter code
					// W,S,B
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					strTemp = GetMeterList(clsRep.Get_Fields("Bill"), ref strAry[0], clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					if (strTemp != string.Empty)
					{
						strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
						if (Information.UBound(strAry, 1) > 0)
						{
							if (Conversion.Val(strAry[0]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
							{
								HandleSpecialCase = Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[0])].Service;
								// may want to check all and combine if need be
							}
						}
						else
						{
							if (Conversion.Val(strAry[0]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
							{
								HandleSpecialCase = Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[0])].Service;
							}
						}
					}
				}
				else if (vbPorterVar == 62)
				{
					// water category
					if (Statics.boolUseWaterBills)
					{
						HandleSpecialCase = GetCategoryForAccount_24(clsRep.Get_Fields_Int32("accountkey"), true, false);
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 63)
				{
					// sewer category
					if (Statics.boolUseSewerBills)
					{
						HandleSpecialCase = GetCategoryForAccount_24(clsRep.Get_Fields_Int32("accountkey"), false, false);
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 64)
				{
					// water category FCConvert.ToInt16(
					if (Statics.boolUseWaterBills)
					{
						HandleSpecialCase = GetCategoryForAccount_24(clsRep.Get_Fields_Int32("AccountKey"), true, true);
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 65)
				{
					// sewer category FCConvert.ToInt16(
					if (Statics.boolUseSewerBills)
					{
						HandleSpecialCase = GetCategoryForAccount_24(clsRep.Get_Fields_Int32("accountkey"), false, true);
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 66)
				{
					// water liened
					if (Statics.boolUseWaterBills)
					{
						// HandleSpecialCase = GetLienAmountForAccount(clsRep.Fields("AccountKey"), True)
						HandleSpecialCase = Strings.Format(modUTCalculations.CalculateAccountUTTotal_6(clsRep.Get_Fields_Int32("accountkey"), true, true, clsRep.Get_Fields_DateTime("billdate"), true), "#,###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 67)
				{
					// sewer liened
					if (Statics.boolUseSewerBills)
					{
						// HandleSpecialCase = GetLienAmountForAccount(clsRep.Fields("accountkey"), False)
						HandleSpecialCase = Strings.Format(modUTCalculations.CalculateAccountUTTotal_6(clsRep.Get_Fields_Int32("accountkey"), false, true, clsRep.Get_Fields_DateTime("billdate"), true), "#,###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 68)
				{
					// total liened
					dblTemp = 0;
					if (Statics.boolUseSewerBills)
					{
						// dblTemp = GetLienAmountForAccount(clsRep.Fields("accountkey"), False)
						dblTemp = modUTCalculations.CalculateAccountUTTotal_6(clsRep.Get_Fields_Int32("accountkey"), false, true, clsRep.Get_Fields_DateTime("billdate"), true);
					}
					if (Statics.boolUseWaterBills)
					{
						// dblTemp = dblTemp + GetLienAmountForAccount(clsRep.Fields("accountkey"), True)
						dblTemp += modUTCalculations.CalculateAccountUTTotal_6(clsRep.Get_Fields_Int32("accountkey"), true, true, clsRep.Get_Fields_DateTime("billdate"), true);
					}
					HandleSpecialCase = Strings.Format(dblTemp, "###,###,##0.00");
				}
				else if (vbPorterVar == 69)
				{
					// Interest Date
					HandleSpecialCase = Strings.Format(GetInterestDateFromRate_2(FCConvert.ToInt32(FCConvert.ToString(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey"))))), strExtraParameters);
				}
				else if (vbPorterVar == 70)
				{
					// Water Interest Rate
					if (Statics.boolUseWaterBills)
					{
						HandleSpecialCase = FCConvert.ToString(FCConvert.ToDouble(GetInterestRateFromRate_8(FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey"))), true)) * 100) + "%";
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 71)
				{
					// Sewer Interest Rate
					if (Statics.boolUseSewerBills)
					{
						HandleSpecialCase = FCConvert.ToString(FCConvert.ToDouble(GetInterestRateFromRate_8(FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey"))), false)) * 100) + "%";
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 72)
				{
					// due date
					strTemp = "";
					// dttemp = GetInterestDateFromRate(Val(clsRep.Fields("billingratekey")))
					// dttemp = dttemp - 1 'due date is day before
					dttemp = FCConvert.ToDateTime(GetDueDateFromRate_2(FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey")))));
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					// kgk 06-05-2012  There are multiple parameters, need to be split
					// kgk 06-05-2012  trout-834  Handle Bath Water due date shift for invoice cloud export
					if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BATH WATER DISTRICT")
					{
						if (Strings.UCase(strAry[1]) == "WATER")
						{
							// kgk 07-10-2012 trout-843  Change to NOT shift due date for Monthly billings
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetMeterBreakdownForBillKey_8(FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields("bill"))), FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("accountkey"))));
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							if (Statics.lngNumMetersLoaded > 0 && Statics.lngMetersLoadedBillKey == Conversion.Val(clsRep.Get_Fields("bill")))
							{
								if (Information.UBound(Statics.aryMeterBreakdown, 1) >= 1)
								{
									if (clsRep.Get_Fields_DateTime("BillDate").ToOADate() <= DateAndTime.DateValue("07-31-2012").ToOADate() || Strings.UCase(Strings.Trim(Statics.aryMeterBreakdown[1].FrequencyShort)) != "MON")
									{
										dttemp = dttemp.AddDays(-30);
									}
								}
							}
						}
					}
					strTemp = Strings.Format(dttemp, strAry[0]);
					// kgk 06-05-2012   strExtraParameters)
					// strExtraParameters)
					if (Strings.UCase(strAry[1]) == "WATER")
					{
						if (Statics.boolUseWaterBills)
						{
						}
						else
						{
							strTemp = "";
						}
					}
					else if (Strings.UCase(strAry[1]) == "SEWER")
					{
						if (Statics.boolUseSewerBills)
						{
						}
						else
						{
							strTemp = "";
						}
					}
					else if (Strings.UCase(strAry[1]) == "ALWAYS")
					{
					}
					else
					{
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 73)
				{
					// interest accrued for water
					dblAry = new double[2 + 1];
					dblTemp = 0;
					if (Statics.boolUseWaterBills)
					{
						modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate"));
						dblTemp += dblAry[0] + dblAry[1];
					}
					HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
				}
				else if (vbPorterVar == 74)
				{
					// interest accrued for sewer
					dblAry = new double[2 + 1];
					dblTemp = 0;
					if (Statics.boolUseSewerBills)
					{
						modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate"));
						dblTemp += dblAry[0] + dblAry[1];
					}
					HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
				}
				else if (vbPorterVar == 75)
				{
					// total interest accrued
					dblAry = new double[2 + 1];
					dblTemp = 0;
					if (Statics.boolUseWaterBills)
					{
						modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate"));
						dblTemp += dblAry[0] + dblAry[1];
					}
					if (Statics.boolUseSewerBills)
					{
						modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), false, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate"));
						dblTemp += dblAry[0] + dblAry[1];
					}
					HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
				}
				else if (vbPorterVar == 76)
				{
					// water past due not counting interest added
					// current due
					dblAry = new double[2 + 1];
					if (Statics.boolUseWaterBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("WPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("WTaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("WPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("WTaxPaid"));
						// total due - current = past due
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), true, false, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
						// pasdue - intadded = past due without interest
						// dbltemp = dbltemp - Val(clsRep.Fields("wintadded"))
						dblTemp += -dblAry[0] - dblAry[1];
						HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 77)
				{
					// sewer past due not counting interest added
					dblAry = new double[2 + 1];
					if (Statics.boolUseSewerBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("SPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("STaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("SPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("STaxPaid"));
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), false, false, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
						// dbltemp = dbltemp - Val(clsRep.Fields("SintAdded"))
						dblTemp += -dblAry[0] - dblAry[1];
						HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
					}
					else
					{
						HandleSpecialCase = "";
					}
				}
				else if (vbPorterVar == 78)
				{
					// total past due not counting interest added
					dblAry = new double[4 + 1];
					dblTemp = 0;
					if (Statics.boolUseSewerBills)
					{
						dblTemp = Conversion.Val(clsRep.Get_Fields_Double("SPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("STaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("SPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("STaxPaid"));
					}
					if (Statics.boolUseWaterBills)
					{
						dblTemp += Conversion.Val(clsRep.Get_Fields_Double("WPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("WTaxOwed"));
						dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("WPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("WTaxPaid"));
					}
					if (Statics.boolUseWaterBills && Statics.boolUseSewerBills)
					{
						dblTemp = modUTCalculations.CalculateAccountUTTotal_32(clsRep.Get_Fields_Int32("accountkey"), false, false, clsRep.Get_Fields_DateTime("billdate")) + modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), true, modExtraModules.Statics.gboolShowLienAmountOnBill, ref dblAry[2], ref dblAry[3], clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
						// dbltemp = dbltemp - Val(clsRep.Fields("WintAdded")) - Val(clsRep.Fields("SintAdded"))
						dblTemp += -dblAry[0] - dblAry[1] - dblAry[2] - dblAry[3];
					}
					else if (Statics.boolUseWaterBills)
					{
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), true, false, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
						dblTemp += -dblAry[0] - dblAry[1];
					}
					else if (Statics.boolUseSewerBills)
					{
						dblTemp = modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), false, false, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate")) - dblTemp;
						dblTemp += -dblAry[0] - dblAry[1];
					}
					HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
				}
				else if (vbPorterVar == 79)
				{
					// Total consumption this quarter
					if (Statics.lngThisQuarterAccount != clsRep.Get_Fields_Int32("AccountKey"))
					{
						Statics.lngThisQuarterAccount = clsRep.Get_Fields_Int32("accountkey");
						CalcThisQuarterStuff(clsRep.Get_Fields_Int32("billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.lngThisQuarterConsumption, "#,###,##0");
				}
				else if (vbPorterVar == 80)
				{
					// Total consumption last quarter
					if (Statics.lngLastQuarterAccount != clsRep.Get_Fields_Int32("accountkey"))
					{
						Statics.lngLastQuarterAccount = clsRep.Get_Fields_Int32("accountkey");
						CalcLastQuarterStuff(clsRep.Get_Fields_Int32("Billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.lngLastQuarterConsumption, "#,###,##0");
				}
				else if (vbPorterVar == 81)
				{
					// Total consumption last year
					if (Statics.lngLastYearAccount != clsRep.Get_Fields_Int32("accountkey"))
					{
						Statics.lngLastYearAccount = clsRep.Get_Fields_Int32("accountkey");
						CalcLastYearStuff(clsRep.Get_Fields_Int32("billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.lngLastYearConsumption, "#,###,##0");
				}
				else if (vbPorterVar == 82)
				{
					// Total service days this quarter
					if (Statics.lngThisQuarterAccount != clsRep.Get_Fields_Int32("accountkey"))
					{
						Statics.lngThisQuarterAccount = clsRep.Get_Fields_Int32("AccountKey");
						CalcThisQuarterStuff(clsRep.Get_Fields_Int32("billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.lngThisQuarterServiceDays, "#,###,##0");
				}
				else if (vbPorterVar == 83)
				{
					// total service days last quarter
					if (Statics.lngLastQuarterAccount != clsRep.Get_Fields_Int32("accountkey"))
					{
						Statics.lngLastQuarterAccount = clsRep.Get_Fields_Int32("accountkey");
						CalcLastQuarterStuff(clsRep.Get_Fields_Int32("billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.lngLastQuarterServiceDays, "#,###,##0");
				}
				else if (vbPorterVar == 84)
				{
					// total service days last year
					if (Statics.lngLastYearAccount != clsRep.Get_Fields_Int32("accountkey"))
					{
						Statics.lngLastYearAccount = clsRep.Get_Fields_Int32("accountkey");
						CalcLastYearStuff(clsRep.Get_Fields_Int32("billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.lngLastYearServiceDays, "#,###,##0");
				}
				else if (vbPorterVar == 85)
				{
					// average consumption this quarter
					if (Statics.lngThisQuarterAccount != clsRep.Get_Fields_Int32("accountkey"))
					{
						Statics.lngThisQuarterAccount = clsRep.Get_Fields_Int32("accountkey");
						CalcThisQuarterStuff(clsRep.Get_Fields_Int32("billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.dblThisQuarterAverageConsumption, "0.00");
				}
				else if (vbPorterVar == 86)
				{
					// average consumption last quarter
					if (Statics.lngLastQuarterAccount != clsRep.Get_Fields_Int32("accountkey"))
					{
						Statics.lngLastQuarterAccount = clsRep.Get_Fields_Int32("accountkey");
						CalcLastQuarterStuff(clsRep.Get_Fields_Int32("billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.dblLastQuarterAverageConsumption, "0.00");
				}
				else if (vbPorterVar == 87)
				{
					// average consumption last year
					if (Statics.lngLastYearAccount != clsRep.Get_Fields_Int32("accountkey"))
					{
						Statics.lngLastYearAccount = clsRep.Get_Fields_Int32("accountkey");
						CalcLastYearStuff(clsRep.Get_Fields_Int32("billingratekey"), ref Statics.dblRRatio);
					}
					HandleSpecialCase = Strings.Format(Statics.dblLastYearAverageConsumption, "0.00");
				}
				else if (vbPorterVar == 90)
				{
					// amount per meter
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					boolTemp = Strings.UCase(strAry[1]) == "INCLUDE TAX";
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					strTemp = GetMeterList(clsRep.Get_Fields("Bill"), ref strAry[0], clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					if (strTemp != string.Empty)
					{
						if (Conversion.Val(strTemp) != 0)
						{
							strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
							dblTemp = 0;
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								if (Statics.lngNumMetersLoaded >= x)
								{
									if (Conversion.Val(strAry[x]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
									{
										dblTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Amount;
										if (boolTemp)
										{
											dblTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Tax;
										}
									}
								}
							}
							// x
						}
						else
						{
							for (x = 1; x <= Information.UBound(Statics.aryMeterBreakdown, 1); x++)
							{
								dblTemp += Statics.aryMeterBreakdown[x].Amount;
								if (boolTemp)
								{
									dblTemp += Statics.aryMeterBreakdown[x].Tax;
								}
							}
							// x
						}
						if (dblTemp != 0)
						{
							HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
						}
					}
				}
				else if (vbPorterVar == 91)
				{
					// estimate or actual code for specified meter(s)
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
					strTemp = GetMeterList(clsRep.Get_Fields("bill"), ref strAry[0], clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					if (strTemp != string.Empty)
					{
						if (Conversion.Val(strTemp) != 0)
						{
							strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
							if (Conversion.Val(strAry[0]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
							{
								HandleSpecialCase = Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[0])].CurrentCode;
							}
						}
						else
						{
							HandleSpecialCase = Statics.aryMeterBreakdown[1].CurrentCode;
						}
					}
				}
				else if (vbPorterVar == 92)
				{
					// descriptions for detail section
					if (Strings.Trim(strExtraParameters) == "")
					{
						strExtraParameters = "DO NOT SHOW SEPARATE";
					}
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					strTemp = "";
					boolOV = false;
					lngTemp = 0;
					// kgk 10-20-2011   trout-766    boolTemp = False
					if (Strings.UCase(strAry[0]) == "SHOW SEPARATE")
					{
						lngTemp = 1;
						// boolTemp = True
					}
					else if (Strings.UCase(strAry[0]) == "ITEMIZE")
					{
						lngTemp = 2;
						// kgk 10-20-2011   trout-766 Add option to itemize
					}
					if (Information.UBound(strAry, 1) > 0)
					{
						if (Strings.UCase(strAry[1]) == "LABEL OVERRIDES")
						{
							if (Conversion.Val(clsRep.Get_Fields_Decimal("WaterOverRideAmount")) != 0 || FCConvert.ToBoolean(Conversion.Val(clsRep.Get_Fields_Int32("SewerOverrideCons"))) | clsRep.Get_Fields_Boolean("wHasOverride"))
							{
								boolOV = true;
							}
						}
					}
					if (Information.UBound(strAry, 1) > 1)
					{
						// kgk 06-26-2012 trout-839
						if (Strings.UCase(strAry[2]) == "YES")
						{
							boolTemp = true;
						}
					}
					if (Statics.boolUseWaterBills)
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						if (Conversion.Val(Statics.DetailBreakDownWater[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
						{
							// boolTemp
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetWaterDetailForBillKey_2097(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("wprinowed")), Conversion.Val(clsRep.Get_Fields_Double("wtaxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("wmiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount")) - Conversion.Val(clsRep.Get_Fields_Decimal("WaterOverRideAmount")), lngTemp, boolOV, clsRep.Get_Fields_DateTime("billdate"), boolTemp);
						}
						// regular,Past Due,Liens,Interest
						for (x = 1; x <= 8; x++)
						{
							// kgk 06-26-2012 trout-839  Breakdown of regular to cons, units and flat
							if (boolTemp && x == CNSTDETAILBREAKDOWNROWREGULAR)
							{
								for (Y = 1; Y <= 3; Y++)
								{
									if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownWaterCUF[Y, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
									{
										strTemp += Statics.DetailBreakdownWaterCUF[Y, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
									}
								}
								// Y
							}
							else
							{
								// kgk 12-01-2011 trout-766  Per Brenda change to always show Sales Tax
								if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0 || x == CNSTDETAILBREAKDOWNROWTAX)
								{
									strTemp += Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
								}
							}
						}
						// x
						// kgk 10-19-2011 trout-766  Add adjustment details for Chapt 660 compliance
						for (x = 1; x <= Information.UBound(Statics.AdjBreakdownWater, 1); x++)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								strTemp += Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
							}
						}
						// x
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 93)
				{
					// amounts for detail section
					if (Strings.Trim(strExtraParameters) == "")
					{
						strExtraParameters = "DO NOT SHOW SEPARATE";
					}
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					strTemp = "";
					lngTemp = 0;
					// kgk 10-20-2011   trout-766    boolTemp = False
					if (Strings.UCase(strAry[0]) == "SHOW SEPARATE")
					{
						lngTemp = 1;
						// boolTemp = True
					}
					else if (Strings.UCase(strAry[0]) == "ITEMIZE")
					{
						lngTemp = 2;
						// kgk 10-20-2011   trout-766 Add option to itemize
					}
					if (Information.UBound(strAry, 1) > 0)
					{
						// kgk 06-26-2012 trout-839
						if (Strings.UCase(strAry[1]) == "YES")
						{
							boolTemp = true;
						}
					}
					if (Statics.boolUseWaterBills)
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						if (Conversion.Val(Statics.DetailBreakDownWater[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetWaterDetailForBillKey_8658(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("wprinowed")), Conversion.Val(clsRep.Get_Fields_Double("wtaxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("wmiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount")), lngTemp, clsRep.Get_Fields_DateTime("billdate"), boolTemp);
						}
						for (x = 1; x <= 8; x++)
						{
							// kgk 06-26-2012 trout-839  Breakdown of regular to cons, units and flat
							if (boolTemp && x == CNSTDETAILBREAKDOWNROWREGULAR)
							{
								for (Y = 1; Y <= 3; Y++)
								{
									if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownWaterCUF[Y, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
									{
										strTemp += Strings.Format(Conversion.Val(Statics.DetailBreakdownWaterCUF[Y, CNSTDETAILBREAKDOWNAMOUNT]), "#,###,##0.00") + "\r\n";
									}
								}
								// Y
							}
							else
							{
								// kgk 12-01-2011 trout-766  Per Brenda change to always show Sales Tax
								if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0 || x == CNSTDETAILBREAKDOWNROWTAX)
								{
									strTemp += Strings.Format(Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "#,###,##0.00") + "\r\n";
								}
							}
						}
						// x
						// kgk 10-19-2011 trout-766  Add adjustment details for Chapt 660 compliance
						for (x = 1; x <= Information.UBound(Statics.AdjBreakdownWater, 1); x++)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								strTemp += Strings.Format(Conversion.Val(Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "#,###,##0.00") + "\r\n";
							}
						}
						// x
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 94)
				{
					// descriptions for sewer detail section
					if (Strings.Trim(strExtraParameters) == "")
					{
						strExtraParameters = "DO NOT SHOW SEPARATE";
					}
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					strTemp = "";
					boolOV = false;
					lngTemp = 0;
					// kgk 10-20-2011   trout-766    boolTemp = False
					// If UCase(strExtraParameters) = "SHOW SEPARATE" Then
					// boolTemp = True
					// End If
					if (Strings.UCase(strAry[0]) == "SHOW SEPARATE")
					{
						lngTemp = 1;
						// boolTemp = True
					}
					else if (Strings.UCase(strAry[0]) == "ITEMIZE")
					{
						lngTemp = 2;
						// kgk 10-20-2011   trout-766 Add option to itemize
					}
					if (Information.UBound(strAry, 1) > 0)
					{
						if (Strings.UCase(strAry[1]) == "LABEL OVERRIDES")
						{
							if (Conversion.Val(clsRep.Get_Fields_Decimal("SewerOverRideAmount")) != 0 || FCConvert.ToBoolean(Conversion.Val(clsRep.Get_Fields_Int32("SewerOverrideCons"))) | clsRep.Get_Fields_Boolean("shasoverride"))
							{
								boolOV = true;
							}
						}
					}
					if (Information.UBound(strAry, 1) > 1)
					{
						// kgk 06-26-2012 trout-839
						if (Strings.UCase(strAry[2]) == "YES")
						{
							boolTemp = true;
						}
					}
					if (Statics.boolUseSewerBills)
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						if (Conversion.Val(Statics.DetailBreakdownSewer[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetSewerDetailForBillKey_720(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("sprinowed")), Conversion.Val(clsRep.Get_Fields_Double("staxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("smiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("sadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("SDEADJUSTAMOUNT")) - Conversion.Val(clsRep.Get_Fields_Decimal("SewerOverRideAmount")), lngTemp, boolOV, clsRep.Get_Fields_DateTime("billdate"), boolTemp);
						}
						// regular,Past Due,Liens,Interest
						for (x = 1; x <= 8; x++)
						{
							// kgk 06-26-2012 trout-839  Breakdown of regular to cons, units and flat
							if (boolTemp && x == CNSTDETAILBREAKDOWNROWREGULAR)
							{
								for (Y = 1; Y <= 3; Y++)
								{
									if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownSewerCUF[Y, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
									{
										strTemp += Statics.DetailBreakdownSewerCUF[Y, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
									}
								}
								// Y
							}
							else
							{
								// kgk 12-01-2011 trout-766  Per Brenda change to always show Sales Tax
								// kgk 01-30-2012 trout-797  Should be only on Water
								if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
								{
									// _ Or x = CNSTDETAILBREAKDOWNROWTAX Then
									// cjg 07-02-2012 trout-836
									// If lngtemp <> 2 Or x <> CNSTDETAILBREAKDOWNROWMISC Then
									strTemp += Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
									// Else
									// End If
								}
							}
						}
						// x
						// kgk 10-19-2011 trout-766  Add adjustment details for Chapt 660 compliance
						for (x = 1; x <= Information.UBound(Statics.AdjBreakdownSewer, 1); x++)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								strTemp += Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
							}
						}
						// x
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 95)
				{
					// amounts for detail section
					if (Strings.Trim(strExtraParameters) == "")
					{
						strExtraParameters = "DO NOT SHOW SEPARATE";
					}
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					strTemp = "";
					lngTemp = 0;
					// kgk 10-20-2011   trout-766    boolTemp = False
					if (Strings.UCase(strAry[0]) == "SHOW SEPARATE")
					{
						lngTemp = 1;
						// boolTemp = True
					}
					else if (Strings.UCase(strAry[0]) == "ITEMIZE")
					{
						lngTemp = 2;
						// kgk 10-20-2011   trout-766 Add option to itemize
					}
					if (Information.UBound(strAry, 1) > 0)
					{
						// kgk 06-26-2012 trout-839
						if (Strings.UCase(strAry[1]) == "YES")
						{
							boolTemp = true;
						}
					}
					if (Statics.boolUseSewerBills)
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						if (Conversion.Val(Statics.DetailBreakdownSewer[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetSewerDetailForBillKey_2907(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("sprinowed")), Conversion.Val(clsRep.Get_Fields_Double("staxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("smiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("sadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("SDEADJUSTAMOUNT")), lngTemp, clsRep.Get_Fields_DateTime("billdate"), boolTemp);
						}
						for (x = 1; x <= 8; x++)
						{
							// kgk 06-26-2012 trout-839  Breakdown of regular to cons, units and flat
							if (boolTemp && x == CNSTDETAILBREAKDOWNROWREGULAR)
							{
								for (Y = 1; Y <= 3; Y++)
								{
									if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownSewerCUF[Y, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
									{
										strTemp += Strings.Format(Conversion.Val(Statics.DetailBreakdownSewerCUF[Y, CNSTDETAILBREAKDOWNAMOUNT]), "#,###,##0.00") + "\r\n";
									}
								}
								// Y
							}
							else
							{
								// kgk 12-01-2011 trout-766  Per Brenda change to always show Sales Tax
								// kgk 01-30-2012 trout-797  Should be only on Water
								if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
								{
									// _ Or x = CNSTDETAILBREAKDOWNROWTAX Then
									// cjg 07-02-2012 trout-836
									// If lngtemp <> 2 Or x <> CNSTDETAILBREAKDOWNROWMISC Then
									strTemp += Strings.Format(Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "#,###,##0.00") + "\r\n";
									// Else
									// End If
								}
							}
						}
						// x
						// kgk 10-19-2011 trout-766  Add adjustment details for Chapt 660 compliance
						for (x = 1; x <= Information.UBound(Statics.AdjBreakdownSewer, 1); x++)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								strTemp += Strings.Format(Conversion.Val(Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "#,###,##0.00") + "\r\n";
							}
						}
						// x
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 96)
				{
					// descriptions for combo detail section
					strTemp = "";
					lngTemp = 0;
					// kgk 10-20-2011   trout-766    boolTemp = False
					if (Strings.Trim(strExtraParameters) == "")
					{
						strExtraParameters = "DO NOT SHOW SEPARATE";
					}
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					if (Strings.UCase(strAry[0]) == "SHOW SEPARATE")
					{
						lngTemp = 1;
						// boolTemp = True
					}
					else if (Strings.UCase(strAry[0]) == "ITEMIZE")
					{
						lngTemp = 2;
						// kgk 10-20-2011   trout-766 Add option to itemize
					}
					if (Statics.boolUseSewerBills)
					{
						boolOV = false;
						if (Information.UBound(strAry, 1) > 0)
						{
							if (Strings.UCase(strAry[1]) == "LABEL OVERRIDES")
							{
								if (Conversion.Val(clsRep.Get_Fields_Decimal("SewerOverRideAmount")) != 0 || FCConvert.ToBoolean(Conversion.Val(clsRep.Get_Fields_Int32("SewerOverrideCons"))) | clsRep.Get_Fields_Boolean("shasoverride"))
								{
									boolOV = true;
								}
							}
						}
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						if (Conversion.Val(Statics.DetailBreakdownSewer[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetSewerDetailForBillKey_720(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("sprinowed")), Conversion.Val(clsRep.Get_Fields_Double("staxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("smiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("sadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("SDEADJUSTAMOUNT")) - Conversion.Val(clsRep.Get_Fields_Decimal("SewerOverRideAmount")), lngTemp, boolOV, clsRep.Get_Fields_DateTime("billdate"));
						}
					}
					if (Statics.boolUseWaterBills)
					{
						boolOV = false;
						if (Information.UBound(strAry, 1) > 0)
						{
							if (Strings.UCase(strAry[1]) == "LABEL OVERRIDES")
							{
								if (Conversion.Val(clsRep.Get_Fields_Decimal("WaterOverRideAmount")) != 0 || FCConvert.ToBoolean(Conversion.Val(clsRep.Get_Fields_Int32("WaterOverrideCons"))) | clsRep.Get_Fields_Boolean("wHasOverride"))
								{
									boolOV = true;
								}
							}
						}
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						if (Conversion.Val(Statics.DetailBreakDownWater[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetWaterDetailForBillKey_2097(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("wprinowed")), Conversion.Val(clsRep.Get_Fields_Double("wtaxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("wmiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount")) - Conversion.Val(clsRep.Get_Fields_Decimal("WaterOverRideAmount")), lngTemp, boolOV, clsRep.Get_Fields_DateTime("billdate"));
						}
					}
					for (x = 1; x <= 8; x++)
					{
						dblTemp = 0;
						if (Statics.boolUseSewerBills)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								dblTemp += Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]);
							}
						}
						if (Statics.boolUseWaterBills)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								dblTemp += Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]);
							}
						}
						// kgk 12-01-2011 trout-766  Per Brenda change to always show Sales Tax
						// kgk 01-30-2012 trout-797  Should be only on Water
						if (dblTemp != 0 || (x == CNSTDETAILBREAKDOWNROWTAX && Statics.boolUseWaterBills))
						{
							if (Statics.boolUseWaterBills)
							{
								strTemp += Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
							}
							else
							{
								strTemp += Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
							}
						}
					}
					// x
					// kgk 10-19-2011 trout-766  Add adjustment details for Chapt 660 compliance
					if (Statics.boolUseSewerBills)
					{
						for (x = 1; x <= Information.UBound(Statics.AdjBreakdownSewer, 1); x++)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								strTemp += Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
							}
						}
						// x
					}
					if (Statics.boolUseWaterBills)
					{
						for (x = 1; x <= Information.UBound(Statics.AdjBreakdownWater, 1); x++)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								strTemp += Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION] + "\r\n";
							}
						}
						// x
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 97)
				{
					// amounts for combo detail section
					strTemp = "";
					lngTemp = 0;
					// kgk 10-20-2011   trout-766    boolTemp = False
					if (Strings.UCase(strExtraParameters) == "SHOW SEPARATE")
					{
						lngTemp = 1;
						// boolTemp = True
					}
					else if (Strings.UCase(strExtraParameters) == "ITEMIZE")
					{
						lngTemp = 2;
						// kgk 10-20-2011   trout-766 Add option to itemize
					}
					if (Statics.boolUseSewerBills)
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						if (Conversion.Val(Statics.DetailBreakdownSewer[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetSewerDetailForBillKey_2907(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("sprinowed")), Conversion.Val(clsRep.Get_Fields_Double("staxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("smiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("sadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("SDEADJUSTAMOUNT")), lngTemp, clsRep.Get_Fields_DateTime("billdate"));
						}
					}
					if (Statics.boolUseWaterBills)
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						if (Conversion.Val(Statics.DetailBreakDownWater[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							GetWaterDetailForBillKey_8658(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("wprinowed")), Conversion.Val(clsRep.Get_Fields_Double("wtaxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("wmiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount")), lngTemp, clsRep.Get_Fields_DateTime("billdate"));
						}
					}
					for (x = 1; x <= 8; x++)
					{
						dblTemp = 0;
						if (Statics.boolUseSewerBills)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								dblTemp += Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]);
							}
						}
						if (Statics.boolUseWaterBills)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								dblTemp += Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]);
							}
						}
						// kgk 12-01-2011 trout-766  Per Brenda change to always show Sales Tax
						// kgk 01-30-2012 trout-797  Should be only on Water
						if (dblTemp != 0 || (x == CNSTDETAILBREAKDOWNROWTAX && Statics.boolUseWaterBills))
						{
							strTemp += Strings.Format(dblTemp, "#,###,###,##0.00") + "\r\n";
						}
					}
					// x
					// kgk 10-19-2011 trout-766  Add adjustment details for Chapt 660 compliance
					if (Statics.boolUseSewerBills)
					{
						for (x = 1; x <= Information.UBound(Statics.AdjBreakdownSewer, 1); x++)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								strTemp += Strings.Format(Conversion.Val(Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00") + "\r\n";
							}
						}
						// x
					}
					if (Statics.boolUseWaterBills)
					{
						for (x = 1; x <= Information.UBound(Statics.AdjBreakdownWater, 1); x++)
						{
							if (Conversion.Val(Strings.Format(Conversion.Val(Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
							{
								strTemp += Strings.Format(Conversion.Val(Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00") + "\r\n";
							}
						}
						// x
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 98)
				{
					// water,sewer,combined label
					strTemp = "";
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					if (Statics.boolUseSewerBills && Statics.boolUseWaterBills)
					{
						strTemp = strAry[2];
					}
					else if (Statics.boolUseWaterBills)
					{
						strTemp = strAry[0];
					}
					else if (Statics.boolUseSewerBills)
					{
						strTemp = strAry[1];
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 99)
				{
					// category (combined)
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						strTemp = Strings.Trim(GetCategoryForAccount_24(clsRep.Get_Fields_Int32("accountkey"), true, false));
					}
					if (strTemp == "" && Statics.boolUseSewerBills)
					{
						strTemp = Strings.Trim(GetCategoryForAccount_24(clsRep.Get_Fields_Int32("accountkey"), false, false));
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 100)
				{
					// category short desc (combined)
					strTemp = "";
					if (Statics.boolUseWaterBills)
					{
						strTemp = Strings.Trim(GetCategoryForAccount_24(clsRep.Get_Fields_Int32("accountkey"), true, true));
					}
					if (strTemp == "" && Statics.boolUseSewerBills)
					{
						strTemp = Strings.Trim(GetCategoryForAccount_24(clsRep.Get_Fields_Int32("accountkey"), false, true));
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 101)
				{
					// messages
					strTemp = "";
					clsTemp.OpenRecordset("select * from custombillmessages where messagenum = " + FCConvert.ToString(Conversion.Val(strExtraParameters)), modExtraModules.strUTDatabase);
					if (!clsTemp.EndOfFile())
					{
						strTemp = FCConvert.ToString(clsTemp.Get_Fields_String("messagetext"));
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 102)
				{
					// return address
					strTemp = "";
					clsTemp.OpenRecordset("select * from disconnectioninfo", modExtraModules.strUTDatabase);
					if (!clsTemp.EndOfFile())
					{
						if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("name"))) != string.Empty)
						{
							strTemp += clsTemp.Get_Fields_String("name");
						}
						if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("address1"))) != string.Empty)
						{
							if (strTemp != string.Empty)
								strTemp += "\r\n";
							strTemp += clsTemp.Get_Fields_String("address1");
						}
						if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("address2"))) != string.Empty)
						{
							if (strTemp != string.Empty)
								strTemp += "\r\n";
							strTemp += clsTemp.Get_Fields_String("address2");
						}
						if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("address3"))) != string.Empty)
						{
							if (strTemp != string.Empty)
								strTemp += "\r\n";
							strTemp += clsTemp.Get_Fields_String("address3");
						}
						if (Strings.Trim(FCConvert.ToString(clsTemp.Get_Fields_String("address4"))) != string.Empty)
						{
							if (strTemp != string.Empty)
								strTemp += "\r\n";
							strTemp += clsTemp.Get_Fields_String("address4");
						}
					}
					HandleSpecialCase = strTemp;
				}
				else if (vbPorterVar == 103)
				{
					// water + sewer adjustment
					dblTemp = 0;
					if (Statics.boolUseWaterBills)
					{
						dblTemp += Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount"));
					}
					if (Statics.boolUseSewerBills)
					{
						dblTemp += Conversion.Val(clsRep.Get_Fields_Decimal("sadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("sdeadjustamount"));
					}
					if (dblTemp > 0)
					{
						HandleSpecialCase = Strings.Format(dblTemp, "#,###,##0.00");
					}
				}
				else if (vbPorterVar == 104)
				{
					// Water adjustment
					if (Statics.boolUseWaterBills)
					{
						if (Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) > 0 || Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount")) > 0)
						{
							HandleSpecialCase = Strings.Format(Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount")), "#,###,##0.00");
						}
					}
				}
				else if (vbPorterVar == 105)
				{
					// sewer adjustment
					if (Statics.boolUseSewerBills)
					{
						if (Conversion.Val(clsRep.Get_Fields_Decimal("sadjustamount")) > 0 || Conversion.Val(clsRep.Get_Fields_Decimal("sdeadjustamount")) > 0)
						{
							HandleSpecialCase = Strings.Format(Conversion.Val(clsRep.Get_Fields_Decimal("sadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("sdeadjustamount")), "#,###,##0.00");
						}
					}
				}
				else if (vbPorterVar == 106)
				{
					// meter detail code
					// first get codes for all meters
					// then add codes for tax,lien,past due,interest
					boolTemp = false;
					bool boolWater106 = false;
					bool boolSewer106 = false;
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					if (Strings.UCase(strAry[0]) != "SEPARATE TAX")
					{
						boolTemp = true;
					}
					if (Strings.UCase(strAry[1]) == "WATER")
					{
						if (Statics.boolUseWaterBills)
						{
							boolWater106 = true;
						}
						else
						{
							boolWater106 = false;
						}
						boolSewer106 = false;
					}
					else if (Strings.UCase(strAry[1]) == "SEWER")
					{
						if (Statics.boolUseSewerBills)
						{
							boolSewer106 = true;
						}
						else
						{
							boolSewer106 = true;
						}
						boolWater106 = false;
					}
					else if (Strings.UCase(strAry[1]) == "COMBINED")
					{
						boolSewer106 = Statics.boolUseSewerBills;
						boolWater106 = Statics.boolUseWaterBills;
					}
					else
					{
						// default to always
						boolSewer106 = Statics.boolUseSewerBills;
						boolWater106 = Statics.boolUseWaterBills;
					}
					if (Strings.UCase(strAry[0]) != "SEPARATE TAX")
					{
						boolTemp = true;
					}
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					strTemp = GetMeterList_6(clsRep.Get_Fields("Bill"), "0", clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					if (strTemp != string.Empty)
					{
						if (Conversion.Val(strTemp) != 0)
						{
							strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
							dblTemp = 0;
							strTemp = "";
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								if (Statics.lngNumMetersLoaded >= x)
								{
									dblTemp = 0;
									if (Conversion.Val(strAry[x]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
									{
										dblTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Amount;
										if (boolTemp)
										{
											dblTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Tax;
										}
										if (dblTemp > 0)
										{
											if (Strings.UCase(Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Service) == "W")
											{
												if (boolWater106)
												{
													strTemp += "WA" + "\r\n";
												}
											}
											else if (Strings.UCase(Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Service) == "S")
											{
												if (boolSewer106)
												{
													strTemp += "SW" + "\r\n";
												}
											}
											else if (Strings.UCase(Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Service) == "B")
											{
												if (boolSewer106 && boolWater106 && Statics.lngNumMetersLoaded == 1)
												{
													strTemp += "WA" + "\r\n";
													strTemp += "SW" + "\r\n";
												}
												else if (boolSewer106 && boolWater106)
												{
													strTemp += "BO" + "\r\n";
												}
												else if (boolSewer106)
												{
													strTemp += "SW" + "\r\n";
												}
												else
												{
													strTemp += "WA" + "\r\n";
												}
											}
										}
									}
								}
							}
							// x
						}
						else
						{
							strTemp = "";
							for (x = 1; x <= Information.UBound(Statics.aryMeterBreakdown, 1); x++)
							{
								dblTemp = 0;
								if (boolWater106)
								{
									dblTemp += Statics.aryMeterBreakdown[x].AmountWater;
								}
								if (boolSewer106)
								{
									dblTemp += Statics.aryMeterBreakdown[x].AmountSewer;
								}
								// dblTemp = dblTemp + aryMeterBreakdown(x).Amount
								if (boolTemp)
								{
									if (boolWater106)
									{
										dblTemp += Statics.aryMeterBreakdown[x].TaxWater;
									}
									if (boolSewer106)
									{
										dblTemp += Statics.aryMeterBreakdown[x].TaxSewer;
									}
									// dblTemp = dblTemp + aryMeterBreakdown(x).Tax
								}
								if (dblTemp > 0)
								{
									if (Strings.UCase(Statics.aryMeterBreakdown[x].Service) == "W")
									{
										if (boolWater106)
										{
											strTemp += "WA" + "\r\n";
										}
									}
									else if (Strings.UCase(Statics.aryMeterBreakdown[x].Service) == "S")
									{
										if (boolSewer106)
										{
											strTemp += "SW" + "\r\n";
										}
									}
									else if (Strings.UCase(Statics.aryMeterBreakdown[x].Service) == "B")
									{
										if (boolWater106 && boolSewer106)
										{
											if (Statics.lngNumMetersLoaded > 1)
											{
												strTemp += "BO" + "\r\n";
											}
											else
											{
												strTemp += "WA" + "\r\n";
												strTemp += "SW" + "\r\n";
											}
										}
										else if (boolSewer106)
										{
											strTemp += "SW" + "\r\n";
										}
										else
										{
											strTemp += "WA" + "\r\n";
										}
									}
								}
							}
							// x
						}
						if (!boolTemp)
						{
							// tax is listed separate
							dblTemp = 0;
							if (boolWater106)
							{
								dblTemp += Conversion.Val(clsRep.Get_Fields_Double("wtaxowed"));
							}
							if (boolSewer106)
							{
								dblTemp += Conversion.Val(clsRep.Get_Fields_Double("staxowed"));
							}
							if (dblTemp > 0)
							{
								strTemp += "TX" + "\r\n";
							}
						}
						if (boolSewer106)
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							if (Conversion.Val(Statics.DetailBreakdownSewer[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
							{
								// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
								GetSewerDetailForBillKey_3312(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("sprinowed")), Conversion.Val(clsRep.Get_Fields_Double("staxowed")), clsRep.Get_Fields_DateTime("billdate"));
							}
						}
						if (boolWater106)
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							if (Conversion.Val(Statics.DetailBreakDownWater[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
							{
								// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
								GetWaterDetailForBillKey_10845(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("wprinowed")), Conversion.Val(clsRep.Get_Fields_Double("wtaxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("wmiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount")), clsRep.Get_Fields_DateTime("billdate"));
							}
						}
						for (x = 2; x <= 7; x++)
						{
							// already got regular for all meters so just get the rest
							dblTemp = 0;
							if (boolSewer106)
							{
								if (Strings.UCase(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION]) != "TAX")
								{
									if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
									{
										if (Strings.UCase(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION]) == "CREDIT")
										{
											dblTemp -= Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]);
										}
										else
										{
											dblTemp += Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]);
										}
									}
								}
							}
							if (boolWater106)
							{
								if (Strings.UCase(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION]) != "TAX")
								{
									if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
									{
										if (Strings.UCase(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION]) == "CREDIT")
										{
											dblTemp -= Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]);
										}
										else
										{
											dblTemp += Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]);
										}
									}
								}
							}
							if (dblTemp != 0)
							{
								switch (x)
								{
									case CNSTDETAILBREAKDOWNROWINTEREST:
										{
											strTemp += "PN" + "\r\n";
											break;
										}
									case CNSTDETAILBREAKDOWNROWLIENS:
										{
											strTemp += "LN" + "\r\n";
											break;
										}
									case CNSTDETAILBREAKDOWNROWPASTDUE:
										{
											if (dblTemp < 0)
											{
												strTemp += "CR" + "\r\n";
											}
											else
											{
												strTemp += "AR" + "\r\n";
											}
											// strTemp = strTemp & "AR" & vbNewLine
											break;
										}
									case CNSTDETAILBREAKDOWNROWREGULAR:
										{
											break;
										}
									case CNSTDETAILBREAKDOWNROWTAX:
										{
											break;
										}
									default:
										{
											strTemp += "MC" + "\r\n";
											break;
										}
								}
								//end switch
							}
						}
						// x
						HandleSpecialCase = strTemp;
					}
				}
				else if (vbPorterVar == 107)
				{
					// meter detail amount
					boolTemp = false;
					bool boolWater107 = false;
					bool boolSewer107 = false;
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					if (Strings.UCase(strAry[0]) != "SEPARATE TAX")
					{
						boolTemp = true;
					}
					if (Strings.UCase(strAry[1]) == "WATER")
					{
						if (Statics.boolUseWaterBills)
						{
							boolWater107 = true;
						}
						else
						{
							boolWater107 = false;
						}
						boolSewer107 = false;
					}
					else if (Strings.UCase(strAry[1]) == "SEWER")
					{
						if (Statics.boolUseSewerBills)
						{
							boolSewer107 = true;
						}
						else
						{
							boolSewer107 = true;
						}
						boolWater107 = false;
					}
					else if (Strings.UCase(strAry[1]) == "COMBINED")
					{
						boolSewer107 = Statics.boolUseSewerBills;
						boolWater107 = Statics.boolUseWaterBills;
					}
					else
					{
						// default to always
						boolSewer107 = Statics.boolUseSewerBills;
						boolWater107 = Statics.boolUseWaterBills;
					}
					// If UCase(strExtraParameters) <> "SEPARATE TAX" Then
					// boolTemp = True
					// End If
					// TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
					strTemp = GetMeterList_6(clsRep.Get_Fields("Bill"), "0", clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					if (strTemp != string.Empty)
					{
						if (Conversion.Val(strTemp) != 0)
						{
							strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
							dblTemp = 0;
							dblTemp2 = 0;
							strTemp = "";
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								if (Statics.lngNumMetersLoaded >= x)
								{
									dblTemp = 0;
									if (Conversion.Val(strAry[x]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
									{
										if (boolWater107)
										{
											dblTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].AmountWater;
										}
										if (boolSewer107)
										{
											// dblTemp = dblTemp + aryMeterBreakdown(strAry(x)).AmountSewer
											dblTemp2 += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].AmountSewer;
										}
										// dblTemp = dblTemp + aryMeterBreakdown(strAry(x)).Amount
										if (boolTemp)
										{
											if (boolWater107)
											{
												dblTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].TaxWater;
											}
											if (boolSewer107)
											{
												// dblTemp = dblTemp + aryMeterBreakdown(strAry(x)).TaxSewer
												dblTemp2 += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].TaxSewer;
											}
											// dblTemp = dblTemp + aryMeterBreakdown(strAry(x)).Tax
										}
										// Select Case UCase(aryMeterBreakdown(strAry(x)).Service)
										// Case "W"
										// If boolWater107 Then
										// strTemp = strTemp & "WA" & vbNewLine
										// End If
										// Case "S"
										// If boolSewer107 Then
										// strTemp = strTemp & "SW" & vbNewLine
										// End If
										// Case "B"
										// If boolWater107 And boolSewer107 Then
										// If lngNumMetersLoaded > 1 Then
										// strTemp = strTemp & "BO" & vbNewLine
										// Else
										// strTemp = strTemp & "WA" & vbNewLine
										// strTemp = strTemp & "SW" & vbNewLine
										// End If
										// ElseIf boolSewer107 Then
										// strTemp = strTemp & "SW" & vbNewLine
										// Else
										// strTemp = strTemp & "WA" & vbNewLine
										// End If
										// End Select
										if (boolWater107 && boolSewer107 && Statics.lngNumMetersLoaded == 1 && Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Service == "B")
										{
											strTemp += Strings.Format(dblTemp, "#,###,##0.00") + "\r\n";
											strTemp += Strings.Format(dblTemp2, "#,###,##0.00") + "\r\n";
										}
										else
										{
											dblTemp += dblTemp2;
											if (dblTemp > 0)
											{
												strTemp += Strings.Format(dblTemp, "#,###,##0.00") + "\r\n";
											}
										}
									}
								}
							}
							// x
						}
						else
						{
							strTemp = "";
							for (x = 1; x <= Information.UBound(Statics.aryMeterBreakdown, 1); x++)
							{
								dblTemp = 0;
								if (boolWater107)
								{
									dblTemp += Statics.aryMeterBreakdown[x].AmountWater;
								}
								if (boolSewer107)
								{
									// dblTemp = dblTemp + aryMeterBreakdown(x).AmountSewer
									dblTemp2 += Statics.aryMeterBreakdown[x].AmountSewer;
								}
								// dblTemp = dblTemp + aryMeterBreakdown(x).Amount
								if (boolTemp)
								{
									if (boolWater107)
									{
										dblTemp += Statics.aryMeterBreakdown[x].TaxWater;
									}
									if (boolSewer107)
									{
										// dblTemp = dblTemp + aryMeterBreakdown(x).TaxSewer
										dblTemp2 += Statics.aryMeterBreakdown[x].TaxSewer;
									}
									// dblTemp = dblTemp + aryMeterBreakdown(x).Tax
								}
								if (boolWater107 && boolSewer107 && Statics.lngNumMetersLoaded == 1 && Statics.aryMeterBreakdown[x].Service == "B")
								{
									strTemp += Strings.Format(dblTemp, "#,###,##0.00") + "\r\n";
									strTemp += Strings.Format(dblTemp2, "#,###,##0.00") + "\r\n";
								}
								else
								{
									dblTemp += dblTemp2;
									if (dblTemp > 0)
									{
										strTemp += Strings.Format(dblTemp, "#,###,##0.00") + "\r\n";
									}
								}
							}
							// x
						}
						if (!boolTemp)
						{
							// tax is listed separate
							dblTemp = 0;
							if (boolWater107)
							{
								dblTemp += Conversion.Val(clsRep.Get_Fields_Double("wtaxowed"));
							}
							if (boolSewer107)
							{
								dblTemp += Conversion.Val(clsRep.Get_Fields_Double("staxowed"));
							}
							if (dblTemp > 0)
							{
								strTemp += Strings.Format(dblTemp, "#,###,##0.00") + "\r\n";
							}
						}
						if (boolSewer107)
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							if (Conversion.Val(Statics.DetailBreakdownSewer[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
							{
								// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
								GetSewerDetailForBillKey_3312(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("sprinowed")), Conversion.Val(clsRep.Get_Fields_Double("staxowed")), clsRep.Get_Fields_DateTime("billdate"));
							}
						}
						if (boolWater107)
						{
							// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
							if (Conversion.Val(Statics.DetailBreakDownWater[1, CNSTDETAILBREAKDOWNBILLNUM]) != Conversion.Val(clsRep.Get_Fields("bill")))
							{
								// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
								GetWaterDetailForBillKey_10845(clsRep.Get_Fields("bill"), clsRep.Get_Fields_Int32("accountkey"), Conversion.Val(clsRep.Get_Fields_Double("wprinowed")), Conversion.Val(clsRep.Get_Fields_Double("wtaxowed")), Conversion.Val(clsRep.Get_Fields_Decimal("wmiscamount")), Conversion.Val(clsRep.Get_Fields_Decimal("wadjustamount")) + Conversion.Val(clsRep.Get_Fields_Decimal("wdeadjustamount")), clsRep.Get_Fields_DateTime("billdate"));
							}
						}
						for (x = 2; x <= 7; x++)
						{
							// already got regular for all meters so just get the rest
							dblTemp = 0;
							if (boolSewer107)
							{
								if (Strings.UCase(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION]) != "TAX")
								{
									if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
									{
										dblTemp += Conversion.Val(Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT]);
									}
								}
							}
							if (boolWater107)
							{
								if (Strings.UCase(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION]) != "TAX")
								{
									if (Conversion.Val(Strings.Format(Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]), "0.00")) != 0)
									{
										dblTemp += Conversion.Val(Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT]);
									}
								}
							}
							if (dblTemp != 0)
							{
								// Select Case x
								strTemp += Strings.Format(dblTemp, "#,###,##0.00") + "\r\n";
								// End Select
								// If boolUseWaterBills Then
								// strTemp = strTemp & DetailBreakDownWater(x, CNSTDETAILBREAKDOWNDESCRIPTION) & vbNewLine
								// Else
								// strTemp = strTemp & DetailBreakdownSewer(x, CNSTDETAILBREAKDOWNDESCRIPTION) & vbNewLine
								// End If
							}
						}
						// x
						HandleSpecialCase = strTemp;
					}
				}
				else if (vbPorterVar == 108)
				{
					// taxrate
					if (Statics.dblTaxRateToShow == 0)
					{
						clsTemp.OpenRecordset("select * from utilitybilling", modExtraModules.strUTDatabase);
						if (!clsTemp.EndOfFile())
						{
							Statics.dblTaxRateToShow = Conversion.Val(clsTemp.Get_Fields_Double("taxrate"));
						}
					}
					HandleSpecialCase = Strings.Format(Statics.dblTaxRateToShow, "0.00");
				}
				else if (vbPorterVar == 109)
				{
					// units
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					if (Information.UBound(strAry, 1) < 0)
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						strTemp = GetMeterList_6(clsRep.Get_Fields("bill"), "0", clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					}
					else
					{
						// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
						strTemp = GetMeterList(clsRep.Get_Fields("bill"), ref strAry[0], clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					}
					if (strTemp != string.Empty)
					{
						strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
						dblTemp = 0;
						if (Conversion.Val(strAry[0]) > 0)
						{
							for (x = 0; x <= Information.UBound(strAry, 1); x++)
							{
								if (Conversion.Val(strAry[x]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
								{
									dblTemp += Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[x])].Units;
								}
							}
							// x
						}
						else
						{
							for (x = 1; x <= Information.UBound(Statics.aryMeterBreakdown, 1); x++)
							{
								dblTemp += Statics.aryMeterBreakdown[x].Units;
							}
							// x
						}
						strTemp = "";
						if (lngTemp != 0)
						{
							strTemp = Strings.Format(lngTemp, "#,###,##0");
						}
						if (dblTemp > 0)
						{
							strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
							if (Information.UBound(strAry, 1) > 0)
							{
								// check to see if they chose to show units
								if (Strings.UCase(strAry[1]) == "YES")
								{
									strTemp = "Units = " + FCConvert.ToString(dblTemp);
								}
								else
								{
									strTemp = FCConvert.ToString(dblTemp);
								}
							}
							else
							{
								strTemp = "Units = " + FCConvert.ToString(dblTemp);
							}
						}
						HandleSpecialCase = strTemp;
					}
				}
				else if (vbPorterVar == 111)
				{
					// total past sewer due (liens and non-lien). With and without interest
					boolTemp = true;
					dblTemp = 0;
					dblAry = new double[2 + 1];
					dblAry[0] = 0;
					dblAry[1] = 0;
					if (Strings.Trim(strExtraParameters) != "")
					{
						strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
						if (Information.UBound(strAry, 1) >= 0)
						{
							if (Strings.LCase(Strings.Trim(strAry[0])) == "no interest")
							{
								boolTemp = false;
							}
						}
					}
					dblTemp = Conversion.Val(clsRep.Get_Fields_Double("SPrinOwed")) + Conversion.Val(clsRep.Get_Fields_Double("STaxOwed"));
					dblTemp += -Conversion.Val(clsRep.Get_Fields_Double("SPrinPaid")) - Conversion.Val(clsRep.Get_Fields_Double("STaxPaid"));
					dblTemp = FCConvert.ToDouble(Strings.Format(modUTCalculations.CalculateAccountUTTotal_31(clsRep.Get_Fields_Int32("accountkey"), false, true, ref dblAry[0], ref dblAry[1], clsRep.Get_Fields_DateTime("billdate")) - dblTemp, "0.00"));
					if (!boolTemp)
					{
						dblTemp += -dblAry[0] - dblAry[1];
					}
					HandleSpecialCase = Strings.Format(dblTemp, "#,##0.00");
				}
				else if (vbPorterVar == 112)
				{
					// # of days
					lngTemp = 0;
					lngTemp = GetPeriodDaysFromRate_2(FCConvert.ToInt32(Math.Round(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey")))));
					HandleSpecialCase = FCConvert.ToString(lngTemp);
				}
				else if (vbPorterVar == 113)
				{
					// Avg usage
					if (Strings.Trim(strExtraParameters) != "")
					{
						if (Strings.UCase(Strings.Trim(strExtraParameters)) == "WATER")
						{
							strTemp = "W";
						}
						else if (Strings.UCase(Strings.Trim(strExtraParameters)) == "SEWER")
						{
							strTemp = "S";
						}
						else
						{
							strTemp = "B";
						}
					}
					lngTemp = GetPeriodDaysFromRate_2(FCConvert.ToInt32(Math.Round(Conversion.Val(clsRep.Get_Fields_Int32("billingratekey")))));
					// Dim tBDT As BreakDownType
					// tBDT.BillKey = Val(clsRep.Fields("bill"))
					HandleSpecialCase = FCConvert.ToString(0);
					if (lngTemp > 0)
					{
						// If GetTotalConsumptionForBill(Val(clsRep.Fields("bill")), tBDT, strTemp) Then
						// If tBDT.Value >= 0 Then
						// dblTemp = tBDT.Value
						// HandleSpecialCase = Format(dblTemp / lngtemp, "0.00")
						// End If
						// End If
						dblTemp = Conversion.Val(clsRep.Get_Fields_Int32("consumption"));
						HandleSpecialCase = Strings.Format(dblTemp / lngTemp, "0.00");
					}
				}
				else if (vbPorterVar == 114)
				{
					// Billing freq
					lngTemp = 1;
					boolTemp = true;
					if (Strings.Trim(strExtraParameters) != "")
					{
						strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
						if (Information.UBound(strAry, 1) >= 0)
						{
							lngTemp = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Trim(strAry[0]))));
						}
						if (Information.UBound(strAry, 1) > 0)
						{
							if (Strings.UCase(Strings.Trim(strAry[1])) == "SHORT")
							{
								boolTemp = false;
							}
						}
					}
					if (lngTemp < 1)
						lngTemp = 1;
					// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
					GetMeterBreakdownForBillKey_8(FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields("bill"))), FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("accountkey"))));
					// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
					if (Statics.lngNumMetersLoaded > 0 && Statics.lngMetersLoadedBillKey == Conversion.Val(clsRep.Get_Fields("bill")))
					{
						if (Information.UBound(Statics.aryMeterBreakdown, 1) >= lngTemp)
						{
							if (boolTemp)
							{
								HandleSpecialCase = Strings.Trim(Statics.aryMeterBreakdown[lngTemp].Frequency);
							}
							else
							{
								HandleSpecialCase = Strings.Trim(Statics.aryMeterBreakdown[lngTemp].FrequencyShort);
							}
						}
					}
					// Case 115
					// meter description
					// trout-684 kgk 03-22-11  Special override for Jay when switching from Flat/Unit to Consumption billing
				}
				else if (vbPorterVar == 115)
				{
					strTemp = "";
					if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
					{
						clsTemp.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
						if (!clsTemp.EndOfFile())
						{
							if (clsTemp.Get_Fields_Boolean("RateTypeChange") == true)
							{
								// check for option in Customize
								// If there is an override then put the message on the bill from the customize / or custom bill screen?
								if (clsRep.Get_Fields_Boolean("SHasOverride") && Conversion.Val(clsRep.Get_Fields_Int32("Consumption")) > 0)
								{
									double dblSActualCons = 0;
									dblSActualCons = FCUtils.Round(modUTBilling.CalculateConsumptionBased_6554(FCConvert.ToInt32(Conversion.Val(clsRep.Get_Fields_Int32("Consumption"))), clsRep.Get_Fields_Int32("SRT1"), false, 0, 0, 0, "", ""), 2);
									if (dblSActualCons > Conversion.Val(clsRep.Get_Fields_Decimal("SewerOverrideAmount")))
									{
										strTemp = Strings.Format(dblSActualCons, "0.00");
									}
								}
							}
						}
					}
					HandleSpecialCase = strTemp;
					// kgk 10-06-11  Add Invoice Cloud account number and invoice number for burst upload of PDF
				}
				else if (vbPorterVar == 117)
				{
					// Invoice Cloud Account Number
					boolTemp = true;
					// default to owner
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						HandleSpecialCase = "UT-" + FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey")));
					}
					else
					{
						HandleSpecialCase = "UT-" + FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey"))) + "A";
					}
				}
				else if (vbPorterVar == 118)
				{
					// Invoice Cloud Invoice Number
					boolTemp = true;
					// default to owner
					if (Statics.boolUseWaterBills)
					{
						// if just water or combined then check water preference
						boolTemp = clsRep.Get_Fields_Boolean("wbillowner");
					}
					else
					{
						boolTemp = clsRep.Get_Fields_Boolean("sbillowner");
					}
					if (boolTemp)
					{
						strTemp = "UT-" + FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey")));
					}
					else
					{
						strTemp = "UT-" + FCConvert.ToString(GetAccountFromKey(clsRep.Get_Fields_Int32("accountkey"))) + "A";
					}
					// kgk 05032012    If clsRep.Fields("Service") = "B" Then                'If service is B then we can only match the PDF to one of the invoices
					if (Statics.boolUseWaterBills && Statics.boolUseSewerBills)
					{
						strTemp += "-S-";
						// kgk 05032012  Actually S comes before W
					}
					else if (Statics.boolUseWaterBills)
					{
						strTemp += "-W-";
					}
					else
					{
						strTemp += "-S-";
					}
					HandleSpecialCase = strTemp + clsRep.Get_Fields_Int32("BillNumber");
				}
				else if (vbPorterVar == 119)
				{
					// Meter change-out flag for specified meter(s)
					string strMsg119 = "";
					strAry = Strings.Split(strExtraParameters, ";", -1, CompareConstants.vbTextCompare);
					if (Information.UBound(strAry, 1) > 0)
					{
						if (strAry[1] == "")
						{
							strMsg119 = "XX";
						}
						else
						{
							strMsg119 = strAry[1];
						}
					}
					else
					{
						strMsg119 = "XX";
					}
					// TODO Get_Fields: Field [bill] not found!! (maybe it is an alias?)
					strTemp = GetMeterList(clsRep.Get_Fields("bill"), ref strAry[0], clsRep.Get_Fields_Int32("accountkey"), clsRep.Get_Fields_Int32("MeterKey"));
					if (strTemp != string.Empty)
					{
						if (Conversion.Val(strTemp) != 0)
						{
							strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
							if (Conversion.Val(strAry[0]) <= Information.UBound(Statics.aryMeterBreakdown, 1))
							{
								if (Statics.aryMeterBreakdown[FCConvert.ToInt32(strAry[0])].ChangedOut)
								{
									HandleSpecialCase = strMsg119;
								}
							}
						}
						else
						{
							if (Statics.aryMeterBreakdown[1].ChangedOut)
							{
								HandleSpecialCase = strMsg119;
							}
						}
					}
					// 120 - 125 Regular Fields
					// Case 126   -   See Case 31 XXXXXXXXXXXXXXX
				}
				return HandleSpecialCase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In HandleSpecialCase with code " + FCConvert.ToString(clsCCode.FieldID), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return HandleSpecialCase;
		}

		private static int GetAccountFromKey(int lngAccountKey)
		{
			int GetAccountFromKey = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			GetAccountFromKey = 0;
			clsLoad.OpenRecordset("select accountnumber from master where ID = " + FCConvert.ToString(lngAccountKey), "twut0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [accountnumber] and replace with corresponding Get_Field method
				GetAccountFromKey = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("accountnumber"))));
			}
			return GetAccountFromKey;
		}
		// vbPorter upgrade warning: lngMeterKey As int	OnWrite(string)
		private static int GetSequenceFromMeterKey_2(int lngMeterKey)
		{
			return GetSequenceFromMeterKey(ref lngMeterKey);
		}

		private static int GetSequenceFromMeterKey(ref int lngMeterKey)
		{
			int GetSequenceFromMeterKey = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			GetSequenceFromMeterKey = 0;
			clsLoad.OpenRecordset("Select Sequence from  metertable where ID = " + FCConvert.ToString(lngMeterKey), "twut0000.vb1");
			if (!clsLoad.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [sequence] and replace with corresponding Get_Field method
				GetSequenceFromMeterKey = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("sequence"))));
			}
			return GetSequenceFromMeterKey;
		}

		public static bool GetTotalFlatForBill(ref int lngBillKey, ref BreakDownType BDT, string strService = "B")
		{
			bool GetTotalFlatForBill = false;
			// looks in the breakdown table and gets the total for all of the flat for this bill
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			string strWhere = "";
			try
			{
				// On Error GoTo ErrorHandler
				GetTotalFlatForBill = false;
				if (strService == "B")
				{
					// both
					strWhere = "";
				}
				else if (strService == "W")
				{
					strWhere = " and service = 'W'";
				}
				else if (strService == "S")
				{
					strWhere = " and service = 'S'";
				}
				strWhere = " where billkey = " + FCConvert.ToString(lngBillKey) + " and type = 'F' " + strWhere;
				strSQL = "select sum(amount) as TheSum,sum(Value) as ValueSum from breakdown " + strWhere;
				clsLoad.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [TheSum] not found!! (maybe it is an alias?)
					BDT.Amount = FCConvert.ToDecimal(Conversion.Val(clsLoad.Get_Fields("TheSum")));
					// TODO Get_Fields: Field [ValueSum] not found!! (maybe it is an alias?)
					BDT.Value = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("ValueSum"))));
				}
				GetTotalFlatForBill = true;
				return GetTotalFlatForBill;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetTotalFlatAmountForBill" + "\r\n" + "Billkey " + FCConvert.ToString(lngBillKey), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetTotalFlatForBill;
		}

		public static bool GetTotalUnitsForBill(ref int lngBillKey, ref BreakDownType BDT, string strService = "B")
		{
			bool GetTotalUnitsForBill = false;
			// looks in the breakdown table and gets the total for all of the units for this bill
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			string strWhere = "";
			try
			{
				// On Error GoTo ErrorHandler
				GetTotalUnitsForBill = false;
				if (strService == "B")
				{
					// both
					strWhere = "";
				}
				else if (strService == "W")
				{
					// water
					strWhere = " and service = 'W'";
				}
				else if (strService == "S")
				{
					// sewer
					strWhere = " and service = 'S'";
				}
				strWhere = " where billkey = " + FCConvert.ToString(lngBillKey) + " and type = 'U' " + strWhere;
				strSQL = "select sum(amount) as TheSum,sum(Value) as ValueSum from breakdown " + strWhere;
				clsLoad.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [ValueSum] not found!! (maybe it is an alias?)
					BDT.Value = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("ValueSum"))));
					// TODO Get_Fields: Field [TheSum] not found!! (maybe it is an alias?)
					BDT.Amount = FCConvert.ToDecimal(Conversion.Val(clsLoad.Get_Fields("TheSum")));
				}
				GetTotalUnitsForBill = true;
				return GetTotalUnitsForBill;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetTotalUnitsForBill" + "\r\n" + "Billkey " + FCConvert.ToString(lngBillKey), "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetTotalUnitsForBill;
		}

		public static bool GetTotalConsumptionForBill(ref int lngBillKey, ref BreakDownType BDT, string strService = "B")
		{
			bool GetTotalConsumptionForBill = false;
			// looks in the breaddown table and gets the total for all of the Consumption for this bill
			clsDRWrapper clsLoad = new clsDRWrapper();
			string strSQL;
			string strWhere = "";
			try
			{
				// On Error GoTo ErrorHandler
				GetTotalConsumptionForBill = false;
				if (strService == "B")
				{
					// both
					// strWhere = ""
					strWhere = " AND Service = 'W'";
				}
				else if (strService == "W")
				{
					// water
					strWhere = " AND Service = 'W'";
				}
				else if (strService == "S")
				{
					// sewer
					strWhere = " AND Service = 'S'";
				}
				strWhere = " WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Type = 'C' " + strWhere;
				strSQL = "SELECT SUM(Amount) AS TheSum,SUM(Value) AS ValueSum FROM Breakdown " + strWhere;
				clsLoad.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Field [ValueSum] not found!! (maybe it is an alias?)
					BDT.Value = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields("ValueSum"))));
					// TODO Get_Fields: Field [TheSum] not found!! (maybe it is an alias?)
					BDT.Amount = FCConvert.ToDecimal(Conversion.Val(clsLoad.Get_Fields("TheSum")));
				}
				GetTotalConsumptionForBill = true;
				return GetTotalConsumptionForBill;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetTotalConsumptionForBill");
			}
			return GetTotalConsumptionForBill;
		}
		// vbPorter upgrade warning: 'Return' As short	OnWrite(long, short)
		public static short GetPeriodDaysFromRate_2(int lngRateKey)
		{
			return GetPeriodDaysFromRate(ref lngRateKey);
		}

		public static short GetPeriodDaysFromRate(ref int lngRateKey)
		{
			short GetPeriodDaysFromRate = 0;
			clsDRWrapper rsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				DateTime dtStart;
				DateTime dtEnd;
				// vbPorter upgrade warning: intTemp As short, int --> As long
				long intTemp;
				GetPeriodDaysFromRate = 0;
				rsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), "twut0000.vb1");
				if (!rsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [end] and replace with corresponding Get_Field method
					if (Information.IsDate(rsLoad.Get_Fields("end")))
					{
						// TODO Get_Fields: Check the table for the column [End] and replace with corresponding Get_Field method
						if (rsLoad.Get_Fields_DateTime("End").ToOADate() != 0)
						{
							// TODO Get_Fields: Check the table for the column [end] and replace with corresponding Get_Field method
							dtEnd = (DateTime)rsLoad.Get_Fields("end");
							if (Information.IsDate(rsLoad.Get_Fields("start")))
							{
								if (rsLoad.Get_Fields_DateTime("start").ToOADate() != 0)
								{
									dtStart = rsLoad.Get_Fields_DateTime("start");
									intTemp = DateAndTime.DateDiff("d", dtStart, dtEnd);
									if (intTemp < 0)
										intTemp = 0;
									GetPeriodDaysFromRate = FCConvert.ToInt16(intTemp);
								}
							}
						}
					}
				}
				return GetPeriodDaysFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetPeriodDaysFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetPeriodDaysFromRate;
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWrite(string)
		public static string GetPeriodEndFromRate_2(int lngRateKey)
		{
			return GetPeriodEndFromRate(ref lngRateKey);
		}

		public static string GetPeriodEndFromRate(ref int lngRateKey)
		{
			string GetPeriodEndFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			// loads the rate with lngratekey and returns the end date
			try
			{
				// On Error GoTo ErrorHandler
				GetPeriodEndFromRate = "";
				clsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), "twut0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("end")))
					{
						if (clsLoad.Get_Fields_DateTime("end").ToOADate() != 0)
						{
							strTemp = Strings.Format(clsLoad.Get_Fields_DateTime("end"), "MM/dd/yyyy");
						}
					}
				}
				GetPeriodEndFromRate = strTemp;
				return GetPeriodEndFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetPeriodEndFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetPeriodEndFromRate;
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWrite(string)
		public static string GetPeriodStartFromRate_2(int lngRateKey)
		{
			return GetPeriodStartFromRate(ref lngRateKey);
		}

		public static string GetPeriodStartFromRate(ref int lngRateKey)
		{
			string GetPeriodStartFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			// loads the rate with lngratekey and returns the startdate
			try
			{
				// On Error GoTo ErrorHandler
				GetPeriodStartFromRate = "";
				clsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), "twut0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("start")))
					{
						if (clsLoad.Get_Fields_DateTime("start").ToOADate() != 0)
						{
							strTemp = Strings.Format(clsLoad.Get_Fields_DateTime("start"), "MM/dd/yyyy");
						}
					}
				}
				GetPeriodStartFromRate = strTemp;
				return GetPeriodStartFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetPeriodStartFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetPeriodStartFromRate;
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWrite(int, string)
		public static string GetBillingDateFromRate_2(int lngRateKey)
		{
			return GetBillingDateFromRate(lngRateKey);
		}

		public static string GetBillingDateFromRate(int lngRateKey)
		{
			string GetBillingDateFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				GetBillingDateFromRate = "";
				clsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), "twut0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("BillDate")))
					{
						if (!clsLoad.IsFieldNull("BillDate"))
						{
							strTemp = Strings.Format(clsLoad.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
						}
					}
				}
				GetBillingDateFromRate = strTemp;
				return GetBillingDateFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number" + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetBillingDateFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetBillingDateFromRate;
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWriteFCConvert.ToDouble(
		public static string GetDueDateFromRate_2(int lngRateKey)
		{
			return GetDueDateFromRate(ref lngRateKey);
		}

		public static string GetDueDateFromRate(ref int lngRateKey)
		{
			string GetDueDateFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				GetDueDateFromRate = "";
				clsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), "twut0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
					if (Information.IsDate(clsLoad.Get_Fields("DueDate")))
					{
						// TODO Get_Fields: Check the table for the column [duedate] and replace with corresponding Get_Field method
						if (clsLoad.Get_Fields("duedate").ToOADate() != 0)
						{
							// TODO Get_Fields: Check the table for the column [DueDate] and replace with corresponding Get_Field method
							strTemp = Strings.Format(clsLoad.Get_Fields("DueDate"), "MM/dd/yyyy");
							GetDueDateFromRate = strTemp;
						}
					}
				}
				return GetDueDateFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetDueDateFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetDueDateFromRate;
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWrite(string)
		public static string GetInterestDateFromRate_2(int lngRateKey)
		{
			return GetInterestDateFromRate(ref lngRateKey);
		}

		public static string GetInterestDateFromRate(ref int lngRateKey)
		{
			string GetInterestDateFromRate = "";
			string strTemp = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				GetInterestDateFromRate = "";
				clsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), "twut0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (Information.IsDate(clsLoad.Get_Fields("Intstart")))
					{
						if (clsLoad.Get_Fields_DateTime("intstart").ToOADate() != 0)
						{
							strTemp = Strings.Format(clsLoad.Get_Fields_DateTime("Intstart"), "MM/dd/yyyy");
							GetInterestDateFromRate = strTemp;
						}
					}
				}
				return GetInterestDateFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetInterestDateFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetInterestDateFromRate;
		}
		// vbPorter upgrade warning: lngRateKey As int	OnWriteFCConvert.ToDouble(
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, double)
		public static string GetInterestRateFromRate_8(int lngRateKey, bool boolWater)
		{
			return GetInterestRateFromRate(ref lngRateKey, ref boolWater);
		}

		public static string GetInterestRateFromRate(ref int lngRateKey, ref bool boolWater)
		{
			string GetInterestRateFromRate = "";
			// vbPorter upgrade warning: dblTemp As double	OnWrite(double, string)
			double dblTemp = 0;
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				GetInterestRateFromRate = "";
				clsLoad.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRateKey), "twut0000.vb1");
				if (!clsLoad.EndOfFile())
				{
					if (boolWater)
					{
						dblTemp = Conversion.Val(clsLoad.Get_Fields_Double("WintRate"));
					}
					else
					{
						dblTemp = Conversion.Val(clsLoad.Get_Fields_Double("SintRate"));
					}
					dblTemp = FCConvert.ToDouble(Strings.Format(dblTemp, "0.0000"));
					GetInterestRateFromRate = FCConvert.ToString(dblTemp);
				}
				return GetInterestRateFromRate;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In GetInterestRateFromRate", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetInterestRateFromRate;
		}

		public static string GetCategoryForAccount_24(int lngAccountKey, bool boolWater, bool boolShort)
		{
			return GetCategoryForAccount(ref lngAccountKey, ref boolWater, ref boolShort);
		}

		public static string GetCategoryForAccount(ref int lngAccountKey, ref bool boolWater, ref bool boolShort)
		{
			string GetCategoryForAccount = "";
			clsDRWrapper clsLoad = new clsDRWrapper();
			try
			{
				// On Error GoTo ErrorHandler
				GetCategoryForAccount = "";
				if (boolWater)
				{
					clsLoad.OpenRecordset("SELECT Category.* FROM Master INNER JOIN Category ON (Master.WaterCategory = Category.Code) WHERE Master.ID = " + FCConvert.ToString(lngAccountKey), "twut0000.vb1");
				}
				else
				{
					clsLoad.OpenRecordset("SELECT Category.* FROM Master INNER JOIN Category ON (Master.SewerCategory = Category.Code) WHERE Master.ID = " + FCConvert.ToString(lngAccountKey), "twut0000.vb1");
				}
				if (!clsLoad.EndOfFile())
				{
					if (boolShort)
					{
						GetCategoryForAccount = FCConvert.ToString(clsLoad.Get_Fields_String("shortdescription"));
					}
					else
					{
						GetCategoryForAccount = FCConvert.ToString(clsLoad.Get_Fields_String("LongDescription"));
					}
				}
				return GetCategoryForAccount;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In GetCategoryForAccount", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetCategoryForAccount;
		}

		public static double GetLienAmountForAccount(ref int lngAccountKey, ref bool boolWater)
		{
			double GetLienAmountForAccount = 0;
			clsDRWrapper rsCL = new clsDRWrapper();
			clsDRWrapper rsLien = new clsDRWrapper();
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will calculate the total remaining balance and
				// return it as a double for the account and type passed in
				string strSQL;
				double dblTotal = 0;
				double dblXtraInt = 0;
				string strWS = "";
				GetLienAmountForAccount = 0;
				if (boolWater)
				{
					strWS = "W";
				}
				else
				{
					strWS = "S";
				}
				strSQL = "SELECT * FROM Bill WHERE Accountkey = " + FCConvert.ToString(lngAccountKey);
				rsLien.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
				rsCL.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!rsCL.EndOfFile())
				{
					while (!rsCL.EndOfFile())
					{
						// calculate each year
						if (FCConvert.ToInt32(rsCL.Get_Fields(strWS + "LienRecordNumber")) == 0)
						{
							// non-lien
							// dblTotal = dblTotal + CalculateAccountUT(rsCL, Date, dblXtraInt, boolWater, , rsCL.Fields("DemandFees") - rsCL.Fields("DemandFeesPaid"))
						}
						else
						{
							// lien
							rsLien.FindFirstRecord("ID", rsCL.Get_Fields(strWS + "LienrecordNumber"));
							if (rsLien.NoMatch)
							{
								// no match has been found
								// do nothing
							}
							else
							{
								// calculate the lien record
								dblTotal += modUTCalculations.CalculateAccountUTLien(rsLien, DateTime.Today, ref dblXtraInt, boolWater);
							}
						}
						rsCL.MoveNext();
					}
				}
				else
				{
					// nothing found
					dblTotal = 0;
				}
				GetLienAmountForAccount = dblTotal;
				rsCL.Reset();
				rsLien.Reset();
				return GetLienAmountForAccount;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Calculate Account Total Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				rsLien.Reset();
				rsCL.Reset();
			}
			return GetLienAmountForAccount;
		}

		private static void CalcThisQuarterStuff(int lngBillingRateKey, ref double dblReadRatio)
		{
			// calcs consumption,service days and average consumption
			// vbPorter upgrade warning: dttemp As DateTime	OnWrite(DateTime, string)
			DateTime dttemp;
			DateTime dtTemp2 = default(DateTime);
			int lngTemp;
			string strTemp;
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				dttemp = DateAndTime.DateValue(GetBillingDateFromRate(lngBillingRateKey));
				lngTemp = dttemp.Year;
				if (dttemp.Month >= 1 && dttemp.Month <= 3)
				{
					dtTemp2 = dttemp;
					// don't get last day of quarter, do last day as of this bill
					dttemp = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngTemp));
				}
				else if (dttemp.Month >= 4 && dttemp.Month <= 6)
				{
					dtTemp2 = dttemp;
					dttemp = FCConvert.ToDateTime("4/1/" + FCConvert.ToString(lngTemp));
				}
				else if (dttemp.Month >= 7 && dttemp.Month <= 9)
				{
					dtTemp2 = dttemp;
					dttemp = FCConvert.ToDateTime("7/1/" + FCConvert.ToString(lngTemp));
				}
				else if (dttemp.Month >= 10 && dttemp.Month <= 12)
				{
					dtTemp2 = dttemp;
					dttemp = FCConvert.ToDateTime("10/1/" + FCConvert.ToString(lngTemp));
				}
				strTemp = modUTCalculations.GetConsumptionAndAverageByDateRange(ref dttemp, ref dtTemp2, Statics.lngThisQuarterAccount, dblReadRatio);
				if (strTemp != string.Empty)
				{
					strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
					Statics.lngThisQuarterConsumption = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					Statics.lngThisQuarterServiceDays = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[2])));
					Statics.dblThisQuarterAverageConsumption = Conversion.Val(strAry[1]);
				}
				else
				{
					Statics.lngThisQuarterAccount = 0;
					Statics.lngThisQuarterConsumption = 0;
					Statics.lngThisQuarterServiceDays = 0;
					Statics.dblThisQuarterAverageConsumption = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CalcThisQuarterStuff", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CalcLastQuarterStuff(int lngBillingRateKey, ref double dblReadRatio)
		{
			// vbPorter upgrade warning: dttemp As DateTime	OnWrite(DateTime, string)
			DateTime dttemp;
			// vbPorter upgrade warning: dtTemp2 As DateTime	OnWrite(string)
			DateTime dtTemp2 = default(DateTime);
			int lngTemp;
			string strTemp;
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				dttemp = DateAndTime.DateValue(GetBillingDateFromRate(lngBillingRateKey));
				lngTemp = dttemp.Year;
				if (dttemp.Month >= 1 && dttemp.Month <= 3)
				{
					dtTemp2 = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngTemp - 1));
					dttemp = FCConvert.ToDateTime("10/1/" + FCConvert.ToString(lngTemp - 1));
				}
				else if (dttemp.Month >= 4 && dttemp.Month <= 6)
				{
					dtTemp2 = FCConvert.ToDateTime("3/31/" + FCConvert.ToString(lngTemp));
					dttemp = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngTemp));
				}
				else if (dttemp.Month >= 7 && dttemp.Month <= 9)
				{
					dtTemp2 = FCConvert.ToDateTime("6/30/" + FCConvert.ToString(lngTemp));
					dttemp = FCConvert.ToDateTime("4/1/" + FCConvert.ToString(lngTemp));
				}
				else if (dttemp.Month >= 10 && dttemp.Month <= 12)
				{
					dtTemp2 = FCConvert.ToDateTime("9/30/" + FCConvert.ToString(lngTemp));
					dttemp = FCConvert.ToDateTime("7/1/" + FCConvert.ToString(lngTemp));
				}
				strTemp = modUTCalculations.GetConsumptionAndAverageByDateRange(ref dttemp, ref dtTemp2, Statics.lngLastQuarterAccount, dblReadRatio);
				if (strTemp != string.Empty)
				{
					strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
					Statics.lngLastQuarterConsumption = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					Statics.lngLastQuarterServiceDays = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[2])));
					Statics.dblLastQuarterAverageConsumption = Conversion.Val(strAry[1]);
				}
				else
				{
					Statics.lngLastQuarterAccount = 0;
					Statics.lngLastQuarterConsumption = 0;
					Statics.lngLastQuarterServiceDays = 0;
					Statics.dblLastQuarterAverageConsumption = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CalcLastQuarterStuff", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CalcLastYearStuff(int lngBillingRateKey, ref double dblReadRatio)
		{
			// vbPorter upgrade warning: dttemp As DateTime	OnWrite(DateTime, string)
			DateTime dttemp;
			// vbPorter upgrade warning: dtTemp2 As DateTime	OnWrite(string)
			DateTime dtTemp2;
			int lngTemp;
			string strTemp;
			string[] strAry = null;
			try
			{
				// On Error GoTo ErrorHandler
				dttemp = DateAndTime.DateValue(GetBillingDateFromRate(lngBillingRateKey));
				lngTemp = dttemp.Year - 1;
				dttemp = FCConvert.ToDateTime("1/1/" + FCConvert.ToString(lngTemp));
				dtTemp2 = FCConvert.ToDateTime("12/31/" + FCConvert.ToString(lngTemp));
				strTemp = modUTCalculations.GetConsumptionAndAverageByDateRange(ref dttemp, ref dtTemp2, Statics.lngLastYearAccount, dblReadRatio);
				if (strTemp != string.Empty)
				{
					strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
					Statics.lngLastYearConsumption = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[0])));
					Statics.lngLastYearServiceDays = FCConvert.ToInt32(Math.Round(Conversion.Val(strAry[2])));
					Statics.dblLastYearAverageConsumption = Conversion.Val(strAry[1]);
				}
				else
				{
					Statics.lngLastYearAccount = 0;
					Statics.lngLastYearConsumption = 0;
					Statics.lngLastYearServiceDays = 0;
					Statics.dblLastYearAverageConsumption = 0;
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In CalcLastYearStuff", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngBillKey As int	OnRead(string)
		// vbPorter upgrade warning: dblSTaxOwed As double	OnRead(string)
		// vbPorter upgrade warning: dblSMisc As double	OnRead(string)
		// vbPorter upgrade warning: dblSadj As double	OnRead(string)
		private static void GetSewerDetailForBillKey_720(int lngBillKey, int lngAccountKey, double dblSPrinOwed, double dblSTaxOwed, double dblSMisc = 0, double dblSadj = 0, int lngAdjustmentOption = 0, bool boolOverride = false, DateTime? dtDateTemp = null/* DateTime dtDate = DateTime.Now */, bool boolBreakdownReg = false)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			GetSewerDetailForBillKey(ref lngBillKey, ref lngAccountKey, ref dblSPrinOwed, ref dblSTaxOwed, dblSMisc, dblSadj, lngAdjustmentOption, boolOverride, dtDate, boolBreakdownReg);
		}

		private static void GetSewerDetailForBillKey_2907(int lngBillKey, int lngAccountKey, double dblSPrinOwed, double dblSTaxOwed, double dblSMisc = 0, double dblSadj = 0, int lngAdjustmentOption = 0, DateTime? dtDateTemp = null/* DateTime dtDate = DateTime.Now */, bool boolBreakdownReg = false)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			GetSewerDetailForBillKey(ref lngBillKey, ref lngAccountKey, ref dblSPrinOwed, ref dblSTaxOwed, dblSMisc, dblSadj, lngAdjustmentOption, false, dtDate, boolBreakdownReg);
		}

		private static void GetSewerDetailForBillKey_3312(int lngBillKey, int lngAccountKey, double dblSPrinOwed, double dblSTaxOwed, DateTime? dtDateTemp = null/* DateTime dtDate = DateTime.Now */, bool boolBreakdownReg = false)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			GetSewerDetailForBillKey(ref lngBillKey, ref lngAccountKey, ref dblSPrinOwed, ref dblSTaxOwed, 0, 0, 0, false, dtDate, boolBreakdownReg);
		}

		private static void GetSewerDetailForBillKey(ref int lngBillKey, ref int lngAccountKey, ref double dblSPrinOwed, ref double dblSTaxOwed, double dblSMisc = 0, double dblSadj = 0, int lngAdjustmentOption = 0, bool boolOverride = false, DateTime? dtDateTemp = null/* DateTime dtDate = DateTime.Now */, bool boolBreakdownReg = false)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			int x;
			// vbPorter upgrade warning: dblRegular As double	OnRead(string)
			double dblRegular;
			// vbPorter upgrade warning: dblPastDue As double	OnRead(string)
			double dblPastDue = 0;
			// vbPorter upgrade warning: dblLiens As double	OnRead(string)
			double dblLiens = 0;
			// vbPorter upgrade warning: dblInterest As double	OnRead(string)
			double dblInterest;
			double dblTotal;
			double dblCurInt = 0;
			double dblTotCharInt = 0;
			clsDRWrapper rsUTCust = new clsDRWrapper();
			// kgk
			// vbPorter upgrade warning: dblCurCost As double	OnRead(string)
			double dblCurCost = 0;
			rsUTCust.OpenRecordset("SELECT * FROM  UtilityBilling", modExtraModules.strUTDatabase);
			// kgk
			for (x = 1; x <= 8; x++)
			{
				Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNBILLNUM] = FCConvert.ToString(lngBillKey);
				Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION] = "";
				Statics.DetailBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(0);
			}
			// x
			for (x = 1; x <= Information.UBound(Statics.AdjBreakdownSewer, 1); x++)
			{
				Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNBILLNUM] = FCConvert.ToString(lngBillKey);
				Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION] = "";
				Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(0);
			}
			// x
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			int lngAsOfRateKey = 0;
			bool blnIsMHReport = false;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			bool boolWater = false;
			bool boolGetOnlyLienAmount = true;
			if (modExtraModules.Statics.gboolShowLienAmountOnBill)
			{
				// dblLiens = GetLienAmountForAccount(lngAccountKey, False)             
				dblLiens = modUTCalculations.CalculateAccountUTTotal(lngAccountKey, boolWater, true, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, boolGetOnlyLienAmount, lngAsOfRateKey, blnIsMHReport, ref dblTotalCost, ref dblTotalPLI);
			}
			else
			{
				dblLiens = 0;
			}
			boolGetOnlyLienAmount = false;
			lngAsOfRateKey = 0;
			blnIsMHReport = false;
			dblTotalPLI = 0;
			boolWater = false;
			dblTotal = modUTCalculations.CalculateAccountUTTotal(lngAccountKey,  boolWater, false, ref dblCurInt, ref dblTotCharInt, ref dtDate, boolGetOnlyLienAmount, lngAsOfRateKey, blnIsMHReport, ref dblCurCost, ref dblTotalPLI);
			// dblTotal = dblTotal - dblLiens
			dblInterest = dblTotCharInt + dblCurInt;
			dblTotal -= dblInterest;
			// dbltotal should now be past due + current due
			dblRegular = dblSPrinOwed;
			// kgk 10-20-2011 trout-766  Add option to itemize adjustments
			// If Not boolSeparateAdjustments Then
			if (lngAdjustmentOption == 0)
			{
				// Do not separate
				dblPastDue = dblTotal - dblRegular - dblSTaxOwed;
			}
			else
			{
				dblRegular += -dblSadj - dblSMisc;
				if (lngAdjustmentOption == 1)
				{
					dblPastDue = dblTotal - dblRegular - dblSTaxOwed - dblSadj - dblSMisc;
				}
				else
				{
					// kgk 11-11-2011 trout-780  Break out cost if itemizing adjustments
					dblPastDue = dblTotal - dblRegular - dblSTaxOwed - dblSadj - dblSMisc - dblCurCost;
				}
			}
			if (dblPastDue >= 0)
			{
				if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_PastDue"))) == "")
				{
					// kgk trout-683 03-04-11
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWPASTDUE, CNSTDETAILBREAKDOWNDESCRIPTION] = "Past Due";
				}
				else
				{
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWPASTDUE, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_PastDue"));
				}
			}
			else
			{
				dblPastDue *= -1;
				if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Credit"))) == "")
				{
					// kgk trout-683 03-04-11
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWPASTDUE, CNSTDETAILBREAKDOWNDESCRIPTION] = "Credit";
				}
				else
				{
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWPASTDUE, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Credit"));
				}
			}
			// kgk 06-25-2012 trout-839  Separate REGULAR into CONS, UNIT, FLAT amounts
			if (boolBreakdownReg)
			{
				GetBillBreakDown_6(lngBillKey, "S");
			}
			Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWREGULAR, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblRegular);
			Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWTAX, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblSTaxOwed);
			Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWINTEREST, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblInterest);
			Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWLIENS, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblLiens);
			Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWPASTDUE, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblPastDue);
			if (!boolOverride)
			{
				if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Regular"))) == "")
				{
					// kgk trout-683 03-04-11
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWREGULAR, CNSTDETAILBREAKDOWNDESCRIPTION] = "Regular";
				}
				else
				{
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWREGULAR, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Regular"));
				}
			}
			else
			{
				if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Override"))) == "")
				{
					// kgk trout-683 03-04-11
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWREGULAR, CNSTDETAILBREAKDOWNDESCRIPTION] = "Override";
				}
				else
				{
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWREGULAR, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Override"));
				}
			}
			if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Interest"))) == "")
			{
				// kgk trout-683 03-04-11
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWINTEREST, CNSTDETAILBREAKDOWNDESCRIPTION] = "Interest";
			}
			else
			{
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWINTEREST, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Interest"));
			}
			if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Liens"))) == "")
			{
				// kgk trout-683 03-04-11
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWLIENS, CNSTDETAILBREAKDOWNDESCRIPTION] = "Liens";
			}
			else
			{
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWLIENS, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Liens"));
			}
			if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Tax"))) == "")
			{
				// kgk trout-683 03-04-11
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWTAX, CNSTDETAILBREAKDOWNDESCRIPTION] = "Sales Tax";
				// kgk 12-01-2011 trout-766  Per Brenda change "Tax" to "Sales Tax"
			}
			else
			{
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWTAX, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Tax"));
			}
			if (lngAdjustmentOption == 1)
			{
				// Separate  'kgk 10-21-2011 trout-766    If boolSeparateAdjustments Then
				if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Misc"))) == "")
				{
					// kgk trout-683 03-04-11
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNDESCRIPTION] = "Misc";
				}
				else
				{
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Misc"));
				}
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblSMisc);
				if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Adj"))) == "")
				{
					// kgk trout-683 03-04-11
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNDESCRIPTION] = "Adj";
				}
				else
				{
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Adj"));
				}
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblSadj);
			}
			else if (lngAdjustmentOption == 2)
			{
				// Itemize
				if (Strings.Trim(FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Misc"))) == "")
				{
					// kgk trout-683 03-04-11
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNDESCRIPTION] = "Misc";
				}
				else
				{
					Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsUTCust.Get_Fields_String("RptDesc_Misc"));
				}
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblSMisc);
				GetAdjBreakdown_6(lngBillKey, "S", dblSadj);
				// kgk 11-11-2011 trout-780  Break cost out from past due when itemizing
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWCOST, CNSTDETAILBREAKDOWNDESCRIPTION] = "Costs";
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWCOST, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblCurCost);
			}
			else
			{
				// Do not separate
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNDESCRIPTION] = "";
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(0);
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNDESCRIPTION] = "";
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(0);
			}
		}
		// vbPorter upgrade warning: lngBillKey As int	OnRead(string)
		// vbPorter upgrade warning: dblWTaxOwed As double	OnRead(string)
		// vbPorter upgrade warning: dblWMisc As double	OnRead(string)
		// vbPorter upgrade warning: dblWAdj As double	OnRead(string)
		private static void GetWaterDetailForBillKey_2097(int lngBillKey, int lngAccountKey, double dblWPrinOwed, double dblWTaxOwed, double dblWMisc = 0, double dblWAdj = 0, int lngAdjustmentOption = 0, bool boolOverride = false, DateTime? dtDateTemp = null/* DateTime dtDate = DateTime.Now */, bool boolBreakdownReg = false)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			GetWaterDetailForBillKey(ref lngBillKey, ref lngAccountKey, ref dblWPrinOwed, ref dblWTaxOwed, 0, dblWMisc, dblWAdj, lngAdjustmentOption, boolOverride, dtDate, boolBreakdownReg);
		}

		private static void GetWaterDetailForBillKey_8658(int lngBillKey, int lngAccountKey, double dblWPrinOwed, double dblWTaxOwed, double dblWMisc = 0, double dblWAdj = 0, int lngAdjustmentOption = 0, DateTime? dtDateTemp = null/* DateTime dtDate = DateTime.Now */, bool boolBreakdownReg = false)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			GetWaterDetailForBillKey(ref lngBillKey, ref lngAccountKey, ref dblWPrinOwed, ref dblWTaxOwed, 0, dblWMisc, dblWAdj, lngAdjustmentOption, false, dtDate, boolBreakdownReg);
		}

		private static void GetWaterDetailForBillKey_10845(int lngBillKey, int lngAccountKey, double dblWPrinOwed, double dblWTaxOwed, double dblWMisc = 0, double dblWAdj = 0, DateTime? dtDateTemp = null/* DateTime dtDate = DateTime.Now */, bool boolBreakdownReg = false)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			GetWaterDetailForBillKey(ref lngBillKey, ref lngAccountKey, ref dblWPrinOwed, ref dblWTaxOwed, 0, dblWMisc, dblWAdj, 0, false, dtDate, boolBreakdownReg);
		}

		private static void GetWaterDetailForBillKey(ref int lngBillKey, ref int lngAccountKey, ref double dblWPrinOwed, ref double dblWTaxOwed, double dblWPrinPaid = 0, double dblWMisc = 0, double dblWAdj = 0, int lngAdjustmentOption = 0, bool boolOverride = false, DateTime? dtDateTemp = null/* ref DateTime dtDate = DateTime.Now */, bool boolBreakdownReg = false)
		{
			DateTime dtDate = dtDateTemp ?? DateTime.Now;
			int x;
			// vbPorter upgrade warning: dblRegular As double	OnRead(string)
			double dblRegular;
			// vbPorter upgrade warning: dblPastDue As double	OnRead(string)
			double dblPastDue = 0;
			// vbPorter upgrade warning: dblLiens As double	OnRead(string)
			double dblLiens = 0;
			// vbPorter upgrade warning: dblInterest As double	OnRead(string)
			double dblInterest;
			double dblTotal;
			double dblCurInt = 0;
			double dblTotCharInt = 0;
			// vbPorter upgrade warning: dblCurCost As double	OnRead(string)
			double dblCurCost = 0;
			for (x = 1; x <= 8; x++)
			{
				Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNBILLNUM] = FCConvert.ToString(lngBillKey);
				Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION] = "";
				Statics.DetailBreakDownWater[x, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(0);
			}
			// x
			for (x = 1; x <= Information.UBound(Statics.AdjBreakdownWater, 1); x++)
			{
				Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNBILLNUM] = FCConvert.ToString(lngBillKey);
				Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION] = "";
				Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(0);
			}
			// x
			double dblTotalCurrentInterest = 0;
			double dblTotalChargedInterest = 0;
			int lngAsOfRateKey = 0;
			bool blnIsMHReport = false;
			double dblTotalCost = 0;
			double dblTotalPLI = 0;
			bool boolWater = true;
			bool boolGetOnlyLienAmount = true;
			if (modExtraModules.Statics.gboolShowLienAmountOnBill)
			{
				// dblLiens = GetLienAmountForAccount(lngAccountKey, True)
				dblLiens = modUTCalculations.CalculateAccountUTTotal(lngAccountKey, boolWater, true, ref dblTotalCurrentInterest, ref dblTotalChargedInterest, ref dtDate, boolGetOnlyLienAmount, lngAsOfRateKey, blnIsMHReport, ref dblTotalCost, ref dblTotalPLI);
			}
			else
			{
				dblLiens = 0;
			}
			dblTotalPLI = 0;
			blnIsMHReport = false;
			lngAsOfRateKey = 0;
			boolGetOnlyLienAmount = false;
			boolWater = true;
			dblTotal = modUTCalculations.CalculateAccountUTTotal(lngAccountKey,  boolWater, false, ref dblCurInt, ref dblTotCharInt, ref dtDate, boolGetOnlyLienAmount, lngAsOfRateKey, blnIsMHReport, ref dblCurCost, ref dblTotalPLI);
			// dblTotal = dblTotal - dblLiens
			dblInterest = dblTotCharInt + dblCurInt;
			dblTotal -= dblInterest;
			// dbltotal should now be past due + current due
			dblRegular = dblWPrinOwed;
			// kgk 10-20-2011 trout-766  Add option to itemize adjustments
			// If Not boolSeparateAdjustments Then
			if (lngAdjustmentOption == 0)
			{
				// Do not separate
				dblPastDue = dblTotal - dblRegular - dblWTaxOwed;
			}
			else
			{
				dblRegular += -dblWAdj - dblWMisc;
				if (lngAdjustmentOption == 1)
				{
					dblPastDue = dblTotal - dblRegular - dblWTaxOwed - dblWAdj - dblWMisc;
				}
				else
				{
					// kgk 11-11-2011 trout-780  Break out cost if itemizing adjustments
					dblPastDue = dblTotal - dblRegular - dblWTaxOwed - dblWAdj - dblWMisc - dblCurCost;
				}
			}
			if (dblPastDue >= 0)
			{
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWPASTDUE, CNSTDETAILBREAKDOWNDESCRIPTION] = "Past Due";
			}
			else
			{
				dblPastDue *= -1;
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWPASTDUE, CNSTDETAILBREAKDOWNDESCRIPTION] = "Credit";
			}
			// kgk 06-25-2012 trout-839  Separate REGULAR into CONS, UNIT, FLAT amounts
			if (boolBreakdownReg)
			{
				GetBillBreakDown_6(lngBillKey, "W");
			}
			Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWREGULAR, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblRegular);
			Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWTAX, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblWTaxOwed);
			Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWINTEREST, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblInterest);
			Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWLIENS, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblLiens);
			Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWPASTDUE, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblPastDue);
			if (!boolOverride)
			{
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWREGULAR, CNSTDETAILBREAKDOWNDESCRIPTION] = "Regular";
			}
			else
			{
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWREGULAR, CNSTDETAILBREAKDOWNDESCRIPTION] = "Override";
			}
			Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWTAX, CNSTDETAILBREAKDOWNDESCRIPTION] = "Sales Tax";
			// kgk 12-01-2011 trout-766  Per Brenda change "Tax" to "Sales Tax"
			if (lngAdjustmentOption == 1)
			{
				// Separate  'kgk 10-21-2011 trout-766    If boolSeparateAdjustments Then
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNDESCRIPTION] = "Misc";
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblWMisc);
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNDESCRIPTION] = "Adj";
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblWAdj);
			}
			else if (lngAdjustmentOption == 2)
			{
				// Itemize
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNDESCRIPTION] = "Misc";
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblWMisc);
				GetAdjBreakdown_6(lngBillKey, "W", dblWAdj);
				// kgk 11-14-2011 trout-780  Break cost out from past due when itemizing
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWCOST, CNSTDETAILBREAKDOWNDESCRIPTION] = "Costs";
				Statics.DetailBreakdownSewer[CNSTDETAILBREAKDOWNROWCOST, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblCurCost);
			}
			else
			{
				// Do not separate
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNDESCRIPTION] = "";
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWMISC, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(0);
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNDESCRIPTION] = "";
				Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWADJ, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(0);
			}
			Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWINTEREST, CNSTDETAILBREAKDOWNDESCRIPTION] = "Interest";
			Statics.DetailBreakDownWater[CNSTDETAILBREAKDOWNROWLIENS, CNSTDETAILBREAKDOWNDESCRIPTION] = "Liens";
		}

		private static void GetMeterBreakdownForBillKey_8(int lngBillKey, int lngAccountKey, int lngMK = 0)
		{
			GetMeterBreakdownForBillKey(lngBillKey, lngAccountKey, lngMK);
		}

		private static void GetMeterBreakdownForBillKey(int lngBillKey, int lngAccountKey, int lngMK = 0)
		{
			// fills array with meter info from breakdown table
			int x;
			clsDRWrapper clsLoad = new clsDRWrapper();
			clsDRWrapper clsMeter = new clsDRWrapper();
			clsDRWrapper rsMtrCons = new clsDRWrapper();
			clsDRWrapper rsCheckBD = new clsDRWrapper();
			string strSQL = "";
			int lngAdj = 0;
			int lngDigits = 0;
			int lngMtrCons;
			bool boolNoBreakdown = false;
			bool boolTotalUnits;
			if (Statics.lngMetersLoadedBillKey != lngBillKey)
			{
				Statics.lngNumMetersLoaded = 0;
				Statics.lngMetersLoadedBillKey = 0;
				FCUtils.EraseSafe(Statics.aryMeterBreakdown);
				if (lngMK != 0)
				{
					rsCheckBD.OpenRecordset("SELECT * FROM Breakdown WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND MeterKey = " + FCConvert.ToString(lngMK), modExtraModules.strUTDatabase);
					if (rsCheckBD.EndOfFile())
					{
						// kgk 07312012     strSQL = "SELECT * FROM MeterTable left join frequency on (frequency.code = metertable.frequency) WHERE MeterTable.ID = " & lngMK
						strSQL = "SELECT MeterTable.ID AS MeterKey,MeterNumber,PreviousReading,CurrentReading,CurrentCode,PrevChangeOut,Service,PreviousReadingdate,CurrentReadingDate,Frequency.LongDescription,Frequency.ShortDescription " + "FROM MeterTable LEFT JOIN Frequency ON (Frequency.Code = MeterTable.Frequency) WHERE MeterTable.ID = " + FCConvert.ToString(lngMK);                       
                        boolNoBreakdown = true;
					}
					else
					{
						strSQL = "SELECT tbl1.MeterKey,MeterNumber,PreviousReading,CurrentReading,CurrentCode,PrevChangeout,Service,PreviousReadingDate,CurrentReadingDate,Frequency.LongDescription,Frequency.ShortDescription FROM Frequency RIGHT JOIN (MeterTable INNER JOIN (SELECT MeterKey,BillKey FROM Breakdown GROUP BY MeterKey,BillKey) AS tbl1 ON (tbl1.MeterKey = MeterTable.ID)) ON (Frequency.Code = MeterTable.Frequency) WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " ORDER BY MeterNumber";
					}
				}
				else
				{
					strSQL = "SELECT tbl1.MeterKey,MeterNumber,PreviousReading,CurrentReading,CurrentCode,PrevChangeout,Service,PreviousReadingDate,CurrentReadingDate,Frequency.LongDescription,Frequency.ShortDescription FROM Frequency RIGHT JOIN (MeterTable INNER JOIN (SELECT MeterKey,BillKey FROM Breakdown GROUP BY MeterKey,BillKey) AS tbl1 ON (tbl1.MeterKey = MeterTable.ID)) ON (Frequency.Code = MeterTable.Frequency) WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " ORDER BY MeterNumber";
				}
				clsLoad.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (!clsLoad.EndOfFile())
				{
					while (!clsLoad.EndOfFile())
					{
						if ((Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("service"))) == "B") || (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("Service"))) == "W" && Statics.boolUseWaterBills) || (Strings.UCase(FCConvert.ToString(clsLoad.Get_Fields_String("Service"))) == "S" && Statics.boolUseSewerBills))
						{
							Statics.lngNumMetersLoaded += 1;
							Array.Resize(ref Statics.aryMeterBreakdown, Statics.lngNumMetersLoaded + 1);
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AccountKey = lngAccountKey;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Amount = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountSewer = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountWater = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].BillKey = lngBillKey;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Current = FCConvert.ToString(clsLoad.Get_Fields_Int32("currentreading"));
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Frequency = FCConvert.ToString(clsLoad.Get_Fields_String("LongDescription"));
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].FrequencyShort = FCConvert.ToString(clsLoad.Get_Fields_String("ShortDescription"));
							if (clsLoad.IsFieldNull("currentreadingdate"))
							{
								Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].CurrentDate = (DateTime)clsLoad.Get_Fields_DateTime("currentreadingdate");
							}
							else
							{
								Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].CurrentDate = DateAndTime.DateValue(FCConvert.ToString(0));
							}
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].CurrentCode = FCConvert.ToString(clsLoad.Get_Fields_String("currentcode"));
							// actual or estimated
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].CurrentSewer = "";
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].CurrentWater = "";
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].MeterKey = FCConvert.ToInt32(clsLoad.Get_Fields_Int32("meterkey"));
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Previous = FCConvert.ToString(clsLoad.Get_Fields_Int32("previousreading"));
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].PreviousDate = (DateTime)clsLoad.Get_Fields_DateTime("previousreadingdate");
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].PreviousSewer = "";
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].PreviousWater = "";
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Service = FCConvert.ToString(clsLoad.Get_Fields_String("service"));
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Tax = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].TaxSewer = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].TaxWater = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Type = "";
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Units = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].UnitsSewer = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].UnitsWater = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Value = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].ValueSewer = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].ValueWater = 0;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].ChangedOut = FCConvert.ToBoolean(clsLoad.Get_Fields_Boolean("PrevChangeOut"));
							// kk01232015 trout-1061  Get the actual meter consumption; the breakdown is the billed consumption and could be an override value (minimum)
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Consumption = -1;
							lngDigits = FCConvert.ToInt32(Math.Round(Conversion.Val(clsLoad.Get_Fields_Int32("Digits"))));
							rsCheckBD.OpenRecordset("SELECT BillDate FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey), modExtraModules.strUTDatabase);
							if (!rsCheckBD.EndOfFile())
							{
								strSQL = "SELECT * FROM MeterConsumption WHERE MeterKey = " + clsLoad.Get_Fields_Int32("MeterKey") + " AND AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillKey = " + FCConvert.ToString(lngBillKey);
								rsMtrCons.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
								if (!rsMtrCons.EndOfFile())
								{
									Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Consumption = FCConvert.ToInt32(rsMtrCons.Get_Fields_Int32("Consumption"));
                                    Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Previous =
                                        rsMtrCons.Get_Fields_Int32("Begin").ToString();
                                    Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Current =
                                        rsMtrCons.Get_Fields_Int32("end").ToString();
                                    Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].PreviousDate =
                                        rsMtrCons.Get_Fields_DateTime("PreviousReadingDate");
                                    Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].CurrentDate =
                                        rsMtrCons.Get_Fields_DateTime("CurrentReadingDate");
                                }
								else
								{
									strSQL = "SELECT * FROM MeterConsumption WHERE MeterKey = " + clsLoad.Get_Fields_Int32("MeterKey") + " AND AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND BillDate = '" + Strings.Format(rsCheckBD.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy") + "'";
									rsMtrCons.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
									if (!rsMtrCons.EndOfFile())
									{
										Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Consumption = FCConvert.ToInt32(rsMtrCons.Get_Fields_Int32("Consumption"));
									}
								}
							}
							if (Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Consumption < 0 && Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Current) >= 0)
							{
								if (Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Current) >= Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Previous))
								{
									Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Consumption = FCConvert.ToInt32(Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Current) - Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Previous));
								}
								else
								{
									switch (modExtraModules.Statics.glngTownReadingUnits)
									{
										case 100000:
											{
												lngAdj = 5;
												break;
											}
										case 10000:
											{
												lngAdj = 4;
												break;
											}
										case 1000:
											{
												lngAdj = 3;
												break;
											}
										case 100:
											{
												lngAdj = 2;
												break;
											}
										case 10:
											{
												lngAdj = 1;
												break;
											}
										case 1:
											{
												lngAdj = 0;
												break;
											}
										default:
											{
												lngAdj = 0;
												break;
											}
									}
									//end switch
									// this must be a rolled over meter
									Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Consumption = FCConvert.ToInt32(Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Current) + ((Math.Pow(10, (lngDigits - lngAdj))) - Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Previous)));
								}
							}
							strSQL = "select * from breakdown where billkey = " + FCConvert.ToString(lngBillKey) + " and meterkey = " + clsLoad.Get_Fields_Int32("meterkey") + " ORDER BY Service, Type, Description";
							clsMeter.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
							while (!clsMeter.EndOfFile())
							{
								// aryMeterBreakdown(lngNumMetersLoaded).Type = UCase(clsMeter.Fields("Type"))
								if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields_String("service"))) == "S")
								{
									// sewer
									if (Statics.boolUseSewerBills)
									{
										// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
										if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields("Type"))) == "T")
										{
											// tax
											// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].TaxSewer += Conversion.Val(clsMeter.Get_Fields("Amount"));
										}
										// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
										else if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields("Type"))) == "C")
										{
											// consumption
											// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].ValueSewer = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMeter.Get_Fields("value"))));
											// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountSewer += Conversion.Val(clsMeter.Get_Fields("amount"));
										}
											// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
											else if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields("Type"))) == "F")
										{
											// flat
											// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountSewer += Conversion.Val(clsMeter.Get_Fields("amount"));
										}
												// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
												else if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields("Type"))) == "U")
										{
											// units
											// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].UnitsSewer = Conversion.Val(clsMeter.Get_Fields("value"));
											// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountSewer += Conversion.Val(clsMeter.Get_Fields("amount"));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountSewer += Conversion.Val(clsMeter.Get_Fields("amount"));
										}
									}
								}
								else if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields_String("service"))) == "W")
								{
									// water
									if (Statics.boolUseWaterBills)
									{
										// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
										if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields("Type"))) == "T")
										{
											// tax
											// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].TaxWater += Conversion.Val(clsMeter.Get_Fields("Amount"));
										}
											// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
											else if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields("Type"))) == "C")
										{
											// consumption
											// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].ValueSewer = FCConvert.ToInt32(Math.Round(Conversion.Val(clsMeter.Get_Fields("value"))));
											// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountWater += Conversion.Val(clsMeter.Get_Fields("amount"));
										}
												// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
												else if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields("Type"))) == "F")
										{
											// flat
											// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountWater += Conversion.Val(clsMeter.Get_Fields("amount"));
										}
													// TODO Get_Fields: Check the table for the column [Type] and replace with corresponding Get_Field method
													else if (Strings.UCase(FCConvert.ToString(clsMeter.Get_Fields("Type"))) == "U")
										{
											// units
											// TODO Get_Fields: Check the table for the column [value] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].UnitsWater = Conversion.Val(clsMeter.Get_Fields("value"));
											// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountWater += Conversion.Val(clsMeter.Get_Fields("amount"));
										}
										else
										{
											// TODO Get_Fields: Check the table for the column [amount] and replace with corresponding Get_Field method
											Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountWater += Conversion.Val(clsMeter.Get_Fields("amount"));
										}
									}
								}
								clsMeter.MoveNext();
							}
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Amount = Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountSewer + Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].AmountWater;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Tax = Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].TaxSewer + Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].TaxWater;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Units = Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].UnitsSewer + Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].UnitsWater;
							Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Value = Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].ValueSewer + Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].ValueWater;
							if (boolNoBreakdown && Statics.lngNumMetersLoaded == 1)
							{
								Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Value = FCConvert.ToInt32(Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Current) - Conversion.Val(Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Previous));
							}
							// If .Units > 0 Then
							// .Previous = "Units = "
							// .Current = .Units
							// Else
							// leave it as whatever it is
							// End If
							// get rid of empty values
							if (Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Amount == 0 && Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Tax == 0 && Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Units == 0 && Statics.aryMeterBreakdown[Statics.lngNumMetersLoaded].Value == 0)
							{
								// lngNumMetersLoaded = lngNumMetersLoaded - 1
							}
						}
						clsLoad.MoveNext();
					}
					Statics.lngMetersLoadedBillKey = lngBillKey;
				}
			}
		}

		private static string GetMeterList_6(int lngBillKey, string strList, int lngAcctKey, int lngMK = 0)
		{
			return GetMeterList(lngBillKey, ref strList, lngAcctKey, lngMK);
		}

		private static string GetMeterList(int lngBillKey, ref string strList, int lngAcctKey, int lngMK = 0)
		{
			string GetMeterList = "";
			string[] strAry = null;
			int x;
			// vbPorter upgrade warning: Y As short --> As int	OnWrite(double, short)
			int Y;
			string[] aryTemp = null;
			string strTemp;
			GetMeterList = "";
			GetMeterBreakdownForBillKey(lngBillKey, lngAcctKey, lngMK);
			if (Statics.lngNumMetersLoaded < 1 || Statics.lngMetersLoadedBillKey != lngBillKey)
			{
				// nothing to report on
				return GetMeterList;
			}
			strTemp = strList;
			if (Strings.InStr(1, strTemp, ",", CompareConstants.vbTextCompare) > 0 || Strings.InStr(1, strTemp, "-", CompareConstants.vbTextCompare) > 0)
			{
				strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
				if (Strings.InStr(1, strTemp, "-", CompareConstants.vbTextCompare) > 0)
				{
					// must break up the range so strary has a complete comma delimted list
					strTemp = "";
					for (x = 0; x <= Information.UBound(strAry, 1); x++)
					{
						if (Strings.InStr(1, strAry[x], "-", CompareConstants.vbTextCompare) <= 0)
						{
							strTemp += strAry[x] + ",";
						}
						else
						{
							// need to make a list of this range
							aryTemp = Strings.Split(strAry[x], "-", -1, CompareConstants.vbTextCompare);
							for (Y = FCConvert.ToInt32(Conversion.Val(aryTemp[0])); Y <= FCConvert.ToInt32(Conversion.Val(aryTemp[1])); Y++)
							{
								strTemp += FCConvert.ToString(Y) + ",";
							}
							// Y
						}
					}
					// x
					if (strTemp != string.Empty)
					{
						strTemp = Strings.Mid(strTemp, 1, strTemp.Length - 1);
						// take off the last comma
					}
					strAry = Strings.Split(strTemp, ",", -1, CompareConstants.vbTextCompare);
				}
			}
			else
			{
				GetMeterList = strList;
			}
			return GetMeterList;
		}

		public static bool AddNewCustomBillCodes()
		{
			bool AddNewCustomBillCodes = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				string strVals = "";
				string strCols = "";
				string strOpt = "";
				clsSave.OpenRecordset("select * from custombillcodes where ID = 92", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					// add these codes
					strOpt = "SET IDENTITY_INSERT CustomBillCodes ON;";
					// kk01282015 trout-135  Need to set IDENTITY_INSERT to specify the ID in Insert
					strCols = "Insert into CustomBillCodes (" + "ID,CategoryNo,OrderNo,Description,DefaultWidth,DefaultHeight,SpecialCase,TableName,DBName,Category," + "HelpDescription,DefaultAlignment,boolUserDefined,FormatString,ExtraParameters,ToolTipText,ParametersToolTip" + ") values (";
					// code 92, Water Detail Description
					strVals = "92,4,14,'Detail Description',1,.6668,1,'Bill','TWUT0000.vb1','Water','Description part of the detail section',0,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 93, Water Detail Amount
					strVals = "93,4,15,'Detail Amount',1,.6668,1,'Bill','TWUT0000.vb1','Water','Amount part of the detail section',1,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 94, Sewer Detail Description
					strVals = "94,5,12,'Detail Description',1,.6668,1,'Bill','TWUT0000.vb1','Sewer','Description part of the detail section',0,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// Code 95, Sewer Detail Amount
					strVals = "95,5,13,'Detail Amount',1,.6668,1,'Bill','TWUT0000.vb1','Sewer','Amount part of the detail section',1,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// Code 96, Combined Detail Description
					strVals = "96,6,8,'Detail Description',1,.6668,1,'Bill','TWUT0000.vb1','Combined','Description part of the detail section',0,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// Code 97, Combined Detail Amount
					strVals = "97,6,9,'Detail Amount',1,.6668,1,'Bill','TWUT0000.vb1','Combined','Amount part of the detail section',1,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// Code 98, Water/Sewer/Combined Label
					strVals = "98,6,10,'Water/Sewer Label',1,.1667,1,'Bill','TWUT0000.vb1','Combined','Water, Sewer or Combined label',0,0,'','Water;Sewer;Combined','Specify the labels contents for water, sewer and combined','Specify the labels contents for water, sewer and combined');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 99, Water/Sewer Category
					strVals = "99,2,32,'Category',1,.1667,1,'Bill','TWUT0000.vb1','Account Information','Water category if applicable, sewer if otherwise',0,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 100
					strVals = "100,2,33,'Category FCConvert.ToInt16(',1,.1667,1,'Bill','TWUT0000.vb1','Account Information','Water category if applicable, sewer if otherwise',0,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 101 Messages
					strVals = "101,1,8,'Bill Message',1,.1667,1,'Bill','TWUT0000.vb1','Billing','Add a predefined message',0,0,'','Message #','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 102 Return Address
					strVals = "102,1,9,'Return Address',1,.6668,1,'Bill','TWUT0000.vb1','Billing','Return Address',0,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// CODE 103 Water + Sewer Adjustment
					strVals = "103,6,11,'Adjustment',1,.1667,1,'Bill','TWUT0000.vb1','Combined','Adjustment amount',1,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 104 water adjustment
					strVals = "104,4,16,'Adjustment',1,.1667,1,'Bill','TWUT0000.vb1','Water','Adjustment amount',1,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 105 sewer adjustment
					strVals = "105,5,14,'Adjustment',1,.1667,1,'Bill','TWUT0000.vb1','Sewer','Adjustment amount',1,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 106
					strVals = "106,3,20,'Meter Detail Code',1,.6668,1,'Bill','TWUT0000.vb1','Meter','Code (WA,SW,TX etc.)',0,0,'','Tax,Do not Separate|Separate Tax','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 107
					strVals = "107,3,21,'Meter Detail Amount',1,.6668,1,'Bill','TWUT0000.vb1','Meter','Amount (Regular,Tax,Past Due, etc.)',1,0,'','Tax,Do not Separate|Separate Tax','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					// code 108
					strVals = "108,1,10,'Tax Rate',1,.1667,1,'Bill','TWUT0000.vb1','Billing','Tax Rate',1,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				// change these codes
				clsSave.Execute("update custombillcodes set specialcase = 1,extraparameters = 'Visible,Always|Water|Sewer' where ID = 22 or ID = 23 or ID = 31 or ID = 32 or ID = 49", modExtraModules.strUTDatabase);
				clsSave.Execute("update custombillcodes set extraparameters = 'Format,MM/dd/yy|MM/dd/yyyy' where ID = 1 or ID = 3", modExtraModules.strUTDatabase);
				clsSave.Execute("UPDATE custombillcodes set extraparameters = 'Tax,Do not Separate|Separate Tax;Include,Combined|Water|Sewer' where ID = 106 or ID = 107", modExtraModules.strUTDatabase);
				return AddNewCustomBillCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In AddNewCustomBillCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddNewCustomBillCodes;
		}

		public static bool AddBillMessageCode()
		{
			bool AddBillMessageCode = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				string strVals = "";
				string strCols = "";
				string strOpt = "";
				clsSave.OpenRecordset("select * from custombillcodes where ID = 110", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strOpt = "SET IDENTITY_INSERT CustomBillCodes ON;";
					// kk01282015 trout-135  Need to set IDENTITY_INSERT to specify the ID in Insert
					strCols = "Insert into CustomBillCodes (" + "ID,CategoryNo,OrderNo,Description,DefaultWidth,DefaultHeight,SpecialCase,FieldName,TableName,DBName,Category,HelpDescription,DefaultAlignment,boolUserDefined,FormatString,ExtraParameters,ToolTipText,ParametersToolTip" + ") values (";
					strVals = "110,2,34,'Account Message',2,.1667,0,'BillMessage','Bill','TWUT0000.vb1','Account Information','Bill message specific to this account',0,0,'','','','');";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 9 and specialcase = 1", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					clsSave.Execute("update custombillcodes set specialcase = 1,extraparameters = 'Adjustments,Include|Do not Include' where ID = 9 ", modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 16 and specialcase = 1", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					clsSave.Execute("update custombillcodes set specialcase = 1,extraparameters = 'Adjustments,Include|Do not Include' where ID = 16 ", modExtraModules.strUTDatabase);
				}
				return AddBillMessageCode;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In AddBillMessageCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddBillMessageCode;
		}

		public static bool AddLatestCodes()
		{
			bool AddLatestCodes = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				string strVals = "";
				string strCols;
				string strOpt;
				strOpt = "SET IDENTITY_INSERT CustomBillCodes ON;";
				// kk01282015 trout-135  Need to set IDENTITY_INSERT to specify the ID in Insert
				strCols = "Insert into CustomBillCodes (" + "ID,CategoryNo,OrderNo,Description,DefaultWidth,DefaultHeight,SpecialCase,FieldName,TableName,DBName,Category," + "HelpDescription,DefaultAlignment,boolUserDefined,FormatString,ExtraParameters,ToolTipText,ParametersToolTip" + ") values (";
				clsSave.OpenRecordset("Select * from custombillcodes where ID = 111", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "111,5,34,'S Past Due (w lien)',1,.1667,1,'','Bill','TWUT0000.vb1','Sewer','Past due including liened amounts',1,0,'','Interest,Include|No Interest','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 112", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "112,3,23,'# Days',1,.1667,1,'','Bill','TWUT0000.vb1','Meter','Number of days in billing period',1,0,'','Meter #','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 113", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "113,3,24,'Average Consumption',1,.1667,1,'','Bill','TWUT0000.vb1','Meter','Average Consumption this billing period',1,0,'','Include,Combined|Water|Sewer','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 114", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "114,3,25,'Frequency',1,.1667,1,'','Bill','TWUT0000.vb1','Meter','Billing frequency for the specified meter',0,0,'','Meter #;Description,Long|Short','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				// trout-684  Add special custom bill code for Jay - one time option
				if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY")
				{
					clsSave.OpenRecordset("select * from custombillcodes where ID = 115", modExtraModules.strUTDatabase);
					if (clsSave.EndOfFile())
					{
						strVals = "115,1,8,'Actual Consumption',2,.1667,1,'','Bill','TWUT0000.vb1','Billing','Actual Consumption Amount',0,0,'','','',''" + ");";
						clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
					}
				}
				// trout-412  Add Bill Number to the custom bill code table
				clsSave.OpenRecordset("select * from custombillcodes where ID = 116", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "116,1,11,'Bill Number',2,.1667,0,'BillNumber','Bill','TWUT0000.vb1','Billing','Bill Number',0,0,'','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				// kgk 10-06-11  Add Invoice Cloud account number and invoice number
				clsSave.OpenRecordset("select * from custombillcodes where ID = 117", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "117,1,12,'InvCld Acct No',1,.1667,1,'','Bill','TWUT0000.vb1','Billing','Invoice Cloud Account Number',0,0,'','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 118", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "118,1,13,'InvCld Inv No',1,.1667,1,'','Bill','TWUT0000.vb1','Billing','Invoice Cloud Invoice Number',0,0,'','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				// kgk 10-18-11  trout-765 Add meter changeout indicator
				clsSave.OpenRecordset("select * from custombillcodes where ID = 119", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "119,3,26,'Meter Change Out',1,.1667,1,'','Bill','TWUT0000.vb1','Meter','Code to indicate meter was changed out',1,0,'','Meter #;Text','','Ex: Enter 0 for all meters,1 for first meter or 2,3,4 for multiples, or 4-9 to combine ranges;Text to display for changed out meter'" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				// kgk 06-26-12  trout-839 Add sewer and water consumption, units and flat totals
				clsSave.OpenRecordset("select * from custombillcodes where ID = 120", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "120,5,15,'S Consumption Amount',1,.1667,0,'SConsumptionAmount','Bill','TWUT0000.vb1','Sewer','Total sewer consumption amount this billing period',1,0,'0.00','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 121", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "121,5,16,'S Units Amount',1,.1667,0,'SUnitsAmount','Bill','TWUT0000.vb1','Sewer','Total sewer units amount this billing period',1,0,'0.00','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 122", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "122,5,17,'S Flat Amount',1,.1667,0,'SFlatAmount','Bill','TWUT0000.vb1','Sewer','Total sewer flat amount this billing period',1,0,'0.00','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 123", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "123,4,17,'W Consumption Amount',1,.1667,0,'WConsumptionAmount','Bill','TWUT0000.vb1','Water','Total water consumption amount this billing period',1,0,'0.00','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 124", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "124,4,18,'W Units Amount',1,.1667,0,'WUnitsAmount','Bill','TWUT0000.vb1','Water','Total water units amount this billing period',1,0,'0.00','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 125", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "125,4,19,'W Flat Amount',1,.1667,0,'WFlatAmount','Bill','TWUT0000.vb1','Water','Total water flat amount this billing period',1,0,'0.00','','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				clsSave.OpenRecordset("select * from custombillcodes where ID = 126", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					strVals = "126,2,35,'Billing Name 1 (Trunc)',1,.1667,1,'BName','Bill','TWUT0000.vb1','Account Information','First Billing Name Truncated',0,0,'','Visible,Always|Water|Sewer;Length','',''" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				return AddLatestCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In AddLatestCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddLatestCodes;
		}

		public static void UpdateInterestCode()
		{
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new clsDRWrapper();
				rsSave.OpenRecordset("select * from custombillcodes where ID = 73", modExtraModules.strUTDatabase);
				if (!FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("specialcase")))
				{
					rsSave.Edit();
					rsSave.Set_Fields("Description", "Interest");
					rsSave.Set_Fields("helpdescription", "Total Interest");
					rsSave.Set_Fields("specialcase", true);
					rsSave.Update();
					rsSave.Execute("update custombillcodes set helpdescription = 'Water past due not including interest' where ID = 76", modExtraModules.strUTDatabase);
				}
				rsSave.OpenRecordset("select * from custombillcodes where ID = 74", modExtraModules.strUTDatabase);
				if (!FCConvert.ToBoolean(rsSave.Get_Fields_Boolean("specialcase")))
				{
					rsSave.Edit();
					rsSave.Set_Fields("Description", "Interest");
					rsSave.Set_Fields("HelpDescription", "Total Interest");
					rsSave.Set_Fields("Specialcase", true);
					rsSave.Update();
					rsSave.Execute("update custombillcodes set helpdescription = 'Sewer past due not including interest' where ID = 77", modExtraModules.strUTDatabase);
					rsSave.OpenRecordset("select * from custombillcodes where ID = 75", modExtraModules.strUTDatabase);
					rsSave.Edit();
					rsSave.Set_Fields("Description", "Interest");
					rsSave.Set_Fields("Helpdescription", "Total Interest");
					rsSave.Set_Fields("Specialcase", true);
					rsSave.Update();
					rsSave.Execute("update custombillcodes set helpdescription = 'Total past due not including interest' where ID = 78", modExtraModules.strUTDatabase);
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In AddLatestCode", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static bool MakeUpdatesToDatabase_2(bool boolOverride)
		{
			return MakeUpdatesToDatabase(ref boolOverride);
		}

		public static bool MakeUpdatesToDatabase(ref bool boolOverride)
		{
			bool MakeUpdatesToDatabase = false;
			// boolOverride is when a user selects Check Database Structure so that all updates will be checked
			bool boolReturn;
			try
			{
				// On Error GoTo ErrorHandler
				MakeUpdatesToDatabase = false;
				boolReturn = true;
				if (boolOverride)
				{
					AddNewCustomBillCodes();
					boolReturn = boolReturn && UpdateCustomBillFields();
					boolReturn = boolReturn && AddNewCustomBillFields();
					boolReturn = boolReturn && AddBillMessageCode();
					// 12/19/2005
					boolReturn = boolReturn && UpdateBillingAddressCodes();
					// 08/29/2006
					UpdateInterestCode();
				}
				// 10/23/2008
				boolReturn = boolReturn && AddLatestCodes();
				MakeUpdatesToDatabase = boolReturn;
				return MakeUpdatesToDatabase;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MakeUpdatesToDatabase", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return MakeUpdatesToDatabase;
		}

		private static bool UpdateCustomBillFields()
		{
			bool UpdateCustomBillFields = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				string strSQL;
				bool boolReturn;
				UpdateCustomBillFields = false;
				boolReturn = true;
				strSQL = "Update custombillcodes set specialcase = 1,extraparameters = 'Visible,Always|Water|Sewer' where ID = 6";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				strSQL = "update custombillcodes set extraparameters = 'Format,MM/dd/yy|MM/dd/yyyy;Visible,Always|Water|Sewer' where ID = 72";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				// kk05192014 trout-1061    strSQL = "update custombillcodes set extraparameters = 'Meter #;Show Units,No|Yes' where ID = 5"
				strSQL = "update custombillcodes set extraparameters = 'Meter #;Show Units,No|Yes;Show Value,Actual|Billed' where ID  = 5";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				strSQL = "update custombillcodes set extraparameters = 'Adjustments,Do Not separate|Show Separate' where ID between 93 and 97 and not (ID = 94 or ID = 96)";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				strSQL = "update custombillcodes set extraparameters = 'Adjustments,Do Not separate|Show Separate;Overrides,Do Not Label|Label Overrides' where ID = 92 or ID = 94 or ID = 96";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				// kgk 10-20-2011 trout-766  Add option to itemize adjustments
				strSQL = "update custombillcodes set extraparameters = 'Adjustments,Do Not separate|Show Separate|Itemize;Overrides,Do Not Label|Label Overrides' where ID = 92 or ID = 94 or ID = 96";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				strSQL = "update custombillcodes set extraparameters = 'Adjustments,Do Not separate|Show Separate|Itemize' where ID = 93 or ID = 95 or ID = 97";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				// kgk 06-26-2012 trout-839  Add option to breakdown regular into consumption, units and flat amounts
				// Added to Separate Water and Sewer details only, not the combined
				strSQL = "update custombillcodes set extraparameters = 'Adjustments,Do Not separate|Show Separate|Itemize;Overrides,Do Not Label|Label Overrides;Breakdown Regular,No|Yes' where ID = 92 or ID = 94";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				strSQL = "update custombillcodes set extraparameters = 'Adjustments,Do Not separate|Show Separate|Itemize;Breakdown Regular,No|Yes' where ID = 93 or ID = 95";
				boolReturn = boolReturn && clsSave.Execute(strSQL, modExtraModules.strUTDatabase);
				UpdateCustomBillFields = boolReturn;
				return UpdateCustomBillFields;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In UpdateCustomBillFields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateCustomBillFields;
		}

		private static bool AddNewCustomBillFields()
		{
			bool AddNewCustomBillFields = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				string strVals = "";
				string strCols = "";
				string strOpt = "";
				bool boolReturn;
				AddNewCustomBillFields = false;
				boolReturn = true;
				clsSave.OpenRecordset("select * from custombillcodes where ID = 109", modExtraModules.strUTDatabase);
				if (clsSave.EndOfFile())
				{
					// add these codes
					// code 109 Units
					strOpt = "SET IDENTITY_INSERT CustomBillCodes ON;";
					// kk01282015 trout-135  Need to set IDENTITY_INSERT to specify the ID in Insert
					strCols = "Insert into CustomBillCodes (" + "ID,CategoryNo,OrderNo,Description,DefaultWidth,DefaultHeight,SpecialCase,TableName,DBName,Category," + "HelpDescription,DefaultAlignment,boolUserDefined,FormatString,ExtraParameters,ToolTipText,ParametersToolTip" + ") values (";
					strVals = "109,3,22,'Units',1,.1667,1,'Bill','TWUT0000.vb1','Meter','Number of units for the specified meter',0,0,'','Meter #;Include Label,Yes|No','','Ex: Enter 0 for all meters,1 for first meter or 2,3,4 for multiples, or 4-9 to combine ranges;Yes to print Units = before the units, No to just print the units'" + ");";
					clsSave.Execute(strOpt + strCols + strVals, modExtraModules.strUTDatabase);
				}
				AddNewCustomBillFields = boolReturn;
				return AddNewCustomBillFields;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In AddNewCustomBillFields", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return AddNewCustomBillFields;
		}

		private static bool UpdateBillingAddressCodes()
		{
			bool UpdateBillingAddressCodes = false;
			try
			{
				// On Error GoTo ErrorHandler
				clsDRWrapper clsSave = new clsDRWrapper();
				string strSQL = "";
				
				clsSave.OpenRecordset("select * from custombillcodes where ID = 33", modExtraModules.strUTDatabase);
				if (!FCConvert.ToBoolean(clsSave.Get_Fields_Boolean("specialcase")))
				{
					clsSave.Execute("UPDATE CUSTOMbillcodes set specialcase = 1 where ID between 33 and 39", modExtraModules.strUTDatabase);
				}
				UpdateBillingAddressCodes = true;
				return UpdateBillingAddressCodes;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description + "\r\n" + "In UpdateBillingAddressCodes", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return UpdateBillingAddressCodes;
		}

		private static void GetAdjBreakdown_6(int lngBillKey, string strService, double dblAdjAmt)
		{
			GetAdjBreakdown(lngBillKey, ref strService, dblAdjAmt);
		}

		private static void GetAdjBreakdown(int lngBillKey, ref string strService, double dblAdjAmt)
		{
			// kgk 10-19-2011 trout-766  Add adj detail for Chapt 660 compliance
			clsDRWrapper rsAdj = new clsDRWrapper();
			int x = 0;
			double dblItemizedTotal;
			rsAdj.OpenRecordset("SELECT Breakdown.Amount as AdjAmount, Adjust.* FROM Breakdown INNER JOIN Adjust ON Adjust.Code = Breakdown.AdjCode WHERE Breakdown.BillKey = " + FCConvert.ToString(lngBillKey) + " AND Breakdown.Service = '" + strService + "'", modExtraModules.strUTDatabase);
			dblItemizedTotal = 0;
			if (rsAdj.RecordCount() > 0)
			{
				for (x = 1; x <= rsAdj.RecordCount(); x++)
				{
					if (strService == "W")
					{
						if (x <= Information.UBound(Statics.AdjBreakdownWater, 1))
						{
							// What do we do if there are more then 6 adjustments
							Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsAdj.Get_Fields_String("LongDescription"));
							// TODO Get_Fields: Field [AdjAmount] not found!! (maybe it is an alias?)
							Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(rsAdj.Get_Fields("AdjAmount"));
						}
					}
					else if (strService == "S")
					{
						if (x <= Information.UBound(Statics.AdjBreakdownSewer, 1))
						{
							// What do we do if there are more then 6 adjustments
							Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsAdj.Get_Fields_String("LongDescription"));
							// TODO Get_Fields: Field [AdjAmount] not found!! (maybe it is an alias?)
							Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(rsAdj.Get_Fields("AdjAmount"));
						}
					}
					// TODO Get_Fields: Field [AdjAmount] not found!! (maybe it is an alias?)
					dblItemizedTotal += rsAdj.Get_Fields("AdjAmount");
					rsAdj.MoveNext();
				}
				// x
			}
			// Check for DE Adjustment
			// cjg 07-02-2012.  Misc charges apparently aren't in the adj amount so make sure we don't make a negative adjustment
			// if DE adjustments can be negative then something else will have to be done to make the breakdown work
			if (dblItemizedTotal != dblAdjAmt)
			{
				// And dblAdjAmt > dblItemizedTotal Then
				if (strService == "W")
				{
					if (x <= Information.UBound(Statics.AdjBreakdownWater, 1))
					{
						// What do we do if there are more then 6 adjustments
						Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNDESCRIPTION] = "Adj";
						Statics.AdjBreakdownWater[x, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblAdjAmt - dblItemizedTotal);
					}
				}
				else if (strService == "S")
				{
					if (x <= Information.UBound(Statics.AdjBreakdownSewer, 1))
					{
						// What do we do if there are more then 6 adjustments
						Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNDESCRIPTION] = "Adj";
						Statics.AdjBreakdownSewer[x, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblAdjAmt - dblItemizedTotal);
					}
				}
			}
		}

		private static void GetBillBreakDown_6(int lngBillKey, string strService)
		{
			GetBillBreakDown(ref lngBillKey, ref strService);
		}

		private static void GetBillBreakDown(ref int lngBillKey, ref string strService)
		{
			// kgk 06-25-2012 trout-839  Breakdown bill amount into Consumption, Unit and Flat amounts
			clsDRWrapper rsBrk = new clsDRWrapper();
			int x;
			// vbPorter upgrade warning: dblTotal As double	OnRead(string)
			double dblTotal = 0;
			string strFields = "";

			if (strService == "W")
			{
				rsBrk.OpenRecordset("SELECT Description, Amount FROM Breakdown WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Type = 'C' AND Service = 'W'");
				if (rsBrk.RecordCount() == 1)
				{
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWCONS, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsBrk.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWCONS, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(rsBrk.Get_Fields("Amount"));
				}
				else
				{
					dblTotal = 0;
					while (!rsBrk.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						dblTotal += Conversion.Val(rsBrk.Get_Fields("Amount"));
						rsBrk.MoveNext();
					}
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWCONS, CNSTDETAILBREAKDOWNDESCRIPTION] = "CONS";
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWCONS, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblTotal);
				}
				rsBrk.OpenRecordset("SELECT Description, Amount FROM Breakdown WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Type = 'U' AND Service = 'W'");
				if (rsBrk.RecordCount() == 1)
				{
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWUNIT, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsBrk.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWUNIT, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(rsBrk.Get_Fields("Amount"));
				}
				else
				{
					dblTotal = 0;
					while (!rsBrk.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						dblTotal += Conversion.Val(rsBrk.Get_Fields("Amount"));
						rsBrk.MoveNext();
					}
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWUNIT, CNSTDETAILBREAKDOWNDESCRIPTION] = "UNIT";
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWUNIT, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblTotal);
				}
				rsBrk.OpenRecordset("SELECT Description, Amount FROM Breakdown WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Type = 'F' AND Service = 'W'");
				if (rsBrk.RecordCount() == 1)
				{
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWFLAT, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsBrk.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWFLAT, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(rsBrk.Get_Fields("Amount"));
				}
				else
				{
					dblTotal = 0;
					while (!rsBrk.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						dblTotal += Conversion.Val(rsBrk.Get_Fields("Amount"));
						rsBrk.MoveNext();
					}
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWFLAT, CNSTDETAILBREAKDOWNDESCRIPTION] = "FLAT";
					Statics.DetailBreakdownWaterCUF[CNSTDETAILBREAKDOWNROWFLAT, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblTotal);
				}
			}
			if (strService == "S")
			{
				rsBrk.OpenRecordset("SELECT Description, Amount FROM Breakdown WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Type = 'C' AND Service = 'S'");
				if (rsBrk.RecordCount() == 1)
				{
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWCONS, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsBrk.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWCONS, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(rsBrk.Get_Fields("Amount"));
				}
				else
				{
					dblTotal = 0;
					while (!rsBrk.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						dblTotal += Conversion.Val(rsBrk.Get_Fields("Amount"));
						rsBrk.MoveNext();
					}
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWCONS, CNSTDETAILBREAKDOWNDESCRIPTION] = "CONS";
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWCONS, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblTotal);
				}
				rsBrk.OpenRecordset("SELECT Description, Amount FROM Breakdown WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Type = 'U' AND Service = 'S'");
				if (rsBrk.RecordCount() == 1)
				{
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWUNIT, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsBrk.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWUNIT, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(rsBrk.Get_Fields("Amount"));
				}
				else
				{
					dblTotal = 0;
					while (!rsBrk.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						dblTotal += Conversion.Val(rsBrk.Get_Fields("Amount"));
						rsBrk.MoveNext();
					}
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWUNIT, CNSTDETAILBREAKDOWNDESCRIPTION] = "UNIT";
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWUNIT, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblTotal);
				}
				rsBrk.OpenRecordset("SELECT Description, Amount FROM Breakdown WHERE BillKey = " + FCConvert.ToString(lngBillKey) + " AND Type = 'F' AND Service = 'S'");
				if (rsBrk.RecordCount() == 1)
				{
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWFLAT, CNSTDETAILBREAKDOWNDESCRIPTION] = FCConvert.ToString(rsBrk.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWFLAT, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(rsBrk.Get_Fields("Amount"));
				}
				else
				{
					dblTotal = 0;
					while (!rsBrk.EndOfFile())
					{
						// TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
						dblTotal += Conversion.Val(rsBrk.Get_Fields("Amount"));
						rsBrk.MoveNext();
					}
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWFLAT, CNSTDETAILBREAKDOWNDESCRIPTION] = "FLAT";
					Statics.DetailBreakdownSewerCUF[CNSTDETAILBREAKDOWNROWFLAT, CNSTDETAILBREAKDOWNAMOUNT] = FCConvert.ToString(dblTotal);
				}
			}
			// Check for DE Adjustment
			// If dblItemizedTotal <> dblAdjAmt Then
			// If strService = "W" Then
			// If x <= UBound(AdjBreakdownWater) Then     ' What do we do if there are more then 6 adjustments
			// AdjBreakdownWater(x, CNSTDETAILBREAKDOWNDESCRIPTION) = "Adj"
			// AdjBreakdownWater(x, CNSTDETAILBREAKDOWNAMOUNT) = dblAdjAmt - dblItemizedTotal
			// End If
			// ElseIf strService = "S" Then
			// If x <= UBound(AdjBreakdownSewer) Then     ' What do we do if there are more then 6 adjustments
			// AdjBreakdownSewer(x, CNSTDETAILBREAKDOWNDESCRIPTION) = "Adj"
			// AdjBreakdownSewer(x, CNSTDETAILBREAKDOWNAMOUNT) = dblAdjAmt - dblItemizedTotal
			// End If
			// End If
			// End If
		}

		public class StaticVariables
		{
			// it would waste a lot of processor time
			// 3 times.  If we had to call once for consumption, once for service days and once for average
			// The following are to make sure we don't have to call certain functions
			public int lngThisQuarterConsumption;
			public int lngLastQuarterConsumption;
			public int lngLastYearConsumption;
			public int lngThisQuarterServiceDays;
			public int lngLastQuarterServiceDays;
			public int lngLastYearServiceDays;
			public double dblThisQuarterAverageConsumption;
			public double dblLastQuarterAverageConsumption;
			public double dblLastYearAverageConsumption;
			public int lngThisQuarterAccount;
			public int lngLastQuarterAccount;
			public int lngLastYearAccount;
			public int lngNumMetersLoaded;
			public int lngMetersLoadedBillKey;
			public bool boolUseSewerBills;
			public bool boolUseWaterBills;
			public double dblTaxRateToShow;
			public double dblReadingUnitsOnBill;
			// vbPorter upgrade warning: dblRRatio As double	OnWrite(short, double, string)
			public double dblRRatio;
			public MeterInfoType[] aryMeterBreakdown = null;
			// vbPorter upgrade warning: DetailBreakDownWater As string	OnWrite(int, string, double)
			public string[,] DetailBreakDownWater = new string[8 + 1, 3 + 1];
			// vbPorter upgrade warning: DetailBreakdownSewer As string	OnWrite(int, string, double)
			public string[,] DetailBreakdownSewer = new string[8 + 1, 3 + 1];
			////public string[,] DetailBreakdownCombined = new string[8 + 1, 3 + 1];
			// vbPorter upgrade warning: DetailBreakdownWaterCUF As string	OnWrite(string, double)
			public string[,] DetailBreakdownWaterCUF = new string[3 + 1, 3 + 1];
			// vbPorter upgrade warning: DetailBreakdownSewerCUF As string	OnWrite(string, double)
			public string[,] DetailBreakdownSewerCUF = new string[3 + 1, 3 + 1];
			// vbPorter upgrade warning: AdjBreakdownWater As string	OnWrite(int, string, double)
			public string[,] AdjBreakdownWater = new string[6 + 1, 3 + 1];
			// vbPorter upgrade warning: AdjBreakdownSewer As string	OnWrite(int, string, double)
			public string[,] AdjBreakdownSewer = new string[6 + 1, 3 + 1];
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
