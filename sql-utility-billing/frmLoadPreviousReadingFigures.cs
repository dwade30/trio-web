﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmLoadPreviousReadingFigures.
	/// </summary>
	public partial class frmLoadPreviousReadingFigures : BaseForm
	{
		public frmLoadPreviousReadingFigures()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmLoadPreviousReadingFigures InstancePtr
		{
			get
			{
				return (frmLoadPreviousReadingFigures)Sys.GetInstance(typeof(frmLoadPreviousReadingFigures));
			}
		}

		protected frmLoadPreviousReadingFigures _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By         David Wade
		// Date               7/21/2004
		// MODIFIED BY    :   Jim Bertolino
		// DATE           :   03/17/2006
		// This form will be used to load previous figures into the
		// meter table
		// ********************************************************
		public string strSQL = "";
		clsDRWrapper rsInfo = new clsDRWrapper();
		int KeyCol;
		int BookCol;
		int SequenceCol;
		int NameCol;
		int LocationCol;
		int AccountNumberCol;
		int ReadingCol;

		private void frmLoadPreviousReadingFigures_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmLoadPreviousReadingFigures_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmLoadPreviousReadingFigures properties;
			//frmLoadPreviousReadingFigures.FillStyle	= 0;
			//frmLoadPreviousReadingFigures.ScaleWidth	= 9045;
			//frmLoadPreviousReadingFigures.ScaleHeight	= 7380;
			//frmLoadPreviousReadingFigures.LinkTopic	= "Form2";
			//frmLoadPreviousReadingFigures.LockControls	= true;
			//frmLoadPreviousReadingFigures.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			KeyCol = 0;
			AccountNumberCol = 1;
			NameCol = 2;
			LocationCol = 3;
			BookCol = 4;
			SequenceCol = 5;
			ReadingCol = 6;
			vsMeters.ColHidden(KeyCol, true);
			vsMeters.TextMatrix(0, AccountNumberCol, "Acct #");
			vsMeters.TextMatrix(0, NameCol, "Name");
			vsMeters.TextMatrix(0, LocationCol, "Location");
			vsMeters.TextMatrix(0, BookCol, "Book #");
			vsMeters.TextMatrix(0, SequenceCol, "Sequence #");
			vsMeters.TextMatrix(0, ReadingCol, "Reading");
			vsMeters.ColAlignment(AccountNumberCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			vsMeters.ColAlignment(BookCol, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			//FC:FINAL:CHN - issue #1048: incorrect ordering by key
			vsMeters.ColDataType(1, FCGrid.DataTypeSettings.flexDTLong);
			rsInfo.OpenRecordset("SELECT MeterTable.ID AS MeterKey, BookNumber, Sequence, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSQL + " ORDER BY BookNumber, Sequence, AccountNumber");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				LoadGrid();
				vsMeters.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 1, 0, vsMeters.Rows - 1, vsMeters.Cols - 2, 0xE0E0E0);
				vsMeters.Row = 1;
			}
			else
			{
				MessageBox.Show("No Info Found", "No Info", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmLoadPreviousReadingFigures_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmLoadPreviousReadingFigures_Resize(object sender, System.EventArgs e)
		{
			int lngWid = 0;
			lngWid = vsMeters.WidthOriginal;
			vsMeters.ColWidth(AccountNumberCol, FCConvert.ToInt32(lngWid * 0.08));
			vsMeters.ColWidth(NameCol, FCConvert.ToInt32(lngWid * 0.29));
			vsMeters.ColWidth(LocationCol, FCConvert.ToInt32(lngWid * 0.29));
			vsMeters.ColWidth(BookCol, FCConvert.ToInt32(lngWid * 0.08));
			vsMeters.ColWidth(SequenceCol, FCConvert.ToInt32(lngWid * 0.14));
			vsMeters.ColWidth(ReadingCol, FCConvert.ToInt32(lngWid * 0.1));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void LoadGrid()
		{
			do
			{
				vsMeters.AddItem("");
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				vsMeters.TextMatrix(vsMeters.Rows - 1, AccountNumberCol, Strings.Format(rsInfo.Get_Fields("AccountNumber"), "0000"));
				vsMeters.TextMatrix(vsMeters.Rows - 1, NameCol, FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				vsMeters.TextMatrix(vsMeters.Rows - 1, LocationCol, rsInfo.Get_Fields("StreetNumber") + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt"))) + " " + rsInfo.Get_Fields_String("StreetName"));
				// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
				vsMeters.TextMatrix(vsMeters.Rows - 1, SequenceCol, FCConvert.ToString(rsInfo.Get_Fields("Sequence")));
				vsMeters.TextMatrix(vsMeters.Rows - 1, BookCol, Strings.Format(rsInfo.Get_Fields_Int32("BookNumber"), "0000"));
				vsMeters.TextMatrix(vsMeters.Rows - 1, KeyCol, FCConvert.ToString(rsInfo.Get_Fields_Int32("MeterKey")));
				vsMeters.TextMatrix(vsMeters.Rows - 1, ReadingCol, FCConvert.ToString(0));
				rsInfo.MoveNext();
			}
			while (rsInfo.EndOfFile() != true);
			//FC:FINAL:CHN - issue #1050: allow to enter numbers only
			vsMeters.EditingControlShowing -= VsMeter_EditingControlShowing;
			vsMeters.EditingControlShowing += VsMeter_EditingControlShowing;
		}
		//FC:FINAL:CHN - issue #1050: allow to enter numbers only
		private void VsMeter_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
		{
			if (e.Control != null)
			{
				if (vsMeters.Col == 6)
				{
					var box = e.Control as TextBox;
					if (box != null)
					{
                        box.AllowOnlyNumericInput();
                        box.RemoveAlphaCharactersOnValueChange();
					}
				}
			}
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsMeter = new clsDRWrapper();
			int counter;
			vsMeters.Col = AccountNumberCol;
			for (counter = 1; counter <= vsMeters.Rows - 1; counter++)
			{
				if (Conversion.Val(vsMeters.TextMatrix(counter, ReadingCol)) != 0)
				{
					rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + vsMeters.TextMatrix(counter, KeyCol));
					if (rsMeter.EndOfFile() != true && rsMeter.BeginningOfFile() != true)
					{
						rsMeter.Edit();
						rsMeter.Set_Fields("CurrentReading", FCConvert.ToString(Conversion.Val(vsMeters.TextMatrix(counter, ReadingCol))));
						rsMeter.Set_Fields("CurrentReadingDate", DateTime.Today);
						rsMeter.Set_Fields("CurrentCode", "A");
						rsMeter.Update();
					}
				}
			}
			MessageBox.Show("Readings saved successfully!", "Save Successful", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //FC:FINAL:BSE #3731 form should not close after saving
			//this.Unload();
		}

		private void vsMeters_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsMeters.Col != ReadingCol)
			{
				vsMeters.Col = ReadingCol;
			}
			vsMeters.EditMaxLength = 7;
			vsMeters.EditCell();
		}

		private void vsMeters_Enter(object sender, System.EventArgs e)
		{
			if (vsMeters.Col != ReadingCol)
			{
				vsMeters.Col = ReadingCol;
			}
			vsMeters.EditMaxLength = 7;
			vsMeters.EditCell();
		}

		private void vsMeters_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Return)
			{
				Support.SendKeys("{tab}", false);
			}
			else if ((KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9) || (KeyAscii == (Keys)8))
			{
				// do nothing
			}
			else
			{
				KeyAscii = 0;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void vsMeters_RowColChange(object sender, System.EventArgs e)
		{
			if (vsMeters.Col != ReadingCol)
			{
				vsMeters.Col = ReadingCol;
			}
			vsMeters.EditMaxLength = 7;
			vsMeters.EditCell();
		}

		private void vsMeters_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			if (vsMeters.GetFlexColIndex(e.ColumnIndex) == ReadingCol)
			{
				if (!Information.IsNumeric(vsMeters.EditText))
				{
					//FC:FINAL:DDU:#1051 - when non numeric is added edit text to 0
					vsMeters.EditText = "0";
					MessageBox.Show("You may only enter a numeric value in this field.", "Invalid Reading", MessageBoxButtons.OK, MessageBoxIcon.Information);
					e.Cancel = true;
				}
			}
		}
	}
}
