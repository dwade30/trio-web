//Fecher vbPorter - Version 1.0.0.90

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class clsACHSetup
	{

        public string ImmediateDestinationName { get; set; } = "";
        public string ImmediateDestinationRT { get; set; } = "";



        public string ImmediateOriginName { get; set; } = "";

        public string ImmediateOriginODFI { get; set; } = "";


        public string ImmediateOriginRT { get; set; } = "";

        public string CompanyName { get; set; } = "";

        public string CompanyRT { get; set; } = "";




        public string CompanyID { get; set; } = "";

        public string CompanyAccount { get; set; } = "";

        public int CompanyAccountType { get; set; } = 0;

	}
}
