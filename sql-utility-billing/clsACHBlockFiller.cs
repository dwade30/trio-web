//Fecher vbPorter - Version 1.0.0.90

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class clsACHBlockFiller
	{

		//=========================================================

		private short intRecordSize;
		private string strFillerChar;

		public clsACHBlockFiller() : base()
		{
			intRecordSize = 94;
			strFillerChar = "9";
		}

		public string FillerChar
		{
			get
			{
					string FillerChar = "";
				FillerChar = strFillerChar;
				return FillerChar;
			}

			set
			{
				strFillerChar = value;
			}
		}



		public short RecordSize
		{
			get
			{
					short RecordSize = 0;
				RecordSize = intRecordSize;
				return RecordSize;
			}

			set
			{
				intRecordSize = value;
			}
		}



		public string OutputLine()
		{
			string OutputLine = "";
			string strLine;
			if (strFillerChar=="") strFillerChar = "9";
			if (strFillerChar.Length>1) {
				strFillerChar = "9";
			}
			strLine = fecherFoundation.Strings.StrDup(intRecordSize, strFillerChar);
			OutputLine = strLine;
			return OutputLine;
		}

	}
}
