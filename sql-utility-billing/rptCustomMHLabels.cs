﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using System.Drawing;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptCustomMHLabels.
	/// </summary>
	public partial class rptCustomMHLabels : BaseSectionReport
	{
		public rptCustomMHLabels()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Custom Labels";
		}

		public static rptCustomMHLabels InstancePtr
		{
			get
			{
				return (rptCustomMHLabels)Sys.GetInstance(typeof(rptCustomMHLabels));
			}
		}

		protected rptCustomMHLabels _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsMort.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptCustomMHLabels	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               08/05/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/05/2004              *
		// ********************************************************
		// THIS REPORT IS TOTOALLY NOT GENERIC AND HAS BEEN ALTERED FOR THIS TWCL
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsMort = new clsDRWrapper();
		// vbPorter upgrade warning: intLabelWidth As short --> As int	OnWrite(int, double)
		float intLabelWidth;
		bool boolDifferentPageSize;
		string strFont;
		bool boolPP;
		bool boolMort;
		bool boolEmpty;
		string strLeftAdjustment = "";
		// vbPorter upgrade warning: lngVertAdjust As int	OnWriteFCConvert.ToDouble(
		int lngVertAdjust;
		int intTypeOfLabel;
		clsPrintLabel labLabels = new clsPrintLabel();
		// vbPorter upgrade warning: lngPrintWidth As int	OnWrite(string, double)
		int lngPrintWidth;
		public int intRateType;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// IF THIS IS THE END OF THE RECORDSET THEN WE NEED A WAY TO GET OUT
			eArgs.EOF = rsData.EndOfFile();
		}
		// vbPorter upgrade warning: intLabelType As short	OnWriteFCConvert.ToInt32(
		public void Init(ref string strSQL, ref string strReportType, int intLabelType, ref string strPrinterName, ref string strFonttoUse)
		{
			string strDBName = "";
			double dblLabelsAdjustment = 0;
			double dblLabelsAdjustmentH = 0;
			string strTemp = "";
			strFont = strFonttoUse;
			
			boolPP = false;
			boolMort = false;
			switch (strReportType)
            {
                case "MORTGAGEHOLDER":
                    strDBName = "CentralData";
                    boolMort = true;

                    break;
                case "RECL":
                case "LABELS":
                    strDBName = modExtraModules.strUTDatabase;

                    break;
            }
			rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
			rsMort.OpenRecordset("SELECT * FROM MortgageHolders", "CentralData");
			intTypeOfLabel = intLabelType;
			// make them choose the printer first if you have to use a custom form
			// RESET THE RECORDSET TO THE BEGINNING OF THE RECORDSET
			if (rsData.RecordCount() != 0)
				rsData.MoveFirst();
			boolDifferentPageSize = false;

			if (intLabelType == 0 || intLabelType == 1)
			{
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMV", ref strTemp);
				dblLabelsAdjustment = Conversion.Val(strTemp);
				modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY + "UT\\", "AdjustmentLabelsDMH", ref strTemp);
				dblLabelsAdjustmentH = Conversion.Val(strTemp);
			}
			else
			{
				dblLabelsAdjustment = modMain.Statics.gdblLabelsAdjustment;
			}
			int intindex;
			intindex = labLabels.Get_IndexFromID(intTypeOfLabel);
			
			if (labLabels.Get_IsDymoLabel(intindex))
			{
				switch (labLabels.Get_ID(intindex))
				{
					case modLabels.CNSTLBLTYPEDYMO30256:
						{
							
							strPrinterName = this.Document.Printer.PrinterName;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(labLabels.Get_TopMargin(intindex) + (dblLabelsAdjustment * 270) / 1440f);
							// 0.128 * 1440
							this.PageSettings.Margins.Bottom = labLabels.Get_BottomMargin(intindex);
							// 0
							this.PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
							// 0
							this.PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
							// 0.128 * 1440
							PrintWidth = labLabels.Get_LabelWidth(intindex) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							// (2.31 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
							intLabelWidth = labLabels.Get_LabelWidth(intindex);
							// (4 * 1440)
							lngPrintWidth = FCConvert.ToInt32(PrintWidth);
							// (4 * 1440) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right
							Detail.Height = labLabels.Get_LabelHeight(intindex) - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
							// (1440 * 2.3125) - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 '2 5/16"
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = labLabels.Get_LabelWidth(intindex);
							// Landscape                    '4 * 1440
							PageSettings.PaperWidth = labLabels.Get_LabelHeight(intindex);
							// 2.31 * 1440
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							break;
						}
					case modLabels.CNSTLBLTYPEDYMO30252:
						{
							// If Not Me.Printer.PrintDialog Then
							// Unload Me
							// Exit Sub
							// End If
							strPrinterName = this.Document.Printer.PrinterName;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							this.PageSettings.Margins.Top = FCConvert.ToSingle(0.128);
							this.PageSettings.Margins.Bottom = 0;
							this.PageSettings.Margins.Right = 0;
							this.PageSettings.Margins.Left = FCConvert.ToSingle(0.128);
							// 
							PrintWidth = (3.5F) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right;
							intLabelWidth = FCConvert.ToInt32((3.5));
							lngPrintWidth = FCConvert.ToInt32((3.5) - this.PageSettings.Margins.Left - this.PageSettings.Margins.Right);
							Detail.Height = (1.1F) - this.PageSettings.Margins.Top - this.PageSettings.Margins.Bottom - 10 / 1440f;
							Detail.ColumnCount = 1;
							PageSettings.PaperHeight = FCConvert.ToSingle(3.5);
							PageSettings.PaperWidth = FCConvert.ToSingle(1.1);
							Detail.NewPage = GrapeCity.ActiveReports.SectionReportModel.NewPage.After;
							this.Document.Printer.DefaultPageSettings.Landscape = true;
							PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
							;
							break;
						}
				}
				//end switch
			}
			else if (labLabels.Get_IsLaserLabel(intindex))
			{
				this.PageSettings.Margins.Top = FCConvert.ToSingle(0.5);
				this.PageSettings.Margins.Top += FCConvert.ToSingle(dblLabelsAdjustment * 270) / 1440f;
				this.PageSettings.Margins.Left = labLabels.Get_LeftMargin(intindex);
				this.PageSettings.Margins.Right = labLabels.Get_RightMargin(intindex);
				PrintWidth = labLabels.Get_PageWidth(intindex) - labLabels.Get_LeftMargin(intindex) - labLabels.Get_RightMargin(intindex);
				intLabelWidth = labLabels.Get_LabelWidth(intindex);
				Detail.Height = labLabels.Get_LabelHeight(intindex) + labLabels.Get_VerticalSpace(intindex);
				if (labLabels.Get_LabelsWide(intindex) > 0)
				{
					Detail.ColumnCount = labLabels.Get_LabelsWide(intindex);
					Detail.ColumnSpacing = labLabels.Get_HorizontalSpace(intindex);
				}
				lngPrintWidth = FCConvert.ToInt32(PrintWidth);
			}
			CreateDataFields();
			frmReportViewer.InstancePtr.Init(this, strPrinterName);
			// kk01132016 trout-1176  Add printer so print dialog is not displayed again
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			if (rsData.RecordCount() > 0)
			{
				//FC:FINAL:DDU:#1134 - fixed intRateType value after frmRateRecChoice is unloadded
				//switch (frmRateRecChoice.InstancePtr.intRateType)
				switch (intRateType)
				{
					case 10:
						{
							modGlobalFunctions.IncrementSavedReports("LastUT30DayLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastUT30DayLabels1.RDF"));
							break;
						}
					case 11:
						{
							modGlobalFunctions.IncrementSavedReports("LastUTTransferToLienLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastUTTransferToLienLabels1.RDF"));
							break;
						}
					case 12:
						{
							modGlobalFunctions.IncrementSavedReports("LastUTLienMaturityLabels");
							this.Document.Save(System.IO.Path.Combine(TWSharedLibrary.Variables.Statics.ReportPath, "LastUTLienMaturityLabels1.RDF"));
							break;
						}
				}
				//end switch
			}
			else
			{
				MessageBox.Show("There are no eligible accounts.", "No Labels", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
            this.Document.Printer.PaperSize = new System.Drawing.Printing.PaperSize("CustomFormat", FCConvert.ToInt32(this.PageSettings.PaperWidth * 100), FCConvert.ToInt32(this.PageSettings.PaperHeight * 100));
		}

		private void CreateDataFields()
		{
			int intControlNumber;
			GrapeCity.ActiveReports.SectionReportModel.TextBox NewField;
			int intRow;
			int intCol;
			int intNumber;
			// CREATE THE CONTROLS AND SET THE POSITION OF THE CONTROLS
			intNumber = 4;
			if (boolMort)
				intNumber = 5;
			for (intRow = 1; intRow <= intNumber; intRow++)
			{
				NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
				NewField.CanGrow = false;
				NewField.Name = "txtData" + FCConvert.ToString(intRow);
				NewField.Top = ((intRow - 1) * 225) / 1440f + lngVertAdjust;
				NewField.Left = 144 / 1440f;
				// one space
				NewField.Width = (FCConvert.ToInt32(PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 145 / 1440f;
				NewField.Height = 225 / 1440f;
				NewField.Text = string.Empty;
				NewField.MultiLine = false;
				NewField.WordWrap = true;
				if (Strings.Trim(strFont) != string.Empty)
				{
					NewField.Font = new Font(strFont, NewField.Font.Size);
				}
				else
				{
					NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
				}
				if (intRow == 1)
				{
					NewField = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
					NewField.CanGrow = false;
					NewField.Name = "txtMort";
					NewField.Top = ((intRow - 1) * 225) / 1440f + lngVertAdjust;
					NewField.Left = (FCConvert.ToInt32(PrintWidth / Detail.ColumnCount) - ((Detail.ColumnCount - 1) * Detail.ColumnSpacing)) - 1145 / 1440f;
					NewField.Width = 999 / 1440f;
					NewField.Height = 225 / 1440f;
					NewField.Text = string.Empty;
					NewField.MultiLine = false;
					NewField.WordWrap = true;
					if (Strings.Trim(strFont) != string.Empty)
					{
						NewField.Font = new Font(strFont, NewField.Font.Size);
					}
					else
					{
						NewField.Font = new Font(lblFont.Font.Name, NewField.Font.Size);
					}
				}
				Detail.Controls.Add(NewField);
			}
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			
		}

		public void ActiveReport_ToolbarClick(/*DDActiveReports2.DDTool Tool*/)
		{
			// Call VerifyPrintToFile(Me, Tool)
			string vbPorterVar = ""/*Tool.Caption*/;
			if (vbPorterVar == "Print...")
			{
				int intNumberOfPages = this.Document.Pages.Count;
				int intPageStart = 1;
				if (frmNumPages.InstancePtr.Init(ref intNumberOfPages, ref intPageStart, this))
				{
					this.PrintReport(false);
				}
			}
		}

		private void PrintMortgageHolders()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				int intControl;
				int intRow;
				string str1 = "";
				string str2 = "";
				string str3 = "";
				string str4 = "";
				string str5 = "";
				int lngAcctNumber;
				if (rsData.EndOfFile() != true)
				{
					str1 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name")));
					str2 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address1")));
					str3 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address2")));
					str4 = strLeftAdjustment + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Address3")));
					str5 = strLeftAdjustment + Strings.Trim(rsData.Get_Fields_String("City") + ", " + rsData.Get_Fields_String("State")) + " " + rsData.Get_Fields_String("Zip");
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Zip4"))) != "")
					{
						str5 = str4 + "-" + rsData.Get_Fields_String("Zip4");
					}
					// condense the labels if some are blank
					if (boolMort)
					{
						if (Strings.Trim(str4) == string.Empty)
						{
							str4 = str5;
							str5 = "";
						}
					}
					if (Strings.Trim(str3) == string.Empty)
					{
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					if (Strings.Trim(str2) == string.Empty)
					{
						str2 = str3;
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					if (Strings.Trim(str1) == string.Empty)
					{
						str1 = str2;
						str2 = str3;
						str3 = str4;
						if (boolMort)
						{
							str4 = str5;
							str5 = "";
						}
						else
						{
							str4 = "";
						}
					}
					for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
					{
						if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
						{
							intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
							switch (intRow)
							{
								case 1:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str1;
										break;
									}
								case 2:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str2;
										break;
									}
								case 3:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str3;
										break;
									}
								case 4:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str4;
										break;
									}
								case 5:
									{
										(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str5;
										break;
									}
							}
							//end switch
						}
						// this will fill the account number in
						// If Trim(Left(Detail.Controls[intControl].Name & "      ", 7)) = "txtAcct" Then
						// Detail.Controls[intControl].Text = Format(rsData.Fields("AccountNumber"), "00000")
						// End If
						if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
						{
							(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields_Int32("MortgageHolderID"), "000");
						}
					}
					// intControl
					rsData.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Printing Mortgage Holders", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PrintLabels()
		{
			// this will fill in the fields for labels
			int intControl;
			int intRow;
			string str1 = "";
			string str2 = "";
			string str3 = "";
			string str4 = "";
			string str5 = "";
			if (rsData.EndOfFile() != true)
			{
				if (FCConvert.ToInt32(rsData.Get_Fields_Int32("MortgageHolder")) != 0)
				{
					boolMort = true;
					rsMort.FindFirstRecord("ID", rsData.Get_Fields_Int32("MortgageHolder"));
					if (!rsMort.EndOfFile())
					{
						str1 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Name")));
						str2 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address1")));
						str3 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address2")));
						str4 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Address3")));
						str5 = Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("City"))) + " " + rsMort.Get_Fields_String("State") + " " + rsMort.Get_Fields_String("Zip");
						if (Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4"))) != "")
						{
							str4 += "-" + Strings.Trim(FCConvert.ToString(rsMort.Get_Fields_String("Zip4")));
						}
					}
					else
					{
						str1 = "Missing Data for Mortgage Holder #" + rsData.Get_Fields_Int32("MortgageHolder");
						str2 = "";
						str3 = "";
						str4 = "";
					}
				}
				else
				{
					boolMort = false;
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2"))) != "")
					{
						str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1"))) + " and " + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name2")));
					}
					else
					{
						str1 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Name1")));
					}
					str2 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSAddr1")));
					str3 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSAddr2")));
					str4 = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSAddr3"))) + " " + rsData.Get_Fields_String("RSState") + " " + rsData.Get_Fields_String("RSZip");
					if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSZip4"))) != "")
					{
						str4 += "-" + Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("RSZip4")));
					}
				}
				if (intTypeOfLabel == 2)
				{
					// small labels
					if (str1.Length > 18)
					{
						str5 = str4;
						str4 = str3;
						str3 = str2;
						str2 = Strings.Right(str1, str1.Length - 18);
						str1 = Strings.Left(str1, 18);
					}
				}
				else
				{
					if (str1.Length > 35)
					{
						// 4" labels
						str5 = str4;
						str4 = str3;
						str3 = str2;
						str2 = Strings.Right(str1, str1.Length - 35);
						str1 = Strings.Left(str1, 35);
					}
				}
				// condense the labels if some are blank
				// If boolMort Then
				if (Strings.Trim(str4) == string.Empty)
				{
					str4 = str5;
					str5 = "";
				}
				// End If
				if (Strings.Trim(str3) == string.Empty)
				{
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str2) == string.Empty)
				{
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				if (Strings.Trim(str1) == string.Empty)
				{
					str1 = str2;
					str2 = str3;
					str3 = str4;
					if (boolMort)
					{
						str4 = str5;
						str5 = "";
					}
					else
					{
						str4 = "";
					}
				}
				for (intControl = 0; intControl <= Detail.Controls.Count - 1; intControl++)
				{
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtData")
					{
						intRow = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(Detail.Controls[intControl].Name, 8))));
						switch (intRow)
						{
							case 1:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str1;
									break;
								}
							case 2:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str2;
									break;
								}
							case 3:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str3;
									break;
								}
							case 4:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str4;
									break;
								}
							case 5:
								{
									(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = strLeftAdjustment + str5;
									break;
								}
						}
						//end switch
					}
					// this will fill the account number in
					if (Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtAcct")
					{
						// TODO Get_Fields: Field [New.Account] not found!! (maybe it is an alias?)
						(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields("New.Account"), "#0000");
					}
					if (boolMort && Strings.Trim(Strings.Left(Detail.Controls[intControl].Name + "      ", 7)) == "txtMort")
					{
						// TODO Get_Fields: Field [MortgageHolderNumber] not found!! (maybe it is an alias?)
						(Detail.Controls[intControl] as GrapeCity.ActiveReports.SectionReportModel.TextBox).Text = Strings.Format(rsData.Get_Fields("MortgageHolderNumber"), "00000");
					}
				}
				// intControl
				rsData.MoveNext();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (Strings.UCase(modUTStatusList.Statics.strReportType) == "LABELS")
			{
				PrintLabels();
			}
			else if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "RECL") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "MORTGAGEHOLDER"))
			{
				PrintMortgageHolders();
			}
		}

		private void rptCustomMHLabels_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptCustomMHLabels properties;
			//rptCustomMHLabels.Caption	= "Custom Labels";
			//rptCustomMHLabels.Icon	= "rptCustomMHLabels.dsx":0000";
			//rptCustomMHLabels.Left	= 0;
			//rptCustomMHLabels.Top	= 0;
			//rptCustomMHLabels.Width	= 11880;
			//rptCustomMHLabels.Height	= 8595;
			//rptCustomMHLabels.StartUpPosition	= 3;
			//rptCustomMHLabels.SectionData	= "rptCustomMHLabels.dsx":058A;
			//End Unmaped Properties
		}
	}
}
