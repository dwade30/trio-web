﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Wisej.Web;
using Global;
using SharedApplication.Extensions;

namespace TWUT0000
{



    public partial class frmCustomNorwayZeroBills : BaseForm
    {
        private int lngProcessPhase = 0;
        private bool boolConfirmed = false;
        private bool boolInProcess = false;
        private int lngBookNum = 0;
        public frmCustomNorwayZeroBills()
        {
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            this.cmdCancel.Click += CmdCancel_Click;
            this.Load += FrmCustomNorwayZeroBills_Load;
        }

        private void FrmCustomNorwayZeroBills_Load(object sender, EventArgs e)
        {
            FillBooksCombo();
            if (cboBooks.ListCount > 0)
            {
                cboBooks.ListIndex = 0;
            }
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);

            lngProcessPhase = 1;
            boolInProcess = false;
        }

        private void FillBooksCombo()
        {
            var rs = new clsDRWrapper();
            cboBooks.Clear();
            rs.OpenRecordset("Select * from Book order by BookNumber");
            while (!rs.EndOfFile())
            {
                cboBooks.AddItem(rs.Get_Fields_Int32("BookNumber").ToString().PadLeft(3,'0') + " - " + rs.Get_Fields_String("Description"));
                cboBooks.ItemData(cboBooks.NewIndex, rs.Get_Fields_Int32("BookNumber"));
            }
        }

        private void CmdCancel_Click(object sender, EventArgs e)
        {
            if (!boolInProcess)
            {
                Unload();
            }
        }

        private void cmdProcess_Click(object sender, EventArgs e)
        {
            ProcessSave();
        }

        private void ProcessSave()
        {
            if (!boolInProcess)
            {
                boolInProcess = true;
                if (lngProcessPhase == 1)
                {
                    if (cboBooks.ListIndex >= 0)
                    {
                        lngBookNum = cboBooks.ItemData(cboBooks.ListIndex);

                        var rsBill = new clsDRWrapper();
                        var lngAcctCnt = 0;
                        rsBill.OpenRecordset(
                            "select distinct ActualAccountNumber from Bill where book = " + lngBookNum.ToString() +
                            " and BillStatus = 'B' and (WPrinOwed <> 0 or SPrinOwed <> 0)", "UtilityBilling");
                        lngAcctCnt = rsBill.RecordCount();

                        var lngBillCnt = 0;
                        rsBill.OpenRecordset(
                            "Select count(id) as TheCount from Bill where Book = " + lngBookNum.ToString() +
                            " and BillStatus = 'B' and (WPrinOwed <> 0 or SPrinOwed <> 0)", "UtilityBilling");
                        lngBillCnt = rsBill.Get_Fields_Int32("TheCount");

                        if (lngBillCnt > 0)
                        {
                            lngProcessPhase = 2;
                            lblWarn1.Text = "ALL BILLS FOR BOOK " + lngBookNum.ToString() + " WILL BE ZEROED";
                            lblWarn3.Text = lngBillCnt.ToString() + " Bills in " + lngAcctCnt +
                                            " accounts will be affected";
                        }
                        else
                        {
                            MessageBox.Show("There are no bills to be zeroed", "Process Stopped", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                            Unload();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Please select a book", "No Book", MessageBoxButtons.OK, MessageBoxIcon.None);
                    }
                }                

                boolInProcess = false;
            }
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            ZeroBills();
        }

        private void ZeroBills()
        {
            if (lngProcessPhase == 2)
            {
                if (!boolInProcess)
                {
                    boolInProcess = true;

                    this.ShowLoader = true;

                    var rsBill = new clsDRWrapper();
                    var strBillList = new StringBuilder();
                    var strSepar = "";
                    rsBill.OpenRecordset(
                        "Select ID from Bill where book = " + lngBookNum.ToString() +
                        " and BillStatus = 'B' and (WPrinOwed <> 0 or SPrinOwed <> 0)", "UtilityBilling");
                    while (!rsBill.EndOfFile())
                    {
                        strBillList.Append(strSepar + rsBill.Get_Fields_Int32("ID").ToString());
                        strSepar = ",";
                        rsBill.MoveNext();
                    }

                    if (CreateZeroBillsJournalEntry(strBillList.ToString()))
                    {
                        rsBill.Execute(
                            "update bill set WPrinOwed = 0, TotalWBillAmount = 0, SPrinOwed = 0, TotalSBillAmount = 0 where id in (" +
                            strBillList.ToString(), "UtilityBilling");
                        modGlobalFunctions.AddCYAEntry("UT", "Zero Bill Process", "Book " + lngBookNum.ToString(), "",
                            "", "");
                        this.ShowLoader = false;
                        MessageBox.Show("The bills were zeroed successfully", "Process Completed", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                    else
                    {
                        this.ShowLoader = false;
                        MessageBox.Show("The bills were not zeroed", "Process Failed", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                    }

                    this.ShowLoader = false;
                    boolInProcess = false;
                }
            }
        }

        private bool CreateZeroBillsJournalEntry(string strBillList)
        {
            try
            {
                var strSAccountNumber = "";
                var strWAccountNumber = "";
                var strSTAAccountNum = "";
                var strWTAAccountNum = "";
                var Master = new clsDRWrapper();
                var dblWFundAmount = new Dictionary<int, double>();
                var dblSFundAmount = new Dictionary<int, double>();

                Master.OpenRecordset("Select * from standardaccounts", "Budgetary");
                if (!Master.EndOfFile())
                {
                    if (Master.FindFirst("Code = 'SR'"))
                    {
                        strSAccountNumber = Master.Get_Fields_String("Account");
                    }

                    if (Master.FindFirst("Code = 'WR'"))
                    {
                        strWAccountNumber = Master.Get_Fields_String("Account");
                    }

                    if (Master.FindFirst("Code = 'ST'"))
                    {
                        strSTAAccountNum = Master.Get_Fields_String("Account");
                    }

                    if (Master.FindFirst("Code = 'WT'"))
                    {
                        strWTAAccountNum = Master.Get_Fields_String("Account");
                    }
                }

                if (modUTCalculations.Statics.gUTSettings.OffersSewerServices())
                {
                    if (string.IsNullOrWhiteSpace(strSAccountNumber))
                    {
                        MessageBox.Show("Please enter the receivable account in budgetary",
                            "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

                if (modUTCalculations.Statics.gUTSettings.OffersWaterServices())
                {
                    if (string.IsNullOrWhiteSpace(strWAccountNumber))
                    {
                        MessageBox.Show("Please enter the receivable account in budgetary",
                            "Missing Receivable Account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }

                var strTableName = "";
                int lngJournal = 1;
                if (!modBudgetaryAccounting.LockJournal())
                {
                    strTableName = "Temp";
                }
                else
                {
                    Master.OpenRecordset("Select top 1 * from JournalMaster order by JournalNumber desc", "Budgetary");
                    if (!Master.EndOfFile())
                    {
                        lngJournal = Master.Get_Fields_Int32("JournalNumber") + 1;
                    }

                    Master.AddNew();
                    Master.Set_Fields("JournalNumber", lngJournal);
                    Master.Set_Fields("Status","E");
                    var dt = DateTime.Now;
                    var time = new DateTime(1899, 12, 30, dt.Hour, dt.Minute, dt.Second);
                    Master.Set_Fields("StatusChangeTime",time);
                    Master.Set_Fields("StatusChangeDate",DateTime.Now.Date);
                    Master.Set_Fields("Description", DateTime.Now.FormatAndPadShortDate() + " UT - Reverse Billing");
                    Master.Set_Fields("Type","GJ");
                    Master.Set_Fields("Period",DateTime.Now.Month);
                    Master.Update();
                    modBudgetaryAccounting.UnlockJournal();
                }

                var rsEntries = new clsDRWrapper();
                var rsBillInfo = new clsDRWrapper();
                var strSql = "";
               
                var fundLength = Convert.ToInt32(modAccountTitle.Statics.Ledger.Left(2));

                rsEntries.OpenRecordset("Select * from " + strTableName + "JournalEntries where id = 0", "Budgetary");
                if (modUTCalculations.Statics.gUTSettings.OffersWaterServices())
                {
                    strSql = "Select Sum(amount) as amt, AccountNumber from Breakdown where billkey in (" +
                             strBillList + ") and Service = 'W' and isnull(TaxAcquired,0) = 0 group by Accountnumber";
                    rsBillInfo.OpenRecordset(strSql);
                    while (!rsBillInfo.EndOfFile())
                    {
                        rsEntries.AddNew();
                        rsEntries.Set_Fields("Type","G");
                        rsEntries.Set_Fields("JournalEntriesDate",DateTime.Now);
                        rsEntries.Set_Fields("Description",DateTime.Now.FormatAndPadShortDate() + " UT - Rev Water");
                        rsEntries.Set_Fields("JournalNumber", lngJournal);
                        var intFund = modBudgetaryAccounting.GetFundFromAccount(rsBillInfo.Get_Fields_String("AccountNumber"));
                        if (intFund > 0)
                        {
                            double dblAmount = 0;
                            dblWFundAmount.TryGetValue(intFund, out dblAmount);
                            dblWFundAmount[intFund] = dblAmount + rsBillInfo.Get_Fields_Double("Amt");
                        }
                        rsEntries.Set_Fields("Account",rsBillInfo.Get_Fields_String("AccountNumber"));
                        rsEntries.Set_Fields("Amount",rsBillInfo.Get_Fields_Double("Amt"));
                        rsEntries.Set_Fields("WarrantNumber",0);
                        rsEntries.Set_Fields("Period", DateTime.Now.Month);
                        rsEntries.Set_Fields("RCB","R");
                        rsEntries.Set_Fields("Status","E");
                        rsEntries.Update();
                        rsBillInfo.MoveNext();
                    }
                    
                    foreach (var kvPair in dblWFundAmount)
                    {
                        if (kvPair.Value != 0)
                        {
                            rsEntries.AddNew();
                            rsEntries.Set_Fields("Type","G");
                            rsEntries.Set_Fields("JournalEntriesDate",DateTime.Now);
                            rsEntries.Set_Fields("Description",DateTime.Now.FormatAndPadShortDate() + " UT - Rev Water Receive");
                            rsEntries.Set_Fields("JournalNumber",lngJournal);
                            rsEntries.Set_Fields("Account","G " + kvPair.Key.ToString().PadLeft(fundLength,'0') + "-" + strWAccountNumber + "-00");
                            rsEntries.Set_Fields("Amount", kvPair.Value * -1);
                            rsEntries.Set_Fields("WarrantNumber",0);
                            rsEntries.Set_Fields("Period",DateTime.Now.Month);
                            rsEntries.Set_Fields("RCB","R");
                            rsEntries.Set_Fields("Status","E");
                            rsEntries.Update();
                        }
                    }
                    dblWFundAmount.Clear();

                    strSql =
                        "Select sum(Amount) as Amt, AccountNumber from Breakdown where billkey in (" + strBillList +
                        ") And Service = 'W' and isnull(TaxAcquired,0) = 1 Group by AccountNumber";
                    rsBillInfo.OpenRecordset(strSql, "UtilityBilling");
                    while (!rsBillInfo.EndOfFile())
                    {
                        rsEntries.AddNew();
                        rsEntries.Set_Fields("Type","G");
                        rsEntries.Set_Fields("JournalEntriesDate",DateTime.Now);
                        rsEntries.Set_Fields("Description", DateTime.Now.FormatAndPadShortDate() + " UT - Rev Water TA");
                        rsEntries.Set_Fields("JournalNumber",lngJournal);
                        var intFund =
                            modBudgetaryAccounting.GetFundFromAccount(rsBillInfo.Get_Fields_String("AccountNumber"));
                        if (intFund > 0)
                        {
                            double dblAmount = 0;
                            dblWFundAmount.TryGetValue(intFund, out dblAmount);
                            dblWFundAmount[intFund] = dblAmount + rsBillInfo.Get_Fields_Double("Amt");
                        }
                        rsEntries.Set_Fields("Account",rsBillInfo.Get_Fields_String("AccountNumber"));
                        rsEntries.Set_Fields("Amount",rsBillInfo.Get_Fields_Double("Amt"));
                        rsEntries.Set_Fields("WarrantNumber",0);
                        rsEntries.Set_Fields("Period",DateTime.Now.Month);
                        rsEntries.Set_Fields("RCB","R");
                        rsEntries.Set_Fields("Status","E");
                        rsEntries.Update();
                        rsBillInfo.MoveNext();
                    }

                    foreach (var kvPair in dblWFundAmount)
                    {
                        if (kvPair.Value != 0)
                        {
                            rsEntries.AddNew();
                            rsEntries.Set_Fields("Type","G");
                            rsEntries.Set_Fields("JournalEntriesDate",DateTime.Now);
                            rsEntries.Set_Fields("Description",DateTime.Now.FormatAndPadShortDate() + " UT Rev Water TA Receive");
                            rsEntries.Set_Fields("JournalNumber",lngJournal);
                            rsEntries.Set_Fields("Account", "G " + kvPair.Key.ToString().PadLeft(fundLength) + "-" + strWTAAccountNum + "-00");
                            rsEntries.Set_Fields("Amount",kvPair.Value * -1);
                            rsEntries.Set_Fields("WarrantNumber",0);
                            rsEntries.Set_Fields("Period",DateTime.Now.Month);
                            rsEntries.Set_Fields("RCB","R");
                            rsEntries.Set_Fields("Status","E");
                            rsEntries.Update();
                        }
                    }
                }

                if (modUTCalculations.Statics.gUTSettings.OffersSewerServices())
                {
                    strSql = "Select Sum(Amount) as Amt, AccountNumber from Breakdown where billkey in (" +
                             strBillList + ") and Service = 'S' and isnull(TaxAcquired,0) = 0 group by accountnumber";
                    rsBillInfo.OpenRecordset(strSql, "UtilityBilling");
                    while (!rsBillInfo.EndOfFile())
                    {
                        rsEntries.AddNew();
                        rsEntries.Set_Fields("Type","G");
                        rsEntries.Set_Fields("JournalEntriesDate",DateTime.Now);
                        rsEntries.Set_Fields("Description",DateTime.Now.FormatAndPadShortDate() + " UT - Rev Sewer");
                        rsEntries.Set_Fields("JournalNumber",lngJournal);
                        var intFund =
                            modBudgetaryAccounting.GetFundFromAccount(rsBillInfo.Get_Fields_String("AccountNumber"));
                        if (intFund > 0)
                        {
                            double dblAmount = 0;
                            dblSFundAmount.TryGetValue(intFund, out dblAmount);
                            dblSFundAmount[intFund] = dblAmount + rsBillInfo.Get_Fields_Double("Amt");
                        }
                        rsEntries.Set_Fields("Account",rsBillInfo.Get_Fields_String("AccountNumber"));
                        rsEntries.Set_Fields("Amount",rsBillInfo.Get_Fields_Double("Amt"));
                        rsEntries.Set_Fields("WarrantNumber",0);
                        rsEntries.Set_Fields("Period",DateTime.Now.Month);
                        rsEntries.Set_Fields("RCB","R");
                        rsEntries.Set_Fields("Status","E");
                        rsEntries.Update();
                        rsBillInfo.MoveNext();
                    }

                    foreach (var kvPair in dblSFundAmount)
                    {
                        if (kvPair.Value != 0)
                        {
                            rsEntries.AddNew();
                            rsEntries.Set_Fields("Type","G");
                            rsEntries.Set_Fields("JournalEntriesDate",DateTime.Now);
                            rsEntries.Set_Fields("Description", DateTime.Now.FormatAndPadShortDate() + " UT Rev Sewer - Receive");
                            rsEntries.Set_Fields("JournalNumber",lngJournal);
                            rsEntries.Set_Fields("Account", "G " + kvPair.Key.ToString().Left(fundLength) + "-" + strSAccountNumber + "-00");
                            rsEntries.Set_Fields("Amount", kvPair.Value * -1);
                            rsEntries.Set_Fields("WarrantNumber",0);
                            rsEntries.Set_Fields("Period",DateTime.Now.Month);
                            rsEntries.Set_Fields("RCB","R");
                            rsEntries.Set_Fields("Status","E");
                            rsEntries.Update();
                        }
                    }

                    dblSFundAmount.Clear();

                    strSql = "Select sum (Amount) as Amt, AccountNumber from breakdown where billkey in (" +
                             strBillList + ") and service = 'S' and isnull(TaxAcquired,0) = 1 group by AccountNumber";
                    rsBillInfo.OpenRecordset(strSql, "UtilityBilling");
                    while (!rsBillInfo.EndOfFile())
                    {
                        rsEntries.AddNew();
                        rsEntries.Set_Fields("Type","G");
                        rsEntries.Set_Fields("JournalEntriesDate",DateTime.Now);
                        rsEntries.Set_Fields("Description", DateTime.Now.FormatAndPadShortDate( ) + " UT - Rev Sewer TA");
                        rsEntries.Set_Fields("JournalNumber",lngJournal);
                        var intFund =
                            modBudgetaryAccounting.GetFundFromAccount(rsBillInfo.Get_Fields_String("AccountNumber"));
                        if (intFund > 0)
                        {
                            double dblAmount = 0;
                            dblSFundAmount.TryGetValue(intFund, out dblAmount);
                            dblSFundAmount[intFund] = dblAmount + rsBillInfo.Get_Fields_Double("Amt");
                        }

                        rsEntries.Set_Fields("Account", rsBillInfo.Get_Fields_String("AccountNumber"));
                        rsEntries.Set_Fields("Amount",rsBillInfo.Get_Fields_Double("Amt"));
                        rsEntries.Set_Fields("WarrantNumber",0);
                        rsEntries.Set_Fields("Period",DateTime.Now.Month);
                        rsEntries.Set_Fields("RCB","R");
                        rsEntries.Set_Fields("Status","E");

                        rsEntries.Update();
                        rsBillInfo.MoveNext();
                    }

                    foreach (var kvPair in dblSFundAmount)
                    {
                        if (kvPair.Value != 0)
                        {
                            rsEntries.AddNew();
                            rsEntries.Set_Fields("Type","G");
                            rsEntries.Set_Fields("JournalEntriesDate",DateTime.Now);
                            rsEntries.Set_Fields("Description",DateTime.Now.FormatAndPadShortDate() + " UT Rev Sewer - TA Receive");
                            rsEntries.Set_Fields("JournalNumber",lngJournal);
                            rsEntries.Set_Fields("Account","G " + kvPair.Key.ToString().Left(fundLength) + "-" + strSTAAccountNum + "-00");
                            rsEntries.Set_Fields("Amount", kvPair.Value * -1);
                            rsEntries.Set_Fields("WarrantNumber",0);
                            rsEntries.Set_Fields("Period",DateTime.Now.Month);
                            rsEntries.Set_Fields("RCB","R");
                            rsEntries.Set_Fields("Status","E");
                            rsEntries.Update();
                        }
                    }
                }

                this.ShowLoader = false;
                if (lngJournal == 0)
                {
                    MessageBox.Show(
                        "Unable to complete the journal. You must go into the Budgetary system > Data Entry > Build Incomplete Journals to finish creating the journal.",
                        "Unable to Save", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    if (modSecurityValidation.ValidPermissions(null, modMain.UTSECAUTOPOSTJOURNAL, false) &&
                        modBudgetaryAccounting.AutoPostAllowed(modBudgetaryAccounting.AutoPostType.aptUTBills))
                    {
                        if (MessageBox.Show("The entries have been saved into journal " + lngJournal +". Would you like to post the journal?","Post Entries", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            var tJournalInfo = new clsPostingJournalInfo();
                            tJournalInfo.JournalNumber = lngJournal;
                            tJournalInfo.JournalType = "GJ";
                            tJournalInfo.Period = DateTime.Now.Month.ToString();
                            tJournalInfo.CheckDate = "";
                            var tPostInfo = new clsBudgetaryPosting();
                            tPostInfo.AddJournal(tJournalInfo);
                            tPostInfo.AllowPreview = true;
                            tPostInfo.PageBreakBetweenJournalsOnReport = true;
                            tPostInfo.WaitForReportToEnd = true;
                            tPostInfo.PostJournals();
                        }
                        else
                        {
                            MessageBox.Show(
                                "The budgetary journal entries were created in journal " + lngJournal.ToString(),
                                "Journal Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                this.ShowLoader = false;
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
    }

}
