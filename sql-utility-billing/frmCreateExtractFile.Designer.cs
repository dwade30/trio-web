﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;
using System.IO;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCreateExtractFile.
	/// </summary>
	partial class frmCreateExtractFile : BaseForm
	{
		public fecherFoundation.FCComboBox cmbAllAccounts;
		public fecherFoundation.FCLabel lblAllAccounts;
		public fecherFoundation.FCComboBox cmbName;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCComboBox cmbCreatePrintCopy;
		public fecherFoundation.FCLabel lblCreatePrintCopy;
		public fecherFoundation.FCFrame fraCreateOptions;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCreateExtractFile));
			this.cmbAllAccounts = new fecherFoundation.FCComboBox();
			this.lblAllAccounts = new fecherFoundation.FCLabel();
			this.cmbName = new fecherFoundation.FCComboBox();
			this.lblName = new fecherFoundation.FCLabel();
			this.cmbCreatePrintCopy = new fecherFoundation.FCComboBox();
			this.lblCreatePrintCopy = new fecherFoundation.FCLabel();
			this.fraCreateOptions = new fecherFoundation.FCFrame();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessSave = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCreateOptions)).BeginInit();
			this.fraCreateOptions.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdProcessSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 369);
			this.BottomPanel.Size = new System.Drawing.Size(610, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraCreateOptions);
			this.ClientArea.Controls.Add(this.cmbCreatePrintCopy);
			this.ClientArea.Controls.Add(this.lblCreatePrintCopy);
			this.ClientArea.Size = new System.Drawing.Size(610, 309);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(610, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(203, 30);
			this.HeaderText.Text = "Create Extract File";
			// 
			// cmbAllAccounts
			// 
			this.cmbAllAccounts.AutoSize = false;
			this.cmbAllAccounts.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbAllAccounts.FormattingEnabled = true;
			this.cmbAllAccounts.Items.AddRange(new object[] {
				"Sewer",
				"Water",
				"Both",
				"All"
			});
			this.cmbAllAccounts.Location = new System.Drawing.Point(175, 90);
			this.cmbAllAccounts.Name = "cmbAllAccounts";
			this.cmbAllAccounts.Size = new System.Drawing.Size(235, 40);
			this.cmbAllAccounts.TabIndex = 2;
			this.cmbAllAccounts.Text = "All";
			// 
			// lblAllAccounts
			// 
			this.lblAllAccounts.AutoSize = true;
			this.lblAllAccounts.Location = new System.Drawing.Point(20, 104);
			this.lblAllAccounts.Name = "lblAllAccounts";
			this.lblAllAccounts.Size = new System.Drawing.Size(112, 15);
			this.lblAllAccounts.TabIndex = 3;
			this.lblAllAccounts.Text = "ACCOUNT TYPES";
			// 
			// cmbName
			// 
			this.cmbName.AutoSize = false;
			this.cmbName.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbName.FormattingEnabled = true;
			this.cmbName.Items.AddRange(new object[] {
				"Name",
				"Location",
				"Sequence"
			});
			this.cmbName.Location = new System.Drawing.Point(175, 30);
			this.cmbName.Name = "cmbName";
			this.cmbName.Size = new System.Drawing.Size(235, 40);
			this.cmbName.TabIndex = 0;
			this.cmbName.Text = "Name";
			// 
			// lblName
			// 
			this.lblName.AutoSize = true;
			this.lblName.Location = new System.Drawing.Point(20, 44);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(72, 15);
			this.lblName.TabIndex = 1;
			this.lblName.Text = "ORDER BY";
			// 
			// cmbCreatePrintCopy
			// 
			this.cmbCreatePrintCopy.AutoSize = false;
			this.cmbCreatePrintCopy.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbCreatePrintCopy.FormattingEnabled = true;
			this.cmbCreatePrintCopy.Items.AddRange(new object[] {
				"Create Print and Copy File",
				"Print and Copy Existing File",
				"Print Existing File",
				"Copy Existing File"
			});
			this.cmbCreatePrintCopy.Location = new System.Drawing.Point(205, 30);
			this.cmbCreatePrintCopy.Name = "cmbCreatePrintCopy";
			this.cmbCreatePrintCopy.Size = new System.Drawing.Size(257, 40);
			this.cmbCreatePrintCopy.TabIndex = 6;
			this.cmbCreatePrintCopy.Text = "Create Print and Copy File";
			this.cmbCreatePrintCopy.SelectedIndexChanged += new System.EventHandler(this.cmbCreatePrintCopy_SelectedIndexChanged);
			// 
			// lblCreatePrintCopy
			// 
			this.lblCreatePrintCopy.AutoSize = true;
			this.lblCreatePrintCopy.Location = new System.Drawing.Point(30, 44);
			this.lblCreatePrintCopy.Name = "lblCreatePrintCopy";
			this.lblCreatePrintCopy.Size = new System.Drawing.Size(124, 15);
			this.lblCreatePrintCopy.TabIndex = 7;
			this.lblCreatePrintCopy.Text = "EXTRACT OPTIONS";
			// 
			// fraCreateOptions
			// 
			this.fraCreateOptions.Controls.Add(this.cmbName);
			this.fraCreateOptions.Controls.Add(this.lblName);
			this.fraCreateOptions.Controls.Add(this.cmbAllAccounts);
			this.fraCreateOptions.Controls.Add(this.lblAllAccounts);
			this.fraCreateOptions.Location = new System.Drawing.Point(30, 90);
			this.fraCreateOptions.Name = "fraCreateOptions";
			this.fraCreateOptions.Size = new System.Drawing.Size(432, 150);
			this.fraCreateOptions.TabIndex = 5;
			this.fraCreateOptions.Text = "Create Options";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessSave
			// 
			this.cmdProcessSave.AppearanceKey = "acceptButton";
			this.cmdProcessSave.Location = new System.Drawing.Point(228, 41);
			this.cmdProcessSave.Name = "cmdProcessSave";
			this.cmdProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessSave.Size = new System.Drawing.Size(88, 48);
			this.cmdProcessSave.TabIndex = 0;
			this.cmdProcessSave.Text = "Save";
			this.cmdProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// frmCreateExtractFile
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(610, 477);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmCreateExtractFile";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Create Extract File";
			this.Load += new System.EventHandler(this.frmCreateExtractFile_Load);
			this.Activated += new System.EventHandler(this.frmCreateExtractFile_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmCreateExtractFile_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraCreateOptions)).EndInit();
			this.fraCreateOptions.ResumeLayout(false);
			this.fraCreateOptions.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessSave;
	}
}
