﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptAnalysisReportsMaster.
	/// </summary>
	public partial class rptAnalysisReportsMaster : BaseSectionReport
	{
		public rptAnalysisReportsMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Analysis Reports";
		}

		public static rptAnalysisReportsMaster InstancePtr
		{
			get
			{
				return (rptAnalysisReportsMaster)Sys.GetInstance(typeof(rptAnalysisReportsMaster));
			}
		}

		protected rptAnalysisReportsMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAnalysisReportsMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/12/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/28/2006              *
		// ********************************************************
		public bool boolWide;
		// True if this report is in Wide Format
		public float lngWidth;
		// This is the Print Width of the report
		bool boolDone;
		public bool boolFinal;
		public string strType = "";
		public bool boolOrderByReceipt;
		// order by receipt
		public bool boolCloseOutAudit;
		// Actually close out the payments
		public bool boolLandscape;
		// is the report to be printed landscape?
		public int intType;
		// 0 - Water, 1 - Sewer, 2 - Both
		public string strSQL = string.Empty;
		public bool boolPreBilling;
		public string strBookList = string.Empty;
		public string strRateKeyList = string.Empty;
		bool boolBillingSummary;
		bool boolDollarAmountsW;
		bool boolConsumptionsW;
		bool boolBillCountW;
		bool boolMeterReportW;
		bool boolSalesTaxW;
		bool boolDollarAmountsS;
		bool boolConsumptionsS;
		bool boolBillCountS;
		bool boolMeterReportS;
		bool boolSalesTaxS;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			// this should let the detail section fire once
			eArgs.EOF = boolDone;
			boolDone = true;
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
			frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading", true);
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void SetupFields()
		{
			// find out which way the user wants to print
			boolWide = boolLandscape;
			if (boolWide)
			{
				// wide format
				lngWidth = 14400 / 1440F;
				this.Document.Printer.DefaultPageSettings.Landscape = true;
			}
			else
			{
				// narrow format
				lngWidth = 10800 / 1440F;
				this.Document.Printer.DefaultPageSettings.Landscape = false;
			}
			// set the widths/lefts of all of the objects
			this.PrintWidth = lngWidth;
			lblHeader.Width = lngWidth;
			lblReportType.Width = lngWidth;
			lblPageNumber.Left = lngWidth - lblPageNumber.Width;
			lblDate.Left = lngWidth - lblDate.Width;
			sarBillingSummary.Width = lngWidth;
			sarDollarAmountsS.Width = lngWidth;
			sarConsumptionsS.Width = lngWidth;
			sarBillCountS.Width = lngWidth;
			sarMeterReportS.Width = lngWidth;
			sarSalesTaxS.Width = lngWidth;
			sarDollarAmountsW.Width = lngWidth;
			sarConsumptionsW.Width = lngWidth;
			sarBillCountW.Width = lngWidth;
			sarMeterReportW.Width = lngWidth;
			sarSalesTaxW.Width = lngWidth;
		}
		// vbPorter upgrade warning: intPassType As short	OnWriteFCConvert.ToInt32(
		public void Init(string strPassSQL, bool boolShowBillingSummary, bool boolShowDollarAmountsW, bool boolShowConsumptionsW, bool boolShowBillCountW, bool boolShowMeterReportW, bool boolShowSalesTaxW, bool boolShowDollarAmountsS, bool boolShowConsumptionsS, bool boolShowBillCountS, bool boolShowMeterReportS, bool boolShowSalesTaxS, bool boolPassLandscape, ref string strPassBookString, int intPassType = 2, bool boolPreBillingRun = false, bool boolSubReport = false, string strPassRKList = "", bool boolPassFinal = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				boolPreBilling = boolPreBillingRun;
				boolFinal = boolPassFinal;
				// 0 - Water, 1 - Sewer, 2 - Both
				intType = intPassType;
				// This is the list of books to include in the report
				strBookList = strPassBookString;
				// this will create a list of billkeys to use in the reports
				strSQL = strPassSQL;
				// List of Rate Keys
				strRateKeyList = strPassRKList;
				boolLandscape = boolPassLandscape;
				// this is to show each report
				boolBillingSummary = boolShowBillingSummary;
				boolDollarAmountsW = boolShowDollarAmountsW;
				boolConsumptionsW = boolShowConsumptionsW;
				boolBillCountW = boolShowBillCountW;
				boolMeterReportW = boolShowMeterReportW;
				boolSalesTaxW = boolShowSalesTaxW;
				boolDollarAmountsS = boolShowDollarAmountsS;
				boolConsumptionsS = boolShowConsumptionsS;
				boolBillCountS = boolShowBillCountS;
				boolMeterReportS = boolShowMeterReportS;
				boolSalesTaxS = boolShowSalesTaxS;
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				// set the size of the objects
				SetupFields();
				frmWait.InstancePtr.IncrementProgress();
				if (!boolSubReport)
				{
					frmReportViewer.InstancePtr.Init(this, "", 0, false, false, "Pages", boolLandscape);
					lblHeader.Text = "Analysis Reports";
				}
				else
				{
					lblHeader.Text = "Billing Edit Summary";
					// Me.Run False
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Start Up", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// this will load all of the reports that are selected by the user
			// Billing Summary
			if (rptAnalysisReportsMaster.InstancePtr.boolBillingSummary)
			{
				sarBillingSummary.Report = new rptAnalBillingSummary();
			}
			else
			{
				sarBillingSummary.Visible = false;
			}
			// Dollar Amounts
			if (rptAnalysisReportsMaster.InstancePtr.boolDollarAmountsW)
			{
				sarDollarAmountsW.Report = new rptAnalDollarAmountsW();
			}
			else
			{
				sarDollarAmountsW.Visible = false;
			}
			if (rptAnalysisReportsMaster.InstancePtr.boolDollarAmountsS)
			{
				sarDollarAmountsS.Report = new rptAnalDollarAmountsS();
			}
			else
			{
				sarDollarAmountsS.Visible = false;
			}
			// Consumption Reports
			if (rptAnalysisReportsMaster.InstancePtr.boolConsumptionsW)
			{
				sarConsumptionsW.Report = new rptAnalConsumptionW();
			}
			else
			{
				sarConsumptionsW.Visible = false;
			}
			if (rptAnalysisReportsMaster.InstancePtr.boolConsumptionsS)
			{
				sarConsumptionsS.Report = new rptAnalConsumptionS();
			}
			else
			{
				sarConsumptionsS.Visible = false;
			}
			// Bill Count
			if (rptAnalysisReportsMaster.InstancePtr.boolBillCountW)
			{
				sarBillCountW.Report = new rptAnalCountW();
			}
			else
			{
				sarBillCountW.Visible = false;
			}
			if (rptAnalysisReportsMaster.InstancePtr.boolBillCountS)
			{
				sarBillCountS.Report = new rptAnalCountS();
			}
			else
			{
				sarBillCountS.Visible = false;
			}
			// Meter Report
			if (rptAnalysisReportsMaster.InstancePtr.boolMeterReportW)
			{
				sarMeterReportW.Report = new rptAnalMeterReportW();
			}
			else
			{
				sarMeterReportW.Visible = false;
			}
			if (rptAnalysisReportsMaster.InstancePtr.boolMeterReportS)
			{
				sarMeterReportS.Report = new rptAnalMeterReportS();
			}
			else
			{
				sarMeterReportS.Visible = false;
			}
			// Sales Tax Report
			if (rptAnalysisReportsMaster.InstancePtr.boolSalesTaxW)
			{
				sarSalesTaxW.Report = new rptAnalSalesTaxW();
			}
			else
			{
				sarSalesTaxW.Visible = false;
			}
			if (rptAnalysisReportsMaster.InstancePtr.boolSalesTaxS)
			{
				sarSalesTaxS.Report = new rptAnalSalesTaxS();
			}
			else
			{
				sarSalesTaxS.Visible = false;
			}
		}

		
	}
}
