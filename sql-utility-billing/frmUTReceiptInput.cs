﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReceiptInput.
	/// </summary>
	public partial class frmReceiptInput : BaseForm
	{
		public frmReceiptInput()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
			//        for(int i = 0; i < modStatusPayments.MAX_PAYMENTS; i++)
			//        {
			//            this.arr[i] = new CashOut_Payment(0);
			//modUseCR.Statics.ReceiptArray[i] = new modUseCR.Receipt(0);
			//        }
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmReceiptInput InstancePtr
		{
			get
			{
				return (frmReceiptInput)Sys.GetInstance(typeof(frmReceiptInput));
			}
		}

		protected frmReceiptInput _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// This is a dummy form to allow the fix for bucksport when the focus was not on
		// the getmasteraccount screen when coming through CR
		public bool boolLoadingUTsearch;

		private void frmReceiptInput_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmReceiptInput properties;
			//frmReceiptInput.ScaleWidth	= 4680;
			//frmReceiptInput.ScaleHeight	= 3090;
			//frmReceiptInput.LinkTopic	= "Form1";
			//End Unmaped Properties
		}
	}
}
