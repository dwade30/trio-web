﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmSetupMeterReports.
	/// </summary>
	public partial class frmSetupMeterReports : BaseForm
	{
		public frmSetupMeterReports()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupMeterReports InstancePtr
		{
			get
			{
				return (frmSetupMeterReports)Sys.GetInstance(typeof(frmSetupMeterReports));
			}
		}

		protected frmSetupMeterReports _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               07/14/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               08/28/2006              *
		// *
		// This form will be used to select which meters should   *
		// be included in the meter slips or meter exchange list  *
		// ********************************************************
		public string strMeterReportType = "";
		// S - Meter Slips  E - Meter Exchange List   L - Labels
		string strSQL = "";
		clsPrintLabel labLabelTypes = new clsPrintLabel();

		private void cboLabelType_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			int intIndex;
			intIndex = labLabelTypes.Get_IndexFromID(cboLabelType.ItemData(cboLabelType.SelectedIndex));
			lblLabelDescription.Text = labLabelTypes.Get_Description(intIndex);
		}

		private void frmSetupMeterReports_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupMeterReports_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupMeterReports properties;
			//frmSetupMeterReports.FillStyle	= 0;
			//frmSetupMeterReports.ScaleWidth	= 5880;
			//frmSetupMeterReports.ScaleHeight	= 4275;
			//frmSetupMeterReports.LinkTopic	= "Form2";
			//frmSetupMeterReports.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// if getting called for meter excchange report then show set date frame and hide slip size frame
			if (strMeterReportType == "E")
			{
				fraSetDate.Visible = true;
				cmbLong.Visible = false;
				lblLong.Visible = false;
				cmbWS.Visible = false;
				fraLabelInformation.Visible = false;
				//optAccount.Enabled = true;
				cmbName.Clear();
				cmbName.Items.Add("Name");
				cmbName.Items.Add("Location");
				cmbName.Items.Add("Account");
				cmbName.Items.Add("Serial Number");
				cmbName.Text = "Name";
				this.Text = "Meter Exchange List";

            }
			else if (strMeterReportType == "S")
			{
				// if slips then show frame to pick slip size and hide set date frame
				fraSetDate.Visible = false;
				cmbLong.Visible = true;
				lblLong.Visible = true;
				cmbWS.Visible = false;
				fraLabelInformation.Visible = false;
				//optAccount.Enabled = true;
				cmbName.Clear();
				cmbName.Items.Add("Name");
				cmbName.Items.Add("Location");
				cmbName.Items.Add("Account");
				cmbName.Items.Add("Serial Number");
				cmbName.Text = "Name";
				this.Text = "Meter Reading Slips";
			}
			else
			{
				// if labels then show frame to enter label information
				fraSetDate.Visible = false;
				cmbLong.Visible = false;
				lblLong.Visible = false;
				cmbWS.Visible = true;
				fraLabelInformation.Visible = true;
				//optAccount.Enabled = true;
				cmbName.Clear();
				cmbName.Items.Add("Name");
				cmbName.Items.Add("Location");
				cmbName.Items.Add("Account");
				cmbName.Items.Add("Serial Number");
				cmbName.Text = "Name";
				this.Text = "Account Labels";
			}
            this.HeaderText.Text = this.Text;
            FillLabelTypeCombo();
			cboLabelType.SelectedIndex = 0;
			if (modUTStatusPayments.Statics.TownService == "B")
			{
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = true;
				//optWS[2].Enabled = true;
				cmbWS.Clear();
				cmbWS.Items.Add("All");
				cmbWS.Items.Add("Water");
				cmbWS.Items.Add("Sewer");
				cmbWS.Text = "All";
			}
			else if (modUTStatusPayments.Statics.TownService == "W")
			{
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = true;
				//optWS[2].Enabled = false;
				cmbWS.Clear();
				cmbWS.Items.Add("All");
				cmbWS.Items.Add("Water");
				cmbWS.Text = "All";
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				//optWS[0].Enabled = true;
				//optWS[1].Enabled = false;
				//optWS[2].Enabled = true;
				cmbWS.Clear();
				cmbWS.Items.Add("All");
				cmbWS.Items.Add("Sewer");
				cmbWS.Text = "All";
			}
			else
			{
				//optWS[0].Enabled = false;
				//optWS[1].Enabled = false;
				//optWS[2].Enabled = false;
				cmbWS.Enabled = false;
			}
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}

		private void frmSetupMeterReports_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			string strOrderBy = "";
			int[] lngFake = null;
			string strPrinter = "";
			int NumFonts = 0;
			bool boolUseFont = false;
			int intCPI = 0;
			int X;
			string strFont = "";
			int intReturn;
			// if this is for the meter exchange report then make sure they selected a valid sate
			if (strMeterReportType == "E")
			{
				if (!Information.IsDate(txtSetDate.Text))
				{
					MessageBox.Show("You must enter a valid date before you may continue.", "Invalid Date", MessageBoxButtons.OK, MessageBoxIcon.Information);
					txtSetDate.Focus();
					return;
				}
			}
			if (cmbName.Text == "Name")
			{
				// trout-1153 03.27.2017 kjr section moved up
				strOrderBy = "N";
				// Name
			}
			else if (cmbName.Text == "Location")
			{
				strOrderBy = "L";
				// Location
			}
			else if (cmbName.Text == "Account" || cmbName.Text == "Sequence #")
			{
				strOrderBy = "A";
				// Sequence
			}
			else
			{
				strOrderBy = "S";
				// Serial Number
			}
			// if the user only wants selected books then go to the book choice screen
			// trout-1153 03.27.2017 kjr added parms to send to book form
			if (cmbAll.SelectedIndex == 1)
			{
				if (strMeterReportType == "E")
				{
					frmBookChoice.InstancePtr.Init(11, 0, false, "", "", true, DateAndTime.DateValue(txtSetDate.Text),
                        chkShowNoMeterSetDate.CheckState == Wisej.Web.CheckState.Checked, cboLabelType.SelectedIndex,
                        cmbLabelLocation.SelectedIndex == 0, strOrderBy, FCConvert.ToInt16(cmbWS.Text == "Water"),
                        FCConvert.ToInt16(cmbWS.Text == "Sewer"), 0 != chkDuplicateLabels.CheckState);
				}
				else if (strMeterReportType == "S")
				{
					frmBookChoice.InstancePtr.Init(10, 0, false, "", "", cmbLong.SelectedIndex == 0, DateTime.Now,
                        false, cboLabelType.SelectedIndex, cmbLabelLocation.SelectedIndex == 0, strOrderBy,
                        FCConvert.ToInt16(cmbWS.Text == "Water"), FCConvert.ToInt16(cmbWS.Text == "Sewer"),
                        0 != chkDuplicateLabels.CheckState);
				}
				else
				{
					frmBookChoice.InstancePtr.Init(12, 0, false, "", "", true, DateTime.Now, false,
                        cboLabelType.ItemData(cboLabelType.SelectedIndex), cmbLabelLocation.SelectedIndex == 0,
                        strOrderBy, FCConvert.ToInt16(cmbWS.Text == "Water"), FCConvert.ToInt16(cmbWS.Text == "Sewer"),
                        0 != chkDuplicateLabels.CheckState);
				}
			}
			else
            {
                // else print the report

                switch (strMeterReportType)
                {
                    case "E":
                        rptMeterExchangeList.InstancePtr.Init("A", ref strOrderBy, DateAndTime.DateValue(txtSetDate.Text),
                                                              chkShowNoMeterSetDate.CheckState == Wisej.Web.CheckState.Checked, 0, ref lngFake);

                        break;
                    case "S":
                    {
                        Information.Err().Clear();
                        
                       if (cmbLong.SelectedIndex == 0)
                        {
                            if (modPrinterFunctions.PrintXsForAlignment("The X should have printed on the K of the line titled:" + "\r\n" + "FROM PREVIOUS BOOK", 36, 1, strPrinter) == DialogResult.Cancel)
                            {
                                return;
                            }

                            rptShortMeterReadingSlip.InstancePtr.Init("A", ref strOrderBy, ref strPrinter, 0, ref lngFake);
                        }
                        else
                        {
                            if (modPrinterFunctions.PrintXsForAlignment("The Xs should have printed with the middle X on the line of the consumption field.", 30, 3, strPrinter) == DialogResult.Cancel)
                            {
                                return;
                            }

                            rptLongMeterReadingSlip.InstancePtr.Init("A", ref strOrderBy, ref strPrinter, 0, ref lngFake);
                        }

                        break;
                    }
                    default:
                    {
                        // build sql statement to pass to report
                        BuildSQL();
                        // find printer font for printer we selected
                        NumFonts = FCGlobal.Printer.FontCount;
                        intCPI = 10;
                        for (X = 0; X <= NumFonts - 1; X++)
                        {
                            strFont = FCGlobal.Printer.Fonts[X];
                            ;
                            if (Strings.UCase(Strings.Right(strFont, 3)) == "CPI")
                            {
                                strFont = Strings.Mid(strFont, 1, strFont.Length - 3);
                                if (Conversion.Val(Strings.Right(strFont, 2)) == intCPI || Conversion.Val(Strings.Right(strFont, 3)) == intCPI)
                                {
                                    boolUseFont = true;
                                    strFont = FCGlobal.Printer.Fonts[X];
                                    break;
                                }
                            }
                        }
                        // X
                        // SHOW THE REPORT
                        if (!boolUseFont)
                            strFont = "";
                        if (cmbLabelLocation.Text == "Address")
                        {
                            rptAccountLabels.InstancePtr.Init(ref strSQL, "ADDRESSLABELS", cboLabelType.ItemData(cboLabelType.SelectedIndex), ref strPrinter, ref strFont);
                        }
                        else
                        {
                            rptAccountLabels.InstancePtr.Init(ref strSQL, "LOCATIONLABELS", cboLabelType.ItemData(cboLabelType.SelectedIndex), ref strPrinter, ref strFont);
                        }

                        break;
                    }
                }
            }
			this.Unload();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			// if all books are chosen then the order by option is the account number
            if (!cmbName.Items.Contains("Sequence #") && cmbName.Items.Contains("Account")) 
                return;

            cmbName.Clear();
            cmbName.Items.Add("Name");
            cmbName.Items.Add("Location");
            cmbName.Items.Add("Account");
            cmbName.Items.Add("Serial Number");
            cmbName.Text = "Name";
        }

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			// if we are only selecting certain books then the order by option changes to sequence number of the meter
			if (strMeterReportType == "L")
			{
				//optAccount.Text = "Account";
                if (!cmbName.Items.Contains("Sequence #") && cmbName.Items.Contains("Account")) 
                    return;

                cmbName.Clear();
                cmbName.Items.Add("Name");
                cmbName.Items.Add("Location");
                cmbName.Items.Add("Account");
                cmbName.Items.Add("Serial Number");
                cmbName.Text = "Name";
            }
			else
			{
				//optAccount.Text = "Sequence #";
				if (!cmbName.Items.Contains("Sequence #") || cmbName.Items.Contains("Account"))
				{
					cmbName.Clear();
					cmbName.Items.Add("Name");
					cmbName.Items.Add("Location");
					cmbName.Items.Add("Sequence #");
					cmbName.Items.Add("Serial Number");
					cmbName.Text = "Name";
				}
			}
		}

		private void BuildSQL()
		{
			string strWS = "";
			string strWS2 = "";
			string strOrderBySQL = "";
			if (strMeterReportType != "E" && strMeterReportType != "S")
			{
				if (cmbWS.Text == "Sewer")
				{
					strWS = " AND ISNULL(Deleted,0) = 0 AND Left(Service,1) <> 'W'";
					strWS2 = " WHERE ISNULL(Deleted,0) = 0 AND Left(Service,1) <> 'W'";
				}
				else if (cmbWS.Text == "Water")
				{
					strWS = " AND ISNULL(Deleted,0) = 0 AND Left(Service,1) <> 'S'";
					strWS2 = " WHERE ISNULL(Deleted,0) = 0 AND Left(Service,1) <> 'S'";
				}
				else
				{
					strWS = " AND ISNULL(Deleted,0) = 0";
					strWS2 = " WHERE ISNULL(Deleted,0) = 0";
				}
			}
			else
			{
				strWS = "";
				strWS2 = "";
			}
			// build SQL statement for account labels
			if (cmbName.Text == "Name")
			{
				strOrderBySQL = " ORDER BY Name";
			}
			else if (cmbName.Text == "Location")
			{
				strOrderBySQL = " ORDER BY StreetName, StreetNumber";
				// ElseIf optSelected Then
				// strOrderBySQL = " ORDER BY UTBook, AccountNumber"
			}
			else
			{
				strOrderBySQL = " ORDER BY AccountNumber";
			}
			if (chkDuplicateLabels.CheckState == Wisej.Web.CheckState.Unchecked)
			{
				// kgk        strSQL = "SELECT * FROM Master WHERE ID IN (SELECT DISTINCT AccountKey FROM MeterTable" & strWS2 & ")" & strOrderBySQL
				strSQL =
                    "SELECT AccountNumber, StreetName, StreetNumber, Apt, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip FROM Master INNER JOIN " +
                    modMain.Statics.strDbCP +
                    "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID WHERE Master.ID IN (SELECT DISTINCT AccountKey FROM MeterTable" +
                    strWS2 + ")" + strOrderBySQL;
			}
			else
			{
				// kgk        strSQL = "SELECT AccountNumber, Name, StreetName, StreetNumber, Apt, BAddress1, BAddress2, BAddress3, BCity, BState, BZip, BZip4 FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID)" & strWS2 & strOrderBySQL
				strSQL =
                    "SELECT AccountNumber, StreetName, StreetNumber, Apt, pBill.FullNameLF AS Name, pBill.Address1 AS BAddress1, pBill.Address2 AS BAddress2, pBill.Address3 AS BAddress3, pBill.City AS BCity, pBill.State AS BState, pBill.Zip AS BZip FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " +
                    modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.ID = Master.BillingPartyID)" +
                    strWS2 + strOrderBySQL;
			}
		}

		private void FillLabelTypeCombo()
		{
			int counter;
			// hide labels that are not supported by BD because of the 5 lines I need to print for an address
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPE5066)
				{
					labLabelTypes.Set_Visible(counter, false);
					break;
				}
			}
			// fill combo box with all available types of labels
			for (counter = 0; counter <= labLabelTypes.TypeCount - 1; counter++)
			{
				if (labLabelTypes.Get_Visible(counter))
				{
					cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
					cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
				}
				else
				{
					if (labLabelTypes.Get_ID(counter) == modLabels.CNSTLBLTYPEDYMO30256)
					{
						cboLabelType.AddItem(labLabelTypes.Get_Caption(counter));
						cboLabelType.ItemData(cboLabelType.NewIndex, labLabelTypes.Get_ID(counter));
					}
				}
			}
		}

		private void cmbAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (cmbAll.Text == "All")
			{
				optAll_CheckedChanged(sender, e);
			}
			else if (cmbAll.Text == "Selected Books")
			{
				optSelected_CheckedChanged(sender, e);
			}
		}
	}
}
