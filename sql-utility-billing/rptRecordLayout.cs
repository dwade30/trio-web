﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptRecordLayout.
	/// </summary>
	public partial class rptRecordLayout : BaseSectionReport
	{
		public rptRecordLayout()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			//FC:FINAL:MSH - I.Issue #969: restoring correct title of the report
			this.Name = "Account Extract Record Layout";
		}

		public static rptRecordLayout InstancePtr
		{
			get
			{
				return (rptRecordLayout)Sys.GetInstance(typeof(rptRecordLayout));
			}
		}

		protected rptRecordLayout _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRecordLayout	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		string[] strDescriptions = null;
		int intField;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				intField += 1;
				if (intField > Information.UBound(strDescriptions, 1))
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label2 As Variant --> As string
			// vbPorter upgrade warning: Label3 As Variant --> As string
			// vbPorter upgrade warning: Label7 As Variant --> As string
			//string Label2, Label3, Label7;  // - "AutoDim"
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			PageCounter = 0;
			intField = 0;
			//FC:FINAL:MSH - I.Issue #969: incorrect converting from VB6 and for setting value need to use Text property
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm tt");
			blnFirstRecord = true;
			strDescriptions = Strings.Split(frmCreateDatabaseExtract.InstancePtr.strTitles, ",", -1, CompareConstants.vbBinaryCompare);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lblFieldNumber As Variant --> As string
			// vbPorter upgrade warning: fldFieldDescription As Variant --> As string
			//string lblFieldNumber, fldFieldDescription; // - "AutoDim"
			//FC:FINAL:MSH - I.Issue #969: incorrect converting from VB6 and for setting value need to use Text property
			lblFieldNumber.Text = "Field " + FCConvert.ToString(intField + 1);
			fldFieldDescription.Text = Strings.Trim(strDescriptions[intField]);
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As Variant --> As string
			//string Label4;  // - "AutoDim"
			//FC:FINAL:MSH - I.Issue #969: incorrect converting from VB6 and for setting value need to use Text property
			PageCounter += 1;
			Label4.Text = "Page " + FCConvert.ToString(PageCounter);
		}

		private void rptRecordLayout_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptRecordLayout properties;
			//rptRecordLayout.Caption	= "Account Extract Record Layout";
			//rptRecordLayout.Icon	= "rptRecordLayout.dsx":0000";
			//rptRecordLayout.Left	= 0;
			//rptRecordLayout.Top	= 0;
			//rptRecordLayout.Width	= 11880;
			//rptRecordLayout.Height	= 8595;
			//rptRecordLayout.StartUpPosition	= 3;
			//rptRecordLayout.SectionData	= "rptRecordLayout.dsx":058A;
			//End Unmaped Properties
		}
	}
}
