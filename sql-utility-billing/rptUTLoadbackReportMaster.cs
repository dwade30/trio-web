﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptLoadbackReportMaster.
	/// </summary>
	public partial class rptLoadbackReportMaster : BaseSectionReport
	{
		public rptLoadbackReportMaster()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Account Detail";
		}

		public static rptLoadbackReportMaster InstancePtr
		{
			get
			{
				return (rptLoadbackReportMaster)Sys.GetInstance(typeof(rptLoadbackReportMaster));
			}
		}

		protected rptLoadbackReportMaster _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptLoadbackReportMaster	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               06/08/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               06/08/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		int lngNumOfAccts;
		bool boolFirstPass;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (boolFirstPass)
			{
				boolFirstPass = false;
			}
			else
			{
				eArgs.EOF = !boolFirstPass;
			}
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
			// catch the esc key and unload the report
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			boolFirstPass = true;
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
			rsData.OpenRecordset("SELECT * FROM Bill WHERE ISNULL(LoadBack,0) = 1 ORDER BY ActualAccountNumber", modExtraModules.strUTDatabase);
			if (rsData.EndOfFile())
			{
				MessageBox.Show("There are no accounts that have been loaded back.", "No Accounts Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
				this.Close();
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				if (!rsData.EndOfFile())
				{
					if (modUTStatusPayments.Statics.TownService != "S")
					{
						// this should set the two sub reports
						srptNonLienObW.Report = new srptLoadbackReport();
						srptNonLienObW.Report.UserData = "WN";
						srptLienObW.Report = new srptLoadbackReport();
						srptLienObW.Report.UserData = "WL";
					}
					if (modUTStatusPayments.Statics.TownService != "W")
					{
						// this should set the two sub reports
						srptNonLienObS.Report = new srptLoadbackReport();
						srptNonLienObS.Report.UserData = "SN";
						srptLienObS.Report = new srptLoadbackReport();
						srptLienObS.Report.UserData = "SL";
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Detail Format Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void rptLoadbackReportMaster_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptLoadbackReportMaster properties;
			//rptLoadbackReportMaster.Caption	= "Account Detail";
			//rptLoadbackReportMaster.Icon	= "rptUTLoadbackReportMaster.dsx":0000";
			//rptLoadbackReportMaster.Left	= 0;
			//rptLoadbackReportMaster.Top	= 0;
			//rptLoadbackReportMaster.Width	= 11880;
			//rptLoadbackReportMaster.Height	= 8595;
			//rptLoadbackReportMaster.StartUpPosition	= 3;
			//rptLoadbackReportMaster.SectionData	= "rptUTLoadbackReportMaster.dsx":058A;
			//End Unmaped Properties
		}
	}
}
