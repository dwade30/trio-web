//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class clsACHBatchControlRecord
	{
		private string strCompanyID = "";
		private string strMessageAuthenticationCode = "";

		public string RecordCode
		{
			get
			{
					string RecordCode = "";
				RecordCode = "8";
				return RecordCode;
			}
		}

        public string LastError { get; set; } = "";

        public bool Balanced { get; set; } = false;
		
        public int EntryAddendaCount { get; set; } = 0;
		
        public double Hash { get; set; } = 0;
		
        public double TotalDebit { get; set; } = 0;
		
        public double TotalCredit { get; set; } = 0;
		
        public string CompanyID { get; set; } = "";
		
        public string MessageAuthenticationCode { get; set; } = "";

        public string OriginatingDFI { get; set; } = "";
		
        public int BatchNumber { get; set; } = 0;

        public string ACHCompanyIDPrefix { get; set; } = "1";
		
		public string ServiceClassCode
		{
			get
			{
					string ServiceClassCode = "";
				if (Balanced) {
					ServiceClassCode = "200";
				} else {
					ServiceClassCode = "220";
				}
				return ServiceClassCode;
			}
		}

		public clsACHBatchControlRecord() : base()
		{

		}

		public string OutputLine()
		{
			string OutputLine = "";
			try
			{	// On Error GoTo ErrorHandler
				string strLine;
				LastError = "";
				strLine = RecordCode;
				strLine += ServiceClassCode;
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(6, "0")+FCConvert.ToString(EntryAddendaCount), 6);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(10, "0")+FCConvert.ToString(Hash), 10);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(12, "0")+FCConvert.ToString(TotalDebit*100), 12);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(12, "0")+FCConvert.ToString(TotalCredit*100), 12);
				strLine += fecherFoundation.Strings.Right(ACHCompanyIDPrefix+strCompanyID, 10);
				strLine += fecherFoundation.Strings.Left(strMessageAuthenticationCode+fecherFoundation.Strings.StrDup(19, " "), 19);
				strLine += fecherFoundation.Strings.StrDup(6, " "); // reserved
				strLine += fecherFoundation.Strings.Left(OriginatingDFI+fecherFoundation.Strings.StrDup(8, "0"), 8);
				strLine += fecherFoundation.Strings.Right(fecherFoundation.Strings.StrDup(7, "0")+FCConvert.ToString(BatchNumber), 7);
				OutputLine = strLine;
				return OutputLine;
			}
			catch (Exception ex)
			{	// ErrorHandler:
				LastError = "Could not build batch header record."+"\n"+ ex.Message;
			}
			return OutputLine;
		}


	}
}
