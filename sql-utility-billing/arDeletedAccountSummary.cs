﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for arDeletedAccountSummary.
	/// </summary>
	public partial class arDeletedAccountSummary : BaseSectionReport
	{
		public arDeletedAccountSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Deleted Accounts Summary";
		}

		public static arDeletedAccountSummary InstancePtr
		{
			get
			{
				return (arDeletedAccountSummary)Sys.GetInstance(typeof(arDeletedAccountSummary));
			}
		}

		protected arDeletedAccountSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	arDeletedAccountSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               03/19/2003              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/17/2005              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();

		public void Init()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// kgk    rsData.OpenRecordset "SELECT * FROM Master WHERE Deleted = TRUE ORDER BY AccountNumber", strUTDatabase
				rsData.OpenRecordset("SELECT AccountNumber, pOwn.FullNameLF AS OwnerName, pOwn.Address1 AS OAddress1, pOwn.Address2 AS OAddress2, pOwn.Address3 AS Address3, pOwn.City AS City, pOwn.State AS OState, pOwn.Zip AS OZip " + "FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pOwn ON pOwn.ID = Master.OwnerPartyID WHERE ISNULL(Deleted,0) = 1 ORDER BY AccountNumber", modExtraModules.strUTDatabase);
				if (!rsData.EndOfFile())
				{
					frmReportViewer.InstancePtr.Init(this);
				}
				else
				{
					MessageBox.Show("No deleted records found.", "No Records", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					this.Close();
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			FillHeaderLabels();
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			if (!rsData.EndOfFile())
			{
				BindFields();
				rsData.MoveNext();
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
			fldAcctNum.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
			// TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
			fldName.Text = FCConvert.ToString(rsData.Get_Fields("OwnerName"));
			fldAddress1.Text = rsData.Get_Fields_String("OAddress1") + " " + rsData.Get_Fields_String("OAddress2") + " " + rsData.Get_Fields_String("OAddress3") + " " + rsData.Get_Fields_String("OCity") + " " + rsData.Get_Fields_String("OState") + " " + rsData.Get_Fields_String("OZip");
		}

		private void arDeletedAccountSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//arDeletedAccountSummary properties;
			//arDeletedAccountSummary.Caption	= "Deleted Accounts Summary";
			//arDeletedAccountSummary.Icon	= "arDeletedAccountSummary.dsx":0000";
			//arDeletedAccountSummary.Left	= 0;
			//arDeletedAccountSummary.Top	= 0;
			//arDeletedAccountSummary.Width	= 11250;
			//arDeletedAccountSummary.Height	= 6795;
			//arDeletedAccountSummary.WindowState	= 2;
			//arDeletedAccountSummary.SectionData	= "arDeletedAccountSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
