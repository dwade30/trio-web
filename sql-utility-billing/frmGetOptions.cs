﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using System;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmGetOptions.
	/// </summary>
	public partial class frmGetOptions : BaseForm
	{
		public frmGetOptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Corey          07/20/2004
		// Modified Date
		// Jim Bertolino  11/17/2006
		// ********************************************************
		int lngReturn;

		public int Init(short intDefaultChoice = 1, string strTitle = "", string strDescription = "", string strOption1 = "", string strOption2 = "", string strOption3 = "", string strOption4 = "", string strOption5 = "", string strOption6 = "")
		{
			int Init = 0;
			// returns negative if cancelled
			lngReturn = -1;
			Init = lngReturn;
			lblDescription.Text = strDescription;
			this.Text = strTitle;
			//Option1[0].Text = strOption1;
			//Option1[1].Text = strOption2;
			//Option1[2].Text = strOption3;
			//Option1[3].Text = strOption4;
			//Option1[4].Text = strOption5;
			//Option1[5].Text = strOption6;
			cmbOption1.Items.Add(strOption6);
			if (strOption1 != string.Empty)
			{
				cmbOption1.Items.Add(strOption1);
			}
			if (strOption2 != string.Empty)
			{
				cmbOption1.Items.Add(strOption2);
			}
			if (strOption3 != string.Empty)
			{
				cmbOption1.Items.Add(strOption3);
			}
			if (strOption4 != string.Empty)
			{
				cmbOption1.Items.Add(strOption4);
			}
			if (strOption5 != string.Empty)
			{
				cmbOption1.Items.Add(strOption5);
			}
			if (strOption6 != string.Empty)
			{
				cmbOption1.Items.Add(strOption6);
			}
			// set default
			if (cmbOption1.Items.Count >= intDefaultChoice - 1)
			{
				cmbOption1.SelectedIndex = intDefaultChoice - 1;
			}
			else
			{
				int x;
				for (x = 0; x <= 5; x++)
				{
					if (cmbOption1.Items.Count >= x)
					{
						cmbOption1.SelectedIndex = x;
						break;
					}
				}
				// x
			}
			this.Show(FCForm.FormShowEnum.Modal);
			Init = lngReturn;
			return Init;
		}

		private void cmdCancel_Click(object sender, System.EventArgs e)
		{
			lngReturn = -1;
			this.Unload();
		}

		private void cmdOK_Click(object sender, System.EventArgs e)
		{
			mnuSaveContinue_Click();
		}

		private void frmGetOptions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			lngReturn = -1;
			this.Unload();
		}

		public void mnuExit_Click()
		{
			mnuExit_Click(mnuExit, new System.EventArgs());
		}

		private void mnuSaveContinue_Click(object sender, System.EventArgs e)
		{
			int X;
			for (X = 0; X <= 5; X++)
			{
				if (cmbOption1.Items.Count >= X)
				{
					lngReturn = X + 1;
					break;
				}
			}
			// X
			this.Unload();
		}

		public void mnuSaveContinue_Click()
		{
			mnuSaveContinue_Click(mnuSaveContinue, new System.EventArgs());
		}

		private void frmGetOptions_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmGetOptions properties;
			//frmGetOptions.FillStyle	= 0;
			//frmGetOptions.ScaleWidth	= 5880;
			//frmGetOptions.ScaleHeight	= 4050;
			//frmGetOptions.LinkTopic	= "Form2";
			//frmGetOptions.LockControls	= true;
			//frmGetOptions.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			// Dim strX            As String
			// Dim X               As New clsSomeDll
			// 
			// strX = "1234567890"
			// strX = strX
			// Set X = New clsSomeDll
			// 
			// strX = X.ReverseString(strX)
			// 
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
		}
	}
}
