﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using GrapeCity.ActiveReports.Document;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Document.Section;
using Microsoft.VisualBasic.ApplicationServices;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;
using Page = Wisej.Web.Page;

namespace TWUT0000
{
    /// <summary>
    /// Summary description for arUTStatusLists.
    /// </summary>
    public partial class arUTStatusLists : BaseSectionReport
    {
        private const string CURRENCY_FORMAT = "#,##0.00";
        private const string CURRENCY_ZERO_VALUE = "0.00";
        private string reportName = "UTStatusList";

        public arUTStatusLists()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
            ReportIdentifier = reportName;
            EnablePageDiskCache = true;
        }

        private void InitializeComponentEx()
        {
            if (_InstancePtr == null)
                _InstancePtr = this;
            Name = "Status Lists";
        }

        public static arUTStatusLists InstancePtr
        {
            get
            {
                return (arUTStatusLists)Sys.GetInstance(typeof(arUTStatusLists));
            }
        }

        protected arUTStatusLists _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (_InstancePtr == this)
            {
                _InstancePtr = null;
                Sys.ClearInstance(this);
            }
            if (disposing)
            {
                rsCalLien.Dispose();
                rsData.Dispose();
            }
            base.Dispose(disposing);
        }
        // nObj = 1
        //   0	arUTStatusLists	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
        string strSQL;
        clsDRWrapper rsData = new clsDRWrapper();
        double[] dblTotals = new double[9 + 1];
        // 0 - Tax Due, 1 - Payment Received, 2 - Total Adjustment, 3 - Total Due, 4 - Total Abatement, 5 - Principal, 6 - Tax, 7 - Interest, 8 - Costs
        double[] dblGroupTotals = new double[9 + 1];
        // 0 - Tax Due, 1 - Payment Received, 2 - Total Adjustment, 3 - Total Due, 4 - Total Abatement, 5 - Principal, 6 - Tax, 7 - Interest, 8 - Costs
        bool boolRTError;
        int lngCount;
        // Dim boolStarted                 As Boolean
        clsDRWrapper rsCalLien = new clsDRWrapper();
        double dblSpecialTotal;
        double dblPDTotal;
        bool boolAdjustedSummary;
        bool boolLienRec;
        bool boolSummaryOnly;
        float lngExtraRows;
        bool boolWater;
        string strWS = string.Empty;
        bool boolShowLien;
        bool boolShowRegular;
        bool boolFirstRecord;
        bool boolGroupTotals;
        bool boolGroupByBook;
        int lngLastAccount;
        string strLastName = string.Empty;
        bool boolFix;
        bool boolPrincipal;
        bool boolInterest;
        bool boolTaxes;
        bool boolCosts;
        string strPaymentService = string.Empty;
        int intTestCT;
        Dictionary<object, object> objDictionary = new Dictionary<object, object>();
        Dictionary<object, object> objLienDictionary = new Dictionary<object, object>();
        // these are for the summaries in the report footer
        Dictionary<int, double> dcYearTotals = new Dictionary<int, double>();
        double[] dblYearTotals = new double[4000 + 1];
        // billingyear - 19800
        double[,] dblPayments = new double[11 + 1, 6 + 1];
        // 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total, 11 - Current Interest
        // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs, 4 - Total, 5 - Tax
        double[,] dblBPayments = new double[11 + 1, 6 + 1];
        // 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total, 11 - Current Interest
        // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs, 4 - Total, 5 - Tax
        double[,] dblPaymentsRev = new double[11 + 1, 6 + 1];
        // 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total, 11 - Current Interest
        // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs, 4 - Total, 5 - Tax
        double[,] dblBPaymentsRev = new double[11 + 1, 6 + 1];
        // 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - U, 8 - X, 9 - Y, 10 - Total, 11 - Current Interest
        // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs, 4 - Total, 5 - Tax

        //public SectionDocument Doc;
        //int docs_count = 0;
        int counter = 0;

        private void ActiveReport_DataInitialize(object sender, EventArgs e)
        {
            Fields.Add("Binder");
        }

        private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
        {
            eArgs.EOF = rsData.EndOfFile();
        }

        private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
        {
            switch (KeyCode)
            {
                case Keys.Escape:
                    {
                        this.Close();
                        break;
                    }
            }
            //end switch
        }

        private void ActiveReport_PageEnd(object sender, EventArgs e)
        {
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;

            // large report chunking for better memory mgt and performance
            //if (this.Document.Pages.Count > 52)  
            //{  
            //    Doc = new SectionDocument();  
            //    for (int j = 0; j < 50; j++)  
            //    {  
            //        Doc.Pages.Add(this.Document.Pages[0].Clone() as GrapeCity.ActiveReports.Document.Section.Page);  
            //        this.Document.Pages.RemoveAt(0);  
            //    }  
            //    Doc.Save($"{Application.CommonAppDataPath}\\{reportName}_{docs_count}_{Application.SessionId}.ddd");  
            //    docs_count++;  
            //    Doc.Pages.Clear();  
            //    Doc.Dispose();  
            //}  
        }

        private void ActiveReport_PageStart(object sender, EventArgs e)
        {
            lblPage.Text = "Page " + PageNumber;
        }

        private void ActiveReport_ReportEnd(object sender, EventArgs e)
        {
            //SectionDocument DocTemp = new SectionDocument();
            //Doc = new SectionDocument();

            //try
            //{
            //    for (int j = 0; j < docs_count; j++)
            //    {
            //        var intermediateReportName = $"{Application.CommonAppDataPath}\\{reportName}_{j}_{Application.SessionId}.ddd";
            //        DocTemp.Load(intermediateReportName);
            //        Doc.Pages.AddRange(DocTemp.Pages.Clone() as PagesCollection);
            //        DocTemp.Pages.Clear();
            //        File.Delete(intermediateReportName);
            //    }

            //    foreach (GrapeCity.ActiveReports.Document.Section.Page p in this.Document.Pages)
            //    {
            //        Doc.Pages.Add(p.Clone() as GrapeCity.ActiveReports.Document.Section.Page);
            //    }

            //    this.Document.Pages.Clear();
            //    this.Document.Pages.AddRange(Doc.Pages);
            //}
            //catch (Exception ex)
            //{
            //    this.Document.Pages.Add(new GrapeCity.ActiveReports.Document.Section.Page() {CanvasItems = {ex.Message}});
            //}
            //finally
            //{
            //    DocTemp.Dispose();
            //    frmWait.InstancePtr.Unload();
                rsData.DisposeOf();
                rsCalLien.DisposeOf();
            //}
            
        }

        private void ActiveReport_ReportStart(object sender, EventArgs e)
        {
            try
            {
                //FC:FINAL:SBE - add controls at report start. Moved code from Format event
                GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
                obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew.Name = "fldSummaryTotal";
                ReportFooter.Controls.Add(obNew);
                GrapeCity.ActiveReports.SectionReportModel.Label obLabel;
                obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
                obLabel.Name = "lblPerDiemTotal";
                ReportFooter.Controls.Add(obLabel);
                GrapeCity.ActiveReports.SectionReportModel.Line obLine;
                obLine = new GrapeCity.ActiveReports.SectionReportModel.Line();
                obLine.Name = "lnFooterSummaryTotal";
                ReportFooter.Controls.Add(obLine);
                // On Error GoTo ERROR_HANDLER
                lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
                lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
                lblMuniName.Text = modGlobalConstants.Statics.MuniName;
                lngCount = 0;
                boolFix = false;
                boolWater = FCConvert.CBool(Strings.UCase(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowWS, 1)) == "WATER");
                // kk09152016 trouts-196   Change Water to Stormwater for Bangor
                if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
                {
                    boolWater = FCConvert.CBool(Strings.UCase(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowWS, 1)) == "STORMWATER");
                }
                boolPrincipal = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[0].CheckState == Wisej.Web.CheckState.Checked);
                boolTaxes = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[1].CheckState == Wisej.Web.CheckState.Checked);
                boolInterest = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[2].CheckState == Wisej.Web.CheckState.Checked);
                boolCosts = FCConvert.CBool(frmUTStatusList.InstancePtr.chkInclude[3].CheckState == Wisej.Web.CheckState.Checked);
                strWS = boolWater ? "W" : "S";
                srptSLAllActivityDetailOB.Visible = frmUTStatusList.InstancePtr.chkShowPayments.CheckState == Wisej.Web.CheckState.Checked;
                boolFirstRecord = true;
                boolShowLien = FCConvert.CBool(Strings.Left(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowShowLien, 1), 1) != "R");
                boolShowRegular = FCConvert.CBool(Strings.Left(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowShowLien, 1), 1) != "L");
                if (frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowGroupBy, 1) != "")
                {
                    boolGroupTotals = true;
                    boolGroupByBook = Strings.Left(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowGroupBy, 1), 1) != "A";
                }
                else
                {
                    boolGroupTotals = false;
                    // GroupHeader1.Visible = False
                    GroupFooter1.Visible = false;
                }
                boolRTError = false;
                boolSummaryOnly = FCConvert.CBool(frmUTStatusList.InstancePtr.chkSummaryOnly.CheckState == Wisej.Web.CheckState.Checked);
                SetupFields();
                // Moves/shows the correct fields into the right places
                SetReportHeader();
                // Sets the titles and moves labels in the header
                // boolStarted = False
                strSQL = BuildSQL();
                // Generates the SQL String
                rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                Document.Printer.DefaultPageSettings.Landscape = true;
                lblOutstanding.Text = "-    -    -    Outstanding    -    -    -";
                if (rsData.EndOfFile() && rsData.BeginningOfFile())
                {
                    NoRecords();
                }
                else
                {
                    // open the lien table to be used later
                    rsCalLien.OpenRecordset("SELECT * FROM Lien", modExtraModules.strUTDatabase);
                }
                string strQry = string.Empty;
                if (Strings.Trim(frmUTStatusList.InstancePtr.strRSWhere) != "")
                {
                    strQry = "SELECT COUNT(BNum) as FieldsCount FROM(SELECT DISTINCT(BillNumber) AS BNum FROM Bill INNER JOIN Master m ON Bill.AccountKey = m.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID WHERE (Service = 'B' OR Service = '" + strWS + "') AND " + frmUTStatusList.InstancePtr.strRSWhere + ") AS qTmp";
                }
                else
                {
                    //FC:FINAL:MSH - issue #946: incorrect SQL syntax
                    //strQry = "SELECT COUNT(BNum) as FieldsCount FROM(SELECT DISTINCT(BillNumber) AS BNum FROM Bill INNER JOIN Master m ON Bill.AccountKey = m.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID WHERE (Service = 'B' OR Service = '" + strWS + "') AND " + frmUTStatusList.InstancePtr.strRSWhere + ") AS qTmp";
                    strQry = "SELECT COUNT(BNum) as FieldsCount FROM(SELECT DISTINCT(BillNumber) AS BNum FROM Bill INNER JOIN Master m ON Bill.AccountKey = m.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID WHERE (Service = 'B' OR Service = '" + strWS + "')) AS qTmp";
                }
                var fieldsCountReader = new clsDRWrapper();

                try
                {
                    int fieldsCount = 0;
                    fieldsCountReader.OpenRecordset(strQry);
                    if (fieldsCountReader.RecordCount() > 0)
                    {
                        // TODO Get_Fields: Field [FieldsCount] not found!! (maybe it is an alias?)
                        fieldsCount = fieldsCountReader.Get_Fields("FieldsCount");
                    }
                    for (int i = 2; i <= fieldsCount; i++)
                    {
                        GrapeCity.ActiveReports.SectionReportModel.TextBox obTextNew;
                        obTextNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                        obTextNew.Name = "fldSummary" + i.ToString();
                        ;
                        obTextNew.Visible = false;
                        ReportFooter.Controls.Add(obTextNew);
                        GrapeCity.ActiveReports.SectionReportModel.Label obLabelNew;
                        obLabelNew = new GrapeCity.ActiveReports.SectionReportModel.Label();
                        obLabelNew.Name = "lblSummary" + i.ToString();
                        obLabelNew.Visible = false;
                        ReportFooter.Controls.Add(obLabelNew);
                    }
                }
                finally
                {
                    fieldsCountReader.DisposeOf();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void NoRecords()
        {
            // this will terminate the report and show a message to the user
            MessageBox.Show("There are no records matching the criteria selected.", "Status List", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
            return;
        }

        private string BuildSQL()
        {
            string BuildSQL = string.Empty;
            string strTemp = string.Empty;
            string strWhereClause = string.Empty;
            string strUTInnerJoin = string.Empty;
            string strAsOfDateString = string.Empty;
            string strSelectFields = string.Empty;
            string strOrder = string.Empty;
            if (modMain.Statics.gboolUTUseAsOfDate)
            {
                strAsOfDateString = " AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'";
            }
            else
            {
                strAsOfDateString = string.Empty;
            }
            if ((Strings.UCase(modUTStatusList.Statics.strReportType) == "ABATE") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "SUPPLEMENTAL") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "DISCOUNT") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "TAXCLUB") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "COSTS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "REFUNDEDABATE") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "PREPAYMENT") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "PAYMENTS") || (Strings.UCase(modUTStatusList.Statics.strReportType) == "CORRECTIONS"))
            {
                // This is a regular report using the Where Criteria to report the correct information
                Debug.Assert(false);
                // These looks like RE CL fields XXXXXXXXXXXXXXXXX
                // is this branch of the Select ever executed?
                strSelectFields = "BillKey, Account, BillingType, BillingYear, Name1, Name2, Address1, Address2, Address3, ";
                strSelectFields += "MapLot, StreetNumber, Apt, StreetName, LandValue, BuildingValue, ExemptValue, ";
                strSelectFields += "TaxDue1, TaxDue2, TaxDue3, TaxDue4, LienRecordNumber, PrincipalPaid, InterestPaid, InterestCharged, ";
                strSelectFields += "DemandFees, DemandFeespaid, InterestAppliedThroughDate, RateKey, BookPage, PIAcres, RSName, RSSecOwner, ";
                strSelectFields += "RSAddr1, RSAddr2, RSAddr3, RSState, RSZip, RSZip4, RSLocNumAlph, RSLocStreet, RSRef1, RSMapLot, RSAccount, RSCard, ";
                strSelectFields += "BillCode, BK, Service";
            }
            else
            {
                // This is a regular report using the Where Criteria to report the correct information
                strUTInnerJoin = " INNER JOIN Master m ON Bill.AccountKey = m.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = m.BillingPartyID";
                strSelectFields =
                    "Bill.ID AS Bill, Bill.MapLot, StreetNumber, Apt, StreetName, BillNumber,BillingRateKey,AccountKey,ActualAccountNumber,BName,Bill.Book,BillNumber,Bill.MapLot," +
                    "SLienRecordNumber,SCombinationLienKey,WLienRecordNumber,WCombinationLienKey," +
                    "SPrinOwed,STaxOwed,SIntOwed,SIntAdded,SCostOwed,SCostAdded,SPrinPaid,STaxPaid,SIntPaid,SCostPaid,SIntPaidDate," +
                    "WPrinOwed,WTaxOwed,WIntOwed,WIntAdded,WCostOwed,WCostAdded,WPrinPaid,WTaxPaid,WIntPaid,WCostPaid,WIntPaidDate," +
                    "pBill.FullNameLF AS Name,UTBook,OName,DeedName1,DeedName2,OName2,SameBillOwner,SBillToOwner,WBillToOwner,sBillOwner,wBillOwner ";
                if (Strings.Trim(frmUTStatusList.InstancePtr.strRSOrder) != "" && Strings.InStr(2, frmUTStatusList.InstancePtr.strRSOrder, "ActualAccountNumber", CompareConstants.vbBinaryCompare) == 0)
                {
                    strOrder = frmUTStatusList.InstancePtr.strRSOrder + ",ActualAccountNumber";
                }
                else
                {
                    strOrder = frmUTStatusList.InstancePtr.strRSOrder;
                }
                if (Strings.Trim(frmUTStatusList.InstancePtr.strRSWhere) != "")
                {
                    if (Strings.Trim(strOrder) != "")
                    {
                        strTemp = "SELECT " + strSelectFields + " FROM (Bill" + strUTInnerJoin + ") INNER JOIN (" +
                                  frmUTStatusList.InstancePtr.strRSFrom +
                                  ") AS List ON Bill.ID = List.BK WHERE (Service = 'B' OR Service = '" + strWS +
                                  "') AND " + frmUTStatusList.InstancePtr.strRSWhere + " ORDER BY " + strOrder;
                    }
                    else
                    {
                        strTemp = "SELECT " + strSelectFields + " FROM (Bill" + strUTInnerJoin + ") INNER JOIN (" +
                                  frmUTStatusList.InstancePtr.strRSFrom +
                                  ") AS List ON Bill.ID = List.BK WHERE (Service = 'B' OR Service = '" + strWS +
                                  "') AND " + frmUTStatusList.InstancePtr.strRSWhere;
                    }
                }
                else
                {
                    if (Strings.Trim(strOrder) != "")
                    {
                        strTemp = "SELECT " + strSelectFields + " FROM (Bill" + strUTInnerJoin + ") INNER JOIN (" +
                                  frmUTStatusList.InstancePtr.strRSFrom +
                                  ") AS List ON Bill.ID = List.BK WHERE (Service = 'B' OR Service = '" + strWS +
                                  "') ORDER BY " + strOrder;
                    }
                    else
                    {
                        strTemp = "SELECT " + strSelectFields + " FROM (Bill" + strUTInnerJoin + ") INNER JOIN (" +
                                  frmUTStatusList.InstancePtr.strRSFrom +
                                  ") AS List ON Bill.ID = List.BK WHERE (Service = 'B' OR Service = '" + strWS + "') ";
                    }
                }
            }
            Debug.WriteLine(strTemp);
            BuildSQL = strTemp;
            return BuildSQL;
        }

        private void SetupFields()
        {
            // Set each field and label's visible property to True/False depending on which fields the user
            // has selected from the Custom Report screen then move them accordingly
            int intRow;
            int intRow2;
            float lngHt;
            if (boolSummaryOnly)
            {
                lblBillDate.Visible = false;
                fldBillDate.Visible = false;
                lblLocation.Visible = false;
                fldLocation.Visible = false;
                lblMapLot.Visible = false;
                fldMapLot.Visible = false;
                fldName.Visible = false;
                fldAccount.Visible = false;
                fldPaymentReceived.Visible = false;
                fldTaxDue.Visible = false;
                fldDue.Visible = false;
                lblAccount.Visible = false;
                lnHeader.Visible = false;
                lnTotals.Visible = false;
                lblBook.Visible = false;
                // MAL@20080801: Add Bill Number
                // Tracker Reference: 13826
                lblBillNum.Visible = false;
                fldBillNum.Visible = false;
                lblFakeHeader1.Visible = true;
                lblFakeHeader2.Visible = true;
                lblFakeHeader3.Visible = true;
                lblFakeHeader4.Visible = true;
                lblFakeHeader5.Visible = true;
                lblFakeHeader6.Visible = true;
                lblFakeHeader7.Visible = true;
                lblFakeHeader8.Visible = true;
                lblTotals.Visible = true;
                fldTotalTaxDue.Visible = true;
                fldTotalPaymentReceived.Visible = true;
                fldTotalDue.Visible = true;
                fldTotalPrincipal.Visible = true;
                fldTotalTax.Visible = true;
                fldTotalInterest.Visible = true;
                fldTotalCost.Visible = true;
                GroupHeader1.Height = 0;
                GroupHeader1.Visible = false;
                GroupFooter1.Height = 0;
                GroupFooter1.Visible = false;
                Detail.Height = 0;
                Detail.Visible = false;
                return;
            }
            intRow = 2;
            lngHt = 270 / 1440F;
            lblBillDate.Visible = true;
            fldBillDate.Visible = true;
            // MAL@20080801: Add Bill Number
            // Tracker Reference: 13826
            lblBillNum.Visible = true;
            fldBillNum.Visible = true;
            // Location
            // If Mid$(frmUTStatusList.strBinaryWhere, 1, 1) = 1 Or frmUTStatusList.boolShowLocation Then
            if (frmUTStatusList.InstancePtr.boolShowLocation)
            {
                lblLocation.Visible = true;
                fldLocation.Visible = true;
                intRow += 1;
            }
            else
            {
                lblLocation.Visible = false;
                fldLocation.Visible = false;
                fldLocation.Top = 0;
            }
            if (frmUTStatusList.InstancePtr.boolShowMapLot)
            {
                lblMapLot.Visible = true;
                fldMapLot.Visible = true;
                intRow += 1;
            }
            else
            {
                lblMapLot.Visible = false;
                fldMapLot.Visible = false;
                lblMapLot.Top = 0;
                fldMapLot.Top = 0;
            }
            // If intRow > 1 Then
            // Me.Detail.Height = 2 * lngHt + 100
            // Else
            Detail.Height = lngHt + 100 / 1440f;
            // End If
        }

        private void BindFields()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill the information into the fields
                clsDRWrapper rsPayment = new clsDRWrapper();
                // kk01092017  not using rsMaster since 9/19/2014 changes     Dim rsMaster                As New clsDRWrapper
                string strTemp = string.Empty;
                double dblTotalPayment;
                double dblTotalAbate;
                double dblTotalRefundAbate;
                double dblTotalPPayment;
                double dblTotalCorrection;
                double dblTaxClub;
                double dblCurInt;
                double dblLienCurInt;
                DateTime dtPaymentDate;
                bool boolMasterInfo;
                double dblDiscount;
                double dblPrePay;
                double dblSupplemental;
                double dblJustAbate;
                double dblPreLienIntNeed;
                double dblLienCostNeed;
                double dblPreLienIntPaid;
                double dblLienCostPaid;
                double dblInterestCharged;
                double dblInterestPaid = 0;
                double dblCurrentInterest;
                // this is the calculated current interest for each bill
                double dblDue;
                string strSLPayRange = string.Empty;
                double dblTest = 0;
                double dblTestInt = 0;
                double dblCostPaid;
                bool boolShowName;
                string strCode = string.Empty;
                double dblPPrin = 0;
                double dblPTax = 0;
                double dblPInt = 0;
                double dblPLInt = 0;
                double dblPCost = 0;
                double dblAmtDue = 0;
                double dblAmtDueP = 0;
                double dblAmtDueT = 0;
                double dblAmtDueI = 0;
                double dblAmtDueC = 0;
                bool boolShowOrig = false;
                bool blnIsPrepayment;
                bool blnIsAddtlLienRec;
                int lngBillKey = 0;
                int lngCheckAccount = 0;
                int lngLien = 0;
                DateTime datTADate;
                bool boolBillOwner = false;

            TRYNEXTACCOUNT:
                ;
                // DoEvents
                // reset fields and variables
                Debug.WriteLine("Start: " + "\t" + FCConvert.ToString(DateTime.Now));
                ClearFields();
                // reset the account payments
                FCUtils.EraseSafe(dblBPayments);
                rsPayment.Reset();
                dblTotalPayment = 0;
                dblTotalAbate = 0;
                dblTotalRefundAbate = 0;
                dblTotalPPayment = 0;
                dblTotalCorrection = 0;
                dblTaxClub = 0;
                dblCurInt = 0;
                dblLienCurInt = 0;
                dtPaymentDate = DateTime.Today;
                boolMasterInfo = false;
                dblDiscount = 0;
                dblPrePay = 0;
                dblSupplemental = 0;
                dblJustAbate = 0;
                dblPreLienIntNeed = 0;
                dblLienCostNeed = 0;
                dblPreLienIntPaid = 0;
                dblLienCostPaid = 0;
                dblCurrentInterest = 0;
                dblCostPaid = 0;
                boolShowName = true;
                blnIsPrepayment = false;
                blnIsAddtlLienRec = false;
                if (rsData.EndOfFile())
                {
                    srptSLAllActivityDetailOB.Visible = false;
                    return;
                }
                if (modMain.Statics.gboolUTSLDateRange)
                {
                    strSLPayRange = " AND (RecordedTransactionDate >= '" + FCConvert.ToString(modMain.Statics.gdtUTSLPaymentDate1) + "' AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTSLPaymentDate2) + "')";
                }
                if (boolWater)
                {
                    strPaymentService = " AND Service = 'W'";
                }
                else
                {
                    strPaymentService = " AND Service = 'S'";
                }
                // Debug.Assert rsData.Fields("BillNumber") <> 225
                if (frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowPaymentType, 1) != "")
                {
                    if (Strings.UCase(Strings.Left(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowPaymentType, 1), 2)) == "PA")
                    {
                        strCode = " AND CODE = 'P'";
                    }
                    else if (Strings.UCase(Strings.Left(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowPaymentType, 1), 2)) == "PR")
                    {
                        strCode = " AND CODE = 'Y'";
                    }
                    else if (Strings.UCase(Strings.Left(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowPaymentType, 1), 2)) == "LI")
                    {
                        strCode = " AND (CODE = 'L' OR Code = '3')";
                        // kgk 03-10-2011 trout-701  was  "... AND Code = '3'"
                    }
                    else if (Strings.UCase(Strings.Left(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowPaymentType, 1), 2)) == "CO")
                    {
                        strCode = " AND CODE = 'C'";
                    }
                    else if (Strings.UCase(Strings.Left(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowPaymentType, 1), 2)) == "AB")
                    {
                        strCode = " AND CODE = 'A'";
                    }
                    strCode += strPaymentService;
                    if (modMain.Statics.gboolUTUseAsOfDate)
                    {
                        if (Conversion.Val(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                        {
                            // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                            rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'");
                        }
                        else
                        {
                            rsCalLien.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
                            if (rsCalLien.Get_Fields_DateTime("DateCreated").ToOADate() <= modMain.Statics.gdtUTStatusListAsOfDate.ToOADate())
                            {
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 1 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'");
                            }
                            else
                            {
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'");
                            }
                        }
                    }
                    else
                    {
                        if (Conversion.Val(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                        {
                            // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                            rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0");
                        }
                        else
                        {
                            // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                            rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 1 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0");
                        }
                    }
                    if (rsPayment.EndOfFile())
                    {
                        // if there is no payment then go to the next account
                        rsData.MoveNext();
                        /*? 130 */
                        goto TRYNEXTACCOUNT;
                    }
                }
                else if (modMain.Statics.gboolUTSLDateRange)
                {
                    if (Conversion.Val(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                    {
                        // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                        rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + ")" + strSLPayRange + strPaymentService + " AND ReceiptNumber >= 0");
                    }
                    else
                    {
                        // MAL@20080918: Change to look at both the lien key and the bill key when the bill is liened
                        // Tracker Reference: 15394
                        // rsPayment.OpenRecordset "SELECT ID FROM PaymentRec WHERE (AccountKey = " & rsData.Fields("AccountKey") & " AND LIEN AND BillKey = " & rsData.Fields("Bill.bill") & ")" & strSLPayRange & strPaymentService & " AND ReceiptNumber >= 0"
                        if (modMain.Statics.gboolUTUseAsOfDate)
                        {
                            rsCalLien.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
                            if (rsCalLien.Get_Fields_DateTime("DateCreated").ToOADate() <= modMain.Statics.gdtUTStatusListAsOfDate.ToOADate())
                            {
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 1 AND (BillKey = " + rsData.Get_Fields("Bill") + "OR BillKey = " + rsData.Get_Fields(strWS + "LienRecordNumber") + "))" + strSLPayRange + strPaymentService + " AND ReceiptNumber >= 0");
                            }
                            else
                            {
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 1 AND BillKey = " + rsData.Get_Fields("Bill") + ")" + strSLPayRange + strPaymentService + " AND ReceiptNumber >= 0");
                            }
                        }
                        else
                        {
                            // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                            rsPayment.OpenRecordset("SELECT ID FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 1 AND (BillKey = " + rsData.Get_Fields("Bill") + "OR BillKey = " + rsData.Get_Fields(strWS + "LienRecordNumber") + "))" + strSLPayRange + strPaymentService + " AND ReceiptNumber >= 0");
                        }
                    }
                    if (rsPayment.EndOfFile())
                    {
                        // if there is no payment then go to the next account
                        rsData.MoveNext();
                        goto TRYNEXTACCOUNT;
                    }
                }
                fldTADate.Text = string.Empty;
                fldTADate.Visible = false;
                if (FCConvert.ToString(rsData.Get_Fields_String("Name")) != "")
                {
                    boolMasterInfo = true;
                    if (modUTStatusPayments.IsAccountTaxAcquired_7(rsData.Get_Fields_Int32("ActualAccountNumber")))
                    {
                        if (frmUTStatusList.InstancePtr.strTAStatus == "N")
                        {
                            rsData.MoveNext();
                            goto TRYNEXTACCOUNT;
                        }
                        fldTADate.Visible = true;
                        datTADate = modUTStatusPayments.GetTaxAcquiredDate(rsData.Get_Fields_Int32("ActualAccountNumber"));
                        if (datTADate.ToOADate() > 0)
                        {
                            fldTADate.Text = "TA : " + Strings.Format(datTADate, "MM/dd/yyyy");
                        }
                        else
                        {
                            fldTADate.Text = "TA";
                        }
                    }
                    else
                    {
                        if (frmUTStatusList.InstancePtr.strTAStatus == "Y")
                        {
                            rsData.MoveNext();
                            goto TRYNEXTACCOUNT;
                        }
                    }
                }
                else
                {
                    boolMasterInfo = false;
                }

                boolBillOwner = false;
                bool didBillOwner;
                if (boolWater)
                {
                    if (rsData.Get_Fields_Boolean("WBillToOwner"))
                    {
                        boolBillOwner = true;
                    }

                    didBillOwner = rsData.Get_Fields_Boolean("WBillOwner");
                }
                else
                {
                    if (rsData.Get_Fields_Boolean("SBillToOwner"))
                    {
                        boolBillOwner = true;
                    }
                    didBillOwner = rsData.Get_Fields_Boolean("SBillOwner");
                }

                string billedName;
                string currentName;
                string currentTenant = rsData.Get_Fields_String("Name");
                string currentOwner = rsData.Get_Fields_String("DeedName1");
                string billOwner = rsData.Get_Fields_String("OName");
                string billTenant = rsData.Get_Fields_String("BName");

                billedName = didBillOwner ? billOwner : billTenant;
                currentName = boolBillOwner ? currentOwner : currentTenant;

                if (boolMasterInfo && frmUTStatusList.InstancePtr.intShowOwnerType != 1)
                {
                    switch (frmUTStatusList.InstancePtr.intShowOwnerType)
                    {
                        case 0:
                            {
                                fldName.Text = currentOwner;
                                break;
                            }
                        case 2:
                            {
                                if (Strings.LCase(billedName) != Strings.LCase(currentName))
                                {
                                    fldName.Text = billedName + @", C\O " + currentOwner;
                                }
                                else
                                {
                                    fldName.Text = billedName;
                                }
                                break;
                            }
                        case 3:
                            {
                                if (Strings.LCase(currentName) != Strings.LCase(billedName))
                                {
                                    fldName.Text = currentName + @", Bill = " + billedName;
                                }
                                else
                                {
                                    fldName.Text = currentName;
                                }
                                break;
                            }
                    }
                    //end switch
                    // kk01092017  not using rsMaster since 9/19/2014 changes; Change the below to use rsData
                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) != "")
                    {
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        fldLocation.Text = Strings.Trim(rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
                    }
                    else
                    {
                        fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
                    }
                    fldMapLot.Text = Strings.Trim(rsData.Get_Fields_String("MapLot") + " ");
                }
                else
                {
                    fldName.Text = "*" + rsData.Get_Fields_String("BName");
                    fldMapLot.Text = Strings.Trim(rsData.Get_Fields_String("MapLot") + " ");

                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields("StreetNumber"))) != "")
                    {
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        fldLocation.Text = Strings.Trim(rsData.Get_Fields("StreetNumber") + " " + rsData.Get_Fields_String("StreetName"));
                    }
                    else
                    {
                        fldLocation.Text = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("StreetName")));
                    }
                    fldMapLot.Text = Strings.Trim(rsData.Get_Fields_String("MapLot") + " ");
                    // kk 06052013  rsData.Fields("Master.MapLot")
                }
                /*? 320 */
                fldAccount.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ActualAccountNumber"));
                if (Conversion.Val(rsData.Get_Fields_Int32("UTBook")) == 0)
                {
                    // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                    fldBook.Text = FCConvert.ToString(rsData.Get_Fields("Book"));
                }
                else
                {
                    fldBook.Text = FCConvert.ToString(rsData.Get_Fields_Int32("UTBook"));
                }
                if (Conversion.Val(fldAccount.Text) == lngLastAccount && Strings.UCase(Strings.Trim(fldName.Text)) == Strings.UCase(Strings.Trim(strLastName)))
                {
                    HideNameField_2(true);
                }
                else
                {
                    HideNameField_2(false);
                }
                dblTotalPayment = 0;
                dblTotalAbate = 0;
                dblTotalRefundAbate = 0;
                dblCurrentInterest = 0;
                dblPreLienIntNeed = 0;
                dblLienCostNeed = 0;

                if ((FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0 && !boolShowRegular) || (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) != 0 && !boolShowLien))
                {
                    // this will check to see if this account should be shown
                    rsData.MoveNext();
                    goto TRYNEXTACCOUNT;
                }
                if (FCConvert.ToInt32(rsData.Get_Fields_Int32("ActualAccountNumber")) != lngCheckAccount || FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) != lngLien)
                {
                    lngCheckAccount = -1;
                    boolLienRec = false;
                    lngLien = -1;
                }
                if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) != 0)
                {
                    // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                    if (rsData.Get_Fields(strWS + "CombinationLienKey") != rsData.Get_Fields("Bill"))
                    {
                        if (boolLienRec)
                        {
                            // MAL@20070911: Check for lien records that are being "separated" due to As Of Date
                            rsData.MoveNext();
                            goto TRYNEXTACCOUNT;
                        }
                    }
                }
                // MAL@20071018: Add check for duplicates
                if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                {
                    // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                    lngBillKey = FCConvert.ToInt32(rsData.Get_Fields("Bill"));
                    if (objDictionary.ContainsKey(lngBillKey))
                    {
                        rsData.MoveNext();
                        /*? 420 */
                        goto TRYNEXTACCOUNT;
                    }
                    else
                    {
                        objDictionary.Add(lngBillKey, objDictionary.Count + 1);
                    }
                }
                else
                {
                    lngBillKey = FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber"));
                    if (objLienDictionary.ContainsKey(lngBillKey))
                    {
                        if (!modMain.Statics.gboolUTUseAsOfDate)
                        {
                            rsData.MoveNext();
                            /*? 450 */
                            goto TRYNEXTACCOUNT;
                        }
                        else
                        {
                            blnIsAddtlLienRec = true;
                        }
                    }
                    else
                    {
                        objLienDictionary.Add(lngBillKey, objLienDictionary.Count + 1);
                    }
                }
                if (modMain.Statics.gboolUTUseAsOfDate)
                {
                    if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                    {
                        if (Information.IsDate(GetBillingDate_2(rsData.Get_Fields_Int32("BillingRateKey"))))
                        {
                            if (String.Compare(GetBillingDate_2(rsData.Get_Fields_Int32("BillingRateKey")), modMain.Statics.gdtUTStatusListAsOfDate.ToString()) <= 0)
                            {
                                fldBillNum.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillingRateKey"));
                                fldBillDate.Text = GetBillingDate_2(rsData.Get_Fields_Int32("BillingRateKey"));
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(Lien,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'" + strPaymentService;
                                boolLienRec = false;
                                if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                                {
                                    dblDue = modUTCalculations.CalculateAccountUT(rsData, modMain.Statics.gdtUTStatusListAsOfDate, ref dblCurrentInterest, boolWater);
                                }
                            }
                            else
                            {
                                fldBillDate.Text = "Not Billed";
                                fldBillNum.Text = string.Empty;
                                boolLienRec = false;
                                blnIsPrepayment = true;
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(Lien,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'" + strPaymentService;
                            }
                        }
                        else if (GetBillingDate_2(rsData.Get_Fields_Int32("BillingRateKey")) == "Not Billed")
                        {
                            if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                            {
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'");
                            }
                            else
                            {
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 1 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'");
                            }
                            if (!rsPayment.EndOfFile())
                            {
                                fldBillDate.Text = "Not Billed";
                                fldBillNum.Text = string.Empty;
                                // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(Lien,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'" + strPaymentService;
                                boolLienRec = false;
                                if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                                {
                                    dblDue = modUTCalculations.CalculateAccountUT(rsData, modMain.Statics.gdtUTStatusListAsOfDate, ref dblCurrentInterest, boolWater);
                                }
                            }
                            else
                            {
                                rsData.MoveNext();
                                goto TRYNEXTACCOUNT;
                            }
                        }
                        else
                        {
                            rsData.MoveNext();
                            goto TRYNEXTACCOUNT;
                        }
                    }
                    else
                    {
                        // lien record
                        // find the information about the payments here
                        strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(Lien,0) = 1 AND BillKey = " + rsData.Get_Fields(strWS + "LienRecordNumber") + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'" + strPaymentService;
                        rsCalLien.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
                        if (rsCalLien.NoMatch)
                        {
                            boolLienRec = false;
                            fldBillDate.Text = GetBillingDate_2(rsData.Get_Fields_Int32("BillingRateKey"));
                            fldBillNum.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillingRateKey"));
                        }
                        else
                        {
                            // MAL@20070911: Add check for lien date ; if after AsOf date then show individual bills instead of lien record
                            // Call Reference: 116673
                            if (Information.IsDate(GetBillingDate_2(rsCalLien.Get_Fields_Int32("RateKey"))))
                            {
                                if (String.Compare(GetBillingDate_2(rsCalLien.Get_Fields_Int32("RateKey")), modMain.Statics.gdtUTStatusListAsOfDate.ToString()) > 0)
                                {
                                    // lien created after As Of Date
                                    boolLienRec = false;
                                    fldBillDate.Text = GetBillingDate_2(rsData.Get_Fields_Int32("BillingRateKey"));
                                    fldBillNum.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillingRateKey"));
                                    // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                    strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(Lien,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'" + strPaymentService;
                                    if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                                    {
                                        dblDue = modUTCalculations.CalculateAccountUT(rsData, modMain.Statics.gdtUTStatusListAsOfDate, ref dblCurrentInterest, boolWater);
                                    }
                                }
                                else
                                {
                                    // MAL@20080715: Check to see if lien record has already been processed
                                    // Tracker Reference: 14639
                                    if (!blnIsAddtlLienRec)
                                    {
                                        boolLienRec = true;
                                        lngCheckAccount = FCConvert.ToInt32(rsData.Get_Fields_Int32("ActualAccountNumber"));
                                        lngLien = FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber"));
                                        fldBillDate.Text = GetBillingDate_2(rsCalLien.Get_Fields_Int32("RateKey"));
                                        fldBillNum.Text = FCConvert.ToString(rsCalLien.Get_Fields_Int32("RateKey"));
                                        if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                                        {
                                            dblDue = modUTCalculations.CalculateAccountUTLien(rsCalLien, modMain.Statics.gdtUTStatusListAsOfDate, ref dblCurrentInterest, boolWater);
                                        }
                                    }
                                    else
                                    {
                                        rsData.MoveNext();
                                        goto TRYNEXTACCOUNT;
                                    }
                                }
                            }
                            else if (GetBillingDate_2(rsData.Get_Fields(FCConvert.ToString(rsData.Get_Fields_Int32("BillingRateKey")))) == "Not Billed")
                            {
                                if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                                {
                                    // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                    rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'");
                                }
                                else
                                {
                                    // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                    rsPayment.OpenRecordset("SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(LIEN,0) = 1 AND BillKey = " + rsData.Get_Fields("Bill") + strCode + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'");
                                }
                                if (!rsPayment.EndOfFile())
                                {
                                    fldBillDate.Text = FCConvert.ToString(rsPayment.Get_Fields_DateTime("RecordedTransactionDate"));
                                    fldBillNum.Text = string.Empty;
                                    // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                                    strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(Lien,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + ") " + strSLPayRange + " AND ReceiptNumber >= 0 AND RecordedTransactionDate <= '" + FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "'" + strPaymentService;
                                    boolLienRec = false;
                                    if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                                    {
                                        dblDue = modUTCalculations.CalculateAccountUT(rsData, modMain.Statics.gdtUTStatusListAsOfDate, ref dblCurrentInterest, boolWater);
                                    }
                                }
                                else
                                {
                                    rsData.MoveNext();
                                    goto TRYNEXTACCOUNT;
                                }
                            }
                            else
                            {
                                rsData.MoveNext();
                                goto TRYNEXTACCOUNT;
                            }
                        }
                    }
                }
                else
                {
                    if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0)
                    {
                        // not liened
                        fldBillDate.Text = GetBillingDate_2(rsData.Get_Fields_Int32("BillingRateKey"));
                        fldBillNum.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillingRateKey"));
                        // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                        strTemp = "SELECT Principal,Tax,CurrentInterest,PreLienInterest,LienCost,Code FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(Lien,0) = 0 AND BillKey = " + rsData.Get_Fields("Bill") + strSLPayRange + " AND Service = '" + strWS + "' AND ReceiptNumber >= 0)";
                        boolLienRec = false;
                        if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                        {
                            dblDue = modUTCalculations.CalculateAccountUT(rsData, DateTime.Today, ref dblCurrentInterest, boolWater);
                        }
                    }
                    else
                    {
                        // lien record
                        // find the information about the payments here
                        // kk 06052013            strTemp = "SELECT * FROM PaymentRec WHERE (AccountKey = " & rsData.Fields("AccountKey") & " AND ISNULL(Lien,0) = 1 AND BillKey = " & rsData.Fields(strWS & "LienRecordNumber") & strSLPayRange & " AND Service = '" & strWS & "')"
                        strTemp = "SELECT Principal,Tax,CurrentInterest,PreLienInterest,LienCost,Code FROM PaymentRec WHERE (AccountKey = " + rsData.Get_Fields_Int32("AccountKey") + " AND ISNULL(Lien,0) = 1 AND BillKey = " + rsData.Get_Fields(strWS + "LienRecordNumber") + strSLPayRange + " AND Service = '" + strWS + "')";
                        rsCalLien.FindFirstRecord("ID", rsData.Get_Fields(strWS + "LienRecordNumber"));
                        if (rsCalLien.NoMatch)
                        {
                            boolLienRec = false;
                            fldBillDate.Text = GetBillingDate_2(rsData.Get_Fields_Int32("BillingRateKey"));
                            fldBillNum.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillingRateKey"));
                        }
                        else
                        {
                            fldBillDate.Text = GetBillingDate_2(rsCalLien.Get_Fields_Int32("RateKey"));
                            fldBillNum.Text = FCConvert.ToString(rsCalLien.Get_Fields_Int32("RateKey"));
                            boolLienRec = true;
                            lngCheckAccount = FCConvert.ToInt32(rsData.Get_Fields_Int32("ActualAccountNumber"));
                            lngLien = FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber"));
                            if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                            {
                                dblDue = modUTCalculations.CalculateAccountUTLien(rsCalLien, DateTime.Today, ref dblCurrentInterest, boolWater);
                            }
                        }
                    }
                }
                // 0 - 3, 1 - A, 2 - C, 3 - D, 4 - I, 5 - L, 6 - P, 7 - R, 8 - U, 9 - X, 10 - Y
                // 0 - Principal, 1 - PreLienInterest, 2 - CurrentInterest, 3 - Costs, 4 - Total
                AddToPaymentArray_728(11, 0, 0, 0, dblCurrentInterest * -1, 0);
                rsPayment.OpenRecordset(strTemp, modMain.DEFAULTDATABASE);
                if (rsPayment.EndOfFile() != true)
                {

                    if (frmUTStatusList.InstancePtr.boolFullStatusAmounts)
                    {
                        // add up all of the payment records
                        while (!rsPayment.EndOfFile())
                        {
                            dblPPrin = 0;
                            dblPTax = 0;
                            dblPInt = 0;
                            dblPLInt = 0;
                            dblPCost = 0;
                            
                            if (boolPrincipal)
                            {
                                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                                dblPPrin = FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
                            }
                            if (boolTaxes)
                            {
                                // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                                dblPTax = FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
                            }
                            if (boolInterest)
                            {
                                dblPInt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("CurrentInterest"));
                                dblPLInt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest"));
                            }
                            if (boolCosts)
                            {
                                dblPCost = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
                            }
                            if (dblPPrin != 0 || dblPTax != 0 || dblPInt != 0 || dblPCost != 0 || dblPLInt != 0)
                            {
                                var code = Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code")));
                                switch (code)
                                {
                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "P":
                                        dblTotalPayment += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(6, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);
                                        dblTotalPPayment += (dblPPrin + dblPTax + dblPInt + dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "U":
                                        dblTaxClub += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        dblTotalPayment += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(8, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "X":
                                        dblTotalPayment += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(9, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "Y":
                                        dblPrePay += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        dblTotalPayment += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(10, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "C":
                                        dblTotalAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(2, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);
                                        dblTotalCorrection += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "A":
                                        dblJustAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        dblTotalAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(1, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "S":
                                        dblSupplemental += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        dblTotalAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        // put these figures in the X category because there are going away
                                        AddToPaymentArray_2(9, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "D":
                                        dblDiscount += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        dblTotalAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(3, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "I":
                                        dblTotalAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(4, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "L":
                                        dblTotalAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(5, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;

                                    // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                    case "3":
                                        dblTotalAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        AddToPaymentArray_2(0, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;
                                    case "R":

                                        // If dblPPrin  + dblPInt + dblPCost > 0 Then
                                        // do not count the negatives because it will all balance to zero and be a worthless number
                                        dblTotalRefundAbate += (dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost);
                                        // End If
                                        AddToPaymentArray_2(7, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);

                                        break;
                                }
                            }
                            rsPayment.MoveNext();
                        }
                    }
                    else
                    {
                        if (boolLienRec)
                        {
                            // if this is a lien then I need to keep track of the PreLienInterest and Cost Paid as well as the principal
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            dblPreLienIntNeed = FCConvert.ToDouble(rsCalLien.Get_Fields("Interest"));
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            dblLienCostNeed = FCConvert.ToDouble(rsCalLien.Get_Fields("Costs"));
                            dblPreLienIntPaid = 0;
                            dblLienCostPaid = 0;
                            dblInterestPaid = 0;
                            dblInterestCharged = 0;
                        }
                        while (!rsPayment.EndOfFile())
                        {
                            dblPPrin = 0;
                            dblPTax = 0;
                            dblPInt = 0;
                            dblPCost = 0;
                            if (boolPrincipal)
                            {
                                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                                dblPPrin = FCConvert.ToDouble(rsPayment.Get_Fields("Principal"));
                            }
                            if (boolTaxes)
                            {
                                // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                                dblPTax = FCConvert.ToDouble(rsPayment.Get_Fields("Tax"));
                            }
                            if (boolInterest)
                            {
                                dblPLInt = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("PreLienInterest"));
                                // + rsPayment.Fields("CurrentInterest")
                                dblPInt = 0;
                            }
                            if (boolCosts)
                            {
                                dblPCost = FCConvert.ToDouble(rsPayment.Get_Fields_Decimal("LienCost"));
                            }
                            if (boolLienRec)
                            {
                                // this will check to make sure that only the amount that is on the lien itself is shown
                                if (dblPreLienIntNeed > 0 && dblPLInt > 0)
                                {
                                    if (dblPLInt >= dblPreLienIntNeed)
                                    {
                                        dblInterestPaid += dblPreLienIntNeed;
                                        dblPreLienIntNeed = 0;
                                    }
                                    else
                                    {
                                        dblInterestPaid += dblPLInt;
                                        dblPreLienIntNeed -= (dblPLInt);
                                    }
                                }
                            }
                            // MAL@20081126: Add charged interest back into totals
                            // Tracker Reference : 16374
                            if (dblPPrin != 0 || dblPTax != 0 || dblPCost != 0 || dblPLInt != 0 || dblPInt != 0)
                            {
                                var code = Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code")));
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                if (code == "P")
                                {
                                    if (boolLienRec)
                                    {
                                        dblTotalPayment += dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost;
                                        AddToPaymentArray_2(6, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);
                                        // 
                                    }
                                    else
                                    {
                                        dblTotalPayment += dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost;
                                        AddToPaymentArray_2(6, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if ((code == "X") || (code == "S"))
                                {
                                    if (boolLienRec)
                                    {
                                        dblTotalPayment += dblPPrin + dblPTax + dblPLInt + dblPCost;
                                        AddToPaymentArray_164(9, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                    }
                                    else
                                    {
                                        dblTotalPayment += dblPPrin + dblPTax + dblPLInt + dblPCost;
                                        AddToPaymentArray_650(9, dblPPrin, dblPTax, dblPLInt, 0, 0);
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "U")
                                {
                                    dblTotalPayment += dblPPrin + dblPTax + dblPLInt + dblPCost;
                                    AddToPaymentArray_164(8, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "Y")
                                {
                                    dblTotalPayment += dblPPrin + dblPTax + dblPLInt + dblPCost;
                                    // (dblPPrin  + dblPInt + dblPCost)
                                    AddToPaymentArray_164(10, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "C")
                                {
                                    if (boolLienRec)
                                    {
                                        dblTotalAbate += dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost;
                                        // (dblPPrin  + dblPInt + dblPCost)
                                        AddToPaymentArray_2(2, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);
                                    }
                                    else
                                    {
                                        dblTotalAbate += dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost;
                                        // (dblPPrin  + dblPInt + dblPCost)
                                        AddToPaymentArray_2(2, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "A")
                                {
                                    if (boolLienRec)
                                    {
                                        dblTotalAbate += dblPPrin + dblPTax + dblPLInt + dblPCost;
                                        // (dblPPrin  + dblPInt + dblPCost)
                                        AddToPaymentArray_164(1, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                    }
                                    else
                                    {
                                        dblTotalAbate += dblPPrin + dblPTax + dblPLInt + dblPCost;
                                        AddToPaymentArray_164(1, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                    }
                                    // Case "S"
                                    // dblTotalAbate = dblTotalAbate + dblPPrin       '(dblPPrin  + dblPInt + dblPCost)
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "D")
                                {
                                    dblTotalAbate += dblPPrin + dblPTax + dblPLInt + dblPCost;
                                    // (dblPPrin  + dblPInt + dblPCost)
                                    AddToPaymentArray_164(3, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "I")
                                {
                                    if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                                    {
                                        dblTotalAbate += -dblPPrin + dblPTax + dblPLInt + dblPInt + dblPCost;
                                        // (dblPPrin  + dblPInt + dblPCost)
                                        AddToPaymentArray_2(4, dblPPrin, dblPTax, dblPLInt, dblPInt, dblPCost);
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "L")
                                {
                                    if (boolLienRec)
                                    {
                                        dblTotalAbate += -dblPPrin + dblPTax + dblPLInt + dblPCost;
                                        // (dblPPrin  + dblPInt + dblPCost)
                                        AddToPaymentArray_164(5, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                    }
                                    else
                                    {
                                        dblTotalAbate += -dblPPrin + dblPTax + dblPLInt + dblPCost;
                                        // (dblPPrin  + dblPInt + dblPCost)
                                        AddToPaymentArray_650(5, dblPPrin, dblPTax, dblPLInt, 0, 0);
                                        // ,  dblPCost
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "3")
                                {
                                    if (boolLienRec)
                                    {
                                        dblTotalAbate += -dblPPrin + dblPTax + dblPLInt + dblPCost;
                                        // (dblPPrin  + dblPInt + dblPCost)
                                        AddToPaymentArray_164(0, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                    }
                                    else
                                    {
                                        if (dblPPrin != 0)
                                        {
                                            dblTotalPayment += dblPPrin + dblPTax + dblPLInt + dblPCost;
                                            AddToPaymentArray_164(0, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                        }
                                        else
                                        {
                                            dblTotalAbate += -dblPPrin + dblPTax + dblPLInt + dblPCost;
                                            // (dblPPrin  + dblPInt + dblPCost)
                                            AddToPaymentArray_164(0, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                        }
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "R")
                                {
                                    if (dblPPrin + dblPInt + dblPCost + dblPLInt > 0)
                                    {
                                        // do not count the negatives because it will all balance to zero and be a worthless number
                                        if (boolLienRec)
                                        {
                                            dblTotalRefundAbate += dblPPrin + dblPTax + dblPLInt;
                                            // + dblPCost  '(dblPPrin  + rsPayment.Fields("CurrentInterest") + dblPCost)
                                        }
                                        else
                                        {
                                            dblTotalRefundAbate += dblPPrin + dblPTax + dblPLInt;
                                        }
                                    }
                                    if (boolLienRec)
                                    {
                                        AddToPaymentArray_164(7, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                    }
                                    else
                                    {
                                        AddToPaymentArray_650(7, dblPPrin, dblPTax, dblPLInt, 0, 0);
                                    }
                                }
                            }
                            else
                            {
                                var code = Strings.UCase(FCConvert.ToString(rsPayment.Get_Fields("Code")));
                                if (code == "P")
                                {
                                    AddToPaymentArray_164(6, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if ((code == "X") || (code == "S"))
                                {
                                    AddToPaymentArray_164(9, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "U")
                                {
                                    AddToPaymentArray_164(8, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "Y")
                                {
                                    AddToPaymentArray_164(10, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "C")
                                {
                                    AddToPaymentArray_164(2, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "A")
                                {
                                    AddToPaymentArray_164(1, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                    // Case "S"
                                    // dblTotalAbate = dblTotalAbate + dblPPrin       '(dblPPrin  + rsPayment.Fields("CurrentInterest") + dblPCost)
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "D")
                                {
                                    AddToPaymentArray_164(3, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "I")
                                {
                                    if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                                    {
                                        AddToPaymentArray_164(4, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                    }
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "L")
                                {
                                    AddToPaymentArray_164(5, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "3")
                                {
                                    AddToPaymentArray_164(0, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                                // TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
                                else if (code == "R")
                                {
                                    AddToPaymentArray_164(7, dblPPrin, dblPTax, dblPLInt, 0, dblPCost);
                                }
                            }
                            rsPayment.MoveNext();
                        }
                        // figure out how much interest/cost payment should be shown if this is a lien
                        if (boolLienRec)
                        {
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            if (dblInterestPaid > FCConvert.ToDouble(rsCalLien.Get_Fields("Interest")))
                            {
                                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                                dblInterestPaid = FCConvert.ToDouble(rsCalLien.Get_Fields("Interest"));
                                dblPreLienIntPaid = dblInterestPaid;
                            }
                            else
                            {
                                // dblIntPaid = rsData.Fields("LienRec.InterestPaid")
                                dblPreLienIntPaid = dblInterestPaid;
                            }
                            if (dblPreLienIntPaid < 0)
                            {
                                dblPreLienIntPaid = 0;
                            }
                        }
                    }
                }
                else if (blnIsPrepayment)
                {
                    ReversePaymentsFromStatusArray(ref rsPayment);
                    rsData.MoveNext();
                    // clear fields
                    goto TRYNEXTACCOUNT;
                }
                else
                {
                    // if no payment records, then make everything zeros
                    fldPaymentReceived.Text = CURRENCY_ZERO_VALUE;
                }
                // this is the current balance due
                // If Not boolStarted Then
                if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0 || !boolLienRec)
                {
                    // MAL@20070911: Added OR to have those lien records that come after as of calculate original amount due
                    // this is the original tax due
                    dblAmtDue = 0;
                    dblAmtDueP = 0;
                    dblAmtDueT = 0;
                    dblAmtDueI = 0;
                    dblAmtDueC = 0;
                    if (modMain.Statics.gboolUTUseAsOfDate)
                    {
                        if (Strings.Left(fldBillDate.Text, 3) != "Not")
                        {
                            boolShowOrig = FCConvert.CBool(DateAndTime.DateValue(fldBillDate.Text).ToOADate() <= modMain.Statics.gdtUTStatusListAsOfDate.ToOADate());
                            /*? 1420 */
                        }
                        else if (blnIsPrepayment)
                        {
                            boolShowOrig = false;
                        }
                        else
                        {
                            boolShowOrig = true;
                        }
                    }
                    else
                    {
                        boolShowOrig = true;
                    }
                    if (boolShowOrig)
                    {
                        if (frmUTStatusList.InstancePtr.boolFullStatusAmounts && boolInterest)
                        {
                            dblAmtDueI = FCConvert.ToDouble(rsData.Get_Fields(strWS + "IntOwed") + dblCurrentInterest + dblPreLienIntNeed);
                            dblAmtDue = FCConvert.ToDouble(rsData.Get_Fields(strWS + "IntOwed") + dblCurrentInterest + dblPreLienIntNeed);
                        }
                        else if (boolInterest)
                        {
                            dblAmtDueI = FCConvert.ToDouble(rsData.Get_Fields(strWS + "IntOwed"));
                            dblAmtDue = FCConvert.ToDouble(rsData.Get_Fields(strWS + "IntOwed"));
                        }
                        if (boolPrincipal)
                        {
                            dblAmtDueP = FCConvert.ToDouble(rsData.Get_Fields(strWS + "PrinOwed"));
                            dblAmtDue += FCConvert.ToDouble(rsData.Get_Fields(strWS + "PrinOwed"));
                        }
                        if (boolTaxes)
                        {
                            dblAmtDueT = FCConvert.ToDouble(rsData.Get_Fields(strWS + "TaxOwed"));
                            dblAmtDue += FCConvert.ToDouble(rsData.Get_Fields(strWS + "TaxOwed"));
                        }
                        if (boolCosts)
                        {
                            dblAmtDueC = FCConvert.ToDouble(rsData.Get_Fields(strWS + "CostOwed"));
                            dblAmtDue += FCConvert.ToDouble(rsData.Get_Fields(strWS + "CostOwed"));
                        }
                    }
                    fldTaxDue.Text = Strings.Format(dblAmtDue - dblCurrentInterest, CURRENCY_FORMAT);
                    // this is the total amount due
                    fldDue.Text = Strings.Format(dblAmtDue - (dblTotalPayment + dblTotalAbate), CURRENCY_FORMAT);
                    if (modMain.Statics.gboolUTSLDateRange)
                    {
                        dblTest = modUTCalculations.CalculateAccountUT(rsData, DateTime.Today, ref dblTestInt, boolWater);
                        if (!frmUTStatusList.InstancePtr.boolFullStatusAmounts)
                        {
                            dblTest -= dblTestInt;
                        }
                        fldDue.Text = Strings.Format(dblTest, CURRENCY_FORMAT);
                    }
                    if (modMain.Statics.gboolUTUseAsOfDate)
                    {
                        if (!blnIsPrepayment)
                        {
                            // MAL@20080125
                            // show the amounts owed for prin/tax/int/costs
                            if (boolPrincipal)
                            {
                                fldPrincipal.Text = Strings.Format(dblAmtDueP - GetBillPaymentAmount_2(0), CURRENCY_FORMAT);
                            }
                            else
                            {
                                fldPrincipal.Text = CURRENCY_ZERO_VALUE;
                            }
                            if (boolTaxes)
                            {
                                fldTax.Text = Strings.Format(dblAmtDueT - GetBillPaymentAmount_2(5), CURRENCY_FORMAT);
                            }
                            else
                            {
                                fldTax.Text = CURRENCY_ZERO_VALUE;
                            }
                            if (frmUTStatusList.InstancePtr.boolFullStatusAmounts && boolInterest)
                            {
                                fldInterest.Text = Strings.Format(dblAmtDueI - GetBillPaymentAmount_2(1) - GetBillPaymentAmount_2(2), CURRENCY_FORMAT);
                                // + dblCurrentInterest
                            }
                            else
                            {
                                fldInterest.Text = CURRENCY_ZERO_VALUE;
                            }
                            if (boolCosts)
                            {
                                fldCosts.Text = Strings.Format(dblAmtDueC - GetBillPaymentAmount_2(3), CURRENCY_FORMAT);
                            }
                            else
                            {
                                fldCosts.Text = CURRENCY_ZERO_VALUE;
                            }
                        }
                        else
                        {
                            // MAL@20080311: Change to show pre-payment amount in the Outstanding Principal
                            // Tracker Reference: 12674
                            // fldPrincipal.Text = "0.00"
                            fldPrincipal.Text = Strings.Format((dblTotalPayment * -1), CURRENCY_FORMAT);
                            fldTax.Text = CURRENCY_ZERO_VALUE;
                            fldInterest.Text = CURRENCY_ZERO_VALUE;
                            fldCosts.Text = CURRENCY_ZERO_VALUE;
                        }
                    }
                    else
                    {
                        // show the amounts owed for prin/tax/int/costs
                        if (boolPrincipal)
                        {
                            fldPrincipal.Text = Strings.Format(dblAmtDueP - rsData.Get_Fields(strWS + "PrinPaid"), CURRENCY_FORMAT);
                        }
                        else
                        {
                            fldPrincipal.Text = CURRENCY_ZERO_VALUE;
                        }
                        if (boolTaxes)
                        {
                            fldTax.Text = Strings.Format(dblAmtDueT - rsData.Get_Fields(strWS + "TaxPaid"), CURRENCY_FORMAT);
                        }
                        else
                        {
                            fldTax.Text = CURRENCY_ZERO_VALUE;
                        }
                        if (frmUTStatusList.InstancePtr.boolFullStatusAmounts && boolInterest)
                        {
                            fldInterest.Text = Strings.Format(dblAmtDueI - rsData.Get_Fields(strWS + "IntAdded") - rsData.Get_Fields(strWS + "IntPaid"), CURRENCY_FORMAT);
                        }
                        else
                        {
                            fldInterest.Text = CURRENCY_ZERO_VALUE;
                            // Format(rsData.Fields(strWS & "IntOwed") - rsData.Fields(strWS & "IntAdded") - rsData.Fields(strWS & "IntPaid"), "#,##0.00")
                        }
                        if (boolCosts)
                        {
                            fldCosts.Text = Strings.Format(dblAmtDueC - rsData.Get_Fields(strWS + "CostAdded") - rsData.Get_Fields(strWS + "CostPaid"), CURRENCY_FORMAT);
                        }
                        else
                        {
                            fldCosts.Text = CURRENCY_ZERO_VALUE;
                        }
                    }
                    fldLienRecord.Visible = false;
                }
                else
                {
                    if (modMain.Statics.gboolUTUseAsOfDate && Strings.Left(fldBillDate.Text, 3) != "Not")
                    {
                        boolShowOrig = FCConvert.CBool(DateAndTime.DateValue(fldBillDate.Text).ToOADate() <= modMain.Statics.gdtUTStatusListAsOfDate.ToOADate());
                    }
                    else if (blnIsPrepayment)
                    {
                        boolShowOrig = false;
                    }
                    else
                    {
                        boolShowOrig = true;
                    }
                    dblAmtDue = 0;
                    dblAmtDueP = 0;
                    dblAmtDueT = 0;
                    dblAmtDueI = 0;
                    dblAmtDueC = 0;
                    if (boolLienRec)
                    {
                        // this is the original tax liened
                        if (boolShowOrig)
                        {
                            if (boolPrincipal)
                            {
                                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                                dblAmtDueP = FCConvert.ToDouble(rsCalLien.Get_Fields("Principal"));
                                // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                                dblAmtDue += FCConvert.ToDouble(rsCalLien.Get_Fields("Principal"));
                            }
                            // Corey 06/17/2009  Added the first half of if statement because current interest not showing for liens
                            if (frmUTStatusList.InstancePtr.boolFullStatusAmounts && boolInterest)
                            {
                                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                                dblAmtDueI = FCConvert.ToDouble(rsCalLien.Get_Fields("Interest") + dblCurrentInterest);
                                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                                dblAmtDue += FCConvert.ToDouble(rsCalLien.Get_Fields("Interest"));
                            }
                            else if (boolInterest)
                            {
                                // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                                dblAmtDueI = FCConvert.ToDouble(rsCalLien.Get_Fields("Interest"));
                                // + dblCurrentInterest  '- rsCalLien.Fields("IntAdded")
                                dblAmtDue += dblAmtDueI;
                                // rsCalLien.Fields("Interest") - rsCalLien.Fields("IntAdded") + dblCurrentInterest
                            }
                            if (boolTaxes)
                            {
                                // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                                dblAmtDueT = FCConvert.ToDouble(rsCalLien.Get_Fields("Tax"));
                                // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                                dblAmtDue += FCConvert.ToDouble(rsCalLien.Get_Fields("Tax"));
                            }
                            if (boolCosts)
                            {
                                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                                dblAmtDueC = FCConvert.ToDouble(rsCalLien.Get_Fields("Costs"));
                                // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                                dblAmtDue += FCConvert.ToDouble(rsCalLien.Get_Fields("Costs"));
                            }
                        }
                        fldTaxDue.Text = Strings.Format(dblAmtDue, CURRENCY_FORMAT);
                        fldDue.Text = Strings.Format(dblAmtDue - (dblTotalPayment + dblTotalAbate) + dblCurrentInterest, CURRENCY_FORMAT);

                        if (modMain.Statics.gboolUTSLDateRange)
                        {
                            dblTest = modUTCalculations.CalculateAccountUTLien(rsCalLien, DateTime.Today, ref dblTestInt, boolWater);
                            if (!frmUTStatusList.InstancePtr.boolFullStatusAmounts)
                            {
                                dblTest -= dblTestInt;
                                dblCurrentInterest = dblTestInt;
                                // this will force the use of the new interest amount
                            }
                            fldDue.Text = Strings.Format(dblTest, CURRENCY_FORMAT);
                        }
                        if (modMain.Statics.gboolUTUseAsOfDate)
                        {
                            if (!blnIsPrepayment)
                            {
                                // show the amounts owed for prin/tax/int/costs
                                if (boolPrincipal)
                                {
                                    fldPrincipal.Text = Strings.Format(dblAmtDueP - GetBillPaymentAmount_2(0), CURRENCY_FORMAT);
                                }
                                else
                                {
                                    fldPrincipal.Text = CURRENCY_ZERO_VALUE;
                                }
                                if (boolTaxes)
                                {
                                    fldTax.Text = Strings.Format(dblAmtDueT - GetBillPaymentAmount_2(5), CURRENCY_FORMAT);
                                }
                                else
                                {
                                    fldTax.Text = CURRENCY_ZERO_VALUE;
                                }
                                if (frmUTStatusList.InstancePtr.boolFullStatusAmounts && boolInterest)
                                {
                                    fldInterest.Text = Strings.Format(dblAmtDueI - GetBillPaymentAmount_2(1) - GetBillPaymentAmount_2(2), CURRENCY_FORMAT);
                                }
                                else if (boolInterest)
                                {
                                    fldInterest.Text = Strings.Format(dblAmtDueI - GetBillPaymentAmount_2(1), CURRENCY_FORMAT);
                                }
                                else
                                {
                                    fldInterest.Text = CURRENCY_ZERO_VALUE;
                                }
                                if (boolCosts)
                                {
                                    fldCosts.Text = Strings.Format(dblAmtDueC - GetBillPaymentAmount_2(3), CURRENCY_FORMAT);
                                }
                                else
                                {
                                    fldCosts.Text = CURRENCY_ZERO_VALUE;
                                }
                            }
                            else
                            {
                                fldPrincipal.Text = Strings.Format((dblTotalPayment * -1), CURRENCY_FORMAT);
                                fldTax.Text = CURRENCY_ZERO_VALUE;
                                fldInterest.Text = CURRENCY_ZERO_VALUE;
                                fldCosts.Text = CURRENCY_ZERO_VALUE;
                            }
                        }
                        else
                        {
                            // show the amounts owed for prin/tax/int/costs
                            if (boolPrincipal)
                            {
                                // TODO Get_Fields: Check the table for the column [PrinPaid] and replace with corresponding Get_Field method
                                fldPrincipal.Text = Strings.Format(dblAmtDueP - rsCalLien.Get_Fields("PrinPaid"), CURRENCY_FORMAT);
                            }
                            else
                            {
                                fldPrincipal.Text = CURRENCY_ZERO_VALUE;
                            }
                            if (boolTaxes)
                            {
                                // TODO Get_Fields: Check the table for the column [TaxPaid] and replace with corresponding Get_Field method
                                fldTax.Text = Strings.Format(dblAmtDueT - rsCalLien.Get_Fields("TaxPaid"), CURRENCY_FORMAT);
                            }
                            else
                            {
                                fldTax.Text = CURRENCY_ZERO_VALUE;
                            }
                            if (frmUTStatusList.InstancePtr.boolFullStatusAmounts && boolInterest)
                            {
                                // TODO Get_Fields: Check the table for the column [IntAdded] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [IntPaid] and replace with corresponding Get_Field method
                                // TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                                fldInterest.Text = Strings.Format(dblAmtDueI - rsCalLien.Get_Fields("IntAdded") - rsCalLien.Get_Fields("IntPaid") - rsCalLien.Get_Fields("PLIPaid"), CURRENCY_FORMAT);
                            }
                            else if (boolInterest)
                            {
                                // TODO Get_Fields: Check the table for the column [PLIPaid] and replace with corresponding Get_Field method
                                fldInterest.Text = Strings.Format(dblAmtDueI - rsCalLien.Get_Fields("PLIPaid"), CURRENCY_FORMAT);
                                // - rsCalLien.Fields("IntAdded") - rsCalLien.Fields("IntPaid")
                            }
                            else
                            {
                                fldInterest.Text = CURRENCY_ZERO_VALUE;
                            }
                            if (boolCosts)
                            {
                                // TODO Get_Fields: Check the table for the column [MaturityFee] and replace with corresponding Get_Field method
                                fldCosts.Text = Strings.Format(dblAmtDueC - rsCalLien.Get_Fields("MaturityFee") - rsCalLien.Get_Fields_Double("CostPaid"), CURRENCY_FORMAT);
                            }
                            else
                            {
                                fldCosts.Text = CURRENCY_ZERO_VALUE;
                            }
                        }
                        fldLienRecord.Visible = true;
                    }
                    else
                    {
                        // this is the original tax due because of an error finding the Lien Record
                        fldTaxDue.Text = "**" + Strings.Format(rsData.Get_Fields_Decimal("PrinOwed") + rsData.Get_Fields_Decimal("TaxOwed"), CURRENCY_FORMAT);
                        fldDue.Text = fldTaxDue.Text;
                        boolRTError = true;
                        // show the error at the bottom of the report
                    }
                }
                if (boolLienRec)
                {
                    fldPaymentReceived.Text = Strings.Format(dblTotalPayment + dblTotalAbate - dblCurrentInterest, CURRENCY_FORMAT);
                }
                else
                {
                    fldPaymentReceived.Text = Strings.Format(dblTotalPayment + dblTotalAbate - dblCurrentInterest, CURRENCY_FORMAT);
                }
                // now check to make sure that this account matches the balance due criteria, if not go to the next account
                if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowBalanceDue, 1)) != "" && Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowBalanceDue, 2)) != "")
                {
                    // Check the balance due
                    if (CheckBalanceDue_2(FCConvert.ToDouble(fldDue.Text)))
                    {
                        // this is ok keep going
                        ResetReversalValues();
                    }
                    else
                    {
                        // reverse the payments that have already been calculated for this account so that they are not shown in the payment summary totals at the bottom
                        ReversePaymentsFromStatusArray(ref rsPayment);
                        rsData.MoveNext();
                        // clear fields
                        goto TRYNEXTACCOUNT;
                    }
                }

                if (frmUTStatusList.InstancePtr.chkShowPayments.CheckState == Wisej.Web.CheckState.Checked)
                {
                    srptSLAllActivityDetailOB.Report = new srptUTSLAllActivityDetail();
                    // TODO Get_Fields: Field [Bill] not found!! (maybe it is an alias?)
                    srptSLAllActivityDetailOB.Report.UserData = rsData.Get_Fields("Bill");
                    srptSLAllActivityDetailOB.Visible = true;
                }
                else
                {
                    srptSLAllActivityDetailOB.Visible = false;
                }
                lngLastAccount = FCConvert.ToInt32(Math.Round(Conversion.Val(fldAccount.Text)));
                strLastName = Strings.Trim(fldName.Text);
                lngCount += 1;
                // overall totals
                dblTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
                dblTotals[1] += FCConvert.ToDouble(fldPaymentReceived.Text);
                dblTotals[2] += dblTotalAbate - dblCurrentInterest;
                if (Strings.Left(fldDue.Text, 2) != "**")
                {
                    dblTotals[3] += FCConvert.ToDouble(fldDue.Text);
                    if (FCConvert.ToInt32(rsData.Get_Fields(strWS + "LienRecordNumber")) == 0 || !boolLienRec)
                    {
                        AddYearTotal(rsData.Get_Fields_Int32("BillingRateKey"), FCConvert.ToDouble(fldDue.Text));
                    }
                    else
                    {
                        AddYearTotal(FCConvert.ToInt32(Conversion.Val(rsCalLien.Get_Fields_Int32("RateKey"))), FCConvert.ToDouble(fldDue.Text));
                    }
                }
                else
                {
                    dblTotals[3] += FCConvert.ToDouble(Strings.Right(fldDue.Text, fldDue.Text.Length - 2));
                    // keep track for the year totals
                    AddYearTotal(rsData.Get_Fields_Int32("billingratekey"), FCConvert.ToDouble(Strings.Right(fldDue.Text, fldDue.Text.Length - 2)));
                }
                dblTotals[4] += dblTotalRefundAbate;
                dblTotals[5] += FCConvert.ToDouble(fldPrincipal.Text);
                dblTotals[6] += FCConvert.ToDouble(fldTax.Text);
                dblTotals[7] += FCConvert.ToDouble(fldInterest.Text);
                dblTotals[8] += FCConvert.ToDouble(fldCosts.Text);
                dblGroupTotals[0] += FCConvert.ToDouble(fldTaxDue.Text);
                dblGroupTotals[1] += FCConvert.ToDouble(fldPaymentReceived.Text);
                dblGroupTotals[2] += dblTotalAbate - dblCurrentInterest;
                if (Strings.Left(fldDue.Text, 2) != "**")
                {
                    dblGroupTotals[3] += FCConvert.ToDouble(fldDue.Text);
                }
                else
                {
                    dblGroupTotals[3] += FCConvert.ToDouble(Strings.Right(fldDue.Text, fldDue.Text.Length - 2));
                }
                dblGroupTotals[4] += dblTotalRefundAbate;
                // Principal
                dblGroupTotals[5] += FCConvert.ToDouble(fldPrincipal.Text);
                // Tax
                dblGroupTotals[6] += FCConvert.ToDouble(fldTax.Text);
                // Interest
                dblGroupTotals[7] += FCConvert.ToDouble(fldInterest.Text);
                // Costs
                dblGroupTotals[8] += FCConvert.ToDouble(fldCosts.Text);
                // If CCur(fldDue) <> CCur(fldPrincipal) + CCur(fldTax) + CCur(fldInterest) + CCur(fldCosts) Then
                // MsgBox "hi"
                // End If
                rsPayment.Dispose();
                return;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ". Line " + Information.Erl(), "Error Binding Fields", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private bool CheckBalanceDue_2(double dblAmt)
        {
            return CheckBalanceDue(ref dblAmt);
        }

        private bool CheckBalanceDue(ref double dblAmt)
        {
            bool CheckBalanceDue = false;
            // this function will return true if this amount matches the
            double dblTestAmt = 0;
            dblTestAmt = FCConvert.ToDouble(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowBalanceDue, 2));
            string vbPorterVar = frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowBalanceDue, 1);
            if (vbPorterVar == ">")
            {
                CheckBalanceDue = dblAmt > dblTestAmt;
            }
            else if (vbPorterVar == "<")
            {
                CheckBalanceDue = dblAmt < dblTestAmt;
            }
            else if (vbPorterVar == "<>")
            {
                CheckBalanceDue = dblAmt != dblTestAmt;
            }
            else if (vbPorterVar == "=")
            {
                CheckBalanceDue = dblAmt == dblTestAmt;
            }
            else
            {
                CheckBalanceDue = false;
            }
            return CheckBalanceDue;
        }

        private void ClearFields()
        {
            // this routine will clear the fields
            fldAccount.Text = string.Empty;
            fldName.Text = string.Empty;
            fldBook.Text = string.Empty;
            fldBillDate.Text = string.Empty;
            fldBillNum.Text = string.Empty;
            fldTaxDue.Text = string.Empty;
            fldPaymentReceived.Text = string.Empty;
            fldDue.Text = string.Empty;
            fldPrincipal.Text = string.Empty;
            fldInterest.Text = string.Empty;
            fldTax.Text = string.Empty;
            fldCosts.Text = string.Empty;
            fldLienRecord.Visible = false;
        }

        private void ActiveReport_Terminate(object sender, EventArgs e)
        {
            FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
            rsData.DisposeOf();
            rsCalLien.DisposeOf();
        }

        private void Detail_Format(object sender, EventArgs e)
        {
            try
            {
                if (!boolFirstRecord)
                {
                    rsData.MoveNext();
                }
                else
                {
                    boolFirstRecord = false;
                }
                BindFields();
                if (rsData.EndOfFile())
                {
                    Detail.Height = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Detail Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void SetupGroupTotals()
        {
            fldGroupTaxDue.Text = Strings.Format(dblGroupTotals[0], CURRENCY_FORMAT);
            fldGroupPayments.Text = Strings.Format(dblGroupTotals[1], CURRENCY_FORMAT);
            fldGroupTotal.Text = Strings.Format(dblGroupTotals[3], CURRENCY_FORMAT);
            fldGroupPrincipal.Text = Strings.Format(dblGroupTotals[5], CURRENCY_FORMAT);
            fldGroupTax.Text = Strings.Format(dblGroupTotals[6], CURRENCY_FORMAT);
            fldGroupInterest.Text = Strings.Format(dblGroupTotals[7], CURRENCY_FORMAT);
            fldGroupCost.Text = Strings.Format(dblGroupTotals[8], CURRENCY_FORMAT);
            lblGroupTotals.Text = "Group Total:";
        }

        private void SetupTotals()
        {
            int intSumRows;
            int intCT;
            double[] aryVal = new double[dcYearTotals.Count];
            int[] aryKeys = new int[dcYearTotals.Count];
            // this sub will fill in the totals line at the bottom of the report
            SetupTotalSummary();
            // Load Summary List
            intSumRows = 1;
            if (dcYearTotals.Count > 0)
            {
                dcYearTotals.Values.CopyTo(aryVal, 0);
                dcYearTotals.Keys.CopyTo(aryKeys, 0);
                for (intCT = 0; intCT <= dcYearTotals.Count - 1; intCT++)
                {
                    if (aryVal[intCT] != 0)
                    {
                        AddSummaryRow_24(intSumRows, aryVal[intCT], aryKeys[intCT]);
                        intSumRows += 1;
                    }
                }
            }
            fldTotalTaxDue.Text = Strings.Format(dblTotals[0], CURRENCY_FORMAT);
            fldTotalPaymentReceived.Text = Strings.Format(dblTotals[1], CURRENCY_FORMAT);
            // fldTotalAbatement.Text = Format(dblTotals(2), "#,##0.00")
            fldTotalDue.Text = Strings.Format(dblTotals[3], CURRENCY_FORMAT);
            fldTotalPrincipal.Text = Strings.Format(dblTotals[5], CURRENCY_FORMAT);
            fldTotalTax.Text = Strings.Format(dblTotals[6], CURRENCY_FORMAT);
            fldTotalInterest.Text = Strings.Format(dblTotals[7], CURRENCY_FORMAT);
            fldTotalCost.Text = Strings.Format(dblTotals[8], CURRENCY_FORMAT);
            // fldTotalAbatementPayments.Text = Format(dblTotals(4), "#,##0.00")
            if (lngCount > 1)
            {
                lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bills:";
            }
            else if (lngCount == 1)
            {
                lblTotals.Text = "Total for " + FCConvert.ToString(lngCount) + " Bill:";
            }
            else
            {
                lblTotals.Text = "No Bills";
            }
            GrapeCity.ActiveReports.SectionReportModel.TextBox obNew = ReportFooter.Controls["fldSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
            obNew.Top = fldSummary1.Top + ((intSumRows - 1) * fldSummary1.Height);
            obNew.Left = fldSummary1.Left;
            obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
            obNew.Width = fldSummary1.Width;
            obNew.Font = lblSummary1.Font;
            obNew.Text = Strings.Format(dblPDTotal, CURRENCY_FORMAT);
            GrapeCity.ActiveReports.SectionReportModel.Label obLabel = ReportFooter.Controls["lblPerDiemTotal"] as GrapeCity.ActiveReports.SectionReportModel.Label;
            obLabel.Top = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
            obLabel.Left = lblSummary1.Left;
            obLabel.Font = lblSummary1.Font;
            obLabel.Text = "Total";
            GrapeCity.ActiveReports.SectionReportModel.Line obLine = ReportFooter.Controls["lnFooterSummaryTotal"] as GrapeCity.ActiveReports.SectionReportModel.Line;
            obLine.X1 = fldSummary1.Left;
            obLine.X2 = fldSummary1.Left + fldSummary1.Width;
            obLine.Y1 = lblSummary1.Top + ((intSumRows - 1) * lblSummary1.Height);
            obLine.Y2 = obLine.Y1;
            //this.ReportFooter.Controls.Add(obLine);
            ReportFooter.Height = lblSummary1.Top + (intSumRows * lblSummary1.Height);
            lngExtraRows = ReportFooter.Height;
        }

        private void GroupFooter1_Format(object sender, EventArgs e)
        {
            SetupGroupTotals();
            dblGroupTotals[0] = 0;
            dblGroupTotals[1] = 0;
            dblGroupTotals[3] = 0;
            dblGroupTotals[5] = 0;
            dblGroupTotals[6] = 0;
            dblGroupTotals[7] = 0;
            dblGroupTotals[8] = 0;
        }

        private void GroupHeader1_Format(object sender, EventArgs e)
        {
        }

        private void ReportFooter_Format(object sender, EventArgs e)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                SetupTotals();
                if (boolRTError)
                {
                    lblRTError.Text = " *  - This account is a liened account. " + "\r\n" + " ** - The rate table or lien record number is missing for this account.";
                    lblRTError.Visible = true;
                    lblRTError.Top = lngExtraRows;
                    ReportFooter.Height = lblRTError.Top + lblRTError.Height;
                }
                else
                {
                    lblRTError.Visible = true;
                    lblRTError.Text = " ";
                    lblRTError.Top = lngExtraRows;
                    ReportFooter.Height = lblRTError.Top + lblRTError.Height;
                    lblRTError.Top = 0;
                    // Me.ReportFooter.Height = 700
                }
                lblSpecialTotal.Visible = false;
                lblSpecialText.Visible = false;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Footer Format", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void SetReportHeader()
        {
            int intCT;
            string strTemp = string.Empty;
            for (intCT = 0; intCT <= frmUTStatusList.InstancePtr.vsWhere.Rows - 1; intCT++)
            {
                if (frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) != "" || frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2) != "")
                {
                    if (intCT == frmUTStatusList.InstancePtr.lngRowAccount)
                    {
                        // Account Number
                        if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
                        {
                            if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
                            {
                                if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
                                {
                                    strTemp += "Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
                                }
                                else
                                {
                                    strTemp += "Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
                                }
                            }
                            else
                            {
                                strTemp += "Below Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
                            }
                        }
                        else
                        {
                            strTemp += "Above Account: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
                        }
                    }
                    else if (intCT == frmUTStatusList.InstancePtr.lngRowName)
                    {
                        // Name
                        if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
                        {
                            if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
                            {
                                if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) == Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)))
                                {
                                    strTemp += " Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " ";
                                }
                                else
                                {
                                    strTemp += " Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
                                }
                            }
                            else
                            {
                                strTemp += " Below Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
                            }
                        }
                        else
                        {
                            strTemp += " Above Name: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
                        }
                    }
                    else if (intCT == frmUTStatusList.InstancePtr.lngRowBill)
                    {
                        // Bill Number
                        if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) != "")
                        {
                            if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)) != "")
                            {
                                // strTemp = strTemp & " Tax Year: " & .TextMatrix(intCT, 1) & " To " & .TextMatrix(intCT, 2)
                                strTemp += " Bill: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
                                // MAL@20070831
                            }
                            else
                            {
                                // strTemp = strTemp & " Tax Year: " & .TextMatrix(intCT, 1)
                                strTemp += " Bill: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
                            }
                        }
                        else
                        {
                            // strTemp = strTemp & " Tax Year: " & .TextMatrix(intCT, 2)
                            strTemp += " Bill: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
                        }
                    }
                    else if (intCT == frmUTStatusList.InstancePtr.lngRowBalanceDue)
                    {
                        // Balance Due
                        strTemp += " Balance Due " + Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)) + " " + Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2));
                        // Case frmUTStatusList.lngRowAsOfDate
                        // strTemp = strTemp & " As Of Date : " & Trim(.TextMatrix(intCT, 1))
                    }
                    else if (intCT == frmUTStatusList.InstancePtr.lngRowPaymentType)
                    {
                        strTemp += " Payment Type : " + Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1));
                    }
                    else if (intCT == frmUTStatusList.InstancePtr.lngRowShowPaymentFrom)
                    {
                        strTemp += " Show Payments From: " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1) + " To " + frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
                    }
                    else
                    {
                        // Exit For
                    }
                }
            }
            if (Strings.Trim(strTemp) == "")
            {
                lblReportType.Text = "Complete List";
            }
            else
            {
                lblReportType.Text = strTemp;
            }
            lblReportType.Text = lblReportType.Text + " Showing :";
            if (boolPrincipal)
            {
                lblReportType.Text = lblReportType.Text + " Principal";
            }
            if (boolTaxes)
            {
                lblReportType.Text = lblReportType.Text + " Tax";
            }
            if (boolInterest)
            {
                lblReportType.Text = lblReportType.Text + " Interest";
            }
            if (boolCosts)
            {
                lblReportType.Text = lblReportType.Text + " Costs";
            }
            if (Strings.UCase(modUTStatusList.Statics.strReportType) == "ACCOUNT")
            {
                lblHeader.Text = "Collection Account Status List";
                // find the sort order
                strTemp = string.Empty;
                for (intCT = 0; intCT <= frmUTStatusList.InstancePtr.lstSort.Items.Count - 1; intCT++)
                {
                    if (frmUTStatusList.InstancePtr.lstSort.Selected(intCT))
                    {
                        strTemp += frmUTStatusList.InstancePtr.lstSort.Items[intCT].Text + ", ";
                    }
                }
                if (strTemp == "")
                {
                    strTemp = "Order By: Name, Account, Year";
                }
                else
                {
                    strTemp = "Order By: " + Strings.Left(strTemp, strTemp.Length - 2);
                }
                lblReportType.Text = lblReportType + "\r\n" + strTemp;
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "ABATE")
            {
                lblHeader.Text = "Abatement Status List";
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "DISCOUNT")
            {
                lblHeader.Text = "Discount Status List";
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "SUPPLEMENTAL")
            {
                lblHeader.Text = "Supplemental Status List";
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "TAXCLUB")
            {
                lblHeader.Text = "Tax Club Status List";
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "COSTS")
            {
                lblHeader.Text = "Lien Costs and 30 Day Notice List";
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "REFUNDEDABATE")
            {
                lblHeader.Text = "Refunded Abatement Status List";
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "PREPAYMENT")
            {
                lblHeader.Text = "Prepayment Status List";
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "PAYMENTS")
            {
                lblHeader.Text = "Payment Status List";
            }
            else if (Strings.UCase(modUTStatusList.Statics.strReportType) == "CORRECTIONS")
            {
                lblHeader.Text = "Correction Status List";
            }
            else
            {
                lblHeader.Text = "Status List";
            }
            if (modMain.Statics.gboolUTUseAsOfDate)
            {
                lblReportType.Text = lblReportType.Text + "\r\n" + "As Of Date: " + Strings.Format(modMain.Statics.gdtUTStatusListAsOfDate, "MM/dd/yyyy");
            }
            if (boolWater)
            {
                // kk trouts-6 02282013  Change Water to Stormwater for Bangor
                if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
                {
                    lblHeader.Text = "Stormwater " + lblHeader.Text;
                }
                else
                {
                    lblHeader.Text = "Water " + lblHeader.Text;
                }
            }
            else
            {
                lblHeader.Text = "Sewer " + lblHeader.Text;
            }
        }
        // vbPorter upgrade warning: intRNum As short	OnWriteFCConvert.ToInt32(
        // vbPorter upgrade warning: lngYear As int	OnWriteFCConvert.ToDouble(
        private void AddSummaryRow_24(int intRNum, double dblAmount, int lngYear)
        {
            AddSummaryRow(ref intRNum, ref dblAmount, ref lngYear);
        }

        private void AddSummaryRow(ref int intRNum, ref double dblAmount, ref int lngYear)
        {
            // this will add another per diem line in the report footer
            if (intRNum == 1)
            {
                fldSummary1.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                fldSummary1.Text = Strings.Format(dblAmount, CURRENCY_FORMAT);
                // MAL@20080129: Check for As of Date
                // Tracker Reference: 12076
                if (modMain.Statics.gboolUTUseAsOfDate)
                {
                    if (lngYear > 0)
                    {
                        if (FCConvert.ToDouble(GetBillingDate(ref lngYear)) > modMain.Statics.gdtUTStatusListAsOfDate.ToOADate())
                        {
                            lblSummary1.Text = "Not Billed";
                        }
                        else
                        {
                            lblSummary1.Text = GetBillingDate(ref lngYear);
                        }
                    }
                    else
                    {
                        lblSummary1.Text = "Not Billed";
                    }
                }
                else
                {
                    lblSummary1.Text = GetBillingDate(ref lngYear);
                }
                dblPDTotal += dblAmount;
            }
            else
            {
                GrapeCity.ActiveReports.SectionReportModel.TextBox obNew;
                // add a field
                //obNew = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
                obNew = ReportFooter.Controls["fldSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.TextBox;
                //obNew.Name = "fldSummary" + FCConvert.ToString(intRNum);
                obNew.Top = fldSummary1.Top + ((intRNum - 1) * fldSummary1.Height);
                obNew.Left = fldSummary1.Left;
                obNew.Width = fldSummary1.Width;
                obNew.Alignment = GrapeCity.ActiveReports.Document.Section.TextAlignment.Right;
                obNew.Font = fldSummary1.Font;
                // this sets the font to the same as the field that is already created
                obNew.Text = Strings.Format(dblAmount, CURRENCY_FORMAT);
                dblPDTotal += dblAmount;
                //FC:TEMP:SBE - controls should be added on ReportStart. We need the maximum number of summary rows, to complete the fix
                //this.ReportFooter.Controls.Add(obNew);
                obNew.Visible = true;
                // add a label
                GrapeCity.ActiveReports.SectionReportModel.Label obLabel;
                //obLabel = new GrapeCity.ActiveReports.SectionReportModel.Label();
                obLabel = ReportFooter.Controls["lblSummary" + FCConvert.ToString(intRNum)] as GrapeCity.ActiveReports.SectionReportModel.Label;
                //obLabel.Name = "lblSummary" + FCConvert.ToString(intRNum);
                if (obLabel != null)
                {
                    obLabel.Top = lblSummary1.Top + ((intRNum - 1) * lblSummary1.Height);
                    obLabel.Left = lblSummary1.Left;
                    obLabel.Font = fldSummary1.Font;

                    // this sets the font to the same as the field that is already created
                    // MAL@20080129: Check for As of Date
                    // Tracker Reference: 12076
                    if (modMain.Statics.gboolUTUseAsOfDate)
                    {
                        if (lngYear == 0)
                        {
                            // kk07272016 trouts-199  RateKey (lngYear) of zero returns "Not Billed", which can't be compared to the As Of Date
                            obLabel.Text = "Not Billed";
                        }
                        else
                            if (FCConvert.ToDouble(GetBillingDate(ref lngYear)) > modMain.Statics.gdtUTStatusListAsOfDate.ToOADate())
                            {
                                obLabel.Text = "Not Billed";
                            }
                            else
                            {
                                obLabel.Text = GetBillingDate(ref lngYear);
                            }
                    }
                    else
                    {
                        obLabel.Text = GetBillingDate(ref lngYear);
                    }

                    obLabel.Visible = true;
                }
            }
        }

        private void SetupTotalSummary()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill the summary labels at the bottom of the page
                // and hide/show the labels when needed
                short intCT;
                short intRow;
                // this will keep track of the row  that I am adding values to
                string strDesc = string.Empty;
                double[] dblTotal = new double[6 + 1];
                double dblSubTotal;
                bool boolAffectThis = false;
                bool boolShowSubtotal = false;
                // fill in the titles
                lblSumHeaderType.Text = "Type";
                lblSumHeaderPrin.Text = "Principal";
                lblSumHeaderTax.Text = "Tax";
                lblSumHeaderInt.Text = "Interest";
                lblSumHeaderCost.Text = "Costs";
                lblSumHeaderTotal.Text = "Total";
                intRow = 1;
                // start at the first row
                for (intCT = 0; intCT <= 10; intCT++)
                {
                    // this will fill the totals element
                    dblPayments[intCT, 4] = dblPayments[intCT, 0] + dblPayments[intCT, 1] + dblPayments[intCT, 2] + dblPayments[intCT, 3] + dblPayments[intCT, 5];
                }
                // first set of payments
                for (intCT = 6; intCT <= 10; intCT++)
                {
                    boolAffectThis = false;
                    if (dblPayments[intCT, 4] != 0)
                    {
                        switch (intCT)
                        {
                            case 6:
                                {
                                    strDesc = "P - Payment";
                                    boolAffectThis = true;
                                    boolShowSubtotal = true;
                                    break;
                                }
                            case 8:
                                {
                                    strDesc = "U - Tax Club";
                                    boolAffectThis = true;
                                    boolShowSubtotal = true;
                                    break;
                                }
                            case 9:
                                {
                                    strDesc = "X - DOS Correction";
                                    boolAffectThis = true;
                                    boolShowSubtotal = true;
                                    break;
                                }
                            case 10:
                                {
                                    strDesc = "Y - Prepayment";
                                    boolAffectThis = true;
                                    boolShowSubtotal = true;
                                    break;
                                }
                        }
                        //end switch
                        if (boolAffectThis)
                        {
                            FillSummaryLine(ref intRow, ref strDesc, ref dblPayments[intCT, 0], ref dblPayments[intCT, 5], ref dblPayments[intCT, 1], ref dblPayments[intCT, 2], ref dblPayments[intCT, 3], ref dblPayments[intCT, 4]);
                            dblTotal[0] += dblPayments[intCT, 0];
                            // this will total all of the seperated payments for the total line
                            dblTotal[1] += dblPayments[intCT, 1];
                            dblTotal[2] += dblPayments[intCT, 2];
                            dblTotal[3] += dblPayments[intCT, 3];
                            dblTotal[4] += dblPayments[intCT, 4];
                            dblTotal[5] += dblPayments[intCT, 5];
                            intRow += 1;
                        }
                    }
                }
                if (boolShowSubtotal)
                {
                    // create a subtotal line if needed
                    FillSummaryLine_6(intRow, "Subtotal", dblTotal[0], dblTotal[5], dblTotal[1], dblTotal[2], dblTotal[3], dblTotal[4]);
                    SetSummarySubTotalLine(ref intRow);
                    intRow += 1;
                }
                else
                {
                    lnSubtotal.Visible = false;
                }
                // the second set of payment types
                for (intCT = 0; intCT <= 7; intCT++)
                {
                    boolAffectThis = false;
                    if (dblPayments[intCT, 4] != 0)
                    {
                        switch (intCT)
                        {
                            case 0:
                                {
                                    strDesc = "3 - 30 DN Costs";
                                    boolAffectThis = true;
                                    break;
                                }
                            case 1:
                                {
                                    strDesc = "A - Abatement";
                                    boolAffectThis = true;
                                    break;
                                }
                            case 2:
                                {
                                    strDesc = "C - Correction";
                                    boolAffectThis = true;
                                    break;
                                }
                            case 3:
                                {
                                    strDesc = "D - Discount";
                                    boolAffectThis = true;
                                    break;
                                }
                            case 4:
                                {
                                    if (frmUTStatusList.InstancePtr.boolShowCurrentInterest)
                                    {
                                        strDesc = "I - Interest Charged";
                                        boolAffectThis = true;
                                    }
                                    break;
                                }
                            case 5:
                                {
                                    strDesc = "L - Lien Costs";
                                    boolAffectThis = true;
                                    break;
                                }
                            case 7:
                                {
                                    strDesc = "R - Refunded Abatement";
                                    boolAffectThis = true;
                                    break;
                                }
                        }
                        //end switch
                        if (boolAffectThis)
                        {
                            FillSummaryLine(ref intRow, ref strDesc, ref dblPayments[intCT, 0], ref dblPayments[intCT, 5], ref dblPayments[intCT, 1], ref dblPayments[intCT, 2], ref dblPayments[intCT, 3], ref dblPayments[intCT, 4]);
                            dblTotal[0] += dblPayments[intCT, 0];
                            // this will total all of the seperated payments for the total line
                            dblTotal[1] += dblPayments[intCT, 1];
                            dblTotal[2] += dblPayments[intCT, 2];
                            dblTotal[3] += dblPayments[intCT, 3];
                            dblTotal[4] += dblPayments[intCT, 4];
                            dblTotal[5] += dblPayments[intCT, 5];
                            intRow += 1;
                        }
                    }
                }
                // show the total line
                FillSummaryLine_6(intRow, "Total", dblTotal[0], dblTotal[5], dblTotal[1], dblTotal[2], dblTotal[3], dblTotal[4]);
                SetSummaryTotalLine(ref intRow);
                for (intCT = FCConvert.ToInt16(intRow + 1); intCT <= 11; intCT++)
                {
                    HideSummaryRow(ref intCT);
                }
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Summary Table", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }
        // vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
        private void SetSummaryTotalLine(ref short intRw)
        {
            switch (intRw)
            {
                case 1:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal1.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal1.Top;
                        break;
                    }
                case 2:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal2.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal2.Top;
                        break;
                    }
                case 3:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal3.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal3.Top;
                        break;
                    }
                case 4:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal4.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal4.Top;
                        break;
                    }
                case 5:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal5.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal5.Top;
                        break;
                    }
                case 6:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal6.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal6.Top;
                        break;
                    }
                case 7:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal7.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal7.Top;
                        break;
                    }
                case 8:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal8.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal8.Top;
                        break;
                    }
                case 9:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal9.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal9.Top;
                        break;
                    }
                case 10:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal10.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal10.Top;
                        break;
                    }
                case 11:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal11.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal11.Top;
                        break;
                    }
                case 12:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal12.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal12.Top;
                        break;
                    }
                case 13:
                    {
                        lnSummaryTotal.Y1 = lblSummaryTotal13.Top;
                        lnSummaryTotal.Y2 = lblSummaryTotal13.Top;
                        break;
                    }
            }
            //end switch
        }
        // vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
        private void SetSummarySubTotalLine(ref short intRw)
        {
            lnSubtotal.X1 = lblSumHeaderTotal.Left;
            lnSubtotal.X2 = lblSumHeaderTotal.Left + lblSumHeaderTotal.Width;
            switch (intRw)
            {
                case 1:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal1.Top;
                        lnSubtotal.Y2 = lblSummaryTotal1.Top;
                        break;
                    }
                case 2:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal2.Top;
                        lnSubtotal.Y2 = lblSummaryTotal2.Top;
                        break;
                    }
                case 3:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal3.Top;
                        lnSubtotal.Y2 = lblSummaryTotal3.Top;
                        break;
                    }
                case 4:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal4.Top;
                        lnSubtotal.Y2 = lblSummaryTotal4.Top;
                        break;
                    }
                case 5:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal5.Top;
                        lnSubtotal.Y2 = lblSummaryTotal5.Top;
                        break;
                    }
                case 6:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal6.Top;
                        lnSubtotal.Y2 = lblSummaryTotal6.Top;
                        break;
                    }
                case 7:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal7.Top;
                        lnSubtotal.Y2 = lblSummaryTotal7.Top;
                        break;
                    }
                case 8:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal8.Top;
                        lnSubtotal.Y2 = lblSummaryTotal8.Top;
                        break;
                    }
                case 9:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal9.Top;
                        lnSubtotal.Y2 = lblSummaryTotal9.Top;
                        break;
                    }
                case 10:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal10.Top;
                        lnSubtotal.Y2 = lblSummaryTotal10.Top;
                        break;
                    }
                case 11:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal11.Top;
                        lnSubtotal.Y2 = lblSummaryTotal11.Top;
                        break;
                    }
                case 12:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal12.Top;
                        lnSubtotal.Y2 = lblSummaryTotal12.Top;
                        break;
                    }
                case 13:
                    {
                        lnSubtotal.Y1 = lblSummaryTotal13.Top;
                        lnSubtotal.Y2 = lblSummaryTotal13.Top;
                        break;
                    }
            }
            //end switch
            lnSubtotal.Visible = true;
        }
        // vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
        private void FillSummaryLine_6(short intRw, string strDescription, double dblPrin, double dblTax, double dblPLI, double dblCurInt, double dblCosts, double dblTotal, bool boolTotalLine = false)
        {
            FillSummaryLine(ref intRw, ref strDescription, ref dblPrin, ref dblTax, ref dblPLI, ref dblCurInt, ref dblCosts, ref dblTotal, boolTotalLine);
        }

        private void FillSummaryLine(ref short intRw, ref string strDescription, ref double dblPrin, ref double dblTax, ref double dblPLI, ref double dblCurInt, ref double dblCosts, ref double dblTotal, bool boolTotalLine = false)
        {
            // this routine will fill in the line summary row
            switch (intRw)
            {
                case 1:
                    {
                        lblSummaryPaymentType1.Text = strDescription;
                        lblSumPrin1.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax1.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt1.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost1.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal1.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 2:
                    {
                        lblSummaryPaymentType2.Text = strDescription;
                        lblSumPrin2.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax2.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt2.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost2.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal2.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 3:
                    {
                        lblSummaryPaymentType3.Text = strDescription;
                        lblSumPrin3.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax3.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt3.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost3.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal3.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 4:
                    {
                        lblSummaryPaymentType4.Text = strDescription;
                        lblSumPrin4.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax4.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt4.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost4.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal4.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 5:
                    {
                        lblSummaryPaymentType5.Text = strDescription;
                        lblSumPrin5.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax5.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt5.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost5.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal5.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 6:
                    {
                        lblSummaryPaymentType6.Text = strDescription;
                        lblSumPrin6.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax6.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt6.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost6.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal6.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 7:
                    {
                        lblSummaryPaymentType7.Text = strDescription;
                        lblSumPrin7.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax7.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt7.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost7.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal7.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 8:
                    {
                        lblSummaryPaymentType8.Text = strDescription;
                        lblSumPrin8.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax8.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt8.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost8.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal8.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 9:
                    {
                        lblSummaryPaymentType9.Text = strDescription;
                        lblSumPrin9.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax9.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt9.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost9.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal9.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 10:
                    {
                        lblSummaryPaymentType10.Text = strDescription;
                        lblSumPrin10.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax10.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt10.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost10.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal10.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 11:
                    {
                        lblSummaryPaymentType11.Text = strDescription;
                        lblSumPrin11.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax11.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt11.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost11.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal11.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 12:
                    {
                        lblSummaryPaymentType12.Text = strDescription;
                        lblSumPrin12.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax12.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt12.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost12.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal12.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
                case 13:
                    {
                        lblSummaryPaymentType13.Text = strDescription;
                        lblSumPrin13.Text = Strings.Format(dblPrin, CURRENCY_FORMAT);
                        lblSumTax13.Text = Strings.Format(dblTax, CURRENCY_FORMAT);
                        lblSumInt13.Text = Strings.Format(dblPLI + dblCurInt, CURRENCY_FORMAT);
                        lblSumCost13.Text = Strings.Format(dblCosts, CURRENCY_FORMAT);
                        lblSummaryTotal13.Text = Strings.Format(dblTotal, CURRENCY_FORMAT);
                        break;
                    }
            }
            //end switch
        }

        private void AddToPaymentArray_2(int lngIndex, double dblPrin, double dblTax, double dblPLI, double dblCurInt, double dblCost)
        {
            AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblTax, ref dblPLI, ref dblCurInt, ref dblCost);
        }

        private void AddToPaymentArray_164(int lngIndex, double dblPrin, double dblTax, double dblPLI, double dblCurInt, double dblCost)
        {
            AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblTax, ref dblPLI, ref dblCurInt, ref dblCost);
        }

        private void AddToPaymentArray_650(int lngIndex, double dblPrin, double dblTax, double dblPLI, double dblCurInt, double dblCost)
        {
            AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblTax, ref dblPLI, ref dblCurInt, ref dblCost);
        }

        private void AddToPaymentArray_728(int lngIndex, double dblPrin, double dblTax, double dblPLI, double dblCurInt, double dblCost)
        {
            AddToPaymentArray(ref lngIndex, ref dblPrin, ref dblTax, ref dblPLI, ref dblCurInt, ref dblCost);
        }

        private void AddToPaymentArray(ref int lngIndex, ref double dblPrin, ref double dblTax, ref double dblPLI, ref double dblCurInt, ref double dblCost)
        {
            dblPayments[lngIndex, 0] += dblPrin;
            dblPayments[lngIndex, 5] += dblTax;
            dblPayments[lngIndex, 1] += dblPLI;
            dblPayments[lngIndex, 2] += dblCurInt;
            dblPayments[lngIndex, 3] += dblCost;
            dblBPayments[lngIndex, 0] += dblPrin;
            dblBPayments[lngIndex, 5] += dblTax;
            dblBPayments[lngIndex, 1] += dblPLI;
            dblBPayments[lngIndex, 2] += dblCurInt;
            dblBPayments[lngIndex, 3] += dblCost;
            dblPaymentsRev[lngIndex, 0] += dblPrin;
            dblPaymentsRev[lngIndex, 5] += dblTax;
            dblPaymentsRev[lngIndex, 1] += dblPLI;
            dblPaymentsRev[lngIndex, 2] += dblCurInt;
            dblPaymentsRev[lngIndex, 3] += dblCost;
            dblBPaymentsRev[lngIndex, 0] += dblPrin;
            dblBPaymentsRev[lngIndex, 5] += dblTax;
            dblBPaymentsRev[lngIndex, 1] += dblPLI;
            dblBPaymentsRev[lngIndex, 2] += dblCurInt;
            dblBPaymentsRev[lngIndex, 3] += dblCost;
        }
        // vbPorter upgrade warning: intRw As short	OnWriteFCConvert.ToInt32(
        private void HideSummaryRow(ref short intRw)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int intCT;
                for (intCT = intRw; intCT <= 11; intCT++)
                {
                    switch (intRw)
                    {
                        case 1:
                            {
                                lblSummaryPaymentType1.Visible = false;
                                lblSumPrin1.Visible = false;
                                lblSumTax1.Visible = false;
                                lblSumInt1.Visible = false;
                                lblSumCost1.Visible = false;
                                lblSummaryTotal1.Visible = false;
                                break;
                            }
                        case 2:
                            {
                                lblSummaryPaymentType2.Visible = false;
                                lblSumPrin2.Visible = false;
                                lblSumTax2.Visible = false;
                                lblSumInt2.Visible = false;
                                lblSumCost2.Visible = false;
                                lblSummaryTotal2.Visible = false;
                                break;
                            }
                        case 3:
                            {
                                lblSummaryPaymentType3.Visible = false;
                                lblSumPrin3.Visible = false;
                                lblSumTax3.Visible = false;
                                lblSumInt3.Visible = false;
                                lblSumCost3.Visible = false;
                                lblSummaryTotal3.Visible = false;
                                break;
                            }
                        case 4:
                            {
                                lblSummaryPaymentType4.Visible = false;
                                lblSumPrin4.Visible = false;
                                lblSumTax4.Visible = false;
                                lblSumInt4.Visible = false;
                                lblSumCost4.Visible = false;
                                lblSummaryTotal4.Visible = false;
                                break;
                            }
                        case 5:
                            {
                                lblSummaryPaymentType5.Visible = false;
                                lblSumPrin5.Visible = false;
                                lblSumTax5.Visible = false;
                                lblSumInt5.Visible = false;
                                lblSumCost5.Visible = false;
                                lblSummaryTotal5.Visible = false;
                                break;
                            }
                        case 6:
                            {
                                lblSummaryPaymentType6.Visible = false;
                                lblSumPrin6.Visible = false;
                                lblSumTax6.Visible = false;
                                lblSumInt6.Visible = false;
                                lblSumCost6.Visible = false;
                                lblSummaryTotal6.Visible = false;
                                break;
                            }
                        case 7:
                            {
                                lblSummaryPaymentType7.Visible = false;
                                lblSumPrin7.Visible = false;
                                lblSumTax7.Visible = false;
                                lblSumInt7.Visible = false;
                                lblSumCost7.Visible = false;
                                lblSummaryTotal7.Visible = false;
                                break;
                            }
                        case 8:
                            {
                                lblSummaryPaymentType8.Visible = false;
                                lblSumPrin8.Visible = false;
                                lblSumTax8.Visible = false;
                                lblSumInt8.Visible = false;
                                lblSumCost8.Visible = false;
                                lblSummaryTotal8.Visible = false;
                                break;
                            }
                        case 9:
                            {
                                lblSummaryPaymentType9.Visible = false;
                                lblSumPrin9.Visible = false;
                                lblSumTax9.Visible = false;
                                lblSumInt9.Visible = false;
                                lblSumCost9.Visible = false;
                                lblSummaryTotal9.Visible = false;
                                break;
                            }
                        case 10:
                            {
                                lblSummaryPaymentType10.Visible = false;
                                lblSumPrin10.Visible = false;
                                lblSumTax10.Visible = false;
                                lblSumInt10.Visible = false;
                                lblSumCost10.Visible = false;
                                lblSummaryTotal10.Visible = false;
                                break;
                            }
                        case 11:
                            {
                                lblSummaryPaymentType11.Visible = false;
                                lblSumPrin11.Visible = false;
                                lblSumTax11.Visible = false;
                                lblSumInt11.Visible = false;
                                lblSumCost11.Visible = false;
                                lblSummaryTotal11.Visible = false;
                                break;
                            }
                        case 12:
                            {
                                lblSummaryPaymentType12.Visible = false;
                                lblSumPrin12.Visible = false;
                                lblSumTax12.Visible = false;
                                lblSumInt12.Visible = false;
                                lblSumCost12.Visible = false;
                                lblSummaryTotal12.Visible = false;
                                break;
                            }
                        case 13:
                            {
                                lblSummaryPaymentType13.Visible = false;
                                lblSumPrin13.Visible = false;
                                lblSumTax13.Visible = false;
                                lblSumInt13.Visible = false;
                                lblSumCost13.Visible = false;
                                lblSummaryTotal13.Visible = false;
                                break;
                            }
                    }
                    //end switch
                }
                if (!boolAdjustedSummary)
                {
                    SetYearSummaryTop_2(lblSummaryPaymentType1.Top + (intRw * lblSummaryPaymentType1.Height) + 300 / 1440f);
                    boolAdjustedSummary = true;
                }
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Hiding Summary Rows", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void SetYearSummaryTop_2(float lngTop)
        {
            SetYearSummaryTop(ref lngTop);
        }

        private void SetYearSummaryTop(ref float lngTop)
        {
            // this will start the year summary at the right height
            lblSummary.Top = lngTop;
            Line1.Y1 = lngTop + lblSummary.Height;
            Line1.Y2 = lngTop + lblSummary.Height;
            lblSummary1.Top = lngTop + lblSummary.Height;
            fldSummary1.Top = lngTop + lblSummary.Height;
        }

        private void ReversePaymentsFromStatusArray(ref clsDRWrapper rsRev)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                int counter;
                int counter2;

                for (counter = 0; counter <= 11; counter++)
                {
                    for (counter2 = 0; counter2 <= 6; counter2++)
                    {
                        dblPayments[counter, counter2] -= dblPaymentsRev[counter, counter2];
                        dblBPayments[counter, counter2] -= dblBPaymentsRev[counter, counter2];
                        dblPaymentsRev[counter, counter2] = 0;
                        dblBPaymentsRev[counter, counter2] = 0;
                    }
                }
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Reversing Payment Counts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
        }

        private void ResetReversalValues()
        {
            int counter;
            int counter2;
            for (counter = 0; counter <= 11; counter++)
            {
                for (counter2 = 0; counter2 <= 6; counter2++)
                {
                    dblPaymentsRev[counter, counter2] = 0;
                    dblBPaymentsRev[counter, counter2] = 0;
                }
            }
        }

        private string GetBillingDate_2(int lngRK)
        {
            return GetBillingDate(ref lngRK);
        }

        private string GetBillingDate(ref int lngRK)
        {
            string GetBillingDate = string.Empty;
            try
            {
                // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsRK = new clsDRWrapper();
                rsRK.OpenRecordset("SELECT BillDate FROM RateKeys WHERE ID = " + FCConvert.ToString(lngRK), modExtraModules.strUTDatabase);
                if (rsRK.EndOfFile())
                {
                    GetBillingDate = "Not Billed";
                }
                else
                {
                    GetBillingDate = Strings.Format(rsRK.Get_Fields_DateTime("BillDate"), "MM/dd/yyyy");
                }
                return GetBillingDate;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Reversing Payment Counts", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                StaticSettings.GlobalTelemetryService.TrackException(ex);
            }
            return GetBillingDate;
        }

        private void HideNameField_2(bool boolHide)
        {
            HideNameField(ref boolHide);
        }

        private void HideNameField(ref bool boolHide)
        {
            int intCT = 0;
            if (boolHide)
            {
                fldName.Visible = false;
                fldAccount.Visible = false;
                fldBillDate.Top = 0;
                fldBillNum.Top = 0;
                fldTaxDue.Top = 0;
                fldPaymentReceived.Top = 0;
                fldDue.Top = 0;
                fldPrincipal.Top = 0;
                fldTax.Top = 0;
                fldInterest.Top = 0;
                fldCosts.Top = 0;
                fldBook.Top = 0;
                fldBook.Text = string.Empty;
                fldTADate.Text = string.Empty;
                fldTADate.Visible = false;
                intCT = 1;
                if (fldLocation.Visible)
                {
                    lblLocation.Top = 270 / 1440f;
                    fldLocation.Top = 270 / 1440f;
                    intCT += 1;
                }
                if (fldMapLot.Visible)
                {
                    lblMapLot.Top = 540 / 1440f;
                    fldMapLot.Top = 540 / 1440f;
                    intCT += 1;
                }
                fldLienRecord.Visible = false;
                // MAL@20080122: 12024
            }
            else
            {
                fldName.Visible = true;
                fldAccount.Visible = true;
                fldBillDate.Top = 270 / 1440f;
                fldBillNum.Top = 270 / 1440f;
                fldTaxDue.Top = 270 / 1440f;
                fldPaymentReceived.Top = 270 / 1440f;
                fldDue.Top = 270 / 1440f;
                fldPrincipal.Top = 270 / 1440f;
                fldTax.Top = 270 / 1440f;
                fldInterest.Top = 270 / 1440f;
                fldCosts.Top = 270 / 1440f;
                fldBook.Top = 270 / 1440f;
                intCT = 2;
                if (fldLocation.Visible)
                {
                    lblLocation.Top = 540 / 1440f;
                    fldLocation.Top = 540 / 1440f;
                    intCT += 1;
                }
                if (fldMapLot.Visible)
                {
                    lblMapLot.Top = 810 / 1440f;
                    fldMapLot.Top = 810 / 1440f;
                    intCT += 1;
                }
                fldLienRecord.Top = 270 / 1440f;
                // MAL@20080122: 12024
            }
            // Me.Detail.Height = 320
            Detail.Height = ((270 * intCT) + 50) / 1440f;
        }

        private void FixThisBill(ref int lngBK)
        {
            clsDRWrapper rsB = new clsDRWrapper();
            int intCT;
            int intCol = 0;
            double dblAmt = 0;
            rsB.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBK), modExtraModules.strUTDatabase);
            if (!rsB.EndOfFile())
            {
                rsB.Edit();
                intCol = 0;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    dblAmt += dblBPayments[intCT, intCol];
                }
                rsB.Set_Fields(strWS + "PrinPaid", dblAmt);
                intCol = 2;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    if (intCT != 4)
                    {
                        dblAmt += dblBPayments[intCT, intCol];
                        // + dblBPayments(intCT, 4)
                    }
                }
                rsB.Set_Fields(strWS + "IntPaid", dblAmt);
                intCol = 2;
                dblAmt = 0;
                dblAmt = dblBPayments[4, intCol];
                rsB.Set_Fields(strWS + "IntAdded", dblAmt);
                intCol = 5;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    dblAmt += dblBPayments[intCT, intCol];
                }
                rsB.Set_Fields(strWS + "TaxPaid", dblAmt);
                intCol = 3;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    if (intCT != 3)
                    {
                        dblAmt += dblBPayments[intCT, intCol];
                    }
                }
                rsB.Set_Fields(strWS + "CostPaid", dblAmt);
                rsB.Update();
            }
        }

        private void FixThisLien(ref int lngLN)
        {
            clsDRWrapper rsB = new clsDRWrapper();
            int intCT;
            int intCol = 0;
            double dblAmt = 0;
            rsB.OpenRecordset("SELECT * FROM Lien WHERE ID = " + FCConvert.ToString(lngLN), modExtraModules.strUTDatabase);
            if (!rsB.EndOfFile())
            {
                rsB.Edit();
                intCol = 0;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    dblAmt += dblBPayments[intCT, intCol];
                }
                rsB.Set_Fields("PrinPaid", dblAmt);
                intCol = 2;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    if (intCT != 4)
                    {
                        dblAmt += dblBPayments[intCT, intCol];
                    }
                }
                rsB.Set_Fields("IntPaid", dblAmt);
                intCol = 1;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    if (intCT != 4)
                    {
                        dblAmt += dblBPayments[intCT, intCol];
                    }
                }
                rsB.Set_Fields("PLIPaid", dblAmt);
                intCol = 2;
                dblAmt = 0;
                dblAmt = dblBPayments[4, intCol];
                rsB.Set_Fields("IntAdded", dblAmt);
                intCol = 5;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    dblAmt += dblBPayments[intCT, intCol];
                }
                rsB.Set_Fields("TaxPaid", dblAmt);
                intCol = 3;
                dblAmt = 0;
                for (intCT = 0; intCT <= 9; intCT++)
                {
                    if (intCT != 3)
                    {
                        dblAmt += dblBPayments[intCT, intCol];
                    }
                }
                rsB.Set_Fields("CostPaid", dblAmt);
                rsB.Update();
            }
        }
        // vbPorter upgrade warning: intindex As object	OnWriteFCConvert.ToInt16(
        private double GetBillPaymentAmount_2(int intindex)
        {
            return GetBillPaymentAmount(ref intindex);
        }

        private double GetBillPaymentAmount(ref int intindex)
        {
            double GetBillPaymentAmount = 0;
            int intCT;
            for (intCT = 0; intCT <= 10; intCT++)
            {
                GetBillPaymentAmount += dblBPayments[intCT, intindex];
            }
            return GetBillPaymentAmount;
        }
        // vbPorter upgrade warning: lngKey As int	OnWriteFCConvert.ToDouble(
        private void AddYearTotal(int lngKey, double dblAmount)
        {
            if (dcYearTotals.ContainsKey(lngKey))
            {
                dcYearTotals[lngKey] = FCConvert.ToDouble(dcYearTotals[lngKey]) + dblAmount;
            }
            else
            {
                SetYearTotal(lngKey, dblAmount);
            }
        }

        private void SetYearTotal(int lngKey, double dblAmount)
        {
            if (dcYearTotals.ContainsKey(lngKey))
            {
                dcYearTotals[lngKey] = dblAmount;
            }
            else
            {
                dcYearTotals.Add(lngKey, dblAmount);
            }
        }
        // vbPorter upgrade warning: 'Return' As double	OnWrite(object, short)
        private double GetYearTotal(int lngKey)
        {
            double GetYearTotal = 0;
            if (dcYearTotals.ContainsKey(lngKey))
            {
                GetYearTotal = dcYearTotals[lngKey];
            }
            else
            {
                GetYearTotal = 0;
            }
            return GetYearTotal;
        }

        private void arUTStatusLists_Load(object sender, System.EventArgs e)
        {
        }
    }
}
