﻿using SharedApplication.Messaging;
using SharedApplication.UtilityBilling.Commands;

namespace TWUT0000
{
    public class ShowUTAccountMasterHandler : CommandHandler<ShowUTAccountMaster>
    {
        protected override void Handle(ShowUTAccountMaster command)
        {
            frmAccountMaster.InstancePtr.Init(command.Id,command.Account);
        }
    }
}