//Fecher vbPorter - Version 1.0.0.90
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;

using SharedApplication.Extensions;

namespace TWUT0000
{
	public class clsACHFileControllerUT
	{

		//=========================================================

		private string strLastError = "";

		public string LastError
		{
			get
			{
					string LastError = "";
				LastError = strLastError;
				return LastError;
			}
		}

		public clsACHFileInfo NewFileInfo()
		{
			clsACHFileInfo NewFileInfo = null;
			clsACHFileInfo tInfo = new/*AsNew*/ clsACHFileInfo();
			NewFileInfo = tInfo;
			return NewFileInfo;
		}

		public bool FillFileInfo(ref clsACHFile OutFile)
		{
			bool FillFileInfo = false;
			try
			{	// On Error GoTo ErrorHandler
				OutFile.FileInfo.EffectiveEntryDate = DateTime.Now.FormatAndPadShortDate();
				clsACHSetup tSetup = new/*AsNew*/ clsACHSetup();
				clsACHSetupControllerUT tCon = new/*AsNew*/ clsACHSetupControllerUT();
				strLastError = "";
				if (!tCon.Load(ref tSetup)) {
					strLastError = "Unable to load ACH setup information.";
					return FillFileInfo;
				}

				OutFile.FileInfo.OriginInfo.ImmediateOriginName = tSetup.ImmediateOriginName;
				OutFile.FileInfo.OriginInfo.ImmediateOriginRT = tSetup.ImmediateOriginRT;
				OutFile.FileInfo.OriginInfo.OriginatingDFI = tSetup.ImmediateOriginODFI;
				OutFile.FileInfo.CompanyInfo.CompanyName = tSetup.CompanyName;
				OutFile.FileInfo.CompanyInfo.CompanyAccount = tSetup.CompanyAccount;
				OutFile.FileInfo.CompanyInfo.CompanyRT = tSetup.CompanyRT;
				OutFile.FileInfo.CompanyInfo.CompanyID = tSetup.CompanyID;
				OutFile.FileInfo.CompanyInfo.AccountType = tSetup.CompanyAccountType.ToString();
				OutFile.FileInfo.DestinationInfo.ACHBankName = tSetup.ImmediateDestinationName;
				OutFile.FileInfo.DestinationInfo.ACHBankRT = tSetup.ImmediateDestinationRT;

				FillFileInfo = true;
				return FillFileInfo;
			}
			catch(Exception ex)
			{	// ErrorHandler:
                strLastError = ex.Message;
            }
			return FillFileInfo;
		}

		public bool FillHeaderRec(ref clsACHFile OutFile)
		{
			bool FillHeaderRec = false;
			try
			{	// On Error GoTo ErrorHandler
				strLastError = "";
				OutFile.FileHeader.FileIDModifier = "1";
				OutFile.FileHeader.ImmediateDestination = OutFile.FileInfo.DestinationInfo.ACHBankRT;
				OutFile.FileHeader.ImmediateDestinationName = OutFile.FileInfo.DestinationInfo.ACHBankName;
				OutFile.FileHeader.ImmediateOriginRT = OutFile.FileInfo.OriginInfo.ImmediateOriginRT;
				OutFile.FileHeader.ImmediateOriginName = OutFile.FileInfo.OriginInfo.ImmediateOriginName;
				FillHeaderRec = true;
				return FillHeaderRec;
			}
			catch (Exception ex)
			{	// ErrorHandler:
                strLastError = ex.Message;
            }
			return FillHeaderRec;
		}

		public bool FillBatchRecs(ref clsACHFile OutFile, ref clsACHBatch tBatch)
		{
			bool FillBatchRecs = false;
			try
			{	// On Error GoTo ErrorHandler
				strLastError = "";
				tBatch.BatchHeader.Balanced = true;
				tBatch.BatchHeader.CompanyID = OutFile.FileInfo.CompanyInfo.CompanyID;
				tBatch.BatchHeader.CompanyName = OutFile.FileInfo.CompanyInfo.CompanyName;
				tBatch.BatchHeader.EffectiveEntryDate = OutFile.FileInfo.EffectiveEntryDate;
				tBatch.BatchHeader.DescriptiveDate = DateTime.Now.FormatAndPadShortDate();
				tBatch.BatchHeader.EntryDescription = "SEWER BILL";
				tBatch.BatchHeader.OriginatingDFI = OutFile.FileInfo.OriginInfo.OriginatingDFI;
				tBatch.BatchHeader.BatchNumber = tBatch.BatchNumber;
				tBatch.BatchControl.Balanced = true;
				tBatch.BatchControl.CompanyID = OutFile.FileInfo.CompanyInfo.CompanyID;
				tBatch.BatchControl.OriginatingDFI = OutFile.FileInfo.OriginInfo.OriginatingDFI;
				FillBatchRecs = true;
				return FillBatchRecs;
			}
			catch (Exception ex)
			{	// ErrorHandler:
                strLastError = ex.Message;
            }
			return FillBatchRecs;
		}

		public bool WriteACHFile(ref clsACHFile OutFile)
		{
			bool WriteACHFile = false;
            bool boolOpen = false;
            try
			{	// On Error GoTo ErrorHandler
				strLastError = "";
				short intRecNum;
				
				intRecNum = 0;
				//FCFCFileSystemObject fso = new/*AsNew*/ FCFCFileSystemObject();
				//TextStream ts;
				//ts = fso.CreateTextFile(OutFile.FileInfo.FileName, true, false);
                var ts = File.CreateText(OutFile.FileInfo.FileName);
				boolOpen = true;
				string strReturn;
				strReturn = "";

				strReturn = fecherFoundation.Strings.UCase(OutFile.FileHeader.OutputLine());
				ts.WriteLine(strReturn);
				IEnumerable<clsACHBatch> tBatches = new List<clsACHBatch>();
				// create batch header
				tBatches = OutFile.Batches;
				if (!(tBatches==null)) {
					foreach (clsACHBatch tBatch in tBatches) {
						strReturn = fecherFoundation.Strings.UCase(tBatch.BatchHeader.OutputLine());
						ts.WriteLine(strReturn);
						foreach (clsACHDetailRecord tDet in tBatch.Details()) {
							strReturn = fecherFoundation.Strings.UCase(tDet.OutputLine());
							if (strReturn!="") {
								ts.WriteLine(strReturn);
							} else {
								strLastError = tDet.LastError;
								ts.Close();
								return WriteACHFile;
							}
						} // tDet
						strReturn = tBatch.BatchControl.OutputLine();
						ts.WriteLine(strReturn);
					} // tBatch
				} else {
					strLastError = "No batches found.";
					ts.Close();
					return WriteACHFile;
				}
				strReturn = fecherFoundation.Strings.UCase(OutFile.FileControl.OutputLine());
				ts.WriteLine(strReturn);
				// fillers
				foreach (clsACHBlockFiller tFill in OutFile.Fillers()) {
					strReturn = fecherFoundation.Strings.UCase(tFill.OutputLine());
					ts.WriteLine(strReturn);
				} // tFill
				boolOpen = false;
				ts.Close();
				WriteACHFile = true;
				return WriteACHFile;
			}
			catch (Exception ex)
			{	// ErrorHandler:
                strLastError = ex.Message;
            }
			return WriteACHFile;
		}

		// vbPorter upgrade warning: dtEntryDate As DateTime	OnWrite(string)
		public clsACHFile CreateACHFile(bool boolRegular, bool boolPrenotes, bool boolDD, bool boolTandA, bool boolBalanced, DateTime dtEntryDate, string strBatchNumber, bool boolForcePrenote)
		{
			clsACHFile CreateACHFile = null;
			strLastError = "";
			try
			{	// On Error GoTo ErrorHandler
				clsACHFile OutFile = new/*AsNew*/ clsACHFile();
				OutFile.FileInfo.Balanced = boolBalanced;

				OutFile.FileInfo.Regular = boolRegular;
				OutFile.FileInfo.PreNote = boolPrenotes;
				OutFile.FileInfo.ForcePreNote = boolForcePrenote;
				if (boolForcePrenote) {
					OutFile.FileInfo.Regular = false;
					OutFile.FileInfo.PreNote = true;
				}
				if (!FillFileInfo(ref OutFile)) {
					return CreateACHFile;
				}
				OutFile.FileInfo.EffectiveEntryDate = FCConvert.ToString(dtEntryDate);
				if (!FillHeaderRec(ref OutFile)) {
					return CreateACHFile;
				}
				clsACHBatch tBat = new/*AsNew*/ clsACHBatch();

				OutFile.AddBatch(ref tBat); // kk switched these 2 lines to get the batch number in the header
				FillBatchRecs(ref OutFile, ref tBat);

				if (boolDD) {
				}

				if (!CreateACHDetails(ref OutFile, strBatchNumber)) {
					return CreateACHFile;
				}

				clsACHDetailRecord tOff = new/*AsNew*/ clsACHDetailRecord();
				tOff.PreNote = boolPrenotes || boolForcePrenote;
				tOff.BankID = OutFile.FileInfo.CompanyInfo.CompanyRT;
				tOff.AccountNumber = OutFile.FileInfo.CompanyInfo.CompanyAccount;
				tOff.AccountType = OutFile.FileInfo.CompanyInfo.AccountType;
				tOff.EntryType = clsACHDetailRecord.ACHDetailEntryType.ACHDetailEntryTypeCredit;
				tOff.Name = OutFile.FileInfo.CompanyInfo.CompanyName;
				if (!tOff.PreNote) {
					tOff.TotalAmount = tBat.CalcBatchTotalAmount();
				} else {
					tOff.TotalAmount = 0;
				}
				tOff.Identifier = OutFile.FileInfo.CompanyInfo.CompanyID;
				tOff.ImmediateOriginRT = OutFile.FileInfo.OriginInfo.OriginatingDFI;
				tBat.AddDetail(ref tOff);

				tBat.BuildBatchControlRecord();

				OutFile.BuildFileControl();
				CreateACHFile = OutFile;

				return CreateACHFile;
			}
			catch (Exception ex)
			{	// ErrorHandler:
                strLastError = ex.Message;
            }
			return CreateACHFile;
		}

		// vbPorter upgrade warning: strBatchNumber As object	OnWrite(string)
		public bool CreateACHDetails(ref clsACHFile OutFile, string strBatchNumber)
		{
			bool CreateACHDetails = false;
			strLastError = "";
			try
			{	// On Error GoTo ErrorHandler
				string strSQL = "";
				string strWhere = "";
				clsDRWrapper rsLoad = new/*AsNew*/ clsDRWrapper();
				clsDRWrapper rsTemp = new/*AsNew*/ clsDRWrapper();
				if ((OutFile.FileInfo.Regular && !OutFile.FileInfo.PreNote) || OutFile.FileInfo.ForcePreNote) {
					strSQL = "SELECT tblautopay.id as AutoPayID,* FROM tblAutoPay INNER JOIN tblAcctACH ON tblAutoPay.AcctACHID = tblAcctACH.ID WHERE tblAutoPay.Service = 'S' AND ACHActive = 1 AND NOT ACHPreNote = 1 AND ACHFlag = 1 AND NOT ACHDone = 1 AND BatchNumber = '"+strBatchNumber+"'"; // trouts 266
				} else { // just prenotes
					strSQL = "SELECT tblautopay.id as AutoPayID,* FROM tblAutoPay INNER JOIN tblAcctACH ON tblAutoPay.AcctACHID = tblAcctACH.ID WHERE tblAutoPay.Service = 'S' AND ACHActive = 1 AND ACHPreNote = 1 AND ACHFlag = 1 AND NOT ACHDone = 1 AND BatchNumber = '"+strBatchNumber+"'"; // trouts 266
				}
				rsLoad.OpenRecordset(strSQL, "twut0000.vb1");
				if (!rsLoad.EndOfFile()) {
					clsACHBatch tBatch;
					clsACHDetailRecord tRec;

                    tBatch = OutFile.Batches.Last(); // OutFile.Batches[OutFile.Batches.Count()];
					while (!rsLoad.EndOfFile()) {
						tRec = new clsACHDetailRecord();

						tRec.PreNote = rsLoad.Get_Fields("ACHPreNote") || OutFile.FileInfo.ForcePreNote;
						tRec.BankID = FCConvert.ToString(rsLoad.Get_Fields("ACHBankRT"));
						tRec.AccountNumber = FCConvert.ToString(rsLoad.Get_Fields("ACHAcctNumber"));
						tRec.AccountType = FCConvert.ToString(rsLoad.Get_Fields("ACHAcctType"));
						tRec.EntryType = clsACHDetailRecord.ACHDetailEntryType.ACHDetailEntryTypeDebit;
						tRec.Name = FCConvert.ToString(rsLoad.Get_Fields("Name"));
						if (!tRec.PreNote) {
							tRec.TotalAmount = fecherFoundation.Conversion.Val(FCConvert.ToString(rsLoad.Get_Fields("ACHAmount")));
						} else {
							tRec.TotalAmount = 0;
						}
						tRec.Identifier = FCConvert.ToString(rsLoad.Get_Fields("AccountNumber"));
						tRec.ImmediateOriginRT = OutFile.FileHeader.ImmediateOriginRT;
						tBatch.AddDetail(ref tRec);

						rsTemp.Execute("UPDATE tblAutoPay SET LineNumber = "+FCConvert.ToString(tRec.RecordNumber)+" WHERE ID = "+rsLoad.Get_Fields("AutoPayID"), "twut0000.vb1"); // trouts-261
						rsLoad.MoveNext();
					}
				}

				CreateACHDetails = true;
				return CreateACHDetails;
			}
			catch (Exception ex)
			{	// ErrorHandler:
                strLastError = ex.Message;
            }
			return CreateACHDetails;
		}

	}
}
