﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTPrintLienDates.
	/// </summary>
	public partial class frmUTPrintLienDates : BaseForm
	{
		public frmUTPrintLienDates()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmUTPrintLienDates InstancePtr
		{
			get
			{
				return (frmUTPrintLienDates)Sys.GetInstance(typeof(frmUTPrintLienDates));
			}
		}

		protected frmUTPrintLienDates _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               11/15/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/15/2005              *
		// ********************************************************
		clsDRWrapper rsRate = new clsDRWrapper();
		bool boolDirty;
		bool boolLoaded;
		bool boolListing;

		private void frmUTPrintLienDates_Activated(object sender, System.EventArgs e)
		{
			if (!boolLoaded)
			{
				FillComboBoxes();
				cmbPrint.Items.Add("All Eligible Rate Keys");
				cmbPrint.Items.Add("One Rate Key");
				cmbPrint.Items.Add("One Year");
				cmbPrint.Items.Add("Anything Currently Pending");
				cmbPrint.Items.Add("Anything Pending within 30 Days");
				boolLoaded = true;
			}
		}

		private void frmUTPrintLienDates_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.F10:
					{
						KeyCode = (Keys)0;
						break;
					}
			}
			//end switch
		}

		private void frmUTPrintLienDates_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmUTPrintLienDates properties;
			//frmUTPrintLienDates.FillStyle	= 0;
			//frmUTPrintLienDates.ScaleWidth	= 3885;
			//frmUTPrintLienDates.ScaleHeight	= 2565;
			//frmUTPrintLienDates.LinkTopic	= "Form2";
			//frmUTPrintLienDates.LockControls	= true;
			//frmUTPrintLienDates.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZESMALL);
			// this will set the size of the form to the smaller TRIO size (1/2 normal)
			modGlobalFunctions.SetTRIOColors(this);
			// this will set all the TRIO colors
		}

		private void frmUTPrintLienDates_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			boolLoaded = false;
		}

		private void mnuFileExit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuFilePrintListing_Click(object sender, System.EventArgs e)
		{
			boolListing = true;
			this.Unload();
			SaveInfo();
		}

		private void mnuFileSaveExit_Click(object sender, System.EventArgs e)
		{
			boolListing = false;
			this.Unload();
			SaveInfo();
		}

		private void FillComboBoxes()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this will fill the year combo with all of the years that this account has liens
				// and it will clear out the book and page fields
				int lngRW;
				int lngLastYear = 0;
				rsRate.OpenRecordset("SELECT * FROM RateKeys ORDER BY ID", modExtraModules.strUTDatabase);
				if (rsRate.EndOfFile())
				{
					MessageBox.Show("There are no rate keys in the database.", "No Rate Keys", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				cmbRK.Clear();
				if (frmUTLienDates.InstancePtr.vsRate.Rows > 1)
				{
					for (lngRW = 1; lngRW <= frmUTLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
					{
						rsRate.FindFirstRecord("ID", frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0));
						if (rsRate.NoMatch)
						{
							cmbRK.AddItem(frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0));
							cmbRK.ItemData(cmbRK.NewIndex, FCConvert.ToInt32(frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0)));
						}
						else
						{
							cmbRK.AddItem(rsRate.Get_Fields_Int32("ID") + " - " + rsRate.Get_Fields_String("Description"));
							cmbRK.ItemData(cmbRK.NewIndex, FCConvert.ToInt32(rsRate.Get_Fields_Int32("ID")));
						}
					}
				}
				cmbYear.Clear();
				rsRate.OpenRecordset("SELECT Distinct BillDate FROM RateKeys ORDER BY BillDate desc", modExtraModules.strUTDatabase);
				// this will load only the year of the bills
				if (rsRate.RecordCount() > 0)
				{
					while (!rsRate.EndOfFile())
					{
						if (Conversion.Val(Strings.Format(rsRate.Get_Fields_DateTime("BillDate"), "yyyy")) != lngLastYear)
						{
							cmbYear.Items.Add(Conversion.Val(Strings.Format(rsRate.Get_Fields_DateTime("BillDate"), "yyyy")));
							lngLastYear = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Format(rsRate.Get_Fields_DateTime("BillDate"), "yyyy"))));
						}
						rsRate.MoveNext();
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Account", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void SaveInfo()
		{
			this.Unload();
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strString = "";
				int lngRW;
				// create the string of RK to show
				if (cmbPrint.Text == "All Eligible Rate Keys")
				{
					// all
					if (!boolListing)
					{
						for (lngRW = 1; lngRW <= frmUTLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
						{
							strString += frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0) + ",";
						}
					}
				}
				else if (cmbPrint.Text == "One Rate Key")
				{
					// one rate record
					strString = FCConvert.ToString(Conversion.Val(cmbRK.Items[cmbRK.SelectedIndex].ToString()));
				}
				else if (cmbPrint.Text == "One Year")
				{
					// one year
					for (lngRW = 1; lngRW <= frmUTLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
					{
						//FC:FINAL:CHN - issue #1205: Print Timelines Error.
						if (Conversion.Val(Strings.Format(frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 1), "yyyy")) == Conversion.Val(cmbYear.Text))//if (Conversion.Val(Strings.Format(frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 1), "yyyy")) == Conversion.Val(cmbYear.Items[cmbYear.SelectedIndex].ToString()))
						{
							strString += frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0) + ",";
						}
					}
				}
				else if (cmbPrint.Text == "Anything Currently Pending")
				{
					// all due
					for (lngRW = 1; lngRW <= frmUTLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
					{
						if (FCConvert.ToDouble(frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 2)) == -1)
						{
							strString += frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0) + ",";
						}
					}
				}
				else
				{
					// all due within 30 days
					for (lngRW = 1; lngRW <= frmUTLienDates.InstancePtr.vsRate.Rows - 1; lngRW++)
					{
						if (FCConvert.ToDouble(frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 3)) == -1)
						{
							strString += frmUTLienDates.InstancePtr.vsRate.TextMatrix(lngRW, 0) + ",";
						}
					}
				}
				if (strString != "" || boolListing)
				{
					if (boolListing)
					{
						rptRateKeyListing.InstancePtr.Init(strString);
					}
					else
					{
						// show them in the viewer
						frmUTLienDates.InstancePtr.strReportHeader = "Lien Dates Timeline";
						rptUTLienDateLine.InstancePtr.Init(ref strString, ref frmUTLienDates.InstancePtr.strWS);
						frmReportViewer.InstancePtr.Init(rptUTLienDateLine.InstancePtr);
					}
				}
				else
				{
					MessageBox.Show("There are no eligible Rate Keys", "No Rate Key Eligible", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Saving Lien", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
