﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmReprintPurgeReport.
	/// </summary>
	partial class frmReprintPurgeReport : BaseForm
	{
		public fecherFoundation.FCComboBox cboReport;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessPreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmReprintPurgeReport));
			this.cboReport = new fecherFoundation.FCComboBox();
			//this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessPreview = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdProcessPreview = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessPreview)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 231);
			this.BottomPanel.Size = new System.Drawing.Size(399, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.cboReport);
            this.ClientArea.Controls.Add(this.cmdProcessPreview);
			this.ClientArea.Size = new System.Drawing.Size(399, 171);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(399, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(245, 30);
			this.HeaderText.Text = "Reprint Purge Report";
			// 
			// cboReport
			// 
			this.cboReport.AutoSize = false;
			this.cboReport.BackColor = System.Drawing.SystemColors.Window;
			this.cboReport.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboReport.FormattingEnabled = true;
			this.cboReport.Location = new System.Drawing.Point(30, 30);
			this.cboReport.Name = "cboReport";
			this.cboReport.Size = new System.Drawing.Size(351, 40);
			this.cboReport.TabIndex = 0;
			this.cboReport.Text = "Combo1";
			// 
			// MainMenu1
			// 
			//this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
			//this.mnuProcess});
			//this.MainMenu1.Name = "MainMenu1";
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = 0;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessPreview,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessPreview
			// 
			this.mnuProcessPreview.Index = 0;
			this.mnuProcessPreview.Name = "mnuProcessPreview";
			this.mnuProcessPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessPreview.Text = "Print Preview";
			this.mnuProcessPreview.Click += new System.EventHandler(this.mnuProcessPreview_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// cmdProcessPreview
			// 
			this.cmdProcessPreview.AppearanceKey = "acceptButton";
			this.cmdProcessPreview.Location = new System.Drawing.Point(30, 90);
			this.cmdProcessPreview.Name = "cmdProcessPreview";
			this.cmdProcessPreview.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdProcessPreview.Size = new System.Drawing.Size(152, 48);
			this.cmdProcessPreview.TabIndex = 0;
			this.cmdProcessPreview.Text = "Print Preview";
			this.cmdProcessPreview.Click += new System.EventHandler(this.mnuProcessPreview_Click);
			// 
			// frmReprintPurgeReport
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(399, 339);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			//this.Menu = this.MainMenu1;
			this.Name = "frmReprintPurgeReport";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Reprint Purge Report";
			this.Load += new System.EventHandler(this.frmReprintPurgeReport_Load);
			this.Activated += new System.EventHandler(this.frmReprintPurgeReport_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmReprintPurgeReport_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdProcessPreview)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdProcessPreview;
	}
}
