﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;
using System.IO;
using fecherFoundation.VisualBasicLayer;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmCreateExtractFile.
	/// </summary>
	public partial class frmCreateExtractFile : BaseForm
	{
		public frmCreateExtractFile()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmCreateExtractFile InstancePtr
		{
			get
			{
				return (frmCreateExtractFile)Sys.GetInstance(typeof(frmCreateExtractFile));
			}
		}

		protected frmCreateExtractFile _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :                                       *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               05/23/2006              *
		// ********************************************************
		private void frmCreateExtractFile_Activated(object sender, System.EventArgs e)
		{
			if (modMain.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmCreateExtractFile_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmCreateExtractFile properties;
			//frmCreateExtractFile.FillStyle	= 0;
			//frmCreateExtractFile.ScaleWidth	= 9045;
			//frmCreateExtractFile.ScaleHeight	= 7080;
			//frmCreateExtractFile.LinkTopic	= "Form2";
			//frmCreateExtractFile.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
			if (modUTStatusPayments.Statics.TownService == "B")
			{
				if (!cmbAllAccounts.Items.Contains("Sewer"))
				{
					cmbAllAccounts.Items.Insert(0, "Sewer");
				}
				if (!cmbAllAccounts.Items.Contains("Water"))
				{
					cmbAllAccounts.Items.Insert(1, "Water");
				}
				if (!cmbAllAccounts.Items.Contains("Both"))
				{
					cmbAllAccounts.Items.Insert(2, "Both");
				}
				cmbAllAccounts.SelectedIndex = 3;
			}
			else if (modUTStatusPayments.Statics.TownService == "W")
			{
				if (cmbAllAccounts.Items.Contains("Sewer"))
				{
					cmbAllAccounts.Items.Remove("Sewer");
				}
				if (!cmbAllAccounts.Items.Contains("Water"))
				{
					cmbAllAccounts.Items.Insert(1, "Water");
				}
				if (cmbAllAccounts.Items.Contains("Both"))
				{
					cmbAllAccounts.Items.Remove("Both");
				}
				if (cmbAllAccounts.Items.Contains("All"))
				{
					cmbAllAccounts.Items.Remove("All");
				}
				//FC:FINAL:CHN - issue #1373: fix selected index.
				// cmbAllAccounts.SelectedIndex = 1;
				cmbAllAccounts.SelectedIndex = 0;
			}
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				if (!cmbAllAccounts.Items.Contains("Sewer"))
				{
					cmbAllAccounts.Items.Insert(0, "Sewer");
				}
				if (cmbAllAccounts.Items.Contains("Water"))
				{
					cmbAllAccounts.Items.Remove("Water");
				}
				if (cmbAllAccounts.Items.Contains("Both"))
				{
					cmbAllAccounts.Items.Remove("Both");
				}
				if (cmbAllAccounts.Items.Contains("All"))
				{
					cmbAllAccounts.Items.Remove("All");
				}
				cmbAllAccounts.SelectedIndex = 0;
			}
		}

		private void frmCreateExtractFile_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				this.Unload();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			this.Unload();
		}

		private void mnuProcessSave_Click(object sender, System.EventArgs e)
		{
			if (cmbCreatePrintCopy.SelectedIndex == 0)
			{
				frmBookChoice.InstancePtr.Init(15, 0, true);
				//FC:FINAL:AM:#3904 - file already downloaded
                //CopyFileToDisk();
				frmReportViewer.InstancePtr.Init(rptConsumptionExtract.InstancePtr);
			}
			else if (cmbCreatePrintCopy.SelectedIndex == 1)
			{
				CopyFileToDisk();
				frmReportViewer.InstancePtr.Init(rptConsumptionExtract.InstancePtr);
			}
			else if (cmbCreatePrintCopy.SelectedIndex == 3)
			{
				CopyFileToDisk();
			}
			else
			{
				if (!FCFileSystem.FileExists("TSCONSUM.DAT"))
				{
					MessageBox.Show("You must create the extract file before you may print the report.", "No File Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				frmReportViewer.InstancePtr.Init(rptConsumptionExtract.InstancePtr);
			}
		}

		private void CopyFileToDisk()
		{
			const int curOnErrorGoToLabel_Default = 0;
			const int curOnErrorGoToLabel_CancelError = 1;
			const int curOnErrorGoToLabel_DiskError = 2;
			int vOnErrorGoToLabel = curOnErrorGoToLabel_Default;
			try
			{
				//Scripting.Drive drv;
				int ans;
				string strDBPath = "";
				string strTemp = "";
				if (!FCFileSystem.FileExists("TSCONSUM.DAT"))
				{
					MessageBox.Show("You must create the extract file before you may copy it to disk.", "No File Found", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				// TRYAGAIN:
				// MsgBox "Please insert a blank disk into your floppy drive and click OK when done.", vbInformation, "Insert Disk"
				// Set drv = fs.GetDrive("A")
				// If drv.IsReady Then
				// do nothing
				// Else
				// Ans = MsgBox("No disk is detected in your floppy drive.  Do you want to copy the extract to disk?", vbQuestion + vbYesNo, "Copy Extract?")
				// If Ans = vbYes Then
				// GoTo TRYAGAIN
				// Else
				// Exit Sub
				// End If
				// End If
				//FC:FINAL:MSH - issue #1100: remove saving file to disk(this was needed in original app for saving file, but in web we can't do this and will be threw an exception. For saving is used Application.Download)
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = vbPorterConverter.cdlOFNNoChangeDir	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//MDIParent.InstancePtr.CommonDialog1.DialogTitle = "Copy To";
				// .FileTitle = "Choose a directory and a name for the file to export"
				//MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
				//MDIParent.InstancePtr.CommonDialog1.FileName = "TSCONSUM.DAT";
				//MDIParent.InstancePtr.CommonDialog1.DefaultExt = "DAT";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				//vOnErrorGoToLabel = curOnErrorGoToLabel_CancelError; /* On Error GoTo CancelError */
				//FC:FINAL:RPU:#i915 - Use Application.Download instead
				//MDIParent.InstancePtr.CommonDialog1.ShowSave();
				/*? On Error GoTo 0 *///strTemp = MDIParent.InstancePtr.CommonDialog1.FileName;
				//strDBPath = Directory.GetDirectoryRoot(strTemp);
				//frmWait.InstancePtr.Init("Please Wait... " + "\r\n" + "Copying File To Disk");
				//if (Strings.Right(strDBPath, 1) == "\\")
				//{
				//    if (Strings.UCase(strDBPath) != Strings.UCase(Application.MapPath("\\")))
				//    {
				//        vOnErrorGoToLabel = curOnErrorGoToLabel_DiskError; /* On Error GoTo DiskError */
				//        //FC:FINAL:RPU:#i915 - Use the full path of file
				//        //File.Copy("TSCONSUM.DAT", strDBPath + "TSCONSUM.DAT", true);
				//        File.Copy(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TSCONSUM.DAT"), strDBPath + "TSCONSUM.DAT", true);
				//    }
				//}
				//else
				//{
				//    if (Strings.UCase(strDBPath) != Strings.UCase(Application.MapPath("\\")))
				//    {
				//        vOnErrorGoToLabel = curOnErrorGoToLabel_DiskError; /* On Error GoTo DiskError */
				//        //FC:FINAL:RPU:#i915 - Use the full path of file
				//        //File.Copy("TSCONSUM.DAT", strDBPath + "\\TSCONSUM.DAT", true);
				//        File.Copy(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TSCONSUM.DAT"), strDBPath + "\\TSCONSUM.DAT", true);
				//    }
				//}
				//frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				//FC:FINAL:RPU:#i915 - Use Application.Download to download the file to client
				FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TSCONSUM.DAT"), "TSCONSUM.DAT");
				MessageBox.Show("Process Completed Successfully!", "Copy Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				CancelError:
				;
				return;
				DiskError:
				;
				// MAL@20070906: Added new error trap for file copy errors ; Call #117010
				frmWait.InstancePtr.Unload();
				MessageBox.Show("There was an error while creating the file." + "\r\n" + "If you are attempting to save the file directly to a CD drive," + "\r\n" + "please save the file on your local disk and " + "copy it manually to the CD.", "Unable to Copy File", MessageBoxButtons.OK, MessageBoxIcon.Information);
				//Application.DoEvents();
				return;
			}
			catch
			{
				switch (vOnErrorGoToLabel)
				{
					default:
					case curOnErrorGoToLabel_Default:
						// ...
						break;
					case curOnErrorGoToLabel_CancelError:
						//? goto CancelError;
						break;
					case curOnErrorGoToLabel_DiskError:
						//? goto DiskError;
						break;
				}
			}
		}

		private void optCopy_CheckedChanged(object sender, System.EventArgs e)
		{
			fraCreateOptions.Enabled = false;
		}

		private void optCreatePrintCopy_CheckedChanged(object sender, System.EventArgs e)
		{
			fraCreateOptions.Enabled = true;
		}

		private void optPrint_CheckedChanged(object sender, System.EventArgs e)
		{
			fraCreateOptions.Enabled = false;
		}

		private void optPrintCopy_CheckedChanged(object sender, System.EventArgs e)
		{
			fraCreateOptions.Enabled = false;
		}

		private void cmbCreatePrintCopy_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (cmbCreatePrintCopy.SelectedIndex != 0)
				fraCreateOptions.Enabled = false;
			else
				fraCreateOptions.Enabled = true;
		}
	}
}
