﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBillPrintOptions.
	/// </summary>
	public partial class frmBillPrintOptions : BaseForm
	{
		public frmBillPrintOptions()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmBillPrintOptions InstancePtr
		{
			get
			{
				return (frmBillPrintOptions)Sys.GetInstance(typeof(frmBillPrintOptions));
			}
		}

		protected frmBillPrintOptions _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation                  *
		// *
		// WRITTEN BY     :   Corey Gray                          *
		// MODIFIED BY    :   Jim Bertolino                       *
		// DATE MODIFIED  :   05/11/2005                          *
		// *
		// Allows the user to pick bill paramters like ranges     *
		// and order then prints the bills                        *
		// ********************************************************
		int lngRateKeyToPrint;
		string strListOfBooks;
		clsDRWrapper rsSettings = new clsDRWrapper();
        private bool boolOutprintBills = false;

		public void Init(ref int lngRateKey, ref string strBookList)
		{
			lngRateKeyToPrint = lngRateKey;
			strListOfBooks = strBookList;
			this.Show(App.MainForm);
		}

		private void frmBillPrintOptions_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						KeyCode = (Keys)0;
						mnuExit_Click();
						break;
					}
			}
			//end switch
		}

		private void frmBillPrintOptions_Load(object sender, System.EventArgs e)
		{
			string strTemp;
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEMEDIUM);
			modGlobalFunctions.SetTRIOColors(this);
			SetupGridIndividual();
			strTemp = modRegistry.GetRegistryKey("UTPrintBillOrder");
			if (Strings.UCase(strTemp) == "ACCOUNT")
			//optOrder[1].Checked = true;
                cmbOrder.Text = "Account";
			else if (Strings.UCase(strTemp) == "SEQUENCE")
			{
				//optOrder[2].Checked = true;
				cmbOrder.Text = "Sequence";
			}
		
			if (modUTStatusPayments.Statics.TownService == "B")
			//optWaterSewer[2].Checked = true;
                cmbWaterSewer.Text = "Both";
			else if (modUTStatusPayments.Statics.TownService == "S")
			{
				cmbWaterSewer.Clear();
				cmbWaterSewer.Items.Add("Sewer");
				cmbWaterSewer.Text = "Sewer";
			}
			else
            {
                if (modUTStatusPayments.Statics.TownService == "W")
                {
                    cmbWaterSewer.Clear();
                    cmbWaterSewer.Items.Add("Water");
                    cmbWaterSewer.Text = "Water";
                }
            }

            clsDRWrapper rsLoad = new clsDRWrapper();
			rsLoad.OpenRecordset("select zerobilloption,DefaultBillFormat from utilitybilling", "UtilityBilling");
			if (!rsLoad.EndOfFile())
			{
				if (FCConvert.ToBoolean(rsLoad.Get_Fields_Boolean("zerobilloption")))
                    chkExcludeZero.CheckState = Wisej.Web.CheckState.Checked;
                else
                    chkExcludeZero.CheckState = Wisej.Web.CheckState.Unchecked;

                if (rsLoad.Get_Fields_Int32("DefaultBillFormat") < 0) boolOutprintBills = true;
            }
			// kk trouts-6 03012013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			//optWaterSewer[0].Text = "Stormwater";
                if (cmbWaterSewer.Items.Contains("Water"))
                {
                    cmbWaterSewer.Items.Remove("Water");
                    cmbWaterSewer.Items.Insert(0, "Stormwater");
                }
		}

		private void GridIndividual_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			//Application.DoEvents();
			CheckGridIndividual();
		}

		private void GridIndividual_KeyDownEvent(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Delete:
					{
						// remove this row
						if (GridIndividual.Row > 0)
						{
							GridIndividual.RemoveItem(GridIndividual.Row);
							CheckGridIndividual();
						}
						break;
					}
			}
			//end switch
		}

		private void GridIndividual_KeyDownEdit(object sender, KeyEventArgs e)
		{
			switch (e.KeyCode)
			{
				case Keys.Return:
					{
						// force a validate edit
						GridIndividual.Row = 0;
						break;
					}
			}
			//end switch
		}

		private void GridIndividual_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = GridIndividual.GetFlexRowIndex(e.RowIndex);
			int col = GridIndividual.GetFlexColIndex(e.ColumnIndex);
			clsDRWrapper clsLoad = new clsDRWrapper();

            try
            {
                if (Conversion.Val(GridIndividual.EditText) > 0)
                {
                    // check to see if it is a legit account number
                    clsLoad.OpenRecordset($"select ID from master where accountnumber = {FCConvert.ToString(Conversion.Val(GridIndividual.EditText))}", modExtraModules.strUTDatabase);
                    if (!clsLoad.EndOfFile())
                    {
                        GridIndividual.TextMatrix(row, 1, FCConvert.ToString(clsLoad.Get_Fields_Int32("ID")));
                        GridIndividual.TextMatrix(row, 0, GridIndividual.EditText);
                    }
                    else
                    {
                        GridIndividual.EditText = "";
                        GridIndividual.TextMatrix(row, 0, "");
                        GridIndividual.TextMatrix(row, 1, FCConvert.ToString(0));
                    }
                }
                else
                {
                    GridIndividual.EditText = "";
                    GridIndividual.TextMatrix(row, 0, "");
                    GridIndividual.TextMatrix(row, 1, FCConvert.ToString(0));
                }
            }
            finally
            {
                clsLoad.DisposeOf();
            }
		}

		public void mnuExit_Click()
		{
			this.Unload();
		}

        private void mnuSaveContinue_Click(object sender, System.EventArgs e)
        {
            string strSQL;
            string strOPSQL = "";
            clsCustomUTBill CBill = new clsCustomUTBill();
            string strWhere = "";
            string strList = "";
            int X;
            string strOrderBy = "";
            clsDRWrapper clsTemp = new clsDRWrapper();
            int intType = 0;
            DateTime dtStatementDate;
            string strSQLTemp = "";
            string strEBillQry;
            string strBillFields;
            string strBillQryFields;
            // kk 03052013
            strBillQryFields = "Bill, qTmp.AccountKey, ActualAccountNumber, BillNumber, MeterKey, Book, qTmp.Service, BillStatus, BillingRateKey, qTmp.NoBill, CombinationCode, CurDate, CurReading, CurCode, PrevDate, PrevReading, PrevCode";
            strBillQryFields += ", Consumption, WaterOverrideCons, WaterOverrideAmount, SewerOverrideCons, SewerOverrideAmount, WMiscAmount, WAdjustAmount, WDEAdjustAmount, WFlatAmount, WUnitsAmount, WConsumptionAmount";
            strBillQryFields += ", SMiscAmount, SAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount, WTax, STax, TotalWBillAmount, TotalSBillAmount";
            strBillQryFields += ", WIntPaidDate, WPrinOwed, WTaxOwed, WIntOwed, WIntAdded, WCostOwed, WCostAdded, WPrinPaid, WTaxPaid, WIntPaid, WCostPaid, SIntPaidDate, SPrinOwed, STaxOwed, SIntOwed, SIntAdded, SCostOwed, SCostAdded, SPrinPaid, STaxPaid, SIntPaid, SCostPaid";
            strBillQryFields += ", WRT1, WRT2, WRT3, WRT4, WRT5, SRT1, SRT2, SRT3, SRT4, SRT5, qTmp.Location, BName, BName2, BAddress1, BAddress2, BAddress3, BCity, BState, BZip, BZip4, OName, OName2, OAddress1, OAddress2, OAddress3, OCity, OState, OZip, OZip4";
            strBillQryFields += ", Note, MapLot, BookPage, Telephone, Email, qTmp.WCat, qTmp.SCat, BillMessage, qTmp.FinalEndDate, qTmp.FinalStartDate, qTmp.FinalBillDate, qTmp.Final, WBillOwner, SBillOwner, BillDate, ReadingUnits";
            strBillQryFields += ", WOrigBillAmount, SOrigBillAmount, SHasOverride , WHasOverride, SDemandGroupID, WDemandGroupID, SendEBill";
            // MAL@20080806: Specify which fields get pulled from the Bill table
            // Tracker Reference: 12862
            strBillFields = "Bill.ID AS Bill, Bill.AccountKey, ActualAccountNumber, BillNumber, Bill.MeterKey, Bill.Book, Bill.Service, Bill.BillStatus, Bill.BillingRateKey, Bill.NoBill";
            strBillFields += ", Bill.CombinationCode, Bill.CurDate, Bill.CurReading, Bill.CurCode, Bill.PrevDate, Bill.PrevReading, Bill.PrevCode, Bill.Consumption";
            strBillFields += ", WaterOverrideCons, WaterOverrideAmount, SewerOverrideCons, SewerOverrideAmount, WMiscAmount";
            strBillFields += ", WAdjustAmount, WDEAdjustAmount, WFlatAmount, WUnitsAmount, WConsumptionAmount, SMiscAmount, SAdjustAmount, SDEAdjustAmount, SFlatAmount, SUnitsAmount, SConsumptionAmount";
            strBillFields += ", WTax, STax, TotalWBillAmount, TotalSBillAmount, WIntPaidDate, WPrinOwed, WTaxOwed, WIntOwed, WIntAdded, WCostOwed, WCostAdded, WPrinPaid, WTaxPaid, WIntPaid, WCostPaid";
            strBillFields += ", SIntPaidDate, SPrinOwed, STaxOwed, SIntOwed, SIntAdded, SCostOwed, SCostAdded, SPrinPaid, STaxPaid, SIntPaid, SCostPaid";
            strBillFields += ", WRT1, WRT2, WRT3, WRT4, WRT5, SRT1, SRT2, SRT3, SRT4, SRT5, Bill.Location";
            strBillFields += ", Bill.BName, Bill.BName2, Bill.BAddress1, Bill.BAddress2, Bill.BAddress3, Bill.BCity, Bill.BState, Bill.BZip, Bill.BZip4, Bill.OName, Bill.OName2, Bill.OAddress1, Bill.OAddress2, Bill.OAddress3, Bill.OCity, Bill.OState, Bill.OZip, Bill.OZip4";
            strBillFields += ", Bill.Note, Bill.MapLot, Bill.BookPage, Bill.Telephone, Bill.Email, Bill.WCat, Bill.SCat, Bill.BillMessage, Bill.FinalEndDate, Bill.FinalStartDate, Bill.FinalBillDate, Bill.Final";
            strBillFields += ", WBillOwner, SBillOwner, BillDate, ReadingUnits, WOrigBillAmount, SOrigBillAmount, SHasOverride, WHasOverride, SDemandGroupID, WDemandGroupID, SendEBill";
            // kk07192016  IIF function is not supported before SQL 2012 - change to CASE
            strEBillQry = "(SELECT DISTINCT icL.AccountKey, " + "(CASE WHEN icL.OwnerTenant = 'O' THEN icL.AcceptEbill ELSE icR.AcceptEbill END) AS OAcceptEbill, " + "(CASE WHEN icL.OwnerTenant = 'T' THEN icL.AcceptEbill ELSE icR.AcceptEbill END) AS BAcceptEbill " + "FROM tblIConnectInfo icL LEFT JOIN tblIConnectInfo icR ON (icL.AccountKey = icR.AccountKey AND icL.ID <> icR.ID))";
            this.LockCloseDuringLongProcess();
            FCUtils.StartTask(this, () =>
            {
                try
                {
                    // On Error GoTo ErrorHandler
                    frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
                    //Application.DoEvents();
                    if (!ValidateChoices()) return;

                    if (cmbOrder.Text == "Name")
                    {
                        strOrderBy = " ORDER BY BName,Bill";
                        modRegistry.SaveRegistryKey("UTPrintBillOrder", "Name");
                    }
                    else
                    {
                        if (cmbOrder.Text == "Account")
                        {
                            // MAL@20080318
                            // strOrderBy = " ORDER BY accountnumber"
                            strOrderBy = " ORDER BY ActualAccountNumber";
                            modRegistry.SaveRegistryKey("UTPrintBillOrder", "Account");
                        }
                        else
                        {
                            if (cmbOrder.Text == "Sequence")
                            {
                                strOrderBy = " ORDER BY Sequence";
                                modRegistry.SaveRegistryKey("UTPrintBillOrder", "Sequence");
                            }
                        }
                    }

                    clsTemp.Execute(chkExcludeZero.CheckState == Wisej.Web.CheckState.Checked ? "update utilitybilling set zerobilloption = 1" : "update utilitybilling set zerobilloption = 0", modExtraModules.strUTDatabase);

                    switch (cmbRange.Text)
                    {
                        case "All":
                            strWhere = "";

                            break;
                        case "Range" when cmbRangeOf.Text == "Name":
                            strWhere = " AND BName BETWEEN '" + Strings.Trim(txtStart.Text) + "' and '" + Strings.Trim(txtEnd.Text) + "' ";

                            break;

                        case "Range":
                        {
                            if (cmbRangeOf.Text == "Account")
                                strWhere = " AND ActualAccountNumber BETWEEN " + FCConvert.ToString(Conversion.Val(txtStart.Text)) + " AND " + FCConvert.ToString(Conversion.Val(txtEnd.Text)) + " ";

                            break;
                        }

                        case "Individual":
                        {
                            for (X = 1; X <= GridIndividual.Rows - 1; X++)
                                if (Conversion.Val(GridIndividual.TextMatrix(X, 0)) > 0)
                                    strList += FCConvert.ToString(Conversion.Val(GridIndividual.TextMatrix(X, 0))) + ",";

                            // X
                            if (strList != string.Empty) strList = Strings.Mid(strList, 1, strList.Length - 1);

                            strWhere = " AND ActualAccountNumber in (" + strList + ") ";

                            break;
                        }
                    }

                    // trout-624 08-08-2011 kgk  add an option to exclude bills for accounts set to email
                    if (chkExcludeEmail.CheckState == Wisej.Web.CheckState.Checked)
                    {
                        strList = "";
                        clsTemp.OpenRecordset("SELECT Master.ID, Master.EmailBill, Master.AccountNumber, Bill.AccountKey, Bill.Book FROM (Bill INNER JOIN Master on Bill.AccountKey = Master.ID) WHERE ISNULL(EmailBill,0) = 1 AND BillingRateKey = " + FCConvert.ToString(lngRateKeyToPrint) + " AND Bill." + strListOfBooks, "TWUT0000.vb1");
                        if (clsTemp.EndOfFile() != true && clsTemp.BeginningOfFile() != true)
                        {
                            do
                            {
                                // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                                strList += clsTemp.Get_Fields("AccountNumber") + ",";
                                clsTemp.MoveNext();
                            }
                            while (clsTemp.EndOfFile() != true);
                            if (strList != string.Empty) strList = Strings.Mid(strList, 1, strList.Length - 1);
                            strWhere += " AND ActualAccountNumber NOT In (" + strList + ") ";
                        }
                    }
                    if (cmbWaterSewer.Text != "Both")
                    {
                        strSQL = $"select * FROM (SELECT {strBillFields},master.accountnumber ,METERtable.sequence, BAcceptEbill, OAcceptEbill FROM (metertable INNER JOIN (bill INNER JOIN master ON bill.AccountKey = master.ID) ON metertable.ID = bill.MeterKey) LEFT JOIN {strEBillQry} AS EbillSub ON bill.AccountKey = EbillSub.AccountKey WHERE billstatus = 'B' and billingratekey = {FCConvert.ToString(lngRateKeyToPrint)}) AS Qtmp WHERE {strListOfBooks} {strWhere}{strOrderBy}";
                        strOPSQL = strSQL;
                    }
                    else
                    {
                        var billType_Combined = FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPECOMBINED);
                        var billType_Sewer = FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPESEWER);
                        var billType_Water = FCConvert.ToString(modCoreysUTCode.CNSTUTPRINTBILLTYPEWATER);

                        modUTBilling.Statics.gstrQryCombinedBillInfo = $"SELECT {billType_Combined} as PrintBillType, {strBillFields} FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'O') WHERE (WBillOwner = 1 AND SBillOwner = 1) AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL) UNION ALL SELECT {billType_Combined} AS PrintBillType, {strBillFields} FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'T') WHERE (WBillOwner = 0 AND SBillOwner = 0) AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL)";
                        
                        // Split bill - Water to Owner, Sewer to Tenant
                        modUTBilling.Statics.gstrQryWaterBillOwnerInfo = $"SELECT {billType_Sewer} as PrintBillType, {strBillFields} FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'T') WHERE WBillOwner = 1 AND SBillOwner = 0 AND Service <> 'W' AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL) UNION ALL SELECT {billType_Water} AS PrintBillType, {strBillFields} FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'O') WHERE WBillOwner = 1 AND SBillOwner = 0 AND Service <> 'S' AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL)";
                       
                        // Split bill - Sewer to Owner, Water to Tenant
                        modUTBilling.Statics.gstrQrySewerBillOwnerInfo = $"SELECT {billType_Sewer} as PrintBillType, {strBillFields} from Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'O') WHERE WBillOwner = 0 AND SBillOwner = 1 AND Service <> 'W' AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL) UNION ALL SELECT {billType_Water} AS PrintBillType, {strBillFields} FROM Bill LEFT JOIN tblIConnectInfo ON (Bill.AccountKey = tblIConnectInfo.AccountKey AND tblIConnectInfo.OwnerTenant = 'T') WHERE WBillOwner = 0 AND SBillOwner = 1 AND Service <> 'S' AND (tblIConnectInfo.AcceptEbill='P' Or tblIConnectInfo.AcceptEbill='B' OR tblIConnectInfo.AcceptEbill IS NULL)";
                        modUTBilling.Statics.gstrQryCombinedSeparateBillInfo = $"SELECT * from ({modUTBilling.Statics.gstrQrySewerBillOwnerInfo}) AS qTmpS union all select * from ({modUTBilling.Statics.gstrQryWaterBillOwnerInfo}) AS qTmpW";
                        modUTBilling.Statics.gstrQryPrintBill = $"SELECT * from ({modUTBilling.Statics.gstrQryCombinedSeparateBillInfo}) AS qTmpSep union all select * from ({modUTBilling.Statics.gstrQryCombinedBillInfo}) AS qTmpComb";
                        
                        strOPSQL = $"SELECT PrintBillType, {strBillQryFields} ,MeterTable.Sequence from MeterTable INNER JOIN ({modUTBilling.Statics.gstrQryPrintBill}) AS qTmp on MeterTable.ID = qTmp.MeterKey WHERE BillStatus = 'B' AND Combine = 'N' and BillingRateKey = {FCConvert.ToString(lngRateKeyToPrint)}{strWhere} and PrintBillType <> 2 AND {strListOfBooks}{strOrderBy}";
                        strSQL = $"SELECT PrintBillType, {strBillQryFields} ,MeterTable.Sequence from MeterTable INNER JOIN ({modUTBilling.Statics.gstrQryPrintBill}) AS qTmp on MeterTable.ID = qTmp.MeterKey WHERE BillStatus = 'B' AND Combine = 'N' and BillingRateKey = {FCConvert.ToString(lngRateKeyToPrint)}{strWhere} and {strListOfBooks}{strOrderBy}";
                    }
                    rsSettings.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
                    frmWait.InstancePtr.Unload();

                    if (rsSettings.EndOfFile()) return;

                    const string SEWER = "Sewer";
                    const string WATER = "Water";
                    const string STORMWATER = "Stormwater";
                    const string BOTH = "Both";

                    if (Conversion.Val(rsSettings.Get_Fields_String("DefaultBillFormat")) < 0)
                    {
                        cUTBill.UTBillType billServiceType;
                        if (cmbWaterSewer.Text == WATER || cmbWaterSewer.Text == STORMWATER)
                        {
                            intType = 0;
                            billServiceType = cUTBill.UTBillType.water;
                        }
                        else
                        {
                            if (cmbWaterSewer.Text == WATER || cmbWaterSewer.Text == STORMWATER)
                            {
                                intType = 1;
                                billServiceType = cUTBill.UTBillType.Sewer;
                            }
                            else
                            {
                                intType = 2;
                                billServiceType = cUTBill.UTBillType.both;
                            }
                        }

                        dtStatementDate = frmUTGetDate.InstancePtr.Init("Enter the date of the statement", DateTime.Today);
                        //modUTBilling.BuildOutPrintingFile_162(strOPSQL, intType, lngRateKeyToPrint, dtStatementDate, chkExcludeZero.CheckState == Wisej.Web.CheckState.Checked, this.Modal);
                        var outPrinter = new cUTOutPrinter();
                        var setController = new cSettingsController();
                        var strSendCopies = setController.GetSettingValue("OutprintSendCopies", "UtilityBilling", "", "", "").Trim().ToLower();
                        outPrinter.BuildOutPrintingFile(strOPSQL, billServiceType, lngRateKeyToPrint,
                                                        dtStatementDate, chkExcludeZero.Value == FCCheckBox.ValueSettings.Checked,
                                                        strSendCopies == "true");
                    }
                    else
                    {
                        CBill.FormatID = frmBillParameters.InstancePtr.Init(cmbWaterSewer.Text == BOTH);

                        if (CBill.FormatID <= 0) return;

                        CBill.PrinterName = "";
                        CBill.DBFile = "TWUT0000.vb1";
                        CBill.Module = "UT";
                        CBill.SQL = strSQL;
                        switch (cmbWaterSewer.Text)
                        {
                            case WATER:
                            case STORMWATER:
                                CBill.UseWaterBills = true;
                                CBill.UseSewerBills = false;

                                break;
                            case SEWER:
                                CBill.UseWaterBills = false;
                                CBill.UseSewerBills = true;

                                break;
                            default:
                                CBill.UseWaterBills = true;
                                CBill.UseSewerBills = true;

                                break;
                        }

                        CBill.SkipZeroNegativeBills = chkExcludeZero.CheckState == Wisej.Web.CheckState.Checked;

                        rptCustomBill.InstancePtr.Unload();
                        rptCustomBill.InstancePtr.Init(CBill, true, false, true);
                            
                        this.Unload();
                        
                    }
                    
                }
                catch (Exception ex)
                {
                    // ErrorHandler:
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("Error Number " + FCConvert.ToString(Information.Err(ex).Number) + "  " + Information.Err(ex).Description + "\r\n" + "In MnuSaveContinue", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                finally
                {
                    this.UnlockCloseDuringLongProcess();
                }
            });
        }

		private bool ValidateChoices()
		{
			bool ret = true;
			switch (cmbRange.Text)
            {
                case "All":

                    // all
                    frmWait.InstancePtr.Unload();
                    ret = true;

                    break;
                // range
                case "Range" when Strings.Trim(txtStart.Text) == string.Empty && Strings.Trim(txtEnd.Text) == string.Empty:
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("You have chosen to print by range but have not provided a range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    
                    break;

                case "Range" when Strings.Trim(txtStart.Text) == string.Empty:
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("You must provide a valid start range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;

                case "Range" when Strings.Trim(txtEnd.Text) == string.Empty:
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show("You must provide a valid end range", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    break;

                case "Range" when cmbRangeOf.Text == "Account":
                {
                    // range of account
                    if (Conversion.Val(txtEnd.Text) < Conversion.Val(txtStart.Text))
                    {
                        frmWait.InstancePtr.Unload();
                        MessageBox.Show("The last account must be equal to or greater than the first account", "Invalid Range", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    break;
                }

                case "Range":
                {
                    if (cmbRangeOf.Text == "Name")
                        if (Strings.CompareString(Strings.Trim(txtEnd.Text), "<", Strings.Trim(txtStart.Text)))
                        {
                            frmWait.InstancePtr.Unload();
                            MessageBox.Show("The end range must be equal to or greater than the start range");
                        }

                    break;
                }

                case "Individual":
                {
                    // individual
                    GridIndividual.Row = 0;
                    //Application.DoEvents();
                    if (GridIndividual.Rows <= 2)
                    {
                        frmWait.InstancePtr.Unload();
                        MessageBox.Show("You have selected individual but have not entered any accounts", "No Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                    break;
                }
            }

            return ret;
		}

		private void optRange_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						// all
						framIndividual.Visible = false;
						framRange.Visible = false;
						break;
					}
				case 1:
					{
						// range
						framIndividual.Visible = false;
						framRange.Visible = true;
						break;
					}
				case 2:
					{
						// individual
						framIndividual.Visible = true;
						framRange.Visible = false;
						break;
					}
			}
			//end switch
		}

		private void optRange_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRange.SelectedIndex;
			optRange_CheckedChanged(index, sender, e);
		}

		private void SetupGridIndividual()
		{
			GridIndividual.Rows = 2;
			GridIndividual.Cols = 2;
			GridIndividual.ColHidden(1, true);
			GridIndividual.TextMatrix(0, 0, "Account");
			GridIndividual.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
		}

		private void CheckGridIndividual()
		{
			int X;
			//Application.DoEvents();
			if (GridIndividual.Rows == 1)
			{
				GridIndividual.Rows = 2;
				GridIndividual.Row = 1;
				return;
			}
			for (X = (GridIndividual.Rows - 1); X >= 1; X--)
                if (Conversion.Val(GridIndividual.TextMatrix(X, 0)) <= 0)
                {
                    if (X != GridIndividual.Rows - 1)
                    {
                        GridIndividual.RemoveItem(X);
                    }
                    else
                    {
                        GridIndividual.TextMatrix(X, 0, "");
                        GridIndividual.TextMatrix(X, 1, FCConvert.ToString(0));
                    }
                }

            // X
			if (Conversion.Val(GridIndividual.TextMatrix(GridIndividual.Rows - 1, 0)) > 0)
			{
				GridIndividual.Rows += 1;
				GridIndividual.Row = GridIndividual.Rows - 1;
			}
		}

		private void optRangeOf_CheckedChanged(int Index, object sender, System.EventArgs e)
		{
			switch (Index)
			{
				case 0:
					{
						break;
					}
				case 1:
					{
						if (Conversion.Val(txtStart.Text) <= 0) txtStart.Text = "";

                        if (Conversion.Val(txtEnd.Text) <= 0) txtEnd.Text = "";

                        break;
					}
			}
			//end switch
		}

		private void optRangeOf_CheckedChanged(object sender, System.EventArgs e)
		{
			int index = cmbRangeOf.SelectedIndex;
			optRangeOf_CheckedChanged(index, sender, e);
		}

		private void txtEnd_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbRangeOf.Text == "Account")
			{
				// account so only numbers allowed
				if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void txtStart_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (cmbRangeOf.Text == "Account")
			{
				// account so only numbers allowed
				if (KeyAscii >= Keys.D0 && KeyAscii <= Keys.D9)
				{
				}
				else
				{
					KeyAscii = (Keys)0;
				}
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}
	}
}
