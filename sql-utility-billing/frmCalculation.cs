﻿//Fecher vbPorter - Version 1.0.0.40
using fecherFoundation;
using Global;
using System;
using System.Drawing;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
    public partial class frmCalculation : BaseForm
    {
        public frmCalculation()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            InitializeComponentEx();
        }

        private void InitializeComponentEx()
        {
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
            if (_InstancePtr == null)
                _InstancePtr = this;
        }
        /// <summary>
        /// Default instance for Form
        /// </summary>
        public static frmCalculation InstancePtr
        {
            get
            {
                return (frmCalculation)Sys.GetInstance(typeof(frmCalculation));
            }
        }

        protected frmCalculation _InstancePtr = null;
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        //=========================================================
        // ********************************************************
        // PROPERTY OF TRIO SOFTWARE CORPORATION                  *
        // *
        // WRITTEN BY     :               Jim Bertolino           *
        // DATE           :               08/13/2004              *
        // *
        // MODIFIED BY    :               Jim Bertolino           *
        // LAST UPDATED   :               04/12/2007              *
        // ********************************************************
        bool FormLvlUp;
        // True if form is exiting to higher level in the form hierarchy, false if moving lower
        clsDRWrapper rsBook;
        // temporary recordsets
        clsDRWrapper rsRate = new clsDRWrapper();
        clsDRWrapper rsMeter;
        clsDRWrapper rsAcct;
        clsDRWrapper rsBill;
        bool FillGrid;
        // tells the grid whether to reformat the grid on form activate
        string[] RTErrorArray = new string[99 + 1];
        int ErrorSeq;
        // holds the sequence that had the error in it
        string strBookList;
        // This will get created to pass to the analysis reports
        bool boolFinal;
        bool boolCurMeterMinimum;
        // on all meters
        bool boolHideVarianceWarning;
        bool boolLimitBillIncr;
        // kgk TROUT-684 Jay billing change
        bool boolBillStormwater;
        // kk trouts-6  06122013  Bill stormwater on monthly billing?
        int CurrentBook;
        // holds the current book value
        string strSQL;
        // vbPorter upgrade warning: Consumption As int	OnWrite(double, int)
        int Consumption;
        // Calculated field (Current Reading - Previous Reading)
        int MaxRows;
        // holds the total rows in the grid
        int i;
        // generic counting variable
        string BillingCode = "";
        // billing code Flat, Units, Consumption
        bool CollapseFlag;
        // True = Collapsing rows do not allow collapse event to happen, False = Allow collapse event to happen
        int CurRow;
        // holds the current row number of the last level 0 row
        int ErrorCheck;
        // flag to watch for errors
        int lngFinalRK;
        double BookWAdjustment;
        // these variables are to gather info for the calculation
        double BookWMisc;
        // summary screen then pass the information to the summary
        double BookWTax;
        // screen when it is loaded
        int BookWCount;
        double BookWFlat;
        double BookWCons;
        double BookWUnits;
        double BookWOverride;
        double BookSAdjustment;
        double BookSMisc;
        double BookSTax;
        int BookSCount;
        double BookSFlat;
        double BookSCons;
        double BookSUnits;
        double BookSOverride;
        double STotal;
        // holds the sewer total for the meter
        double WTotal;
        // holds the water total for the meter
        double dblSGrandTotal;
        // holds the sewer total for the grid
        double dblWGrandTotal;
        // holds the water total for the grid
        public int lngGRIDCOLTree;
        public int lngGridColSeq;
        public int lngGridColPrevious;
        public int lngGridColCurrent;
        public int lngGridColConsumption;
        public int lngGridColWater;
        public int lngGridColSewer;
        public int lngGridColAccount;
        public int lngGridColAccountKey;
        public int lngGRIDCOLHidden;
        public int lngGridColName;
        public int lngGridColLocation;
        public int lngGridColBookNumber;
        int[] BookArray = null;
        // holds the books to be billed
        int intCount;
        bool boolCalculate;
        // vbPorter upgrade warning: intCT As short	OnWriteFCConvert.ToInt32(
        // vbPorter upgrade warning: lngPassFinalRK As int	OnWriteFCConvert.ToDouble(
        public void Init(bool boolPassCalculate, short intCT, ref int[] PassBookArray, bool boolFinalBilling = false, int lngPassFinalRK = 0)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                dblSGrandTotal = 0;
                dblWGrandTotal = 0;
                lngFinalRK = lngPassFinalRK;
                boolFinal = boolFinalBilling;
                intCount = intCT;
                boolCalculate = boolPassCalculate;
                if (boolFinal)
                {
                    // this will find all of the books that would be needed to show
                    if (!FindFinalBooks())
                    {
                        MessageBox.Show("No eligible bills.", "No Bills Found", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        Unload();
                        return;
                    }
                }
                else
                {
                    BookArray = PassBookArray;
                }
                strBookList = "";
                rsRate.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);
                if (!rsRate.EndOfFile())
                {
                    boolHideVarianceWarning = FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("HideMinimumVarianceWarning"));
                    // kgk trout-684 add option for rate type switch for Jay
                    boolLimitBillIncr = FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("RateTypeChange"));
                    // kk trouts-6 06242013  Add Option to Bill Bangor Stormwater Fee or Skip
                    boolBillStormwater = FCConvert.ToBoolean(rsRate.Get_Fields_Boolean("BillStormwaterFee"));
                }
                if (boolFinalBilling)
                {
                    // this will make it so that the SQL statement will look for all the final bills
                    strBookList = " Final = TRUE";
                }
                else
                {
                    for (i = 1; i <= Information.UBound(BookArray, 1); i++)
                    {
                        strBookList += " Book = " + FCConvert.ToString(BookArray[i]) + " OR";
                    }
                    // take off the last "OR"
                    if (strBookList != string.Empty)
                    {
                        strBookList = Strings.Mid(strBookList, 1, strBookList.Length - 2);
                    }
                }
                if (modUTBilling.CheckForUTAccountsSetup())
                {
                    Show(App.MainForm);
                }
                else
                {
                    MessageBox.Show("Before continuing, please setup your UT Accounts in File Maintenance > Customize.", "Missing Valid Accounts", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Unload();
                }
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Form", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        private void chkShowSubtotal_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkShowSubtotal.CheckState == Wisej.Web.CheckState.Checked)
            {
                ShowSubtotalsRows_2(true);
            }
            else
            {
                ShowSubtotalsRows_2(false);
            }
        }

        private void frmCalculation_Activated(object sender, System.EventArgs e)
        {
            
        }

        private void frmCalculation_Load(object sender, System.EventArgs e)
        {
            //Begin Unmaped Properties
            //frmCalculation properties;
            //frmCalculation.FillStyle	= 0;
            //frmCalculation.ScaleWidth	= 9300;
            //frmCalculation.ScaleHeight	= 7770;
            //frmCalculation.LinkTopic	= "Form2";
            //frmCalculation.LockControls	= true;
            //frmCalculation.PaletteMode	= 1  'UseZOrder;
            //End Unmaped Properties
            lngGRIDCOLTree = 0;
            lngGridColSeq = 1;
            lngGridColPrevious = 2;
            lngGridColCurrent = 3;
            lngGridColConsumption = 4;
            lngGridColWater = 5;
            lngGridColSewer = 6;
            lngGridColAccount = 7;
            lngGridColAccountKey = 8;
            lngGRIDCOLHidden = 9;
            lngGridColName = 10;
            lngGridColLocation = 11;
            lngGridColBookNumber = 2;
            FillGrid = true;
            modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
            modGlobalFunctions.SetTRIOColors(this);
            // kk trouts-11 06122013  Combine Calc & Edit and Billing Edit
            // If boolCalculate Then
            Text = "Book Calculation & Edit";
            // Else
            // Me.Caption = "Billing Edit"
            // End If

            //moved code from form activated
            this.ShowWait();
            FCUtils.StartTask(this, () =>
            {
                try
                {
                    // On Error GoTo ERROR_HANDLER
                    if (modMain.FormExist(this))
                        return;
                    FormLvlUp = false;
                    // initialize variables
                    modUTBilling.Statics.WarningArray = new modUTBilling.WarningStruct[0 + 1];
                    // this will reset the warning array
                    if (FillGrid)
                    {
                        Format_Grid();
                        // sets the grid up
                        //TODO async task
                        StartBookCalculation();
                        CollapseFlag = false;
                        // initialize the collapse flag
                        //optDetail[2].Checked = true;
                        cmbDetail.Text = "Meter Info";
                        //FC:FINAL:DSE:#i930 force checked changed event
                        optDetail_CheckedChanged(2, sender, e);
                        SetSummaryScreen();
                        // sends all the information to the summary screen
                    }
                    return;
                }
                catch (Exception ex)
                {
                    
                    MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Form Activate", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                finally
                {
                    this.EndWait();
                }
            });
        }

        private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
        {
            if (modGlobalConstants.Statics.gblnAutoPrepayments)
            {
                // kk trouts-11 06122013  Combine Calc & Edit and Billing Edit    And boolCalculate Then
                modUTBilling.PrintPrePayReport();
            }
            if (boolFinal)
            {
                // this will call the bill function using just the final bills
                //FC:FINAL:RPU:#i998 - Hide the current form to set focus to new one
                //Hide();
                frmBillsPreview.InstancePtr.Init(BookArray.UBound(1), ref BookArray, true, ref lngFinalRK, boolFinal);
                //Visible = true;
            }
        }

        private void frmCalculation_Resize(object sender, System.EventArgs e)
        {
            Format_Grid();
            vsCalc_Collapsed();
        }

        private void frmCalculation_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
        {
            Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
            // catches the escape and enter keys
            if (KeyAscii == Keys.Escape)
            {
                KeyAscii = (Keys)0;
                FormLvlUp = true;
                Unload();
                frmCalculationSummary.InstancePtr.Unload();
                // if escape is pressed, unload the summary page too
            }
            else if (KeyAscii == Keys.Return)
            {
                KeyAscii = (Keys)0;
                Support.SendKeys("{TAB}", false);
            }
            e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
        }

        private void mnuFilePrint_Click(object sender, System.EventArgs e)
        {
            arBillingEdit.InstancePtr.Init(ref BookArray, false, FCConvert.CBool(chkShowSubtotal.CheckState == Wisej.Web.CheckState.Checked), true, true, FCConvert.CBool(chkShowMapLot.CheckState == Wisej.Web.CheckState.Checked), boolFinal);
        }

        private void mnuFilePrintSummary_Click(object sender, System.EventArgs e)
        {
            clsDRWrapper rsA = new clsDRWrapper();

            try
            {
                // This is only printing the summary
                frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading Data");
                rsA.OpenRecordset("SELECT * FROM DefaultAnalysisReports", modExtraModules.strUTDatabase);

                if (!rsA.EndOfFile())
                {
                    arBillingEdit.InstancePtr.Init(ref BookArray, false, FCConvert.CBool(chkShowSubtotal.CheckState == Wisej.Web.CheckState.Checked), true, false);
                }

                frmWait.InstancePtr.Unload();

                return;
            }
            catch (Exception ex)
            {
                
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Loading Summary", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsA.DisposeOf();
            }
        }

        private void mnuQuit_Click(object sender, System.EventArgs e)
        {
            FormLvlUp = true;
            Unload();
        }

        private void mnuSummary_Click(object sender, System.EventArgs e)
        {
            // If boolCalculate Then
            // show the calculate summary
            FillGrid = false;
            //FC:FINAL:AM:#4382 - fill the summary values again because they are lost when the summary form is closed
            SetSummaryScreen();
            //! Load frmCalculationSummary;
            Hide();
            frmCalculationSummary.InstancePtr.Init(ref boolCalculate, ref BookArray);
            // Else
            // show the billing edit summary
            // 
            // End If
        }

        private bool GetBookInfo(ref int BookNum)
        {
            bool GetBookInfo = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // will show all book information in the flexgrid
                bool temp;
                int answer;
                string strCurStatus;
                bool boolErrors = false;
                int lngBookLine;
                rsBook = new clsDRWrapper();
                // If rsBook.EditMode <> 0 Then rsBook.Update
                rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + FCConvert.ToString(BookNum), modExtraModules.strUTDatabase);
                strCurStatus = Strings.Left(FCConvert.ToString(rsBook.Get_Fields_String("CurrentStatus")), 1);
                temp = true;
                if (boolCalculate)
                {
                    if (strCurStatus == "C" || strCurStatus == "B" || strCurStatus == "W" || strCurStatus == "S")
                    {
                        //frmWait.InstancePtr.Unload();
                        MessageBox.Show("The book chosen is not ready to be calculated." + "\r\n" + "Check the status and try again.", "Status Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        temp = false;
                    }
                }
                else
                {
                    //if (strCurStatus == Strings.Left(FCConvert.ToString(rsBook.Get_Fields("CurrentStatus")), 1) == "X" || strCurStatus == Strings.Left(FCConvert.ToString(rsBook.Get_Fields("CurrentStatus")), 1) == "E")
                    //VGE#i877 if ((strCurStatus = Strings.Left(FCConvert.ToString(rsBook.Get_Fields("CurrentStatus")), 1)) == "X" || (strCurStatus = Strings.Left(FCConvert.ToString(rsBook.Get_Fields("CurrentStatus")), 1)) == "E")
                    // FC:FINAL:VGE - #i877 Changing to compare from assign/compare (strCurStatus isn't used further). Upper string is a valid 1-to-1 conversion
                    if (strCurStatus == "X" || strCurStatus == "E")
                    {
                        //frmWait.InstancePtr.Unload();
                        MessageBox.Show("The book chosen is not ready to be calculated." + "\r\n" + "Check the status and try again.", "Status Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        temp = false;
                    }
                }
                // Else
                if (temp)
                {
                    // ###
                    lngBookLine = AddBookLine_24(BookNum, rsBook.Get_Fields_String("Description"), 0);
                    if (rsBook.RecordCount() > 0)
                    {
                        if (Fill_Grid(vsCalc, ref BookNum, ref boolErrors, 1))
                        {
                            // calculate each meter and fill the grid with calculated figures
                            if (CalculateBook() || boolFinal)
                            {
                                // if all meters are calculated correctly then
                                GetBookInfo = true;
                            }
                            else
                            {
                                //frmWait.InstancePtr.Unload();
                                MessageBox.Show("Book #" + rsBook.Get_Fields_Int32("BookNumber") + " was not processed.  Check that all meter data has been entered." + "\r\n" + "Error in Sequence #" + FCConvert.ToString(ErrorSeq) + ".", "Status Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                        else
                        {
                            if (!rsMeter.EndOfFile())
                            {
                                MessageBox.Show("Meter #" + rsMeter.Get_Fields_Int32("MeterKey") + " was not processed.", "Error in Meter Information", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            }
                        }
                        if (boolErrors)
                        {
                            // if there are warnings or errors then show
                            vsCalc.TextMatrix(lngBookLine, lngGridColAccount, "  *  *  *  ERRORS  *  *  *  ");
                            vsCalc.TextMatrix(lngBookLine, lngGridColAccountKey, "");
                            vsCalc.TextMatrix(lngBookLine, lngGridColName, "");
                            vsCalc.TextMatrix(lngBookLine, lngGridColLocation, "");
                            //FC:FINAL:DSE:#i930 Cell values are missing if full row is merged
                            //vsCalc.MergeRow(lngBookLine, true);
                            vsCalc.MergeRow(lngBookLine, true, lngGridColAccount, lngGridColLocation);
                            //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngBookLine, lngGridColAccount, lngBookLine, lngGridColLocation, modGlobalConstants.Statics.TRIOCOLORRED);
                            vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngBookLine, lngGridColAccount, lngBookLine, lngGridColLocation, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                        }
                    }
                }
                // End If
                return GetBookInfo;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Getting Book Info", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetBookInfo;
        }

        private bool CalculateBook()
        {
            bool CalculateBook = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                string strType;
                strType = "E";
                // x    End If
                // this function returns true if all meters are calculated correctly
                rsMeter = new clsDRWrapper();
                strSQL = "SELECT * FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(CurrentBook) + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(NegativeConsumption,0) = 0 AND ISNULL(NoBill,0) = 0 ORDER BY Sequence";
                // builds the SQL statement to select all records
                rsMeter.OpenRecordset(strSQL);
                // selects all the meter records
                if (rsMeter.RecordCount() > 0)
                {
                    if (rsMeter.EndOfFile() != true && rsMeter.BeginningOfFile() != true)
                    {
                        // if not at the beginning or the end of the recordset
                        while (!rsMeter.EndOfFile())
                        {
                            // check each meter for the correct billing status
                            //Application.DoEvents();
                            if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields_String("BillingStatus")) == false)
                            {
                                if (FCConvert.ToString(rsMeter.Get_Fields_String("BillingStatus")) != strType)
                                {
                                    // if not "X" or "E" then exit sub and return false
                                    CalculateBook = false;
                                    // TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
                                    ErrorSeq = FCConvert.ToInt32(rsMeter.Get_Fields("Sequence"));
                                    return CalculateBook;
                                }
                            }
                            rsMeter.MoveNext();
                        }
                    }
                }
                CalculateBook = true;
                return CalculateBook;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Checking Book Status", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CalculateBook;
        }

        private void Format_Grid()
        {
            //FC:FINAL:MSH - issue #999: change size of columns
            //FC:FINAL:DSE:#999 Use fixed size for the SEQ column
            //vsCalc.ColWidth(lngGridColSeq, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.06));
            vsCalc.ColWidth(lngGridColSeq, 1275);
            vsCalc.ColWidth(lngGridColPrevious, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.08));
            vsCalc.ColWidth(lngGridColCurrent, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.06));
            vsCalc.ColWidth(lngGridColConsumption, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.12));
            vsCalc.ColWidth(lngGridColWater, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.09));
            vsCalc.ColWidth(lngGridColSewer, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.09));
            vsCalc.ColWidth(lngGridColAccount, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.19));
            vsCalc.ColWidth(lngGridColAccountKey, 0);
            vsCalc.ColWidth(lngGRIDCOLHidden, 0);
            //FC:FINAL:DSE:#999 Compute name column width based on  the fixed width seq column
            //vsCalc.ColWidth(lngGridColName, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.19));
            vsCalc.ColWidth(lngGridColName, FCConvert.ToInt32((vsCalc.WidthOriginal - 1275) * 0.13));
            vsCalc.ColWidth(lngGridColLocation, FCConvert.ToInt32(vsCalc.WidthOriginal * 0.12));
            vsCalc.ExtendLastCol = true;
            vsCalc.MergeCells = FCGrid.MergeCellsSettings.flexMergeSpill;
            vsCalc.TextMatrix(1, lngGridColSeq, "Seq#");
            vsCalc.TextMatrix(1, lngGridColPrevious, "Previous");
            vsCalc.TextMatrix(1, lngGridColCurrent, "Current");
            vsCalc.TextMatrix(1, lngGridColConsumption, "Cons");
            // kk trouts-6 03012013  Change Water to Stormwater for Bangor
            if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
            {
                vsCalc.TextMatrix(1, lngGridColWater, "Stormwater");
            }
            else
            {
                vsCalc.TextMatrix(1, lngGridColWater, "Water");
            }
            vsCalc.TextMatrix(1, lngGridColSewer, "Sewer");
            vsCalc.TextMatrix(1, lngGridColAccount, "Account");
            vsCalc.TextMatrix(1, lngGridColName, "Name");
            vsCalc.TextMatrix(1, lngGridColLocation, "Location");
            vsCalc.TextMatrix(0, lngGridColPrevious, "   Readings   ");
            // .Cell(FCGrid.CellPropertySettings.flexcpFontName, 0, 0, 1, lngGridColLocation) = "Tahoma"
            //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 1, lngGridColLocation, FCGrid.AlignmentSettings.flexAlignCenterCenter);
            vsCalc.ColFormat(lngGridColWater, "#,###.00");
            vsCalc.ColFormat(lngGridColSewer, "#,###.00");
            vsCalc.ColAlignment(lngGridColWater, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsCalc.ColAlignment(lngGridColSewer, FCGrid.AlignmentSettings.flexAlignRightCenter);
            vsCalc.ScrollBars = FCGrid.ScrollBarsSettings.flexScrollVertical;
        }

        private void optDetail_CheckedChanged(int Index, object sender, System.EventArgs e)
        {
            int rowNumber;
            int lngLastMeterRow = 0;
            switch (Index)
            {
                case 0:
                    {
                        // show meter detail
                        CollapseRows_2(true);
                        vsCalc_Collapsed();
                        break;
                    }
                case 1:
                    {
                        // show all detail
                        CollapseRows_2(false);
                        vsCalc_Collapsed();
                        break;
                    }
                case 2:
                    {
                        // show errors and exceptions
                        CollapseRows_2(true);
                        for (rowNumber = 2; rowNumber <= (vsCalc.Rows - 1); rowNumber++)
                        {
                            // expand the highest level first
                            if (vsCalc.RowOutlineLevel(rowNumber) == 0)
                                vsCalc.IsCollapsed(rowNumber, FCGrid.CollapsedSettings.flexOutlineExpanded);
                            if (vsCalc.RowOutlineLevel(rowNumber) == 1)
                                lngLastMeterRow = rowNumber;
                            if (lngLastMeterRow < 2)
                                lngLastMeterRow = 2;
                            if (hasErrorOrNoteOrWarning(rowNumber, lngGridColCurrent) ||
                                hasErrorOrNoteOrWarning(rowNumber, lngGridColConsumption) ||
                                hasErrorOrNoteOrWarning(rowNumber, lngGridColWater) ||
                                hasErrorOrNoteOrWarning(rowNumber, lngGridColSewer) ||
                                hasErrorOrNoteOrWarning(rowNumber, lngGridColAccount))
                                vsCalc.IsCollapsed(lngLastMeterRow, FCGrid.CollapsedSettings.flexOutlineExpanded);
                        }
                        vsCalc_Collapsed();
                        break;
                    }
                case 3:
                    {
                        CollapseRows_2(true);
                        for (rowNumber = 2; rowNumber <= (vsCalc.Rows - 1); rowNumber++)
                        {
                            if (vsCalc.RowOutlineLevel(rowNumber) < 1) vsCalc.IsCollapsed(rowNumber, FCGrid.CollapsedSettings.flexOutlineExpanded);
                        }
                        vsCalc_Collapsed();
                        break;
                    }
            }
            //end switch
        }

        private bool hasErrorOrNoteOrWarning(int row, int column)
        {
            return vsCalc.TextMatrix(row, column) == "ERROR" || vsCalc.TextMatrix(row, column) == "NOTE" || vsCalc.TextMatrix(row, column) == "WARNING";
        }

        private void optDetail_CheckedChanged(object sender, System.EventArgs e)
        {
            int index = cmbDetail.SelectedIndex;
            optDetail_CheckedChanged(index, sender, e);
        }

        private void vsCalc_AfterCollapse(object sender, DataGridViewRowEventArgs e)
        {
            vsCalc_Collapsed();
            //FC:FINAL:MSH - issue #997: force trigger handler to hide subtotal rows if checkbox unchecked(after expanding will be changed visibility of all rows and their child rows)
            chkShowSubtotal_CheckedChanged(chkShowSubtotal, new EventArgs());
        }

        private void vsCalc_Collapsed()
        {
            // when the a row is collapsed or expanded
            int ExposedRows = 0;
            // keeps track of rows that are not collapsed
            if (CollapseFlag == false)
            {
                ExposedRows = 2;
                for (i = 2; i <= vsCalc.Rows - 1; i++)
                {
                    // check each row
                    // If i = 88 Then Stop
                    if (vsCalc.RowOutlineLevel(i) == 0)
                    {
                        // if it is the upper level row
                        // if row is collapsed
                        if (vsCalc.IsCollapsed(i) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                        {
                            ExposedRows += 1;
                            // count the exposed row (Upper level)
                            // show the totals in the initial line
                            //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, i, lngGridColWater, i, lngGridColSewer, Color.White);
                            i += 1;
                            // move to the next row
                            // check each row for subordinate rows and do
                            // not count the subordinate rows because they are collapsed
                            while (vsCalc.RowOutlineLevel(i) != 0)
                            {
                                if (i < vsCalc.Rows - 1)
                                {
                                    i += 1;
                                }
                                else
                                {
                                    break;
                                }
                            }
                            i -= 1;
                        }
                        else
                        {
                            // if the row is not collapsed then
                            ExposedRows += 1;
                            // count a row
                            // hide the totals in the initial line
                            if (i < vsCalc.Rows - 1)
                            {
                                // this is so that the totals are not messed up
                                if (Strings.Left(vsCalc.TextMatrix(i, lngGridColPrevious), 4) != "Book")
                                {
                                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, i, lngGridColWater, i, lngGridColSewer, vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, i, lngGridColWater, i, lngGridColSewer));
                                }
                            }
                        }
                    }
                    else
                    {
                        if (vsCalc.RowOutlineLevel(i) == 1)
                        {
                            // if it is the upper level row but not a level 0
                            // if row is collapsed
                            if (vsCalc.IsCollapsed(i) == FCGrid.CollapsedSettings.flexOutlineCollapsed)
                            {
                                ExposedRows += 1;
                                // count the exposed row (Upper level)
                                // show the totals in the initial line
                                //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, i, lngGridColWater, i, lngGridColSewer, Color.Black);
                                i += 1;
                                // move to the next row
                                // check each row for subordinate rows and do
                                // not count the subordinate rows because they are collapsed
                                while (vsCalc.RowOutlineLevel(i) != 1)
                                {
                                    if (i < vsCalc.Rows - 1)
                                    {
                                        i += 1;
                                    }
                                    else
                                    {
                                        break;
                                    }
                                }
                                i -= 1;
                            }
                            else
                            {
                                // if the row is not collapsed then
                                ExposedRows += 1;
                                // count a row
                                // hide the totals in the initial line
                                if (Strings.Left(vsCalc.TextMatrix(i, lngGridColPrevious), 4) != "Book")
                                {
                                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, i, lngGridColWater, i, lngGridColSewer, vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, i, lngGridColWater, i, lngGridColSewer));
                                }
                            }
                        }
                        else
                        {
                            ExposedRows += 1;
                            // if the row is a subordinate row, then count it
                            // show the totals in the initial line
                        }
                    }
                }
            }
        }
        // vbPorter upgrade warning: Grid As object	OnWrite(FCGridCtl.VSFlexGrid)
        private bool Fill_Grid(FCGrid Grid, ref int lngBookNum, ref bool boolBookErrors/*= false*/, int intRowOutLineLevel = 0)
        {
            bool Fill_Grid = false;
            clsDRWrapper rsB = new clsDRWrapper();
            clsDRWrapper rsAccount = new clsDRWrapper();

            try
            {
                double dblWTotal = 0;
                double dblSTotal = 0;
                double dblWSum = 0;
                double dblSSum = 0;
                int lngParentBookRow = 0;

                // pass in the grid and the book number then this function will fill the grid
                // get all of the information for 1 book
                CurrentBook = lngBookNum;

                // set the book number
                rsMeter = new clsDRWrapper();
                ErrorCheck = 0;
                strSQL = "SELECT * FROM Metertable WHERE BookNumber = " + FCConvert.ToString(CurrentBook) + " AND (Combine = 'N' OR Combine = 'B') AND ISNULL(FinalBilled,0) = 0 ORDER BY Sequence";
                strSQL = "SELECT * FROM Metertable WHERE BookNumber = " + FCConvert.ToString(CurrentBook) + " AND Combine = 'N' AND ISNULL(FinalBilled,0) = 0 ORDER BY Sequence";

                // builds the SQL statement to select all records
                rsMeter.OpenRecordset(strSQL, modExtraModules.strUTDatabase);

                // selects all the records
                // Grid.rows = 2                                  'clears the grid
                // fill the information
                if (rsMeter.RecordCount() > 0)
                {
                    // Open "CheckCalculationOfMeter.txt" For Output As #1
                    if (rsMeter.EndOfFile() != true && rsMeter.BeginningOfFile() != true)
                    {
                        // if not at the beginning or the end of the recordset
                        //frmWait.InstancePtr.Init("Please Wait" + "\r\n" + "Processing Book #" + FCConvert.ToString(CurrentBook), true, rsMeter.RecordCount());
                        this.UpdateWait("Please Wait" + "\r\n" + "Processing Book #" + FCConvert.ToString(CurrentBook));//, true, rsMeter.RecordCount());
                        do
                        {
                            //Application.DoEvents();
                            //kkkkfrmWait.InstancePtr.IncrementProgress();
                            // Tracker Reference: 11863
                            rsAccount.OpenRecordset("SELECT * FROM Master WHERE ID = " + rsMeter.Get_Fields_Int32("AccountKey"), modExtraModules.strUTDatabase);

                            if (rsAccount.RecordCount() > 0)
                            {
                                if (!FCConvert.ToBoolean(rsAccount.Get_Fields_Boolean("Deleted")))
                                {
                                    if (boolFinal)
                                    {
                                        rsB.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsMeter.Get_Fields_Int32("ID") + " AND BillStatus <> 'B' AND ISNULL(Final,0) = 1", modExtraModules.strUTDatabase);
                                    }
                                    else
                                    {
                                        rsB.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsMeter.Get_Fields_Int32("ID") + " AND BillStatus <> 'B'", modExtraModules.strUTDatabase);
                                    }

                                    if (!rsB.EndOfFile())
                                    {
                                        if (boolCalculate)
                                        {
                                            // clear all of the breakdown information for the bill here
                                            // DJW@10132009 Deleted all record to not get double adjustments
                                            modUTBilling.ClearBreakDownRecords_80(true, rsB.Get_Fields_Int32("ID"), false, 0);

                                            // rsMeter.Fields("ID")
                                        }

                                        // fill the grid, the bill and the breakdown table
                                        AddMeterData(Grid, ref intRowOutLineLevel, Grid.Rows, ref dblWTotal, ref dblSTotal, ref boolBookErrors);

                                        // adds meter data and any combined meters and will return the water and sewer total
                                        dblWSum += dblWTotal;

                                        // these are the totals of the meter
                                        dblSSum += dblSTotal;

                                        // update the meter amounts
                                        if (boolCalculate || boolFinal)
                                        {
                                            rsMeter.Edit();
                                            rsMeter.Set_Fields("WaterCharged", dblWTotal);
                                            rsMeter.Set_Fields("SewerCharged", dblSTotal);

                                            // Print #1, "Account : " & PadToString(GetAccountNumber(rsMeter.Fields("AccountKey")), 6) & " MK - " & PadToString(rsMeter.Fields("MeterKey"), 3) & "  W - " & PadStringWithSpaces(Format(dblWTotal, "#,##0.00"), 12) & "  S - " & PadStringWithSpaces(Format(dblSTotal, "#,##0.00"), 12)
                                            rsMeter.Update();
                                        }
                                    }

                                    dblWTotal = 0;
                                    dblSTotal = 0;
                                }
                            }

                            rsMeter.MoveNext();
                        } while (!rsMeter.EndOfFile());
                    }

                    // Close #1
                    // add a book total line
                    CurRow = Grid.Rows;
                    AddCurrentRow(CurRow, intRowOutLineLevel);

                    //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CurRow, 1, CurRow, Grid.Cols - 1, Color.White);
                    //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, CurRow, lngGridColPrevious, CurRow, lngGridColSewer, 0xFF8080);
                    // blue
                    //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, CurRow, lngGridColPrevious, CurRow, lngGridColSewer, Color.White);
                    Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, CurRow, lngGridColPrevious, CurRow, lngGridColSewer, true);
                    Grid.TextMatrix(CurRow, lngGridColPrevious, "Book Totals:");

                    // book total for col 5 (water)
                    Grid.TextMatrix(CurRow, lngGridColWater, dblWSum);

                    // book total for col 6 (sewer)
                    Grid.TextMatrix(CurRow, lngGridColSewer, dblSSum);
                    lngParentBookRow = GetLastParentBookRow(ref CurRow);

                    if (lngParentBookRow > 0)
                    {
                        // book total for col 5 (water)
                        Grid.TextMatrix(lngParentBookRow, lngGridColWater, dblWSum);

                        // book total for col 6 (sewer)
                        Grid.TextMatrix(lngParentBookRow, lngGridColSewer, dblSSum);
                    }
                }
                else
                {
                    //frmWait.InstancePtr.Unload();
                    MessageBox.Show("No Meters in this book", "No Meters", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Fill_Grid = false;

                    return Fill_Grid;
                }
                modColorScheme.ColorGrid(Grid);
                Fill_Grid = true;

            }
            catch (Exception ex)
            {
                
                //frmWait.InstancePtr.Unload();
                Fill_Grid = false;
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Filling Grid", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsAccount.DisposeOf();
                rsB.DisposeOf();
            }

            return Fill_Grid;
        }

        private bool BilledType_8(short BType, bool boolWater)
        {
            return BilledType(ref BType, ref boolWater);
        }

        private bool BilledType(ref short BType, ref bool boolWater)
        {
            bool BilledType = false;
            // in the billcode, each number is what type of billing there is for each meter
            // this function will return True is the number passed in is found in the billing code of
            // the current meter, False otherwise
            int intCT;
            string strWS = "";
            if (boolWater)
            {
                strWS = "Water";
                if (FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "S")
                {
                    return BilledType;
                }
            }
            else
            {
                strWS = "Sewer";
                if (FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "W")
                {
                    return BilledType;
                }
            }
            // must have rsMeter on the correct record
            // 1-Consumption 2-Flat 3-Unit
            for (intCT = 1; intCT <= 5; intCT++)
            {
                // TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
                if (FCConvert.ToBoolean(rsMeter.Get_Fields("UseRate" + FCConvert.ToString(intCT))))
                {
                    // check to see if this number is used
                    if (FCConvert.ToInt32(rsMeter.Get_Fields(strWS + "Type" + FCConvert.ToString(intCT))) == BType)
                    {
                        // then check to see if the water type is the same
                        BilledType = true;
                        break;
                    }
                }
            }
            return BilledType;
        }

        private short UpdateBillRecord_18(clsDRWrapper rsM, int BK, bool boolAddBreakdownRecords = false, clsDRWrapper rsMaster = null)
        {
            return UpdateBillRecord(ref rsM, ref BK, boolAddBreakdownRecords, rsMaster);
        }

        private short UpdateBillRecord(ref clsDRWrapper rsM, ref int BK, bool boolAddBreakdownRecords = false, clsDRWrapper rsMaster = null)
        {
            short UpdateBillRecord = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this sub will calculate the current bill/meter information in order to
                // fill the rest of the bill record
                string Service = "";
                int intCT;
                double[,] dblBillAmount = new double[5 + 1, 1 + 1];
                // (1 - Cons, 2 - Flat, 3 - Units, 4 - Misc1, 5 - Misc2, 0 - Water, 1 - Sewer)
                // vbPorter upgrade warning: lngCons As int	OnWrite(int, double)
                int lngCons = 0;
                int lngActualCons = 0;
                double dblUnits = 0;
                int lngRT = 0;
                string strMisc1Desc = "";
                string strMisc2Desc = "";
                double dblMisc1 = 0;
                double dblMisc2 = 0;
                double dblMiscSum1 = 0;
                double dblMiscSum2 = 0;
                double dblSAdjustAmount = 0;
                double dblWAdjustAmount = 0;
                double dblSDEAdjustAmount = 0;
                double dblWDEAdjustAmount = 0;
                double dblDEAdjustAmount = 0;
                double dblCons = 0;
                double dblFlat = 0;
                double dblUnit = 0;
                double dblSumCons = 0;
                double dblSumFlat = 0;
                double dblSumUnit = 0;
                double dblWTax = 0;
                double dblSTax = 0;
                double dblTaxableAmount;
                // vbPorter upgrade warning: dblOverrideAmount As double	OnRead(int, double)
                double dblOverrideAmount = 0;
                double dblDEOverrideW = 0;
                double dblDEOverrideS = 0;
                // this array will only hold the taxable amount
                // 0 - Consumption, 1- Flat, 2- Units, 3 - Misc, 4 - Data Entry Adjustments, 5 - Meter Adjustments
                double[] dblTaxes = new double[5 + 1];
                rsBill = new clsDRWrapper();
                dblTaxableAmount = 0;
                boolCurMeterMinimum = true;

                strSQL = "SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(BK) + " AND BillStatus <> 'B' ORDER BY CurDate Asc";
                rsBill.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                if (rsBill.RecordCount() != 0)
                {
                    rsBill.Edit();
                    dblMiscSum1 = 0;
                    dblMiscSum2 = 0;
                    dblSumCons = 0;
                    dblSumFlat = 0;
                    dblSumUnit = 0;
                    dblCons = 0;
                    dblFlat = 0;
                    dblUnit = 0;
                    if (!(rsMaster == null))
                    {
                        if (!rsMaster.EndOfFile())
                        {
                            // TODO Get_Fields: Field [OwnerName] not found!! (maybe it is an alias?)
                            rsBill.Set_Fields("OName", rsMaster.Get_Fields("OwnerName"));
                            // TODO Get_Fields: Field [SecondOwnerName] not found!! (maybe it is an alias?)
                            rsBill.Set_Fields("OName2", rsMaster.Get_Fields("SecondOwnerName"));
                            rsBill.Set_Fields("OAddress1", rsMaster.Get_Fields_String("OAddress1"));
                            rsBill.Set_Fields("OAddress2", rsMaster.Get_Fields_String("OAddress2"));
                            rsBill.Set_Fields("OAddress3", rsMaster.Get_Fields_String("OAddress3"));
                            rsBill.Set_Fields("OCity", rsMaster.Get_Fields_String("OCity"));
                            rsBill.Set_Fields("OState", rsMaster.Get_Fields_String("OState"));
                            rsBill.Set_Fields("OZip", rsMaster.Get_Fields_String("OZip"));
                            rsBill.Set_Fields("OZip4", rsMaster.Get_Fields_String("OZip4"));
                            if (rsMaster.Get_Fields_Int32("SameBillOwner") == 0)
                            {
                                rsBill.Set_Fields("BName", rsMaster.Get_Fields_String("Name"));
                                rsBill.Set_Fields("BName2", rsMaster.Get_Fields_String("Name2"));
                            }
                            else
                            {
                                rsBill.Set_Fields("BName", rsMaster.Get_Fields_String("OwnerName"));
                                rsBill.Set_Fields("BName2", rsMaster.Get_Fields_String("SecondOwnerName"));
                            }

                            rsBill.Set_Fields("BAddress1", rsMaster.Get_Fields_String("BAddress1"));
                            rsBill.Set_Fields("BAddress2", rsMaster.Get_Fields_String("BAddress2"));
                            rsBill.Set_Fields("BAddress3", rsMaster.Get_Fields_String("BAddress3"));
                            rsBill.Set_Fields("BCity", rsMaster.Get_Fields_String("BCity"));
                            rsBill.Set_Fields("BState", rsMaster.Get_Fields_String("BState"));
                            rsBill.Set_Fields("BZip", rsMaster.Get_Fields_String("BZip"));
                            rsBill.Set_Fields("BZip4", rsMaster.Get_Fields_String("BZip4"));
                        }
                    }
                    rsBill.Set_Fields("Book", rsMeter.Get_Fields_Int32("BookNumber"));
                    if ((FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "S" && rsBill.Get_Fields_Double("WPrinPaid") != 0) || (FCConvert.ToString(rsMeter.Get_Fields_String("Service")) == "W" && rsBill.Get_Fields_Double("SPrinPaid") != 0))
                    {
                        // kk04222014 trout-1081  Don't change service if a prepay exists on other service
                        rsBill.Set_Fields("Service", "B");
                    }
                    else
                    {
                        rsBill.Set_Fields("Service", rsMeter.Get_Fields_String("Service"));
                    }
                    if (!FCConvert.ToBoolean(rsBill.Get_Fields_Boolean("NoBill")))
                    {
                        // If rsM.Fields("FinalBillDate") <> "" Then
                        // frmWait.Top = -2000
                        // MsgBox "Stop"
                        // End If
                        // If .Fields("ActualAccountNumber") = 1 Then
                        // frmWait.Top = -2000
                        // MsgBox "stop"
                        // End If
                        // check to see if this meter is combined by consumption
                        // the combination by consumption will be taken care
                        // of when calculating the main meter
                        if (rsM.Get_Fields_String("Combine") != "C")
                        {
                            // Water
                            if (rsM.Get_Fields_String("Service") != "S")
                            {
                                // kk01282014 trouts-68  ALWAYS Bill Stormwater fee, no exceptions
                                // kk 03052013 trouts-6  Bangor Stormwater Fee   '05212013  Only stormwater fee on "W" meter
                                // kk 06122013 trouts-6  Change back to bill stormwater with sewer
                                // If UCase(Left(MuniName, 6)) = "BANGOR" And Not boolFinal Then               ' And rsM.Fields("Service") = "W" Then
                                // IF FREQUENCY IS QUARTERLY OR (MONTHLY AND BEGINNING OF PERIOD)
                                // If (rsM.Fields("Frequency") = 1 Or rsM.Fields("Frequency") = 2) And boolBillStormwater Then
                                // dblFlat = GetStormwaterFee(rsAcct.Fields("ImpervSurfArea"), rsM.Fields("Frequency") = 2)
                                // dblSumFlat = dblSumFlat + dblFlat
                                // If dblFlat <> 0 And boolAddBreakdownRecords Then
                                // AddBreakDownRecord True, .Fields("ID"), rsM.Fields("ID"), "W", "Stormwater Fee", 0, "", dblFlat, "F"
                                // End If
                                // End If
                                // Else
                                if (Conversion.Val(rsBill.Get_Fields_Decimal("WaterOverrideAmount")) != 0)
                                {
                                    // this is a bill override from the DE screen that will
                                    // not allow anything else to get calculated
                                    if (rsM.Get_Fields_String("Combine") == "N")
                                    {
                                        dblDEOverrideW = Conversion.Val(rsBill.Get_Fields_Decimal("WaterOverrideAmount"));
                                        dblTaxableAmount = Conversion.Val(rsBill.Get_Fields_Decimal("WaterOverrideAmount"));
                                    }
                                    else
                                    {
                                        dblDEOverrideW = 0;
                                        dblTaxableAmount = 0;
                                    }
                                    if (boolAddBreakdownRecords && FCUtils.Round(dblDEOverrideW, 2) != 0)
                                    {
                                        modUTBilling.AddBreakDownRecord_15290(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "DE Override", 0, "", dblDEOverrideW, "O");
                                    }
                                }
                                else
                                {
                                    dblDEOverrideW = 0;
                                    for (intCT = 1; intCT <= 5; intCT++)
                                    {
                                        //Application.DoEvents();
                                        // TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
                                        if (rsM.Get_Fields("UseRate" + FCConvert.ToString(intCT)))
                                        {
                                            dblMisc1 = 0;
                                            dblMisc2 = 0;
                                            strMisc1Desc = "";
                                            strMisc2Desc = "";
                                            lngRT = rsM.Get_Fields_Int32("WaterKey" + FCConvert.ToString(intCT));
                                            // get the rate table
                                            rsBill.Set_Fields("WRT" + intCT, lngRT);
                                            // If rsM.Fields("AccountKey") = 161 Then
                                            // frmWait.Visible = False
                                            // MsgBox "Stop"
                                            // End If
                                            // TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
                                            if (Conversion.Val(rsM.Get_Fields("WaterAmount" + FCConvert.ToString(intCT))) != 0)
                                            {
                                                // TODO Get_Fields: Field [WaterAmount] not found!! (maybe it is an alias?)
                                                dblOverrideAmount = FCConvert.ToDouble(rsM.Get_Fields("WaterAmount" + FCConvert.ToString(intCT)));
                                                // get the override amount
                                                // MAL@20071129: Add change to override flag
                                                rsBill.Set_Fields("WHasOverride", true);
                                            }
                                            else
                                            {
                                                dblOverrideAmount = 0;
                                            }
                                            // TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
                                            if (rsM.Get_Fields("WaterType" + FCConvert.ToString(intCT)) == 1)
                                            {
                                                // consumption
                                                // DJW 07/09/09 Commented out check for current reading not being 0 since it coudl have been changed out and have a reading of 0 and still have replacementconsumption
                                                // If rsM.Fields("CurrentReading") <> 0 Then
                                                // kk 110812 trout-884 Change so -1 is no reading
                                                if (rsM.Get_Fields_Int32("CurrentReading") != -1)
                                                {
                                                    if (rsM.Get_Fields_Int32("MeterNumber") == 1)
                                                    {
                                                        if (FCConvert.ToInt32(rsBill.Get_Fields_Int32("WaterOverrideCons")) == 0)
                                                        {
                                                            lngCons = modUTBilling.FindTotalMeterConsumption_26(rsBill.Get_Fields_Int32("AccountKey"), true, rsBill.Get_Fields_Int32("WaterOverrideCons"));
                                                        }
                                                        else
                                                        {
                                                            lngCons = FCConvert.ToInt32(rsBill.Get_Fields_Int32("WaterOverrideCons"));
                                                        }
                                                    }
                                                    else if (FCConvert.ToInt32(dblOverrideAmount) != 0)
                                                    {
                                                        lngCons = FCConvert.ToInt32(dblOverrideAmount);
                                                    }
                                                    else
                                                    {
                                                        lngCons = rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading");
                                                    }
                                                }
                                                else
                                                {
                                                    if (FCConvert.ToInt32(dblOverrideAmount) != 0)
                                                    {
                                                        // No reading, check for override cons
                                                        lngCons = FCConvert.ToInt32(dblOverrideAmount);
                                                    }
                                                    else
                                                    {
                                                        lngCons = 0;
                                                    }
                                                }
                                                lngActualCons = lngCons;
                                                dblCons = FCUtils.Round(modUTBilling.CalculateConsumptionBased_13140(ref lngCons, lngRT, true, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), ref boolCurMeterMinimum), 2);
                                                dblSumCons += dblCons;
                                                if (dblCons != 0 && boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_133388(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Consumption", 0, "", dblCons, "C", lngRT, FCConvert.ToDouble(lngCons));
                                                }
                                            }
                                            // TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
                                            else if (rsM.Get_Fields("WaterType" + FCConvert.ToString(intCT)) == 2)
                                            {
                                                // flat
                                                dblFlat = FCUtils.Round((modUTBilling.CalculateFlatBased_6(lngRT, true, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, dblOverrideAmount, ref strMisc1Desc, ref strMisc2Desc) * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
                                                dblSumFlat += dblFlat;
                                                if (dblFlat != 0 && boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_19664(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Flat", 0, "", dblFlat, "F", lngRT);
                                                }
                                            }
                                            // TODO Get_Fields: Field [WaterType] not found!! (maybe it is an alias?)
                                            else if (rsM.Get_Fields("WaterType" + FCConvert.ToString(intCT)) == 3)
                                            {
                                                // units
                                                lngCons = FCConvert.ToInt32(dblOverrideAmount);
                                                // set the amount of units
                                                dblUnits = dblOverrideAmount;
                                                dblUnit = FCUtils.Round(modUTBilling.CalculateUnitBased_13140(dblUnits, lngRT, true, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
                                                dblSumUnit += dblUnit;
                                                if (dblUnit != 0 && boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_15290(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Unit", 0, "", dblUnit, "U", lngRT, dblUnits);
                                                    // kgk 12-07-2011 trout-789  Change to use double 'CDbl(lngCons)
                                                }
                                            }
                                            // add up all of the misc amounts
                                            if (dblMisc1 != 0)
                                            {
                                                if (boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_15128(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", strMisc1Desc, 0, "", dblMisc1, "M", lngRT);
                                                }
                                                dblMiscSum1 += dblMisc1;
                                            }
                                            if (dblMisc2 != 0)
                                            {
                                                if (boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_15128(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", strMisc2Desc, 0, "", dblMisc2, "M", lngRT);
                                                }
                                                dblMiscSum2 += dblMisc2;
                                            }
                                        }
                                    }
                                    if (rsM.Get_Fields_Boolean("UseAdjustRate") && rsM.Get_Fields_Double("WaterAdjustAmount") != 0)
                                    {
                                        dblWAdjustAmount = rsM.Get_Fields_Double("WaterAdjustAmount");
                                        if (boolAddBreakdownRecords)
                                        {
                                            modUTBilling.AddBreakDownRecord_15290(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Adjustment", 0, "", dblWAdjustAmount, "A");
                                        }
                                    }
                                    else
                                    {
                                        dblWAdjustAmount = 0;
                                    }
                                    // DJW@10132009 Commented this out as it was doublgin the adjustment entered in the data entry screen
                                    // DJW@10062010 Uncommented because the adjustment is not getting taken into acocunt when doing a final billing
                                    // kgk 12-08-2011 trout-786  Change to only add a generic adjustment breakdown record if required
                                    dblWDEAdjustAmount = FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WDEAdjustAmount"));
                                    dblDEAdjustAmount = GetDEAdjustValues_2("W");
                                    if (dblWDEAdjustAmount != dblDEAdjustAmount)
                                    {
                                        // If dblWDEAdjustAmount <> 0 Then
                                        if (boolAddBreakdownRecords)
                                        {
                                            modUTBilling.AddBreakDownRecord_19664(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Adjustment", 0, "", dblWDEAdjustAmount - dblDEAdjustAmount, "A");
                                        }
                                    }
                                    rsBill.Set_Fields("Consumption", lngActualCons);
                                }
                                // kk01282014 trouts-68
                                // End If   ' BANGOR STORMWATER FEE trouts-6
                                // fill in the water category for this bill
                                rsBill.Set_Fields("WCat", modMain.GetCategory_8(true, rsBill.Get_Fields_Int32("AccountKey")));
                            }
                            else
                            {
                                rsBill.Set_Fields("WCat", 0);
                            }
                            // add taxes
                            dblWTax = CalculateTaxes_2(true, rsM.Get_Fields_Int32("ID"), dblTaxableAmount);
                            if (boolAddBreakdownRecords && FCUtils.Round(dblWTax, 2) != 0)
                            {
                                modUTBilling.AddBreakDownRecord_15290(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Tax", 0, "", dblWTax, "T");
                            }
                            dblTaxableAmount = 0;
                            dblOverrideAmount = 0;
                            if (rsM.Get_Fields_String("Combine") == "B")
                            {
                                // WDEAdjustAmount is filled in the DE screen
                                rsBill.Set_Fields("WConsumptionAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WConsumptionAmount")) + dblSumCons);
                                rsBill.Set_Fields("WFlatAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WFlatAmount")) + dblSumFlat);
                                rsBill.Set_Fields("WUnitsAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WUnitsAmount")) + dblSumUnit);
                                rsBill.Set_Fields("WMiscAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WMiscAmount")) + dblMiscSum1 + dblMiscSum2);
                                // TODO Get_Fields: Check the table for the column [WTax] and replace with corresponding Get_Field method
                                rsBill.Set_Fields("WTax", Conversion.Val(rsBill.Get_Fields("WTax")) + dblWTax);
                                rsBill.Set_Fields("WAdjustAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("WAdjustAmount")) + dblWAdjustAmount + dblDEOverrideW);
                            }
                            else
                            {
                                // WDEAdjustAmount is filled in the DE screen
                                rsBill.Set_Fields("WConsumptionAmount", dblSumCons);
                                rsBill.Set_Fields("WFlatAmount", dblSumFlat);
                                rsBill.Set_Fields("WUnitsAmount", dblSumUnit);
                                rsBill.Set_Fields("WMiscAmount", dblMiscSum1 + dblMiscSum2);
                                rsBill.Set_Fields("WTax", dblWTax);
                                rsBill.Set_Fields("WAdjustAmount", dblWAdjustAmount + dblDEOverrideW);
                            }
                            dblWTax = 0;
                            dblMiscSum1 = 0;
                            dblMiscSum2 = 0;
                            dblSumCons = 0;
                            dblSumFlat = 0;
                            dblSumUnit = 0;
                            dblCons = 0;
                            dblFlat = 0;
                            dblUnit = 0;
                            // If rsM.Fields("AccountKey") = 161 Then
                            // frmWait.Visible = False
                            // MsgBox "Stop"
                            // End If
                            // Sewer
                            if (rsM.Get_Fields_String("Service") != "W")
                            {
                                // trout-684 kgk 03-21-2011  Mod for Jay - don't allow bill to exceed 2x the prev bill
                                // when switching from Flat to Consumption
                                if (Conversion.Val(rsBill.Get_Fields_Decimal("SewerOverrideAmount")) == 0)
                                {
                                    if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "JAY" && boolLimitBillIncr == true)
                                    {
                                        // Customize option
                                        Limit2xIncrOnSwitch_2(rsBill.Get_Fields_Int32("AccountKey"), rsBill, rsM);
                                    }
                                }
                                if (Conversion.Val(rsBill.Get_Fields_Decimal("SewerOverrideAmount")) != 0)
                                {
                                    // this is a bill override from the DE screen that will
                                    // not allow anything else to get calculated
                                    if (rsM.Get_Fields_String("Combine") == "N")
                                    {
                                        dblDEOverrideS = Conversion.Val(rsBill.Get_Fields_Decimal("SewerOverrideAmount"));
                                        dblTaxableAmount = Conversion.Val(rsBill.Get_Fields_Decimal("SewerOverrideAmount"));
                                    }
                                    else
                                    {
                                        dblDEOverrideS = 0;
                                        dblTaxableAmount = 0;
                                    }
                                    if (boolAddBreakdownRecords && FCUtils.Round(dblDEOverrideS, 2) != 0)
                                    {
                                        modUTBilling.AddBreakDownRecord_15290(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "DE Override", 0, "", dblDEOverrideS, "O");
                                    }
                                }
                                else
                                {
                                    dblDEOverrideS = 0;
                                    for (intCT = 1; intCT <= 5; intCT++)
                                    {
                                        // TODO Get_Fields: Field [UseRate] not found!! (maybe it is an alias?)
                                        if (rsM.Get_Fields("UseRate" + FCConvert.ToString(intCT)))
                                        {
                                            dblMisc1 = 0;
                                            dblMisc2 = 0;
                                            strMisc1Desc = "";
                                            strMisc2Desc = "";
                                            lngRT = rsM.Get_Fields_Int32("SewerKey" + FCConvert.ToString(intCT));
                                            // get the rate table
                                            rsBill.Set_Fields("SRT" + intCT, lngRT);
                                            // TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
                                            if (Conversion.Val(rsM.Get_Fields("SewerAmount" + FCConvert.ToString(intCT))) != 0)
                                            {
                                                // TODO Get_Fields: Field [SewerAmount] not found!! (maybe it is an alias?)
                                                dblOverrideAmount = FCConvert.ToDouble(rsM.Get_Fields("SewerAmount" + FCConvert.ToString(intCT)));
                                                // get the override amount
                                                // MAL@20071129: Add change to override flag
                                                rsBill.Set_Fields("SHasOverride", true);
                                            }
                                            else
                                            {
                                                dblOverrideAmount = 0;
                                            }
                                            // TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
                                            if (rsM.Get_Fields("SewerType" + FCConvert.ToString(intCT)) == 1)
                                            {
                                                // consumption
                                                // DJW 07/09/09 Commented out check for current reading not being 0 since it coudl have been changed out and have a reading of 0 and still have replacementconsumption
                                                // kk 110812 trout-884  change to -1 is no read
                                                if (rsM.Get_Fields_Int32("CurrentReading") != -1)
                                                {
                                                    if (rsM.Get_Fields_Int32("MeterNumber") == 1)
                                                    {
                                                        if (FCConvert.ToInt32(rsBill.Get_Fields_Int32("SewerOverrideCons")) == 0)
                                                        {
                                                            lngCons = modUTBilling.FindTotalMeterConsumption_26(rsBill.Get_Fields_Int32("AccountKey"), false, rsBill.Get_Fields_Int32("SewerOverrideCons"));
                                                        }
                                                        else
                                                        {
                                                            lngCons = FCConvert.ToInt32(rsBill.Get_Fields_Int32("SewerOverrideCons"));
                                                        }
                                                    }
                                                    else if (FCConvert.ToInt32(dblOverrideAmount) != 0)
                                                    {
                                                        lngCons = FCConvert.ToInt32(dblOverrideAmount);
                                                    }
                                                    else
                                                    {
                                                        lngCons = rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading");
                                                    }
                                                }
                                                else
                                                {
                                                    if (FCConvert.ToInt32(dblOverrideAmount) != 0)
                                                    {
                                                        // No read w/ override cons
                                                        lngCons = FCConvert.ToInt32(dblOverrideAmount);
                                                    }
                                                    else
                                                    {
                                                        lngCons = 0;
                                                    }
                                                }
                                                lngActualCons = lngCons;
                                                dblCons = FCUtils.Round(modUTBilling.CalculateConsumptionBased_13140(ref lngCons, lngRT, false, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), ref boolCurMeterMinimum), 2);
                                                dblSumCons += dblCons;
                                                if (dblCons != 0 && boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_133388(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Consumption", 0, "", dblCons, "C", lngRT, FCConvert.ToDouble(lngCons));
                                                }
                                            }
                                            // TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
                                            else if (rsM.Get_Fields("SewerType" + FCConvert.ToString(intCT)) == 2)
                                            {
                                                // flat
                                                dblFlat = FCUtils.Round((modUTBilling.CalculateFlatBased_6(lngRT, false, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, dblOverrideAmount, ref strMisc1Desc, ref strMisc2Desc) * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
                                                dblSumFlat += dblFlat;
                                                if (dblFlat != 0 && boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_19664(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Flat", 0, "", dblFlat, "F", lngRT);
                                                }
                                            }
                                            // TODO Get_Fields: Field [SewerType] not found!! (maybe it is an alias?)
                                            else if (rsM.Get_Fields("SewerType" + FCConvert.ToString(intCT)) == 3)
                                            {
                                                // units
                                                lngCons = FCConvert.ToInt32(dblOverrideAmount);
                                                // set the amount of units
                                                dblUnits = dblOverrideAmount;
                                                dblUnit = FCUtils.Round(modUTBilling.CalculateUnitBased_13140(dblUnits, lngRT, false, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
                                                dblSumUnit += dblUnit;
                                                if (dblUnit != 0 && boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_15290(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Unit", 0, "", dblUnit, "U", lngRT, dblUnits);
                                                    // kgk 12-07-2011 trout-789  Change to use double 'CDbl(lngCons)
                                                }
                                            }
                                            // add up all of the misc amounts
                                            if (dblMisc1 != 0)
                                            {
                                                if (boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_15128(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", strMisc1Desc, 0, "", dblMisc1, "M", lngRT);
                                                }
                                                dblMiscSum1 += dblMisc1;
                                            }
                                            if (dblMisc2 != 0)
                                            {
                                                if (boolAddBreakdownRecords)
                                                {
                                                    modUTBilling.AddBreakDownRecord_15128(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", strMisc2Desc, 0, "", dblMisc2, "M", lngRT);
                                                }
                                                dblMiscSum2 += dblMisc2;
                                            }
                                        }
                                    }
                                    if (rsM.Get_Fields_Boolean("UseAdjustRate") && rsM.Get_Fields_Double("SewerAdjustAmount") != 0)
                                    {
                                        dblSAdjustAmount = FCConvert.ToDouble(rsM.Get_Fields_Double("SewerAdjustAmount"));
                                        if (boolAddBreakdownRecords)
                                        {
                                            modUTBilling.AddBreakDownRecord_15290(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Adjustment", 0, "", dblSAdjustAmount, "A");
                                        }
                                    }
                                    else
                                    {
                                        dblSAdjustAmount = 0;
                                    }
                                    // DJW@10132009 Commented this out as it was doublgin the adjustment entered in the data entry screen
                                    // kgk 12-08-2011 trout-786  Change to only add a generic adjustment breakdown record if required
                                    dblSDEAdjustAmount = FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SDEAdjustAmount"));
                                    dblDEAdjustAmount = FCConvert.ToDouble(GetDEAdjustValues_2("S"));
                                    if (dblSDEAdjustAmount != dblDEAdjustAmount)
                                    {
                                        // If dblSDEAdjustAmount <> 0 Then
                                        if (boolAddBreakdownRecords)
                                        {
                                            modUTBilling.AddBreakDownRecord_19664(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Adjustment", 0, "", dblSDEAdjustAmount - dblDEAdjustAmount, "A");
                                        }
                                    }
                                    rsBill.Set_Fields("Consumption", lngActualCons);
                                }
                                // fill in the sewer category for this bill
                                rsBill.Set_Fields("SCat", modMain.GetCategory_8(false, rsBill.Get_Fields_Int32("AccountKey")));
                            }
                            else
                            {
                                rsBill.Set_Fields("SCat", 0);
                            }
                            dblSTax = CalculateTaxes_2(false, rsM.Get_Fields_Int32("ID"), dblTaxableAmount);
                            if (boolAddBreakdownRecords && FCUtils.Round(dblSTax, 2) != 0)
                            {
                                modUTBilling.AddBreakDownRecord_15290(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "S", "Tax", 0, "", dblSTax, "T");
                            }
                            dblTaxableAmount = 0;
                            dblOverrideAmount = 0;
                            if (rsM.Get_Fields_String("Combine") == "B")
                            {
                                // SDEAdjustAmount is filled in the DE screen
                                rsBill.Set_Fields("SConsumptionAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SConsumptionAmount")) + dblSumCons);
                                rsBill.Set_Fields("SFlatAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SFlatAmount")) + dblSumFlat);
                                rsBill.Set_Fields("SUnitsAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SUnitsAmount")) + dblSumUnit);
                                rsBill.Set_Fields("SMiscAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SMiscAmount")) + dblMiscSum1 + dblMiscSum2);
                                // TODO Get_Fields: Check the table for the column [STax] and replace with corresponding Get_Field method
                                rsBill.Set_Fields("STax", Conversion.Val(rsBill.Get_Fields("STax")) + dblSTax);
                                rsBill.Set_Fields("SAdjustAmount", FCConvert.ToDouble(rsBill.Get_Fields_Decimal("SAdjustAmount")) + dblSAdjustAmount + dblDEOverrideS);
                            }
                            else
                            {
                                // SDEAdjustAmount is filled in the DE screen
                                rsBill.Set_Fields("SConsumptionAmount", dblSumCons);
                                rsBill.Set_Fields("SFlatAmount", dblSumFlat);
                                rsBill.Set_Fields("SUnitsAmount", dblSumUnit);
                                rsBill.Set_Fields("SMiscAmount", dblMiscSum1 + dblMiscSum2);
                                rsBill.Set_Fields("STax", dblSTax);
                                rsBill.Set_Fields("SAdjustAmount", dblSAdjustAmount + dblDEOverrideS);
                            }
                        }
                    }
                    else
                    {
                        // DJW@01112013 TROUT-895 added category so you cna see consumption for no charge bills on consumption summary in billing edit report
                        if (rsM.Get_Fields_String("Service") != "S")
                        {
                            rsBill.Set_Fields("WCat", modMain.GetCategory_8(true, rsBill.Get_Fields_Int32("AccountKey")));
                        }
                        else
                        {
                            rsBill.Set_Fields("WCat", 0);
                        }
                        rsBill.Set_Fields("WConsumptionAmount", 0);
                        // kk08272015 trouts-158  This is wiping Bangor's stormwater fee out on Combined meters with No Charge checked
                        if (!(Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR"))
                        {
                            rsBill.Set_Fields("WFlatAmount", 0);
                        }
                        rsBill.Set_Fields("WUnitsAmount", 0);
                        rsBill.Set_Fields("WMiscAmount", 0);
                        rsBill.Set_Fields("WTax", 0);
                        rsBill.Set_Fields("WAdjustAmount", 0);
                        // DJW@01112013 TROUT-895 added category so you cna see consumption for no charge bills on consumption summary in billing edit report
                        if (rsM.Get_Fields_String("Service") != "W")
                        {
                            rsBill.Set_Fields("SCat", modMain.GetCategory_8(false, rsBill.Get_Fields_Int32("AccountKey")));
                        }
                        else
                        {
                            rsBill.Set_Fields("SCat", 0);
                        }
                        rsBill.Set_Fields("SConsumptionAmount", 0);
                        rsBill.Set_Fields("SFlatAmount", 0);
                        rsBill.Set_Fields("SUnitsAmount", 0);
                        rsBill.Set_Fields("SMiscAmount", 0);
                        rsBill.Set_Fields("STax", 0);
                        rsBill.Set_Fields("SAdjustAmount", 0);
                    }
                    // kk01212016 trouts-162 Don't Bill Stormwater if No Charge flag is set on the account
                    // kk01282014 trouts-68  ALWAYS Bill Stormwater fee, no exceptions
                    // kk 06122013 trouts-6  Change back to bill stormwater with sewer
                    // kk 03052013 trouts-6  Bangor Stormwater Fee   '05212013  Only stormwater fee on "W" meter
                    if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
                    {
                        // kk03132015 trouts-106   And Not boolFinal Then               ' And rsM.Fields("Service") = "W" Then
                        if (!rsMaster.Get_Fields_Boolean("NoBill"))
                        {
                            // IF FREQUENCY IS QUARTERLY OR (MONTHLY AND BEGINNING OF PERIOD)      'kk07082015 trouts-149  Add a check for ImpervSurfArea >= 0
                            // TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                            // TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                            if ((rsM.Get_Fields("Frequency") == 1 || rsM.Get_Fields("Frequency") == 2) && boolBillStormwater && rsAcct.Get_Fields_Decimal("ImpervSurfArea") >= 0)
                            {
                                // TODO Get_Fields: Check the table for the column [Frequency] and replace with corresponding Get_Field method
                                dblFlat = FCUtils.Round((modUTBilling.GetStormwaterFee_8(FCConvert.ToDouble(rsAcct.Get_Fields_Decimal("ImpervSurfArea")), rsM.Get_Fields("Frequency") == 2) * modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal)), 2);
                                // kk08062014 trouts-109   This is totaling Sewer flat fee + Stormwater fee and saving as Stormwater Fee    ' dblSumFlat = dblSumFlat + dblFlat
                                if (dblFlat != 0 && boolAddBreakdownRecords)
                                {
                                    modUTBilling.AddBreakDownRecord_19664(true, rsBill.Get_Fields_Int32("ID"), rsM.Get_Fields_Int32("ID"), "W", "Stormwater Fee", 0, "", dblFlat, "F");
                                    rsBill.Set_Fields("WConsumptionAmount", 0);
                                    rsBill.Set_Fields("WFlatAmount", dblFlat);
                                    // kk08062014 trouts-109   dblSumFlat
                                    rsBill.Set_Fields("WUnitsAmount", 0);
                                    rsBill.Set_Fields("WMiscAmount", 0);
                                    rsBill.Set_Fields("WTax", 0);
                                    rsBill.Set_Fields("WAdjustAmount", 0);
                                    // kk01282013 trouts-68  Force a Stormwater Bill even if No Bill is set
                                    // 
                                }
                            }
                        }
                    }
                    // save the changes made
                    rsBill.Update();
                }
                UpdateBillRecord = 0;
                return UpdateBillRecord;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Updating Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return UpdateBillRecord;
        }

        private int ShowBreakdownRecords_24(int lngBK, int lngMK, int intROLevel, ref double dblTotalW, ref double dblTotals)
        {
            return ShowBreakdownRecords(ref lngBK, ref lngMK, ref intROLevel, ref dblTotalW, ref dblTotals);
        }

        private int ShowBreakdownRecords(ref int lngBK, ref int lngMK, ref int intROLevel, ref double dblTotalW, ref double dblTotals)
        {
            int ShowBreakdownRecords = 0;
            clsDRWrapper rsBD = new clsDRWrapper();
            clsDRWrapper rsCM = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will calculate and return the regular water and sewer total
                // as well as create subtotal rows if necessary
                // to load the breakdown records for each bill
                // this will be used to load the combined meters for main meters
                double TotalS = 0;

                // Sewer Subtotal
                double TotalW = 0;

                // Water Subtotal
                int XtraRows = 0;

                // count of how many rows that were added
                int lngRow = 0;
                int lngLastParentRow = 0;
                double dblWTax;
                double dblSTax;
                lngRow = vsCalc.Rows - 1;
                rsBD.OpenRecordset("SELECT * FROM TempBreakDown WHERE BillKey = " + FCConvert.ToString(lngBK) + " AND MeterKey = " + FCConvert.ToString(lngMK) + " ORDER BY Service, Type, Description", modExtraModules.strUTDatabase);

                if (!rsBD.EndOfFile())
                {
                    while (!rsBD.EndOfFile())
                    {
                        // this will fill the grid with all of the breakdown
                        // If rsBD.Fields("Type") <> "T" Then
                        XtraRows += 1;
                        string vbPorterVar = rsBD.Get_Fields_String("Service");

                        if (vbPorterVar.Equals("W"))
                        {
                            TotalW += FillBreakDownRow_18(rsBD, intROLevel, true);
                        }
                        else
                            if (vbPorterVar.Equals("S"))
                        {
                            TotalS += FillBreakDownRow_18(rsBD, intROLevel, false);
                        }

                        // End If
                        rsBD.MoveNext();
                    }

                    // check to see if there are any meters combined by bill that have breakdown records to add
                    rsCM.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(lngMK), modExtraModules.strUTDatabase);

                    if (!rsCM.EndOfFile())
                    {
                        if (FCConvert.ToInt32(rsCM.Get_Fields_Int32("MeterNumber")) == 1)
                        {
                            rsCM.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsCM.Get_Fields_Int32("AccountKey") + " AND Combine = 'B' ORDER BY Sequence", modExtraModules.strUTDatabase);

                            while (!rsCM.EndOfFile())
                            {
                                rsBD.OpenRecordset("SELECT * FROM TempBreakDown WHERE BillKey = " + FCConvert.ToString(lngBK) + " AND MeterKey = " + rsCM.Get_Fields_Int32("ID") + " ORDER BY Service, Type, Description", modExtraModules.strUTDatabase);

                                while (!rsBD.EndOfFile())
                                {
                                    // this will fill the grid with all of the breakdown
                                    // If rsBD.Fields("Type") <> "T" Then
                                    XtraRows += 1;
                                    string vbPorterVar1 = rsBD.Get_Fields_String("Service");

                                    if (vbPorterVar1.Equals("W"))
                                    {
                                        TotalW += FillBreakDownRow_18(rsBD, intROLevel, true);
                                    }
                                    else
                                        if (vbPorterVar1.Equals("S"))
                                    {
                                        TotalS += FillBreakDownRow_18(rsBD, intROLevel, false);
                                    }

                                    // End If
                                    rsBD.MoveNext();
                                }

                                rsCM.MoveNext();
                            }
                        }
                    }

                    // this sets up the sub total row
                    AddCurrentRow(vsCalc.Rows, intROLevel);
                    lngRow = vsCalc.Rows - 1;

                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColConsumption, lngRow, lngGridColSewer, 0xFFC0C0);
                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColConsumption, lngRow, lngGridColSewer, Color.White);
                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngRow, lngGridColConsumption, lngRow, lngGridColSewer, true);
                    vsCalc.TextMatrix(lngRow, lngGridColConsumption, "Subtotals");
                    vsCalc.TextMatrix(lngRow, lngGridColWater, FCConvert.ToString(TotalW));
                    vsCalc.TextMatrix(lngRow, lngGridColSewer, FCConvert.ToString(TotalS));
                    lngLastParentRow = GetLastParentMeterRow(ref lngRow);

                    if (lngLastParentRow > 0)
                    {
                        // put the same totals in the parent meter row
                        vsCalc.TextMatrix(lngLastParentRow, lngGridColWater, FCConvert.ToString(TotalW));
                        vsCalc.TextMatrix(lngLastParentRow, lngGridColSewer, FCConvert.ToString(TotalS));
                    }

                    // keep track of the grand totals
                    dblTotalW = TotalW;
                    dblTotals = TotalS;

                    // rsCM.OpenRecordset "SELECT * FROM MeterTable WHERE MeterKey = " & lngMK, strUTDatabase
                    // If Not rsCM.EndOfFile Then
                    // Debug.Print rsCM.Fields("Sequence") & vbTab & dblTotalW
                    // Else
                    // Debug.Print "No meter for " & lngMK
                    // End If
                    dblWGrandTotal += dblTotalW;
                    dblSGrandTotal += dblTotals;
                }
                else
                {
                    // check to see if there are any meters combined by bill that have breakdown records to add
                    rsCM.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(lngMK), modExtraModules.strUTDatabase);

                    if (!rsCM.EndOfFile())
                    {
                        if (FCConvert.ToInt32(rsCM.Get_Fields_Int32("MeterNumber")) == 1)
                        {
                            rsCM.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsCM.Get_Fields_Int32("AccountKey") + " AND Combine = 'B' ORDER BY Sequence", modExtraModules.strUTDatabase);

                            while (!rsCM.EndOfFile())
                            {
                                rsBD.OpenRecordset("SELECT * FROM TempBreakDown WHERE BillKey = " + FCConvert.ToString(lngBK) + " AND MeterKey = " + rsCM.Get_Fields_Int32("ID") + " ORDER BY Service, Type, Description", modExtraModules.strUTDatabase);

                                while (!rsBD.EndOfFile())
                                {
                                    // this will fill the grid with all of the breakdown
                                    // If rsBD.Fields("Type") <> "T" Then
                                    XtraRows += 1;
                                    string vbPorterVar2 = rsBD.Get_Fields_String("Service");

                                    if (vbPorterVar2.Equals("W"))
                                    {
                                        TotalW += FillBreakDownRow_18(rsBD, intROLevel, true);
                                    }
                                    else
                                        if (vbPorterVar2.Equals("S"))
                                    {
                                        TotalS += FillBreakDownRow_18(rsBD, intROLevel, false);
                                    }

                                    // End If
                                    rsBD.MoveNext();
                                }

                                rsCM.MoveNext();
                            }
                        }
                    }

                    if (TotalW != 0 || TotalS != 0)
                    {
                        // this sets up the sub total row
                        AddCurrentRow(vsCalc.Rows, intROLevel);
                        lngRow = vsCalc.Rows - 1;

                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColConsumption, lngRow, lngGridColSewer, 0xFFC0C0);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColConsumption, lngRow, lngGridColSewer, Color.White);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngRow, lngGridColConsumption, lngRow, lngGridColSewer, true);
                        vsCalc.TextMatrix(lngRow, lngGridColConsumption, "Subtotals");
                        vsCalc.TextMatrix(lngRow, lngGridColWater, FCConvert.ToString(TotalW));
                        vsCalc.TextMatrix(lngRow, lngGridColSewer, FCConvert.ToString(TotalS));
                        lngLastParentRow = GetLastParentMeterRow(ref lngRow);

                        if (lngLastParentRow > 0)
                        {
                            // put the same totals in the parent meter row
                            vsCalc.TextMatrix(lngLastParentRow, lngGridColWater, FCConvert.ToString(TotalW));
                            vsCalc.TextMatrix(lngLastParentRow, lngGridColSewer, FCConvert.ToString(TotalS));
                        }

                        // keep track of the grand totals
                        dblTotalW = TotalW;
                        dblTotals = TotalS;
                        dblWGrandTotal += dblTotalW;
                        dblSGrandTotal += dblTotals;
                    }
                }

                // kk07292015 trout-1135  Force the last row to be set in case there are no breakdown records to show
                if (lngLastParentRow > 0)
                {
                    ShowBreakdownRecords = lngLastParentRow + XtraRows;

                    // returns the last row used
                }
                else
                {
                    ShowBreakdownRecords = vsCalc.Rows - 1;
                }

            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Adding Meter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsCM.DisposeOf();
                rsBD.DisposeOf();
            }

            return ShowBreakdownRecords;
        }

        private void CollapseRows_2(bool Bool)
        {
            CollapseRows(ref Bool);
        }

        private void CollapseRows(ref bool Bool)
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // if Bool = True then collapse all of the rows starting with the subordinate rows
                // if Bool = False then open all rows starting with the subordinate rows
                int Row;
                CollapseFlag = true;
                if (Bool == true)
                {
                    for (Row = 2; Row <= vsCalc.Rows - 1; Row++)
                    {
                        if (vsCalc.RowOutlineLevel(Row) == 1)
                        {
                            if (vsCalc.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
                            {
                                vsCalc.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
                            }
                        }
                    }
                    for (Row = 2; Row <= vsCalc.Rows - 1; Row++)
                    {
                        if (vsCalc.RowOutlineLevel(Row) == 0)
                        {
                            if (vsCalc.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineCollapsed)
                            {
                                vsCalc.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineCollapsed);
                            }
                        }
                    }
                }
                else if (Bool == false)
                {
                    for (Row = 2; Row <= vsCalc.Rows - 1; Row++)
                    {
                        if (vsCalc.RowOutlineLevel(Row) == 0)
                        {
                            if (vsCalc.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineExpanded)
                            {
                                vsCalc.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
                            }
                        }
                    }
                    for (Row = 2; Row <= vsCalc.Rows - 1; Row++)
                    {
                        if (vsCalc.RowOutlineLevel(Row) == 1)
                        {
                            if (vsCalc.IsCollapsed(Row) != FCGrid.CollapsedSettings.flexOutlineExpanded)
                            {
                                vsCalc.IsCollapsed(Row, FCGrid.CollapsedSettings.flexOutlineExpanded);
                            }
                        }
                    }
                }
                vsCalc_Collapsed();
                CollapseFlag = false;
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Collapsing Rows", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: Row As object	OnWriteFCConvert.ToInt32(
        // vbPorter upgrade warning: lvl As object	OnWriteFCConvert.ToInt16(
        private void AddCurrentRow_6(int Row, int lvl, bool boolSubtotal = true)
        {
            AddCurrentRow(Row, lvl, boolSubtotal);
        }

        private void AddCurrentRow_24(int Row, int lvl, bool boolSubtotal = true)
        {
            AddCurrentRow(Row, lvl, boolSubtotal);
        }

        private void AddCurrentRow(int Row, int lvl, bool boolSubtotal = true)
        {
            // set the row as a group
            if (MaxRows == 0)
            {
                MaxRows = 2;
            }
            MaxRows += 1;
            vsCalc.AddItem("", Row);
            // increments the number of rows
            switch (lvl)
            {
                case 0:
                    {
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsCalc.Cols - 1, 0x407000);
                        // &H8080FF &H8000000F&
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, 1, Row, vsCalc.Cols - 1, Color.White);
                        break;
                    }
                case 1:
                    {
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsCalc.Cols - 1, 0x80000018);
                        break;
                    }
                case 2:
                    {
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsCalc.Cols - 1, Color.White);
                        break;
                    }
                default:
                    {
                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, lngGridColCurrent, Row, vsCalc.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsCalc.Cols - 1, 0x80000016);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, lngGridColSeq, Row, lngGridColSeq, 0x80000016);
                        break;
                    }
            }
            //end switch
            // set the indentation level of the group
            vsCalc.RowOutlineLevel(Row, FCConvert.ToInt16(lvl));
            // If lvl = 0 Then
            vsCalc.IsSubtotal(Row, boolSubtotal);
        }

        private void Level(ref int Row, ref int lvl)
        {
            // set the row as a group
            if (MaxRows == 0)
            {
                MaxRows = 2;
            }
            MaxRows += 1;
            vsCalc.Rows = MaxRows + 1;
            // increments the number of rows
            if (FCConvert.ToInt32(lvl) == 0)
            {
                //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsCalc.Cols - 1, 0x80000018);
            }
            else if (FCConvert.ToInt32(lvl) == 1)
            {
                vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, Row, lngGridColCurrent, Row, vsCalc.Cols - 1, FCGrid.AlignmentSettings.flexAlignRightCenter);
                //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsCalc.Cols - 1, 0x80000016);
                //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Row, 1, Row, 1, 0x80000016);
            }
            else
            {
                //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, Row, 1, Row, vsCalc.Cols - 1, Color.White);
            }
            // set the indentation level of the group
            vsCalc.RowOutlineLevel(Row, FCConvert.ToInt16(lvl));
            if (FCConvert.ToInt32(lvl) == 0)
                vsCalc.IsSubtotal(Row, true);
        }

        private double GetMiscValue(ref string Svr)
        {
            double GetMiscValue = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // returns the value of the misc charges from the rate table screen
                double miscTotal = 0;
                // TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
                if (fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields("MiscChargeAmount" + Svr + "1")) == false)
                {
                    // TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
                    miscTotal += Conversion.Val(rsRate.Get_Fields("MiscChargeAmount" + Svr + "1"));
                }
                // TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
                if (fecherFoundation.FCUtils.IsNull(rsRate.Get_Fields("MiscChargeAmount" + Svr + "2")) == false)
                {
                    // TODO Get_Fields: Field [MiscChargeAmount] not found!! (maybe it is an alias?)
                    miscTotal += Conversion.Val(rsRate.Get_Fields("MiscChargeAmount" + Svr + "2"));
                }
                GetMiscValue = miscTotal;
                return GetMiscValue;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Getting Misc Value", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetMiscValue;
        }

        private double GetAdjustValues(ref string Svr)
        {
            double GetAdjustValues = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // returns the adjustments from the data entry screen
                double Adjust = 0;
                // TODO Get_Fields: Field [DE] not found!! (maybe it is an alias?)
                if (fecherFoundation.FCUtils.IsNull(rsBill.Get_Fields("DE" + Svr + "Adjustment1")) == false)
                {
                    // TODO Get_Fields: Field [DE] not found!! (maybe it is an alias?)
                    Adjust += rsBill.Get_Fields("DE" + Svr + "Adjustment1");
                }
                // TODO Get_Fields: Field [DE] not found!! (maybe it is an alias?)
                if (fecherFoundation.FCUtils.IsNull(rsBill.Get_Fields("DE" + Svr + "Adjustment2")) == false)
                {
                    // TODO Get_Fields: Field [DE] not found!! (maybe it is an alias?)
                    Adjust += rsBill.Get_Fields("DE" + Svr + "Adjustment2");
                }
                // TODO Get_Fields: Field [DE] not found!! (maybe it is an alias?)
                if (fecherFoundation.FCUtils.IsNull(rsBill.Get_Fields("DE" + Svr + "Adjustment3")) == false)
                {
                    // TODO Get_Fields: Field [DE] not found!! (maybe it is an alias?)
                    Adjust += rsBill.Get_Fields("DE" + Svr + "Adjustment3");
                }
                // TODO Get_Fields: Field [DE] not found!! (maybe it is an alias?)
                if (fecherFoundation.FCUtils.IsNull(rsBill.Get_Fields("DE" + Svr + "Adjustment4")) == false)
                {
                    // TODO Get_Fields: Field [DE] not found!! (maybe it is an alias?)
                    Adjust += rsBill.Get_Fields("DE" + Svr + "Adjustment4");
                }
                GetAdjustValues = Adjust;
                return GetAdjustValues;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Getting Adjust Values", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return GetAdjustValues;
        }

        private double GetDEAdjustValues_2(string Svr)
        {
            return GetDEAdjustValues(ref Svr);
        }

        private double GetDEAdjustValues(ref string Svr)
        {
            double GetDEAdjustValues = 0;
            clsDRWrapper rsAdj = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // returns the total of adjustments from the data entry screen
                // for the selected service from the tempBreakdown Records
                GetDEAdjustValues = 0;
                rsAdj.OpenRecordset("SELECT Sum(Amount) as SumAdj FROM TempBreakDown WHERE BillKey = " + rsBill.Get_Fields_Int32("ID") + " AND TYPE = 'D' AND SERVICE = '" + Svr + "'", modExtraModules.strUTDatabase);

                if (rsAdj.RecordCount() != 0)
                {
                    // TODO Get_Fields: Field [SumAdj] not found!! (maybe it is an alias?)
                    GetDEAdjustValues = Conversion.Val(rsAdj.Get_Fields("SumAdj"));
                }

            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Getting Adjust Values", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsAdj.DisposeOf();
            }

            return GetDEAdjustValues;
        }

        private void SetSummaryScreen()
        {
            // this will set all of the totals for the summary screen from the grid
            int l;
            // vbPorter upgrade warning: lngAccount As int	OnWrite(string)
            int lngAccount;
            for (l = 2; l <= vsCalc.Rows - 1; l++)
            {
                if (Information.IsNumeric(vsCalc.TextMatrix(l, lngGridColAccount)))
                {
                    lngAccount = FCConvert.ToInt32(vsCalc.TextMatrix(l, lngGridColAccount));
                }
                if (vsCalc.RowOutlineLevel(l) == 2)
                {
                    if ((Strings.UCase(Strings.Left(vsCalc.TextMatrix(l, lngGridColConsumption), 3)) == "OVE") || (Strings.UCase(Strings.Left(vsCalc.TextMatrix(l, lngGridColConsumption), 3)) == "DE "))
                    {
                        BookWOverride += Conversion.Val(vsCalc.TextMatrix(l, lngGridColWater));
                        BookSOverride += Conversion.Val(vsCalc.TextMatrix(l, lngGridColSewer));
                    }
                    else if (Strings.UCase(Strings.Left(vsCalc.TextMatrix(l, lngGridColConsumption), 3)) == "FLA")
                    {
                        BookWFlat += Conversion.Val(vsCalc.TextMatrix(l, lngGridColWater));
                        BookSFlat += Conversion.Val(vsCalc.TextMatrix(l, lngGridColSewer));
                    }
                    else if (Strings.UCase(Strings.Left(vsCalc.TextMatrix(l, lngGridColConsumption), 3)) == "UNI")
                    {
                        BookWUnits += Conversion.Val(vsCalc.TextMatrix(l, lngGridColWater));
                        BookSUnits += Conversion.Val(vsCalc.TextMatrix(l, lngGridColSewer));
                    }
                    else if (Strings.UCase(Strings.Left(vsCalc.TextMatrix(l, lngGridColConsumption), 3)) == "CON")
                    {
                        BookWCons += Conversion.Val(vsCalc.TextMatrix(l, lngGridColWater));
                        BookSCons += Conversion.Val(vsCalc.TextMatrix(l, lngGridColSewer));
                    }
                    else if (Strings.UCase(Strings.Left(vsCalc.TextMatrix(l, lngGridColConsumption), 3)) == "ADJ")
                    {
                        BookWAdjustment += Conversion.Val(vsCalc.TextMatrix(l, lngGridColWater));
                        BookSAdjustment += Conversion.Val(vsCalc.TextMatrix(l, lngGridColSewer));
                    }
                    else if (Strings.UCase(Strings.Left(vsCalc.TextMatrix(l, lngGridColConsumption), 3)) == "MIS")
                    {
                        BookWMisc += Conversion.Val(vsCalc.TextMatrix(l, lngGridColWater));
                        BookSMisc += Conversion.Val(vsCalc.TextMatrix(l, lngGridColSewer));
                    }
                    else if (Strings.UCase(Strings.Left(vsCalc.TextMatrix(l, lngGridColConsumption), 3)) == "TAX")
                    {
                        BookWTax += Conversion.Val(vsCalc.TextMatrix(l, lngGridColWater));
                        BookSTax += Conversion.Val(vsCalc.TextMatrix(l, lngGridColSewer));
                    }
                }
                if (vsCalc.RowOutlineLevel(l) == 0)
                {
                    // counts the number of each meter type
                    if (vsCalc.TextMatrix(l, lngGridColWater) != "")
                    {
                        BookWCount += 1;
                    }
                    if (vsCalc.TextMatrix(l, lngGridColSewer) != "")
                    {
                        BookSCount += 1;
                    }
                }
            }
            // gets the book's totals
            if (Conversion.Val(vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColWater)) != 0)
            {
                WTotal = FCConvert.ToDouble(vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColWater));
            }
            else
            {
                WTotal = 0;
            }
            if (Conversion.Val(vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColSewer)) != 0)
            {
                STotal = FCConvert.ToDouble(vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColSewer));
            }
            else
            {
                STotal = 0;
            }
            // this will send all the information needed to the summary form
            frmCalculationSummary.InstancePtr.WOverride = BookWOverride;
            frmCalculationSummary.InstancePtr.SOverride = BookSOverride;
            frmCalculationSummary.InstancePtr.WFlat = BookWFlat;
            frmCalculationSummary.InstancePtr.SFlat = BookSFlat;
            frmCalculationSummary.InstancePtr.WUnits = BookWUnits;
            frmCalculationSummary.InstancePtr.SUnits = BookSUnits;
            frmCalculationSummary.InstancePtr.WCons = BookWCons;
            frmCalculationSummary.InstancePtr.SCons = BookSCons;
            frmCalculationSummary.InstancePtr.WAdjust = BookWAdjustment;
            frmCalculationSummary.InstancePtr.SAdjust = BookSAdjustment;
            frmCalculationSummary.InstancePtr.WMisc = BookWMisc;
            frmCalculationSummary.InstancePtr.SMisc = BookSMisc;
            frmCalculationSummary.InstancePtr.WTax = BookWTax;
            frmCalculationSummary.InstancePtr.STax = BookSTax;
            frmCalculationSummary.InstancePtr.WBillCount = BookWCount;
            frmCalculationSummary.InstancePtr.SBillCount = BookSCount;
            frmCalculationSummary.InstancePtr.WSubTotal = WTotal;
            frmCalculationSummary.InstancePtr.SSubTotal = STotal;
            // this resets the variables that pass information to the calculation summary screen
            BookWOverride = 0;
            BookSOverride = 0;
            BookWFlat = 0;
            BookSFlat = 0;
            BookWUnits = 0;
            BookSUnits = 0;
            BookWCons = 0;
            BookSCons = 0;
            BookWAdjustment = 0;
            BookSAdjustment = 0;
            BookWMisc = 0;
            BookSMisc = 0;
            BookWTax = 0;
            BookSTax = 0;
            BookWCount = 0;
            BookSCount = 0;
            WTotal = 0;
            STotal = 0;
        }

        private void vsCalc_MouseMoveEvent(object sender, DataGridViewCellFormattingEventArgs e)
        {
            int nr = 0;
            int nc = 0;
            // get coordinates
            nr = e.RowIndex;
            nc = e.ColumnIndex;
            //FC:FINAL:MSH - added an extra comparing to avoid exceptions if index less than 0
            if (nr > -1 && nc > -1)
            {
                DataGridViewCell cell = vsCalc[nc, nr];
                if (nr < 0 || nc < 0)
                    return;
                if (Statics.c != nc || Statics.R != nr)
                {
                    Statics.R = nr;
                    Statics.c = nc;
                    if (Strings.Left(vsCalc.TextMatrix(Statics.R, Statics.c), lngGridColWater) == "ERROR" || Strings.Left(vsCalc.TextMatrix(Statics.R, Statics.c), lngGridColAccount) == "WARNING" || Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColConsumption), 4) == "NOTE")
                    {
                        if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 1)
                        {
                            cell.ToolTipText = "Rate Table #" + modMain.PadToString_8(FCConvert.ToInt32(Conversion.Val(Strings.Right(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2))), 2) + " Definition Error.  Please check the Rate Table definition on the Table and File Setup screen.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 2)
                        {
                            cell.ToolTipText = "Coded for No Bill.  This meter will not be billed.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 3)
                        {
                            cell.ToolTipText = "Added to another meter.  This will be reflected on the bill.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 4)
                        {
                            cell.ToolTipText = "Final Bill has been issued for this bill.  This meter will not be billed.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 5)
                        {
                            cell.ToolTipText = "No current reading.  Check the account in the Data Entry screen and key in the correct reading.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 6)
                        {
                            cell.ToolTipText = "Current reading is below the previous reading.  If this is not correct, check the data and go back to the Data Entry screen.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 7)
                        {
                            cell.ToolTipText = "No bill record created.  Please go to the data entry screen to update this meter.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 9)
                        {
                            cell.ToolTipText = "This is the primary meter of a combination of meters.  Other consumption totals or bill amounts will be added to this meter's bill.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 10)
                        {
                            cell.ToolTipText = "The consumption for this bill has changed more than the warning percentage level.";
                        }
                        else if (Conversion.Val(Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColSeq), 2)) == 11)
                        {
                            cell.ToolTipText = "Consumption from the changed out meter is included in the calculated consumption.";
                        }
                    }
                    else if (Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColAccount), 4) == "NOTE")
                    {
                        if (Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColName), 41) == "This meter has a combined consumption.")
                        {
                            cell.ToolTipText = "Added to another meter.  This will be reflected on the Bill.";
                        }
                    }
                    else if (Strings.Left(vsCalc.TextMatrix(Statics.R, lngGridColCurrent), 7) == "WARNING")
                    {
                        // Warnings go here
                        cell.ToolTipText = "The consumption for this bill has changed more than the warning percentage level.";
                    }
                    else if (vsCalc.RowOutlineLevel(Statics.R) == 1)
                    {
                        // show the location
                        cell.ToolTipText = vsCalc.TextMatrix(Statics.R, lngGridColLocation);
                    }
                    else
                    {
                        // all other rows have no tool tip
                        cell.ToolTipText = "";
                    }
                }
            }
        }

        private void RateTableError(int RT, int CR)
        {
            // this sub will display an "Undefined RateTable Error"
            RTErrorArray[RT] += ", " + FCConvert.ToString(CR);
        }

        private void AddMeterData(FCGrid Grid, ref int intStartOutlineLevel, int lngNextRow, ref double dblTotalW, ref double dblTotals, ref bool boolMeterErrors)
        {
            clsDRWrapper rsCMeter = new clsDRWrapper();
            clsDRWrapper rsFindBill = new clsDRWrapper();
            clsDRWrapper rsDefaults = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will add meter data to the grid passed in
                bool NoBilled = false;
                bool FinalBilled = false;

                // vbPorter upgrade warning: temp As string	OnWriteFCConvert.ToInt32(
                string temp = "";

                int lngBillKey = 0;
                bool boolConsumptionPercentageChange = false;
                double dblPercentChanged = 0;
                int lngAdj = 0;
                rsAcct = new clsDRWrapper();
                temp = FCConvert.ToString(rsMeter.RecordCount());
                rsFindBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND MeterKey = " + rsMeter.Get_Fields_Int32("ID") + " AND BillStatus <> 'B'", modExtraModules.strUTDatabase);
                lngBillKey = FCConvert.ToInt32(rsFindBill.Get_Fields_Int32("ID"));

                // If .Fields("AccountKey") = 2542 Then
                // Stop
                // End If
                rsAcct.OpenRecordset(modUTStatusPayments.UTMasterQuery(rsMeter.Get_Fields_Int32("AccountKey")), modExtraModules.strUTDatabase);

                // DJW@10132009 Changed false to true to delete all records in breakdown table
                modUTBilling.ClearBreakDownRecords_20(true, lngBillKey, false);

                // this will calculate all of the meters that are combined with this meter
                // and add all of the breakdown records
                ErrorCheck = UpdateBillRecord_18(rsMeter, lngBillKey, true, rsAcct);

                //Application.DoEvents();
                // fills the primary level of the summary for the meter
                AddCurrentRow(lngNextRow, intStartOutlineLevel);

                // Sequence - 1
                // TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
                if (fecherFoundation.FCUtils.IsNull(rsMeter.Get_Fields("Sequence")) == false)
                {
                    // TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
                    Grid.TextMatrix(lngNextRow, lngGridColSeq, rsMeter.Get_Fields("Sequence"));
                }

                string billOwner = rsAcct.Get_Fields_String("OwnerName");
                string billTenant = rsAcct.Get_Fields_String("Name");
                var didBillOwner = rsFindBill.Get_Fields_Boolean(rsFindBill.Get_Fields_String("Service")
                                                                           .ToLower()
                                                                 == "w"
                                                                     ? "WBillOwner"
                                                                     : "SBillOwner");
                var billedName = didBillOwner ? billOwner : billTenant;
                Grid.TextMatrix(lngNextRow, lngGridColName, billedName);

                if (rsAcct.EndOfFile() != true && rsAcct.BeginningOfFile() != true)
                {
                    // Account Number - 7
                    // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                    if (fecherFoundation.FCUtils.IsNull(rsAcct.Get_Fields("AccountNumber")) == false)
                    {
                        // TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
                        Grid.TextMatrix(lngNextRow, lngGridColAccount, rsAcct.Get_Fields("AccountNumber"));
                    }
                }

                // Location - 9
                if (fecherFoundation.FCUtils.IsNull(rsAcct.Get_Fields_String("StreetName")) == false)
                {
                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    if (Strings.Trim(FCConvert.ToString(rsAcct.Get_Fields("StreetNumber"))) != "")
                    {
                        // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                        Grid.TextMatrix(lngNextRow, lngGridColLocation, rsAcct.Get_Fields("StreetNumber") + " " + Strings.Trim(FCConvert.ToString(rsAcct.Get_Fields_String("StreetName"))));
                    }
                    else
                    {
                        Grid.TextMatrix(lngNextRow, lngGridColLocation, Strings.Trim(FCConvert.ToString(rsAcct.Get_Fields_String("StreetName"))));
                    }
                }

                if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("MeterNumber")) == 1)
                {
                    // this will find all of the combined meters for this account
                    rsCMeter.OpenRecordset("SELECT * FROM Metertable WHERE AccountKey = " + rsMeter.Get_Fields_Int32("AccountKey") + " AND BookNumber = " + FCConvert.ToString(CurrentBook) + " AND Combine <> 'N' ORDER BY Sequence Desc");

                    if (rsCMeter.RecordCount() > 0)
                    {
                        // now include all of the meters that are combined
                        while (!rsCMeter.EndOfFile())
                        {
                            AddCombinedMeterData_18(rsCMeter, lngBillKey, intStartOutlineLevel + 1);

                            // adds meter data at current row
                            rsCMeter.MoveNext();
                        }
                    }
                }

                // check to see if this bill is final billed or no billed
                FinalBilled = FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("FinalBilled"));
                NoBilled = rsMeter.Get_Fields_Boolean("NoBill") || rsFindBill.Get_Fields_Boolean("NoBill");

                // kgk 09062012 trout-849  Change to calc consumption on NoBill  ' If Not (FinalBilled Or NoBilled) Then
                if (!FinalBilled)
                {
                    // if bill is not final billed then keep going
                    // Previous Reading - 2
                    Grid.TextMatrix(lngNextRow, lngGridColPrevious, rsMeter.Get_Fields_Int32("PreviousReading"));

                    // *************************FIX THIS**************************
                    // *******Must have a WARNING here rather than an ERROR*******
                    // Current Reading - 3
                    if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading")) != -1)
                    {
                        // kk 110812 trout-884  Change from 0 to -1 for No Read
                        if (rsMeter.Get_Fields_Int32("CurrentReading") < rsMeter.Get_Fields_Int32("PreviousReading"))
                        {
                            lngAdj = 0;
                            rsDefaults.OpenRecordset("SELECT * FROM UtilityBilling", modExtraModules.strUTDatabase);

                            if (!rsDefaults.EndOfFile())
                            {
                                if (rsMeter.Get_Fields_Int32("Multiplier") == 100000)
                                {
                                    lngAdj = 5;
                                }
                                else
                                    if (rsMeter.Get_Fields_Int32("Multiplier") == 10000)
                                {
                                    lngAdj = 4;
                                }
                                else
                                        if (rsMeter.Get_Fields_Int32("Multiplier") == 1000)
                                {
                                    lngAdj = 3;
                                }
                                else
                                            if (rsMeter.Get_Fields_Int32("Multiplier") == 100)
                                {
                                    lngAdj = 2;
                                }
                                else
                                                if (rsMeter.Get_Fields_Int32("Multiplier") == 10)
                                {
                                    lngAdj = 1;
                                }
                                else
                                                    if (rsMeter.Get_Fields_Int32("Multiplier") == 1)
                                {
                                    lngAdj = 0;
                                }
                                else
                                {
                                    lngAdj = 0;
                                }

                                switch (modExtraModules.Statics.glngTownReadingUnits)
                                {
                                    case 100000:
                                        {
                                            lngAdj -= 5;

                                            break;
                                        }

                                    case 10000:
                                        {
                                            lngAdj -= 4;

                                            break;
                                        }

                                    case 1000:
                                        {
                                            lngAdj -= 3;

                                            break;
                                        }

                                    case 100:
                                        {
                                            lngAdj -= 2;

                                            break;
                                        }

                                    case 10:
                                        {
                                            lngAdj -= 1;

                                            break;
                                        }

                                    case 1:
                                        {
                                            lngAdj = lngAdj;

                                            break;
                                        }

                                    default:
                                        {
                                            lngAdj = lngAdj;

                                            break;
                                        }
                                }

                                //end switch
                            }

                            ErrorCheck = 1;

                            // Current Reading Lower than Previous Warning, Calculation can Continue
                            Grid.TextMatrix(lngNextRow, lngGridColCurrent, rsMeter.Get_Fields_Int32("CurrentReading"));
                            Consumption = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading") + ((Math.Pow(10, (rsMeter.Get_Fields_Int32("Digits") - lngAdj))) - rsMeter.Get_Fields_Int32("PreviousReading")));

                            // kk 08292013 trout-984  Indicate change out consumption
                            if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseMeterChangeOut")))
                            {
                                if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter")) != 0)
                                {
                                    Consumption += rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter");
                                }
                            }

                            Grid.TextMatrix(lngNextRow, lngGridColConsumption, Consumption);
                        }
                        else
                        {
                            // Consumption - 4
                            Grid.TextMatrix(lngNextRow, lngGridColCurrent, rsMeter.Get_Fields_Int32("CurrentReading"));
                            Consumption = FCConvert.ToInt32(rsMeter.Get_Fields_Int32("CurrentReading") - rsMeter.Get_Fields_Int32("PreviousReading"));

                            // kk 08292013 trout-984  Indicate change out consumption
                            if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseMeterChangeOut")))
                            {
                                if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter")) != 0)
                                {
                                    Consumption += rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter");
                                }
                            }

                            Grid.TextMatrix(lngNextRow, lngGridColConsumption, Consumption);
                        }
                    }
                    else
                        if (!BilledType_8(1, true) && !BilledType_8(1, false))
                    {
                        // if there is no consumption then do not show an error
                    }
                    else
                    {
                        ErrorCheck = 2;

                        // no current reading
                    }

                    // check consumption percentage change
                    boolConsumptionPercentageChange = CheckPercentageChange_8(rsMeter.Get_Fields_Int32("AccountKey"), rsFindBill.Get_Fields_Int32("ID"), Consumption, ref dblPercentChanged);

                    // this will show all of the breakdown records and create the total line
                    lngNextRow = ShowBreakdownRecords_24(lngBillKey, rsMeter.Get_Fields_Int32("ID"), intStartOutlineLevel + 1, ref dblTotalW, ref dblTotals);
                    lngNextRow += 1;

                    // fills the secondary level of the summary
                    // If rsAcct.Fields("AccountNumber") = 427 Then
                    // frmWait.Top = -2000
                    // MsgBox "Stop"
                    // End If
                    // kk 08292013 trout-984  Indicate change out consumption
                    if (FCConvert.ToBoolean(rsMeter.Get_Fields_Boolean("UseMeterChangeOut")))
                    {
                        if (FCConvert.ToInt32(rsMeter.Get_Fields_Int32("ConsumptionOfChangedOutMeter")) != 0)
                        {
                            AddCurrentRow_6(lngNextRow, intStartOutlineLevel + 1);

                            // kk 12112013  Grid.rows, intStartOutlineLevel + 1
                            Grid.TextMatrix(lngNextRow, lngGridColConsumption, "NOTE");
                            AddWarningString_24(lngBillKey, "N", "Change out consumption has been added.");
                            Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                            Grid.TextMatrix(lngNextRow, lngGridColAccount, "Change out consumption has been added.");
                            Grid.TextMatrix(lngNextRow, lngGridColSeq, "11");
                            Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                            Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColSeq, Color.White);
                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColPrevious, Color.White);
                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, Color.White);
                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, Color.Blue);
                            Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, true);
                        }
                    }

                    if (NoBilled)
                    {
                        AddCurrentRow_6(Grid.Rows, intStartOutlineLevel + 1);
                        lngNextRow = Grid.Rows - 1;
                        Grid.TextMatrix(lngNextRow, lngGridColConsumption, "NOTE");
                        AddWarningString_24(lngBillKey, "N", "Meter has been coded for No Bill.");
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                        Grid.TextMatrix(lngNextRow, lngGridColAccount, "Meter has been coded for No Bill.");

                        //FC:TEMP:DSE:#1040 Merge next 2 cells so that the text should be completely visible
                        Grid.MergeRow(lngNextRow, true, lngGridColAccount, lngGridColAccount + 1);
                        Grid.TextMatrix(lngNextRow, lngGridColSeq, "2");
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                        Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColSeq, Color.White);
                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColPrevious, Color.White);
                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, Color.White);
                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, Color.Blue);
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, true);
                        lngNextRow += 1;
                    }

                    // kk 110912 trout-884  The error should have priority over warnings in Reports
                    if (ErrorCheck == 2)
                    {
                        if (!NoBilled)
                        {
                            // Let's not show this error if it's NoBill
                            AddWarningString_24(lngBillKey, "E", "No current reading.");
                        }
                    }

                    if (!boolHideVarianceWarning)
                    {
                        // HideMinimumVarianceWarning
                        boolCurMeterMinimum = false;
                    }

                    if (boolConsumptionPercentageChange && !boolCurMeterMinimum)
                    {
                        boolMeterErrors = true;

                        // add a warning
                        if (lngNextRow > 2)
                        {
                            AddCurrentRow_6(lngNextRow, intStartOutlineLevel + 1);
                            Grid.TextMatrix(lngNextRow, lngGridColCurrent, "WARNING");

                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, 0x2DAFFF);
                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, Color.White);
                            Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, true);

                            if (dblPercentChanged >= 0)
                            {
                                Grid.TextMatrix(lngNextRow, lngGridColConsumption, "Consumption UP " + Strings.Format(dblPercentChanged, "#0") + "%");
                                AddWarningString_24(lngBillKey, "W", "Consumption UP " + Strings.Format(dblPercentChanged, "#0") + "%");
                            }
                            else
                            {
                                Grid.TextMatrix(lngNextRow, lngGridColConsumption, "Consumption DOWN " + Strings.Format(Math.Abs(dblPercentChanged), "#0") + "%");
                                AddWarningString_24(lngBillKey, "W", "Consumption DOWN " + Strings.Format(Math.Abs(dblPercentChanged), "#0") + "%");
                            }

                            //FC:TEMP:DSE Merge next 2 cells so that the text should be completely visible
                            Grid.MergeRow(lngNextRow, true, lngGridColConsumption, lngGridColConsumption + 1);
                            Grid.TextMatrix(lngNextRow, lngGridColSeq, "10");
                            Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                            Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColSeq, Color.White);
                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColPrevious, Color.White);
                        }
                        else
                        {
                            // msgbox "bad row
                        }
                    }

                    if (ErrorCheck != 0)
                    {
                        boolMeterErrors = true;

                        // this set of errors will create thier own row to be shown on
                        // lngNextRow = lngNextRow + 1
                        // kk07292015 trout-1135  Fix funky spacing
                        // AddCurrentRow Grid.rows, intStartOutlineLevel + 1
                        // lngNextRow = Grid.rows - 1
                        switch (ErrorCheck)
                        {
                            // this error number will be stored in column 1 with the fore and the backcolor the same so that the error numbers
                            // are not shown to the users
                            case 1:
                                {
                                    AddCurrentRow_6(Grid.Rows, intStartOutlineLevel + 1);

                                    // kk07292015 trout-1135  Fix funky spacing
                                    lngNextRow = Grid.Rows - 1;
                                    Grid.TextMatrix(lngNextRow, lngGridColCurrent, "WARNING");
                                    AddWarningString_24(lngBillKey, "W", "Current reading cannot be lower than previous reading unless meter has rolled over.");

                                    //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, 0x2DAFFF);
                                    //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, Color.White);
                                    Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, true);
                                    Grid.TextMatrix(lngNextRow, lngGridColConsumption, "Current reading cannot be lower than previous reading unless meter has rolled over.");
                                    Grid.TextMatrix(lngNextRow, lngGridColSeq, "6");
                                    Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                                    Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColSeq, Color.White);
                                    //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColPrevious, Color.White);
                                    lngNextRow += 1;

                                    // kk07292015 trout-1135  Fix funky spacing
                                    break;
                                }

                            case 2:
                                {
                                    if (!NoBilled)
                                    {
                                        // Let's not show this error if it's NoBill
                                        AddCurrentRow_6(Grid.Rows, intStartOutlineLevel + 1);

                                        // kk07292015 trout-1135  Fix funky spacing
                                        lngNextRow = Grid.Rows - 1;
                                        Grid.TextMatrix(lngNextRow, lngGridColCurrent, "ERROR");
                                        AddWarningString_24(lngBillKey, "E", "No current reading.");

                                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, Color.Red);
                                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, Color.White);
                                        Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, true);
                                        Grid.TextMatrix(lngNextRow, lngGridColConsumption, "No current reading.");

                                        //FC:FINAL:MSH - issue #996: Merge next 2 cells for fully text displaying
                                        Grid.MergeRow(lngNextRow, true, lngGridColConsumption, lngGridColConsumption + 1);
                                        Grid.TextMatrix(lngNextRow, lngGridColSeq, "5");
                                        Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                                        Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColSeq, Color.White);
                                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColPrevious, Color.White);
                                        lngNextRow += 1;

                                        // kk07292015 trout-1135  Fix funky spacing
                                    }

                                    break;
                                }

                            case 3:
                                {
                                    if (!NoBilled)
                                    {
                                        // Let's not show this error if it's NoBill
                                        AddCurrentRow_6(Grid.Rows, intStartOutlineLevel + 1);

                                        // kk07292015 trout-1135  Fix funky spacing
                                        lngNextRow = Grid.Rows - 1;
                                        Grid.TextMatrix(lngNextRow, lngGridColCurrent, "WARNING");
                                        AddWarningString_24(lngBillKey, "W", "No bill created for this meter.");

                                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, 0x2DAFFF);
                                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, Color.White);
                                        Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngNextRow, lngGridColCurrent, lngNextRow, lngGridColCurrent, true);
                                        Grid.TextMatrix(lngNextRow, lngGridColConsumption, "No bill created for this meter.");
                                        Grid.TextMatrix(lngNextRow, lngGridColSeq, "7");
                                        Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                                        Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColSeq, Color.White);
                                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColPrevious, Color.White);
                                        lngNextRow += 1;

                                        // kk07292015 trout-1135  Fix funky spacing
                                    }

                                    break;
                                }
                        }

                        //end switch
                        ErrorCheck = 0;

                        // kk07292015 trout-1135  Fix funky spacing
                        // lngNextRow = lngNextRow + 1
                        // AddCurrentRow lngNextRow, intStartOutlineLevel + 1
                    }
                }
                else
                {
                    if (FinalBilled)
                    {
                        lngNextRow += 1;
                        AddCurrentRow_6(lngNextRow, intStartOutlineLevel + 1);
                        Grid.TextMatrix(lngNextRow, lngGridColConsumption, "NOTE");
                        AddWarningString_24(lngBillKey, "N", "Meter has been Final Billed.");
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                        Grid.TextMatrix(lngNextRow, lngGridColAccount, "Meter has been Final Billed.");
                        Grid.TextMatrix(lngNextRow, lngGridColSeq, "4");
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                        Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColSeq, Color.White);
                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColPrevious, Color.White);
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, Color.White);
                        //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, Color.Blue);
                        Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, true);
                        lngNextRow += 1;
                    }
                    else
                    {
                        if (NoBilled)
                        {
                            lngNextRow += 1;
                            AddCurrentRow_6(lngNextRow, intStartOutlineLevel + 1);
                            Grid.TextMatrix(lngNextRow, lngGridColConsumption, "NOTE");
                            AddWarningString_24(lngBillKey, "N", "Meter has been coded for No Bill.");
                            Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                            Grid.TextMatrix(lngNextRow, lngGridColAccount, "Meter has been coded for No Bill.");

                            //FC:TEMP:DSE:#1040 Merge next 2 cells so that the text should be completely visible
                            Grid.MergeRow(lngNextRow, true, lngGridColAccount, lngGridColAccount + 1);
                            Grid.TextMatrix(lngNextRow, lngGridColSeq, "2");
                            Grid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngNextRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                            Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColSeq, Color.White);
                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColSeq, lngNextRow, lngGridColPrevious, Color.White);
                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, Color.White);
                            //Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, Color.Blue);
                            Grid.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngNextRow, lngGridColConsumption, lngNextRow, lngGridColConsumption, true);
                            lngNextRow += 1;
                        }
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                
                frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Adding Meter Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsDefaults.DisposeOf();
                rsCMeter.DisposeOf();
                rsFindBill.DisposeOf();
            }
        }

        private void AddCombinedMeterData_18(clsDRWrapper rsM, int lngBK, int intLvl)
        {
            AddCombinedMeterData(ref rsM, ref lngBK, ref intLvl);
        }

        private void AddCombinedMeterData(ref clsDRWrapper rsM, ref int lngBK, ref int intLvl)
        {
            clsDRWrapper rsMast = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will add meter data to the grid
                bool NoBilled = false;

                // (Y)es or (N)o
                bool FinalBilled = false;

                // (Y)es or (N)o
                string Combined = "";

                // Combine by (B)ill or (C)onsumption
                int lngRow = 0;
                int lngBookRow;

                if (fecherFoundation.FCUtils.IsNull(rsM.Get_Fields_String("Combine")) == false)
                {
                    Combined = rsM.Get_Fields_String("Combine");
                }

                int lngAcctKey = rsM.Get_Fields_Int32("AccountKey");
                rsMast.OpenRecordset(modUTStatusPayments.UTMasterQuery(lngAcctKey), modExtraModules.strUTDatabase);

                // fills the primary level of the summary
                ErrorCheck = UpdateBillRecord_18(rsM, lngBK, FCConvert.CBool(rsM.Get_Fields_String("Combine") == "B"), rsMast);

                // finds and returns the row where the meter should be inserted
                lngRow = FindCurrentRow(rsM.Get_Fields_Int32("AccountKey"));

                if (lngRow == -1)
                {
                    // -1 is returned if there is an error finding the Primary Meter
                    lngRow = vsCalc.Rows;

                    // just add it to the end of the summary
                }

                AddCurrentRow(lngRow, intLvl);

                //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, 1, lngRow, vsCalc.Cols - 1, 0x80000018);
                // adds information
                // Sequence - 1
                // TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
                vsCalc.TextMatrix(lngRow, lngGridColSeq, rsM.Get_Fields("Sequence"));
                rsAcct.OpenRecordset("SELECT m.*,pBill.FullNameLF AS Name FROM Master m INNER JOIN " + modMain.Statics.strDbCP + "PartyAndAddressView pBill ON pBill.PartyID = m.BillingPartyID WHERE m.ID = " + rsM.Get_Fields_Int32("AccountKey"));

                if (!rsAcct.EndOfFile())
                {
                    // BillTo Name - 8
                    if (rsAcct.Get_Fields_Boolean("samebillowner"))
                    {
                        vsCalc.TextMatrix(lngRow, lngGridColName, FCConvert.ToString(rsAcct.Get_Fields_String("DeedName1")));
                    }
                    else
                    {
                        vsCalc.TextMatrix(lngRow, lngGridColName, FCConvert.ToString(rsAcct.Get_Fields_String("Name")));
                    }

                    // Account Number - 7
                    vsCalc.TextMatrix(lngRow, lngGridColAccount, FCConvert.ToString(rsAcct.Get_Fields("AccountNumber")));
                    vsCalc.TextMatrix(lngRow, lngGridColAccountKey, FCConvert.ToString(rsAcct.Get_Fields_Int32("ID")));
                }

                // Location - 9
                if (fecherFoundation.FCUtils.IsNull(rsAcct.Get_Fields_String("StreetName")) == false)
                {
                    // TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
                    vsCalc.TextMatrix(lngRow, lngGridColLocation, rsAcct.Get_Fields("StreetNumber") + " " + rsAcct.Get_Fields_String("StreetName"));
                }

                // align the cells to the left
                vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColName, lngRow, lngGridColLocation, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                // check to see if this bill is final billed or no billed
                FinalBilled = rsM.Get_Fields_Boolean("FinalBilled");
                NoBilled = rsM.Get_Fields_Boolean("NoBill");

                if (!FinalBilled && !NoBilled)
                {
                    // if bill is not final billed or nobilled then keep going
                    // Previous Reading - 2
                    if (fecherFoundation.FCUtils.IsNull(rsM.Get_Fields_Int32("PreviousReading")) == false)
                    {
                        vsCalc.TextMatrix(lngRow, lngGridColPrevious, rsM.Get_Fields_Int32("PreviousReading"));
                    }
                    else
                    {
                        vsCalc.TextMatrix(lngRow, lngGridColPrevious, FCConvert.ToString(0));
                    }

                    // Current Reading - 3
                    if (fecherFoundation.FCUtils.IsNull(rsM.Get_Fields_Int32("CurrentReading")) == false && rsM.Get_Fields_Int32("CurrentReading") != -1)
                    {
                        // kk 110812 trout-884  Add check for -1 (No Read)
                        if (rsM.Get_Fields_Int32("CurrentReading") < rsM.Get_Fields_Int32("PreviousReading"))
                        {
                            ErrorCheck = 1;

                            // Current Reading Lower than Previous Warning, Calculation can Continue
                            vsCalc.TextMatrix(lngRow, lngGridColCurrent, rsM.Get_Fields_Int32("CurrentReading"));
                            Consumption = FCConvert.ToInt32(rsM.Get_Fields_Int32("CurrentReading") + ((Math.Pow(10, rsM.Get_Fields_Int32("Digits"))) - rsM.Get_Fields_Int32("PreviousReading")));
                            vsCalc.TextMatrix(lngRow, lngGridColConsumption, FCConvert.ToString(Consumption));
                        }
                        else
                        {
                            // Consumption - 4
                            vsCalc.TextMatrix(lngRow, lngGridColCurrent, rsM.Get_Fields_Int32("CurrentReading"));
                            Consumption = rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading");

                            if (Combined == "B")
                            {
                                // show this so the user can see how much was charged on
                                vsCalc.TextMatrix(lngRow, lngGridColConsumption, FCConvert.ToString(Consumption));
                            }
                            else
                            {
                                vsCalc.TextMatrix(lngRow, lngGridColConsumption, "Combined");
                            }
                        }
                    }
                    else
                    {
                        ErrorCheck = 2;

                        // no current reading
                    }

                    if (Combined == "B")
                    {
                        // Regular Water - 5
                        // Regular Sewer - 6
                        // lngRow = GetRegularValues(rsBill) 'will calculate and display regular water/sewer values
                        // also will create subtotal rows
                        // then will return the current currow value to start the next row
                    }

                    // fills the secondary level of the summary
                    // two different messages depending on the type of combination
                    if (Combined == "B")
                    {
                        vsCalc.TextMatrix(lngRow, lngGridColName, "This meter has a combined bill.");

                        //FC:TEMP:DSE:#1040 Merge next 2 cells so that the text should be completely visible
                        vsCalc.MergeRow(lngRow, true, lngGridColName, lngGridColName + 1);

                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColName, FCGrid.AlignmentSettings.flexAlignLeftCenter);
                        vsCalc.TextMatrix(lngRow, lngGridColAccount, "NOTE");

                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColAccount, lngRow, lngGridColAccount, Color.Blue);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColAccount, lngRow, lngGridColAccount, Color.White);
                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngRow, lngGridColAccount, true);
                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                        vsCalc.TextMatrix(lngRow, lngGridColLocation, "");
                    }
                    else
                    {
                        vsCalc.TextMatrix(lngRow, lngGridColName, "This meter has a combined consumption.");

                        //FC:TEMP:DSE:#1040 Merge next 2 cells so that the text should be completely visible
                        vsCalc.MergeRow(lngRow, true, lngGridColName, lngGridColName + 1);
                        vsCalc.TextMatrix(lngRow, lngGridColLocation, "");

                        // replaces owner info to remind the user that this meter is combined with another meter
                        vsCalc.TextMatrix(lngRow, lngGridColAccount, "NOTE");

                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColAccount, lngRow, lngGridColAccount, Color.Blue);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColAccount, lngRow, lngGridColAccount, Color.White);
                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngRow, lngGridColAccount, true);

                        // kk trouts-11 06282013  Combine Calc & Edit and Billing Edit
                        rsMeter.Edit();

                        // updates the meter status
                        // x                If boolCalculate Then
                        // x                    rsMeter.Fields("BillingStatus") = "X"
                        // x                Else
                        rsMeter.Set_Fields("BillingStatus", "E");

                        // x                End If
                        rsMeter.Update();
                        rsBill.Edit();

                        // updates the bill status
                        // x                If boolCalculate Then
                        // x                    rsBill.Fields("BillStatus") = "X"
                        // x                Else
                        rsBill.Set_Fields("BillStatus", "E");

                        // x                End If
                        rsBill.Update();
                    }

                    if (ErrorCheck != 0)
                    {
                        // this set of errors will create thier own row to be shown on
                        lngRow += 1;
                        AddCurrentRow_6(lngRow, 1);

                        // add a row for the error message
                        vsCalc.TextMatrix(lngRow, lngGridColConsumption, "ERROR");

                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColConsumption, lngRow, lngGridColConsumption, Color.Red);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColConsumption, lngRow, lngGridColConsumption, Color.White);
                        switch (ErrorCheck)
                        {
                            // this error number will be stored in column 1 with the fore and the backcolor the same so that the error numbers
                            // are not shown to the users
                            // column 1 is the error code
                            // column 5-6 is where the "ERROR" text will be stored
                            // column 7 is the user message
                            case 1:
                                {
                                    vsCalc.TextMatrix(lngRow, lngGridColAccount, "Current reading cannot be lower than previous reading unless the meter has rolled over.");
                                    vsCalc.TextMatrix(lngRow, lngGridColSeq, "6");
                                    vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColSeq, lngRow, lngGridColSeq, 0x80000016);
                                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColSeq, lngRow, lngGridColPrevious, 0x80000016);
                                    break;
                                }

                            case 2:
                                {
                                    vsCalc.TextMatrix(lngRow, lngGridColAccount, "No current reading.");
                                    vsCalc.TextMatrix(lngRow, lngGridColSeq, "5");
                                    vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColSeq, lngRow, lngGridColSeq, 0x80000016);
                                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColSeq, lngRow, lngGridColPrevious, 0x80000016);
                                    break;
                                }

                            case 3:
                                {
                                    vsCalc.TextMatrix(lngRow, lngGridColAccount, "No bill created for this meter.");
                                    vsCalc.TextMatrix(lngRow, lngGridColSeq, "7");
                                    vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColSeq, lngRow, lngGridColSeq, 0x80000016);
                                    //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColSeq, lngRow, lngGridColPrevious, 0x80000016);
                                    break;
                                }
                        }

                        //end switch
                        ErrorCheck = 0;
                        AddCurrentRow_6(lngRow, 1);
                        lngRow += 1;
                    }
                }
                else
                {
                    if (FinalBilled)
                    {
                        lngRow += 1;
                        AddCurrentRow_6(lngRow, 1);
                        vsCalc.TextMatrix(lngRow, lngGridColConsumption, "NOTE");
                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignCenterCenter);
                        vsCalc.TextMatrix(lngRow, lngGridColAccount, "Meter has been Final Billed.");
                        vsCalc.TextMatrix(lngRow, lngGridColSeq, "4");
                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColSeq, lngRow, lngGridColSeq, 0x80000016);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColSeq, lngRow, 2, 0x80000016);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColConsumption, lngRow, lngGridColConsumption, Color.White);
                        //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColConsumption, lngRow, lngGridColConsumption, Color.Blue);
                        vsCalc.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngRow, lngGridColConsumption, lngRow, lngGridColConsumption, true);
                        lngRow += 1;
                    }
                    else
                    {
                        if (NoBilled)
                        {
                            lngRow += 1;
                            AddCurrentRow_6(lngRow, 1);

                            // adds a row that this bill has been coded for nobill
                            vsCalc.TextMatrix(lngRow, lngGridColConsumption, "NOTE");

                            // "NOTE" text in column 4
                            vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColConsumption, FCGrid.AlignmentSettings.flexAlignCenterCenter);

                            // align the "NOTE" text
                            vsCalc.TextMatrix(lngRow, lngGridColAccount, "Meter has been coded for No Bill.");

                            // user message
                            vsCalc.TextMatrix(lngRow, lngGridColSeq, "2");

                            // tool tip text code
                            vsCalc.Cell(FCGrid.CellPropertySettings.flexcpAlignment, lngRow, lngGridColAccount, FCGrid.AlignmentSettings.flexAlignLeftCenter);

                            // align the message
                            //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColSeq, lngRow, lngGridColSeq, 0x80000016);
                            // sets the fore and back color to the same grey
                            //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColSeq, lngRow, lngGridColPrevious, 0x80000016);
                            // so that the user cannot see the tool tip text code
                            //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpForeColor, lngRow, lngGridColConsumption, lngRow, lngGridColConsumption, Color.White);
                            // forecolor of "NOTE"
                            //vsCalc.Cell(FCGrid.CellPropertySettings.flexcpBackColor, lngRow, lngGridColConsumption, lngRow, lngGridColConsumption, Color.Blue);
                            // backcolor of "NOTE"
                            vsCalc.Cell(FCGrid.CellPropertySettings.flexcpFontBold, lngRow, lngGridColConsumption, lngRow, lngGridColConsumption, true);

                            // "NOTE" to bold
                            lngRow += 1;

                            // next row
                        }
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Adding Combined Meter Data", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsMast.DisposeOf();
            }
        }

        private int FindCurrentRow(int AC)
        {
            int FindCurrentRow = 0;
            clsDRWrapper rsTemp = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will return the row to start adding meter information into for the
                // account number that was passed in
                // the function is looking for the Primary meter for the account MeterNumber = 1 so that
                // all meters that are combined will be side by side
                int l = 0;
                // find the primary meter for the account number that is passed in
                rsTemp.OpenRecordset("SELECT * FROM MeterTable WHERE MeterNumber = 1 AND AccountKey = " + FCConvert.ToString(AC), modExtraModules.strUTDatabase);
                if (!rsTemp.EndOfFile())
                {
                    // if there is one, find the row number that it resides on in the grid
                    // find the book line so the correct seq is found
                    for (l = 2; l <= vsCalc.Rows - 1; l++)
                    {
                        if (vsCalc.RowOutlineLevel(l) == 0 && Conversion.Val(vsCalc.TextMatrix(l, lngGridColBookNumber)) == rsTemp.Get_Fields_Int32("BookNumber"))
                        {
                            // And .TextMatrix(l, lngGridColAccountKey) = rsTemp.Fields("AccountKey") Then
                            break;
                        }
                    }
                    // start at the book row and find the correct seq number
                    for (l = l; l <= vsCalc.Rows - 1; l++)
                    {
                        // TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
                        if (vsCalc.RowOutlineLevel(l) == 1 && Conversion.Val(vsCalc.TextMatrix(l, lngGridColSeq)) == rsTemp.Get_Fields("Sequence"))
                        {
                            // And .TextMatrix(l, lngGridColAccountKey) = rsTemp.Fields("AccountKey") Then
                            l += 1;
                            goto FOUNDROW;
                        }
                    }
                    l = -1;
                FOUNDROW:
                    ;
                }
                // return the information
                FindCurrentRow = l;
                return FindCurrentRow;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Finding Current Row", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally { rsTemp.DisposeOf(); }
            return FindCurrentRow;
        }

        private void CompleteBillRecord()
        {
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this sub will put the sewer and/or water totals in the bill record
                // it will ignore the other fields within the bill record (in the case of combining meters)
                // and store the final bill totals
                int i;
                for (i = 2; i <= vsCalc.Rows - 1; i++)
                {
                    // check all rows and update each bill as the primary line is encountered
                    if (vsCalc.RowOutlineLevel(i) == 0 && Conversion.Val(vsCalc.TextMatrix(i, lngGridColSeq)) != 0)
                    {
                        CompleteMeter_78(CurrentBook, FCConvert.ToInt32(Conversion.Val(vsCalc.TextMatrix(i, lngGridColSeq))), FCConvert.ToDouble(vsCalc.TextMatrix(i, lngGridColWater)), FCConvert.ToDouble(vsCalc.TextMatrix(i, lngGridColSewer)), i);
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Complting Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }
        // vbPorter upgrade warning: Seq As int	OnWriteFCConvert.ToDouble(
        private int CompleteMeter_78(int BN, int Seq, double WaterTot, double SewerTot, int j)
        {
            return CompleteMeter(ref BN, ref Seq, ref WaterTot, ref SewerTot, ref j);
        }

        private int CompleteMeter(ref int BN, ref int Seq, ref double WaterTot, ref double SewerTot, ref int j)
        {
            int CompleteMeter = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this sub actually changed each bill when necessary
                strSQL = "SELECT * FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE MeterTable.BookNumber = " + FCConvert.ToString(BN) + " AND MeterTable.Sequence = " + FCConvert.ToString(Seq) + " AND Bill.BillStatus <> 'B'";
                rsBill.OpenRecordset(strSQL);
                if (rsBill.RecordCount() > 0)
                {
                    rsBill.Edit();
                    rsBill.Set_Fields("Note", "");
                    rsBill.Set_Fields("TotalWBillAmount", WaterTot);
                    rsBill.Set_Fields("TotalSBillAmount", SewerTot);
                    // next row is Overrides
                    if (vsCalc.RowOutlineLevel(j + 1) > 0 && (vsCalc.TextMatrix(j, lngGridColWater) != "NOTE"))
                    {
                        rsBill.Set_Fields("WaterOverrideAmount", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 1, lngGridColWater))));
                        rsBill.Set_Fields("SewerOverrideAmount", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 1, lngGridColSewer))));
                        if (rsBill.Get_Fields_Decimal("WaterOverrideAmount") == 0)
                        {
                            if (vsCalc.TextMatrix(j + 2, lngGridColWater) == "ERROR" || vsCalc.TextMatrix(j + 2, lngGridColSewer) == "ERROR" || vsCalc.TextMatrix(j + 2, lngGridColAccount) == "ERROR")
                            {
                                rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + " ERROR - " + vsCalc.TextMatrix(j + 2, lngGridColAccount));
                            }
                            else
                            {
                                rsBill.Set_Fields("WFlatAmount", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 2, lngGridColWater))));
                            }
                            if (vsCalc.TextMatrix(j + 3, lngGridColWater) == "ERROR" || vsCalc.TextMatrix(j + 3, lngGridColSewer) == "ERROR" || vsCalc.TextMatrix(j + 3, lngGridColAccount) == "ERROR")
                            {
                                rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + " ERROR - " + vsCalc.TextMatrix(j + 3, lngGridColAccount));
                            }
                            else
                            {
                                rsBill.Set_Fields("WUnitsAmount", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 3, lngGridColWater))));
                            }
                            if (vsCalc.TextMatrix(j + 4, lngGridColWater) == "ERROR" || vsCalc.TextMatrix(j + 4, lngGridColSewer) == "ERROR" || vsCalc.TextMatrix(j + 4, lngGridColAccount) == "ERROR")
                            {
                                rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + " ERROR - " + vsCalc.TextMatrix(j + 4, lngGridColAccount));
                            }
                            else
                            {
                                rsBill.Set_Fields("WConsumptionAmount", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 4, lngGridColWater))));
                            }
                        }
                        else
                        {
                            rsBill.Set_Fields("WFlatAmount", 0);
                            rsBill.Set_Fields("WUnitsAmount", 0);
                            rsBill.Set_Fields("WConsumptionAmount", 0);
                        }
                        if (rsBill.Get_Fields_Decimal("SewerOverrideAmount") == 0)
                        {
                            if (vsCalc.TextMatrix(j + 2, lngGridColSewer) == "ERROR")
                            {
                                rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + " ERROR - " + vsCalc.TextMatrix(j + 2, lngGridColAccount));
                            }
                            else
                            {
                                rsBill.Set_Fields("SFlatAmount", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 2, lngGridColSewer))));
                            }
                            if (vsCalc.TextMatrix(j + 3, lngGridColSewer) == "ERROR")
                            {
                                rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + " ERROR - " + vsCalc.TextMatrix(j + 3, lngGridColAccount));
                            }
                            else
                            {
                                rsBill.Set_Fields("SUnitsAmount", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 3, lngGridColSewer))));
                            }
                            if (vsCalc.TextMatrix(j + 4, lngGridColSewer) == "ERROR")
                            {
                                rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + " ERROR - " + vsCalc.TextMatrix(j + 4, lngGridColAccount));
                            }
                            else
                            {
                                rsBill.Set_Fields("SConsumptionAmount", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 4, lngGridColSewer))));
                            }
                        }
                        else
                        {
                            rsBill.Set_Fields("SFlatAmount", 0);
                            rsBill.Set_Fields("SUnitsAmount", 0);
                            rsBill.Set_Fields("SConsumptionAmount", 0);
                        }
                        rsBill.Set_Fields("WTax", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 7, lngGridColWater))));
                        rsBill.Set_Fields("STax", FCConvert.ToString(Conversion.Val(vsCalc.TextMatrix(j + 7, lngGridColSewer))));
                        if (vsCalc.TextMatrix(j + 9, lngGridColConsumption) == "NOTE" && vsCalc.RowOutlineLevel(j + 9) != 0)
                        {
                            rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + "  " + vsCalc.TextMatrix(j + 9, lngGridColName));
                        }
                        else if (vsCalc.TextMatrix(j + 9, lngGridColCurrent) == "WARNING" && vsCalc.RowOutlineLevel(j + 9) != 0)
                        {
                            rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + "  WARNING - " + Strings.Trim(vsCalc.TextMatrix(j + 9, lngGridColConsumption)));
                        }
                    }
                    else
                    {
                        rsBill.Set_Fields("WaterOverrideAmount", 0);
                        rsBill.Set_Fields("SewerOverrideAmount", 0);
                        rsBill.Set_Fields("WUnitsAmount", 0);
                        rsBill.Set_Fields("SUnitsAmount", 0);
                        rsBill.Set_Fields("WConsumptionAmount", 0);
                        rsBill.Set_Fields("SConsumptionAmount", 0);
                        rsBill.Set_Fields("WTax", 0);
                        rsBill.Set_Fields("STax", 0);
                        if (vsCalc.TextMatrix(j, lngGridColAccount) == "NOTE")
                        {
                            rsBill.Set_Fields("Note", rsBill.Get_Fields_String("Note") + "  " + vsCalc.TextMatrix(j, lngGridColName));
                        }
                    }
                    rsBill.Update();
                }
                return CompleteMeter;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Completing Meter", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return CompleteMeter;
        }

        private void SetStatus_6(int BookNum, string NewStatus)
        {
            SetStatus(ref BookNum, ref NewStatus);
        }

        private void SetStatus(ref int BookNum, ref string NewStatus)
        {
            clsDRWrapper rsStatus = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this sub will change the status of the book, meter and current bill of the book designated in BookNum
                DateTime dtDate;
                rsStatus.DefaultDB = modExtraModules.strUTDatabase;
                dtDate = DateTime.Now;
                if (!boolFinal)
                {
                    // set the book status
                    strSQL = "UPDATE Book SET CurrentStatus = '" + NewStatus + "', " + Strings.Left(NewStatus, 1) + "Date = '" + FCConvert.ToString(dtDate) + "', DateUpdated = '" + FCConvert.ToString(dtDate) + "' WHERE BookNumber = " + FCConvert.ToString(BookNum);
                    rsStatus.Execute(strSQL, modExtraModules.strUTDatabase);
                    // set the meter status
                    strSQL = "SELECT * FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(BookNum);
                    rsStatus.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
                    while (!rsStatus.EndOfFile())
                    {
                        rsStatus.Edit();
                        rsStatus.Set_Fields("BillingStatus", Strings.Left(NewStatus, 1));
                        rsStatus.Update();
                        rsStatus.MoveNext();
                    }
                }
                // set the bill status
                if (boolFinal)
                {
                    // strSQL = "SELECT * FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE MeterTable.BookNumber = " & BookNum & " AND Bill.BillStatus <> 'B' AND ISNULL(Bill.Final,0) = 1"
                    strSQL = "UPDATE Bill SET Bill.BillStatus = '" + Strings.Left(NewStatus, 1) + "' FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE MeterTable.BookNumber = " + FCConvert.ToString(BookNum) + " AND Bill.BillStatus <> 'B' AND ISNULL(Bill.Final,0) = 1";
                }
                else
                {
                    // strSQL = "SELECT * FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE MeterTable.BookNumber = " & BookNum & " AND Bill.BillStatus <> 'B'"
                    strSQL = "UPDATE Bill SET Bill.BillStatus = '" + Strings.Left(NewStatus, 1) + "' FROM Bill INNER JOIN MeterTable ON Bill.MeterKey = MeterTable.ID WHERE MeterTable.BookNumber = " + FCConvert.ToString(BookNum) + " AND Bill.BillStatus <> 'B'";
                }
                rsStatus.Execute(strSQL, modExtraModules.strUTDatabase);
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Setting Status", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally {rsStatus.DisposeOf();}
        }

        private bool StartBookCalculation()
        {
            bool StartBookCalculation = false;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this validation event just calls the function to get the book info and start the calculation
                // only valid books are shown in the combo box
                // valid book numbers are from 0001 to 9999
                int lngBNum = 0;
                int intCT;
                //frmWait.InstancePtr.Init("Loading Data." + "\r\n" + "  Please Wait...");
                this.UpdateWait("Loading Data." + "\r\n" + "  Please Wait...");
                FillGrid = false;
                vsCalc.Visible = false;
                vsCalc.Rows = 2;
                mnuSummary.Enabled = false;
                cmdSummary.Enabled = false;
                if (boolFinal)
                {
                    if (modGlobalFunctions.IsBounded(BookArray))
                    {
                        intCount = Information.UBound(BookArray, 1);
                    }
                    else
                    {
                        intCount = 0;
                    }
                }
                // go through the book array and show each book
                for (intCT = 1; intCT <= intCount; intCT++)
                {
                    //Application.DoEvents();
                    // get the book number from the array
                    lngBNum = BookArray[intCT];
                    if (lngBNum < 10000 && lngBNum > 0)
                    {
                        // make sure that this is a valid book number
                        // kk trouts-11 06282013  Combine Calc & Edit and Billing Edit steps
                        // x            If boolCalculate Then
                        // x                SetStatus lngBNum, "XB"  'this will set the book status to Calculated Beginning
                        // x            Else
                        SetStatus_6(lngBNum, "EB");
                        // this will set the book status to Edited Beginning
                        // x            End If
                        if (GetBookInfo(ref lngBNum))
                        {
                            // fill the grid with this book information
                            // x                If boolCalculate Then
                            // x                    SetStatus lngBNum, "XE"  'this will set the book status to Calculated End
                            // x                Else
                            SetStatus_6(lngBNum, "EE");
                            // this will set the book status to Edited End
                            // x                End If
                        }
                    }
                }
                if (intCount > 1)
                {
                    // this is when more than one book is selected, if there are multiple
                    // books shown then this grid will need a grand total line at the bottom
                    AddCurrentRow_24(vsCalc.Rows, 0, true);
                    vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColWater, Strings.Format(dblWGrandTotal, "#,##0.00"));
                    vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColSewer, Strings.Format(dblSGrandTotal, "#,##0.00"));
                    vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColConsumption, "Totals:");
                }
                mnuSummary.Enabled = true;
                cmdSummary.Enabled = true;
                vsCalc.Visible = true;
                //frmWait.InstancePtr.Unload();
                StartBookCalculation = true;
                return StartBookCalculation;
            }
            catch (Exception ex)
            {
                
                //frmWait.InstancePtr.Unload();
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Book Calculation", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return StartBookCalculation;
        }

        private double FillBreakDownRow_18(clsDRWrapper rsBreakDown, int intROL, bool boolWater)
        {
            return FillBreakDownRow(ref rsBreakDown, ref intROL, ref boolWater);
        }

        private double FillBreakDownRow(ref clsDRWrapper rsBreakDown, ref int intROL, ref bool boolWater)
        {
            double FillBreakDownRow = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this will fill the breakdown line in
                AddCurrentRow(vsCalc.Rows, intROL);
                vsCalc.TextMatrix(vsCalc.Rows - 1, lngGRIDCOLHidden, rsBreakDown.Get_Fields_Int32("ID"));
                if (boolWater)
                {
                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColWater, rsBreakDown.Get_Fields("Amount"));
                }
                else
                {
                    // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                    vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColSewer, rsBreakDown.Get_Fields("Amount"));
                }
                vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColConsumption, rsBreakDown.Get_Fields_String("Description"));
                vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColName, " ");
                // TODO Get_Fields: Check the table for the column [Amount] and replace with corresponding Get_Field method
                FillBreakDownRow = Conversion.Val(rsBreakDown.Get_Fields("Amount"));
                return FillBreakDownRow;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Adding Breakdown Line", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return FillBreakDownRow;
        }

        private double CalculateTaxes_2(bool boolWater, int lngMeterKey, double dblAmount)
        {
            return CalculateTaxes(ref boolWater, ref lngMeterKey, ref dblAmount);
        }

        private double CalculateTaxes(ref bool boolWater, ref int lngMeterKey, ref double dblAmount)
        {
            double CalculateTaxes = 0;
            clsDRWrapper rsM = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will calculate the tax for each meter and combine them if needed
                // then return the total tax for the Water or Sewer for that bill
                string strWaterSewer = "";
                string strWS = "";
                double dblMeterTax = 0;
                double dblTaxableAmount = 0;
                double dblTaxablePercent = 0;

                if (boolWater)
                {
                    strWaterSewer = "Water";
                    strWS = "W";
                }
                else
                {
                    strWaterSewer = "Sewer";
                    strWS = "S";
                }

                rsM.OpenRecordset("SELECT * FROM MeterTable WHERE ID = " + FCConvert.ToString(lngMeterKey), modExtraModules.strUTDatabase);

                if (!rsM.EndOfFile())
                {
                    // get the taxable amount
                    dblTaxablePercent = rsM.Get_Fields(strWaterSewer + "TaxPercent");

                    if (dblTaxablePercent == 0) return CalculateTaxes;

                    // this meter is not taxable (ie customer)
                    dblTaxableAmount = dblAmount * (dblTaxablePercent / 100);

                    // find the percentage to be taxes
                    dblMeterTax = dblTaxableAmount * modUTStatusPayments.Statics.gdblUTMuniTaxRate;
                }

                CalculateTaxes = FCUtils.Round(dblMeterTax, 2);

                return CalculateTaxes;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Calculating Tax", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsM.DisposeOf();
            }
            return CalculateTaxes;
        }

        private int AddBookLine_24(int lngBookNum, string strDesc, short intROL)
        {
            return AddBookLine(ref lngBookNum, ref strDesc, ref intROL);
        }

        private int AddBookLine(ref int lngBookNum, ref string strDesc, ref short intROL)
        {
            int AddBookLine = 0;
            AddCurrentRow(vsCalc.Rows, intROL);
            vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColBookNumber, Strings.Format(lngBookNum, "0000"));
            vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColName, strDesc);
            vsCalc.TextMatrix(vsCalc.Rows - 1, lngGridColSeq, "Book");
            AddBookLine = vsCalc.Rows - 1;
            // return the line that was added
            return AddBookLine;
        }

        private int GetLastParentMeterRow(ref int lngRW)
        {
            int GetLastParentMeterRow = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will start at the row passed in and check the
                // row outline level of the rows until it finds the parent
                // meter row for this account
                int lngCT;
                int intLevel;
                GetLastParentMeterRow = -1;
                intLevel = 1;
                for (lngCT = lngRW; lngCT >= 1; lngCT--)
                {
                    if (vsCalc.RowOutlineLevel(lngCT) == intLevel)
                    {
                        // If vsCalc.RowOutlineLevel(lngCt - 1) <> intLevel Then
                        if (vsCalc.RowOutlineLevel(lngCT - 1) != intLevel || FCConvert.ToDouble(vsCalc.TextMatrix(lngCT, lngGridColAccount)) != 0)
                        {
                            // this is the row we want
                            GetLastParentMeterRow = lngCT;
                            break;
                        }
                        else
                        {
                            // this must be a combined meter, check the next one
                        }
                    }
                }
                return GetLastParentMeterRow;
            }
            catch
            {
                
                GetLastParentMeterRow = -1;
            }
            return GetLastParentMeterRow;
        }

        private int GetLastParentBookRow(ref int lngRW)
        {
            int GetLastParentBookRow = 0;
            try
            {
                // On Error GoTo ERROR_HANDLER
                // this function will start at the row passed in and check the
                // row outline level of the rows until it finds the parent book row
                // for this account
                int lngCT;
                int intLevel;
                GetLastParentBookRow = -1;
                intLevel = 0;
                for (lngCT = lngRW; lngCT >= 1; lngCT--)
                {
                    if (vsCalc.RowOutlineLevel(lngCT) == intLevel)
                    {
                        // this is the row we want
                        GetLastParentBookRow = lngCT;
                        break;
                    }
                }
                return GetLastParentBookRow;
            }
            catch
            {
                
                GetLastParentBookRow = -1;
            }
            return GetLastParentBookRow;
        }

        private bool CheckPercentageChange_8(int lngAccountKey, int lngBillKey, int lngConsumption, ref double dblPercentageChanged)
        {
            return CheckPercentageChange(ref lngAccountKey, ref lngBillKey, ref lngConsumption, ref dblPercentageChanged);
        }

        private bool CheckPercentageChange(ref int lngAccountKey, ref int lngBillKey, ref int lngConsumption, ref double dblPercentageChanged)
        {
            clsDRWrapper rsLastBill = new clsDRWrapper();
            clsDRWrapper rsPercentage = new clsDRWrapper();
            bool CheckPercentageChange = false;
            // this function will check to see if the percentage difference in the last bill is higher than
            // what the user has set up in File Maintenace > Customize > Billing Options
            // if it is set to 0 then I will not check this or show a warning
            try
            {
                // On Error GoTo ERROR_HANDLER

                double dblCheckPercentage = 0;
                int lngDifference = 0;
                int lngOldConsumption = 0;
                rsPercentage.OpenRecordset("SELECT ConsumptionPercent FROM UtilityBilling", modExtraModules.strUTDatabase);

                if (!rsPercentage.EndOfFile())
                {
                    if (rsPercentage.Get_Fields_Double("ConsumptionPercent") > 0)
                    {
                        if (Conversion.Val(rsPercentage.Get_Fields_Double("ConsumptionPercent")) > 0)
                        {
                            dblCheckPercentage = rsPercentage.Get_Fields_Double("ConsumptionPercent");
                        }
                        else
                        {
                            CheckPercentageChange = false;

                            return CheckPercentageChange;
                        }
                    }
                    else
                    {
                        CheckPercentageChange = false;

                        return CheckPercentageChange;
                    }
                }
                else
                {
                    CheckPercentageChange = false;

                    return CheckPercentageChange;
                }

                rsLastBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND ID < " + FCConvert.ToString(lngBillKey) + " ORDER BY ID desc", modExtraModules.strUTDatabase);

                if (!rsLastBill.EndOfFile())
                {
                    // rsPercentage.OpenRecordset "SELECT * FROM RateTable WHERE
                    lngOldConsumption = FCConvert.ToInt32(rsLastBill.Get_Fields_Int32("Consumption"));
                    lngDifference = lngConsumption - lngOldConsumption;

                    if (lngOldConsumption != 0)
                    {
                        // if the old consumption is zero then do not do a division
                        if (lngDifference > 0)
                        {
                            dblPercentageChanged = Math.Abs(FCConvert.ToDouble(lngDifference) / lngOldConsumption) * 100;
                        }
                        else
                        {
                            dblPercentageChanged = Math.Abs(FCConvert.ToDouble(lngDifference) / lngOldConsumption) * -100;
                        }
                    }
                    else
                    {
                        // DJW@01092013 TROUT-793 if old colsumption si 0 and current consumption is 0 then no change
                        if (lngDifference > 0)
                        {
                            dblPercentageChanged = 100;
                        }
                        else
                        {
                            dblPercentageChanged = 0;
                        }
                    }

                    if (Math.Abs(dblPercentageChanged) > dblCheckPercentage)
                    {
                        CheckPercentageChange = true;
                    }
                    else
                    {
                        CheckPercentageChange = false;
                    }
                }
                else
                {
                    CheckPercentageChange = false;
                }

                return CheckPercentageChange;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Checking Consumption Percentage", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsLastBill.DisposeOf();
                rsPercentage.DisposeOf();
            }

            return CheckPercentageChange;
        }

        private void AddWarningString_24(int lngBK, string strType, string strText)
        {
            AddWarningString(ref lngBK, ref strType, ref strText);
        }

        private void AddWarningString(ref int lngBK, ref string strType, ref string strText)
        {
            int lngCT;
            lngCT = Information.UBound(modUTBilling.Statics.WarningArray, 1);
            lngCT += 1;
            Array.Resize(ref modUTBilling.Statics.WarningArray, lngCT + 1);
            modUTBilling.Statics.WarningArray[lngCT].BillKey = lngBK;
            modUTBilling.Statics.WarningArray[lngCT].Type = strType;
            modUTBilling.Statics.WarningArray[lngCT].Message = strText;
        }

        private void ShowSubtotalsRows_2(bool boolShow)
        {
            ShowSubtotalsRows(ref boolShow);
        }

        private void ShowSubtotalsRows(ref bool boolShow)
        {
            // this routine will go through the grid to hide each subtotal row
            int lngRW;
            for (lngRW = 1; lngRW <= vsCalc.Rows - 1; lngRW++)
            {
                if (Strings.UCase(Strings.Trim(vsCalc.TextMatrix(lngRW, lngGridColConsumption))) == "SUBTOTALS")
                {
                    if (boolShow)
                    {
                        //FC:FINAL:DSE:#i930 Hide the row, insteada of setting its height to 0
                        //vsCalc.RowHeight(lngRW, vsCalc.RowHeight(0));
                        vsCalc.RowHidden(lngRW, false);
                    }
                    else
                    {
                        //FC:FINAL:DSE:#i930 Hide the row, insteada of setting its height to 0
                        //vsCalc.RowHeight(lngRW, 0);
                        vsCalc.RowHidden(lngRW, true);
                    }
                }
            }
        }

        private bool FindFinalBooks()
        {
            bool FindFinalBooks = false;
            clsDRWrapper books = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                int lngCT;
                lngCT = 1;
                books.OpenRecordset("SELECT Distinct Book FROM Bill WHERE ISNULL(Final,0) = 1 AND BillStatus <> 'B' ORDER BY Book", modExtraModules.strUTDatabase);

                while (!books.EndOfFile())
                {
                    Array.Resize(ref BookArray, lngCT + 1);

                    if (lngCT <= Information.UBound(BookArray, 1))
                    {
                        // TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
                        BookArray[lngCT] = FCConvert.ToInt32(books.Get_Fields("Book"));
                        lngCT += 1;
                    }

                    books.MoveNext();
                }

                if (lngCT == 1)
                {
                    FindFinalBooks = false;

                    // if no books were added then add all books
                    // rsB.OpenRecordset "SELECT * FROM Book ORDER BY BookNumber", strUTDatabase
                    // Do Until rsB.EndOfFile
                    // ReDim Preserve BookArray(lngCT)
                    // If lngCT <= UBound(BookArray) Then
                    // BookArray(lngCT) = rsB.Fields("BookNumber")
                    // lngCT = lngCT + 1
                    // End If
                    // rsB.MoveNext
                    // Loop
                }
                else
                {
                    FindFinalBooks = true;
                }

                return FindFinalBooks;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Finding Final Books", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                /*? Resume; */
            }
            finally
            {
                books.DisposeOf();
            }
            return FindFinalBooks;
        }

        public void Limit2xIncrOnSwitch_2(int lngAccountKey, clsDRWrapper rsBill, clsDRWrapper rsM)
        {
            Limit2xIncrOnSwitch(ref lngAccountKey, ref rsBill, ref rsM);
        }

        public void Limit2xIncrOnSwitch(ref int lngAccountKey, ref clsDRWrapper rsBill, ref clsDRWrapper rsM)
        {
            clsDRWrapper rsPrevBill = new clsDRWrapper();

            try
            {
                // On Error GoTo ERROR_HANDLER
                double dblPrevAmount = 0;
                int lngBillKey = 0;
                int lngCons = 0;
                double dblCons = 0;
                int lngRT = 0;
                int intCT;

                // Dim lngActualCons                   As Long
                double dblUnits;
                string strMisc1Desc = "";
                string strMisc2Desc = "";
                double dblMisc1 = 0;
                double dblMisc2 = 0;

                // Dim dblMiscSum1                     As Double
                // Dim dblMiscSum2                     As Double
                // Dim dblSAdjustAmount                As Double
                // Dim dblWAdjustAmount                As Double
                // Dim dblSDEAdjustAmount              As Double
                // Dim dblWDEAdjustAmount              As Double
                // Dim dblFlat                         As Double
                // Dim dblUnit                         As Double
                // Dim dblSumCons                      As Double
                // Dim dblSumFlat                      As Double
                // Dim dblSumUnit                      As Double
                // Dim dblWTax                         As Double
                // Dim dblSTax                         As Double
                double dblTaxableAmount = 0;
                double dblOverrideAmount = 0;

                if (rsM.Get_Fields_Boolean("UseRate1"))
                {
                    dblMisc1 = 0;
                    dblMisc2 = 0;
                    strMisc1Desc = "";
                    strMisc2Desc = "";
                    lngRT = rsM.Get_Fields_Int32("SewerKey1");

                    // get the rate table
                    if (Conversion.Val(rsM.Get_Fields_Double("SewerAmount1")) != 0)
                    {
                        dblOverrideAmount = FCConvert.ToDouble(rsM.Get_Fields_Double("SewerAmount1"));

                        // get the override amount
                    }
                    else
                    {
                        dblOverrideAmount = 0;
                    }

                    if (rsM.Get_Fields_Int32("MeterNumber") == 1)
                    {
                        if (rsBill.Get_Fields_Int32("SewerOverrideCons") == 0)
                        {
                            lngCons = modUTBilling.FindTotalMeterConsumption_6(rsBill.Get_Fields_Int32("AccountKey"), false, rsBill.Get_Fields_Int32("SewerOverrideCons"));
                        }
                        else
                        {
                            lngCons = rsBill.Get_Fields_Int32("SewerOverrideCons");
                        }
                    }
                    else
                        if (FCConvert.ToInt32(dblOverrideAmount) != 0)
                    {
                        lngCons = FCConvert.ToInt32(dblOverrideAmount);
                    }
                    else
                    {
                        if (rsM.Get_Fields_Int32("CurrentReading") != -1)
                        {
                            // kk 110812 trout-884
                            lngCons = rsM.Get_Fields_Int32("CurrentReading") - rsM.Get_Fields_Int32("PreviousReading");
                        }
                        else
                        {
                            lngCons = 0;
                        }
                    }

                    // lngActualCons = lngCons
                    dblCons = FCUtils.Round(modUTBilling.CalculateConsumptionBased_13140(ref lngCons, lngRT, false, ref dblMisc1, ref dblMisc2, ref dblTaxableAmount, ref strMisc1Desc, ref strMisc2Desc, modUTBilling.FindFinalPercentage(ref rsBill, ref boolFinal), ref boolCurMeterMinimum), 2);

                    // Get previous billings (2) and make sure they are Flat/Unit based
                    lngBillKey = rsBill.Get_Fields_Int32("ID");
                    rsPrevBill.OpenRecordset("SELECT * FROM Bill WHERE AccountKey = " + FCConvert.ToString(lngAccountKey) + " AND ID < " + FCConvert.ToString(lngBillKey) + " ORDER BY ID Desc", modExtraModules.strUTDatabase);

                    if (!rsPrevBill.EndOfFile())
                    {
                        if (((DateTime)rsPrevBill.Get_Fields_DateTime("BillDate")).Year == DateTime.Now.Year - 1)
                        {
                            if (FCConvert.ToInt32(rsPrevBill.Get_Fields_Int32("Consumption")) == 0)
                            {
                                if (rsPrevBill.Get_Fields_Double("SPrinOwed") > 0)
                                {
                                    // rsPrevBill.Fields("SUnitsAmount")   ' bug in Split routine
                                    dblPrevAmount = rsPrevBill.Get_Fields_Double("SPrinOwed");
                                }

                                rsPrevBill.MoveNext();

                                if (!rsPrevBill.EndOfFile())
                                {
                                    if (((DateTime)rsPrevBill.Get_Fields_DateTime("BillDate")).Year == DateTime.Now.Year - 1)
                                    {
                                        if (rsPrevBill.Get_Fields_Double("SPrinOwed") > 0)
                                        {
                                            // rsPrevBill.Fields("SUnitsAmount")   ' bug in Split routine
                                            dblPrevAmount += rsPrevBill.Get_Fields_Double("SPrinOwed");
                                        }
                                    }
                                }

                                if (dblPrevAmount * 2 < dblCons)
                                {
                                    rsBill.Set_Fields("SewerOverrideAmount", dblPrevAmount * 2);
                                    rsBill.Set_Fields("SHasOverride", true);
                                }
                            }
                        }
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                
                MessageBox.Show("Error #"
                                + FCConvert.ToString(Information.Err(ex)
                                                                .Number)
                                + " - "
                                + Information.Err(ex)
                                             .Description
                                + ".", "Error Updating Bill Record", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                rsPrevBill.DisposeOf();
            }
        }

        public class StaticVariables
        {
            /// </summary>
            /// Summary description for frmCalculation.
            /// <summary>
            public int R = 0;
            public int c = 0;
        }

        public static StaticVariables Statics
        {
            get
            {
                return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
            }
        }
    }
}
