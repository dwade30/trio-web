﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arRateTableSummary.
	/// </summary>
	partial class arRateTableSummary
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arRateTableSummary));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.lblAccountNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblWater = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label8 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label10 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label11 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldAcct = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMeter = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldService = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldW1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldW2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldW3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldS1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldS2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldS3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldW4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldW5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.FldS4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldS5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWKey1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWKey2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWKey3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSKey1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSKey2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSKey3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWKey4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWKey5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSKey4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSKey5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSUnits1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWUnits2 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWUnits3 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWUnits4 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWUnits5 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblWaterTot = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line4 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label16 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldWUnitsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWFlatTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldWConsumptionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label19 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line5 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label20 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label21 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label22 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldSUnitsTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSFlatTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSConsumptionTotal = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWater)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeter)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldService)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.FldS4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterTot)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnitsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWFlatTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWConsumptionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnitsTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSFlatTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSConsumptionTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldAcct,
				this.fldName,
				this.fldMeter,
				this.fldService,
				this.fldW1,
				this.fldW2,
				this.fldW3,
				this.fldS1,
				this.fldS2,
				this.fldS3,
				this.fldW4,
				this.fldW5,
				this.FldS4,
				this.fldS5,
				this.fldWKey1,
				this.fldWKey2,
				this.fldWKey3,
				this.fldSKey1,
				this.fldSKey2,
				this.fldSKey3,
				this.fldWKey4,
				this.fldWKey5,
				this.fldSKey4,
				this.fldSKey5,
				this.fldWUnits1,
				this.fldSUnits1,
				this.fldSUnits2,
				this.fldSUnits3,
				this.fldSUnits4,
				this.fldSUnits5,
				this.fldWUnits2,
				this.fldWUnits3,
				this.fldWUnits4,
				this.fldWUnits5
			});
			this.Detail.Height = 0.9583333F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblAccountNumber,
				this.lblName,
				this.Label4,
				this.Label5,
				this.lblWater,
				this.Label1,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.Label8,
				this.Line1,
				this.Line2,
				this.Label9,
				this.Label10,
				this.Label11,
				this.Label12,
				this.Label13,
				this.Label14,
				this.Line3
			});
			this.PageHeader.Height = 0.8645833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblWaterTot,
				this.Line4,
				this.Label16,
				this.Label17,
				this.Label18,
				this.fldWUnitsTotal,
				this.fldWFlatTotal,
				this.fldWConsumptionTotal,
				this.Label19,
				this.Line5,
				this.Label20,
				this.Label21,
				this.Label22,
				this.fldSUnitsTotal,
				this.fldSFlatTotal,
				this.fldSConsumptionTotal
			});
			this.GroupFooter1.Height = 0.90625F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// lblAccountNumber
			// 
			this.lblAccountNumber.Height = 0.1875F;
			this.lblAccountNumber.HyperLink = null;
			this.lblAccountNumber.Left = 0F;
			this.lblAccountNumber.Name = "lblAccountNumber";
			this.lblAccountNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.lblAccountNumber.Text = "Account";
			this.lblAccountNumber.Top = 0.65625F;
			this.lblAccountNumber.Width = 0.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.6875F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.65625F;
			this.lblName.Width = 1.375F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 2.34375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
			this.Label4.Text = "Meter #";
			this.Label4.Top = 0.65625F;
			this.Label4.Width = 0.5F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 2.84375F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label5.Text = "Serv";
			this.Label5.Top = 0.65625F;
			this.Label5.Width = 0.5F;
			// 
			// lblWater
			// 
			this.lblWater.Height = 0.1875F;
			this.lblWater.HyperLink = null;
			this.lblWater.Left = 3.6875F;
			this.lblWater.Name = "lblWater";
			this.lblWater.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblWater.Text = "Water";
			this.lblWater.Top = 0.4375F;
			this.lblWater.Width = 1.5625F;
			// 
			// Label1
			// 
			this.Label1.Height = 0.25F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 0F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.Label1.Text = "Rate Table Summary";
			this.Label1.Top = 0F;
			this.Label1.Width = 7.96875F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.375F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 6.96875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 6.96875F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1F;
			// 
			// Label8
			// 
			this.Label8.Height = 0.1875F;
			this.Label8.HyperLink = null;
			this.Label8.Left = 5.84375F;
			this.Label8.Name = "Label8";
			this.Label8.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; te" + "xt-decoration: underline; ddo-char-set: 0";
			this.Label8.Text = "Sewer";
			this.Label8.Top = 0.4375F;
			this.Label8.Width = 1.5625F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.34375F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.84375F;
			this.Line1.Width = 7.5F;
			this.Line1.X1 = 0.34375F;
			this.Line1.X2 = 7.84375F;
			this.Line1.Y1 = 0.84375F;
			this.Line1.Y2 = 0.84375F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 3.34375F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.625F;
			this.Line2.Width = 2.0625F;
			this.Line2.X1 = 3.34375F;
			this.Line2.X2 = 5.40625F;
			this.Line2.Y1 = 0.625F;
			this.Line2.Y2 = 0.625F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 3.34375F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label9.Text = "Type";
			this.Label9.Top = 0.65625F;
			this.Label9.Width = 0.5F;
			// 
			// Label10
			// 
			this.Label10.Height = 0.1875F;
			this.Label10.HyperLink = null;
			this.Label10.Left = 4.21875F;
			this.Label10.Name = "Label10";
			this.Label10.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: left; ddo-" + "char-set: 0";
			this.Label10.Text = "Table";
			this.Label10.Top = 0.65625F;
			this.Label10.Width = 0.53125F;
			// 
			// Label11
			// 
			this.Label11.Height = 0.1875F;
			this.Label11.HyperLink = null;
			this.Label11.Left = 4.9375F;
			this.Label11.Name = "Label11";
			this.Label11.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label11.Text = "Units";
			this.Label11.Top = 0.65625F;
			this.Label11.Width = 0.4375F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 5.625F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label12.Text = "Type";
			this.Label12.Top = 0.65625F;
			this.Label12.Width = 0.5F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 6.5F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.Label13.Text = "Table";
			this.Label13.Top = 0.65625F;
			this.Label13.Width = 0.53125F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 7.28125F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" + "-char-set: 0";
			this.Label14.Text = "Units";
			this.Label14.Top = 0.65625F;
			this.Label14.Width = 0.4375F;
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 5.625F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0.625F;
			this.Line3.Width = 2.09375F;
			this.Line3.X1 = 5.625F;
			this.Line3.X2 = 7.71875F;
			this.Line3.Y1 = 0.625F;
			this.Line3.Y2 = 0.625F;
			// 
			// fldAcct
			// 
			this.fldAcct.Height = 0.1875F;
			this.fldAcct.Left = 0F;
			this.fldAcct.Name = "fldAcct";
			this.fldAcct.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldAcct.Text = null;
			this.fldAcct.Top = 0F;
			this.fldAcct.Width = 0.625F;
			// 
			// fldName
			// 
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.6875F;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 9pt; ddo-char-set: 0";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 1.59375F;
			// 
			// fldMeter
			// 
			this.fldMeter.Height = 0.1875F;
			this.fldMeter.Left = 2.34375F;
			this.fldMeter.Name = "fldMeter";
			this.fldMeter.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldMeter.Text = null;
			this.fldMeter.Top = 0F;
			this.fldMeter.Width = 0.5F;
			// 
			// fldService
			// 
			this.fldService.Height = 0.1875F;
			this.fldService.Left = 2.84375F;
			this.fldService.Name = "fldService";
			this.fldService.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: center; ddo-char-set: 0";
			this.fldService.Text = null;
			this.fldService.Top = 0F;
			this.fldService.Width = 0.5F;
			// 
			// fldW1
			// 
			this.fldW1.Height = 0.1875F;
			this.fldW1.Left = 3.34375F;
			this.fldW1.Name = "fldW1";
			this.fldW1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldW1.Text = null;
			this.fldW1.Top = 0F;
			this.fldW1.Width = 0.875F;
			// 
			// fldW2
			// 
			this.fldW2.Height = 0.1875F;
			this.fldW2.Left = 3.34375F;
			this.fldW2.Name = "fldW2";
			this.fldW2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldW2.Text = null;
			this.fldW2.Top = 0.1875F;
			this.fldW2.Width = 0.875F;
			// 
			// fldW3
			// 
			this.fldW3.Height = 0.1875F;
			this.fldW3.Left = 3.34375F;
			this.fldW3.Name = "fldW3";
			this.fldW3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldW3.Text = null;
			this.fldW3.Top = 0.375F;
			this.fldW3.Width = 0.875F;
			// 
			// fldS1
			// 
			this.fldS1.Height = 0.1875F;
			this.fldS1.Left = 5.65625F;
			this.fldS1.Name = "fldS1";
			this.fldS1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldS1.Text = null;
			this.fldS1.Top = 0F;
			this.fldS1.Width = 0.875F;
			// 
			// fldS2
			// 
			this.fldS2.Height = 0.1875F;
			this.fldS2.Left = 5.65625F;
			this.fldS2.Name = "fldS2";
			this.fldS2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldS2.Text = null;
			this.fldS2.Top = 0.1875F;
			this.fldS2.Width = 0.875F;
			// 
			// fldS3
			// 
			this.fldS3.Height = 0.1875F;
			this.fldS3.Left = 5.65625F;
			this.fldS3.Name = "fldS3";
			this.fldS3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldS3.Text = null;
			this.fldS3.Top = 0.375F;
			this.fldS3.Width = 0.875F;
			// 
			// fldW4
			// 
			this.fldW4.Height = 0.1875F;
			this.fldW4.Left = 3.34375F;
			this.fldW4.Name = "fldW4";
			this.fldW4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldW4.Text = null;
			this.fldW4.Top = 0.5625F;
			this.fldW4.Width = 0.875F;
			// 
			// fldW5
			// 
			this.fldW5.Height = 0.1875F;
			this.fldW5.Left = 3.34375F;
			this.fldW5.Name = "fldW5";
			this.fldW5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldW5.Text = null;
			this.fldW5.Top = 0.75F;
			this.fldW5.Width = 0.875F;
			// 
			// FldS4
			// 
			this.FldS4.Height = 0.1875F;
			this.FldS4.Left = 5.65625F;
			this.FldS4.Name = "FldS4";
			this.FldS4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.FldS4.Text = null;
			this.FldS4.Top = 0.5625F;
			this.FldS4.Width = 0.875F;
			// 
			// fldS5
			// 
			this.fldS5.Height = 0.1875F;
			this.fldS5.Left = 5.65625F;
			this.fldS5.Name = "fldS5";
			this.fldS5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldS5.Text = null;
			this.fldS5.Top = 0.75F;
			this.fldS5.Width = 0.875F;
			// 
			// fldWKey1
			// 
			this.fldWKey1.Height = 0.1875F;
			this.fldWKey1.Left = 4.21875F;
			this.fldWKey1.Name = "fldWKey1";
			this.fldWKey1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldWKey1.Text = null;
			this.fldWKey1.Top = 0F;
			this.fldWKey1.Width = 0.53125F;
			// 
			// fldWKey2
			// 
			this.fldWKey2.Height = 0.1875F;
			this.fldWKey2.Left = 4.21875F;
			this.fldWKey2.Name = "fldWKey2";
			this.fldWKey2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldWKey2.Text = null;
			this.fldWKey2.Top = 0.1875F;
			this.fldWKey2.Width = 0.53125F;
			// 
			// fldWKey3
			// 
			this.fldWKey3.Height = 0.1875F;
			this.fldWKey3.Left = 4.21875F;
			this.fldWKey3.Name = "fldWKey3";
			this.fldWKey3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldWKey3.Text = null;
			this.fldWKey3.Top = 0.375F;
			this.fldWKey3.Width = 0.53125F;
			// 
			// fldSKey1
			// 
			this.fldSKey1.Height = 0.1875F;
			this.fldSKey1.Left = 6.53125F;
			this.fldSKey1.Name = "fldSKey1";
			this.fldSKey1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldSKey1.Text = null;
			this.fldSKey1.Top = 0F;
			this.fldSKey1.Width = 0.53125F;
			// 
			// fldSKey2
			// 
			this.fldSKey2.Height = 0.1875F;
			this.fldSKey2.Left = 6.53125F;
			this.fldSKey2.Name = "fldSKey2";
			this.fldSKey2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldSKey2.Text = null;
			this.fldSKey2.Top = 0.1875F;
			this.fldSKey2.Width = 0.53125F;
			// 
			// fldSKey3
			// 
			this.fldSKey3.Height = 0.1875F;
			this.fldSKey3.Left = 6.53125F;
			this.fldSKey3.Name = "fldSKey3";
			this.fldSKey3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldSKey3.Text = null;
			this.fldSKey3.Top = 0.375F;
			this.fldSKey3.Width = 0.53125F;
			// 
			// fldWKey4
			// 
			this.fldWKey4.Height = 0.1875F;
			this.fldWKey4.Left = 4.21875F;
			this.fldWKey4.Name = "fldWKey4";
			this.fldWKey4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldWKey4.Text = null;
			this.fldWKey4.Top = 0.5625F;
			this.fldWKey4.Width = 0.53125F;
			// 
			// fldWKey5
			// 
			this.fldWKey5.Height = 0.1875F;
			this.fldWKey5.Left = 4.21875F;
			this.fldWKey5.Name = "fldWKey5";
			this.fldWKey5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldWKey5.Text = null;
			this.fldWKey5.Top = 0.75F;
			this.fldWKey5.Width = 0.53125F;
			// 
			// fldSKey4
			// 
			this.fldSKey4.Height = 0.1875F;
			this.fldSKey4.Left = 6.53125F;
			this.fldSKey4.Name = "fldSKey4";
			this.fldSKey4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldSKey4.Text = null;
			this.fldSKey4.Top = 0.5625F;
			this.fldSKey4.Width = 0.53125F;
			// 
			// fldSKey5
			// 
			this.fldSKey5.Height = 0.1875F;
			this.fldSKey5.Left = 6.53125F;
			this.fldSKey5.Name = "fldSKey5";
			this.fldSKey5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: left; ddo-char-set: 0";
			this.fldSKey5.Text = null;
			this.fldSKey5.Top = 0.75F;
			this.fldSKey5.Width = 0.53125F;
			// 
			// fldWUnits1
			// 
			this.fldWUnits1.Height = 0.1875F;
			this.fldWUnits1.Left = 4.78125F;
			this.fldWUnits1.Name = "fldWUnits1";
			this.fldWUnits1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldWUnits1.Text = null;
			this.fldWUnits1.Top = 0F;
			this.fldWUnits1.Width = 0.625F;
			// 
			// fldSUnits1
			// 
			this.fldSUnits1.Height = 0.1875F;
			this.fldSUnits1.Left = 7.09375F;
			this.fldSUnits1.Name = "fldSUnits1";
			this.fldSUnits1.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSUnits1.Text = null;
			this.fldSUnits1.Top = 0F;
			this.fldSUnits1.Width = 0.625F;
			// 
			// fldSUnits2
			// 
			this.fldSUnits2.Height = 0.1875F;
			this.fldSUnits2.Left = 7.09375F;
			this.fldSUnits2.Name = "fldSUnits2";
			this.fldSUnits2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSUnits2.Text = null;
			this.fldSUnits2.Top = 0.1875F;
			this.fldSUnits2.Width = 0.625F;
			// 
			// fldSUnits3
			// 
			this.fldSUnits3.Height = 0.1875F;
			this.fldSUnits3.Left = 7.09375F;
			this.fldSUnits3.Name = "fldSUnits3";
			this.fldSUnits3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSUnits3.Text = null;
			this.fldSUnits3.Top = 0.375F;
			this.fldSUnits3.Width = 0.625F;
			// 
			// fldSUnits4
			// 
			this.fldSUnits4.Height = 0.1875F;
			this.fldSUnits4.Left = 7.09375F;
			this.fldSUnits4.Name = "fldSUnits4";
			this.fldSUnits4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSUnits4.Text = null;
			this.fldSUnits4.Top = 0.5625F;
			this.fldSUnits4.Width = 0.625F;
			// 
			// fldSUnits5
			// 
			this.fldSUnits5.Height = 0.1875F;
			this.fldSUnits5.Left = 7.09375F;
			this.fldSUnits5.Name = "fldSUnits5";
			this.fldSUnits5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldSUnits5.Text = null;
			this.fldSUnits5.Top = 0.75F;
			this.fldSUnits5.Width = 0.625F;
			// 
			// fldWUnits2
			// 
			this.fldWUnits2.Height = 0.1875F;
			this.fldWUnits2.Left = 4.78125F;
			this.fldWUnits2.Name = "fldWUnits2";
			this.fldWUnits2.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldWUnits2.Text = null;
			this.fldWUnits2.Top = 0.1875F;
			this.fldWUnits2.Width = 0.625F;
			// 
			// fldWUnits3
			// 
			this.fldWUnits3.Height = 0.1875F;
			this.fldWUnits3.Left = 4.78125F;
			this.fldWUnits3.Name = "fldWUnits3";
			this.fldWUnits3.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldWUnits3.Text = null;
			this.fldWUnits3.Top = 0.375F;
			this.fldWUnits3.Width = 0.625F;
			// 
			// fldWUnits4
			// 
			this.fldWUnits4.Height = 0.1875F;
			this.fldWUnits4.Left = 4.78125F;
			this.fldWUnits4.Name = "fldWUnits4";
			this.fldWUnits4.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldWUnits4.Text = null;
			this.fldWUnits4.Top = 0.5625F;
			this.fldWUnits4.Width = 0.625F;
			// 
			// fldWUnits5
			// 
			this.fldWUnits5.Height = 0.1875F;
			this.fldWUnits5.Left = 4.78125F;
			this.fldWUnits5.Name = "fldWUnits5";
			this.fldWUnits5.Style = "font-family: \'Tahoma\'; font-size: 9pt; text-align: right; ddo-char-set: 0";
			this.fldWUnits5.Text = null;
			this.fldWUnits5.Top = 0.75F;
			this.fldWUnits5.Width = 0.625F;
			// 
			// lblWaterTot
			// 
			this.lblWaterTot.Height = 0.19F;
			this.lblWaterTot.HyperLink = null;
			this.lblWaterTot.Left = 2.1875F;
			this.lblWaterTot.Name = "lblWaterTot";
			this.lblWaterTot.Style = "font-weight: bold; text-align: center";
			this.lblWaterTot.Text = "Water Totals";
			this.lblWaterTot.Top = 0.1875F;
			this.lblWaterTot.Width = 1.46875F;
			// 
			// Line4
			// 
			this.Line4.Height = 0F;
			this.Line4.Left = 2.03125F;
			this.Line4.LineWeight = 1F;
			this.Line4.Name = "Line4";
			this.Line4.Top = 0.34375F;
			this.Line4.Width = 1.78125F;
			this.Line4.X1 = 2.03125F;
			this.Line4.X2 = 3.8125F;
			this.Line4.Y1 = 0.34375F;
			this.Line4.Y2 = 0.34375F;
			// 
			// Label16
			// 
			this.Label16.Height = 0.19F;
			this.Label16.HyperLink = null;
			this.Label16.Left = 2.03125F;
			this.Label16.Name = "Label16";
			this.Label16.Style = "font-weight: bold";
			this.Label16.Text = "Units";
			this.Label16.Top = 0.375F;
			this.Label16.Width = 1F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.19F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 2.03125F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-weight: bold";
			this.Label17.Text = "Flat";
			this.Label17.Top = 0.53125F;
			this.Label17.Width = 1F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.19F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.03125F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-weight: bold";
			this.Label18.Text = "Consumption";
			this.Label18.Top = 0.6875F;
			this.Label18.Width = 1F;
			// 
			// fldWUnitsTotal
			// 
			this.fldWUnitsTotal.Height = 0.19F;
			this.fldWUnitsTotal.Left = 3.0625F;
			this.fldWUnitsTotal.Name = "fldWUnitsTotal";
			this.fldWUnitsTotal.Style = "font-weight: bold; text-align: right";
			this.fldWUnitsTotal.Text = "Field1";
			this.fldWUnitsTotal.Top = 0.375F;
			this.fldWUnitsTotal.Width = 0.75F;
			// 
			// fldWFlatTotal
			// 
			this.fldWFlatTotal.Height = 0.19F;
			this.fldWFlatTotal.Left = 3.0625F;
			this.fldWFlatTotal.Name = "fldWFlatTotal";
			this.fldWFlatTotal.Style = "font-weight: bold; text-align: right";
			this.fldWFlatTotal.Text = "Field2";
			this.fldWFlatTotal.Top = 0.53125F;
			this.fldWFlatTotal.Width = 0.75F;
			// 
			// fldWConsumptionTotal
			// 
			this.fldWConsumptionTotal.Height = 0.19F;
			this.fldWConsumptionTotal.Left = 3.0625F;
			this.fldWConsumptionTotal.Name = "fldWConsumptionTotal";
			this.fldWConsumptionTotal.Style = "font-weight: bold; text-align: right";
			this.fldWConsumptionTotal.Text = "Field3";
			this.fldWConsumptionTotal.Top = 0.6875F;
			this.fldWConsumptionTotal.Width = 0.75F;
			// 
			// Label19
			// 
			this.Label19.Height = 0.19F;
			this.Label19.HyperLink = null;
			this.Label19.Left = 4.34375F;
			this.Label19.Name = "Label19";
			this.Label19.Style = "font-weight: bold; text-align: center";
			this.Label19.Text = "SewerTotals";
			this.Label19.Top = 0.1875F;
			this.Label19.Width = 1.46875F;
			// 
			// Line5
			// 
			this.Line5.Height = 0F;
			this.Line5.Left = 4.1875F;
			this.Line5.LineWeight = 1F;
			this.Line5.Name = "Line5";
			this.Line5.Top = 0.34375F;
			this.Line5.Width = 1.78125F;
			this.Line5.X1 = 4.1875F;
			this.Line5.X2 = 5.96875F;
			this.Line5.Y1 = 0.34375F;
			this.Line5.Y2 = 0.34375F;
			// 
			// Label20
			// 
			this.Label20.Height = 0.19F;
			this.Label20.HyperLink = null;
			this.Label20.Left = 4.1875F;
			this.Label20.Name = "Label20";
			this.Label20.Style = "font-weight: bold";
			this.Label20.Text = "Units";
			this.Label20.Top = 0.375F;
			this.Label20.Width = 1F;
			// 
			// Label21
			// 
			this.Label21.Height = 0.19F;
			this.Label21.HyperLink = null;
			this.Label21.Left = 4.1875F;
			this.Label21.Name = "Label21";
			this.Label21.Style = "font-weight: bold";
			this.Label21.Text = "Flat";
			this.Label21.Top = 0.53125F;
			this.Label21.Width = 1F;
			// 
			// Label22
			// 
			this.Label22.Height = 0.19F;
			this.Label22.HyperLink = null;
			this.Label22.Left = 4.1875F;
			this.Label22.Name = "Label22";
			this.Label22.Style = "font-weight: bold";
			this.Label22.Text = "Consumption";
			this.Label22.Top = 0.6875F;
			this.Label22.Width = 1F;
			// 
			// fldSUnitsTotal
			// 
			this.fldSUnitsTotal.Height = 0.19F;
			this.fldSUnitsTotal.Left = 5.21875F;
			this.fldSUnitsTotal.Name = "fldSUnitsTotal";
			this.fldSUnitsTotal.Style = "font-weight: bold; text-align: right";
			this.fldSUnitsTotal.Text = "Field1";
			this.fldSUnitsTotal.Top = 0.375F;
			this.fldSUnitsTotal.Width = 0.75F;
			// 
			// fldSFlatTotal
			// 
			this.fldSFlatTotal.Height = 0.19F;
			this.fldSFlatTotal.Left = 5.21875F;
			this.fldSFlatTotal.Name = "fldSFlatTotal";
			this.fldSFlatTotal.Style = "font-weight: bold; text-align: right";
			this.fldSFlatTotal.Text = "Field2";
			this.fldSFlatTotal.Top = 0.53125F;
			this.fldSFlatTotal.Width = 0.75F;
			// 
			// fldSConsumptionTotal
			// 
			this.fldSConsumptionTotal.Height = 0.19F;
			this.fldSConsumptionTotal.Left = 5.21875F;
			this.fldSConsumptionTotal.Name = "fldSConsumptionTotal";
			this.fldSConsumptionTotal.Style = "font-weight: bold; text-align: right";
			this.fldSConsumptionTotal.Text = "Field3";
			this.fldSConsumptionTotal.Top = 0.6875F;
			this.fldSConsumptionTotal.Width = 0.75F;
			// 
			// arRateTableSummary
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0.25F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.96875F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblAccountNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWater)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label8)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label10)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label11)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAcct)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMeter)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldService)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldW5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.FldS4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldS5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWKey5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSKey5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnits5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblWaterTot)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label16)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWUnitsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWFlatTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldWConsumptionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label19)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label20)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label21)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label22)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSUnitsTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSFlatTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSConsumptionTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAcct;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMeter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldService;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldW1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldW2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldW3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldS1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldS2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldS3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldW4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldW5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox FldS4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldS5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWKey1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWKey2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWKey3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSKey1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSKey2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSKey3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWKey4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWKey5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSKey4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSKey5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSUnits1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWUnits2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWUnits3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWUnits4;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWUnits5;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccountNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWater;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label8;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label10;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label11;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblWaterTot;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label16;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWUnitsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWFlatTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldWConsumptionTotal;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label19;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label20;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label21;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label22;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSUnitsTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSFlatTotal;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSConsumptionTotal;
	}
}
