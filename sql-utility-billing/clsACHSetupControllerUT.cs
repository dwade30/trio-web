//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class clsACHSetupControllerUT
	{

		//=========================================================

		public bool Load(ref clsACHSetup tACHSetup)
		{
			bool Load = false;
			try
			{	// On Error GoTo ErrorHandler
				
				clsDRWrapper rsLoad = new/*AsNew*/ clsDRWrapper();

				rsLoad.OpenRecordset("SELECT * FROM tblACHInformation", "twut0000.vb1");
				if (!rsLoad.EndOfFile()) {
					tACHSetup.CompanyAccount = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("CompanyAccount")));
					int intTemp = 0;
					intTemp = (int)Math.Round(fecherFoundation.Conversion.Val(FCConvert.ToString(rsLoad.Get_Fields("CompanyAccountType"))));
					if (intTemp<2) {
						intTemp = 2;
					}
					tACHSetup.CompanyAccountType = intTemp;
					tACHSetup.CompanyID = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("CompanyID")));
					tACHSetup.CompanyName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("CompanyName")));
					tACHSetup.CompanyRT = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("CompanyRT")));
					tACHSetup.ImmediateOriginName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("OriginName")));
					tACHSetup.ImmediateOriginODFI = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("ODFINum"))); // tACHSetup.ImmediateOriginODFI
					tACHSetup.ImmediateOriginRT = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("OriginRT"))); // tACHSetup.ImmediateOriginRT
					tACHSetup.ImmediateDestinationName = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("DestinationName")));
					tACHSetup.ImmediateDestinationRT = fecherFoundation.Strings.Trim(FCConvert.ToString(rsLoad.Get_Fields("DestinationRT")));

				} else {
					rsLoad.OpenRecordset("select * from GLOBALVARIABLES", "twgn0000.vb1");
					tACHSetup.CompanyName = rsLoad.Get_Fields("CityTown")+" of "+rsLoad.Get_Fields("MuniName");
					tACHSetup.CompanyAccountType = 27;
					return Load;
				}
				Load = true;
				return Load;
			}
			catch
			{	// ErrorHandler:

			}
			return Load;
		}

		public bool Save(ref clsACHSetup tACHSetup)
		{
			bool Save = false;
			try
			{	// On Error GoTo ErrorHandler
				clsDRWrapper rsSave = new/*AsNew*/ clsDRWrapper();

				rsSave.OpenRecordset("SELECT * FROM tblACHInformation", "twut0000.vb1");
				if (!rsSave.EndOfFile()) {
					rsSave.Edit();
				} else {
					rsSave.AddNew();
				}
				rsSave.Set_Fields("DestinationRT", tACHSetup.ImmediateDestinationRT);
				rsSave.Set_Fields("DestinationName", tACHSetup.ImmediateDestinationName);
				rsSave.Set_Fields("CompanyName", tACHSetup.CompanyName);
				rsSave.Set_Fields("CompanyRT", tACHSetup.CompanyRT);
				rsSave.Set_Fields("CompanyAccount", tACHSetup.CompanyAccount);
				rsSave.Set_Fields("CompanyAccountType", tACHSetup.CompanyAccountType);
				rsSave.Set_Fields("CompanyID", tACHSetup.CompanyID);
				rsSave.Update();
				Save = true;
				return Save;
			}
			catch
			{	// ErrorHandler:

			}
			return Save;
		}

	}
}
