﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptMeterReadingList.
	/// </summary>
	public partial class rptMeterReadingList : BaseSectionReport
	{
		public rptMeterReadingList()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			// FC:FINAL:VGE - i913 Name Changed
			this.Name = "Meter Reading List";
		}

		public static rptMeterReadingList InstancePtr
		{
			get
			{
				return (rptMeterReadingList)Sys.GetInstance(typeof(rptMeterReadingList));
			}
		}

		protected rptMeterReadingList _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsBook.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptMeterReadingList	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               09/07/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               11/01/2006              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsBook = new clsDRWrapper();
		string strBooks = "";
		int[] BookArray = null;
		double dblStartSeq;
		double dblEndSeq;

		public void Init(ref int[] BA, double dblPassStartSeq, double dblPassEndSeq)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strWhere = "";
				BookArray = BA;
				dblStartSeq = dblPassStartSeq;
				dblEndSeq = dblPassEndSeq;
				if (dblEndSeq > 0)
				{
					strWhere = " AND (Sequence >= " + FCConvert.ToString(dblStartSeq) + " AND Sequence <= " + FCConvert.ToString(dblEndSeq) + ")";
				}
				else
				{
					strWhere = "";
				}
				SetBookString();
				rsData.OpenRecordset("SELECT AccountNumber, CurrentReading, PreviousReading, Sequence, SerialNumber, UseMeterChangeOut, BookNumber, StreetNumber, StreetName, pBill.FullNameLF AS Name " + "FROM MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView pBill ON pBill.ID = Master.BillingPartyID WHERE BookNumber " + strBooks + strWhere + " ORDER BY BookNumber, Sequence", modExtraModules.strUTDatabase);
				lblCriteria.Text = "For Book(s): " + strBooks;
				frmReportViewer.InstancePtr.Init(this);
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Initializing Consumption Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void ActiveReport_DataInitialize(object sender, EventArgs e)
		{
			this.Fields.Add("TheBinder");
			this.Fields["TheBinder"].Value = 0;
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
			if (!eArgs.EOF)
			{
				this.Fields["TheBinder"].Value = rsData.Get_Fields_Int32("BookNumber");
			}
		}

		private void ActiveReport_KeyPress(ref short KeyAscii)
		{
			// captures the escape key
			if (KeyAscii == 27)
			{
				KeyAscii = 0;
				this.Close();
			}
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
				FillHeaderLabels();
				return;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Starting Report", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void FillHeaderLabels()
		{
			lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm:ss tt");
			lblMuniName.Text = modGlobalConstants.Statics.MuniName;
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void GroupHeader1_Format(object sender, EventArgs e)
		{
			rsBook.OpenRecordset("SELECT DISTINCT BookNumber FROM MeterTable WHERE BookNumber = " + this.Fields["TheBinder"].Value, modExtraModules.strUTDatabase);
			if (!rsBook.EndOfFile())
			{
				fldBookNum.Text = FCConvert.ToString(this.Fields["TheBinder"].Value);
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			lblPageNumber.Text = "Page " + this.PageNumber;
		}

		private void BindFields()
		{
			if (!rsData.EndOfFile())
			{
				//FC:FINAL:MSH - can't implicitly convert from in to string
				// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
				fldAccount.Text = FCConvert.ToString(rsData.Get_Fields("AccountNumber"));
				fldCurrentReading.Text = "________";
				// kk07252014 trout-1080  Previous Reading shows as NR, need to check the billing status
				// If rsData.Fields("CurrentReading") = -1 Then     'kk 110812 trout-884  Catch No Read (-1)
				// fldPreviousReading.Text = "NR"
				// Else
				// fldPreviousReading.Text = rsData.Fields("CurrentReading")
				// End If
				if (FCConvert.ToString(rsData.Get_Fields_String("BillingStatus")) == "C" || FCConvert.ToInt32(rsData.Get_Fields_Int32("CurrentReading")) == -1)
				{
					fldPreviousReading.Text = FCConvert.ToString(rsData.Get_Fields_Int32("PreviousReading"));
				}
				else
				{
					fldPreviousReading.Text = FCConvert.ToString(rsData.Get_Fields_Int32("CurrentReading"));
				}
				if (Conversion.Val(fldPreviousReading.Text) == -1)
				{
					fldPreviousReading.Text = "NR";
				}
				// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
				fldSequence.Text = FCConvert.ToString(rsData.Get_Fields("Sequence"));
				fldSerialNum.Text = FCConvert.ToString(rsData.Get_Fields_String("SerialNumber"));
				if (rsData.Get_Fields_Boolean("UseMeterChangeOut") == true)
				{
					fldSerialNum.Text = fldSerialNum.Text + "**";
				}
				// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
				fldLocation.Text = FCConvert.ToString(rsData.Get_Fields("StreetNumber")) + " " + rsData.Get_Fields_String("StreetName");
				fldName.Text = FCConvert.ToString(rsData.Get_Fields_String("Name"));
				rsData.MoveNext();
			}
		}

		private void SetBookString()
		{
			int intCT;
			strBooks = "";
			for (intCT = 1; intCT <= Information.UBound(BookArray, 1); intCT++)
			{
				strBooks += FCConvert.ToString(BookArray[intCT]) + ",";
			}
			strBooks = " IN (" + Strings.Left(strBooks, strBooks.Length - 1) + ")";
		}

		private void rptMeterReadingList_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptMeterReadingList properties;
			//rptMeterReadingList.Caption	= "Meter Reading List";
			//rptMeterReadingList.Icon	= "rptMeterReadingList.dsx":0000";
			//rptMeterReadingList.Left	= 0;
			//rptMeterReadingList.Top	= 0;
			//rptMeterReadingList.Width	= 11880;
			//rptMeterReadingList.Height	= 8460;
			//rptMeterReadingList.WindowState	= 2;
			//rptMeterReadingList.SectionData	= "rptMeterReadingList.dsx":058A;
			//End Unmaped Properties
		}
	}
}
