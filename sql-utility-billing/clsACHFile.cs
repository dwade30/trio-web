//Fecher vbPorter - Version 1.0.0.90
using System;
using System.Collections.Generic;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class clsACHFile
	{

		//=========================================================

		private clsACHFileInfo tFileInfo = new/*AsNew*/ clsACHFileInfo();
		private clsACHFileHeaderRecord tFileHeader = new/*AsNew*/ clsACHFileHeaderRecord();
		private clsACHFileControlRecord tfilecontrol = new/*AsNew*/ clsACHFileControlRecord();
		private List<clsACHBatch> tBatches = new List<clsACHBatch>();
		private List<clsACHBlockFiller> tFillers = new List<clsACHBlockFiller>();

		public clsACHFileHeaderRecord FileHeader
        {
            get { return tFileHeader; }
        }
		

		public clsACHFileControlRecord FileControl
        {
            get { return tfilecontrol; }
        }
		
		public clsACHFileInfo FileInfo
		{
            get { return tFileInfo; }
        }

		public IEnumerable<clsACHBatch> Batches
		{
            get { return tBatches; }
        }

		public IEnumerable<clsACHBlockFiller> Fillers()
		{
			return tFillers;
		}

		public void AddBatch(ref clsACHBatch tBatch)
		{
			tBatches.Add(tBatch);
			tBatch.BatchNumber = tBatches.Count;
		}

		public void CreateFillers(int intNumFillers)
		{
			int X;
			tFillers.Clear();
			clsACHBlockFiller tBlockFill;

			for(X=1; X<=intNumFillers; X++) {
				tBlockFill = new clsACHBlockFiller();
				tFillers.Add(tBlockFill);
			} // X
		}

		public void BuildFileControl()
		{
			double dblDebits = 0;
			double dblCredits = 0;
			double dblHash = 0;
			int intEntries = 0;
			int intBlocks;
			int intBlockRecs = 0;

			foreach (clsACHBatch tBatch in tBatches) {
				dblDebits += tBatch.Debits;
				dblCredits += tBatch.Credits;
				dblHash += tBatch.Hash;
				intEntries += tBatch.BatchControl.EntryAddendaCount;
				intBlockRecs += tBatch.BlockRecords;
			} // tBatch
			tfilecontrol.TotalCredit = dblCredits;
			tfilecontrol.TotalDebit = dblDebits;
			tfilecontrol.Hash = dblHash;
			tfilecontrol.BatchCount = tBatches.Count;
			tfilecontrol.EntryAddendaCount = intEntries;
			intBlockRecs += 2; // file header and control
			intBlocks = Convert.ToInt16(intBlockRecs / 10.0);
			if (intBlockRecs % 10>0) {
				intBlocks += 1;
			}
			tfilecontrol.BlockCount = intBlocks;
			CreateFillers((10-(intBlockRecs % 10)) % 10);
		}

	}
}
