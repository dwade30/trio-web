//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;


namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTOutstandingBalancesAll.
	/// </summary>
	public class rptUTOutstandingBalancesAll : fecherFoundation.FCForm
	{

// nObj = 1
//   0	rptUTOutstandingBalancesAll	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}


		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               12/02/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/02/2004              *
		// ********************************************************
		public double dblTotalsPay;
		public double dblTotalsPrin;
		public int lngCount;
		public int intSuppReportType;
		bool boolStarted;
		bool boolSummaryOnly;

		private void ActiveReport_FetchData(ref bool EOF)
		{
			EOF = boolStarted;
		}

		private void ActiveReport_KeyDown(ref short KeyCode, int Shift)
		{
			switch (KeyCode) {
				
				case Keys.Escape:
				{
					this.Unload();
					break;
				}
			} //end switch
		}

		private void ActiveReport_PageEnd()
		{
			FCGlobal.Screen.MousePointer = 1;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_PageStart()
		{
			lblPage.Caption = "Page "+this.pageNumber;
		}

		private void ActiveReport_ReportEnd()
		{
			FCGlobal.Screen.MousePointer = 1;
			frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart()
		{
			lblDate.Caption = Strings.Format(DateTime.Today, "MM/DD/YYYY"); // fill in the page header
			lblTime.Caption = Strings.Format(DateAndTime.TimeOfDay, "HH:MM AMPM");
			lblMuniName.Caption = modGlobalConstants.MuniName;

			boolSummaryOnly = FCConvert.CBool(frmUTStatusList.InstancePtr.chkSummaryOnly.CheckState==Wisej.Web.CheckState.Checked);

			SetReportHeader(); // this will find which criteria is used and create the page header for the report

			frmWait.InstancePtr.Init("Please Wait..."+"\r\n"+"Loading");

			lngCount = 0; // reset the counters to 0
			dblTotalsPay = 0;
			dblTotalsPrin = 0;

			modMain.boolSubReport = true; // this tells the reports that are going to be used as a sub report,
			// to send the data that they have back to the parent form
			modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID); // sets the size of the report

			boolStarted = false; // defaults the started boolean

			sarOB1.Width = this.PrintWidth; // set the sizes of the sub reports
			sarOb2.Width = this.PrintWidth;
			// point the sub report objects to the correct reports
			switch (frmUTStatusList.InstancePtr.intMasterReportType) {
				
				case 0:
				{
					// outstanding balance
					sarOB1.Object = new rptUTOutstandingBalances();
					sarOb2.Object = new rptUTOutstandingLienBalances();
					lblHeader.Caption = "Outstanding Balance Report";
					break;
				}
				case 4:
				{
					// zero balance
					sarOB1.Object = new rptUTZeroBalances();
					sarOb2.Object = new rptUTZeroLienBalances();
					lblHeader.Caption = "Zero Balance Report";
					break;
				}
				case 5:
				{
					// negative balance
					sarOB1.Object = new rptUTNegativeBalances();
					sarOb2.Object = new rptUTNegativeLienBalances();
					lblHeader.Caption = "Negative Balance Report";
					break;
				}
				case 6:
				{
					// supplemental balances
					intSuppReportType = 1;
					sarOB1.Object = new rptUTOutstandingBalances();
					sarOb2.Object = new rptUTOutstandingLienBalances();
					lblHeader.Caption = "Supplemental Balance Report";
					break;
				}
				case 8:
				{
					// zero balance
					intSuppReportType = 1;
					sarOB1.Object = new rptUTZeroBalances();
					sarOb2.Object = new rptUTZeroLienBalances();
					lblHeader.Caption = "Zero Balance Report";
					break;
				}
				case 7:
				{
					// negative balance
					intSuppReportType = 1;
					sarOB1.Object = new rptUTNegativeBalances();
					sarOb2.Object = new rptUTNegativeLienBalances();
					lblHeader.Caption = "Negative Balance Report";
					break;
				}
			} //end switch
		}

		private void ActiveReport_Terminate()
		{
			FCGlobal.Screen.MousePointer = 1;
			modMain.boolSubReport = false;
		}

		private void Detail_Format()
		{
			// BindFields
			boolStarted = true;
		}

		private void SetupTotals()
		{
			// this sub will fill in the totals line at the bottom of the report
			fldTotalTaxDue.Text = Strings.Format(dblTotalsPrin, "#,##0.00");
			fldTotalPaymentReceived.Text = Strings.Format(dblTotalsPay, "#,##0.00");
			fldTotalDue.Text = Strings.Format(Convert.ToDouble(fldTotalTaxDue.Text)-Convert.ToDouble(fldTotalPaymentReceived.Text), "#,##0.00");

			if (lngCount>1) { // this just shows the correct phrases
				lblTotals.Caption = "Total for "+Convert.ToString(lngCount)+" Accounts:";
			} else if (lngCount==1) {
				lblTotals.Caption = "Total for "+Convert.ToString(lngCount)+" Account:";
			} else {
				lblTotals.Caption = "No Accounts Processed";
			}
		}

		private void ReportFooter_Format()
		{
			SetupTotals();
		}

		private void SetReportHeader()
		{
			int intCT;
			string strTemp = "";


			for(intCT=0; intCT<=frmUTStatusList.InstancePtr.vsWhere.Rows-2; intCT++) {
				if (frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)!="" || frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2)!="") {
					switch (intCT) {
						
						case 0:
						{
							// Account Number
							if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2))!="") {
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2))!="") {
									if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1))==Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2))) {
										strTemp += "Account: "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)+" ";
									} else {
										strTemp += "Account: "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)+" To "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
									}
								} else {
									strTemp += "Below Account: "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
								}
							} else {
								strTemp += "Above Account: "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
							}
							break;
						}
						case 2:
						{
							// Tax Year
							if (Strings.Trim(strTemp)!="") {
								strTemp += ";";
							}
							if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1))!="") {
								if (Strings.Trim(frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2))!="") {
									strTemp += " Tax Year: "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1)+" To "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
								} else {
									strTemp += " Tax Year: "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 1);
								}
							} else {
								strTemp += " Tax Year: "+frmUTStatusList.InstancePtr.vsWhere.TextMatrix(intCT, 2);
							}
							break;
						}
						default: {
							break;
							break;
						}
					} //end switch
				}
			}


			if (boolSummaryOnly) {
				lblAccount.Visible = false;
				lblName.Visible = false;
				lblYear.Visible = false;
			}

			if (Strings.Trim(strTemp)=="") {
				lblReportType.Caption = "Complete List"; // & vbCrLf & "Lien Accounts"
			} else {
				lblReportType.Caption = strTemp; // & vbCrLf & "Lien Accounts"
			}
			if (modMain.gboolUTUseAsOfDate) {
				lblReportType.Caption = lblReportType.Caption+"\r\n"+"As of: "+Strings.Format(modMain.gdtUTStatusListAsOfDate, "MM/DD/YYYY");
			}
		}

		private void rptUTOutstandingBalancesAll_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUTOutstandingBalancesAll properties;
			//rptUTOutstandingBalancesAll.Caption	= "Outstanding Balance Report";
			//rptUTOutstandingBalancesAll.Icon	= "rptUTOutstandingBalanceAll.dsx":0000";
			//rptUTOutstandingBalancesAll.Left	= 0;
			//rptUTOutstandingBalancesAll.Top	= 0;
			//rptUTOutstandingBalancesAll.Width	= 11880;
			//rptUTOutstandingBalancesAll.Height	= 8595;
			//rptUTOutstandingBalancesAll.StartUpPosition	= 3;
			//rptUTOutstandingBalancesAll.SectionData	= "rptUTOutstandingBalanceAll.dsx":058A;
			//End Unmaped Properties
		}
	}
}