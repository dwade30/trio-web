﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmBillInvCldExpOptions.
	/// </summary>
	partial class frmBillInvCldExpOptions : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWaterSewer;
		public fecherFoundation.FCLabel lblWaterSewer;
		public fecherFoundation.FCComboBox cmbRangeOf;
		public fecherFoundation.FCLabel lblRangeOf;
		public fecherFoundation.FCLabel lblRange;
		public fecherFoundation.FCComboBox cmbRange;
		public fecherFoundation.FCComboBox cmbOrder;
		public fecherFoundation.FCLabel lblOrder;
		public fecherFoundation.FCCheckBox chkExcludeEmail;
		public fecherFoundation.FCCheckBox chkExcludeZero;
		public fecherFoundation.FCFrame framIndividual;
		public FCGrid GridIndividual;
		public fecherFoundation.FCFrame framRange;
		public fecherFoundation.FCTextBox txtEnd;
		public fecherFoundation.FCTextBox txtStart;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCButton cmdSaveContinue;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle3 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle4 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBillInvCldExpOptions));
			this.cmbWaterSewer = new fecherFoundation.FCComboBox();
			this.lblWaterSewer = new fecherFoundation.FCLabel();
			this.cmbRangeOf = new fecherFoundation.FCComboBox();
			this.lblRangeOf = new fecherFoundation.FCLabel();
			this.cmbRange = new fecherFoundation.FCComboBox();
			this.lblRange = new fecherFoundation.FCLabel();
			this.cmbOrder = new fecherFoundation.FCComboBox();
			this.lblOrder = new fecherFoundation.FCLabel();
			this.chkExcludeEmail = new fecherFoundation.FCCheckBox();
			this.chkExcludeZero = new fecherFoundation.FCCheckBox();
			this.framIndividual = new fecherFoundation.FCFrame();
			this.GridIndividual = new fecherFoundation.FCGrid();
			this.framRange = new fecherFoundation.FCFrame();
			this.txtEnd = new fecherFoundation.FCTextBox();
			this.txtStart = new fecherFoundation.FCTextBox();
			this.Label1 = new fecherFoundation.FCLabel();
			this.cmdSaveContinue = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkExcludeEmail)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.chkExcludeZero)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framIndividual)).BeginInit();
			this.framIndividual.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.GridIndividual)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.framRange)).BeginInit();
			this.framRange.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdSaveContinue);
			this.BottomPanel.Location = new System.Drawing.Point(0, 444);
			this.BottomPanel.Size = new System.Drawing.Size(617, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.chkExcludeEmail);
			this.ClientArea.Controls.Add(this.chkExcludeZero);
			this.ClientArea.Controls.Add(this.cmbWaterSewer);
			this.ClientArea.Controls.Add(this.lblWaterSewer);
			this.ClientArea.Controls.Add(this.cmbRange);
			this.ClientArea.Controls.Add(this.lblRange);
			this.ClientArea.Controls.Add(this.cmbOrder);
			this.ClientArea.Controls.Add(this.lblOrder);
			this.ClientArea.Controls.Add(this.framRange);
			this.ClientArea.Controls.Add(this.framIndividual);
			this.ClientArea.Size = new System.Drawing.Size(617, 384);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(617, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(115, 30);
			this.HeaderText.Text = "Print Bills";
			// 
			// cmbWaterSewer
			// 
			this.cmbWaterSewer.AutoSize = false;
			this.cmbWaterSewer.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWaterSewer.FormattingEnabled = true;
			this.cmbWaterSewer.Items.AddRange(new object[] {
				"Water",
				"Sewer",
				"Both"
			});
			this.cmbWaterSewer.Location = new System.Drawing.Point(440, 30);
			this.cmbWaterSewer.Name = "cmbWaterSewer";
			this.cmbWaterSewer.Size = new System.Drawing.Size(150, 40);
			this.cmbWaterSewer.TabIndex = 22;
			this.cmbWaterSewer.Text = "Both";
			// 
			// lblWaterSewer
			// 
			this.lblWaterSewer.AutoSize = true;
			this.lblWaterSewer.Location = new System.Drawing.Point(322, 44);
			this.lblWaterSewer.Name = "lblWaterSewer";
			this.lblWaterSewer.Size = new System.Drawing.Size(62, 15);
			this.lblWaterSewer.TabIndex = 23;
			this.lblWaterSewer.Text = "INCLUDE";
			this.lblWaterSewer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbRangeOf
			// 
			this.cmbRangeOf.AutoSize = false;
			this.cmbRangeOf.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRangeOf.FormattingEnabled = true;
			this.cmbRangeOf.Items.AddRange(new object[] {
				"Name",
				"Account"
			});
			this.cmbRangeOf.Location = new System.Drawing.Point(156, 17);
			this.cmbRangeOf.Name = "cmbRangeOf";
			this.cmbRangeOf.Size = new System.Drawing.Size(150, 40);
			this.cmbRangeOf.TabIndex = 7;
			this.cmbRangeOf.Text = "Name";
			this.cmbRangeOf.SelectedIndexChanged += new System.EventHandler(this.optRangeOf_CheckedChanged);
			// 
			// lblRangeOf
			// 
			this.lblRangeOf.AutoSize = true;
			this.lblRangeOf.Location = new System.Drawing.Point(30, 31);
			this.lblRangeOf.Name = "lblRangeOf";
			this.lblRangeOf.Size = new System.Drawing.Size(72, 15);
			this.lblRangeOf.TabIndex = 8;
			this.lblRangeOf.Text = "RANGE OF";
			this.lblRangeOf.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbRange
			// 
			this.cmbRange.AutoSize = false;
			this.cmbRange.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbRange.FormattingEnabled = true;
			this.cmbRange.Items.AddRange(new object[] {
				"All",
				"Range",
				"Individual"
			});
			this.cmbRange.Location = new System.Drawing.Point(156, 30);
			this.cmbRange.Name = "cmbRange";
			this.cmbRange.Size = new System.Drawing.Size(150, 40);
			this.cmbRange.TabIndex = 24;
			this.cmbRange.Text = "All";
			this.cmbRange.SelectedIndexChanged += new System.EventHandler(this.optRange_CheckedChanged);
			// 
			// lblRange
			// 
			this.lblRange.AutoSize = true;
			this.lblRange.Location = new System.Drawing.Point(30, 44);
			this.lblRange.Name = "lblRange";
			this.lblRange.Size = new System.Drawing.Size(58, 15);
			this.lblRange.TabIndex = 25;
			this.lblRange.Text = "EXPORT";
			this.lblRange.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// cmbOrder
			// 
			this.cmbOrder.AutoSize = false;
			this.cmbOrder.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbOrder.FormattingEnabled = true;
			this.cmbOrder.Items.AddRange(new object[] {
				"Name",
				"Account",
				"Sequence"
			});
			this.cmbOrder.Location = new System.Drawing.Point(156, 90);
			this.cmbOrder.Name = "cmbOrder";
			this.cmbOrder.Size = new System.Drawing.Size(150, 40);
			this.cmbOrder.TabIndex = 26;
			this.cmbOrder.Text = "Name";
			// 
			// lblOrder
			// 
			this.lblOrder.AutoSize = true;
			this.lblOrder.Location = new System.Drawing.Point(30, 104);
			this.lblOrder.Name = "lblOrder";
			this.lblOrder.Size = new System.Drawing.Size(72, 15);
			this.lblOrder.TabIndex = 27;
			this.lblOrder.Text = "ORDER BY";
			this.lblOrder.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// chkExcludeEmail
			// 
			this.chkExcludeEmail.Location = new System.Drawing.Point(30, 313);
			this.chkExcludeEmail.Name = "chkExcludeEmail";
			this.chkExcludeEmail.Size = new System.Drawing.Size(379, 27);
			this.chkExcludeEmail.TabIndex = 21;
			this.chkExcludeEmail.Text = "Exclude bills with Send Bills by E-Mail option set";
			this.chkExcludeEmail.Visible = false;
			// 
			// chkExcludeZero
			// 
			this.chkExcludeZero.Location = new System.Drawing.Point(30, 277);
			this.chkExcludeZero.Name = "chkExcludeZero";
			this.chkExcludeZero.Size = new System.Drawing.Size(322, 27);
			this.chkExcludeZero.TabIndex = 20;
			this.chkExcludeZero.Text = "Exclude bills with negative or 0 balances";
			// 
			// framIndividual
			// 
			this.framIndividual.AppearanceKey = "groupBoxNoBorders";
			this.framIndividual.BackColor = System.Drawing.Color.White;
			this.framIndividual.Controls.Add(this.GridIndividual);
			this.framIndividual.Location = new System.Drawing.Point(0, 138);
			this.framIndividual.Name = "framIndividual";
			this.framIndividual.Size = new System.Drawing.Size(615, 133);
			this.framIndividual.TabIndex = 13;
			this.framIndividual.Visible = false;
			// 
			// GridIndividual
			// 
			this.GridIndividual.AllowSelection = false;
			this.GridIndividual.AllowUserToResizeColumns = false;
			this.GridIndividual.AllowUserToResizeRows = false;
			this.GridIndividual.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.GridIndividual.BackColorAlternate = System.Drawing.Color.Empty;
			this.GridIndividual.BackColorBkg = System.Drawing.Color.Empty;
			this.GridIndividual.BackColorFixed = System.Drawing.Color.Empty;
			this.GridIndividual.BackColorSel = System.Drawing.Color.Empty;
			this.GridIndividual.Cols = 2;
			dataGridViewCellStyle3.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.GridIndividual.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this.GridIndividual.ColumnHeadersHeight = 30;
			this.GridIndividual.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle4.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.GridIndividual.DefaultCellStyle = dataGridViewCellStyle4;
			this.GridIndividual.DragIcon = null;
			this.GridIndividual.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.GridIndividual.ExtendLastCol = true;
			this.GridIndividual.FixedCols = 0;
			this.GridIndividual.ForeColorFixed = System.Drawing.Color.Empty;
			this.GridIndividual.FrozenCols = 0;
			this.GridIndividual.GridColor = System.Drawing.Color.Empty;
			this.GridIndividual.GridColorFixed = System.Drawing.Color.Empty;
			this.GridIndividual.Location = new System.Drawing.Point(30, 13);
			this.GridIndividual.Name = "GridIndividual";
			this.GridIndividual.ReadOnly = true;
			this.GridIndividual.RowHeadersVisible = false;
			this.GridIndividual.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.GridIndividual.RowHeightMin = 0;
			this.GridIndividual.Rows = 2;
			this.GridIndividual.ScrollTipText = null;
			this.GridIndividual.ShowColumnVisibilityMenu = false;
			this.GridIndividual.Size = new System.Drawing.Size(560, 113);
			this.GridIndividual.StandardTab = true;
			this.GridIndividual.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.GridIndividual.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.GridIndividual.TabIndex = 7;
			this.GridIndividual.KeyDownEdit += new Wisej.Web.KeyEventHandler(this.GridIndividual_KeyDownEdit);
			this.GridIndividual.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.GridIndividual_AfterEdit);
			this.GridIndividual.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.GridIndividual_ValidateEdit);
			this.GridIndividual.KeyDown += new Wisej.Web.KeyEventHandler(this.GridIndividual_KeyDownEvent);
			// 
			// framRange
			// 
			this.framRange.AppearanceKey = "groupBoxNoBorders";
			this.framRange.BackColor = System.Drawing.Color.White;
			this.framRange.Controls.Add(this.txtEnd);
			this.framRange.Controls.Add(this.cmbRangeOf);
			this.framRange.Controls.Add(this.lblRangeOf);
			this.framRange.Controls.Add(this.txtStart);
			this.framRange.Controls.Add(this.Label1);
			this.framRange.Location = new System.Drawing.Point(0, 132);
			this.framRange.Name = "framRange";
			this.framRange.Size = new System.Drawing.Size(615, 137);
			this.framRange.TabIndex = 12;
			this.framRange.Visible = false;
			// 
			// txtEnd
			// 
			this.txtEnd.AutoSize = false;
			this.txtEnd.BackColor = System.Drawing.SystemColors.Window;
			this.txtEnd.LinkItem = null;
			this.txtEnd.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtEnd.LinkTopic = null;
			this.txtEnd.Location = new System.Drawing.Point(255, 77);
			this.txtEnd.Name = "txtEnd";
			this.txtEnd.Size = new System.Drawing.Size(171, 40);
			this.txtEnd.TabIndex = 6;
			this.txtEnd.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtEnd_KeyPress);
			// 
			// txtStart
			// 
			this.txtStart.AutoSize = false;
			this.txtStart.BackColor = System.Drawing.SystemColors.Window;
			this.txtStart.LinkItem = null;
			this.txtStart.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtStart.LinkTopic = null;
			this.txtStart.Location = new System.Drawing.Point(30, 77);
			this.txtStart.Name = "txtStart";
			this.txtStart.Size = new System.Drawing.Size(171, 40);
			this.txtStart.TabIndex = 5;
			this.txtStart.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtStart_KeyPress);
			// 
			// Label1
			// 
			this.Label1.Location = new System.Drawing.Point(203, 89);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(50, 19);
			this.Label1.TabIndex = 14;
			this.Label1.Text = "TO";
			this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// cmdSaveContinue
			// 
			this.cmdSaveContinue.AppearanceKey = "acceptButton";
			this.cmdSaveContinue.Location = new System.Drawing.Point(200, 30);
			this.cmdSaveContinue.Name = "cmdSaveContinue";
			this.cmdSaveContinue.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSaveContinue.Size = new System.Drawing.Size(184, 48);
			this.cmdSaveContinue.TabIndex = 0;
			this.cmdSaveContinue.Text = "Save & Continue";
			this.cmdSaveContinue.Click += new System.EventHandler(this.mnuSaveContinue_Click);
			// 
			// frmBillInvCldExpOptions
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(617, 552);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmBillInvCldExpOptions";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Print Bills";
			this.Load += new System.EventHandler(this.frmBillInvCldExpOptions_Load);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmBillInvCldExpOptions_KeyDown);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.chkExcludeEmail)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.chkExcludeZero)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framIndividual)).EndInit();
			this.framIndividual.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.GridIndividual)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.framRange)).EndInit();
			this.framRange.ResumeLayout(false);
			this.framRange.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSaveContinue)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
	}
}
