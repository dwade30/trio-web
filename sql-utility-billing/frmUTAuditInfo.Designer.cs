﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTAuditInfo.
	/// </summary>
	partial class frmUTAuditInfo : BaseForm
	{
		public fecherFoundation.FCComboBox cmbRateRecType;
		public fecherFoundation.FCLabel lblRateRecType;
		public fecherFoundation.FCTextBox txtSIntRate;
		public Global.T2KDateBox t2kBilldate;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCTextBox txtWIntRate;
		public Global.T2KDateBox txtEndDate;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtIntDate;
		public Global.T2KDateBox txtDueDate;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblIntDateTitle;
		public fecherFoundation.FCLabel lblEndDateTitle;
		public fecherFoundation.FCLabel lblStartDateTitle;
		public fecherFoundation.FCLabel lblSIntRateTitle;
		public fecherFoundation.FCLabel Label24;
		public fecherFoundation.FCLabel lblWIntRateTitle;
		public fecherFoundation.FCLabel lblBillDate;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSepar;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTAuditInfo));
            this.cmbRateRecType = new fecherFoundation.FCComboBox();
            this.lblRateRecType = new fecherFoundation.FCLabel();
            this.txtSIntRate = new fecherFoundation.FCTextBox();
            this.t2kBilldate = new Global.T2KDateBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.txtWIntRate = new fecherFoundation.FCTextBox();
            this.txtEndDate = new Global.T2KDateBox();
            this.txtStartDate = new Global.T2KDateBox();
            this.txtIntDate = new Global.T2KDateBox();
            this.txtDueDate = new Global.T2KDateBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.lblIntDateTitle = new fecherFoundation.FCLabel();
            this.lblEndDateTitle = new fecherFoundation.FCLabel();
            this.lblStartDateTitle = new fecherFoundation.FCLabel();
            this.lblSIntRateTitle = new fecherFoundation.FCLabel();
            this.Label24 = new fecherFoundation.FCLabel();
            this.lblWIntRateTitle = new fecherFoundation.FCLabel();
            this.lblBillDate = new fecherFoundation.FCLabel();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSepar = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kBilldate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIntDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 515);
            this.BottomPanel.Size = new System.Drawing.Size(744, 108);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.txtSIntRate);
            this.ClientArea.Controls.Add(this.cmbRateRecType);
            this.ClientArea.Controls.Add(this.lblRateRecType);
            this.ClientArea.Controls.Add(this.t2kBilldate);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.txtWIntRate);
            this.ClientArea.Controls.Add(this.txtEndDate);
            this.ClientArea.Controls.Add(this.txtStartDate);
            this.ClientArea.Controls.Add(this.txtIntDate);
            this.ClientArea.Controls.Add(this.txtDueDate);
            this.ClientArea.Controls.Add(this.Label3);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.Label1);
            this.ClientArea.Controls.Add(this.lblIntDateTitle);
            this.ClientArea.Controls.Add(this.lblEndDateTitle);
            this.ClientArea.Controls.Add(this.lblStartDateTitle);
            this.ClientArea.Controls.Add(this.lblSIntRateTitle);
            this.ClientArea.Controls.Add(this.Label24);
            this.ClientArea.Controls.Add(this.lblWIntRateTitle);
            this.ClientArea.Controls.Add(this.lblBillDate);
            this.ClientArea.Size = new System.Drawing.Size(744, 455);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(744, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(221, 30);
            this.HeaderText.Text = "Add a Rate Record";
            // 
            // cmbRateRecType
            // 
            this.cmbRateRecType.Items.AddRange(new object[] {
            "Regular",
            "Lien",
            "Supplemental"});
            this.cmbRateRecType.Location = new System.Drawing.Point(220, 30);
            this.cmbRateRecType.Name = "cmbRateRecType";
            this.cmbRateRecType.Size = new System.Drawing.Size(246, 40);
            this.cmbRateRecType.TabIndex = 1;
            this.cmbRateRecType.Visible = false;
            // 
            // lblRateRecType
            // 
            this.lblRateRecType.AutoSize = true;
            this.lblRateRecType.Location = new System.Drawing.Point(30, 44);
            this.lblRateRecType.Name = "lblRateRecType";
            this.lblRateRecType.Size = new System.Drawing.Size(133, 15);
            this.lblRateRecType.Text = "RATE RECORD TYPE";
            this.lblRateRecType.Visible = false;
            // 
            // txtSIntRate
            // 
            this.txtSIntRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtSIntRate.Location = new System.Drawing.Point(220, 210);
            this.txtSIntRate.Name = "txtSIntRate";
            this.txtSIntRate.Size = new System.Drawing.Size(106, 40);
            this.txtSIntRate.TabIndex = 8;
            this.txtSIntRate.Enter += new System.EventHandler(this.txtSIntRate_Enter);
            this.txtSIntRate.TextChanged += new System.EventHandler(this.txtSIntRate_TextChanged);
            this.txtSIntRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtSIntRate_KeyPress);
            // 
            // t2kBilldate
            // 
            this.t2kBilldate.Location = new System.Drawing.Point(561, 150);
            this.t2kBilldate.Mask = "##/##/####";
            this.t2kBilldate.MaxLength = 10;
            this.t2kBilldate.Name = "t2kBilldate";
            this.t2kBilldate.Size = new System.Drawing.Size(115, 40);
            this.t2kBilldate.TabIndex = 11;
            this.t2kBilldate.TextChanged += new System.EventHandler(this.t2kBilldate_Change);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(220, 90);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(458, 40);
            this.txtDescription.TabIndex = 3;
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // txtWIntRate
            // 
            this.txtWIntRate.BackColor = System.Drawing.SystemColors.Window;
            this.txtWIntRate.Location = new System.Drawing.Point(220, 150);
            this.txtWIntRate.Name = "txtWIntRate";
            this.txtWIntRate.Size = new System.Drawing.Size(106, 40);
            this.txtWIntRate.TabIndex = 5;
            this.txtWIntRate.Enter += new System.EventHandler(this.txtWIntRate_Enter);
            this.txtWIntRate.TextChanged += new System.EventHandler(this.txtWIntRate_TextChanged);
            this.txtWIntRate.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtWIntRate_KeyPress);
            // 
            // txtEndDate
            // 
            this.txtEndDate.Location = new System.Drawing.Point(561, 270);
            this.txtEndDate.Mask = "##/##/####";
            this.txtEndDate.MaxLength = 10;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(115, 40);
            this.txtEndDate.TabIndex = 15;
            this.txtEndDate.TextChanged += new System.EventHandler(this.txtEndDate_Change);
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(561, 210);
            this.txtStartDate.Mask = "##/##/####";
            this.txtStartDate.MaxLength = 10;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(115, 40);
            this.txtStartDate.TabIndex = 13;
            this.txtStartDate.TextChanged += new System.EventHandler(this.txtStartDate_Change);
            // 
            // txtIntDate
            // 
            this.txtIntDate.Location = new System.Drawing.Point(561, 330);
            this.txtIntDate.Mask = "##/##/####";
            this.txtIntDate.MaxLength = 10;
            this.txtIntDate.Name = "txtIntDate";
            this.txtIntDate.Size = new System.Drawing.Size(115, 40);
            this.txtIntDate.TabIndex = 17;
            this.txtIntDate.TextChanged += new System.EventHandler(this.txtIntDate_Change);
            // 
            // txtDueDate
            // 
            this.txtDueDate.Location = new System.Drawing.Point(561, 390);
            this.txtDueDate.Mask = "##/##/####";
            this.txtDueDate.MaxLength = 10;
            this.txtDueDate.Name = "txtDueDate";
            this.txtDueDate.Size = new System.Drawing.Size(115, 40);
            this.txtDueDate.TabIndex = 19;
            this.txtDueDate.TextChanged += new System.EventHandler(this.txtDueDate_Change);
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(411, 404);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(124, 25);
            this.Label3.TabIndex = 18;
            this.Label3.Text = "DUE DATE";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(332, 224);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(13, 25);
            this.Label2.TabIndex = 9;
            this.Label2.Text = "%";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(332, 164);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(13, 25);
            this.Label1.TabIndex = 6;
            this.Label1.Text = "%";
            // 
            // lblIntDateTitle
            // 
            this.lblIntDateTitle.Location = new System.Drawing.Point(411, 344);
            this.lblIntDateTitle.Name = "lblIntDateTitle";
            this.lblIntDateTitle.Size = new System.Drawing.Size(124, 25);
            this.lblIntDateTitle.TabIndex = 16;
            this.lblIntDateTitle.Text = "INTEREST DATE";
            // 
            // lblEndDateTitle
            // 
            this.lblEndDateTitle.Location = new System.Drawing.Point(411, 284);
            this.lblEndDateTitle.Name = "lblEndDateTitle";
            this.lblEndDateTitle.Size = new System.Drawing.Size(124, 25);
            this.lblEndDateTitle.TabIndex = 14;
            this.lblEndDateTitle.Text = "END DATE";
            // 
            // lblStartDateTitle
            // 
            this.lblStartDateTitle.Location = new System.Drawing.Point(411, 224);
            this.lblStartDateTitle.Name = "lblStartDateTitle";
            this.lblStartDateTitle.Size = new System.Drawing.Size(124, 25);
            this.lblStartDateTitle.TabIndex = 12;
            this.lblStartDateTitle.Text = "START DATE";
            // 
            // lblSIntRateTitle
            // 
            this.lblSIntRateTitle.Location = new System.Drawing.Point(30, 224);
            this.lblSIntRateTitle.Name = "lblSIntRateTitle";
            this.lblSIntRateTitle.Size = new System.Drawing.Size(139, 25);
            this.lblSIntRateTitle.TabIndex = 7;
            this.lblSIntRateTitle.Text = "SEWER INTEREST RATE";
            // 
            // Label24
            // 
            this.Label24.Location = new System.Drawing.Point(30, 104);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(165, 20);
            this.Label24.TabIndex = 2;
            this.Label24.Text = "DESCRIPTION / TITLE";
            // 
            // lblWIntRateTitle
            // 
            this.lblWIntRateTitle.Location = new System.Drawing.Point(30, 164);
            this.lblWIntRateTitle.Name = "lblWIntRateTitle";
            this.lblWIntRateTitle.Size = new System.Drawing.Size(163, 25);
            this.lblWIntRateTitle.TabIndex = 4;
            this.lblWIntRateTitle.Text = "WATER INTEREST RATE";
            // 
            // lblBillDate
            // 
            this.lblBillDate.Location = new System.Drawing.Point(411, 164);
            this.lblBillDate.Name = "lblBillDate";
            this.lblBillDate.Size = new System.Drawing.Size(124, 25);
            this.lblBillDate.TabIndex = 10;
            this.lblBillDate.Text = "BILL DATE";
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileSave,
            this.mnuFileSaveExit,
            this.mnuSepar,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 0;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFileSave.Text = "Save";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSaveExit
            // 
            this.mnuFileSaveExit.Index = 1;
            this.mnuFileSaveExit.Name = "mnuFileSaveExit";
            this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSaveExit.Text = "Save & Exit";
            this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
            // 
            // mnuSepar
            // 
            this.mnuSepar.Index = 2;
            this.mnuSepar.Name = "mnuSepar";
            this.mnuSepar.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 3;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(298, 30);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(100, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // frmUTAuditInfo
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(744, 623);
            this.FillColor = 0;
            this.KeyPreview = true;
            this.Name = "frmUTAuditInfo";
            this.StartPosition = Wisej.Web.FormStartPosition.CenterParent;
            this.Text = "Add A Rate Record";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmUTAuditInfo_Load);
            this.Activated += new System.EventHandler(this.frmUTAuditInfo_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmUTAuditInfo_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTAuditInfo_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.t2kBilldate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtIntDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDueDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
	}
}
