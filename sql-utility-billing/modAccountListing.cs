﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	public class modAccountListing
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               02/09/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/14/2005              *
		// ********************************************************
		// all of the other functions that are used for frmAccountListing are in
		// modUTStatusLists because they are shared with frmAccountListings
		public static void SetupAccountListingCombos_2(bool boolRegular)
		{
			SetupAccountListingCombos(boolRegular);
		}

		public static void SetupAccountListingCombos(bool boolRegular = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsTemp = new clsDRWrapper();
				string strRT;
				string strSize;
				string strCat;
				// this will reset the arrays
				modUTStatusList.ClearComboListArray();
				strSize = "";
				strRT = "";
				strCat = "";
				// fill the meter size string
				rsTemp.OpenRecordset("SELECT * FROM MeterSizes ORDER BY Code", modExtraModules.strUTDatabase);
				while (!rsTemp.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					strSize += "#" + rsTemp.Get_Fields("Code") + ";" + modMain.PadToString_8(rsTemp.Get_Fields("Code"), 5) + "\t" + rsTemp.Get_Fields_String("ShortDescription") + "|";
					rsTemp.MoveNext();
				}
				// fill the rate table string
				rsTemp.OpenRecordset("SELECT * FROM RateTable ORDER BY RateTableNumber", modExtraModules.strUTDatabase);
				while (!rsTemp.EndOfFile())
				{
					strRT += "#" + rsTemp.Get_Fields_Int32("RateTableNumber") + ";" + modMain.PadToString_8(rsTemp.Get_Fields_Int32("RateTableNumber"), 5) + "\t" + "  " + rsTemp.Get_Fields_String("RateTableDescription") + "|";
					rsTemp.MoveNext();
				}
				// fill the meter size string
				rsTemp.OpenRecordset("SELECT * FROM Category ORDER BY Code", modExtraModules.strUTDatabase);
				while (!rsTemp.EndOfFile())
				{
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					strCat += "#" + rsTemp.Get_Fields("Code") + ";" + modMain.PadToString_8(rsTemp.Get_Fields("Code"), 5) + "\t" + rsTemp.Get_Fields_String("ShortDescription") + "|";
					rsTemp.MoveNext();
				}
				// fill them correctly
				// lngRowWS
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowWS, 0] = "#0;Water|#1;Sewer|#2;Both|#3;Water Only|#4;Sewer Only";
				// lngRowBillMessage = 4
				// lngRowBillType = 5
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowBillType, 0] = "#0;All Types|#1;Consumption|#2;Flat|#3;Units|#4;Adjustment";
				// lngRowBillToSameAsOwner = 6
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowBillToSameAsOwner, 0] = "#0;Yes|#1;No";
				// lngRowCombinationType = 7
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowCombinationType, 0] = "#0;Not Combined|#1;Consumption|#2;Bill";
				// lngRowDataEntryMessage = 8
				// lngRowMeterFinalBill = 9
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowMeterFinalBill, 0] = "#0;Final Bill|#1;Active|#2;Both";
				// lngRowAcctFinalBill = 10
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowAcctFinalBill, 0] = "#0;Final Bill|#1;Active|#2;Both";
				// lngRowMeterSize = 11
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowMeterSize, 0] = strSize;
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowMeterSize, 1] = strSize;
				// lngRowMeterNoBill = 12
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowMeterNoBill, 0] = "#0;No Bill|#1;Active|#2;Both";
				// lngRowAcctNoBill = 13
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowAcctNoBill, 0] = "#0;No Bill|#1;Active|#2;Both";
				// lngRowREAccount = 14
				// lngRowMasterOverrideS = 15
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowMasterOverrideS, 0] = "#0;>|#1;<|#2;=|#3;<>";
				// lngRowRateTableS = 16
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowRateTableS, 0] = strRT;
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowRateTableS, 1] = strRT;
				// lngRowCategoryS = 17
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowCategoryS, 0] = strCat;
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowCategoryS, 1] = strCat;
				// lngRowMasterOverrideW = 18
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowMasterOverrideW, 0] = "#0;>|#1;<|#2;=|#3;<>";
				// lngRowRateTableW = 19
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowRateTableW, 0] = strRT;
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowRateTableW, 1] = strRT;
				// lngRowCategoryW = 20
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowCategoryW, 0] = strCat;
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowCategoryW, 1] = strCat;
				// MAL@20071211: Add Use RE Option
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowUseRE, 0] = "#0;Yes|#1;No";
				modUTStatusList.Statics.strComboList[frmAccountListing.InstancePtr.lngRowReadType, 0] = "#0;Manual|#1;Auto, Non Radio|#2;Auto, Radio";
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Loading Combos", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
	}
}
