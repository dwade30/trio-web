﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for srptUTSLAllActivityDetail.
	/// </summary>
	public partial class srptUTSLAllActivityDetail : FCSectionReport
	{
		public srptUTSLAllActivityDetail()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "ActivityDetail";
		}

		public static srptUTSLAllActivityDetail InstancePtr
		{
			get
			{
				return (srptUTSLAllActivityDetail)Sys.GetInstance(typeof(srptUTSLAllActivityDetail));
			}
		}

		protected srptUTSLAllActivityDetail _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
				rsBill.Dispose();
				rsLN.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	srptUTSLAllActivityDetail	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/31/2006              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               02/01/2006              *
		// ********************************************************
		int lngBillKey;
		clsDRWrapper rsData = new clsDRWrapper();
		clsDRWrapper rsBill = new clsDRWrapper();
		clsDRWrapper rsLN = new clsDRWrapper();
		double dblPrincipal;
		double dblTax;
		double dblInterest;
		double dblCost;
		double dblTotalPrincipal;
		// vbPorter upgrade warning: dblTotalTax As object	OnWrite(object, short, double)
		double dblTotalTax;
		double dblTotalInterest;
		double dblTotalCost;
		bool boolLien;
		bool boolInterestLine;
		double dblCurrentInterest;
		double dblTotal;
		bool boolWater;
		string strWS = "";

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (rsData.EndOfFile())
			{
				if (frmUTStatusList.InstancePtr.chkCurrentInterest.Checked & boolInterestLine != true)
				{
					//FC:FINAL:MSH - issue #946: add Get_Fields_Int32 to prevent error if data will be equal an empty string 
					if (rsBill.Get_Fields_Int32(strWS + "LienRecordNumber") == 0)
					{
						dblTotal = modUTCalculations.CalculateAccountUT(rsBill, rptUTStatusListAccountDetail.InstancePtr.dtAsOfDate, ref dblCurrentInterest, boolWater);
					}
					else
					{
						dblTotal = modUTCalculations.CalculateAccountUTLien(rsLN, rptUTStatusListAccountDetail.InstancePtr.dtAsOfDate, ref dblCurrentInterest, boolWater);
					}
					if (dblCurrentInterest != 0)
					{
						boolInterestLine = true;
					}
				}
				else
				{
					boolInterestLine = false;
				}
				rsBill.MoveNext();
				eArgs.EOF = rsBill.EndOfFile();
			}
			else
			{
				eArgs.EOF = false;
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			var rsRateKey = new clsDRWrapper();
            try
            {
                // On Error GoTo ERROR_HANDLER
                string strAODate = "";

                // this is how I will pass the billkey into this subreport
                lngBillKey = FCConvert.ToInt32(Math.Round(Conversion.Val(this.UserData)));
                boolWater = FCConvert.CBool(
                    Strings.UCase(
                        frmUTStatusList.InstancePtr.vsWhere.TextMatrix(frmUTStatusList.InstancePtr.lngRowWS, 1)) ==
                    "WATER");
                if (boolWater)
                {
                    strWS = "W";
                }
                else
                {
                    strWS = "S";
                }

                if (modMain.Statics.gboolUTUseAsOfDate)
                {
                    strAODate = " RecordedTransactionDate <= '" +
                                FCConvert.ToString(modMain.Statics.gdtUTStatusListAsOfDate) + "' AND ";
                }
                else
                {
                    strAODate = "";
                }

                // If lngBillKey = 8773 Then
                // MsgBox "hi"
                // End If
                if (lngBillKey > 0)
                {
                    rsBill.OpenRecordset("SELECT * FROM Bill WHERE ID = " + FCConvert.ToString(lngBillKey));
                    //FC:FINAL:MSH - issue #946: add Get_Fields_Int32 to prevent error if data will be equal an empty string 
                    if (rsBill.Get_Fields_Int32(strWS + "LienRecordNumber") != 0)
                    {
                        boolLien = true;
                        rsLN.OpenRecordset(
                            "SELECT * FROM Lien WHERE ID = " + rsBill.Get_Fields(strWS + "LienRecordNumber"),
                            modExtraModules.strUTDatabase);
                        if (strAODate != "")
                        {
                            rsRateKey.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " +
                                                    rsLN.Get_Fields_Int32("RateKey"));
                            if (rsRateKey.EndOfFile() != true && rsRateKey.BeginningOfFile() != true)
                            {
                                if (rsRateKey.Get_Fields_DateTime("BillDate").ToOADate() >
                                    modMain.Statics.gdtUTStatusListAsOfDate.ToOADate())
                                {
                                    //TODO
                                    //goto ShowBillPayments;
                                }
                            }
                        }

                        rsData.OpenRecordset(
                            "SELECT * FROM PaymentRec WHERE " + strAODate + " BillKey = " +
                            rsBill.Get_Fields(strWS + "LienRecordNumber") + " AND ISNULL(LIEN,0) = 1 AND Service = '" +
                            strWS + "'", modExtraModules.strUTDatabase);
                    }
                    else
                    {
                        ShowBillPayments: ;
                        boolLien = false;
                        rsData.OpenRecordset("SELECT * FROM PaymentRec WHERE " + strAODate + " BillKey = " +
                                             FCConvert.ToString(lngBillKey) +
                                             " AND ISNULL(LIEN,0) = 0 AND Service = '" + strWS + "'");
                    }

                    if (rsData.EndOfFile())
                    {
                        // no payments were found for this account
                        this.Close();
                    }
                    else
                    {
                        // rock on
                        if (boolLien)
                        {
                            // TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
                            //FC:FINAL:MSH - issue #946: add Get_Fields_Double to prevent error if data will be equal an empty string 
                            dblTotalPrincipal = rsLN.Get_Fields_Double("Principal");
                            // TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
                            //FC:FINAL:MSH - issue #946: add Get_Fields_Double to prevent error if data will be equal an empty string 
                            dblTotalTax = rsLN.Get_Fields_Double("Tax");
                            // TODO Get_Fields: Check the table for the column [Interest] and replace with corresponding Get_Field method
                            //FC:FINAL:MSH - issue #946: add Get_Fields_Double to prevent error if data will be equal an empty string 
                            dblTotalInterest = rsLN.Get_Fields_Double("Interest");
                            // TODO Get_Fields: Check the table for the column [Costs] and replace with corresponding Get_Field method
                            //FC:FINAL:MSH - issue #946: add Get_Fields_Double to prevent error if data will be equal an empty string 
                            dblTotalCost = rsLN.Get_Fields_Double("Costs");
                        }
                        else
                        {
                            //FC:FINAL:MSH - issue #946: add Get_Fields_Double to prevent error if data will be equal an empty string 
                            dblTotalPrincipal = rsBill.Get_Fields_Double(strWS + "PrinOwed");
                            dblTotalTax = rsBill.Get_Fields_Double(strWS + "TaxOwed");
                            dblTotalInterest = rsBill.Get_Fields_Double(strWS + "IntOwed");
                            dblTotalCost = rsBill.Get_Fields_Double(strWS + "CostOwed");
                            // kk05232014 trout-911  Fix the way prepays are shown
                            if (rsBill.Get_Fields_Int32("BillingRateKey") != 0)
                            {
                                if (strAODate != "")
                                {
                                    rsRateKey.OpenRecordset("SELECT * FROM RateKeys WHERE ID = " +
                                                            rsBill.Get_Fields_Int32("BillingRateKey"));
                                    if (rsRateKey.EndOfFile() != true && rsRateKey.BeginningOfFile() != true)
                                    {
                                        if (rsRateKey.Get_Fields_DateTime("BillDate").ToOADate() >
                                            modMain.Statics.gdtUTStatusListAsOfDate.ToOADate())
                                        {
                                            dblTotalPrincipal = 0;
                                            dblTotalTax = 0;
                                            dblTotalInterest = 0;
                                            dblTotalCost = 0;
                                        }
                                    }
                                    else
                                    {
                                        dblTotalPrincipal = 0;
                                        dblTotalTax = 0;
                                        dblTotalInterest = 0;
                                        dblTotalCost = 0;
                                    }
                                }
                            }
                            else
                            {
                                dblTotalPrincipal = 0;
                                dblTotalTax = 0;
                                dblTotalInterest = 0;
                                dblTotalCost = 0;
                            }
                        }
                    }
                }
                else
                {
                    // no bill key was passed in
                    this.Close();
                }

                return;
            }
            catch (Exception ex)
            {

                MessageBox.Show(
                    "Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " +
                    Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK,
                    MessageBoxIcon.Hand);
            }
            finally
            {
				rsRateKey.Dispose();
            }
		}

		private void BindFields()
		{
			// this will put the information into the fields
			if (!rsData.EndOfFile())
			{
				// Date
				fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy");
				// Code
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCode.Text = FCConvert.ToString(rsData.Get_Fields("Code"));
				// Ref
				fldRef.Text = FCConvert.ToString(rsData.Get_Fields_String("Reference"));
				// Payment
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblPrincipal = FCConvert.ToDouble(rsData.Get_Fields_Decimal("Principal"));
				// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
				dblTax = FCConvert.ToDouble(rsData.Get_Fields_Decimal("Tax"));
				dblInterest = FCConvert.ToDouble(rsData.Get_Fields_Decimal("CurrentInterest") + rsData.Get_Fields_Decimal("PreLienInterest"));
				dblCost = FCConvert.ToDouble(rsData.Get_Fields_Decimal("LienCost"));
				// show the payment values
				fldPrincipal.Text = Strings.Format(dblPrincipal, "#,##0.00");
				fldTax.Text = Strings.Format(dblTax, "#,##0.00");
				fldInterest.Text = Strings.Format(dblInterest, "#,##0.00");
				fldCost.Text = Strings.Format(dblCost, "#,##0.00");
				fldTotal.Text = Strings.Format(dblPrincipal + dblTax + dblInterest + dblCost, "#,##0.00");
				// update the running total
				dblTotalPrincipal -= dblPrincipal;
				dblTotalTax -= dblTax;
				dblTotalInterest -= dblInterest;
				dblTotalCost -= dblCost;
				rsData.MoveNext();
			}
			else if (boolInterestLine)
			{
				fldDate.Text = "";
				// Code
				fldCode.Text = "";
				// Ref
				fldRef.Text = "CURINT";
				// Payment
				dblPrincipal = 0;
				dblInterest = dblCurrentInterest;
				dblCost = 0;
				// show the payment values
				fldPrincipal.Text = Strings.Format(dblPrincipal, "#,##0.00");
				fldTax.Text = Strings.Format(dblTax, "#,##0.00");
				fldInterest.Text = Strings.Format(dblInterest, "#,##0.00");
				fldCost.Text = Strings.Format(dblCost, "#,##0.00");
				fldTotal.Text = Strings.Format(dblPrincipal + dblInterest + dblCost, "#,##0.00");
				// update the running total
				dblTotalInterest -= dblInterest;
			}
			else
			{
				fldDate.Visible = false;
				fldCode.Visible = false;
				fldRef.Visible = false;
				fldPrincipal.Visible = false;
				fldTax.Visible = false;
				fldInterest.Visible = false;
				fldCost.Visible = false;
				fldTotal.Visible = false;
				Detail.Height = 0;
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			fldTotalPrincipal.Text = Strings.Format(dblTotalPrincipal, "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTotalTax, "#,##0.00");
			fldTotalInterest.Text = Strings.Format(dblTotalInterest, "#,##0.00");
			fldTotalCost.Text = Strings.Format(dblTotalCost, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotalPrincipal + dblTotalInterest + dblTotalCost, "#,##0.00");
		}

		private void ReportHeader_Format(object sender, EventArgs e)
		{
			fldOriginalPrincipal.Text = Strings.Format(dblTotalPrincipal, "#,##0.00");
			fldOriginalTax.Text = Strings.Format(dblTotalTax, "#,##0.00");
			fldOriginalInterest.Text = Strings.Format(dblTotalInterest, "#,##0.00");
			fldOriginalCost.Text = Strings.Format(dblTotalCost, "#,##0.00");
			fldOriginalTotal.Text = Strings.Format(dblTotalPrincipal + dblTotalInterest + dblTotalCost, "#,##0.00");
		}

		private void srptUTSLAllActivityDetail_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//srptUTSLAllActivityDetail properties;
			//srptUTSLAllActivityDetail.Caption	= "ActivityDetail";
			//srptUTSLAllActivityDetail.Icon	= "srptUTSLAllActivityDetail.dsx":0000";
			//srptUTSLAllActivityDetail.Left	= 0;
			//srptUTSLAllActivityDetail.Top	= 0;
			//srptUTSLAllActivityDetail.Width	= 11880;
			//srptUTSLAllActivityDetail.Height	= 8580;
			//srptUTSLAllActivityDetail.StartUpPosition	= 3;
			//srptUTSLAllActivityDetail.SectionData	= "srptUTSLAllActivityDetail.dsx":058A;
			//End Unmaped Properties
		}
	}
}
