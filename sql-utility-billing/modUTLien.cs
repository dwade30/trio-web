﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	public class modUTLien
	{
		//=========================================================
		// ****************************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                          *
		// *
		// WRITTEN BY     :                       Jim Bertolino           *
		// DATE           :                       07/21/2004              *
		// *
		// MODIFIED BY    :                       Jim Bertolino           *
		// LAST UPDATED   :                       11/21/2006              *
		// ****************************************************************
		public struct AccountFeesAdded
		{
			public int Account;
			// vbPorter upgrade warning: BillKey As int	OnWrite(string)
			public int BillKey;
			public int BillNumber;
			public double CertifiedMailFee;
			public double Fee;
			public string Name;
			public bool Used;
			public bool Processed;
			public string OldAddress;
			public string NewAddress;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public AccountFeesAdded(int unusedParam)
			{
				this.Account = 0;
				this.BillKey = 0;
				this.BillNumber = 0;
				this.CertifiedMailFee = 0;
				this.Fee = 0;
				this.Name = string.Empty;
				this.Used = false;
				this.Processed = false;
				this.OldAddress = string.Empty;
				this.NewAddress = string.Empty;
			}
		};
		// this is for the maturity process
		public static void DisconnectOutPrint(ref int[] BookArray)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this routine will create the disconnection notice outprinting file
				clsDRWrapper rsInfo = new clsDRWrapper();
				string strSQL = "";
				int counter;
				if (frmSetupDisconnectNotices.InstancePtr.cmbAll.Text == "All")
				{
					strSQL = "";
				}
				else if (frmSetupDisconnectNotices.InstancePtr.cmbAll.Text == "Selected Books")
				{
					if (Information.UBound(BookArray, 1) == 1)
					{
						strSQL = " = " + FCConvert.ToString(BookArray[1]);
					}
					else
					{
						for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
						{
							strSQL += FCConvert.ToString(BookArray[counter]) + ",";
						}
						strSQL = "IN (" + Strings.Left(strSQL, strSQL.Length - 1) + ")";
					}
				}
				else
				{
					strSQL = "= " + frmSetupDisconnectNotices.InstancePtr.txtAccountNumber.Text;
				}
				// If lngNewestRateKey > 0 Then
				// strRKSQL = " AND BillingRateKey <= " & lngNewestRateKey
				// End If
				// 
				// If strBooksSelection = "A" Then
				// If .chkIncludeSewer.Value Then
				// rsInfo.OpenRecordset "SELECT * FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " & strRKSQL & " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) WHERE (WaterTotal + SewerTotal) >= " & dbl(.txtMinimumAmount.Text)
				// & " ORDER BY BookNumber, Sequence, AccountKey"
				// Else
				// rsInfo.OpenRecordset "SELECT * FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " & strRKSQL & " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) WHERE WaterTotal >= " & dbl(.txtMinimumAmount.Text) & "  ORDER BY BookNumber, Sequence, AccountKey"
				// End If
				// ElseIf strBooksSelection = "B" Then
				// If .chkIncludeSewer.Value Then
				// rsInfo.OpenRecordset "SELECT * FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " & strRKSQL & " AND Sequence > " & Val(frmSetupDisconnectEditReport.txtSequenceNumber.Text) & " AND MeterTable.BookNumber " & strSQL &
				// " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) WHERE (WaterTotal + SewerTotal) >= " & dbl(.txtMinimumAmount.Text) & " ORDER BY BookNumber, Sequence, AccountKey"
				// Else
				// rsInfo.OpenRecordset "SELECT * FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 " & strRKSQL & " AND Sequence > " & Val(frmSetupDisconnectEditReport.txtSequenceNumber.Text) & " AND MeterTable.BookNumber " & strSQL &
				// " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey) WHERE WaterTotal >= " & dbl(.txtMinimumAmount.Text) & " ORDER BY BookNumber, Sequence, AccountKey"
				// End If
				// Else
				// If .chkIncludeSewer.Value Then
				// rsInfo.OpenRecordset "SELECT * FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM ((MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey) INNER JOIN Master ON Bill.AccountKey = Master.Key) WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 AND Master.AccountNumber " & strSQL & strRKSQL & " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey)" &
				// " WHERE (WaterTotal + SewerTotal) >= " & dbl(.txtMinimumAmount.Text) & " ORDER BY BookNumber, Sequence, AccountKey"
				// Else
				// rsInfo.OpenRecordset "SELECT * FROM (SELECT MeterTable.Sequence as Sequence, MeterTable.BookNumber as BookNumber, Bill.AccountKey as AccountKey, SUM((Bill.WPrinOwed - Bill.WPrinPaid) + (Bill.WTaxOwed - Bill.WTaxPaid) + (Bill.WIntOwed - Bill.WIntAdded - Bill.WIntPaid) + (Bill.WCostOwed - Bill.WCostAdded - Bill.WCostPaid)) AS WaterTotal, SUM((Bill.SPrinOwed - Bill.SPrinPaid) + (Bill.STaxOwed - Bill.STaxPaid) + (Bill.SIntOwed - Bill.SIntAdded - Bill.SIntPaid) + (Bill.SCostOwed - Bill.SCostAdded - Bill.SCostPaid)) AS SewerTotal FROM ((MeterTable INNER JOIN Bill ON Bill.MeterKey = MeterTable.MeterKey) INNER JOIN Master ON Bill.AccountKey = Master.Key) WHERE ((Bill.WPrinOwed > Bill.WPrinPaid) OR (Bill.WTaxOwed <> Bill.WTaxPaid) OR (Bill.WIntOwed - Bill.WIntAdded > Bill.WIntPaid) OR (Bill.WCostOwed - Bill.WCostAdded <> Bill.WCostPaid)) AND WLienRecordNumber = 0 AND Master.AccountNumber " & strSQL & strRKSQL & " GROUP BY MeterTable.BookNumber, MeterTable.Sequence, Bill.AccountKey)" &
				// " WHERE WaterTotal >= " & dbl(.txtMinimumAmount.Text) & " ORDER BY BookNumber, Sequence, AccountKey"
				// End If
				// End If
				// create file
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Disconnection Outprint", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public class StaticVariables
		{
			public AccountFeesAdded[] arrDemand = null;
			public bool gboolUseMailDateForMaturity;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
