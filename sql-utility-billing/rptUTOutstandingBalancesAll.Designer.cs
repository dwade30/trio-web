﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptUTOutstandingBalancesAll.
	/// </summary>
	partial class rptUTOutstandingBalancesAll
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptUTOutstandingBalancesAll));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.ReportHeader = new GrapeCity.ActiveReports.SectionReportModel.ReportHeader();
			this.ReportFooter = new GrapeCity.ActiveReports.SectionReportModel.ReportFooter();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPage = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lnHeader = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblYear = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTaxDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReportType = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.sarOB1 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.sarOb2 = new GrapeCity.ActiveReports.SectionReportModel.SubReport();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalTaxDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalPaymentReceived = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lnTotals = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.CanShrink = true;
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.sarOB1,
				this.sarOb2,
				this.Line1
			});
			this.Detail.Height = 0.6041667F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// ReportHeader
			// 
			this.ReportHeader.Height = 0F;
			this.ReportHeader.Name = "ReportHeader";
			// 
			// ReportFooter
			// 
			this.ReportFooter.Format += new System.EventHandler(this.ReportFooter_Format);
			this.ReportFooter.CanGrow = false;
			this.ReportFooter.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalTaxDue,
				this.fldTotalPaymentReceived,
				this.fldTotalDue,
				this.lnTotals,
				this.lblTotals
			});
			this.ReportFooter.Height = 0.28125F;
			this.ReportFooter.KeepTogether = true;
			this.ReportFooter.Name = "ReportFooter";
			// 
			// PageHeader
			// 
			this.PageHeader.CanGrow = false;
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblHeader,
				this.lblDate,
				this.lblPage,
				this.lblTime,
				this.lblMuniName,
				this.lnHeader,
				this.lblAccount,
				this.lblYear,
				this.lblTaxDue,
				this.lblPaymentReceived,
				this.lblDue,
				this.lblReportType,
				this.lblName
			});
			this.PageHeader.Height = 1.1875F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblHeader
			// 
			this.lblHeader.Height = 0.1875F;
			this.lblHeader.HyperLink = null;
			this.lblHeader.Left = 0F;
			this.lblHeader.Name = "lblHeader";
			this.lblHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center";
			this.lblHeader.Text = "Outstanding Status List";
			this.lblHeader.Top = 0F;
			this.lblHeader.Width = 7F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 5.875F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1.125F;
			// 
			// lblPage
			// 
			this.lblPage.Height = 0.1875F;
			this.lblPage.HyperLink = null;
			this.lblPage.Left = 5.875F;
			this.lblPage.Name = "lblPage";
			this.lblPage.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblPage.Text = null;
			this.lblPage.Top = 0.1875F;
			this.lblPage.Width = 1.125F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.1875F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.1875F;
			this.lblTime.Width = 1.125F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lnHeader
			// 
			this.lnHeader.Height = 0F;
			this.lnHeader.Left = 0F;
			this.lnHeader.LineWeight = 1F;
			this.lnHeader.Name = "lnHeader";
			this.lnHeader.Top = 1.1875F;
			this.lnHeader.Width = 6.9375F;
			this.lnHeader.X1 = 0F;
			this.lnHeader.X2 = 6.9375F;
			this.lnHeader.Y1 = 1.1875F;
			this.lnHeader.Y2 = 1.1875F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.125F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right";
			this.lblAccount.Text = "Acct";
			this.lblAccount.Top = 1F;
			this.lblAccount.Width = 0.5F;
			// 
			// lblYear
			// 
			this.lblYear.Height = 0.1875F;
			this.lblYear.HyperLink = null;
			this.lblYear.Left = 3.3125F;
			this.lblYear.Name = "lblYear";
			this.lblYear.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblYear.Text = "Year";
			this.lblYear.Top = 1F;
			this.lblYear.Width = 0.5F;
			// 
			// lblTaxDue
			// 
			this.lblTaxDue.Height = 0.375F;
			this.lblTaxDue.HyperLink = null;
			this.lblTaxDue.Left = 4.0625F;
			this.lblTaxDue.Name = "lblTaxDue";
			this.lblTaxDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblTaxDue.Text = "Original Tax";
			this.lblTaxDue.Top = 0.8125F;
			this.lblTaxDue.Width = 0.8125F;
			// 
			// lblPaymentReceived
			// 
			this.lblPaymentReceived.Height = 0.375F;
			this.lblPaymentReceived.HyperLink = null;
			this.lblPaymentReceived.Left = 5F;
			this.lblPaymentReceived.Name = "lblPaymentReceived";
			this.lblPaymentReceived.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblPaymentReceived.Text = "Payment / Adjustments";
			this.lblPaymentReceived.Top = 0.8125F;
			this.lblPaymentReceived.Width = 0.9375F;
			// 
			// lblDue
			// 
			this.lblDue.Height = 0.375F;
			this.lblDue.HyperLink = null;
			this.lblDue.Left = 6.25F;
			this.lblDue.Name = "lblDue";
			this.lblDue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: center";
			this.lblDue.Text = "Amount Due";
			this.lblDue.Top = 0.8125F;
			this.lblDue.Width = 0.75F;
			// 
			// lblReportType
			// 
			this.lblReportType.Height = 0.8125F;
			this.lblReportType.HyperLink = null;
			this.lblReportType.Left = 0F;
			this.lblReportType.Name = "lblReportType";
			this.lblReportType.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center";
			this.lblReportType.Text = "Report Type";
			this.lblReportType.Top = 0.1875F;
			this.lblReportType.Width = 7F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.875F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblName.Text = "Name ----";
			this.lblName.Top = 1F;
			this.lblName.Width = 1.0625F;
			// 
			// sarOB1
			// 
			this.sarOB1.CloseBorder = false;
			this.sarOB1.Height = 0.125F;
			this.sarOB1.Left = 0F;
			this.sarOB1.Name = "sarOB1";
			this.sarOB1.Report = null;
			this.sarOB1.Top = 0.0625F;
			this.sarOB1.Width = 6.875F;
			// 
			// sarOb2
			// 
			this.sarOb2.CloseBorder = false;
			this.sarOb2.Height = 0.125F;
			this.sarOb2.Left = 0F;
			this.sarOb2.Name = "sarOb2";
			this.sarOb2.Report = null;
			this.sarOb2.Top = 0.375F;
			this.sarOb2.Width = 6.875F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.25F;
			this.Line1.Width = 7F;
			this.Line1.X1 = 0F;
			this.Line1.X2 = 7F;
			this.Line1.Y1 = 0.25F;
			this.Line1.Y2 = 0.25F;
			// 
			// fldTotalTaxDue
			// 
			this.fldTotalTaxDue.Height = 0.1875F;
			this.fldTotalTaxDue.Left = 3.8125F;
			this.fldTotalTaxDue.Name = "fldTotalTaxDue";
			this.fldTotalTaxDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalTaxDue.Text = "0.00";
			this.fldTotalTaxDue.Top = 0.0625F;
			this.fldTotalTaxDue.Width = 1.0625F;
			// 
			// fldTotalPaymentReceived
			// 
			this.fldTotalPaymentReceived.Height = 0.1875F;
			this.fldTotalPaymentReceived.Left = 4.875F;
			this.fldTotalPaymentReceived.Name = "fldTotalPaymentReceived";
			this.fldTotalPaymentReceived.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalPaymentReceived.Text = "0.00";
			this.fldTotalPaymentReceived.Top = 0.0625F;
			this.fldTotalPaymentReceived.Width = 1.0625F;
			// 
			// fldTotalDue
			// 
			this.fldTotalDue.Height = 0.1875F;
			this.fldTotalDue.Left = 5.9375F;
			this.fldTotalDue.Name = "fldTotalDue";
			this.fldTotalDue.Style = "font-family: \'Tahoma\'; text-align: right";
			this.fldTotalDue.Text = "0.00";
			this.fldTotalDue.Top = 0.0625F;
			this.fldTotalDue.Width = 1.0625F;
			// 
			// lnTotals
			// 
			this.lnTotals.Height = 0F;
			this.lnTotals.Left = 2.875F;
			this.lnTotals.LineWeight = 2F;
			this.lnTotals.Name = "lnTotals";
			this.lnTotals.Top = 0F;
			this.lnTotals.Width = 4.125F;
			this.lnTotals.X1 = 2.875F;
			this.lnTotals.X2 = 7F;
			this.lnTotals.Y1 = 0F;
			this.lnTotals.Y2 = 0F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 2F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold";
			this.lblTotals.Text = "Total:";
			this.lblTotals.Top = 0.0625F;
			this.lblTotals.Width = 1.8125F;
			// 
			// rptUTOutstandingBalancesAll
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.ReportHeader);
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.Sections.Add(this.ReportFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.Disposed += new System.EventHandler(ActiveReport_Terminate);
			((System.ComponentModel.ISupportInitialize)(this.lblHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblYear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReportType)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTaxDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalPaymentReceived)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarOB1;
		private GrapeCity.ActiveReports.SectionReportModel.SubReport sarOb2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.ReportHeader ReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.ReportFooter ReportFooter;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDue;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnTotals;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPage;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Line lnHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblYear;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTaxDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPaymentReceived;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportType;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
