//Fecher vbPorter - Version 1.0.0.90
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;


namespace TWUT0000
{
	public class cUTBillParty
	{

		//=========================================================

		private string strName;
		private string strName2;
		private string strAddress1;
		private string strAddress2;
		private string strAddress3;
		private string strCity;
		private string strState;
		private string strZip;
		private string strZip4;

		public string Name
		{
			set
			{
				strName = value;
			}

			get
			{
					string Name = "";
				Name = strName;
				return Name;
			}
		}



		public string Name2
		{
			set
			{
				strName2 = value;
			}

			get
			{
					string Name2 = "";
				Name2 = strName2;
				return Name2;
			}
		}



		public string Address1
		{
			set
			{
				strAddress1 = value;
			}

			get
			{
					string Address1 = "";
				Address1 = strAddress1;
				return Address1;
			}
		}




		public string Address2
		{
			set
			{
				strAddress2 = value;
			}

			get
			{
					string Address2 = "";
				Address2 = strAddress2;
				return Address2;
			}
		}



		public string Address3
		{
			set
			{
				strAddress3 = value;
			}

			get
			{
					string Address3 = "";
				Address3 = strAddress3;
				return Address3;
			}
		}



		public string City
		{
			set
			{
				strCity = value;
			}

			get
			{
					string City = "";
				City = strCity;
				return City;
			}
		}



		public string State
		{
			set
			{
				strState = value;
			}

			get
			{
					string State = "";
				State = strState;
				return State;
			}
		}



		public string Zip
		{
			set
			{
				strZip = value;
			}

			get
			{
					string Zip = "";
				Zip = strZip;
				return Zip;
			}
		}



		public string Zip4
		{
			set
			{
				strZip4 = value;
			}

			get
			{
					string Zip4 = "";
				Zip4 = strZip4;
				return Zip4;
			}
		}




		public cUTBillParty GetCopy()
		{
			cUTBillParty GetCopy = null;
			cUTBillParty returnParty = new/*AsNew*/ cUTBillParty();
			returnParty.Address1 = Address1;
			returnParty.Address2 = Address2;
			returnParty.Address3 = Address3;
			returnParty.City = City;
			returnParty.State = State;
			returnParty.Zip = Zip;
			returnParty.Zip4 = Zip4;
			returnParty.Name = Name;
			returnParty.Name2 = Name2;
			GetCopy = returnParty;
			return GetCopy;
		}

		public bool IsSame(ref cUTBillParty compareParty)
		{
			bool IsSame = false;
			if (fecherFoundation.Strings.LCase(FCConvert.ToString(Name))!=fecherFoundation.Strings.LCase(compareParty.Name)) {
				return IsSame;
			}
			if (fecherFoundation.Strings.LCase(FCConvert.ToString(Name2))!=fecherFoundation.Strings.LCase(compareParty.Name2)) {
				return IsSame;
			}
			if (fecherFoundation.Strings.LCase(FCConvert.ToString(Address1))!=fecherFoundation.Strings.LCase(compareParty.Address1)) {
				return IsSame;
			}
			if (fecherFoundation.Strings.LCase(FCConvert.ToString(Address2))!=fecherFoundation.Strings.LCase(compareParty.Address2)) {
				return IsSame;
			}
			if (fecherFoundation.Strings.LCase(FCConvert.ToString(Address3))!=fecherFoundation.Strings.LCase(compareParty.Address3)) {
				return IsSame;
			}
			if (fecherFoundation.Strings.LCase(FCConvert.ToString(City))!=fecherFoundation.Strings.LCase(compareParty.City)) {
				return IsSame;
			}
			if (fecherFoundation.Strings.LCase(FCConvert.ToString(State))!=fecherFoundation.Strings.LCase(compareParty.State)) {
				return IsSame;
			}
			if (Zip!=compareParty.Zip) {
				return IsSame;
			}
			IsSame = true;
			return IsSame;
		}

	}
}
