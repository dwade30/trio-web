//Fecher vbPorter - Version 1.0.0.90

using fecherFoundation;
using fecherFoundation.Extensions;

namespace TWUT0000
{
	public class cUTBillAdjustment
	{

		//=========================================================


		private string strDescription;
		private double dblAdjustment;

		public string Description
		{
			set
			{
				strDescription = value;
			}

			get
			{
					string Description = "";
				Description = strDescription;
				return Description;
			}
		}



		public double Adjustment
		{
			set
			{
				dblAdjustment = value;
			}

			get
			{
					double Adjustment = 0;
				Adjustment = dblAdjustment;
				return Adjustment;
			}
		}



		public cUTBillAdjustment GetCopy()
		{
			cUTBillAdjustment GetCopy = null;
			cUTBillAdjustment copyAdjustment = new/*AsNew*/ cUTBillAdjustment();
			copyAdjustment.Adjustment = Adjustment;
			copyAdjustment.Description = Description;
			GetCopy = copyAdjustment;
			return GetCopy;
		}

	}
}
