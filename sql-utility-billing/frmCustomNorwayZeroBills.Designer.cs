﻿namespace TWUT0000
{
    partial class frmCustomNorwayZeroBills
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
		{
            fecherFoundation.Sys.ClearInstance(this);
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

        #region Wisej Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdProcess = new fecherFoundation.FCButton();
            this.fraConfirm = new fecherFoundation.FCPanel();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOk = new fecherFoundation.FCButton();
            this.lblWarn4 = new Wisej.Web.Label();
            this.lblWarn3 = new Wisej.Web.Label();
            this.lblWarn2 = new Wisej.Web.Label();
            this.lblWarn1 = new Wisej.Web.Label();
            this.cboBooks = new fecherFoundation.FCComboBox();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraConfirm)).BeginInit();
            this.fraConfirm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdProcess);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraConfirm);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.cboBooks);
            // 
            // HeaderText
            // 
            this.HeaderText.AutoSize = false;
            this.HeaderText.Size = new System.Drawing.Size(247, 28);
            this.HeaderText.Text = "Zero Bills by Book";
            // 
            // cmdProcess
            // 
            this.cmdProcess.AppearanceKey = "acceptButton";
            this.cmdProcess.Location = new System.Drawing.Point(457, 30);
            this.cmdProcess.Name = "cmdProcess";
            this.cmdProcess.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdProcess.Size = new System.Drawing.Size(100, 48);
            this.cmdProcess.TabIndex = 1;
            this.cmdProcess.Text = "Process";
            this.cmdProcess.Click += new System.EventHandler(this.cmdProcess_Click);
            // 
            // fraConfirm
            // 
            this.fraConfirm.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraConfirm.Controls.Add(this.cmdCancel);
            this.fraConfirm.Controls.Add(this.cmdOk);
            this.fraConfirm.Controls.Add(this.lblWarn4);
            this.fraConfirm.Controls.Add(this.lblWarn3);
            this.fraConfirm.Controls.Add(this.lblWarn2);
            this.fraConfirm.Controls.Add(this.lblWarn1);
            this.fraConfirm.Location = new System.Drawing.Point(4, 6);
            this.fraConfirm.Name = "fraConfirm";
            this.fraConfirm.Size = new System.Drawing.Size(359, 220);
            this.fraConfirm.TabIndex = 6;
            this.fraConfirm.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Location = new System.Drawing.Point(185, 186);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.TabIndex = 11;
            this.cmdCancel.Text = "Cancel";
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left)));
            this.cmdOk.Location = new System.Drawing.Point(37, 186);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.TabIndex = 10;
            this.cmdOk.Text = "Continue";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // lblWarn4
            // 
            this.lblWarn4.Location = new System.Drawing.Point(20, 102);
            this.lblWarn4.Name = "lblWarn4";
            this.lblWarn4.Size = new System.Drawing.Size(318, 17);
            this.lblWarn4.TabIndex = 9;
            this.lblWarn4.Text = "This process cannot be reversed";
            // 
            // lblWarn3
            // 
            this.lblWarn3.Location = new System.Drawing.Point(20, 66);
            this.lblWarn3.Name = "lblWarn3";
            this.lblWarn3.Size = new System.Drawing.Size(311, 17);
            this.lblWarn3.TabIndex = 8;
            this.lblWarn3.Text = "XX Bills in XX Accounts will be affected";
            // 
            // lblWarn2
            // 
            this.lblWarn2.Location = new System.Drawing.Point(18, 36);
            this.lblWarn2.Name = "lblWarn2";
            this.lblWarn2.Size = new System.Drawing.Size(338, 24);
            this.lblWarn2.TabIndex = 7;
            this.lblWarn2.Text = "AND REVERSING JOURNAL ENTRIES CREATED ";
            // 
            // lblWarn1
            // 
            this.lblWarn1.Location = new System.Drawing.Point(20, 7);
            this.lblWarn1.Name = "lblWarn1";
            this.lblWarn1.Size = new System.Drawing.Size(336, 23);
            this.lblWarn1.TabIndex = 6;
            this.lblWarn1.Text = "ALL BILLS FOR BOOK XXX WILL BE ZEROED";
            // 
            // cboBooks
            // 
            this.cboBooks.Location = new System.Drawing.Point(23, 54);
            this.cboBooks.Name = "cboBooks";
            this.cboBooks.Size = new System.Drawing.Size(254, 28);
            this.cboBooks.TabIndex = 7;
            // 
            // fcLabel1
            // 
            this.fcLabel1.AutoSize = true;
            this.fcLabel1.Location = new System.Drawing.Point(23, 25);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(48, 17);
            this.fcLabel1.TabIndex = 8;
            this.fcLabel1.Text = "BOOK";
            // 
            // frmCustomNorwayZeroBills
            // 
            this.Name = "frmCustomNorwayZeroBills";
            this.Text = "Zero Bills by Book";
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdProcess)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraConfirm)).EndInit();
            this.fraConfirm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOk)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private fecherFoundation.FCButton cmdProcess;
        private fecherFoundation.FCPanel fraConfirm;
        private fecherFoundation.FCButton cmdCancel;
        private fecherFoundation.FCButton cmdOk;
        private Wisej.Web.Label lblWarn4;
        private Wisej.Web.Label lblWarn3;
        private Wisej.Web.Label lblWarn2;
        private Wisej.Web.Label lblWarn1;
        private fecherFoundation.FCLabel fcLabel1;
        private fecherFoundation.FCComboBox cboBooks;
    }
}