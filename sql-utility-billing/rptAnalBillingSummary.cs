﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptAnalBillingSummary.
	/// </summary>
	public partial class rptAnalBillingSummary : BaseSectionReport
	{
		public rptAnalBillingSummary()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Billing Edit Report";
		}

		public static rptAnalBillingSummary InstancePtr
		{
			get
			{
				return (rptAnalBillingSummary)Sys.GetInstance(typeof(rptAnalBillingSummary));
			}
		}

		protected rptAnalBillingSummary _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();
            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptAnalBillingSummary	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Jim Bertolino           *
		// DATE           :               01/12/2005              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               03/16/2006              *
		// ********************************************************
		double[] dblTotals = new double[10 + 1];
		// holds totals for all books
		int ct;
		clsDRWrapper rsData = new clsDRWrapper();
		bool boolPreBilling;
		bool boolLandscape;
		float lngWidth;

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_KeyDown(ref Keys KeyCode, int Shift)
		{
			switch (KeyCode)
			{
				case Keys.Escape:
					{
						this.Close();
						break;
					}
			}
			//end switch
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			// sets the book number on the Page header
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.GRID);
			lngWidth = rptAnalysisReportsMaster.InstancePtr.lngWidth;
			boolLandscape = rptAnalysisReportsMaster.InstancePtr.boolLandscape;
			boolPreBilling = rptAnalysisReportsMaster.InstancePtr.boolPreBilling;
			BuildSQL();
			SetupFields();
		}

		private void BindFields()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				double dblTemp;
				int lngBook = 0;
				int lngBills = 0;
				double dblWTotal = 0;
				double dblSTotal = 0;
				double dblWTax = 0;
				double dblSTax = 0;
				double dblWRegular = 0;
				double dblSRegular = 0;
				double dblWMisc = 0;
				double dblSMisc = 0;
				double dblBookTotal = 0;
				if (!rsData.EndOfFile())
				{
					// calculate the amounts
					// TODO Get_Fields: Check the table for the column [Book] and replace with corresponding Get_Field method
					lngBook = FCConvert.ToInt32(rsData.Get_Fields("Book"));
					// TODO Get_Fields: Field [NumberOfBills] not found!! (maybe it is an alias?)
					lngBills = FCConvert.ToInt32(rsData.Get_Fields("NumberOfBills"));
					// TODO Get_Fields: Field [WFlat] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [WUnits] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [WConsumption] not found!! (maybe it is an alias?)
					dblWRegular = FCConvert.ToDouble(rsData.Get_Fields("WFlat") + rsData.Get_Fields("WUnits") + rsData.Get_Fields("WConsumption"));
					// TODO Get_Fields: Field [WMisc] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [WAdjust] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [WDEAdjust] not found!! (maybe it is an alias?)
					dblWMisc = FCConvert.ToDouble(rsData.Get_Fields("WMisc") + rsData.Get_Fields("WAdjust") + rsData.Get_Fields("WDEAdjust"));
					// TODO Get_Fields: Field [WTx] not found!! (maybe it is an alias?)
					dblWTax = FCConvert.ToDouble(rsData.Get_Fields("WTx"));
					// TODO Get_Fields: Field [SFlat] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [SUnits] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [SConsumption] not found!! (maybe it is an alias?)
					dblSRegular = FCConvert.ToDouble(rsData.Get_Fields("SFlat") + rsData.Get_Fields("SUnits") + rsData.Get_Fields("SConsumption"));
					// TODO Get_Fields: Field [SMisc] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [SAdjust] not found!! (maybe it is an alias?)
					// TODO Get_Fields: Field [SDEAdjust] not found!! (maybe it is an alias?)
					dblSMisc = FCConvert.ToDouble(rsData.Get_Fields("SMisc") + rsData.Get_Fields("SAdjust") + rsData.Get_Fields("SDEAdjust"));
					// TODO Get_Fields: Field [STx] not found!! (maybe it is an alias?)
					dblSTax = FCConvert.ToDouble(rsData.Get_Fields("STx"));
					// keep track of totals for the book to put at the last line of this report
					dblWTotal = dblWRegular + dblWTax + dblWMisc;
					dblSTotal = dblSRegular + dblSTax + dblSMisc;
					dblBookTotal = dblWTotal + dblSTotal;
					// fill the fields on the report
					// fldBook
					fldBook.Text = lngBook.ToString();
					// fldOverride
					// fldBills
					fldBills.Text = lngBills.ToString();
					// fldWRegular
					fldWRegular.Text = Strings.Format(dblWRegular, "#,##0.00");
					// fldWMisc
					fldWMisc.Text = Strings.Format(dblWMisc, "#,##0.00");
					// fldWTax
					fldWTax.Text = Strings.Format(dblWTax, "#,##0.00");
					// fldWTotal
					fldWTotal.Text = Strings.Format(dblWTotal, "#,##0.00");
					// fldSRegular
					fldSRegular.Text = Strings.Format(dblSRegular, "#,##0.00");
					// fldSMisc
					fldSMisc.Text = Strings.Format(dblSMisc, "#,##0.00");
					// fldSTax
					fldSTax.Text = Strings.Format(dblSTax, "#,##0.00");
					// fldSTotal
					fldSTotal.Text = Strings.Format(dblSTotal, "#,##0.00");
					// fldAcctTotal
					fldAcctTotal.Text = Strings.Format(dblBookTotal, "#,##0.00");
					rsData.MoveNext();
				}
				// update the sums of all of the columns
				dblTotals[1] += lngBills;
				dblTotals[2] += dblWRegular;
				dblTotals[3] += dblWMisc;
				dblTotals[4] += dblWTax;
				dblTotals[5] += dblWTotal;
				dblTotals[6] += dblSRegular;
				dblTotals[7] += dblSMisc;
				dblTotals[8] += dblSTax;
				dblTotals[9] += dblSTotal;
				dblTotals[10] += dblBookTotal;
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Binding Fields - BS", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strTemp = "";
				string strFields;
				string strRK = "";
				string strFinal = "";
				string strSQL;
				// MAL@20070925
				if (rptAnalysisReportsMaster.InstancePtr.boolFinal)
				{
					strFinal = " AND ISNULL(Final,0) = 1 ";
				}
				else
				{
					strFinal = "";
				}
				if (boolPreBilling)
				{
					strTemp = "<>";
					strRK = "";
				}
				else
				{
					strTemp = "=";
					strRK = " AND BillingRateKey IN " + rptAnalysisReportsMaster.InstancePtr.strRateKeyList;
				}
				strFields = "Book, Count(Book) AS NumberOfBills, SUM(WMiscAmount) AS WMisc, SUM(WAdjustAmount) AS WAdjust, SUM(WDEAdjustAmount) AS WDEAdjust, SUM(WFlatAmount) AS WFlat, SUM(WUnitsAmount) AS WUnits, SUM(WConsumptionAmount) AS WConsumption, SUM(WTax) AS WTx, SUM(WaterOverrideAmount) AS WaterOverride, SUM(SMiscAmount) AS SMisc, SUM(SAdjustAmount) AS SAdjust, SUM(SDEAdjustAmount) AS SDEAdjust, SUM(SFlatAmount) AS SFlat, SUM(SUnitsAmount) AS SUnits, SUM(SConsumptionAmount) AS SConsumption, SUM(STax) AS STx, SUM(SewerOverrideAmount) AS SewerOverride";
				strSQL = "SELECT " + strFields + " FROM Bill WHERE (" + rptAnalysisReportsMaster.InstancePtr.strBookList + ") AND BillStatus " + strTemp + " 'B'" + strRK + strFinal + " GROUP BY Book ORDER BY Book";
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				return BuildSQL;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Building SQL - BS", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return BuildSQL;
		}

		private void ShowEndTotals()
		{
            // this will fill the totals in
            lblBillsTotal.Text = FCConvert.ToString(dblTotals[1]);
			fldWRegularTotal.Text = Strings.Format(dblTotals[2], "#,##0.00");
			fldWMiscTotal.Text = Strings.Format(dblTotals[3], "#,##0.00");
			fldWTaxTotal.Text = Strings.Format(dblTotals[4], "#,##0.00");
			fldGrandWTotal.Text = Strings.Format(dblTotals[5], "#,##0.00");
			fldSRegularTotal.Text = Strings.Format(dblTotals[6], "#,##0.00");
			fldSMiscTotal.Text = Strings.Format(dblTotals[7], "#,##0.00");
			fldSTaxTotal.Text = Strings.Format(dblTotals[8], "#,##0.00");
			fldGrandSTotal.Text = Strings.Format(dblTotals[9], "#,##0.00");
			fldGrandTotal.Text = Strings.Format(dblTotals[10], "#,##0.00");
		}

		private void SetupFields()
		{
			if (boolLandscape)
			{
				// landscape
				// set all of the header labels, all of the other fields will mimic the settings here
				lblBook.Left = 0;
				lblOverride.Left = 720 / 1440f;
				lblBills.Left = 1710 / 1440f;
				lblWRegular.Left = 2430 / 1440f;
				lblWMisc.Left = 3870 / 1440f;
				lblWTax.Left = 4950 / 1440f;
				lblWTotal.Left = 6030 / 1440f;
				lblSRegular.Left = 7650 / 1440f;
				lblSMisc.Left = 9090 / 1440f;
				lblSTax.Left = 10170 / 1440f;
				lblSTotal.Left = 11250 / 1440f;
				lblTotal.Left = 12780 / 1440f;
				lblBook.Width = 720 / 1440f;
				lblOverride.Width = 990 / 1440f;
				lblBills.Width = 720 / 1440f;
				lblWRegular.Width = 1440 / 1440f;
				lblWMisc.Width = 1080 / 1440f;
				lblWTax.Width = 1080 / 1440f;
				lblWTotal.Width = 1530 / 1440f;
				lblSRegular.Width = 1350 / 1440f;
				lblSMisc.Width = 1080 / 1440f;
				lblSTax.Width = 1080 / 1440f;
				lblSTotal.Width = 1530 / 1440f;
				lblTotal.Width = 1620 / 1440f;
			}
			else
			{
				// portrait
				// set all of the header labels, all of the other fields will mimic the settings here
				lblBook.Left = 0;
				lblOverride.Left = 720 / 1440f;
				lblBills.Left = 1530 / 1440f;
				lblWRegular.Left = 2070 / 1440f;
				lblWMisc.Left = 3060 / 1440f;
				lblWTax.Left = 3870 / 1440f;
				lblWTotal.Left = 4680 / 1440f;
				lblSRegular.Left = 5850 / 1440f;
				lblSMisc.Left = 6840 / 1440f;
				lblSTax.Left = 7650 / 1440f;
				lblSTotal.Left = 8460 / 1440f;
				lblTotal.Left = 9630 / 1440f;
				lblBook.Width = 720 / 1440f;
				lblOverride.Width = 810 / 1440f;
				lblBills.Width = 540 / 1440f;
				lblWRegular.Width = 990 / 1440f;
				lblWMisc.Width = 810 / 1440f;
				lblWTax.Width = 810 / 1440f;
				lblWTotal.Width = 1170 / 1440f;
				lblSRegular.Width = 990 / 1440f;
				lblSMisc.Width = 810 / 1440f;
				lblSTax.Width = 810 / 1440f;
				lblSTotal.Width = 1170 / 1440f;
				lblTotal.Width = 1170 / 1440f;
			}
			// report header
			lblHeader.Width = lngWidth;
			// set the lines
			lnHeader.X1 = 0;
			lnHeader.X2 = lngWidth;
			lnTotals.X1 = lblBillsTotal.Left;
			lnTotals.X2 = lngWidth;
			// set all of the other fields to the settings above
			fldBook.Left = lblBook.Left;
			fldOverride.Left = fldOverride.Left;
			fldBills.Left = lblBills.Left;
			fldWRegular.Left = lblWRegular.Left;
			fldWMisc.Left = lblWMisc.Left;
			fldWTax.Left = lblWTax.Left;
			fldWTotal.Left = lblWTotal.Left;
			fldSRegular.Left = lblSRegular.Left;
			fldSMisc.Left = lblSMisc.Left;
			fldSTax.Left = lblSTax.Left;
			fldSTotal.Left = lblSTotal.Left;
			fldAcctTotal.Left = lblTotal.Left;
			lblFooterTitle.Left = lblBook.Left;
			lblBillsTotal.Left = lblBills.Left;
			fldWRegularTotal.Left = lblWRegular.Left;
			fldWMiscTotal.Left = lblWMisc.Left;
			fldWTaxTotal.Left = lblWTax.Left;
			fldGrandWTotal.Left = lblWTotal.Left;
			fldSRegularTotal.Left = lblSRegular.Left;
			fldSMiscTotal.Left = lblSMisc.Left;
			fldSTaxTotal.Left = lblSTax.Left;
			fldGrandSTotal.Left = lblSTotal.Left;
			fldGrandTotal.Left = lblTotal.Left;
			fldBook.Width = lblBook.Width;
			fldOverride.Width = fldOverride.Width;
			fldBills.Width = lblBills.Width;
			fldWRegular.Width = lblWRegular.Width;
			fldWMisc.Width = lblWMisc.Width;
			fldWTax.Width = lblWTax.Width;
			fldWTotal.Width = lblWTotal.Width;
			fldSRegular.Width = lblSRegular.Width;
			fldSMisc.Width = lblSMisc.Width;
			fldSTax.Width = lblSTax.Width;
			fldSTotal.Width = lblSTotal.Width;
			fldAcctTotal.Width = lblTotal.Width;
			lblFooterTitle.Width = lblBook.Width;
			lblBillsTotal.Width = lblBills.Width;
			fldWRegularTotal.Width = lblWRegular.Width;
			fldWMiscTotal.Width = lblWMisc.Width;
			fldWTaxTotal.Width = lblWTax.Width;
			fldGrandWTotal.Width = lblWTotal.Width;
			fldSRegularTotal.Width = lblSRegular.Width;
			fldSMiscTotal.Width = lblSMisc.Width;
			fldSTaxTotal.Width = lblSTax.Width;
			fldGrandSTotal.Width = lblSTotal.Width;
			fldGrandTotal.Width = lblTotal.Width;
			// set the header labels
			lblWaterHeader.Left = lblWRegular.Left;
			lblSewerHeader.Left = lblSRegular.Left;
			lblWaterHeader.Width = lblSRegular.Left - lblWRegular.Left;
			lblSewerHeader.Width = lblTotal.Left - lblSRegular.Left;
			lblSewerHeader.Text = "-   -   -   -   -   Sewer   -   -   -   -   -";
			lblWaterHeader.Text = "-   -   -   -   -   Water   -   -   -   -   -";
			// kk trouts-6 02282013  Change Water to Stormwater for Bangor
			if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "BANGOR")
			{
				lblWaterHeader.Text = "-   -   -   -   - Stormwater -   -   -   -   -";
				lblWTotal.Text = "Stormwater Total";
			}
		}
		// Object list to copy
		// lblBook
		// lblOverride
		// lblBills
		// lblWRegular
		// lblWMisc
		// lblWTax
		// lblWTotal
		// lblSRegular
		// lblSMisc
		// lblSTax
		// lblSTotal
		// lblTotal
		//
		// fldBook
		// fldOverride
		// fldBills
		// fldWRegular
		// fldWMisc
		// fldWTax
		// fldWTotal
		// fldSRegular
		// fldSMisc
		// fldSTax
		// fldSTotal
		// fldAcctTotal
		//
		// lblFooterTitle
		// lblBillsTotal
		// fldWRegularTotal
		// fldWMiscTotal
		// fldWTaxTotal
		// fldGrandWTotal
		// fldSRegularTotal
		// fldSMiscTotal
		// fldSTaxTotal
		// fldGrandSTotal
		// fldGrandTotal
		//
		// lblWaterHeader
		// lblSewerHeader
		//
		// lnHeader
		// lnTotals
		private void GroupFooter1_Format(object sender, EventArgs e)
		{
			// fill in the report footer
			ShowEndTotals();
		}

		private void rptAnalBillingSummary_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptAnalBillingSummary properties;
			//rptAnalBillingSummary.Caption	= "Billing Edit Report";
			//rptAnalBillingSummary.Icon	= "rptAnalBillingSummary.dsx":0000";
			//rptAnalBillingSummary.Left	= 0;
			//rptAnalBillingSummary.Top	= 0;
			//rptAnalBillingSummary.Width	= 11880;
			//rptAnalBillingSummary.Height	= 4935;
			//rptAnalBillingSummary.WindowState	= 2;
			//rptAnalBillingSummary.SectionData	= "rptAnalBillingSummary.dsx":058A;
			//End Unmaped Properties
		}
	}
}
