﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmLienExclusion.
	/// </summary>
	partial class frmLienExclusion : BaseForm
	{
		public fecherFoundation.FCPanel fraGrid;
		public fecherFoundation.FCGrid vsDemand;
		public fecherFoundation.FCLine lnDemand;
		public fecherFoundation.FCLabel lblInstruction;
		//private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileClear;
		public fecherFoundation.FCToolStripMenuItem mnuFileSelectAll;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle1 = new Wisej.Web.DataGridViewCellStyle();
			Wisej.Web.DataGridViewCellStyle dataGridViewCellStyle2 = new Wisej.Web.DataGridViewCellStyle();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLienExclusion));
			this.fraGrid = new fecherFoundation.FCPanel();
			this.vsDemand = new fecherFoundation.FCGrid();
			this.lnDemand = new fecherFoundation.FCLine();
			this.lblInstruction = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileClear = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSelectAll = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdFileSave = new fecherFoundation.FCButton();
			this.cmdFileClear = new fecherFoundation.FCButton();
			this.cmdFileSelectAll = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).BeginInit();
			this.fraGrid.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Controls.Add(this.cmdFileSave);
			this.BottomPanel.Location = new System.Drawing.Point(0, 515);
			this.BottomPanel.Size = new System.Drawing.Size(687, 108);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.fraGrid);
			this.ClientArea.Size = new System.Drawing.Size(687, 455);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileSelectAll);
			this.TopPanel.Controls.Add(this.cmdFileClear);
			this.TopPanel.Size = new System.Drawing.Size(687, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileClear, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileSelectAll, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(265, 30);
			this.HeaderText.Text = "Exclusion List";
			// 
			// fraGrid
			// 
			this.fraGrid.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.fraGrid.AppearanceKey = "groupBoxNoBorders";
			this.fraGrid.Controls.Add(this.vsDemand);
			this.fraGrid.Controls.Add(this.lnDemand);
			this.fraGrid.Controls.Add(this.lblInstruction);
			this.fraGrid.Location = new System.Drawing.Point(0, 0);
			this.fraGrid.Name = "fraGrid";
			this.fraGrid.Size = new System.Drawing.Size(687, 453);
			this.fraGrid.TabIndex = 0;
			this.fraGrid.Visible = false;
			// 
			// vsDemand
			// 
			this.vsDemand.AllowSelection = false;
			this.vsDemand.AllowUserToResizeColumns = false;
			this.vsDemand.AllowUserToResizeRows = false;
			this.vsDemand.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) | Wisej.Web.AnchorStyles.Left) | Wisej.Web.AnchorStyles.Right)));
			this.vsDemand.AutoSizeMode = fecherFoundation.FCGrid.AutoSizeSettings.flexAutoSizeColWidth;
			this.vsDemand.BackColorAlternate = System.Drawing.Color.Empty;
			this.vsDemand.BackColorBkg = System.Drawing.Color.Empty;
			this.vsDemand.BackColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.BackColorSel = System.Drawing.Color.Empty;
			this.vsDemand.Cols = 5;
			dataGridViewCellStyle1.Alignment = Wisej.Web.DataGridViewContentAlignment.MiddleLeft;
			this.vsDemand.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this.vsDemand.ColumnHeadersHeight = 30;
			this.vsDemand.ColumnHeadersHeightSizeMode = Wisej.Web.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
			dataGridViewCellStyle2.WrapMode = Wisej.Web.DataGridViewTriState.False;
			this.vsDemand.DefaultCellStyle = dataGridViewCellStyle2;
			this.vsDemand.DragIcon = null;
			this.vsDemand.EditMode = Wisej.Web.DataGridViewEditMode.EditOnEnter;
			this.vsDemand.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExSort;
			this.vsDemand.ExtendLastCol = true;
			this.vsDemand.FixedCols = 0;
			this.vsDemand.ForeColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.FrozenCols = 0;
			this.vsDemand.GridColor = System.Drawing.Color.Empty;
			this.vsDemand.GridColorFixed = System.Drawing.Color.Empty;
			this.vsDemand.Location = new System.Drawing.Point(30, 90);
			this.vsDemand.Name = "vsDemand";
			this.vsDemand.ReadOnly = true;
			this.vsDemand.RowHeadersVisible = false;
			this.vsDemand.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
			this.vsDemand.RowHeightMin = 0;
			this.vsDemand.Rows = 1;
			this.vsDemand.ScrollTipText = null;
			this.vsDemand.ShowColumnVisibilityMenu = false;
			this.vsDemand.Size = new System.Drawing.Size(637, 343);
			this.vsDemand.StandardTab = true;
			this.vsDemand.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTBelow;
			this.vsDemand.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabControls;
			this.vsDemand.TabIndex = 1;
			this.vsDemand.CurrentCellChanged += new System.EventHandler(this.vsDemand_RowColChange);
			this.vsDemand.CellFormatting += new Wisej.Web.DataGridViewCellFormattingEventHandler(this.vsDemand_MouseMoveEvent);
			this.vsDemand.Click += new System.EventHandler(this.vsDemand_ClickEvent);
			this.vsDemand.DoubleClick += new System.EventHandler(this.vsDemand_DblClick);
			// 
			// lnDemand
			// 
			this.lnDemand.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(255)))));
			this.lnDemand.BorderWidth = ((short)(1));
			this.lnDemand.LineWidth = 0;
			this.lnDemand.Location = new System.Drawing.Point(120, 1003);
			this.lnDemand.Name = "lnDemand";
			this.lnDemand.Size = new System.Drawing.Size(8640, 1);
			this.lnDemand.Visible = false;
			this.lnDemand.X1 = 120F;
			this.lnDemand.X2 = 8760F;
			this.lnDemand.Y1 = 1003F;
			this.lnDemand.Y2 = 1003F;
			// 
			// lblInstruction
			// 
			this.lblInstruction.Location = new System.Drawing.Point(30, 30);
			this.lblInstruction.Name = "lblInstruction";
			this.lblInstruction.Size = new System.Drawing.Size(596, 43);
			this.lblInstruction.TabIndex = 0;
			this.lblInstruction.Visible = false;
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileClear,
				this.mnuFileSelectAll,
				this.mnuFileSeperator,
				this.mnuFileSave,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileClear
			// 
			this.mnuFileClear.Index = 0;
			this.mnuFileClear.Name = "mnuFileClear";
			this.mnuFileClear.Text = "Clear All";
			this.mnuFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// mnuFileSelectAll
			// 
			this.mnuFileSelectAll.Index = 1;
			this.mnuFileSelectAll.Name = "mnuFileSelectAll";
			this.mnuFileSelectAll.Text = "Select All";
			this.mnuFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// mnuFileSeperator
			// 
			this.mnuFileSeperator.Index = 2;
			this.mnuFileSeperator.Name = "mnuFileSeperator";
			this.mnuFileSeperator.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 3;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSave.Text = "Process";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 5;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdFileSave
			// 
			this.cmdFileSave.AppearanceKey = "acceptButton";
			this.cmdFileSave.Location = new System.Drawing.Point(204, 37);
			this.cmdFileSave.Name = "cmdFileSave";
			this.cmdFileSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdFileSave.Size = new System.Drawing.Size(105, 48);
			this.cmdFileSave.TabIndex = 0;
			this.cmdFileSave.Text = "Process";
			this.cmdFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// cmdFileClear
			// 
			this.cmdFileClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileClear.AppearanceKey = "toolbarButton";
			this.cmdFileClear.Location = new System.Drawing.Point(571, 29);
			this.cmdFileClear.Name = "cmdFileClear";
			this.cmdFileClear.Size = new System.Drawing.Size(75, 24);
			this.cmdFileClear.TabIndex = 1;
			this.cmdFileClear.Text = "Clear All";
			this.cmdFileClear.Click += new System.EventHandler(this.mnuFileClear_Click);
			// 
			// cmdFileSelectAll
			// 
			this.cmdFileSelectAll.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileSelectAll.AppearanceKey = "toolbarButton";
			this.cmdFileSelectAll.Location = new System.Drawing.Point(483, 29);
			this.cmdFileSelectAll.Name = "cmdFileSelectAll";
			this.cmdFileSelectAll.Size = new System.Drawing.Size(82, 24);
			this.cmdFileSelectAll.TabIndex = 2;
			this.cmdFileSelectAll.Text = "Select All";
			this.cmdFileSelectAll.Click += new System.EventHandler(this.mnuFileSelectAll_Click);
			// 
			// frmLienExclusion
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(687, 623);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmLienExclusion";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Add Lien Maturity Fees";
			this.Load += new System.EventHandler(this.frmLienExclusion_Load);
			this.Activated += new System.EventHandler(this.frmLienExclusion_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmLienExclusion_KeyPress);
			this.Resize += new System.EventHandler(this.frmLienExclusion_Resize);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraGrid)).EndInit();
			this.fraGrid.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.vsDemand)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileClear)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileSelectAll)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdFileSave;
		private FCButton cmdFileSelectAll;
		private FCButton cmdFileClear;
	}
}
