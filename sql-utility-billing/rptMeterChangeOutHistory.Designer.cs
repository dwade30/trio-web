﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for rptMeterChangeOutHistory.
	/// </summary>
	partial class rptMeterChangeOutHistory
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptMeterChangeOutHistory));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.lblReportHeader = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblCriteria = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblLocation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblSetDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Field1 = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label5 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label6 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblReason = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.fldLastReading = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSize = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSetDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldMultiplier = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDigits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldRemoteNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldSerialNum = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldReason = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCriteria)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSetDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReason)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLastReading)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSize)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSetDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMultiplier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigits)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemoteNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReason)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldLastReading,
				this.fldAccount,
				this.fldName,
				this.fldSize,
				this.fldSetDate,
				this.fldMultiplier,
				this.fldDigits,
				this.fldRemoteNum,
				this.fldSerialNum,
				this.fldReason
			});
			this.Detail.Height = 0.1875F;
			this.Detail.KeepTogether = true;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblReportHeader,
				this.lblMuniName,
				this.lblTime,
				this.lblDate,
				this.lblPageNumber,
				this.Line2,
				this.Label2,
				this.lblCriteria,
				this.lblAccount,
				this.lblName,
				this.lblLocation,
				this.lblSetDate,
				this.Field1,
				this.Label4,
				this.Label5,
				this.Label6,
				this.Label7,
				this.lblReason
			});
			this.PageHeader.Height = 0.9895833F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// lblReportHeader
			// 
			this.lblReportHeader.Height = 0.25F;
			this.lblReportHeader.HyperLink = null;
			this.lblReportHeader.Left = 0F;
			this.lblReportHeader.Name = "lblReportHeader";
			this.lblReportHeader.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 0";
			this.lblReportHeader.Text = "Meter Change Out History";
			this.lblReportHeader.Top = 0F;
			this.lblReportHeader.Width = 9.9375F;
			// 
			// lblMuniName
			// 
			this.lblMuniName.Height = 0.1875F;
			this.lblMuniName.HyperLink = null;
			this.lblMuniName.Left = 0F;
			this.lblMuniName.Name = "lblMuniName";
			this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblMuniName.Text = null;
			this.lblMuniName.Top = 0F;
			this.lblMuniName.Width = 2.625F;
			// 
			// lblTime
			// 
			this.lblTime.Height = 0.2F;
			this.lblTime.HyperLink = null;
			this.lblTime.Left = 0F;
			this.lblTime.Name = "lblTime";
			this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
			this.lblTime.Text = null;
			this.lblTime.Top = 0.2F;
			this.lblTime.Width = 1F;
			// 
			// lblDate
			// 
			this.lblDate.Height = 0.1875F;
			this.lblDate.HyperLink = null;
			this.lblDate.Left = 8.9375F;
			this.lblDate.Name = "lblDate";
			this.lblDate.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblDate.Text = null;
			this.lblDate.Top = 0F;
			this.lblDate.Width = 1F;
			// 
			// lblPageNumber
			// 
			this.lblPageNumber.Height = 0.1875F;
			this.lblPageNumber.HyperLink = null;
			this.lblPageNumber.Left = 8.9375F;
			this.lblPageNumber.Name = "lblPageNumber";
			this.lblPageNumber.Style = "font-family: \'Tahoma\'; text-align: right";
			this.lblPageNumber.Text = null;
			this.lblPageNumber.Top = 0.1875F;
			this.lblPageNumber.Width = 1F;
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 0.0625F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0.9375F;
			this.Line2.Width = 9.8125F;
			this.Line2.X1 = 0.0625F;
			this.Line2.X2 = 9.875F;
			this.Line2.Y1 = 0.9375F;
			this.Line2.Y2 = 0.9375F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 7F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left; ddo-c" + "har-set: 0";
			this.Label2.Text = "Final Rdg";
			this.Label2.Top = 0.75F;
			this.Label2.Width = 0.75F;
			// 
			// lblCriteria
			// 
			this.lblCriteria.Height = 0.375F;
			this.lblCriteria.HyperLink = null;
			this.lblCriteria.Left = 0F;
			this.lblCriteria.Name = "lblCriteria";
			this.lblCriteria.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
			this.lblCriteria.Text = null;
			this.lblCriteria.Top = 0.25F;
			this.lblCriteria.Width = 9.9375F;
			// 
			// lblAccount
			// 
			this.lblAccount.Height = 0.1875F;
			this.lblAccount.HyperLink = null;
			this.lblAccount.Left = 0.0625F;
			this.lblAccount.Name = "lblAccount";
			this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left";
			this.lblAccount.Text = "Account";
			this.lblAccount.Top = 0.75F;
			this.lblAccount.Width = 0.625F;
			// 
			// lblName
			// 
			this.lblName.Height = 0.1875F;
			this.lblName.HyperLink = null;
			this.lblName.Left = 0.6875F;
			this.lblName.Name = "lblName";
			this.lblName.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left";
			this.lblName.Text = "Name";
			this.lblName.Top = 0.75F;
			this.lblName.Width = 1.875F;
			// 
			// lblLocation
			// 
			this.lblLocation.Height = 0.1875F;
			this.lblLocation.HyperLink = null;
			this.lblLocation.Left = 5.1875F;
			this.lblLocation.Name = "lblLocation";
			this.lblLocation.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left";
			this.lblLocation.Text = "Size";
			this.lblLocation.Top = 0.75F;
			this.lblLocation.Width = 0.6875F;
			// 
			// lblSetDate
			// 
			this.lblSetDate.Height = 0.1875F;
			this.lblSetDate.HyperLink = null;
			this.lblSetDate.Left = 2.5625F;
			this.lblSetDate.Name = "lblSetDate";
			this.lblSetDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left";
			this.lblSetDate.Text = "Set Date";
			this.lblSetDate.Top = 0.75F;
			this.lblSetDate.Width = 0.6875F;
			// 
			// Field1
			// 
			this.Field1.CanGrow = false;
			this.Field1.Height = 0.1875F;
			this.Field1.Left = 3.4375F;
			this.Field1.MultiLine = false;
			this.Field1.Name = "Field1";
			this.Field1.Style = "font-family: \'Tahoma\'; white-space: nowrap";
			this.Field1.Text = null;
			this.Field1.Top = 0F;
			this.Field1.Width = 0.8125F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 3.3125F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left";
			this.Label4.Text = "Serial Num";
			this.Label4.Top = 0.75F;
			this.Label4.Width = 0.8125F;
			// 
			// Label5
			// 
			this.Label5.Height = 0.1875F;
			this.Label5.HyperLink = null;
			this.Label5.Left = 4.1875F;
			this.Label5.Name = "Label5";
			this.Label5.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left";
			this.Label5.Text = "Remote Num";
			this.Label5.Top = 0.75F;
			this.Label5.Width = 0.9375F;
			// 
			// Label6
			// 
			this.Label6.Height = 0.1875F;
			this.Label6.HyperLink = null;
			this.Label6.Left = 5.875F;
			this.Label6.Name = "Label6";
			this.Label6.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left";
			this.Label6.Text = "Digits";
			this.Label6.Top = 0.75F;
			this.Label6.Width = 0.5F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 6.4375F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left";
			this.Label7.Text = "Mult";
			this.Label7.Top = 0.75F;
			this.Label7.Width = 0.5625F;
			// 
			// lblReason
			// 
			this.lblReason.Height = 0.1875F;
			this.lblReason.HyperLink = null;
			this.lblReason.Left = 7.8125F;
			this.lblReason.Name = "lblReason";
			this.lblReason.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: left; ddo-c" + "har-set: 0";
			this.lblReason.Text = "Reason / Comment";
			this.lblReason.Top = 0.75F;
			this.lblReason.Width = 1.6875F;
			// 
			// fldLastReading
			// 
			this.fldLastReading.Height = 0.1875F;
			this.fldLastReading.Left = 7F;
			this.fldLastReading.Name = "fldLastReading";
			this.fldLastReading.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
			this.fldLastReading.Text = null;
			this.fldLastReading.Top = 0F;
			this.fldLastReading.Width = 0.75F;
			// 
			// fldAccount
			// 
			this.fldAccount.CanGrow = false;
			this.fldAccount.Height = 0.1875F;
			this.fldAccount.Left = 0.0625F;
			this.fldAccount.MultiLine = false;
			this.fldAccount.Name = "fldAccount";
			this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; white-space: nowrap";
			this.fldAccount.Text = null;
			this.fldAccount.Top = 0F;
			this.fldAccount.Width = 0.5625F;
			// 
			// fldName
			// 
			this.fldName.CanGrow = false;
			this.fldName.Height = 0.1875F;
			this.fldName.Left = 0.6875F;
			this.fldName.MultiLine = false;
			this.fldName.Name = "fldName";
			this.fldName.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap";
			this.fldName.Text = null;
			this.fldName.Top = 0F;
			this.fldName.Width = 1.75F;
			// 
			// fldSize
			// 
			this.fldSize.CanGrow = false;
			this.fldSize.Height = 0.1875F;
			this.fldSize.Left = 5.1875F;
			this.fldSize.Name = "fldSize";
			this.fldSize.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldSize.Text = null;
			this.fldSize.Top = 0F;
			this.fldSize.Width = 0.6875F;
			// 
			// fldSetDate
			// 
			this.fldSetDate.CanGrow = false;
			this.fldSetDate.Height = 0.1875F;
			this.fldSetDate.Left = 2.5625F;
			this.fldSetDate.MultiLine = false;
			this.fldSetDate.Name = "fldSetDate";
			this.fldSetDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; white-space: nowrap";
			this.fldSetDate.Text = null;
			this.fldSetDate.Top = 0F;
			this.fldSetDate.Width = 0.6875F;
			// 
			// fldMultiplier
			// 
			this.fldMultiplier.CanGrow = false;
			this.fldMultiplier.Height = 0.1875F;
			this.fldMultiplier.Left = 6.4375F;
			this.fldMultiplier.Name = "fldMultiplier";
			this.fldMultiplier.Style = "font-family: \'Tahoma\'";
			this.fldMultiplier.Text = null;
			this.fldMultiplier.Top = 0F;
			this.fldMultiplier.Width = 0.5625F;
			// 
			// fldDigits
			// 
			this.fldDigits.CanGrow = false;
			this.fldDigits.Height = 0.1875F;
			this.fldDigits.Left = 5.875F;
			this.fldDigits.Name = "fldDigits";
			this.fldDigits.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldDigits.Text = null;
			this.fldDigits.Top = 0F;
			this.fldDigits.Width = 0.25F;
			// 
			// fldRemoteNum
			// 
			this.fldRemoteNum.CanGrow = false;
			this.fldRemoteNum.Height = 0.1875F;
			this.fldRemoteNum.Left = 4.1875F;
			this.fldRemoteNum.Name = "fldRemoteNum";
			this.fldRemoteNum.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldRemoteNum.Text = null;
			this.fldRemoteNum.Top = 0F;
			this.fldRemoteNum.Width = 0.9375F;
			// 
			// fldSerialNum
			// 
			this.fldSerialNum.CanGrow = false;
			this.fldSerialNum.Height = 0.1875F;
			this.fldSerialNum.Left = 3.3125F;
			this.fldSerialNum.Name = "fldSerialNum";
			this.fldSerialNum.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.fldSerialNum.Text = null;
			this.fldSerialNum.Top = 0F;
			this.fldSerialNum.Width = 0.8125F;
			// 
			// fldReason
			// 
			this.fldReason.Height = 0.1875F;
			this.fldReason.Left = 7.8125F;
			this.fldReason.Name = "fldReason";
			this.fldReason.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
			this.fldReason.Text = null;
			this.fldReason.Top = 0F;
			this.fldReason.Width = 2.0625F;
			// 
			// rptMeterChangeOutHistory
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.25F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.25F;
			this.PageSettings.Orientation = GrapeCity.ActiveReports.Document.Section.PageOrientation.Landscape;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 10F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.lblReportHeader)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblCriteria)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblLocation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblSetDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Field1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label5)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label6)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblReason)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldLastReading)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSize)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSetDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldMultiplier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDigits)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldRemoteNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldSerialNum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldReason)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldLastReading;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSize;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSetDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMultiplier;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDigits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldRemoteNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSerialNum;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldReason;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReportHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCriteria;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblLocation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSetDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Field1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label5;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label6;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblReason;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
