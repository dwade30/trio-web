﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SharedApplication.Messaging;
using SharedApplication.UtilityBilling;

namespace TWUT0000
{
	public class InitializeUtilityBillingCommandHandler : CommandHandler<InitializeUtilityBilling>
	{
		protected override void Handle(InitializeUtilityBilling command)
		{
			modMain.InitializeUtilityBillingStatics();
		}
	}
}
