﻿namespace TWUT0000
{
	/// <summary>
	/// Summary description for arMeterCheck.
	/// </summary>
	partial class arMeterCheck
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(arMeterCheck));
            this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
            this.fldAccount = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldMeterNumber = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSeq = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldBook = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldService = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldSize = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldDigits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldUnits = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCategory = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldCombine = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.fldStatus = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
            this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
            this.lblAccount = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMeterNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblBook = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSeq = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblMuniName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblTime = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblPageNumber = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
            this.lblService = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblSize = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblDigits = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblUnits = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblName = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCategory = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblCombine = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.lblHeaderStatus = new GrapeCity.ActiveReports.SectionReportModel.Label();
            this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMeterNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDigits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCombine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeterNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBook)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDigits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategory)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCombine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.fldAccount,
            this.fldMeterNumber,
            this.fldSeq,
            this.fldBook,
            this.fldService,
            this.fldSize,
            this.fldDigits,
            this.fldUnits,
            this.fldName,
            this.fldCategory,
            this.fldCombine,
            this.fldStatus});
            this.Detail.Height = 0.21875F;
            this.Detail.KeepTogether = true;
            this.Detail.Name = "Detail";
            this.Detail.Format += new System.EventHandler(this.Detail_Format);
            // 
            // fldAccount
            // 
            this.fldAccount.Height = 0.1875F;
            this.fldAccount.Left = 0F;
            this.fldAccount.Name = "fldAccount";
            this.fldAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.fldAccount.Text = null;
            this.fldAccount.Top = 0F;
            this.fldAccount.Width = 0.8125F;
            // 
            // fldMeterNumber
            // 
            this.fldMeterNumber.Height = 0.1875F;
            this.fldMeterNumber.Left = 0.8125F;
            this.fldMeterNumber.Name = "fldMeterNumber";
            this.fldMeterNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.fldMeterNumber.Text = null;
            this.fldMeterNumber.Top = 0F;
            this.fldMeterNumber.Width = 0.5F;
            // 
            // fldSeq
            // 
            this.fldSeq.Height = 0.1875F;
            this.fldSeq.Left = 5.25F;
            this.fldSeq.Name = "fldSeq";
            this.fldSeq.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
            this.fldSeq.Text = null;
            this.fldSeq.Top = 0F;
            this.fldSeq.Width = 0.375F;
            // 
            // fldBook
            // 
            this.fldBook.Height = 0.1875F;
            this.fldBook.Left = 4.875F;
            this.fldBook.Name = "fldBook";
            this.fldBook.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
            this.fldBook.Text = null;
            this.fldBook.Top = 0F;
            this.fldBook.Width = 0.375F;
            // 
            // fldService
            // 
            this.fldService.Height = 0.1875F;
            this.fldService.Left = 5.625F;
            this.fldService.Name = "fldService";
            this.fldService.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: center; ddo-char-set: 0";
            this.fldService.Text = null;
            this.fldService.Top = 0F;
            this.fldService.Width = 0.375F;
            // 
            // fldSize
            // 
            this.fldSize.Height = 0.1875F;
            this.fldSize.Left = 6F;
            this.fldSize.Name = "fldSize";
            this.fldSize.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
            this.fldSize.Text = null;
            this.fldSize.Top = 0F;
            this.fldSize.Width = 0.4375F;
            // 
            // fldDigits
            // 
            this.fldDigits.Height = 0.1875F;
            this.fldDigits.Left = 6.4375F;
            this.fldDigits.Name = "fldDigits";
            this.fldDigits.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
            this.fldDigits.Text = null;
            this.fldDigits.Top = 0F;
            this.fldDigits.Width = 0.3125F;
            // 
            // fldUnits
            // 
            this.fldUnits.Height = 0.1875F;
            this.fldUnits.Left = 7F;
            this.fldUnits.Name = "fldUnits";
            this.fldUnits.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
            this.fldUnits.Text = null;
            this.fldUnits.Top = 0F;
            this.fldUnits.Width = 0.5F;
            // 
            // fldName
            // 
            this.fldName.Height = 0.1875F;
            this.fldName.Left = 1.3125F;
            this.fldName.Name = "fldName";
            this.fldName.Style = "font-family: \'Tahoma\'; font-size: 10pt; ddo-char-set: 0";
            this.fldName.Text = null;
            this.fldName.Top = 0F;
            this.fldName.Width = 2.5625F;
            // 
            // fldCategory
            // 
            this.fldCategory.Height = 0.1875F;
            this.fldCategory.Left = 4.1875F;
            this.fldCategory.Name = "fldCategory";
            this.fldCategory.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
            this.fldCategory.Text = null;
            this.fldCategory.Top = 0F;
            this.fldCategory.Width = 0.6875F;
            // 
            // fldCombine
            // 
            this.fldCombine.Height = 0.1875F;
            this.fldCombine.Left = 3.8125F;
            this.fldCombine.Name = "fldCombine";
            this.fldCombine.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: left; ddo-char-set: 0";
            this.fldCombine.Text = null;
            this.fldCombine.Top = 0F;
            this.fldCombine.Width = 0.375F;
            // 
            // fldStatus
            // 
            this.fldStatus.Height = 0.1875F;
            this.fldStatus.Left = 6.75F;
            this.fldStatus.Name = "fldStatus";
            this.fldStatus.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 0";
            this.fldStatus.Text = null;
            this.fldStatus.Top = 0F;
            this.fldStatus.Width = 0.25F;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
            this.lblAccount,
            this.lblMeterNumber,
            this.lblBook,
            this.lblSeq,
            this.Label1,
            this.lblMuniName,
            this.lblTime,
            this.lblDate,
            this.lblPageNumber,
            this.Line2,
            this.lblService,
            this.lblSize,
            this.lblDigits,
            this.lblUnits,
            this.lblName,
            this.lblCategory,
            this.lblCombine,
            this.lblHeaderStatus});
            this.PageHeader.Height = 0.7083333F;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
            // 
            // lblAccount
            // 
            this.lblAccount.Height = 0.1875F;
            this.lblAccount.HyperLink = null;
            this.lblAccount.Left = 0F;
            this.lblAccount.Name = "lblAccount";
            this.lblAccount.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblAccount.Text = "Account";
            this.lblAccount.Top = 0.5F;
            this.lblAccount.Width = 0.8125F;
            // 
            // lblMeterNumber
            // 
            this.lblMeterNumber.Height = 0.1875F;
            this.lblMeterNumber.HyperLink = null;
            this.lblMeterNumber.Left = 0.8125F;
            this.lblMeterNumber.Name = "lblMeterNumber";
            this.lblMeterNumber.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblMeterNumber.Text = "Meter";
            this.lblMeterNumber.Top = 0.5F;
            this.lblMeterNumber.Width = 0.5F;
            // 
            // lblBook
            // 
            this.lblBook.Height = 0.1875F;
            this.lblBook.HyperLink = null;
            this.lblBook.Left = 4.875F;
            this.lblBook.Name = "lblBook";
            this.lblBook.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblBook.Text = "Book";
            this.lblBook.Top = 0.5F;
            this.lblBook.Width = 0.4F;
            // 
            // lblSeq
            // 
            this.lblSeq.Height = 0.1875F;
            this.lblSeq.HyperLink = null;
            this.lblSeq.Left = 5.25F;
            this.lblSeq.Name = "lblSeq";
            this.lblSeq.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblSeq.Text = "Seq";
            this.lblSeq.Top = 0.5F;
            this.lblSeq.Width = 0.375F;
            // 
            // Label1
            // 
            this.Label1.Height = 0.25F;
            this.Label1.HyperLink = null;
            this.Label1.Left = 0F;
            this.Label1.Name = "Label1";
            this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" +
    "o-char-set: 0";
            this.Label1.Text = "Meter Error Report";
            this.Label1.Top = 0F;
            this.Label1.Width = 7.5F;
            // 
            // lblMuniName
            // 
            this.lblMuniName.Height = 0.1875F;
            this.lblMuniName.HyperLink = null;
            this.lblMuniName.Left = 0F;
            this.lblMuniName.Name = "lblMuniName";
            this.lblMuniName.Style = "font-family: \'Tahoma\'; text-align: left";
            this.lblMuniName.Text = null;
            this.lblMuniName.Top = 0F;
            this.lblMuniName.Width = 2.625F;
            // 
            // lblTime
            // 
            this.lblTime.Height = 0.2F;
            this.lblTime.HyperLink = null;
            this.lblTime.Left = 0F;
            this.lblTime.Name = "lblTime";
            this.lblTime.Style = "font-family: \'Tahoma\'; text-align: left";
            this.lblTime.Text = null;
            this.lblTime.Top = 0.2F;
            this.lblTime.Width = 1F;
            // 
            // lblDate
            // 
            this.lblDate.Height = 0.1875F;
            this.lblDate.HyperLink = null;
            this.lblDate.Left = 6.5F;
            this.lblDate.Name = "lblDate";
            this.lblDate.Style = "font-family: \'Tahoma\'";
            this.lblDate.Text = null;
            this.lblDate.Top = 0F;
            this.lblDate.Width = 1F;
            // 
            // lblPageNumber
            // 
            this.lblPageNumber.Height = 0.1875F;
            this.lblPageNumber.HyperLink = null;
            this.lblPageNumber.Left = 6.5F;
            this.lblPageNumber.Name = "lblPageNumber";
            this.lblPageNumber.Style = "font-family: \'Tahoma\'";
            this.lblPageNumber.Text = null;
            this.lblPageNumber.Top = 0.1875F;
            this.lblPageNumber.Width = 1F;
            // 
            // Line2
            // 
            this.Line2.Height = 0F;
            this.Line2.Left = 0F;
            this.Line2.LineWeight = 1F;
            this.Line2.Name = "Line2";
            this.Line2.Top = 0.6875F;
            this.Line2.Width = 7.5F;
            this.Line2.X1 = 0F;
            this.Line2.X2 = 7.5F;
            this.Line2.Y1 = 0.6875F;
            this.Line2.Y2 = 0.6875F;
            // 
            // lblService
            // 
            this.lblService.Height = 0.1875F;
            this.lblService.HyperLink = null;
            this.lblService.Left = 5.625F;
            this.lblService.Name = "lblService";
            this.lblService.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblService.Text = "SVR";
            this.lblService.Top = 0.5F;
            this.lblService.Width = 0.375F;
            // 
            // lblSize
            // 
            this.lblSize.Height = 0.1875F;
            this.lblSize.HyperLink = null;
            this.lblSize.Left = 6F;
            this.lblSize.Name = "lblSize";
            this.lblSize.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblSize.Text = "Size";
            this.lblSize.Top = 0.5F;
            this.lblSize.Width = 0.4375F;
            // 
            // lblDigits
            // 
            this.lblDigits.Height = 0.1875F;
            this.lblDigits.HyperLink = null;
            this.lblDigits.Left = 6.4375F;
            this.lblDigits.Name = "lblDigits";
            this.lblDigits.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblDigits.Text = "Dig";
            this.lblDigits.Top = 0.5F;
            this.lblDigits.Width = 0.3125F;
            // 
            // lblUnits
            // 
            this.lblUnits.Height = 0.1875F;
            this.lblUnits.HyperLink = null;
            this.lblUnits.Left = 7F;
            this.lblUnits.Name = "lblUnits";
            this.lblUnits.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: right; ddo" +
    "-char-set: 0";
            this.lblUnits.Text = "Units";
            this.lblUnits.Top = 0.5F;
            this.lblUnits.Width = 0.5F;
            // 
            // lblName
            // 
            this.lblName.Height = 0.1875F;
            this.lblName.HyperLink = null;
            this.lblName.Left = 1.3125F;
            this.lblName.Name = "lblName";
            this.lblName.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblName.Text = "Name";
            this.lblName.Top = 0.5F;
            this.lblName.Width = 0.5F;
            // 
            // lblCategory
            // 
            this.lblCategory.Height = 0.1875F;
            this.lblCategory.HyperLink = null;
            this.lblCategory.Left = 4.1875F;
            this.lblCategory.Name = "lblCategory";
            this.lblCategory.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblCategory.Text = "Category";
            this.lblCategory.Top = 0.5F;
            this.lblCategory.Width = 0.6875F;
            // 
            // lblCombine
            // 
            this.lblCombine.Height = 0.1875F;
            this.lblCombine.HyperLink = null;
            this.lblCombine.Left = 3.8125F;
            this.lblCombine.Name = "lblCombine";
            this.lblCombine.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblCombine.Text = "Cmb";
            this.lblCombine.Top = 0.5F;
            this.lblCombine.Width = 0.375F;
            // 
            // lblHeaderStatus
            // 
            this.lblHeaderStatus.Height = 0.1875F;
            this.lblHeaderStatus.HyperLink = null;
            this.lblHeaderStatus.Left = 6.75F;
            this.lblHeaderStatus.Name = "lblHeaderStatus";
            this.lblHeaderStatus.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; ddo-char-set: 0";
            this.lblHeaderStatus.Text = "BS";
            this.lblHeaderStatus.Top = 0.5F;
            this.lblHeaderStatus.Width = 0.25F;
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 0F;
            this.PageFooter.Name = "PageFooter";
            // 
            // arMeterCheck
            // 
            this.MasterReport = false;
            this.PageSettings.Margins.Bottom = 0.25F;
            this.PageSettings.Margins.Left = 0.5F;
            this.PageSettings.Margins.Right = 0.5F;
            this.PageSettings.Margins.Top = 0.25F;
            this.PageSettings.PaperHeight = 11F;
            this.PageSettings.PaperWidth = 8.5F;
            this.PrintWidth = 7.5F;
            this.ScriptLanguage = "VB.NET";
            this.Sections.Add(this.PageHeader);
            this.Sections.Add(this.Detail);
            this.Sections.Add(this.PageFooter);
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" +
            "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " +
            "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
            this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
            this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
            this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
            this.ReportEnd += new System.EventHandler(this.ActiveReport_ReportEnd);
            ((System.ComponentModel.ISupportInitialize)(this.fldAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldMeterNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldDigits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldCombine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fldStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblAccount)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMeterNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblBook)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblMuniName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblPageNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblDigits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblUnits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCategory)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblCombine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lblHeaderStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldAccount;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldMeterNumber;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSeq;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldBook;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldService;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldSize;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDigits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldUnits;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldName;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCategory;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCombine;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldStatus;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblAccount;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMeterNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblBook;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSeq;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTime;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPageNumber;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblService;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblSize;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDigits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblUnits;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblName;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCategory;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblCombine;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblHeaderStatus;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
	}
}
