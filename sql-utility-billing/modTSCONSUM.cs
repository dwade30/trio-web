﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.Runtime.InteropServices;
using Microsoft.VisualBasic.CompilerServices;
using fecherFoundation.VisualBasicLayer;
using modUTStatusPayments = Global.modUTFunctions;
using System.IO;
using System.Linq;
using System.Text;
using SharedApplication.Extensions;
using System.Text.RegularExpressions;
using GrapeCity.ActiveReports.Extensions;

namespace TWUT0000
{
	public class modTSCONSUM
	{
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Dave Wade               *
		// DATE           :               10/19/2004              *
		// *
		// MODIFIED BY    :               Jim Bertolino           *
		// LAST UPDATED   :               12/11/2006              *
		// ********************************************************
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct CONSUMPTIONRECORD
		{
			// vbPorter upgrade warning: AcctNumber As FixedString	OnWriteFCConvert.ToDouble(
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] AcctNumberCharArray;
			public string AcctNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(AcctNumberCharArray);
				}
				set
				{
					AcctNumberCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 25)]
			public char[] NameCharArray;
			public string Name
			{
				get
				{
					return FCUtils.FixedStringFromArray(NameCharArray);
				}
				set
				{
					NameCharArray = FCUtils.FixedStringToArray(value, 25);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space1CharArray;
			public string Space1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space1CharArray);
				}
				set
				{
					Space1CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public char[] BookCharArray;
			public string Book
			{
				get
				{
					return FCUtils.FixedStringFromArray(BookCharArray);
				}
				set
				{
					BookCharArray = FCUtils.FixedStringToArray(value, 4);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space2CharArray;
			public string Space2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space2CharArray);
				}
				set
				{
					Space2CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public char[] SequenceCharArray;
			public string Sequence
			{
				get
				{
					return FCUtils.FixedStringFromArray(SequenceCharArray);
				}
				set
				{
					SequenceCharArray = FCUtils.FixedStringToArray(value, 4);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space3CharArray;
			public string Space3
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space3CharArray);
				}
				set
				{
					Space3CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
			public char[] ConsumptionCharArray;
			public string Consumption
			{
				get
				{
					return FCUtils.FixedStringFromArray(ConsumptionCharArray);
				}
				set
				{
					ConsumptionCharArray = FCUtils.FixedStringToArray(value, 12);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space4CharArray;
			public string Space4
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space4CharArray);
				}
				set
				{
					Space4CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
			public char[] PreviousCharArray;
			public string Previous
			{
				get
				{
					return FCUtils.FixedStringFromArray(PreviousCharArray);
				}
				set
				{
					PreviousCharArray = FCUtils.FixedStringToArray(value, 12);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space5CharArray;
			public string Space5
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space5CharArray);
				}
				set
				{
					Space5CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
			public char[] CurrentCharArray;
			public string Current
			{
				get
				{
					return FCUtils.FixedStringFromArray(CurrentCharArray);
				}
				set
				{
					CurrentCharArray = FCUtils.FixedStringToArray(value, 12);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space6CharArray;
			public string Space6
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space6CharArray);
				}
				set
				{
					Space6CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
			public char[] LocationCharArray;
			public string Location
			{
				get
				{
					return FCUtils.FixedStringFromArray(LocationCharArray);
				}
				set
				{
					LocationCharArray = FCUtils.FixedStringToArray(value, 40);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public CONSUMPTIONRECORD(int unusedParam)
			{
				this.AcctNumberCharArray = new string(' ', 10).ToArray();
				this.NameCharArray = new string(' ', 25).ToArray();
				this.Space1CharArray = new string(' ', 1).ToArray();
				this.BookCharArray = new string(' ', 4).ToArray();
				this.Space2CharArray = new string(' ', 1).ToArray();
				this.SequenceCharArray = new string(' ', 4).ToArray();
				this.Space3CharArray = new string(' ', 1).ToArray();
				this.ConsumptionCharArray = new string(' ', 12).ToArray();
				this.Space4CharArray = new string(' ', 1).ToArray();
				this.PreviousCharArray = new string(' ', 12).ToArray();
				this.Space5CharArray = new string(' ', 1).ToArray();
				this.CurrentCharArray = new string(' ', 12).ToArray();
				this.Space6CharArray = new string(' ', 1).ToArray();
				this.LocationCharArray = new string(' ', 40).ToArray();
				this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct DOSCONSUMPTIONRECORD
		{
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] AcctNumberCharArray;
			public string AcctNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(AcctNumberCharArray);
				}
				set
				{
					AcctNumberCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
			public char[] NameCharArray;
			public string Name
			{
				get
				{
					return FCUtils.FixedStringFromArray(NameCharArray);
				}
				set
				{
					NameCharArray = FCUtils.FixedStringToArray(value, 26);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space1CharArray;
			public string Space1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space1CharArray);
				}
				set
				{
					Space1CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			// vbPorter upgrade warning: Book As FixedString	OnReadFCConvert.ToInt32(
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public char[] BookCharArray;
			public string Book
			{
				get
				{
					return FCUtils.FixedStringFromArray(BookCharArray);
				}
				set
				{
					BookCharArray = FCUtils.FixedStringToArray(value, 4);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space2CharArray;
			public string Space2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space2CharArray);
				}
				set
				{
					Space2CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			// vbPorter upgrade warning: Sequence As FixedString	OnReadFCConvert.ToInt32(
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public char[] SequenceCharArray;
			public string Sequence
			{
				get
				{
					return FCUtils.FixedStringFromArray(SequenceCharArray);
				}
				set
				{
					SequenceCharArray = FCUtils.FixedStringToArray(value, 4);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] Space3CharArray;
			public string Space3
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space3CharArray);
				}
				set
				{
					Space3CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
			public char[] ConsumptionCharArray;
			public string Consumption
			{
				get
				{
					return FCUtils.FixedStringFromArray(ConsumptionCharArray);
				}
				set
				{
					ConsumptionCharArray = FCUtils.FixedStringToArray(value, 12);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public DOSCONSUMPTIONRECORD(int unusedParam)
			{
				this.AcctNumberCharArray = new string(' ', 10).ToArray();
				this.NameCharArray = new string(' ', 26).ToArray();
				this.Space1CharArray = new string(' ', 1).ToArray();
				this.BookCharArray = new string(' ', 4).ToArray();
				this.Space2CharArray = new string(' ', 1).ToArray();
				this.SequenceCharArray = new string(' ', 4).ToArray();
				this.Space3CharArray = new string(' ', 1).ToArray();
				this.ConsumptionCharArray = new string(' ', 12).ToArray();
				this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct AquaAmericaInputFile
		{
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] LocationNumberCharArray;
			public string LocationNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(LocationNumberCharArray);
				}
				set
				{
					LocationNumberCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] AccountNumberCharArray;
			public string AccountNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(AccountNumberCharArray);
				}
				set
				{
					AccountNumberCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] ResidentNumberCharArray;
			public string ResidentNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(ResidentNumberCharArray);
				}
				set
				{
					ResidentNumberCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
			public char[] NameCharArray;
			public string Name
			{
				get
				{
					return FCUtils.FixedStringFromArray(NameCharArray);
				}
				set
				{
					NameCharArray = FCUtils.FixedStringToArray(value, 26);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
			public char[] AddressCharArray;
			public string Address
			{
				get
				{
					return FCUtils.FixedStringFromArray(AddressCharArray);
				}
				set
				{
					AddressCharArray = FCUtils.FixedStringToArray(value, 26);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] ConsumptionCharArray;
			public string Consumption
			{
				get
				{
					return FCUtils.FixedStringFromArray(ConsumptionCharArray);
				}
				set
				{
					ConsumptionCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] ReadingDateCharArray;
			public string ReadingDate
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingDateCharArray);
				}
				set
				{
					ReadingDateCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] InactiveCodeCharArray;
			public string InactiveCode
			{
				get
				{
					return FCUtils.FixedStringFromArray(InactiveCodeCharArray);
				}
				set
				{
					InactiveCodeCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] FillerCharArray;
			public string Filler
			{
				get
				{
					return FCUtils.FixedStringFromArray(FillerCharArray);
				}
				set
				{
					FillerCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// vbPorter upgrade warning: Reading As FixedString	OnWriteFCConvert.ToInt32(
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] ReadingCharArray;
			public string Reading
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingCharArray);
				}
				set
				{
					ReadingCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] ReadingTypeCharArray;
			public string ReadingType
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingTypeCharArray);
				}
				set
				{
					ReadingTypeCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public AquaAmericaInputFile(int unusedParam)
			{
				this.LocationNumberCharArray = new string(' ', 2).ToArray();
				this.AccountNumberCharArray = new string(' ', 6).ToArray();
				this.ResidentNumberCharArray = new string(' ', 2).ToArray();
				this.NameCharArray = new string(' ', 26).ToArray();
				this.AddressCharArray = new string(' ', 26).ToArray();
				this.ConsumptionCharArray = new string(' ', 9).ToArray();
				this.ReadingDateCharArray = new string(' ', 6).ToArray();
				this.InactiveCodeCharArray = new string(' ', 1).ToArray();
				this.FillerCharArray = new string(' ', 2).ToArray();
				this.ReadingCharArray = new string(' ', 9).ToArray();
				this.ReadingTypeCharArray = new string(' ', 1).ToArray();
				this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};
		// Public Type RVSInputFile   'This is the RVS format AMSD = Anson Madison Sewer Disctrict
		// LocationNumber      As String * 2
		// AccountNumber       As String * 6
		// ResidentNumber      As String * 2
		// Name                As String * 26
		// Address             As String * 26
		// Consumption         As String * 9
		// ReadingDate         As String * 6
		// InactiveCode        As String * 1
		// Filler              As String * 2
		// Reading             As String * 9
		// ReadingType         As String * 1
		// CRLF                As String * 2
		// End Type
		public struct RVSInputFile
		{
			// This is the RVS format AMSD = Anson Madison Sewer Disctrict
			public string Name;
			public string Address1;
			public string Address2;
			public string Address3;
			public string Zip;
			public string Location;
			public int Current;
			public int Previous;
			// vbPorter upgrade warning: Account As int	OnWrite(int, string)
			public int Account;
			public int Route;
			public string Code;
			public int Q1;
			public int Q2;
			public int Q3;
			public int Q4;
			public DateTime ReadingDate;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public RVSInputFile(int unusedParam)
			{
				this.Name = string.Empty;
				this.Address1 = string.Empty;
				this.Address2 = string.Empty;
				this.Address3 = string.Empty;
				this.Zip = string.Empty;
				this.Location = string.Empty;
				this.Current = 0;
				this.Previous = 0;
				this.Account = 0;
				this.Route = 0;
				this.Code = string.Empty;
				this.Q1 = 0;
				this.Q2 = 0;
				this.Q3 = 0;
				this.Q4 = 0;
				this.ReadingDate = default(DateTime);
			}
		};
		// MAL@20071019: Added new extract file type
		public struct EZREADERCONSUMPTIONRECORD
		{
			public string MeterSerial;
			public string Name;
			public string Location;
			public string Current;
			public string Previous;
			// vbPorter upgrade warning: Account As string	OnReadFCConvert.ToInt32(
			public string Account;
			// vbPorter upgrade warning: ReadingDate As string	OnWrite(DateTime, string)
			public string ReadingDate;
			public string CRLF;
			// vbPorter upgrade warning: Route As string	OnWrite(int, string)
			public string Route;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public EZREADERCONSUMPTIONRECORD(int unusedParam)
			{
				this.MeterSerial = string.Empty;
				this.Name = string.Empty;
				this.Location = string.Empty;
				this.Current = string.Empty;
				this.Previous = string.Empty;
				this.Account = string.Empty;
				this.ReadingDate = string.Empty;
				this.CRLF = string.Empty;
				this.Route = string.Empty;
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct BELFASTCONSUMPTIONRECORD
		{
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 7)]
			public char[] WaterAcctNumberCharArray;
			public string WaterAcctNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(WaterAcctNumberCharArray);
				}
				set
				{
					WaterAcctNumberCharArray = FCUtils.FixedStringToArray(value, 7);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
			public char[] Space1CharArray;
			public string Space1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space1CharArray);
				}
				set
				{
					Space1CharArray = FCUtils.FixedStringToArray(value, 16);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 19)]
			public char[] SewerAcctNumberCharArray;
			public string SewerAcctNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(SewerAcctNumberCharArray);
				}
				set
				{
					SewerAcctNumberCharArray = FCUtils.FixedStringToArray(value, 19);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] ConsumptionCharArray;
			public string Consumption
			{
				get
				{
					return FCUtils.FixedStringFromArray(ConsumptionCharArray);
				}
				set
				{
					ConsumptionCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public BELFASTCONSUMPTIONRECORD(int unusedParam)
			{
				this.WaterAcctNumberCharArray = new string(' ', 7).ToArray();
				this.Space1CharArray = new string(' ', 16).ToArray();
				this.SewerAcctNumberCharArray = new string(' ', 19).ToArray();
				this.ConsumptionCharArray = new string(' ', 10).ToArray();
				this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct OAKLANDCONSUMPTIONRECORD
		{
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
			public char[] SerialNumberCharArray;
			public string SerialNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(SerialNumberCharArray);
				}
				set
				{
					SerialNumberCharArray = FCUtils.FixedStringToArray(value, 8);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] Space1CharArray;
			public string Space1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space1CharArray);
				}
				set
				{
					Space1CharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
			public char[] OwnerNameCharArray;
			public string OwnerName
			{
				get
				{
					return FCUtils.FixedStringFromArray(OwnerNameCharArray);
				}
				set
				{
					OwnerNameCharArray = FCUtils.FixedStringToArray(value, 26);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
			public char[] OwnerAddressCharArray;
			public string OwnerAddress
			{
				get
				{
					return FCUtils.FixedStringFromArray(OwnerAddressCharArray);
				}
				set
				{
					OwnerAddressCharArray = FCUtils.FixedStringToArray(value, 26);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] ConsumptionCharArray;
			public string Consumption
			{
				get
				{
					return FCUtils.FixedStringFromArray(ConsumptionCharArray);
				}
				set
				{
					ConsumptionCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] ReadingDateCharArray;
			public string ReadingDate
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingDateCharArray);
				}
				set
				{
					ReadingDateCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
			public char[] Space2CharArray;
			public string Space2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Space2CharArray);
				}
				set
				{
					Space2CharArray = FCUtils.FixedStringToArray(value, 3);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public OAKLANDCONSUMPTIONRECORD(int unusedParam)
			{
				this.SerialNumberCharArray = new string(' ', 8).ToArray();
				this.Space1CharArray = new string(' ', 2).ToArray();
				this.OwnerNameCharArray = new string(' ', 26).ToArray();
				this.OwnerAddressCharArray = new string(' ', 26).ToArray();
				this.ConsumptionCharArray = new string(' ', 9).ToArray();
				this.ReadingDateCharArray = new string(' ', 6).ToArray();
				this.Space2CharArray = new string(' ', 3).ToArray();
				this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		public struct OaklandMeterInfo
		{
			public int AccountKey;
			public int AccountNumber;
			public int MeterKey;
			public string SerialNumber;
			public int Book;
			public int Sequence;
			public string OwnerName;
			public DateTime ReadingDate;
			public int PreviousReading;
			// vbPorter upgrade warning: Consumption As int	OnWrite(double, short)
			public int Consumption;
			public int CurrentReading;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public OaklandMeterInfo(int unusedParam)
			{
				this.AccountKey = 0;
				this.AccountNumber = 0;
				this.MeterKey = 0;
				this.SerialNumber = string.Empty;
				this.Book = 0;
				this.Sequence = 0;
				this.OwnerName = string.Empty;
				this.ReadingDate = default(DateTime);
				this.PreviousReading = 0;
				this.Consumption = 0;
				this.CurrentReading = 0;
			}
		};
		// kk 01182013 trout-896  Update the format to match the Spec supplied by Badger
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct BadgerFormat
		{
			// vbPorter upgrade warning: zFiller0 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
			public char[] zFiller0CharArray;
			public string zFiller0
			{
				get
				{
					return FCUtils.FixedStringFromArray(zFiller0CharArray);
				}
				set
				{
					zFiller0CharArray = FCUtils.FixedStringToArray(value, 8);
				}
			}
			// vbPorter upgrade warning: CustomerName As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
			public char[] CustomerNameCharArray;
			public string CustomerName
			{
				get
				{
					return FCUtils.FixedStringFromArray(CustomerNameCharArray);
				}
				set
				{
					CustomerNameCharArray = FCUtils.FixedStringToArray(value, 20);
				}
			}
			// vbPorter upgrade warning: ServiceAddress As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
			public char[] ServiceAddressCharArray;
			public string ServiceAddress
			{
				get
				{
					return FCUtils.FixedStringFromArray(ServiceAddressCharArray);
				}
				set
				{
					ServiceAddressCharArray = FCUtils.FixedStringToArray(value, 20);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] ModuleSerialCharArray;
			public string ModuleSerial
			{
				get
				{
					return FCUtils.FixedStringFromArray(ModuleSerialCharArray);
				}
				set
				{
					ModuleSerialCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			// vbPorter upgrade warning: zFiller1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public char[] zFiller1CharArray;
			public string zFiller1
			{
				get
				{
					return FCUtils.FixedStringFromArray(zFiller1CharArray);
				}
				set
				{
					zFiller1CharArray = FCUtils.FixedStringToArray(value, 4);
				}
			}
			// vbPorter upgrade warning: ReadType As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] ReadTypeCharArray;
			public string ReadType
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadTypeCharArray);
				}
				set
				{
					ReadTypeCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] ServiceCodeCharArray;
			public string ServiceCode
			{
				get
				{
					return FCUtils.FixedStringFromArray(ServiceCodeCharArray);
				}
				set
				{
					ServiceCodeCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			// vbPorter upgrade warning: MeterType As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] MeterTypeCharArray;
			public string MeterType
			{
				get
				{
					return FCUtils.FixedStringFromArray(MeterTypeCharArray);
				}
				set
				{
					MeterTypeCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			// vbPorter upgrade warning: NetworkID As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] NetworkIDCharArray;
			public string NetworkID
			{
				get
				{
					return FCUtils.FixedStringFromArray(NetworkIDCharArray);
				}
				set
				{
					NetworkIDCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] MeterSerialCharArray;
			public string MeterSerial
			{
				get
				{
					return FCUtils.FixedStringFromArray(MeterSerialCharArray);
				}
				set
				{
					MeterSerialCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			// vbPorter upgrade warning: zFiller2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] zFiller2CharArray;
			public string zFiller2
			{
				get
				{
					return FCUtils.FixedStringFromArray(zFiller2CharArray);
				}
				set
				{
					zFiller2CharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			// vbPorter upgrade warning: HighReadAudit As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] HighReadAuditCharArray;
			public string HighReadAudit
			{
				get
				{
					return FCUtils.FixedStringFromArray(HighReadAuditCharArray);
				}
				set
				{
					HighReadAuditCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			// vbPorter upgrade warning: LowReadAudit As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] LowReadAuditCharArray;
			public string LowReadAudit
			{
				get
				{
					return FCUtils.FixedStringFromArray(LowReadAuditCharArray);
				}
				set
				{
					LowReadAuditCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			// vbPorter upgrade warning: CurrentReading As FixedString	OnWrite(string, double)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] CurrentReadingCharArray;
			public string CurrentReading
			{
				get
				{
					return FCUtils.FixedStringFromArray(CurrentReadingCharArray);
				}
				set
				{
					CurrentReadingCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			// vbPorter upgrade warning: ReadTime As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
			public char[] ReadTimeCharArray;
			public string ReadTime
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadTimeCharArray);
				}
				set
				{
					ReadTimeCharArray = FCUtils.FixedStringToArray(value, 8);
				}
			}
			// vbPorter upgrade warning: ReadMethod As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] ReadMethodCharArray;
			public string ReadMethod
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadMethodCharArray);
				}
				set
				{
					ReadMethodCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			// vbPorter upgrade warning: TamperMode As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] TamperModeCharArray;
			public string TamperMode
			{
				get
				{
					return FCUtils.FixedStringFromArray(TamperModeCharArray);
				}
				set
				{
					TamperModeCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			// vbPorter upgrade warning: AlertCode As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] AlertCodeCharArray;
			public string AlertCode
			{
				get
				{
					return FCUtils.FixedStringFromArray(AlertCodeCharArray);
				}
				set
				{
					AlertCodeCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
			public char[] RouteNumberCharArray;
			public string RouteNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(RouteNumberCharArray);
				}
				set
				{
					RouteNumberCharArray = FCUtils.FixedStringToArray(value, 4);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 15)]
			public char[] AccountNumberCharArray;
			public string AccountNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(AccountNumberCharArray);
				}
				set
				{
					AccountNumberCharArray = FCUtils.FixedStringToArray(value, 15);
				}
			}
			// vbPorter upgrade warning: ReadDate As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
			public char[] ReadDateCharArray;
			public string ReadDate
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadDateCharArray);
				}
				set
				{
					ReadDateCharArray = FCUtils.FixedStringToArray(value, 8);
				}
			}
			// vbPorter upgrade warning: DeviceCode As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] DeviceCodeCharArray;
			public string DeviceCode
			{
				get
				{
					return FCUtils.FixedStringFromArray(DeviceCodeCharArray);
				}
				set
				{
					DeviceCodeCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
			public char[] LatitudeCharArray;
			public string Latitude
			{
				get
				{
					return FCUtils.FixedStringFromArray(LatitudeCharArray);
				}
				set
				{
					LatitudeCharArray = FCUtils.FixedStringToArray(value, 12);
				}
			}
			// vbPorter upgrade warning: zFiller3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] zFiller3CharArray;
			public string zFiller3
			{
				get
				{
					return FCUtils.FixedStringFromArray(zFiller3CharArray);
				}
				set
				{
					zFiller3CharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] TestCircleCodeCharArray;
			public string TestCircleCode
			{
				get
				{
					return FCUtils.FixedStringFromArray(TestCircleCodeCharArray);
				}
				set
				{
					TestCircleCodeCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] ReadingSequenceCharArray;
			public string ReadingSequence
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingSequenceCharArray);
				}
				set
				{
					ReadingSequenceCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			// vbPorter upgrade warning: MfgModel As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
			public char[] MfgModelCharArray;
			public string MfgModel
			{
				get
				{
					return FCUtils.FixedStringFromArray(MfgModelCharArray);
				}
				set
				{
					MfgModelCharArray = FCUtils.FixedStringToArray(value, 20);
				}
			}
			// vbPorter upgrade warning: UtilityUseOnly As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 30)]
			public char[] UtilityUseOnlyCharArray;
			public string UtilityUseOnly
			{
				get
				{
					return FCUtils.FixedStringFromArray(UtilityUseOnlyCharArray);
				}
				set
				{
					UtilityUseOnlyCharArray = FCUtils.FixedStringToArray(value, 30);
				}
			}
			// vbPorter upgrade warning: ReaderID As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
			public char[] ReaderIDCharArray;
			public string ReaderID
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReaderIDCharArray);
				}
				set
				{
					ReaderIDCharArray = FCUtils.FixedStringToArray(value, 3);
				}
			}
			// vbPorter upgrade warning: ReadCode1 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] ReadCode1CharArray;
			public string ReadCode1
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadCode1CharArray);
				}
				set
				{
					ReadCode1CharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// vbPorter upgrade warning: ReadCode2 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] ReadCode2CharArray;
			public string ReadCode2
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadCode2CharArray);
				}
				set
				{
					ReadCode2CharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// vbPorter upgrade warning: ReadCode3 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] ReadCode3CharArray;
			public string ReadCode3
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadCode3CharArray);
				}
				set
				{
					ReadCode3CharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] LocationCharArray;
			public string Location
			{
				get
				{
					return FCUtils.FixedStringFromArray(LocationCharArray);
				}
				set
				{
					LocationCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			// vbPorter upgrade warning: zFiller4 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] zFiller4CharArray;
			public string zFiller4
			{
				get
				{
					return FCUtils.FixedStringFromArray(zFiller4CharArray);
				}
				set
				{
					zFiller4CharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 12)]
			public char[] LongitudeCharArray;
			public string Longitude
			{
				get
				{
					return FCUtils.FixedStringFromArray(LongitudeCharArray);
				}
				set
				{
					LongitudeCharArray = FCUtils.FixedStringToArray(value, 12);
				}
			}
			// vbPorter upgrade warning: zFiller5 As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] zFiller5CharArray;
			public string zFiller5
			{
				get
				{
					return FCUtils.FixedStringFromArray(zFiller5CharArray);
				}
				set
				{
					zFiller5CharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// vbPorter upgrade warning: CRLF As FixedString	OnWrite(string)
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public BadgerFormat(int unusedParam)
			{
				this.zFiller0CharArray = new string(' ', 8).ToArray();
				this.CustomerNameCharArray = new string(' ', 20).ToArray();
				this.ServiceAddressCharArray = new string(' ', 20).ToArray();
				this.ModuleSerialCharArray = new string(' ', 10).ToArray();
				this.zFiller1CharArray = new string(' ', 4).ToArray();
				this.ReadTypeCharArray = new string(' ', 1).ToArray();
				this.ServiceCodeCharArray = new string(' ', 1).ToArray();
				this.MeterTypeCharArray = new string(' ', 1).ToArray();
				this.NetworkIDCharArray = new string(' ', 2).ToArray();
				this.MeterSerialCharArray = new string(' ', 9).ToArray();
				this.zFiller2CharArray = new string(' ', 1).ToArray();
				this.HighReadAuditCharArray = new string(' ', 9).ToArray();
				this.LowReadAuditCharArray = new string(' ', 9).ToArray();
				this.CurrentReadingCharArray = new string(' ', 9).ToArray();
				this.ReadTimeCharArray = new string(' ', 8).ToArray();
				this.ReadMethodCharArray = new string(' ', 1).ToArray();
				this.TamperModeCharArray = new string(' ', 1).ToArray();
				this.AlertCodeCharArray = new string(' ', 2).ToArray();
				this.RouteNumberCharArray = new string(' ', 4).ToArray();
				this.AccountNumberCharArray = new string(' ', 15).ToArray();
				this.ReadDateCharArray = new string(' ', 8).ToArray();
				this.DeviceCodeCharArray = new string(' ', 1).ToArray();
				this.LatitudeCharArray = new string(' ', 12).ToArray();
				this.zFiller3CharArray = new string(' ', 6).ToArray();
				this.TestCircleCodeCharArray = new string(' ', 2).ToArray();
				this.ReadingSequenceCharArray = new string(' ', 6).ToArray();
				this.MfgModelCharArray = new string(' ', 20).ToArray();
				this.UtilityUseOnlyCharArray = new string(' ', 30).ToArray();
				this.ReaderIDCharArray = new string(' ', 3).ToArray();
				this.ReadCode1CharArray = new string(' ', 2).ToArray();
				this.ReadCode2CharArray = new string(' ', 2).ToArray();
				this.ReadCode3CharArray = new string(' ', 2).ToArray();
				this.LocationCharArray = new string(' ', 9).ToArray();
				this.zFiller4CharArray = new string(' ', 2).ToArray();
				this.LongitudeCharArray = new string(' ', 12).ToArray();
				this.zFiller5CharArray = new string(' ', 2).ToArray();
				this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};
		// kk 03132013 trouts-5  Bangor's billing file is not a fixed length format
		// This is just a record format to store the error records
		public struct BangorErrorFormat
		{
			// vbPorter upgrade warning: AccountNumber As FixedString	OnWrite(string)
			public string AccountNumber;
			// vbPorter upgrade warning: CustomerName As FixedString	OnWrite(string)
			public string CustomerName;
			// vbPorter upgrade warning: ServiceAddress As FixedString	OnWrite(string)
			public string ServiceAddress;
			// vbPorter upgrade warning: BookNum As FixedString	OnWrite(string)
			public string BookNum;
			// vbPorter upgrade warning: CurrentReading As FixedString	OnWriteFCConvert.ToInt32(
			public string CurrentReading;
			// vbPorter upgrade warning: ReadDate As FixedString	OnWrite(string)
			public string ReadDate;
			// vbPorter upgrade warning: NDSSequence As FixedString	OnWrite(string)
			public string NDSSequence;
			//FC:FINAL:RPU - Use custom constructor to initialize fields
			public BangorErrorFormat(int unusedParam)
			{
				this.AccountNumber = new string(' ', 15);
				this.CustomerName = new string(' ', 20);
				this.ServiceAddress = new string(' ', 20);
				this.BookNum = new string(' ', 4);
				this.CurrentReading = new string(' ', 9);
				this.ReadDate = new string(' ', 8);
				this.NDSSequence = new string(' ', 6);
			}
		};
		// kk 07172013 trout-5  Bangor's billing file meter info
		public struct BangorMeters
		{
			public int PrevReading;
			// vbPorter upgrade warning: CurReading As int	OnRead(FixedString)
			public int CurReading;
			public int Consumption;
			public int MeterNumber;
			public string Category;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public BangorMeters(int unusedParam)
			{
				this.PrevReading = 0;
				this.CurReading = 0;
				this.Consumption = 0;
				this.MeterNumber = 0;
				this.Category = string.Empty;
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct MillinocketInputFile
		{
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] LocationNumberCharArray;
			public string LocationNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(LocationNumberCharArray);
				}
				set
				{
					LocationNumberCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] AccountNumberCharArray;
			public string AccountNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(AccountNumberCharArray);
				}
				set
				{
					AccountNumberCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] ResidentNumberCharArray;
			public string ResidentNumber
			{
				get
				{
					return FCUtils.FixedStringFromArray(ResidentNumberCharArray);
				}
				set
				{
					ResidentNumberCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
			public char[] NameCharArray;
			public string Name
			{
				get
				{
					return FCUtils.FixedStringFromArray(NameCharArray);
				}
				set
				{
					NameCharArray = FCUtils.FixedStringToArray(value, 26);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 26)]
			public char[] AddressCharArray;
			public string Address
			{
				get
				{
					return FCUtils.FixedStringFromArray(AddressCharArray);
				}
				set
				{
					AddressCharArray = FCUtils.FixedStringToArray(value, 26);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 9)]
			public char[] ConsumptionCharArray;
			public string Consumption
			{
				get
				{
					return FCUtils.FixedStringFromArray(ConsumptionCharArray);
				}
				set
				{
					ConsumptionCharArray = FCUtils.FixedStringToArray(value, 9);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] ReadingDateCharArray;
			public string ReadingDate
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingDateCharArray);
				}
				set
				{
					ReadingDateCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
			public char[] InactiveCodeCharArray;
			public string InactiveCode
			{
				get
				{
					return FCUtils.FixedStringFromArray(InactiveCodeCharArray);
				}
				set
				{
					InactiveCodeCharArray = FCUtils.FixedStringToArray(value, 1);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] FillerCharArray;
			public string Filler
			{
				get
				{
					return FCUtils.FixedStringFromArray(FillerCharArray);
				}
				set
				{
					FillerCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			// Reading             As String * 9
			// ReadingType         As String * 1
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 2)]
			public char[] CRLFCharArray;
			public string CRLF
			{
				get
				{
					return FCUtils.FixedStringFromArray(CRLFCharArray);
				}
				set
				{
					CRLFCharArray = FCUtils.FixedStringToArray(value, 2);
				}
			}
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public MillinocketInputFile(int unusedParam)
			{
				this.LocationNumberCharArray = new string(' ', 2).ToArray();
				this.AccountNumberCharArray = new string(' ', 6).ToArray();
				this.ResidentNumberCharArray = new string(' ', 2).ToArray();
				this.NameCharArray = new string(' ', 26).ToArray();
				this.AddressCharArray = new string(' ', 26).ToArray();
				this.ConsumptionCharArray = new string(' ', 9).ToArray();
				this.ReadingDateCharArray = new string(' ', 6).ToArray();
				this.InactiveCodeCharArray = new string(' ', 1).ToArray();
                this.FillerCharArray = new string(' ', 2).ToArray();
                this.CRLFCharArray = new string(' ', 2).ToArray();
			}
		};

		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		public struct LisbonInputFile
		{
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
			public char[] BookCharArray;
			public string Book
			{
				get
				{
					return FCUtils.FixedStringFromArray(BookCharArray);
				}
				set
				{
					BookCharArray = FCUtils.FixedStringToArray(value, 10);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
			public char[] SequenceCharArray;
			public string Sequence
			{
				get
				{
					return FCUtils.FixedStringFromArray(SequenceCharArray);
				}
				set
				{
					SequenceCharArray = FCUtils.FixedStringToArray(value, 8);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 49)]
			public char[] Filler1CharArray;
			public string Filler1
			{
				get
				{
					return FCUtils.FixedStringFromArray(Filler1CharArray);
				}
				set
				{
					Filler1CharArray = FCUtils.FixedStringToArray(value, 49);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 21)]
			public char[] ReadingCharArray;
			public string Reading
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingCharArray);
				}
				set
				{
					ReadingCharArray = FCUtils.FixedStringToArray(value, 21);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 47)]
			public char[] Filler2CharArray;
			public string Filler2
			{
				get
				{
					return FCUtils.FixedStringFromArray(Filler2CharArray);
				}
				set
				{
					Filler2CharArray = FCUtils.FixedStringToArray(value, 47);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 6)]
			public char[] ReadingDateCharArray;
			public string ReadingDate
			{
				get
				{
					return FCUtils.FixedStringFromArray(ReadingDateCharArray);
				}
				set
				{
					ReadingDateCharArray = FCUtils.FixedStringToArray(value, 6);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 301)]
			public char[] Filler3CharArray;
			public string Filler3
			{
				get
				{
					return FCUtils.FixedStringFromArray(Filler3CharArray);
				}
				set
				{
					Filler3CharArray = FCUtils.FixedStringToArray(value, 301);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 25)]
			public char[] XRef1CharArray;
			public string XRef1
			{
				get
				{
					return FCUtils.FixedStringFromArray(XRef1CharArray);
				}
				set
				{
					XRef1CharArray = FCUtils.FixedStringToArray(value, 25);
				}
			}
			[MarshalAs(UnmanagedType.ByValArray, SizeConst = 116)]
			public char[] FillerCharArray;
			public string Filler
			{
				get
				{
					return FCUtils.FixedStringFromArray(FillerCharArray);
				}
				set
				{
					FillerCharArray = FCUtils.FixedStringToArray(value, 116);
				}
			}
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public LisbonInputFile(int unusedParam)
			{
				this.BookCharArray = new string(' ', 10).ToArray();
				this.SequenceCharArray = new string(' ', 8).ToArray();
				this.Filler1CharArray = new string(' ', 49).ToArray();
				this.ReadingCharArray = new string(' ', 21).ToArray();
				this.Filler2CharArray = new string(' ', 47).ToArray();
				this.ReadingDateCharArray = new string(' ', 6).ToArray();
				this.ReadingDateCharArray = new string(' ', 6).ToArray();
				this.Filler3CharArray = new string(' ', 301).ToArray();
				this.XRef1CharArray = new string(' ', 25).ToArray();
				this.FillerCharArray = new string(' ', 116).ToArray();
			}
		};

		public struct HampdenImportFile
		{
			public string ServiceID;
			public string CurrentReading;
			public string PreviousReading;
			public string Usage;
			public string BillDate;
			public string DueDate;
			public string Name;
			public string SecondName;
			public string MailingAddress;
			public string Location;
			public string Route;
			public string ReadingDate;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public HampdenImportFile(int unusedParam)
			{
				this.ServiceID = string.Empty;
				this.CurrentReading = string.Empty;
				this.PreviousReading = string.Empty;
				this.Usage = string.Empty;
				this.BillDate = string.Empty;
				this.DueDate = string.Empty;
				this.Name = string.Empty;
				this.SecondName = string.Empty;
				this.MailingAddress = string.Empty;
				this.Location = string.Empty;
				this.Route = string.Empty;
				this.ReadingDate = string.Empty;
			}
		};

		public struct BookSeq
		{
			public int Book;
			public int Seq;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public BookSeq(int unusedParam)
			{
				this.Book = 0;
				this.Seq = 0;
			}
		};

        public struct TopshamImportFile
        {
            public string AccountNumber;
            public string SerialNumber;
            public string ReadingDate;
            // vbPorter upgrade warning: Reading As string	OnWrite(int)
            public string Reading;
        };

        public static void ConvertTiSalesExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
                string strMtrSQL = "";
                clsDRWrapper rsMeterInfo = new clsDRWrapper();
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.exp|*.exp";
					MDIParent.InstancePtr.CommonDialog1.FilterIndex = 0;
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "EXP")
						{
							ans = MessageBox.Show("You must select a .exp file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				// ChDir strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				FCFileSystem.FileClose(1);
				FCFileSystem.FileClose(2);
				FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				datDefaultReading = DateTime.Today;
				object strInput = datDefaultReading;
				if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					counter = 1;
					while (!FCFileSystem.EOF(1))
					{
						strRecord = FCFileSystem.LineInput(1);
                        if (modGlobalConstants.Statics.MuniName.ToLower().StartsWith("winthrop util"))
                        {
                            strMtrSQL = "select * from metertable where booknumber = " + Conversion.Val(Strings.Mid(strRecord, 1, 4).Trim()) + " and left(Sequence,4) = '" + Conversion.Val(Strings.Mid(strRecord,15,4).Trim()) + "'";
                            if (Conversion.Val(Strings.Mid(strRecord, 19, 2).Trim()) > 0)
                            {
                                strMtrSQL += " and MeterNumber = " + Conversion.Val(Strings.Mid(strRecord, 19, 2));
                            }

                            rsMeterInfo.OpenRecordset(strMtrSQL, "UtilityBilling");
                            if (!rsMeterInfo.EndOfFile())
                            {
                                strLine = rsMeterInfo.Get_Fields_Int32("id").ToString().PadLeft(6, '0');
                                strLine += Strings.Mid(strRecord, 1, 4);
                                strLine += Strings.Mid(strRecord, 15, 4);
                                strLine += Strings.Mid(strRecord, 69, 10).Right(6);
                            }
                            else
                            {
                                MessageBox.Show(
                                    @"Could not find book / sequence :" +
                                    Conversion.Val(Strings.Mid(strRecord, 1, 4).Trim()) + @" / " + Conversion.Val(Strings.Mid(strRecord,15,4).Trim()), "Error Converting File",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {

                            strLine = Strings.Format(counter, "000000");
                            strLine += Strings.Mid(strRecord, 1, 4);
                            strLine += Strings.Mid(strRecord, 15, 4);
                            rsMeterInfo.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber = " +
                                                      FCConvert.ToString(
                                                          Conversion.Val(Strings.Trim(Strings.Mid(strRecord, 1, 4)))) +
                                                      " AND Sequence = " +
                                                      FCConvert.ToString(
                                                          Conversion.Val(Strings.Trim(Strings.Mid(strRecord, 15, 4)))));
                            if (rsMeterInfo.EndOfFile() != true && rsMeterInfo.BeginningOfFile() != true)
                            {
                                // Corey 06/24/2009 Why would we ever just shove the previous reading in here!?!?
                                // strLine = strLine & Format(rsMeterInfo.Fields("PreviousReading"), "000000")
                                strLine += Strings.Right(Strings.Mid(strRecord, 69, 10), 6);
                            }
                            else
                            {
                                MessageBox.Show(
                                    "Could not find book / sequence :" +
                                    FCConvert.ToString(Conversion.Val(Strings.Trim(Strings.Mid(strRecord, 1, 4)))) +
                                    " / " + FCConvert.ToString(
                                        Conversion.Val(Strings.Trim(Strings.Mid(strRecord, 15, 4)))),
                                    "Error Converting File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                FCFileSystem.FileClose();
                                return;
                            }
                        }
                        strLine += Strings.Mid(strRecord, 136, 6);
						FCFileSystem.PrintLine(2, strLine);
						counter += 1;
					}
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
                    //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From Ti Sales", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");

                }
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting Ti Sales File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ConvertPrescottExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				int lngAcct = 0;
				int lngMeter = 0;
				string strBook = "";
				string strSeq = "";
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//FC:FINAL:CHN - issue #1083: Unknown error.
				MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					//FC:FINAL:CHN - issue #1083: Unknown error.
					try
					{
						MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					}
					catch
					{
					}
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				// ChDir strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				FCFileSystem.FileClose(1);
				FCFileSystem.FileClose(2);
				FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				datDefaultReading = DateTime.Today;
				object strInput = datDefaultReading;
				if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					//FC:FINAL:DDU:converted back to correct variable
					datDefaultReading = FCConvert.ToDateTime(strInput);
					counter = 1;
					while (!FCFileSystem.EOF(1))
					{
						strRecord = FCFileSystem.LineInput(1);
						if (Strings.Trim(strRecord) != "")
						{
							if (Convert.ToByte(Strings.Left(strRecord, 1)[0]) != 0)
							{
								lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Left(strRecord, 6))));
								lngMeter = FCConvert.ToInt32(Math.Round(Conversion.Val(Strings.Mid(strRecord, 11, 3))));
								if (lngAcct > 0)
								{
									rsMeterInfo.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAcct)) + " AND MeterNumber = " + FCConvert.ToString(lngMeter), modExtraModules.strUTDatabase);
									if (rsMeterInfo.EndOfFile() != true && rsMeterInfo.BeginningOfFile() != true)
									{
										strLine = Strings.Format(lngAcct, "000000");
										strLine += Strings.Format(rsMeterInfo.Get_Fields_Int32("BookNumber"), "0000");
										// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
										strLine += Strings.Format(rsMeterInfo.Get_Fields("Sequence"), "0000");
										strLine += Strings.Mid(strRecord, 21, 10);
										// this is the reading
										if (Information.IsDate(Strings.Mid(strRecord, 31, 8)))
										{
											strLine += Strings.Mid(strRecord, 31, 8);
											// this is the reading date
										}
										else
										{
											strLine += Strings.Format(datDefaultReading, "MMddyyyy");
											// this is the reading date
										}
										FCFileSystem.PrintLine(2, strLine);
										counter += 1;
									}
									else
									{
										MessageBox.Show("Could not find Account / Meter : " + FCConvert.ToString(lngAcct) + " / " + FCConvert.ToString(lngMeter), "Error Converting File", MessageBoxButtons.OK, MessageBoxIcon.Warning);
										// Close
										// Exit Sub
									}
								}
							}
						}
					}
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
                    //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From Prescott Sales", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                }
				return;
			}
			catch (Exception ex)
			{
				
				FCFileSystem.FileClose(1);
				FCFileSystem.FileClose(2);
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting Prescott Sales File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ConvertTRIOWindowsExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				// vbPorter upgrade warning: counter As short --> As int	OnWrite(short, double)
				int counter;
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				string strAcctNumber = "";
				int lngBook = 0;
				int lngSeq = 0;
				int lngMKey = 0;
				int lngErrorIndex = 0;
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					// ChDir strCurDir
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				FCFileSystem.FileClose(1);
				FCFileSystem.FileClose(2);
				FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.CR));
				FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				datDefaultReading = DateTime.Today;
				object strInput = datDefaultReading;
				if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					//FC:FINAL:DDU:converted back to correct variable
					datDefaultReading = FCConvert.ToDateTime(strInput);
					for (counter = 1; counter <= FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.CR)); counter++)
					{
						lngBook = 0;
						lngSeq = 0;
						lngMKey = 0;
						FCFileSystem.FileGet(1, ref Statics.CR, counter);
						// kk 10212013  Use whole account number for Gardiner   TROUTS-42
						if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
						{
							int lngPrev = 0;
                            if (GetMeterInformation(Statics.CR.AcctNumber, ref lngBook, ref lngSeq, ref lngMKey, ref lngPrev))
							{
								strLine = Strings.Format(lngMKey, "000000");
								strLine += Strings.Format(lngBook, "0000");
								strLine += Strings.Format(lngSeq, "0000");
								if (Strings.Format(Statics.CR.Current, "000000").Length <= 6)
								{
									strLine += Strings.Format(Statics.CR.Current, "000000");
								}
								else
								{
									strLine += Strings.Left(Strings.Format(Statics.CR.Current, "000000"), 6);
									// <--- this is a hack to fix a 7 digit reading in Anson Water, when we get rid of the last DOS town then I can change this to 9 digits
								}
								//FC:FINAL:MSH - incorrect date format (same with internal issue #914)
								strLine += Strings.Format(datDefaultReading, "MMddyy");
								FCFileSystem.PrintLine(2, strLine);
							}
							else
							{
								Array.Resize(ref Statics.CRMeterImportError, lngErrorIndex + 1);
								//Application.DoEvents();
								Statics.CRMeterImportError[lngErrorIndex].AcctNumber = FCConvert.ToString(Conversion.Val(Statics.CR.AcctNumber));
								Statics.CRMeterImportError[lngErrorIndex].Name = Statics.CR.Name;
								Statics.CRMeterImportError[lngErrorIndex].Location = Statics.CR.Location;
								Statics.CRMeterImportError[lngErrorIndex].Current = Statics.CR.Current;
								lngErrorIndex += 1;
							}
						}
						else
						{
							strLine = Strings.Format(counter, "000000");
							strLine += Statics.CR.Book;
							strLine += Statics.CR.Sequence;
							if (Strings.Format(Statics.CR.Current, "000000").Length <= 6)
							{
								strLine += Strings.Format(Statics.CR.Current, "000000");
							}
							else
							{
								strLine += Strings.Left(Strings.Format(Statics.CR.Current, "000000"), 6);
								// <--- this is a hack to fix a 7 digit reading in Anson Water, when we get rid of the last DOS town then I can change this to 9 digits
							}
							//FC:FINAL:MSH - incorrect date format (same with internal issue #914)
							strLine += Strings.Format(datDefaultReading, "MMddyy");
							FCFileSystem.PrintLine(2, strLine);
						}
					}
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
                    //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From TRIO Windows", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                    if (lngErrorIndex > 0)
					{
						//Application.DoEvents();
						// show a report of the errors
						rptMeterImportErrorReport.InstancePtr.Init(5);
					}
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting TRIO Windows File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ConvertTRIOWindowsXRefExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				// vbPorter upgrade warning: counter As short --> As int	OnWrite(short, double)
				int counter;
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				int lngBook = 0;
				int lngSeq = 0;
				int lngMKey = 0;
				int lngPrev = 0;
				MessageBox.Show("Make sure that all books being affected are at the status of Cleared.  This will affect the readings.", "Clear Books", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				// ChDir strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				FCFileSystem.FileClose(1);
				FCFileSystem.FileClose(2);
				FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.CR));
				FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				datDefaultReading = DateTime.Today;
				object strInput = datDefaultReading;
				if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					//FC:FINAL:DDU:converted back to correct variable
					datDefaultReading = FCConvert.ToDateTime(strInput);
					for (counter = 1; counter <= FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.CR)); counter++)
					{
						FCFileSystem.FileGet(1, ref Statics.CR, counter);
						// strLine = Format(counter, "000000")
						// strLine = strLine & PadToString(Val(cr.Book), 4)
						// strLine = strLine & PadToString(Val(cr.Sequence), 4)
						// strLine = strLine & PadToString(Val(cr.Consumption), 6)
						// strLine = strLine & Format(datDefaultReading, "MMddyy")
						// Print #2, strLine
						// If Val(cr.AcctNumber) = 316 Then Stop
						if (Convert.ToByte(Strings.Left(Statics.CR.AcctNumber, 1)[0]) != 0)
						{
							// Find the account number, book and seq by using the cross reference field
							if (GetMeterInformation_2("A" + Strings.Right(Statics.CR.AcctNumber, 3), ref lngBook, ref lngSeq, ref lngMKey, ref lngPrev))
							{
								// If lngSeq Then Stop
								// Meter Key
								strLine = Strings.Format(lngMKey, "000000");
								// Book
								strLine += Strings.Format(lngBook, "0000");
								// Sequence
								strLine += Strings.Format(lngSeq, "0000");
								// Current Reading
								strLine += Strings.Format(Conversion.Val(Statics.CR.Consumption) + lngPrev, "000000");
								// Reading Date
								strLine += Strings.Format(datDefaultReading, "MMddyy");
								FCFileSystem.PrintLine(2, strLine);
								//Application.DoEvents();
							}
							else
							{
								// add an error line in the error array becuase the meter was not found
								// ReDim Preserve RVSMeterImportError(lngErrorIndex)
								//Application.DoEvents();
								// doMeterImportError(lngErrorIndex) = RVSIF
								// lngErrorIndex = lngErrorIndex + 1
							}
						}
					}
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
                    //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From TRIO DOS (XRef)", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                }
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting TRIO DOS File (XRef)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ConvertTRIODOSExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				// vbPorter upgrade warning: counter As short --> As int	OnWrite(short, double)
				int counter;
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				// vbPorter upgrade warning: lngBook As int	OnWrite(FixedString)
				int lngBook = 0;
				// vbPorter upgrade warning: lngSeq As int	OnWrite(FixedString)
				int lngSeq = 0;
				int lngMKey = 0;
				int lngPrev = 0;
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				// ChDir strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				FCFileSystem.FileClose(1);
				FCFileSystem.FileClose(2);
				FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.DOSCR));
				FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				datDefaultReading = DateTime.Today;
				object strInput = datDefaultReading;
				if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					//FC:FINAL:DDU:converted back to correct variable
					datDefaultReading = FCConvert.ToDateTime(strInput);
					for (counter = 1; counter <= FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.DOSCR)); counter++)
					{
						FCFileSystem.FileGet(1, ref Statics.DOSCR, counter);
						// strLine = Format(counter, "000000")
						// strLine = strLine & PadToString(Val(DOSCR.Book), 4)
						// strLine = strLine & PadToString(Val(DOSCR.Sequence), 4)
						// strLine = strLine & PadToString(Val(DOSCR.Consumption), 6)
						// strLine = strLine & Format(datDefaultReading, "MMddyy")
						lngBook = FCConvert.ToInt32(Statics.DOSCR.Book);
						lngSeq = FCConvert.ToInt32(Statics.DOSCR.Sequence);
						if (Convert.ToByte(Strings.Left(Statics.DOSCR.AcctNumber, 1)[0]) != 0)
						{
							if (GetMeterInformation_488("A" + Strings.Right(Statics.DOSCR.AcctNumber, 3), ref lngBook, ref lngSeq, ref lngMKey, ref lngPrev, true))
							{
								// If lngSeq Then Stop
								// Meter Key
								strLine = Strings.Format(lngMKey, "000000");
								// Book
								strLine += Strings.Format(lngBook, "0000");
								// Sequence
								strLine += Strings.Format(lngSeq, "0000");
								// Current Reading
								if (Strings.Format(Conversion.Val(Statics.DOSCR.Consumption) + lngPrev, "000000").Length <= 6)
								{
									strLine += Strings.Format(Conversion.Val(Statics.DOSCR.Consumption) + lngPrev, "000000");
								}
								else
								{
									strLine += Strings.Left(Strings.Format(Conversion.Val(Statics.DOSCR.Consumption) + lngPrev, "000000"), 6);
								}
								// Reading Date
								strLine += Strings.Format(datDefaultReading, "MMddyy");
								FCFileSystem.PrintLine(2, strLine);
								//Application.DoEvents();
							}
							else
							{
								// add an error line in the error array becuase the meter was not found
								// ReDim Preserve RVSMeterImportError(lngErrorIndex)
								//Application.DoEvents();
								// doMeterImportError(lngErrorIndex) = RVSIF
								// lngErrorIndex = lngErrorIndex + 1
							}
						}
						// Print #2, strLine
					}
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
                    //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From TRIO DOS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                }
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting TRIO DOS File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ConvertTRIODOSXRefExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				// vbPorter upgrade warning: counter As short --> As int	OnWrite(short, double)
				int counter;
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				int lngBook = 0;
				int lngSeq = 0;
				int lngMKey = 0;
				int lngPrev = 0;
				MessageBox.Show("Make sure that all books being affected are at the status of Cleared.  This will affect the readings.", "Clear Books", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				// ChDir strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				FCFileSystem.FileClose(1);
				FCFileSystem.FileClose(2);
				FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.DOSCR));
				FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				datDefaultReading = DateTime.Today;
				object strInput = datDefaultReading;
				if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					//FC:FINAL:DDU:converted back to correct variable
					datDefaultReading = FCConvert.ToDateTime(strInput);
					for (counter = 1; counter <= FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.DOSCR)); counter++)
					{
						FCFileSystem.FileGet(1, ref Statics.DOSCR, counter);
						// strLine = Format(counter, "000000")
						// strLine = strLine & PadToString(Val(DOSCR.Book), 4)
						// strLine = strLine & PadToString(Val(DOSCR.Sequence), 4)
						// strLine = strLine & PadToString(Val(DOSCR.Consumption), 6)
						// strLine = strLine & Format(datDefaultReading, "MMddyy")
						// Print #2, strLine
						// If Val(DOSCR.AcctNumber) = 316 Then Stop
						if (Convert.ToByte(Strings.Left(Statics.DOSCR.AcctNumber, 1)[0]) != 0)
						{
							// Find the account number, book and seq by using the cross reference field
							if (GetMeterInformation_2("A" + Strings.Right(Statics.DOSCR.AcctNumber, 3), ref lngBook, ref lngSeq, ref lngMKey, ref lngPrev))
							{
								// If lngSeq Then Stop
								// Meter Key
								strLine = Strings.Format(lngMKey, "000000");
								// Book
								strLine += Strings.Format(lngBook, "0000");
								// Sequence
								strLine += Strings.Format(lngSeq, "0000");
								// Current Reading
								strLine += Strings.Format(Conversion.Val(Statics.DOSCR.Consumption) + lngPrev, "000000");
								// Reading Date
								strLine += Strings.Format(datDefaultReading, "MMddyy");
								FCFileSystem.PrintLine(2, strLine);
								//Application.DoEvents();
							}
							else
							{
								// add an error line in the error array becuase the meter was not found
								// ReDim Preserve RVSMeterImportError(lngErrorIndex)
								//Application.DoEvents();
								// doMeterImportError(lngErrorIndex) = RVSIF
								// lngErrorIndex = lngErrorIndex + 1
							}
						}
					}
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
                    //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From TRIO DOS (XRef)", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                }
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting TRIO DOS File (XRef)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ConvertAquaAmericaExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// I think that this file will read the file created by Aqua America and
				// convert it into TSUTXX41.DAT in order to use the Electronic Data Entry
				// The input file has the actual current reading so I will be able to use
				// that directly instead of making a computation to find it
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsCrossRef = new clsDRWrapper();
				// This is to find the correct meter using AA account number
				int lngBook = 0;
				int lngSeq = 0;
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					// open the input file from AA
					FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.AAIF));
					// open the output file
					FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					lngError = 5;
					// clear the error array
					FCUtils.EraseSafe(Statics.MeterImportError);
					lngError = 10;
					// default reading date
					datDefaultReading = DateTime.Today;
					object strInput = datDefaultReading;
					if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
					{
						lngError = 11;
						counter = 1;
						while (!FCFileSystem.EOF(1))
						{
                            FCFileSystem.FileGet(1, ref Statics.AAIF, counter);
							// Line Input #1, strRecord
							if (Convert.ToByte(Strings.Left(Statics.AAIF.AccountNumber, 1)[0]) != 0)
							{
								// Find the account number, book and seq by using the cross reference field
								int lngPrev = 0;
								if (GetMeterInformation_2(Statics.AAIF.AccountNumber + Statics.AAIF.ResidentNumber, ref lngBook, ref lngSeq, ref lngAcct, ref lngPrev))
								{
									// Account Number
									strLine = Strings.Format(lngAcct, "000000");
									// Book
									strLine += Strings.Format(lngBook, "0000");
									// Sequence
									strLine += Strings.Format(lngSeq, "0000");
									// Current Reading
									strLine += Strings.Format(Statics.AAIF.Reading, "000000");
									// Reading Date
									strLine += Strings.Format(Statics.AAIF.ReadingDate, "000000");
									FCFileSystem.PrintLine(2, strLine);
									//Application.DoEvents();
								}
								else
								{
									// add an error line in the error array becuase the meter was not found
									Array.Resize(ref Statics.MeterImportError, lngErrorIndex + 1);
									//Application.DoEvents();
									Statics.MeterImportError[lngErrorIndex] = Statics.AAIF;
									lngErrorIndex += 1;
								}
							}
							counter += 1;
						}
						lngError = 20;
						FCFileSystem.FileClose(1);
						lngError = 30;
						FCFileSystem.FileClose(2);
						lngError = 31;
                        //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From Aqua America", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                        lngError = 32;
						if (lngErrorIndex > 0)
						{
							lngError = 33;
							//Application.DoEvents();
							// show a report of the errors
							rptMeterImportErrorReport.InstancePtr.Init(0);
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting Aqua America File - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertMillinocketExtractFile()
		{
			// Tracker Reference: 15568
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsCrossRef = new clsDRWrapper();
				// This is to find the correct meter using AA account number
				int lngBook = 0;
				int lngSeq = 0;
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				int lngPrev = 0;
				// vbPorter upgrade warning: lngCalcCur As int	OnWriteFCConvert.ToDouble(	OnRead(FixedString)
				int lngCalcCur = 0;
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					// open the input file from AA
					FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.MIF));
					// open the output file
					FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					lngError = 5;
					// clear the error array
					FCUtils.EraseSafe(Statics.MeterImportError);
					lngError = 10;
					// default reading date
					datDefaultReading = DateTime.Today;
					object strInput = datDefaultReading;
					if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
					{
						lngError = 11;
						counter = 1;
						while (!FCFileSystem.EOF(1))
						{
							FCFileSystem.FileGet(1, ref Statics.MIF, counter);
							// Line Input #1, strRecord
							if (Convert.ToByte(Strings.Left(Statics.MIF.AccountNumber, 1)[0]) != 0)
							{
								// Find the account number, book and seq by using the cross reference field
								if (GetMeterInformation(Statics.MIF.AccountNumber, ref lngBook, ref lngSeq, ref lngAcct, ref lngPrev))
								{
									// Account Number
									strLine = Strings.Format(lngAcct, "000000");
									// Book
									strLine += Strings.Format(lngBook, "0000");
									// Sequence
									strLine += Strings.Format(lngSeq, "0000");
									// Current Reading
									// MAL@20090112: Corrected so previous reading is added to the consumption to get the current reading
									// Tracker Reference: 16842
									// kgk 01-31-2012 trout-799  Catch calc'd reading > 6 digits until file TSUTXX41 format can be updated to 9 digits
									// create an error log entry
									// strLine = strLine & Format(Val(MIF.Consumption) + lngPrev, "000000")
									lngCalcCur = FCConvert.ToInt32(Conversion.Val(Statics.MIF.Consumption) + lngPrev);
									if (lngCalcCur > 999999)
									{
										strLine = "";
										// add an error line in the error array becuase the meter was not found
										Array.Resize(ref Statics.MeterImportError, lngErrorIndex + 1);
										//Application.DoEvents();
										Statics.MeterImportError[lngErrorIndex].AccountNumber = Statics.MIF.AccountNumber;
										Statics.MeterImportError[lngErrorIndex].Name = Statics.MIF.Name;
										Statics.MeterImportError[lngErrorIndex].Address = Statics.MIF.Address;
										Statics.MeterImportError[lngErrorIndex].Reading = lngCalcCur.ToString();
										Statics.MeterImportError[lngErrorIndex].ReadingDate = Statics.MIF.ReadingDate;
										// MeterImportError(lngErrorIndex) = MIF
										lngErrorIndex += 1;
									}
									else
									{
										strLine += Strings.Format(lngCalcCur, "000000");
										// Reading Date
										strLine += Strings.Format(Statics.MIF.ReadingDate, "000000");
										FCFileSystem.PrintLine(2, strLine);
									}
									//Application.DoEvents();
								}
								else
								{
									// add an error line in the error array becuase the meter was not found
									Array.Resize(ref Statics.MeterImportError, lngErrorIndex + 1);
									//Application.DoEvents();
									Statics.MeterImportError[lngErrorIndex].AccountNumber = Statics.MIF.AccountNumber;
									Statics.MeterImportError[lngErrorIndex].Name = Statics.MIF.Name;
									Statics.MeterImportError[lngErrorIndex].Address = Statics.MIF.Address;
									Statics.MeterImportError[lngErrorIndex].Consumption = Statics.MIF.Consumption;
									Statics.MeterImportError[lngErrorIndex].ReadingDate = Statics.MIF.ReadingDate;
									// MeterImportError(lngErrorIndex) = MIF
									lngErrorIndex += 1;
								}
							}
							counter += 1;
						}
						lngError = 20;
						FCFileSystem.FileClose(1);
						lngError = 30;
						FCFileSystem.FileClose(2);
						lngError = 31;
                        //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From Custom Import", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                        lngError = 32;
						if (lngErrorIndex > 0)
						{
							lngError = 33;
							//Application.DoEvents();
							// show a report of the errors
							rptMeterImportErrorReport.InstancePtr.Init(0);
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting Aqua America File - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertLisbonExtractFile()
		{
			// Tracker Reference: 15568
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsCrossRef = new clsDRWrapper();
				// This is to find the correct meter using AA account number
				int lngBook = 0;
				int lngSeq = 0;
				int lngAcct = 0;
				int lngErrorIndex;
				int lngError;
				int lngPrev = 0;
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					// open the input file from AA
					FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.LIF));
					// open the output file
					FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					lngError = 5;
					// default reading date
					datDefaultReading = DateTime.Today;
					object strInput = datDefaultReading;
					if (frmInput.InstancePtr.Init(ref strInput, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
					{
						lngError = 11;
						counter = 1;
						while (!FCFileSystem.EOF(1))
						{
							FCFileSystem.FileGet(1, ref Statics.LIF, counter);
							// Line Input #1, strRecord
							if (Convert.ToByte(Strings.Left(Strings.Trim(Statics.LIF.XRef1), 1)[0]) != 0)
							{
								// Find the account number, book and seq by using the cross reference field
								if (GetMeterInformation_2(Strings.Trim(Statics.LIF.XRef1), ref lngBook, ref lngSeq, ref lngAcct, ref lngPrev))
								{
									// Account Number
									strLine = Strings.Format(lngAcct, "000000");
									// Book
									strLine += Strings.Format(lngBook, "0000");
									// Sequence
									strLine += Strings.Format(lngSeq, "0000");
									// Current Reading
									// MAL@20090112: Corrected so previous reading is added to the consumption to get the current reading
									// Tracker Reference: 16842
									strLine += Strings.Format(Conversion.Val(Strings.Trim(Statics.LIF.Reading)) * 100, "000000");
									// Reading Date
									strLine += Strings.Format(Statics.LIF.ReadingDate, "000000");
									FCFileSystem.PrintLine(2, strLine);
									//Application.DoEvents();
								}
							}
							counter += 1;
						}
						lngError = 20;
						FCFileSystem.FileClose(1);
						lngError = 30;
						FCFileSystem.FileClose(2);
						lngError = 31;
                        //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From Custom Import", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                    }
					return;
				}
				catch (Exception ex)
				{
					
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting Aqua America File - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertRVSExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// I think that this file will read the file created by RVS and
				// convert it into TSUTXX41.DAT in order to use the Electronic Data Entry
				// The input file has the actual current reading so I will be able to use
				// that directly instead of making a computation to find it
				string strCurDir;
				string strFileName = "";
				int ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsCrossRef = new clsDRWrapper();
				// This is to find the correct meter using RVS account number
				int lngBook = 0;
				int lngSeq = 0;
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				string[] strInput = null;
				string strLeader;
				strLeader = "M";
				// This is hard coded for Anson/Madison Sewer District
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.*";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						// If UCase(Right(strFileName, 3)) <> "DAT" Then
						// Ans = MsgBox("You must select a .dat file before you may continue.  Do you wish to try again?", vbQuestion + vbYesNo, "Select File?")
						// If Ans = vbYes Then
						// GoTo PickAgain
						// Else
						// Exit Sub
						// End If
						// End If
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					if (strFileName != "")
					{
						// open the input file from AA
						FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.RVSIF));
						// open the output file
						FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						lngError = 5;
						// clear the error array
						FCUtils.EraseSafe(Statics.RVSMeterImportError);
						lngError = 10;
						// default reading date
						datDefaultReading = DateTime.Today;
						object datTempDate = datDefaultReading;
						if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
						{
							lngError = 11;
							counter = 1;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true);
							while (!FCFileSystem.EOF(1))
							{
								// Get #1, counter, RVSIF
								//Application.DoEvents();
								strRecord = FCFileSystem.LineInput(1);
								frmWait.InstancePtr.IncrementProgress();
								strRecord = strRecord.Replace(FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)), "|");
								strRecord = Strings.Left(strRecord, strRecord.Length - 2);
								strRecord = Strings.Right(strRecord, strRecord.Length - 1);
								strInput = Strings.Split(strRecord, "|", -1, CompareConstants.vbBinaryCompare);
								Statics.RVSIF.Name = strInput[0];
								Statics.RVSIF.Address1 = strInput[1];
								Statics.RVSIF.Address2 = strInput[2];
								Statics.RVSIF.Address3 = strInput[3];
								Statics.RVSIF.Zip = strInput[4];
								Statics.RVSIF.Location = strInput[5];
								Statics.RVSIF.Current = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[6])));
								Statics.RVSIF.Previous = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[7])));
								Statics.RVSIF.Account = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[8])));
								Statics.RVSIF.Route = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[9])));
								Statics.RVSIF.Code = strInput[10];
								Statics.RVSIF.Q1 = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[11])));
								Statics.RVSIF.Q2 = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[12])));
								Statics.RVSIF.Q3 = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[13])));
								Statics.RVSIF.Q4 = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[14])));
								if (Information.IsDate(strInput[15]))
								{
									Statics.RVSIF.ReadingDate = DateAndTime.DateValue(strInput[15]);
								}
								else
								{
									Statics.RVSIF.ReadingDate = datDefaultReading;
								}
								if (Convert.ToByte(Strings.Left(FCConvert.ToString(Statics.RVSIF.Account), 1)[0]) != 0)
								{
									// Find the account number, book and seq by using the cross reference field
									int lngPrev = 0;
									if (GetMeterInformation_2(strLeader + modMain.PadToString_6(ref Statics.RVSIF.Account, 4), ref lngBook, ref lngSeq, ref lngAcct, ref lngPrev))
									{
										// Account Number
										strLine = Strings.Format(lngAcct, "000000");
										// Book
										strLine += Strings.Format(lngBook, "0000");
										// Sequence
										strLine += Strings.Format(lngSeq, "0000");
										// Current Reading
										strLine += Strings.Format(Statics.RVSIF.Current, "000000");
										// Reading Date
										strLine += Strings.Format(Statics.RVSIF.ReadingDate, "MMddyy");
										FCFileSystem.PrintLine(2, strLine);
										//Application.DoEvents();
									}
									else
									{
										// add an error line in the error array becuase the meter was not found
										Array.Resize(ref Statics.RVSMeterImportError, lngErrorIndex + 1);
										//Application.DoEvents();
										Statics.RVSMeterImportError[lngErrorIndex] = Statics.RVSIF;
										lngErrorIndex += 1;
									}
								}
								counter += 1;
							}
							lngError = 20;
							FCFileSystem.FileClose(1);
							lngError = 30;
							FCFileSystem.FileClose(2);
							lngError = 31;
							frmWait.InstancePtr.Unload();
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From RVS Extract", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                            lngError = 32;
							if (lngErrorIndex > 0)
							{
								lngError = 33;
								//Application.DoEvents();
								// show a report of the errors
								rptMeterImportErrorReport.InstancePtr.Init(1);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting RVS Extract - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertRVSWindowsExtractFile()
		{
			// MAL@20070925: Added for new RVS Windows extract file
			// Call Reference: 117185
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				int ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsCrossRef = new clsDRWrapper();
				// This is to find the correct meter using RVS account number
				int lngBook = 0;
				int lngSeq = 0;
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				// vbPorter upgrade warning: strInput As string()	OnRead(int, string)
				string[] strInput = null;
				string strLeader;
				strLeader = "M";
				// This is hard coded for Anson/Madison Sewer District
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.*";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					if (strFileName != "")
					{
						// open the input file from AA
						FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.RVSIF));
						// open the output file
						FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						lngError = 5;
						// clear the error array
						FCUtils.EraseSafe(Statics.RVSMeterImportError);
						lngError = 10;
						// default reading date
						datDefaultReading = DateTime.Today;
						object datTempDate = datDefaultReading;
						if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
						{
							//FC:FINAL:DDU:converted back to correct variable
							datDefaultReading = FCConvert.ToDateTime(strInput);
							lngError = 11;
							counter = 1;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true);
							while (!FCFileSystem.EOF(1))
							{
								// Get #1, counter, RVSIF
								//Application.DoEvents();
								strRecord = FCFileSystem.LineInput(1);
								if (Strings.Left(strRecord, 11) != "Acct Number")
								{
									// MAL@20070925: Skip Header Line
									frmWait.InstancePtr.IncrementProgress();
									strRecord = strRecord.Replace(FCConvert.ToString(Convert.ToChar(34)) + "," + FCConvert.ToString(Convert.ToChar(34)), "|");
									strRecord = Strings.Left(strRecord, strRecord.Length - 2);
									strRecord = Strings.Right(strRecord, strRecord.Length - 1);
									strInput = Strings.Split(strRecord, "|", -1, CompareConstants.vbBinaryCompare);
									Statics.RVSIF.Account = FCConvert.ToInt32(strInput[0]);
									Statics.RVSIF.Name = strInput[1];
									Statics.RVSIF.Address1 = strInput[2];
									Statics.RVSIF.Address2 = strInput[3];
									Statics.RVSIF.Location = strInput[4];
									Statics.RVSIF.Address3 = strInput[5];
									Statics.RVSIF.Zip = strInput[7];
									Statics.RVSIF.Code = strInput[8];
									Statics.RVSIF.Route = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[9])));
									if (Information.IsDate(strInput[10]))
									{
										Statics.RVSIF.ReadingDate = DateAndTime.DateValue(strInput[10]);
									}
									else
									{
										Statics.RVSIF.ReadingDate = datDefaultReading;
									}
									Statics.RVSIF.Current = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[11])));
									Statics.RVSIF.Previous = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[12])));
									Statics.RVSIF.Q1 = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[13])));
									Statics.RVSIF.Q2 = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[14])));
									Statics.RVSIF.Q3 = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[15])));
									Statics.RVSIF.Q4 = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[16])));
									if (Convert.ToByte(Strings.Left(FCConvert.ToString(Statics.RVSIF.Account), 1)[0]) != 0)
									{
										// Find the account number, book and seq by using the cross reference field
										int lngPrev = 0;
										if (GetMeterInformation_2(strLeader + modMain.PadToString_6(ref Statics.RVSIF.Account, 4), ref lngBook, ref lngSeq, ref lngAcct, ref lngPrev))
										{
											// Account Number
											strLine = Strings.Format(lngAcct, "000000");
											// Book
											strLine += Strings.Format(lngBook, "0000");
											// Sequence
											strLine += Strings.Format(lngSeq, "0000");
											// Current Reading
											strLine += Strings.Format(Statics.RVSIF.Current, "000000");
											// Reading Date
											strLine += Strings.Format(Statics.RVSIF.ReadingDate, "MMddyy");
											FCFileSystem.PrintLine(2, strLine);
											//Application.DoEvents();
										}
										else
										{
											// add an error line in the error array becuase the meter was not found
											Array.Resize(ref Statics.RVSMeterImportError, lngErrorIndex + 1);
											//Application.DoEvents();
											Statics.RVSMeterImportError[lngErrorIndex] = Statics.RVSIF;
											lngErrorIndex += 1;
										}
									}
									counter += 1;
								}
							}
							lngError = 20;
							FCFileSystem.FileClose(1);
							lngError = 30;
							FCFileSystem.FileClose(2);
							lngError = 31;
							frmWait.InstancePtr.Unload();
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From RVS Extract", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                            lngError = 32;
							if (lngErrorIndex > 0)
							{
								lngError = 33;
								//Application.DoEvents();
								// show a report of the errors
								rptMeterImportErrorReport.InstancePtr.Init(1);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting RVS Extract - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertBadgerExtractFile()
		{
			try
			{
				// convert it into TSUTXX41.DAT in order to use the Electronic Data Entry
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsCrossRef = new clsDRWrapper();
				// This is to find the correct meter using AA account number
				int lngBook = 0;
				int lngSeq = 0;
				int lngMeterKey = 0;
				int lngErrorIndex = 0;
				int lngError;
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "DAT")
						{
							ans = MessageBox.Show("You must select a .dat file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					// open the input file from AA
					FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.BadgerIF));
					// open the output file
					FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					lngError = 5;
					// clear the error array
					FCUtils.EraseSafe(Statics.BadgerMeterImportError);
					lngError = 10;
					// default reading date
					datDefaultReading = DateTime.Today;
					object datTempDate = datDefaultReading;
					if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
					{
						lngError = 11;
						counter = 1;
						while (!FCFileSystem.EOF(1))
						{
							FCFileSystem.FileGet(1, ref Statics.BadgerIF, counter);
							// Line Input #1, strRecord
                            if (Conversion.Val(Statics.BadgerIF.RouteNumber) != 0 && Conversion.Val(Statics.BadgerIF.ReadingSequence) != 0)
							//if (Convert.ToByte(Strings.Left(Statics.BadgerIF.AccountNumber, 1)[0]) != 0 )
							{
                                // Find the account number, book and seq by using the cross reference field
                                lngBook = FCConvert.ToInt32(Conversion.Val(Statics.BadgerIF.RouteNumber));
                                lngSeq = FCConvert.ToInt32(Conversion.Val(Statics.BadgerIF.ReadingSequence));
                                int lngPrev = 0;
								if (GetMeterInformation_2(Statics.BadgerIF.AccountNumber.ToString(), ref lngBook, ref lngSeq, ref lngMeterKey, ref lngPrev,true))
								{
									// Account Number
									strLine = Strings.Format(lngMeterKey, "000000");
									// Book
									strLine += Strings.Format(lngBook, "0000");
									// Sequence
                                    if (lngSeq <= 9999)
                                    {
                                        strLine += Strings.Format(lngSeq, "0000");
                                    }
                                    else
                                    {
                                        strLine += lngSeq.ToString().Right(4);
                                    }

                                    // Current Reading
                                    if (!string.IsNullOrWhiteSpace(Statics.BadgerIF.CurrentReading))
                                    {
                                        strLine += Strings.Format(Statics.BadgerIF.CurrentReading, "000000");
                                    }
                                    else
                                    {
                                        strLine += " ".PadLeft(6, ' ');
                                    }
                                    // Reading Date
                                    if (!string.IsNullOrWhiteSpace(Statics.BadgerIF.ReadDate))
                                    {
                                        var tempDate = DateTime.Parse(Statics.BadgerIF.ReadDate.Substring(0,2) + @"/" + Statics.BadgerIF.ReadDate.Substring(2,2) + @"/" + Statics.BadgerIF.ReadDate.Right(4));
                                        strLine += Strings.Format(tempDate, "MMDDYY");
                                    }
                                    else
                                    {
                                        strLine += Strings.Format(datDefaultReading, "MMDDYY");
                                    }

                                    FCFileSystem.PrintLine(2, strLine);
									//Application.DoEvents();
								}
								else
								{
									// add an error line in the error array becuase the meter was not found
									Array.Resize(ref Statics.BadgerMeterImportError, lngErrorIndex + 1);
									//Application.DoEvents();
									Statics.BadgerMeterImportError[lngErrorIndex] = Statics.BadgerIF;
									lngErrorIndex += 1;
								}
							}
							counter += 1;
						}
						lngError = 20;
						FCFileSystem.FileClose(1);
						lngError = 30;
						FCFileSystem.FileClose(2);
						lngError = 31;
                        //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From Badger Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                        lngError = 32;
						if (lngErrorIndex > 0)
						{
							lngError = 33;
							//Application.DoEvents();
							// show a report of the errors
							rptMeterImportErrorReport.InstancePtr.Init(0);
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting Aqua America File - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertBangorExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strLine = "";
				int counter;
				int lngErrorIndex = 0;
				int lngBook = 0;
				int lngSeq = 0;
				int lngMKey = 0;
				int lngPrev;
				string strAcctNum = "";
				DateTime datCurrRead = default(DateTime);
				DateTime datPrevRead = default(DateTime);
				string strNDSBookNum = "";
				string strNDSSequence = "";
				string strServiceAddr = "";
				string strCustName = "";
				string strTemp = "";
				int i = 0;
				BangorMeters[] Meter = new BangorMeters[4 + 1];
				int mtrCnt = 0;
				bool boolProcessingFile;
				bool boolReading = false;
				int lngLineNumber = 0;
				boolProcessingFile = false;
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.txt";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "TXT")
						{
							ans = MessageBox.Show("You must select a .txt file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					// On Error GoTo 0
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
					FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					lngLineNumber = 0;
					datDefaultReading = DateTime.Today;
					object datTempDate = datDefaultReading;
					if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
					{
						frmWait.InstancePtr.Init("Please wait..." + "\r\n" + "Converting Consumption File", true, FCConvert.ToInt32(FCFileSystem.LOF(1) / 1640), true);
						boolProcessingFile = true;
						boolReading = true;
						while (!FCFileSystem.EOF(1))
						{
							//Application.DoEvents();
							FCUtils.EraseSafe(Meter);
							mtrCnt = 1;
							for (i = 1; i <= 230; i++)
							{
								//Application.DoEvents();
								if (!FCFileSystem.EOF(1))
								{
									// Last record does not have an End of Record CR/LF
									strTemp = FCFileSystem.LineInput(1);
									lngLineNumber += 1;
									switch (i)
									{
										case 2:
											{
												// NDS Account Number
												strAcctNum = Strings.Trim(strTemp);
												break;
											}
										case 4:
											{
												// Previous Reading Date
												datPrevRead = DateAndTime.DateValue(strTemp);
												break;
											}
										case 5:
											{
												// Current Reading Date
												datCurrRead = DateAndTime.DateValue(strTemp);
												break;
											}
										case 15:
											{
												// Book Number
												// lngBook = Val(strTemp)
												strNDSBookNum = Strings.Trim(strTemp);
												break;
											}
										case 16:
											{
												// NDS Sequence (try to use it if it's integer???
												// If CInt(strTemp) = Val(strTemp) Then
												// lngSeq = CInt(strTemp)
												// Else
												// lngSeq = -1
												// End If
												strNDSSequence = Strings.Trim(strTemp);
												break;
											}
										case 29:
											{
												// Service Location
												strServiceAddr = Strings.Trim(strTemp);
												break;
											}
										case 30:
											{
												// Name
												strCustName = Strings.Trim(strTemp);
												break;
											}
										case 37:
										case 48:
										case 59:
										case 70:
											{
												// Previous Reading
												if (Strings.Trim(strTemp) != "")
												{
													Meter[mtrCnt].PrevReading = FCConvert.ToInt32(Conversion.Val(strTemp));
												}
												else
												{
													Meter[mtrCnt].PrevReading = 0;
												}
												break;
											}
										case 38:
										case 49:
										case 60:
										case 71:
											{
												// Current Reading
												if (Strings.Trim(strTemp) != "")
												{
													Meter[mtrCnt].CurReading = FCConvert.ToInt32(Conversion.Val(strTemp));
												}
												else
												{
													Meter[mtrCnt].CurReading = 0;
												}
												break;
											}
										case 39:
										case 50:
										case 61:
										case 72:
											{
												// Usage / Consumption
												if (Strings.Trim(strTemp) != "")
												{
													Meter[mtrCnt].Consumption = FCConvert.ToInt32(Conversion.Val(strTemp));
												}
												else
												{
													Meter[mtrCnt].Consumption = 0;
												}
												break;
											}
										case 40:
										case 51:
										case 62:
										case 73:
											{
												// Trans Code - Determine if Main(1) or Compound(2) Meter
												if (Strings.Trim(strTemp) != "")
												{
													if (strTemp == "451" || strTemp == "452")
													{
														Meter[mtrCnt].MeterNumber = 2;
													}
													else
													{
														Meter[mtrCnt].MeterNumber = 1;
													}
												}
												else
												{
													Meter[mtrCnt].MeterNumber = 0;
												}
												break;
											}
										case 45:
										case 56:
										case 67:
										case 78:
											{
												// Category / Service
												Meter[mtrCnt].Category = Strings.Trim(strTemp);
												break;
											}
										case 46:
										case 57:
										case 68:
										case 79:
											{
												// Rate (Last field for Meter)
												mtrCnt += 1;
												break;
											}
									}
									//end switch
								}
							}
							boolReading = false;
							if (Strings.Trim(strAcctNum) != "")
							{
								for (i = 1; i <= 4; i++)
								{
									// Find the meter key using the cross reference field
									if (Meter[i].Category == "SW")
									{
										// kk 11042013 trouts-55    And Meter(i).CurReading > 0 Then
										if (GetBangorMeterInformation_2(Strings.Trim(strAcctNum), ref lngBook, ref lngSeq, ref lngMKey, Meter[i].PrevReading, datPrevRead, Meter[i].MeterNumber))
										{
											// Meter Key
											strLine = Strings.Format(lngMKey, "000000");
											// Book
											strLine += Strings.Format(lngBook, "0000");
											// Sequence Number
											strLine += Strings.Format(lngSeq, "0000");
											// Current Reading
											strLine += Strings.Format(Meter[i].CurReading, "000000");
											// Reading Date
											strLine += Strings.Format(datCurrRead, "MMddyy");
											FCFileSystem.PrintLine(2, strLine);
											//Application.DoEvents();
										}
										else
										{
											// add an error line in the error array becuase the meter was not found
											Array.Resize(ref Statics.BangorMeterImportError, lngErrorIndex + 1);
											//Application.DoEvents();
											Statics.BangorMeterImportError[lngErrorIndex].AccountNumber = Strings.Trim(strAcctNum);
											Statics.BangorMeterImportError[lngErrorIndex].BookNum = Strings.Trim(strNDSBookNum);
											Statics.BangorMeterImportError[lngErrorIndex].CurrentReading = Strings.Trim(Meter[i].CurReading.ToString());
											Statics.BangorMeterImportError[lngErrorIndex].CustomerName = Strings.Trim(strCustName);
											Statics.BangorMeterImportError[lngErrorIndex].NDSSequence = Strings.Trim(strNDSSequence);
											Statics.BangorMeterImportError[lngErrorIndex].ReadDate = Strings.Format(datCurrRead, "MM/dd/yy");
											Statics.BangorMeterImportError[lngErrorIndex].ServiceAddress = Strings.Trim(strServiceAddr);
											lngErrorIndex += 1;
										}
									}
								}
							}
							frmWait.InstancePtr.IncrementProgress();
						}
						boolProcessingFile = false;
						FCFileSystem.FileClose(1);
						FCFileSystem.FileClose(2);
						frmWait.InstancePtr.Unload();
						//Application.DoEvents();
						if (lngErrorIndex < 1)
						{
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                        }
						else
						{
							MessageBox.Show("TSUTXX41.DAT has been created but there were errors", "File Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						}
						if (lngErrorIndex > 0)
						{
							//Application.DoEvents();
							// show a report of the errors
							rptMeterImportErrorReport.InstancePtr.Init(4);
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					if (!boolProcessingFile || !boolReading)
					{
						MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
					else
					{
						MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + "\r\n" + "Line Number: " + FCConvert.ToString(lngLineNumber) + ", Record Line: " + FCConvert.ToString(i) + ", Acct: " + strAcctNum + "\r\n" + "Line contents: " + strTemp, "Error Converting File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					}
				}
			}
			catch
			{
			}
		}

		private static bool GetBangorMeterInformation_2(string strAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, int lngPrev, DateTime dtPrevRd, int lngMeterNumber = 1)
		{
			return GetBangorMeterInformation(ref strAccountNumber, ref lngBook, ref lngSeq, ref lngMK, ref lngPrev, ref dtPrevRd, lngMeterNumber);
		}

		private static bool GetBangorMeterInformation(ref string strAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, ref int lngPrev, ref DateTime dtPrevRd, int lngMeterNumber = 1)
		{
			bool GetBangorMeterInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				string strAcct = "";
				int i;
				// If the Account number contains any letters then just use it as is
				if (LikeOperator.LikeString(strAccountNumber, "*[A-Z]*", Microsoft.VisualBasic.CompareMethod.Text))
				{
					strAcct = strAccountNumber;
				}
				else
				{
					// If the account number doesn't contain any letters and is too short, then left pad with zeros
					strAcct = Strings.Format(strAccountNumber, "000000000000");
				}
				lngBook = 0;
				lngSeq = 0;
				lngMK = 0;
				// XXX The stormwater meter always has to be the last meter on the account
				// kk06272014 trouts-102  Check for deleted account  rsM.OpenRecordset "SELECT * FROM MeterTable WHERE XRef1 = '" & strAcct & "' AND MeterNumber = " & lngMeterNumber, strUTDatabase
				rsM.OpenRecordset("SELECT m.ID, m.BookNumber, m.Sequence, m.PreviousReadingDate, m.PreviousReading, m.PreviousCode, m.Service FROM MeterTable m INNER JOIN Master ON m.AccountKey = Master.ID WHERE XRef1 = '" + strAcct + "' AND MeterNumber = " + FCConvert.ToString(lngMeterNumber) + " AND ISNULL(Deleted, 0) = 0", modExtraModules.strUTDatabase);
				if (!rsM.EndOfFile())
				{
					if (Strings.UCase(FCConvert.ToString(rsM.Get_Fields_String("Service"))) != "W")
					{
						// kk03242015 trouts-136  Add check for service = B or S
						GetBangorMeterInformation = true;
						lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(rsM.Get_Fields_Int32("BookNumber"))));
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						lngSeq = FCConvert.ToInt32(Math.Round(Conversion.Val(rsM.Get_Fields("Sequence"))));
						lngMK = FCConvert.ToInt32(rsM.Get_Fields_Int32("ID"));
						// Set the previous reading date if it is empty (first run after conversion)
						if (rsM.Get_Fields_DateTime("PreviousReadingDate").ToOADate() == 0 || rsM.Get_Fields_DateTime("PreviousReadingDate").ToOADate() == DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
						{
							// kk07162014 trouts-102  Changed to view above so can't use rsM.Update here
							// rsM.Fields("PreviousReadingDate") = Format(dtPrevRd, "MM/dd/yyyy")
							// rsM.Fields("PreviousReading") = lngPrev
							// rsM.Fields("PreviousCode") = "A"
							// rsM.Update
							rsM.Execute("UPDATE MeterTable SET PreviousReadingDate = '" + Strings.Format(dtPrevRd, "MM/dd/yyyy") + "', PreviousReading = " + FCConvert.ToString(lngPrev) + ", PreviousCode = 'A' WHERE ID = " + rsM.Get_Fields_Int32("ID"), modExtraModules.strUTDatabase);
						}
					}
				}
				return GetBangorMeterInformation;
			}
			catch (Exception ex)
			{
				
				frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + Information.Err(ex).Number + " - " + Information.Err(ex).Description + "\r\n" + "Account: " + strAccountNumber + ", Book: " + FCConvert.ToString(lngBook) + ", Seq: " + FCConvert.ToString(lngSeq) + "\r\n" + ", MeterKey: " + FCConvert.ToString(lngMK) + ", Prev Read: " + FCConvert.ToString(lngPrev) + ", Prev Read Date: " + Strings.Format(dtPrevRd, "MM/dd/yyyy"), FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + "\r\n" + "Account: " + strAccountNumber + ", Book: " + FCConvert.ToString(lngBook) + ", Seq: " + FCConvert.ToString(lngSeq) + "\r\n" + ", MeterKey: " + FCConvert.ToString(lngMK) + ", Prev Read: " + FCConvert.ToString(lngPrev) + ", Prev Read Date: " + Strings.Format(dtPrevRd, "MM/dd/yyyy"));
			}
			return GetBangorMeterInformation;
		}

		public static void ConvertVeazieExtractFile()
		{
			// kk09022015  Added conversion of Veazie extract for Orono
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				int ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				string strBook = "";
				int lngBook = 0;
				string strSeq = "";
				int lngSeq = 0;
				int lngMK = 0;
				// vbPorter upgrade warning: strAcct As string	OnReadFCConvert.ToInt32(
				string strAcct = "";
				// vbPorter upgrade warning: strName As Variant --> As string
				string strName = "", strName2 = "";
				string strLoc = "";
				object strTBD1 = null, strTBD2 = null, strTBD3 = null, strTBD4 = null, strTBD5 = null;
				string strTBD6 = "";
				string strEstimate = "";
				string strCOName = "";
				// vbPorter upgrade warning: strAddr1 As Variant --> As string
				// vbPorter upgrade warning: strAddr2 As Variant --> As string
				// vbPorter upgrade warning: strAddr3 As Variant --> As string
				string strAddr1 = "", strAddr2 = "", strAddr3 = "";
				object strAddr4 = null;
				string strCSZ = "";
				object strCur = null, strPrev = null;
				string strCons = "";
				string strReadDate = "";
				int lngErrorIndex = 0;
				int lngError;
				string[] strInput = null;
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
                StreamReader sr = null;
                // MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
                //- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
                /*? On Error Resume Next  */
                try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.csv";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
					}
					else
					{
						return;
					}
					lngError = 2;
					//ChDrive(strCurDir);
					//Environment.CurrentDirectory = strCurDir;
					lngError = 3;
					//FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					if (strFileName != "")
					{
						// open the input file from AA
						//FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
                        sr = new StreamReader(strFileName);
						// Len = Len(RVSIF)
						// open the output file
						FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						lngError = 5;
						// clear the error array
						FCUtils.EraseSafe(Statics.RVSMeterImportError);
						lngError = 10;
						// default reading date
						datDefaultReading = DateTime.Today;
						object datTempDate = datDefaultReading;
						if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
						{
							//FC:FINAL:DDU:converted back to correct variable
							datDefaultReading = FCConvert.ToDateTime(datTempDate);
							lngError = 11;
							counter = 1;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true);
							//while (!FCFileSystem.EOF(1))
                            while(!sr.EndOfStream)
							{
                                // Get #1, counter, RVSIF
                                //Application.DoEvents();
                                //FC:FINAL:AM:#3742 - rewrite the code
                                string line = sr.ReadLine();
                                Match match = Regex.Match(line, "\"(.*?)\",|$");
                                strAcct = match.Groups[1].Value;
                                strName = (match = match.NextMatch()).Groups[1].Value;
                                strName2 = (match = match.NextMatch()).Groups[1].Value;
                                strLoc = (match = match.NextMatch()).Groups[1].Value;
                                strTBD1 = (match = match.NextMatch()).Groups[1].Value;
                                strCOName = (match = match.NextMatch()).Groups[1].Value;
                                strAddr1 = (match = match.NextMatch()).Groups[1].Value;
                                strAddr2 = (match = match.NextMatch()).Groups[1].Value;
                                strAddr3 = (match = match.NextMatch()).Groups[1].Value;
                                strAddr4 = (match = match.NextMatch()).Groups[1].Value;
                                strCSZ = (match = match.NextMatch()).Groups[1].Value;
                                strTBD2 = (match = match.NextMatch()).Groups[1].Value;
                                strBook = (match = match.NextMatch()).Groups[1].Value;
                                strSeq = (match = match.NextMatch()).Groups[1].Value;
                                strPrev = (match = match.NextMatch()).Groups[1].Value;
                                strCur = (match = match.NextMatch()).Groups[1].Value;
                                strCons = (match = match.NextMatch()).Groups[1].Value;
                                strReadDate = (match = match.NextMatch()).Groups[1].Value;
                                strTBD3 = (match = match.NextMatch()).Groups[1].Value;
                                strTBD4 = (match = match.NextMatch()).Groups[1].Value;
                                strTBD5 = (match = match.NextMatch()).Groups[1].Value;
                                strTBD6 = (match = match.NextMatch()).Groups[1].Value;
                                strEstimate = (match = match.NextMatch()).Groups[1].Value;
                                //FCFileSystem.Input(1, ref strName);
                                //FCFileSystem.Input(1, ref strName2);
                                //FCFileSystem.Input(1, ref strLoc);
                                //FCFileSystem.Input(1, ref strTBD1);
                                //FCFileSystem.Input(1, ref strCOName);
                                //FCFileSystem.Input(1, ref strAddr1);
                                //FCFileSystem.Input(1, ref strAddr2);
                                //FCFileSystem.Input(1, ref strAddr3);
                                //FCFileSystem.Input(1, ref strAddr4);
                                //FCFileSystem.Input(1, ref strCSZ);
                                //FCFileSystem.Input(1, ref strTBD2);
                                //FCFileSystem.Input(1, ref strBook);
                                //FCFileSystem.Input(1, ref strSeq);
                                //FCFileSystem.Input(1, ref strPrev);
                                //FCFileSystem.Input(1, ref strCur);
                                //FCFileSystem.Input(1, ref strCons);
                                //FCFileSystem.Input(1, ref strReadDate);
                                //FCFileSystem.Input(1, ref strTBD3);
                                //FCFileSystem.Input(1, ref strTBD4);
                                //FCFileSystem.Input(1, ref strTBD5);
                                //FCFileSystem.Input(1, ref strTBD6);
                                //FCFileSystem.Input(1, ref strEstimate);
                                // kk09082015 - take out and match by XRef1 - take off the any leading zeros
                                if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "ORONO")
								{
									strAcct = FCConvert.ToString(Conversion.Val(strAcct));
								}
								Statics.RVSIF.Account = FCConvert.ToInt32(strAcct);
								Statics.RVSIF.Name = strName;
								Statics.RVSIF.Address1 = strAddr1;
								Statics.RVSIF.Address2 = strAddr2;
								Statics.RVSIF.Location = strLoc;
								Statics.RVSIF.Address3 = strAddr3;
								// RVSIF.Zip = strInput(7)
								// RVSIF.Code = strInput(8)
								Statics.RVSIF.Route = FCConvert.ToInt32(Math.Round(Conversion.Val(strBook)));
								lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(strBook)));
								lngSeq = FCConvert.ToInt32(Math.Round(Conversion.Val(strSeq)));
                                //FC:FINAL:SBE - #3742 - force the same behavior as VB6 application. For the "041218" string, IsDate() function returns false in VB6
								if (Information.IsDate(strReadDate, forceVB6Behavior: true))
								{
									Statics.RVSIF.ReadingDate = DateAndTime.DateValue(strReadDate);
								}
								else
								{
									Statics.RVSIF.ReadingDate = datDefaultReading;
								}
								Statics.RVSIF.Current = FCConvert.ToInt32(Math.Round(Conversion.Val(strCur)));
								Statics.RVSIF.Previous = FCConvert.ToInt32(Math.Round(Conversion.Val(strPrev)));
								// RVSIF.Q1 = Val(strInput(13))
								// RVSIF.Q2 = Val(strInput(14))
								// RVSIF.Q3 = Val(strInput(15))
								// RVSIF.Q4 = Val(strInput(16))
								if (Convert.ToByte(Strings.Left(FCConvert.ToString(Statics.RVSIF.Account), 1)[0]) != 0)
								{
									// Find the meter from the Book and Seq
									if (GetMeterInformation_567(strAcct, ref lngBook, ref lngSeq, ref lngMK, false))
									{
										// Meter Key
										strLine = Strings.Format(lngMK, "000000");
										// Book
										strLine += Strings.Format(lngBook, "0000");
										// Sequence
										if (lngSeq <= 9999)
										{
											strLine += Strings.Format(lngSeq, "0000");
										}
										else
										{
											strLine += Strings.Left(Strings.Format(lngSeq, "0000"), 4);
										}
										// Current Reading
										strLine += Strings.Format(Statics.RVSIF.Current, "000000");
										// Reading Date
										strLine += Strings.Format(Statics.RVSIF.ReadingDate, "MMddyy");
										FCFileSystem.PrintLine(2, strLine);
										//Application.DoEvents();
									}
									else
									{
										// add an error line in the error array becuase the meter was not found
										Array.Resize(ref Statics.RVSMeterImportError, lngErrorIndex + 1);
										//Application.DoEvents();
										Statics.RVSMeterImportError[lngErrorIndex] = Statics.RVSIF;
										lngErrorIndex += 1;
									}
									counter += 1;
								}
							}
							lngError = 20;
							//FCFileSystem.FileClose(1);
							lngError = 30;
							FCFileSystem.FileClose(2);
							lngError = 31;
							frmWait.InstancePtr.Unload();
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From RVS Extract", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                            lngError = 32;
							if (lngErrorIndex > 0)
							{
								lngError = 33;
								//Application.DoEvents();
								// show a report of the errors
								rptMeterImportErrorReport.InstancePtr.Init(1);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting RVS Extract - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
                finally
                {
                    sr?.Close();   
                }
			}
			catch
			{
			}
		}

		public static void ConvertFarmingtonXRefExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// I think that this file will read the file created by RVS and
				// convert it into TSUTXX41.DAT in order to use the Electronic Data Entry
				// The input file has the actual current reading so I will be able to use
				// that directly instead of making a computation to find it
				string strCurDir;
				string strFileName = "";
				int ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsMaster = new clsDRWrapper();
				// This is to find the correct meter using RVS account number
				int lngBook = 0;
				int lngSeq = 0;
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				string[] strInput = null;
				string strLeader;
				int lngCons = 0;
				strLeader = "M";
				// This is hard coded for Anson/Madison Sewer District
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.*";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						// If UCase(Right(strFileName, 3)) <> "DAT" Then
						// Ans = MsgBox("You must select a .dat file before you may continue.  Do you wish to try again?", vbQuestion + vbYesNo, "Select File?")
						// If Ans = vbYes Then
						// GoTo PickAgain
						// Else
						// Exit Sub
						// End If
						// End If
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					if (strFileName != "")
					{
						// open the input file from AA
						FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.RVSIF));
						// open the output file
						FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						lngError = 5;
						// clear the error array
						FCUtils.EraseSafe(Statics.RVSMeterImportError);
						lngError = 10;
						// default reading date
						datDefaultReading = DateTime.Today;
						object datTempDate = datDefaultReading;
						if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
						{
							//FC:FINAL:DDU:converted back to correct variable
							datDefaultReading = FCConvert.ToDateTime(datTempDate);
							lngError = 11;
							counter = 1;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true);
							while (!FCFileSystem.EOF(1))
							{
								//Application.DoEvents();
								// Get #1, counter, RVSIF
								strRecord = FCFileSystem.LineInput(1);
								frmWait.InstancePtr.IncrementProgress();
								// strRecord = Replace(strRecord, Chr(34) & "," & Chr(34), "|")
								strRecord = Strings.Left(strRecord, strRecord.Length - 2);
								strRecord = Strings.Right(strRecord, strRecord.Length - 1);
								strInput = Strings.Split(strRecord, ",", -1, CompareConstants.vbBinaryCompare);
								// Split(strRecord, "|")
								Statics.RVSIF.Name = strInput[0].Replace(FCConvert.ToString(Convert.ToChar(34)), "") + " " + strInput[4].Replace(FCConvert.ToString(Convert.ToChar(34)), "");
								Statics.RVSIF.Address1 = strInput[5].Replace(FCConvert.ToString(Convert.ToChar(34)), "");
								Statics.RVSIF.Address2 = "";
								Statics.RVSIF.Address3 = "";
								Statics.RVSIF.Zip = "";
								Statics.RVSIF.Location = strInput[1].Replace(FCConvert.ToString(Convert.ToChar(34)), "");
								Statics.RVSIF.Current = 0;
								Statics.RVSIF.Previous = 0;
								Statics.RVSIF.Account = FCConvert.ToInt32(strInput[2].Replace(FCConvert.ToString(Convert.ToChar(34)), ""));
								Statics.RVSIF.Code = strInput[2].Replace(FCConvert.ToString(Convert.ToChar(34)), "");
								lngCons = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[3])));
								// If IsDate(strInput(15)) Then
								// RVSIF.ReadingDate = CDate(strInput(15))
								// Else
								Statics.RVSIF.ReadingDate = datDefaultReading;
								// End If
								if (Convert.ToByte(Strings.Left(FCConvert.ToString(Statics.RVSIF.Account), 1)[0]) != 0)
								{
									// Find the account number, book and seq by using the cross reference field
									if (GetMeterInformation_2(Statics.RVSIF.Code, ref lngBook, ref lngSeq, ref lngAcct, ref Statics.RVSIF.Previous))
									{
										// Meter Key
										strLine = Strings.Format(lngAcct, "000000");
										// Book
										strLine += Strings.Format(lngBook, "0000");
										// Sequence
										strLine += Strings.Format(lngSeq, "0000");
										// Current Reading
										Statics.RVSIF.Current = Statics.RVSIF.Previous + lngCons;
										strLine += Strings.Format(Statics.RVSIF.Current, "000000");
										// Reading Date
										strLine += Strings.Format(Statics.RVSIF.ReadingDate, "MMddyy");
										FCFileSystem.PrintLine(2, strLine);
										//Application.DoEvents();
									}
									else
									{
										// add an error line in the error array becuase the meter was not found
										Array.Resize(ref Statics.RVSMeterImportError, lngErrorIndex + 1);
										//Application.DoEvents();
										Statics.RVSMeterImportError[lngErrorIndex] = Statics.RVSIF;
										lngErrorIndex += 1;
									}
								}
								counter += 1;
							}
							lngError = 20;
							FCFileSystem.FileClose(1);
							lngError = 30;
							FCFileSystem.FileClose(2);
							lngError = 31;
							frmWait.InstancePtr.Unload();
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From RVS Extract", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                            lngError = 32;
							if (lngErrorIndex > 0)
							{
								lngError = 33;
								//Application.DoEvents();
								// show a report of the errors
								rptMeterImportErrorReport.InstancePtr.Init(1);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting RVS Extract - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertNewFarmingtonXRefExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// I think that this file will read the file created by RVS and
				// convert it into TSUTXX41.DAT in order to use the Electronic Data Entry
				// The input file has the actual current reading so I will be able to use
				// that directly instead of making a computation to find it
				string strCurDir;
				string strFileName = "";
				int ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsMaster = new clsDRWrapper();
				// This is to find the correct meter using RVS account number
				int lngBook = 0;
				int lngSeq = 0;
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				string[] strInput = null;
				string strLeader;
				int lngCons = 0;
				strLeader = "M";
				// This is hard coded for Anson/Madison Sewer District
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.csv";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						// If UCase(Right(strFileName, 3)) <> "DAT" Then
						// Ans = MsgBox("You must select a .dat file before you may continue.  Do you wish to try again?", vbQuestion + vbYesNo, "Select File?")
						// If Ans = vbYes Then
						// GoTo PickAgain
						// Else
						// Exit Sub
						// End If
						// End If
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					if (strFileName != "")
					{
						// open the input file from AA
						FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.RVSIF));
						// open the output file
						FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						lngError = 5;
						// clear the error array
						FCUtils.EraseSafe(Statics.RVSMeterImportError);
						lngError = 10;
						// default reading date
						datDefaultReading = DateTime.Today;
						object datTempDate = datDefaultReading;
						if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
						{
							//FC:FINAL:DDU:converted back to correct variable
							datDefaultReading = FCConvert.ToDateTime(datTempDate);
							lngError = 11;
							counter = 1;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true);
							GetNext:
							;
							while (!FCFileSystem.EOF(1))
							{
								//Application.DoEvents();
								// Get #1, counter, RVSIF
								strRecord = FCFileSystem.LineInput(1);
								frmWait.InstancePtr.IncrementProgress();
								// strRecord = Replace(strRecord, Chr(34) & "," & Chr(34), "|")
								// strRecord = Left(strRecord, Len(strRecord) - 2)
								strInput = Strings.Split(strRecord, ",", -1, CompareConstants.vbBinaryCompare);
								// Split(strRecord, "|")
								if (!Information.IsNumeric(strInput[4]))
								{
									goto GetNext;
								}
								Statics.RVSIF.Name = Strings.Trim(strInput[1].Replace(FCConvert.ToString(Convert.ToChar(34)), "") + " " + strInput[2].Replace(FCConvert.ToString(Convert.ToChar(34)), ""));
								Statics.RVSIF.Address1 = strInput[3].Replace(FCConvert.ToString(Convert.ToChar(34)), "");
								Statics.RVSIF.Address2 = "";
								Statics.RVSIF.Address3 = "";
								Statics.RVSIF.Zip = "";
								Statics.RVSIF.Location = "";
								Statics.RVSIF.Current = 0;
								Statics.RVSIF.Previous = 0;
								Statics.RVSIF.Account = FCConvert.ToInt32(strInput[0].Replace(FCConvert.ToString(Convert.ToChar(34)), ""));
								Statics.RVSIF.Code = strInput[0].Replace(FCConvert.ToString(Convert.ToChar(34)), "");
								lngCons = FCConvert.ToInt32(Math.Round(Conversion.Val(strInput[4])));
								// If IsDate(strInput(15)) Then
								// RVSIF.ReadingDate = CDate(strInput(15))
								// Else
								Statics.RVSIF.ReadingDate = datDefaultReading;
								// End If
								if (Convert.ToByte(Strings.Left(FCConvert.ToString(Statics.RVSIF.Account), 1)[0]) != 0)
								{
									// Find the account number, book and seq by using the cross reference field
									if (GetMeterInformation_2(Statics.RVSIF.Code, ref lngBook, ref lngSeq, ref lngAcct, ref Statics.RVSIF.Previous))
									{
										// Meter Key
										strLine = Strings.Format(lngAcct, "000000");
										// Book
										strLine += Strings.Format(lngBook, "0000");
										// Sequence
										strLine += Strings.Format(lngSeq, "0000");
										// Current Reading
										Statics.RVSIF.Current = Statics.RVSIF.Previous + lngCons;
										strLine += Strings.Format(Statics.RVSIF.Current, "000000");
										// Reading Date
										strLine += Strings.Format(Statics.RVSIF.ReadingDate, "MMddyy");
										FCFileSystem.PrintLine(2, strLine);
										//Application.DoEvents();
									}
									else
									{
										// add an error line in the error array becuase the meter was not found
										Array.Resize(ref Statics.RVSMeterImportError, lngErrorIndex + 1);
										//Application.DoEvents();
										Statics.RVSMeterImportError[lngErrorIndex] = Statics.RVSIF;
										lngErrorIndex += 1;
									}
								}
								counter += 1;
							}
							lngError = 20;
							FCFileSystem.FileClose(1);
							lngError = 30;
							FCFileSystem.FileClose(2);
							lngError = 31;
							frmWait.InstancePtr.Unload();
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From RVS Extract", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                            lngError = 32;
							if (lngErrorIndex > 0)
							{
								lngError = 33;
								//Application.DoEvents();
								// show a report of the errors
								rptMeterImportErrorReport.InstancePtr.Init(1);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting RVS Extract - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void CreateExtractForReader(ref int[] BookArray)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsDefaults = new clsDRWrapper();
				rsDefaults.OpenRecordset("SELECT * FROM UtilityBilling");
				if (rsDefaults.EndOfFile() != true && rsDefaults.BeginningOfFile() != true)
				{
					var vbPorterVar = rsDefaults.Get_Fields_String("ReaderExtractType");
					if (vbPorterVar == "T")
					{
						CreateTiSalesExtract_6(BookArray, rsDefaults.Get_Fields_Boolean("ExportNoBillRecords"));
					}
					else if (vbPorterVar == "P")
					{
						CreatePrescottExtract_78(BookArray, rsDefaults.Get_Fields_Boolean("ExportNoBillRecords"), FCConvert.ToInt32(Conversion.Val(rsDefaults.Get_Fields_Int32("RemoteReaderExportType"))), rsDefaults.Get_Fields_Boolean("ExportLocation"));
					}
					else if (vbPorterVar == "B")
					{
						// kk 01182013 trout-896  Implement Badger Extract/Import
						CreateBadgerExtract_78(BookArray, rsDefaults.Get_Fields_Boolean("ExportNoBillRecords"), FCConvert.ToInt32(Conversion.Val(rsDefaults.Get_Fields_Int32("RemoteReaderExportType"))), rsDefaults.Get_Fields_Boolean("ExportLocation"));
					}
					else if (vbPorterVar == "R")
					{
						MessageBox.Show("This option has not been created.", "Not Created", MessageBoxButtons.OK, MessageBoxIcon.Warning);
						return;
					}
					else if (vbPorterVar == "E")
					{
						// MAL@20071019: New Extract Type ; Tracker Reference: 11058
						CreateEZReaderExtract_6(BookArray, rsDefaults.Get_Fields_Boolean("ExportNoBillRecords"));
					}
                    else if (vbPorterVar == "S")
                    {
                        CreateVflexExtract(ref BookArray, rsDefaults.Get_Fields_Boolean("ExportNoBillRecords"));
                    }
					else
					{
						if (FCConvert.ToString(rsDefaults.Get_Fields_String("ReaderExtractType")) == "")
						{
							// kgk trout-754  Add handling for Electronic Data Entry processing only - no reader extract
							MessageBox.Show("You must select the reader type in your customize screen before you may continue with this process.", "Invalid Reader Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						else
						{
							MessageBox.Show("The selected extract type has no reader associated with it.", "Invalid Reader Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
						}
						return;
					}
				}
				else
				{
					MessageBox.Show("You must select the reader type in your customize screen before you may continue with this process.", "Invalid Reader Type", MessageBoxButtons.OK, MessageBoxIcon.Information);
					return;
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Extract for Reader", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private static void CreateEZReaderExtract_6(int[] BookArray, bool boolShowNoBill)
		{
			CreateEZReaderExtract(ref BookArray, ref boolShowNoBill);
		}

		private static void CreateEZReaderExtract(ref int[] BookArray, ref bool boolShowNoBill)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				string strSql = "";
				string strBooks = "";
				int counter;
				string strLine = "";
				int lngMeterCounter;
				string strBookNum = "";
				string strName = "";
				string strLocation = "";
				string strSequence = "";
				string strMeterNum = "";
				string strAccount = "";
				string strMeterType = "";
				string strMessage = "";
				string strHighAudit = "";
				string strLowAudit = "";
				string strCurrRead = "";
				string strReadDate = "";
				string strReadTime = "";
				string strDataPath;
				FCFileSystem.FileClose(1);
				CheckForEZDataDirectory();
				//FC:FINAL:AM:#1306 - don't use C drive
				//strDataPath = "C:\\EZReader\\Download.dat";
				strDataPath = "Download.dat";
				FCFileSystem.FileOpen(1, strDataPath, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				lngMeterCounter = 0;
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					strBooks = "Book " + FCConvert.ToString(BookArray[1]);
					strSql = " = " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					strBooks = "Books ";
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strSql += FCConvert.ToString(BookArray[counter]) + ",";
						strBooks += FCConvert.ToString(BookArray[counter]) + ",";
					}
					strSql = "IN (" + Strings.Left(strSql, strSql.Length - 1) + ")";
					strBooks = Strings.Left(strBooks, strBooks.Length - 1);
				}
				// MAL@20071214: Tracker Reference 11538
				// If boolShowNoBill Then
				// rsInfo.OpenRecordset "SELECT BookNumber, Sequence, Service, Name,StreetNumber, StreetName, Apt, " &
				// "SerialNumber, Radio, RadioAccessType, CurrentReading, PreviousReading, MeterTable.Comment as Comment, " &
				// "Digits, AccountNumber,CurrentReadingDate,PreviousReadingDate, MeterKey " &
				// "FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL &
				// " AND NOT FinalBilled AND (Service = 'W' OR Service = 'B') ORDER BY BookNumber, Sequence, AccountNumber"
				// Else
				// rsInfo.OpenRecordset "SELECT BookNumber, Sequence, Service, Name,StreetNumber, StreetName, Apt, " &
				// "SerialNumber, Radio, RadioAccessType, CurrentReading, PreviousReading, MeterTable.Comment as Comment, " &
				// "Digits, AccountNumber,CurrentReadingDate,PreviousReadingDate, MeterKey " &
				// "FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL &
				// " AND NOT FinalBilled AND (Service = 'W' OR Service = 'B') AND (NOT MeterTable.NoBill AND NOT Master.NoBill) " &
				// "ORDER BY BookNumber, Sequence, AccountNumber"
				// End If
				if (boolShowNoBill)
				{
					rsInfo.OpenRecordset("SELECT BookNumber, Sequence, Service, p.FullNameLF AS Name, StreetNumber, StreetName, Apt, " + "SerialNumber, Radio, RadioAccessType, CurrentReading, PreviousReading, MeterTable.Comment as Comment, " + "Digits, AccountNumber,CurrentReadingDate,PreviousReadingDate, MeterTable.ID AS MeterKey " + "FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSql + " AND ISNULL(Deleted,0) = 0 AND ISNULL(FinalBilled,0) = 0 AND (Service = 'W' OR Service = 'B') ORDER BY BookNumber, Sequence, AccountNumber");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT BookNumber, Sequence, Service, p.FullNameLF AS Name, StreetNumber, StreetName, Apt, " + "SerialNumber, Radio, RadioAccessType, CurrentReading, PreviousReading, MeterTable.Comment as Comment, " + "Digits, AccountNumber,CurrentReadingDate,PreviousReadingDate, MeterTable.ID AS MeterKey " + "FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID)  INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSql + " AND ISNULL(Deleted,0) = 0 AND ISNULL(FinalBilled,0) = 0 AND (Service = 'W' OR Service = 'B') AND (ISNULL(MeterTable.NoBill,0) = 0 AND ISNULL(Master.NoBill,0) = 0) " + "ORDER BY BookNumber, Sequence, AccountNumber");
				}
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						// Set Variables for items that may be longer then the allowed length
						strBookNum = Strings.Right(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_Int32("BookNumber"))), 2);
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						strSequence = Strings.Right(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("Sequence"))), 10);
						strName = Strings.Left(FCConvert.ToString(rsInfo.Get_Fields_String("Name")), 25);
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						strLocation = Strings.Left(Strings.Trim(rsInfo.Get_Fields("StreetNumber") + " " + rsInfo.Get_Fields_String("StreetName") + " " + rsInfo.Get_Fields_String("Apt")), 21);
						strMeterNum = Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("SerialNumber"))), 20);
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						strAccount = Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("AccountNumber"))), 12);
						strMessage = Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Comment"))), 100);
						if (rsInfo.Get_Fields_Int32("RadioAccessType") == 0)
						{
							strMeterType = "V";
						}
						else if (rsInfo.Get_Fields_Int32("RadioAccessType") == 1)
						{
							strMeterType = "T";
						}
						else if (rsInfo.Get_Fields_Int32("RadioAccessType") == 2)
						{
							strMeterType = "R";
						}
						else
						{
							strMeterType = "";
						}
						GetAverages_EZReader(rsInfo.Get_Fields_Int32("MeterKey"), ref strHighAudit, ref strLowAudit);
						strLine = Strings.StrDup(280, " ");
						// Route #
						fecherFoundation.Strings.MidSet(ref strLine, 1, Strings.Len(strBookNum), strBookNum);
						// Read Sequence
						fecherFoundation.Strings.MidSet(ref strLine, 3, Strings.Len(strSequence), strSequence);
						// Previous Tamper
						fecherFoundation.Strings.MidSet(ref strLine, 13, 1, Strings.Space(1));
						// New Tamper
						fecherFoundation.Strings.MidSet(ref strLine, 14, 1, Strings.Space(1));
						// Future Use
						fecherFoundation.Strings.MidSet(ref strLine, 15, 6, Strings.Space(6));
						// Service Type
						fecherFoundation.Strings.MidSet(ref strLine, 21, 1, "W");
						// Name
						fecherFoundation.Strings.MidSet(ref strLine, 22, Strings.Len(strName), strName);
						// Location
						fecherFoundation.Strings.MidSet(ref strLine, 47, Strings.Len(strLocation), strLocation);
						// Meter Number
						fecherFoundation.Strings.MidSet(ref strLine, 68, Strings.Len(strMeterNum), strMeterNum);
						// Meter Type
						fecherFoundation.Strings.MidSet(ref strLine, 88, Strings.Len(strMeterType), strMeterType);
						// High Audit Reading
						fecherFoundation.Strings.MidSet(ref strLine, 89, Strings.Len(strHighAudit), strHighAudit);
						// Low Audit Reading
						fecherFoundation.Strings.MidSet(ref strLine, 99, Strings.Len(strLowAudit), strLowAudit);
						// Message
						fecherFoundation.Strings.MidSet(ref strLine, 109, Strings.Len(strMessage), strMessage);
						// Number of Dials
						int length = FCConvert.ToInt32(rsInfo.Get_Fields_Int32("Digits"));
						fecherFoundation.Strings.MidSet(ref strLine, 209, 1, length.ToString());
						// Future Use
						fecherFoundation.Strings.MidSet(ref strLine, 211, 1, Strings.Space(1));
						// Future Use
						fecherFoundation.Strings.MidSet(ref strLine, 212, 5, Strings.Space(5));
						// Account #
						fecherFoundation.Strings.MidSet(ref strLine, 217, Strings.Len(strAccount), strAccount);
						// New Reading
						fecherFoundation.Strings.MidSet(ref strLine, 229, 10, Strings.Space(10));
						// New Demand Reading
						fecherFoundation.Strings.MidSet(ref strLine, 239, 10, Strings.Space(10));
						// Date of Reading
						fecherFoundation.Strings.MidSet(ref strLine, 249, 6, Strings.Space(6));
						// Time of Reading
						fecherFoundation.Strings.MidSet(ref strLine, 255, 6, Strings.Space(6));
						// New Route Sequence
						fecherFoundation.Strings.MidSet(ref strLine, 261, 10, Strings.Space(10));
						// Future Use
						fecherFoundation.Strings.MidSet(ref strLine, 271, 2, Strings.Space(2));
						// Future Use
						fecherFoundation.Strings.MidSet(ref strLine, 273, 8, Strings.Space(8));
						FCFileSystem.PrintLine(1, strLine);
						lngMeterCounter += 1;
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				FCFileSystem.FileClose(1);
				//FC:FINAL:AM:#1306 - download the file
				//MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter) + "\r\n" + "File " + strDataPath, "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, strDataPath));
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating EZ Reader Extract", MessageBoxButtons.OK, MessageBoxIcon.Hand);
				/*? Resume; */
			}
		}
		// vbPorter upgrade warning: strHighAudit As string	OnWrite(string, int)
		// vbPorter upgrade warning: strLowAudit As string	OnWrite(string, short)
		private static void GetAverages_EZReader(int lngMeterKey, ref string strHighAudit, ref string strLowAudit)
		{
			// MAL@20071019: Added function to return high audit reading for EZ Reader export
			bool blnHasHist = false;
			clsDRWrapper rsMeter = new clsDRWrapper();
			double dblTotal = 0;
			double dblAvg = 0;
			int intCount;
			string strResult = "";
			int intLen;
			intCount = 0;
			rsMeter.OpenRecordset("SELECT * FROM MeterConsumption WHERE MeterKey = " + FCConvert.ToString(lngMeterKey) + " ORDER BY BillDate Desc", modExtraModules.strUTDatabase);
			if (rsMeter.RecordCount() > 0)
			{
				blnHasHist = true;
				strLowAudit = FCConvert.ToString(rsMeter.Get_Fields_Int32("Consumption"));
				// First Record is Low Consumption
				while (!(intCount == 3 || rsMeter.EndOfFile()))
				{
					dblTotal += rsMeter.Get_Fields_Int32("Consumption");
					intCount += 1;
					rsMeter.MoveNext();
				}
			}
			else
			{
				blnHasHist = false;
				strLowAudit = FCConvert.ToString(0);
			}
			if (blnHasHist)
			{
				dblAvg = (dblTotal / intCount) * 2;
				strHighAudit = FCConvert.ToString(FCUtils.Round(dblAvg, 0));
			}
			else
			{
				strHighAudit = FCConvert.ToString(0);
			}
			if (strHighAudit.Length < 10)
			{
				while (!(strHighAudit.Length == 10))
				{
					strHighAudit = "0" + strHighAudit;
				}
			}
			if (strLowAudit.Length < 10)
			{
				while (!(strLowAudit.Length == 10))
				{
					strLowAudit = "0" + strLowAudit;
				}
			}
		}

		private static void CreateTiSalesExtract_6(int[] BookArray, bool boolShowNoBill)
		{
			CreateTiSalesExtract(ref BookArray, ref boolShowNoBill);
		}

		private static void CreateTiSalesExtract(ref int[] BookArray, ref bool boolShowNoBill)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				string strSql = "";
				string strBooks = "";
				int counter;
				string strLine = "";
				int lngMeterCounter;
				bool blnIsClear = false;
				clsDRWrapper rsBook = new clsDRWrapper();
				FCFileSystem.FileClose(1);
				FCFileSystem.FileOpen(1, "TSUTXX40.IMP", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				lngMeterCounter = 0;
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					strBooks = "Book " + FCConvert.ToString(BookArray[1]);
					strSql = " = " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					strBooks = "Books ";
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strSql += FCConvert.ToString(BookArray[counter]) + ",";
						strBooks += FCConvert.ToString(BookArray[counter]) + ",";
					}
					strSql = "IN (" + Strings.Left(strSql, strSql.Length - 1) + ")";
					strBooks = Strings.Left(strBooks, strBooks.Length - 1);
				}
				// MAL@20071214: Tracker Reference 11538
				if (boolShowNoBill)
				{
					rsInfo.OpenRecordset("SELECT BookNumber, Sequence, Digits, Remote, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt, MapLot, Radio, RadioAccessType, MXU, CurrentReading, PreviousReading, MeterNumber FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSql + "  AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0 ORDER BY BookNumber, Sequence, AccountNumber");
				}
				else
				{
					rsInfo.OpenRecordset("SELECT BookNumber, Sequence, Digits, Remote, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt, MapLot, Radio, RadioAccessType, MXU, CurrentReading, PreviousReading, MeterNumber FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSql + "  AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0 AND (ISNULL(MeterTable.NoBill,0) = 0 AND ISNULL(Master.NoBill,0) = 0) ORDER BY BookNumber, Sequence, AccountNumber");
				}
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						strLine = Strings.StrDup(507, " ");
						int length = FCConvert.ToInt32(Strings.Len(rsInfo.Get_Fields_Int32("BookNumber")));
						string newString = FCConvert.ToString(rsInfo.Get_Fields_Int32("BookNumber"));
						fecherFoundation.Strings.MidSet(ref strLine, 1, length, newString);
						// MAL@20090623: Tracker Reference: 19163
						// Determine if book is cleared to get right reading
						rsBook.OpenRecordset("SELECT * FROM Book WHERE BookNumber = " + rsInfo.Get_Fields_Int32("BookNumber"), modExtraModules.strUTDatabase);
						if (rsBook.RecordCount() > 0)
						{
							blnIsClear = FCConvert.CBool(rsBook.Get_Fields_String("CurrentStatus") == "CE");
						}
						else
						{
							blnIsClear = false;
						}
						fecherFoundation.Strings.MidSet(ref strLine, 11, 4, "0000");
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						length = Strings.Len(rsInfo.Get_Fields("Sequence"));
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						newString = FCConvert.ToString(rsInfo.Get_Fields("Sequence"));
						fecherFoundation.Strings.MidSet(ref strLine, 15, length, newString);
						// Mid(strLine, 19, 2) = "00"
						newString = Strings.Format(rsInfo.Get_Fields_Int32("MeterNumber"), "00");
						fecherFoundation.Strings.MidSet(ref strLine, 19, 2, newString);
						// trout-1294
						newString = FCConvert.ToString(rsInfo.Get_Fields_Int32("Digits"));
						fecherFoundation.Strings.MidSet(ref strLine, 28, 1, newString);
						// Mid(strLine, 29, Len(rsInfo.Fields("Remote"))) = rsInfo.Fields("Remote")
						// MAL@20090623: Call Reference: 117295/19163
						// If rsInfo.Fields("Radio") Then
						if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("RadioAccessType")) == 1)
						{
							fecherFoundation.Strings.MidSet(ref strLine, 135, 1, "A");
							length = Strings.Len(rsInfo.Get_Fields_String("MXU"));
							newString = FCConvert.ToString(rsInfo.Get_Fields_String("MXU"));
							fecherFoundation.Strings.MidSet(ref strLine, 29, length, newString);
						}
						else if (FCConvert.ToInt32(rsInfo.Get_Fields_Int32("RadioAccessType")) == 2)
						{
							fecherFoundation.Strings.MidSet(ref strLine, 135, 1, "Z");
							length = Strings.Len(rsInfo.Get_Fields_String("MXU"));
							newString = FCConvert.ToString(rsInfo.Get_Fields_String("MXU"));
							fecherFoundation.Strings.MidSet(ref strLine, 29, length, newString);
						}
						else
						{
							fecherFoundation.Strings.MidSet(ref strLine, 135, 1, "M");
							length = Strings.Len(rsInfo.Get_Fields_String("Remote"));
							newString = FCConvert.ToString(rsInfo.Get_Fields_String("Remote"));
							fecherFoundation.Strings.MidSet(ref strLine, 29, length, newString);
						}
						if (blnIsClear)
						{
							length = Strings.Len(rsInfo.Get_Fields_Int32("PreviousReading"));
							newString = FCConvert.ToString(rsInfo.Get_Fields_Int32("PreviousReading"));
							fecherFoundation.Strings.MidSet(ref strLine, 163, length, newString);
						}
						else
						{
							length = Strings.Len(rsInfo.Get_Fields_Int32("CurrentReading"));
							newString = FCConvert.ToString(rsInfo.Get_Fields_Int32("CurrentReading"));
							fecherFoundation.Strings.MidSet(ref strLine, 163, length, newString);
						}
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						length = Strings.Len(rsInfo.Get_Fields("StreetNumber") + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))));
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						newString = FCConvert.ToString(rsInfo.Get_Fields("StreetNumber") + Strings.Trim(rsInfo.Get_Fields_String("Apt")) + " " + Strings.Trim(rsInfo.Get_Fields_String("StreetName")));
						fecherFoundation.Strings.MidSet(ref strLine, 179, length, newString);
						length = Strings.Len(rsInfo.Get_Fields_String("MapLot"));
						newString = FCConvert.ToString(rsInfo.Get_Fields_String("MapLot"));
						fecherFoundation.Strings.MidSet(ref strLine, 203, length, newString);
						length = Strings.Len(rsInfo.Get_Fields_String("Name"));
						newString = FCConvert.ToString(rsInfo.Get_Fields_String("Name"));
						fecherFoundation.Strings.MidSet(ref strLine, 227, length, newString);
						length = Strings.Len(Strings.Format(DateTime.Today, "MM/dd/yy") + " " + Strings.Format(DateAndTime.TimeOfDay, "hh:mm:ss"));
						newString = Strings.Format(DateTime.Today, "MM/dd/yy") + " " + Strings.Format(DateAndTime.TimeOfDay, "hh:mm:ss");
						fecherFoundation.Strings.MidSet(ref strLine, 251, length, newString);
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						length = Strings.Len(rsInfo.Get_Fields("AccountNumber"));
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						newString = FCConvert.ToString(rsInfo.Get_Fields("AccountNumber"));
						fecherFoundation.Strings.MidSet(ref strLine, 468, length, newString);
						FCFileSystem.PrintLine(1, strLine);
						lngMeterCounter += 1;
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				FCFileSystem.FileClose(1);
				//FC:FINAL:AM:#1306 - download the file
				//MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter) + "\r\n" + "File " + Application.MapPath("\\") + "\\TSUTXX40.IMP", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TSUTXX40.IMP"));
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Ti Sales Extract", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngExportVersion As int	OnWriteFCConvert.ToDouble(
		private static void CreatePrescottExtract_78(int[] BookArray, bool boolShowNoBill, int lngExportVersion = 0, bool boolShowLocation = false)
		{
			CreatePrescottExtract(ref BookArray, ref boolShowNoBill, lngExportVersion, boolShowLocation);
		}

		private static void CreatePrescottExtract(ref int[] BookArray, ref bool boolShowNoBill, int lngExportVersion = 0, bool boolShowLocation = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				string strSql = "";
				string strBooks = "";
				int counter;
				string strLine = "";
				int lngMeterCounter;
				FCFileSystem.FileClose(1);
				FCFileSystem.FileOpen(1, "TSUTXX41.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				lngMeterCounter = 0;
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					strBooks = "Book " + FCConvert.ToString(BookArray[1]);
					strSql = " = " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					strBooks = "Books ";
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strSql += FCConvert.ToString(BookArray[counter]) + ",";
						strBooks += FCConvert.ToString(BookArray[counter]) + ",";
					}
					strSql = "IN (" + Strings.Left(strSql, strSql.Length - 1) + ")";
					strBooks = Strings.Left(strBooks, strBooks.Length - 1);
				}
				// MAL@20071214: Added check for deleted accounts
				// Tracker Reference: 11538
				// If boolShowNoBill Then
				// rsInfo.OpenRecordset "SELECT MeterTable.Comment as Comment, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Remote, AccountNumber, Name, StreetName, StreetNumber, Apt, MapLot, Radio, MXU, RadioAccessType FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL & " AND NOT FinalBilled ORDER BY BookNumber, Sequence, AccountNumber"
				// Else
				// rsInfo.OpenRecordset "SELECT MeterTable.Comment as Comment, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Remote, AccountNumber, Name, StreetName, StreetNumber, Apt, MapLot, Radio, MXU, RadioAccessType FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.Key) WHERE BookNumber " & strSQL & " AND NOT FinalBilled AND (NOT MeterTable.NoBill AND NOT Master.NoBill) ORDER BY BookNumber, Sequence, AccountNumber"
				// End If
				// kgk 12-19-2011 trout-767  Added MeterTable.Location to query
				if (boolShowNoBill)
				{
					//rsInfo.OpenRecordset("SELECT MeterTable.Comment as Comment, MeterTable.Location, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Remote, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt, MapLot, Radio, MXU, RadioAccessType, Long, Lat FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSql + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0  ORDER BY BookNumber, Sequence, AccountNumber");
                    rsInfo.OpenRecordset ("SELECT p.FullNameLF as Name, MeterTable.Comment as Comment, MeterTable.Location, MeterNumber, BillingStatus, SerialNumber, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Remote, AccountNumber, StreetName, StreetNumber, Apt, MapLot, Radio, MXU, RadioAccessType, Long, Lat, ReaderCode FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.id) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSql + " AND isnull(FinalBilled,0) = 0 AND isnull(Deleted,0) = 0 ORDER BY BookNumber, Sequence, AccountNumber");
                }
				else
				{
                    //rsInfo.OpenRecordset("SELECT MeterTable.Comment as Comment, MeterTable.Location, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Remote, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt, MapLot, Radio, MXU, RadioAccessType, Long, Lat FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSql + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0  AND (ISNULL(MeterTable.NoBill,0) = 0 AND ISNULL(Master.NoBill,0) = 0) ORDER BY BookNumber, Sequence, AccountNumber");
                    rsInfo.OpenRecordset ("SELECT p.FullNameLF as Name, MeterTable.Comment as Comment, MeterTable.Location, MeterNumber, BillingStatus, SerialNumber, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Remote, AccountNumber,  StreetName, StreetNumber, Apt, MapLot, Radio, MXU, RadioAccessType, Long, Lat, ReaderCode FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.id) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE BookNumber " + strSql + " AND  isnull(FinalBilled,0) = 0 AND isnull(Deleted,0) = 0 AND (isnull(MeterTable.NoBill,0) = 0 AND isnull(Master.NoBill,0) = 0) ORDER BY BookNumber, Sequence, AccountNumber");
                }
				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						strLine = "";
						strLine += Strings.Format(rsInfo.Get_Fields_Int32("BookNumber"), "0000");
						// 4
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						strLine += Strings.Format(rsInfo.Get_Fields("Sequence"), "0000");
						// 4
						// DJW 01092013 TROUT-882 clip accoutn to 6 digits if the length is more
						// KGK 02282013 TROUT-882 Only clip for Norway
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						if (FCConvert.ToString(rsInfo.Get_Fields("AccountNumber")).Length > 6 && Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "NORWAY")
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							strLine += Strings.Format(Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("AccountNumber")), 6), "000000");
							// 6
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							strLine += Strings.Format(rsInfo.Get_Fields("AccountNumber"), "000000");
							// 6
						}
						if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))).Length >= 34)
						{
							// 34
							strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))), 34);
						}
						else
						{
							strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))) + Strings.StrDup(34 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))).Length, " ");
						}
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("StreetNumber"))).Length >= 5)
						{
							// 5
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("StreetNumber"))), 5);
						}
						else
						{
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
							strLine += Strings.StrDup(5 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("StreetNumber"))).Length, " ") + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("StreetNumber")));
						}
						if (Strings.Trim(rsInfo.Get_Fields_String("StreetName") + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt")))).Length >= 26)
						{
							// 26
							strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))), 26 - (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt"))).Length + 1)) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt")));
						}
						else
						{
							strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt"))) + Strings.StrDup(26 - (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt")))).Length, " ");
						}
						if (lngExportVersion == 0)
						{
							if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Remote"))).Length >= 15)
							{
								// 15
								strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Remote"))), 15);
							}
							else
							{
								strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Remote"))) + Strings.StrDup(15 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Remote"))).Length, " ");
							}
						}
						else
						{
							if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Remote"))).Length >= 30)
							{
								// 30
								strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Remote"))), 30);
							}
							else
							{
								strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Remote"))) + Strings.StrDup(30 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Remote"))).Length, " ");
							}
						}
						strLine += Strings.Format(rsInfo.Get_Fields_Int32("PreviousReading"), "000000000");
						// 9
						// DJW@01092013 TROUT-902 Made current reading 0 if it was -1
						if (rsInfo.Get_Fields_Int32("CurrentReading") < 0)
						{
							strLine += "000000000";
						}
						else
						{
							strLine += Strings.Format(rsInfo.Get_Fields_Int32("CurrentReading"), "000000000");
							// 9
						}
						// kgk 12-19-2011 trout-767  Add option to write Location in place of Comment
						if (!boolShowLocation)
						{
							if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Comment"))).Length >= 40)
							{
								// 40
								strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Comment"))), 40);
							}
							else
							{
								strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Comment"))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Comment"))).Length, " ");
							}
						}
						else
						{
							if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Location"))).Length >= 40)
							{
								// 40
								strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Location"))), 40);
							}
							else
							{
								strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Location"))) + Strings.StrDup(40 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Location"))).Length, " ");
							}
						}
						strLine += Strings.Format(DateTime.Today, "MM/dd/yy");
						// 10
						if (FCConvert.ToBoolean(lngExportVersion))
						{
							if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("MXU"))).Length >= 30)
							{
								// 30
								strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("MXU"))), 30);
							}
							else
							{
								strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("MXU"))) + Strings.StrDup(30 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("MXU"))).Length, " ");
							}
							// Reader Type
							if (Conversion.Val(rsInfo.Get_Fields_Int32("RadioAccessType")) == 0)
							{
								strLine += "M ";
							}
							else if (Conversion.Val(rsInfo.Get_Fields_Int32("RadioAccessType")) == 1)
							{
								strLine += "B ";
							}
							else if (Conversion.Val(rsInfo.Get_Fields_Int32("RadioAccessType")) == 2)
							{
								strLine += "BB";
							}
							else if (Conversion.Val(rsInfo.Get_Fields_Int32("RadioAccessType")) == 3)
							{
								strLine += "BC";
							}
							// GIS Lat
							strLine += rsInfo.Get_Fields_String("Lat") + Strings.StrDup(15 - FCConvert.ToString(rsInfo.Get_Fields_String("Lat")).Length, " ");
							// GIS Long
							strLine += rsInfo.Get_Fields_String("Long") + Strings.StrDup(15 - FCConvert.ToString(rsInfo.Get_Fields_String("Long")).Length, " ");
							// Extra space
							strLine += Strings.StrDup(100, " ");
						}
						FCFileSystem.PrintLine(1, strLine);
						lngMeterCounter += 1;
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				FCFileSystem.FileClose(1);
				//FC:FINAL:AM:#1306 - download the file
				//MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter) + "\r\n" + "File " + Application.MapPath("\\") + "\\TSUTXX41.ASC", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TSUTXX41.ASC"));
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Prescott Extract", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: lngExportVersion As int	OnWriteFCConvert.ToDouble(
		private static void CreateBadgerExtract_78(int[] BookArray, bool boolShowNoBill, int lngExportVersion = 0, bool boolShowLocation = false)
		{
			CreateBadgerExtract(ref BookArray, ref boolShowNoBill, lngExportVersion, boolShowLocation);
		}

		private static void CreateBadgerExtract(ref int[] BookArray, ref bool boolShowNoBill, int lngExportVersion = 0, bool boolShowLocation = false)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				string strSql = "";
				string strBooks = "";
				int counter;
				string strLine = "";
				int lngMeterCounter;
				string strTemp = "";
				FCFileSystem.FileClose(1);
				FCFileSystem.FileOpen(1, "TSUTXX41.ASC", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				lngMeterCounter = 0;
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					strBooks = "Book " + FCConvert.ToString(BookArray[1]);
					strSql = " = " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					strBooks = "Books ";
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strSql += FCConvert.ToString(BookArray[counter]) + ",";
						strBooks += FCConvert.ToString(BookArray[counter]) + ",";
					}
					strSql = "IN (" + Strings.Left(strSql, strSql.Length - 1) + ")";
					strBooks = Strings.Left(strBooks, strBooks.Length - 1);
				}
				if (boolShowNoBill)
				{				
                    rsInfo.OpenRecordset(
	                    "SELECT DeedName1 as Name, MeterTable.Comment as Comment, MeterTable.Location, MeterNumber, BillingStatus, SerialNumber, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Remote, AccountNumber, StreetName, StreetNumber, Apt, MapLot, Radio, MXU, RadioAccessType, Long, Lat, ReaderCode FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.id) WHERE BookNumber " + strSql + " AND isnull(FinalBilled,0) = 0 AND isnull(Deleted,0) = 0 ORDER BY BookNumber, Sequence, AccountNumber");
                }
				else
				{					
                    rsInfo.OpenRecordset(
	                    "SELECT DeedName1 as Name, MeterTable.Comment as Comment, MeterTable.Location, MeterNumber, BillingStatus, SerialNumber, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Remote, AccountNumber,  StreetName, StreetNumber, Apt, MapLot, Radio, MXU, RadioAccessType, Long, Lat, ReaderCode FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.id) WHERE BookNumber " + strSql + " AND  isnull(FinalBilled,0) = 0 AND isnull(Deleted,0) = 0 AND (isnull(MeterTable.NoBill,0) = 0 AND isnull(Master.NoBill,0) = 0) ORDER BY BookNumber, Sequence, AccountNumber");
                }

				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						Statics.BadgerIF.zFiller0 = "";
						// Customer Name - 20 char, Left Justify, Space Fill
						Statics.BadgerIF.CustomerName = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name")));
						// Service Address - 20 char, Left Justify, Space Fill
						// TODO Get_Fields: Check the table for the column [StreetNumber] and replace with corresponding Get_Field method
						strTemp = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("StreetNumber")));
						if (strTemp != "")
						{
							strTemp += " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt")));
						}
						else
						{
							strTemp = Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt")));
						}
						Statics.BadgerIF.ServiceAddress = Strings.Trim(strTemp);
						// Module Serial # (MXU) - 10 Char, Left Justify, Space Fill
						Statics.BadgerIF.ModuleSerial = rsInfo.Get_Fields_String("MXU");
						Statics.BadgerIF.zFiller1 = "";
						Statics.BadgerIF.ReadType = "";
						Statics.BadgerIF.ServiceCode = FCConvert.ToString(rsInfo.Get_Fields_Int32("MeterNumber"));
						Statics.BadgerIF.MeterType = "W";
						Statics.BadgerIF.NetworkID = "";
						Statics.BadgerIF.MeterSerial = rsInfo.Get_Fields_String("SerialNumber");
						Statics.BadgerIF.zFiller2 = "";
						Statics.BadgerIF.HighReadAudit = " 99999999";
						// XXX  Need to calc high meter reading limit
						Statics.BadgerIF.LowReadAudit = "        0";
						// XXX  Probably don't bother
						if (FCConvert.ToString(rsInfo.Get_Fields_String("BillingStatus")) == "B" || FCConvert.ToString(rsInfo.Get_Fields_String("BillingStatus")) == "S" || FCConvert.ToString(rsInfo.Get_Fields_String("BillingStatus")) == "W")
						{
							if (rsInfo.Get_Fields_Int32("CurrentReading") < 0)
							{
								Statics.BadgerIF.CurrentReading = "";
							}
							else
							{
								Statics.BadgerIF.CurrentReading = FCConvert.ToString(Conversion.Val(rsInfo.Get_Fields_Int32("CurrentReading")));
							}
						}
						else
						{
							if (rsInfo.Get_Fields_Int32("PreviousReading") < 0)
							{
								Statics.BadgerIF.CurrentReading = "";
							}
							else
							{
								Statics.BadgerIF.CurrentReading = FCConvert.ToString(Conversion.Val(rsInfo.Get_Fields_Int32("PreviousReading")));
							}
						}
						Statics.BadgerIF.ReadTime = "";
						Statics.BadgerIF.ReadMethod = "";
						Statics.BadgerIF.TamperMode = "";
						Statics.BadgerIF.AlertCode = "";
						Statics.BadgerIF.RouteNumber = FCConvert.ToString(rsInfo.Get_Fields_Int32("BookNumber"));
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						Statics.BadgerIF.AccountNumber = FCConvert.ToString(rsInfo.Get_Fields("AccountNumber"));
						Statics.BadgerIF.ReadDate = "";
						if (rsInfo.Get_Fields_Int32("RadioAccessType") == 0)
						{
							// Manual
							Statics.BadgerIF.DeviceCode = "C";
						}
						else if (rsInfo.Get_Fields_Int32("RadioAccessType") == 1)
						{
							// PI Trace
							Statics.BadgerIF.DeviceCode = "P";
						}
						else if (rsInfo.Get_Fields_Int32("RadioAccessType") == 2)
						{
							// MMI Trace
							Statics.BadgerIF.DeviceCode = "M";
						}
						else if (rsInfo.Get_Fields_Int32("RadioAccessType") == 3)
						{
							// Dialog/Badger Touch
							Statics.BadgerIF.DeviceCode = "D";
						}
						else if (rsInfo.Get_Fields_Int32("RadioAccessType") == 4)
						{
							// ORION
							Statics.BadgerIF.DeviceCode = "Z";
						}
						else if (rsInfo.Get_Fields_Int32("RadioAccessType") == 5)
						{
							// ORION ME/SE
							Statics.BadgerIF.DeviceCode = "N";
						}
						else if (rsInfo.Get_Fields_Int32("RadioAccessType") == 6)
						{
							// GALAXY
							Statics.BadgerIF.DeviceCode = "G";
						}
						else
						{
							Statics.BadgerIF.DeviceCode = "C";
						}
						Statics.BadgerIF.Latitude = rsInfo.Get_Fields_String("Lat");
						Statics.BadgerIF.zFiller3 = "";
						Statics.BadgerIF.TestCircleCode = rsInfo.Get_Fields_String("ReaderCode");
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						Statics.BadgerIF.ReadingSequence = FCConvert.ToString(rsInfo.Get_Fields("Sequence"));
						Statics.BadgerIF.MfgModel = "";
						Statics.BadgerIF.UtilityUseOnly = "";
						Statics.BadgerIF.ReaderID = "";
						Statics.BadgerIF.ReadCode1 = "";
						Statics.BadgerIF.ReadCode2 = "";
						Statics.BadgerIF.ReadCode3 = "";
						Statics.BadgerIF.Location = rsInfo.Get_Fields_String("Location");
						Statics.BadgerIF.zFiller4 = "";
						Statics.BadgerIF.Longitude = rsInfo.Get_Fields_String("Long");
						Statics.BadgerIF.zFiller5 = "";
						Statics.BadgerIF.CRLF = ("\r\n");
						strLine = Statics.BadgerIF.zFiller0 + Statics.BadgerIF.CustomerName + Statics.BadgerIF.ServiceAddress + Statics.BadgerIF.ModuleSerial + Statics.BadgerIF.zFiller1 + Statics.BadgerIF.ReadType + Statics.BadgerIF.ServiceCode + Statics.BadgerIF.MeterType + Statics.BadgerIF.NetworkID + Statics.BadgerIF.MeterSerial + Statics.BadgerIF.zFiller2 + Statics.BadgerIF.HighReadAudit + Statics.BadgerIF.LowReadAudit + Statics.BadgerIF.CurrentReading + Statics.BadgerIF.ReadTime + Statics.BadgerIF.ReadMethod + Statics.BadgerIF.TamperMode + Statics.BadgerIF.AlertCode + Statics.BadgerIF.RouteNumber + Statics.BadgerIF.AccountNumber + Statics.BadgerIF.ReadDate + Statics.BadgerIF.DeviceCode + Statics.BadgerIF.Latitude + Statics.BadgerIF.zFiller3 + Statics.BadgerIF.TestCircleCode + Statics.BadgerIF.ReadingSequence + Statics.BadgerIF.MfgModel + Statics.BadgerIF.UtilityUseOnly + Statics.BadgerIF.ReaderID + Statics.BadgerIF.ReadCode1 + Statics.BadgerIF.ReadCode2 + Statics.BadgerIF.ReadCode3 + Statics.BadgerIF.Location + Statics.BadgerIF.zFiller4 + Statics.BadgerIF.Longitude + Statics.BadgerIF.zFiller5;
						if (lngExportVersion == 0)
						{
						}
						FCFileSystem.PrintLine(1, strLine);
						lngMeterCounter += 1;
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
				}
				FCFileSystem.FileClose(1);
				//FC:FINAL:AM:#1306 - download the file
				//MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter) + "\r\n" + "File " + Application.MapPath("\\") + "\\TSUTXX41.ASC", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
				FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TSUTXX41.ASC"));
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Badger Extract", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void CreateTSCONSUMExtractFile(ref int[] BookArray)
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsInfo = new clsDRWrapper();
				string strOrderBy = "";
				string strSql = "";
				string strBooks = "";
				int counter;
				string strLine = "";
				string strLine2 = "";
				int lngMeterCounter;
				FCFileSystem.FileClose(1);
				FCFileSystem.FileOpen(1, "TSCONSUM.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				FCFileSystem.FileClose(2);
				FCFileSystem.FileOpen(2, "TSCONSUM.DOS", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				lngMeterCounter = 0;
				// if we are only reporting on selected books then build the sql string to find the records we want
				if (Information.UBound(BookArray, 1) == 1)
				{
					strBooks = "Book " + FCConvert.ToString(BookArray[1]);
					strSql = " = " + FCConvert.ToString(BookArray[1]);
				}
				else
				{
					strBooks = "Books ";
					for (counter = 1; counter <= Information.UBound(BookArray, 1); counter++)
					{
						strSql += FCConvert.ToString(BookArray[counter]) + ",";
						strBooks += FCConvert.ToString(BookArray[counter]) + ",";
					}
					strSql = "IN (" + Strings.Left(strSql, strSql.Length - 1) + ")";
					strBooks = Strings.Left(strBooks, strBooks.Length - 1);
				}
				if (frmCreateExtractFile.InstancePtr.cmbName.Text == "Name")
				{
					strOrderBy = "Name";
				}
				else if (frmCreateExtractFile.InstancePtr.cmbName.Text == "Location")
				{
					strOrderBy = "StreetName, StreetNumber";
					// XXXXX kgk val(StreetNumber)
				}
				else
				{
					strOrderBy = "Sequence";
				}
				if (frmCreateExtractFile.InstancePtr.cmbAllAccounts.Text == "Water")
				{
					rsInfo.OpenRecordset("SELECT BookNumber, Sequence, CurrentReading, PreviousReading, BilledConsumption, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE ISNULL(IncludeInExtract,0) = 1 AND (Service = 'W' OR Service = 'B') AND BookNumber " + strSql + " ORDER BY BookNumber, " + strOrderBy);
				}
				else if (frmCreateExtractFile.InstancePtr.cmbAllAccounts.Text == "Sewer")
				{
					rsInfo.OpenRecordset("SELECT BookNumber, Sequence, CurrentReading, PreviousReading, BilledConsumption, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE ISNULL(IncludeInExtract,0) = 1 AND (Service = 'S' OR Service = 'B') AND BookNumber " + strSql + " ORDER BY BookNumber, " + strOrderBy);
				}
				else if (frmCreateExtractFile.InstancePtr.cmbAllAccounts.Text == "Both")
				{
					rsInfo.OpenRecordset("SELECT BookNumber, Sequence, CurrentReading, PreviousReading, BilledConsumption, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE ISNULL(IncludeInExtract,0) = 1 AND Service = 'B' AND BookNumber " + strSql + " ORDER BY BookNumber, " + strOrderBy);
				}
				else
				{
					rsInfo.OpenRecordset("SELECT BookNumber, Sequence, CurrentReading, PreviousReading, BilledConsumption, AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID WHERE ISNULL(IncludeInExtract,0) = 1 AND BookNumber " + strSql + " ORDER BY BookNumber, " + strOrderBy);
				}

				if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
				{
					do
					{
						strLine = "";
						strLine2 = "";
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						strLine += Strings.Format(rsInfo.Get_Fields("AccountNumber"), "0000000000");
						// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
						strLine2 += Strings.Format(rsInfo.Get_Fields("AccountNumber"), "0000000000");
						if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))).Length >= 25)
						{
							strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))), 25);
						}
						else
						{
							strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))) + Strings.StrDup(25 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))).Length, " ");
						}
						if (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))).Length >= 26)
						{
							strLine2 += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))), 26);
						}
						else
						{
							strLine2 += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))) + Strings.StrDup(26 - Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Name"))).Length, " ");
						}
						strLine += " ";
						strLine += Strings.Format(rsInfo.Get_Fields_Int32("BookNumber"), "0000");
						strLine += " ";
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						strLine += Strings.Format(rsInfo.Get_Fields("Sequence"), "0000");
						strLine += " ";
						strLine2 += " ";
						strLine2 += Strings.Format(rsInfo.Get_Fields_Int32("BookNumber"), "0000");
						strLine2 += " ";
						// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
						strLine2 += Strings.Format(rsInfo.Get_Fields("Sequence"), "0000");
						strLine2 += " ";
						if (Conversion.Val(rsInfo.Get_Fields_Int32("BilledConsumption")) != 0)
						{
							strLine += Strings.Format(Conversion.Val(rsInfo.Get_Fields_Int32("BilledConsumption")), "000000000000");
							strLine2 += Strings.Format(Conversion.Val(rsInfo.Get_Fields_Int32("BilledConsumption")), "000000000000");
						}
						else
						{
                            if (Conversion.Val(rsInfo.Get_Fields_Int32("CurrentReading")) > 0)
                            {

                                if (Conversion.Val(rsInfo.Get_Fields_Int32("CurrentReading")) >=
                                    Conversion.Val(rsInfo.Get_Fields_Int32("PreviousReading")))
                                {
                                    strLine += Strings.Format(
                                        Conversion.Val(rsInfo.Get_Fields_Int32("CurrentReading")) -
                                        Conversion.Val(rsInfo.Get_Fields_Int32("PreviousReading")), "000000000000");
                                    strLine2 += Strings.Format(
                                        Conversion.Val(rsInfo.Get_Fields_Int32("CurrentReading")) -
                                        Conversion.Val(rsInfo.Get_Fields_Int32("PreviousReading")), "000000000000");
                                }
                                else
                                {
                                    strLine += Strings.Format(
                                        Conversion.Val("1" + Strings.Format(rsInfo.Get_Fields_Int32("CurrentReading"),
                                                           Strings.StrDup(
                                                               FCConvert.ToString(
                                                                   rsInfo.Get_Fields_Int32("PreviousReading")).Length,
                                                               "0"))) - Conversion.Val(
                                            rsInfo.Get_Fields_Int32("PreviousReading")), "000000000000");
                                    strLine2 += Strings.Format(
                                        Conversion.Val("1" + Strings.Format(rsInfo.Get_Fields_Int32("CurrentReading"),
                                                           Strings.StrDup(
                                                               FCConvert.ToString(
                                                                   rsInfo.Get_Fields_Int32("PreviousReading")).Length,
                                                               "0"))) - Conversion.Val(
                                            rsInfo.Get_Fields_Int32("PreviousReading")), "000000000000");
                                }
                            }
                            else
                            {
                                strLine += "000000000000";
                                strLine2 += "000000000000";
                            }
                        }
						// strLine2 = strLine
						strLine += " ";
						strLine += Strings.Format(rsInfo.Get_Fields_Int32("PreviousReading"), "000000000000");
						strLine += " ";
						strLine += Strings.Format(rsInfo.Get_Fields_Int32("CurrentReading"), "000000000000");
						strLine += " ";
						if (Strings.Trim(rsInfo.Get_Fields_String("StreetName") + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt")))).Length >= 40)
						{
							strLine += Strings.Left(Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))), 40 - (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt"))).Length + 1)) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt")));
						}
						else
						{
							strLine += Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt"))) + Strings.StrDup(40 - (Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("StreetName"))) + " " + Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields_String("Apt")))).Length, " ");
						}
						FCFileSystem.PrintLine(1, strLine);
						FCFileSystem.PrintLine(2, strLine2);
						lngMeterCounter += 1;
						rsInfo.MoveNext();
					}
					while (rsInfo.EndOfFile() != true);
                    FCFileSystem.FileClose(1);
                    FCFileSystem.FileClose(2);
					//FC:FINAL:AM:#1306 - download the file
					//MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter) + "\r\n" + "File " + Application.MapPath("\\") + "\\TSCONSUM.DAT", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
					MessageBox.Show(strBooks + "\r\n" + "Records " + FCConvert.ToString(lngMeterCounter), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
					FCUtils.Download(Path.Combine(FCFileSystem.Statics.UserDataFolder, "TSCONSUM.DAT"));
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Creating Extract", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ConvertNorthernDataXRefExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter;
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				int lngBook = 0;
				int lngSeq = 0;
				int lngMKey = 0;
				int lngPrev = 0;
				int lngCur = 0;
				int lngAcct = 0;
				string[] strArray = null;
				StreamReader stream;
				MessageBox.Show("Make sure that all books being affected are at the status of Cleared.  This will affect the readings.", "Clear Books", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.dat";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "CSV")
						{
							ans = MessageBox.Show("You must select a .csv file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				// ChDir strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				FCFileSystem.FileClose(2);
				if (System.IO.File.Exists(strFileName))
				{
					// open the file
					stream = System.IO.File.OpenText(strFileName/*, IOMode.ForReading, false, Tristate.TristateUseDefault*/);
					FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					datDefaultReading = DateTime.Today;
					object datTempDate = datDefaultReading;
					if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
					{
						while (!stream.EndOfStream)
						{
							strRecord = stream.ReadLine();
							strRecord = FixCommas(ref strRecord);
							strArray = Strings.Split(strRecord, ",", -1, CompareConstants.vbBinaryCompare);
							lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(strArray[9])));
							if (lngAcct != 0)
							{
								// If lngAcct = 3175 Then
								// MsgBox "Stop"
								// End If
								// 
								lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(strArray[6])));
								lngSeq = FCConvert.ToInt32(Math.Round(Conversion.Val(strArray[7])));
								lngCur = FCConvert.ToInt32(Math.Round(Conversion.Val(strArray[5])));
								// Find the account number, book and seq by using the cross reference field
								if (GetMeterInformation_2(lngAcct.ToString(), ref lngBook, ref lngSeq, ref lngMKey, ref lngPrev))
								{
									// If lngMKey = 245 Then Stop
									// Meter Key
									strLine = Strings.Format(lngMKey, "000000");
									// Book
									strLine += Strings.Format(lngBook, "0000");
									// Sequence
									strLine += Strings.Left(Strings.Format(lngSeq, "0000"), 4);
									// Current Reading
									strLine += Strings.Format(lngCur, "000000");
									// Reading Date
									strLine += Strings.Format(DateAndTime.DateValue(strArray[3]), "MMddyy");
									FCFileSystem.PrintLine(2, strLine);
									//Application.DoEvents();
								}
								else
								{
									// add an error line in the error array becuase the meter was not found
									// ReDim Preserve RVSMeterImportError(lngErrorIndex)
									//Application.DoEvents();
									// doMeterImportError(lngErrorIndex) = RVSIF
									// lngErrorIndex = lngErrorIndex + 1
								}
							}
						}
						FCFileSystem.FileClose(1);
						FCFileSystem.FileClose(2);
                        //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From NDS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                    }
				}
				else
				{
					MessageBox.Show("Could not open " + strFileName + ".", "No Meter Affected", MessageBoxButtons.OK, MessageBoxIcon.Warning);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting TRIO DOS File (XRef)", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}
		// vbPorter upgrade warning: strAAAccountNumber As string	OnWrite(FixedString, string)
		private static bool GetMeterInformation_2(string strAAAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, ref int lngPrev/* = 0 */, bool boolUseBookSeq = false)
		{
			return GetMeterInformation(strAAAccountNumber, ref lngBook, ref lngSeq, ref lngMK, ref lngPrev, boolUseBookSeq);
		}

		private static bool GetMeterInformation_488(string strAAAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, ref int lngPrev/* = 0 */, bool boolUseBookSeq = false)
		{
			return GetMeterInformation(strAAAccountNumber, ref lngBook, ref lngSeq, ref lngMK, ref lngPrev, boolUseBookSeq);
		}

		private static bool GetMeterInformation_567(string strAAAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, bool boolUseBookSeq = false)
		{
			int lngPrev = 0;
			return GetMeterInformation(strAAAccountNumber, ref lngBook, ref lngSeq, ref lngMK, ref lngPrev, boolUseBookSeq);
		}

		private static bool GetMeterInformation_569(string strAAAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, bool boolUseBookSeq = false)
		{
			int lngPrev = 0;
			return GetMeterInformation(strAAAccountNumber, ref lngBook, ref lngSeq, ref lngMK, ref lngPrev, boolUseBookSeq);
		}

		private static bool GetMeterInformation(string strAAAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, ref int lngPrev/*= 0*/, bool boolUseBookSeq = false)
		{
			bool GetMeterInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				int lngAcctNum = 0;
				if (boolUseBookSeq)
				{
					rsM.OpenRecordset("SELECT * FROM MeterTable WHERE BookNumber = " + FCConvert.ToString(lngBook) + " AND Sequence = " + FCConvert.ToString(lngSeq), modExtraModules.strUTDatabase);
				}
				else
				{
					if (Strings.UCase(modGlobalConstants.Statics.MuniName) == "BUCKSPORT")
					{
						rsM.OpenRecordset("SELECT * FROM MeterTable WHERE left(XRef1, " + FCConvert.ToString(strAAAccountNumber.Length - 2) + ") = '" + Strings.Left(strAAAccountNumber, strAAAccountNumber.Length - 2) + "'", modExtraModules.strUTDatabase);
						// kk 06242013 trout-931   Add Veazie Import from Water Dept; Meters resequenced in new Water system     'kk09022015 add Orono
						// kk 10212013 trouts-42   Add Gardiner Import from Water district - match by Account Number, no combined meters
					}
					else if (Strings.UCase(Strings.Left(modGlobalConstants.Statics.MuniName, 6)) == "VEAZIE" || Strings.UCase(modGlobalConstants.Statics.MuniName) == "GARDINER")
					{
						lngAcctNum = FCConvert.ToInt32(Math.Round(Conversion.Val(strAAAccountNumber)));
						rsM.OpenRecordset("SELECT * FROM MeterTable WHERE MeterNumber = 1 AND AccountKey = " + FCConvert.ToString(modUTStatusPayments.GetAccountKeyUT(ref lngAcctNum)), modExtraModules.strUTDatabase);
					}
					else
					{
						rsM.OpenRecordset("SELECT * FROM MeterTable WHERE XRef1 = '" + strAAAccountNumber + "'", modExtraModules.strUTDatabase);
					}
				}
				if (!rsM.EndOfFile())
				{
					GetMeterInformation = true;
					lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(rsM.Get_Fields_Int32("BookNumber"))));
					// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
					lngSeq = FCConvert.ToInt32(Math.Round(Conversion.Val(rsM.Get_Fields("Sequence"))));
					lngMK = FCConvert.ToInt32(rsM.Get_Fields_Int32("ID"));
					// GetAccountNumber(Val(rsM.Fields("AccountKey")))
					lngPrev = FCConvert.ToInt32(rsM.Get_Fields_Int32("PreviousReading"));
				}
				else
				{
					lngBook = 0;
					lngSeq = 0;
					lngMK = 0;
					lngPrev = 0;
				}
				return GetMeterInformation;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #", FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MessageBoxButtons.OK, MessageBoxIcon.Information/*? , "Error Getting Meter Information" */);
			}
			return GetMeterInformation;
		}

		private static bool GetBelfastMeterInformation_2(string strAAAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, ref int lngPrev/*= 0*/)
		{
			return GetBelfastMeterInformation(ref strAAAccountNumber, ref lngBook, ref lngSeq, ref lngMK, ref lngPrev);
		}

		private static bool GetBelfastMeterInformation(ref string strAAAccountNumber, ref int lngBook, ref int lngSeq, ref int lngMK, ref int lngPrev/*= 0*/)
		{
			bool GetBelfastMeterInformation = false;
			try
			{
				// On Error GoTo ERROR_HANDLER
				clsDRWrapper rsM = new clsDRWrapper();
				rsM.OpenRecordset("SELECT * FROM MeterTable WHERE SerialNumber = '" + strAAAccountNumber + "'", modExtraModules.strUTDatabase);
				if (!rsM.EndOfFile())
				{
					GetBelfastMeterInformation = true;
					lngBook = FCConvert.ToInt32(Math.Round(Conversion.Val(rsM.Get_Fields_Int32("BookNumber"))));
					// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
					lngSeq = FCConvert.ToInt32(Math.Round(Conversion.Val(rsM.Get_Fields("Sequence"))));
					lngMK = FCConvert.ToInt32(rsM.Get_Fields_Int32("ID"));
					lngPrev = FCConvert.ToInt32(rsM.Get_Fields_Int32("PreviousReading"));
				}
				else
				{
					lngBook = 0;
					lngSeq = 0;
					lngMK = 0;
					lngPrev = 0;
				}
				return GetBelfastMeterInformation;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #", FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MessageBoxButtons.OK, MessageBoxIcon.Information/*? , "Error Getting Meter Information" */);
			}
			return GetBelfastMeterInformation;
		}

		private static string FixCommas(ref string strFix)
		{
			string FixCommas = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				bool boolRemoveComma = false;
				int lngCT;
				string strChar = "";
				for (lngCT = 1; lngCT <= strFix.Length; lngCT++)
				{
					strChar = Strings.Mid(strFix, lngCT, 1);
					if (strChar == ",")
					{
						if (!boolRemoveComma)
						{
							// this should effectively remove any commas that are inside of quotes
							FixCommas = FixCommas + strChar;
						}
					}
					else if (strChar == "\"")
					{
						boolRemoveComma = !boolRemoveComma;
						FixCommas = FixCommas + strChar;
					}
					else
					{
						// continue
						FixCommas = FixCommas + strChar;
					}
				}
				return FixCommas;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #", FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", MessageBoxButtons.OK, MessageBoxIcon.Information/*? , "Error Fixing Commas" */);
			}
			return FixCommas;
		}

		public static void ConvertBelfastExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				// vbPorter upgrade warning: counter As short --> As int	OnWrite(short, double)
				int counter;
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				int lngBook = 0;
				int lngSeq = 0;
				int lngMKey = 0;
				int lngPrev = 0;
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.txt";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						if (Strings.UCase(Strings.Right(strFileName, 3)) != "TXT")
						{
							ans = MessageBox.Show("You must select a .txt file before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				// ChDir strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				FCFileSystem.FileClose(1);
				FCFileSystem.FileClose(2);
				FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.BELCR));
				FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				datDefaultReading = DateTime.Today;
				object datTempDate = datDefaultReading;
				if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					//FC:FINAL:DDU:converted back to correct variable
					datDefaultReading = FCConvert.ToDateTime(datTempDate);
					frmWait.InstancePtr.Init("Please wait..." + "\r\n" + "Converting Consumption File", true, FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.BELCR)), true);
					for (counter = 1; counter <= FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.BELCR)); counter++)
					{
						//Application.DoEvents();
						FCFileSystem.FileGet(1, ref Statics.BELCR, counter);
						if (Strings.Trim(Statics.BELCR.WaterAcctNumber) != "" && Strings.Trim(Statics.BELCR.SewerAcctNumber) != "" && Conversion.Val(Strings.Trim(Statics.BELCR.SewerAcctNumber)) != 0)
						{
							if (GetBelfastMeterInformation_2(Strings.Trim(Statics.BELCR.WaterAcctNumber), ref lngBook, ref lngSeq, ref lngMKey, ref lngPrev))
							{
								// If lngSeq Then Stop
								// Meter Key
								strLine = Strings.Format(lngMKey, "000000");
								// Book
								strLine += Strings.Format(lngBook, "0000");
								// Sequence
								strLine += Strings.Format(lngSeq, "0000");
								// Current Reading
								strLine += Strings.Format(Conversion.Val(Strings.Trim(Statics.BELCR.Consumption)), "000000");
								// Reading Date
								strLine += Strings.Format(datDefaultReading, "MMddyy");
								if (Conversion.Val(Strings.Trim(Statics.BELCR.Consumption)) != 0)
								{
									FCFileSystem.PrintLine(2, strLine);
								}
								//Application.DoEvents();
							}
							else
							{
								// add an error line in the error array becuase the meter was not found
								// ReDim Preserve RVSMeterImportError(lngErrorIndex)
								//Application.DoEvents();
								// doMeterImportError(lngErrorIndex) = RVSIF
								// lngErrorIndex = lngErrorIndex + 1
							}
						}
						frmWait.InstancePtr.IncrementProgress();
					}
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					frmWait.InstancePtr.Unload();
                    //Application.DoEvents();
                    //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From TRIO DOS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                }
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting TRIO DOS File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void ConvertOaklandExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				// vbPorter upgrade warning: counter As short --> As int	OnWrite(int, double)
				int counter;
				clsDRWrapper rsMeterInfo = new clsDRWrapper();
				int lngBook = 0;
				int lngSeq = 0;
				int lngMKey = 0;
				int lngPrev = 0;
				bool blnMultipleDisks;
				int lngCurIndex = 0;
				Statics.lngNoMatchCounter = 0;
				blnMultipleDisks = false;
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					// MDIParent.CommonDialog1.Filter = "sewrdata"
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						// If InStr(1, UCase(strFileName), "SEWRDATA") = 0 Then
						// Ans = MsgBox("You must select a sewrdata file before you may continue.  Do you wish to try again?", vbQuestion + vbYesNo, "Select File?")
						// If Ans = vbYes Then
						// GoTo PickAgain
						// Else
						// Exit Sub
						// End If
						// Else
						if (blnMultipleDisks)
						{
							//TODO
							//goto ProcessMultipleDisks;
						}
						// End If
					}
					else
					{
						if (!blnMultipleDisks)
						{
							return;
						}
						else
						{
							//TODO
							//goto FinishedProcessing;
						}
					}
					/*? On Error GoTo 0 */
				}
				catch
				{
				}
				// ChDrive strCurDir
				// ChDir strCurDir
				modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
				datDefaultReading = DateTime.Today;
				object datTempDate = datDefaultReading;
				if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
				{
					//FC:FINAL:DDU:converted back to correct variable
					DateTime temp = FCConvert.ToDateTime(datTempDate);
					FillOaklandReportInfo(ref temp);
					datDefaultReading = temp;
					FCFileSystem.FileClose(2);
					FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
					ProcessMultipleDisks:
					;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileOpen(1, strFileName, OpenMode.Random, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.OAKCR));
					frmWait.InstancePtr.Init("Please wait..." + "\r\n" + "Converting Consumption File", true, FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.OAKCR)), true);
					for (counter = 1; counter <= FCConvert.ToInt32(FCFileSystem.LOF(1) / Marshal.SizeOf(Statics.OAKCR)); counter++)
					{
						//Application.DoEvents();
						FCFileSystem.FileGet(1, ref Statics.OAKCR, counter);
						if (Strings.Trim(Statics.OAKCR.SerialNumber) != "")
						{
							if (GetBelfastMeterInformation_2(Strings.Trim(Statics.OAKCR.SerialNumber), ref lngBook, ref lngSeq, ref lngMKey, ref lngPrev))
							{
								lngCurIndex = GetOaklandIndex_2(Strings.Trim(Statics.OAKCR.SerialNumber));
								if (lngCurIndex != -1)
								{
									if (Statics.OaklandReportInfo[lngCurIndex].Consumption == -1)
									{
										Statics.OaklandReportInfo[lngCurIndex].Consumption = FCConvert.ToInt32(Conversion.Val(Strings.Trim(Statics.OAKCR.Consumption)) / 100);
									}
									else
									{
										Statics.OaklandReportInfo[lngCurIndex].Consumption += FCConvert.ToInt32(Conversion.Val(Strings.Trim(Statics.OAKCR.Consumption)) / 100);
									}
									Statics.OaklandReportInfo[lngCurIndex].CurrentReading = Statics.OaklandReportInfo[lngCurIndex].PreviousReading + Statics.OaklandReportInfo[lngCurIndex].Consumption;
								}
								//Application.DoEvents();
							}
							else
							{
								Array.Resize(ref Statics.NoAccountFound, Statics.lngNoMatchCounter + 1);
								Statics.NoAccountFound[Statics.lngNoMatchCounter].Consumption = Statics.OAKCR.Consumption;
								Statics.NoAccountFound[Statics.lngNoMatchCounter].OwnerAddress = Statics.OAKCR.OwnerAddress;
								Statics.NoAccountFound[Statics.lngNoMatchCounter].OwnerName = Statics.OAKCR.OwnerName;
								Statics.NoAccountFound[Statics.lngNoMatchCounter].ReadingDate = Statics.OAKCR.ReadingDate;
								Statics.NoAccountFound[Statics.lngNoMatchCounter].SerialNumber = Statics.OAKCR.SerialNumber;
								Statics.lngNoMatchCounter += 1;
								//Application.DoEvents();
							}
						}
						frmWait.InstancePtr.IncrementProgress();
					}
					frmWait.InstancePtr.Unload();
					//Application.DoEvents();
					FCFileSystem.FileClose(1);
					ans = MessageBox.Show("Finished processing current disk.  Do you have another disk you wish to process?", "Process Another?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
					if (ans == DialogResult.Yes)
					{
						blnMultipleDisks = true;
						goto PickAgain;
					}
					FinishedProcessing:
					;
					for (counter = 0; counter <= Information.UBound(Statics.OaklandReportInfo, 1); counter++)
					{
						//Application.DoEvents();
						// If lngSeq Then Stop
						// Meter Key
						strLine = Strings.Format(Statics.OaklandReportInfo[counter].MeterKey, "000000");
						// Book
						strLine += Strings.Format(Statics.OaklandReportInfo[counter].Book, "0000");
						// Sequence
						strLine += Strings.Format(Statics.OaklandReportInfo[counter].Sequence, "0000");
						// Current Reading
						strLine += Strings.Format(Statics.OaklandReportInfo[counter].CurrentReading, "000000");
						// Reading Date
						strLine += Strings.Format(datDefaultReading, "MMddyy");
						if (Statics.OaklandReportInfo[counter].Consumption > 0)
						{
							FCFileSystem.PrintLine(2, strLine);
						}
					}
					FCFileSystem.FileClose(2);
					frmWait.InstancePtr.Unload();
                    //Application.DoEvents();
                    //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From TRIO DOS", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                    frmReportViewer.InstancePtr.Init(rptOaklandConsumptionReport.InstancePtr);
				}
				return;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting TRIO DOS File", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		public static void FillOaklandReportInfo(ref DateTime datReading)
		{
			clsDRWrapper rsAcctInfo = new clsDRWrapper();
			clsDRWrapper rsMeterInfo = new clsDRWrapper();
			int lngCounter;
			lngCounter = 0;
			rsAcctInfo.OpenRecordset("SELECT Master.ID, AccountNumber, p.FullNameLF AS Name FROM Master INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID ORDER BY AccountNumber", modExtraModules.strUTDatabase);
			if (rsAcctInfo.EndOfFile() != true && rsAcctInfo.BeginningOfFile() != true)
			{
				frmWait.InstancePtr.Init("Please wait..." + "\r\n" + "Loading Account Information", true, rsAcctInfo.RecordCount(), true);
				do
				{
					//Application.DoEvents();
					rsMeterInfo.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + rsAcctInfo.Get_Fields_Int32("ID") + " ORDER BY MeterNumber", modExtraModules.strUTDatabase);
					if (rsMeterInfo.EndOfFile() != true && rsMeterInfo.BeginningOfFile() != true)
					{
						do
						{
							//Application.DoEvents();
							Array.Resize(ref Statics.OaklandReportInfo, lngCounter + 1);
							Statics.OaklandReportInfo[lngCounter].AccountKey = FCConvert.ToInt32(rsMeterInfo.Get_Fields_Int32("AccountKey"));
							// TODO Get_Fields: Check the table for the column [AccountNumber] and replace with corresponding Get_Field method
							Statics.OaklandReportInfo[lngCounter].AccountNumber = FCConvert.ToInt32(rsAcctInfo.Get_Fields("AccountNumber"));
							Statics.OaklandReportInfo[lngCounter].Book = FCConvert.ToInt32(rsMeterInfo.Get_Fields_Int32("BookNumber"));
							Statics.OaklandReportInfo[lngCounter].Consumption = -1;
							Statics.OaklandReportInfo[lngCounter].CurrentReading = -1;
							Statics.OaklandReportInfo[lngCounter].MeterKey = FCConvert.ToInt32(rsMeterInfo.Get_Fields_Int32("ID"));
							Statics.OaklandReportInfo[lngCounter].OwnerName = FCConvert.ToString(rsAcctInfo.Get_Fields_String("Name"));
							Statics.OaklandReportInfo[lngCounter].PreviousReading = FCConvert.ToInt32(rsMeterInfo.Get_Fields_Int32("PreviousReading"));
							// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
							Statics.OaklandReportInfo[lngCounter].Sequence = FCConvert.ToInt32(rsMeterInfo.Get_Fields("Sequence"));
							Statics.OaklandReportInfo[lngCounter].SerialNumber = FCConvert.ToString(rsMeterInfo.Get_Fields_String("SerialNumber"));
							Statics.OaklandReportInfo[lngCounter].ReadingDate = datReading;
							lngCounter += 1;
							rsMeterInfo.MoveNext();
						}
						while (rsMeterInfo.EndOfFile() != true);
					}
					rsAcctInfo.MoveNext();
					frmWait.InstancePtr.IncrementProgress();
				}
				while (rsAcctInfo.EndOfFile() != true);
			}
			frmWait.InstancePtr.Unload();
		}

		public static int GetOaklandIndex_2(string strSerialNumber)
		{
			return GetOaklandIndex(ref strSerialNumber);
		}

		public static int GetOaklandIndex(ref string strSerialNumber)
		{
			int GetOaklandIndex = 0;
			int counter;
			GetOaklandIndex = -1;
			for (counter = 0; counter <= Information.UBound(Statics.OaklandReportInfo, 1); counter++)
			{
				//Application.DoEvents();
				if (Statics.OaklandReportInfo[counter].SerialNumber == strSerialNumber)
				{
					GetOaklandIndex = counter;
					return GetOaklandIndex;
				}
			}
			return GetOaklandIndex;
		}

		private static void CheckForEZDataDirectory()
		{
			//FC:FINAL:AM:#1306 - don't use C drive
			//if (!Directory.Exists("C:\\EZReader"))
			//{
			//    Directory.CreateDirectory("C:\\EZReader");
			//}
		}

		public static void ConvertEZReaderExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				string strCurDir;
				string strFileName = "";
				// vbPorter upgrade warning: ans As short, int --> As DialogResult
				DialogResult ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsCrossRef = new clsDRWrapper();
				// vbPorter upgrade warning: lngBook As int	OnWrite(string)	OnRead(string)
				int lngBook = 0;
				// vbPorter upgrade warning: lngSeq As int	OnWrite(string)
				int lngSeq = 0;
				// vbPorter upgrade warning: lngAcct As int	OnWrite(string)
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				string[] strInput = null;
				string strLeader = "";
				string strMonth = "";
				string strDay = "";
				string strYear = "";
				int intCompare = 0;
				lngError = 1;
				CheckForEZDataDirectory();
				// Have the user select the file
				strCurDir = "C:\\EZReader\\";
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.*";
					MDIParent.InstancePtr.CommonDialog1.InitDir = strCurDir;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						intCompare = Strings.StrComp(Strings.Right(strFileName, 10), "Upload.dat", CompareConstants.vbTextCompare);
						if (intCompare != 0)
						{
							ans = MessageBox.Show("You must select a file named 'Upload.dat' before you may continue.  Do you wish to try again?", "Select File?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
							if (ans == DialogResult.Yes)
							{
								goto PickAgain;
							}
							else
							{
								return;
							}
						}
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					if (strFileName != "")
					{
						// open the input file from AA
						FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), Marshal.SizeOf(Statics.RVSIF));
						// open the output file
						FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						lngError = 5;
						// clear the error array
						FCUtils.EraseSafe(Statics.EZImportError);
						lngError = 10;
						// default reading date
						datDefaultReading = DateTime.Today;
						object datTempDate = datDefaultReading;
						if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
						{
							lngError = 11;
							counter = 1;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true);
							while (!FCFileSystem.EOF(1))
							{
								//Application.DoEvents();
								// Get #1, counter, RVSIF
								strRecord = FCFileSystem.LineInput(1);
								frmWait.InstancePtr.IncrementProgress();
								Statics.EZCR.Account = Strings.Format(Strings.Trim(Strings.Mid(strRecord, 217, 12)), "0000");
								Statics.EZCR.Current = Strings.Mid(strRecord, 229, 10);
								Statics.EZCR.Location = Strings.Trim(Strings.Mid(strRecord, 47, 21));
								Statics.EZCR.MeterSerial = Strings.Trim(Strings.Mid(strRecord, 68, 20));
								Statics.EZCR.Name = Strings.Trim(Strings.Mid(strRecord, 22, 25));
								strMonth = Strings.Mid(strRecord, 249, 2);
								strDay = Strings.Mid(strRecord, 251, 2);
								strYear = Strings.Mid(strRecord, 253, 2);
								Statics.EZCR.ReadingDate = FCConvert.ToString(DateAndTime.DateValue(strMonth + "/" + strDay + "/" + strYear));
								lngBook = FCConvert.ToInt32(Strings.Mid(strRecord, 1, 2));
								Statics.EZCR.Route = FCConvert.ToString(lngBook);
								lngSeq = FCConvert.ToInt32(Strings.Mid(strRecord, 3, 10));
								lngAcct = FCConvert.ToInt32(Statics.EZCR.Account);
								// MAL@20080423: Add check for reading greater than 6 digits (temporary fix until rest of app can be updated to accomodate up to 8)
								// Tracker Reference:
								if (Strings.Format(Statics.EZCR.Current, "000000").Length > 6)
								{
									// add an error line in the error array becuase the meter was not found
									Array.Resize(ref Statics.EZImportError, lngErrorIndex + 1);
									//Application.DoEvents();
									Statics.EZImportError[lngErrorIndex] = Statics.EZCR;
									lngErrorIndex += 1;
								}
								else
								{
									if (Convert.ToByte(Strings.Left(Statics.EZCR.Account, 1)[0]) != 0)
									{
										// Find the account number, book and seq by using the cross reference field
										if (GetMeterInformation_569(modMain.PadToString_6(ref lngAcct, 4), ref lngBook, ref lngSeq, ref lngAcct, true))
										{
											// Account Number
											strLine = Strings.Format(lngAcct, "000000");
											// Book
											strLine += Strings.Format(lngBook, "0000");
											// Sequence
											strLine += Strings.Format(lngSeq, "0000");
											// Current Reading
											strLine += Strings.Format(Statics.EZCR.Current, "000000");
											// Reading Date
											strLine += Strings.Format(Statics.EZCR.ReadingDate, "MMddyy");
											FCFileSystem.PrintLine(2, strLine);
											//Application.DoEvents();
										}
										else
										{
											// add an error line in the error array becuase the meter was not found
											Array.Resize(ref Statics.EZImportError, lngErrorIndex + 1);
											//Application.DoEvents();
											Statics.EZImportError[lngErrorIndex] = Statics.EZCR;
											lngErrorIndex += 1;
										}
									}
								}
								counter += 1;
							}
							lngError = 20;
							FCFileSystem.FileClose(1);
							lngError = 30;
							FCFileSystem.FileClose(2);
							lngError = 31;
							frmWait.InstancePtr.Unload();
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From EZ Reader Extract", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                            lngError = 32;
							if (lngErrorIndex > 0)
							{
								lngError = 33;
								//Application.DoEvents();
								// show a report of the errors
								rptMeterImportErrorReport.InstancePtr.Init(3);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting RVS Extract - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertDoverFoxcroftExtractFile()
		{
			try
			{
				// On Error GoTo ERROR_HANDLER
				// Read the CSV created by the water district and
				// convert it into TSUTXX41.DAT in order to use the Electronic Data Entry
				// CSV File with or without header with the following fields
				// 00    Account             As String
				// 01    Tenant1             As String
				// 02    Tenant2             As String
				// 03    Mail1               As String
				// 04    Mail2               As String
				// 05    City                As String
				// 06    Location            As String
				// 07    StreetName          As String
				// 08    MapLot              As String
				// 09    ReadDate            As Date
				// 10    CurReading          As Long
				// 11    PrevReading         As Long
				// 12    RouteNum            As String
				// 13    Type                As String
				// 14    Usage               As Long
				string strCurDir;
				string strFileName = "";
				int ans;
				string strDestinationPath = "";
				DateTime datDefaultReading;
				string strRecord = "";
				string strLine = "";
				int counter = 0;
				clsDRWrapper rsMaster = new clsDRWrapper();
				clsDRWrapper rsMeter = new clsDRWrapper();
				int lngBook;
				int lngSeq;
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				string[] strArray = null;
				string strLeader = "";
				int lngCons;
				bool boolError = false;
				DateTime dtReadDate;
				string strAccount = "";
				string strName1 = "";
				string strName2 = "";
				string strMail1 = "";
				string strMail2 = "";
				string strCity = "";
				string strLoc = "";
				string strStreet = "";
				string strMapLot = "";
				string strReadDate = "";
				string strCurRead = "";
				string strPrvRead = "";
				string strRoute = "";
				string strType = "";
				string strUsage = "";
				string strTotUsage = "";
				string strNoBill = "";
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.FileName = "*.csv";
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.csv";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
						// If UCase(Right(strFileName, 3)) <> "DAT" Then
						// Ans = MsgBox("You must select a .dat file before you may continue.  Do you wish to try again?", vbQuestion + vbYesNo, "Select File?")
						// If Ans = vbYes Then
						// GoTo PickAgain
						// Else
						// Exit Sub
						// End If
						// End If
					}
					else
					{
						return;
					}
					lngError = 2;
					// ChDrive strCurDir
					// ChDir strCurDir
					modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					if (strFileName != "")
					{
						// open the input file from AA
						FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
						// open the output file
						FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						lngError = 5;
						// XX Hold onto this for now
						// XX        'clear the error array
						// XX        Erase DFFileImportError
						lngError = 10;
						// default reading date
						datDefaultReading = DateTime.Today;
						object datTempDate = datDefaultReading;
						if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
						{
							//FC:FINAL:DDU:converted back to correct variable
							datDefaultReading = FCConvert.ToDateTime(datTempDate);
							lngError = 11;
							counter = 1;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true);
							GetNext:
							;
							while (!FCFileSystem.EOF(1))
							{
								//Application.DoEvents();
								// Input #1, strAccount, strName1, strName2, strMail1, strMail2, strCity, strLoc, strStreet, strMapLot, strReadDate, strCurRead, strPrvRead, strRoute, strType, strUsage, strTotUsage, strNoBill
								strRecord = FCFileSystem.LineInput(1);
								strRecord = FixCommas(ref strRecord);
								strArray = Strings.Split(strRecord, ",", -1, CompareConstants.vbBinaryCompare);
								frmWait.InstancePtr.IncrementProgress();
								if (Strings.UCase(Strings.Left(strArray[1], 3)) == "COM")
								{
									// kgk 12-08-2011  "ACC" Then
									goto GetNext;
								}
								if (!Information.IsNumeric(strArray[1]))
								{
									goto GetNext;
								}
								lngAcct = FCConvert.ToInt32(Math.Round(Conversion.Val(strArray[1])));
								boolError = true;
								if (Information.IsDate(strArray[16]))
								{
									// kgk 12-08-2011  was 16
									dtReadDate = DateAndTime.DateValue(strArray[16]);
								}
								else
								{
									dtReadDate = datDefaultReading;
								}
								if (Conversion.Val(strArray[17]) - Conversion.Val(strArray[18]) == Conversion.Val(strArray[43]))
								{
									// kgk 12-08-2011 was 10, 11 and 14
									if (lngAcct != 0)
									{
										rsMaster.OpenRecordset("SELECT ID FROM Master WHERE AccountNumber = " + FCConvert.ToString(lngAcct), "twut0000.vb1");
										if (!rsMaster.EndOfFile() && !rsMaster.BeginningOfFile())
										{
											rsMeter.OpenRecordset("SELECT * FROM MeterTable WHERE AccountKey = " + FCConvert.ToString(Conversion.Val(rsMaster.Get_Fields_Int32("ID"))), "twut0000.vb1");
											if (!rsMeter.EndOfFile() && !rsMeter.BeginningOfFile())
											{
												rsMeter.MoveLast();
												// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
												if (rsMeter.RecordCount() == 1 && Conversion.Val(rsMeter.Get_Fields("Sequence")) < 10000 && Conversion.Val(strArray[17]) < 1000000)
												{
													// was 10
													boolError = false;
													// Meter Key
													// Is this supposed to be the TRIO meter key? RSV sets to account number
													// strLine = Format(lngAcct, "000000")
													strLine = Strings.Format(rsMeter.Get_Fields_Int32("ID"), "000000");
													// Book
													strLine += Strings.Format(Conversion.Val(rsMeter.Get_Fields_Int32("BookNumber")), "0000");
													// Sequence
													// Issue #1 Duplicate Sequences and Sequence Numbers > 4 digits
													// TODO Get_Fields: Check the table for the column [Sequence] and replace with corresponding Get_Field method
													strLine += Strings.Format(Conversion.Val(rsMeter.Get_Fields("Sequence")), "0000");
													// Current Reading
													// Issue #2 Readings > 999999
													strLine += Strings.Format(Conversion.Val(strArray[17]), "000000");
													// kgk 12-08-2011 was 17
													// Reading Date
													strLine += Strings.Format(dtReadDate, "MMddyy");
													FCFileSystem.PrintLine(2, strLine);
												}
											}
										}
									}
								}
								if (boolError)
								{
									// add an error line in the error array becuase the account or meter was not found
									Array.Resize(ref Statics.EZImportError, lngErrorIndex + 1);
									Statics.EZImportError[lngErrorIndex].Account = strArray[1];
									Statics.EZImportError[lngErrorIndex].Current = strArray[17];
									// kgk 12-08-2011 was 10
									Statics.EZImportError[lngErrorIndex].Location = strArray[9];
									// was 6
									Statics.EZImportError[lngErrorIndex].Name = strArray[4];
									// was 1
									Statics.EZImportError[lngErrorIndex].ReadingDate = strArray[16];
									// was 9
									Statics.EZImportError[lngErrorIndex].Route = "";
									// Left(strArray(12), 3)  - Route is too long for rpt field
									lngErrorIndex += 1;
								}
								counter += 1;
								//Application.DoEvents();
							}
							lngError = 20;
							FCFileSystem.FileClose(1);
							lngError = 30;
							FCFileSystem.FileClose(2);
							lngError = 31;
							frmWait.InstancePtr.Unload();
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From RVS Extract", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                            lngError = 32;
							if (lngErrorIndex > 0)
							{
								lngError = 33;
								//Application.DoEvents();
								// show a report of the errors
								rptMeterImportErrorReport.InstancePtr.Init(3);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting RVS Extract - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

		public static void ConvertHampdenExtractFile()
		{
			// JIRA TROUT-1245
			try
			{
				// On Error GoTo ERROR_HANDLER
				// Read the CSV created by the water district and
				// convert it into TSUTXX41.DAT in order to use the Electronic Data Entry
				// CSV File with or without header with the following fields
				// 00    ServiceID           As String
				// 01    CurReading          As String
				// 02    PrevReading         As String
				// 03    Usage               As String
				// 04    BillDate            As String
				// 05    DueDate             As String
				// 06    CustName            As String
				// 07    SecondName          As String
				// 08    MailAddr            As String
				// 09    ServiceLoc          As String
				// 10    Route               As String
				string strCurDir;
				string strFileName = "";
				string strRecord = "";
				string strLine = "";
				DateTime datDefaultReading;
				int counter = 0;
				int lngBook = 0;
				int lngSeq = 0;
				int lngAcct = 0;
				int lngErrorIndex = 0;
				int lngError;
				string[] strArray = null;
				bool boolError;
				bool boolAppend;
				lngError = 1;
				// Have the user select the file
				strCurDir = FCFileSystem.Statics.UserDataFolder;
				boolAppend = false;
				PickAgain:
				;
				Information.Err().Clear();
				// MDIParent.InstancePtr.CommonDialog1_Save.Flags = 0x8	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
				//- MDIParent.InstancePtr.CommonDialog1.CancelError = true;
				/*? On Error Resume Next  */
				try
				{
					MDIParent.InstancePtr.CommonDialog1.FileName = "*.csv";
					MDIParent.InstancePtr.CommonDialog1.Filter = "*.csv";
					MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;
					MDIParent.InstancePtr.CommonDialog1.ShowOpen();
					if (Information.Err().Number == 0)
					{
						strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
					}
					else
					{
						return;
					}
					lngError = 2;
					//ChDrive(strCurDir);
					Environment.CurrentDirectory = strCurDir;
					lngError = 3;
					FCFileSystem.FileClose(1);
					FCFileSystem.FileClose(2);
					lngError = 4;
					if (strFileName != "")
					{
						// open the input file from Hampden
						FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess)(-1), (OpenShare)(-1), -1);
						// open the output file
						FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
						lngError = 5;
						// clear the error array
						if (!boolAppend)
						{
							FCUtils.EraseSafe(Statics.HampdenImportError);
						}
						// default reading date
						datDefaultReading = DateTime.Today;
						object datTempDate = datDefaultReading;
						if (frmInput.InstancePtr.Init(ref datTempDate, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1700, false, modGlobalConstants.InputDTypes.idtDate))
						{
							//FC:FINAL:DDU:converted back to correct variable
							datDefaultReading = FCConvert.ToDateTime(datTempDate);
							lngError = 11;
							counter = 1;
							frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters", true);
							GetNext:
							;
							while (!FCFileSystem.EOF(1))
							{
								strRecord = FCFileSystem.LineInput(1);
								strRecord = FixCommas(ref strRecord);
								strArray = Strings.Split(strRecord, ",", -1, CompareConstants.vbBinaryCompare);
								frmWait.InstancePtr.IncrementProgress();
								if (Strings.UCase(Strings.Left(strArray[0], 7)) == "SERVICE")
								{
									goto GetNext;
								}
								boolError = true;
								if (Convert.ToByte(Strings.Left(strArray[0], 1)[0]) != 0)
								{
									// Find the account number, book and seq by using the Service ID field
									if (GetMeterInformation_567(strArray[0], ref lngBook, ref lngSeq, ref lngAcct, false))
									{
										// Account Number
										strLine = Strings.Format(lngAcct, "000000");
										// Book
										strLine += Strings.Format(lngBook, "0000");
										// Sequence
										strLine += Strings.Format(lngSeq, "0000");
										// Current Reading
										strLine += Strings.Format(strArray[1], "000000");
										// Reading Date
										strLine += Strings.Format(datDefaultReading, "MMddyy");
										FCFileSystem.PrintLine(2, strLine);
										//Application.DoEvents();
									}
									else
									{
										// add an error line in the error array becuase the account or meter was not found
										Array.Resize(ref Statics.HampdenImportError, lngErrorIndex + 1);
										//Application.DoEvents();
										Statics.HampdenImportError[lngErrorIndex].ServiceID = strArray[0];
										Statics.HampdenImportError[lngErrorIndex].CurrentReading = strArray[1];
										Statics.HampdenImportError[lngErrorIndex].Location = strArray[9];
										Statics.HampdenImportError[lngErrorIndex].Name = strArray[6];
										Statics.HampdenImportError[lngErrorIndex].ReadingDate = Strings.Format(datDefaultReading, "MM/dd/yy");
										Statics.HampdenImportError[lngErrorIndex].Route = Strings.Left(strArray[10], 3);
										lngErrorIndex += 1;
									}
									counter += 1;
									//Application.DoEvents();
								}
							}
							lngError = 20;
							FCFileSystem.FileClose(1);
							lngError = 30;
							FCFileSystem.FileClose(2);
							lngError = 31;
							frmWait.InstancePtr.Unload();
                            //MessageBox.Show(Application.MapPath("\\") + "\\TSUTXX41.DAT had been created successfully!", "File Created From Custom Hampden File", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            FCUtils.Download(FCFileSystem.Statics.UserDataFolder + "\\TSUTXX41.DAT");
                            lngError = 32;
							if (lngErrorIndex > 0)
							{
								lngError = 33;
								//Application.DoEvents();
								// show a report of the errors
								rptMeterImportErrorReport.InstancePtr.Init(6);
							}
						}
					}
					return;
				}
				catch (Exception ex)
				{
					
					frmWait.InstancePtr.Unload();
					MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Converting Custom Hampden File - " + FCConvert.ToString(lngError), MessageBoxButtons.OK, MessageBoxIcon.Hand);
				}
			}
			catch
			{
			}
		}

        public static void ConvertBTWDConsumptionFile()
        {
            // kk 01062014 trout-998 Added conversion for Topshsm Sewer (consumption file from Water)
            // File is tab separated text: Water Acct Number <tab> Meter S/N <tab>  Reading, Time & Date (1-4 reading, 5-8 time, 9-16 date, but the readings can run over to > 4 digits
            // kk01202014 File is NOT tab delimited text, it is fixed length, the sample files were edited in Excel and saved as tab delimited.
            // kk01222014 This file comes from Brunswick-Topsham Water District to Topsham and Brunswick.
            // Updating UT to show this selection for Topsham Sewer and Brunswick Sewer.
            try
            {
                // On Error GoTo ERROR_HANDLER

                string strCurDir;
                string strFileName = "";
                short ans;
                //FCFCFileSystemObject fs = new /*AsNew*/ FCFCFileSystemObject();
                string strDestinationPath = "";
                DateTime datDefaultReading;
                string strRecord = "";
                string strLine = "";
                short counter = 0;
                int lngBook = 0;
                int lngSeq = 0;
                int lngMK = 0;
                string strAcct = "";
                int lngErrorIndex = 0;
                int lngError;
                string strXRef = "";
                string strSerNum = "";
                string strTemp = "";
                bool boolCont = false;

                bool boolAppend;

                bool boolResp;

                // vbPorter upgrade warning: dtReadingDate As DateTime	OnWrite(DateTime, string)
                DateTime dtReadingDate = DateTime.MinValue;
                // vbPorter upgrade warning: lngCurrent As int	OnRead(string)
                int lngCurrent = 0;
                string[] strArr = null;

                int lngFileVer = 0;

                lngError = 1;

                strCurDir = FCFileSystem.Statics.UserDataFolder;
                boolAppend = false;
                PickAgain: ;

                try
                {
                    MDIParent.InstancePtr.CommonDialog1.FileName = "*.txt";
                    MDIParent.InstancePtr.CommonDialog1.Filter = "*.txt";
                    MDIParent.InstancePtr.CommonDialog1.InitDir = fecherFoundation.VisualBasicLayer.FCFileSystem.Statics.UserDataFolder;

                    if (MDIParent.InstancePtr.CommonDialog1.ShowOpen())
                    {
                        strFileName = MDIParent.InstancePtr.CommonDialog1.FileName;
                    }
                    else
                    {
                        strFileName = "";
                        if (!boolAppend)
                        {
                            return;
                        }
                    }
                    modAPIsConst.SetCurrentDirectoryWrp(ref strCurDir);
                    FCFileSystem.FileClose(1);
                    if (!boolAppend)
                    { // kk07072015 trout-1143
                        FCFileSystem.FileClose(2);
                    }
                    lngError = 4;
                    boolResp = false;

                    if (strFileName != "" || boolAppend)
                    {
                        // kk07072015 trout-1143  Need to get to end of process if append is canceled
                        if (strFileName != "")
                        {
                            // kk02172105 trout-1136  Check the file format
                            FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess) (-1), (OpenShare) (-1),
                                -1);
                            strRecord = FCFileSystem.LineInput(1);
                            FCFileSystem.FileClose(1);
                            if (fecherFoundation.Strings.InStr(strRecord, "|", CompareConstants.vbBinaryCompare) > 0)
                            {
                                lngFileVer = 2;
                            }
                            else
                            {
                                lngFileVer = 1;
                            }

                            // open the input file from
                            FCFileSystem.FileOpen(1, strFileName, OpenMode.Input, (OpenAccess) (-1), (OpenShare) (-1),
                                -1);

                            // open the output file
                            // kk07072014 trout-1143  Add option to append to dat file so File Ver 1 and 2 can be imported together
                            if (!boolAppend)
                            {
                                FCFileSystem.FileOpen(2, "TSUTXX41.DAT", OpenMode.Output, (OpenAccess) (-1),
                                    (OpenShare) (-1), -1);
                            }

                            lngError = 5;

                            // clear the error array
                            if (!boolAppend)
                            {
                                // kk07072015 trout-1143
                                FCUtils.EraseSafe(Statics.TopshamImportError);
                            }

                            if (lngFileVer == 1)
                            {
                                // kk02172105 trout-1136  Setup the array if this is the old file format
                                strArr = new string[4 + 1];
                            }

                            lngError = 10;

                            // default reading date
                            datDefaultReading = DateTime.Today;

                            // kk07072015 trout-1143  Need to run to end of process if appending
                            // If frmInput.Init(datDefaultReading, "Input Default Reading Date", "Please input the default reading date to be used for this file conversion.", 1500, , idtDate) Then
                            object tempDat = datDefaultReading;
                            boolResp = frmInput.InstancePtr.Init(ref tempDat, "Input Default Reading Date",
                                "Please input the default reading date to be used for this file conversion.", 1500,
                                false, modGlobalConstants.InputDTypes.idtDate);
                            if (boolResp || boolAppend)
                            {
                                if (boolResp)
                                {
                                    lngError = 11;
                                    counter = 1;
                                    frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Processing Meters");
                                    while (!FCFileSystem.EOF(1))
                                    {

                                        strRecord = FCFileSystem.LineInput(1);

                                        if (fecherFoundation.Strings.UCase(
                                                fecherFoundation.Strings.Left(strRecord, 4)) != "ACCT")
                                        {

                                            // kk02172015 trout-1136  Handle new file format
                                            if (lngFileVer == 2)
                                            {

                                                strArr = fecherFoundation.Strings.Split(strRecord, "|", -1, CompareConstants.vbBinaryCompare); // strArr(0) = ?, strArr(1) = BTWD Account Number, strArr(2) = Meter Tag Number, strArr(3) = Reading, strArr(4) = Read Date

                                                // Build the TRIO account number from the Account Number in the file
                                                strXRef = fecherFoundation.Strings.Format(
                                                    fecherFoundation.Strings.Trim(strArr[1]), "00000000000");
                                                strSerNum = fecherFoundation.Strings.Trim(strArr[2]);
                                                strAcct = fecherFoundation.Strings.Left(strXRef, 7) +
                                                          fecherFoundation.Strings.Right(strXRef, 2);
                                                lngCurrent = FCUtils.iDiv(fecherFoundation.Conversion.Val(strArr[3]),
                                                    100);
                                                dtReadingDate = fecherFoundation.DateAndTime.DateValue(strArr[4]);

                                                if (fecherFoundation.Conversion.Val(strArr[3]) == 0 &&
                                                    dtReadingDate.ToOADate() == fecherFoundation.DateAndTime
                                                        .DateValue("01/01/2001").ToOADate())
                                                {
                                                    lngCurrent = -1;
                                                }

                                            }
                                            else if (lngFileVer == 1)
                                            {
                                                // This is the old file format

                                                // kk01202014 Fix conversion   strArr() = Split(strRecord, vbTab)
                                                strArr[0] = fecherFoundation.Strings.Trim(
                                                    fecherFoundation.Strings.Left(strRecord,
                                                        11)); // BTWD Account Number
                                                strArr[1] = fecherFoundation.Strings.Trim(
                                                    fecherFoundation.Strings.Mid(strRecord, 16, 8)); // Meter Tag Number
                                                strArr[2] = fecherFoundation.Strings.Trim(
                                                    fecherFoundation.Strings.Mid(strRecord, 25,
                                                        8)); // Reading (4 or 8 digit)
                                                strArr[3] = fecherFoundation.Strings.Trim(
                                                    fecherFoundation.Strings.Mid(strRecord, 34)); // Read Time and Date

                                                // Build the TRIO account number from the Account Number in the file
                                                strXRef = fecherFoundation.Strings.Format(
                                                    fecherFoundation.Strings.Trim(strArr[0]), "00000000000");
                                                strSerNum = fecherFoundation.Strings.Trim(strArr[1]);
                                                strAcct = fecherFoundation.Strings.Left(strXRef, 7) +
                                                          fecherFoundation.Strings.Right(strXRef, 2);

                                                // kk03112014  Change to BTWD File Format
                                                // Get the reading (first 4 digits of the 3rd field, but it is allowed to overrun)
                                                if (strArr[2].Length == 4)
                                                {
                                                    lngCurrent =
                                                        (int) Math.Round(fecherFoundation.Conversion.Val(strArr[2]));
                                                }
                                                else if (strArr[2].Length == 8)
                                                {
                                                    lngCurrent = (int) Math.Round(
                                                        fecherFoundation.Conversion.Val(
                                                            fecherFoundation.Strings.Left(strArr[2], 4)));
                                                }
                                                else
                                                {
                                                    lngCurrent = -1;
                                                }

                                                // Get the reading date - strip the B off the end of the line
                                                strTemp = fecherFoundation.Strings.Right(strArr[3], 9);
                                                strTemp = fecherFoundation.Strings.Left(strTemp, 8);
                                                dtReadingDate = DateTime.Parse(
                                                    fecherFoundation.Strings.Left(strTemp, 2) + "/" +
                                                    fecherFoundation.Strings.Mid(strTemp, 3, 2) + "/" +
                                                    fecherFoundation.Strings.Right(strTemp, 4));

                                            }

                                            // Debug.Print strXRef, strAcct, strSerNum, lngCurrent, Format(dtReadingDate, "MM/dd/yyyy")

                                            if (strAcct != "" && fecherFoundation.Conversion.Val(strAcct) != 0 &&
                                                lngCurrent != -1)
                                            {
                                                var lngPrev = 0;
                                                // Find the meter from the XRef1 field
                                                if (fecherFoundation.Strings.UCase(
                                                        fecherFoundation.Strings.Left(
                                                            modGlobalConstants.Statics.MuniName, 12)) == "BRUNSWICK SE")
                                                {
                                                    boolCont = GetBrunswickMeterInformation(ref strXRef, ref lngBook,
                                                        ref lngSeq, ref lngMK, ref strSerNum, ref lngPrev);
                                                }
                                                else
                                                {
                                                    boolCont = GetMeterInformation(strAcct, ref lngBook, ref lngSeq,
                                                        ref lngMK, ref lngPrev, false);
                                                }

                                                if (boolCont)
                                                {
                                                    // Meter Key
                                                    strLine = fecherFoundation.Strings.Format(lngMK, "000000");
                                                    // Book
                                                    strLine += fecherFoundation.Strings.Format(lngBook, "0000");
                                                    // Sequence
                                                    if (lngSeq <= 9999)
                                                    {
                                                        strLine += fecherFoundation.Strings.Format(lngSeq, "0000");
                                                    }
                                                    else
                                                    {
                                                        strLine += fecherFoundation.Strings.Left(
                                                            fecherFoundation.Strings.Format(lngSeq, "0000"), 4);
                                                    }

                                                    // Current Reading
                                                    strLine += fecherFoundation.Strings.Format(lngCurrent, "000000");
                                                    // Reading Date
                                                    strLine += fecherFoundation.Strings.Format(dtReadingDate, "MMDDYY");
                                                    FCFileSystem.PrintLine(2, strLine);
                                                    //////Application.DoEvents();
                                                }
                                                else
                                                {
                                                    if (!IsInExceptionList(ref strXRef))
                                                    {
                                                        // add an error line in thne error array becuase the meter was not found
                                                        Array.Resize(ref Statics.TopshamImportError, lngErrorIndex + 1);
                                                        //////Application.DoEvents();
                                                        Statics.TopshamImportError[lngErrorIndex].AccountNumber = strAcct;
                                                        if (lngCurrent != -1)
                                                        {
                                                            Statics.TopshamImportError[lngErrorIndex].Reading =
                                                                FCConvert.ToString(lngCurrent);
                                                        }
                                                        else
                                                        {
                                                            Statics.TopshamImportError[lngErrorIndex].Reading =
                                                                FCConvert.ToString(0);
                                                        }

                                                        Statics.TopshamImportError[lngErrorIndex].ReadingDate =
                                                            fecherFoundation.Strings.Format(dtReadingDate, "MM/DD/YY");
                                                        Statics.TopshamImportError[lngErrorIndex].SerialNumber = strSerNum;

                                                        lngErrorIndex += 1;
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                // add an error line in the error array becuase the meter was not found
                                                Array.Resize(ref Statics.TopshamImportError, lngErrorIndex + 1);
                                                //////Application.DoEvents();
                                                Statics.TopshamImportError[lngErrorIndex].AccountNumber = strAcct;
                                                if (lngCurrent != -1)
                                                {
                                                    Statics.TopshamImportError[lngErrorIndex].Reading =
                                                        FCConvert.ToString(lngCurrent);
                                                }
                                                else
                                                {
                                                    Statics.TopshamImportError[lngErrorIndex].Reading = FCConvert.ToString(0);
                                                }

                                                Statics.TopshamImportError[lngErrorIndex].ReadingDate =
                                                    fecherFoundation.Strings.Format(dtReadingDate, "MM/DD/YY");
                                                Statics.TopshamImportError[lngErrorIndex].SerialNumber = strSerNum;

                                                lngErrorIndex += 1;
                                            }

                                            counter += 1;
                                        }
                                    }

                                    lngError = 20;
                                    FCFileSystem.FileClose(1);
                                    lngError = 30;

                                    // kk07072014 trout-1143  Add option to append to dat file so File Ver 1 and 2 can be imported together
                                    // Close #2
                                    // lngError = 31
                                    frmWait.InstancePtr.Unload();
                                } // If boolResp Then
                            } // If boolResp Or boolAppend Then
                        } // If strFileName <> "" Then

                        // kk07072014 trout-1143  Add option to append to dat file so File Ver 1 and 2 can be imported together
                        if (boolResp || boolAppend)
                        {
                            var lngResp =  MessageBox.Show(
                                "Would you like to append another file to the current data file?",
                                "Append additional readings", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (lngResp == DialogResult.Yes)
                            {
                                boolAppend = true;
                                goto PickAgain;
                            }

                            FCFileSystem.FileClose(2);
                            lngError = 31;

                            MessageBox.Show(
                                Environment.CurrentDirectory + "\\TSUTXX41.DAT had been created successfully!",
                                "File Created From BTWD Extract", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            lngError = 32;

                            if (lngErrorIndex > 0)
                            {
                                lngError = 33;
                                //////Application.DoEvents();
                                // show a report of the errors
                                rptMeterImportErrorReport.InstancePtr.Init(7);
                            }
                        }
                    } // If strFileName <> "" Or boolAppend Then

                    return;
                }
                catch (Exception ex)
                {
                    
                    frmWait.InstancePtr.Unload();
                    MessageBox.Show(
                        "Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " +
                        fecherFoundation.Information.Err(ex).Description + ".",
                        "Error Converting BTWD Extract - " + FCConvert.ToString(lngError), MessageBoxButtons.OK,
                        MessageBoxIcon.Hand);
                }

            }
            catch
            {
            }

        }

        private static bool GetBrunswickMeterInformation(ref string strAcctNum, ref int lngBook, ref int lngSeq, ref int lngMK, ref string strSerNum, ref int lngPrev )
        {
            bool GetBrunswickMeterInformation = false;
            try
            {   // On Error GoTo ERROR_HANDLER
                clsDRWrapper rsM = new/*AsNew*/ clsDRWrapper();
                int lngAcctNum;

                // Brunswick Sewer - Match by XRef1 and Meter Serial Number
                if (strSerNum != "")
                { // kk04032015 trout-1140  Add check for FinalBill so we don't pick up duplicate matches
                    rsM.OpenRecordset("SELECT * FROM MeterTable WHERE NOT FinalBilled = 1 AND Left(XRef1, 7) = '" + fecherFoundation.Strings.Left(strAcctNum, 7) + "' AND (SerialNumber = '" + fecherFoundation.Strings.Trim(strSerNum) + "' OR Remote = '" + fecherFoundation.Strings.Trim(strSerNum) + "')", modExtraModules.strUTDatabase);
                    if (rsM.EndOfFile())
                    {
                        rsM.OpenRecordset("SELECT * FROM MeterTable WHERE NOT FinalBilled = 1 AND Left(XRef1, 7) = '" + fecherFoundation.Strings.Left(strAcctNum, 7) + "'", modExtraModules.strUTDatabase);
                    }
                }

                if (!rsM.EndOfFile())
                {
                    GetBrunswickMeterInformation = true;
                    lngBook = (int)Math.Round(fecherFoundation.Conversion.Val(FCConvert.ToString(rsM.Get_Fields("BookNumber"))));
                    lngSeq = (int)Math.Round(fecherFoundation.Conversion.Val(FCConvert.ToString(rsM.Get_Fields("Sequence"))));
                    lngMK = FCConvert.ToInt32(rsM.Get_Fields("ID"));
                    lngPrev = FCConvert.ToInt32(rsM.Get_Fields("PreviousReading"));
                }
                else
                {
                    lngBook = 0;
                    lngSeq = 0;
                    lngMK = 0;
                    lngPrev = 0;
                }
                return GetBrunswickMeterInformation;
            }
            catch (Exception ex)
            {   
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);                
            }
            return GetBrunswickMeterInformation;
        }
        private static void CreateVflexExtract(ref int[] BookArray,  bool boolShowNoBill)
        {
            try
            {   // On Error GoTo ERROR_HANDLER

                clsDRWrapper rsInfo = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsCat = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsSize = new/*AsNew*/ clsDRWrapper();
                clsDRWrapper rsBill = new/*AsNew*/ clsDRWrapper();

                string strTemp = "";
                string strSQL = "";
                string strBooks = "";
                string strQryFlds;

                
                int counter;
                //string strLine = "";
                int lngMeterCounter;
                string strRNIname;
                string strTimeStmp;
                string strFileName;

                string strSrvcAddr = "";
                int lngLastCat;
                string strCategory;
                int lngLastSize;
                string strSize;
                string strMXU = "";
                // vbPorter upgrade warning: lngLastReading As int	OnWrite(string)
                int lngLastReading = 0;
                string strLastBillDate = "";

                cSettingsController tSettings;

                tSettings = new cSettingsController();
                strRNIname = tSettings.GetSettingValue("VFlexSiteCode", "UtilityBilling", "", "", "");
                if (strRNIname == "")
                {
                    strRNIname = "HLGME"; // Default to something?  BTHME is Bath Water
                }

                strTimeStmp = fecherFoundation.Strings.Format(DateTime.Now, "yyyyMMddhhmmss");

                strFileName = strRNIname + "_VFLEX_W_" + strTimeStmp + ".csv";
                string tempFolder = Path.Combine(FCFileSystem.Statics.UserDataFolder, "Temp");
                if (!Directory.Exists(tempFolder))
                {
                    Directory.CreateDirectory(tempFolder);
                }

                lngMeterCounter = 0;

                // if we are only reporting on selected books then build the sql string to find the records we want
                if (fecherFoundation.Information.UBound(BookArray, 1) == 1)
                {
                    strBooks = "Book " + FCConvert.ToString(BookArray[1]);
                    strSQL = " = " + FCConvert.ToString(BookArray[1]);
                }
                else
                {
                    strBooks = "Books ";
                    for (counter = 1; counter <= (short)(fecherFoundation.Information.UBound(BookArray, 1)); counter++)
                    {
                        strSQL += FCConvert.ToString(BookArray[counter]) + ",";
                        strBooks += FCConvert.ToString(BookArray[counter]) + ",";
                    }
                    strSQL = "IN (" + fecherFoundation.Strings.Left(strSQL, strSQL.Length - 1) + ")";
                    strBooks = fecherFoundation.Strings.Left(strBooks, strBooks.Length - 1);
                }

                strQryFlds = "Master.Comment, MeterTable.Location, BookNumber, Sequence, CurrentReading, PreviousReading, Digits, Multiplier, Size, Remote, SerialNumber, MXU, Radio, RadioAccessType, " + "AccountNumber, p.FullNameLF AS Name, StreetName, StreetNumber, Apt, MapLot, Long, Lat, Telephone, Master.Email, WCat FROM (MeterTable INNER JOIN Master ON MeterTable.AccountKey = Master.ID) " + "INNER JOIN " + modMain.Statics.strDbCP + "PartyNameView p ON p.ID = Master.BillingPartyID";

                if (boolShowNoBill)
                {
                    rsInfo.OpenRecordset("SELECT " + strQryFlds + " WHERE BookNumber " + strSQL + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0  ORDER BY BookNumber, Sequence, AccountNumber");
                }
                else
                {
                    rsInfo.OpenRecordset("SELECT " + strQryFlds + " WHERE BookNumber " + strSQL + " AND ISNULL(FinalBilled,0) = 0 AND ISNULL(Deleted,0) = 0  AND (ISNULL(MeterTable.NoBill,0) = 0 " + "AND ISNULL(Master.NoBill,0) = 0) ORDER BY BookNumber, Sequence, AccountNumber");
                }

                lngLastCat = 0;
                strCategory = "";
                lngLastSize = 0;
                strSize = "";

                if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
                {
                    using (var streamWriter = File.CreateText(Path.Combine(tempFolder, strFileName)))
                    {
                        do
                        {
                            // Get the category
                            if (fecherFoundation.Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("WCat"))) !=
                                lngLastCat)
                            {
                                lngLastCat =
                                    (int) Math.Round(
                                        fecherFoundation.Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("WCat"))));
                                rsCat.OpenRecordset("SELECT LongDescription FROM Category WHERE Code = " +
                                                    FCConvert.ToString(lngLastCat));
                                if (!rsCat.EndOfFile())
                                {
                                    strCategory =
                                        fecherFoundation.Strings.Trim(
                                            FCConvert.ToString(rsCat.Get_Fields("LongDescription")));
                                }
                                else
                                {
                                    strCategory = "";
                                }
                            }

                            if (fecherFoundation.Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("Size"))) !=
                                lngLastSize)
                            {
                                lngLastSize =
                                    (int) Math.Round(
                                        fecherFoundation.Conversion.Val(FCConvert.ToString(rsInfo.Get_Fields("Size"))));
                                rsSize.OpenRecordset("SELECT ShortDescription FROM MeterSizes WHERE Code = " +
                                                     FCConvert.ToString(lngLastSize));
                                if (!rsSize.EndOfFile())
                                {
                                    strSize = fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(rsSize.Get_Fields("ShortDescription")));
                                }
                                else
                                {
                                    strSize = "";
                                }
                            }

                            rsBill.OpenRecordset("SELECT TOP 1 BillDate FROM Bill WHERE ActualAccountNumber = " +
                                                 rsInfo.Get_Fields("AccountNumber") +
                                                 " AND BillingRateKey > 0 AND BillStatus = 'B' ORDER BY ID DESC");
                            if (!rsBill.EndOfFile())
                            {
                                strTemp = fecherFoundation.Strings.Format(rsBill.Get_Fields("BillDate"), "mm/dd/yy");
                                strLastBillDate = fecherFoundation.Strings.Right(strTemp, 2) +
                                                  fecherFoundation.Strings.Left(strTemp, 2) +
                                                  fecherFoundation.Strings.Mid(strTemp, 4, 2); // YYMMDD
                            }
                            else
                            {
                                strLastBillDate = "";
                            }

                            // Build the service address
                            if (fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("StreetNumber"))) ==
                                "")
                            {
                                strSrvcAddr =
                                    fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("StreetName"))) +
                                    " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("Apt")));
                            }
                            else
                            {
                                strSrvcAddr =
                                    fecherFoundation.Strings.Trim(
                                        FCConvert.ToString(rsInfo.Get_Fields("StreetNumber"))) + " " +
                                    fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("StreetName"))) +
                                    " " + fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("Apt")));
                            }

                            if (FCConvert.ToString(rsInfo.Get_Fields("BillingStatus")) == "B")
                            {
                                if (rsInfo.Get_Fields("CurrentReading") < 0)
                                {
                                    lngLastReading = FCConvert.ToInt32("");
                                }
                                else
                                {
                                    lngLastReading = FCConvert.ToInt32(rsInfo.Get_Fields("CurrentReading"));
                                }
                            }
                            else
                            {
                                if (rsInfo.Get_Fields("PreviousReading") < 0)
                                {
                                    lngLastReading = FCConvert.ToInt32("");
                                }
                                else
                                {
                                    lngLastReading = FCConvert.ToInt32(rsInfo.Get_Fields("PreviousReading"));
                                }
                            }

                            // 05212018  Remove leading 'C' from MXU
                            if (fecherFoundation.Strings.UCase(
                                    fecherFoundation.Strings.Left(FCConvert.ToString(rsInfo.Get_Fields("MXU")), 1)) ==
                                "C")
                            {
                                strMXU = fecherFoundation.Strings.Mid(FCConvert.ToString(rsInfo.Get_Fields("MXU")), 2);
                            }
                            else
                            {
                                strMXU = FCConvert.ToString(rsInfo.Get_Fields("MXU"));
                            }

                            StringBuilder strLine = new StringBuilder(
                                FCConvert.ToString(rsInfo.Get_Fields("Remote")).ToString() + "," +
                                fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("SerialNumber"))) +
                                ",");
                            strLine.Append(fecherFoundation.Strings.Trim(strMXU) + "," +
                                           rsInfo.Get_Fields("BookNumber") +
                                           ",");
                            strLine.Append(rsInfo.Get_Fields("Sequence") + "," + rsInfo.Get_Fields("AccountNumber") +
                                           ",");
                            strLine.Append(
                                rsInfo.Get_Fields_String("Name").Trim().WithQuotes() + "," +
                                strSrvcAddr.WithQuotes() + ",");
                            strLine.Append(lngLastReading.ToString() + "," +
                                           fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("Lat"))) +
                                           ",");
                            strLine.Append(
                                fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("Long"))) + ",");
                            strLine.Append(
                                fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("Multiplier"))) +
                                ",");
                            strLine.Append(
                                fecherFoundation.Strings.Trim(FCConvert.ToString(rsInfo.Get_Fields("Digits"))) + ",");
                            strLine.Append(strCategory + "," + strSize + ",");
                            strLine.Append(
	                            rsInfo.Get_Fields_String("Comment").Trim().WithQuotes() + ",");
                            strLine.Append(
	                            rsInfo.Get_Fields_String("Location").Trim().WithQuotes() + ",");
                            strLine.Append(
	                            rsInfo.Get_Fields_String("Telephone").Trim().WithQuotes() +
                                ",");
                            strLine.Append(
	                            rsInfo.Get_Fields_String("Email").Trim().WithQuotes() + ",");
                            strLine.Append(strLastBillDate);

                            streamWriter.WriteLine(strLine.ToString());

                            lngMeterCounter += 1;
                            rsInfo.MoveNext();
                        } while (rsInfo.EndOfFile() != true);
                    }

                    MessageBox.Show(strBooks + "\n" + "Records " + FCConvert.ToString(lngMeterCounter), "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    FCUtils.Download(Path.Combine(tempFolder, strFileName), strFileName);
                }
                else
                {
                    MessageBox.Show(strBooks + "\n" + "No Records", "Process Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                

                return;
            }
            catch (Exception ex)
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Error Creating Prescott Extract", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }


        private static bool IsInExceptionList(ref string strAccountNumber)
        {
            bool IsInExceptionList = false;
            try
            {   // On Error GoTo ERROR_HANDLER

                clsDRWrapper rsExcChk = new/*AsNew*/ clsDRWrapper();

                if (rsExcChk.UpdateDatabaseTable("tblImportExceptions", modExtraModules.strUTDatabase))
                {
                    // kk12142016 tromv-1076  Change exception list to only look at first 7 digits
                    rsExcChk.OpenRecordset("SELECT ID FROM tblImportExceptions WHERE Left(AccountNumber,7) = '" + fecherFoundation.Strings.Left(strAccountNumber, 7) + "'", modExtraModules.strUTDatabase);
                    IsInExceptionList = !(rsExcChk.BeginningOfFile() || rsExcChk.EndOfFile());
                }

                return IsInExceptionList;
            }
            catch (Exception ex)
            {   
                MessageBox.Show("Error #" + FCConvert.ToString(fecherFoundation.Information.Err(ex).Number) + " - " + fecherFoundation.Information.Err(ex).Description + ".", "Exception List", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            return IsInExceptionList;
        }

        public class StaticVariables
		{
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public CONSUMPTIONRECORD CR = new CONSUMPTIONRECORD(0);
			public CONSUMPTIONRECORD[] CRMeterImportError = null;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public DOSCONSUMPTIONRECORD DOSCR = new DOSCONSUMPTIONRECORD(0);
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public AquaAmericaInputFile AAIF = new AquaAmericaInputFile(0);
			public AquaAmericaInputFile[] MeterImportError = null;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public RVSInputFile RVSIF = new RVSInputFile(0);
			public RVSInputFile[] RVSMeterImportError = null;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public EZREADERCONSUMPTIONRECORD EZCR = new EZREADERCONSUMPTIONRECORD(0);
			public EZREADERCONSUMPTIONRECORD[] EZImportError = null;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public BELFASTCONSUMPTIONRECORD BELCR = new BELFASTCONSUMPTIONRECORD(0);
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public OAKLANDCONSUMPTIONRECORD OAKCR = new OAKLANDCONSUMPTIONRECORD(0);
			public OAKLANDCONSUMPTIONRECORD[] NoAccountFound = null;
			public int lngNoMatchCounter;
			public OaklandMeterInfo[] OaklandReportInfo = null;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public BadgerFormat BadgerIF = new BadgerFormat(0);
			public BadgerFormat[] BadgerMeterImportError = null;
			public BangorErrorFormat[] BangorMeterImportError = null;
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public MillinocketInputFile MIF = new MillinocketInputFile(0);
			//FC:FINAL:RPU:#i862 - Use custom constructor to initialize string fields with empty value
			public LisbonInputFile LIF = new LisbonInputFile(0);
			public HampdenImportFile[] HampdenImportError = null;
            public TopshamImportFile[] TopshamImportError = null;
			//public BookSeq[] BookSeqArray = null;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}

       
	}
}
