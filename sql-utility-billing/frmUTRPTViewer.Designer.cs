﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using System.IO;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmUTRPTViewer.
	/// </summary>
	partial class frmUTRPTViewer : BaseForm
	{
		public fecherFoundation.FCFrame fraRecreateReport;
		public fecherFoundation.FCComboBox cmbRecreate;
		public fecherFoundation.FCLabel lblRecreateInstructions;
		public ARViewer arView;
		public fecherFoundation.FCFrame fraNumber;
		public fecherFoundation.FCComboBox cmbNumber;
		public fecherFoundation.FCLabel lblInstruction;
		private Wisej.Web.MainMenu MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileEmail;
		public fecherFoundation.FCToolStripMenuItem mnuEMailRTF;
		public fecherFoundation.FCToolStripMenuItem mnuEmailPDF;
		public fecherFoundation.FCToolStripMenuItem mnuFileExport;
		public fecherFoundation.FCToolStripMenuItem mnuFileExportRTF;
		public fecherFoundation.FCToolStripMenuItem mnuFileExportPDF;
		public fecherFoundation.FCToolStripMenuItem mnuFileExportHTML;
		public fecherFoundation.FCToolStripMenuItem mnuFileExportExcel;
		public fecherFoundation.FCToolStripMenuItem mnuFileSpacer;
		public fecherFoundation.FCToolStripMenuItem mnuFileChange;
		public fecherFoundation.FCToolStripMenuItem mnuFileSwitch;
		public fecherFoundation.FCToolStripMenuItem mnuFileRecreate;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUTRPTViewer));
            this.fraRecreateReport = new fecherFoundation.FCFrame();
            this.cmbRecreate = new fecherFoundation.FCComboBox();
            this.lblRecreateInstructions = new fecherFoundation.FCLabel();
            this.arView = new Global.ARViewer();
            this.fraNumber = new fecherFoundation.FCFrame();
            this.cmbNumber = new fecherFoundation.FCComboBox();
            this.lblInstruction = new fecherFoundation.FCLabel();
            this.MainMenu1 = new Wisej.Web.MainMenu(this.components);
            this.mnuEMailRTF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuEmailPDF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExportRTF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExportPDF = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExportHTML = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExportExcel = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileEmail = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExport = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSpacer = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileChange = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSwitch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileRecreate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
            this.toolBar1 = new Wisej.Web.ToolBar();
            this.toolBarButtonEmailPDF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonEmailRTF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportPDF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportRTF = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportHTML = new Wisej.Web.ToolBarButton();
            this.toolBarButtonExportExcel = new Wisej.Web.ToolBarButton();
            this.toolBarButtonPrint = new Wisej.Web.ToolBarButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.TopWrapper = new fecherFoundation.FCPanel();
            this.cmdFileRecreate = new fecherFoundation.FCButton();
            this.cmdFileChange = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecreateReport)).BeginInit();
            this.fraRecreateReport.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraNumber)).BeginInit();
            this.fraNumber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopWrapper)).BeginInit();
            this.TopWrapper.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileRecreate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileChange)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 666);
            this.BottomPanel.Size = new System.Drawing.Size(746, 0);
            // 
            // ClientArea
            // 
			 this.ClientArea.Controls.Add(this.fraRecreateReport);
            this.ClientArea.Controls.Add(this.fraNumber);
            this.ClientArea.Controls.Add(this.arView);
            this.ClientArea.Controls.Add(this.TopWrapper);
            this.ClientArea.Controls.Add(this.toolBar1);
            this.ClientArea.Size = new System.Drawing.Size(746, 606);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdFileChange);
            this.TopPanel.Controls.Add(this.cmdFileRecreate);
            this.TopPanel.Size = new System.Drawing.Size(746, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileRecreate, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdFileChange, 0);
            // 
            // HeaderText
            // 
			 this.HeaderText.Location = new System.Drawing.Point(30, 26);
            this.HeaderText.Size = new System.Drawing.Size(379, 30);
            this.HeaderText.Text = "Redisplay Last Daily Audit Report";
            // 
            // fraRecreateReport
            // 
            this.fraRecreateReport.AppearanceKey = "groupBoxNoBorders";
            this.fraRecreateReport.Controls.Add(this.cmbRecreate);
            this.fraRecreateReport.Controls.Add(this.lblRecreateInstructions);
            this.fraRecreateReport.Location = new System.Drawing.Point(10, 49);
            this.fraRecreateReport.Name = "fraRecreateReport";
            this.fraRecreateReport.Size = new System.Drawing.Size(338, 106);
            this.fraRecreateReport.TabIndex = 1;
            this.fraRecreateReport.Visible = false;
            // 
            // cmbRecreate
            // 
            this.cmbRecreate.AutoSize = false;
            this.cmbRecreate.BackColor = System.Drawing.SystemColors.Window;
			 this.cmbRecreate.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbRecreate.FormattingEnabled = true;
            this.cmbRecreate.Location = new System.Drawing.Point(20, 54);
            this.cmbRecreate.Name = "cmbRecreate";
            this.cmbRecreate.Size = new System.Drawing.Size(234, 40);
            this.cmbRecreate.TabIndex = 1;
            // 
            // lblRecreateInstructions
            // 
            this.lblRecreateInstructions.Location = new System.Drawing.Point(20, 28);
            this.lblRecreateInstructions.Name = "lblRecreateInstructions";
            this.lblRecreateInstructions.Size = new System.Drawing.Size(267, 15);
			 this.lblRecreateInstructions.TabIndex = 0;
            this.lblRecreateInstructions.Text = "CHOOSE THE REPORT NUMBER TO VIEW";
            // 
            // arView
            // 
            this.arView.Dock = Wisej.Web.DockStyle.Fill;
            this.arView.Location = new System.Drawing.Point(0, 40);
            this.arView.Name = "arView";
			this.arView.ReportName = null;
            this.arView.ReportSource = null;
            this.arView.ScrollBars = false;
            this.arView.Size = new System.Drawing.Size(746, 566);
            this.arView.TabIndex = 2;
            // 
            // fraNumber
            // 
            this.fraNumber.AppearanceKey = "groupBoxNoBorders";
            
            this.fraNumber.Controls.Add(this.cmbNumber);
            this.fraNumber.Controls.Add(this.lblInstruction);
            this.fraNumber.Location = new System.Drawing.Point(30, 49);
            this.fraNumber.Name = "fraNumber";
            this.fraNumber.Size = new System.Drawing.Size(292, 100);
			this.fraNumber.TabIndex = 0;
            this.fraNumber.Visible = false;
            // 
            // cmbNumber
            // 
			 this.cmbNumber.AutoSize = false;
            this.cmbNumber.BackColor = System.Drawing.SystemColors.Window;
			this.cmbNumber.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbNumber.FormattingEnabled = true;
            this.cmbNumber.Location = new System.Drawing.Point(20, 50);
            this.cmbNumber.Name = "cmbNumber";
            this.cmbNumber.Size = new System.Drawing.Size(231, 40);
            this.cmbNumber.TabIndex = 1;
            // 
            // lblInstruction
            // 
            this.lblInstruction.Location = new System.Drawing.Point(20, 28);
            this.lblInstruction.Name = "lblInstruction";
            this.lblInstruction.Size = new System.Drawing.Size(270, 15);
			this.lblInstruction.TabIndex = 0;
            this.lblInstruction.Text = "CHOOSE THE REPORT NUMBER TO VIEW";
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuEMailRTF,
            this.mnuEmailPDF,
            this.mnuFileExportRTF,
            this.mnuFileExportPDF,
            this.mnuFileExportHTML,
            this.mnuFileExportExcel});
            this.MainMenu1.Name = "MainMenu1";
            // 
            // mnuEMailRTF
            // 
            this.mnuEMailRTF.Index = 0;
            this.mnuEMailRTF.Name = "mnuEMailRTF";
            this.mnuEMailRTF.Text = "E-mail as Rich Text";
            this.mnuEMailRTF.Click += new System.EventHandler(this.mnuEMailRTF_Click);
            // 
            // mnuEmailPDF
            // 
            this.mnuEmailPDF.Index = 1;
            this.mnuEmailPDF.Name = "mnuEmailPDF";
            this.mnuEmailPDF.Text = "E-mail as a PDF";
            this.mnuEmailPDF.Click += new System.EventHandler(this.mnuEmailPDF_Click);
            // 
            // mnuFileExportRTF
            // 
            this.mnuFileExportRTF.Index = 2;
            this.mnuFileExportRTF.Name = "mnuFileExportRTF";
            this.mnuFileExportRTF.Text = "Export as Rich Text";
            this.mnuFileExportRTF.Click += new System.EventHandler(this.mnuFileExportRTF_Click);
            // 
            // mnuFileExportPDF
            // 
            this.mnuFileExportPDF.Index = 3;
            this.mnuFileExportPDF.Name = "mnuFileExportPDF";
            this.mnuFileExportPDF.Text = "Export as PDF";
            this.mnuFileExportPDF.Click += new System.EventHandler(this.mnuFileExportPDF_Click);
            // 
            // mnuFileExportHTML
            // 
            this.mnuFileExportHTML.Index = 4;
            this.mnuFileExportHTML.Name = "mnuFileExportHTML";
            this.mnuFileExportHTML.Text = "Export as HTML";
            this.mnuFileExportHTML.Click += new System.EventHandler(this.mnuFileExportHTML_Click);
            // 
            // mnuFileExportExcel
            // 
            this.mnuFileExportExcel.Index = 5;
            this.mnuFileExportExcel.Name = "mnuFileExportExcel";
            this.mnuFileExportExcel.Text = "Export as Excel";
            this.mnuFileExportExcel.Click += new System.EventHandler(this.mnuFileExportExcel_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileEmail,
            this.mnuFileExport,
            this.mnuFileSpacer,
            this.mnuFileChange,
            this.mnuFileSwitch,
            this.mnuFileRecreate,
            this.mnuFileSave,
            this.mnuFileSeperator,
            this.mnuFileExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            this.mnuFile.Click += new System.EventHandler(this.mnuFile_Click);
            // 
            // mnuFileEmail
            // 
            this.mnuFileEmail.Index = 0;
            this.mnuFileEmail.Name = "mnuFileEmail";
            this.mnuFileEmail.Text = "E-mail";
            // 
            // mnuFileExport
            // 
            this.mnuFileExport.Index = 1;
            this.mnuFileExport.Name = "mnuFileExport";
            this.mnuFileExport.Text = "Export";
            // 
            // mnuFileSpacer
            // 
            this.mnuFileSpacer.Index = 2;
            this.mnuFileSpacer.Name = "mnuFileSpacer";
            this.mnuFileSpacer.Text = "-";
            // 
            // mnuFileChange
            // 
            this.mnuFileChange.Index = 3;
            this.mnuFileChange.Name = "mnuFileChange";
            this.mnuFileChange.Text = "Change Report";
            this.mnuFileChange.Click += new System.EventHandler(this.mnuFileChange_Click);
            // 
            // mnuFileSwitch
            // 
            this.mnuFileSwitch.Index = 4;
            this.mnuFileSwitch.Name = "mnuFileSwitch";
            this.mnuFileSwitch.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuFileSwitch.Text = "Show RE Audit";
            this.mnuFileSwitch.Visible = false;
            this.mnuFileSwitch.Click += new System.EventHandler(this.mnuFileSwitch_Click);
            // 
            // mnuFileRecreate
            // 
            this.mnuFileRecreate.Index = 5;
            this.mnuFileRecreate.Name = "mnuFileRecreate";
            this.mnuFileRecreate.Text = "Recreate Daily Audit";
            this.mnuFileRecreate.Click += new System.EventHandler(this.mnuFileRecreate_Click);
            // 
            // mnuFileSave
            // 
            this.mnuFileSave.Index = 6;
            this.mnuFileSave.Name = "mnuFileSave";
            this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFileSave.Text = "Save && Continue";
            this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // mnuFileSeperator
            // 
            this.mnuFileSeperator.Index = 7;
            this.mnuFileSeperator.Name = "mnuFileSeperator";
            this.mnuFileSeperator.Text = "-";
            // 
            // mnuFileExit
            // 
            this.mnuFileExit.Index = 8;
            this.mnuFileExit.Name = "mnuFileExit";
            this.mnuFileExit.Text = "Exit";
            this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
            // 
            // toolBar1
            // 
            this.toolBar1.AutoSize = false;
            this.toolBar1.BackColor = System.Drawing.Color.FromArgb(244, 247, 249);
            this.toolBar1.Buttons.AddRange(new Wisej.Web.ToolBarButton[] {
            this.toolBarButtonEmailPDF,
            this.toolBarButtonEmailRTF,
            this.toolBarButtonExportPDF,
            this.toolBarButtonExportRTF,
            this.toolBarButtonExportHTML,
            this.toolBarButtonExportExcel,
            this.toolBarButtonPrint});
            this.toolBar1.Location = new System.Drawing.Point(0, 0);
            this.toolBar1.Name = "toolBar1";
            this.toolBar1.Size = new System.Drawing.Size(746, 40);
            this.toolBar1.TabIndex = 0;
            this.toolBar1.TabStop = false;
			this.toolBar1.ShowToolTips = true;
            this.toolBar1.ButtonClick += new Wisej.Web.ToolBarButtonClickEventHandler(this.toolBar1_ButtonClick);
            // 
            // toolBarButtonEmailPDF
            // 
            this.toolBarButtonEmailPDF.ImageSource = "icon-report-email-pdf";
            this.toolBarButtonEmailPDF.Name = "toolBarButtonEmailPDF";
            this.toolBarButtonEmailPDF.ToolTipText = "Email as PDF";
            this.toolBarButtonEmailPDF.Click += new System.EventHandler(this.mnuEmailPDF_Click);
            // 
            // toolBarButtonEmailRTF
            // 
            this.toolBarButtonEmailRTF.ImageSource = "icon-report-email-rtf";
            this.toolBarButtonEmailRTF.Name = "toolBarButtonEmailRTF";
            this.toolBarButtonEmailRTF.ToolTipText = "Email as RTF";
            this.toolBarButtonEmailRTF.Click += new System.EventHandler(this.mnuEMailRTF_Click);
            // 
            // toolBarButtonExportPDF
            // 
            this.toolBarButtonExportPDF.ImageSource = "icon-report-export-pdf";
            this.toolBarButtonExportPDF.Name = "toolBarButtonExportPDF";
            this.toolBarButtonExportPDF.ToolTipText = "Export as PDF";
            this.toolBarButtonExportPDF.Click += new System.EventHandler(this.mnuFileExportPDF_Click);
            // 
            // toolBarButtonExportRTF
            // 
            this.toolBarButtonExportRTF.ImageSource = "icon-report-export-rtf";
            this.toolBarButtonExportRTF.Name = "toolBarButtonExportRTF";
            this.toolBarButtonExportRTF.ToolTipText = "Export as RTF";
            this.toolBarButtonExportRTF.Click += new System.EventHandler(this.mnuFileExportRTF_Click);
            // 
            // toolBarButtonExportHTML
            // 
            this.toolBarButtonExportHTML.ImageSource = "icon-report-export-html";
            this.toolBarButtonExportHTML.Name = "toolBarButtonExportHTML";
            this.toolBarButtonExportHTML.ToolTipText = "Export as HTML";
            this.toolBarButtonExportHTML.Click += new System.EventHandler(this.mnuFileExportHTML_Click);
            // 
            // toolBarButtonExportExcel
            // 
            this.toolBarButtonExportExcel.ImageSource = "icon-report-export-excel";
            this.toolBarButtonExportExcel.Name = "toolBarButtonExportExcel";
            this.toolBarButtonExportExcel.ToolTipText = "Export as Excel";
            this.toolBarButtonExportExcel.Click += new System.EventHandler(this.mnuFileExportExcel_Click);
            // 
            // toolBarButtonPrint
            // 
            this.toolBarButtonPrint.ImageSource = "icon-report-print";
            this.toolBarButtonPrint.Name = "toolBarButtonPrint";
            this.toolBarButtonPrint.ToolTipText = "Print";
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 50);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(167, 48);
			 this.cmdSave.TabIndex = 0;
            this.cmdSave.Text = "Save & Continue";
            this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
            // 
            // TopWrapper
            // 
            this.TopWrapper.Controls.Add(this.cmdSave);
            this.TopWrapper.Location = new System.Drawing.Point(0, 120);
            this.TopWrapper.Name = "TopWrapper";
            this.TopWrapper.Size = new System.Drawing.Size(300, 110);
			this.TopWrapper.TabIndex = 0;
            this.TopWrapper.Visible = false;
            // 
            // cmdFileRecreate
            // 
            this.cmdFileRecreate.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileRecreate.AppearanceKey = "toolbarButton";
            this.cmdFileRecreate.Location = new System.Drawing.Point(578, 29);
            this.cmdFileRecreate.Name = "cmdFileRecreate";
            this.cmdFileRecreate.Size = new System.Drawing.Size(140, 24);
            this.cmdFileRecreate.TabIndex = 1;
            this.cmdFileRecreate.Text = "Recreate Daily Audit";
            this.cmdFileRecreate.Click += new System.EventHandler(this.mnuFileRecreate_Click);
            // 
            // cmdFileChange
            // 
            this.cmdFileChange.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileChange.AppearanceKey = "toolbarButton";
            this.cmdFileChange.Location = new System.Drawing.Point(463, 29);
            this.cmdFileChange.Name = "cmdFileChange";
            this.cmdFileChange.Size = new System.Drawing.Size(109, 24);
            this.cmdFileChange.TabIndex = 2;
            this.cmdFileChange.Text = "Change Report";
            this.cmdFileChange.Click += new System.EventHandler(this.mnuFileChange_Click);
            // 
            // frmUTRPTViewer
            // 
            this.BackColor = System.Drawing.Color.FromName("@window");
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(746, 666);
            this.FillColor = 0;
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
			this.Menu = null;
            this.Name = "frmUTRPTViewer";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Redisplay Last Daily Audit Report";
            this.Load += new System.EventHandler(this.frmUTRPTViewer_Load);
            this.Activated += new System.EventHandler(this.frmUTRPTViewer_Activated);
            this.Resize += new System.EventHandler(this.frmUTRPTViewer_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmUTRPTViewer_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraRecreateReport)).EndInit();
            this.fraRecreateReport.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraNumber)).EndInit();
            this.fraNumber.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopWrapper)).EndInit();
            this.TopWrapper.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileRecreate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdFileChange)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdFileRecreate;
		private FCButton cmdFileChange;
		//FC:FINAL:CHN - issues #1375, 1216: Redesign.
		private FCPanel TopWrapper;
        // To add margin-top to report (Location with using DockStyle.Fill not working)
        private ToolBar toolBar1;
        private ToolBarButton toolBarButtonEmailPDF;
        private ToolBarButton toolBarButtonExportPDF;
        private ToolBarButton toolBarButtonEmailRTF;
        private ToolBarButton toolBarButtonExportRTF;
        private ToolBarButton toolBarButtonExportHTML;
        private ToolBarButton toolBarButtonExportExcel;
        private ToolBarButton toolBarButtonPrint;
    }
}
