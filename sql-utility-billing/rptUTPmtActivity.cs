﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	public partial class rptUTPmtActivity : BaseSectionReport
	{
		//FC:FINAL:RPU:#i1151 - Add a flag if the report must be unloaded
		private bool unloadReport = false;

		public rptUTPmtActivity()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Payment Activity";
		}

		public static rptUTPmtActivity InstancePtr
		{
			get
			{
				return (rptUTPmtActivity)Sys.GetInstance(typeof(rptUTPmtActivity));
			}
		}

		protected rptUTPmtActivity _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
            if (disposing)
            {
				rsData.Dispose();

            }
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptUTPmtActivity	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :               Kevin Kelly             *
		// DATE           :               10/21/2014              *
		// *
		// MODIFIED BY    :               Kevin Kelly             *
		// LAST UPDATED   :               09/21/2016              *
		// ********************************************************
		clsDRWrapper rsData = new clsDRWrapper();
		double dblPrincipal;
		double dblTax;
		double dblInterest;
		double dblPLI;
		double dblCost;
		double dblTotalPrincipal;
		double dblTotalTax;
		double dblTotalInterest;
		double dblTotalPLI;
		double dblTotalCost;
		double dblSRegPrincipal;
		double dblSRegTax;
		double dblSRegInterest;
		double dblSRegCost;
		double dblSLienPrincipal;
		double dblSLienTax;
		double dblSLienInterest;
		double dblSLienPLI;
		double dblSLienCost;
		double dblWRegPrincipal;
		double dblWRegTax;
		double dblWRegInterest;
		double dblWRegCost;
		double dblWLienPrincipal;
		double dblWLienTax;
		double dblWLienInterest;
		double dblWLienPLI;
		double dblWLienCost;
		bool boolShowWReg;
		bool boolShowSReg;
		bool boolShowWLien;
		bool boolShowSLien;
		private DateTime dtStartDate;
		// Date Range
		private DateTime dtEndDate;
		private string strStartName = "";
		// Name Range
		private string strEndName = "";
		private int lngStartAcct;
		// Account Range
		private int lngEndAcct;
		private int lngStartBill;
		// Bill Number Range
		private int lngEndBill;
		private string strService = "";
		// Service - "S" - Sewer, "W" - Water, "B" or "" - Both
		private int lngTownCode;
		// Town Code for Multi-Town location
		private string strBillType = "";
		// Bill Type - "L" - Lien, "R" - Regular or "" - All
		private string strPmtTypes = "";
		private string strSortOrder = "";
		private string strRefFilter = "";

		public DateTime StartDate
		{
			get
			{
				DateTime StartDate = System.DateTime.Now;
				StartDate = dtStartDate;
				return StartDate;
			}
			set
			{
				if (dtEndDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() && value.ToOADate() > dtEndDate.ToOADate())
				{
					// this is no good!
				}
				else
				{
					dtStartDate = value;
				}
			}
		}

		public DateTime EndDate
		{
			get
			{
				DateTime EndDate = System.DateTime.Now;
				EndDate = dtEndDate;
				return EndDate;
			}
			set
			{
				if (dtStartDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() && value.ToOADate() < dtStartDate.ToOADate())
				{
					// this is no good!
				}
				else
				{
					dtEndDate = value;
				}
			}
		}

		public string Service
		{
			get
			{
				string Service = "";
				Service = strService;
				return Service;
			}
			set
			{
				if (value != "S" && value != "W" && value != "B" && value != "")
				{
					// this is no good
				}
				else
				{
					strService = value;
				}
			}
		}

		public string StartName
		{
			get
			{
				string StartName = "";
				StartName = strStartName;
				return StartName;
			}
			set
			{
				strStartName = value;
			}
		}

		public string EndName
		{
			get
			{
				string EndName = "";
				EndName = strEndName;
				return EndName;
			}
			set
			{
				strEndName = value;
			}
		}

		public int StartAccount
		{
			get
			{
				int StartAccount = 0;
				StartAccount = lngStartAcct;
				return StartAccount;
			}
			set
			{
				lngStartAcct = value;
			}
		}

		public int EndAccount
		{
			get
			{
				int EndAccount = 0;
				EndAccount = lngEndAcct;
				return EndAccount;
			}
			set
			{
				lngEndAcct = value;
			}
		}

		public int StartBill
		{
			get
			{
				int StartBill = 0;
				StartBill = lngStartBill;
				return StartBill;
			}
			set
			{
				lngStartBill = value;
			}
		}

		public int EndBill
		{
			get
			{
				int EndBill = 0;
				EndBill = lngEndBill;
				return EndBill;
			}
			set
			{
				lngEndBill = value;
			}
		}

		public string BillType
		{
			get
			{
				string BillType = "";
				BillType = strBillType;
				return BillType;
			}
			set
			{
				if (value != "L" && value != "R" && value != "")
				{
					// this is not good
				}
				else
				{
					strBillType = value;
				}
			}
		}

		public string PaymentTypes
		{
			get
			{
				string PaymentTypes = "";
				PaymentTypes = strPmtTypes;
				return PaymentTypes;
			}
			set
			{
				string[] arrCodes = null;
				string strTemp;
				int i;
				strPmtTypes = "";
				strTemp = Strings.Trim(value);
				if (strTemp != "")
				{
					if (strTemp.Length == 1)
					{
						if (ValidPmtCode(strTemp))
						{
							strPmtTypes = strTemp;
						}
					}
					else
					{
						arrCodes = Strings.Split(strTemp, ",", -1, CompareConstants.vbBinaryCompare);
						for (i = 0; i <= Information.UBound(arrCodes, 1); i++)
						{
							strTemp = Strings.Trim(arrCodes[i]);
							if (ValidPmtCode(strTemp))
							{
								if (strPmtTypes != "")
								{
									strPmtTypes += ",";
								}
								strPmtTypes += strTemp;
							}
						}
					}
				}
			}
		}

		public string SortOrder
		{
			get
			{
				string SortOrder = "";
				SortOrder = strSortOrder;
				return SortOrder;
			}
			set
			{
				strSortOrder = value;
			}
		}

		public string Reference
		{
			get
			{
				string Reference = "";
				Reference = strRefFilter;
				return Reference;
			}
			set
			{
				strRefFilter = value;
			}
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			eArgs.EOF = rsData.EndOfFile();
		}

		private void ActiveReport_Initialize()
		{
			strService = "B";
			strBillType = "";
			dtStartDate = DateAndTime.DateValue(FCConvert.ToString(0));
			dtEndDate = DateAndTime.DateValue(FCConvert.ToString(0));
			lngStartAcct = 0;
			lngEndAcct = 0;
			lngStartBill = 0;
			lngEndBill = 0;
			lngTownCode = 0;
			strSortOrder = "";
			strPmtTypes = "";
			strRefFilter = "";
			boolShowSReg = false;
			boolShowWReg = false;
			boolShowSLien = false;
			boolShowWLien = false;
		}

		private void ActiveReport_PageStart(object sender, EventArgs e)
		{
			lblPage.Text = "Page " + this.PageNumber;
		}

		private void ActiveReport_ReportEnd(object sender, EventArgs e)
		{
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			//frmWait.InstancePtr.Unload();
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			string strSQL;
			try
			{
				// On Error GoTo ERROR_HANDLER
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbHourglass;
				lblDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
				lblTime.Text = Strings.Format(DateAndTime.TimeOfDay, "hh:mm tt");
				lblMuniName.Text = modGlobalConstants.Statics.MuniName;
				dblPrincipal = 0;
				dblTax = 0;
				dblInterest = 0;
				dblPLI = 0;
				dblCost = 0;
				dblTotalPrincipal = 0;
				dblTotalTax = 0;
				dblTotalInterest = 0;
				dblTotalPLI = 0;
				dblTotalCost = 0;
				dblSRegPrincipal = 0;
				dblSRegTax = 0;
				dblSRegInterest = 0;
				dblSRegCost = 0;
				dblSLienPrincipal = 0;
				dblSLienTax = 0;
				dblSLienInterest = 0;
				dblSLienPLI = 0;
				dblSLienCost = 0;
				dblWRegPrincipal = 0;
				dblWRegTax = 0;
				dblWRegInterest = 0;
				dblWRegCost = 0;
				dblWLienPrincipal = 0;
				dblWLienTax = 0;
				dblWLienInterest = 0;
				dblWLienPLI = 0;
				dblWLienCost = 0;
				//frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
				strSQL = BuildSQL();
				rsData.OpenRecordset(strSQL, modExtraModules.strUTDatabase);
				if (rsData.EndOfFile())
				{
					// No records error
					//frmWait.InstancePtr.Unload();
					//MessageBox.Show("There are no records matching the criteria selected.", "Status List", MessageBoxButtons.OK, MessageBoxIcon.Information, modal:false);
					MessageBox.Show("There are no records matching the criteria selected.", "Status List", MessageBoxButtons.OK, MessageBoxIcon.Information);
					//FC:FINAL:RPU: #i1151 - Don't unload the report here, Cancet it instead and set the flag unloadReport to be true
					//this.Unload();
					this.Cancel();
					unloadReport = true;
				}
				else
				{
					//frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Loading");
				}
				return;
			}
			catch (Exception ex)
			{
				
				//frmWait.InstancePtr.Unload();
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error In Report Start", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
		}

		private void BindFields()
		{
			bool boolLien = false;
			bool boolWater = false;
			// this will put the information into the fields
			//frmWait.InstancePtr.IncrementProgress();
			if (!rsData.EndOfFile())
			{
				// Date
				fldDate.Text = Strings.Format(rsData.Get_Fields_DateTime("RecordedTransactionDate"), "MM/dd/yyyy");
				// Account Number
				fldAcct.Text = FCConvert.ToString(rsData.Get_Fields_Int32("ActualAccountNumber"));
				// Bill Number
				fldBill.Text = FCConvert.ToString(rsData.Get_Fields_Int32("BillNumber"));
				if (FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Lien")))
				{
					fldBill.Text = fldBill.Text + "*";
				}
				boolLien = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Lien"));
				// Service
				fldService.Text = rsData.Get_Fields_String("Service");
				boolWater = FCConvert.CBool(rsData.Get_Fields_String("Service") == "W");
				// Code
				// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
				fldCode.Text = FCConvert.ToString(rsData.Get_Fields("Code"));
				// Ref
				fldRef.Text = rsData.Get_Fields_String("Reference");
				// Receipt
				if (!rsData.IsFieldNull("ReceiptNumber") && Conversion.Val(rsData.Get_Fields_Int32("ReceiptNumber")) != 0)
				{
					fldRcpt.Text = GetActualReceiptNumber_8(rsData.Get_Fields_Int32("ReceiptNumber"), rsData.Get_Fields_DateTime("ActualSystemDate"));
				}
				else
				{
					fldRcpt.Text = "";
				}
				// Payment
				// TODO Get_Fields: Check the table for the column [Principal] and replace with corresponding Get_Field method
				dblPrincipal = FCConvert.ToDouble(rsData.Get_Fields("Principal"));
				// TODO Get_Fields: Check the table for the column [Tax] and replace with corresponding Get_Field method
				dblTax = FCConvert.ToDouble(rsData.Get_Fields("Tax"));
				dblInterest = FCConvert.ToDouble(rsData.Get_Fields_Decimal("CurrentInterest"));
				dblPLI = FCConvert.ToDouble(rsData.Get_Fields_Decimal("PreLienInterest"));
				dblCost = FCConvert.ToDouble(rsData.Get_Fields_Decimal("LienCost"));
				// show the payment values
				fldPrincipal.Text = Strings.Format(dblPrincipal, "#,##0.00");
				fldTax.Text = Strings.Format(dblTax, "#,##0.00");
				fldInterest.Text = Strings.Format(dblInterest, "#,##0.00");
				fldPLI.Text = Strings.Format(dblPLI, "#,##0.00");
				fldCost.Text = Strings.Format(dblCost, "#,##0.00");
				fldTotal.Text = Strings.Format(dblPrincipal + dblTax + dblInterest + dblPLI + dblCost, "#,##0.00");
				// update the running totala
				if (!boolLien)
				{
					if (boolWater)
					{
						boolShowWReg = true;
						dblWRegPrincipal += dblPrincipal;
						dblWRegTax += dblTax;
						dblWRegInterest += dblInterest;
						// dblwregPLI = dblwregPLI + dblPLI                  'shouldn't be any
						dblWRegCost += dblCost;
					}
					else
					{
						boolShowSReg = true;
						dblSRegPrincipal += dblPrincipal;
						dblSRegTax += dblTax;
						dblSRegInterest += dblInterest;
						// dblSRegPLI = dblSRegPLI + dblPLI
						dblSRegCost += dblCost;
					}
				}
				else
				{
					if (boolWater)
					{
						boolShowWLien = true;
						dblWLienPrincipal += dblPrincipal;
						dblWLienTax += dblTax;
						dblWLienInterest += dblInterest;
						dblWLienPLI += dblPLI;
						// shouldn't be any
						dblWLienCost += dblCost;
					}
					else
					{
						boolShowSLien = true;
						dblSLienPrincipal += dblPrincipal;
						dblSLienTax += dblTax;
						dblSLienInterest += dblInterest;
						dblSLienPLI += dblPLI;
						dblSLienCost += dblCost;
					}
				}
				dblTotalPrincipal += dblPrincipal;
				dblTotalTax += dblTax;
				dblTotalInterest += dblInterest;
				dblTotalPLI += dblPLI;
				dblTotalCost += dblCost;
				rsData.MoveNext();
			}
			else
			{
				fldDate.Visible = false;
				fldCode.Visible = false;
				fldRef.Visible = false;
				fldPrincipal.Visible = false;
				fldTax.Visible = false;
				fldInterest.Visible = false;
				fldPLI.Visible = false;
				fldCost.Visible = false;
				fldTotal.Visible = false;
				Detail.Height = 0;
			}
			//Application.DoEvents();
		}

		private string BuildSQL()
		{
			string BuildSQL = "";
			string strTemp;
			string strAccts;
			string strDates;
			string strSrvc = "";
			string strBills;
			string strCodes;
			string[] arrCodes = null;
			string strRef;
			string strTown;
			string strTownBillJoin;
			string strTownLienJoin;
			string strWhere;
			string strOrder = "";
			string strQR;
			string strQSL;
			string strQWL;
			int i;
			strWhere = "";
			// Account Range
			strAccts = "";
			strTemp = "";
			if (lngStartAcct != 0 || lngEndAcct != 0)
			{
				if (lngStartAcct == lngEndAcct)
				{
					strAccts = "ActualAccountNumber = " + FCConvert.ToString(lngStartAcct);
				}
				else
				{
					if (lngStartAcct != 0)
					{
						strTemp = "ActualAccountNumber >= " + FCConvert.ToString(lngStartAcct);
					}
					if (lngEndAcct != 0)
					{
						if (lngStartAcct == 0)
						{
							strTemp = "ActualAccountNumber <= " + FCConvert.ToString(lngEndAcct);
						}
						else
						{
							strTemp = "(" + strTemp + " AND ActualAccountNumber <= " + FCConvert.ToString(lngEndAcct) + ")";
						}
					}
					strAccts = strTemp;
				}
			}
			// Bill Range
			strBills = "";
			strTemp = "";
			if (lngStartBill != 0 || lngEndBill != 0)
			{
				if (lngStartBill == lngEndBill)
				{
					strBills = "BillNumber = " + FCConvert.ToString(lngStartBill);
				}
				else
				{
					if (lngStartBill != 0)
					{
						strTemp = "BillNumber >= " + FCConvert.ToString(lngStartBill);
					}
					if (lngEndBill != 0)
					{
						if (lngStartBill == 0)
						{
							strTemp = "BillNumber <= " + FCConvert.ToString(lngEndBill);
						}
						else
						{
							strTemp = "(" + strTemp + " AND BillNumber <= " + FCConvert.ToString(lngEndBill) + ")";
						}
					}
					strBills = strTemp;
				}
			}
			if (strAccts != "" || strBills != "")
			{
				strWhere = " WHERE";
				strTemp = "";
				if (strAccts != "")
				{
					strTemp = strAccts;
				}
				if (strBills != "")
				{
					if (strTemp != "")
					{
						strTemp += " AND " + strBills;
					}
					else
					{
						strTemp = strBills;
					}
				}
				strWhere = " WHERE " + strTemp;
			}
			// Date Range
			strDates = "";
			strTemp = "";
			if (dtStartDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() || dtEndDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
			{
				if (dtStartDate.ToOADate() == dtEndDate.ToOADate())
				{
					strDates = " AND RecordedTransactionDate = '" + Strings.Format(dtStartDate, "MM/dd/yyyy") + "'";
				}
				else
				{
					if (dtStartDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
					{
						strTemp = "RecordedTransactionDate >= '" + Strings.Format(dtStartDate, "MM/dd/yyyy") + "'";
					}
					if (dtEndDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
					{
						if (dtStartDate.ToOADate() == DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
						{
							strTemp = "RecordedTransactionDate <= '" + Strings.Format(dtEndDate, "MM/dd/yyyy") + "'";
						}
						else
						{
							strTemp = "(" + strTemp + " AND RecordedTransactionDate <= '" + Strings.Format(dtEndDate, "MM/dd/yyyy") + "')";
						}
					}
					strDates = " AND " + strTemp;
				}
			}
			// Service
			if (strService != "B" && strService != "")
			{
				strSrvc = " AND Service = '" + strService + "'";
			}
			// Payment Codes
			strCodes = "";
			strTemp = "";
			if (strPmtTypes != "")
			{
				if (strPmtTypes.Length == 1)
				{
					if (ValidPmtCode(strPmtTypes))
					{
						strCodes = " AND Code = '" + strPmtTypes + "'";
					}
				}
				else
				{
					arrCodes = Strings.Split(strPmtTypes, ",", -1, CompareConstants.vbBinaryCompare);
					for (i = 0; i <= Information.UBound(arrCodes, 1); i++)
					{
						if (strTemp != "")
						{
							strTemp += ",";
						}
						strTemp += "'" + Strings.Trim(arrCodes[i]) + "'";
					}
					strCodes = " AND Code IN (" + strTemp + ")";
				}
			}
			// Refernce Filter
			strRef = "";
			if (strRefFilter != "")
			{
				strRef = " AND Reference = '" + strRefFilter + "'";
			}
			// Town Code
			strTown = "";
			strTownBillJoin = "";
			strTownLienJoin = "";
			// If lngTownCode <> 0 Then
			// strTown = " AND TranCode = " & lngTownCode
			// strTownBillJoin = "INNER JOIN (SELECT Bill,TranCode FROM Bill) AS qTmp ON PaymentRec.BillKey = qTmp.BillKey "
			// strTownLienJoin = "INNER JOIN (SELECT Bill.LienRecordNumber AS BillKey,TranCode FROM BillingMaster INNER JOIN LienRec ON BillingMaster.LienRecordNumber = LienRec.LienRecordNumber) AS qTmp ON PaymentRec.BillKey = qTmp.BillKey "
			// End If
			// Query for Payments to Regular Bills
			strQR = "SELECT qR1.*,ActualAccountNumber,Bill.BillNumber FROM " + "(SELECT RecordedTransactionDate,ActualSystemDate,Teller,Reference,Code,ReceiptNumber,Principal,PreLienInterest,CurrentInterest,LienCost,Tax,CashDrawer,GeneralLedger,Service,Lien,BillKey " + "FROM PaymentRec WHERE ReceiptNumber > -1 AND ISNULL(Lien, 0) = 0" + strCodes + strSrvc + strDates + strRef + ") AS qR1 INNER JOIN Bill ON qR1.BillKey = Bill.ID" + strWhere;
			// Query for Payments to Sewer Liens
			strQSL = "SELECT qS1.*,ActualAccountNumber,BillNumber FROM " + "(SELECT RecordedTransactionDate,ActualSystemDate,Teller,Reference,Code,ReceiptNumber,Principal,PreLienInterest,CurrentInterest,LienCost,Tax,CashDrawer,GeneralLedger,Service,Lien,BillKey " + "FROM PaymentRec WHERE ISNULL(Lien,0) = 1 AND Service = 'S'" + strCodes + strDates + strRef + ") AS qS1 " + "INNER JOIN " + "(SELECT ActualAccountNumber,ID,RateKey AS BillNumber FROM " + "(SELECT ActualAccountNumber,SLienRecordNumber FROM Bill WHERE SLienRecordNumber <> 0 AND SCombinationLienKey = ID) AS qS3 INNER JOIN Lien ON qS3.SLienRecordNumber = Lien.ID) AS qS2 " + "ON qS1.BillKey = qS2.ID" + strWhere;
			// Query for Payments to Water Liens
			strQWL = "SELECT qW1.*,ActualAccountNumber,BillNumber FROM " + "(SELECT RecordedTransactionDate,ActualSystemDate,Teller,Reference,Code,ReceiptNumber,Principal,PreLienInterest,CurrentInterest,LienCost,Tax,CashDrawer,GeneralLedger,Service,Lien,BillKey " + "FROM PaymentRec WHERE ISNULL(Lien,0) = 1 AND Service = 'W'" + strCodes + strDates + strRef + ") AS qW1 " + "INNER JOIN " + "(SELECT ActualAccountNumber,ID,RateKey AS BillNumber FROM " + "(SELECT ActualAccountNumber,WLienRecordNumber FROM Bill WHERE WLienRecordNumber <> 0 AND WCombinationLienKey = ID) AS qW3 INNER JOIN Lien ON qW3.WLienRecordNumber = Lien.ID) AS qW2 " + "ON qW1.BillKey = qW2.ID" + strWhere;
			// Sort Order
			if (strSortOrder != "")
			{
				strOrder = " ORDER BY " + strSortOrder;
			}
			else
			{
				strOrder = " ORDER BY RecordedTransactionDate";
			}
			if (strBillType == "R")
			{
				BuildSQL = strQR + strOrder;
			}
			else
			{
				if (strService == "S")
				{
					strTemp = strQSL;
				}
				else if (strService == "W")
				{
					strTemp = strQWL;
				}
				else
				{
					strTemp = strQSL + " UNION " + strQWL;
				}
				if (strBillType == "L")
				{
					BuildSQL = strTemp + strOrder;
				}
				else
				{
					BuildSQL = strQR + " UNION " + strTemp + strOrder;
				}
			}
			return BuildSQL;
		}

		private bool ValidPmtCode(string strCode)
		{
			bool ValidPmtCode = false;
			// Return True if the string is a valid payment type code
			bool boolRet;
			boolRet = false;
			if (strCode.Length == 1)
			{
				boolRet = FCConvert.CBool(Strings.InStr("PCY3AILUXSDR", strCode, CompareConstants.vbBinaryCompare) != 0);
			}
			ValidPmtCode = boolRet;
			return ValidPmtCode;
		}
		// vbPorter upgrade warning: 'Return' As string	OnWrite(string, int)
		private string GetActualReceiptNumber_8(int lngRN, DateTime dtDate)
		{
			return GetActualReceiptNumber(ref lngRN, ref dtDate);
		}

		private string GetActualReceiptNumber(ref int lngRN, ref DateTime dtDate)
		{
			string GetActualReceiptNumber = "";
			try
			{
				// On Error GoTo ERROR_HANDLER
				// this function will accept a long of a receipt number key in the CR table
				// if the receipt does not exist or any other error occurs, the function will
				// return 0, otherwise it returns the receipt number shown on the receipt
				if (lngRN > 0)
				{
					if (modGlobalConstants.Statics.gboolCR)
					{
						Statics.rsRN.OpenRecordset("SELECT ReceiptNumber FROM Receipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND Cast(ReceiptDate AS Date) = '" + Strings.Format(dtDate, "MM/dd/yyyy") + "'", modExtraModules.strCRDatabase);
						if (Statics.rsRN.EndOfFile() != true && Statics.rsRN.BeginningOfFile() != true)
						{
							GetActualReceiptNumber = FCConvert.ToString(Statics.rsRN.Get_Fields_Int32("ReceiptNumber"));
						}
						else
						{
							Statics.rsRN.OpenRecordset("SELECT ReceiptNumber FROM LastYearReceipt WHERE ReceiptKey = " + FCConvert.ToString(lngRN) + " AND Cast(LastYearReceiptDate AS Date) = '" + Strings.Format(dtDate, "MM/dd/yyyy") + "'", modExtraModules.strCRDatabase);
							if (Statics.rsRN.EndOfFile() != true && Statics.rsRN.BeginningOfFile() != true)
							{
								GetActualReceiptNumber = FCConvert.ToString(Statics.rsRN.Get_Fields_Int32("ReceiptNumber"));
							}
							else
							{
								GetActualReceiptNumber = FCConvert.ToString(lngRN) + "*";
							}
						}
					}
					else
					{
						GetActualReceiptNumber = FCConvert.ToString(lngRN);
					}
				}
				else
				{
					GetActualReceiptNumber = FCConvert.ToString(0);
				}
				return GetActualReceiptNumber;
			}
			catch (Exception ex)
			{
				
				MessageBox.Show("Error #" + FCConvert.ToString(Information.Err(ex).Number) + " - " + Information.Err(ex).Description + ".", "Error Returning Receipt Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
			}
			return GetActualReceiptNumber;
		}

		private void ActiveReport_Terminate(object sender, EventArgs e)
		{
			//FC: FINAL: RPU:#i1151 - Unload the report if needed
			if (unloadReport)
			{
				unloadReport = false;
				this.Unload();
			}
			FCGlobal.Screen.MousePointer = MousePointerConstants.vbArrow;
			//frmWait.InstancePtr.Unload();
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			BindFields();
		}

		private void ReportFooter_Format(object sender, EventArgs e)
		{
			float lngLastY;
			float lngDelta;
			fldTotalPrincipal.Text = Strings.Format(dblTotalPrincipal, "#,##0.00");
			fldTotalTax.Text = Strings.Format(dblTotalTax, "#,##0.00");
			fldTotalInterest.Text = Strings.Format(dblTotalInterest, "#,##0.00");
			fldTotalPLI.Text = Strings.Format(dblTotalPLI, "#,##0.00");
			fldTotalCost.Text = Strings.Format(dblTotalCost, "#,##0.00");
			fldTotalTotal.Text = Strings.Format(dblTotalPrincipal + dblTotalTax + dblTotalInterest + dblTotalPLI + dblTotalCost, "#,##0.00");
			lblSRegTotal.Visible = boolShowSReg;
			fldSRegPrincipal.Visible = boolShowSReg;
			fldSRegTax.Visible = boolShowSReg;
			fldSRegInterest.Visible = boolShowSReg;
			fldSRegCost.Visible = boolShowSReg;
			fldSRegTotal.Visible = boolShowSReg;
			lblWRegTotal.Visible = boolShowWReg;
			fldWRegPrincipal.Visible = boolShowWReg;
			fldWRegTax.Visible = boolShowWReg;
			fldWRegInterest.Visible = boolShowWReg;
			fldWRegCost.Visible = boolShowWReg;
			fldWRegTotal.Visible = boolShowWReg;
			lblSLienTotal.Visible = boolShowSLien;
			fldSLienPrincipal.Visible = boolShowSLien;
			fldSLienTax.Visible = boolShowSLien;
			fldSLienInterest.Visible = boolShowSLien;
			fldSLienPLI.Visible = boolShowSLien;
			fldSLienCost.Visible = boolShowSLien;
			fldSLienTotal.Visible = boolShowSLien;
			lblWLienTotal.Visible = boolShowWLien;
			fldWLienPrincipal.Visible = boolShowWLien;
			fldWLienTax.Visible = boolShowWLien;
			fldWLienInterest.Visible = boolShowWLien;
			fldWLienPLI.Visible = boolShowWLien;
			fldWLienCost.Visible = boolShowWLien;
			fldWLienTotal.Visible = boolShowWLien;
			lngLastY = lblSRegTotal.Top;
			lngDelta = lblWRegTotal.Top - lblSRegTotal.Top;
			if (boolShowSReg)
			{
				lngLastY += lngDelta;
				fldSRegPrincipal.Text = Strings.Format(dblSRegPrincipal, "#,##0.00");
				fldSRegTax.Text = Strings.Format(dblSRegTax, "#,##0.00");
				fldSRegInterest.Text = Strings.Format(dblSRegInterest, "#,##0.00");
				fldSRegCost.Text = Strings.Format(dblSRegCost, "#,##0.00");
				fldSRegTotal.Text = Strings.Format(dblSRegPrincipal + dblSRegTax + dblSRegInterest + dblSRegCost, "#,##0.00");
			}
			if (boolShowWReg)
			{
				lblWRegTotal.Top = lngLastY;
				fldWRegPrincipal.Top = lngLastY;
				fldWRegTax.Top = lngLastY;
				fldWRegInterest.Top = lngLastY;
				fldWRegCost.Top = lngLastY;
				fldWRegTotal.Top = lngLastY;
				lngLastY += lngDelta;
				fldWRegPrincipal.Text = Strings.Format(dblWRegPrincipal, "#,##0.00");
				fldWRegTax.Text = Strings.Format(dblWRegTax, "#,##0.00");
				fldWRegInterest.Text = Strings.Format(dblWRegInterest, "#,##0.00");
				fldWRegCost.Text = Strings.Format(dblWRegCost, "#,##0.00");
				fldWRegTotal.Text = Strings.Format(dblWRegPrincipal + dblWRegTax + dblWRegInterest + dblWRegCost, "#,##0.00");
				lngLastY += lblSRegTotal.Height;
			}
			if (boolShowSLien)
			{
				lblSLienTotal.Top = lngLastY;
				fldSLienPrincipal.Top = lngLastY;
				fldSLienTax.Top = lngLastY;
				fldSLienInterest.Top = lngLastY;
				fldSLienPLI.Top = lngLastY;
				fldSLienCost.Top = lngLastY;
				fldSLienTotal.Top = lngLastY;
				lngLastY += lngDelta;
				fldSLienPrincipal.Text = Strings.Format(dblSLienPrincipal, "#,##0.00");
				fldSLienTax.Text = Strings.Format(dblSLienTax, "#,##0.00");
				fldSLienInterest.Text = Strings.Format(dblSLienInterest, "#,##0.00");
				fldSLienPLI.Text = Strings.Format(dblSLienPLI, "#,##0.00");
				fldSLienCost.Text = Strings.Format(dblSLienCost, "#,##0.00");
				fldSLienTotal.Text = Strings.Format(dblSLienPrincipal + dblWLienTax + dblSLienInterest + dblSLienPLI + dblSLienCost, "#,##0.00");
			}
			if (boolShowWLien)
			{
				lblWLienTotal.Top = lngLastY;
				fldWLienPrincipal.Top = lngLastY;
				fldWLienTax.Top = lngLastY;
				fldWLienInterest.Top = lngLastY;
				fldWLienPLI.Top = lngLastY;
				fldWLienCost.Top = lngLastY;
				fldWLienTotal.Top = lngLastY;
				lngLastY += lngDelta;
				fldWLienPrincipal.Text = Strings.Format(dblWLienPrincipal, "#,##0.00");
				fldWLienTax.Text = Strings.Format(dblWLienTax, "#,##0.00");
				fldWLienInterest.Text = Strings.Format(dblWLienInterest, "#,##0.00");
				fldWLienPLI.Text = Strings.Format(dblWLienPLI, "#,##0.0");
				fldWLienCost.Text = Strings.Format(dblWLienCost, "#,##0.00");
				fldWLienTotal.Text = Strings.Format(dblWLienPrincipal + dblWLienTax + dblWLienInterest + dblWLienPLI + dblWLienCost, "#,##0.00");
			}
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			string strTemp = "";
			// Dim rsTemp  As New clsDRWrapper
			if (this.PageNumber == 1)
			{
				if (modGlobalConstants.Statics.gboolCR)
				{
					fldHeaderRcpt.Visible = true;
				}
				else
				{
					fldHeaderRcpt.Visible = false;
				}
				// Service
				if (strService != "" && modUTStatusPayments.Statics.TownService == "B")
				{
					if (strService == "S")
					{
						strTemp += "Sewer; ";
					}
					else if (strService == "W")
					{
						strTemp += "Water; ";
					}
				}
				// Account Number
				if (lngStartAcct != 0 || lngEndAcct != 0)
				{
					if (lngStartAcct == lngEndAcct)
					{
						strTemp += "Account " + FCConvert.ToString(lngStartAcct) + "; ";
					}
					else if (lngStartAcct != 0 && lngEndAcct != 0)
					{
						strTemp += "Accounts " + FCConvert.ToString(lngStartAcct) + " To " + FCConvert.ToString(lngEndAcct) + "; ";
					}
					else if (lngEndAcct != 0)
					{
						strTemp += "Accounts Below " + FCConvert.ToString(lngEndAcct) + "; ";
					}
					else
					{
						strTemp += "Accounts Above " + FCConvert.ToString(lngStartAcct) + "; ";
					}
				}
				// Name   -  Not implemented
				// If strStartName <> "" Or strEndName <> "" Then
				// If strStartName = strEndName Then
				// strTemp = strTemp & "Name: " & strStartName & " "
				// ElseIf strStartName <> "" And strEndName <> "" Then
				// strTemp = strTemp & "Name: " & strStartName & " To " & strEndName & " "
				// ElseIf strEndName <> "" Then
				// strTemp = strTemp & "Below Name: " & strEndName & " "
				// Else
				// strTemp = strTemp & "Above Name: " & strStartName & " "
				// End If
				// End If
				// Bill Range
				if (lngStartBill != 0 || lngEndBill != 0)
				{
					if (lngStartBill == lngEndBill)
					{
						strTemp += "Bill " + FCConvert.ToString(lngStartBill) + "; ";
					}
					else if (lngStartBill != 0 && lngEndBill != 0)
					{
						strTemp += "Bills " + FCConvert.ToString(lngStartBill) + " To " + FCConvert.ToString(lngEndBill) + "; ";
					}
					else if (lngEndBill != 0)
					{
						strTemp += "Bills Below " + FCConvert.ToString(lngEndBill) + "; ";
					}
					else
					{
						strTemp += "Bills Above " + FCConvert.ToString(lngStartBill) + "; ";
					}
				}
				// Date Range
				if (dtStartDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() || dtEndDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
				{
					if (dtStartDate.ToOADate() == dtEndDate.ToOADate())
					{
						strTemp += "On " + Strings.Format(dtStartDate, "MM/dd/yyyy") + "; ";
					}
					else if (dtStartDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate() && dtEndDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
					{
						strTemp += "From " + Strings.Format(dtStartDate, "MM/dd/yyyy") + " To " + Strings.Format(dtEndDate, "MM/dd/yyyy") + "; ";
					}
					else if (dtEndDate.ToOADate() != DateAndTime.DateValue(FCConvert.ToString(0)).ToOADate())
					{
						strTemp += "Before " + Strings.Format(dtEndDate, "MM/dd/yyyy") + "; ";
					}
					else
					{
						strTemp += "After " + Strings.Format(dtStartDate, "MM/dd/yyyy") + "; ";
					}
				}
				// Payment Types
				if (strPmtTypes != "")
				{
					// And strPmtTypes <> constAllPmtTypes Then
					strTemp += "Types " + strPmtTypes + "; ";
				}
				// Reference
				if (strRefFilter != "")
				{
					strTemp += "Ref " + strRefFilter + "; ";
				}
				// Town Code
				// If gboolMultipleTowns Then
				// If lngTownCode <> 0 Then
				// rsTemp.OpenRecordset "SELECT * FROM tblRegions WHERE TownNumber = " & lngTownCode, strGNDatabase
				// If rsTemp.RecordCount > 0 Then
				// strTemp = strTemp & "Town: " & rsTemp.Fields("TownName") & " "
				// End If
				// End If
				// End If
				lblReportType.Text = Strings.Trim(strTemp);
			}
		}

		private void rptUTPmtActivity_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//rptUTPmtActivity properties;
			//rptUTPmtActivity.Caption	= "Payment Activity";
			//rptUTPmtActivity.Icon	= "rptUTPmtActivity.dsx":0000";
			//rptUTPmtActivity.Left	= 0;
			//rptUTPmtActivity.Top	= 0;
			//rptUTPmtActivity.Width	= 28680;
			//rptUTPmtActivity.Height	= 15690;
			//rptUTPmtActivity.StartUpPosition	= 3;
			//rptUTPmtActivity.SectionData	= "rptUTPmtActivity.dsx":058A;
			//End Unmaped Properties
		}

		public class StaticVariables
		{
			/// </summary>
			/// Summary description for rptUTPmtActivity.
			/// <summary>
			//FC:FINAL:DDU: AutoInitialize clsDRWrapper when declared with as New in VB6
			//public clsDRWrapper rsRN = new clsDRWrapper();
			public clsDRWrapper rsRN_AutoInitialized = null;
			public clsDRWrapper rsRN
			{
				get
				{
					if ( rsRN_AutoInitialized == null)
					{
						 rsRN_AutoInitialized = new clsDRWrapper();
					}
					return rsRN_AutoInitialized;
				}
				set
				{
					 rsRN_AutoInitialized = value;
				}
			}
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
