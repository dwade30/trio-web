﻿//Fecher vbPorter - Version 1.0.0.40
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using Wisej.Web;
using modUTStatusPayments = Global.modUTFunctions;

namespace TWUT0000
{
	/// <summary>
	/// Summary description for frmEditBookPage.
	/// </summary>
	partial class frmEditBookPage : BaseForm
	{
		public fecherFoundation.FCComboBox cmbWS;
		public fecherFoundation.FCLabel lblWS;
		public fecherFoundation.FCComboBox cmbYear;
		public fecherFoundation.FCTextBox txtAccount;
		public fecherFoundation.FCTextBox txtPage;
		public fecherFoundation.FCTextBox txtBook;
		public fecherFoundation.FCLabel lblName;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCLabel lblPage;
		public fecherFoundation.FCLabel lblBook;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileGetAccount;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuFileSave;
		public fecherFoundation.FCToolStripMenuItem mnuFileSaveExit;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuFileExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEditBookPage));
			this.cmbWS = new fecherFoundation.FCComboBox();
			this.lblWS = new fecherFoundation.FCLabel();
			this.cmbYear = new fecherFoundation.FCComboBox();
			this.txtAccount = new fecherFoundation.FCTextBox();
			this.txtPage = new fecherFoundation.FCTextBox();
			this.txtBook = new fecherFoundation.FCTextBox();
			this.lblName = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.Label1 = new fecherFoundation.FCLabel();
			this.lblPage = new fecherFoundation.FCLabel();
			this.lblBook = new fecherFoundation.FCLabel();
			this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileGetAccount = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSave = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileSaveExit = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuFileExit = new fecherFoundation.FCToolStripMenuItem();
			this.cmdSave = new fecherFoundation.FCButton();
			this.cmdFileGetAccount = new fecherFoundation.FCButton();
			this.BottomPanel.SuspendLayout();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileGetAccount)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 319);
			this.BottomPanel.Size = new System.Drawing.Size(681, 108);
            // 
            // ClientArea
            // 

            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.cmbWS);
			this.ClientArea.Controls.Add(this.lblWS);
			this.ClientArea.Controls.Add(this.cmbYear);
			this.ClientArea.Controls.Add(this.txtAccount);
			this.ClientArea.Controls.Add(this.txtPage);
			this.ClientArea.Controls.Add(this.txtBook);
			this.ClientArea.Controls.Add(this.lblName);
			this.ClientArea.Controls.Add(this.Label2);
			this.ClientArea.Controls.Add(this.Label1);
			this.ClientArea.Controls.Add(this.lblPage);
			this.ClientArea.Controls.Add(this.lblBook);
			this.ClientArea.Size = new System.Drawing.Size(681, 259);
			// 
			// TopPanel
			// 
			this.TopPanel.Controls.Add(this.cmdFileGetAccount);
			this.TopPanel.Size = new System.Drawing.Size(681, 60);
			this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
			this.TopPanel.Controls.SetChildIndex(this.cmdFileGetAccount, 0);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(181, 30);
			this.HeaderText.Text = "Edit Book Page";
			// 
			// cmbWS
			// 
			this.cmbWS.AutoSize = false;
			this.cmbWS.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbWS.FormattingEnabled = true;
			this.cmbWS.Items.AddRange(new object[] {
				"Water",
				"Sewer"
			});
			this.cmbWS.Location = new System.Drawing.Point(152, 30);
			this.cmbWS.Name = "cmbWS";
			this.cmbWS.Size = new System.Drawing.Size(180, 40);
			this.cmbWS.TabIndex = 2;
			this.cmbWS.Text = "Water";
			this.cmbWS.SelectedIndexChanged += new System.EventHandler(this.optWS_CheckedChanged);
			// 
			// lblWS
			// 
			this.lblWS.AutoSize = true;
			this.lblWS.Location = new System.Drawing.Point(30, 44);
			this.lblWS.Name = "lblWS";
			this.lblWS.Size = new System.Drawing.Size(107, 15);
			this.lblWS.TabIndex = 1;
			this.lblWS.Text = "WATER / SEWER";
			// 
			// cmbYear
			// 
			this.cmbYear.AutoSize = false;
			this.cmbYear.BackColor = System.Drawing.SystemColors.Window;
			this.cmbYear.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cmbYear.FormattingEnabled = true;
			this.cmbYear.Location = new System.Drawing.Point(152, 196);
			this.cmbYear.Name = "cmbYear";
			this.cmbYear.Size = new System.Drawing.Size(180, 40);
			this.cmbYear.TabIndex = 6;
			this.cmbYear.SelectedIndexChanged += new System.EventHandler(this.cmbYear_SelectedIndexChanged);
			this.cmbYear.DropDown += new System.EventHandler(this.cmbYear_DropDown);
			// 
			// txtAccount
			// 
			this.txtAccount.AutoSize = false;
			this.txtAccount.BackColor = System.Drawing.SystemColors.Window;
			this.txtAccount.LinkItem = null;
			this.txtAccount.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtAccount.LinkTopic = null;
			this.txtAccount.Location = new System.Drawing.Point(152, 126);
			this.txtAccount.Name = "txtAccount";
			this.txtAccount.Size = new System.Drawing.Size(180, 40);
			this.txtAccount.TabIndex = 4;
			this.txtAccount.KeyDown += new Wisej.Web.KeyEventHandler(this.txtAccount_KeyDown);
			this.txtAccount.Enter += new System.EventHandler(this.txtAccount_Enter);
			this.txtAccount.Validating += new System.ComponentModel.CancelEventHandler(this.txtAccount_Validating);
			// 
			// txtPage
			// 
			this.txtPage.MaxLength = 6;
			this.txtPage.AutoSize = false;
			this.txtPage.BackColor = System.Drawing.SystemColors.Window;
			this.txtPage.LinkItem = null;
			this.txtPage.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtPage.LinkTopic = null;
			this.txtPage.Location = new System.Drawing.Point(543, 196);
			this.txtPage.Name = "txtPage";
			this.txtPage.Size = new System.Drawing.Size(79, 40);
			this.txtPage.TabIndex = 10;
			this.txtPage.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtPage_KeyPress);
			this.txtPage.Enter += new System.EventHandler(this.txtPage_Enter);
			// 
			// txtBook
			// 
			this.txtBook.MaxLength = 6;
			this.txtBook.AutoSize = false;
			this.txtBook.BackColor = System.Drawing.SystemColors.Window;
			this.txtBook.LinkItem = null;
			this.txtBook.LinkMode = fecherFoundation.FCForm.LinkModeConstants.VbLinkNone;
			this.txtBook.LinkTopic = null;
			this.txtBook.Location = new System.Drawing.Point(543, 126);
			this.txtBook.Name = "txtBook";
			this.txtBook.Size = new System.Drawing.Size(79, 40);
			this.txtBook.TabIndex = 8;
			this.txtBook.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtBook_KeyPress);
			this.txtBook.Enter += new System.EventHandler(this.txtBook_Enter);
			// 
			// lblName
			// 
			this.lblName.Location = new System.Drawing.Point(30, 90);
			this.lblName.Name = "lblName";
			this.lblName.Size = new System.Drawing.Size(302, 16);
			this.lblName.TabIndex = 0;
			this.lblName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// Label2
			// 
			this.Label2.AutoSize = true;
			this.Label2.Location = new System.Drawing.Point(30, 140);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(69, 15);
			this.Label2.TabIndex = 3;
			this.Label2.Text = "ACCOUNT";
			// 
			// Label1
			// 
			this.Label1.AutoSize = true;
			this.Label1.Location = new System.Drawing.Point(30, 210);
			this.Label1.Name = "Label1";
			this.Label1.Size = new System.Drawing.Size(33, 15);
			this.Label1.TabIndex = 5;
			this.Label1.Text = "BILL";
			// 
			// lblPage
			// 
			this.lblPage.AutoSize = true;
			this.lblPage.Location = new System.Drawing.Point(452, 210);
			this.lblPage.Name = "lblPage";
			this.lblPage.Size = new System.Drawing.Size(42, 15);
			this.lblPage.TabIndex = 9;
			this.lblPage.Text = "PAGE";
			// 
			// lblBook
			// 
			this.lblBook.AutoSize = true;
			this.lblBook.Location = new System.Drawing.Point(452, 140);
			this.lblBook.Name = "lblBook";
			this.lblBook.Size = new System.Drawing.Size(43, 15);
			this.lblBook.TabIndex = 7;
			this.lblBook.Text = "BOOK";
			// 
			// mnuFile
			// 
			this.mnuFile.Index = -1;
			this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuFileGetAccount,
				this.mnuFileSeperator2,
				this.mnuFileSave,
				this.mnuFileSaveExit,
				this.Seperator,
				this.mnuFileExit
			});
			this.mnuFile.Name = "mnuFile";
			this.mnuFile.Text = "File";
			// 
			// mnuFileGetAccount
			// 
			this.mnuFileGetAccount.Index = 0;
			this.mnuFileGetAccount.Name = "mnuFileGetAccount";
			this.mnuFileGetAccount.Text = "Get Lien Account";
			this.mnuFileGetAccount.Click += new System.EventHandler(this.mnuFileGetAccount_Click);
			// 
			// mnuFileSeperator2
			// 
			this.mnuFileSeperator2.Index = 1;
			this.mnuFileSeperator2.Name = "mnuFileSeperator2";
			this.mnuFileSeperator2.Text = "-";
			// 
			// mnuFileSave
			// 
			this.mnuFileSave.Index = 2;
			this.mnuFileSave.Name = "mnuFileSave";
			this.mnuFileSave.Shortcut = Wisej.Web.Shortcut.F11;
			this.mnuFileSave.Text = "Save";
			this.mnuFileSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// mnuFileSaveExit
			// 
			this.mnuFileSaveExit.Index = 3;
			this.mnuFileSaveExit.Name = "mnuFileSaveExit";
			this.mnuFileSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuFileSaveExit.Text = "Save & Exit";
			this.mnuFileSaveExit.Click += new System.EventHandler(this.mnuFileSaveExit_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 4;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuFileExit
			// 
			this.mnuFileExit.Index = 5;
			this.mnuFileExit.Name = "mnuFileExit";
			this.mnuFileExit.Text = "Exit";
			this.mnuFileExit.Click += new System.EventHandler(this.mnuFileExit_Click);
			// 
			// cmdSave
			// 
			this.cmdSave.AppearanceKey = "acceptButton";
			this.cmdSave.Location = new System.Drawing.Point(30, 270);
			this.cmdSave.Name = "cmdSave";
			this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.cmdSave.Size = new System.Drawing.Size(100, 48);
			this.cmdSave.TabIndex = 0;
			this.cmdSave.Text = "Save";
			this.cmdSave.Click += new System.EventHandler(this.mnuFileSave_Click);
			// 
			// cmdFileGetAccount
			// 
			this.cmdFileGetAccount.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
			this.cmdFileGetAccount.AppearanceKey = "toolbarButton";
			this.cmdFileGetAccount.Location = new System.Drawing.Point(531, 29);
			this.cmdFileGetAccount.Name = "cmdFileGetAccount";
			this.cmdFileGetAccount.Size = new System.Drawing.Size(122, 24);
			this.cmdFileGetAccount.TabIndex = 1;
			this.cmdFileGetAccount.Text = "Get Lien Account";
			this.cmdFileGetAccount.Click += new System.EventHandler(this.mnuFileGetAccount_Click);
			// 
			// frmEditBookPage
			// 
			this.BackColor = System.Drawing.Color.FromName("@window");
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(681, 427);
			this.FillColor = 0;
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Name = "frmEditBookPage";
			this.StartPosition = Wisej.Web.FormStartPosition.Manual;
			this.Text = "Edit Book Page";
			this.Load += new System.EventHandler(this.frmEditBookPage_Load);
			this.Activated += new System.EventHandler(this.frmEditBookPage_Activated);
			this.QueryUnload += new EventHandler<FCFormClosingEventArgs>(this.Form_QueryUnload);
			this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmEditBookPage_KeyDown);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmEditBookPage_KeyPress);
			this.BottomPanel.ResumeLayout(false);
			this.ClientArea.ResumeLayout(false);
			this.ClientArea.PerformLayout();
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.cmdFileGetAccount)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdSave;
		private FCButton cmdFileGetAccount;
	}
}
