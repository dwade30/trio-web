﻿using System;
using System.Collections.Generic;
using System.Linq;
using SharedApplication;
using SharedApplication.Payroll;

namespace TrioDataAPIConsumer.Payroll
{
    public class PayrollCloudApiConsumer : PayrollAPIBase, IPayrollCloudAPI
    {
        private cGlobalSettings globalSettings;

        //public PayrollCloudApiConsumer(string apiUri) : base(apiUri)
        //{

        //}

        public PayrollCloudApiConsumer(cGlobalSettings globalSettings) : base(globalSettings.TrioDataAPIUri)
        {
            this.globalSettings = globalSettings;
        }


        public IEnumerable<TaxTableGroup> GetTaxTableGroupsForYear(int year)
        {
           // throw new System.NotImplementedException();
            var parameters = new List<(string Name, string value)>();
            parameters.Add((Name: "taxyear", value: year.ToString()));
            var groupDtos = GetListWithParameters<TaxTableGroupDTO>("TaxGroupsForYear", parameters).ToList();
            return groupDtos.ToTaxTableGroups();
        }

        public IEnumerable<int> GetAvailableYears()
        {
            return GetList<int>("TaxYears").ToList();
        }

        //public IEnumerable<ILocalityTaxSetup> GetTaxSetupsForYear(int year)
        //{
        //    var setups = new List<ILocalityTaxSetup>();

        //    setups.Add(GetFederalTaxSetup());
        //    setups.Add(GetMaineSetup());
        //    return setups;
        //}



        //private IEnumerable<TaxTable> GetMaineTables()
        //{
        //    var tables = new List<TaxTable>();
        //    tables.Add(MakeStateMarried2Table());
        //    tables.Add(MakeMaineSingle());
        //    return tables;
        //}

        //private TaxTable MakeMaineSingle()
        //{
        //    var table = new TaxTable();
        //    table.Locality = new PayrollLocality("US","ME");
        //    table.LocalityType = PayrollLocalityType.State;
        //    table.TableName = "US_ME_Single";
        //    table.TableDescription = "Single";
        //    var entries = new List<TaxTableEntry>();

        //    entries.Add(MakeEntry(0,21850,.058M,0));
        //    entries.Add(MakeEntry(21850,51700,.0675M,1267));
        //    entries.Add(MakeEntry(51700,99999999,.0715M,3282));

        //    table.Entries = entries;
        //    return table;
        //}

        //private TaxTable MakeStateMarried2Table()
        //{
        //    var table = new TaxTable();
        //    table.LocalityType = PayrollLocalityType.State;
        //    table.Locality = new PayrollLocality("US","ME");
        //    table.TableName = "US_ME_Married";
        //    table.TableDescription = "Married";
        //    var entries = new List<TaxTableEntry>();

        //    entries.Add(MakeEntry(0,44450,.058M,0));
        //    entries.Add(MakeEntry(44450,105200,.0675M,2535));
        //    entries.Add(MakeEntry(105200,99999999,.0715M,6565));

        //    table.Entries = entries;
        //    return table;
        //}

     //private IEnumerable<TaxTable> GetFederalTables()
     //   {
     //       var tables = new List<TaxTable>();
     //       tables.Add(MakeHeadOfHouseHoldMultiTable());
     //       tables.Add(MakeHeadOfHouseholdTable());
     //       tables.Add(MakeFederalMarriedMulti());
     //       tables.Add(MakeFederalMarried());
     //       tables.Add(MakeFederalSingleMulti());
     //       tables.Add(MakeFederalSingle());
     //       return tables;
     //   }

     //   private TaxTable MakeFederalSingle()
     //   {
     //       var table = new TaxTable();
     //       table.LocalityType = PayrollLocalityType.Federal;
     //       table.Locality = new PayrollLocality("US");
     //       table.TableName = "US_Single";
     //       table.TableDescription = "Single";
     //       var entries = new List<TaxTableEntry>();

     //       entries.Add(MakeEntry(0,3800,0,0));
     //       entries.Add(MakeEntry(3800,13500,.10M,0));
     //       entries.Add(MakeEntry(13500,43275,.12M,970));
     //       entries.Add(MakeEntry(43275,88000,.22M,4543));
     //       entries.Add(MakeEntry(88000,164525,.24M,14382.5M));
     //       entries.Add(MakeEntry(164525,207900,.32M,32748.5M));
     //       entries.Add(MakeEntry(207900,514100,.35M,46628.5M));
     //       entries.Add(MakeEntry(514100,99999999,.37M,153798));

     //       table.Entries = entries;
     //       return table;
     //   }

     //   private TaxTable MakeFederalSingleMulti()
     //   {
     //       var table = new TaxTable();
     //       table.LocalityType = PayrollLocalityType.Federal;
     //       table.Locality = new PayrollLocality("US");
     //       table.TableName = "US_Single_MultiJobs";
     //       table.TableDescription = "Single multiple jobs";
     //       var entries = new List<TaxTableEntry>();

     //       entries.Add(MakeEntry(0,6200,0,0));
     //       entries.Add(MakeEntry(6200,11138,.10M,0));
     //       entries.Add(MakeEntry(11138,26263,.12M,493.75M));
     //       entries.Add(MakeEntry(26263,48963,.22M,2308.75M));
     //       entries.Add(MakeEntry(48963,87850,.24M,7302.75M));
     //       entries.Add(MakeEntry(87850,109875,.32M,16635.75M));
     //       entries.Add(MakeEntry(109875,265400,.35M,23683.75M));
     //       entries.Add(MakeEntry(265400,99999999,.37M,78117.5M));

     //       table.Entries = entries;
     //       return table;
     //   }

     //   private TaxTable MakeFederalMarried()
     //   {
     //       var table = new TaxTable();
     //       table.LocalityType = PayrollLocalityType.Federal;
     //       table.Locality = new PayrollLocality("US");
     //       table.TableName = "US_Married";
     //       table.TableDescription = "Married";
     //       var entries = new List<TaxTableEntry>();

     //       entries.Add(MakeEntry(0,11800,0,0));
     //       entries.Add(MakeEntry(11800,31200,.10M,0));
     //       entries.Add(MakeEntry(31200,90750,.12M,1940));
     //       entries.Add(MakeEntry(90750,180200,.22M,9086));
     //       entries.Add(MakeEntry(180200,333250,.24M,28765));
     //       entries.Add(MakeEntry(333250,420000,.32M,65497));
     //       entries.Add(MakeEntry(420000,624150,.35M,93257));
     //       entries.Add(MakeEntry(624150,99999999,.37M,164709.5M));

     //       table.Entries = entries;
     //       return table;
     //   }

     //   private TaxTable MakeFederalMarriedMulti()
     //   {
     //       var table = new TaxTable();
     //       table.LocalityType = PayrollLocalityType.Federal;
     //       table.Locality = new PayrollLocality("US");
     //       table.TableName = "US_Married_MultiJobs";
     //       table.TableDescription = "Married multiple jobs";
     //       var entries = new List<TaxTableEntry>();

     //       entries.Add(MakeEntry(0,12400,0,0));
     //       entries.Add(MakeEntry(12400,22275,.10M,0));
     //       entries.Add(MakeEntry(22275,52525,.12M,987.5M));
     //       entries.Add(MakeEntry(52525,97925,.22M,4617.5M));
     //       entries.Add(MakeEntry(97925,175700,.24M,14605.5M));
     //       entries.Add(MakeEntry(175700,219750,.32M,33271.5M));
     //       entries.Add(MakeEntry(219750,323425,.35M,47367.5M));
     //       entries.Add(MakeEntry(323425,99999999,.37M,83653.75M));

     //       table.Entries = entries;
     //       return table;
     //   }

     //   private TaxTable MakeHeadOfHouseholdTable()
     //   {
     //       var table = new TaxTable();
     //       table.LocalityType = PayrollLocalityType.Federal;
     //       table.Locality = new PayrollLocality("US");
     //       table.TableName = "US_HeadOfHousehold";
     //       table.TableDescription = "Head of household";
     //       var entries = new List<TaxTableEntry>();

     //       entries.Add(MakeEntry(0,10050,0,0));
     //       entries.Add(MakeEntry(10050,24150,.10M,0));
     //       entries.Add(MakeEntry(24150,63750,.12M,1410));
     //       entries.Add(MakeEntry(63750,95550,.22M,6162));
     //       entries.Add(MakeEntry(95550,173350,.24M,13158));
     //       entries.Add(MakeEntry(173350,217400,.32M,31830));
     //       entries.Add(MakeEntry(217400,528450,.35M,45926));
     //       entries.Add(MakeEntry(528450,99999999,.37M,154793.5M));

     //       table.Entries = entries;
     //       return table;
     //   }
     //   private TaxTable MakeHeadOfHouseHoldMultiTable()
     //   {
     //       var table = new TaxTable();
     //       table.LocalityType = PayrollLocalityType.Federal;
     //       table.Locality = new PayrollLocality("US");
     //       table.TableName = "US_HeadOfHouseholdMultiJobs";
     //       table.TableDescription = "Head of household multiple jobs";
     //       var entries = new List<TaxTableEntry>();
     //       entries.Add(MakeEntry(0,9325,0,0));
     //       entries.Add(MakeEntry(9325,16375,.10M,0));
     //       entries.Add(MakeEntry(16375,36175,.12M,705));
     //       entries.Add(MakeEntry(36175,52075,.22M,3081));
     //       entries.Add(MakeEntry(52075,90975,.24M,6579));
     //       entries.Add(MakeEntry(90975,113000,.32M,15915));
     //       entries.Add(MakeEntry(11300,268525,.35M,22963));
     //       entries.Add(MakeEntry(268525,99999999,.37M,77396.75M));
     //       table.Entries = entries;
     //       return table;
     //   }
     //   private TaxTableEntry MakeEntry(decimal minAmount, decimal maxAmount,decimal taxPercentage, decimal tax)
     //   { 
     //       return new TaxTableEntry()
     //       {
     //           AmountOver = minAmount,
     //           MaximumAmount = maxAmount,
     //           MinimumAmount = minAmount,
     //           TaxAmount =  tax,
     //           TaxPercentage = taxPercentage
     //       };
     //   }

        //private ILocalityTaxSetup GetFederalTaxSetup()
        //{
        //    var locality = new PayrollLocality("US");
        //    return new LocalityTaxSetup(locality,GetFederalSetupAmounts());
        //}

        //private IEnumerable<(string Name, decimal Amount)> GetFederalSetupAmounts()
        //{
        //    var setups = new List<(string Name, decimal Amount)>();
        //    setups.Add((Name:"StandardDeduction",Amount: 4200));
        //    setups.Add((Name: "SingleDeduction", Amount: 8400 ));
        //    setups.Add((Name: "MarriedDeduction",Amount:12600));
        //    setups.Add((Name: "MedicareMaximumWage",Amount:99999999));
        //    setups.Add((Name: "MedicareEmployeeRate",Amount:.0145M));
        //    setups.Add((Name: "MedicareEmployerRate",Amount:.0145M));
        //    setups.Add((Name:"MedicareThreshold",Amount:200000));
        //    setups.Add((Name: "MedicareThresholdRate", Amount: .009M));
        //    setups.Add((Name:"FicaMaximumWage",Amount:132900));
        //    setups.Add((Name:"FicaEmployeeRate",Amount:.062M));
        //    setups.Add((Name:"FicaEmployerRate",Amount:.062M));
        //    setups.Add((Name:"QualifyingChildrenCredit",Amount:2000));
        //    setups.Add((Name: "OtherDependentsCredit",Amount:500));
        //    return setups;
        //}

        //private ILocalityTaxSetup GetMaineSetup()
        //{
        //    var locality = new PayrollLocality("US","ME");
        //    return  new LocalityTaxSetup(locality,GetMaineSetupAmounts());
        //}

        //private IEnumerable<(string Name, decimal Amount)> GetMaineSetupAmounts()
        //{
        //    var setups = new List<(string Name, decimal Amount)>();
        //    setups.Add((Name:"WithholdingAllowance",Amount:4200));
        //    setups.Add((Name:"SingleDeduction",Amount:9350));
        //    setups.Add((Name: "MarriedDeduction",Amount:21550));
        //    setups.Add((Name: "SingleFullDeductionWageLimit",Amount:82900));
        //    setups.Add((Name: "MarriedFullDeductionWageLimit",Amount: 165800));
        //    setups.Add((Name:"SingleDeductionWageCutoff",Amount:157900));
        //    setups.Add((Name:"MarriedDeductionWageCutoff",Amount:315800));
        //    return setups;
        //}
    }

    public static class TaxTableExtensions
    {
        public static TaxTableGroup ToTaxTableGroup(this TaxTableGroupDTO tableDto)
        {
            if (tableDto != null)
            {
                return new TaxTableGroup(tableDto.Locality, tableDto.TaxTables.ToTaxTables(tableDto.Locality), new LocalityTaxSetup(tableDto.Locality, tableDto.Amounts));
            }

            return null;
        }

        public static IEnumerable<TaxTableGroup> ToTaxTableGroups(this IEnumerable<TaxTableGroupDTO> tableDtos)
        {
            return tableDtos.Select(t => t.ToTaxTableGroup()).ToList();
        }

        public static TaxTable ToTaxTable(this TaxTableDTO tableDto,PayrollLocality locality)
        {
            if (tableDto != null)
            {
                return  new TaxTable(){Entries =  tableDto.Entries, TableDescription = tableDto.TableDescription, TableName = tableDto.TableName,Locality = locality,LocalityType = locality.LocalityType};
            }

            return null;
        }

        public static IEnumerable<TaxTable> ToTaxTables(this IEnumerable<TaxTableDTO> tableDtos, PayrollLocality locality)
        {
            return tableDtos.Select(t => t.ToTaxTable(locality)).ToList();
        }
    }
}