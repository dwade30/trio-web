﻿namespace TrioDataAPIConsumer.Payroll
{
    public class PayrollAPIBase : TrioDataAPIConsumerBase
    {
        protected PayrollAPIBase(string apiUri) : base(apiUri + @"/payroll")
        {
        }
    }
}