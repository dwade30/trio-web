﻿using Autofac;
using SharedApplication.BlueBook;
using SharedApplication.Payroll;
using TrioDataAPIConsumer.BlueBook;
using TrioDataAPIConsumer.Payroll;

namespace TrioDataAPIConsumer.Configuration
{
    public class ConfigurationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
           //builder.RegisterType<TrioDataAPIBlueBookRepository>().AsSelf(); //.As<IBlueBookRepository>();
            builder.RegisterType<PayrollCloudApiConsumer>().As<IPayrollCloudAPI>();
        }
    }
}