﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using SharedApplication.BlueBook.Models;

namespace TrioDataAPIConsumer
{
    public abstract class TrioDataAPIConsumerBase
    {
        protected string apiUri;
        protected const string apiKey = "56E2731B-C7EC-40F6-9E6B-35E7B221CCEC";
        protected const string apiKeyName = "X-Api-Key";
        public TrioDataAPIConsumerBase(string apiUri)
        {
            this.apiUri = apiUri;
        }

        protected string CreateFullPath(string relativePath)
        {
            return apiUri + @"/" + relativePath;
        }

        protected List<T> GetList<T,TFilter>(string relativePath,TFilter filterCriteria)
        {
            using (var client = new HttpClient())
            {
                var jsonString = JsonSerializer.Serialize(filterCriteria);
                var postContent = new StringContent(jsonString, Encoding.UTF8, "application/json");
                var fullUri = CreateFullPath(relativePath);
                AddHeaders(client);
                try
                {
                    var result =
                        client.PostAsync(fullUri, postContent).Result;

                    if (!result.IsSuccessStatusCode)
                    {
                        if (result.StatusCode == HttpStatusCode.Unauthorized)
                        {

                        }
                        else
                        {

                        }
                    }
                    else
                    {

                        var jsonResult = result.Content.ReadAsStringAsync().Result;
                        
                        return JsonSerializer.Deserialize<List<T>>(jsonResult,new JsonSerializerOptions( ){PropertyNamingPolicy = JsonNamingPolicy.CamelCase,PropertyNameCaseInsensitive = true});
                    }
                }
                catch (Exception ex)
                {

                }
            }
            return new List<T>();
        }

        protected IEnumerable<T> GetList<T>(string relativePath)
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath( relativePath)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<List<T>>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true });
                }
                else
                {
                    return new List<T>();
                }
            }
        }

        protected T GetById<T>(string relativePath,int id) where T: class
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath(relativePath + @"/" + id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<T>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true });
                }
                else
                {
                    return null;
                }
            }
        }

        protected void AddHeaders(HttpClient client)
        {
            client.DefaultRequestHeaders.Add(apiKeyName, apiKey);
        }

        protected IEnumerable<T> GetListWithParameters<T>(string relativePath,
            IEnumerable<(string Name, string value)> parameters)
        {
            using (var client = new HttpClient() )
            {
                AddHeaders(client);
                var fullUri = CreateFullPath(relativePath);
                var uriBuilder = new UriBuilder(fullUri);
                var kvps = parameters.ToDictionary(p => p.Name,p => p.value);
                //var parameters = new Dictionary<string,string>(p);
                string query;
                using (var content = new FormUrlEncodedContent(kvps))
                {
                    query = content.ReadAsStringAsync().Result; 
                }

                uriBuilder.Query = query;
                try
                {


                    var response = client.GetAsync(uriBuilder.Uri).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var jsonResult = response.Content.ReadAsStringAsync().Result;
                        return JsonSerializer.Deserialize<List<T>>(jsonResult,
                            new JsonSerializerOptions()
                            {
                                PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true, AllowTrailingCommas = true, IgnoreNullValues = true
                            });
                    }
                    else
                    {
                        return new List<T>();
                    }
                }
                catch (Exception ex)
                {
                    return new List<T>();
                }
            }
        }
    }
}
