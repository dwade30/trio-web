﻿using System.Collections.Generic;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Rvs;

namespace TrioDataAPIConsumer.BlueBook
{
    public class RvService : BlueBookAPIBase, IRvService
    {
        public RvService(string apiUri) : base(apiUri)
        {
        }

        public Rv GetRv(int id)
        {
            return GetById<Rv>("rv", id);
        }

        public IEnumerable<Rv> GetRvs(RvFilterCriteria filterCriteria)
        {
            return GetList<Rv, RvFilterCriteria>("rv", filterCriteria);
        }
    }
}