﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using BlueBook;
using SharedApplication.BlueBook;

namespace TrioDataAPIConsumer.BlueBook
{
    public class CycleMakeService : BlueBookAPIBase, ICycleMakeListService
    {
        public CycleMakeService(string apiUri) : base(apiUri)
        {
            
        }

        public IEnumerable<CycleMakeList> GetCycleMakes()
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath(@"cyclemake")).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<IEnumerable<CycleMakeList>>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true });
                }
                else
                {
                    return new List<CycleMakeList>();
                }
            }
        }
    }
}