﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.TruckBodies;

namespace TrioDataAPIConsumer.BlueBook
{
    public class TruckBodyService : BlueBookAPIBase, ITruckBodyDataService
    {
        public TruckBodyService(string apiUri) : base(apiUri)
        {
        }

        public TruckBodyData GetTruckBodyData(int id)
        {
            return GetById<TruckBodyData>("truckbody", id);
        }

        public IEnumerable<TruckBodyData> GetTruckBodyDatas(TruckBodyDataFilterCriteria filterCriteria)
        {
            return GetList<TruckBodyData, TruckBodyDataFilterCriteria>("truckbody", filterCriteria);
        }

        public IEnumerable<(string manufacturer, int year)> GetTrailerMakeForYear(int year)
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath("TruckBodyMakeByYear" + @"/" + year)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<IEnumerable<ManufacturerYearPair>>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true }).ToList().ToTupleList();
                }
                else
                {
                    return null;
                }
            }
        }

        public IEnumerable<VehicleManufacturer> GetTruckBodyManufacturers(int year)
        {
            throw new NotImplementedException();
        }
    }
}