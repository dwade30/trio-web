﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.RvXref;

namespace TrioDataAPIConsumer.BlueBook
{
    public class RvXrefService : BlueBookAPIBase, IRVXrefService
    {
        public RvXrefService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<Rvxref> GetRvxrefs(RvxrefFilterCriteria filterCriteria)
        {
            return GetList<Rvxref, RvxrefFilterCriteria>("rvxref", filterCriteria);
        }
    }
}