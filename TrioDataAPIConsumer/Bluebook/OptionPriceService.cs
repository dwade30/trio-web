﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.OptionPrices;

namespace TrioDataAPIConsumer.BlueBook
{
    public class OptionPriceService : BlueBookAPIBase, IOptionPricesService
    {
        public OptionPriceService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<OptionPrice> GetOptionPrices(OptionPriceFilterCriteria filterCriteria)
        {
            return GetList<OptionPrice, OptionPriceFilterCriteria>("optionprice", filterCriteria);
        }
    }
}