﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;

namespace TrioDataAPIConsumer.BlueBook
{
    public class HeavyTruckMakesService : BlueBookAPIBase, IHeavyTruckMakeService
    {
        public HeavyTruckMakesService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<HeavyTruckMakeList> GetHeavyTruckMakes()
        {
            return GetList<HeavyTruckMakeList>("HeavyTruckMake");
        }
    }
}