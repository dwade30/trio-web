﻿using System.Collections.Generic;
using System.Linq;

namespace TrioDataAPIConsumer.BlueBook
{
    public class ManufacturerYearPair
    {
        public string Manufacturer { get; set; }
        public int Year { get; set; }

       
    }

    public static class ManufacturerYearPairExtensions
    {
        public static IEnumerable<(string manufacturer, int year)> ToTupleList(this IEnumerable<ManufacturerYearPair> pairs)
        {
            return pairs.Select(p => ToTuple(p)).ToList();
        }

        public static (string manufacturer, int year) ToTuple(this ManufacturerYearPair pair)
        {
            return (manufacturer: pair.Manufacturer, year: pair.Year);
        }
    }
}