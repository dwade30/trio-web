﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.HeavyTrucks;

namespace TrioDataAPIConsumer.BlueBook
{
    public class HeavyTruckService : BlueBookAPIBase, IHeavyTruckService
    {
        public HeavyTruckService(string apiUri) : base(apiUri)
        {
        }

        public HeavyTruck GetHeavyTruck(int id)
        {
            return GetById<HeavyTruck>("HeavyTruck", id);
        }

        public IEnumerable<HeavyTruck> GetHeavyTrucks(HeavyTruckFilterCriteria filterCriteria)
        {
            return GetList<HeavyTruck, HeavyTruckFilterCriteria>("HeavyTruck", filterCriteria);
        }
    }
}