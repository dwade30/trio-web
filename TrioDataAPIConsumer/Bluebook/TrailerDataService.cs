﻿using System.Collections.Generic;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;

namespace TrioDataAPIConsumer.BlueBook
{
    public class TrailerDataService : BlueBookAPIBase, ITrailerDataService
    {
        public TrailerDataService(string apiUri) : base(apiUri)
        {
        }

        public TrailerData GetTrailerData(int id)
        {
            return GetById<TrailerData>("trailerdata", id);
        }

        public IEnumerable<TrailerData> GetTrailerDatas(TrailerDataFilterCriteria filterCriteria)
        {
            return GetList<TrailerData, TrailerDataFilterCriteria>("trailerdata", filterCriteria);
        }

        public IEnumerable<VehicleManufacturer> GetTrailerManufacturersForYear(int year)
        {
            throw new System.NotImplementedException();
        }
    }
}