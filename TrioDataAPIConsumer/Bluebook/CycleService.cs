﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;
using SharedApplication.BlueBook.Services;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using SharedApplication.CashReceipts.Receipting;

namespace TrioDataAPIConsumer.BlueBook
{
    public class CycleService : BlueBookAPIBase, ICycleService
    {
       
        public CycleService(string apiUri):base(apiUri)
        { 
            
        }

        public Cycle GetCycle(int id)
        {
            return GetById<Cycle>("cycle", id);
        }
      

        public IEnumerable<Cycle> GetCycles(CycleFilterCriteria filterCriteria)
        {
            return GetList<Cycle, CycleFilterCriteria>("cycle",filterCriteria);
        }
       
    }
}