﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Manufacturer;

namespace TrioDataAPIConsumer.BlueBook
{
    public class ManufacturerService: BlueBookAPIBase, IManufacturerService
    {
        public ManufacturerService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<Mfg> GetManufacturers(ManufacturerFilterCriteria filterCriteria)
        {
            return GetList<Mfg, ManufacturerFilterCriteria>("manufacturer", filterCriteria);
        }
    }
}