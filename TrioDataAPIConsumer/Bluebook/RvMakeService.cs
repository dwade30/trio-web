﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;

namespace TrioDataAPIConsumer.BlueBook
{
    public class RvMakeService : BlueBookAPIBase, IRvMakeListService
    {
        public RvMakeService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<RvmakeList> GetRvMakes()
        {
            return GetList<RvmakeList>("rvmake");
        }
    }
}