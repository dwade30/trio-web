﻿namespace TrioDataAPIConsumer.BlueBook
{
    public abstract class BlueBookAPIBase: TrioDataAPIConsumerBase
    {
        protected BlueBookAPIBase(string apiUri) : base(apiUri + @"/bluebook")
        {
        }
    }
}