﻿using SharedApplication;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Services;


namespace TrioDataAPIConsumer.BlueBook
{
    public class TrioDataAPIBlueBookRepository : IBlueBookRepository
    {
        private cGlobalSettings globalSettings;
        public TrioDataAPIBlueBookRepository(cGlobalSettings globalSettings)
        {
            this.globalSettings = globalSettings;
            Cycles = new CycleService(globalSettings.TrioDataAPIUri);
            CycleMakes = new CycleMakeService(globalSettings.TrioDataAPIUri);
            Makes = new MakeService(globalSettings.TrioDataAPIUri);
            HeavyTruckMakes = new HeavyTruckMakesService(globalSettings.TrioDataAPIUri);
            Eddies = new EddieService(globalSettings.TrioDataAPIUri);
            HeavyTrucks = new HeavyTruckService(globalSettings.TrioDataAPIUri);
            Manufacturers = new ManufacturerService(globalSettings.TrioDataAPIUri);
            OptionPrices = new OptionPriceService(globalSettings.TrioDataAPIUri);
            RbCars = new RbCarService(globalSettings.TrioDataAPIUri);
            RvMakes = new RvMakeService(globalSettings.TrioDataAPIUri);
            RvXrefs = new RvXrefService(globalSettings.TrioDataAPIUri);
            TrailerData = new TrailerDataService(globalSettings.TrioDataAPIUri);
            TruckBodyDatas = new TruckBodyService(globalSettings.TrioDataAPIUri);
            TruckBodyOptions = new TruckBodyOptionService(globalSettings.TrioDataAPIUri);
            Rvs = new RvService(globalSettings.TrioDataAPIUri);
        }

        public ICycleService Cycles { get; }
        public IHeavyTruckService HeavyTrucks { get; }
        public IManufacturerService Manufacturers { get; }
        public IRbCarService RbCars { get; }
        public ITrailerDataService TrailerData { get; }
        public IRvService Rvs { get; }
        public IHeavyTruckMakeService HeavyTruckMakes { get; }
        public IRvMakeListService RvMakes { get; }
        public ICycleMakeListService CycleMakes { get; }
        public IOptionsService Options { get; }
        public IOptionPricesService OptionPrices { get; }
        public IRVXrefService RvXrefs { get; }
        public ITruckBodyOptionService TruckBodyOptions { get; }
        public IEddieService Eddies { get; }
        public IMakeService Makes { get; }
        public ITruckBodyDataService TruckBodyDatas { get; }
        IVehicleConfigurationsService IBlueBookRepository.VehicleConfigurations => VehicleConfigurations;

        public IVehicleConfigurationsService VehicleConfigurations { get; }
        public IVinService VehicleInformation { get; }
        public IVehicleSpecService VehicleSpecs { get; }
    }
}