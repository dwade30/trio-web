﻿using System.Collections.Generic;
using SharedApplication.BlueBook;

namespace TrioDataAPIConsumer.BlueBook
{
    public class TruckBodyOptionService : BlueBookAPIBase, ITruckBodyOptionService
    {
        public TruckBodyOptionService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<TruckBodyOption> GetTruckBodyOptions(TruckBodyOptionFilterCriteria filterCriteria)
        {
            return GetList<TruckBodyOption, TruckBodyOptionFilterCriteria>("truckbodyoption", filterCriteria);
        }
    }
}