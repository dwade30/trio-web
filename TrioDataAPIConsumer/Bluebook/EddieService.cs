﻿using System.Collections.Generic;
using SharedApplication.BlueBook;

namespace TrioDataAPIConsumer.BlueBook
{
    public class EddieService : BlueBookAPIBase, IEddieService
    {
        public EddieService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<Eddie> GetEddies(EddieFilterCriteria filterCriteria)
        {
            return GetList<Eddie, EddieFilterCriteria>("Eddie",filterCriteria);
        }
    }
}