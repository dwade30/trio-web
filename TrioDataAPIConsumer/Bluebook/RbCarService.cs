﻿using System.Collections.Generic;
using BlueBook;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Rbcars;

namespace TrioDataAPIConsumer.BlueBook
{
    public class RbCarService : BlueBookAPIBase, IRbCarService
    {
        public RbCarService(string apiUri) : base(apiUri)
        {
        }

        public Rbcar GetRbCar(int id)
        {
            return GetById<Rbcar>("rbcar", id);
        }

        public IEnumerable<Rbcar> GetRbCars(RbCarFilterCriteria filterCriteria)
        {
            //return GetList<Rbcar, RbCarFilterCriteria>("rbcar", filterCriteria);
            return GetList<Rbcar, RbCarFilterCriteria>("rbcar", filterCriteria);
        }
    }
}