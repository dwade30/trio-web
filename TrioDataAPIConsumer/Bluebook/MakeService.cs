﻿using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using SharedApplication.BlueBook;
using SharedApplication.BlueBook.Models;

namespace TrioDataAPIConsumer.BlueBook
{
    public class MakeService : BlueBookAPIBase, IMakeService
    {
        public MakeService(string apiUri) : base(apiUri)
        {
        }

        public IEnumerable<string> GetCarManufacturers()
        {
            //using (var client = new HttpClient())
            //{
            //    var response = client.GetAsync(CreateFullPath(@"/CarManufacturer")).Result;
            //    if (response.IsSuccessStatusCode)
            //    {
            //        var jsonResult = response.Content.ReadAsStringAsync().Result;
            //        return JsonSerializer.Deserialize<IEnumerable<string>>(jsonResult);
            //    }
            //    else
            //    {
            //        return new List<string>();
            //    }
            //}

            return GetList<string>("CarManufacturer");
        }

        public IEnumerable<string> GetCarAndLightTruckManufacturers()
        {
            return GetList<string>("CarAndLightTruckManufacturer");

            //using (var client = new HttpClient())
            //{
            //    var response = client.GetAsync(CreateFullPath(@"/CarAndLightTruckManufacturer")).Result;
            //    if (response.IsSuccessStatusCode)
            //    {
            //        var jsonResult = response.Content.ReadAsStringAsync().Result;
            //        return JsonSerializer.Deserialize<IEnumerable<string>>(jsonResult);
            //    }
            //    else
            //    {
            //        return new List<string>();
            //    }
            //}
        }

        public IEnumerable<string> GetLightTruckManufacturers()
        {
            return GetList<string>("LightTruckManufacturer");
            //using (var client = new HttpClient())
            //{
            //    var response = client.GetAsync(CreateFullPath(@"/LightTruckManufacturer")).Result;
            //    if (response.IsSuccessStatusCode)
            //    {
            //        var jsonResult = response.Content.ReadAsStringAsync().Result;
            //        return JsonSerializer.Deserialize<IEnumerable<string>>(jsonResult);
            //    }
            //    else
            //    {
            //        return new List<string>();
            //    }
            //}
        }

        public IEnumerable<string> GetCarAndLightTruckManufacturerShortDescriptions(string manufacturer)
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath(@"CarAndLightTruckManufacturerShortDescription/" + manufacturer)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<IEnumerable<string>>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true });
                }
                else
                {
                    return new List<string>();
                }
            }
        }

        public IEnumerable<(string manufacturer, int year)> GetTrailerMakeForYear(int year)
        {
            using (var client = new HttpClient())
            {
                AddHeaders(client);
                var response = client.GetAsync(CreateFullPath(@"TrailerMakeByYear/" + year)).Result;
                if (response.IsSuccessStatusCode)
                {
                    var jsonResult = response.Content.ReadAsStringAsync().Result;
                    return JsonSerializer.Deserialize<IEnumerable<ManufacturerYearPair>>(jsonResult, new JsonSerializerOptions() { PropertyNamingPolicy = JsonNamingPolicy.CamelCase, PropertyNameCaseInsensitive = true }).ToList().ToTupleList();
                }
                else
                {
                    return new List<(string manufacturer, int year)>();
                }
            }
        }

        //private IEnumerable<(string manufacturer, int year)> ToTupleList(IEnumerable<ManufacturerYearPair> pairs)
        //{
        //    return pairs.Select(p => ToTuple(p)).ToList();
        //}

        //private (string manufacturer, int year) ToTuple(ManufacturerYearPair pair)
        //{
        //    return (manufacturer: pair.Manufacturer, year: pair.Year);
        //}
    }
}