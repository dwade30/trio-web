﻿namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptCustomReport.
	/// </summary>
	partial class rptCustomReport
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptCustomReport));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.txtTitle = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtPage = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.txtMuniName = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.DLG1 = new GrapeCity.ActiveReports.SectionReportModel.ARControl();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.txtTime = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCurrentValue = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblTotalCost = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalDepreciation = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotalCurrentValue = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblTotals = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCurrentValue)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			this.Detail.Height = 0.1041667F;
			this.Detail.Name = "Detail";
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.txtTitle,
				this.txtDate,
				this.txtPage,
				this.txtMuniName,
				this.DLG1,
				this.Line1,
				this.txtTime
			});
			this.PageHeader.Height = 0.84375F;
			this.PageHeader.Name = "PageHeader";
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldTotalCost,
				this.fldTotalCurrentValue,
				this.fldTotalDepreciation,
				this.lblTotalCost,
				this.lblTotalDepreciation,
				this.lblTotalCurrentValue,
				this.lblTotals
			});
			this.GroupFooter1.Height = 0.8125F;
			this.GroupFooter1.Name = "GroupFooter1";
			// 
			// txtTitle
			// 
			this.txtTitle.Height = 0.25F;
			this.txtTitle.Left = 0.625F;
			this.txtTitle.MultiLine = false;
			this.txtTitle.Name = "txtTitle";
			this.txtTitle.Style = "font-family: \'Tahoma\'; font-size: 10pt; font-weight: bold; text-align: center";
			this.txtTitle.Text = "Title";
			this.txtTitle.Top = 0.0625F;
			this.txtTitle.Width = 6.25F;
			// 
			// txtDate
			// 
			this.txtDate.Height = 0.1875F;
			this.txtDate.Left = 5.3125F;
			this.txtDate.MultiLine = false;
			this.txtDate.Name = "txtDate";
			this.txtDate.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtDate.Text = "Date";
			this.txtDate.Top = 0.0625F;
			this.txtDate.Width = 2.1875F;
			// 
			// txtPage
			// 
			this.txtPage.Height = 0.1875F;
			this.txtPage.Left = 6.375F;
			this.txtPage.MultiLine = false;
			this.txtPage.Name = "txtPage";
			this.txtPage.Style = "font-family: \'Tahoma\'; font-size: 8.5pt; text-align: right";
			this.txtPage.Text = "Page";
			this.txtPage.Top = 0.25F;
			this.txtPage.Width = 1.125F;
			// 
			// txtMuniName
			// 
			this.txtMuniName.Height = 0.1875F;
			this.txtMuniName.Left = 0.0625F;
			this.txtMuniName.MultiLine = false;
			this.txtMuniName.Name = "txtMuniName";
			this.txtMuniName.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtMuniName.Text = "t";
			this.txtMuniName.Top = 0.0625F;
			this.txtMuniName.Width = 1.0625F;
			// 
			// DLG1
			// 
			this.DLG1.Height = 0.3333333F;
			this.DLG1.Left = 0.0625F;
			this.DLG1.Name = "DLG1";
			this.DLG1.Top = 0.04166667F;
			this.DLG1.Visible = false;
			this.DLG1.Width = 0.3333333F;
			// 
			// Line1
			// 
			this.Line1.Height = 0.01041669F;
			this.Line1.Left = 0.03125F;
			this.Line1.LineWeight = 3F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.78125F;
			this.Line1.Width = 7.4375F;
			this.Line1.X1 = 0.03125F;
			this.Line1.X2 = 7.46875F;
			this.Line1.Y1 = 0.7916667F;
			this.Line1.Y2 = 0.78125F;
			// 
			// txtTime
			// 
			this.txtTime.Height = 0.2083333F;
			this.txtTime.Left = 0.0625F;
			this.txtTime.MultiLine = false;
			this.txtTime.Name = "txtTime";
			this.txtTime.Style = "font-family: \'Tahoma\'; font-size: 8pt";
			this.txtTime.Text = "t";
			this.txtTime.Top = 0.25F;
			this.txtTime.Width = 1.0625F;
			// 
			// fldTotalCost
			// 
			this.fldTotalCost.Height = 0.1875F;
			this.fldTotalCost.Left = 3.53125F;
			this.fldTotalCost.Name = "fldTotalCost";
			this.fldTotalCost.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.fldTotalCost.Text = "Field1";
			this.fldTotalCost.Top = 0.15625F;
			this.fldTotalCost.Width = 0.96875F;
			// 
			// fldTotalCurrentValue
			// 
			this.fldTotalCurrentValue.Height = 0.1875F;
			this.fldTotalCurrentValue.Left = 3.53125F;
			this.fldTotalCurrentValue.Name = "fldTotalCurrentValue";
			this.fldTotalCurrentValue.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.fldTotalCurrentValue.Text = "Field1";
			this.fldTotalCurrentValue.Top = 0.375F;
			this.fldTotalCurrentValue.Width = 0.96875F;
			// 
			// fldTotalDepreciation
			// 
			this.fldTotalDepreciation.Height = 0.1875F;
			this.fldTotalDepreciation.Left = 3.53125F;
			this.fldTotalDepreciation.Name = "fldTotalDepreciation";
			this.fldTotalDepreciation.Style = "font-family: \'Tahoma\'; font-weight: bold; text-align: right; ddo-char-set: 1";
			this.fldTotalDepreciation.Text = "Field1";
			this.fldTotalDepreciation.Top = 0.59375F;
			this.fldTotalDepreciation.Width = 0.96875F;
			// 
			// lblTotalCost
			// 
			this.lblTotalCost.Height = 0.1875F;
			this.lblTotalCost.HyperLink = null;
			this.lblTotalCost.Left = 2.53125F;
			this.lblTotalCost.Name = "lblTotalCost";
			this.lblTotalCost.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblTotalCost.Text = "Cost";
			this.lblTotalCost.Top = 0.15625F;
			this.lblTotalCost.Width = 0.96875F;
			// 
			// lblTotalDepreciation
			// 
			this.lblTotalDepreciation.Height = 0.1875F;
			this.lblTotalDepreciation.HyperLink = null;
			this.lblTotalDepreciation.Left = 2.53125F;
			this.lblTotalDepreciation.Name = "lblTotalDepreciation";
			this.lblTotalDepreciation.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblTotalDepreciation.Text = "Depreciation";
			this.lblTotalDepreciation.Top = 0.59375F;
			this.lblTotalDepreciation.Width = 0.96875F;
			// 
			// lblTotalCurrentValue
			// 
			this.lblTotalCurrentValue.Height = 0.1875F;
			this.lblTotalCurrentValue.HyperLink = null;
			this.lblTotalCurrentValue.Left = 2.53125F;
			this.lblTotalCurrentValue.Name = "lblTotalCurrentValue";
			this.lblTotalCurrentValue.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblTotalCurrentValue.Text = "Current Value";
			this.lblTotalCurrentValue.Top = 0.375F;
			this.lblTotalCurrentValue.Width = 0.96875F;
			// 
			// lblTotals
			// 
			this.lblTotals.Height = 0.1875F;
			this.lblTotals.HyperLink = null;
			this.lblTotals.Left = 1.40625F;
			this.lblTotals.Name = "lblTotals";
			this.lblTotals.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblTotals.Text = "Final Totals:";
			this.lblTotals.Top = 0.15625F;
			this.lblTotals.Width = 0.875F;
			// 
			// rptCustomReport
			// 
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.PageStart += new System.EventHandler(this.ActiveReport_PageStart);
			this.PageEnd += new System.EventHandler(this.ActiveReport_PageEnd);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.Disposed += new System.EventHandler(RptCustomReport_Disposed);
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.5F;
			this.PageSettings.Margins.Right = 0.5F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.MirrorMargins = true;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.5F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			((System.ComponentModel.ISupportInitialize)(this.txtTitle)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtPage)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtMuniName)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtTime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCurrentValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotalCurrentValue)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblTotals)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTitle;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtPage;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtMuniName;
		private GrapeCity.ActiveReports.SectionReportModel.ARControl DLG1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox txtTime;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCurrentValue;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotalCurrentValue;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblTotals;
	}
}
