﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmVendors.
	/// </summary>
	public partial class frmVendors : BaseForm
	{
		public frmVendors()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmVendors InstancePtr
		{
			get
			{
				return (frmVendors)Sys.GetInstance(typeof(frmVendors));
			}
		}

		protected frmVendors _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   12/04/2002                          *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :                                       *
		// ********************************************************
		string strSort = "";
		bool boolDataChanged;
		const string GRIDCOLUMN0 = "ID";
		const string GRIDCOLUMN1 = "Changed";
		const string GRIDCOLUMN2 = "Vendor #";
		const string GRIDCOLUMN3 = "Name";

		private void SetGridProperties()
		{
			VendorGrid.FixedCols = 0;
			VendorGrid.FixedRows = 1;
			VendorGrid.Rows = 1;
			VendorGrid.Cols = 4;
			VendorGrid.ColHidden(0, true);
			VendorGrid.ColHidden(1, true);
			VendorGrid.ColWidth(2, 1300);
			VendorGrid.ColWidth(3, 420);
			//VendorGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, VendorGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			VendorGrid.TextMatrix(0, 0, GRIDCOLUMN0);
			VendorGrid.TextMatrix(0, 1, GRIDCOLUMN1);
			VendorGrid.TextMatrix(0, 2, GRIDCOLUMN2);
			VendorGrid.TextMatrix(0, 3, GRIDCOLUMN3);
			VendorGrid.Select(0, 0);
		}

		private void frmVendors_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmVendors_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
			{
				KeyCode = (Keys)0;
				mnuNew_Click();
			}
		}

		private void frmVendors_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				this.VendorGrid.Select(0, 0);
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmVendors_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmVendors.ScaleWidth	= 5880;
			//frmVendors.ScaleHeight	= 3990;
			//frmVendors.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetGridProperties();
			VendorGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			VendorGrid.ColDataType(2, FCGrid.DataTypeSettings.flexDTLong);
			lblBudLabel.Visible = modVariables.Statics.boolVendorsFromBud;
			lblBudLabel.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
			//mnuDelete.Enabled = !modVariables.boolVendorsFromBud;
			//mnuNew.Enabled = !modVariables.boolVendorsFromBud;
			//mnuSave.Enabled = !modVariables.boolVendorsFromBud;
			//mnuSaveExit.Enabled = !modVariables.boolVendorsFromBud;
			cmdDelete.Enabled = !modVariables.Statics.boolVendorsFromBud;
			cmdNew.Enabled = !modVariables.Statics.boolVendorsFromBud;
			cmdSave.Enabled = !modVariables.Statics.boolVendorsFromBud;
			LoadRecord();
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			VendorGrid.Select(0, 0);
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("Data has changed. Save changes?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intResponse)
				{
					case DialogResult.Yes:
						{
							// YES
							modVariables.Statics.gboolCancelSelected = false;
							if (SaveRecord())
							{
								e.Cancel = false;
							}
							else
							{
								e.Cancel = true;
							}
							break;
						}
					case DialogResult.No:
						{
							// NO
							modVariables.Statics.gboolCancelSelected = false;
							Close();
							break;
						}
					case DialogResult.Cancel:
						{
							// CANCEL
							e.Cancel = true;
							modVariables.Statics.gboolCancelSelected = true;
							this.Focus();
							break;
						}
				}
				//end switch
			}
			else
			{
				Close();
			}
		}

		private void frmVendors_Resize(object sender, System.EventArgs e)
		{
			VendorGrid.ColWidth(2, FCConvert.ToInt32(VendorGrid.WidthOriginal * 0.23));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void VendorGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			VendorGrid.TextMatrix(VendorGrid.Row, 1, FCConvert.ToString(1));
			boolDataChanged = true;
		}

		private void VendorGrid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (VendorGrid.Col == 2)
			{
				VendorGrid.EditMask = "#####";
			}
			else
			{
				VendorGrid.EditMask = string.Empty;
			}
		}

		private void VendorGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (VendorGrid.MouseRow == 0)
			{
				// sort this grid
				if (VendorGrid.Rows < 2)
					return;
				VendorGrid.Select(1, VendorGrid.Col);
				if (strSort == "flexSortGenericAscending")
				{
					VendorGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
					strSort = "flexSortGenericDescending";
				}
				else
				{
					VendorGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
					strSort = "flexSortGenericAscending";
				}
			}
		}

		public void clrForm(bool boolClear = true)
		{
			object[] tmpString = null;
			// - "AutoDim"
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is MaskedTextBox)
				{
					(obj as MaskedTextBox).Mask = "";
					obj.Text = "";
					(obj as MaskedTextBox).Mask = "##/##/####";
				}
				if (obj is FCComboBox)
				{
					if (FCConvert.ToBoolean(boolClear))
						(obj as FCComboBox).Clear();
				}
			}
			// obj
			VendorGrid.Rows = 1;
			tmpString = new object[3 + 1];
		}

		private bool SaveRecord()
		{
			bool SaveRecord = false;
			try
			{
				// On Error GoTo ErrorHandler
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				for (intCounter = 1; intCounter <= VendorGrid.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(VendorGrid.TextMatrix(intCounter, 0)) != 0)
					{
						if (Strings.Trim(VendorGrid.TextMatrix(intCounter, 2)) == "")
						{
							MessageBox.Show("You must enter a vendor number before you may proceed.", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							VendorGrid.Select(intCounter, 2);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (Strings.Trim(VendorGrid.TextMatrix(intCounter, 3)) == "")
						{
							MessageBox.Show("You must enter a vendor name before you may proceed.", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							VendorGrid.Select(intCounter, 3);
							SaveRecord = false;
							return SaveRecord;
						}
					}
					else
					{
						if (Strings.Trim(VendorGrid.TextMatrix(intCounter, 2)) == "" && Strings.Trim(VendorGrid.TextMatrix(intCounter, 3)) == "")
						{
							// do nothing
						}
						else if (Strings.Trim(VendorGrid.TextMatrix(intCounter, 2)) == "")
						{
							MessageBox.Show("You must enter a vendor number before you may proceed.", "Invalid Vendor Number", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							VendorGrid.Select(intCounter, 2);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (Strings.Trim(VendorGrid.TextMatrix(intCounter, 3)) == "")
						{
							MessageBox.Show("You must enter a vendor name before you may proceed.", "Invalid Vendor Name", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							VendorGrid.Select(intCounter, 3);
							SaveRecord = false;
							return SaveRecord;
						}
					}
				}
				for (intCounter = 1; intCounter <= VendorGrid.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(VendorGrid.TextMatrix(intCounter, 1)) == 1)
					{
						// then this record has changed
						if (FCConvert.ToDouble(VendorGrid.TextMatrix(intCounter, 0)) == 0)
						{
							// this is a new record
							if (Strings.Trim(VendorGrid.TextMatrix(intCounter, 2)) == "" && Strings.Trim(VendorGrid.TextMatrix(intCounter, 3)) == "")
							{
								// dont save record if no information has been entered
							}
							else
							{
								rsData.Execute("Insert into tblVendors (Code,Description) VALUES ('" + modGlobalRoutines.FixQuote(VendorGrid.TextMatrix(intCounter, 2)) + "','" + modGlobalRoutines.FixQuote(VendorGrid.TextMatrix(intCounter, 3)) + "')", modGlobal.DatabaseNamePath);
							}
						}
						else
						{
							// this is an edit record
							rsData.OpenRecordset("Select * from tblVendors where ID = " + VendorGrid.TextMatrix(intCounter, 0), modGlobal.DatabaseNamePath);
							if (rsData.EndOfFile())
							{
								rsData.AddNew();
							}
							else
							{
								rsData.Edit();
							}
							rsData.Set_Fields("Code", Strings.Trim(modGlobalRoutines.FixQuote(VendorGrid.TextMatrix(intCounter, 2))));
							rsData.Set_Fields("Description", Strings.Trim(modGlobalRoutines.FixQuote(VendorGrid.TextMatrix(intCounter, 3))));
							rsData.Update();
						}
					}
				}
				if (boolDataChanged)
					LoadRecord();
				SaveRecord = true;
				MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
			return SaveRecord;
		}

		private void LoadRecord()
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			VendorGrid.Rows = 1;
			if (modVariables.Statics.boolVendorsFromBud)
			{
				// need to change this database to be twdb0000.vb1
				rsData.OpenRecordset("Select * from VendorMaster WHERE Status <> 'D' Order by VendorNumber", "TWBD0000.vb1");
				VendorGrid.Rows = rsData.RecordCount() + 1;
				VendorGrid.Editable = FCGrid.EditableSettings.flexEDNone;
				for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
				{
					VendorGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("VendorNumber")));
					VendorGrid.TextMatrix(intCounter, 1, FCConvert.ToString(0));
					VendorGrid.TextMatrix(intCounter, 2, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_Int32("VendorNumber"))));
					VendorGrid.TextMatrix(intCounter, 3, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("CheckName"))));
					rsData.MoveNext();
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from tblVendors Order by Code", modGlobal.DatabaseNamePath);
				VendorGrid.Rows = rsData.RecordCount() + 1;
				VendorGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
				for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
				{
					VendorGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					VendorGrid.TextMatrix(intCounter, 1, FCConvert.ToString(0));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					VendorGrid.TextMatrix(intCounter, 2, Strings.Trim(FCConvert.ToString(rsData.Get_Fields("Code"))));
					VendorGrid.TextMatrix(intCounter, 3, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Description"))));
					rsData.MoveNext();
				}
			}
			if (VendorGrid.Rows > 1)
			{
				VendorGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 2, VendorGrid.Rows - 1, VendorGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			boolDataChanged = false;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			int intRow = 0;
			int intCol = 0;
			if (VendorGrid.Row > 0)
			{
				intRow = VendorGrid.Row;
				intCol = VendorGrid.Col;
				if (MessageBox.Show("Are you sure you wish to delete this item?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					if (Conversion.Val(VendorGrid.TextMatrix(VendorGrid.Row, 0)) == 0)
					{
						VendorGrid.RemoveItem(VendorGrid.Row);
					}
					else
					{
						rsData.Execute("Delete from tblVendors where ID = " + VendorGrid.TextMatrix(VendorGrid.Row, 0), modGlobal.DatabaseNamePath);
						VendorGrid.RemoveItem(VendorGrid.Row);
						// Call LoadRecord
					}
					if (intRow <= VendorGrid.Rows - 1)
					{
						VendorGrid.Select(intRow, intCol);
					}
					else if (VendorGrid.Rows > 1)
					{
						VendorGrid.Select(intRow - 1, intCol);
					}
					MessageBox.Show("Delete completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			int intResponse;
			// If boolDataChanged Then
			// intResponse = MsgBox("Data has changed. Save changes?", vbQuestion + vbYesNoCancel, "TRIO Software")
			// Select Case intResponse
			// Case vbYes
			// VendorGrid.Select 0, 0
			// If SaveRecord Then Unload Me
			// Case vbNo
			// Unload Me
			// Case 2
			// 
			// End Select
			// Else
			Close();
			// End If
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			// vbPorter upgrade warning: lngNextNumber As int	OnWrite(short, double)
			int lngNextNumber = 0;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select Max(Code) as MaxNumber from tblVendors", modGlobal.DatabaseNamePath);
			if (rsData.EndOfFile())
			{
				lngNextNumber = 1;
			}
			else
			{
				// TODO Get_Fields: Field [MaxNumber] not found!! (maybe it is an alias?)
				lngNextNumber = FCConvert.ToInt32(Conversion.Val(rsData.Get_Fields("MaxNumber")) + 1);
			}
			if (VendorGrid.Rows > 1)
			{
				for (intCounter = 1; intCounter <= VendorGrid.Rows - 1; intCounter++)
				{
					if (Conversion.Val(VendorGrid.TextMatrix(intCounter, 2)) >= lngNextNumber)
					{
						lngNextNumber = FCConvert.ToInt32(VendorGrid.TextMatrix(intCounter, 2)) + 1;
						// Exit For
					}
				}
			}
			VendorGrid.Rows += 1;
			VendorGrid.TextMatrix(VendorGrid.Rows - 1, 0, FCConvert.ToString(0));
			VendorGrid.TextMatrix(VendorGrid.Rows - 1, 1, FCConvert.ToString(1));
			VendorGrid.TextMatrix(VendorGrid.Rows - 1, 2, FCConvert.ToString(lngNextNumber));
			VendorGrid.Select(VendorGrid.Rows - 1, 2);
			VendorGrid.TopRow = VendorGrid.Rows - 1;
			VendorGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 2, VendorGrid.Rows - 1, VendorGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			// boolDataChanged = True
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			VendorGrid.Select(0, 0);
			SaveRecord();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			VendorGrid.Select(0, 0);
			if (SaveRecord())
				Close();
		}

		private void VendorGrid_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			// if we are in the last cell of the grid and the user pushed enter then create a new row and go to the beginning of that row
			if (keyAscii == 13)
			{
				if (VendorGrid.Row == VendorGrid.Rows - 1 && VendorGrid.Col == VendorGrid.Cols - 1)
				{
					keyAscii = 0;
					if (!modVariables.Statics.boolVendorsFromBud)
					{
						mnuNew_Click();
					}
				}
				else
				{
					keyAscii = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void VendorGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			// if we are in the last cell of the grid and the user pushed enter then create a new row and go to the beginning of that row
			if (keyAscii == 13)
			{
				if (VendorGrid.Row == VendorGrid.Rows - 1 && VendorGrid.Col == VendorGrid.Cols - 1)
				{
					keyAscii = 0;
					if (!modVariables.Statics.boolVendorsFromBud)
					{
						mnuNew_Click();
					}
				}
				else
				{
					keyAscii = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void VendorGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = VendorGrid.GetFlexRowIndex(e.RowIndex);
			int col = VendorGrid.GetFlexColIndex(e.ColumnIndex);
			if (col == 2)
			{
				if (modGlobalRoutines.DuplicateFound(2, row, VendorGrid, FCConvert.ToString(Conversion.Val(VendorGrid.EditText))))
				{
					MessageBox.Show("This vendor code already exists.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					VendorGrid.TextMatrix(row, 2, string.Empty);
					e.Cancel = true;
				}
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(sender, e);
		}

		private void cmdNew_Click(object sender, EventArgs e)
		{
			mnuNew_Click();
		}

		private void cmdDelete_Click(object sender, EventArgs e)
		{
			mnuDelete_Click(sender, e);
		}
	}
}
