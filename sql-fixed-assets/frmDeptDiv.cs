﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmDeptDiv.
	/// </summary>
	public partial class frmDeptDiv : BaseForm
	{
		public frmDeptDiv()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmDeptDiv InstancePtr
		{
			get
			{
				return (frmDeptDiv)Sys.GetInstance(typeof(frmDeptDiv));
			}
		}

		protected frmDeptDiv _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   12/16/2002                          *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :                                       *
		// ********************************************************
		string strSort = "";
		bool boolDataChanged;
		public bool blnFromMaster;
		const string GRIDCOLUMN0 = "ID";
		const string GRIDCOLUMN1 = "Changed";
		const string GRIDCOLUMN2 = "Dept";
		const string GRIDCOLUMN3 = "Div";
		const string GRIDCOLUMN4 = "Description";

		private void SetGridProperties()
		{
			DeptDivGrid.FixedCols = 0;
			DeptDivGrid.FixedRows = 1;
			DeptDivGrid.Rows = 1;
			DeptDivGrid.Cols = 5;
			DeptDivGrid.ColHidden(0, true);
			DeptDivGrid.ColHidden(1, true);
			if (!modVariables.Statics.boolUseDivision)
			{
				DeptDivGrid.ColHidden(3, true);
			}
			else
			{
				DeptDivGrid.ColHidden(3, false);
			}
			DeptDivGrid.ColWidth(2, 1200);
			DeptDivGrid.ColWidth(3, 1200);
			DeptDivGrid.ColWidth(4, 300);
			//DeptDivGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 2, 0, DeptDivGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			DeptDivGrid.TextMatrix(0, 0, GRIDCOLUMN0);
			DeptDivGrid.TextMatrix(0, 1, GRIDCOLUMN1);
			DeptDivGrid.TextMatrix(0, 2, GRIDCOLUMN2);
			DeptDivGrid.TextMatrix(0, 3, GRIDCOLUMN3);
			DeptDivGrid.TextMatrix(0, 4, GRIDCOLUMN4);
			DeptDivGrid.Select(0, 0);
		}

		private void DeptDivGrid_KeyPressEvent(object sender, KeyPressEventArgs e)
		{
			int keyAscii = Strings.Asc(e.KeyChar);
			// if we are in the last cell of the grid and the user pushed enter then create a new row and go to the beginning of that row
			if (keyAscii == 13)
			{
				if (DeptDivGrid.Row == DeptDivGrid.Rows - 1 && DeptDivGrid.Col == DeptDivGrid.Cols - 1)
				{
					keyAscii = 0;
					if (!modVariables.Statics.boolDeptDivFromBud)
					{
						mnuNew_Click();
					}
				}
				else
				{
					keyAscii = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void DeptDivGrid_KeyPressEdit(object sender, KeyPressEventArgs e)
		{
			//FC:FINAL:AM:#283 - in VB6 KeyPressEdit is not fired for the TAB key
			if (e.KeyChar == 9)
			{
				return;
			}
			int keyAscii = Strings.Asc(e.KeyChar);
			// if we are in the last cell of the grid and the user pushed enter then create a new row and go to the beginning of that row
			if (keyAscii == 13)
			{
				if (DeptDivGrid.Row == DeptDivGrid.Rows - 1 && DeptDivGrid.Col == DeptDivGrid.Cols - 1)
				{
					keyAscii = 0;
					if (!modVariables.Statics.boolDeptDivFromBud)
					{
						mnuNew_Click();
					}
				}
				else
				{
					keyAscii = 0;
					Support.SendKeys("{TAB}", false);
				}
			}
		}

		private void DeptDivGrid_ValidateEdit(object sender, DataGridViewCellValidatingEventArgs e)
		{
			//FC:FINAL:MSH - save and use correct indexes of the cell
			int row = DeptDivGrid.GetFlexRowIndex(e.RowIndex);
			int col = DeptDivGrid.GetFlexColIndex(e.ColumnIndex);
			int intCounter;
			if (col == 3)
			{
				for (intCounter = 1; intCounter <= DeptDivGrid.Rows - 1; intCounter++)
				{
					if (intCounter != col)
					{
						if (Conversion.Val(DeptDivGrid.TextMatrix(intCounter, 3)) == Conversion.Val(DeptDivGrid.EditText))
						{
							if (Conversion.Val(DeptDivGrid.TextMatrix(intCounter, 2)) == Conversion.Val(DeptDivGrid.TextMatrix(col, 2)))
							{
								MessageBox.Show("This Dept / Div code already exists.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
								DeptDivGrid.TextMatrix(col, 3, string.Empty);
								e.Cancel = true;
							}
						}
					}
				}
			}
		}

		private void frmDeptDiv_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
				return;
		}

		private void frmDeptDiv_KeyDown(object sender, Wisej.Web.KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			int Shift = (FCConvert.ToInt32(e.KeyData) / 0x10000);
			if (KeyCode == Keys.Insert)
				mnuNew_Click();
		}

		private void frmDeptDiv_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii >= Keys.NumPad1 && KeyAscii <= Keys.F11)
			{
				KeyAscii = KeyAscii - 32;
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void frmDeptDiv_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmDeptDiv.ScaleWidth	= 5880;
			//frmDeptDiv.ScaleHeight	= 4185;
			//frmDeptDiv.LinkTopic	= "Form1";
			//End Unmaped Properties
			modGlobalFunctions.SetFixedSize(this, 1);
			modGlobalFunctions.SetTRIOColors(this, false);
			SetGridProperties();
			DeptDivGrid.Editable = FCGrid.EditableSettings.flexEDKbdMouse;
			lblBudLabel.Visible = modVariables.Statics.boolDeptDivFromBud;
			lblBudLabel.ForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORRED);
			//mnuDelete.Enabled = !modVariables.boolDeptDivFromBud;
			//mnuNew.Enabled = !modVariables.boolDeptDivFromBud;
			//mnuSave.Enabled = !modVariables.boolDeptDivFromBud;
			//mnuSaveExit.Enabled = !modVariables.boolDeptDivFromBud;
			cmdDelete.Enabled = !modVariables.Statics.boolDeptDivFromBud;
			cmdNew.Enabled = !modVariables.Statics.boolDeptDivFromBud;
			cmdSave.Enabled = !modVariables.Statics.boolDeptDivFromBud;
			LoadRecord();
		}
		// vbPorter upgrade warning: Cancel As short	OnWrite(bool)
		private void Form_QueryUnload(object sender, FCFormClosingEventArgs e)
		{
			// vbPorter upgrade warning: intResponse As short, int --> As DialogResult
			DialogResult intResponse;
			DeptDivGrid.Select(0, 0);
			if (boolDataChanged)
			{
				intResponse = MessageBox.Show("Data has changed. Save changes?", "TRIO Software", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
				switch (intResponse)
				{
					case DialogResult.Yes:
						{
							// YES
							modVariables.Statics.gboolCancelSelected = false;
							if (SaveRecord())
							{
								e.Cancel = false;
								if (!blnFromMaster)
								{
									//MDIParent.InstancePtr.Focus();
								}
							}
							else
							{
								e.Cancel = true;
							}
							break;
						}
					case DialogResult.No:
						{
							// NO
							modVariables.Statics.gboolCancelSelected = false;
							Close();
							if (!blnFromMaster)
							{
								//MDIParent.InstancePtr.Focus();
							}
							break;
						}
					case DialogResult.Cancel:
						{
							// CANCEL
							modVariables.Statics.gboolCancelSelected = true;
							e.Cancel = true;
							this.Focus();
							break;
						}
				}
				//end switch
			}
			else
			{
				Close();
				if (!blnFromMaster)
				{
					//MDIParent.InstancePtr.Focus();
				}
			}
		}

		private void frmDeptDiv_Resize(object sender, System.EventArgs e)
		{
			DeptDivGrid.ColWidth(2, FCConvert.ToInt32(DeptDivGrid.WidthOriginal * 0.12));
			DeptDivGrid.ColWidth(3, FCConvert.ToInt32(DeptDivGrid.WidthOriginal * 0.12));
		}

		private void Form_Unload(object sender, FCFormClosingEventArgs e)
		{
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}

		private void DeptDivGrid_AfterEdit(object sender, DataGridViewCellEventArgs e)
		{
			DeptDivGrid.TextMatrix(DeptDivGrid.Row, 1, FCConvert.ToString(1));
			boolDataChanged = true;
		}

		private void DeptDivGrid_BeforeEdit(object sender, DataGridViewCellCancelEventArgs e)
		{
			if (DeptDivGrid.Col == 2)
				DeptDivGrid.EditMask = "#####";
			if (DeptDivGrid.Col == 3)
				DeptDivGrid.EditMask = "#####";
			if (DeptDivGrid.Col == 4)
				DeptDivGrid.EditMask = string.Empty;
		}

		private void DeptDivGrid_ClickEvent(object sender, System.EventArgs e)
		{
			if (DeptDivGrid.MouseRow == 0)
			{
				// sort this grid
				if (DeptDivGrid.Rows > 1)
				{
					DeptDivGrid.Select(1, DeptDivGrid.Col);
					if (strSort == "flexSortGenericAscending")
					{
						DeptDivGrid.Sort = FCGrid.SortSettings.flexSortGenericDescending;
						strSort = "flexSortGenericDescending";
					}
					else
					{
						DeptDivGrid.Sort = FCGrid.SortSettings.flexSortGenericAscending;
						strSort = "flexSortGenericAscending";
					}
				}
			}
		}

		public void clrForm(bool boolClear = true)
		{
			object[] tmpString = null;
			// - "AutoDim"
			/* object obj; */
			foreach (Control obj in this.GetAllControls())
			{
				if (obj is FCTextBox)
					obj.Text = "";
				if (obj is MaskedTextBox)
				{
					(obj as MaskedTextBox).Mask = "";
					obj.Text = "";
					(obj as MaskedTextBox).Mask = "##/##/####";
				}
				if (obj is FCComboBox)
				{
					if (FCConvert.ToBoolean(boolClear))
						(obj as FCComboBox).Clear();
				}
			}
			// obj
			DeptDivGrid.Rows = 1;
			tmpString = new object[3 + 1];
		}

		private bool SaveRecord()
		{
			bool SaveRecord = false;
			try
			{
				// On Error GoTo ErrorHandler
				int intCounter;
				clsDRWrapper rsData = new clsDRWrapper();
				for (intCounter = 1; intCounter <= DeptDivGrid.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(DeptDivGrid.TextMatrix(intCounter, 0)) != 0)
					{
						if (Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 2)) == "")
						{
							MessageBox.Show("You must enter a department before you may proceed.", "Invalid Department", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							DeptDivGrid.Select(intCounter, 2);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (modVariables.Statics.boolUseDivision && Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 3)) == "")
						{
							MessageBox.Show("You must enter a division before you may proceed.", "Invalid Division", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							DeptDivGrid.Select(intCounter, 3);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 4)) == "")
						{
							MessageBox.Show("You must enter a dept / div description before you may proceed.", "Invalid Dept / Div Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							DeptDivGrid.Select(intCounter, 4);
							SaveRecord = false;
							return SaveRecord;
						}
					}
					else
					{
						if (Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 2)) == "" && Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 3)) == "" && Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 4)) == "")
						{
							// do nothing
						}
						else if (Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 2)) == "")
						{
							MessageBox.Show("You must enter a department before you may proceed.", "Invalid Department", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							DeptDivGrid.Select(intCounter, 2);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (modVariables.Statics.boolUseDivision && Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 3)) == "")
						{
							MessageBox.Show("You must enter a division before you may proceed.", "Invalid Division", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							DeptDivGrid.Select(intCounter, 3);
							SaveRecord = false;
							return SaveRecord;
						}
						else if (Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 4)) == "")
						{
							MessageBox.Show("You must enter a dept / div description before you may proceed.", "Invalid Dept / Div Description", MessageBoxButtons.OK, MessageBoxIcon.Hand);
							DeptDivGrid.Select(intCounter, 4);
							DeptDivGrid.EditCell();
							SaveRecord = false;
							return SaveRecord;
						}
					}
				}
				for (intCounter = 1; intCounter <= DeptDivGrid.Rows - 1; intCounter++)
				{
					if (FCConvert.ToDouble(DeptDivGrid.TextMatrix(intCounter, 1)) == 1)
					{
						// then this record has changed
						if (FCConvert.ToDouble(DeptDivGrid.TextMatrix(intCounter, 0)) == 0)
						{
							// this is a new record
							if (Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 2)) == "" && Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 3)) == "" && Strings.Trim(DeptDivGrid.TextMatrix(intCounter, 4)) == "")
							{
								// dont save record if no information has been entered for it
							}
							else
							{
								rsData.Execute("Insert into tblDeptDiv (Dept,Div,Description) VALUES ('" + Strings.Trim(modGlobalRoutines.FixQuote(DeptDivGrid.TextMatrix(intCounter, 2))) + "','" + Strings.Trim(modGlobalRoutines.FixQuote(DeptDivGrid.TextMatrix(intCounter, 3))) + "','" + Strings.Trim(modGlobalRoutines.FixQuote(DeptDivGrid.TextMatrix(intCounter, 4))) + "')", modGlobal.DatabaseNamePath);
							}
						}
						else
						{
							// this is an edit record
							rsData.OpenRecordset("Select * from tblDeptDiv where ID = " + DeptDivGrid.TextMatrix(intCounter, 0), modGlobal.DatabaseNamePath);
							if (rsData.EndOfFile())
							{
								rsData.AddNew();
							}
							else
							{
								rsData.Edit();
							}
							rsData.Set_Fields("Dept", Strings.Trim(modGlobalRoutines.FixQuote(DeptDivGrid.TextMatrix(intCounter, 2))));
							rsData.Set_Fields("Div", Strings.Trim(modGlobalRoutines.FixQuote(DeptDivGrid.TextMatrix(intCounter, 3))));
							rsData.Set_Fields("Description", Strings.Trim(modGlobalRoutines.FixQuote(DeptDivGrid.TextMatrix(intCounter, 4))));
							rsData.Update();
						}
					}
				}
				if (boolDataChanged)
					LoadRecord();
				SaveRecord = true;
				MessageBox.Show("Save completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
				return SaveRecord;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
			}
			return SaveRecord;
		}

		private void LoadRecord()
		{
			int intCounter;
			clsDRWrapper rsData = new clsDRWrapper();
			DeptDivGrid.Rows = 1;
			// this is an edit record
			if (modVariables.Statics.boolDeptDivFromBud)
			{
				DeptDivGrid.Editable = FCGrid.EditableSettings.flexEDNone;
				rsData.OpenRecordset("Select * from DeptDivTitles Order by Department", "TWBD0000.vb1");
				DeptDivGrid.Rows = rsData.RecordCount() + 1;
				for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
				{
					DeptDivGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					DeptDivGrid.TextMatrix(intCounter, 1, FCConvert.ToString(0));
					DeptDivGrid.TextMatrix(intCounter, 2, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Department"))));
					DeptDivGrid.TextMatrix(intCounter, 3, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Division"))));
					DeptDivGrid.TextMatrix(intCounter, 4, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("ShortDescription"))));
					rsData.MoveNext();
				}
			}
			else
			{
				rsData.OpenRecordset("Select * from tblDeptDiv Order by Dept,Div", modGlobal.DatabaseNamePath);
				DeptDivGrid.Rows = rsData.RecordCount() + 1;
				for (intCounter = 1; intCounter <= rsData.RecordCount(); intCounter++)
				{
					DeptDivGrid.TextMatrix(intCounter, 0, FCConvert.ToString(rsData.Get_Fields_Int32("ID")));
					DeptDivGrid.TextMatrix(intCounter, 1, FCConvert.ToString(0));
					DeptDivGrid.TextMatrix(intCounter, 2, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Dept"))));
					DeptDivGrid.TextMatrix(intCounter, 3, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Div"))));
					DeptDivGrid.TextMatrix(intCounter, 4, Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Description"))));
					rsData.MoveNext();
				}
			}
			if (DeptDivGrid.Rows > 1)
			{
				DeptDivGrid.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 1, 2, DeptDivGrid.Rows - 1, DeptDivGrid.Cols - 1, FCGrid.AlignmentSettings.flexAlignLeftCenter);
			}
			boolDataChanged = false;
		}

		private void mnuDelete_Click(object sender, System.EventArgs e)
		{
			clsDRWrapper rsData = new clsDRWrapper();
			int intRow = 0;
			int intCol = 0;
			if (DeptDivGrid.Row > 0)
			{
				intRow = DeptDivGrid.Row;
				intCol = DeptDivGrid.Col;
				if (Conversion.Val(DeptDivGrid.TextMatrix(DeptDivGrid.Row, 0)) == 0 && Strings.Trim(DeptDivGrid.TextMatrix(DeptDivGrid.Row, 2)) == "" && Strings.Trim(DeptDivGrid.TextMatrix(DeptDivGrid.Row, 3)) == "")
				{
					DeptDivGrid.RemoveItem(DeptDivGrid.Row);
				}
				else
				{
					if (MessageBox.Show("Are you sure you wish to delete this item?", null, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
					{
						if (Conversion.Val(DeptDivGrid.TextMatrix(DeptDivGrid.Row, 0)) == 0)
						{
							DeptDivGrid.RemoveItem(DeptDivGrid.Row);
						}
						else
						{
							rsData.Execute("Delete from tblDeptDiv where ID = " + DeptDivGrid.TextMatrix(DeptDivGrid.Row, 0), modGlobal.DatabaseNamePath);
							DeptDivGrid.RemoveItem(DeptDivGrid.Row);
						}
						if (intRow <= DeptDivGrid.Rows - 1)
						{
							DeptDivGrid.Select(intRow, intCol);
						}
						else if (DeptDivGrid.Rows > 1)
						{
							DeptDivGrid.Select(intRow - 1, intCol);
						}
						MessageBox.Show("Delete completed successfully.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
					}
				}
			}
		}

		private void mnuExit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void mnuNew_Click(object sender, System.EventArgs e)
		{
			int intCounter;
			int lngNextNumber;
			clsDRWrapper rsData = new clsDRWrapper();
			DeptDivGrid.Rows += 1;
			DeptDivGrid.TextMatrix(DeptDivGrid.Rows - 1, 0, FCConvert.ToString(0));
			DeptDivGrid.TextMatrix(DeptDivGrid.Rows - 1, 1, FCConvert.ToString(1));
			DeptDivGrid.Select(DeptDivGrid.Rows - 1, 2);
		}

		public void mnuNew_Click()
		{
			mnuNew_Click(mnuNew, new System.EventArgs());
		}

		private void mnuSave_Click(object sender, System.EventArgs e)
		{
			DeptDivGrid.Select(0, 0);
			SaveRecord();
		}

		private void mnuSaveExit_Click(object sender, System.EventArgs e)
		{
			DeptDivGrid.Select(0, 0);
			if (SaveRecord())
				Close();
		}

		private void cmdNew_Click(object sender, EventArgs e)
		{
			mnuNew_Click();
		}

		private void cmdDelete_Click(object sender, EventArgs e)
		{
			mnuDelete_Click(sender, e);
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuSave_Click(sender, e);
		}
	}
}
