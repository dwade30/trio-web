﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using GrapeCity.ActiveReports;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmSetupDepraciationTotals.
	/// </summary>
	public partial class frmSetupDepraciationTotals : BaseForm
	{
		public frmSetupDepraciationTotals()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
			cmbAll.SelectedIndex = 0;
			cmbAllDate.SelectedIndex = 0;
			cmbDescription.SelectedIndex = 0;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static frmSetupDepraciationTotals InstancePtr
		{
			get
			{
				return (frmSetupDepraciationTotals)Sys.GetInstance(typeof(frmSetupDepraciationTotals));
			}
		}

		protected frmSetupDepraciationTotals _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		//=========================================================
		// ********************************************************
		// Property of TRIO Software Corporation
		// Written By
		// Date
		// ********************************************************
		// vbPorter upgrade warning: datReturnedDate As DateTime	OnWriteFCConvert.ToInt16(
		DateTime datReturnedDate;
		int SelectCol;
		int CodeCol;
		int DescriptionCol;

		private void cmdDiscardedEndDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEndDiscardedDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdDiscardedStartDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtStartDiscardedDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdEndDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEndDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdPurchaseEndDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtEndPurchaseDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdPurchaseStartDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtStartPurchaseDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void cmdStartDate_Click(object sender, System.EventArgs e)
		{
			datReturnedDate = DateTime.FromOADate(0);
			frmCalender.InstancePtr.Init(ref datReturnedDate);
			if (datReturnedDate.ToOADate() != 0)
			{
				txtStartDate.Text = Strings.Format(datReturnedDate, "MM/dd/yyyy");
			}
		}

		private void frmSetupDepraciationTotals_Activated(object sender, System.EventArgs e)
		{
			if (modGlobal.FormExist(this))
			{
				return;
			}
			this.Refresh();
		}

		private void frmSetupDepraciationTotals_Load(object sender, System.EventArgs e)
		{
			//Begin Unmaped Properties
			//frmSetupDepraciationTotals.FillStyle	= 0;
			//frmSetupDepraciationTotals.ScaleWidth	= 9045;
			//frmSetupDepraciationTotals.ScaleHeight	= 7470;
			//frmSetupDepraciationTotals.LinkTopic	= "Form2";
			//frmSetupDepraciationTotals.PaletteMode	= 1  'UseZOrder;
			//End Unmaped Properties
			clsDRWrapper rsInfo = new clsDRWrapper();
			SelectCol = 0;
			CodeCol = 1;
			DescriptionCol = 2;
			vsClass.TextMatrix(0, CodeCol, "Code");
			vsClass.TextMatrix(0, DescriptionCol, "Description");
			vsClass.ColDataType(SelectCol, FCGrid.DataTypeSettings.flexDTBoolean);
			rsInfo.OpenRecordset("SELECT * FROM tblClassCodes ORDER BY Code", "TWFA0000.vb1");
			if (rsInfo.EndOfFile() != true && rsInfo.BeginningOfFile() != true)
			{
				do
				{
					vsClass.Rows += 1;
					vsClass.TextMatrix(vsClass.Rows - 1, SelectCol, FCConvert.ToString(false));
					// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
					vsClass.TextMatrix(vsClass.Rows - 1, CodeCol, FCConvert.ToString(rsInfo.Get_Fields("Code")));
					vsClass.TextMatrix(vsClass.Rows - 1, DescriptionCol, FCConvert.ToString(rsInfo.Get_Fields_String("Description")));
					rsInfo.MoveNext();
				}
				while (rsInfo.EndOfFile() != true);
			}
			txtStartDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			txtEndDate.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			modGlobalFunctions.SetFixedSize(this, modGlobalConstants.TRIOWINDOWSIZEBIGGIE);
			modGlobalFunctions.SetTRIOColors(this);
            //FC:FINAL:PB: - issue #2141 increased column width
            vsClass.ColWidth(DescriptionCol, 4400);
        }

        private void frmSetupDepraciationTotals_KeyPress(object sender, Wisej.Web.KeyPressEventArgs e)
		{
			Keys KeyAscii = (Keys)Strings.Asc(e.KeyChar);
			// catches the escape and enter keys
			if (KeyAscii == Keys.Escape)
			{
				KeyAscii = (Keys)0;
				Close();
			}
			else if (KeyAscii == Keys.Return)
			{
				KeyAscii = (Keys)0;
				Support.SendKeys("{TAB}", false);
			}
			e.KeyChar = Strings.Chr(FCConvert.ToInt32(KeyAscii));
		}

		private void mnuFilePreview_Click(object sender, System.EventArgs e)
		{
			if (cmbAllDate.SelectedIndex == 0)
			{
				// do nothing
			}
			else if (cmbAllDate.SelectedIndex == 1)
			{
				if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
				{
					MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbAllDate.SelectedIndex == 2)
			{
				if (DateAndTime.DateValue(txtStartPurchaseDate.Text).ToOADate() > DateAndTime.DateValue(txtEndPurchaseDate.Text).ToOADate())
				{
					MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (DateAndTime.DateValue(txtStartDiscardedDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDiscardedDate.Text).ToOADate())
				{
					MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			this.Hide();
			frmReportViewer.InstancePtr.Init(rptDepreciationTotals.InstancePtr);
		}

		private void mnuFilePrint_Click(object sender, System.EventArgs e)
		{
			if (cmbAllDate.SelectedIndex == 0)
			{
				// do nothing
			}
			else if (cmbAllDate.SelectedIndex == 1)
			{
				if (DateAndTime.DateValue(txtStartDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDate.Text).ToOADate())
				{
					MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else if (cmbAllDate.SelectedIndex == 2)
			{
				if (DateAndTime.DateValue(txtStartPurchaseDate.Text).ToOADate() > DateAndTime.DateValue(txtEndPurchaseDate.Text).ToOADate())
				{
					MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			else
			{
				if (DateAndTime.DateValue(txtStartDiscardedDate.Text).ToOADate() > DateAndTime.DateValue(txtEndDiscardedDate.Text).ToOADate())
				{
					MessageBox.Show("Your beginning date must be earlier then your ending date", "Invalid Date Renage", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					return;
				}
			}
			rptDepreciationTotals.InstancePtr.PrintReport();
		}

		private void mnuProcessQuit_Click(object sender, System.EventArgs e)
		{
			Close();
		}

		private void optAll_CheckedChanged(object sender, System.EventArgs e)
		{
			int counter;
			fraSelected.Enabled = false;
			for (counter = 1; counter <= vsClass.Rows - 1; counter++)
			{
				vsClass.TextMatrix(counter, SelectCol, FCConvert.ToString(false));
			}
		}

		private void optAllDate_CheckedChanged(object sender, System.EventArgs e)
		{
			chkZeroCurDep.Enabled = true;
			txtStartPurchaseDate.Text = "";
			txtEndPurchaseDate.Text = "";
			txtStartPurchaseDate.Enabled = false;
			txtEndPurchaseDate.Enabled = false;
			lblToPurchase.Enabled = false;
			cmdPurchaseStartDate.Enabled = false;
			cmdPurchaseEndDate.Enabled = false;
			fraPurchaseDateRange.Enabled = false;
			txtStartDate.Text = "";
			txtEndDate.Text = "";
			txtStartDate.Enabled = false;
			txtEndDate.Enabled = false;
			lblTo.Enabled = false;
			cmdStartDate.Enabled = false;
			cmdEndDate.Enabled = false;
			fraDateRange.Enabled = false;
			txtStartDiscardedDate.Text = "";
			txtEndDiscardedDate.Text = "";
			txtStartDiscardedDate.Enabled = false;
			txtEndDiscardedDate.Enabled = false;
			lblToDiscarded.Enabled = false;
			cmdDiscardedStartDate.Enabled = false;
			cmdDiscardedEndDate.Enabled = false;
			fraDiscardedDateRange.Enabled = false;
		}

		private void optDepDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			chkZeroCurDep.CheckState = Wisej.Web.CheckState.Unchecked;
			chkZeroCurDep.Enabled = false;
			txtStartDiscardedDate.Text = "";
			txtEndDiscardedDate.Text = "";
			txtStartDiscardedDate.Enabled = false;
			txtEndDiscardedDate.Enabled = false;
			lblToDiscarded.Enabled = false;
			cmdDiscardedStartDate.Enabled = false;
			cmdDiscardedEndDate.Enabled = false;
			fraDiscardedDateRange.Enabled = false;
			txtStartPurchaseDate.Text = "";
			txtEndPurchaseDate.Text = "";
			txtStartPurchaseDate.Enabled = false;
			txtEndPurchaseDate.Enabled = false;
			lblToPurchase.Enabled = false;
			cmdPurchaseStartDate.Enabled = false;
			cmdPurchaseEndDate.Enabled = false;
			fraPurchaseDateRange.Enabled = false;
			txtStartDate.Enabled = true;
			txtEndDate.Enabled = true;
			lblTo.Enabled = true;
			cmdStartDate.Enabled = true;
			cmdEndDate.Enabled = true;
			fraDateRange.Enabled = true;
			txtStartDate.Focus();
		}

		private void optDiscardedDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			chkZeroCurDep.Enabled = true;
			txtStartDate.Text = "";
			txtEndDate.Text = "";
			txtStartDate.Enabled = false;
			txtEndDate.Enabled = false;
			lblTo.Enabled = false;
			cmdStartDate.Enabled = false;
			cmdEndDate.Enabled = false;
			fraDateRange.Enabled = false;
			txtStartPurchaseDate.Text = "";
			txtEndPurchaseDate.Text = "";
			txtStartPurchaseDate.Enabled = false;
			txtEndPurchaseDate.Enabled = false;
			lblToPurchase.Enabled = false;
			cmdPurchaseStartDate.Enabled = false;
			cmdPurchaseEndDate.Enabled = false;
			fraPurchaseDateRange.Enabled = false;
			txtStartDiscardedDate.Enabled = true;
			txtEndDiscardedDate.Enabled = true;
			lblToDiscarded.Enabled = true;
			cmdDiscardedStartDate.Enabled = true;
			cmdDiscardedEndDate.Enabled = true;
			fraDiscardedDateRange.Enabled = true;
			txtStartDiscardedDate.Focus();
		}

		private void optPurchaseDateRange_CheckedChanged(object sender, System.EventArgs e)
		{
			chkZeroCurDep.Enabled = true;
			txtStartDate.Text = "";
			txtEndDate.Text = "";
			txtStartDate.Enabled = false;
			txtEndDate.Enabled = false;
			lblTo.Enabled = false;
			cmdStartDate.Enabled = false;
			cmdEndDate.Enabled = false;
			fraDateRange.Enabled = false;
			txtStartDiscardedDate.Text = "";
			txtEndDiscardedDate.Text = "";
			txtStartDiscardedDate.Enabled = false;
			txtEndDiscardedDate.Enabled = false;
			lblToDiscarded.Enabled = false;
			cmdDiscardedStartDate.Enabled = false;
			cmdDiscardedEndDate.Enabled = false;
			fraDiscardedDateRange.Enabled = false;
			txtStartPurchaseDate.Enabled = true;
			txtEndPurchaseDate.Enabled = true;
			lblToPurchase.Enabled = true;
			cmdPurchaseStartDate.Enabled = true;
			cmdPurchaseEndDate.Enabled = true;
			fraPurchaseDateRange.Enabled = true;
			txtStartPurchaseDate.Focus();
		}

		private void optSelected_CheckedChanged(object sender, System.EventArgs e)
		{
			fraSelected.Enabled = true;
			vsClass.Focus();
		}

		private void vsClass_ClickEvent(object sender, System.EventArgs e)
		{
			if (vsClass.Row > 0)
			{
				if (FCUtils.CBool(vsClass.TextMatrix(vsClass.Row, SelectCol)) == false)
				{
					vsClass.TextMatrix(vsClass.Row, SelectCol, FCConvert.ToString(true));
				}
				else
				{
					vsClass.TextMatrix(vsClass.Row, SelectCol, FCConvert.ToString(false));
				}
			}
		}

		private void vsClass_KeyDownEvent(object sender, KeyEventArgs e)
		{
			Keys KeyCode = e.KeyCode;
			if (KeyCode == Keys.Space)
			{
				KeyCode = 0;
				if (vsClass.Row > 0)
				{
					if (FCUtils.CBool(vsClass.TextMatrix(vsClass.Row, SelectCol)) == false)
					{
						vsClass.TextMatrix(vsClass.Row, SelectCol, FCConvert.ToString(true));
					}
					else
					{
						vsClass.TextMatrix(vsClass.Row, SelectCol, FCConvert.ToString(false));
					}
				}
			}
		}

		private void cmdSave_Click(object sender, EventArgs e)
		{
			mnuFilePreview_Click(sender, e);
		}

		private void cmbAllDate_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (cmbAllDate.SelectedIndex)
			{
				case 0:
					optAllDate_CheckedChanged(sender, e);
					break;
				case 1:
					optDepDateRange_CheckedChanged(sender, e);
					break;
				case 2:
					optPurchaseDateRange_CheckedChanged(sender, e);
					break;
				case 3:
					optDiscardedDateRange_CheckedChanged(sender, e);
					break;
			}
		}

		private void cmbAll_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (cmbAll.SelectedIndex)
			{
				case 0:
					optAll_CheckedChanged(sender, e);
					break;
				case 1:
					optSelected_CheckedChanged(sender, e);
					break;
			}
		}

		private void cmdPrint_Click(object sender, EventArgs e)
		{
			mnuFilePrint_Click(sender, e);
		}
	}
}
