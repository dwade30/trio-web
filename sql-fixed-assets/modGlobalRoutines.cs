﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	public class modGlobalRoutines
	{
		// vbPorter upgrade warning: GridName As object	OnWrite(VSFlex7DAOCtl.VSFlexGrid)
		// vbPorter upgrade warning: strValue As string	OnWrite(string, double)
		public static bool DuplicateFound(int intColumnNumber, int intRowNumber, FCGrid GridName, string strValue)
		{
			bool DuplicateFound = false;
			int intCounter;
			if (Strings.Trim(strValue) != "")
			{
				for (intCounter = 1; intCounter <= GridName.Rows - 1; intCounter++)
				{
					if (intCounter != intRowNumber)
					{
						if (GridName.TextMatrix(intCounter, intColumnNumber) == strValue)
						{
							DuplicateFound = true;
							return DuplicateFound;
						}
					}
				}
			}
			DuplicateFound = false;
			return DuplicateFound;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string GetDateName(int intDateID)
		{
			string GetDateName = "";
			switch (intDateID)
			{
				case 1:
					{
						GetDateName = "January";
						break;
					}
				case 2:
					{
						GetDateName = "February";
						break;
					}
				case 3:
					{
						GetDateName = "March";
						break;
					}
				case 4:
					{
						GetDateName = "April";
						break;
					}
				case 5:
					{
						GetDateName = "May";
						break;
					}
				case 6:
					{
						GetDateName = "June";
						break;
					}
				case 7:
					{
						GetDateName = "July";
						break;
					}
				case 8:
					{
						GetDateName = "August";
						break;
					}
				case 9:
					{
						GetDateName = "September";
						break;
					}
				case 10:
					{
						GetDateName = "October";
						break;
					}
				case 11:
					{
						GetDateName = "November";
						break;
					}
				case 12:
					{
						GetDateName = "December";
						break;
					}
			}
			//end switch
			return GetDateName;
		}

		public static void GetBudFormats()
		{
			int intCounter;
			string strFormat = "";
			string strTemp = "";
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from Budgetary", "TWBD0000.vb1");
			if (!rsData.EndOfFile())
			{
				strTemp = FCConvert.ToString(rsData.Get_Fields_String("Expenditure"));
				for (intCounter = 0; intCounter <= 3; intCounter++)
				{
					if (Conversion.Val(Strings.Left(strTemp, 2)) > 0)
					{
						strFormat += Strings.StrDup(FCConvert.ToInt32(Strings.Left(strTemp, 2)), "_") + "-";
					}
					if (strTemp.Length > 2)
					{
						strTemp = Strings.Mid(strTemp, 3, strTemp.Length - 2);
					}
					else
					{
						strFormat = Strings.Left(strFormat, strFormat.Length - 1);
						break;
					}
				}
				//Statics.EFormat = "E " + strFormat;
				strFormat = string.Empty;
				strTemp = FCConvert.ToString(rsData.Get_Fields_String("Revenue"));
				for (intCounter = 0; intCounter <= 3; intCounter++)
				{
					if (Conversion.Val(Strings.Left(strTemp, 2)) > 0)
					{
						strFormat += Strings.StrDup(FCConvert.ToInt32(Strings.Left(strTemp, 2)), "_") + "-";
					}
					if (strTemp.Length > 2)
					{
						strTemp = Strings.Mid(strTemp, 3, strTemp.Length - 2);
					}
					else
					{
						strFormat = Strings.Left(strFormat, strFormat.Length - 1);
						break;
					}
				}
				//Statics.RFormat = "R " + strFormat;
				strFormat = string.Empty;
				strTemp = FCConvert.ToString(rsData.Get_Fields_String("Ledger"));
				for (intCounter = 0; intCounter <= 3; intCounter++)
				{
					if (Conversion.Val(Strings.Left(strTemp, 2)) > 0)
					{
						strFormat += Strings.StrDup(FCConvert.ToInt32(Strings.Left(strTemp, 2)), "_") + "-";
					}
					if (strTemp.Length > 2)
					{
						strTemp = Strings.Mid(strTemp, 3, strTemp.Length - 2);
					}
					else
					{
						strFormat = Strings.Left(strFormat, strFormat.Length - 1);
						break;
					}
				}
				//Statics.GFormat = "G " + strFormat;
			}
		}

		public static object GetCountyFromTown(int intTownName)
		{
			object GetCountyFromTown = null;
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from DefaultTowns where ID = " + FCConvert.ToString(intTownName), modGlobal.DatabaseNamePath);
			if (!rsData.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
				rsData.OpenRecordset("Select * from DefaultCounties where ID = " + rsData.Get_Fields("County"), modGlobal.DatabaseNamePath);
				if (!rsData.EndOfFile())
				{
					GetCountyFromTown = rsData.Get_Fields_String("Name");
				}
			}
			return GetCountyFromTown;
		}

		public static void GetCustomizeSettings()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from tblSettings", modGlobal.DatabaseNamePath);
			if (rsData.EndOfFile())
			{
				modVariables.Statics.boolVendorsFromBud = false;
				modVariables.Statics.boolDeptDivFromBud = false;
				modVariables.Statics.boolUseDivision = true;
				modStartup.Statics.strLoc1Title = "Location 1";
				modStartup.Statics.strLoc2Title = "Location 2";
				modStartup.Statics.strLoc3Title = "Location 3";
				modStartup.Statics.strLoc4Title = "Location 4";
			}
			else
			{
				modVariables.Statics.boolVendorsFromBud = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("Vendors"));
				// TODO Get_Fields: Check the table for the column [DeptDiv] and replace with corresponding Get_Field method
				modVariables.Statics.boolDeptDivFromBud = FCConvert.ToBoolean(rsData.Get_Fields("DeptDiv"));
				modVariables.Statics.boolUseDivision = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("DivisionUsed"));
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location1Caption"))) == "")
				{
					modStartup.Statics.strLoc1Title = "Location 1";
				}
				else
				{
					modStartup.Statics.strLoc1Title = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location1Caption")));
				}
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location2Caption"))) == "")
				{
					modStartup.Statics.strLoc2Title = "Location 2";
				}
				else
				{
					modStartup.Statics.strLoc2Title = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location2Caption")));
				}
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location3Caption"))) == "")
				{
					modStartup.Statics.strLoc3Title = "Location 3";
				}
				else
				{
					modStartup.Statics.strLoc3Title = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location3Caption")));
				}
				if (Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location4Caption"))) == "")
				{
					modStartup.Statics.strLoc4Title = "Location 4";
				}
				else
				{
					modStartup.Statics.strLoc4Title = Strings.Trim(FCConvert.ToString(rsData.Get_Fields_String("Location4Caption")));
				}
			}
		}

		public static void GetBudgetarySettings()
		{
			clsDRWrapper rsData = new clsDRWrapper();
			rsData.OpenRecordset("Select * from Modules", "SystemSettings");
			if (!rsData.EndOfFile())
			{
				modVariables.Statics.boolBudgetaryExists = FCConvert.ToBoolean(rsData.Get_Fields_Boolean("BD"));
			}
			else
			{
				modVariables.Statics.boolBudgetaryExists = false;
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string CheckForNA(string strValue)
		{
			string CheckForNA = "";
			if (Strings.Trim(strValue) == "N/A")
			{
				CheckForNA = "";
			}
			else
			{
				CheckForNA = Strings.Trim(strValue);
			}
			return CheckForNA;
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string CheckForNAReturnComma(string strValue)
		{
			string CheckForNAReturnComma = "";
			if (Strings.Trim(strValue) == "N/A")
			{
				CheckForNAReturnComma = "";
			}
			else
			{
				CheckForNAReturnComma = ", ";
			}
			return CheckForNAReturnComma;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object GetDefaultTownName(int intTown)
		{
			object GetDefaultTownName = null;
			clsDRWrapper rsD = new clsDRWrapper();
			rsD.OpenRecordset("SELECT * FROM DefaultTowns where ID = " + FCConvert.ToString(intTown), modGlobal.DEFAULTDATABASENAME);
			if (!rsD.EndOfFile())
			{
				GetDefaultTownName = rsD.Get_Fields_String("Name");
			}
			else
			{
				GetDefaultTownName = "OTHER";
			}
			return GetDefaultTownName;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(double, string)
		public static object GetCountyIndex(int intTown)
		{
			object GetCountyIndex = null;
			clsDRWrapper rsD = new clsDRWrapper();
			rsD.OpenRecordset("SELECT * FROM DefaultTowns where ID = " + FCConvert.ToString(intTown), modGlobal.DEFAULTDATABASENAME);
			if (!rsD.EndOfFile())
			{
				// TODO Get_Fields: Check the table for the column [County] and replace with corresponding Get_Field method
				GetCountyIndex = Conversion.Val(rsD.Get_Fields("County"));
			}
			else
			{
				GetCountyIndex = "OTHER";
			}
			return GetCountyIndex;
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object GetDefaultStateName(int intTown)
		{
			object GetDefaultStateName = null;
			clsDRWrapper rsD = new clsDRWrapper();
			rsD.OpenRecordset("SELECT * FROM States where ID = " + FCConvert.ToString(intTown), modGlobal.DEFAULTDATABASENAME);
			if (!rsD.EndOfFile())
			{
				GetDefaultStateName = rsD.Get_Fields_String("State");
			}
			else
			{
				GetDefaultStateName = "OTHER";
			}
			return GetDefaultStateName;
		}

		public static void GetAllTowns(ref FCComboBox ControlName)
		{
			try
			{
				// On Error GoTo ErrorHandler
				int i;
				clsDRWrapper rsTowns = new clsDRWrapper();
				rsTowns.OpenRecordset("SELECT * FROM DefaultTowns order by DefaultTowns.Name", modGlobal.DEFAULTDATABASENAME);
				ControlName.Clear();
				// ControlName.AddItem "OTHER"
				// ControlName.ItemData(ControlName.NewIndex) = 0
				while (!rsTowns.EndOfFile())
				{
					ControlName.AddItem(rsTowns.Get_Fields_String("Name"));
					ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsTowns.Get_Fields_Int32("ID")));
					rsTowns.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		public static void GetAllCounties(ref FCComboBox ControlName)
		{
			try
			{
				// On Error GoTo ErrorHandler
				int i;
				clsDRWrapper rsTowns = new clsDRWrapper();
				rsTowns.OpenRecordset("SELECT * FROM DefaultCounties order by Name", modGlobal.DEFAULTDATABASENAME);
				ControlName.Clear();
				// ControlName.AddItem "OTHER"
				// ControlName.ItemData(ControlName.NewIndex) = 0
				while (!rsTowns.EndOfFile())
				{
					ControlName.AddItem(rsTowns.Get_Fields_String("Name"));
					ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsTowns.Get_Fields_Int32("ID")));
					rsTowns.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		////public static void ShowWaitMessage(ref Form FormName, short intSleepTime = 600)
		////{
		////	modGlobal.Sleep(intSleepTime);
		////}

		public static void SetComboByIndex(ref FCComboBox ControlName, int intIndex)
		{
			int intCounter;
			if (intIndex == -1)
			{
				if (ControlName.Items.Count > 0)
				{
					ControlName.SelectedIndex = 0;
				}
			}
			else
			{
				for (intCounter = 0; intCounter <= ControlName.Items.Count - 1; intCounter++)
				{
					if (ControlName.ItemData(intCounter) == intIndex)
					{
						ControlName.SelectedIndex = intCounter;
						break;
					}
				}
				if (intCounter == ControlName.Items.Count)
				{
					if (ControlName.Items.Count > 0)
						ControlName.SelectedIndex = 0;
				}
			}
		}

		public static void SetComboByName(ref FCComboBox ControlName, string strIndex)
		{
			int intCounter;
			if (strIndex == string.Empty)
			{
			}
			else
			{
				for (intCounter = 0; intCounter <= ControlName.Items.Count - 1; intCounter++)
				{
					if (ControlName.Items[intCounter].ToString() == strIndex)
					{
						ControlName.SelectedIndex = intCounter;
						break;
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string FixQuote(string strValue)
		{
			string FixQuote = "";
			FixQuote = strValue.Replace("'", "''");
			return FixQuote;
		}

		public static void LoadStates(ref FCComboBox ControlName)
		{
			try
			{
				// On Error GoTo ErrorHandler
				int i;
				clsDRWrapper rsState = new clsDRWrapper();
				rsState.OpenRecordset("SELECT * FROM States order by state", modGlobal.DEFAULTDATABASENAME);
				ControlName.Clear();
				while (!rsState.EndOfFile())
				{
					ControlName.AddItem(rsState.Get_Fields_String("State") + " - " + rsState.Get_Fields_String("Description"));
					ControlName.ItemData(ControlName.NewIndex, FCConvert.ToInt32(rsState.Get_Fields_Int32("ID")));
					rsState.MoveNext();
				}
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
			}
		}

		//public class StaticVariables
		//{
		//	//=========================================================
		//	// string EFormat = "";
		//	//public string GFormat = "";
		//	// string RFormat = "";
		//}

		//public static StaticVariables Statics
		//{
		//	get
		//	{
		//		return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
		//	}
		//}
	}
}
