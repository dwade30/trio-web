﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Wisej.Core;
using Global;
using fecherFoundation.VisualBasicLayer;
using SharedApplication.Enums;
using TWSharedLibrary;
using SharedApplication.Models;
using SharedApplication.FixedAssets;

namespace TWFA0000
{
	public class modStartup
	{
		//=========================================================
		// Security Constants
		public const int ADDASSET = 10;
		public const int DELETEASSET = 12;
		public const int EDITASSET = 11;
		public const int FILEMAINTENANCE = 2;
		public const int ADDCLASS = 5;
		public const int ADDDEPT = 6;
		public const int ADDLOCATIONS = 4;
		public const int ADDVENDORS = 7;
		public const int CUSTOMIZE = 3;
		public const int POSTDEPRECIATION = 9;
		public const int PRINTING = 8;
		public const int AUTOPOSTDEPRECIATIONJOURNAL = 13;
		public const string DEFAULTDATABASE = "TWFA0000.VB1";

		public static clsDRWrapper rsDBConnection
		{
			get
			{
				if (Statics.rsDBConnection_AutoInitialized == null)
				{
					Statics.rsDBConnection_AutoInitialized = new clsDRWrapper();
				}
				return Statics.rsDBConnection_AutoInitialized;
			}
			set
			{
				Statics.rsDBConnection_AutoInitialized = value;
			}
		}

		public static void Main()
		{
			bool found;
			try
			{
				// On Error GoTo ErrorHandler
				Statics.boolError = false;
				// kk04142015 troges-39  Change to single config file
				if (!modGlobalFunctions.LoadSQLConfig("TWFA0000.VB1"))
				{
					MessageBox.Show("Error loading connection information", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					return;
				}
				clsDRWrapper rs = new clsDRWrapper();
				modGlobalFunctions.UpdateUsedModules();
				if (!modGlobalConstants.Statics.gboolFA)
				{
					MessageBox.Show("Fixed Assets has expired.  Please call TRIO!", "Module Expired", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
				}
				if (modGlobalConstants.Statics.gboolBD)
				{
					rsDBConnection.OpenRecordset("SELECT * FROM Budgetary", "TWBD0000.vb1");
				}
				modGlobalFunctions.GetGlobalRegistrySetting();
				modReplaceWorkFiles.GetGlobalVariables();
				modGlobalRoutines.GetBudgetarySettings();
				modNewAccountBox.GetFormats();
				if (modVariables.Statics.boolBudgetaryExists)
				{
					modValidateAccount.SetBDFormats();
				}
				
				modGlobalFunctions.GetGlobalRegistrySetting();
				
				
				modReplaceWorkFiles.Statics.gstrNetworkFlag = (modRegistry.GetRegistryKey("NetworkFlag")) != string.Empty ? modReplaceWorkFiles.Statics.gstrReturn : "Y";
				modReplaceWorkFiles.Statics.gintSecurityID = TWSharedLibrary.Variables.Statics.IntUserID;
				modGlobal.Statics.clsFASecurity = new clsTrioSecurity();
				modGlobalConstants.Statics.MuniName = Strings.UCase(Strings.Trim(modGlobalConstants.Statics.MuniName));
				//modGlobal.Statics.TrioSDrive = Strings.Trim(modGlobal.Statics.TrioSDrive);
				modGlobal.Statics.clsFASecurity.Init("FA");
				modGlobalRoutines.GetCustomizeSettings();
				if (Strings.Right(FCFileSystem.Statics.UserDataFolder, 1) == "\\")
				{
					App.HelpFile = FCFileSystem.Statics.UserDataFolder + "TWFA0000.hlp";
				}
				else
				{
					App.HelpFile = FCFileSystem.Statics.UserDataFolder + "\\TWFA0000.hlp";
				}
				// this routine will validate that the user has all the fields
				// in the database. If they do not exist then it will add them
				rs.OpenRecordset("Select Version from tblSettings", "Twfa0000.vb1");
				bool executeCheckFields = false;
				if (!rs.EndOfFile())
				{
					if (Strings.Trim(rs.Get_Fields_String("Version") + " ") != FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision))
					{
						executeCheckFields = true;
						goto CheckFields;
					}
				}
				else
				{
					executeCheckFields = true;
					goto CheckFields;
				}
				CheckFields:
				;
				if (executeCheckFields)
				{
					rs.Execute("Delete from tblCustomReports", modGlobal.DatabaseNamePath);
					rs.Execute("Update tblSettings set Version = '" + FCConvert.ToString(App.Major) + "." + FCConvert.ToString(App.Minor) + "." + FCConvert.ToString(App.Revision) + "'", "Twfa0000.vb1");
					FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				}
                var dbInfo = new SQLConfigInfo()
                {
                    ServerInstance = StaticSettings.gGlobalSettings.DataSource,
                    UserName = StaticSettings.gGlobalSettings.UserName,
                    Password = StaticSettings.gGlobalSettings.Password
                };
                StaticSettings.GlobalCommandDispatcher.Send(new UpdateFixedAssetsDatabase(dbInfo, StaticSettings.gGlobalSettings.DataEnvironment));

				modGlobalConstants.Statics.gstrUserID = FCConvert.ToString(modGlobal.Statics.clsFASecurity.Get_UsersUserID());
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				MDIParent.InstancePtr.Init();
				return;
			}
			catch (Exception ex)
			{
				// ErrorHandler:
				FCGlobal.Screen.MousePointer = MousePointerConstants.vbDefault;
				if (Information.Err(ex).Number == 3078)
				{
					MessageBox.Show("Invalid General Entry database structure.", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Hand);
					Application.Exit();
				}
				else
				{
					MessageBox.Show(FCConvert.ToString(Information.Err(ex).Number) + " " + Information.Err(ex).Description);
				}
			}
		}

		public class StaticVariables
		{
            public bool moduleInitialized = false;
			public string strLoc1Title = "";
			public string strLoc2Title = "";
			public string strLoc3Title = "";
			public string strLoc4Title = "";
			public bool boolError;
			public clsDRWrapper rsDBConnection_AutoInitialized;
			public string strReturnedValidAccount = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
