//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmCustomReport.
	/// </summary>
	partial class frmCustomReport : BaseForm
	{
		public fecherFoundation.FCComboBox cmbPortrait;
		public fecherFoundation.FCLabel lblPortrait;
		public fecherFoundation.FCComboBox cmbReport;
		public fecherFoundation.FCLabel lblReport;
		public fecherFoundation.FCFrame fraClassCodes;
		public fecherFoundation.FCButton cmdCancel;
		public fecherFoundation.FCButton cmdOK;
		public fecherFoundation.FCGrid vsClassCodes;
		public fecherFoundation.FCFrame fraWhere;
		public fecherFoundation.FCGrid vsWhere;
		public fecherFoundation.FCButton cmdClear;
		public fecherFoundation.FCFrame fraReports;
		public fecherFoundation.FCTextBox txtHeading;
		public fecherFoundation.FCButton cmdAdd;
		public fecherFoundation.FCComboBox cboSavedReport;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame fraSort;
		public fecherFoundation.FCListBox lstSort;
		public fecherFoundation.FCFrame fraFields;
		public fecherFoundation.FCListBox lstFields;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCGrid vsLayout;
		public fecherFoundation.FCFrame fraMessage;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCPictureBox Image1;
		public Wisej.Web.ImageList ImageList1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuClear;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuAddRow;
		public fecherFoundation.FCToolStripMenuItem mnuAddColumn;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteRow;
		public fecherFoundation.FCToolStripMenuItem mnuDeleteColumn;
		public fecherFoundation.FCToolStripMenuItem mnuSP21;
		public fecherFoundation.FCToolStripMenuItem mnuPrint;
		public fecherFoundation.FCToolStripMenuItem mnuPrintPreview;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCustomReport));
            Wisej.Web.ImageListEntry imageListEntry1 = new Wisej.Web.ImageListEntry(((System.Drawing.Image)(resources.GetObject("ImageList1.Images"))));
            this.cmbPortrait = new fecherFoundation.FCComboBox();
            this.lblPortrait = new fecherFoundation.FCLabel();
            this.cmbReport = new fecherFoundation.FCComboBox();
            this.lblReport = new fecherFoundation.FCLabel();
            this.fraClassCodes = new fecherFoundation.FCFrame();
            this.cmdCancel = new fecherFoundation.FCButton();
            this.cmdOK = new fecherFoundation.FCButton();
            this.vsClassCodes = new fecherFoundation.FCGrid();
            this.fraWhere = new fecherFoundation.FCFrame();
            this.vsWhere = new fecherFoundation.FCGrid();
            this.cmdClear = new fecherFoundation.FCButton();
            this.fraReports = new fecherFoundation.FCFrame();
            this.txtHeading = new fecherFoundation.FCTextBox();
            this.cmdAdd = new fecherFoundation.FCButton();
            this.cboSavedReport = new fecherFoundation.FCComboBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.fraSort = new fecherFoundation.FCFrame();
            this.lstSort = new fecherFoundation.FCListBox();
            this.fraFields = new fecherFoundation.FCFrame();
            this.lstFields = new fecherFoundation.FCListBox();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.vsLayout = new fecherFoundation.FCGrid();
            this.fraMessage = new fecherFoundation.FCFrame();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.ImageList1 = new Wisej.Web.ImageList(this.components);
            this.cmdAddColumn = new fecherFoundation.FCButton();
            this.cmdAddRow = new fecherFoundation.FCButton();
            this.cmdDeleteColumn = new fecherFoundation.FCButton();
            this.cmdDeleteRow = new fecherFoundation.FCButton();
            this.cmdPrint = new fecherFoundation.FCButton();
            this.mnuAddRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClear = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuAddColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteRow = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeleteColumn = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP21 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuPrintPreview = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.customProperties1 = new Wisej.Web.Ext.CustomProperties.CustomProperties(this.components);
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraClassCodes)).BeginInit();
            this.fraClassCodes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsClassCodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).BeginInit();
            this.fraWhere.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReports)).BeginInit();
            this.fraReports.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).BeginInit();
            this.fraSort.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).BeginInit();
            this.fraFields.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).BeginInit();
            this.vsLayout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).BeginInit();
            this.fraMessage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdPrint);
            this.BottomPanel.Location = new System.Drawing.Point(0, 668);
            this.BottomPanel.Size = new System.Drawing.Size(1068, 96);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraReports);
            this.ClientArea.Controls.Add(this.fraClassCodes);
            this.ClientArea.Controls.Add(this.cmbPortrait);
            this.ClientArea.Controls.Add(this.fraWhere);
            this.ClientArea.Controls.Add(this.lblPortrait);
            this.ClientArea.Controls.Add(this.fraSort);
            this.ClientArea.Controls.Add(this.fraFields);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Size = new System.Drawing.Size(1088, 640);
            this.ClientArea.Controls.SetChildIndex(this.Frame1, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraFields, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraSort, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPortrait, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraWhere, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbPortrait, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraClassCodes, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraReports, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdDeleteRow);
            this.TopPanel.Controls.Add(this.cmdDeleteColumn);
            this.TopPanel.Controls.Add(this.cmdAddRow);
            this.TopPanel.Controls.Add(this.cmdAddColumn);
            this.TopPanel.Controls.Add(this.cmdClear);
            this.TopPanel.Size = new System.Drawing.Size(1088, 60);
            this.TopPanel.Controls.SetChildIndex(this.cmdClear, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdAddRow, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteColumn, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDeleteRow, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(165, 28);
            this.HeaderText.Text = "Custom Report";
            // 
            // cmbPortrait
            // 
            this.cmbPortrait.Items.AddRange(new object[] {
            "Portrait",
            "Landscape"});
            this.cmbPortrait.Location = new System.Drawing.Point(861, 460);
            this.cmbPortrait.Name = "cmbPortrait";
            this.cmbPortrait.Size = new System.Drawing.Size(185, 40);
            this.cmbPortrait.TabIndex = 1;
            this.cmbPortrait.SelectedIndexChanged += new System.EventHandler(this.cmdPortrait_CheckedChanged);
            // 
            // lblPortrait
            // 
            this.lblPortrait.AutoSize = true;
            this.lblPortrait.Location = new System.Drawing.Point(667, 474);
            this.lblPortrait.Name = "lblPortrait";
            this.lblPortrait.Size = new System.Drawing.Size(151, 15);
            this.lblPortrait.TabIndex = 1001;
            this.lblPortrait.Text = "REPORT ORIENTATION";
            // 
            // cmbReport
            // 
            this.cmbReport.Items.AddRange(new object[] {
            "Create / Edit Report",
            "Show Saved Report",
            "Delete Saved Report"});
            this.cmbReport.Location = new System.Drawing.Point(136, 25);
            this.cmbReport.Name = "cmbReport";
            this.cmbReport.Size = new System.Drawing.Size(219, 40);
            this.cmbReport.TabIndex = 1;
            this.cmbReport.SelectedIndexChanged += new System.EventHandler(this.optReport_CheckedChanged);
            // 
            // lblReport
            // 
            this.lblReport.AutoSize = true;
            this.lblReport.Location = new System.Drawing.Point(20, 35);
            this.lblReport.Name = "lblReport";
            this.lblReport.Size = new System.Drawing.Size(58, 15);
            this.lblReport.TabIndex = 6;
            this.lblReport.Text = "REPORT";
            // 
            // fraClassCodes
            // 
            this.fraClassCodes.Controls.Add(this.cmdCancel);
            this.fraClassCodes.Controls.Add(this.cmdOK);
            this.fraClassCodes.Controls.Add(this.vsClassCodes);
            this.fraClassCodes.Location = new System.Drawing.Point(700, 30);
            this.fraClassCodes.Name = "fraClassCodes";
            this.fraClassCodes.Size = new System.Drawing.Size(345, 242);
            this.fraClassCodes.TabIndex = 1;
            this.fraClassCodes.Text = "Select Class Codes";
            this.fraClassCodes.Visible = false;
            // 
            // cmdCancel
            // 
            this.cmdCancel.AppearanceKey = "actionButton";
            this.cmdCancel.Location = new System.Drawing.Point(125, 182);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(86, 40);
            this.cmdCancel.TabIndex = 2;
            this.cmdCancel.Text = "Cancel";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOK
            // 
            this.cmdOK.AppearanceKey = "actionButton";
            this.cmdOK.Location = new System.Drawing.Point(20, 182);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(85, 40);
            this.cmdOK.TabIndex = 1;
            this.cmdOK.Text = "OK";
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // vsClassCodes
            // 
            this.vsClassCodes.Cols = 3;
            this.vsClassCodes.FixedCols = 0;
            this.vsClassCodes.Location = new System.Drawing.Point(20, 30);
            this.vsClassCodes.Name = "vsClassCodes";
            this.vsClassCodes.RowHeadersVisible = false;
            this.vsClassCodes.Rows = 1;
            this.vsClassCodes.ShowFocusCell = false;
            this.vsClassCodes.Size = new System.Drawing.Size(305, 132);
            this.vsClassCodes.TabIndex = 3;
            this.vsClassCodes.Click += new System.EventHandler(this.vsClassCodes_ClickEvent);
            this.vsClassCodes.KeyDown += new Wisej.Web.KeyEventHandler(this.vsClassCodes_KeyDownEvent);
            // 
            // fraWhere
            // 
            this.fraWhere.Controls.Add(this.vsWhere);
            this.fraWhere.Location = new System.Drawing.Point(30, 463);
            this.fraWhere.Name = "fraWhere";
            this.fraWhere.Size = new System.Drawing.Size(608, 205);
            this.fraWhere.TabIndex = 5;
            this.fraWhere.Text = "Selection Criteria";
            this.fraWhere.DoubleClick += new System.EventHandler(this.fraWhere_DoubleClick);
            // 
            // vsWhere
            // 
            this.vsWhere.Cols = 10;
            this.vsWhere.ColumnHeadersVisible = false;
            this.vsWhere.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsWhere.FixedRows = 0;
            this.vsWhere.Location = new System.Drawing.Point(20, 30);
            this.vsWhere.Name = "vsWhere";
            this.vsWhere.ReadOnly = false;
            this.vsWhere.Rows = 0;
            this.vsWhere.ShowFocusCell = false;
            this.vsWhere.Size = new System.Drawing.Size(568, 155);
            this.vsWhere.StandardTab = false;
            this.vsWhere.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsWhere.TabIndex = 0;
            this.vsWhere.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsWhere_AfterEdit);
            this.vsWhere.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsWhere_BeforeEdit);
            this.vsWhere.CellValidating += new Wisej.Web.DataGridViewCellValidatingEventHandler(this.vsWhere_ValidateEdit);
            this.vsWhere.CurrentCellChanged += new System.EventHandler(this.vsWhere_RowColChange);
            // 
            // cmdClear
            // 
            this.cmdClear.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdClear.Location = new System.Drawing.Point(949, 29);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(97, 24);
            this.cmdClear.TabIndex = 6;
            this.cmdClear.Text = "Clear Criteria";
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // fraReports
            // 
            this.fraReports.Controls.Add(this.txtHeading);
            this.fraReports.Controls.Add(this.cmbReport);
            this.fraReports.Controls.Add(this.lblReport);
            this.fraReports.Controls.Add(this.cmdAdd);
            this.fraReports.Controls.Add(this.cboSavedReport);
            this.fraReports.Controls.Add(this.Label4);
            this.fraReports.Location = new System.Drawing.Point(707, 247);
            this.fraReports.Name = "fraReports";
            this.fraReports.Size = new System.Drawing.Size(379, 185);
            this.fraReports.TabIndex = 4;
            this.fraReports.Text = "Report";
            // 
            // txtHeading
            // 
            this.txtHeading.BackColor = System.Drawing.SystemColors.Window;
            this.txtHeading.Location = new System.Drawing.Point(136, 126);
            this.txtHeading.Name = "txtHeading";
            this.txtHeading.Size = new System.Drawing.Size(219, 40);
            this.txtHeading.TabIndex = 5;
            // 
            // cmdAdd
            // 
            this.cmdAdd.AppearanceKey = "actionButton";
            this.cmdAdd.Location = new System.Drawing.Point(20, 75);
            this.cmdAdd.Name = "cmdAdd";
            this.cmdAdd.Size = new System.Drawing.Size(110, 40);
            this.cmdAdd.TabIndex = 3;
            this.cmdAdd.Text = "Save Report";
            this.cmdAdd.Click += new System.EventHandler(this.cmdAdd_Click);
            // 
            // cboSavedReport
            // 
            this.cboSavedReport.BackColor = System.Drawing.SystemColors.Window;
            this.cboSavedReport.Location = new System.Drawing.Point(136, 75);
            this.cboSavedReport.Name = "cboSavedReport";
            this.cboSavedReport.Size = new System.Drawing.Size(217, 40);
            this.cboSavedReport.TabIndex = 2;
            this.cboSavedReport.Visible = false;
            this.cboSavedReport.SelectedIndexChanged += new System.EventHandler(this.cboSavedReport_SelectedIndexChanged);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 137);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(61, 15);
            this.Label4.TabIndex = 4;
            this.Label4.Text = "HEADING";
            // 
            // fraSort
            // 
            this.fraSort.Controls.Add(this.lstSort);
            this.fraSort.Location = new System.Drawing.Point(344, 247);
            this.fraSort.Name = "fraSort";
            this.fraSort.Size = new System.Drawing.Size(343, 186);
            this.fraSort.TabIndex = 3;
            this.fraSort.Text = "Fields To Sort By";
            this.fraSort.DoubleClick += new System.EventHandler(this.fraSort_DoubleClick);
            // 
            // lstSort
            // 
            this.lstSort.AllowDrag = true;
            this.lstSort.AllowDrop = true;
            this.lstSort.BackColor = System.Drawing.SystemColors.Window;
            this.lstSort.CheckBoxes = true;
            this.lstSort.Location = new System.Drawing.Point(20, 30);
            this.lstSort.Name = "lstSort";
            this.lstSort.Size = new System.Drawing.Size(303, 136);
            this.lstSort.Style = 1;
            this.lstSort.TabIndex = 0;
            this.lstSort.MouseEnter += new System.EventHandler(this.lstSort_MouseEnter);
            this.lstSort.MouseLeave += new System.EventHandler(this.lstSort_MouseLeave);
            this.lstSort.DragDrop += new Wisej.Web.DragEventHandler(this.lstSort_DragDrop);
            // 
            // fraFields
            // 
            this.fraFields.Controls.Add(this.lstFields);
            this.fraFields.Location = new System.Drawing.Point(30, 247);
            this.fraFields.Name = "fraFields";
            this.fraFields.Size = new System.Drawing.Size(294, 186);
            this.fraFields.TabIndex = 2;
            this.fraFields.Text = "Fields To Display On Report";
            // 
            // lstFields
            // 
            this.lstFields.BackColor = System.Drawing.SystemColors.Window;
            this.lstFields.Location = new System.Drawing.Point(20, 30);
            this.lstFields.Name = "lstFields";
            this.lstFields.Size = new System.Drawing.Size(254, 136);
            this.lstFields.TabIndex = 0;
            this.lstFields.SelectedIndexChanged += new System.EventHandler(this.lstFields_SelectedIndexChanged);
            this.lstFields.MouseDown += new Wisej.Web.MouseEventHandler(this.lstFields_MouseDown);
            this.lstFields.MouseUp += new Wisej.Web.MouseEventHandler(this.lstFields_MouseUp);
            // 
            // Frame1
            // 
            this.Frame1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Frame1.AppearanceKey = "groupBoxNoBorders";
            this.Frame1.Controls.Add(this.vsLayout);
            this.Frame1.Controls.Add(this.Image1);
            this.Frame1.Location = new System.Drawing.Point(10, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1058, 239);
            this.Frame1.TabIndex = 1002;
            // 
            // vsLayout
            // 
            this.vsLayout.AllowUserResizing = fecherFoundation.FCGrid.AllowUserResizeSettings.flexResizeColumns;
            this.vsLayout.AllowUserToOrderColumns = true;
            this.vsLayout.AllowUserToResizeColumns = true;
            this.vsLayout.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.vsLayout.Controls.Add(this.fraMessage);
            this.vsLayout.Editable = fecherFoundation.FCGrid.EditableSettings.flexEDKbdMouse;
            this.vsLayout.ExplorerBar = fecherFoundation.FCGrid.ExplorerBarSettings.flexExMove;
            this.vsLayout.FixedCols = 0;
            this.vsLayout.Location = new System.Drawing.Point(20, 57);
            this.vsLayout.Name = "vsLayout";
            this.vsLayout.ReadOnly = false;
            this.vsLayout.RowHeadersVisible = false;
            this.vsLayout.RowHeadersWidthSizeMode = Wisej.Web.DataGridViewRowHeadersWidthSizeMode.EnableResizing;
            this.vsLayout.Rows = 1;
            this.vsLayout.ShowFocusCell = false;
            this.vsLayout.Size = new System.Drawing.Size(1012, 161);
            this.vsLayout.SubtotalPosition = fecherFoundation.FCGrid.SubtotalPositionSettings.flexSTAbove;
            this.vsLayout.TabIndex = 2;
            this.vsLayout.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsLayout_AfterEdit);
            this.vsLayout.CellMouseDown += new Wisej.Web.DataGridViewCellMouseEventHandler(this.vsLayout_MouseDownEvent);
            // 
            // fraMessage
            // 
            this.fraMessage.Anchor = ((Wisej.Web.AnchorStyles)((((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Bottom) 
            | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.fraMessage.AppearanceKey = "groupBoxNoBorders";
            this.fraMessage.Controls.Add(this.Label3);
            this.fraMessage.Location = new System.Drawing.Point(-1, -1);
            this.fraMessage.Name = "fraMessage";
            this.fraMessage.Size = new System.Drawing.Size(1012, 161);
            this.fraMessage.TabIndex = 0;
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(6, 11);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(514, 45);
            this.Label3.TabIndex = 0;
            this.Label3.Text = "THIS REPORT IS PRE-DEFINED. SOME AREAS ON THIS FORM MAY NOT BE ACCESSIBLE";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Image1
            // 
            this.Image1.Anchor = ((Wisej.Web.AnchorStyles)(((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Left) 
            | Wisej.Web.AnchorStyles.Right)));
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.Image = ((System.Drawing.Image)(resources.GetObject("Image1.Image")));
            this.Image1.Location = new System.Drawing.Point(20, 25);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(1012, 30);
            // 
            // ImageList1
            // 
            this.ImageList1.Images.AddRange(new Wisej.Web.ImageListEntry[] {
            imageListEntry1});
            this.ImageList1.ImageSize = new System.Drawing.Size(256, 30);
            this.ImageList1.TransparentColor = System.Drawing.Color.FromArgb(192, 192, 192);
            // 
            // cmdAddColumn
            // 
            this.cmdAddColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddColumn.Location = new System.Drawing.Point(853, 29);
            this.cmdAddColumn.Name = "cmdAddColumn";
            this.cmdAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.cmdAddColumn.Size = new System.Drawing.Size(93, 24);
            this.cmdAddColumn.TabIndex = 8;
            this.cmdAddColumn.Text = "Add Column";
            this.cmdAddColumn.Click += new System.EventHandler(this.cmdAddColumn_Click);
            // 
            // cmdAddRow
            // 
            this.cmdAddRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdAddRow.Location = new System.Drawing.Point(778, 29);
            this.cmdAddRow.Name = "cmdAddRow";
            this.cmdAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.cmdAddRow.Size = new System.Drawing.Size(72, 24);
            this.cmdAddRow.TabIndex = 9;
            this.cmdAddRow.Text = "Add Row";
            this.cmdAddRow.Click += new System.EventHandler(this.cmdAddRow_Click);
            // 
            // cmdDeleteColumn
            // 
            this.cmdDeleteColumn.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteColumn.Location = new System.Drawing.Point(666, 29);
            this.cmdDeleteColumn.Name = "cmdDeleteColumn";
            this.cmdDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.cmdDeleteColumn.Size = new System.Drawing.Size(108, 24);
            this.cmdDeleteColumn.TabIndex = 10;
            this.cmdDeleteColumn.Text = "Delete Column";
            this.cmdDeleteColumn.Click += new System.EventHandler(this.cmdDeleteColumn_Click);
            // 
            // cmdDeleteRow
            // 
            this.cmdDeleteRow.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDeleteRow.Location = new System.Drawing.Point(574, 29);
            this.cmdDeleteRow.Name = "cmdDeleteRow";
            this.cmdDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdDeleteRow.Size = new System.Drawing.Size(88, 24);
            this.cmdDeleteRow.TabIndex = 11;
            this.cmdDeleteRow.Text = "Delete Row";
            this.cmdDeleteRow.Click += new System.EventHandler(this.cmdDeleteRow_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.AppearanceKey = "acceptButton";
            this.cmdPrint.Location = new System.Drawing.Point(532, 30);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrint.Size = new System.Drawing.Size(147, 48);
            this.cmdPrint.TabIndex = 2;
            this.cmdPrint.Text = "Print Preview";
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // mnuAddRow
            // 
            this.mnuAddRow.Index = 2;
            this.mnuAddRow.Name = "mnuAddRow";
            this.mnuAddRow.Shortcut = Wisej.Web.Shortcut.F2;
            this.mnuAddRow.Text = "Add Row                            ";
            this.mnuAddRow.Click += new System.EventHandler(this.mnuAddRow_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuClear
            // 
            this.mnuClear.Index = 0;
            this.mnuClear.Name = "mnuClear";
            this.mnuClear.Text = "Clear &Selection Criteria";
            this.mnuClear.Click += new System.EventHandler(this.mnuClear_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuClear,
            this.mnuSP2,
            this.mnuAddRow,
            this.mnuAddColumn,
            this.mnuDeleteRow,
            this.mnuDeleteColumn,
            this.mnuSP21,
            this.mnuPrint,
            this.mnuPrintPreview,
            this.mnuSP1,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuAddColumn
            // 
            this.mnuAddColumn.Index = 3;
            this.mnuAddColumn.Name = "mnuAddColumn";
            this.mnuAddColumn.Shortcut = Wisej.Web.Shortcut.F3;
            this.mnuAddColumn.Text = "Add Column";
            this.mnuAddColumn.Click += new System.EventHandler(this.mnuAddColumn_Click);
            // 
            // mnuDeleteRow
            // 
            this.mnuDeleteRow.Index = 4;
            this.mnuDeleteRow.Name = "mnuDeleteRow";
            this.mnuDeleteRow.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuDeleteRow.Text = "Delete Row";
            this.mnuDeleteRow.Click += new System.EventHandler(this.mnuDeleteRow_Click);
            // 
            // mnuDeleteColumn
            // 
            this.mnuDeleteColumn.Index = 5;
            this.mnuDeleteColumn.Name = "mnuDeleteColumn";
            this.mnuDeleteColumn.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuDeleteColumn.Text = "Delete Column";
            this.mnuDeleteColumn.Click += new System.EventHandler(this.mnuDeleteColumn_Click);
            // 
            // mnuSP21
            // 
            this.mnuSP21.Index = 6;
            this.mnuSP21.Name = "mnuSP21";
            this.mnuSP21.Text = "-";
            // 
            // mnuPrint
            // 
            this.mnuPrint.Index = 7;
            this.mnuPrint.Name = "mnuPrint";
            this.mnuPrint.Text = "Print";
            this.mnuPrint.Click += new System.EventHandler(this.mnuPrint_Click);
            // 
            // mnuPrintPreview
            // 
            this.mnuPrintPreview.Index = 8;
            this.mnuPrintPreview.Name = "mnuPrintPreview";
            this.mnuPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuPrintPreview.Text = "Print / Preview";
            this.mnuPrintPreview.Click += new System.EventHandler(this.mnuPrintPreview_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 9;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 10;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // frmCustomReport
            // 
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1088, 700);
            this.KeyPreview = true;
            this.Name = "frmCustomReport";
            this.Text = "Custom Report";
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmCustomReport_Load);
            this.Resize += new System.EventHandler(this.frmCustomReport_Resize);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmCustomReport_KeyDown);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraClassCodes)).EndInit();
            this.fraClassCodes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdCancel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdOK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.vsClassCodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraWhere)).EndInit();
            this.fraWhere.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsWhere)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdClear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraReports)).EndInit();
            this.fraReports.ResumeLayout(false);
            this.fraReports.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAdd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraSort)).EndInit();
            this.fraSort.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraFields)).EndInit();
            this.fraFields.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsLayout)).EndInit();
            this.vsLayout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraMessage)).EndInit();
            this.fraMessage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdAddRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteColumn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDeleteRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrint)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdAddColumn;
		private FCButton cmdDeleteRow;
		private FCButton cmdDeleteColumn;
		private FCButton cmdAddRow;
		private FCButton cmdPrint;
        private Wisej.Web.Ext.CustomProperties.CustomProperties customProperties1;
    }
}