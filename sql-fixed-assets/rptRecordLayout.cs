﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Global;
using TWSharedLibrary;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptRecordLayout.
	/// </summary>
	public partial class rptRecordLayout : BaseSectionReport
	{
		public static rptRecordLayout InstancePtr
		{
			get
			{
				return (rptRecordLayout)Sys.GetInstance(typeof(rptRecordLayout));
			}
		}

		protected rptRecordLayout _InstancePtr = null;
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			base.Dispose(disposing);
		}
		// nObj = 1
		//   0	rptRecordLayout	{9EB8768B-CDFA-44DF-8F3E-857A8405E1DB}
		//=========================================================
		int PageCounter;
		bool blnFirstRecord;
		string[] strDescriptions = null;
		int intField;

		public rptRecordLayout()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			if (_InstancePtr == null)
				_InstancePtr = this;
			this.Name = "Fixed Assets Extract Record Layout";
		}

		private void ActiveReport_FetchData(object sender, FetchEventArgs eArgs)
		{
			if (blnFirstRecord)
			{
				blnFirstRecord = false;
				eArgs.EOF = false;
			}
			else
			{
				intField += 1;
				if (intField > Information.UBound(strDescriptions, 1))
				{
					eArgs.EOF = true;
				}
				else
				{
					eArgs.EOF = false;
				}
			}
		}

		private void ActiveReport_ReportStart(object sender, EventArgs e)
		{
			//modGlobalFunctions.SetFixedSizeReport(this, ref MDIParent.InstancePtr.Grid);
			PageCounter = 0;
			intField = 0;
			Label2.Text = modGlobalConstants.Statics.MuniName;
			Label3.Text = Strings.Format(DateTime.Today, "MM/dd/yyyy");
			Label7.Text = Strings.Format(DateAndTime.TimeOfDay, "h:mm AM/PM");
			blnFirstRecord = true;
			strDescriptions = Strings.Split(MDIParent.InstancePtr.strTitles, ",", -1, CompareConstants.vbBinaryCompare);
		}

		private void Detail_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: lblFieldNumber As Variant --> As string
			// vbPorter upgrade warning: fldFieldDescription As Variant --> As string
			string lblFieldNumber, fldFieldDescription;
			// - "AutoDim"
			lblFieldNumber = "Field " + FCConvert.ToString(intField + 1);
			fldFieldDescription = strDescriptions[intField];
		}

		private void PageHeader_Format(object sender, EventArgs e)
		{
			// vbPorter upgrade warning: Label4 As Variant --> As string
			string Label4;
			// - "AutoDim"
			PageCounter += 1;
			Label4 = "Page " + FCConvert.ToString(PageCounter);
		}

		
	}
}
