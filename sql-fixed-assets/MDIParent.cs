﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;
using System.Drawing;
using Wisej.Core;
using Global;
using TWSharedLibrary;

namespace TWFA0000
{
	public class MDIParent
	//: BaseForm
	{

		public MDIParent()
		{
			//
			// Required for Windows Form Designer support
			//
			//
			//InitializeComponent();
			InitializeComponentEx();
		}

		private void InitializeComponentEx()
		{
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			if (_InstancePtr == null)
				_InstancePtr = this;
		}
		/// <summary>
		/// Default instance for Form
		/// </summary>
		public static MDIParent InstancePtr
		{
			get
			{
				return (MDIParent)Sys.GetInstance(typeof(MDIParent));
			}
		}

		protected MDIParent _InstancePtr = null;

		//=========================================================
		// ********************************************************
		// PROPERTY OF TRIO SOFTWARE CORPORATION                  *
		// *
		// WRITTEN BY     :   Matthew Larrabee                    *
		// Date           :   12/04/2002                          *
		// *
		// MODIFIED BY    :                                       *
		// Last Updated   :                                       *
		// ********************************************************
		private int[] LabelNumber = new int[200 + 1];
		private int intExit;
		private int intCount;
		public string strTitles = "";
		const string strTrio = "TRIO Software - Fixed Assets";

		public void DisableMenuOption(bool boolState, int intRow)
		{
			//if (!boolState)
			//{
			//	MDIParent.InstancePtr.Grid.Select(intRow, 1);
			//	MDIParent.InstancePtr.Grid.CellForeColor = ColorTranslator.FromOle(modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION);
			//}
		}

		private void SetMenuOptions(string strMenu)
		{
			//modGlobal.CloseChildForms();
			//Grid.Tag = (System.Object)(strMenu);
			//Grid.Clear();
			if (Strings.UCase(strMenu) == "MAIN")
			{
				MainCaptions();
			}
			//else if (Strings.UCase(strMenu) == "FILE")
			//{
			//    FileCaptions();
			//}
			//else if (Strings.UCase(strMenu) == "PRINT")
			//{
			//    PrintCaptions();
			//}
			else
			{
				MainCaptions();
			}
		}

		public void Menu1()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
				modVariables.Statics.glngMasterIDNumber = 0;
				frmMaster.InstancePtr.blnFromSearch = false;
				frmMaster.InstancePtr.Show(App.MainForm);
				frmMaster.InstancePtr.SetDefaults();
			}
			else if (vbPorterVar == "FILE")
			{
				frmSettings.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "PRINT")
			{
				frmCustomReport.InstancePtr.Show(App.MainForm);
				frmCustomReport.InstancePtr.Text = "Custom Reports";
				modCustomReport.SetFormFieldCaptions(frmCustomReport.InstancePtr, "ASSETS");
				frmCustomReport.InstancePtr.vsWhere.TextMatrix(20, 1, "A");
			}
			else
			{
			}
		}

		public void Menu10()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu11()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu12()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "FILE")
			{
				// Call SetMenuOptions("MAIN")
			}
			else
			{
			}
		}

		public void Menu13()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu14()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu15()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
			ErrorHandler:
			;
			if (Information.Err().Number == 3031)
			{
				// not a valid password
				{
					/*? Resume Next; */}
			}
			else
			{
				// GoTo CallErrorRoutine
			}
		}

		public void Menu16()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu17()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu18()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else
			{
			}
		}

		public void Menu19()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "SYSTEM")
			{
			}
			else if (vbPorterVar == "FILE")
			{
			}
			else
			{
			}
		}

		public void Menu2()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
				frmSearch.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "FILE")
			{
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Updating Database Structure", true);
				frmWait.InstancePtr.prgProgress.Value = 100;
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show("Database update complete", "Update Complete", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else if (vbPorterVar == "PRINT")
			{
				frmSetupDepraciationTotals.InstancePtr.Show(App.MainForm);
			}
			else
			{
			}
		}

		public void Menu3()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
				frmDepreciation.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "FILE")
			{
				frmDeptDiv.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "PRINT")
			{
				frmSetupAssetDepreciation.InstancePtr.Show(App.MainForm);
			}
			else
			{
			}
		}

		public void Menu4()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
				//SetMenuOptions("PRINT");
			}
			else if (vbPorterVar == "FILE")
			{
				frmClassCodes.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "PRINT")
			{
				//SetMenuOptions("MAIN");
			}
			else
			{
			}
		}

		public void Menu5()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
				//SetMenuOptions("FILE");
			}
			else if (vbPorterVar == "FILE")
			{
				frmLocations.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "REPORTS")
			{
			}
			else
			{
			}
		}

		public void Menu6()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			//if (vbPorterVar == "MAIN")
			//{
			//	modGlobal.CloseChildForms();
			//	if (modVariables.gboolCancelSelected)
			//	{
			//	}
			//	else
			//	{
			//		Close();
			//	}
			//}
			if (vbPorterVar == "FILE")
			{
				frmVendors.InstancePtr.Show(App.MainForm);
			}
		}

		public void Menu7()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "FILE")
			{
				frmConditions.InstancePtr.Show(App.MainForm);
			}
			else if (vbPorterVar == "REPORTS")
			{
			}
			else
			{
			}
		}

		public void Menu8()
		{
			var vbPorterVar = App.MainForm.NavigationMenu.CurrentItem.Menu;
			if (vbPorterVar == "MAIN")
			{
			}
			else if (vbPorterVar == "FILE")
			{
				// Call CreateDatabaseExtract
				//SetMenuOptions("MAIN");
			}
			else
			{
			}
		}

		private void mnuBudgetaryHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWBD0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuCashReceiptsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWCR0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuClerkHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWCK0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuCodeEnforcementHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWCE0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuEnhanced911Help_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWE90000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuFileFormsPrintForms_Click(object sender, System.EventArgs e)
		{
			frmScreenPrint.InstancePtr.Show(FCForm.FormShowEnum.Modal, App.MainForm);
			//this.ZOrder(ZOrderConstants.SendToBack);
		}

		private void mnuFixedAssetsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWFA0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuGeneralEntryHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWGN0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuHAbout_Click(object sender, System.EventArgs e)
		{
			frmAbout.InstancePtr.Show(App.MainForm);
		}

		private void mnuMotorVehicleRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWMV0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		public void Menu9()
		{
			//string vbPorterVar = FCConvert.ToString(Grid.Tag);
			//if (vbPorterVar == "MAIN")
			//{
			//}
			//else if (vbPorterVar == "FILE")
			//{
			//}
			//else
			//{
			//}
		}

		private void MDIParent_Activated(object sender, System.EventArgs e)
		{
			//modArchiveImage.ShowArchiveImage(ref modGlobalConstants.Statics.gstrArchiveYear);
			//modGlobalFunctions.AutoSizeMenu_6(Grid, 20);
			//FCUtils.CallByName(App.MainForm, "Grid_GotFocus", CallType.Method);
		}
		//private void MDIParent_Load(object sender, System.EventArgs e)
		//{
		//	//Begin Unmaped Properties
		//	//MDIParent.LinkTopic	= "MDIForm1";
		//	//End Unmaped Properties
		//	this.Grid.ColWidth(0, 200);
		//	this.Grid.ColWidth(1, 2400);
		//	this.Grid.ColWidth(2, 0);
		//	Grid.Width = 2700;
		//	Grid.Select(0, 1);
		//	string strReturn = "";
		//	modRegistry.GetKeyValues(modGlobalConstants.HKEY_CURRENT_USER, "SOFTWARE\\TrioVB\\", "Maximise Forms", ref strReturn);
		//	modErrorHandler.Statics.gstrFormName = this.Name;
		//	this.StatusBar1.Panels[0].Text = Strings.Trim(modGlobalConstants.MuniName);
		//	this.StatusBar1.Panels[0].AutoSize = StatusBarPanelAutoSize.Contents;
		//	if (strReturn == string.Empty)
		//		strReturn = "False";
		//	modGlobalConstants.Statics.boolMaxForms = FCConvert.CBool(strReturn);
		//	mnuOMaxForms.Checked = modGlobalConstants.Statics.boolMaxForms;
		//	modGlobalFunctions.LoadTRIOColors();
		//	SetMenuOptions("MAIN");
		//	modGlobalFunctions.SetTRIOColors(this);
		//	//modGlobalFunctions.SetMenuBackColor();
		//	Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, Grid.Cols - 1, Grid.BackColor);
		//}
		public void Init()
		{
			modGlobalFunctions.LoadTRIOColors();
			App.MainForm.StatusBarText1 = StaticSettings.EnvironmentSettings.ClientName;
			App.MainForm.NavigationMenu.Owner = this;
			App.MainForm.menuTree.ImageList = CreateImageList();
			MainCaptions();
		}

		public ImageList CreateImageList()
		{
			ImageList imageList = new ImageList();
			imageList.ImageSize = new System.Drawing.Size(18, 18);
			imageList.Images.AddRange(new ImageListEntry[] {
				new ImageListEntry("menutree-file-maintenance", "file-maintenance"),
				new ImageListEntry("menutree-printing", "printing"),
				new ImageListEntry("menutree-add-assets", "add-assets"),
				new ImageListEntry("menutree-search-assets", "search-assets"),
				new ImageListEntry("menutree-posting", "posting"),
				new ImageListEntry("menutree-file-maintenance-active", "file-maintenance-active"),
				new ImageListEntry("menutree-printing-active", "printing-active"),
				new ImageListEntry("menutree-add-assets-active", "add-assets-active"),
				new ImageListEntry("menutree-search-assets-active", "search-assets-active"),
				new ImageListEntry("menutree-posting-active", "posting-active"),
			});
			return imageList;
		}

		public object MainCaptions()
		{
			object MainCaptions = null;
			string strTemp = "";
			bool boolDisable = false;
			string imageKey = "";
			string menu = "MAIN";
			int lngFCode = 0;
			App.MainForm.Text = strTrio + "   [Main]";
			App.MainForm.Caption = "FIXED ASSETS";
			App.MainForm.ApplicationIcon = "icon-fixed-assets";
            App.MainForm.NavigationMenu.OriginName = "Fixed Assets";
			string assemblyName = this.GetType().Assembly.GetName().Name;
			if (!App.MainForm.ApplicationIcons.ContainsKey(assemblyName))
			{
				App.MainForm.ApplicationIcons.Add(assemblyName, "icon-fixed-assets");
			}
			for (intCount = 1; intCount <= 5; intCount++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Add Assets";
							imageKey = "add-assets";
							LabelNumber[Strings.Asc("1")] = 1;
							lngFCode = modStartup.ADDASSET;
							break;
						}
					case 2:
						{
							strTemp = "Search Assets";
							imageKey = "search-assets";
							LabelNumber[Strings.Asc("2")] = 2;
							lngFCode = 0;
							break;
						}
					case 3:
						{
							strTemp = "Post New Depreciation";
							imageKey = "posting";
							LabelNumber[Strings.Asc("3")] = 3;
							lngFCode = modStartup.POSTDEPRECIATION;
							break;
						}
					case 4:
						{
							strTemp = "Printing";
							imageKey = "printing";
							LabelNumber[Strings.Asc("4")] = 4;
							lngFCode = modStartup.PRINTING;
							break;
						}
					case 5:
						{
							strTemp = "File Maintenance";
							imageKey = "file-maintenance";
							LabelNumber[Strings.Asc("M")] = 5;
							lngFCode = modStartup.FILEMAINTENANCE;
							break;
						}
				//case 6:
				//	{
				//                       strTemp = "Exit Fixed Assets";
				//		LabelNumber[Strings.Asc("X")] = 6;
				//		intExit = 6;
				//		break;
				//	}
				//default:
				//	{
				//		Grid.TextMatrix(intCount, 1, string.Empty);
				//		break;
				//	}
				}
				//end switch
				if (lngFCode != 0)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				FCMenuItem newItem = App.MainForm.NavigationMenu.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 1, imageKey);
				switch (intCount)
				{
					case 4:
						{
							PrintCaptions(newItem);
							break;
						}
					case 5:
						{
							FileCaptions(newItem);
							break;
						}
				}
			}
			//Grid.Select(1, 1);
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, ImageList1.Images["Left"]);
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, Grid.Cols - 1, Grid.BackColor);
			return MainCaptions;
		}

		public void PrintCaptions(FCMenuItem parent)
		{
			string menu = "PRINT";
			int lngFCode = 0;
			string strTemp = "";
			bool boolDisable = false;
			string imageKey = "";
			App.MainForm.Text = strTrio + "   [Print]";
			for (intCount = 1; intCount <= 3; intCount++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Custom Report";
							LabelNumber[Strings.Asc("1")] = 1;
							lngFCode = 0;
							break;
						}
					case 2:
						{
							strTemp = "Depreciation Totals";
							LabelNumber[Strings.Asc("2")] = 2;
							lngFCode = 0;
							break;
						}
					case 3:
						{
							strTemp = "Asset Depreciation Report";
							LabelNumber[Strings.Asc("3")] = 3;
							lngFCode = 0;
							break;
						}
				//case 4:
				//	{
				//                       strTemp = "Return to Main Menu";
				//		LabelNumber[Strings.Asc("X")] = 4;
				//		intExit = 4;
				//		break;
				//	}
				//default:
				//	{
				//		Grid.TextMatrix(intCount, 1, string.Empty);
				//		break;
				//	}
				}
				//end switch
				if (lngFCode != 0)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				FCMenuItem newItem = parent.SubItems.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2);
			}
			//Grid.Select(1, 1);
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, ImageList1.Images["Left"]);
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, Grid.Cols - 1, Grid.BackColor);
		}

		public void FileCaptions(FCMenuItem parent)
		{
			string menu = "FILE";
			int lngFCode = 0;
			string strTemp = "";
			bool boolDisable = false;
			string imageKey = "";
			App.MainForm.Text = strTrio + "   [File Maintenance]";
			for (intCount = 1; intCount <= 7; intCount++)
			{
				lngFCode = 0;
				boolDisable = false;
				switch (intCount)
				{
					case 1:
						{
							strTemp = "Customize";
							LabelNumber[Strings.Asc("1")] = 1;
							lngFCode = modStartup.CUSTOMIZE;
							break;
						}
					case 2:
						{
							strTemp = "Check Database Structure";
							LabelNumber[Strings.Asc("2")] = 2;
							lngFCode = 0;
							break;
						}
					case 3:
						{
							strTemp = "Dept / Div Codes";
							LabelNumber[Strings.Asc("3")] = 3;
							lngFCode = modStartup.ADDDEPT;
							break;
						}
					case 4:
						{
							strTemp = "Class Codes";
							LabelNumber[Strings.Asc("4")] = 4;
							lngFCode = modStartup.ADDCLASS;
							break;
						}
					case 5:
						{
							strTemp = "Location Codes";
							LabelNumber[Strings.Asc("5")] = 5;
							lngFCode = modStartup.ADDLOCATIONS;
							break;
						}
					case 6:
						{
							strTemp = "Vendor Codes";
							LabelNumber[Strings.Asc("6")] = 6;
							lngFCode = modStartup.ADDVENDORS;
							break;
						}
					case 7:
						{
							strTemp = "Condition Codes";
							LabelNumber[Strings.Asc("7")] = 7;
							lngFCode = 0;
							break;
						}
				//case 8:
				//	{
				//                       strTemp = "Return to Main";
				//		LabelNumber[Strings.Asc("X")] = 8;
				//		intExit = 8;
				//		break;
				//	}
				//default:
				//	{
				//		Grid.TextMatrix(intCount, 1, string.Empty);
				//		break;
				//	}
				}
				//end switch
				if (lngFCode != 0)
				{
					boolDisable = !modSecurity.ValidPermissions(this, ref lngFCode, false);
				}
				FCMenuItem newItem = parent.SubItems.Add(strTemp, "Menu" + intCount, menu, !boolDisable, 2);
			}
			//Grid.Select(1, 1);
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, ImageList1.Images["Left"]);
			//Grid.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, Grid.Cols - 1, Grid.BackColor);
		}

		private void MDIForm_Unload(object sender, FCFormClosingEventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "MDIForm_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			modBlockEntry.WriteYY();
            //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
            //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
            App.MainForm.OpenModule("TWGNENTY");
			Application.Exit();
		}

		private void mnuFExit_Click(object sender, System.EventArgs e)
		{
			try
			{
				// On Error GoTo CallErrorRoutine
				modErrorHandler.Statics.gstrCurrentRoutine = "MDIForm_Unload";
				clsMousePointer Mouse;
				Mouse = new clsMousePointer();
				goto ResumeCode;
			}
			catch (Exception ex)
			{
				// CallErrorRoutine:
				modErrorHandler.SetErrorHandler(ex);
				return;
			}
			ResumeCode:
			;
			modBlockEntry.WriteYY();
            //FC:FINAL:SBE - #i1769 - Interaction.Shell is not supported. It was used to switch the module in original application 
            //Interaction.Shell("TWGNENTY.EXE", System.Diagnostics.ProcessWindowStyle.Maximized, false, -1);
            App.MainForm.OpenModule("TWGNENTY");
			Application.Exit();
		}

		//private void mnuOMaxForms_Click(object sender, System.EventArgs e)
		//{
		//	modRegistry.RegCreateKeyWrp(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, 0);
		//	if (mnuOMaxForms.Checked)
		//	{
		//		modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Maximise Forms", "False");
		//	}
		//	else
		//	{
		//		modRegistry.SaveKeyValue(modGlobalConstants.HKEY_CURRENT_USER, modGlobalConstants.REGISTRYKEY, "Maximise Forms", "True");
		//	}
		//	modGlobalConstants.Statics.boolMaxForms = !modGlobalConstants.Statics.boolMaxForms;
		//	mnuOMaxForms.Checked = !mnuOMaxForms.Checked;
		//}

		private void mnuPayrollHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWPY0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuPersonalPropertyHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWPP0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuRealEstateHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWRE0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuRedBookHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWRB0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuTaxBillingHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWBL0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuTaxCollectionsHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWCL0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuUtilityBillingHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWUT0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		private void mnuVoterRegistrationHelp_Click(object sender, System.EventArgs e)
		{
			modGlobalFunctions.HelpShell("TWVR0000.hlp", AppWinStyle.MaximizedFocus, FCFileSystem.Statics.UserDataFolder + "\\");
		}

		public void Grid_Enter(object sender, System.EventArgs e)
		{
			int a;
		}
		//private void Grid_KeyDownEvent(object sender, KeyEventArgs e)
		//{
		//	int intCounter;
		//	int keyCode = e.KeyValue;
		//	if (keyCode > 96 && keyCode < 106)
		//	{
		//		keyCode -= 48;
		//	}
		//	if (keyCode == 13)
		//	{
		//		if (Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.MouseRow, 1) == MDIParent.InstancePtr.BackColor)
		//			return;
		//		FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(LabelNumber[Convert.ToByte(Strings.UCase(Strings.Left(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col)), 1))[0])]), CallType.Method);
		//	}
		//	for (intCounter = 1; intCounter <= Grid.Rows - 1; intCounter++)
		//	{
		//		if (Strings.Left(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, intCounter, 1)), 1) == FCConvert.ToString(Convert.ToChar(keyCode)))
		//		{
		//			if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, intCounter, 1))) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//				return;
		//			FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(intCounter), CallType.Method);
		//			keyCode = 0;
		//			break;
		//		}
		//		Grid.Focus();
		//	}
		//}
		//private void Grid_MouseDownEvent(object sender, MouseEventArgs e)
		//{
		//	if (Grid.MouseCol != 1)
		//		return;
		//	/*? On Error Resume Next  */
		//	if (FCConvert.ToInt32(Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, Grid.MouseRow, 1)) == modGlobalConstants.Statics.TRIOCOLORDISABLEDOPTION)
		//		return;
		//	FCUtils.CallByName(App.MainForm, "Menu" + FCConvert.ToString(LabelNumber[Convert.ToByte(Strings.UCase(Strings.Left(FCConvert.ToString(Grid.Cell(FCGrid.CellPropertySettings.flexcpTextDisplay, Grid.Row, Grid.Col)), 1))[0])]), CallType.Method);
		//}
		//private void Grid_MouseMoveEvent(object sender, MouseEventArgs e)
		//{
		//	double i;
		//	// avoid extra work
		//	if (Grid.MouseRow == R)
		//		return;
		//	R = Grid.MouseRow;
		//	// move selection (even with no click)
		//	Grid.Select(R, 1);
		//	// remove cursor image
		//	Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, 1, 0, Grid.Rows - 1, null);
		//	// show cursor image if there is some text in column 5
		//	if (R == 0)
		//		return;
		//	if (Grid.Cell(FCGrid.CellPropertySettings.flexcpForeColor, R, 1) == MDIParent.InstancePtr.BackColor || R > LabelNumber[Convert.ToByte("X"[0])])
		//	{
		//		Grid.HighLight = FCGrid.HighLightSettings.flexHighlightNever;
		//		return;
		//	}
		//	else
		//	{
		//		Grid.HighLight = FCGrid.HighLightSettings.flexHighlightAlways;
		//	}
		//	// show cursor image if there is some text in column 5
		//	if (R == 0)
		//		return;
		//	Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, R, 0, ImageList1.Images["Left"]);
		//	Grid.Cell(FCGrid.CellPropertySettings.flexcpPicture, R, 2, ImageList1.Images["Right"]);
		//}
		private void CreateDatabaseExtract()
		{
			string strDirectory;
			string temp = "";
			int intRecords = 0;
			strTitles += "Description, Year, Make, Model, Serial Number, Purchase Date, Dept, Div, Class Code, Cost";
			strDirectory = FCFileSystem.Statics.UserDataFolder;
			Information.Err().Clear();
			// App.MainForm.CommonDialog1.Flags = 0	// - UPGRADE_WARNING: MSComDlg.CommonDialog property Flags has a new behavior.Click for more: 'ms-help://MS.VSCC.v80/dv_commoner/local/redirect.htm?keyword="DFCDE711-9694-47D7-9C50-45A99CD8E91E";
			//- App.MainForm.CommonDialog1.CancelError = true;
			/*? On Error Resume Next  */
			App.MainForm.CommonDialog1.Filter = "*.txt";
			try
			{
				App.MainForm.CommonDialog1.ShowOpen();
			}
			catch
			{
			}
			if (Information.Err().Number == 0)
			{
				frmWait.InstancePtr.Init("Please Wait..." + "\r\n" + "Extracting Data", true);
				temp = App.MainForm.CommonDialog1.FileName;
				if (Strings.Right(temp, 4) != ".txt")
				{
					temp += ".txt";
				}
				FCFileSystem.ChDrive(strDirectory);
				//Application.StartupPath = strDirectory;
				intRecords = CreateDataExtract(ref temp);
				frmWait.InstancePtr.Unload();
				//Application.DoEvents();
				MessageBox.Show(FCConvert.ToString(intRecords) + " records have been extracted to " + temp + " successfully!");
				frmReportViewer.InstancePtr.Init(rptRecordLayout.InstancePtr);
			}
			else
			{
				FCFileSystem.ChDrive(strDirectory);
				//Application.StartupPath = strDirectory;
				return;
			}
		}

		private short CreateDataExtract(ref string strFilename)
		{
			short CreateDataExtract = 0;
			clsDRWrapper rsAssetInfo = new clsDRWrapper();
			clsDRWrapper rsClassCodeInfo = new clsDRWrapper();
			int intItemsWritten;
			string strLatestDate = "";
			CreateDataExtract = 0;
			rsAssetInfo.OpenRecordset("SELECT * FROM tblMaster ORDER BY ID", "TWFA0000.vb1");
			if (rsAssetInfo.EndOfFile() != true && rsAssetInfo.BeginningOfFile() != true)
			{
				FCFileSystem.FileClose(1);
				FCFileSystem.FileOpen(1, strFilename, OpenMode.Output, (OpenAccess)(-1), (OpenShare)(-1), -1);
				do
				{
					//Application.DoEvents();
					frmWait.InstancePtr.prgProgress.Value = FCConvert.ToInt32((FCConvert.ToDouble(CreateDataExtract + 1) / rsAssetInfo.RecordCount()) * 100);
					frmWait.InstancePtr.Refresh();
					intItemsWritten = 0;
					// "Description, Year, Make, Model, Serial Number, Purchase Date, Dept, Div, Class Code, Cost"
					FCFileSystem.Write(1, rsAssetInfo.Get_Fields_String("Description"));
					// TODO Get_Fields: Check the table for the column [Year] and replace with corresponding Get_Field method
					FCFileSystem.Write(1, rsAssetInfo.Get_Fields("Year"));
					FCFileSystem.Write(1, rsAssetInfo.Get_Fields_String("Make"));
					FCFileSystem.Write(1, rsAssetInfo.Get_Fields_String("Model"));
					FCFileSystem.Write(1, rsAssetInfo.Get_Fields_String("SerialNumber"));
					FCFileSystem.Write(1, Strings.Format(rsAssetInfo.Get_Fields_DateTime("PurchaseDate"), "MM/dd/yyyy"));
					FCFileSystem.Write(1, rsAssetInfo.Get_Fields_String("Dept"));
					FCFileSystem.Write(1, rsAssetInfo.Get_Fields_String("Div"));
					rsClassCodeInfo.OpenRecordset("SELECT * FROM tblClassCodes WHERE ID = " + rsAssetInfo.Get_Fields_String("ClassCode"), "TWFA0000.vb1");
					if (rsClassCodeInfo.EndOfFile() != true && rsClassCodeInfo.BeginningOfFile() != true)
					{
						// TODO Get_Fields: Check the table for the column [Code] and replace with corresponding Get_Field method
						FCFileSystem.Write(1, rsClassCodeInfo.Get_Fields("Code"));
					}
					else
					{
						FCFileSystem.Write(1, "");
					}
					FCFileSystem.WriteLine(1, rsAssetInfo.Get_Fields_Decimal("BasisCost"));
					CreateDataExtract += 1;
					rsAssetInfo.MoveNext();
				}
				while (rsAssetInfo.EndOfFile() != true);
			}
			FCFileSystem.FileClose(1);
			return CreateDataExtract;
		}

		public class StaticVariables
		{
			/// </summary>
			/// Summary description for MDIParent.
			/// <summary>
			public int R = 0, c;
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
