﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using fecherFoundation.VisualBasicLayer;
using Wisej.Web;
using System.Runtime.InteropServices;
using Global;
using System.IO;
using Wisej.Core;

namespace TWFA0000
{
	public class modGlobal
	{
		//=========================================================
		// Public Const ADDEDIT = 1
		// Public Const SEARCH = 2
		// Public Const REPORTS = 4
		// Public Const FILEMAINTENANCE = 5
		// Public Const POSTDEPRECIATION = 3
		// Public Const LOCATIONS = 1
		// Public Const CUSTOMIZE = 3
		// Public Const DEPTDIV = 4
		// Public Const CLASSCODES = 5
		public const int GRIDROWS = 19;
		public const string DEFAULTDATABASENAME = "Twfa0000.vb1";
		public const string DEFAULTbdDATABASENAME = "TWBD0000.vb1";
		
		// screen setup items
		const string Logo = "TRIO Software - Clerk ";
		public const string SearchInputBoxTitle = "Real Estate Assessment Search";

		

		

		//public struct typOldVitals
		//{
		//	public string AttestBy;
		//	public string FileNumber;
		//	public string pageNumber;
		//	public string FamilyName;
		//	public string ChildName1;
		//	public string ChildName2;
		//	public string ChildName3;
		//	public string DateOfEvent;
		//	public string Parent1;
		//	public string Parent2;
		//	public string Parent3;
		//	public string Parent4;
		//	public string PlaceOfEvent;
		//	public string PlaceOfResidence;
		//	public string NameOfClerk1;
		//	public string NameOfClerk2;
		//	public string TownOfClerk1;
		//	public string TownOfClerk2;
		//	public string DateTo;
		//	public string DateFrom;
		//	public string Notes;
		//	//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//	public typOldVitals(int unusedParam)
		//	{
		//		this.AttestBy = string.Empty;
		//		this.FileNumber = string.Empty;
		//		this.pageNumber = string.Empty;
		//		this.FamilyName = string.Empty;
		//		this.ChildName1 = string.Empty;
		//		this.ChildName2 = string.Empty;
		//		this.ChildName3 = string.Empty;
		//		this.DateFrom = string.Empty;
		//		this.Parent1 = string.Empty;
		//		this.Parent2 = string.Empty;
		//		this.Parent3 = string.Empty;
		//		this.Parent4 = string.Empty;
		//		this.PlaceOfEvent = string.Empty;
		//		this.PlaceOfResidence = string.Empty;
		//		this.NameOfClerk1 = string.Empty;
		//		this.NameOfClerk2 = string.Empty;
		//		this.TownOfClerk1 = string.Empty;
		//		this.TownOfClerk2 = string.Empty;
		//		this.DateFrom = string.Empty;
		//		this.DateTo = string.Empty;
		//		this.DateOfEvent = string.Empty;
		//		this.Notes = string.Empty;
		//	}
		//};

		//public struct typConsent
		//{
		//	public bool ThreeDayWaiver;
		//	public bool ThirtyDayWaiver;
		//	public string FromPerson;
		//	public string ToPerson;
		//	public string DateOfConsent;
		//	public string ClerkName;
		//	public string TownName;
		//	public string Notes;
		//	public bool NeedConsent;
		//	public string GroomBride;
		//	public string MarriageDate;
		//	//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//	public typConsent(int unusedParam)
		//	{
		//		this.ThreeDayWaiver = false;
		//		this.ThirtyDayWaiver = false;
		//		this.FromPerson = string.Empty;
		//		this.ToPerson = string.Empty;
		//		this.DateOfConsent = string.Empty;
		//		this.ClerkName = string.Empty;
		//		this.TownName = string.Empty;
		//		this.Notes = string.Empty;
		//		this.NeedConsent = false;
		//		this.GroomBride = string.Empty;
		//		this.MarriageDate = string.Empty;
		//	}
		//};

		//public struct ControlProportions
		//{
		//	public float WidthProportions;
		//	public float HeightProportions;
		//	public float TopProportions;
		//	public float LeftProportions;
		//};

		//public struct ClerkDefault
		//{
		//	public string Address1;
		//	public string Address2;
		//	public string phone;
		//	public string AgentNumber;
		//	public string City;
		//	public string State;
		//	public string Zip;
		//	public string County;
		//	public string LegalResidence;
		//	//FC:FINAL:DSE Use custom constructor to initialize string fields with empty value
		//	public ClerkDefault(int unusedParam)
		//	{
		//		this.Address1 = string.Empty;
		//		this.Address2 = string.Empty;
		//		this.phone = string.Empty;
		//		this.AgentNumber = string.Empty;
		//		this.City = string.Empty;
		//		this.State = string.Empty;
		//		this.Zip = string.Empty;
		//		this.County = string.Empty;
		//		this.LegalResidence = string.Empty;
		//	}
		//};

		//public static clsDRWrapper rsMarriageCertificate
		//{
		//	get
		//	{
		//		if (Statics.rsMarriageCertificate_AutoInitialized == null)
		//		{
		//			Statics.rsMarriageCertificate_AutoInitialized = new clsDRWrapper();
		//		}
		//		return Statics.rsMarriageCertificate_AutoInitialized;
		//	}
		//	set
		//	{
		//		Statics.rsMarriageCertificate_AutoInitialized = value;
		//	}
		//}

		
		public const string DatabaseNamePath = "FixedAssets";

		

		

		public static string PadToString(object intValue, short intLength)
		{
			string PadToString = "";
			// converts an integer to string
			// this routine is faster than ConvertToString
			// to use this function pass it a value and then length of string
			// Example:  XYZStr = PadToString(XYZInt, 6)
			// if XYZInt is equal to 123 then XYZStr is equal to "000123"
			// 
			if (fecherFoundation.FCUtils.IsNull(intValue) == true)
				intValue = 0;
			PadToString = Strings.StrDup(intLength - FCConvert.ToString(intValue).Length, "0") + intValue;
			return PadToString;
		}


		public static double Round_6(double dValue, short iDigits)
		{
			return Round(ref dValue, ref iDigits);
		}

		public static double Round(ref double dValue, ref short iDigits)
		{
			double Round = 0;
			//FC:FINAL:BBE:#i568 - Due to rounding errors, use System.Math.Round instead
			//Round = FCConvert.ToInt32(Conversion.Val(dValue * (Math.Pow(10, iDigits))) + 0.5) / (Math.Pow(10, iDigits));
			Round = Math.Round(dValue, iDigits);
			return Round;
		}

		
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmMaster, frmSettings, frmSearch, frmClassCodes, frmLocations, frmConditions, frmDepreciation, frmDeptDiv, frmInitialDepreciation, frmCalender, frmSetupDepraciationTotals, frmVendors, frmLoadValidAccounts, frmSetupAssetDepreciation, frmSelectBankNumber, frmSelectPostingJournal)
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool FormExist(Form FormName)
		{
			bool FormExist = false;
			foreach (Form frm in FCGlobal.Statics.Forms)
			{
				if (frm.Name == FormName.Name)
				{
					if (FCConvert.ToString(frm.Tag) == "Open")
					{
						FormExist = true;
						return FormExist;
					}
				}
			}
			FormName.Tag = "Open";
			return FormExist;
		}

		public class StaticVariables
		{
			public clsTrioSecurity clsFASecurity;
			
			public string strPreSetReport = "";
			
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
