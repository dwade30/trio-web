﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmSettings.
	/// </summary>
	partial class frmSettings : BaseForm
	{
		public fecherFoundation.FCFrame Frame3;
		public fecherFoundation.FCCheckBox chkDiv;
		public fecherFoundation.FCCheckBox chkDeptDiv;
		public fecherFoundation.FCLabel lblBud;
		public fecherFoundation.FCFrame Frame2;
		public fecherFoundation.FCCheckBox chkVendors;
		public fecherFoundation.FCLabel lblBud2;
		public fecherFoundation.FCFrame Frame1;
		public fecherFoundation.FCTextBox txtLocation4;
		public fecherFoundation.FCTextBox txtLocation3;
		public fecherFoundation.FCTextBox txtLocation2;
		public fecherFoundation.FCTextBox txtLocation1;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCFrame fraSettings;
		public fecherFoundation.FCCheckBox chkSearch;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.Frame3 = new fecherFoundation.FCFrame();
            this.chkDiv = new fecherFoundation.FCCheckBox();
            this.chkDeptDiv = new fecherFoundation.FCCheckBox();
            this.lblBud = new fecherFoundation.FCLabel();
            this.Frame2 = new fecherFoundation.FCFrame();
            this.chkVendors = new fecherFoundation.FCCheckBox();
            this.lblBud2 = new fecherFoundation.FCLabel();
            this.Frame1 = new fecherFoundation.FCFrame();
            this.txtLocation4 = new fecherFoundation.FCTextBox();
            this.txtLocation3 = new fecherFoundation.FCTextBox();
            this.txtLocation2 = new fecherFoundation.FCTextBox();
            this.txtLocation1 = new fecherFoundation.FCTextBox();
            this.Label4 = new fecherFoundation.FCLabel();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.fraSettings = new fecherFoundation.FCFrame();
            this.chkSearch = new fecherFoundation.FCCheckBox();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdSave = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).BeginInit();
            this.Frame3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeptDiv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).BeginInit();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkVendors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraSettings)).BeginInit();
            this.fraSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSearch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 578);
            this.BottomPanel.Size = new System.Drawing.Size(995, 10);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdSave);
            this.ClientArea.Controls.Add(this.Frame3);
            this.ClientArea.Controls.Add(this.Frame2);
            this.ClientArea.Controls.Add(this.Frame1);
            this.ClientArea.Controls.Add(this.fraSettings);
            this.ClientArea.Size = new System.Drawing.Size(995, 518);
            // 
            // TopPanel
            // 
            this.TopPanel.Size = new System.Drawing.Size(995, 60);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(128, 30);
            this.HeaderText.Text = "Customize";
            // 
            // Frame3
            // 
            this.Frame3.Controls.Add(this.chkDiv);
            this.Frame3.Controls.Add(this.chkDeptDiv);
            this.Frame3.Controls.Add(this.lblBud);
            this.Frame3.Location = new System.Drawing.Point(30, 132);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(357, 156);
            this.Frame3.TabIndex = 15;
            this.Frame3.Text = "Dept / Div";
            // 
            // chkDiv
            // 
            this.chkDiv.Location = new System.Drawing.Point(20, 78);
            this.chkDiv.Name = "chkDiv";
            this.chkDiv.Size = new System.Drawing.Size(150, 27);
            this.chkDiv.TabIndex = 2;
            this.chkDiv.Text = "Division is used?";
            this.chkDiv.CheckedChanged += new System.EventHandler(this.chkDiv_CheckedChanged);
            // 
            // chkDeptDiv
            // 
            this.chkDeptDiv.Location = new System.Drawing.Point(20, 30);
            this.chkDeptDiv.Name = "chkDeptDiv";
            this.chkDeptDiv.Size = new System.Drawing.Size(335, 27);
            this.chkDeptDiv.TabIndex = 1;
            this.chkDeptDiv.Text = "Use Dept / Div information from Budgetary";
            this.chkDeptDiv.CheckedChanged += new System.EventHandler(this.chkDeptDiv_CheckedChanged);
            // 
            // lblBud
            // 
            this.lblBud.Location = new System.Drawing.Point(20, 123);
            this.lblBud.Name = "lblBud";
            this.lblBud.Size = new System.Drawing.Size(262, 15);
            this.lblBud.TabIndex = 16;
            this.lblBud.Text = "BUDGETARY DOES NOT EXIST FOR THIS USER";
            // 
            // Frame2
            // 
            this.Frame2.Controls.Add(this.chkVendors);
            this.Frame2.Controls.Add(this.lblBud2);
            this.Frame2.Location = new System.Drawing.Point(30, 312);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(357, 115);
            this.Frame2.TabIndex = 14;
            this.Frame2.Text = "Vendors";
            // 
            // chkVendors
            // 
            this.chkVendors.Location = new System.Drawing.Point(20, 30);
            this.chkVendors.Name = "chkVendors";
            this.chkVendors.Size = new System.Drawing.Size(317, 27);
            this.chkVendors.TabIndex = 3;
            this.chkVendors.Text = "Use Vendor information from Budgetary";
            this.chkVendors.CheckedChanged += new System.EventHandler(this.chkVendors_CheckedChanged);
            // 
            // lblBud2
            // 
            this.lblBud2.Location = new System.Drawing.Point(20, 78);
            this.lblBud2.Name = "lblBud2";
            this.lblBud2.Size = new System.Drawing.Size(262, 15);
            this.lblBud2.TabIndex = 17;
            this.lblBud2.Text = "BUDGETARY DOES NOT EXIST FOR THIS USER";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.txtLocation4);
            this.Frame1.Controls.Add(this.txtLocation3);
            this.Frame1.Controls.Add(this.txtLocation2);
            this.Frame1.Controls.Add(this.txtLocation1);
            this.Frame1.Controls.Add(this.Label4);
            this.Frame1.Controls.Add(this.Label3);
            this.Frame1.Controls.Add(this.Label2);
            this.Frame1.Controls.Add(this.Label1);
            this.Frame1.Location = new System.Drawing.Point(504, 30);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(450, 240);
            this.Frame1.TabIndex = 9;
            this.Frame1.Text = "Location Titles";
            // 
            // txtLocation4
            // 
            this.txtLocation4.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation4.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLocation4.Location = new System.Drawing.Point(182, 180);
            this.txtLocation4.MaxLength = 35;
            this.txtLocation4.Name = "txtLocation4";
            this.txtLocation4.Size = new System.Drawing.Size(248, 40);
            this.txtLocation4.TabIndex = 7;
            this.txtLocation4.TextChanged += new System.EventHandler(this.txtLocation4_TextChanged);
            // 
            // txtLocation3
            // 
            this.txtLocation3.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation3.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLocation3.Location = new System.Drawing.Point(182, 130);
            this.txtLocation3.MaxLength = 35;
            this.txtLocation3.Name = "txtLocation3";
            this.txtLocation3.Size = new System.Drawing.Size(248, 40);
            this.txtLocation3.TabIndex = 6;
            this.txtLocation3.TextChanged += new System.EventHandler(this.txtLocation3_TextChanged);
            // 
            // txtLocation2
            // 
            this.txtLocation2.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation2.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLocation2.Location = new System.Drawing.Point(182, 80);
            this.txtLocation2.MaxLength = 35;
            this.txtLocation2.Name = "txtLocation2";
            this.txtLocation2.Size = new System.Drawing.Size(248, 40);
            this.txtLocation2.TabIndex = 5;
            this.txtLocation2.TextChanged += new System.EventHandler(this.txtLocation2_TextChanged);
            // 
            // txtLocation1
            // 
            this.txtLocation1.BackColor = System.Drawing.SystemColors.Window;
            this.txtLocation1.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtLocation1.Location = new System.Drawing.Point(182, 30);
            this.txtLocation1.MaxLength = 35;
            this.txtLocation1.Name = "txtLocation1";
            this.txtLocation1.Size = new System.Drawing.Size(248, 40);
            this.txtLocation1.TabIndex = 4;
            this.txtLocation1.TextChanged += new System.EventHandler(this.txtLocation1_TextChanged);
            // 
            // Label4
            // 
            this.Label4.Location = new System.Drawing.Point(20, 194);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(127, 18);
            this.Label4.TabIndex = 13;
            this.Label4.Text = "LOCATION 4 CAPTION";
            // 
            // Label3
            // 
            this.Label3.Location = new System.Drawing.Point(20, 144);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(127, 18);
            this.Label3.TabIndex = 12;
            this.Label3.Text = "LOCATION 3 CAPTION";
            // 
            // Label2
            // 
            this.Label2.Location = new System.Drawing.Point(20, 94);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(127, 18);
            this.Label2.TabIndex = 11;
            this.Label2.Text = "LOCATION 2 CAPTION";
            // 
            // Label1
            // 
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(124, 18);
            this.Label1.TabIndex = 10;
            this.Label1.Text = "LOCATION 1 CAPTION";
            // 
            // fraSettings
            // 
            this.fraSettings.Controls.Add(this.chkSearch);
            this.fraSettings.Location = new System.Drawing.Point(30, 30);
            this.fraSettings.Name = "fraSettings";
            this.fraSettings.Size = new System.Drawing.Size(357, 78);
            this.fraSettings.TabIndex = 8;
            this.fraSettings.Text = "Search Parameters";
            // 
            // chkSearch
            // 
            this.chkSearch.Location = new System.Drawing.Point(20, 30);
            this.chkSearch.Name = "chkSearch";
            this.chkSearch.Size = new System.Drawing.Size(308, 27);
            this.chkSearch.Text = "Refresh search grid on each key press";
            this.chkSearch.CheckedChanged += new System.EventHandler(this.chkSearch_CheckedChanged);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuSP1,
            this.mnuSave,
            this.mnuSaveExit,
            this.mnuSP2,
            this.mnuExit});
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = 0;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            this.mnuSP1.Visible = false;
            // 
            // mnuSave
            // 
            this.mnuSave.Index = 1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                 ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = 2;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = 3;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = 4;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(30, 458);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // frmSettings
            // 
            this.ClientSize = new System.Drawing.Size(995, 588);
            this.KeyPreview = true;
            this.Name = "frmSettings";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Customize";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.FormUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_Unload);
            this.Load += new System.EventHandler(this.frmSettings_Load);
            this.Activated += new System.EventHandler(this.frmSettings_Activated);
            this.KeyDown += new Wisej.Web.KeyEventHandler(this.frmSettings_KeyDown);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSettings_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Frame3)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDeptDiv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame2)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkVendors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.fraSettings)).EndInit();
            this.fraSettings.ResumeLayout(false);
            this.fraSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkSearch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdSave;
	}
}
