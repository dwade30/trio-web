﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmInitialDepreciation.
	/// </summary>
	partial class frmInitialDepreciation : BaseForm
	{
		public fecherFoundation.FCFrame fraSLDepreciation;
		public Global.T2KBackFillDecimal txtSLInitialDepreciationAmount;
		public Global.T2KDateBox txtSLInitialDepreciationDate;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCFrame fraDBDepreciation;
		public Global.T2KBackFillDecimal txtDBInitialDepreciationAmount;
		public Global.T2KDateBox txtDBInitialDepreciationDate;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCComboBox cboDepreciationType;
		public fecherFoundation.FCFrame fraUNDepreciation;
		public Global.T2KBackFillWhole txtUNInitialAmountUsed;
		public Global.T2KBackFillDecimal txtUNInitialDepreciationAmount;
		public Global.T2KDateBox txtUNInitialDepreciationDate;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel Label11;
		public fecherFoundation.FCLabel lblInitialAmountUsed;
		public fecherFoundation.FCLabel lblDepreciationType;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuProcessSave;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.fraSLDepreciation = new fecherFoundation.FCFrame();
			this.txtSLInitialDepreciationAmount = new Global.T2KBackFillDecimal();
			this.txtSLInitialDepreciationDate = new Global.T2KDateBox();
			this.Label5 = new fecherFoundation.FCLabel();
			this.Label4 = new fecherFoundation.FCLabel();
			this.fraDBDepreciation = new fecherFoundation.FCFrame();
			this.txtDBInitialDepreciationAmount = new Global.T2KBackFillDecimal();
			this.txtDBInitialDepreciationDate = new Global.T2KDateBox();
			this.Label3 = new fecherFoundation.FCLabel();
			this.Label2 = new fecherFoundation.FCLabel();
			this.cboDepreciationType = new fecherFoundation.FCComboBox();
			this.fraUNDepreciation = new fecherFoundation.FCFrame();
			this.txtUNInitialAmountUsed = new Global.T2KBackFillWhole();
			this.txtUNInitialDepreciationAmount = new Global.T2KBackFillDecimal();
			this.txtUNInitialDepreciationDate = new Global.T2KDateBox();
			this.Label10 = new fecherFoundation.FCLabel();
			this.Label11 = new fecherFoundation.FCLabel();
			this.lblInitialAmountUsed = new fecherFoundation.FCLabel();
			this.lblDepreciationType = new fecherFoundation.FCLabel();
			this.MainMenu1 = new fecherFoundation.FCMenuStrip();
			this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessSave = new fecherFoundation.FCToolStripMenuItem();
			this.Seperator = new fecherFoundation.FCToolStripMenuItem();
			this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
			this.btnSave = new fecherFoundation.FCButton();
			this.ClientArea.SuspendLayout();
			this.TopPanel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSLDepreciation)).BeginInit();
			this.fraSLDepreciation.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtSLInitialDepreciationAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSLInitialDepreciationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDBDepreciation)).BeginInit();
			this.fraDBDepreciation.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDBInitialDepreciationAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDBInitialDepreciationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fraUNDepreciation)).BeginInit();
			this.fraUNDepreciation.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtUNInitialAmountUsed)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUNInitialDepreciationAmount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUNInitialDepreciationDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).BeginInit();
			this.SuspendLayout();
			// 
			// BottomPanel
			// 
			this.BottomPanel.Location = new System.Drawing.Point(0, 570);
			this.BottomPanel.Size = new System.Drawing.Size(845, 21);
			// 
			// ClientArea
			// 
			this.ClientArea.Controls.Add(this.btnSave);
			this.ClientArea.Controls.Add(this.fraSLDepreciation);
			this.ClientArea.Controls.Add(this.fraDBDepreciation);
			this.ClientArea.Controls.Add(this.cboDepreciationType);
			this.ClientArea.Controls.Add(this.fraUNDepreciation);
			this.ClientArea.Controls.Add(this.lblDepreciationType);
			this.ClientArea.Size = new System.Drawing.Size(845, 510);
			// 
			// TopPanel
			// 
			this.TopPanel.Size = new System.Drawing.Size(845, 60);
			// 
			// HeaderText
			// 
			this.HeaderText.Location = new System.Drawing.Point(30, 26);
			this.HeaderText.Size = new System.Drawing.Size(215, 30);
			this.HeaderText.Text = "Initial Depreciation";
			// 
			// fraSLDepreciation
			// 
			this.fraSLDepreciation.Controls.Add(this.txtSLInitialDepreciationAmount);
			this.fraSLDepreciation.Controls.Add(this.txtSLInitialDepreciationDate);
			this.fraSLDepreciation.Controls.Add(this.Label5);
			this.fraSLDepreciation.Controls.Add(this.Label4);
			this.fraSLDepreciation.FormatCaption = false;
			this.fraSLDepreciation.Location = new System.Drawing.Point(30, 274);
			this.fraSLDepreciation.Name = "fraSLDepreciation";
			this.fraSLDepreciation.Size = new System.Drawing.Size(372, 150);
			this.fraSLDepreciation.TabIndex = 16;
			this.fraSLDepreciation.Text = "SL Depreciation";
			this.fraSLDepreciation.Visible = false;
			// 
			// txtSLInitialDepreciationAmount
			// 
			this.txtSLInitialDepreciationAmount.MaxLength = 12;
			this.txtSLInitialDepreciationAmount.Location = new System.Drawing.Point(231, 90);
			this.txtSLInitialDepreciationAmount.MaxLength = 12;
			this.txtSLInitialDepreciationAmount.Name = "txtSLInitialDepreciationAmount";
			this.txtSLInitialDepreciationAmount.Size = new System.Drawing.Size(116, 40);
			this.txtSLInitialDepreciationAmount.TabIndex = 5;
			this.txtSLInitialDepreciationAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtSLInitialDepreciationDate
			// 
			this.txtSLInitialDepreciationDate.MaxLength = 10;
			this.txtSLInitialDepreciationDate.Location = new System.Drawing.Point(231, 30);
			this.txtSLInitialDepreciationDate.Mask = "##/##/####";
			this.txtSLInitialDepreciationDate.Name = "txtSLInitialDepreciationDate";
			this.txtSLInitialDepreciationDate.Size = new System.Drawing.Size(116, 40);
			this.txtSLInitialDepreciationDate.TabIndex = 4;
			this.txtSLInitialDepreciationDate.Text = "  /  /";
			// 
			// Label5
			// 
			this.Label5.Location = new System.Drawing.Point(20, 44);
			this.Label5.Name = "Label5";
			this.Label5.Size = new System.Drawing.Size(120, 17);
			this.Label5.TabIndex = 18;
			this.Label5.Text = "DEPRECIATION DATE";
			// 
			// Label4
			// 
			this.Label4.Location = new System.Drawing.Point(20, 104);
			this.Label4.Name = "Label4";
			this.Label4.Size = new System.Drawing.Size(141, 17);
			this.Label4.TabIndex = 17;
			this.Label4.Text = "DEPRECIATION AMOUNT";
			// 
			// fraDBDepreciation
			// 
			this.fraDBDepreciation.Controls.Add(this.txtDBInitialDepreciationAmount);
			this.fraDBDepreciation.Controls.Add(this.txtDBInitialDepreciationDate);
			this.fraDBDepreciation.Controls.Add(this.Label3);
			this.fraDBDepreciation.Controls.Add(this.Label2);
			this.fraDBDepreciation.FormatCaption = false;
			this.fraDBDepreciation.Location = new System.Drawing.Point(30, 99);
			this.fraDBDepreciation.Name = "fraDBDepreciation";
			this.fraDBDepreciation.Size = new System.Drawing.Size(372, 150);
			this.fraDBDepreciation.TabIndex = 13;
			this.fraDBDepreciation.Text = "DB Depreciation";
			this.fraDBDepreciation.Visible = false;
			// 
			// txtDBInitialDepreciationAmount
			// 
			this.txtDBInitialDepreciationAmount.MaxLength = 12;
			this.txtDBInitialDepreciationAmount.Location = new System.Drawing.Point(231, 90);
			this.txtDBInitialDepreciationAmount.MaxLength = 12;
			this.txtDBInitialDepreciationAmount.Name = "txtDBInitialDepreciationAmount";
			this.txtDBInitialDepreciationAmount.Size = new System.Drawing.Size(116, 40);
			this.txtDBInitialDepreciationAmount.TabIndex = 3;
			this.txtDBInitialDepreciationAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtDBInitialDepreciationDate
			// 
			this.txtDBInitialDepreciationDate.MaxLength = 10;
			this.txtDBInitialDepreciationDate.Location = new System.Drawing.Point(231, 30);
			this.txtDBInitialDepreciationDate.Mask = "##/##/####";
			this.txtDBInitialDepreciationDate.Name = "txtDBInitialDepreciationDate";
			this.txtDBInitialDepreciationDate.Size = new System.Drawing.Size(116, 40);
			this.txtDBInitialDepreciationDate.TabIndex = 2;
			this.txtDBInitialDepreciationDate.Text = "  /  /";
			// 
			// Label3
			// 
			this.Label3.Location = new System.Drawing.Point(20, 104);
			this.Label3.Name = "Label3";
			this.Label3.Size = new System.Drawing.Size(141, 17);
			this.Label3.TabIndex = 15;
			this.Label3.Text = "DEPRECIATION AMOUNT";
			// 
			// Label2
			// 
			this.Label2.Location = new System.Drawing.Point(20, 44);
			this.Label2.Name = "Label2";
			this.Label2.Size = new System.Drawing.Size(120, 17);
			this.Label2.TabIndex = 14;
			this.Label2.Text = "DEPRECIATION DATE";
			// 
			// cboDepreciationType
			// 
			this.cboDepreciationType.AutoSize = false;
			this.cboDepreciationType.BackColor = System.Drawing.SystemColors.Window;
			this.cboDepreciationType.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
			this.cboDepreciationType.FormattingEnabled = true;
			this.cboDepreciationType.Items.AddRange(new object[] {
				"SL Depreciation",
				"DB Depreciation",
				"DB and SL Depreciation"
			});
			this.cboDepreciationType.Location = new System.Drawing.Point(168, 30);
			this.cboDepreciationType.Name = "cboDepreciationType";
			this.cboDepreciationType.Size = new System.Drawing.Size(234, 40);
			this.cboDepreciationType.TabIndex = 1;
			this.cboDepreciationType.SelectedIndexChanged += new System.EventHandler(this.cboDepreciationType_SelectedIndexChanged);
			// 
			// fraUNDepreciation
			// 
			this.fraUNDepreciation.Controls.Add(this.txtUNInitialAmountUsed);
			this.fraUNDepreciation.Controls.Add(this.txtUNInitialDepreciationAmount);
			this.fraUNDepreciation.Controls.Add(this.txtUNInitialDepreciationDate);
			this.fraUNDepreciation.Controls.Add(this.Label10);
			this.fraUNDepreciation.Controls.Add(this.Label11);
			this.fraUNDepreciation.Controls.Add(this.lblInitialAmountUsed);
			this.fraUNDepreciation.FormatCaption = false;
			this.fraUNDepreciation.Location = new System.Drawing.Point(451, 99);
			this.fraUNDepreciation.Name = "fraUNDepreciation";
			this.fraUNDepreciation.Size = new System.Drawing.Size(341, 210);
			this.fraUNDepreciation.TabIndex = 0;
			this.fraUNDepreciation.Text = "SL Depreciation";
			this.fraUNDepreciation.Visible = false;
			// 
			// txtUNInitialAmountUsed
			// 
			this.txtUNInitialAmountUsed.MaxLength = 10;
			this.txtUNInitialAmountUsed.Location = new System.Drawing.Point(231, 150);
			this.txtUNInitialAmountUsed.MaxLength = 10;
			this.txtUNInitialAmountUsed.Name = "txtUNInitialAmountUsed";
			this.txtUNInitialAmountUsed.Size = new System.Drawing.Size(90, 40);
			this.txtUNInitialAmountUsed.TabIndex = 8;
			this.txtUNInitialAmountUsed.Visible = false;
			// 
			// txtUNInitialDepreciationAmount
			// 
			this.txtUNInitialDepreciationAmount.MaxLength = 12;
			this.txtUNInitialDepreciationAmount.Location = new System.Drawing.Point(231, 90);
			this.txtUNInitialDepreciationAmount.MaxLength = 12;
			this.txtUNInitialDepreciationAmount.Name = "txtUNInitialDepreciationAmount";
			this.txtUNInitialDepreciationAmount.Size = new System.Drawing.Size(90, 40);
			this.txtUNInitialDepreciationAmount.TabIndex = 7;
			this.txtUNInitialDepreciationAmount.TextAlign = Wisej.Web.HorizontalAlignment.Right;
			// 
			// txtUNInitialDepreciationDate
			// 
			this.txtUNInitialDepreciationDate.MaxLength = 10;
			this.txtUNInitialDepreciationDate.Location = new System.Drawing.Point(231, 30);
			this.txtUNInitialDepreciationDate.Mask = "##/##/####";
			this.txtUNInitialDepreciationDate.Name = "txtUNInitialDepreciationDate";
			this.txtUNInitialDepreciationDate.Size = new System.Drawing.Size(90, 40);
			this.txtUNInitialDepreciationDate.TabIndex = 6;
			this.txtUNInitialDepreciationDate.Text = "  /  /";
			// 
			// Label10
			// 
			this.Label10.Location = new System.Drawing.Point(20, 44);
			this.Label10.Name = "Label10";
			this.Label10.Size = new System.Drawing.Size(120, 17);
			this.Label10.TabIndex = 11;
			this.Label10.Text = "DEPRECIATION DATE";
			// 
			// Label11
			// 
			this.Label11.Location = new System.Drawing.Point(20, 104);
			this.Label11.Name = "Label11";
			this.Label11.Size = new System.Drawing.Size(141, 17);
			this.Label11.TabIndex = 10;
			this.Label11.Text = "DEPRECIATION AMOUNT";
			// 
			// lblInitialAmountUsed
			// 
			this.lblInitialAmountUsed.Location = new System.Drawing.Point(20, 164);
			this.lblInitialAmountUsed.Name = "lblInitialAmountUsed";
			this.lblInitialAmountUsed.Size = new System.Drawing.Size(90, 17);
			this.lblInitialAmountUsed.TabIndex = 9;
			this.lblInitialAmountUsed.Text = "AMOUNT USED";
			this.lblInitialAmountUsed.Visible = false;
			// 
			// lblDepreciationType
			// 
			this.lblDepreciationType.Location = new System.Drawing.Point(30, 44);
			this.lblDepreciationType.Name = "lblDepreciationType";
			this.lblDepreciationType.Size = new System.Drawing.Size(132, 17);
			this.lblDepreciationType.TabIndex = 12;
			this.lblDepreciationType.Text = "DEPRECIATION TYPE";
			// 
			// MainMenu1
			// 
			this.MainMenu1.Name = null;
			// 
			// mnuProcess
			// 
			this.mnuProcess.Index = -1;
			this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
				this.mnuProcessSave,
				this.Seperator,
				this.mnuProcessQuit
			});
			this.mnuProcess.Name = "mnuProcess";
			this.mnuProcess.Text = "File";
			// 
			// mnuProcessSave
			// 
			this.mnuProcessSave.Index = 0;
			this.mnuProcessSave.Name = "mnuProcessSave";
			this.mnuProcessSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.mnuProcessSave.Text = "Save & Exit";
			this.mnuProcessSave.Click += new System.EventHandler(this.mnuProcessSave_Click);
			// 
			// Seperator
			// 
			this.Seperator.Index = 1;
			this.Seperator.Name = "Seperator";
			this.Seperator.Text = "-";
			// 
			// mnuProcessQuit
			// 
			this.mnuProcessQuit.Index = 2;
			this.mnuProcessQuit.Name = "mnuProcessQuit";
			this.mnuProcessQuit.Text = "Exit";
			this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
			// 
			// btnSave
			// 
			this.btnSave.AppearanceKey = "acceptButton";
			this.btnSave.Location = new System.Drawing.Point(30, 453);
			this.btnSave.Name = "btnSave";
			this.btnSave.Shortcut = Wisej.Web.Shortcut.F12;
			this.btnSave.Size = new System.Drawing.Size(80, 48);
			this.btnSave.TabIndex = 17;
			this.btnSave.Text = "Save";
			this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
			// 
			// frmInitialDepreciation
			// 
			this.ClientSize = new System.Drawing.Size(845, 591);
			this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
			this.KeyPreview = true;
			this.Menu = this.MainMenu1;
			this.Name = "frmInitialDepreciation";
			this.StartPosition = Wisej.Web.FormStartPosition.CenterScreen;
			this.Text = "Initial Depreciation";
			this.Load += new System.EventHandler(this.frmInitialDepreciation_Load);
			this.Activated += new System.EventHandler(this.frmInitialDepreciation_Activated);
			this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmInitialDepreciation_KeyPress);
			this.ClientArea.ResumeLayout(false);
			this.TopPanel.ResumeLayout(false);
			this.TopPanel.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.fraSLDepreciation)).EndInit();
			this.fraSLDepreciation.ResumeLayout(false);
			this.fraSLDepreciation.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtSLInitialDepreciationAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtSLInitialDepreciationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraDBDepreciation)).EndInit();
			this.fraDBDepreciation.ResumeLayout(false);
			this.fraDBDepreciation.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtDBInitialDepreciationAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtDBInitialDepreciationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fraUNDepreciation)).EndInit();
			this.fraUNDepreciation.ResumeLayout(false);
			this.fraUNDepreciation.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.txtUNInitialAmountUsed)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUNInitialDepreciationAmount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.txtUNInitialDepreciationDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.btnSave)).EndInit();
			this.ResumeLayout(false);
		}
		#endregion

		private FCButton btnSave;
	}
}
