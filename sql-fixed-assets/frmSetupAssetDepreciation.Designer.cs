﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;
using GrapeCity.ActiveReports;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmSetupAssetDepreciation.
	/// </summary>
	partial class frmSetupAssetDepreciation : BaseForm
	{
		public fecherFoundation.FCCheckBox chkIncludeDiscarded;
		public fecherFoundation.FCFrame fraDateRange;
		public fecherFoundation.FCButton cmdStartDate;
		public fecherFoundation.FCButton cmdEndDate;
		public Global.T2KDateBox txtStartDate;
		public Global.T2KDateBox txtEndDate;
		public fecherFoundation.FCLabel lblTo;
		public fecherFoundation.FCCheckBox chkZeroCurDep;
		public fecherFoundation.FCCheckBox chkDep;
		//private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuProcess;
		public fecherFoundation.FCToolStripMenuItem mnuFilePrint;
		public fecherFoundation.FCToolStripMenuItem mnuFilePreview;
		public fecherFoundation.FCToolStripMenuItem Seperator;
		public fecherFoundation.FCToolStripMenuItem mnuProcessQuit;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.chkIncludeDiscarded = new fecherFoundation.FCCheckBox();
            this.fraDateRange = new fecherFoundation.FCFrame();
            this.cmdStartDate = new fecherFoundation.FCButton();
            this.cmdEndDate = new fecherFoundation.FCButton();
            this.txtStartDate = new Global.T2KDateBox();
            this.txtEndDate = new Global.T2KDateBox();
            this.lblTo = new fecherFoundation.FCLabel();
            this.chkZeroCurDep = new fecherFoundation.FCCheckBox();
            this.chkDep = new fecherFoundation.FCCheckBox();
            this.mnuProcess = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePrint = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFilePreview = new fecherFoundation.FCToolStripMenuItem();
            this.Seperator = new fecherFoundation.FCToolStripMenuItem();
            this.mnuProcessQuit = new fecherFoundation.FCToolStripMenuItem();
            this.cmdPrintPreview = new fecherFoundation.FCButton();
            this.fcButton1 = new fecherFoundation.FCButton();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeDiscarded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).BeginInit();
            this.fraDateRange.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkZeroCurDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDep)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fcButton1)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Location = new System.Drawing.Point(0, 327);
            this.BottomPanel.Size = new System.Drawing.Size(533, 13);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.cmdPrintPreview);
            this.ClientArea.Controls.Add(this.chkIncludeDiscarded);
            this.ClientArea.Controls.Add(this.fraDateRange);
            this.ClientArea.Controls.Add(this.chkZeroCurDep);
            this.ClientArea.Controls.Add(this.chkDep);
            this.ClientArea.Size = new System.Drawing.Size(553, 441);
            this.ClientArea.Controls.SetChildIndex(this.chkDep, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkZeroCurDep, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraDateRange, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkIncludeDiscarded, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmdPrintPreview, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.fcButton1);
            this.TopPanel.Size = new System.Drawing.Size(553, 60);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.fcButton1, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(300, 30);
            this.HeaderText.Text = "Asset Depreciation Report";
            // 
            // chkIncludeDiscarded
            // 
            this.chkIncludeDiscarded.Location = new System.Drawing.Point(30, 217);
            this.chkIncludeDiscarded.Name = "chkIncludeDiscarded";
            this.chkIncludeDiscarded.Size = new System.Drawing.Size(180, 24);
            this.chkIncludeDiscarded.TabIndex = 11;
            this.chkIncludeDiscarded.Text = "Include Discarded Items";
            // 
            // fraDateRange
            // 
            this.fraDateRange.Controls.Add(this.cmdStartDate);
            this.fraDateRange.Controls.Add(this.cmdEndDate);
            this.fraDateRange.Controls.Add(this.txtStartDate);
            this.fraDateRange.Controls.Add(this.txtEndDate);
            this.fraDateRange.Controls.Add(this.lblTo);
            this.fraDateRange.Location = new System.Drawing.Point(30, 30);
            this.fraDateRange.Name = "fraDateRange";
            this.fraDateRange.Size = new System.Drawing.Size(397, 90);
            this.fraDateRange.TabIndex = 8;
            this.fraDateRange.Text = "Current Depreciation Range";
            // 
            // cmdStartDate
            // 
            this.cmdStartDate.AppearanceKey = "imageButton";
            this.cmdStartDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdStartDate.Location = new System.Drawing.Point(139, 30);
            this.cmdStartDate.Name = "cmdStartDate";
            this.cmdStartDate.Size = new System.Drawing.Size(40, 40);
            this.cmdStartDate.TabIndex = 1;
            this.cmdStartDate.Click += new System.EventHandler(this.cmdStartDate_Click);
            // 
            // cmdEndDate
            // 
            this.cmdEndDate.AppearanceKey = "imageButton";
            this.cmdEndDate.ImageSource = "icon - calendar?color=#707884";
            this.cmdEndDate.Location = new System.Drawing.Point(349, 30);
            this.cmdEndDate.Name = "cmdEndDate";
            this.cmdEndDate.Size = new System.Drawing.Size(40, 40);
            this.cmdEndDate.TabIndex = 3;
            this.cmdEndDate.Click += new System.EventHandler(this.cmdEndDate_Click);
            // 
            // txtStartDate
            // 
            this.txtStartDate.Location = new System.Drawing.Point(20, 30);
            this.txtStartDate.Mask = "##/##/####";
            this.txtStartDate.MaxLength = 10;
            this.txtStartDate.Name = "txtStartDate";
            this.txtStartDate.Size = new System.Drawing.Size(115, 22);
            // 
            // txtEndDate
            // 
            this.txtEndDate.Location = new System.Drawing.Point(229, 30);
            this.txtEndDate.Mask = "##/##/####";
            this.txtEndDate.MaxLength = 10;
            this.txtEndDate.Name = "txtEndDate";
            this.txtEndDate.Size = new System.Drawing.Size(115, 22);
            this.txtEndDate.TabIndex = 2;
            // 
            // lblTo
            // 
            this.lblTo.Enabled = false;
            this.lblTo.Location = new System.Drawing.Point(188, 44);
            this.lblTo.Name = "lblTo";
            this.lblTo.Size = new System.Drawing.Size(36, 23);
            this.lblTo.TabIndex = 9;
            this.lblTo.Text = "TO";
            // 
            // chkZeroCurDep
            // 
            this.chkZeroCurDep.Location = new System.Drawing.Point(30, 185);
            this.chkZeroCurDep.Name = "chkZeroCurDep";
            this.chkZeroCurDep.Size = new System.Drawing.Size(312, 24);
            this.chkZeroCurDep.TabIndex = 7;
            this.chkZeroCurDep.Text = "Include Assets With No Current Depreciation";
            // 
            // chkDep
            // 
            this.chkDep.Location = new System.Drawing.Point(30, 152);
            this.chkDep.Name = "chkDep";
            this.chkDep.Size = new System.Drawing.Size(284, 24);
            this.chkDep.TabIndex = 6;
            this.chkDep.Text = "Include Completeley Depreciated Assets";
            // 
            // mnuProcess
            // 
            this.mnuProcess.Index = -1;
            this.mnuProcess.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFilePrint,
            this.mnuFilePreview,
            this.Seperator,
            this.mnuProcessQuit});
            this.mnuProcess.Name = "mnuProcess";
            this.mnuProcess.Text = "File";
            // 
            // mnuFilePrint
            // 
            this.mnuFilePrint.Index = 0;
            this.mnuFilePrint.Name = "mnuFilePrint";
            this.mnuFilePrint.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuFilePrint.Text = "Print";
            this.mnuFilePrint.Click += new System.EventHandler(this.mnuFilePrint_Click);
            // 
            // mnuFilePreview
            // 
            this.mnuFilePreview.Index = 1;
            this.mnuFilePreview.Name = "mnuFilePreview";
            this.mnuFilePreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuFilePreview.Text = "Print Preview";
            this.mnuFilePreview.Click += new System.EventHandler(this.mnuFilePreview_Click);
            // 
            // Seperator
            // 
            this.Seperator.Index = 2;
            this.Seperator.Name = "Seperator";
            this.Seperator.Text = "-";
            // 
            // mnuProcessQuit
            // 
            this.mnuProcessQuit.Index = 3;
            this.mnuProcessQuit.Name = "mnuProcessQuit";
            this.mnuProcessQuit.Text = "Exit";
            this.mnuProcessQuit.Click += new System.EventHandler(this.mnuProcessQuit_Click);
            // 
            // cmdPrintPreview
            // 
            this.cmdPrintPreview.AppearanceKey = "acceptButton";
            this.cmdPrintPreview.Location = new System.Drawing.Point(30, 279);
            this.cmdPrintPreview.Name = "cmdPrintPreview";
            this.cmdPrintPreview.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdPrintPreview.Size = new System.Drawing.Size(133, 48);
            this.cmdPrintPreview.TabIndex = 12;
            this.cmdPrintPreview.Text = "Print Preview";
            this.cmdPrintPreview.Click += new System.EventHandler(this.cmdPrintPreview_Click);
            // 
            // fcButton1
            // 
            this.fcButton1.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.fcButton1.Location = new System.Drawing.Point(487, 29);
            this.fcButton1.Name = "fcButton1";
            this.fcButton1.Shortcut = Wisej.Web.Shortcut.F11;
            this.fcButton1.Size = new System.Drawing.Size(50, 24);
            this.fcButton1.TabIndex = 1;
            this.fcButton1.Text = "Print";
            this.fcButton1.Click += new System.EventHandler(this.fcButton1_Click);
            // 
            // frmSetupAssetDepreciation
            // 
            this.ClientSize = new System.Drawing.Size(553, 501);
            this.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.KeyPreview = true;
            this.Name = "frmSetupAssetDepreciation";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Asset Depreciation Report";
            this.Load += new System.EventHandler(this.frmSetupAssetDepreciation_Load);
            this.Activated += new System.EventHandler(this.frmSetupAssetDepreciation_Activated);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmSetupAssetDepreciation_KeyPress);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkIncludeDiscarded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDateRange)).EndInit();
            this.fraDateRange.ResumeLayout(false);
            this.fraDateRange.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtStartDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtEndDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkZeroCurDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkDep)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPrintPreview)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fcButton1)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private FCButton cmdPrintPreview;
		private FCButton fcButton1;
	}
}
