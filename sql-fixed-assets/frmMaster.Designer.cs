﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using System.Drawing;
using System.Runtime.InteropServices;
using Global;

namespace TWFA0000
{
	/// <summary>
	/// Summary description for frmMaster.
	/// </summary>
	partial class frmMaster : BaseForm
	{
		public fecherFoundation.FCLabel lblDepreciation;
		public fecherFoundation.FCComboBox cmbDepreciation;
		public System.Collections.Generic.List<fecherFoundation.FCLabel> lblUnits;
		public fecherFoundation.FCFrame fraWarranty;
		public fecherFoundation.FCTextBox txtWarrantyDescription;
		public fecherFoundation.FCMaskedTextBox mskWarrantyDate;
		public fecherFoundation.FCLabel Label3;
		public fecherFoundation.FCLabel Label1;
		public fecherFoundation.FCCheckBox chkDiscarded;
		public fecherFoundation.FCFrame fraDiscardedDate;
		public Global.T2KDateBox txtDiscardedDate;
		public fecherFoundation.FCTextBox txtImprovements;
		public Global.T2KBackFillDecimal txtSalvageValue;
		public fecherFoundation.FCTextBox txtTotalDepreciated;
		public fecherFoundation.FCComboBox cboClassCode;
		public fecherFoundation.FCComboBox cboDiv;
		public fecherFoundation.FCComboBox cboDept;
		public fecherFoundation.FCFrame fraDepreciationMethod;
		public fecherFoundation.FCComboBox cboCondition;
		public Global.T2KBackFillWhole txtTotalUsed;
		public fecherFoundation.FCComboBox cboRate;
		public fecherFoundation.FCTextBox txtUN;
		public Global.T2KBackFillWhole txtLife;
		public fecherFoundation.FCLabel lblCondition;
		public fecherFoundation.FCLabel lblUM;
		public fecherFoundation.FCLabel lblRate;
		public fecherFoundation.FCLabel lblUnits_1;
		public fecherFoundation.FCLabel lblUnits_0;
		public fecherFoundation.FCLabel lblTotalUsed;
		public fecherFoundation.FCLabel lblLife;
		public fecherFoundation.FCFrame fraDepreciationsPosted;
		public fecherFoundation.FCTextBox txtValue;
		public fecherFoundation.FCGrid vsDepreciationsPosted;
		public fecherFoundation.FCLabel Label9;
		public FCGrid txtAcct2;
		public FCGrid txtAcct4;
		public FCGrid txtAcct1;
		public FCGrid txtAcct3;
		public fecherFoundation.FCLabel Label8;
		public fecherFoundation.FCLabel Label6;
		public fecherFoundation.FCLabel Label5;
		public fecherFoundation.FCLabel Label2;
		public fecherFoundation.FCCheckBox chkExpensedFirstYear;
		public fecherFoundation.FCCheckBox chkFederalFunds;
		public fecherFoundation.FCMaskedTextBox mskPurchaseDate;
		public fecherFoundation.FCComboBox cboVendor;
		public fecherFoundation.FCTextBox txtTagNumber;
		public fecherFoundation.FCTextBox txtSerialNumber;
		public fecherFoundation.FCTextBox txtModel;
		public fecherFoundation.FCTextBox txtMake;
		public fecherFoundation.FCTextBox txtYear;
		public fecherFoundation.FCTextBox txtDescription;
		public fecherFoundation.FCMaskedTextBox mskDateLastDepreciation;
		public fecherFoundation.FCTextBox txtTotalPosted;
		public fecherFoundation.FCMaskedTextBox mebOutOfServiceDate;
		public Global.T2KBackFillDecimal txtBasisCost;
		public Global.T2KBackFillDecimal txtReplacementCost;
		public fecherFoundation.FCPictureBox Image1;
		public fecherFoundation.FCLabel Label10;
		public fecherFoundation.FCLabel lblImprovements;
		public fecherFoundation.FCLabel Label7;
		public fecherFoundation.FCLabel Label4;
		public fecherFoundation.FCLabel txtDeptDivDescription;
		public fecherFoundation.FCLabel lblDeptDiv;
		public fecherFoundation.FCLabel lblDash;
		public fecherFoundation.FCLabel lblDateLastDepreciation;
		public fecherFoundation.FCLabel lblTotalPosted;
		public fecherFoundation.FCLabel lblSalvageValue;
		public fecherFoundation.FCLabel lblBasicCost;
		public fecherFoundation.FCLabel lblClassCode;
		public fecherFoundation.FCLabel lblPurchaseDate;
		public fecherFoundation.FCLabel lblVendor;
		public fecherFoundation.FCLabel lblTagNumber;
		public fecherFoundation.FCLabel lblSerialNumber;
		public fecherFoundation.FCLabel lblModel;
		public fecherFoundation.FCLabel lblMake;
		public fecherFoundation.FCLabel lblYear;
		public fecherFoundation.FCLabel lblItem;
		private fecherFoundation.FCMenuStrip MainMenu1;
		public fecherFoundation.FCToolStripMenuItem mnuFile;
		public fecherFoundation.FCToolStripMenuItem mnuFileComments;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator2;
		public fecherFoundation.FCToolStripMenuItem mnuNew;
		public fecherFoundation.FCToolStripMenuItem mnuDelete;
		public fecherFoundation.FCToolStripMenuItem mnuSP2;
		public fecherFoundation.FCToolStripMenuItem mnuSearch;
		public fecherFoundation.FCToolStripMenuItem mnuLocations;
		public fecherFoundation.FCToolStripMenuItem mnuVendors;
		public fecherFoundation.FCToolStripMenuItem mnuDeptDiv;
		public fecherFoundation.FCToolStripMenuItem mnuClass;
		public fecherFoundation.FCToolStripMenuItem mnuLocationCaptions;
		public fecherFoundation.FCToolStripMenuItem mnuFileSeperator10;
		public fecherFoundation.FCToolStripMenuItem mnuFileRemoveDepreciation;
		public fecherFoundation.FCToolStripMenuItem mnuDepreciate;
		public fecherFoundation.FCToolStripMenuItem mnuFileInitialDepreciation;
		public fecherFoundation.FCToolStripMenuItem mnuSP3;
		public fecherFoundation.FCToolStripMenuItem mnuSave;
		public fecherFoundation.FCToolStripMenuItem mnuSaveExit;
		public fecherFoundation.FCToolStripMenuItem mnuSP1;
		public fecherFoundation.FCToolStripMenuItem mnuExit;
		private Wisej.Web.ToolTip ToolTip1;

		protected override void Dispose(bool disposing)
		{
			if (_InstancePtr == this)
			{
				_InstancePtr = null;
				Sys.ClearInstance(this);
			}
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            this.lblDepreciation = new fecherFoundation.FCLabel();
            this.cmbDepreciation = new fecherFoundation.FCComboBox();
            this.fraWarranty = new fecherFoundation.FCFrame();
            this.txtWarrantyDescription = new fecherFoundation.FCTextBox();
            this.mskWarrantyDate = new fecherFoundation.FCMaskedTextBox();
            this.Label3 = new fecherFoundation.FCLabel();
            this.Label1 = new fecherFoundation.FCLabel();
            this.chkDiscarded = new fecherFoundation.FCCheckBox();
            this.fraDiscardedDate = new fecherFoundation.FCFrame();
            this.txtDiscardedDate = new Global.T2KDateBox();
            this.txtImprovements = new fecherFoundation.FCTextBox();
            this.txtSalvageValue = new Global.T2KBackFillDecimal();
            this.txtTotalDepreciated = new fecherFoundation.FCTextBox();
            this.cboClassCode = new fecherFoundation.FCComboBox();
            this.cboDiv = new fecherFoundation.FCComboBox();
            this.cboDept = new fecherFoundation.FCComboBox();
            this.cmbLocation = new Wisej.Web.ComboBox();
            this.cmbLocationType = new fecherFoundation.FCComboBox();
            this.fraDepreciationMethod = new fecherFoundation.FCFrame();
            this.cboCondition = new fecherFoundation.FCComboBox();
            this.txtTotalUsed = new Global.T2KBackFillWhole();
            this.cboRate = new fecherFoundation.FCComboBox();
            this.txtUN = new fecherFoundation.FCTextBox();
            this.txtLife = new Global.T2KBackFillWhole();
            this.lblCondition = new fecherFoundation.FCLabel();
            this.lblUM = new fecherFoundation.FCLabel();
            this.lblRate = new fecherFoundation.FCLabel();
            this.lblUnits_1 = new fecherFoundation.FCLabel();
            this.lblUnits_0 = new fecherFoundation.FCLabel();
            this.lblTotalUsed = new fecherFoundation.FCLabel();
            this.lblLife = new fecherFoundation.FCLabel();
            this.fraDepreciationsPosted = new fecherFoundation.FCFrame();
            this.txtValue = new fecherFoundation.FCTextBox();
            this.vsDepreciationsPosted = new fecherFoundation.FCGrid();
            this.Label9 = new fecherFoundation.FCLabel();
            this.txtAcct2 = new fecherFoundation.FCGrid();
            this.txtAcct4 = new fecherFoundation.FCGrid();
            this.txtAcct1 = new fecherFoundation.FCGrid();
            this.txtAcct3 = new fecherFoundation.FCGrid();
            this.Label8 = new fecherFoundation.FCLabel();
            this.Label6 = new fecherFoundation.FCLabel();
            this.Label5 = new fecherFoundation.FCLabel();
            this.Label2 = new fecherFoundation.FCLabel();
            this.chkExpensedFirstYear = new fecherFoundation.FCCheckBox();
            this.chkFederalFunds = new fecherFoundation.FCCheckBox();
            this.mskPurchaseDate = new fecherFoundation.FCMaskedTextBox();
            this.cboVendor = new fecherFoundation.FCComboBox();
            this.txtTagNumber = new fecherFoundation.FCTextBox();
            this.txtSerialNumber = new fecherFoundation.FCTextBox();
            this.txtModel = new fecherFoundation.FCTextBox();
            this.txtMake = new fecherFoundation.FCTextBox();
            this.txtYear = new fecherFoundation.FCTextBox();
            this.txtDescription = new fecherFoundation.FCTextBox();
            this.mskDateLastDepreciation = new fecherFoundation.FCMaskedTextBox();
            this.txtTotalPosted = new fecherFoundation.FCTextBox();
            this.mebOutOfServiceDate = new fecherFoundation.FCMaskedTextBox();
            this.txtBasisCost = new Global.T2KBackFillDecimal();
            this.txtReplacementCost = new Global.T2KBackFillDecimal();
            this.Image1 = new fecherFoundation.FCPictureBox();
            this.Label10 = new fecherFoundation.FCLabel();
            this.lblImprovements = new fecherFoundation.FCLabel();
            this.Label7 = new fecherFoundation.FCLabel();
            this.Label4 = new fecherFoundation.FCLabel();
            this.txtDeptDivDescription = new fecherFoundation.FCLabel();
            this.lblDeptDiv = new fecherFoundation.FCLabel();
            this.lblDash = new fecherFoundation.FCLabel();
            this.lblDateLastDepreciation = new fecherFoundation.FCLabel();
            this.lblTotalPosted = new fecherFoundation.FCLabel();
            this.lblSalvageValue = new fecherFoundation.FCLabel();
            this.lblBasicCost = new fecherFoundation.FCLabel();
            this.lblClassCode = new fecherFoundation.FCLabel();
            this.lblPurchaseDate = new fecherFoundation.FCLabel();
            this.lblVendor = new fecherFoundation.FCLabel();
            this.lblTagNumber = new fecherFoundation.FCLabel();
            this.lblSerialNumber = new fecherFoundation.FCLabel();
            this.lblModel = new fecherFoundation.FCLabel();
            this.lblMake = new fecherFoundation.FCLabel();
            this.lblYear = new fecherFoundation.FCLabel();
            this.lblItem = new fecherFoundation.FCLabel();
            this.MainMenu1 = new fecherFoundation.FCMenuStrip();
            this.mnuFileComments = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSearch = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLocations = new fecherFoundation.FCToolStripMenuItem();
            this.mnuVendors = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDeptDiv = new fecherFoundation.FCToolStripMenuItem();
            this.mnuClass = new fecherFoundation.FCToolStripMenuItem();
            this.mnuLocationCaptions = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileSeperator10 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileInitialDepreciation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP1 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuExit = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFile = new fecherFoundation.FCToolStripMenuItem();
            this.mnuNew = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDelete = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP2 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuFileRemoveDepreciation = new fecherFoundation.FCToolStripMenuItem();
            this.mnuDepreciate = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSP3 = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSave = new fecherFoundation.FCToolStripMenuItem();
            this.mnuSaveExit = new fecherFoundation.FCToolStripMenuItem();
            this.ToolTip1 = new Wisej.Web.ToolTip(this.components);
            this.cmdNew = new fecherFoundation.FCButton();
            this.cmdDelete = new fecherFoundation.FCButton();
            this.cmdSave = new fecherFoundation.FCButton();
            this.cmdRemoveLast = new fecherFoundation.FCButton();
            this.cmdPostNew = new fecherFoundation.FCButton();
            this.fcLabel1 = new fecherFoundation.FCLabel();
            this.BottomPanel.SuspendLayout();
            this.ClientArea.SuspendLayout();
            this.TopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWarranty)).BeginInit();
            this.fraWarranty.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDiscarded)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDiscardedDate)).BeginInit();
            this.fraDiscardedDate.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscardedDate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalvageValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDepreciationMethod)).BeginInit();
            this.fraDepreciationMethod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalUsed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLife)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDepreciationsPosted)).BeginInit();
            this.fraDepreciationsPosted.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.vsDepreciationsPosted)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpensedFirstYear)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFederalFunds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBasisCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacementCost)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveLast)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPostNew)).BeginInit();
            this.SuspendLayout();
            // 
            // BottomPanel
            // 
            this.BottomPanel.Controls.Add(this.cmdSave);
            this.BottomPanel.Location = new System.Drawing.Point(0, 783);
            this.BottomPanel.Size = new System.Drawing.Size(1063, 96);
            // 
            // ClientArea
            // 
            this.ClientArea.Controls.Add(this.fraDiscardedDate);
            this.ClientArea.Controls.Add(this.fraDepreciationsPosted);
            this.ClientArea.Controls.Add(this.txtAcct2);
            this.ClientArea.Controls.Add(this.txtAcct4);
            this.ClientArea.Controls.Add(this.txtAcct1);
            this.ClientArea.Controls.Add(this.fcLabel1);
            this.ClientArea.Controls.Add(this.txtAcct3);
            this.ClientArea.Controls.Add(this.cmbLocation);
            this.ClientArea.Controls.Add(this.Label8);
            this.ClientArea.Controls.Add(this.cmbLocationType);
            this.ClientArea.Controls.Add(this.Label6);
            this.ClientArea.Controls.Add(this.Label5);
            this.ClientArea.Controls.Add(this.Label2);
            this.ClientArea.Controls.Add(this.fraDepreciationMethod);
            this.ClientArea.Controls.Add(this.fraWarranty);
            this.ClientArea.Controls.Add(this.lblSerialNumber);
            this.ClientArea.Controls.Add(this.txtImprovements);
            this.ClientArea.Controls.Add(this.txtSalvageValue);
            this.ClientArea.Controls.Add(this.txtTotalDepreciated);
            this.ClientArea.Controls.Add(this.cboClassCode);
            this.ClientArea.Controls.Add(this.cboDiv);
            this.ClientArea.Controls.Add(this.cboDept);
            this.ClientArea.Controls.Add(this.chkExpensedFirstYear);
            this.ClientArea.Controls.Add(this.chkFederalFunds);
            this.ClientArea.Controls.Add(this.mskPurchaseDate);
            this.ClientArea.Controls.Add(this.cboVendor);
            this.ClientArea.Controls.Add(this.txtTagNumber);
            this.ClientArea.Controls.Add(this.txtSerialNumber);
            this.ClientArea.Controls.Add(this.txtModel);
            this.ClientArea.Controls.Add(this.txtMake);
            this.ClientArea.Controls.Add(this.txtYear);
            this.ClientArea.Controls.Add(this.txtDescription);
            this.ClientArea.Controls.Add(this.mskDateLastDepreciation);
            this.ClientArea.Controls.Add(this.txtTotalPosted);
            this.ClientArea.Controls.Add(this.mebOutOfServiceDate);
            this.ClientArea.Controls.Add(this.txtBasisCost);
            this.ClientArea.Controls.Add(this.txtReplacementCost);
            this.ClientArea.Controls.Add(this.Label10);
            this.ClientArea.Controls.Add(this.lblImprovements);
            this.ClientArea.Controls.Add(this.Label7);
            this.ClientArea.Controls.Add(this.Label4);
            this.ClientArea.Controls.Add(this.txtDeptDivDescription);
            this.ClientArea.Controls.Add(this.lblDeptDiv);
            this.ClientArea.Controls.Add(this.lblDash);
            this.ClientArea.Controls.Add(this.lblDateLastDepreciation);
            this.ClientArea.Controls.Add(this.lblTotalPosted);
            this.ClientArea.Controls.Add(this.lblSalvageValue);
            this.ClientArea.Controls.Add(this.lblBasicCost);
            this.ClientArea.Controls.Add(this.lblClassCode);
            this.ClientArea.Controls.Add(this.lblPurchaseDate);
            this.ClientArea.Controls.Add(this.lblVendor);
            this.ClientArea.Controls.Add(this.lblTagNumber);
            this.ClientArea.Controls.Add(this.lblModel);
            this.ClientArea.Controls.Add(this.lblMake);
            this.ClientArea.Controls.Add(this.lblYear);
            this.ClientArea.Controls.Add(this.lblItem);
            this.ClientArea.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.ClientArea.Size = new System.Drawing.Size(1083, 833);
            this.ClientArea.TabIndex = 0;
            this.ClientArea.Controls.SetChildIndex(this.lblItem, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblMake, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblModel, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTagNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblVendor, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblPurchaseDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblClassCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblBasicCost, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSalvageValue, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblTotalPosted, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDateLastDepreciation, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDash, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblDeptDiv, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDeptDivDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label4, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label7, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblImprovements, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label10, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtReplacementCost, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtBasisCost, 0);
            this.ClientArea.Controls.SetChildIndex(this.mebOutOfServiceDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTotalPosted, 0);
            this.ClientArea.Controls.SetChildIndex(this.mskDateLastDepreciation, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtDescription, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtMake, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtModel, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSerialNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTagNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboVendor, 0);
            this.ClientArea.Controls.SetChildIndex(this.mskPurchaseDate, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkFederalFunds, 0);
            this.ClientArea.Controls.SetChildIndex(this.chkExpensedFirstYear, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboDept, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboDiv, 0);
            this.ClientArea.Controls.SetChildIndex(this.cboClassCode, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtTotalDepreciated, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtSalvageValue, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtImprovements, 0);
            this.ClientArea.Controls.SetChildIndex(this.lblSerialNumber, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraWarranty, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraDepreciationMethod, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label2, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label5, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label6, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbLocationType, 0);
            this.ClientArea.Controls.SetChildIndex(this.Label8, 0);
            this.ClientArea.Controls.SetChildIndex(this.cmbLocation, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAcct3, 0);
            this.ClientArea.Controls.SetChildIndex(this.fcLabel1, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAcct1, 0);
            this.ClientArea.Controls.SetChildIndex(this.BottomPanel, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAcct4, 0);
            this.ClientArea.Controls.SetChildIndex(this.txtAcct2, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraDepreciationsPosted, 0);
            this.ClientArea.Controls.SetChildIndex(this.fraDiscardedDate, 0);
            // 
            // TopPanel
            // 
            this.TopPanel.Controls.Add(this.cmdPostNew);
            this.TopPanel.Controls.Add(this.cmdRemoveLast);
            this.TopPanel.Controls.Add(this.cmdNew);
            this.TopPanel.Controls.Add(this.cmdDelete);
            this.TopPanel.Controls.Add(this.Image1);
            this.TopPanel.Size = new System.Drawing.Size(1083, 60);
            this.TopPanel.Controls.SetChildIndex(this.Image1, 0);
            this.TopPanel.Controls.SetChildIndex(this.HeaderText, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdDelete, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdNew, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdRemoveLast, 0);
            this.TopPanel.Controls.SetChildIndex(this.cmdPostNew, 0);
            // 
            // HeaderText
            // 
            this.HeaderText.Size = new System.Drawing.Size(186, 30);
            this.HeaderText.Text = "Add/Edit Assets";
            // 
            // lblDepreciation
            // 
            this.lblDepreciation.AutoSize = true;
            this.lblDepreciation.Location = new System.Drawing.Point(20, 44);
            this.lblDepreciation.Name = "lblDepreciation";
            this.lblDepreciation.Size = new System.Drawing.Size(109, 17);
            this.lblDepreciation.TabIndex = 2;
            this.lblDepreciation.Text = "DEPRECIATION";
            this.lblDepreciation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbDepreciation
            // 
            this.cmbDepreciation.Items.AddRange(new object[] {
            "SL - Straight Line",
            "DB - Declining Balance",
            "UN - Units Based",
            "CD - Condition Based"});
            this.cmbDepreciation.Location = new System.Drawing.Point(153, 30);
            this.cmbDepreciation.Name = "cmbDepreciation";
            this.cmbDepreciation.Size = new System.Drawing.Size(258, 40);
            this.cmbDepreciation.TabIndex = 1;
            this.cmbDepreciation.SelectedIndexChanged += new System.EventHandler(this.optDepreciation_CheckedChanged);
            // 
            // fraWarranty
            // 
            this.fraWarranty.Controls.Add(this.txtWarrantyDescription);
            this.fraWarranty.Controls.Add(this.mskWarrantyDate);
            this.fraWarranty.Controls.Add(this.Label3);
            this.fraWarranty.Controls.Add(this.Label1);
            this.fraWarranty.Location = new System.Drawing.Point(33, 207);
            this.fraWarranty.Name = "fraWarranty";
            this.fraWarranty.Size = new System.Drawing.Size(574, 90);
            this.fraWarranty.TabIndex = 21;
            this.fraWarranty.Text = "Warranty Information";
            // 
            // txtWarrantyDescription
            // 
            this.txtWarrantyDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtWarrantyDescription.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtWarrantyDescription.Location = new System.Drawing.Point(348, 30);
            this.txtWarrantyDescription.Name = "txtWarrantyDescription";
            this.txtWarrantyDescription.Size = new System.Drawing.Size(200, 40);
            this.txtWarrantyDescription.TabIndex = 3;
            this.txtWarrantyDescription.Enter += new System.EventHandler(this.txtWarrantyDescription_Enter);
            this.txtWarrantyDescription.TextChanged += new System.EventHandler(this.txtWarrantyDescription_TextChanged);
            // 
            // mskWarrantyDate
            // 
            this.mskWarrantyDate.Location = new System.Drawing.Point(101, 30);
            this.mskWarrantyDate.Mask = "##/##/####";
            this.mskWarrantyDate.MaxLength = 10;
            this.mskWarrantyDate.Name = "mskWarrantyDate";
            this.mskWarrantyDate.Size = new System.Drawing.Size(115, 22);
            this.mskWarrantyDate.TabIndex = 1;
            this.mskWarrantyDate.Enter += new System.EventHandler(this.mskWarrantyDate_Enter);
            this.mskWarrantyDate.TextChanged += new System.EventHandler(this.mskWarrantyDate_TextChanged);
            this.mskWarrantyDate.Validating += new System.ComponentModel.CancelEventHandler(this.mskWarrantyDate_Validating);
            // 
            // Label3
            // 
            this.Label3.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label3.Location = new System.Drawing.Point(254, 44);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(96, 14);
            this.Label3.TabIndex = 2;
            this.Label3.Text = "DESCRIPTION";
            this.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label1
            // 
            this.Label1.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label1.Location = new System.Drawing.Point(20, 44);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(75, 14);
            this.Label1.TabIndex = 4;
            this.Label1.Text = "EXP DATE";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkDiscarded
            // 
            this.chkDiscarded.Location = new System.Drawing.Point(3, 19);
            this.chkDiscarded.Name = "chkDiscarded";
            this.chkDiscarded.Size = new System.Drawing.Size(91, 24);
            this.chkDiscarded.TabIndex = 1;
            this.chkDiscarded.Text = "Discarded";
            this.chkDiscarded.CheckedChanged += new System.EventHandler(this.chkDiscarded_CheckedChanged);
            // 
            // fraDiscardedDate
            // 
            this.fraDiscardedDate.AppearanceKey = "groupBoxNoBorders";
            this.fraDiscardedDate.Controls.Add(this.txtDiscardedDate);
            this.fraDiscardedDate.Controls.Add(this.chkDiscarded);
            this.fraDiscardedDate.Location = new System.Drawing.Point(624, 235);
            this.fraDiscardedDate.Name = "fraDiscardedDate";
            this.fraDiscardedDate.Size = new System.Drawing.Size(234, 52);
            this.fraDiscardedDate.TabIndex = 23;
            // 
            // txtDiscardedDate
            // 
            this.txtDiscardedDate.AutoSize = false;
            this.txtDiscardedDate.Enabled = false;
            this.txtDiscardedDate.Location = new System.Drawing.Point(113, 8);
            this.txtDiscardedDate.Mask = "##/##/####";
            this.txtDiscardedDate.MaxLength = 10;
            this.txtDiscardedDate.Name = "txtDiscardedDate";
            this.txtDiscardedDate.Size = new System.Drawing.Size(115, 40);
            this.txtDiscardedDate.TabIndex = 2;
            // 
            // txtImprovements
            // 
            this.txtImprovements.BackColor = System.Drawing.Color.FromArgb(192, 192, 192);
            this.txtImprovements.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtImprovements.Location = new System.Drawing.Point(484, 371);
            this.txtImprovements.LockedOriginal = true;
            this.txtImprovements.Name = "txtImprovements";
            this.txtImprovements.ReadOnly = true;
            this.txtImprovements.Size = new System.Drawing.Size(123, 40);
            this.txtImprovements.TabIndex = 34;
            this.txtImprovements.TabStop = false;
            this.txtImprovements.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // txtSalvageValue
            // 
            this.txtSalvageValue.Location = new System.Drawing.Point(160, 371);
            this.txtSalvageValue.MaxLength = 13;
            this.txtSalvageValue.Name = "txtSalvageValue";
            this.txtSalvageValue.Size = new System.Drawing.Size(122, 22);
            this.txtSalvageValue.TabIndex = 32;
            this.txtSalvageValue.Enter += new System.EventHandler(this.txtSalvageValue_Enter);
            this.txtSalvageValue.TextChanged += new System.EventHandler(this.txtSalvageValue_Change);
            this.txtSalvageValue.Validating += new System.ComponentModel.CancelEventHandler(this.txtSalvageValue_Validate);
            // 
            // txtTotalDepreciated
            // 
            this.txtTotalDepreciated.BackColor = System.Drawing.Color.FromArgb(192, 192, 192);
            this.txtTotalDepreciated.CharacterCasing = Wisej.Web.CharacterCasing.Upper;
            this.txtTotalDepreciated.Location = new System.Drawing.Point(484, 421);
            this.txtTotalDepreciated.LockedOriginal = true;
            this.txtTotalDepreciated.Name = "txtTotalDepreciated";
            this.txtTotalDepreciated.ReadOnly = true;
            this.txtTotalDepreciated.Size = new System.Drawing.Size(123, 40);
            this.txtTotalDepreciated.TabIndex = 46;
            this.txtTotalDepreciated.TabStop = false;
            this.txtTotalDepreciated.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            this.txtTotalDepreciated.TextChanged += new System.EventHandler(this.txtTotalDepreciated_TextChanged);
            // 
            // cboClassCode
            // 
            this.cboClassCode.BackColor = System.Drawing.SystemColors.Window;
            this.cboClassCode.Location = new System.Drawing.Point(147, 471);
            this.cboClassCode.Name = "cboClassCode";
            this.cboClassCode.Size = new System.Drawing.Size(460, 40);
            this.cboClassCode.TabIndex = 48;
            this.cboClassCode.SelectedIndexChanged += new System.EventHandler(this.cboClassCode_SelectedIndexChanged);
            this.cboClassCode.DropDown += new System.EventHandler(this.cboClassCode_DropDown);
            this.cboClassCode.TextChanged += new System.EventHandler(this.cboClassCode_TextChanged);
            // 
            // cboDiv
            // 
            this.cboDiv.BackColor = System.Drawing.SystemColors.Window;
            this.cboDiv.Location = new System.Drawing.Point(838, 292);
            this.cboDiv.Name = "cboDiv";
            this.cboDiv.Size = new System.Drawing.Size(80, 40);
            this.cboDiv.Sorted = true;
            this.cboDiv.TabIndex = 30;
            this.cboDiv.SelectedIndexChanged += new System.EventHandler(this.cboDiv_SelectedIndexChanged);
            // 
            // cboDept
            // 
            this.cboDept.BackColor = System.Drawing.SystemColors.Window;
            this.cboDept.Location = new System.Drawing.Point(679, 292);
            this.cboDept.Name = "cboDept";
            this.cboDept.Size = new System.Drawing.Size(98, 40);
            this.cboDept.Sorted = true;
            this.cboDept.TabIndex = 28;
            this.cboDept.SelectedIndexChanged += new System.EventHandler(this.cboDept_SelectedIndexChanged);
            // 
            // cmbLocation
            // 
            this.cmbLocation.AutoSize = false;
            this.cmbLocation.DisplayMember = "Description";
            this.cmbLocation.DropDownStyle = Wisej.Web.ComboBoxStyle.DropDownList;
            this.cmbLocation.Location = new System.Drawing.Point(327, 162);
            this.cmbLocation.Name = "cmbLocation";
            this.cmbLocation.Size = new System.Drawing.Size(280, 40);
            this.cmbLocation.TabIndex = 18;
            this.cmbLocation.ValueMember = "ID";
            // 
            // cmbLocationType
            // 
            this.cmbLocationType.BackColor = System.Drawing.SystemColors.Window;
            this.cmbLocationType.Location = new System.Drawing.Point(134, 162);
            this.cmbLocationType.Name = "cmbLocationType";
            this.cmbLocationType.Size = new System.Drawing.Size(187, 40);
            this.cmbLocationType.Sorted = true;
            this.cmbLocationType.TabIndex = 17;
            // 
            // fraDepreciationMethod
            // 
            this.fraDepreciationMethod.Controls.Add(this.cboCondition);
            this.fraDepreciationMethod.Controls.Add(this.cmbDepreciation);
            this.fraDepreciationMethod.Controls.Add(this.lblDepreciation);
            this.fraDepreciationMethod.Controls.Add(this.txtTotalUsed);
            this.fraDepreciationMethod.Controls.Add(this.cboRate);
            this.fraDepreciationMethod.Controls.Add(this.txtUN);
            this.fraDepreciationMethod.Controls.Add(this.txtLife);
            this.fraDepreciationMethod.Controls.Add(this.lblCondition);
            this.fraDepreciationMethod.Controls.Add(this.lblUM);
            this.fraDepreciationMethod.Controls.Add(this.lblRate);
            this.fraDepreciationMethod.Controls.Add(this.lblUnits_1);
            this.fraDepreciationMethod.Controls.Add(this.lblUnits_0);
            this.fraDepreciationMethod.Controls.Add(this.lblTotalUsed);
            this.fraDepreciationMethod.Controls.Add(this.lblLife);
            this.fraDepreciationMethod.Location = new System.Drawing.Point(33, 523);
            this.fraDepreciationMethod.Name = "fraDepreciationMethod";
            this.fraDepreciationMethod.Size = new System.Drawing.Size(574, 187);
            this.fraDepreciationMethod.TabIndex = 49;
            this.fraDepreciationMethod.Text = "Depreciation Method";
            // 
            // cboCondition
            // 
            this.cboCondition.BackColor = System.Drawing.SystemColors.Window;
            this.cboCondition.Items.AddRange(new object[] {
            "Excellent",
            "Good",
            "Fair",
            "Poor"});
            this.cboCondition.Location = new System.Drawing.Point(462, 131);
            this.cboCondition.Name = "cboCondition";
            this.cboCondition.Size = new System.Drawing.Size(98, 40);
            this.cboCondition.TabIndex = 11;
            // 
            // txtTotalUsed
            // 
            this.txtTotalUsed.Location = new System.Drawing.Point(462, 80);
            this.txtTotalUsed.MaxLength = 10;
            this.txtTotalUsed.Name = "txtTotalUsed";
            this.txtTotalUsed.Size = new System.Drawing.Size(98, 22);
            this.txtTotalUsed.TabIndex = 7;
            this.txtTotalUsed.Enter += new System.EventHandler(this.txtTotalUsed_Enter);
            this.txtTotalUsed.TextChanged += new System.EventHandler(this.txtTotalUsed_Change);
            // 
            // cboRate
            // 
            this.cboRate.BackColor = System.Drawing.SystemColors.Window;
            this.cboRate.Items.AddRange(new object[] {
            "1.5",
            "2"});
            this.cboRate.Location = new System.Drawing.Point(153, 80);
            this.cboRate.Name = "cboRate";
            this.cboRate.Size = new System.Drawing.Size(72, 40);
            this.cboRate.TabIndex = 4;
            // 
            // txtUN
            // 
            this.txtUN.BackColor = System.Drawing.SystemColors.Window;
            this.txtUN.Location = new System.Drawing.Point(153, 80);
            this.txtUN.MaxLength = 13;
            this.txtUN.Name = "txtUN";
            this.txtUN.Size = new System.Drawing.Size(96, 40);
            this.txtUN.TabIndex = 5;
            this.txtUN.Enter += new System.EventHandler(this.txtUN_Enter);
            this.txtUN.TextChanged += new System.EventHandler(this.txtUN_TextChanged);
            // 
            // txtLife
            // 
            this.txtLife.Location = new System.Drawing.Point(153, 131);
            this.txtLife.MaxLength = 3;
            this.txtLife.Name = "txtLife";
            this.txtLife.Size = new System.Drawing.Size(73, 22);
            this.txtLife.TabIndex = 9;
            this.txtLife.Leave += new System.EventHandler(this.txtLife_Leave);
            this.txtLife.TextChanged += new System.EventHandler(this.txtLife_Change);
            // 
            // lblCondition
            // 
            this.lblCondition.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblCondition.Location = new System.Drawing.Point(362, 145);
            this.lblCondition.Name = "lblCondition";
            this.lblCondition.Size = new System.Drawing.Size(94, 14);
            this.lblCondition.TabIndex = 10;
            this.lblCondition.Text = "CONDITION";
            this.lblCondition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUM
            // 
            this.lblUM.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblUM.Location = new System.Drawing.Point(20, 94);
            this.lblUM.Name = "lblUM";
            this.lblUM.Size = new System.Drawing.Size(123, 14);
            this.lblUM.TabIndex = 3;
            this.lblUM.Text = "UNIT OF MEASURE";
            this.lblUM.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblRate
            // 
            this.lblRate.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblRate.Location = new System.Drawing.Point(20, 94);
            this.lblRate.Name = "lblRate";
            this.lblRate.Size = new System.Drawing.Size(35, 14);
            this.lblRate.TabIndex = 2;
            this.lblRate.Text = "RATE";
            this.lblRate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnits_1
            // 
            this.lblUnits_1.Location = new System.Drawing.Point(500, 244);
            this.lblUnits_1.Name = "lblUnits_1";
            this.lblUnits_1.Size = new System.Drawing.Size(74, 14);
            this.lblUnits_1.TabIndex = 13;
            // 
            // lblUnits_0
            // 
            this.lblUnits_0.Location = new System.Drawing.Point(243, 145);
            this.lblUnits_0.Name = "lblUnits_0";
            this.lblUnits_0.Size = new System.Drawing.Size(74, 14);
            this.lblUnits_0.TabIndex = 12;
            // 
            // lblTotalUsed
            // 
            this.lblTotalUsed.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblTotalUsed.Location = new System.Drawing.Point(362, 94);
            this.lblTotalUsed.Name = "lblTotalUsed";
            this.lblTotalUsed.Size = new System.Drawing.Size(37, 14);
            this.lblTotalUsed.TabIndex = 6;
            this.lblTotalUsed.Text = "USED";
            this.lblTotalUsed.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLife
            // 
            this.lblLife.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblLife.Location = new System.Drawing.Point(20, 145);
            this.lblLife.Name = "lblLife";
            this.lblLife.Size = new System.Drawing.Size(58, 14);
            this.lblLife.TabIndex = 8;
            this.lblLife.Text = "LIFE";
            this.lblLife.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // fraDepreciationsPosted
            // 
            this.fraDepreciationsPosted.Controls.Add(this.txtValue);
            this.fraDepreciationsPosted.Controls.Add(this.vsDepreciationsPosted);
            this.fraDepreciationsPosted.Controls.Add(this.Label9);
            this.fraDepreciationsPosted.Location = new System.Drawing.Point(630, 576);
            this.fraDepreciationsPosted.Name = "fraDepreciationsPosted";
            this.fraDepreciationsPosted.Size = new System.Drawing.Size(428, 207);
            this.fraDepreciationsPosted.TabIndex = 50;
            this.fraDepreciationsPosted.Text = "Depreciation Posted";
            // 
            // txtValue
            // 
            this.txtValue.BackColor = System.Drawing.Color.FromArgb(192, 192, 192);
            this.txtValue.Location = new System.Drawing.Point(153, 30);
            this.txtValue.LockedOriginal = true;
            this.txtValue.Name = "txtValue";
            this.txtValue.ReadOnly = true;
            this.txtValue.Size = new System.Drawing.Size(142, 40);
            this.txtValue.TabIndex = 1;
            this.txtValue.TabStop = false;
            this.txtValue.TextAlign = Wisej.Web.HorizontalAlignment.Right;
            // 
            // vsDepreciationsPosted
            // 
            this.vsDepreciationsPosted.Cols = 10;
            this.vsDepreciationsPosted.ExtendLastCol = true;
            this.vsDepreciationsPosted.Location = new System.Drawing.Point(10, 80);
            this.vsDepreciationsPosted.Name = "vsDepreciationsPosted";
            this.vsDepreciationsPosted.Rows = 50;
            this.vsDepreciationsPosted.Size = new System.Drawing.Size(412, 120);
            this.vsDepreciationsPosted.StandardTab = false;
            this.vsDepreciationsPosted.TabBehavior = fecherFoundation.FCGrid.TabBehaviorSettings.flexTabCells;
            this.vsDepreciationsPosted.TabIndex = 2;
            this.vsDepreciationsPosted.TabStop = false;
            this.vsDepreciationsPosted.AfterEdit += new Wisej.Web.DataGridViewCellEventHandler(this.vsDepreciationsPosted_AfterEdit);
            this.vsDepreciationsPosted.CellBeginEdit += new Wisej.Web.DataGridViewCellCancelEventHandler(this.vsDepreciationsPosted_BeforeEdit);
            this.vsDepreciationsPosted.Click += new System.EventHandler(this.vsDepreciationsPosted_ClickEvent);
            // 
            // Label9
            // 
            this.Label9.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label9.Location = new System.Drawing.Point(10, 44);
            this.Label9.Name = "Label9";
            this.Label9.Size = new System.Drawing.Size(127, 20);
            this.Label9.TabIndex = 3;
            this.Label9.Text = "CURRENT VALUE";
            this.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAcct2
            // 
            this.txtAcct2.Cols = 1;
            this.txtAcct2.ColumnHeadersVisible = false;
            this.txtAcct2.ExtendLastCol = true;
            this.txtAcct2.FixedCols = 0;
            this.txtAcct2.FixedRows = 0;
            this.txtAcct2.Location = new System.Drawing.Point(753, 371);
            this.txtAcct2.Name = "txtAcct2";
            this.txtAcct2.RowHeadersVisible = false;
            this.txtAcct2.Rows = 1;
            this.txtAcct2.Size = new System.Drawing.Size(242, 42);
            this.txtAcct2.TabIndex = 36;
            this.txtAcct2.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.txtAcct2_ChangeEdit);
            this.txtAcct2.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.txtAcct2_MouseMoveEvent);
            // 
            // txtAcct4
            // 
            this.txtAcct4.Cols = 1;
            this.txtAcct4.ColumnHeadersVisible = false;
            this.txtAcct4.ExtendLastCol = true;
            this.txtAcct4.FixedCols = 0;
            this.txtAcct4.FixedRows = 0;
            this.txtAcct4.Location = new System.Drawing.Point(753, 421);
            this.txtAcct4.Name = "txtAcct4";
            this.txtAcct4.RowHeadersVisible = false;
            this.txtAcct4.Rows = 1;
            this.txtAcct4.Size = new System.Drawing.Size(242, 42);
            this.txtAcct4.TabIndex = 38;
            this.txtAcct4.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.txtAcct4_ChangeEdit);
            this.txtAcct4.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.txtAcct4_MouseMoveEvent);
            // 
            // txtAcct1
            // 
            this.txtAcct1.Cols = 1;
            this.txtAcct1.ColumnHeadersVisible = false;
            this.txtAcct1.ExtendLastCol = true;
            this.txtAcct1.FixedCols = 0;
            this.txtAcct1.FixedRows = 0;
            this.txtAcct1.Location = new System.Drawing.Point(753, 471);
            this.txtAcct1.Name = "txtAcct1";
            this.txtAcct1.RowHeadersVisible = false;
            this.txtAcct1.Rows = 1;
            this.txtAcct1.Size = new System.Drawing.Size(242, 42);
            this.txtAcct1.TabIndex = 40;
            this.txtAcct1.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.txtAcct1_ChangeEdit);
            this.txtAcct1.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.txtAcct1_MouseMoveEvent);
            // 
            // txtAcct3
            // 
            this.txtAcct3.Cols = 1;
            this.txtAcct3.ColumnHeadersVisible = false;
            this.txtAcct3.ExtendLastCol = true;
            this.txtAcct3.FixedCols = 0;
            this.txtAcct3.FixedRows = 0;
            this.txtAcct3.Location = new System.Drawing.Point(753, 521);
            this.txtAcct3.Name = "txtAcct3";
            this.txtAcct3.RowHeadersVisible = false;
            this.txtAcct3.Rows = 1;
            this.txtAcct3.Size = new System.Drawing.Size(242, 42);
            this.txtAcct3.TabIndex = 42;
            this.txtAcct3.CellValueChanged += new Wisej.Web.DataGridViewCellEventHandler(this.txtAcct3_ChangeEdit);
            this.txtAcct3.CellMouseMove += new Wisej.Web.DataGridViewCellMouseEventHandler(this.txtAcct3_MouseMoveEvent);
            // 
            // Label8
            // 
            this.Label8.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label8.Location = new System.Drawing.Point(630, 435);
            this.Label8.Name = "Label8";
            this.Label8.Size = new System.Drawing.Size(97, 14);
            this.Label8.TabIndex = 37;
            this.Label8.Text = "ASSET";
            // 
            // Label6
            // 
            this.Label6.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label6.Location = new System.Drawing.Point(630, 485);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(97, 14);
            this.Label6.TabIndex = 39;
            this.Label6.Text = "EXPENSE";
            this.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label5
            // 
            this.Label5.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label5.Location = new System.Drawing.Point(630, 385);
            this.Label5.Name = "Label5";
            this.Label5.Size = new System.Drawing.Size(97, 14);
            this.Label5.TabIndex = 35;
            this.Label5.Text = "PURCHASE";
            this.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label2
            // 
            this.Label2.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label2.Location = new System.Drawing.Point(630, 535);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(115, 20);
            this.Label2.TabIndex = 41;
            this.Label2.Text = "DEPRECIATION";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkExpensedFirstYear
            // 
            this.chkExpensedFirstYear.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkExpensedFirstYear.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.chkExpensedFirstYear.Location = new System.Drawing.Point(1095, 336);
            this.chkExpensedFirstYear.Name = "chkExpensedFirstYear";
            this.chkExpensedFirstYear.Size = new System.Drawing.Size(164, 24);
            this.chkExpensedFirstYear.TabIndex = 45;
            this.chkExpensedFirstYear.Text = "    Expensed first year";
            this.chkExpensedFirstYear.Visible = false;
            this.chkExpensedFirstYear.CheckedChanged += new System.EventHandler(this.chkExpensedFirstYear_CheckedChanged);
            // 
            // chkFederalFunds
            // 
            this.chkFederalFunds.Location = new System.Drawing.Point(627, 207);
            this.chkFederalFunds.Name = "chkFederalFunds";
            this.chkFederalFunds.Size = new System.Drawing.Size(232, 24);
            this.chkFederalFunds.TabIndex = 22;
            this.chkFederalFunds.Text = "Federal Funds used to purchase";
            this.chkFederalFunds.CheckedChanged += new System.EventHandler(this.chkFederalFunds_CheckedChanged);
            // 
            // mskPurchaseDate
            // 
            this.mskPurchaseDate.Location = new System.Drawing.Point(753, 114);
            this.mskPurchaseDate.Mask = "##/##/####";
            this.mskPurchaseDate.MaxLength = 10;
            this.mskPurchaseDate.Name = "mskPurchaseDate";
            this.mskPurchaseDate.Size = new System.Drawing.Size(115, 22);
            this.mskPurchaseDate.TabIndex = 15;
            this.mskPurchaseDate.Enter += new System.EventHandler(this.mskPurchaseDate_Enter);
            this.mskPurchaseDate.Leave += new System.EventHandler(this.mskPurchaseDate_Leave);
            this.mskPurchaseDate.TextChanged += new System.EventHandler(this.mskPurchaseDate_TextChanged);
            this.mskPurchaseDate.Validating += new System.ComponentModel.CancelEventHandler(this.mskPurchaseDate_Validating);
            // 
            // cboVendor
            // 
            this.cboVendor.BackColor = System.Drawing.SystemColors.Window;
            this.cboVendor.Location = new System.Drawing.Point(134, 114);
            this.cboVendor.Name = "cboVendor";
            this.cboVendor.Size = new System.Drawing.Size(473, 40);
            this.cboVendor.TabIndex = 13;
            this.cboVendor.SelectedIndexChanged += new System.EventHandler(this.cboVendor_SelectedIndexChanged);
            // 
            // txtTagNumber
            // 
            this.txtTagNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtTagNumber.Location = new System.Drawing.Point(753, 63);
            this.txtTagNumber.Name = "txtTagNumber";
            this.txtTagNumber.Size = new System.Drawing.Size(242, 40);
            this.txtTagNumber.TabIndex = 11;
            this.txtTagNumber.Enter += new System.EventHandler(this.txtTagNumber_Enter);
            this.txtTagNumber.TextChanged += new System.EventHandler(this.txtTagNumber_TextChanged);
            // 
            // txtSerialNumber
            // 
            this.txtSerialNumber.BackColor = System.Drawing.SystemColors.Window;
            this.txtSerialNumber.Location = new System.Drawing.Point(753, 13);
            this.txtSerialNumber.MaxLength = 25;
            this.txtSerialNumber.Name = "txtSerialNumber";
            this.txtSerialNumber.Size = new System.Drawing.Size(242, 40);
            this.txtSerialNumber.TabIndex = 3;
            this.txtSerialNumber.Enter += new System.EventHandler(this.txtSerialNumber_Enter);
            this.txtSerialNumber.TextChanged += new System.EventHandler(this.txtSerialNumber_TextChanged);
            // 
            // txtModel
            // 
            this.txtModel.BackColor = System.Drawing.SystemColors.Window;
            this.txtModel.Location = new System.Drawing.Point(474, 63);
            this.txtModel.Name = "txtModel";
            this.txtModel.Size = new System.Drawing.Size(135, 40);
            this.txtModel.TabIndex = 9;
            this.txtModel.Enter += new System.EventHandler(this.txtModel_Enter);
            this.txtModel.TextChanged += new System.EventHandler(this.txtModel_TextChanged);
            // 
            // txtMake
            // 
            this.txtMake.BackColor = System.Drawing.SystemColors.Window;
            this.txtMake.Location = new System.Drawing.Point(283, 63);
            this.txtMake.Name = "txtMake";
            this.txtMake.Size = new System.Drawing.Size(100, 40);
            this.txtMake.TabIndex = 7;
            this.txtMake.Enter += new System.EventHandler(this.txtMake_Enter);
            this.txtMake.TextChanged += new System.EventHandler(this.txtMake_TextChanged);
            // 
            // txtYear
            // 
            this.txtYear.BackColor = System.Drawing.SystemColors.Window;
            this.txtYear.Location = new System.Drawing.Point(134, 63);
            this.txtYear.MaxLength = 4;
            this.txtYear.Name = "txtYear";
            this.txtYear.Size = new System.Drawing.Size(69, 40);
            this.txtYear.TabIndex = 5;
            this.txtYear.Enter += new System.EventHandler(this.txtYear_Enter);
            this.txtYear.TextChanged += new System.EventHandler(this.txtYear_TextChanged);
            this.txtYear.KeyPress += new Wisej.Web.KeyPressEventHandler(this.txtYear_KeyPress);
            // 
            // txtDescription
            // 
            this.txtDescription.BackColor = System.Drawing.SystemColors.Window;
            this.txtDescription.Location = new System.Drawing.Point(134, 13);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(475, 40);
            this.txtDescription.TabIndex = 1;
            this.txtDescription.Enter += new System.EventHandler(this.txtDescription_Enter);
            this.txtDescription.TextChanged += new System.EventHandler(this.txtDescription_TextChanged);
            // 
            // mskDateLastDepreciation
            // 
            this.mskDateLastDepreciation.Location = new System.Drawing.Point(484, 321);
            this.mskDateLastDepreciation.Mask = "##/##/####";
            this.mskDateLastDepreciation.MaxLength = 10;
            this.mskDateLastDepreciation.Name = "mskDateLastDepreciation";
            this.mskDateLastDepreciation.Size = new System.Drawing.Size(123, 22);
            this.mskDateLastDepreciation.TabIndex = 26;
            this.mskDateLastDepreciation.Enter += new System.EventHandler(this.mskDateLastDepreciation_Enter);
            this.mskDateLastDepreciation.Validating += new System.ComponentModel.CancelEventHandler(this.mskDateLastDepreciation_Validating);
            // 
            // txtTotalPosted
            // 
            this.txtTotalPosted.BackColor = System.Drawing.SystemColors.Window;
            this.txtTotalPosted.Enabled = false;
            this.txtTotalPosted.Location = new System.Drawing.Point(221, 521);
            this.txtTotalPosted.Name = "txtTotalPosted";
            this.txtTotalPosted.Size = new System.Drawing.Size(119, 40);
            this.txtTotalPosted.TabIndex = 42;
            this.txtTotalPosted.Visible = false;
            this.txtTotalPosted.Enter += new System.EventHandler(this.txtTotalPosted_Enter);
            this.txtTotalPosted.TextChanged += new System.EventHandler(this.txtTotalPosted_TextChanged);
            // 
            // mebOutOfServiceDate
            // 
            this.mebOutOfServiceDate.Location = new System.Drawing.Point(752, 162);
            this.mebOutOfServiceDate.Mask = "##/##/####";
            this.mebOutOfServiceDate.MaxLength = 10;
            this.mebOutOfServiceDate.Name = "mebOutOfServiceDate";
            this.mebOutOfServiceDate.Size = new System.Drawing.Size(115, 22);
            this.mebOutOfServiceDate.TabIndex = 20;
            this.mebOutOfServiceDate.Enter += new System.EventHandler(this.mebOutOfServiceDate_Enter);
            this.mebOutOfServiceDate.Validating += new System.ComponentModel.CancelEventHandler(this.mebOutOfServiceDate_Validating);
            // 
            // txtBasisCost
            // 
            this.txtBasisCost.Location = new System.Drawing.Point(160, 321);
            this.txtBasisCost.MaxLength = 13;
            this.txtBasisCost.Name = "txtBasisCost";
            this.txtBasisCost.Size = new System.Drawing.Size(122, 22);
            this.txtBasisCost.TabIndex = 24;
            this.txtBasisCost.Enter += new System.EventHandler(this.txtBasisCost_Enter);
            this.txtBasisCost.TextChanged += new System.EventHandler(this.txtBasisCost_Change);
            this.txtBasisCost.Validating += new System.ComponentModel.CancelEventHandler(this.txtBasisCost_Validate);
            // 
            // txtReplacementCost
            // 
            this.txtReplacementCost.Location = new System.Drawing.Point(160, 421);
            this.txtReplacementCost.MaxLength = 13;
            this.txtReplacementCost.Name = "txtReplacementCost";
            this.txtReplacementCost.Size = new System.Drawing.Size(121, 22);
            this.txtReplacementCost.TabIndex = 44;
            this.txtReplacementCost.Enter += new System.EventHandler(this.txtReplacementCost_Enter);
            this.txtReplacementCost.TextChanged += new System.EventHandler(this.txtReplacementCost_Change);
            this.txtReplacementCost.Validating += new System.ComponentModel.CancelEventHandler(this.txtReplacementCost_Validate);
            // 
            // Image1
            // 
            this.Image1.BorderStyle = Wisej.Web.BorderStyle.None;
            this.Image1.ImageSource = "imgnote?color=#707884";
            this.Image1.Location = new System.Drawing.Point(234, 24);
            this.Image1.Name = "Image1";
            this.Image1.Size = new System.Drawing.Size(31, 33);
            this.Image1.SizeMode = Wisej.Web.PictureBoxSizeMode.StretchImage;
            this.Image1.Visible = false;
            this.Image1.Click += new System.EventHandler(this.Image1_Click);
            // 
            // Label10
            // 
            this.Label10.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label10.Location = new System.Drawing.Point(33, 435);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(90, 18);
            this.Label10.TabIndex = 43;
            this.Label10.Text = "REPLACE COST";
            this.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblImprovements
            // 
            this.lblImprovements.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblImprovements.Location = new System.Drawing.Point(325, 385);
            this.lblImprovements.Name = "lblImprovements";
            this.lblImprovements.Size = new System.Drawing.Size(127, 18);
            this.lblImprovements.TabIndex = 33;
            this.lblImprovements.Text = "IMPROVEMENTS";
            this.lblImprovements.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label7
            // 
            this.Label7.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label7.Location = new System.Drawing.Point(325, 435);
            this.Label7.Name = "Label7";
            this.Label7.Size = new System.Drawing.Size(140, 16);
            this.Label7.TabIndex = 45;
            this.Label7.Text = "TOTAL DEPRECIATED";
            this.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Label4
            // 
            this.Label4.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.Label4.Location = new System.Drawing.Point(627, 176);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(129, 14);
            this.Label4.TabIndex = 19;
            this.Label4.Text = "OUT OF SERVICE";
            this.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDeptDivDescription
            // 
            this.txtDeptDivDescription.Location = new System.Drawing.Point(630, 337);
            this.txtDeptDivDescription.Name = "txtDeptDivDescription";
            this.txtDeptDivDescription.Size = new System.Drawing.Size(288, 30);
            this.txtDeptDivDescription.TabIndex = 46;
            this.txtDeptDivDescription.TextChanged += new System.EventHandler(this.txtDeptDivDescription_TextChanged);
            // 
            // lblDeptDiv
            // 
            this.lblDeptDiv.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblDeptDiv.Location = new System.Drawing.Point(630, 305);
            this.lblDeptDiv.Name = "lblDeptDiv";
            this.lblDeptDiv.Size = new System.Drawing.Size(35, 16);
            this.lblDeptDiv.TabIndex = 27;
            this.lblDeptDiv.Text = "DEPT";
            this.lblDeptDiv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDash
            // 
            this.lblDash.Location = new System.Drawing.Point(806, 305);
            this.lblDash.Name = "lblDash";
            this.lblDash.Size = new System.Drawing.Size(26, 16);
            this.lblDash.TabIndex = 29;
            this.lblDash.Text = "DIV";
            this.lblDash.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDateLastDepreciation
            // 
            this.lblDateLastDepreciation.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblDateLastDepreciation.Location = new System.Drawing.Point(325, 335);
            this.lblDateLastDepreciation.Name = "lblDateLastDepreciation";
            this.lblDateLastDepreciation.Size = new System.Drawing.Size(143, 18);
            this.lblDateLastDepreciation.TabIndex = 25;
            this.lblDateLastDepreciation.Text = "LAST DEP. DATE";
            this.lblDateLastDepreciation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTotalPosted
            // 
            this.lblTotalPosted.Enabled = false;
            this.lblTotalPosted.Location = new System.Drawing.Point(33, 535);
            this.lblTotalPosted.Name = "lblTotalPosted";
            this.lblTotalPosted.Size = new System.Drawing.Size(170, 18);
            this.lblTotalPosted.TabIndex = 41;
            this.lblTotalPosted.Text = "TOTAL USED (UNITS ONLY)";
            this.lblTotalPosted.Visible = false;
            // 
            // lblSalvageValue
            // 
            this.lblSalvageValue.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblSalvageValue.Location = new System.Drawing.Point(33, 385);
            this.lblSalvageValue.Name = "lblSalvageValue";
            this.lblSalvageValue.Size = new System.Drawing.Size(112, 18);
            this.lblSalvageValue.TabIndex = 31;
            this.lblSalvageValue.Text = "SALVAGE VALUE";
            this.lblSalvageValue.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblBasicCost
            // 
            this.lblBasicCost.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblBasicCost.Location = new System.Drawing.Point(33, 335);
            this.lblBasicCost.Name = "lblBasicCost";
            this.lblBasicCost.Size = new System.Drawing.Size(50, 18);
            this.lblBasicCost.TabIndex = 23;
            this.lblBasicCost.Text = "COST";
            this.lblBasicCost.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblClassCode
            // 
            this.lblClassCode.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblClassCode.Location = new System.Drawing.Point(33, 485);
            this.lblClassCode.Name = "lblClassCode";
            this.lblClassCode.Size = new System.Drawing.Size(78, 16);
            this.lblClassCode.TabIndex = 47;
            this.lblClassCode.Text = "CLASS CODE";
            this.lblClassCode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPurchaseDate
            // 
            this.lblPurchaseDate.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblPurchaseDate.Location = new System.Drawing.Point(627, 128);
            this.lblPurchaseDate.Name = "lblPurchaseDate";
            this.lblPurchaseDate.Size = new System.Drawing.Size(120, 14);
            this.lblPurchaseDate.TabIndex = 14;
            this.lblPurchaseDate.Text = "PURCHASE DATE";
            this.lblPurchaseDate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVendor
            // 
            this.lblVendor.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblVendor.Location = new System.Drawing.Point(33, 128);
            this.lblVendor.Name = "lblVendor";
            this.lblVendor.Size = new System.Drawing.Size(57, 14);
            this.lblVendor.TabIndex = 12;
            this.lblVendor.Text = "VENDOR";
            this.lblVendor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTagNumber
            // 
            this.lblTagNumber.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblTagNumber.Location = new System.Drawing.Point(627, 77);
            this.lblTagNumber.Name = "lblTagNumber";
            this.lblTagNumber.Size = new System.Drawing.Size(94, 14);
            this.lblTagNumber.TabIndex = 10;
            this.lblTagNumber.Text = "TAG NUMBER";
            this.lblTagNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblSerialNumber
            // 
            this.lblSerialNumber.AutoSize = true;
            this.lblSerialNumber.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblSerialNumber.Location = new System.Drawing.Point(627, 27);
            this.lblSerialNumber.Name = "lblSerialNumber";
            this.lblSerialNumber.Size = new System.Drawing.Size(119, 17);
            this.lblSerialNumber.TabIndex = 2;
            this.lblSerialNumber.Text = "SERIAL NUMBER";
            this.lblSerialNumber.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblModel
            // 
            this.lblModel.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblModel.Location = new System.Drawing.Point(405, 77);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(55, 14);
            this.lblModel.TabIndex = 8;
            this.lblModel.Text = "MODEL";
            this.lblModel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblMake
            // 
            this.lblMake.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblMake.Location = new System.Drawing.Point(221, 77);
            this.lblMake.Name = "lblMake";
            this.lblMake.Size = new System.Drawing.Size(56, 14);
            this.lblMake.TabIndex = 6;
            this.lblMake.Text = "MAKE";
            this.lblMake.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblYear
            // 
            this.lblYear.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblYear.Location = new System.Drawing.Point(33, 77);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(43, 14);
            this.lblYear.TabIndex = 4;
            this.lblYear.Text = "YEAR";
            this.lblYear.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblItem
            // 
            this.lblItem.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.lblItem.Location = new System.Drawing.Point(33, 27);
            this.lblItem.Name = "lblItem";
            this.lblItem.Size = new System.Drawing.Size(95, 14);
            this.lblItem.TabIndex = 1001;
            this.lblItem.Text = "DESCRIPTION";
            this.lblItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // MainMenu1
            // 
            this.MainMenu1.MenuItems.AddRange(new Wisej.Web.MenuItem[] {
            this.mnuFileComments,
            this.mnuFileSeperator2,
            this.mnuSearch,
            this.mnuLocations,
            this.mnuVendors,
            this.mnuDeptDiv,
            this.mnuClass,
            this.mnuLocationCaptions,
            this.mnuFileSeperator10,
            this.mnuFileInitialDepreciation});
            this.MainMenu1.Name = null;
            // 
            // mnuFileComments
            // 
            this.mnuFileComments.Enabled = false;
            this.mnuFileComments.Index = 0;
            this.mnuFileComments.Name = "mnuFileComments";
            this.mnuFileComments.Shortcut = Wisej.Web.Shortcut.F5;
            this.mnuFileComments.Text = "Comments";
            this.mnuFileComments.Click += new System.EventHandler(this.mnuFileComments_Click);
            // 
            // mnuFileSeperator2
            // 
            this.mnuFileSeperator2.Index = 1;
            this.mnuFileSeperator2.Name = "mnuFileSeperator2";
            this.mnuFileSeperator2.Text = "-";
            // 
            // mnuSearch
            // 
            this.mnuSearch.Index = 2;
            this.mnuSearch.Name = "mnuSearch";
            this.mnuSearch.Text = "Search for Item";
            this.mnuSearch.Click += new System.EventHandler(this.mnuSearch_Click);
            // 
            // mnuLocations
            // 
            this.mnuLocations.Index = 3;
            this.mnuLocations.Name = "mnuLocations";
            this.mnuLocations.Text = "Add / Edit Locations";
            this.mnuLocations.Click += new System.EventHandler(this.mnuLocations_Click);
            // 
            // mnuVendors
            // 
            this.mnuVendors.Index = 4;
            this.mnuVendors.Name = "mnuVendors";
            this.mnuVendors.Text = "Add / Edit Vendors";
            this.mnuVendors.Click += new System.EventHandler(this.mnuVendors_Click);
            // 
            // mnuDeptDiv
            // 
            this.mnuDeptDiv.Index = 5;
            this.mnuDeptDiv.Name = "mnuDeptDiv";
            this.mnuDeptDiv.Text = "Add / Edit Dept / Div";
            this.mnuDeptDiv.Click += new System.EventHandler(this.mnuDeptDiv_Click);
            // 
            // mnuClass
            // 
            this.mnuClass.Index = 6;
            this.mnuClass.Name = "mnuClass";
            this.mnuClass.Text = "Add / Edit Class Codes";
            this.mnuClass.Click += new System.EventHandler(this.mnuClass_Click);
            // 
            // mnuLocationCaptions
            // 
            this.mnuLocationCaptions.Index = 7;
            this.mnuLocationCaptions.Name = "mnuLocationCaptions";
            this.mnuLocationCaptions.Text = "Edit Location Captions";
            this.mnuLocationCaptions.Click += new System.EventHandler(this.mnuLocationCaptions_Click);
            // 
            // mnuFileSeperator10
            // 
            this.mnuFileSeperator10.Index = 8;
            this.mnuFileSeperator10.Name = "mnuFileSeperator10";
            this.mnuFileSeperator10.Text = "-";
            // 
            // mnuFileInitialDepreciation
            // 
            this.mnuFileInitialDepreciation.Index = 9;
            this.mnuFileInitialDepreciation.Name = "mnuFileInitialDepreciation";
            this.mnuFileInitialDepreciation.Text = "Enter Initial Depreciation";
            this.mnuFileInitialDepreciation.Click += new System.EventHandler(this.mnuFileInitialDepreciation_Click);
            // 
            // mnuSP1
            // 
            this.mnuSP1.Index = -1;
            this.mnuSP1.Name = "mnuSP1";
            this.mnuSP1.Text = "-";
            // 
            // mnuExit
            // 
            this.mnuExit.Index = -1;
            this.mnuExit.Name = "mnuExit";
            this.mnuExit.Text = "Exit";
            this.mnuExit.Click += new System.EventHandler(this.mnuExit_Click);
            // 
            // mnuFile
            // 
            this.mnuFile.Index = -1;
            this.mnuFile.Name = "mnuFile";
            this.mnuFile.Text = "File";
            this.mnuFile.Click += new System.EventHandler(this.mnuFile_Click);
            // 
            // mnuNew
            // 
            this.mnuNew.Index = -1;
            this.mnuNew.Name = "mnuNew";
            this.mnuNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.mnuNew.Text = "New Item                                        ";
            this.mnuNew.Click += new System.EventHandler(this.mnuNew_Click);
            // 
            // mnuDelete
            // 
            this.mnuDelete.Index = -1;
            this.mnuDelete.Name = "mnuDelete";
            this.mnuDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
            this.mnuDelete.Text = "Delete Item";
            this.mnuDelete.Click += new System.EventHandler(this.mnuDelete_Click);
            // 
            // mnuSP2
            // 
            this.mnuSP2.Index = -1;
            this.mnuSP2.Name = "mnuSP2";
            this.mnuSP2.Text = "-";
            // 
            // mnuFileRemoveDepreciation
            // 
            this.mnuFileRemoveDepreciation.Index = -1;
            this.mnuFileRemoveDepreciation.Name = "mnuFileRemoveDepreciation";
            this.mnuFileRemoveDepreciation.Shortcut = Wisej.Web.Shortcut.F4;
            this.mnuFileRemoveDepreciation.Text = "Remove Last Depreciation";
            this.mnuFileRemoveDepreciation.Click += new System.EventHandler(this.mnuFileRemoveDepreciation_Click);
            // 
            // mnuDepreciate
            // 
            this.mnuDepreciate.Index = -1;
            this.mnuDepreciate.Name = "mnuDepreciate";
            this.mnuDepreciate.Shortcut = Wisej.Web.Shortcut.F6;
            this.mnuDepreciate.Text = "Post New Depreciation";
            this.mnuDepreciate.Click += new System.EventHandler(this.mnuDepreciate_Click);
            // 
            // mnuSP3
            // 
            this.mnuSP3.Index = -1;
            this.mnuSP3.Name = "mnuSP3";
            this.mnuSP3.Text = "-";
            // 
            // mnuSave
            // 
            this.mnuSave.Index = -1;
            this.mnuSave.Name = "mnuSave";
            this.mnuSave.Shortcut = Wisej.Web.Shortcut.F11;
            this.mnuSave.Text = "Save                      ";
            this.mnuSave.Click += new System.EventHandler(this.mnuSave_Click);
            // 
            // mnuSaveExit
            // 
            this.mnuSaveExit.Index = -1;
            this.mnuSaveExit.Name = "mnuSaveExit";
            this.mnuSaveExit.Shortcut = Wisej.Web.Shortcut.F12;
            this.mnuSaveExit.Text = "Save & Exit";
            this.mnuSaveExit.Click += new System.EventHandler(this.mnuSaveExit_Click);
            // 
            // cmdNew
            // 
            this.cmdNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdNew.Location = new System.Drawing.Point(995, 29);
            this.cmdNew.Name = "cmdNew";
            this.cmdNew.Shortcut = Wisej.Web.Shortcut.CtrlN;
            this.cmdNew.Size = new System.Drawing.Size(45, 24);
            this.cmdNew.TabIndex = 89;
            this.cmdNew.Text = "New";
            this.cmdNew.Click += new System.EventHandler(this.cmdNew_Click);
            // 
            // cmdDelete
            // 
            this.cmdDelete.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdDelete.Location = new System.Drawing.Point(1044, 29);
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Shortcut = Wisej.Web.Shortcut.CtrlD;
            this.cmdDelete.Size = new System.Drawing.Size(56, 24);
            this.cmdDelete.TabIndex = 88;
            this.cmdDelete.Text = "Delete";
            this.cmdDelete.Click += new System.EventHandler(this.cmdDelete_Click);
            // 
            // cmdSave
            // 
            this.cmdSave.AppearanceKey = "acceptButton";
            this.cmdSave.Location = new System.Drawing.Point(529, 26);
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = Wisej.Web.Shortcut.F12;
            this.cmdSave.Size = new System.Drawing.Size(80, 48);
            this.cmdSave.TabIndex = 5;
            this.cmdSave.Text = "Save";
            this.cmdSave.Click += new System.EventHandler(this.cmdSave_Click);
            // 
            // cmdRemoveLast
            // 
            this.cmdRemoveLast.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdRemoveLast.Location = new System.Drawing.Point(813, 29);
            this.cmdRemoveLast.Name = "cmdRemoveLast";
            this.cmdRemoveLast.Shortcut = Wisej.Web.Shortcut.F4;
            this.cmdRemoveLast.Size = new System.Drawing.Size(178, 24);
            this.cmdRemoveLast.TabIndex = 90;
            this.cmdRemoveLast.Text = "Remove Last Depreciation";
            this.cmdRemoveLast.Click += new System.EventHandler(this.cmdRemoveLast_Click);
            // 
            // cmdPostNew
            // 
            this.cmdPostNew.Anchor = ((Wisej.Web.AnchorStyles)((Wisej.Web.AnchorStyles.Top | Wisej.Web.AnchorStyles.Right)));
            this.cmdPostNew.Location = new System.Drawing.Point(653, 29);
            this.cmdPostNew.Name = "cmdPostNew";
            this.cmdPostNew.Shortcut = Wisej.Web.Shortcut.F6;
            this.cmdPostNew.Size = new System.Drawing.Size(156, 24);
            this.cmdPostNew.TabIndex = 91;
            this.cmdPostNew.Text = "Post New Depreciation";
            this.cmdPostNew.Click += new System.EventHandler(this.cmdPostNew_Click);
            // 
            // fcLabel1
            // 
            this.fcLabel1.ForeColor = System.Drawing.Color.FromArgb(127, 143, 164);
            this.fcLabel1.Location = new System.Drawing.Point(33, 176);
            this.fcLabel1.Name = "fcLabel1";
            this.fcLabel1.Size = new System.Drawing.Size(95, 14);
            this.fcLabel1.TabIndex = 16;
            this.fcLabel1.Text = "LOCATION";
            this.fcLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmMaster
            // 
            this.ClientSize = new System.Drawing.Size(1083, 893);
            this.KeyPreview = true;
            this.Menu = this.MainMenu1;
            this.Name = "frmMaster";
            this.StartPosition = Wisej.Web.FormStartPosition.Manual;
            this.Text = "Add / Edit Assets";
            this.QueryUnload += new System.EventHandler<fecherFoundation.FCFormClosingEventArgs>(this.Form_QueryUnload);
            this.Load += new System.EventHandler(this.frmMaster_Load);
            this.Activated += new System.EventHandler(this.frmMaster_Activated);
            this.Resize += new System.EventHandler(this.frmMaster_Resize);
            this.KeyPress += new Wisej.Web.KeyPressEventHandler(this.frmMaster_KeyPress);
            this.BottomPanel.ResumeLayout(false);
            this.ClientArea.ResumeLayout(false);
            this.ClientArea.PerformLayout();
            this.TopPanel.ResumeLayout(false);
            this.TopPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.fraWarranty)).EndInit();
            this.fraWarranty.ResumeLayout(false);
            this.fraWarranty.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkDiscarded)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDiscardedDate)).EndInit();
            this.fraDiscardedDate.ResumeLayout(false);
            this.fraDiscardedDate.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDiscardedDate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSalvageValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDepreciationMethod)).EndInit();
            this.fraDepreciationMethod.ResumeLayout(false);
            this.fraDepreciationMethod.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTotalUsed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLife)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fraDepreciationsPosted)).EndInit();
            this.fraDepreciationsPosted.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.vsDepreciationsPosted)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtAcct3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkExpensedFirstYear)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chkFederalFunds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBasisCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtReplacementCost)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Image1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdNew)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDelete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdSave)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdRemoveLast)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdPostNew)).EndInit();
            this.ResumeLayout(false);

		}
		#endregion

		private System.ComponentModel.IContainer components;
		private FCButton cmdNew;
		private FCButton cmdDelete;
		private FCButton cmdSave;
		private FCButton cmdPostNew;
		private FCButton cmdRemoveLast;
        public FCComboBox cmbLocationType;
        private ComboBox cmbLocation;
        public FCLabel fcLabel1;
    }
}
