﻿//Fecher vbPorter - Version 1.0.0.27
using System;
using fecherFoundation;
using fecherFoundation.Extensions;
using Wisej.Web;
using Global;

namespace TWFA0000
{
	public class modCustomReport
	{
		// vbPorter upgrade warning: GRIDTEXT As short --> As int	OnRead(string)
		public const int GRIDTEXT = 1;
		// vbPorter upgrade warning: GRIDDATE As short --> As int	OnRead(string)
		public const int GRIDDATE = 2;
		public const int GRIDCOMBOIDTEXT = 3;
		// vbPorter upgrade warning: GRIDNUMRANGE As short --> As int	OnRead(string)
		public const int GRIDNUMRANGE = 4;
		// vbPorter upgrade warning: GRIDCOMBOIDNUM As short --> As int	OnRead(string)
		public const int GRIDCOMBOIDNUM = 5;
		// vbPorter upgrade warning: GRIDCOMBOTEXT As short --> As int	OnRead(string)
		public const int GRIDCOMBOTEXT = 6;
		// vbPorter upgrade warning: GRIDTEXTRANGE As short --> As int	OnRead(string)
		public const int GRIDTEXTRANGE = 7;
		// vbPorter upgrade warning: GRIDBOOLEAN As short --> As int	OnRead(string)
		public const int GRIDBOOLEAN = 8;
		// vbPorter upgrade warning: GRIDCOMBOIDNUMRANGE As short --> As int	OnRead(string)
		public const int GRIDCOMBOIDNUMRANGE = 9;
		public const string CUSTOMREPORTDATABASE = "Twck0000.vb1";
		// vbPorter upgrade warning: 'Return' As Variant --> As string
		public static string FixQuotes(string strValue)
		{
			string FixQuotes = "";
			FixQuotes = strValue.Replace("'", "''");
			return FixQuotes;
		}

		public static void SetColumnCaptions(dynamic FormName)
		{
			// THIS ROUTINE GETS THE CAPTIONS THAT ARE TO BE DISPLAYED ON
			// THE REPORT ITSELF
			// THIS ARRAY (strFieldCaptions) HOLDS ONLY THE FIELD NAMES THAT ARE
			// TO BE DISPLAYED WHERE THE ARRAY (strCaptions) HOLDS ALL POSSIBLE
			// FIELD NAMES
			int intCounter;
			int intCount;
			intCount = 0;
			for (intCounter = 0; intCounter <= FormName.lstFields.ListCount - 1; intCounter++)
			{
				if (FormName.lstFields.Selected(intCounter))
				{
					Statics.strFieldCaptions[intCount] = Statics.strCaptions[FormName.lstFields.ItemData(intCounter)];
					intCount += 1;
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object GetASFieldName(string strFieldName, short intSegment = 2)
		{
			object GetASFieldName = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (Strings.UCase(strTemp[1]) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					// If intSegment = 1 Then
					GetASFieldName = strTemp[0];
					// Else
					// GetASFieldName = strTemp(2)
					// End If
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					GetASFieldName = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				GetASFieldName = Strings.Trim(strTemp[0]);
			}
			return GetASFieldName;
		}

		public static void GetNumberOfFields(string strSQL)
		{
			// THIS ROUTINE GETS THE NUMBER OF FIELDS TO REPORT ON.
			// ALSO...THIS FILLS THE ARRAY (strFieldNames) WITH ALL OF THE FIELDS
			// TO SHOW IN THE REPORT. THESE ARE NOT THE DISPLAY CAPTIONS BUT THE
			// ACTUAL DATABASE FIELD NAMES TO BE USED IN THE SQL STATEMENT
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			object strTemp2;
			int intCount;
			int intCounter;
			if (Strings.Trim(strSQL) == string.Empty)
			{
				Statics.intNumberOfSQLFields = -1;
				return;
			}
			strTemp = Strings.Split(strSQL, "from", -1, CompareConstants.vbBinaryCompare);
			strSQL = Strings.Mid(strTemp[0], 7, FCConvert.ToString(strTemp[0]).Length - 6);
			strTemp = Strings.Split(strSQL, ",", -1, CompareConstants.vbBinaryCompare);
			Statics.intNumberOfSQLFields = Information.UBound((object[])strTemp, 1);
			for (intCounter = 0; intCounter <= Statics.intNumberOfSQLFields; intCounter++)
			{
				if (Strings.Trim(strTemp[intCounter]) != string.Empty)
				{
					// CHECK TO SEE IF AN ALIAS WAS USED IN THE SQL STATEMENT
					Statics.strFieldNames[intCounter] = FCConvert.ToString(CheckForAS(strTemp[intCounter]));
					// NEED TO SET THE CAPTIONS ARRAY SO THAT IF SHOWING A SAVED
					// REPORT THE CORRECT CAPTION WILL SHOW ON THE REPORT
					for (intCount = 0; intCount <= Statics.intNumberOfSQLFields; intCount++)
					{
						if (Statics.strFields[intCount] == Strings.Trim(strTemp[intCounter]))
						{
							Statics.strFieldCaptions[intCounter] = Statics.strCaptions[intCount];
							break;
						}
					}
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As object	OnWrite(object, string)
		public static object CheckForAS(string strFieldName, short intSegment = 2)
		{
			object CheckForAS = null;
			// THIS FUNCTION WILL RETURN THE SQL REFERENCE FIELD NAME.
			// IF AN ALIAS WAS USED THEN IT GETS THE ALIAS NAME AND NOT THE
			// DATABASE FIELD NAME
			// vbPorter upgrade warning: strTemp As Variant --> As string()
			string[] strTemp = null;
			if (Strings.InStr(1, strFieldName, " AS ", CompareConstants.vbTextCompare) != 0)
			{
				// SEE IF THE WORD AS IS USED TO INDICATE AN ALIAS
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				if (Strings.UCase(strTemp[1]) == "AS")
				{
					// RETURN THE SQL REFERENCE FIELD NAME
					if (intSegment == 1)
					{
						CheckForAS = strTemp[0];
					}
					else
					{
						CheckForAS = strTemp[2];
					}
				}
				else
				{
					// RETURN THE ACTUAL DATABASE FIELD NAME
					CheckForAS = strTemp[0];
				}
			}
			else
			{
				// AN ALIAS WAS NOT USED SO JUST USE THE DATABASE FIELD NAME
				strTemp = Strings.Split(Strings.Trim(strFieldName), " ", -1, CompareConstants.vbBinaryCompare);
				CheckForAS = Strings.Trim(strTemp[0]);
			}
			return CheckForAS;
		}

		public static void ClearComboListArray()
		{
			// CLEAR THE COMBO LIST ARRAY.
			// THIS IS THE ARRAY THAT HOLDS THE COMBO LIST IN THE WHERE GRID
			int intCount;
			for (intCount = 0; intCount <= 50; intCount++)
			{
				Statics.strComboList[intCount, 0] = string.Empty;
				Statics.strComboList[intCount, 1] = string.Empty;
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(frmCustomReport)
		public static void SetFormFieldCaptions(Form FormName, string ReportType)
		{
			// THIS IS THE MAIN ROUTINE THAT FILLS THE CUSTOM REPORT FORM.
			// 
			// ****************************************************************
			// THIS IS THE ONLY ROUTINE THAT NEEDS TO BE ALTERED WHEN YOU WANT
			// TO ADD A NEW REPORT TYPE.
			// ****************************************************************
			Statics.strReportType = Strings.UCase(ReportType);
			// CLEAR THE COMBO LIST ARRAY
			ClearComboListArray();
			if (Strings.UCase(ReportType) == "ASSETS")
			{
				SetAssetParameters(FormName);
			}
			else
			{
			}
			// POPULATE THE SORT LIST WITH THE SAME FIELDS AS THE FIELD LIST BOX
			LoadSortList(FormName);
			// POPULATE THE WHERE GRID WITH THE FIELDS FROM THE FIELD LIST BOX
			LoadWhereGrid(FormName);
			// LOAD THE SAVED REPORT COMBO ON THE FORM
			FCUtils.CallByName(FormName, "LoadCombo", CallType.Method);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		public static void SetAssetParameters(dynamic FormName)
		{
			Statics.strCustomTitle = "**** Custom Reports ****";
			FormName.lstFields.AddItem("Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 1);
			FormName.lstFields.AddItem("Year");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 2);
			FormName.lstFields.AddItem("Make");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 3);
			FormName.lstFields.AddItem("Model");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 4);
			FormName.lstFields.AddItem("Serial Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 5);
			FormName.lstFields.AddItem("Tag Number");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 6);
			FormName.lstFields.AddItem("Purchase Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 7);
			FormName.lstFields.AddItem("Vendor Code");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 8);
			FormName.lstFields.AddItem(modStartup.Statics.strLoc1Title + " Code");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 9);
			FormName.lstFields.AddItem(modStartup.Statics.strLoc2Title + " Code");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 10);
			FormName.lstFields.AddItem(modStartup.Statics.strLoc3Title + " Code");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 11);
			FormName.lstFields.AddItem(modStartup.Statics.strLoc4Title + " Code");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 12);
			FormName.lstFields.AddItem(modStartup.Statics.strLoc1Title + " Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 13);
			FormName.lstFields.AddItem(modStartup.Statics.strLoc2Title + " Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 14);
			FormName.lstFields.AddItem(modStartup.Statics.strLoc3Title + " Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 15);
			FormName.lstFields.AddItem(modStartup.Statics.strLoc4Title + " Description");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 16);
			FormName.lstFields.AddItem("Cost");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 17);
			FormName.lstFields.AddItem("Salvage");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 18);
			FormName.lstFields.AddItem("Current Value");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 19);
			FormName.lstFields.AddItem("Class Code");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 20);
			FormName.lstFields.AddItem("Class Desc");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 21);
			FormName.lstFields.AddItem("Dep Method");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 22);
			FormName.lstFields.AddItem("Last Dep Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 23);
			FormName.lstFields.AddItem("Warranty Exp Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 24);
			FormName.lstFields.AddItem("Warranty Desc");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 25);
			FormName.lstFields.AddItem("Fed Funds Used");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 26);
			// FormName.lstFields.AddItem "Expensed 1st Yr"
			// FormName.lstFields.ItemData(FormName.lstFields.NewIndex) = 19
			FormName.lstFields.AddItem("Total Dep");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 27);
			FormName.lstFields.AddItem("Department");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 28);
			FormName.lstFields.AddItem("Division");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 29);
			FormName.lstFields.AddItem("Purchase Acct");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 30);
			FormName.lstFields.AddItem("Asset Acct");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 31);
			FormName.lstFields.AddItem("Expense Acct");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 32);
			FormName.lstFields.AddItem("Depreciation Acct");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 33);
			FormName.lstFields.AddItem("Life");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 34);
			FormName.lstFields.AddItem("Out of Service Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 35);
			FormName.lstFields.AddItem("Discarded");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 36);
			FormName.lstFields.AddItem("Discarded Date");
			FormName.lstFields.ItemData(FormName.lstFields.NewIndex, 37);
			Statics.strFields[1] = "tblMaster.Description as MasterDescription";
			Statics.strFields[2] = "Year";
			Statics.strFields[3] = "Make";
			Statics.strFields[4] = "Model";
			Statics.strFields[5] = "SerialNumber";
			Statics.strFields[6] = "TagNumber";
			Statics.strFields[7] = "PurchaseDate";
			Statics.strFields[8] = "VendorCode";
			Statics.strFields[9] = "tblLocations.Code as Code1";
			Statics.strFields[10] = "tblLocations_1.Code as Code2";
			Statics.strFields[11] = "tblLocations_2.Code as Code3";
			Statics.strFields[12] = "tblLocations_3.Code as Code4";
			Statics.strFields[13] = "tblLocations.Description as Description1";
			Statics.strFields[14] = "tblLocations_1.Description as Description2";
			Statics.strFields[15] = "tblLocations_2.Description as Description3";
			Statics.strFields[16] = "tblLocations_3.Description as Description4";
			Statics.strFields[17] = "BasisCost";
			Statics.strFields[18] = "SalvageValue";
			Statics.strFields[19] = "TotalValue";
			Statics.strFields[20] = "tblClassCodes.Code as ClassCode";
			Statics.strFields[21] = "tblClassCodes.Description as ClassDescription";
			Statics.strFields[22] = "DepreciationMethod";
			Statics.strFields[23] = "DateLastDepreciation";
			Statics.strFields[24] = "WarrantyExpDate";
			Statics.strFields[25] = "WarrantyDescription";
			Statics.strFields[26] = "FederalFunds";
			// strFields(19) = "ExpensedFirstYear"
			Statics.strFields[27] = "TotalDepreciated";
			Statics.strFields[28] = "Dept";
			Statics.strFields[29] = "Div";
			Statics.strFields[30] = "Acct2";
			Statics.strFields[31] = "Acct4";
			Statics.strFields[32] = "Acct1";
			Statics.strFields[33] = "Acct3";
			Statics.strFields[34] = "tblMaster.Life as Life, tblMaster.LifeUnits as LifeUnits";
			Statics.strFields[35] = "OutOfServiceDate";
			Statics.strFields[36] = "Discarded";
			Statics.strFields[37] = "DiscardedDate";
			Statics.strCaptions[1] = "Description";
			Statics.strCaptions[2] = "Year";
			Statics.strCaptions[3] = "Make";
			Statics.strCaptions[4] = "Model";
			Statics.strCaptions[5] = "Serial Number";
			Statics.strCaptions[6] = "Tag Number";
			Statics.strCaptions[7] = "Purchase Date";
			Statics.strCaptions[8] = "Vendor Code";
			Statics.strCaptions[9] = "Loc1 Code";
			Statics.strCaptions[10] = "Loc2 Code";
			Statics.strCaptions[11] = "Loc3 Code";
			Statics.strCaptions[12] = "Loc4 Code";
			Statics.strCaptions[13] = "Loc1 Desc";
			Statics.strCaptions[14] = "Loc2 Desc";
			Statics.strCaptions[15] = "Loc3 Desc";
			Statics.strCaptions[16] = "Loc4 Desc";
			Statics.strCaptions[17] = "Basis";
			Statics.strCaptions[18] = "Salvage";
			Statics.strCaptions[19] = "Cur Value";
			Statics.strCaptions[20] = "Class Code";
			Statics.strCaptions[21] = "Class Desc";
			Statics.strCaptions[22] = "Dep Method";
			Statics.strCaptions[23] = "Last Dep Date";
			Statics.strCaptions[24] = "Warranty Exp Date";
			Statics.strCaptions[25] = "Warranty Desc";
			Statics.strCaptions[26] = "Fed Funds Used";
			// strCaptions(19) = "Expensed 1st Yr"
			Statics.strCaptions[27] = "Total Dep";
			Statics.strCaptions[28] = "Dept";
			Statics.strCaptions[29] = "Div";
			Statics.strCaptions[30] = "Purchase Acct";
			Statics.strCaptions[31] = "Asset Acct";
			Statics.strCaptions[32] = "Expense Acct";
			Statics.strCaptions[33] = "Depreciation Acct";
			Statics.strCaptions[34] = "Life";
			Statics.strCaptions[35] = "Out of Service Date";
			Statics.strCaptions[36] = "Discarded";
			Statics.strCaptions[37] = "Discarded Date";
			Statics.strWhereType[1] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[2] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[3] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[4] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[5] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[6] = FCConvert.ToString(GRIDTEXTRANGE);
			Statics.strWhereType[7] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[8] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[9] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[10] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[11] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[12] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[13] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[14] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[15] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[16] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[17] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[18] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[19] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[20] = FCConvert.ToString(GRIDCOMBOIDNUM);
			Statics.strWhereType[21] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[22] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[23] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[24] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[25] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[26] = FCConvert.ToString(GRIDBOOLEAN);
			// strWhereType(19) = GRIDBOOLEAN
			Statics.strWhereType[27] = FCConvert.ToString(GRIDNUMRANGE);
			Statics.strWhereType[28] = FCConvert.ToString(GRIDCOMBOIDNUMRANGE);
			Statics.strWhereType[29] = FCConvert.ToString(GRIDCOMBOTEXT);
			Statics.strWhereType[30] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[31] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[32] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[33] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[34] = FCConvert.ToString(GRIDTEXT);
			Statics.strWhereType[35] = FCConvert.ToString(GRIDDATE);
			Statics.strWhereType[36] = FCConvert.ToString(GRIDBOOLEAN);
			Statics.strWhereType[37] = FCConvert.ToString(GRIDDATE);
			// 0=LEFT 1=RIGHT 2=CENTER
			Statics.strJustify[1] = FCConvert.ToString(0);
			Statics.strJustify[2] = FCConvert.ToString(0);
			Statics.strJustify[3] = FCConvert.ToString(0);
			Statics.strJustify[4] = FCConvert.ToString(0);
			Statics.strJustify[5] = FCConvert.ToString(0);
			Statics.strJustify[6] = FCConvert.ToString(0);
			Statics.strJustify[7] = FCConvert.ToString(2);
			Statics.strJustify[8] = FCConvert.ToString(0);
			Statics.strJustify[9] = FCConvert.ToString(0);
			Statics.strJustify[10] = FCConvert.ToString(0);
			Statics.strJustify[11] = FCConvert.ToString(0);
			Statics.strJustify[12] = FCConvert.ToString(0);
			Statics.strJustify[13] = FCConvert.ToString(0);
			Statics.strJustify[14] = FCConvert.ToString(0);
			Statics.strJustify[15] = FCConvert.ToString(0);
			Statics.strJustify[16] = FCConvert.ToString(0);
			Statics.strJustify[17] = FCConvert.ToString(1);
			Statics.strJustify[18] = FCConvert.ToString(1);
			Statics.strJustify[19] = FCConvert.ToString(1);
			Statics.strJustify[20] = FCConvert.ToString(0);
			Statics.strJustify[21] = FCConvert.ToString(0);
			Statics.strJustify[22] = FCConvert.ToString(0);
			Statics.strJustify[23] = FCConvert.ToString(2);
			Statics.strJustify[24] = FCConvert.ToString(2);
			Statics.strJustify[25] = FCConvert.ToString(0);
			Statics.strJustify[26] = FCConvert.ToString(0);
			// strJustify(19) = 0
			Statics.strJustify[27] = FCConvert.ToString(1);
			Statics.strJustify[28] = FCConvert.ToString(0);
			Statics.strJustify[29] = FCConvert.ToString(0);
			Statics.strJustify[30] = FCConvert.ToString(0);
			Statics.strJustify[31] = FCConvert.ToString(0);
			Statics.strJustify[32] = FCConvert.ToString(0);
			Statics.strJustify[33] = FCConvert.ToString(0);
			Statics.strJustify[34] = FCConvert.ToString(2);
			Statics.strJustify[35] = FCConvert.ToString(2);
			Statics.strJustify[36] = FCConvert.ToString(0);
			Statics.strJustify[37] = FCConvert.ToString(2);
			if (modVariables.Statics.boolVendorsFromBud)
			{
				LoadGridCellAsCombo_5832(FormName, 8, "Select * from VendorMaster WHERE Status <> 'D' Order by VendorNumber", "CheckName", "VendorNumber", "VendorCode", true, "TWBD0000.vb1");
			}
			else
			{
				LoadGridCellAsCombo_1458(FormName, 8, "Select * from tblVendors Order by code", "Description", "Code", "VendorCode", true);
			}
			LoadGridCellAsCombo(FormName, 9, "Select * from tblLocations Where TypeID = 1 Order by code", "Code", "ID", "tblLocations.ID");
			LoadGridCellAsCombo(FormName, 10, "Select * from tblLocations Where TypeID = 2 Order by code", "Code", "ID", "tblLocations_1.ID");
			LoadGridCellAsCombo(FormName, 11, "Select * from tblLocations Where TypeID = 3 Order by code", "Code", "ID", "tblLocations_2.ID");
			LoadGridCellAsCombo(FormName, 12, "Select * from tblLocations Where TypeID = 4 Order by code", "Code", "ID", "tblLocations_3.ID");
			LoadGridCellAsCombo(FormName, 13, "Select * from tblLocations Where TypeID = 1 Order by Description", "Description", "ID", "tblLocations.Description");
			LoadGridCellAsCombo(FormName, 14, "Select * from tblLocations Where TypeID = 2 Order by Description", "Description", "ID", "tblLocations_1.Description");
			LoadGridCellAsCombo(FormName, 15, "Select * from tblLocations Where TypeID = 3 Order by Description", "Description", "ID", "tblLocations_2.Description");
			LoadGridCellAsCombo(FormName, 16, "Select * from tblLocations Where TypeID = 4 Order by Description", "Description", "ID", "tblLocations_3.Description");
			LoadGridCellAsCombo(FormName, 20, "#A;A" + "\t" + "All Codes|#S;S" + "\t" + "Selected Codes");
			LoadGridCellAsCombo(FormName, 26, "#0;False|#1;True");
			LoadGridCellAsCombo(FormName, 36, "#0;False|#1;True");
			if (modVariables.Statics.boolDeptDivFromBud)
			{
				LoadBudGridCellAsCombo(FormName, 28, "Select Department, ID from DeptDivTitles WHERE convert(int, Division) = 0 Order by Department", "Department", "ID", "Dept");
			}
			else
			{
				LoadDistinctGridCellAsCombo(FormName, 28, "Select distinct tblDeptDiv.* from tblDeptDiv Order by Dept", "Dept", "ID", "Dept");
			}
			if (modVariables.Statics.boolDeptDivFromBud)
			{
				LoadBudGridCellAsCombo(FormName, 29, "Select DISTINCT Division from DeptDivTitles WHERE convert(int, Division) <> 0 Order by Division", "Division", "Division", "Div");
			}
			else
			{
				LoadDistinctGridCellAsCombo(FormName, 29, "Select distinct tblDeptDiv.* from tblDeptDiv Order by Dept,Div", "Div", "ID", "Div");
			}
			FormName.fraFields.Tag = "From ((((tblMaster LEFT JOIN tblLocations ON tblMaster.Location1Code = tblLocations.ID) LEFT JOIN tblLocations AS tblLocations_1 ON tblMaster.Location2Code = tblLocations_1.ID) LEFT JOIN tblLocations AS tblLocations_2 ON tblMaster.Location3Code = tblLocations_2.ID) LEFT JOIN tblLocations AS tblLocations_3 ON tblMaster.Location4Code = tblLocations_3.ID) LEFT JOIN tblClassCodes ON convert(int, tblMaster.ClassCode) = tblClassCodes.ID";
		}

		public static void SetHeaderGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE HEADINGS
			switch (Strings.UCase(Statics.strReportType))
			{
			// Case "DOGS"
			// 
			// 
			// Case "BIRTHS"
				default:
					{
						// GridName.BackColor = vbBlue
						// GridName.ForeColor = vbWhite
						break;
					}
			}
			//end switch
			// GridName.Select 0, 0, 0, GridName.Cols - 1
			// GridName.CellFontBold = True
		}

		public static void SetDataGridProperties(ref object GridName)
		{
			// THIS ROUTINE CAN BE USED IF YOU WANT SPECIFIC GRID FORMATTING DONE
			// THIS IS FORMATTING FOR THE GRID THAT DISPLAYS THE REPORTS DATA
			//switch (Strings.UCase(strReportType)) {
			//		// Case "DOGS"
			//		// Case "BIRTHS"
			//		break;
			//	}
			//} //end switch
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadSortList(dynamic FormName)
		{
			// LOAD THE SORT LIST BOX WITH THE FIELDS FROM THE FIELDS LIST BOX
			int intCounter;
			for (intCounter = 0; intCounter <= FormName.lstFields.Items.Count - 1; intCounter++)
			{
				FormName.lstSort.AddItem(FormName.lstFields.List(intCounter));
				FormName.lstSort.ItemData(FormName.lstSort.NewIndex, FormName.lstFields.ItemData(intCounter));
            }
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadWhereGrid(dynamic FormName)
		{
			// LOAD THE WHERE GRID WITH FIELDS FROM THE FIELD LIST BOX
			int intCounter;
			for (intCounter = 1; intCounter <= FormName.lstFields.ListCount; intCounter++)
			{
				FormName.vsWhere.AddItem(FormName.lstFields.List(intCounter - 1));
				// IF THE TYPE OF THE FIELD IS A DATE THEN WE WANT TO ALLOW
				// THE USER TO PUT IN A RANGE OF DATES SO WE NEED TO ALLOW
				// THEM TWO FIELDS UNLIKE ALL OTHER TYPES WHERE WE ONLY WANT ONE
				if (FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDDATE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDNUMRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDTEXTRANGE && FCConvert.ToDouble(Statics.strWhereType[intCounter]) != GRIDCOMBOIDNUMRANGE)
				{
					FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, FormName.vsWhere.Rows - 1, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
				}
			}
			FormName.vsWhere.FixedRows = 1;
			FormName.vsWhere.TextMatrix(0, 0, "Field");
			FormName.vsWhere.TextMatrix(0, 1, "From");
			FormName.vsWhere.TextMatrix(0, 2, "To");
			//FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpBackColor, 0, 0, 0, 2, modGlobalConstants.Statics.TRIOCOLORGRAYBACKGROUND);
			//FormName.vsWhere.Cell(FCGrid.CellPropertySettings.flexcpAlignment, 0, 0, 0, 2, FCGrid.AlignmentSettings.flexAlignCenterCenter);
			// SET THE NUMBER OF COLUMNS AND THE WIDTH OF THOSE COLUMNS
			// THE LAST COLUMN IS THERE TO HOLD ID VALUES IF THE TYPE IS OF
			// A COMBO BOX BECAUSE THE LIST DOES NOT STAY WITH THAT CELL ONCE
			// THE USER MOVES TO A NEW ONE.
			FormName.vsWhere.Cols = 4;
			FormName.vsWhere.ColWidth(0, 3500);
			FormName.vsWhere.ColWidth(1, 1600);
			FormName.vsWhere.ColWidth(2, 1600);
			FormName.vsWhere.ColWidth(3, 0);
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadGridCellAsCombo_1458(Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "", bool ShowMore = false, string strDatabase = modGlobal.DEFAULTDATABASENAME)
		{
			LoadGridCellAsCombo(FormName, intRowNumber, strSQL, FieldName, IDField, DatabaseFieldName, ShowMore, strDatabase);
		}

		private static void LoadGridCellAsCombo_5832(Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "", bool ShowMore = false, string strDatabase = modGlobal.DEFAULTDATABASENAME)
		{
			LoadGridCellAsCombo(FormName, intRowNumber, strSQL, FieldName, IDField, DatabaseFieldName, ShowMore, strDatabase);
		}

		private static void LoadGridCellAsCombo(Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "", bool ShowMore = false, string strDatabase = modGlobal.DEFAULTDATABASENAME)
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, strDatabase);
				if (!ShowMore)
				{
					while (!rsCombo.EndOfFile())
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
						Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
						rsCombo.MoveNext();
					}
				}
				else
				{
					Statics.strComboList[intRowNumber, 0] += "|";
					Statics.strComboList[intRowNumber, 0] += "#0;0" + "\t" + "Temporary Vendors";
					while (!rsCombo.EndOfFile())
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(IDField) + "\t" + rsCombo.Get_Fields(FieldName);
						rsCombo.MoveNext();
					}
				}
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadBudGridCellAsCombo(Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, modGlobal.DEFAULTbdDATABASENAME);
				while (!rsCombo.EndOfFile())
				{
					Statics.strComboList[intRowNumber, 0] += "|";
					Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
					Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
					rsCombo.MoveNext();
				}
			}
		}
		// vbPorter upgrade warning: FormName As Form	OnWrite(Form)
		private static void LoadDistinctGridCellAsCombo(Form FormName, int intRowNumber, string strSQL, string FieldName = "", string IDField = "", string DatabaseFieldName = "")
		{
			// THIS ROUTINE BUILDS A COMBO LIST THAT MAKE THE CELL WORK LIKE A
			// COMBO BOX ONCE THE USER CLICKS ON IT. THE LIST ARE GOVERNED BY THE
			// ROW THAT IT IS IN THE GIRD.
			// THE SECOND ELEMENT IN THE ARRAY HOLDS THE DATABASE FIELD NAME SO
			// WHEN THE WHERE CLAUSE IS BUILT IT WILL KNOW WHAT FIELD NAME TO USE
			string strValue = "";
			if (Information.IsNothing(FieldName) || FieldName == string.Empty)
			{
				Statics.strComboList[intRowNumber, 0] = strSQL;
				// ElseIf UCase(FieldName) = "LISTTAB" Then
			}
			else
			{
				int intCounter;
				clsDRWrapper rsCombo = new clsDRWrapper();
				rsCombo.OpenRecordset(strSQL, modGlobal.DEFAULTDATABASENAME);
				while (!rsCombo.EndOfFile())
				{
					if (FCConvert.ToString(rsCombo.Get_Fields(FieldName)) != strValue)
					{
						Statics.strComboList[intRowNumber, 0] += "|";
						Statics.strComboList[intRowNumber, 0] += "#" + rsCombo.Get_Fields(IDField) + ";" + rsCombo.Get_Fields(FieldName);
						Statics.strComboList[intRowNumber, 1] = DatabaseFieldName;
					}
					strValue = FCConvert.ToString(rsCombo.Get_Fields(FieldName));
					rsCombo.MoveNext();
				}
			}
		}
		// vbPorter upgrade warning: 'Return' As Variant --> As bool
		public static bool IsValidDate(string DateToCheck)
		{
			bool IsValidDate = false;
			string strAnnTemp;
			// vbPorter upgrade warning: intMonth As short --> As int	OnWrite(string)
			int intMonth;
			// hold month
			// vbPorter upgrade warning: intDay As short --> As int	OnWrite(string)
			int intDay;
			// hold day
			int intYear;
			// hold year
			int intCheckFormat;
			// make sure date is format 'mm/dd'
			strAnnTemp = DateToCheck;
			intMonth = FCConvert.ToInt32(Strings.Left(strAnnTemp, 2));
			intDay = FCConvert.ToInt32(Strings.Mid(strAnnTemp, 4, 2));
			switch (intMonth)
			{
				case 2:
					{
						// feb can't be more then 29 days
						if (intDay > 29)
						{
							MessageBox.Show("Invalid Month on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 1:
				case 3:
				case 5:
				case 7:
				case 8:
				case 10:
				case 12:
					{
						// these months no more then 31 days
						if (intDay > 31)
						{
							MessageBox.Show("Invalid day on  Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				case 4:
				case 6:
				case 9:
				case 11:
					{
						// these months no more then 30 days
						if (intDay > 30)
						{
							MessageBox.Show("Invalid day on Date", "ErTRIO Softwarevror", MessageBoxButtons.OK, MessageBoxIcon.Information);
							return IsValidDate;
						}
						break;
					}
				default:
					{
						// not even a month
						MessageBox.Show("Invalid Month on Date", "TRIO Software", MessageBoxButtons.OK, MessageBoxIcon.Information);
						return IsValidDate;
					}
			}
			//end switch
			IsValidDate = true;
			return IsValidDate;
		}

		public class StaticVariables
		{
			//=========================================================
			public int intNumberOfSQLFields;
			public int[] strFieldWidth = new int[50 + 1];
			public string strCustomSQL = "";
			public string strColumnCaptions = "";
			public string[] strFields = new string[50 + 1];
			public string[] strFieldNames = new string[50 + 1];
			public string[] strFieldCaptions = new string[50 + 1];
			public string[] strCaptions = new string[50 + 1];
			public string strCustomTitle = string.Empty;
			public string strReportType = string.Empty;
			public string[,] strComboList = new string[50 + 1, 50 + 1];
			// vbPorter upgrade warning: strWhereType As string	OnWriteFCConvert.ToInt32(
			public string[] strWhereType = new string[50 + 1];
			// vbPorter upgrade warning: strJustify As string	OnWriteFCConvert.ToInt16(
			public string[] strJustify = new string[50 + 1];
			public string strReportHeading = "";
		}

		public static StaticVariables Statics
		{
			get
			{
				return (StaticVariables)fecherFoundation.Sys.GetInstance(typeof(StaticVariables));
			}
		}
	}
}
