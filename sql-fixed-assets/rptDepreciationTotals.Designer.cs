﻿namespace TWFA0000
{
	/// <summary>
	/// Summary description for rptDepreciationTotals.
	/// </summary>
	partial class rptDepreciationTotals
	{
		#region ActiveReport Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rptDepreciationTotals));
			this.Detail = new GrapeCity.ActiveReports.SectionReportModel.Detail();
			this.fldDescription = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalDepreciated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldPurchaseDate = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClass = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTag = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.PageHeader = new GrapeCity.ActiveReports.SectionReportModel.PageHeader();
			this.Label1 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label7 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label2 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label4 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label3 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblDateRange = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label9 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label12 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Line1 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.Label13 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label14 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label15 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblPurchaseDate = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Label18 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.lblClassCodes = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.PageFooter = new GrapeCity.ActiveReports.SectionReportModel.PageFooter();
			this.GroupHeader1 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.GroupFooter1 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line2 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldTotalDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalTotalDepreciated = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldTotalCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.Label17 = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.GroupHeader2 = new GrapeCity.ActiveReports.SectionReportModel.GroupHeader();
			this.lblClass = new GrapeCity.ActiveReports.SectionReportModel.Label();
			this.Binder = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.GroupFooter2 = new GrapeCity.ActiveReports.SectionReportModel.GroupFooter();
			this.Line3 = new GrapeCity.ActiveReports.SectionReportModel.Line();
			this.fldClassDepreciation = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassTotalDeprecition = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.fldClassCost = new GrapeCity.ActiveReports.SectionReportModel.TextBox();
			this.lblClassTotal = new GrapeCity.ActiveReports.SectionReportModel.Label();
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTag)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPurchaseDate)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblClassCodes)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalDepreciated)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblClass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassDepreciation)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassTotalDeprecition)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassCost)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.lblClassTotal)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
			// 
			// Detail
			// 
			this.Detail.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.fldDescription,
				this.fldDepreciation,
				this.fldTotalDepreciated,
				this.fldCost,
				this.fldPurchaseDate,
				this.fldClass,
				this.fldTag
			});
			this.Detail.Height = 0.1979167F;
			this.Detail.Name = "Detail";
			this.Detail.Format += new System.EventHandler(this.Detail_Format);
			// 
			// fldDescription
			// 
			this.fldDescription.Height = 0.1875F;
			this.fldDescription.Left = 0.15625F;
			this.fldDescription.Name = "fldDescription";
			this.fldDescription.Style = "font-family: \'Tahoma\'; font-size: 8pt; ddo-char-set: 1";
			this.fldDescription.Text = "Field1";
			this.fldDescription.Top = 0F;
			this.fldDescription.Width = 2.34375F;
			// 
			// fldDepreciation
			// 
			this.fldDepreciation.Height = 0.1875F;
			this.fldDepreciation.Left = 5.96875F;
			this.fldDepreciation.Name = "fldDepreciation";
			this.fldDepreciation.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldDepreciation.Text = "Field1";
			this.fldDepreciation.Top = 0F;
			this.fldDepreciation.Width = 0.9375F;
			// 
			// fldTotalDepreciated
			// 
			this.fldTotalDepreciated.Height = 0.1875F;
			this.fldTotalDepreciated.Left = 6.96875F;
			this.fldTotalDepreciated.Name = "fldTotalDepreciated";
			this.fldTotalDepreciated.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldTotalDepreciated.Text = "Field1";
			this.fldTotalDepreciated.Top = 0F;
			this.fldTotalDepreciated.Width = 0.9375F;
			// 
			// fldCost
			// 
			this.fldCost.Height = 0.1875F;
			this.fldCost.Left = 4.96875F;
			this.fldCost.Name = "fldCost";
			this.fldCost.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: right; ddo-char-set: 1";
			this.fldCost.Text = "Field1";
			this.fldCost.Top = 0F;
			this.fldCost.Width = 0.9375F;
			// 
			// fldPurchaseDate
			// 
			this.fldPurchaseDate.Height = 0.1875F;
			this.fldPurchaseDate.Left = 4.125F;
			this.fldPurchaseDate.Name = "fldPurchaseDate";
			this.fldPurchaseDate.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: center; ddo-char-set: 1";
			this.fldPurchaseDate.Text = "Field1";
			this.fldPurchaseDate.Top = 0F;
			this.fldPurchaseDate.Width = 0.8125F;
			// 
			// fldClass
			// 
			this.fldClass.Height = 0.1875F;
			this.fldClass.Left = 3.25F;
			this.fldClass.Name = "fldClass";
			this.fldClass.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldClass.Text = "Field1";
			this.fldClass.Top = 0F;
			this.fldClass.Width = 0.84375F;
			// 
			// fldTag
			// 
			this.fldTag.Height = 0.1875F;
			this.fldTag.Left = 2.5625F;
			this.fldTag.Name = "fldTag";
			this.fldTag.Style = "font-family: \'Tahoma\'; font-size: 8pt; text-align: left; ddo-char-set: 1";
			this.fldTag.Text = "Field1";
			this.fldTag.Top = 0F;
			this.fldTag.Width = 0.625F;
			// 
			// PageHeader
			// 
			this.PageHeader.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Label1,
				this.Label7,
				this.Label2,
				this.Label4,
				this.Label3,
				this.lblDateRange,
				this.Label9,
				this.Label12,
				this.Line1,
				this.Label13,
				this.Label14,
				this.Label15,
				this.lblPurchaseDate,
				this.Label18,
				this.lblClassCodes
			});
			this.PageHeader.Height = 0.9583333F;
			this.PageHeader.Name = "PageHeader";
			this.PageHeader.Format += new System.EventHandler(this.PageHeader_Format);
			// 
			// Label1
			// 
			this.Label1.Height = 0.21875F;
			this.Label1.HyperLink = null;
			this.Label1.Left = 1.5F;
			this.Label1.Name = "Label1";
			this.Label1.Style = "font-family: \'Tahoma\'; font-size: 12pt; font-weight: bold; text-align: center; dd" + "o-char-set: 1";
			this.Label1.Text = "Depreciation Totals";
			this.Label1.Top = 0F;
			this.Label1.Width = 5.09375F;
			// 
			// Label7
			// 
			this.Label7.Height = 0.1875F;
			this.Label7.HyperLink = null;
			this.Label7.Left = 0F;
			this.Label7.Name = "Label7";
			this.Label7.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label7.Text = "Label7";
			this.Label7.Top = 0.1875F;
			this.Label7.Width = 1.5F;
			// 
			// Label2
			// 
			this.Label2.Height = 0.1875F;
			this.Label2.HyperLink = null;
			this.Label2.Left = 0F;
			this.Label2.Name = "Label2";
			this.Label2.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label2.Text = "Label2";
			this.Label2.Top = 0F;
			this.Label2.Width = 1.5F;
			// 
			// Label4
			// 
			this.Label4.Height = 0.1875F;
			this.Label4.HyperLink = null;
			this.Label4.Left = 6.59375F;
			this.Label4.Name = "Label4";
			this.Label4.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label4.Text = "Label4";
			this.Label4.Top = 0.1875F;
			this.Label4.Width = 1.3125F;
			// 
			// Label3
			// 
			this.Label3.Height = 0.1875F;
			this.Label3.HyperLink = null;
			this.Label3.Left = 6.59375F;
			this.Label3.Name = "Label3";
			this.Label3.Style = "font-family: \'Tahoma\'; font-size: 10pt; text-align: right; ddo-char-set: 1";
			this.Label3.Text = "Label3";
			this.Label3.Top = 0F;
			this.Label3.Width = 1.3125F;
			// 
			// lblDateRange
			// 
			this.lblDateRange.Height = 0.1875F;
			this.lblDateRange.HyperLink = null;
			this.lblDateRange.Left = 1.5F;
			this.lblDateRange.Name = "lblDateRange";
			this.lblDateRange.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblDateRange.Text = "Label6";
			this.lblDateRange.Top = 0.21875F;
			this.lblDateRange.Width = 5.09375F;
			// 
			// Label9
			// 
			this.Label9.Height = 0.1875F;
			this.Label9.HyperLink = null;
			this.Label9.Left = 0.1875F;
			this.Label9.Name = "Label9";
			this.Label9.Style = "font-family: \'Tahoma\'; ddo-char-set: 1";
			this.Label9.Text = "Description";
			this.Label9.Top = 0.75F;
			this.Label9.Width = 2.3125F;
			// 
			// Label12
			// 
			this.Label12.Height = 0.1875F;
			this.Label12.HyperLink = null;
			this.Label12.Left = 6F;
			this.Label12.Name = "Label12";
			this.Label12.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label12.Text = "Curr Dep";
			this.Label12.Top = 0.75F;
			this.Label12.Width = 0.9375F;
			// 
			// Line1
			// 
			this.Line1.Height = 0F;
			this.Line1.Left = 0.0625F;
			this.Line1.LineWeight = 1F;
			this.Line1.Name = "Line1";
			this.Line1.Top = 0.9375F;
			this.Line1.Width = 7.8125F;
			this.Line1.X1 = 0.0625F;
			this.Line1.X2 = 7.875F;
			this.Line1.Y1 = 0.9375F;
			this.Line1.Y2 = 0.9375F;
			// 
			// Label13
			// 
			this.Label13.Height = 0.1875F;
			this.Label13.HyperLink = null;
			this.Label13.Left = 7F;
			this.Label13.Name = "Label13";
			this.Label13.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label13.Text = "Total Dep";
			this.Label13.Top = 0.75F;
			this.Label13.Width = 0.9375F;
			// 
			// Label14
			// 
			this.Label14.Height = 0.1875F;
			this.Label14.HyperLink = null;
			this.Label14.Left = 3.25F;
			this.Label14.Name = "Label14";
			this.Label14.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label14.Text = "Class";
			this.Label14.Top = 0.75F;
			this.Label14.Width = 0.875F;
			// 
			// Label15
			// 
			this.Label15.Height = 0.1875F;
			this.Label15.HyperLink = null;
			this.Label15.Left = 5F;
			this.Label15.Name = "Label15";
			this.Label15.Style = "font-family: \'Tahoma\'; text-align: right; ddo-char-set: 1";
			this.Label15.Text = "Cost";
			this.Label15.Top = 0.75F;
			this.Label15.Width = 0.9375F;
			// 
			// lblPurchaseDate
			// 
			this.lblPurchaseDate.Height = 0.1875F;
			this.lblPurchaseDate.HyperLink = null;
			this.lblPurchaseDate.Left = 4.125F;
			this.lblPurchaseDate.Name = "lblPurchaseDate";
			this.lblPurchaseDate.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblPurchaseDate.Text = "Pur. Date";
			this.lblPurchaseDate.Top = 0.75F;
			this.lblPurchaseDate.Width = 0.8125F;
			// 
			// Label18
			// 
			this.Label18.Height = 0.1875F;
			this.Label18.HyperLink = null;
			this.Label18.Left = 2.5625F;
			this.Label18.Name = "Label18";
			this.Label18.Style = "font-family: \'Tahoma\'; text-align: left; ddo-char-set: 1";
			this.Label18.Text = "Tag #";
			this.Label18.Top = 0.75F;
			this.Label18.Width = 0.625F;
			// 
			// lblClassCodes
			// 
			this.lblClassCodes.Height = 0.1875F;
			this.lblClassCodes.HyperLink = null;
			this.lblClassCodes.Left = 1.5F;
			this.lblClassCodes.Name = "lblClassCodes";
			this.lblClassCodes.Style = "font-family: \'Tahoma\'; text-align: center; ddo-char-set: 1";
			this.lblClassCodes.Text = "Label6";
			this.lblClassCodes.Top = 0.40625F;
			this.lblClassCodes.Width = 5.125F;
			// 
			// PageFooter
			// 
			this.PageFooter.Height = 0F;
			this.PageFooter.Name = "PageFooter";
			// 
			// GroupHeader1
			// 
			this.GroupHeader1.Height = 0F;
			this.GroupHeader1.Name = "GroupHeader1";
			// 
			// GroupFooter1
			// 
			this.GroupFooter1.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line2,
				this.fldTotalDepreciation,
				this.fldTotalTotalDepreciated,
				this.fldTotalCost,
				this.Label17
			});
			this.GroupFooter1.Name = "GroupFooter1";
			this.GroupFooter1.Format += new System.EventHandler(this.GroupFooter1_Format);
			// 
			// Line2
			// 
			this.Line2.Height = 0F;
			this.Line2.Left = 4F;
			this.Line2.LineWeight = 1F;
			this.Line2.Name = "Line2";
			this.Line2.Top = 0F;
			this.Line2.Width = 3.90625F;
			this.Line2.X1 = 4F;
			this.Line2.X2 = 7.90625F;
			this.Line2.Y1 = 0F;
			this.Line2.Y2 = 0F;
			// 
			// fldTotalDepreciation
			// 
			this.fldTotalDepreciation.Height = 0.1875F;
			this.fldTotalDepreciation.Left = 5.96875F;
			this.fldTotalDepreciation.Name = "fldTotalDepreciation";
			this.fldTotalDepreciation.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalDepreciation.Text = "Field1";
			this.fldTotalDepreciation.Top = 0.03125F;
			this.fldTotalDepreciation.Width = 0.9375F;
			// 
			// fldTotalTotalDepreciated
			// 
			this.fldTotalTotalDepreciated.Height = 0.1875F;
			this.fldTotalTotalDepreciated.Left = 6.96875F;
			this.fldTotalTotalDepreciated.Name = "fldTotalTotalDepreciated";
			this.fldTotalTotalDepreciated.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalTotalDepreciated.Text = "Field1";
			this.fldTotalTotalDepreciated.Top = 0.03125F;
			this.fldTotalTotalDepreciated.Width = 0.9375F;
			// 
			// fldTotalCost
			// 
			this.fldTotalCost.Height = 0.1875F;
			this.fldTotalCost.Left = 4.96875F;
			this.fldTotalCost.Name = "fldTotalCost";
			this.fldTotalCost.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldTotalCost.Text = "Field1";
			this.fldTotalCost.Top = 0.03125F;
			this.fldTotalCost.Width = 0.9375F;
			// 
			// Label17
			// 
			this.Label17.Height = 0.1875F;
			this.Label17.HyperLink = null;
			this.Label17.Left = 4F;
			this.Label17.Name = "Label17";
			this.Label17.Style = "font-size: 8pt; font-weight: bold";
			this.Label17.Text = "Final Totals:";
			this.Label17.Top = 0.03125F;
			this.Label17.Width = 0.90625F;
			// 
			// GroupHeader2
			// 
			this.GroupHeader2.CanShrink = true;
			this.GroupHeader2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.lblClass,
				this.Binder
			});
			this.GroupHeader2.DataField = "Binder";
			this.GroupHeader2.Height = 0.1979167F;
			this.GroupHeader2.Name = "GroupHeader2";
			this.GroupHeader2.Format += new System.EventHandler(this.GroupHeader2_Format);
			// 
			// lblClass
			// 
			this.lblClass.Height = 0.1875F;
			this.lblClass.HyperLink = null;
			this.lblClass.Left = 0F;
			this.lblClass.Name = "lblClass";
			this.lblClass.Style = "font-family: \'Tahoma\'; font-weight: bold; ddo-char-set: 1";
			this.lblClass.Text = "Label19";
			this.lblClass.Top = 0F;
			this.lblClass.Width = 2.65625F;
			// 
			// Binder
			// 
			this.Binder.DataField = "Binder";
			this.Binder.Height = 0.125F;
			this.Binder.Left = 5.03125F;
			this.Binder.Name = "Binder";
			this.Binder.Text = "Field1";
			this.Binder.Top = 0.03125F;
			this.Binder.Visible = false;
			this.Binder.Width = 0.96875F;
			// 
			// GroupFooter2
			// 
			this.GroupFooter2.CanShrink = true;
			this.GroupFooter2.Controls.AddRange(new GrapeCity.ActiveReports.SectionReportModel.ARControl[] {
				this.Line3,
				this.fldClassDepreciation,
				this.fldClassTotalDeprecition,
				this.fldClassCost,
				this.lblClassTotal
			});
			this.GroupFooter2.Name = "GroupFooter2";
			this.GroupFooter2.Format += new System.EventHandler(this.GroupFooter2_Format);
			// 
			// Line3
			// 
			this.Line3.Height = 0F;
			this.Line3.Left = 4F;
			this.Line3.LineWeight = 1F;
			this.Line3.Name = "Line3";
			this.Line3.Top = 0F;
			this.Line3.Width = 3.90625F;
			this.Line3.X1 = 4F;
			this.Line3.X2 = 7.90625F;
			this.Line3.Y1 = 0F;
			this.Line3.Y2 = 0F;
			// 
			// fldClassDepreciation
			// 
			this.fldClassDepreciation.Height = 0.1875F;
			this.fldClassDepreciation.Left = 5.96875F;
			this.fldClassDepreciation.Name = "fldClassDepreciation";
			this.fldClassDepreciation.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldClassDepreciation.Text = "Field1";
			this.fldClassDepreciation.Top = 0.03125F;
			this.fldClassDepreciation.Width = 0.9375F;
			// 
			// fldClassTotalDeprecition
			// 
			this.fldClassTotalDeprecition.Height = 0.1875F;
			this.fldClassTotalDeprecition.Left = 6.96875F;
			this.fldClassTotalDeprecition.Name = "fldClassTotalDeprecition";
			this.fldClassTotalDeprecition.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldClassTotalDeprecition.Text = "Field1";
			this.fldClassTotalDeprecition.Top = 0.03125F;
			this.fldClassTotalDeprecition.Width = 0.9375F;
			// 
			// fldClassCost
			// 
			this.fldClassCost.Height = 0.1875F;
			this.fldClassCost.Left = 4.96875F;
			this.fldClassCost.Name = "fldClassCost";
			this.fldClassCost.Style = "font-family: \'Tahoma\'; font-size: 8pt; font-weight: bold; text-align: right; ddo-" + "char-set: 1";
			this.fldClassCost.Text = "Field1";
			this.fldClassCost.Top = 0.03125F;
			this.fldClassCost.Width = 0.9375F;
			// 
			// lblClassTotal
			// 
			this.lblClassTotal.Height = 0.1875F;
			this.lblClassTotal.HyperLink = null;
			this.lblClassTotal.Left = 4F;
			this.lblClassTotal.Name = "lblClassTotal";
			this.lblClassTotal.Style = "font-size: 8pt; font-weight: bold";
			this.lblClassTotal.Text = "Class Totals:";
			this.lblClassTotal.Top = 0.03125F;
			this.lblClassTotal.Width = 0.90625F;
			// 
			// rptDepreciationTotals
			// 
			this.MasterReport = false;
			this.PageSettings.Margins.Bottom = 0.5F;
			this.PageSettings.Margins.Left = 0.25F;
			this.PageSettings.Margins.Right = 0F;
			this.PageSettings.Margins.Top = 0.5F;
			this.PageSettings.PaperHeight = 11F;
			this.PageSettings.PaperWidth = 8.5F;
			this.PrintWidth = 7.9375F;
			this.ScriptLanguage = "VB.NET";
			this.Sections.Add(this.PageHeader);
			this.Sections.Add(this.GroupHeader1);
			this.Sections.Add(this.GroupHeader2);
			this.Sections.Add(this.Detail);
			this.Sections.Add(this.GroupFooter2);
			this.Sections.Add(this.GroupFooter1);
			this.Sections.Add(this.PageFooter);
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule(resources.GetString("$this.StyleSheet"), "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 16pt; font-size-adjust: inherit; font-stretch: inherit", "Heading1", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'Times New Roman\'; font-style: italic; font-variant: inherit; font-w" + "eight: bold; font-size: 14pt; font-size-adjust: inherit; font-stretch: inherit", "Heading2", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("font-family: \'inherit\'; font-style: inherit; font-variant: inherit; font-weight: " + "bold; font-size: 13pt; font-size-adjust: inherit; font-stretch: inherit", "Heading3", "Normal"));
			this.StyleSheet.Add(new DDCssLib.StyleSheetRule("", "Heading4", "Normal"));
			this.ReportEndedAndCanceled += new System.EventHandler(this.rptDepreciationTotals_ReportEndedAndCanceled);
			this.FetchData += new GrapeCity.ActiveReports.SectionReport.FetchEventHandler(this.ActiveReport_FetchData);
			this.ReportStart += new System.EventHandler(this.ActiveReport_ReportStart);
			this.DataInitialize += new System.EventHandler(this.ActiveReport_DataInitialize);
			((System.ComponentModel.ISupportInitialize)(this.fldDescription)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldPurchaseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTag)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label7)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblDateRange)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label9)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label12)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label13)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label14)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label15)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblPurchaseDate)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label18)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblClassCodes)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalTotalDepreciated)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldTotalCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Label17)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblClass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Binder)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassDepreciation)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassTotalDeprecition)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.fldClassCost)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.lblClassTotal)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this)).EndInit();
		}
		#endregion

		private GrapeCity.ActiveReports.SectionReportModel.Detail Detail;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDescription;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDepreciated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldCost;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldPurchaseDate;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTag;
		private GrapeCity.ActiveReports.SectionReportModel.PageHeader PageHeader;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label7;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label2;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label4;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label3;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblDateRange;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label9;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label12;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line1;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label13;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label14;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label15;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblPurchaseDate;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label18;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblClassCodes;
		private GrapeCity.ActiveReports.SectionReportModel.PageFooter PageFooter;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader1;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter1;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line2;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalTotalDepreciated;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldTotalCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label Label17;
		private GrapeCity.ActiveReports.SectionReportModel.GroupHeader GroupHeader2;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblClass;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox Binder;
		private GrapeCity.ActiveReports.SectionReportModel.GroupFooter GroupFooter2;
		private GrapeCity.ActiveReports.SectionReportModel.Line Line3;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassDepreciation;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassTotalDeprecition;
		private GrapeCity.ActiveReports.SectionReportModel.TextBox fldClassCost;
		private GrapeCity.ActiveReports.SectionReportModel.Label lblClassTotal;
	}
}
